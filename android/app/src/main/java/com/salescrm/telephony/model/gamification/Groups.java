package com.salescrm.telephony.model.gamification;

/**
 * Created by bannhi on 19/12/17.
 */

public class Groups {

    int id;
    String groupName;

    public Groups(int id, String groupName){
        this.id = id;
        this.groupName = groupName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
