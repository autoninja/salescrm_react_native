import { StyleSheet } from "react-native";
export default styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 6
  },
  textHeader: {
    color: "#303030",
    fontSize: 16,
    fontWeight: "bold",
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 10
  },
  mapView: {
    height: 200,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10
  },
  mapViewHolder: {
    marginTop: 36
  },
  mapViewLocation: {
    fontSize: 16,
    color: "#303030",
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 4
  },
  title: {
    fontSize:18,
    color:"#303030",
    fontWeight:'bold',
    textAlign:'center'
  }
});
