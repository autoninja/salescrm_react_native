import React, { Component } from "react";
import {
  View,
  Text,
  ActivityIndicator,
  NativeModules,
  TouchableOpacity,
  Platform,
  FlatList,
  Linking
} from "react-native";
import styles from "./ColdVisitDashboard.styles";
import Toast from "react-native-easy-toast";
import ColdVisitDetailsItem from "./ColdVisitDetailsItem";
import RestClient from "../../../network/rest_client";
export default class ColdVisitDashboardDetails extends Component {
  static navigationOptions = ({ navigation }) => {
    if (navigation) {
      let title = "Cold Visits";
      let data = navigation.getParam("data");
      if (data && data.info && data.info.title) {
        title = data.info.title;
      }
      let startDateData = global.global_filter_date.startDateStr.split("-");
      let endDateData = global.global_filter_date.endDateStr.split("-");
      let date =
        ("0" + startDateData[2]).slice(-2) +
        "/" +
        ("0" + startDateData[1]).slice(-2) +
        "-" +
        ("0" + endDateData[2]).slice(-2) +
        "/" +
        ("0" + endDateData[1]).slice(-2);
      return {
        title,
        headerRight: (
          <Text
            style={{
              color: "#fff",
              fontSize: 14,
              paddingRight: 20,
              paddingLeft: 10,
              width: 140,
              textAlign: "right"
            }}
          >
            {date}
          </Text>
        )
      };
    }
  };
  constructor(props) {
    super(props);
    this.state = {
      isApiCalledAtleastOnce: false,
      loading: true,
      result: [],
      error: false,
      count: 0
    };
  }
  componentDidMount() {
    this._mounted = true;
    this.getColdVisitsDetails();
  }
  componentWillUnmount() {
    this._mounted = false;
  }
  showAlert(alert) {
    if (Platform.OS === "android") {
      NativeModules.ReactNativeToAndroid.showToast(alert);
    }
  }
  call = phoneNumber => {
    if (Platform.OS === "android") {
      NativeModules.ReactNativeToAndroid.makePhoneCall(phoneNumber + "");
    } else {
      Linking.openURL(`tel:${phoneNumber}`);
    }
  };
  openMap = (label, lat, long) => {
    let scheme = Platform.select({ ios: "maps:0,0?q=", android: "geo:0,0?q=" });
    let latLng = `${lat},${long}`;
    let url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`
    });

    Linking.openURL(url);
  };
  getColdVisitsDetails() {
    let data = this.props.navigation.getParam("data");
    this.setState({ loading: true, error: false, result: [], count: 0 });
    new RestClient()
      .getColdVisitDetails({
        start_date: global.global_filter_date.startDateStr,
        end_date: global.global_filter_date.endDateStr,
        user_id: data.info.user_id,
        role_id: data.info.user_role_id,
        location_id: data.info.location_id
      })
      .then(data => {
        if (!this._mounted) {
          return;
        }
        if (data) {
          if (data.statusCode == 2002 && data.result) {
            this.setState({
              loading: false,
              result: data.result.data,
              count: data.result.count
            });
          } else if (data.message) {
            this.setState({
              loading: false,
              error: true
            });
            this.showAlert(data.message);
          } else {
            this.setState({
              loading: false,
              error: true
            });
            this.showAlert("Failed to get the response! Try Again");
          }
        } else {
          this.setState({ loading: false, error: true });
          this.showAlert("Failed to get the response! Try Again");
        }
      })
      .catch(error => {
        if (!this._mounted) {
          return;
        }
        this.setState({ loading: false, error: true });
        this.showAlert("Failed to get the response! Try Again");
        if (!error.status) {
          this.refs.toast.show("Connection Error");
        }
      });
  }

  render() {
    let result = this.state.result;
    let count = this.state.count;
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#fff"
          }}
        >
          <ActivityIndicator
            animating={this.state.loading}
            style={{ flex: 1, alignSelf: "center" }}
            color="#2e5e86"
            size="small"
            hidesWhenStopped={true}
          />
        </View>
      );
    }
    return (
      <View style={styles.main}>
        {!this.state.error && (
          <Text style={{ color: "#303030", fontSize: 18, textAlign: "center" }}>
            {"Cold Visits (" + count + ")"}
          </Text>
        )}
        {this.state.error ||
          (result.length == 0 && (
            <View
              style={{
                flex: 1,
                alignItems: "center",
                backgroundColor: "#fff",
                justifyContent: "center"
              }}
            >
              <Text>No data</Text>
              <TouchableOpacity
                onPress={() => {
                  this.getColdVisitsDetails();
                }}
              >
                <Text
                  style={{
                    marginTop: 10,
                    padding: 10,
                    borderColor: "#c4c4c4",
                    borderRadius: 4,
                    borderWidth: 1
                  }}
                >
                  {" "}
                  Reload{" "}
                </Text>
              </TouchableOpacity>
            </View>
          ))}
        {result.length > 0 && (
          <FlatList
            style={{ marginBottom: 10 }}
            showsVerticalScrollIndicator={false}
            extraData={this.state}
            data={result}
            renderItem={({ item, index }) => {
              return (
                <ColdVisitDetailsItem
                  openMap={this.openMap.bind(this)}
                  call={this.call.bind(this)}
                  data={item}
                />
              );
            }}
            keyExtractor={(item, index) => index + ""}
            onRefresh={() => this.getColdVisitsDetails()}
            refreshing={this.state.loading}
          />
        )}
      </View>
    );
  }
}
