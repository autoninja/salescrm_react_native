package com.salescrm.telephony.telephonyModule;

import java.util.List;

/**
 * Created by subaharan on 25/5/16.
 */
public class SyncDbResponse {

    private String statusCode;

    private String message;

    private List<Result> result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List <Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private String id;

        private String status;

        private String called_number;

        private String lead_id;

        private String duration;

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCalled_number() {
            return called_number;
        }

        public void setCalled_number(String called_number) {
            this.called_number = called_number;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [id = " + id + ", status = " + status + "]";
        }
    }

    public class Error {
        private String details;

        private String type;

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }



        @Override
        public String toString() {
            return "ClassPojo [details = " + details + ", type = " + type + "]";
        }

    }
}