package com.salescrm.telephony.model;

import com.salescrm.telephony.utils.WSConstants;

import java.util.List;

/**
 * Created by bharath on 1/9/16.
 */
public class AddLeadDSEInputData {

    private String dseBranchId;
  /*  private String salesManagerId;
    private String teamLeadId;
    private String dseNameId;*/
    private List<CreateLeadInputData.Lead_data.User_details> user_detailsList;
   /* user_details.setRole_id(WSConstants.MANAGER_ROLE_ID);
    user_details.setUser_id(dseInputData.getSalesManagerId());
    user_detailsList.add()*/
    private String tagId;


    public AddLeadDSEInputData(String dseBranchId,List<CreateLeadInputData.Lead_data.User_details> user_detailsList, String tagId) {
        this.dseBranchId = dseBranchId;
        this.user_detailsList = user_detailsList;
        this.tagId = tagId;
    }

    public List<CreateLeadInputData.Lead_data.User_details> getUser_detailsList() {
        return user_detailsList;
    }

    public void setUser_detailsList(List<CreateLeadInputData.Lead_data.User_details> user_detailsList) {
        this.user_detailsList = user_detailsList;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getDseBranchId() {
        return dseBranchId;
    }

    public void setDseBranchId(String dseBranch) {
        this.dseBranchId = dseBranchId;
    }
}
