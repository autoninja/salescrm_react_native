package com.salescrm.telephony.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.CardView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.PlanNextTaskResultListener;
import com.salescrm.telephony.model.LostDropAllowed;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 22/6/17.
 */

public class PlanNextTaskPicker {

    private Context context;
    private PlanNextTaskResultListener listener;
    private Dialog dialog;
    private TextView tvTitle;
    private GridView gridView;
    private CardView cardBookCar;
    private TextView tvMainAction;
    private boolean isClientRoyalEnfield = false;
    private ProgressDialog progressDialog;
    private Preferences pref;

    private boolean itemClickedForNextStep = false;
    private boolean isClientILom = false;
    private boolean isClientILomBank = false;
    private int activityId = -1;

    public PlanNextTaskPicker() {
    }

    public PlanNextTaskPicker(Context context, PlanNextTaskResultListener listener) {
        this.context = context;
        this.listener = listener;
        this.isClientRoyalEnfield = DbUtils.isBike();
        pref = Preferences.getInstance();
        pref.load(context);
        this.isClientILom = pref.isClientILom();
        this.isClientILomBank = pref.isClientILBank();

    }
    PlanNextTaskPicker(Context context, PlanNextTaskResultListener listener, int activityId) {
        this.context = context;
        this.listener = listener;
        this.isClientRoyalEnfield = DbUtils.isBike();
        pref = Preferences.getInstance();
        pref.load(context);
        this.isClientILom = pref.isClientILom();
        this.activityId = activityId;
        this.isClientILomBank = pref.isClientILBank();

    }


    public void show() {
        dialog = new Dialog(context,R.style.AppTheme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
          //  dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }

        dialog.setContentView(R.layout.layout_tasks_picker);
        tvTitle = (TextView) dialog.findViewById(R.id.tv_pnt_title);
        gridView = (GridView) dialog.findViewById(R.id.grid_view_pnt);
        cardBookCar = (CardView) dialog.findViewById(R.id.card_pnt_book_car);
        tvMainAction = (TextView) dialog.findViewById(R.id.tv_pnt_main_action);

        cardBookCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    if(dialog!=null){
                        dialog.dismiss();
                    }
                    if(isClientILom) {
                        listener.onPlanNextTaskResult(WSConstants.FormAnswerId.SALES_CONFIRMED);
                    }
                    else {
                        listener.onPlanNextTaskResult(WSConstants.FormAnswerId.BOOK_CAR_ID);
                    }

                }
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        updateDialog();


    }

    private void updateDialog(){
        itemClickedForNextStep = false;
        if(isClientRoyalEnfield) {
            String[] imageCaption = new String[]{"Follow Up", "Test Ride", "Dropout", "Converted"};
            int[] imgSource = new int[]{
                    R.drawable.ic_pnt_call,
                    R.drawable.ic_pnt_test_ride,
                    R.drawable.ic_pnt_lost_drop,
                    R.drawable.ic_pnt_book_car};

            int[] fAnswerId = new int[]{WSConstants.FormAnswerId.FOLLOW_UP_CALL_RE,
                    WSConstants.FormAnswerId.TEST_RIDE_INIT_RE,
                    WSConstants.FormAnswerId.DROP_OUT_RE,
                    WSConstants.FormAnswerId.BOOK_RE
            };
            callDialog("Plan Next Task", imgSource, imageCaption,fAnswerId);
        }
        else if(DbUtils.isClientTwoWheeler() || pref.isTwoWheelerClient()) {
            String[] imageCaption = new String[]{"Call From Consultant", "Follow Up\n", "Lost Drop\n", "Booked\n"};
            int[] imgSource = new int[]{
                    R.drawable.ic_pnt_arrange_call,
                    R.drawable.ic_pnt_call,
                    R.drawable.ic_pnt_lost_drop,
                    R.drawable.ic_pnt_book_car};

            int[] fAnswerId = new int[]{WSConstants.FormAnswerId.ARRANGE_CALL,
                    WSConstants.FormAnswerId.FOLLOW_UP_CALL,
                    WSConstants.FormAnswerId.LOST_DROP,
                    WSConstants.FormAnswerId.BOOKED
            };
            callDialog("Plan Next Task", imgSource, imageCaption,fAnswerId);
        }
        else if(isClientILom) {
            String[] imageCaption =
                    new String[]{"Call Back", "Prospect", "IL Renewed", "Not Eligible/Not Interested"};
            int[] imgSource =
                    new int[]{R.drawable.ic_pnt_callback,
                    R.drawable.ic_pnt_prospects,
                    R.drawable.ic_il_renewed, R.drawable.ic_pnt_lost_drop};

            int[] fAnswerId =
                    new int[]{
                            activityId == WSConstants.TaskActivityName.PROSPECT_CALL?
                                    WSConstants.FormAnswerId.DISABLED:
                                    WSConstants.FormAnswerId.CALL_BACK,
                            WSConstants.FormAnswerId.PROSPECT,
                            WSConstants.FormAnswerId.IL_RENEWED,
                            WSConstants.FormAnswerId.LOST_NOT_ELIGIBLE
            };
            callDialog("Plan Next Task", imgSource, imageCaption,fAnswerId);
        }
        else if(isClientILomBank) {
            String[] imageCaption;
            int[] imgSource;
            int[] fAnswerId;
            if(DbUtils.isUserVerifier()) {
                imageCaption = new String[] {"Sales Confirmed", "Back to Sale"};
                imgSource = new int[]{R.drawable.ic_pnt_book_car,
                        R.drawable.ic_pnt_back_to_sale};
                fAnswerId = new int[]{
                        WSConstants.FormAnswerId.SALES_CONFIRMED,
                        WSConstants.FormAnswerId.BACK_TO_SALE
                };
            }
            else {
                imageCaption = new String[] {"Call Back", "Prospect", "Verification", "Not Eligible/Not Interested"};
                imgSource = new int[]{R.drawable.ic_pnt_callback,
                        R.drawable.ic_pnt_prospects,
                        R.drawable.ic_pnt_verification,
                        R.drawable.ic_pnt_lost_drop};
                fAnswerId = new int[]{
                        (activityId == WSConstants.TaskActivityName.PROSPECT_CALL
                                || activityId == WSConstants.TaskActivityName.BACK_TO_SALE)?
                                WSConstants.FormAnswerId.DISABLED:
                                WSConstants.FormAnswerId.CALL_BACK,
                        activityId == WSConstants.TaskActivityName.BACK_TO_SALE?
                                WSConstants.FormAnswerId.DISABLED:
                                WSConstants.FormAnswerId.PROSPECT,
                        WSConstants.FormAnswerId.VERIFICATION,
                        WSConstants.FormAnswerId.LOST_NOT_ELIGIBLE
                };
            }

            callDialog("Plan Next Task", imgSource, imageCaption,fAnswerId);
        }
        else {
            String[] imageCaption = new String[]{"Follow Up", "Test Drive", "Visit", "Lost/Drop"};
            int[] imgSource = new int[]{R.drawable.ic_pnt_call,
                    R.drawable.ic_pnt_test_drive,
                    R.drawable.ic_pnt_visit, R.drawable.ic_pnt_lost_drop};

            int[] fAnswerId = new int[]{WSConstants.FormAnswerId.FOLLOW_UP_CALL,
                    WSConstants.FormAnswerId.TEST_DRIVE_INIT,
                    WSConstants.FormAnswerId.HOME_INIT,
                    WSConstants.FormAnswerId.LOST_DROP
            };
            callDialog("Plan Next Task", imgSource, imageCaption,fAnswerId);
        }

    }

    private void callDialog(String title, int[] imageSource, final String[] imageCaption, final int[] fAnswerId){
        if(title!=null){
            tvTitle.setText(title);
        }
        if(isClientILomBank || isClientRoyalEnfield || itemClickedForNextStep || DbUtils.isClientTwoWheeler() || pref.isTwoWheelerClient()){
            cardBookCar.setVisibility(View.GONE);
        }
        else {
            if(isClientILom) {
                tvMainAction.setText("Sales Confirmed");
            }
            else {
                tvMainAction.setText("Book Car");
            }
            cardBookCar.setVisibility(View.VISIBLE);
        }
        gridView.setAdapter(new TaskPickerGridAdapter(context, imageSource, imageCaption, fAnswerId));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(fAnswerId[position] == WSConstants.FormAnswerId.DISABLED) {
                        Util.showToast(context, "Disabled", Toast.LENGTH_SHORT);
                        return;
                    }
                    if(fAnswerId[position]==WSConstants.FormAnswerId.TEST_DRIVE_INIT ||
                        fAnswerId[position]==WSConstants.FormAnswerId.HOME_INIT){
                        itemClickedForNextStep = true;

                        //Show selector init
                        String title;
                        int[] imageSource = new int[]{R.drawable.ic_pnt_home,
                                R.drawable.ic_pnt_showroom};
                        String[] imageCaption = new String[]{"Home","Showroom"};
                        int [] fAnswerIdSelect =new int[2];

                        if(fAnswerId[position]==WSConstants.FormAnswerId.TEST_DRIVE_INIT){
                            fAnswerIdSelect[0] =WSConstants.FormAnswerId.TEST_DRIVE_HOME;
                            fAnswerIdSelect[1] =WSConstants.FormAnswerId.TEST_DRIVE_SHOW_ROOM;
                            title ="Select Test Drive Type";

                        }
                        else {
                            fAnswerIdSelect[0] =WSConstants.FormAnswerId.HOME_VISIT;
                            fAnswerIdSelect[1] =WSConstants.FormAnswerId.SHOWROOM_VISIT;
                            title ="Select Visit Type";
                        }
                        callDialog(title,imageSource,imageCaption, fAnswerIdSelect);
                    }
                    //For test_ride
                    else if(fAnswerId[position] == WSConstants.FormAnswerId.TEST_RIDE_INIT_RE) {
                        itemClickedForNextStep = true;
                        String title ="Select Test Ride Type";
                        int[] imageSource = new int[] {
                                R.drawable.ic_pnt_home,
                                R.drawable.ic_pnt_showroom,
                                R.drawable.ic_pnt_test_ride_exp,
                                R.drawable.ic_pnt_test_ride_demo
                        };
                        String[] imageCaption = new String[] {
                                "Home",
                                "Showroom",
                                "Experiential",
                                "Demo Day"
                        };

                        int[] fAnswerIdSelect = new int[]{WSConstants.FormAnswerId.TEST_RIDE_HOME_RE,
                                WSConstants.FormAnswerId.TEST_RIDE_SHOWROOM_RE,
                                WSConstants.FormAnswerId.TEST_RIDE_EXP_RE,
                                WSConstants.FormAnswerId.TEST_RIDE_DEMO_RE
                        };

                        callDialog(title,imageSource,imageCaption, fAnswerIdSelect);
                    }
                    else if(fAnswerId[position] == WSConstants.FormAnswerId.LOST_DROP && !isClientILom && !isClientILomBank){
                        System.out.println("pref.getLeadID, "+pref.getLeadID());
                        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getProceedFlag(pref.getLeadID(), new Callback<LostDropAllowed>() {
                            @Override
                            public void success(LostDropAllowed lostDropAllowed, Response response) {
                                if(lostDropAllowed.getResult()!= null && lostDropAllowed.getResult().isLd_flag()){
                                    itemClickedForNextStep = false;
                                    dialog.dismiss();
                                    if(listener!=null){
                                        listener.onPlanNextTaskResult(fAnswerId[position]);
                                    }
                                }else{
                                    Toast.makeText(context, (lostDropAllowed.getResult().getMessage()!= null) ?
                                            lostDropAllowed.getResult().getMessage() : "You can not mark lost drop now"+"", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                    else{
                        itemClickedForNextStep = false;
                        dialog.dismiss();
                        if(listener!=null){
                            listener.onPlanNextTaskResult(fAnswerId[position]);
                        }
                    }
            }
        });

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if(itemClickedForNextStep){
                        updateDialog();
                        return true;
                    }

                }
                return false;
            }
        });


    }

    public class TaskPickerGridAdapter extends BaseAdapter {
        private final int[] imageSource;
        private final String[] imageCaption;
        private final int[] fAnswerId;
        private Context mContext;

        public TaskPickerGridAdapter(Context c, int[] imageSource, String[] strings, int[] fAnswerId) {
            mContext = c;
            this.imageSource = imageSource;
            this.imageCaption = strings;
            this.fAnswerId = fAnswerId;
        }

        @Override
        public int getCount() {
            return imageSource.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)
                    mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View gridView;
            if (convertView == null) {
                gridView = new View(mContext);
                // get layout from mobile.xml
                gridView = inflater.inflate(R.layout.layout_grid_card, null);
                // set value into textview
                TextView textView = (TextView)
                        gridView.findViewById(R.id.tv_grid_caption);
                textView.setText(imageCaption[position]);
                // set image based on selected text
                ImageView imageView = (ImageView)
                        gridView.findViewById(R.id.img_grid);
                imageView.setImageResource(imageSource[position]);
                if(position< fAnswerId.length) {
                    if(fAnswerId[position] == WSConstants.FormAnswerId.DISABLED) {
                        gridView.setAlpha(0.6f);
                    }
                    else {
                        gridView.setAlpha(1f);
                    }
                }
                else {
                    gridView.setAlpha(1f);
                }
            } else {
                gridView = (View) convertView;
            }
            return gridView;
        }
    }

}
