package com.salescrm.telephony.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by bharath on 29/6/17.
 */

public class NotificationRedirectActivity extends AppCompatActivity {
    private TextView tvTitleLoader;
    private String firebaseEvent;
    private int leadId, scheduledActivityId, activityId, locationId;
    private Preferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_support);
        preferences = Preferences.getInstance();
        preferences.load(this);
        tvTitleLoader = (TextView) findViewById(R.id.tv_loader_title);

        if (tvTitleLoader != null) {
            tvTitleLoader.setText("Please wait !!");
        }
        init();
    }

    private void init() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();


            leadId              = bundle.getInt(WSConstants.LEAD_ID, -1);
            scheduledActivityId = bundle.getInt(WSConstants.SCHEDULED_ACTIVITY_ID, -1);
            activityId          = bundle.getInt(WSConstants.NOTIFICATION_ACTIVITY_ID, -1);
            firebaseEvent       = bundle.getString(WSConstants.NOTIFICATION_FIREBASE_EVENT, WSConstants.FirebaseEvent.DEFAULT);
            locationId          = bundle.getInt(WSConstants.NOTIFICATION_LOCATION_ID, -1);

            if(firebaseEvent != null && !firebaseEvent.equalsIgnoreCase(WSConstants.FirebaseEvent.DEFAULT)){
                if(leadId>0) {
                    boolean isRedirectToC360;
                    isRedirectToC360 = !(firebaseEvent.equalsIgnoreCase(WSConstants.FirebaseEvent.TASK_ASSIGNED) ||
                            firebaseEvent.equalsIgnoreCase(WSConstants.FirebaseEvent.PAYMENT_RECEIVED)
                    );

                    if (isRedirectToC360) {
                        openC360Activity();
                    } else {
                        //Download data
                        fetchTasks();
                    }
                }
                else {
                    openTaskActivity();
                }


            }
        }
    }

    private void fetchTasks() {
        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(leadId+"");
        new FetchC360InfoOnCreateLead(new FetchC360OnCreateLeadListener() {
            @Override
            public void onFetchC360OnCreateLeadSuccess() {
               openTaskActivity();
            }

            @Override
            public void onFetchC360OnCreateLeadError() {
                openC360Activity();
            }
        }, getApplicationContext(), leadData, preferences.getAppUserId()).call(true);
    }

    private void openTaskActivity() {
        Intent intent = new Intent(NotificationRedirectActivity.this, HomeActivity.class);
        intent.putExtra(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY,true);
        intent.putExtra(WSConstants.LEAD_ID,leadId);
        intent.putExtra(WSConstants.SCHEDULED_ACTIVITY_ID,scheduledActivityId);
        intent.putExtra(WSConstants.NOTIFICATION_ACTIVITY_ID,activityId);
        intent.putExtra(WSConstants.NOTIFICATION_CLICKED_NAME,firebaseEvent);
        intent.putExtra(WSConstants.NOTIFICATION_LOCATION_ID,locationId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(0,0);
        NotificationRedirectActivity.this.finish();
    }

    private void openC360Activity() {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_TYPE, CleverTapConstants.EVENT_NOTIFICATION_TYPE_CLICK);
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_FROM, CleverTapConstants.EVENT_NOTIFICATION_FROM_VALUE_FCM);
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_EVENT, firebaseEvent);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_NOTIFICATION, hashMap);

        preferences.setLeadID(leadId+"");
        Intent intent = new Intent(this,C360Activity.class);
        intent.putExtra(WSConstants.OPEN_C360_FROM_NOTIFICATION_TRAY,true);
        NotificationRedirectActivity.this.finish();
        startActivity(intent);

    }
}
