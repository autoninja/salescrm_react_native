package com.salescrm.telephony.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.activity.HomeActivity;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.NotificationsUI;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 2/6/17.
 */

public class NotificationReceiver extends BroadcastReceiver {
    private Realm realm;
    private Preferences preferences;
    @Override
    public void onReceive(final Context context, Intent intent) {
        realm = Realm.getDefaultInstance();
        preferences = Preferences.getInstance();
        preferences.load(context);
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED") ||
                intent.getAction().equals("android.intent.action.QUICKBOOT_POWERON")) {
            System.out.println("Boot completed ..");
            recreateProcessor(context);
        } else if (intent.getAction().equals("android.intent.action.PACKAGE_REPLACED") &&
                intent.getData().getSchemeSpecificPart().equals("com.salescrm.telephony")) {
            System.out.println("App updated ..");
            recreateProcessor(context);
        } else if (intent.getAction().equalsIgnoreCase(WSConstants.START_NINJA_NOTIFY)) {
            System.out.println("Fired Notification");
            int leadId = (int) intent.getExtras().getLong(WSConstants.LEAD_ID,0);
            int scheduledActivityId = intent.getExtras().getInt(WSConstants.SCHEDULED_ACTIVITY_ID,0);
            long scheduledDate = intent.getExtras().getLong(WSConstants.SCHEDULED_DATE,0);
            String customerName = intent.getExtras().getString(WSConstants.NOTIFICATION_CUSTOMER_NAME,"");
            String activityName  = intent.getExtras().getString(WSConstants.ACTIVITY_NAME,"Notification");
            int activityId  = intent.getExtras().getInt(WSConstants.NOTIFICATION_ACTIVITY_ID,-1);
            if(leadId!=0&&scheduledActivityId!=0) {
                Util.showAppNotification(context,leadId,customerName,scheduledActivityId,activityName,scheduledDate,activityId,false,null, -1);
                if(activityId == WSConstants.TaskActivityName.EVALUATION_REQUEST_ID){
                    if(new ConnectionDetectorService(context).isConnectingToInternet()){
                        upcomingActivityNotify(scheduledActivityId);
                    }
                }
            }

        } else if(intent.getAction().equalsIgnoreCase(WSConstants.InAppNotification.START_NINJA_NOTIFY_CUSTOM)) {
            System.out.println("Custom Notification Fired");

            realm.where(SalesCRMRealmTable.class)
                    .equalTo("isCreatedTemporary", false)
                    .equalTo("isDone", false)
                    .equalTo("isLeadActive", 1)
                    .equalTo("createdOffline", false)
                    .equalTo("dseId", preferences.getAppUserId())
                    .greaterThanOrEqualTo("activityScheduleDate", Util.getTime(0))
                    .lessThan("activityScheduleDate", Util.getTime(1)).findAllAsync().addChangeListener(new RealmChangeListener<RealmResults<SalesCRMRealmTable>>() {
                @Override
                public void onChange(RealmResults<SalesCRMRealmTable> salesCRMRealmTables) {
                    if (salesCRMRealmTables.size() > 0) {
                        String tasks = salesCRMRealmTables.size()==1?"task":"tasks";
                                NotificationsUI.showNotification(context, "Alert", salesCRMRealmTables.size() + " today's " + tasks + " not completed");
                        System.out.println("Custom Notification Fired::" + salesCRMRealmTables.size());
                    }
                }
            });
        }
    }

    private void upcomingActivityNotify(Integer scheduledActivityId) {
        ApiUtil.GetRestApiWithHeader(preferences.getAccessToken()).upcomingActivityReminder(scheduledActivityId, new Callback<Object>() {
            @Override
            public void success(Object o, Response response) {
                System.out.println("Success");
            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println("Error");
            }
        });
    }

    private void recreateProcessor(Context context) {

    }
}
