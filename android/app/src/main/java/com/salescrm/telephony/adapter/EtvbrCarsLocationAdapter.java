/*
package com.salescrm.telephony.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

*/
/**
 * Created by prateek on 13/11/17.
 *//*


public class EtvbrCarsLocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private RealmResults<EtvbrCarsAbsolute> resultEtvbrAbsolutes;
    private RealmResults<EtvbrCarsRelational> resultEtvbrRelational;
    private Realm realm;
    RealmResults<ResultEtvbrCars> resultEtvbrCars;
    public EtvbrCarsLocationAdapter(Context context, RealmResults<EtvbrCarsRelational> resultEtvbrRelational, RealmResults<EtvbrCarsAbsolute> resultEtvbrAbsolutes, RealmResults<ResultEtvbrCars> resultEtvbrCars, Realm realm) {
        this.context = context;
        this.realm = realm;
        this.resultEtvbrAbsolutes = resultEtvbrAbsolutes;
        this.resultEtvbrRelational = resultEtvbrRelational;
        this.resultEtvbrCars = resultEtvbrCars;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.etvbr_cars_adapter_view, parent, false);
        return new EtvbrCarsHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(EtvbrCarsFragment.CARS_PERCENT){
            viewForRel(holder, position);
        }else {
            viewForAbsolute(holder, position);
        }

    }

    private void viewForRel(RecyclerView.ViewHolder holder, int position) {
        EtvbrCarsHolder carsHolder = (EtvbrCarsHolder) holder;
        if(position % 2 == 0){
            carsHolder.llCarsEtvbr.setBackgroundColor(Color.parseColor("#243F6D"));
        }else {
            carsHolder.llCarsEtvbr.setBackgroundColor(Color.TRANSPARENT);
        }
        if(resultEtvbrRelational!= null && position < resultEtvbrRelational.size()){
            carsHolder.tvCarsEnquiry.setText(""+resultEtvbrRelational.get(position).getEnquiriesPer());
            carsHolder.tvCarsTDrive.setText(""+resultEtvbrRelational.get(position).getTdrivesPer());
            carsHolder.tvCarsVisit.setText(""+resultEtvbrRelational.get(position).getVisitsPer());
            carsHolder.tvCarsBooking.setText(""+resultEtvbrRelational.get(position).getBookingsPer());
            carsHolder.tvCarsRetail.setText(""+resultEtvbrRelational.get(position).getRetailPer());
            carsHolder.tvCarsName.setText(""+resultEtvbrRelational.get(position).getModelName());
            carsHolder.tvCarsName.setVisibility(View.VISIBLE);
            carsHolder.tvCarsTotal.setVisibility(View.GONE);

            carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsTDrive.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsVisit.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsBooking.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsRetail.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsName.setTypeface(null, Typeface.NORMAL);

            carsHolder.viewEnquiry.setVisibility(View.VISIBLE);
            carsHolder.viewTdrive.setVisibility(View.VISIBLE);
            carsHolder.viewBooking.setVisibility(View.VISIBLE);
            carsHolder.viewVisit.setVisibility(View.VISIBLE);
            carsHolder.viewRetail.setVisibility(View.VISIBLE);
        }else {

            if (resultEtvbrCars != null && resultEtvbrCars.size() > 0) {

                carsHolder.tvCarsEnquiry.setText("" + resultEtvbrCars.get(0).getRelTotal().getEnqTotal());
                carsHolder.tvCarsTDrive.setText("" + resultEtvbrCars.get(0).getRelTotal().getTdTotal());
                carsHolder.tvCarsVisit.setText("" + resultEtvbrCars.get(0).getRelTotal().getVisitsTotal());
                carsHolder.tvCarsBooking.setText("" + resultEtvbrCars.get(0).getRelTotal().getBookTotal());
                carsHolder.tvCarsRetail.setText("" + resultEtvbrCars.get(0).getRelTotal().getRetailTotal());

                //carsHolder.tvCarsEnquiry.setText("0");
                //carsHolder.tvCarsTDrive.setText("0");
                //carsHolder.tvCarsVisit.setText("0");
                //carsHolder.tvCarsBooking.setText("0");
                //carsHolder.tvCarsRetail.setText("0");
                carsHolder.tvCarsName.setVisibility(View.GONE);
                carsHolder.tvCarsTotal.setVisibility(View.VISIBLE);
                carsHolder.tvCarsTotal.setText("Total%");
                carsHolder.tvCarsTotal.setTypeface(null, Typeface.BOLD);

                carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsTDrive.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsVisit.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsBooking.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsRetail.setTypeface(null, Typeface.BOLD);

                carsHolder.viewEnquiry.setVisibility(View.GONE);
                carsHolder.viewTdrive.setVisibility(View.GONE);
                carsHolder.viewBooking.setVisibility(View.GONE);
                carsHolder.viewVisit.setVisibility(View.GONE);
                carsHolder.viewRetail.setVisibility(View.GONE);
            } else {

                */
/*carsHolder.tvCarsEnquiry.setText("" + resultEtvbrCars.get(0).getRelTotal().getEnqTotal());
                carsHolder.tvCarsTDrive.setText("" + resultEtvbrCars.get(0).getRelTotal().getTdTotal());
                carsHolder.tvCarsVisit.setText("" + resultEtvbrCars.get(0).getRelTotal().getVisitsTotal());
                carsHolder.tvCarsBooking.setText("" + resultEtvbrCars.get(0).getRelTotal().getBookTotal());
                carsHolder.tvCarsRetail.setText("" + resultEtvbrCars.get(0).getRelTotal().getRetailTotal());*//*


                carsHolder.tvCarsEnquiry.setText("0");
                carsHolder.tvCarsTDrive.setText("0");
                carsHolder.tvCarsVisit.setText("0");
                carsHolder.tvCarsBooking.setText("0");
                carsHolder.tvCarsRetail.setText("0");

                carsHolder.tvCarsName.setVisibility(View.GONE);
                carsHolder.tvCarsTotal.setVisibility(View.VISIBLE);
                carsHolder.tvCarsTotal.setText("Total%");
                carsHolder.tvCarsTotal.setTypeface(null, Typeface.BOLD);

                carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsTDrive.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsVisit.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsBooking.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsRetail.setTypeface(null, Typeface.BOLD);

                carsHolder.viewEnquiry.setVisibility(View.GONE);
                carsHolder.viewTdrive.setVisibility(View.GONE);
                carsHolder.viewBooking.setVisibility(View.GONE);
                carsHolder.viewVisit.setVisibility(View.GONE);
                carsHolder.viewRetail.setVisibility(View.GONE);
            }
        }
    }

    private void viewForAbsolute(RecyclerView.ViewHolder holder, int position) {
        EtvbrCarsHolder carsHolder = (EtvbrCarsHolder) holder;
        if(position % 2 == 0){
            carsHolder.llCarsEtvbr.setBackgroundColor(Color.parseColor("#243F6D"));
        }else {
            carsHolder.llCarsEtvbr.setBackgroundColor(Color.TRANSPARENT);
        }
        if(resultEtvbrAbsolutes!= null && position < resultEtvbrAbsolutes.size()){

            carsHolder.tvCarsEnquiry.setText(""+resultEtvbrAbsolutes.get(position).getEnquiries());
            carsHolder.tvCarsTDrive.setText(""+resultEtvbrAbsolutes.get(position).getTdrives());
            carsHolder.tvCarsVisit.setText(""+resultEtvbrAbsolutes.get(position).getVisits());
            carsHolder.tvCarsBooking.setText(""+resultEtvbrAbsolutes.get(position).getBookings());
            carsHolder.tvCarsRetail.setText(""+resultEtvbrAbsolutes.get(position).getRetail());
            carsHolder.tvCarsName.setText(""+resultEtvbrAbsolutes.get(position).getModelName());
            carsHolder.tvCarsName.setVisibility(View.VISIBLE);
            carsHolder.tvCarsTotal.setVisibility(View.GONE);

            carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsTDrive.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsVisit.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsBooking.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsRetail.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsName.setTypeface(null, Typeface.NORMAL);

            carsHolder.viewEnquiry.setVisibility(View.VISIBLE);
            carsHolder.viewTdrive.setVisibility(View.VISIBLE);
            carsHolder.viewBooking.setVisibility(View.VISIBLE);
            carsHolder.viewVisit.setVisibility(View.VISIBLE);
            carsHolder.viewRetail.setVisibility(View.VISIBLE);
        }else{
            if(resultEtvbrCars != null && resultEtvbrCars.size() >0) {

                carsHolder.tvCarsEnquiry.setText("" + resultEtvbrCars.get(0).getAbsTotal().getEnqTotal());
                carsHolder.tvCarsTDrive.setText("" + resultEtvbrCars.get(0).getAbsTotal().getTdTotal());
                carsHolder.tvCarsVisit.setText("" + resultEtvbrCars.get(0).getAbsTotal().getVisitsTotal());
                carsHolder.tvCarsBooking.setText("" + resultEtvbrCars.get(0).getAbsTotal().getBookTotal());
                carsHolder.tvCarsRetail.setText("" + resultEtvbrCars.get(0).getAbsTotal().getRetailTotal());

                //carsHolder.tvCarsEnquiry.setText("0");
                //carsHolder.tvCarsTDrive.setText("0");
                //carsHolder.tvCarsVisit.setText("0");
                //carsHolder.tvCarsBooking.setText("0");
                //carsHolder.tvCarsRetail.setText("0");
                carsHolder.tvCarsName.setVisibility(View.GONE);
                carsHolder.tvCarsTotal.setVisibility(View.VISIBLE);
                carsHolder.tvCarsTotal.setText("Total");
                carsHolder.tvCarsTotal.setTypeface(null, Typeface.BOLD);

                carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsTDrive.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsVisit.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsBooking.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsRetail.setTypeface(null, Typeface.BOLD);

                carsHolder.viewEnquiry.setVisibility(View.GONE);
                carsHolder.viewTdrive.setVisibility(View.GONE);
                carsHolder.viewBooking.setVisibility(View.GONE);
                carsHolder.viewVisit.setVisibility(View.GONE);
                carsHolder.viewRetail.setVisibility(View.GONE);

            }else {

                carsHolder.tvCarsEnquiry.setText("0");
                carsHolder.tvCarsTDrive.setText("0");
                carsHolder.tvCarsVisit.setText("0");
                carsHolder.tvCarsBooking.setText("0");
                carsHolder.tvCarsRetail.setText("0");

                carsHolder.tvCarsName.setVisibility(View.GONE);
                carsHolder.tvCarsTotal.setVisibility(View.VISIBLE);
                carsHolder.tvCarsTotal.setText("Total");
                carsHolder.tvCarsTotal.setTypeface(null, Typeface.BOLD);

                carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsTDrive.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsVisit.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsBooking.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsRetail.setTypeface(null, Typeface.BOLD);

                carsHolder.viewEnquiry.setVisibility(View.GONE);
                carsHolder.viewTdrive.setVisibility(View.GONE);
                carsHolder.viewBooking.setVisibility(View.GONE);
                carsHolder.viewVisit.setVisibility(View.GONE);
                carsHolder.viewRetail.setVisibility(View.GONE);


            }
        }
    }

    @Override
    public int getItemCount() {
        return resultEtvbrAbsolutes != null ? resultEtvbrAbsolutes.size()+1:0;
    }


    public class EtvbrCarsHolder extends RecyclerView.ViewHolder{

        TextView tvCarsEnquiry, tvCarsTDrive, tvCarsVisit, tvCarsBooking, tvCarsRetail;
        TextView tvCarsName, tvCarsTotal;
        LinearLayout llCarsEtvbr;
        View viewEnquiry, viewTdrive, viewVisit, viewBooking, viewRetail;

        public EtvbrCarsHolder(View itemView) {
            super(itemView);

            tvCarsEnquiry = (TextView) itemView.findViewById(R.id.txt_car_enquiry);
            tvCarsTDrive = (TextView) itemView.findViewById(R.id.txt_car_tdrive);
            tvCarsVisit = (TextView) itemView.findViewById(R.id.txt_car_visit);
            tvCarsBooking = (TextView) itemView.findViewById(R.id.txt_car_booking);
            tvCarsRetail = (TextView) itemView.findViewById(R.id.txt_car_retail);

            tvCarsName = (TextView) itemView.findViewById(R.id.tv_car_name);
            tvCarsTotal = (TextView) itemView.findViewById(R.id.tv_car_total);

            llCarsEtvbr = (LinearLayout) itemView.findViewById(R.id.ll_cars_etvbr);
            viewEnquiry = (View) itemView.findViewById(R.id.view_enquiry);
            viewTdrive = (View) itemView.findViewById(R.id.view_tdrive);
            viewVisit = (View) itemView.findViewById(R.id.view_visit);
            viewBooking = (View) itemView.findViewById(R.id.view_booking);
            viewRetail = (View) itemView.findViewById(R.id.view_retail);
        }
    }
}
*/
