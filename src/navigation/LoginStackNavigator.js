import { createStackNavigator } from 'react-navigation';
import Login from  '../components/login';
import ValidateOtp from  '../components/validate_otp';

const LoginStack = createStackNavigator({
        Login : {
            screen:Login,
            navigationOptions: ({ navigation }) => ({
                header: null,
                headerLeft: null,
            }),
        },
        ValidateOtp : {
            screen:ValidateOtp,
            navigationOptions: ({ navigation }) => ({
                header: null,
            }),
        },
    },
    {
        initialRouteName: 'Login',
    },
    {
        cardStyle: {
            backgroundColor: '#303030BE',
            opacity: 1,
        },
        mode:'modal'
    },
);

export default LoginStack;