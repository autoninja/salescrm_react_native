import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';

export default class CustomButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let disabled =this.props.disabled;
    let text = this.props.text;
    return(
      <TouchableOpacity  style={{alignItems: "center",}} disabled = { disabled } onPress ={()=> this.onClick()}>
         <View style={{height:40, minWidth:100,justifyContent: "center", marginTop:10,backgroundColor:disabled?'#CFCFCF':'#007fff', borderRadius:6}}>
           <Text adjustsFontSizeToFit={true}
                numberOfLines={1} style={{textAlign:'center', fontSize:18,textAlignVertical: 'center', color:'#fff'}}>
           {text}
           </Text>
         </View>
       </TouchableOpacity>
    )

  }
}
