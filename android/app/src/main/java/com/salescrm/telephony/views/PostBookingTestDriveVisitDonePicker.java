package com.salescrm.telephony.views;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.CardView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.interfaces.PlanNextTaskPostBookingListener;
import com.salescrm.telephony.utils.WSConstants;

/**
 * Created by bharath on 22/6/17.
 */

public class PostBookingTestDriveVisitDonePicker {

    private Context context;
    private PlanNextTaskPostBookingListener listener;
    private Dialog dialog;
    private TextView tvTitle;
    private GridView gridView;
    private CardView cardBookCar;

    private boolean itemClickedForNextStep = false;
    private int[] fActivitySelect = new int[2];

    public PostBookingTestDriveVisitDonePicker() {
    }

    public PostBookingTestDriveVisitDonePicker(Context context, PlanNextTaskPostBookingListener listener) {
        this.context = context;
        this.listener = listener;

    }


    public void show() {
        dialog = new Dialog(context, R.style.AppTheme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
            //  dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }

        dialog.setContentView(R.layout.layout_tasks_picker);
        tvTitle = (TextView) dialog.findViewById(R.id.tv_pnt_title);
        gridView = (GridView) dialog.findViewById(R.id.grid_view_pnt);
        cardBookCar = (CardView) dialog.findViewById(R.id.card_pnt_book_car);


        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        updateDialog();


    }

    private void updateDialog() {
        itemClickedForNextStep = false;

        String[] imageCaption = new String[]{ "Test Drive", "Visit"};
        int[] imgSource = new int[]{
                R.drawable.ic_pnt_test_drive,
                R.drawable.ic_pnt_visit};

        int[] fAnswerId = new int[]{
                WSConstants.FormAnswerId.TEST_DRIVE_INIT,
                WSConstants.FormAnswerId.HOME_INIT,
        };
        callDialog("What have you done?", imgSource, imageCaption, fAnswerId);


    }

    private void callDialog(String title, int[] imageSource, final String[] imageCaption, final int[] fAnswerId) {
        if (title != null) {
            tvTitle.setText(title);
        }
        cardBookCar.setVisibility(View.GONE);
        gridView.setAdapter(new TaskPickerGridAdapter(context, imageSource, imageCaption));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (fAnswerId[position] == WSConstants.FormAnswerId.TEST_DRIVE_INIT ||
                        fAnswerId[position] == WSConstants.FormAnswerId.HOME_INIT) {
                    itemClickedForNextStep = true;

                    //Show selector init
                    String title;
                    int[] imageSource = new int[]{R.drawable.ic_pnt_home,
                            R.drawable.ic_pnt_showroom};
                    String[] imageCaption = new String[]{"Home", "Showroom"};
                    int[] fAnswerIdSelect = new int[2];


                    if (fAnswerId[position] == WSConstants.FormAnswerId.TEST_DRIVE_INIT) {
                        fAnswerIdSelect[0] =WSConstants.FormAnswerId.TEST_DRIVE_HOME;
                        fAnswerIdSelect[1] =WSConstants.FormAnswerId.TEST_DRIVE_SHOW_ROOM;

                        fActivitySelect[0] = WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID;
                        fActivitySelect[1] = WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID;


                        title = "Select Test Drive Type";

                    } else {
                        fAnswerIdSelect[0] =WSConstants.FormAnswerId.HOME_VISIT;
                        fAnswerIdSelect[1] =WSConstants.FormAnswerId.SHOWROOM_VISIT;

                        fActivitySelect[0] = WSConstants.TaskActivityName.HOME_VISIT_ID;
                        fActivitySelect[1] = WSConstants.TaskActivityName.SHOWROOM_VISIT_ID;

                        title = "Select Visit Type";
                    }
                    callDialog(title, imageSource, imageCaption, fAnswerIdSelect);
                }
                else {
                    itemClickedForNextStep = false;
                    dialog.dismiss();
                    if (listener != null) {
                        listener.onPlanNextTaskPostBookingResult(fActivitySelect[position], fAnswerId[position]);
                    }
                }
            }
        });

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (itemClickedForNextStep) {
                        updateDialog();
                        return true;
                    }

                }
                return false;
            }
        });


    }

    public class TaskPickerGridAdapter extends BaseAdapter {
        private final int[] imageSource;
        private final String[] imageCaption;
        private Context mContext;

        public TaskPickerGridAdapter(Context c, int[] imageSource, String[] strings) {
            mContext = c;
            this.imageSource = imageSource;
            this.imageCaption = strings;
        }

        @Override
        public int getCount() {
            return imageSource.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)
                    mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View gridView;
            if (convertView == null) {
                gridView = new View(mContext);
                // get layout from mobile.xml
                gridView = inflater.inflate(R.layout.layout_grid_card, null);
                // set value into textview
                TextView textView = (TextView)
                        gridView.findViewById(R.id.tv_grid_caption);
                textView.setText(imageCaption[position]);
                // set image based on selected text
                ImageView imageView = (ImageView)
                        gridView.findViewById(R.id.img_grid);
                imageView.setImageResource(imageSource[position]);
            } else {
                gridView = (View) convertView;
            }
            return gridView;
        }
    }

}
