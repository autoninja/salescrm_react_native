import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image, CheckBox } from 'react-native';

export default class CheckboxView extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			isFetching: false,
			checked: false,
			selectedAnswer: ''
		}
	}

	componentWillUnmount() {
		if (this.props.radioObject && this.props.radioObject.fQId) {
			this.props.pushToarray({ name: this.props.radioObject.fQId }, -1)
		}
		if (this.props.radioObject.ifVisibleMandatory) {
			this.props.mandatoryField({ id: this.props.radioObject.fQId }, -1)
		}
	}

	generateView(innerObjectsValues) {
		//console.log("innerObjectRadio questionChildren CheckboxView", JSON.stringify(innerObjectsValues))
		//this.props.callMainGenerateViews(item.values[i], item.values[i].formInputType)
		let { selectedAnswer } = this.state;
		let { checked } = this.state;
		let isDisabled = (innerObjectsValues.editable == 0) ? true : false;
		if (!selectedAnswer) {
			if (innerObjectsValues.defaultFAId) {
				selectedAnswer = innerObjectsValues.defaultFAId;
				if (innerObjectsValues.defaultFAId.answerValue == "1") {
					() => this.setState({ checked: true })
				} else {
					() => this.setState({ checked: false })
				}
			}

			if (selectedAnswer) {
				this.props.pushToarray({ name: innerObjectsValues.fQId, value: selectedAnswer, questionId: innerObjectsValues.questionId })

				if (innerObjectsValues.ifVisibleMandatory) {
					this.props.mandatoryField({ id: innerObjectsValues.fQId, isSelected: true })
				}
			}
		}
		return (
			<View>
				<View style={{ flexDirection: "row", alignItems: 'center', margin: 5 }}>
					<TouchableOpacity disabled={isDisabled} style={{ alignSelf: 'center' }}
						onPress={() => {
							let { checked } = this.state
							checked = !checked
							this.setState({ checked })

							this.props.pushToarray({
								name: innerObjectsValues.fQId,
								value: (checked) ? innerObjectsValues.answerChildren[0] : innerObjectsValues.answerChildren[1], questionId: innerObjectsValues.questionId
							})

							if (innerObjectsValues.ifVisibleMandatory) {
								this.props.mandatoryField({ id: innerObjectsValues.fQId, isSelected: checked })
							}
						}}>
						<View style={{ flexDirection: 'row' }}>
							{(checked) ?
								<Image
									style={{ width: 24, height: 24, alignItems: 'center' }}
									source={require('../../images/ic_checkbox_checked.png')} />
								:
								<Image
									style={{ width: 24, height: 24, alignItems: 'center' }}
									source={require('../../images/ic_checkbox_unchecked.png')} />
							}
							<Text style={{ fontSize: 18, marginLeft: 5 }}>{innerObjectsValues.title}</Text>
						</View>
					</TouchableOpacity>
					{/* <CheckBox
							title={innerObjectsValues.title}
							value={checked}
							onValueChange={() => {
								let {checked} = this.state
								checked = !checked
								this.setState({ checked})
								this.props.pushToarray({name:innerObjectsValues.fQId, 
									value:(checked)?innerObjectsValues.answerChildren[0]:innerObjectsValues.answerChildren[1], questionId:innerObjectsValues.questionId})

									if(innerObjectsValues.ifVisibleMandatory){
										this.props.mandatoryField({id:innerObjectsValues.fQId, isSelected:checked})
									}
								}
							}
							/> */}
				</View>

				{(this.state.checked) ?
					innerObjectsValues.questionChildren.map((item, key) => {
						console.log("item.displayTextttt", item.key, item.values.length)
						return item.values.map(item => {
							//console.log("cheekBox", JSON.stringify(item), item.formInputType)
							return this.props.callMainGenerateViews(item, item.formInputType)
						})
					}
					) : null}
			</View>
		)


	}

	render() {
		//console.log("innerObjects", "reached")
		//console.log("innerObjects", this.props.innerObjects.fQId)
		//console.error("innerObjectRadio")
		//console.log("innerObjectRadio", JSON.stringify(this.props.innerObjects))
		return (
			<View style={{ flex: 1 }}>
				{this.generateView(this.props.radioObject)}
			</View>
		)
	}
}