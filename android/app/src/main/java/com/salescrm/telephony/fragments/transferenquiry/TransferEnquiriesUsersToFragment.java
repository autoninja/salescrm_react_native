package com.salescrm.telephony.fragments.transferenquiry;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.TransferEnquiriesRecyclerAdapter;
import com.salescrm.telephony.model.TransferEnquiriesModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bharath on 19/8/17.
 */

public class TransferEnquiriesUsersToFragment extends Fragment implements TransferEnquiriesRecyclerAdapter.TransferEnqClickListener {


    private View inflatedView;
    private RecyclerView recyclerViewUsers;
    private TransferEnquiriesRecyclerAdapter adapter;
    private List<TransferEnquiriesModel.Item> usersList;
    private TransferEnquiriesModel transferEnquiriesModel;
    private EditText etSearch;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        inflatedView = inflater.inflate(R.layout.fragment_transfer_enquiries_users, container, false);

        transferEnquiriesModel = TransferEnquiriesModel.getInstance();
        etSearch          = (EditText) inflatedView.findViewById(R.id.et_trans_enq_search);
        recyclerViewUsers = (RecyclerView) inflatedView.findViewById(R.id.recycler_view_trans_enq);
        usersList = new ArrayList<>();
        usersList.addAll(transferEnquiriesModel.getUserListTo());

        adapter = new TransferEnquiriesRecyclerAdapter(this,usersList,getContext(),false,false);
        recyclerViewUsers.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewUsers.setHasFixedSize(true);
        recyclerViewUsers.setItemAnimator(new DefaultItemAnimator());
        recyclerViewUsers.setAdapter(adapter);
        populateUserLists();
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        Drawable drawable = VectorDrawableCompat
                .create(getResources(), R.drawable.ic_search_white_24dp, null);

        etSearch.setCompoundDrawablesWithIntrinsicBounds(null,null, drawable,null);
        return inflatedView;
    }

    @Override
    public void onResume() {
        super.onResume();
        populateUserLists();
    }

    private void populateUserLists() {
        usersList.clear();
        usersList.addAll(transferEnquiriesModel.getUserListTo());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(int position,TransferEnquiriesModel.Item item) {
        if(usersList.get(position).isChecked()){
            item.setChecked(false);
        }
        else {
            item.setChecked(true);
        }
        for(TransferEnquiriesModel.Item itemData: usersList){
            if(itemData.getId().equalsIgnoreCase(item.getId())){
                itemData.setChecked(item.isChecked());
            }
        }
        transferEnquiriesModel.setUserListTo(position,item);
        transferEnquiriesModel.userItemToChanged();
        adapter.notifyItemChanged(position);
    }
}
