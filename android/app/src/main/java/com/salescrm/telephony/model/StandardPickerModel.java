package com.salescrm.telephony.model;

/**
 * Created by bharath on 14/3/18.
 */

public class StandardPickerModel {
    private int id;
    private int imageSrc;
    private String title;

    public StandardPickerModel(int id, int imageSrc, String title) {
        this.id = id;
        this.imageSrc = imageSrc;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(int imageSrc) {
        this.imageSrc = imageSrc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
