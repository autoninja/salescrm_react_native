package com.salescrm.telephony.fragments.gamification;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.gamification.BadgesResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.services.service_handlers.GamificationServiceHandler;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

public class LeaderBoardDseBadgesFragment extends Fragment {
    private static final String USERID = "userId";
    private Preferences pref;
    private RelativeLayout relLoading;
    private ArrayList<BadgesResponse.Result> earnedBadges = new ArrayList<>();
    ConnectionDetectorService connectionDetectorService;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private LinearLayout ll_shelf_1, ll_shelf_2, ll_shelf_3, main_shelf;
    //private ImageView img_badge_1, img_badge_2, img_badge_3, img_badge_4, img_badge_5, img_badge_6,
    //      img_badge_7, img_badge_8, img_badge_9;
    //private RelativeLayout rll_badge_1, rll_badge_2, rll_badge_3, rll_badge_4, rll_badge_5, rll_badge_6,
    // rll_badge_7, rll_badge_8, rll_badge_9;
    //private TextView txt_badge_1, txt_badge_2, txt_badge_3, txt_badge_4, txt_badge_5, txt_badge_6,
    //      txt_badge_7, txt_badge_8, txt_badge_9;

    private ArrayList<RelativeLayout> earnedRelativeLayouts = new ArrayList<RelativeLayout>();
    private ArrayList<ImageView> earnedBadgesImageViews = new ArrayList<ImageView>();
    private ArrayList<TextView> earnedBadgesTitlesTextViews = new ArrayList<TextView>();

    private LinearLayout llNotEarned;

    @Override
    public void onStop() {
        super.onStop();
        SalesCRMApplication.getBus().unregister(this);
    }

    public LeaderBoardDseBadgesFragment() {
        // Required empty public constructor
    }

    public static LeaderBoardDseBadgesFragment newInstance(String userId) {
        LeaderBoardDseBadgesFragment fragment = new LeaderBoardDseBadgesFragment();
        Bundle args = new Bundle();
        args.putString(USERID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(USERID);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_leader_board_dse_badges, container, false);
        llNotEarned = view.findViewById(R.id.ll_leader_dse_badges_not_earned);

        ll_shelf_1 = view.findViewById(R.id.ll_shelf_1);
        ll_shelf_2 = view.findViewById(R.id.ll_shelf_2);
        ll_shelf_3 = view.findViewById(R.id.ll_shelf_3);
        main_shelf = view.findViewById(R.id.main_shelf_ll);

        main_shelf.setVisibility(View.INVISIBLE);

        relLoading = view.findViewById(R.id.rel_loading_frame);

        earnedBadgesTitlesTextViews = new ArrayList<TextView>() {
            {
                add((TextView) view.findViewById(R.id.txt_badge_1));
                add((TextView) view.findViewById(R.id.txt_badge_2));
                add((TextView) view.findViewById(R.id.txt_badge_3));
                add((TextView) view.findViewById(R.id.txt_badge_4));
                add((TextView) view.findViewById(R.id.txt_badge_5));
                add((TextView) view.findViewById(R.id.txt_badge_6));
                add((TextView) view.findViewById(R.id.txt_badge_7));
                add((TextView) view.findViewById(R.id.txt_badge_8));
                add((TextView) view.findViewById(R.id.txt_badge_9));

            }
        };
        earnedRelativeLayouts = new ArrayList<RelativeLayout>() {
            {
                add((RelativeLayout) view.findViewById(R.id.rll_badge_1));
                add((RelativeLayout) view.findViewById(R.id.rll_badge_2));
                add((RelativeLayout) view.findViewById(R.id.rll_badge_3));
                add((RelativeLayout) view.findViewById(R.id.rll_badge_4));
                add((RelativeLayout) view.findViewById(R.id.rll_badge_5));
                add((RelativeLayout) view.findViewById(R.id.rll_badge_6));
                add((RelativeLayout) view.findViewById(R.id.rll_badge_7));
                add((RelativeLayout) view.findViewById(R.id.rll_badge_8));
                add((RelativeLayout) view.findViewById(R.id.rll_badge_9));


            }
        };
        earnedBadgesImageViews = new ArrayList<ImageView>() {
            {
                add((ImageView) view.findViewById(R.id.img_badge_1));
                add((ImageView) view.findViewById(R.id.img_badge_2));
                add((ImageView) view.findViewById(R.id.img_badge_3));
                add((ImageView) view.findViewById(R.id.img_badge_4));
                add((ImageView) view.findViewById(R.id.img_badge_5));
                add((ImageView) view.findViewById(R.id.img_badge_6));
                add((ImageView) view.findViewById(R.id.img_badge_7));
                add((ImageView) view.findViewById(R.id.img_badge_8));
                add((ImageView) view.findViewById(R.id.img_badge_9));


            }
        };

        pref = Preferences.getInstance();
        pref.load(getContext());
        connectionDetectorService = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (connectionDetectorService.isConnectingToInternet()) {
            relLoading.setVisibility(View.VISIBLE);
            GamificationServiceHandler.getInstance(getContext())
                    .fetchDSEBadges(pref.getAccessToken(), Integer.parseInt(getArguments().getString(USERID)), Util.getMonthYear(pref.getSelectedGameDate()));
        } else {
            Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_LONG).show();
        }

        return view;
    }


    private void setBadges(ArrayList<BadgesResponse.Result> badges) {
        ll_shelf_1.setVisibility(View.GONE);
        ll_shelf_2.setVisibility(View.GONE);
        ll_shelf_3.setVisibility(View.GONE);
        for (int j = 0; j < earnedRelativeLayouts.size(); j++) {
            earnedRelativeLayouts.get(j).setVisibility(View.INVISIBLE);
        }
        int size = badges.size();
        for (int i = 0; i < size; i++) {
            if (badges.get(i).getBadge_count() != 0) {
                earnedBadges.add(badges.get(i));
            }
        }

        showEarnedBadges();
    }


    private void showEarnedBadges() {
        int earnedSize = earnedBadges.size();
        //int trackEarned = 0;

        if (earnedSize > 0 && earnedSize <= 3) {
            System.out.println("EARNED SIZE 1" + earnedSize);
            ll_shelf_1.setVisibility(View.VISIBLE);
        } else if (earnedSize > 3 && earnedSize <= 6) {

            System.out.println("EARNED SIZE 2" + earnedSize);
            ll_shelf_1.setVisibility(View.VISIBLE);
            ll_shelf_2.setVisibility(View.VISIBLE);
        } else if (earnedSize > 6 && earnedSize <= 9) {
            System.out.println("EARNED SIZE 3" + earnedSize);
            ll_shelf_1.setVisibility(View.VISIBLE);
            ll_shelf_2.setVisibility(View.VISIBLE);
            ll_shelf_3.setVisibility(View.VISIBLE);
        }
        for (int i = 0; i < earnedSize; i++) {
            String name = earnedBadges.get(i).getName();
            String badgeId = earnedBadges.get(i).getId() + "";
            int badgeCount = 0;
            int iconId = 0;
            if (earnedBadges.get(i).getBadge_count() != null) {
                badgeCount = earnedBadges.get(i).getBadge_count();

            }
            System.out.println("BADGE ID" + badgeId + "i" + i);
            if (badgeCount > 1) {
                iconId = WSConstants.BADGES.badgesMap.get(badgeId).getMultiIconId();
                name = name + " ("+badgeCount+")";
            } else if (badgeCount == 1) {
                iconId = WSConstants.BADGES.badgesMap.get(badgeId).getSingleIconId();
            }

            System.out.println("ICON ID" + iconId);
            earnedRelativeLayouts.get(i).setVisibility(View.VISIBLE);
            earnedBadgesImageViews.get(i).setImageResource(iconId);
            earnedBadgesTitlesTextViews.get(i).setText(name);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Subscribe
    public void dseBadgesNotify(final BadgesResponse badgesResponse) {
        relLoading.setVisibility(View.GONE);
        main_shelf.setVisibility(View.VISIBLE);
        BadgesResponse realResponse = badgesResponse.getBadgesResponse();
        if (realResponse != null && realResponse.getResult() != null) {
            setBadges(realResponse.getResult());
            for (int i = 0; i < realResponse.getResult().size(); i++) {
                if (realResponse.getResult().get(i).getBadge_count() == 0) {
                    addNotEarnedBadge(realResponse.getResult().get(i));
                } else {
                    addEarnedBadge(realResponse.getResult().get(i));
                }

            }
        } else {
            //Toast.makeText(get, "Coming Soon :)", Toast.LENGTH_LONG).show();
        }

    }

    private void addEarnedBadge(BadgesResponse.Result result) {

    }

    private void addNotEarnedBadge(BadgesResponse.Result result) {

        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_dse_badge_not_earned, llNotEarned, false);
        TextView tvTitle = view.findViewById(R.id.tv_dse_badge_not_earned_title);
        TextView tvSubTitle = view.findViewById(R.id.tv_dse_badge_not_earned_sub_title);
        ImageView imageView = view.findViewById(R.id.image_dse_badge_not_earned);

        imageView.setImageResource(getImage(result.getId()));
        tvTitle.setText(result.getName());
        tvSubTitle.setText(result.getDescription());

        llNotEarned.addView(view);
    }

    private int getImage(Integer id) {
        if(id == null) {
            return R.drawable.badge_trophy;
        }
        int image;
        switch (id) {
            case 1:
                image = R.drawable.badge_bulb;
                break;
            case 2:
                image = R.drawable.badge_car;
                break;
            case 3:
                image = R.drawable.badge_target;
                break;
            case 4:
                image = R.drawable.badge_fire;
                break;
            //Best Guru
            case 5:
                image = R.drawable.badge_explorer;
                break;
            case 6:
                image = R.drawable.badge_achiever;
                break;
            case 7:
                image = R.drawable.badge_dynamo;
                break;
            case 8:
                image = R.drawable.badge_guru;
                break;
            case 9:
                image = R.drawable.badge_trophy;
                break;

            default:
                image = R.drawable.badge_trophy;
        }
        return image;
    }

}
