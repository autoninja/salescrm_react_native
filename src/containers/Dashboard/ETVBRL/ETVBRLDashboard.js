import React, { Component } from 'react'
import { Platform, View, Text, Image, TouchableOpacity, YellowBox, FlatList, ActivityIndicator } from 'react-native'
import PercentageSwitch from '../../../components/uielements/PercentageSwitch'
import styles from '../../../styles/styles'
import ETVBRItem from '../../../components/etvbrl/etvbr_item'
import ETVBRItemExpandable from '../../../components/etvbrl/etvbr_item_expandable'
import ETVBRResultItem from '../../../components/etvbrl/etvbr_result_item'
import ETVBRItemChild from '../../../components/etvbrl/etvbr_item_child'
import ETVBRHeader from '../../../components/etvbrl/etvbr_header'
import Toast from 'react-native-easy-toast'

import RestClient from '../../../network/rest_client'

import { eventTeamDashboard } from '../../../clevertap/CleverTapConstants';
import CleverTapPush from '../../../clevertap/CleverTapPush'
import { NavigationEvents } from "react-navigation";

import { connect } from 'react-redux';
import { updateTeamDashboard } from '../../../actions/team_dashboard_actions';



class ETVBRLDashboard extends Component {

  state = {
    update_etvbr: false,
    update_live_ef: false
  }
  constructor(props) {

    super(props);

    var endDate = new Date();
    var startDate = new Date();
    startDate.setDate(1);

    this.state = {
      isApiCalledAtleastOnce: false, showPercentage: false, isLoading: true, result: {}, carModelResult: {}, dateRangeVisibilty: false,
      dateRange: { startDate, endDate }, bottomItem: { visibility: true, selected: 1 }, show_car_percentage: false, location_selected: 0,
      initDate: { startDate: global_filter_date.startDateStr, endDate: global_filter_date.endDateStr }
    }

    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

  }

  componentDidMount() {

    this.getETVBRData();
    this._mounted = true;

  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // if (prevProps.specificProperty !== this.props.filter_applied) {
    //       Alert.alert('Hello'+this.state.filter_applied);
    // }
    if (this.props.update_etvbr) {
      this._onRefresh();
    }
    this.props.add(false, this.props.update_live_ef);
    //Alert.alert('Hello'+(this.props.filter_applied));
  }

  updateOnFocusChange() {
    if (this.state.initDate.startDate != global.global_filter_date.startDateStr ||
      this.state.initDate.endDate != global.global_filter_date.endDateStr) {
      //  console.error(JSON.stringify(this.state.initDate));
      this._onRefresh();
    }
    if (this.state.bottomItem.visibility) {
      if (this.state.bottomItem.selected == 1) {
        //console.error("Team");
        this.pushCleverTapEvent({ 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrTeams });
      }
      else if (this.state.bottomItem.selected == 2) {
        this.pushCleverTapEvent({ 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrCars });

      }
    }
    else {
      this.pushCleverTapEvent({ 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrLocation });
    }
  }

  componentWillUnmount() {
    this._mounted = false;
  }
  pushCleverTapEvent(props) {
    CleverTapPush.pushEvent(eventTeamDashboard.event, props);

  }
  getETVBRData() {

    new RestClient().getETVBRTeam({
      start_date: global.global_filter_date.startDateStr,
      end_date: global.global_filter_date.endDateStr,
      filters: encodeURIComponent(JSON.stringify(global.global_etvbr_filters_selected))
    })
      .then((data) => {
        if (!this._mounted) {
          return;
        }
        console.log("Datat");
        console.log(JSON.stringify(data));
        let showBottom = false;
        if (data.result.locations.length == 1) {
          showBottom = true;
          if (!this.state.isApiCalledAtleastOnce) {
            this.pushCleverTapEvent({ 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrTeams });
          }

        }
        else {
          showBottom = false;
          if (!this.state.isApiCalledAtleastOnce) {
            this.pushCleverTapEvent({ 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrLocation });
          }
        }
        this.setState({ isApiCalledAtleastOnce: true, isLoading: false, result: data.result, bottomItem: { visibility: showBottom, selected: 1 } })

      }).catch(error => {
        //console.error(error);
        if (!this._mounted) {
          return;
        }
        this.setState({ isLoading: false });
        if (!error.status) {
          this.refs.toast.show('Connection Error');
        }
      });
  }


  _showDateRangePicker(navigation) {
    navigation.navigate('DateRangePickerDialog', {
      initDate: this.state.dateRange, onFilterApply: (s, e) => {

        if (s && e) {
          start = s.split('-');
          end = e.split('-')

          localDateRange = {};

          localStartDate = new Date();
          localStartDate.setDate(start[2]);
          localStartDate.setMonth(start[1] - 1);
          localStartDate.setYear(start[0]);
          localDateRange.startDate = localStartDate;

          localEndDate = new Date();
          localEndDate.setDate(end[2]);
          localEndDate.setMonth(end[1] - 1);
          localEndDate.setYear(end[0]);
          localDateRange.endDate = localEndDate;

          endDate = new Date();

          console.log("showDateRangePickerrr" + s + ':::' + e);
          this.setState({ dateRange: localDateRange, initDate: { startDate: global_filter_date.startDateStr, endDate: global_filter_date.endDateStr } })
          this._onRefresh();
          CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrDate });

        }

      }
    })

    console.log("showDateRangePickerrr");
  }

  _showDetailsOnLocation(navigation, title, location, from, parent_role_id, location_id, location_selected) {
    let showBottom = false;
    console.log('parent_role_id' + parent_role_id);
    if (this.state.result && this.state.result.locations.length > 1) {
      showBottom = true;
    }
    if (parent_role_id == 6) {
      //show scs of location wise
      navigation.navigate('ETVBRLDashboardChild', {
        title,
        location,
        from: 'mutilple_location_tl',
        showBottom,
        parent_role_id,
        location_id,
        location_selected
      });
    }
    else {
      navigation.navigate('ETVBRLDashboardChild', {
        title,
        location,
        from,
        showBottom,
        parent_role_id,
        location_id,
        location_selected
      });
    }

    console.log("showDateRangePicker");
  }


  _showDetailsOnSalesManager(navigation, title, sales_manager, from, location_id) {
    let showBottom = false;
    if (this.state.result && this.state.result.locations.length > 1) {
      showBottom = true;
    }
    navigation.navigate('ETVBRLDashboardChild', {
      title,
      sales_manager,
      from,
      showBottom,
      location_id,
    });
    console.log("_showDetailsOnSalesManager");
  }

  leadSourceCarModelWiseETVBR(navigation, name, dp_url, location_id, user_id, role_id) {
    navigation.navigate('LeadSourceCarModelDistribution',
      {
        name,
        dp_url,
        start_date: global.global_filter_date.startDateStr,
        end_date: global.global_filter_date.endDateStr,
        location_id,
        user_id,
        role_id
      })

    console.log('leadSourceCarModelWiseETVBR');
  }

  _showETVBRFilterScreen(navigation) {
    navigation.navigate('ETVBRFilters', {
      onGoBack: () => {
        //  this._onRefresh()
        //  global.updateDashboard = true;
      }
    })

    console.log('_showETVBRFilterScreen');
  }

  _onRefresh(itemSelected, clickedEvent) {
    if (clickedEvent) {
      if (itemSelected == 1) {
        CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrTeams });

      }
      else if (itemSelected == 2) {
        CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrCars });

      }
    }
    let selectedItem = 1;
    if (itemSelected) {
      selectedItem = itemSelected;
    }
    this.setState({ isLoading: true, bottomItem: { selected: selectedItem }, initDate: { startDate: global_filter_date.startDateStr, endDate: global_filter_date.endDateStr } }, function () { this.getETVBRData() });
  }
  _showUserInfo(navigation, name, dp_url) {
    navigation.navigate('UserInfo', {
      name,
      dp_url,

    });
    console.log("_showUserInfo");
  }

  _etvbrLongPress(navigation, from, data) {
    if (from === 'user_info') {
      navigation.navigate('UserInfo', {
        name: data.info.name,
        dp_url: data.info.dp_url

      });
    }
    else if (from == 'etvbr_details') {
      navigation.navigate('ETVBRDetails', {
        data
      });
    }

    console.log("_etvbrLongPress" + from);
    console.log(data.info.title);
  }

  switchTab(tab) {
    console.log('Tab Switch Called' + tab);
  }



  render() {

    if (this.state.isLoading) {
      return (
        <View style={{
          display: (this.state.isLoading ? 'flex' : 'none'), flex: 1, justifyContent: 'center',
          alignItems: 'center', backgroundColor: '#fff'
        }}>

          <ActivityIndicator
            animating={this.state.isLoading}
            style={{ flex: 1, alignSelf: 'center', }}
            color="#2e5e86"
            size="small"
            hidesWhenStopped={true}

          />

          <NavigationEvents
            onWillFocus={payload => {
              this.updateOnFocusChange();
            }}
          />

        </View>

      )
    }
    let flat_list;
    let { result } = this.state;
    if (result
      && result.locations
      && result.locations.length > 0) {
      console.log('result.locations is >0');
      if (result.locations.length > 1) {
        console.log('result.locations is >1');
        flat_list =

          <FlatList
            showsVerticalScrollIndicator={false}
            data={result.locations}
            renderItem={({ item, index }) => {

              let mainView = <TouchableOpacity onPress={this._showDetailsOnLocation.bind(this, this.props.navigation, item.location_name, item, 'location', result.role_id, item.location_id, index)}>
                {//<Text style={{ color:'#2e5e86', fontSize:15, marginTop:10}}> {item.location_name} </Text>
                }

                <ETVBRItem
                  leadSourceCarModelWiseETVBR={this.leadSourceCarModelWiseETVBR.bind(this,
                    this.props.navigation,
                    item.location_name,
                    item.location_name,
                    item.location_id,
                    global.appUserId,
                    result.role_id)}
                  onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                  onPress={this._showDetailsOnLocation.bind(this, this.props.navigation, item.location_name, item, 'location', result.role_id, item.location_id, index)}
                  from_location={true} showPercentage={this.state.showPercentage} abs={
                    {
                      e: item.location_abs.enquiries,
                      t: item.location_abs.tdrives,
                      v: item.location_abs.visits,
                      b: item.location_abs.bookings,
                      r: item.location_abs.retails,
                      l: item.location_abs.lost_drop,
                    }
                  }

                  rel={
                    {
                      e: item.location_rel.enquiries,
                      t: item.location_rel.tdrives,
                      v: item.location_rel.visits,
                      b: item.location_rel.bookings,
                      r: item.location_rel.retails,
                      l: item.location_rel.lost_drop,
                    }
                  }

                  info={
                    {
                      user_id: global.appUserId,
                      name: item.location_name,
                      location_id: item.location_id,
                      user_role_id: result.role_id,
                    }
                  }
                  targets={
                    {
                      e: item.location_targets.enquiries,
                      t: item.location_targets.tdrives,
                      v: item.location_targets.visits,
                      b: item.location_targets.bookings,
                      r: item.location_targets.retails,
                      l: item.location_targets.lost_drop,
                    }
                  }></ETVBRItem>
              </TouchableOpacity>;

              if (index == result.locations.length - 1) {
                return (<View>
                  {mainView}
                  <ETVBRResultItem

                    onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}

                    showPercentage={this.state.showPercentage} result={result}

                    abs={
                      {
                        e: result.total_abs.enquiries,
                        t: result.total_abs.tdrives,
                        v: result.total_abs.visits,
                        b: result.total_abs.bookings,
                        r: result.total_abs.retails,
                        l: result.total_abs.lost_drop,
                      }
                    }
                    rel={
                      {
                        e: result.total_rel.enquiries,
                        t: result.total_rel.tdrives,
                        v: result.total_rel.visits,
                        b: result.total_rel.bookings,
                        r: result.total_rel.retails,
                        l: result.total_rel.lost_drop,
                      }
                    }

                    info={
                      {
                        user_id: global.appUserId,
                        name: 'All Location',
                        location_id: '',
                        user_role_id: result.role_id,
                      }
                    }
                    targets={
                      {
                        e: result.total_targets.enquiries,
                        t: result.total_targets.tdrives,
                        v: result.total_targets.visits,
                        b: result.total_targets.bookings,
                        r: result.total_targets.retails,
                        l: result.total_targets.lost_drop,
                      }
                    }

                  ></ETVBRResultItem>
                </View>);
              }
              else {
                return (mainView);
              }


            }}
            keyExtractor={(item, index) => index + ''}
            onRefresh={() => this._onRefresh()}
            refreshing={this.state.isLoading}
          />
      }
      else if (result.locations[0].sales_managers && result.locations[0].sales_managers.length > 0) {
        console.log('result.location = 1 result.locations[0].sales_managers.length is > 0');
        console.log('result.locations[0].sales_managers.length is > 0');
        if (result.locations[0].sales_managers.length > 1) {
          console.log('result.locations[0].sales_managers.length is > 1');

          //showSalesManagers
          let location = result.locations[0];

          flat_list = <FlatList
            showsVerticalScrollIndicator={false}
            data={location.sales_managers}
            renderItem={({ item, index }) => {

              let mainView = <TouchableOpacity onPress={this._showDetailsOnSalesManager.bind(this, this.props.navigation, item.info.name + '\'s Team', item, 'sales_manager', location.location_id)}>

                <ETVBRItem
                  leadSourceCarModelWiseETVBR={this.leadSourceCarModelWiseETVBR.bind(this,
                    this.props.navigation,
                    item.info.name,
                    item.info.dp_url,
                    location.location_id,
                    item.info.id,
                    item.info.user_role_id)}
                  onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                  onPress={this._showDetailsOnSalesManager.bind(this, this.props.navigation, item.info.name + '\'s Team', item, 'sales_manager', location.location_id)}
                  showPercentage={this.state.showPercentage} abs={
                    {
                      e: item.info.abs.enquiries,
                      t: item.info.abs.tdrives,
                      v: item.info.abs.visits,
                      b: item.info.abs.bookings,
                      r: item.info.abs.retails,
                      l: item.info.abs.lost_drop,
                    }
                  }

                  rel={
                    {
                      e: item.info.rel.enquiries,
                      t: item.info.rel.tdrives,
                      v: item.info.rel.visits,
                      b: item.info.rel.bookings,
                      r: item.info.rel.retails,
                      l: item.info.rel.lost_drop,
                    }
                  }

                  info={
                    {
                      dp_url: item.info.dp_url,
                      name: item.info.name,
                      user_id: item.info.id,
                      location_id: location.location_id,
                      user_role_id: item.info.user_role_id,
                    }
                  }
                  targets={
                    {
                      e: item.info.targets.enquiries,
                      t: item.info.targets.tdrives,
                      v: item.info.targets.visits,
                      b: item.info.targets.bookings,
                      r: item.info.targets.retails,
                      l: item.info.targets.lost_drop,
                    }
                  }></ETVBRItem>
              </TouchableOpacity>;

              if (index == location.sales_managers.length - 1) {
                return (<View>
                  {mainView}
                  <ETVBRResultItem
                    onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                    showPercentage={this.state.showPercentage} result={result}

                    abs={
                      {
                        e: location.location_abs.enquiries,
                        t: location.location_abs.tdrives,
                        v: location.location_abs.visits,
                        b: location.location_abs.bookings,
                        r: location.location_abs.retails,
                        l: location.location_abs.lost_drop,
                      }
                    }
                    rel={
                      {
                        e: location.location_rel.enquiries,
                        t: location.location_rel.tdrives,
                        v: location.location_rel.visits,
                        b: location.location_rel.bookings,
                        r: location.location_rel.retails,
                        l: location.location_rel.lost_drop,
                      }
                    }

                    info={
                      {
                        user_id: global.appUserId,
                        name: 'All SM',
                        location_id: location.location_id,
                        user_role_id: result.role_id,
                      }
                    }
                    targets={
                      {
                        e: location.location_targets.enquiries,
                        t: location.location_targets.tdrives,
                        v: location.location_targets.visits,
                        b: location.location_targets.bookings,
                        r: location.location_targets.retails,
                        l: location.location_targets.lost_drop,
                      }
                    }

                  ></ETVBRResultItem>
                </View>);
              }
              else {
                return (mainView);
              }


            }}
            keyExtractor={(item, index) => index + ''}
            onRefresh={() => this._onRefresh()}
            refreshing={this.state.isLoading}
          />




        }
        else {
          console.log('result.locations[0].sales_managers.length is = 1');
          if (result.locations[0].sales_managers[0].team_leaders && result.locations[0].sales_managers[0].team_leaders.length > 0) {
            console.log('result.locations[0].sales_managers[0].team_leaders.length is > 0');
            if (result.locations[0].sales_managers[0].team_leaders.length > 1) {
              console.log('result.locations[0].sales_managers[0].team_leaders.length > 1');


              //showTLs
              let sales_manager = result.locations[0].sales_managers[0];

              flat_list = <FlatList
                showsVerticalScrollIndicator={false}
                data={sales_manager.team_leaders}
                renderItem={({ item, index }) => {

                  let mainView = <ETVBRItemExpandable
                    leadSourceCarModelWiseETVBRInner={
                      this.leadSourceCarModelWiseETVBR.bind(this, this.props.navigation)
                    }
                    leadSourceCarModelWiseETVBR=
                    {this.leadSourceCarModelWiseETVBR.bind(this,
                      this.props.navigation,
                      item.info.name,
                      item.info.dp_url,
                      result.locations[0].location_id,
                      item.info.id,
                      item.info.user_role_id)}
                    onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                    team_leader={item} showPercentage={this.state.showPercentage} abs={
                      {
                        e: item.info.abs.enquiries,
                        t: item.info.abs.tdrives,
                        v: item.info.abs.visits,
                        b: item.info.abs.bookings,
                        r: item.info.abs.retails,
                        l: item.info.abs.lost_drop,
                      }
                    }

                    rel={
                      {
                        e: item.info.rel.enquiries,
                        t: item.info.rel.tdrives,
                        v: item.info.rel.visits,
                        b: item.info.rel.bookings,
                        r: item.info.rel.retails,
                        l: item.info.rel.lost_drop,
                      }
                    }

                    info={
                      {
                        dp_url: item.info.dp_url,
                        name: item.info.name,
                        user_id: item.info.id,
                        location_id: result.locations[0].location_id,
                        user_role_id: item.info.user_role_id,
                      }
                    }
                    targets={
                      {
                        e: item.info.targets.enquiries,
                        t: item.info.targets.tdrives,
                        v: item.info.targets.visits,
                        b: item.info.targets.bookings,
                        r: item.info.targets.retails,
                        l: item.info.targets.lost_drop,
                      }
                    }></ETVBRItemExpandable>
                    ;

                  if (index == sales_manager.team_leaders.length - 1) {
                    return (<View>
                      {mainView}
                      <ETVBRResultItem
                        onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                        showPercentage={this.state.showPercentage} result={result}

                        abs={
                          {
                            e: sales_manager.info.abs.enquiries,
                            t: sales_manager.info.abs.tdrives,
                            v: sales_manager.info.abs.visits,
                            b: sales_manager.info.abs.bookings,
                            r: sales_manager.info.abs.retails,
                            l: sales_manager.info.abs.lost_drop,
                          }
                        }
                        rel={
                          {
                            e: sales_manager.info.rel.enquiries,
                            t: sales_manager.info.rel.tdrives,
                            v: sales_manager.info.rel.visits,
                            b: sales_manager.info.rel.bookings,
                            r: sales_manager.info.rel.retails,
                            l: sales_manager.info.rel.lost_drop,
                          }
                        }

                        info={
                          {
                            user_id: global.appUserId,
                            name: 'All Team Leader',
                            location_id: result.locations[0].location_id,
                            user_role_id: sales_manager.info.id,
                          }
                        }
                        targets={
                          {
                            e: sales_manager.info.targets.enquiries,
                            t: sales_manager.info.targets.tdrives,
                            v: sales_manager.info.targets.visits,
                            b: sales_manager.info.targets.bookings,
                            r: sales_manager.info.targets.retails,
                            l: sales_manager.info.targets.lost_drop,
                          }
                        }

                      ></ETVBRResultItem>
                    </View>);
                  }
                  else {
                    return (mainView);
                  }


                }}
                keyExtractor={(item, index) => index + ''}
                onRefresh={() => this._onRefresh()}
                refreshing={this.state.isLoading}
              />






            }
            else {
              console.log('result.locations[0].sales_managers[0].team_leaders.length = 1');
              if (result.locations[0].sales_managers[0].team_leaders[0].sales_consultants.length > 1) {
                console.log('result.locations[0].sales_managers[0].team_leaders[0].sales_consultants.length > 1');
                //show scs for single tl
                let scData = result.locations[0].sales_managers[0].team_leaders[0];
                flat_list =
                  <FlatList
                    showsVerticalScrollIndicator={false}
                    data={scData.sales_consultants}
                    renderItem={({ item, index }) => {

                      let mainView = <TouchableOpacity >

                        <ETVBRItemChild
                          leadSourceCarModelWiseETVBR=
                          {this.leadSourceCarModelWiseETVBR.bind(this,
                            this.props.navigation,
                            item.info.name,
                            item.info.dp_url,
                            result.locations[0].location_id,
                            item.info.id,
                            item.info.user_role_id)}
                          onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                          showPercentage={this.state.showPercentage}
                          abs={
                            {
                              e: item.info.abs.enquiries,
                              t: item.info.abs.tdrives,
                              v: item.info.abs.visits,
                              b: item.info.abs.bookings,
                              r: item.info.abs.retails,
                              l: item.info.abs.lost_drop,
                            }
                          }
                          rel={
                            {
                              e: item.info.rel.enquiries,
                              t: item.info.rel.tdrives,
                              v: item.info.rel.visits,
                              b: item.info.rel.bookings,
                              r: item.info.rel.retails,
                              l: item.info.rel.lost_drop,
                            }
                          }
                          info={
                            {
                              dp_url: item.info.dp_url,
                              name: item.info.name,
                              user_id: item.info.id,
                              location_id: result.locations[0].location_id,
                              user_role_id: item.info.user_role_id,
                            }
                          }
                          targets={
                            {
                              e: item.info.targets.enquiries,
                              t: item.info.targets.tdrives,
                              v: item.info.targets.visits,
                              b: item.info.targets.bookings,
                              r: item.info.targets.retails,
                              l: item.info.targets.lost_drop,
                            }
                          }></ETVBRItemChild>
                      </TouchableOpacity>;

                      if (index == scData.sales_consultants.length - 1) {
                        return (<View>
                          {mainView}
                          <ETVBRResultItem
                            onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                            showPercentage={this.state.showPercentage}

                            abs={
                              {
                                e: scData.info.abs.enquiries,
                                t: scData.info.abs.tdrives,
                                v: scData.info.abs.visits,
                                b: scData.info.abs.bookings,
                                r: scData.info.abs.retails,
                                l: scData.info.abs.lost_drop,
                              }
                            }
                            rel={
                              {
                                e: scData.info.rel.enquiries,
                                t: scData.info.rel.tdrives,
                                v: scData.info.rel.visits,
                                b: scData.info.rel.bookings,
                                r: scData.info.rel.retails,
                                l: scData.info.rel.lost_drop,
                              }
                            }
                            info={
                              {
                                user_id: global.appUserId,
                                name: 'Team\'s Data',
                                location_id: result.locations[0].location_id,
                                user_role_id: result.role_id,
                              }
                            }
                            targets={
                              {
                                e: scData.info.targets.enquiries,
                                t: scData.info.targets.tdrives,
                                v: scData.info.targets.visits,
                                b: scData.info.targets.bookings,
                                r: scData.info.targets.retails,
                                l: scData.info.targets.lost_drop,
                              }
                            }

                          ></ETVBRResultItem>
                        </View>);
                      }
                      else {
                        return (mainView);
                      }


                    }}
                    keyExtractor={(item, index) => index + ''}
                    onRefresh={() => this._onRefresh()}
                    refreshing={this.state.isLoading}
                  />
              }
              else {
                console.log('result.locations[0].sales_managers[0].team_leaders[0].sales_consultants.length  == 1');

                let salesConsultant = result.locations[0].sales_managers[0].team_leaders[0].sales_consultants;
                flat_list =
                  <FlatList
                    showsVerticalScrollIndicator={false}
                    data={salesConsultant}
                    renderItem={({ item, index }) => {

                      let mainView = <TouchableOpacity >

                        <ETVBRItemChild
                          leadSourceCarModelWiseETVBR=
                          {this.leadSourceCarModelWiseETVBR.bind(this,
                            this.props.navigation,
                            item.info.name,
                            item.info.dp_url,
                            result.locations[0].location_id,
                            item.info.id,
                            item.info.user_role_id)}
                          onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                          showPercentage={this.state.showPercentage}
                          abs={
                            {
                              e: item.info.abs.enquiries,
                              t: item.info.abs.tdrives,
                              v: item.info.abs.visits,
                              b: item.info.abs.bookings,
                              r: item.info.abs.retails,
                              l: item.info.abs.lost_drop,
                            }
                          }
                          rel={
                            {
                              e: item.info.rel.enquiries,
                              t: item.info.rel.tdrives,
                              v: item.info.rel.visits,
                              b: item.info.rel.bookings,
                              r: item.info.rel.retails,
                              l: item.info.rel.lost_drop,
                            }
                          }
                          info={
                            {
                              dp_url: item.info.dp_url,
                              name: item.info.name,
                              user_id: item.info.id,
                              location_id: result.locations[0].location_id,
                              user_role_id: item.info.user_role_id,
                            }
                          }
                          targets={
                            {
                              e: item.info.targets.enquiries,
                              t: item.info.targets.tdrives,
                              v: item.info.targets.visits,
                              b: item.info.targets.bookings,
                              r: item.info.targets.retails,
                              l: item.info.targets.lost_drop,
                            }
                          }></ETVBRItemChild>
                      </TouchableOpacity>;


                      return (mainView);




                    }}
                    keyExtractor={(item, index) => index + ''}
                    onRefresh={() => this._onRefresh()}
                    refreshing={this.state.isLoading}
                  />



              }


            }
          }
          else {

          }
        }
      }


    }
    else {
      flat_list = <View style={{ alignItems: 'center', flex: 1, alignSelf: 'center', justifyContent: 'center' }}>
        <Text>No data from server</Text>
        <TouchableOpacity onPress={() => { this._onRefresh() }}>
          <Text style={{ marginTop: 10, padding: 10, borderColor: '#c4c4c4', borderRadius: 4, borderWidth: 1, }}> Reload </Text>
        </TouchableOpacity>
      </View>
      console.log('result || result.locations is empty ');
    }

    let startDateData = global.global_filter_date.startDateStr.split('-');
    let endDateData = global.global_filter_date.endDateStr.split('-');

    date = ("0" + startDateData[2]).slice(-2) + '/' + ("0" + startDateData[1]).slice(-2)
      + '-' + ("0" + endDateData[2]).slice(-2) + '/' + ("0" + endDateData[1]).slice(-2);

    return (
      <View style={{
        flex: 1,
        paddingTop: (Platform.OS) === 'ios' ? 20 : 10,
        backgroundColor: '#fff'
      }} >

        <Toast
          ref="toast"
          position='top'
          opacity={0.8}
        />


        <View style={{
          flex: 1,
          paddingBottom: 10,
          paddingLeft: 10,
          paddingRight: 10,
        }} >

          <View style={{
            flexDirection: 'row',
            height: 30,
          }}>


            <View style={{ flex: 1 / 2, height: 30, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
              <View style={{ backgroundColor: '#F4F4F4', borderWidth: 1, borderColor: '#D0D8E4', borderRadius: 6, justifyContent: 'center' }}>
                <Text style={{ fontSize: 14, padding: 4, color: '#1B143C', textAlign: 'center' }}>{date}</Text>
              </View>
              <TouchableOpacity onPress={this._showDateRangePicker.bind(this, this.props.navigation)}>

                <Image source={require('../../../images/ic_calender_blue.png')}
                  style={[styles.icon, { marginLeft: 10 }]}
                />
              </TouchableOpacity>

            </View>


            <View style={{ flexDirection: 'row', height: 30, flex: 1 / 2, alignItems: 'center', justifyContent: 'flex-end', }} >


              <PercentageSwitch
                value={this.state.showPercentage}
                onValueChange={(value) => {
                  this.setState({ showPercentage: value });
                  if (value) {
                    CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrPercentage });
                  }
                }}
              />
              <TouchableOpacity onPress={this._showETVBRFilterScreen.bind(this, this.props.navigation)}>
                <View style={{ flexDirection: 'row' }}>

                  <Image source={require('../../../images/ic_etvbr_filter.png')}
                    style={[styles.icon, { marginLeft: 10, }]} />


                  <View style={{ display: (isFilterSelected() ? 'flex' : 'none'), marginLeft: -14, marginTop: -6, backgroundColor: '#FFCA28', height: 16, width: 16, alignItems: 'center', borderRadius: 10, justifyContent: "center", alignItems: "center" }}>
                    <Text adjustsFontSizeToFit={true}
                      numberOfLines={1} style={{ textAlign: 'center', fontSize: 8, textAlignVertical: 'center', color: '#303030' }}>{getFilterSelectedCount()}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <ETVBRHeader showPercentage={this.state.showPercentage} />


          {flat_list}
          <NavigationEvents
            onWillFocus={payload => {
              this.updateOnFocusChange();
            }}
          />

        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    update_etvbr: state.updateTeamDashboard.update_etvbr,
    update_live_ef: state.updateTeamDashboard.update_live_ef
  }
}

const mapDispatchToProps = dispatch => {
  return {
    add: (update_etvbr, update_live_ef) => {
      dispatch(updateTeamDashboard({ update_etvbr, update_live_ef }))
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ETVBRLDashboard)
