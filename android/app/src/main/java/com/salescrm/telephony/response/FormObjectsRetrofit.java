package com.salescrm.telephony.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bharath on 6/12/16.
 */

public class FormObjectsRetrofit implements Serializable{

    private String statusCode;

    private String message;

    private List<Result> result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public List<Result> getResult ()
    {
        return result;
    }

    public void setResult (List<Result> result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result implements Serializable{
        private int lead_id;
        private int action_id;
        private int scheduled_activity_id;
        private FormObject form_object;
        private String user_id;

        public int getLead_id() {
            return lead_id;
        }

        public void setLead_id(int lead_id) {
            this.lead_id = lead_id;
        }

        public int getAction_id() {
            return action_id;
        }

        public void setAction_id(int action_id) {
            this.action_id = action_id;
        }

        public int getScheduled_activity_id() {
            return scheduled_activity_id;
        }

        public void setScheduled_activity_id(int scheduled_activity_id) {
            this.scheduled_activity_id = scheduled_activity_id;
        }

        public FormObject getForm_object() {
            return form_object;
        }

        public void setForm_object(FormObject form_object) {
            this.form_object = form_object;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public class FormObject implements Serializable
        {
            private int scheduled_activity_id;
            private int action_id;

            private int leadId;



            private String questionIndent;

            private List<Activity_to_schedule.Form_object.QuestionChildren> questionChildren;

            private String answerIndent;

            private String ifVisibleMandatory;

            private String newFormCondition;

            private String fQId;

            private Activity_to_schedule.DefaultFAId defaultFAId;

            private List<Activity_to_schedule.AnswerChildren> answerChildren;

            private Activity_to_schedule.ValidationObject validationObject;

            private String[] ifMeOneOfTheseMandatory;

            private String title;

            private String formInputType;

            private String hidden;

            private String answersInLine;

            private String validationRegex;

            private String answerNewLine;

            private String popupTitle;

            private String dependent_form_question_id;

            private Activity_to_schedule.Formatting formatting;

            public String getQuestionIndent ()
            {
                return questionIndent;
            }

            public int getScheduled_activity_id() {
                return scheduled_activity_id;
            }

            public void setScheduled_activity_id(int scheduled_activity_id) {
                this.scheduled_activity_id = scheduled_activity_id;
            }

            public int getAction_id() {
                return action_id;
            }

            public void setAction_id(int action_id) {
                this.action_id = action_id;
            }

            public int getLeadId() {
                return leadId;
            }


            public void setLeadId(int leadId) {
                this.leadId = leadId;
            }



            public String getfQId() {
                return fQId;
            }

            public void setfQId(String fQId) {
                this.fQId = fQId;
            }

            public void setQuestionIndent (String questionIndent)
            {
                this.questionIndent = questionIndent;
            }

            public List<Activity_to_schedule.Form_object.QuestionChildren> getQuestionChildren ()
            {
                return questionChildren;
            }

            public void setQuestionChildren (List<Activity_to_schedule.Form_object.QuestionChildren> questionChildren)
            {
                this.questionChildren = questionChildren;
            }

            public String getAnswerIndent ()
            {
                return answerIndent;
            }

            public void setAnswerIndent (String answerIndent)
            {
                this.answerIndent = answerIndent;
            }

            public String getIfVisibleMandatory ()
            {
                return ifVisibleMandatory;
            }

            public void setIfVisibleMandatory (String ifVisibleMandatory)
            {
                this.ifVisibleMandatory = ifVisibleMandatory;
            }

            public String getNewFormCondition ()
            {
                return newFormCondition;
            }

            public void setNewFormCondition (String newFormCondition)
            {
                this.newFormCondition = newFormCondition;
            }

            public Activity_to_schedule.DefaultFAId getDefaultFAId ()
            {
                return defaultFAId;
            }

            public void setDefaultFAId (Activity_to_schedule.DefaultFAId defaultFAId)
            {
                this.defaultFAId = defaultFAId;
            }

            public List<Activity_to_schedule.AnswerChildren>  getAnswerChildren ()
            {
                return answerChildren;
            }

            public void setAnswerChildren (List<Activity_to_schedule.AnswerChildren>  answerChildren)
            {
                this.answerChildren = answerChildren;
            }

            public Activity_to_schedule.ValidationObject getValidationObject ()
            {
                return validationObject;
            }

            public void setValidationObject (Activity_to_schedule.ValidationObject validationObject)
            {
                this.validationObject = validationObject;
            }

            public String[] getIfMeOneOfTheseMandatory ()
            {
                return ifMeOneOfTheseMandatory;
            }

            public void setIfMeOneOfTheseMandatory (String[] ifMeOneOfTheseMandatory)
            {
                this.ifMeOneOfTheseMandatory = ifMeOneOfTheseMandatory;
            }

            public String getTitle ()
            {
                return title;
            }

            public void setTitle (String title)
            {
                this.title = title;
            }

            public String getFormInputType ()
            {
                return formInputType;
            }

            public void setFormInputType (String formInputType)
            {
                this.formInputType = formInputType;
            }

            public String getHidden ()
            {
                return hidden;
            }

            public void setHidden (String hidden)
            {
                this.hidden = hidden;
            }

            public String getAnswersInLine ()
            {
                return answersInLine;
            }

            public void setAnswersInLine (String answersInLine)
            {
                this.answersInLine = answersInLine;
            }

            public String getValidationRegex ()
            {
                return validationRegex;
            }

            public void setValidationRegex (String validationRegex)
            {
                this.validationRegex = validationRegex;
            }

            public String getAnswerNewLine ()
            {
                return answerNewLine;
            }

            public void setAnswerNewLine (String answerNewLine)
            {
                this.answerNewLine = answerNewLine;
            }

            public String getPopupTitle ()
            {
                return popupTitle;
            }

            public void setPopupTitle (String popupTitle)
            {
                this.popupTitle = popupTitle;
            }

            public String getDependent_form_question_id ()
            {
                return dependent_form_question_id;
            }

            public void setDependent_form_question_id (String dependent_form_question_id)
            {
                this.dependent_form_question_id = dependent_form_question_id;
            }

            public Activity_to_schedule.Formatting getFormatting ()
            {
                return formatting;
            }

            public void setFormatting (Activity_to_schedule.Formatting formatting)
            {
                this.formatting = formatting;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [questionIndent = "+questionIndent+", questionChildren = "+questionChildren+", answerIndent = "+answerIndent+", ifVisibleMandatory = "+ifVisibleMandatory+", newFormCondition = "+newFormCondition+", fQId = "+fQId+", defaultFAId = "+defaultFAId+", answerChildren = "+answerChildren+", validationObject = "+validationObject+", ifMeOneOfTheseMandatory = "+ifMeOneOfTheseMandatory+", title = "+title+", formInputType = "+formInputType+", hidden = "+hidden+", answersInLine = "+answersInLine+", validationRegex = "+validationRegex+", answerNewLine = "+answerNewLine+", popupTitle = "+popupTitle+", dependent_form_question_id = "+dependent_form_question_id+", formatting = "+formatting+"]";
            }
        }
    }





}

