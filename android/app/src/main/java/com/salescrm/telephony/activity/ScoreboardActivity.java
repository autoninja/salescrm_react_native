package com.salescrm.telephony.activity;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.model.EvaluationManagerModel;
import com.salescrm.telephony.offline.FetchScoreboardData;
import com.salescrm.telephony.db.ScoreboardDB;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.model.AllLeadEnquiryListResponse;
import com.salescrm.telephony.model.BookingAllocationModel;
import com.salescrm.telephony.model.NewFilter.NewFilter;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.ActionPlanResponse;
import com.salescrm.telephony.response.ActiveEventsResponse;
import com.salescrm.telephony.response.AddLeadElementsResponse;
import com.salescrm.telephony.response.AllLeadCustomerDetailsResponse;
import com.salescrm.telephony.response.AppConfResponse;
import com.salescrm.telephony.response.AppUserResponse;
import com.salescrm.telephony.response.CarBrandModelVariantsResponse;
import com.salescrm.telephony.response.CreForActivityResponses;
import com.salescrm.telephony.response.DealershipsResponse;
import com.salescrm.telephony.response.EventCategoryResponse;
import com.salescrm.telephony.response.FilterActivityAndStagesResponse;
import com.salescrm.telephony.response.LeadMobileResponse;
import com.salescrm.telephony.response.LeadTeamUserResponse;
import com.salescrm.telephony.response.ScoreboardResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.Util;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.RetrofitError;

/**
 * Created by bharath on 03/6/16.
 */
public class ScoreboardActivity extends AppCompatActivity implements OfflineSupportListener {

    Preferences pref;
    TextView tvLeadsScore, tvTestDrivesScore, tvExchangeScore;
    View progressBarLeads, progressBarTestDrives, progressBarExchange;
    int progressLeadVal, progressTestDrivesVal, progressExchangeVal;
    int offsetProgressWidth = 5;
    int maxProgressWidth = 130;
    int[] scoreboardVal;
    TextView[] textViews;
    View[] progressViews;
    LinearLayout llBooking, llRetail;
    int[] starValues;
    LinearLayout[] llStars;
    ImageView imageViewBackButton, imageViewRefresh;
    private int apiCallCount = 0;
    private String TAG = "ScoreboardActivity:";
    private TextView tvScoreBoardGoAction, tvPersonName, tvScoreboardDate;
    private Realm realm;
    private ProgressBar progressBarLoading;

    private TextView tvTodayTestDrive, tvTodayFinance, tvTodayMeeting, tvTodayCallback, tvTotalHot, tvTotalWarm, tvTotalCold;
    private StringBuilder ids;
    private ScoreboardDB scoreboardDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoreboard);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(getApplicationContext());


        tvPersonName = (TextView) findViewById(R.id.tv_scoreboard_per);
        tvScoreboardDate = (TextView) findViewById(R.id.tv_scoreboard_date);

        tvTodayTestDrive = (TextView) findViewById(R.id.tv_scoreboard_today_td);
        tvTodayFinance = (TextView) findViewById(R.id.tv_scoreboard_today_finance);
        tvTodayMeeting = (TextView) findViewById(R.id.tv_scoreboard_today_meetings);
        tvTodayCallback = (TextView) findViewById(R.id.tv_scoreboard_today_callback);
        tvTotalHot = (TextView) findViewById(R.id.tv_scoreboard_hot);
        tvTotalWarm = (TextView) findViewById(R.id.tv_scoreboard_warm);
        tvTotalCold = (TextView) findViewById(R.id.tv_scoreboard_cold);


        imageViewBackButton = (ImageView) findViewById(R.id.iv_scoreboard_back);
        imageViewRefresh = (ImageView) findViewById(R.id.iv_scoreboard_refreshh);
        tvLeadsScore = (TextView) findViewById(R.id.tv_scoreboard_lead_score);
        tvTestDrivesScore = (TextView) findViewById(R.id.tv_scoreboard_td_score);
        tvExchangeScore = (TextView) findViewById(R.id.tv_scoreboard_ex_score);
        tvScoreBoardGoAction = (TextView) findViewById(R.id.tv_scoreboard_go_action);
        progressBarLeads = findViewById(R.id.pb_scoreboard_leads);
        progressBarTestDrives = findViewById(R.id.pb_scoreboard_td);
        progressBarExchange = findViewById(R.id.pb_scoreboard_ex);
        progressBarLoading = (ProgressBar) findViewById(R.id.scoreboard_progress);

        llBooking = (LinearLayout) findViewById(R.id.ll_booking_stars);
        llRetail = (LinearLayout) findViewById(R.id.ll_retail_stars);
        llStars = new LinearLayout[]{llBooking, llRetail};
        textViews = new TextView[]{tvLeadsScore, tvTestDrivesScore, tvExchangeScore};

        //Recommended value below should be 2 digit

        imageViewRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
                    getScoreboardData();
                } else {
                    Toast.makeText(getApplicationContext(), "Internet Required", Toast.LENGTH_SHORT).show();
                }

            }
        });
        tvScoreBoardGoAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        imageViewBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        progressViews = new View[]{progressBarLeads, progressBarTestDrives, progressBarExchange};

        setup();

        scoreboardDB = realm.where(ScoreboardDB.class).equalTo("user_id", ids.toString()).findFirst();
        if (scoreboardDB != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    init();
                }
            });
        }
    }

    private void setup() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM yyyy", Locale.getDefault());
        tvScoreboardDate.setText(dateFormatter.format(Util.getTime(0)));
        tvPersonName.setText(String.format("%s's ScoreBoard", pref.getDseName()));
//        System.out.println("FetchScoreBoardData Key1:" + realm.where(UserDetails.class).findFirst().getUserId());
        ids = new StringBuilder();
        RealmList<UserRolesDB> rolesList = realm.where(UserDetails.class).findFirst() == null ? null : realm.where(UserDetails.class).findFirst().getUserRoles();
        for (int i = 0; i < (rolesList == null ? 0 : rolesList.size()); i++) {
            ids.append(rolesList.get(i).getId());
            if (i < rolesList.size() - 1) {
                ids.append(",");
            }
        }
    }

    public void init() {
        llBooking.removeAllViews();
        llRetail.removeAllViews();
        progressLeadVal = scoreboardDB.getLeadsnumber();
        progressTestDrivesVal = scoreboardDB.getTestdrivetotal();
        progressExchangeVal = scoreboardDB.getExchange();
        scoreboardVal = new int[]{progressLeadVal, progressTestDrivesVal, progressExchangeVal};
        starValues = new int[]{scoreboardDB.getBooked(), scoreboardDB.getRetail()};

        tvTodayTestDrive.setText(String.format(Locale.getDefault(), "%d", scoreboardDB.getToday_test_drive()));
        tvTodayFinance.setText(String.format(Locale.getDefault(), "%d", scoreboardDB.getToday_finance()));
        tvTodayMeeting.setText(String.format(Locale.getDefault(), "%d", scoreboardDB.getToday_meeting()));
        tvTodayCallback.setText(String.format(Locale.getDefault(), "%d", scoreboardDB.getToday_callback()));


        tvTotalHot.setText(String.format(Locale.getDefault(), "%d", scoreboardDB.getLeads_hot()));
        tvTotalWarm.setText(String.format(Locale.getDefault(), "%d", scoreboardDB.getLeads_warm()));
        tvTotalCold.setText(String.format(Locale.getDefault(), "%d", scoreboardDB.getLeads_cold()));

        for (int i = 0; i < 3; i++) {
            textViews[i].setText(String.format(Locale.getDefault(), "%d", scoreboardVal[i]));
            ValueAnimator anim = ValueAnimator.ofInt(progressViews[i].getMeasuredWidth(),
                    scoreboardVal[i] <= 50 ? (int) Util.convertDpToPixel(scoreboardVal[i] * 2 + offsetProgressWidth, getApplicationContext()) :
                            (int) Util.convertDpToPixel(maxProgressWidth, getApplicationContext()));
            if (scoreboardVal[i] == 0) {
                anim = ValueAnimator.ofInt(progressViews[i].getMeasuredWidth(), 0);
            }
            final int finalI = i;
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int val = (Integer) valueAnimator.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = progressViews[finalI].getLayoutParams();
                    layoutParams.width = val;
                    progressViews[finalI].setLayoutParams(layoutParams);
                }

            });
            anim.setDuration(1000);
            anim.start();

        }


        Drawable drawable = VectorDrawableCompat.create(getResources(), R.drawable.ic_medal_star_24dp, getTheme());
        Drawable drawableText = VectorDrawableCompat.create(getResources(), R.drawable.ic_medal_24dp, getTheme());

        for (int i = 0; i < llStars.length; i++) {
            if (starValues[i] < 0) {
                continue;
            }
            if (starValues[i] == 0) {
                addTextView(llStars[i], "-", null, -4, 6);
                continue;
            }
            if (starValues[i] == 1) {
                addTextView(llStars[i], starValues[i] + "", drawableText, -4, 6);
                continue;
            }

            for (int k = 0; starValues[i] < 7 ? k < starValues[i] - 1 : k < 6; k++) {
                addTextView(llStars[i], "", drawable, k == 0 ? -4 : -24, 6);
            }
            addTextView(llStars[i], starValues[i] + "", drawableText, -24, 6);

        }


    }

    public void addTextView(LinearLayout parent, String text, Drawable drawable, int marginOffset, int paddingTop) {
        TextView textView = new TextView(getApplicationContext());
        textView.setWidth((int) Util.convertDpToPixel(36, getApplicationContext()));
        textView.setHeight((int) Util.convertDpToPixel(36, getApplicationContext()));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        textView.setTextColor(Color.parseColor("#8c7418"));
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(0, 0, 0, (int) Util.convertDpToPixel(paddingTop, getApplicationContext()));
        textView.setText(String.format("%s", text));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            textView.setBackground(drawable);
        } else {
            textView.setBackgroundDrawable(drawable);
        }
        parent.addView(textView);
        MarginLayoutParams params = (MarginLayoutParams) textView.getLayoutParams();
        params.leftMargin = (int) Util.convertDpToPixel(marginOffset, getApplicationContext());
    }

    @Override
    public void onBackPressed() {
        ScoreboardActivity.this.finish();
        overridePendingTransition(0, 0);
        startActivity(new Intent(ScoreboardActivity.this, HomeActivity.class));
    }

    public void getScoreboardData() {
        progressBarLoading.setVisibility(View.VISIBLE);
        new FetchScoreboardData(this, getApplicationContext()).call();
    }

    @Override
    public void onActionPlanFetched(List<ActionPlanResponse.Result.Data> data) {

    }


    @Override
    public void onC360InformationFetched(AllLeadCustomerDetailsResponse data) {

    }

    @Override
    public void onScoreboardDataFetched(ScoreboardResponse data) {
        progressBarLoading.setVisibility(View.INVISIBLE);
        scoreboardDB = realm.where(ScoreboardDB.class).equalTo("user_id", ids.toString()).findFirst();
        if (scoreboardDB != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    init();
                }
            });
        }
    }

    @Override
    public void onLeadElementsFetched(AddLeadElementsResponse addLeadElementsResponse) {

    }

    @Override
    public void onFilterDataFetched(FilterActivityAndStagesResponse data) {

    }

    @Override
    public void onTeamUsersFetched(LeadTeamUserResponse leadTeamUserResponse) {

    }

    @Override
    public void onOfflineSupportApiError(RetrofitError error, int fromId) {
        progressBarLoading.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onActivityUsersFetched(CreForActivityResponses creForActivityResponse) {

    }

    @Override
    public void onCarModelAndVariantsFetched(CarBrandModelVariantsResponse carBrandModelVariantsResponse) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }


    @Override
    public void onUserDataFetched(AppUserResponse resp) {

    }

    @Override
    public void onAppConfFetched(AppConfResponse resp) {

    }

    @Override
    public void onLeadMobileFetched(LeadMobileResponse response) {

    }

    @Override
    public void onEtvbrFiltersFetched(NewFilter response) {

    }

    @Override
    public void onDealerShipFetched(DealershipsResponse dealershipsResponse) {

    }

    @Override
    public void onBookingAllocationDataFetched(BookingAllocationModel response) {

    }

    @Override
    public void onFetchActiveEvents(ActiveEventsResponse response) {

    }

    @Override
    public void onFetchEventCategories(EventCategoryResponse response) {

    }

    @Override
    public void onFetchEvaluationManager(EvaluationManagerModel response) {

    }

    @Override
    public void onFetchAwsCredentials() {

    }
}
