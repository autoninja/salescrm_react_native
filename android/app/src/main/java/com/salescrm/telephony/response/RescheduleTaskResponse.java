package com.salescrm.telephony.response;

/**
 * Created by bannhi on 22/6/17.
 */

public class RescheduleTaskResponse {

    private String message;

    private String statusCode;

    private Result result;

    private Error error;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", statusCode = "+statusCode+", result = "+result+", error = "+error+"]";
    }


    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }

    public class Result {
        private Integer scheduled_activity_id;
        private Integer action_id;

        public Integer getScheduled_activity_id() {
            return scheduled_activity_id;
        }

        public void setScheduled_activity_id(Integer scheduled_activity_id) {
            this.scheduled_activity_id = scheduled_activity_id;
        }

        public Integer getAction_id() {
            return action_id;
        }

        public void setAction_id(Integer action_id) {
            this.action_id = action_id;
        }
    }
}