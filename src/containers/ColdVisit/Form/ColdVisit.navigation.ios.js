import React from 'react';
import { createStackNavigator } from 'react-navigation';
import ColdVisitForm from './index'
import BackIcon from '../../../components/uielements/BackIcon';
import UserInfoService from '../../../storage/user_info_service';
const ColdVisitRouter = createStackNavigator({
    AddEnquiry: {
        screen: props => (
            <ColdVisitForm
                {...props}
                userLocations={UserInfoService.getLocationsList()}
                HERE_MAP_APP_ID={global.HERE_MAP_APP_ID}
                HERE_MAP_APP_CODE={global.HERE_MAP_APP_CODE}
            />
        ),

        navigationOptions: ({ navigation }) => ({
            title: "Add Cold Visit",
            headerBackTitle: null,
            headerLeft: <BackIcon navigationProps={navigation} invert={true} />,
            headerStyle: {
                backgroundColor: '#fff',
                elevation: 0,
            },
            headerTransperent: true,
            headerTintColor: '#303030',
            headerTitleStyle: {
                fontWeight: 'bold',
                width: '100%',
                textAlign: 'left',
            },
        })
    }
});

export default ColdVisitRouter;
