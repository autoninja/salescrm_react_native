import React, { Component } from "react";
import { View, Image, Text, TouchableOpacity, Animated } from 'react-native'
import Toast, { DURATION } from 'react-native-easy-toast';
export default class GridItem extends Component {
    constructor(props) {
        super(props)
    }
    getStyle(index, dataLength) {
        let style = {};

        if (index % 2 == 0) {
            if (index == 0) {
                style.borderTopStartRadius = 10;
            }
            if (index == dataLength - 1 || index == dataLength - 2) {
                style.borderBottomStartRadius = 10;
            }

            style.marginEnd = 1;
            if (index != dataLength - 1 && index != dataLength - 2) {
                style.marginBottom = 1;
            }
        }
        else {
            if (index == 1) {
                style.borderTopEndRadius = 10;
            }
            if (index == dataLength - 1) {
                style.borderBottomEndRadius = 10;
            }
            if (index != dataLength - 1) {
                style.marginBottom = 1;
            }
        }
        return style;
    }
    render() {
        let index = this.props.index;
        let dataLength = this.props.length;
        let pntModel = this.props.pntModel;
        return (
            <TouchableOpacity
                onPress={() => this.props.onPress()}
                style={[{
                    padding: 20, flex: 1, backgroundColor: '#4C6794',
                    justifyContent: 'center', alignItems: 'center'
                },
                this.getStyle(index, dataLength),
                { opacity: pntModel.disabled ? 0.5 : 1 }]}>
                <Image
                    source={pntModel.image}
                    style={{ width: 36, height: 36 }}
                    resizeMode={"contain"}
                />
                <Text style={{ textAlign: 'center', color: '#fff', fontSize: 16, marginTop: 20, flexWrap: 'wrap' }}>{pntModel.title}</Text>
            </TouchableOpacity>
        )
    }
}