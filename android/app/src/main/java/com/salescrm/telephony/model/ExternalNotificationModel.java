package com.salescrm.telephony.model;

import java.io.Serializable;

/**
 * Created by bharath on 28/6/17.
 */

public class ExternalNotificationModel implements Serializable {
    private String message;

    private Body body;

    private String title;

    private String event;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Body getBody ()
    {
        return body;
    }

    public void setBody (Body body)
    {
        this.body = body;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getEvent ()
    {
        return event;
    }

    public void setEvent (String event)
    {
        this.event = event;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", body = "+body+", title = "+title+", event = "+event+"]";
    }

    public class Body implements Serializable
    {
        private Integer activity_id;

        private Integer scheduled_activity_id;

        private Integer lead_id;

        private Integer location_id;

        public Integer getActivity_id ()
        {
            return activity_id;
        }

        public void setActivity_id (Integer activity_id)
        {
            this.activity_id = activity_id;
        }

        public Integer getScheduled_activity_id ()
        {
            return scheduled_activity_id;
        }

        public void setScheduled_activity_id (Integer scheduled_activity_id)
        {
            this.scheduled_activity_id = scheduled_activity_id;
        }

        public Integer getLead_id ()
        {
            return lead_id;
        }

        public void setLead_id (Integer lead_id)
        {
            this.lead_id = lead_id;
        }

        public Integer getLocation_id() {
            return location_id;
        }

        public void setLocation_id(Integer location_id) {
            this.location_id = location_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [activity_id = "+activity_id+", scheduled_activity_id = "+scheduled_activity_id+", lead_id = "+lead_id+"]";
        }
    }


}
