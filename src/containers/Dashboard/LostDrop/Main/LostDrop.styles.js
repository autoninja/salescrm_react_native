import { StyleSheet } from 'react-native';
export default styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 10,
    marginTop:10,
  },
  heading: {
    fontSize: 20,
    textAlign: 'center',
    marginBottom: 10
  },
  subContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    width: '100%'
  },
  button: {
    alignSelf: 'stretch',
    width: '100'
  },
  innerView: {
    flex:1,
    flexWrap:'wrap',
    alignItems: 'center',
    margin: 5,
    borderRadius:5,
    backgroundColor:'#fff',
    elevation:3,
  },
  innerRow: {
    width: '100%',
    flexDirection: 'row',
  },
  textTitle: {
    flex:1,
    fontSize: 18,
    textAlign: 'left',
    alignItems: 'flex-start',
    color: '#2e5e86',
    padding:6,
    marginLeft:10,

  },
  textTitleclickable: {
    flex:1,
    fontSize: 20,
    textAlign: 'right',
    alignItems: 'flex-end',
    color: '#00ACF1',
    textDecorationLine:'underline',
    fontWeight:'700',
    padding:6
  },
  line: {
    width: '100%',
    height: 0.5,
    backgroundColor: '#E9E9E9',
  },
  icon:{
    width: 24,
    height: 24,
  },

  shadowsView: {
    width:'100%',
    shadowColor: "#808080",
    shadowOpacity: 0.4,
    shadowRadius: 2,
    shadowOffset: {
     height: 1,
     width: 0
   },
 },
 wrapView: {
   marginTop:-10,
   flexWrap: 'wrap',
   flexDirection:'row',
   alignItems: 'flex-start',
   justifyContent:'center',
   backgroundColor:'#fff',
 },
 wrapButton: {
   backgroundColor:'#808080',
   paddingLeft:4,
   paddingRight:4,
   paddingTop:2,
   paddingBottom:2,
   margin:5,
   marginLeft:10,
   borderRadius:5,
   elevation:2,
   width:'40%',
   alignItems:'center'
 },
 wrapText: {
   color:'#fff',
   fontSize:16,
 }
});
