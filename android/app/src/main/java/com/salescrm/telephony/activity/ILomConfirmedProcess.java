package com.salescrm.telephony.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.dbOperation.TasksDbOperation;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.model.SubmitILomModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.BookingConfirmResponse;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ILomConfirmedProcess {
    ProgressDialog progressDialog;
    Realm realm;
    Preferences pref;
    String leadId, scheduledActivityId, leadLastUpdated;
    Context context;
    Activity activity;
    int from;
    private static final String TITLE_IL_DONE = "Already Renewed by ICICI Lombard";
    private static final String TITLE_SALE_CONFIRMED = "Sales Confirmed";
    private int answerId;
    private String selectedTenure;
    public static final int FROM_C_360 = 1;
    static final int FROM_FORM_RENDERING = 2;
    public static final int FROM_FORM_TASKS_LIST = 3;
    private FormSubmissionInputData prevForm = null;
    private String path;

    public void show(Activity activity, Context context, String leadId, String scheduledActivityId, int answerId,
                     FormSubmissionInputData formSubmissionInputData, int from) {
        this.activity = activity;
        this.context = context;
        progressDialog = new ProgressDialog(context);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        Preferences.load(context);

        this.leadId = leadId;
        this.scheduledActivityId = scheduledActivityId;
        this.leadLastUpdated = getLeadLastUpdated();
        this.from = from;
        this.prevForm = formSubmissionInputData;
        this.answerId = answerId;
        startBookingProcess();
    }

    private String getLeadLastUpdated() {
        SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Util.getInt(leadId))
                .findAllSorted("leadLastUpdated", Sort.DESCENDING).first();
        if (salesCRMRealmTable != null) {
            return salesCRMRealmTable.getLeadLastUpdated();
        }
        return null;
    }

    private void startBookingProcess() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.dialog_i_lom_form_submission);

        RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radio_group_i_lom_form);
        FrameLayout frameSubmission = (FrameLayout) dialog.findViewById(R.id.frame_i_lom_form_submission);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tv_custom_dialog_title);
        TextView tvYes = (TextView) dialog.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvNo = (TextView) dialog.findViewById(R.id.bt_custom_dialog_no);

        if (answerId == WSConstants.FormAnswerId.IL_RENEWED) {
            tvTitle.setText(TITLE_IL_DONE);
            radioGroup.setVisibility(View.GONE);
            path = "book_renewal";
        } else if (answerId == WSConstants.FormAnswerId.SALES_CONFIRMED) {
            tvTitle.setText(TITLE_SALE_CONFIRMED);
            if(pref.isClientILBank()) {
                radioGroup.setVisibility(View.GONE);
                tvYes.setTextColor(Color.parseColor("#ffffff"));
                tvYes.setEnabled(true);
            }
            else {
                radioGroup.setVisibility(View.VISIBLE);
                tvYes.setTextColor(Color.parseColor("#808080"));
                tvYes.setEnabled(false);
            }


            path = "book_sale_confirmed";
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                tvYes.setTextColor(Color.parseColor("#ffffff"));
                tvYes.setEnabled(true);
                switch (i) {
                    case R.id.radio_button_i_lom_form_one_year:
                        selectedTenure = "1 Year";
                        break;
                    case R.id.radio_button_i_lom_form_two_year:
                        selectedTenure = "2 Year";
                        break;
                    case R.id.radio_button_i_lom_form_three_year:
                        selectedTenure = "3 Year";
                        break;
                }
            }
        });

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
                dialog.dismiss();
            }
        });
        tvNo = (TextView) dialog.findViewById(R.id.bt_custom_dialog_no);

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void submitForm() {
        progressDialog.setMessage("Please wait..");
        progressDialog.show();
        SubmitILomModel submitILomModel = new SubmitILomModel();
        submitILomModel.setLead_id(leadId);
        submitILomModel.setLead_last_updated(leadLastUpdated);
        if (answerId == WSConstants.FormAnswerId.SALES_CONFIRMED && pref.isClientILom()) {
            submitILomModel.setTenure(selectedTenure);
        }
        if (prevForm == null) {
            FormSubmissionInputData formSubmissionInputData = new FormSubmissionInputData();
            formSubmissionInputData.setLead_id(leadId);
            formSubmissionInputData.setLead_last_updated(leadLastUpdated);
            formSubmissionInputData.setScheduled_activity_id(scheduledActivityId);
        } else {
            //Came from FormRendering
            submitILomModel.setForm_object(prevForm);
        }

        ApiUtil.GetRestApiWithHeader(Preferences.getAccessToken()).submitILom(path, submitILomModel, new Callback<BookingConfirmResponse>() {
            @Override
            public void success(BookingConfirmResponse o, Response response) {
                Util.updateHeaders(response.getHeaders());
                if (!o.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    handleFailure();
                } else {
                    if (o.getResult() != null) {
                        TasksDbOperation.getInstance().updateLeadLastUpdated(realm, leadId, o.getResult().getLead_last_updated());
                        handleSuccess();
                        Util.showToast(context, "Form has been submitted", Toast.LENGTH_LONG);
                    } else {
                        handleFailure();

                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                handleFailure();
            }
        });

    }

    private void handleFailure() {
        Util.showToast(context, "Form will be refreshed, Try again", Toast.LENGTH_SHORT);
        progressDialog.dismiss();
        navigate();
    }

    private void handleSuccess() {
        realm.beginTransaction();
        try {
            SalesCRMRealmTable data = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", Util.getInt(scheduledActivityId)).findFirst();
            if (data != null) {
                data.setDone(true);
            }
        } catch (Exception e) {
        }

        realm.commitTransaction();
        progressDialog.dismiss();
        navigate();
    }

    private void navigate() {
        switch (from) {
            case FROM_C_360:
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        if(activity != null) {
                            Intent intent = activity.getIntent();
                            activity.overridePendingTransition(0, 0);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            activity.finish();
                            activity.overridePendingTransition(0, 0);
                            Intent in = new Intent(context, C360Activity.class);
                            context.startActivity(in);
                        }
                        else {
                            Intent in = new Intent(context, C360Activity.class);
                            in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(in);
                        }

                    }
                });
                break;
            case FROM_FORM_TASKS_LIST:
                Intent in = new Intent(context, C360Activity.class);
                context.startActivity(in);
                break;
            case FROM_FORM_RENDERING:
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = activity.getIntent();
                        activity.overridePendingTransition(0, 0);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        activity.finish();
                    }
                });
                break;
        }

    }

}
