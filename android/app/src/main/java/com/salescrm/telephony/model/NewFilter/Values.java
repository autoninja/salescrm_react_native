package com.salescrm.telephony.model.NewFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prateek on 4/8/18.
 */

public class Values {

    private String id;
    private boolean has_children;
    private boolean set_checked;
    private List<Subcategories> subcategories = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Subcategories> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<Subcategories> subcategories) {
        this.subcategories = subcategories;
    }

    public boolean isHas_children() {
        return has_children;
    }

    public void setHas_children(boolean has_children) {
        this.has_children = has_children;
    }

    public boolean isSet_checked() {
        return set_checked;
    }

    public void setSet_checked(boolean set_checked) {
        this.set_checked = set_checked;
    }
}
