package com.salescrm.telephony.model.booking;

public class ApiInputExchFinDetails {

    private String finance_type, exchanged;

    public ApiInputExchFinDetails(String finance, String exchange) {
        this.finance_type = finance;
        this.exchanged = exchange;
    }

    public String getFinance() {
        return finance_type;
    }

    public void setFinance(String finance) {
        this.finance_type = finance;
    }

    public String getExchange() {
        return exchanged;
    }

    public void setExchange(String exchange) {
        this.exchanged = exchange;
    }
}
