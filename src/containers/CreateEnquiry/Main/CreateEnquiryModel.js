let CreateEnquiryModel = {
  getInputDataServer: (enquiryFields, location_data) => {
    let customerDetailsUser = enquiryFields[1].data;
    let buyerDetailsUser = enquiryFields[0].data;
    let leadDetailsUser = enquiryFields[2].data;
    let interestedVehicleDetailsUser = enquiryFields[3].data;
    let extraVehicleDetailsUser = enquiryFields[4].data;
    let activityDetailsUser = enquiryFields[5].data;

    let pin_code;
    let residence_address;
    let residence_locality;
    let city;

    let office_pin_code;
    let office_address;
    let office_locality;
    let office_city;

    let state;

    if (customerDetailsUser.customerAddressType.id == 1) {
      //Pass office address
      office_pin_code = customerDetailsUser.customerPincode;
      office_address = customerDetailsUser.customerAddress;
      office_locality = customerDetailsUser.customerLocality
        ? customerDetailsUser.customerLocality.text
        : "";
      office_city = customerDetailsUser.customerCity;
      state = customerDetailsUser.customerState;
    } else if (customerDetailsUser.customerAddressType.id == 2) {
      //Pass home address

      pin_code = customerDetailsUser.customerPincode;
      residence_address = customerDetailsUser.customerAddress;
      residence_locality = customerDetailsUser.customerLocality
        ? customerDetailsUser.customerLocality.text
        : "";
      city = customerDetailsUser.customerCity;
      state = customerDetailsUser.customerState;
    }

    //Lead data for server
    let mobileNumbers = [];
    let emails = [];
    let interestedVehicles = [];
    let extraVehicles = [];

    let userDetails = [];
    let activityOwner = new ActivityOwner(
      activityDetailsUser.salesConsultant.id,
      4
    );
    if (activityDetailsUser.isClientTwoWheeler) {
      activityOwner = new ActivityOwner(
        activityDetailsUser.salesConsultant.team_leader_id,
        6
      );
    }

    mobileNumbers.push(
      new CustomerMobileNumber(customerDetailsUser.primaryMobile, true)
    );
    customerDetailsUser.secondoryMobile.map(number => {
      mobileNumbers.push(new CustomerMobileNumber(number, false));
    });

    emails.push(new CustomerEmail(customerDetailsUser.customerEmail, true));
    customerDetailsUser.secondoryEmail.map(email => {
      emails.push(new CustomerEmail(email, false));
    });

    let isActvityTargetAdded = false;
    interestedVehicleDetailsUser.map((vehicle, index) => {
      if (vehicle && vehicle.vehicleModel) {
        let is_activity_target = false;
        if (
          !isActvityTargetAdded &&
          activityDetailsUser.testDriveVehicle &&
          vehicle.vehicleModel.id == activityDetailsUser.testDriveVehicle.id
        ) {
          is_activity_target = true;
          isActvityTargetAdded = true;
        }
        interestedVehicles.push(
          new InterestedCarDetails(
            vehicle.vehicleModel.id,
            vehicle.vehicleModel.text,
            vehicle.vehicleVariant ? vehicle.vehicleVariant.id : null,
            vehicle.vehicleVariant ? vehicle.vehicleVariant.text : null,
            vehicle.vehicleColor ? vehicle.vehicleColor.id : null,
            vehicle.vehicleColor ? vehicle.vehicleColor.text : null,
            vehicle.vehicleFuelType ? vehicle.vehicleFuelType.id : null,
            vehicle.vehicleFuelType ? vehicle.vehicleFuelType.text : null,
            index == 0 ? true : false,
            is_activity_target
          )
        );
      }
    });

    extraVehicleDetailsUser.map((vehicle, index) => {
      if (vehicle && vehicle.vehicleModel) {
        extraVehicles.push(
          new ExtraCarDetails(
            vehicle.vehicleModel.id,
            vehicle.vehicleYear,
            vehicle.vehicleRegNumber,
            vehicle.vehicleKMRun,
            index == 0 ? true : false,
            false
          )
        );
      }
    });

    userDetails.push(
      new UserDetails(activityDetailsUser.salesConsultant.id, 4)
    );

    let customerInput = new CustomerDetails(
      buyerDetailsUser.buyerType,
      customerDetailsUser.customerType.server_id,
      customerDetailsUser.customerTitle,
      customerDetailsUser.customerNameFirst,
      customerDetailsUser.customerNameLast,
      customerDetailsUser.customerAge,
      customerDetailsUser.customerCompany,
      customerDetailsUser.customerDesignation,
      pin_code,
      residence_address,
      residence_locality,
      city,
      office_pin_code,
      office_address,
      office_locality,
      office_city,
      state
    );
    let leadInput = new LeadData(
      buyerDetailsUser.showroomLocation.id,
      buyerDetailsUser.leadTagId,
      customerInput,
      customerDetailsUser.expectedBuyingDate,
      customerDetailsUser.modeOfPayment
        ? customerDetailsUser.modeOfPayment.text
        : null,
      mobileNumbers,
      emails,
      leadDetailsUser.enquirySource.id,
      leadDetailsUser.enquirySourceCategory
        ? leadDetailsUser.enquirySourceCategory.id
        : null,
      leadDetailsUser.vinNumber,
      leadDetailsUser.eventSelected ? leadDetailsUser.eventSelected.id : null,
      new Date(),
      interestedVehicles,
      buyerDetailsUser.buyerType == "3" ? extraVehicles : [],
      buyerDetailsUser.buyerType == "2" ? extraVehicles : [],
      userDetails,
      location_data
    );

    let activityInput = new ActivityData(
      activityDetailsUser.activity.id,
      activityDetailsUser.activity.text,
      activityDetailsUser.activityScheduledDate,
      activityDetailsUser.remarks,
      activityOwner,
      activityDetailsUser.isClientTwoWheeler
        ? activityDetailsUser.salesConsultant.team_leader_name
        : activityDetailsUser.salesConsultant.text,
      activityDetailsUser.testDriveVehicle
        ? activityDetailsUser.testDriveVehicle.id
        : null,
      activityDetailsUser.testDriveVehicle
        ? activityDetailsUser.testDriveVehicle.text
        : null,
      activityDetailsUser.activityScheduledDate,
      activityDetailsUser.visitType,
      activityDetailsUser.visitAddress,
      activityDetailsUser.showroomLocation
        ? activityDetailsUser.showroomLocation.id
        : null
    );

    return new CreateEnquiryInputServer(
      leadInput,
      activityInput,
      activityDetailsUser.remarks
    );
  }
};

class CreateEnquiryInputServer {
  constructor(lead_data, activity_data, remarks) {
    this.lead_data = lead_data;
    this.activity_data = activity_data;
    this.remarks = remarks;
  }
}
class ActivityData {
  constructor(
    activity,
    activityName,
    scheduledDateTime,
    remarks,
    activity_owner,
    assignName,
    carId,
    carName,
    visit_time,
    visit_type,
    address,
    location_id
  ) {
    this.activity = activity;
    this.activityName = activityName;
    this.scheduledDateTime = scheduledDateTime;
    this.remarks = remarks;
    this.activity_owner = activity_owner;
    this.assignName = assignName;
    this.carId = carId;
    this.carName = carName;
    this.visit_time = visit_time;
    this.visit_type = visit_type;
    this.address = address;
    this.location_id = location_id;
  }
}

class ActivityOwner {
  constructor(user_id, role_id) {
    this.user_id = user_id;
    this.role_id = role_id;
  }
}

class LeadData {
  constructor(
    location_id,
    lead_tag_id,
    customer_details,
    exp_closing_date,
    mode_of_pay,
    mobile_numbers,
    emails,
    lead_source_id,
    lead_source_category_id,
    ref_vin_reg_no,
    event_id,
    lead_enq_date,
    car_details,
    ex_car_details,
    ad_car_details,
    user_details,
    location_data
  ) {
    this.location_id = location_id;
    this.lead_tag_id = lead_tag_id;
    this.customer_details = customer_details;
    this.exp_closing_date = exp_closing_date;
    this.mode_of_pay = mode_of_pay;
    this.mobile_numbers = mobile_numbers;
    this.emails = emails;
    this.lead_source_id = lead_source_id;
    this.lead_source_category_id = lead_source_category_id;
    this.ref_vin_reg_no = ref_vin_reg_no;
    this.event_id = event_id;
    this.lead_enq_date = lead_enq_date;
    this.car_details = car_details;
    this.ex_car_details = ex_car_details;
    this.ad_car_details = ad_car_details;
    this.user_details = user_details;
    this.location_data = location_data;
  }
}

class CustomerDetails {
  constructor(
    buyer_type_id,
    type_of_cust_id,
    title,
    first_name,
    last_name,
    age,
    company_name,
    occupation,
    pin_code,
    residence_address,
    locality,
    city,
    office_pin_code,
    office_address,
    office_locality,
    office_city,
    state
  ) {
    this.buyer_type_id = buyer_type_id;
    this.type_of_cust_id = type_of_cust_id;
    this.title = title;
    this.first_name = first_name;
    this.last_name = last_name;
    this.age = age;
    this.company_name = company_name;
    this.occupation = occupation;

    this.pin_code = pin_code;
    this.residence_address = residence_address;
    this.locality = locality;
    this.city = city;

    this.office_pin_code = office_pin_code;
    this.office_address = office_address;
    this.office_locality = office_locality;
    this.office_city = office_city;

    this.state = state;
  }
}

class CustomerMobileNumber {
  constructor(number, is_primary) {
    this.number = number;
    this.is_primary = is_primary;
  }
}

class CustomerEmail {
  constructor(address, is_primary) {
    this.address = address;
    this.is_primary = is_primary;
  }
}

class InterestedCarDetails {
  constructor(
    model_id,
    model_name,
    variant_id,
    variant_name,
    color_id,
    color_name,
    fuel_type_id,
    fuel_name,
    is_primary,
    is_activity_target
  ) {
    this.model_id = model_id;
    this.variant_id = variant_id;
    this.color_id = color_id;
    this.fuel_type_id = fuel_type_id;
    this.is_primary = is_primary;
    this.is_activity_target = is_activity_target;
  }
}

class ExtraCarDetails {
  constructor(
    model_id,
    purchase_date,
    reg_no,
    kms_run,
    is_primary,
    is_activity_target
  ) {
    this.model_id = model_id;
    this.purchase_date = purchase_date;
    this.reg_no = reg_no;
    this.kms_run = kms_run;
    this.is_primary = is_primary;
    this.is_activity_target = is_activity_target;
  }
}

class UserDetails {
  constructor(user_id, role_id) {
    this.user_id = user_id;
    this.role_id = role_id;
  }
}

module.exports = CreateEnquiryModel;
