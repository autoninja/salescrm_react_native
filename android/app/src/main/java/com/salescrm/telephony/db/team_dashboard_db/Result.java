package com.salescrm.telephony.db.team_dashboard_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.salescrm.telephony.db.team_dashboard_gm_db.AbsTotal;
import com.salescrm.telephony.db.team_dashboard_gm_db.RelTotal;
import com.salescrm.telephony.model.EtvbrPojo;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 17/6/17.
 */

public class Result extends RealmObject {

    @SerializedName("team_id")
    @Expose
    private Integer teamId;
    @SerializedName("team_name")
    @Expose
    private String teamName;
    @SerializedName("abs")
    @Expose
    public RealmList<Ab> abs = new RealmList<>();
    @SerializedName("rel")
    @Expose
    public RealmList<Rel> rel = new RealmList<>();

    @SerializedName("abs_total")
    @Expose
    private AbsTotal absTotal;
    @SerializedName("rel_total")
    @Expose
    private RelTotal relTotal;

    public AbsTotal getAbsTotal() {
        return absTotal;
    }

    public void setAbsTotal(AbsTotal absTotal) {
        this.absTotal = absTotal;
    }

    public RelTotal getRelTotal() {
        return relTotal;
    }

    public void setRelTotal(RelTotal relTotal) {
        this.relTotal = relTotal;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public RealmList<Ab> getAbs() {
        return abs;
    }

    public void setAbs(RealmList<Ab> abs) {
        this.abs = abs;
    }

    public RealmList<Rel> getRel() {
        return rel;
    }

    public void setRel(RealmList<Rel> rel) {
        this.rel = rel;
    }
}
