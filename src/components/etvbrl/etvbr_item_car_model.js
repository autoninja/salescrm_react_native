import React, { Component } from 'react'
import { View, Text, Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Alert} from 'react-native'


export default class ETVBRItemCarModel extends Component {
  constructor(props) {
    super(props);
    this.state ={showImagePlaceHolder:true};
  }

  render() {


    console.log('showPercentage:'+this.props.showPercentage);
    even = this.props.index %2 == 0;
    if(this.props.showPercentage) {
      return (
      <View style={{flexDirection: 'row', height:60, backgroundColor:(even?'#0000003c':'#2e5e86'), alignItems:'center'}}>

      <View style={{flex:1.5, alignItems:'center'}} >

      <Text style={{ color:'#FFFF00', padding:4}} >{this.props.info.car}</Text>


      </View>


      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.rel.e}</Text>
        </View>
      </View>



      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.rel.t}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.rel.v}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.rel.b}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.rel.r}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

          <Text style={{ color:'red'}} >{this.props.rel.l}</Text>
      </View>


      </View>
    );
    }
    else {
    return(
      <View style={{flexDirection: 'row', height:60, backgroundColor:(even?'#0000003c':'#2e5e86'), alignItems:'center'}}>

      <View style={{flex:1.5, alignItems:'center'}} >

        <Text style={{ color:'#FFFF00',padding:4}} >{this.props.info.car}</Text>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.abs.e}</Text>
        </View>

      </View>




      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.abs.t}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.abs.v}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.abs.b}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.abs.r}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

          <Text style={{ color:'red'}} >{this.props.abs.l}</Text>
      </View>


      </View>
    );
  }
  }
}
