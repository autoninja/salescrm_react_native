import React, { Component } from "react";
import { View, Image, TouchableOpacity, NativeModules } from "react-native";

class HamburgerIcon extends Component {
  toggleDrawer() {
    console.log(this.props.navigationProps);
    if (global.fromAndroid) {
      NativeModules.ReactNativeToAndroid.openDrawerMenu();
    } else {
      this.props.navigationProps.toggleDrawer();
    }
  }

  render() {
    return (
      <View style={{ flexDirection: "row" }}>
        <TouchableOpacity
          onPress={this.toggleDrawer.bind(this)}
          hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
        >
          <Image
            source={require("../../images/ic_menu.png")}
            style={{ width: 24, height: 24, marginLeft: 20 }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default HamburgerIcon;
