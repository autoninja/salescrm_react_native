import React, { Component } from 'react';
import { View, Text, FlatList, Image, Alert } from 'react-native';
import PercentageSwitch from '../../../components/uielements/PercentageSwitch'
import Header from '../../../components/leadSourceCarModelETVBR/Header'
import CarModelItemExpandableETVBLR from '../../../components/leadSourceCarModelETVBR/CarModelItemExpandableETVBLR';
import CarModelItemExpandableLiveEF from '../../../components/leadSourceCarModelETVBR/CarModelItemExpandableLiveEF'
import TotalETVBRL from '../../../components/leadSourceCarModelETVBR/TotalETVBRL'
import TotalLiveEF from '../../../components/leadSourceCarModelETVBR/TotalLiveEF';
import LiveEFFooter from '../../../components/liveEF/live_ef_footer';

import CleverTapPush from '../../../clevertap/CleverTapPush'
import {eventTeamDashboard} from '../../../clevertap/CleverTapConstants';
import { NavigationEvents } from "react-navigation";

export default class CarModelDistribution extends Component {
    
    constructor(props) {
        super(props);
        this.state = {showPercentage:false}
    }

    componentDidMount () {
        // this.props.navigation.addListener('willFocus', (route) => { 
        //     //tab changed 
            
        // });
    }
    updateOnFocusChange(){
        if(this.props.isFromLiveEF) {
            CleverTapPush.pushEvent(eventTeamDashboard.event, {'Dashboard Type': eventTeamDashboard.keyType.valuesType.liveEFCars});
        }
        else {
            CleverTapPush.pushEvent(eventTeamDashboard.event, {'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrCars});
        }
    }
    render() {
        let modelWise = [];
        if(this.props.modelWise) {
            modelWise = this.props.modelWise;
        }
        let isFromLiveEF = this.props.isFromLiveEF;
        if(modelWise.length == 0) {
            return(
                <View style={{flex:1, margin:10, alignItems:'center', justifyContent:'center'}}>
                    <Text style={{fontSize:16, color:'#303030'}}>No Data</Text>
                    <NavigationEvents
                        onWillFocus={payload => {
                        this.updateOnFocusChange();
                        }}
                    />
                </View>
            )
        }
        return(
            <View style={{flex:1, margin:10}}>
                <NavigationEvents
                        onWillFocus={payload => {
                        this.updateOnFocusChange();
                        }}
                />
                <View style={{alignSelf:'flex-end', marginBottom:8,marginRight:2}}>
                <PercentageSwitch 
                value={this.state.showPercentage}
                onValueChange={(value)=>{
                    this.setState({showPercentage:value})
                }}
                />
                </View>
                <Header isFromLiveEF = {isFromLiveEF} showPercentage={this.state.showPercentage}/>

                <FlatList
                showsVerticalScrollIndicator={false}
                data = {modelWise}
                style={{marginTop:10}}
                keyExtractor= {(item, index) => index.toString() }
                renderItem={({item, index})=> {
                    let totalData = {
                        rel:{
                            e:this.props.total.rel.enquiries,
                            t:this.props.total.rel.tdrives,
                            v:this.props.total.rel.visits,
                            b:this.props.total.rel.bookings,
                            r:this.props.total.rel.retails,
                            l:this.props.total.rel.lost_drop,
                            ex:this.props.total.rel.exchanged,
                            f_i:this.props.total.rel.in_house_financed,
                            f_o:this.props.total.rel.out_house_financed,
                            pb:this.props.total.rel.pending_bookings,
                            le:this.props.total.rel.live_enquiries,
                        },
                        abs:{
                            e:this.props.total.abs.enquiries,
                            t:this.props.total.abs.tdrives,
                            v:this.props.total.abs.visits,
                            b:this.props.total.abs.bookings,
                            r:this.props.total.abs.retails,
                            l:this.props.total.abs.lost_drop,
                            ex:this.props.total.abs.exchanged,
                            f_i:this.props.total.abs.in_house_financed,
                            fin_o:this.props.total.abs.out_house_financed,
                            pb:this.props.total.abs.pending_bookings,
                            le:this.props.total.abs.live_enquiries,
                        },
                        info: {
                            sourceName: item.name,
                            sourceId:item.id,
                        },
                        sources:item.sources
                    }
                        
                    let data = {
                            rel:{
                                e:item.rel.enquiries,
                                t:item.rel.tdrives,
                                v:item.rel.visits,
                                b:item.rel.bookings,
                                r:item.rel.retails,
                                l:item.rel.lost_drop,
                                ex:item.rel.exchanged,
                                f_i:item.rel.in_house_financed,
                                f_o:item.rel.out_house_financed,
                                pb:item.rel.pending_bookings,
                                le:item.rel.live_enquiries,
                            },
                            abs:{
                                e:item.abs.enquiries,
                                t:item.abs.tdrives,
                                v:item.abs.visits,
                                b:item.abs.bookings,
                                r:item.abs.retails,
                                l:item.abs.lost_drop,
                                ex:item.abs.exchanged,
                                f_i:item.abs.in_house_financed,
                                f_o:item.abs.out_house_financed,
                                pb:item.abs.pending_bookings,
                                le:item.abs.live_enquiries,
                            },
                            info: {
                                carName: item.name,
                                carId:item.id,
                            },
                            sources:item.source_categories
                        }
                        if(isFromLiveEF) {
                            return(
                                <View> 
                                    <CarModelItemExpandableLiveEF
                                    data={data}
                                    key={index+''} 
                                    showPercentage={this.state.showPercentage}/>
                                    {index == modelWise.length-1 && 
                                    <View>
                                        <TotalLiveEF 
                                        title="Achieved"
                                        data={totalData} 
                                        key={index+''} 
                                        showPercentage={this.state.showPercentage} />
                                        <LiveEFFooter/>
                                    </View>
                                    }
                                </View>    
                                );
                        }
                        else {
                            return(
                                <View> 
                                    <CarModelItemExpandableETVBLR 
                                    data={data}
                                    key={index+''} 
                                    showPercentage={this.state.showPercentage}/>
                                    {index == modelWise.length-1 && 
                                     <TotalETVBRL 
                                     title="Achieved"
                                     data={totalData} 
                                     key={index+''} 
                                     showPercentage={this.state.showPercentage} />
                                    }
                                </View>    
                                );
                        }
                       
                    
                }}
                />
                
            </View>
        )
    }
}