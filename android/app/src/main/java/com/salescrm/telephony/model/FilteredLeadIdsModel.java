package com.salescrm.telephony.model;

/**
 * Created by bharath on 23/8/17.
 */

public class FilteredLeadIdsModel {
    private String statusCode;

    private String message;

    private Result[] result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result[] getResult ()
    {
        return result;
    }

    public void setResult (Result[] result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private String lead_id;

        public String getLead_id ()
        {
            return lead_id;
        }

        public void setLead_id (String lead_id)
        {
            this.lead_id = lead_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [lead_id = "+lead_id+"]";
        }
    }
}
