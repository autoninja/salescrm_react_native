import { UPDATE_CURRENT_VIEW } from '../actions/main_header_types';

const initialState = {
  current_view: 'TODAYS_TASK',
  isEventTabActive: false,
};

const mainHeaderReducer = (state = initialState, action) => {
  switch(action.type) {
    case UPDATE_CURRENT_VIEW:
      return {
        ...state,
        current_view: action.payload.current_view,
        isEventTabActive: action.payload.isEventTabActive,
      };
    default:
      return state;
  }
}

export default mainHeaderReducer;
