package com.salescrm.telephony.db;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 14-10-2016.
 */

public class FormObjectQuestionValues extends RealmObject {

    private String fQId;

    private String title;
    private String popupTitle;
    private String formInputType;
    private String formatting;
    private String questionIndent;
    private String validationRegex;
    private boolean ifVisibleMandatory;
    private String ifMeOneOfTheseMandatory;
    private String hidden;
    private Integer editable;

    public RealmList<FormObjectAnswerChildren> answerChildren;

    @SerializedName("questionChildren")
    public RealmList<FormObjectQuestionChildren> questionChildren;

    public RealmList<FormObjectQuestionValuesDefaultFAId> formObjectQuestionValuesDefaultFAId;

    private DefaultFAId defaultFAId;
    private ValidationObject validationObject;
    private String newFormCondition;
    private String dependent_form_question_id;

    private Integer questionId;
    private RealmList<DefaultCases> defaultCases;

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public RealmList<DefaultCases> getDefaultCases() {
        return defaultCases;
    }

    public void setDefaultCases(RealmList<DefaultCases> defaultCases) {
        this.defaultCases = defaultCases;
    }

    public String getfQId() {
        return fQId;
    }

    public void setfQId(String fQId) {
        this.fQId = fQId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPopupTitle() {
        return popupTitle;
    }

    public void setPopupTitle(String popupTitle) {
        this.popupTitle = popupTitle;
    }

    public String getFormInputType() {
        return formInputType;
    }

    public void setFormInputType(String formInputType) {
        this.formInputType = formInputType;
    }

    public String getFormatting() {
        return formatting;
    }

    public void setFormatting(String formatting) {
        this.formatting = formatting;
    }

    public String getQuestionIndent() {
        return questionIndent;
    }

    public void setQuestionIndent(String questionIndent) {
        this.questionIndent = questionIndent;
    }

    public String getValidationRegex() {
        return validationRegex;
    }

    public void setValidationRegex(String validationRegex) {
        this.validationRegex = validationRegex;
    }

    public boolean isIfVisibleMandatory() {
        return ifVisibleMandatory;
    }

    public void setIfVisibleMandatory(boolean ifVisibleMandatory) {
        this.ifVisibleMandatory = ifVisibleMandatory;
    }

    public String getIfMeOneOfTheseMandatory() {
        return ifMeOneOfTheseMandatory;
    }

    public void setIfMeOneOfTheseMandatory(String ifMeOneOfTheseMandatory) {
        this.ifMeOneOfTheseMandatory = ifMeOneOfTheseMandatory;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }

    public DefaultFAId getDefaultFAId() {
        return defaultFAId;
    }

    public void setDefaultFAId(DefaultFAId defaultFAId) {
        this.defaultFAId = defaultFAId;
    }

    public ValidationObject getValidationObject() {
        return validationObject;
    }

    public void setValidationObject(ValidationObject validationObject) {
        this.validationObject = validationObject;
    }

    public String getNewFormCondition() {
        return newFormCondition;
    }

    public void setNewFormCondition(String newFormCondition) {
        this.newFormCondition = newFormCondition;
    }

    public String getDependent_form_question_id() {
        return dependent_form_question_id;
    }

    public void setDependent_form_question_id(String dependent_form_question_id) {
        this.dependent_form_question_id = dependent_form_question_id;
    }

    public RealmList<FormObjectAnswerChildren> getAnswerChildren() {
        return answerChildren;
    }

    public void setAnswerChildren(RealmList<FormObjectAnswerChildren> formObjectAnswerChildren) {
        this.answerChildren = formObjectAnswerChildren;
    }

    public RealmList<FormObjectQuestionChildren> getQuestionChildren() {
        return questionChildren;
    }

    public void setQuestionChildren(RealmList<FormObjectQuestionChildren> formObjectQuestionChildren) {
        this.questionChildren = formObjectQuestionChildren;
    }

    public RealmList<FormObjectQuestionValuesDefaultFAId> getFormObjectQuestionValuesDefaultFAId() {
        return formObjectQuestionValuesDefaultFAId;
    }

    public void setFormObjectQuestionValuesDefaultFAId(RealmList<FormObjectQuestionValuesDefaultFAId> formObjectQuestionValuesDefaultFAId) {
        this.formObjectQuestionValuesDefaultFAId = formObjectQuestionValuesDefaultFAId;
    }

    public Integer getEditable() {
        return editable;
    }

    public void setEditable(Integer editable) {
        this.editable = editable;
    }
}
