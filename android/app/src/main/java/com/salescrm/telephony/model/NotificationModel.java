package com.salescrm.telephony.model;

public class NotificationModel {
    private String name;
    private int id;
    private Payload payload;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public class Payload {
        public Payload(int pendingLocationId) {
            this.pendingLocationId = pendingLocationId;
        }

        private int pendingLocationId;

        public int getPendingLocationId() {
            return pendingLocationId;
        }

        public void setPendingLocationId(int pendingLocationId) {
            this.pendingLocationId = pendingLocationId;
        }
    }
}
