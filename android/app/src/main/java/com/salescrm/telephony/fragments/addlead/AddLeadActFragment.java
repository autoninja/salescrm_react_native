package com.salescrm.telephony.fragments.addlead;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddLeadActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.ActivitiesDB;
import com.salescrm.telephony.db.AllCarsDB;
import com.salescrm.telephony.db.LocationsDB;
import com.salescrm.telephony.db.crm_user.ActivityUserDB;
import com.salescrm.telephony.db.crm_user.TeamData;
import com.salescrm.telephony.db.crm_user.UsersDB;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.AddLeadCommunication;
import com.salescrm.telephony.model.AddLeadActivityInputData;
import com.salescrm.telephony.model.AddLeadCarInputData;
import com.salescrm.telephony.model.AddLeadInputPrimaryElements;
import com.salescrm.telephony.model.CreateLeadInputData;
import com.salescrm.telephony.model.ViewPagerSwiped;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.CreForActivityResponse;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

import static android.text.TextUtils.isEmpty;
import static com.salescrm.telephony.utils.Util.showDatePicker;
import static com.salescrm.telephony.utils.Util.showTimePicker;

/**
 * Created by bharath on 30/5/16.
 */
public class AddLeadActFragment extends Fragment implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener, View.OnFocusChangeListener, View.OnTouchListener {

    AutoCompleteTextView activityName;
    Realm realm;
    private AddLeadCommunication addLeadCommunication;
    private String TAG = "AddLeadActFrag";
    private TextView tvScheduledAt, tvDynamic;
    private String locationId = "-1";
    private String enqSourceId;
    private ProgressDialog progressDialog;
    private Preferences pref = null;
    private int apiCallCount = 0;
    private List<CreForActivityResponse.Result> creForActivity;
    private AutoCompleteTextView autoTvAssignTo;
    private Button btCreateLead;
    private EditText etRemarks, etDynamic;
    private LinearLayout llDynamic, llTestDriveDynamic;
    private AddLeadCarInputData carInputData;
    private Spinner carSpinner, carTestVisitSpinner;
    private TextView etTestDriveDynamic;
    private TextView tvScheduledAtTestDrive;
    private RealmResults<ActivityUserDB> activityUserDBList;
    private TextView currentTvDate;

    private RadioGroup radioGroupVisitType;
    private LinearLayout llShowroom, llShowRoomVisit;
    private Spinner spinnerShowroom;
    private Spinner spinnerShowroomVisit;
    private RadioButton radioHome, radioOffice, radioOther,radioShowroom;
    private EditText etHome, etOffice, etOther;
    private TextView radioVisitTypeHeader;
    private String prevAssignedId = "";
    private String assigneeId;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lead_act, container, false);


        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(getContext());


        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);

        llShowRoomVisit = (LinearLayout) view.findViewById(R.id.ll_add_lead_showroom_visit);
        radioGroupVisitType = (RadioGroup) view.findViewById(R.id.radio_group_add_lead_visit_type);
        radioVisitTypeHeader = (TextView) view.findViewById(R.id.tv_lead_visit_adr_header);
        radioHome = (RadioButton) view.findViewById(R.id.radio_add_lead_visit_home);
        etHome = (EditText) view.findViewById(R.id.et_add_lead_visit_home);
        radioOffice = (RadioButton) view.findViewById(R.id.radio_add_lead_visit_office);
        etOffice = (EditText) view.findViewById(R.id.et_add_lead_visit_office);
        llShowroom = (LinearLayout) view.findViewById(R.id.ll_add_lead_showroom);
        radioShowroom = (RadioButton) view.findViewById(R.id.radio_add_lead_visit_showroom);
        spinnerShowroom = (Spinner) view.findViewById(R.id.spinner_add_lead_showroom);
        spinnerShowroomVisit = (Spinner) view.findViewById(R.id.spinner_add_lead_showroom_visit);
        radioOther = (RadioButton) view.findViewById(R.id.radio_add_lead_visit_other);
        etOther = (EditText) view.findViewById(R.id.et_add_lead_visit_other);
        getShowRoom(spinnerShowroom);
        getShowRoom(spinnerShowroomVisit);
        hideVisitTypeViews();
        radioGroupVisitType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                hideVisitTypeViews();
                switch (checkedId) {
                    case R.id.radio_add_lead_visit_home:
                        etHome.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radio_add_lead_visit_office:
                        etOffice.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radio_add_lead_visit_showroom:
                        llShowroom.setVisibility(View.VISIBLE);
                        spinnerShowroom.setVisibility(View.VISIBLE);
                        getShowRoom(spinnerShowroom);

                        break;
                    case R.id.radio_add_lead_visit_other:
                        etOther.setVisibility(View.VISIBLE);

                        break;

                }
            }
        });


        llDynamic = (LinearLayout) view.findViewById(R.id.ll_add_lead_act_dynamic);
        llTestDriveDynamic = (LinearLayout) view.findViewById(R.id.ll_add_lead_act_test_drive_dynamic);
        tvDynamic = (TextView) view.findViewById(R.id.tv_add_lead_act_dynamic);
        etDynamic = (EditText) view.findViewById(R.id.et_add_lead_act_dynamic);
        carSpinner = (Spinner) view.findViewById(R.id.spinner_add_lead_act_dynamic);
        carTestVisitSpinner = (Spinner) view.findViewById(R.id.spinner_add_lead_act_test_drive_dynamic);
        etTestDriveDynamic = (TextView) view.findViewById(R.id.et_add_lead_act_test_addr_dynamic);
        tvScheduledAtTestDrive = (TextView) view.findViewById(R.id.tv_lead_scheduled_test_drive_date_val);
        setSpinnerAdapter(carTestVisitSpinner, new String[]{"HOME", "Office", "Other"});


        activityName = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_add_lead_act_name);
        autoTvAssignTo = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_add_lead_assign_to);
        tvScheduledAt = (TextView) view.findViewById(R.id.tv_lead_scheduled_date_val);
        btCreateLead = (Button) view.findViewById(R.id.bt_add_lead);
        etRemarks = (EditText) view.findViewById(R.id.et_tv_add_lead_remarks);
        btCreateLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (isFormValid()) {
                    AddLeadActivity.activityInputData = getActivityInputData();
                    AddLeadActivity.carInputData = carInputData;
                    addLeadCommunication.onSelectActivityEdited(getActivityInputData(), carInputData, 4);
                }
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, getActivityNames());
        activityName.setAdapter(adapter);
        activityName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && TextUtils.isEmpty(activityName.getText()) && activityName.getAdapter() != null){
                    activityName.showDropDown();
                    Util.HideKeyboard(getContext());

                }
            }
        });
        autoTvAssignTo.setOnFocusChangeListener(this);
        autoTvAssignTo.setOnTouchListener(this);
        activityName.setOnTouchListener(this);
        activityName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(getContext());
                tvScheduledAtTestDrive.setText("");
                tvScheduledAt.setText("");
                tvScheduledAtTestDrive.setTag(R.id.autoninja_date, null);
                tvScheduledAt.setTag(R.id.autoninja_date, null);
                etDynamic.setText("");
                autoTvAssignTo.setText("");
                if (isDataValid()) {
                    handleViewChanges();
                    //  System.out.println("Acticity: size::"+realm.where(ActivityUserDB.class).findAll().size());
                    populateAdaptersOffline();
                } else {
                    activityName.setText("");
                }

                if(getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.FIRST_CALL_ID+"")
                        && DbUtils.isClientTwoWheeler() && DbUtils.getFirstCallAdvanceDate()>0) {
                    currentTvDate = tvScheduledAt;
                    Calendar today = Calendar.getInstance();
                    today.set(Calendar.HOUR_OF_DAY,10);
                    today.set(Calendar.MINUTE,0);
                    today.set(Calendar.SECOND,0);
                    today.set(Calendar.MILLISECOND,0);
                    today.add(Calendar.DATE,DbUtils.getFirstCallAdvanceDate());
                    tvScheduledAt.setText(String.format(Locale.getDefault(), "%d %s %d", today.get(Calendar.DAY_OF_MONTH),
                            today.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), today.get(Calendar.YEAR)));
                    currentTvDate.append(String.format(Locale.getDefault(), ", %s", Util.getAmPmTime(today)));
                    tvScheduledAt.setTag(R.id.autoninja_date, today);
                    currentTvDate.setTag(R.id.autoninja_date, today);
                    tvScheduledAt.setEnabled(false);
                    tvScheduledAt.setTextColor(Color.parseColor("#979797"));
                }
                else {
                    tvScheduledAt.setEnabled(true);
                    tvScheduledAt.setTextColor(Color.BLACK);
                }


               /* if (new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                    Util.HideKeyboard(getActivity());
                    progressDialog.show();
                    handleViewChanges();

                    //getCreForActivity();
                } else {
                    System.out.println("No Internet connection");
                }*/
            }
        });

        tvScheduledAt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calenderMaxVal = Calendar.getInstance();
                calenderMaxVal.add(Calendar.DAY_OF_MONTH,10);
                showDatePicker(AddLeadActFragment.this, getActivity(), R.id.tv_lead_scheduled_date_val, Calendar.getInstance(), calenderMaxVal);
            }
        });

        tvScheduledAtTestDrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calenderMaxVal = Calendar.getInstance();
                calenderMaxVal.add(Calendar.DAY_OF_MONTH,10);
                showDatePicker(AddLeadActFragment.this, getActivity(), R.id.tv_lead_scheduled_test_drive_date_val, Calendar.getInstance(), calenderMaxVal);
            }
        });

        return view;


    }

    private void getShowRoom(Spinner spinner) {

        String[] arr = new String[realm.where(LocationsDB.class).findAll().size() + 1];
        arr[0] = "Select showroom";
        int position = 0;
        for (int i = 0; i < realm.where(LocationsDB.class).findAll().size(); i++) {
            arr[i + 1] = realm.where(LocationsDB.class).findAll().get(i).getName();
            if (AddLeadActivity.dseInputData != null && AddLeadActivity.dseInputData.getDseBranchId() != null) {
                if ((realm.where(LocationsDB.class).findAll().get(i).getId() + "").equalsIgnoreCase(AddLeadActivity.dseInputData.getDseBranchId())) {
                    position = i + 1;
                }
            }

        }
        spinner.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, arr));
        spinner.setSelection(position);

    }

    private void hideVisitTypeViews() {
        etHome.setText("");
        etOffice.setText("");
        if (AddLeadActivity.customerInputData != null) {
            if (AddLeadActivity.customerInputData.getAddressType() == WSConstants.ADDRESS_TYPE_OFFICE) {
                etOffice.setText(AddLeadActivity.customerInputData.getAddress());
            } else if (AddLeadActivity.customerInputData.getAddressType() == WSConstants.ADDRESS_TYPE_HOME) {
                etHome.setText(AddLeadActivity.customerInputData.getAddress());
            }
        }
        etOther.setText("");
        etOffice.setVisibility(View.GONE);
        etHome.setVisibility(View.GONE);
        etOther.setVisibility(View.GONE);
        llShowroom.setVisibility(View.GONE);
        spinnerShowroom.setVisibility(View.GONE);
    }

    private boolean isDataValid() {
        if (getActivityId().equalsIgnoreCase("")) {
            return false;
        }
        if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID + "")
                || getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID + "")) {
            if (AddLeadActivity.carInputData == null || AddLeadActivity.carInputData.getNewCarInputData().size() == 0) {
                AddLeadActivity.showAlert("Car not added to do task");
                return false;
            }
        }
        if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID + "")) {
            if (AddLeadActivity.carInputData == null || AddLeadActivity.carInputData.getExCarInputData().size() == 0) {
                AddLeadActivity.showAlert("Exchange car not added to do task");
                return false;
            }
        }
        if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.LOST_DROP_ID + "")) {
                AddLeadActivity.showAlert("Access Denied");
                return false;
        }

        return true;
    }

    private void populateAdaptersOffline() {
        if (!TextUtils.isEmpty(getActivityId())) {
            String locationData = AddLeadActivity.dseInputData.getDseBranchId();
            if (locationData == null || TextUtils.isEmpty(AddLeadActivity.dseInputData.getDseBranchId()) || AddLeadActivity.dseInputData.getDseBranchId().equalsIgnoreCase("0")) {
                locationData = "-1";
            }

            System.out.println("In add lead act:getDseBranchId" + AddLeadActivity.dseInputData.getDseBranchId());
            System.out.println("In add lead act:activity_id" + getActivityId());
            System.out.println("In add lead act:EnqSourceId" + AddLeadActivity.leadInputData.getEnqSourceId());
            activityUserDBList = realm.where(ActivityUserDB.class)
                    .equalTo("location_id", locationData)
                    .equalTo("activity_id", getActivityId())
                    .beginGroup()
                    .equalTo("lead_source_id", AddLeadActivity.leadInputData.getEnqSourceId())
                    .or()
                    .equalTo("lead_source_id", "-1")
                    .endGroup()
                    .findAll();

            String preDseAssigned = "";
            String[] arr  = new String[activityUserDBList.size()];
            for (int i = 0; i < activityUserDBList.size(); i++) {
                arr[i] = activityUserDBList.get(i).getName();
                if(AddLeadActivity.dseInputData!=null&&AddLeadActivity.dseInputData.getUser_detailsList().size()>0){
                    if(AddLeadActivity.dseInputData.getUser_detailsList().get(0).getUser_id().equalsIgnoreCase( activityUserDBList.get(i).getId())){
                        preDseAssigned = activityUserDBList.get(i).getName();
                    }
                }
            }
            if(DbUtils.isClientTwoWheeler()) {
                preDseAssigned = getAssigneeTL();
            }

            autoTvAssignTo.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, arr));
            autoTvAssignTo.setText(preDseAssigned);
            if(preDseAssigned==null||preDseAssigned.equalsIgnoreCase("")){
                autoTvAssignTo.requestFocus();
                autoTvAssignTo.setEnabled(true);
            }
            else {
                autoTvAssignTo.setEnabled(false);
            }


        }
    }

    private void handleViewChanges() {

        if (getActivityId().equalsIgnoreCase("")||getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.FOLLOWUP_CALL_ID + "")
                || getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.FIRST_CALL_ID + "")) {
            radioGroupVisitType.clearCheck();
            llDynamic.setVisibility(View.GONE);
            llShowRoomVisit.setVisibility(View.GONE);
        } else {
            radioShowroom.setVisibility(View.GONE);
            if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID + "")) {
                radioShowroom.setVisibility(View.VISIBLE);
            }
            radioGroupVisitType.setVisibility(View.VISIBLE);
            radioVisitTypeHeader.setText("Visit Type");
            llShowRoomVisit.setVisibility(View.GONE);
            radioGroupVisitType.clearCheck();
            etDynamic.setText("");
            autoTvAssignTo.setText("");
            llDynamic.setVisibility(View.VISIBLE);
            etDynamic.setVisibility(View.VISIBLE);
            tvDynamic.setVisibility(View.VISIBLE);
            carSpinner.setVisibility(View.VISIBLE);
            llTestDriveDynamic.setVisibility(View.GONE);

            if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID + "") ||
                    getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID + "")) {
                setSpinnerAdapter(carSpinner, getNewCarData());
            } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID + "")) {
                setSpinnerAdapter(carSpinner, getExCarData());
            }

            if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.HOME_VISIT_ID + "")) {
                radioVisitTypeHeader.setText("Visit address");
                llTestDriveDynamic.setVisibility(View.VISIBLE);
                carSpinner.setVisibility(View.GONE);
                etDynamic.setVisibility(View.GONE);
                tvDynamic.setVisibility(View.GONE);

            } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.SHOWROOM_VISIT_ID + "")) {
                llShowRoomVisit.setVisibility(View.VISIBLE);
                getShowRoom(spinnerShowroomVisit);
                carSpinner.setVisibility(View.GONE);
                etDynamic.setVisibility(View.GONE);
                tvDynamic.setVisibility(View.GONE);
            } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID + "")) {
                carSpinner.setVisibility(View.VISIBLE);
                llTestDriveDynamic.setVisibility(View.VISIBLE);
                etDynamic.setVisibility(View.GONE);
                tvDynamic.setVisibility(View.GONE);
            } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID + "")) {
                llShowRoomVisit.setVisibility(View.VISIBLE);
                getShowRoom(spinnerShowroomVisit);
                carSpinner.setVisibility(View.VISIBLE);
                llTestDriveDynamic.setVisibility(View.VISIBLE);
                etDynamic.setVisibility(View.GONE);
                tvDynamic.setVisibility(View.GONE);
                radioGroupVisitType.setVisibility(View.GONE);
            } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID + "")) {
                llTestDriveDynamic.setVisibility(View.VISIBLE);
                carSpinner.setVisibility(View.VISIBLE);
                etDynamic.setVisibility(View.GONE);
                tvDynamic.setVisibility(View.GONE);
            }
        }
    }

    private void setDefaultPosition(LinearLayout llShowRoomVisit) {
    }

   /* private void getCreForActivity() {
        System.out.println(TAG);
        System.out.println("EnqSourceId:" + enqSourceId);
        System.out.println("locationId:" + locationId);
        System.out.println("locationId:" + locationId);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetCreForActivity(getActivityId(), enqSourceId, locationId, "", this);

    }*/

    private String getActivityId() {
        if (realm.where(ActivitiesDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, activityName.getText().toString()).findAll().size() > 0) {
            return String.valueOf(realm.where(ActivitiesDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, activityName.getText().toString()).findFirst().getId());
        } else {
            return "";
        }
    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        if ((Integer.parseInt(view.getTag())) == R.id.tv_lead_scheduled_date_val) {
            tvScheduledAt.setText(String.format(Locale.getDefault(), "%d %s %d", dayOfMonth, calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), year));
            tvScheduledAt.setTag(R.id.autoninja_date, calendar);
            currentTvDate = tvScheduledAt;
            showTimePicker(AddLeadActFragment.this, getActivity(), R.id.tv_lead_scheduled_date_val);
        } else {
            currentTvDate = tvScheduledAtTestDrive;
            tvScheduledAtTestDrive.setText(String.format(Locale.getDefault(), "%d %s %d", dayOfMonth, calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), year));
            tvScheduledAtTestDrive.setTag(R.id.autoninja_date, calendar);
            showTimePicker(AddLeadActFragment.this, getActivity(), tvScheduledAtTestDrive.getId());

        }


    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        if (currentTvDate != null) {
            Calendar calDate = (Calendar) currentTvDate.getTag(R.id.autoninja_date);
            calDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calDate.set(Calendar.MINUTE, minute);
            calDate.set(Calendar.SECOND, second);
            currentTvDate.setTag(R.id.autoninja_date, calDate);
            currentTvDate.append(String.format(Locale.getDefault(), ", %s", Util.getAmPmTime(calDate)));
        }


    }


    private String[] getActivityNames() {
        RealmResults<ActivitiesDB> realmData = realm.where(ActivitiesDB.class).findAll();
        List<String> activitiesVal = new ArrayList<>();

        for(ActivitiesDB activitiesDB:realmData){
           if(activitiesDB.getId()!=WSConstants.TaskActivityName.EVALUATION_REQUEST_ID &&
                   (activitiesDB.getId()!=WSConstants.TaskActivityName.LOST_DROP_ID)){
                activitiesVal.add(activitiesDB.getName());
            }
        }
        String[] arr = new String[activitiesVal.size()];
        for (int i = 0; i < activitiesVal.size(); i++) {

            arr[i] = activitiesVal.get(i);

        }
        return arr;
    }

    public void setSpinnerAdapter(Spinner spinner, String data[]) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, data);
        spinner.setAdapter(adapter);
    }


    @Override
    public void onAttach(Context context) {
        try {
            addLeadCommunication = (AddLeadCommunication) context;
        } catch (Exception e) {
            System.out.println("ClassCastException");
        }
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);

    }

    @Subscribe
    public void onViewPagerMoved(ViewPagerSwiped viewPagerSwiped) {
        System.out.println("Viewpager swiped::::::"+viewPagerSwiped.getOldPosition() +":::"+viewPagerSwiped.getNewPosition());


        if(viewPagerSwiped.getOldPosition() == 3 && viewPagerSwiped.getNewPosition() == 4 && isLeadTagAdded()) {
            Handler handler =new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    activityName.requestFocus();
                    if(activityName.hasFocus() && TextUtils.isEmpty(activityName.getText()) && activityName.getAdapter() != null){
                        activityName.showDropDown();

                    }
                }
            },50);

            if(!prevAssignedId.equalsIgnoreCase(getAssignedDseId())){
                activityName.setText("");
                tvScheduledAtTestDrive.setText("");
                tvScheduledAt.setText("");
                tvScheduledAtTestDrive.setTag(R.id.autoninja_date, null);
                tvScheduledAt.setTag(R.id.autoninja_date, null);
                etDynamic.setText("");
                autoTvAssignTo.setText("");
                handleViewChanges();
            }

            if (AddLeadActivity.dseInputData!=null&&AddLeadActivity.dseInputData.getUser_detailsList().size() > 0) {
                prevAssignedId = AddLeadActivity.dseInputData.getUser_detailsList().get(0).getUser_id();
            } else {
                prevAssignedId = "";
            }
        }
        if (viewPagerSwiped.getOldPosition() == 4 && viewPagerSwiped.getNewPosition() == 5) {
            System.out.println(TAG + "ViewPagerTryToMoving");
            if (isFormValid()) {
                AddLeadActivity.activityInputData = getActivityInputData();
                AddLeadActivity.carInputData = carInputData;
                addLeadCommunication.onSelectActivityEdited(getActivityInputData(), carInputData, viewPagerSwiped.getOldPosition());
            }
        }
    }

    private boolean isLeadTagAdded() {
        return AddLeadActivity.dseInputData != null && Util.isNotNull(AddLeadActivity.dseInputData.getTagId());

    }

    private String getAssignedDseId() {
        if(AddLeadActivity.dseInputData!=null && AddLeadActivity.dseInputData.getUser_detailsList().size()>0){
            return AddLeadActivity.dseInputData.getUser_detailsList().get(0).getUser_id();
        }
        else {
            return "";
        }
    }

    private boolean isFormValid() {
        if (!activityName.getText().toString().isEmpty()) {
            if (getActivityId().isEmpty()) {
                Toast.makeText(getContext(), "Invalid activity name", Toast.LENGTH_SHORT).show();
                return false;
            } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID + "")
                    || getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID + "") ||
                    getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID + "")) {

                if (getActivityOwnerOffline() == null || getActivityOwnerOffline().getRole_id() == null) {
                    Toast.makeText(getContext(), "Invalid assign", Toast.LENGTH_SHORT).show();
                    return false;
                } else if (carSpinner.getSelectedItemPosition() == 0) {
                    Toast.makeText(getContext(), "Car not selected", Toast.LENGTH_SHORT).show();
                    return false;
                } else if (tvScheduledAt.getTag(R.id.autoninja_date) == null) {
                    Toast.makeText(getContext(), "Select scheduled date", Toast.LENGTH_SHORT).show();
                    return false;
                }
                if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID + "")) {
                    if (!checkRadioVisitDataValid((radioGroupVisitType.getCheckedRadioButtonId()), false)) {
                        Toast.makeText(getContext(), "Select visit address", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID + "")) {
                    if (spinnerShowroomVisit.getSelectedItemPosition() == 0) {
                        Toast.makeText(getContext(), "Select showroom", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID + "")) {
                    if (!checkRadioVisitDataValid((radioGroupVisitType.getCheckedRadioButtonId()), true)) {
                        Toast.makeText(getContext(), "Select visit address", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
            } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.HOME_VISIT_ID + "")) {
                if (getActivityOwnerOffline() == null || getActivityOwnerOffline().getRole_id() == null) {
                    Toast.makeText(getContext(), "Invalid assign", Toast.LENGTH_SHORT).show();
                    return false;
                } else if (!checkRadioVisitDataValid((radioGroupVisitType.getCheckedRadioButtonId()), false)) {
                    Toast.makeText(getContext(), "Select visit address", Toast.LENGTH_SHORT).show();
                    return false;
                } else if (tvScheduledAt.getTag(R.id.autoninja_date) == null) {
                    Toast.makeText(getContext(), "Select scheduled date", Toast.LENGTH_SHORT).show();
                    return false;
                }
            } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.SHOWROOM_VISIT_ID + "")) {
                if (getActivityOwnerOffline() == null || getActivityOwnerOffline().getRole_id() == null) {
                    Toast.makeText(getContext(), "Invalid assign", Toast.LENGTH_SHORT).show();
                    return false;
                } else if (spinnerShowroomVisit.getSelectedItemPosition() == 0) {
                    Toast.makeText(getContext(), "Select showroom", Toast.LENGTH_SHORT).show();
                    return false;
                } else if (tvScheduledAt.getTag(R.id.autoninja_date) == null) {
                    Toast.makeText(getContext(), "Select scheduled date", Toast.LENGTH_SHORT).show();
                    return false;
                }
            } else {
                if (getActivityOwnerOffline() == null || getActivityOwnerOffline().getRole_id() == null) {
                    Toast.makeText(getContext(), "Invalid assign", Toast.LENGTH_SHORT).show();
                    return false;
                } else if (tvScheduledAt.getTag(R.id.autoninja_date) == null) {
                    Toast.makeText(getContext(), "Select scheduled date", Toast.LENGTH_SHORT).show();
                    return false;
                }
                else if (TextUtils.isEmpty(etRemarks.getText())) {
                    AddLeadActivity.showAlert("Remarks should not be empty");
                    etRemarks.requestFocus();
                    return false;
                }
            }

        }
        else {
            Util.showToast(getContext(), "Please schedule a task", Toast.LENGTH_LONG);
            activityName.requestFocus();
            return false;
        }
        return true;
    }

    private boolean checkRadioVisitDataValid(int id, boolean fromEvaluation) {
        switch (id) {
            case R.id.radio_add_lead_visit_home:
                if (etHome.getText().toString().isEmpty()) {
                    return false;
                }
                break;
            case R.id.radio_add_lead_visit_office:
                if (etOffice.getText().toString().isEmpty()) {
                    return false;
                }
                break;
            case R.id.radio_add_lead_visit_other:
                if (etOther.getText().toString().isEmpty()) {
                    return false;
                }
                break;
            case R.id.radio_add_lead_visit_showroom:
                if (fromEvaluation && spinnerShowroom.getSelectedItemPosition() == 0) {
                    return false;
                }
                break;
            default:
                return false;
        }
        return true;
    }

    private AddLeadActivityInputData getActivityInputData() {
        CreateLeadInputData.Activity_data activity_data = new CreateLeadInputData().new Activity_data();

        if (!TextUtils.isEmpty(getActivityId())) {

            activity_data.setAddress(etDynamic.getText().toString().toUpperCase());
            activity_data.setActivity(getActivityId());
            if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID + "")
                    || getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID + "")) {
                setCarActivityTarget(0);
            } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID + "")) {
                setCarActivityTarget(1);
            }

            if (llTestDriveDynamic.getVisibility() == View.VISIBLE) {
                if (radioGroupVisitType.getVisibility() == View.VISIBLE) {
                    if (llShowroom.getVisibility() == View.VISIBLE) {
                        activity_data.setLocation_id(getLocationId(spinnerShowroom));
                    }
                    switch (radioGroupVisitType.getCheckedRadioButtonId()) {
                        case R.id.radio_add_lead_visit_home:
                            activity_data.setVisit_type("1");
                            activity_data.setAddress(etHome.getText().toString().toUpperCase());
                            break;
                        case R.id.radio_add_lead_visit_office:
                            activity_data.setVisit_type("2");
                            activity_data.setAddress(etOffice.getText().toString().toUpperCase());
                            break;
                        case R.id.radio_add_lead_visit_other:
                            activity_data.setVisit_type(null);
                            activity_data.setAddress(etOther.getText().toString().toUpperCase());
                            break;
                    }
                }

            }
            activity_data.setVisit_time(getScheduledDateTime());
            if (llShowRoomVisit.getVisibility() == View.VISIBLE) {
                activity_data.setLocation_id(getLocationId(spinnerShowroomVisit));
            }
            activity_data.setActivityName(activityName.getText().toString());
            activity_data.setAssignName(autoTvAssignTo.getText().toString());
            if (carSpinner.getSelectedItem() != null) {
                activity_data.setCarName(carSpinner.getSelectedItem().toString());
            }
            activity_data.setRemarks(etRemarks.getText().toString().toUpperCase());
            if (getActivityOwnerOffline().getRole_id() != null) {
                activity_data.setActivity_owner(getActivityOwnerOffline());
            }
            if (!TextUtils.isEmpty(getScheduledDateTime())) {
                activity_data.setScheduledDateTime(getScheduledDateTime());
            }
        }


        System.out.println("getScheduled Date:" + getScheduledDateTime());
        return new AddLeadActivityInputData(activity_data, etRemarks.getText().toString());
    }

    private String getLocationId(Spinner spinner) {
        if (realm.where(LocationsDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, spinner.getSelectedItem().toString()).findAll().size() > 0) {
            return String.valueOf(realm.where(LocationsDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, spinner.getSelectedItem().toString()).findFirst().getId());
        } else {
            return null;
        }
    }


    private String getScheduledDateTime() {
        if (tvScheduledAt.getTag(R.id.autoninja_date) != null) {
            Calendar cal = (Calendar) tvScheduledAt.getTag(R.id.autoninja_date);
            System.out.println(TAG + "SQLTime:" + Util.getSQLDateTime(cal.getTime()));
            return Util.getSQLDateTime((cal.getTime()));
        }
        return "";
    }


    @Subscribe
    public void onAddLeadPrimaryElements(AddLeadInputPrimaryElements data) {
        if (data.getEnqSourceId() != null) {
            this.enqSourceId = data.getEnqSourceId();
        }

        if (data.getLocationId() != null) {
            this.locationId = data.getLocationId();
        }
        this.carInputData = data.getCarInputData();


    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus && v instanceof AutoCompleteTextView && isEmpty(((AutoCompleteTextView) v).getText().toString())) {
            ((AutoCompleteTextView) v).showDropDown();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (TextUtils.isEmpty(((AutoCompleteTextView) v).getText().toString())) {
            ((AutoCompleteTextView) v).showDropDown();
        }
        return false;
    }


    private CreateLeadInputData.Activity_data.Activity_owner getActivityOwner() {
        CreateLeadInputData.Activity_data.Activity_owner activity_owner = new CreateLeadInputData().new Activity_data().new Activity_owner();
        if (activityUserDBList != null) {
            for (int i = 0; i < activityUserDBList.size(); i++) {
                if (autoTvAssignTo.getText().toString().equalsIgnoreCase(activityUserDBList.get(i).getName())) {
                    activity_owner.setRole_id(activityUserDBList.get(i).getRole_id());
                    activity_owner.setUser_id(activityUserDBList.get(i).getId());
                    return activity_owner;
                }
            }
        }
        return activity_owner;
    }

    public CreateLeadInputData.Activity_data.Activity_owner getActivityOwnerOffline() {
        CreateLeadInputData.Activity_data.Activity_owner activity_owner = new CreateLeadInputData().new Activity_data().new Activity_owner();

        if (!TextUtils.isEmpty(getActivityId())) {
            String locationData = AddLeadActivity.dseInputData.getDseBranchId();
            if (locationData == null || TextUtils.isEmpty(AddLeadActivity.dseInputData.getDseBranchId()) || AddLeadActivity.dseInputData.getDseBranchId().equalsIgnoreCase("0")) {
                locationData = "-1";
            }
            ActivityUserDB activityUserDB = realm.where(ActivityUserDB.class)
                    .equalTo("location_id", locationData)
                    .equalTo("activity_id", getActivityId())
                    .beginGroup()
                    .equalTo("lead_source_id", AddLeadActivity.leadInputData.getEnqSourceId())
                    .or()
                    .equalTo("lead_source_id", "-1")
                    .endGroup()
                    .equalTo("id",DbUtils.isClientTwoWheeler()?assigneeId : getAssignedDseId())
                    .findFirst();
            if (activityUserDB != null) {
                activity_owner.setRole_id(activityUserDB.getRole_id());
                activity_owner.setUser_id(activityUserDB.getId());
                return activity_owner;
            }

        }
        return activity_owner;
    }

    public String[] getNewCarData() {
        String data[] = new String[carInputData.getNewCarInputData().size() + 1];
        data[0] = "Select Test Drive Car";
        for (int i = 0; i < carInputData.getNewCarInputData().size(); i++) {
            data[i + 1] = getCarModelName(Integer.parseInt(carInputData.getNewCarInputData().get(i).getModel_id()));
        }
        return data;
    }

    private String getCarModelName(int id) {
        if (realm.where(AllCarsDB.class).equalTo("id", id).findAll().size() > 0) {
            return String.valueOf(realm.where(AllCarsDB.class).equalTo("id", id).findFirst().getName());
        } else {
            return "";
        }
    }

    private String getCarModelId(String name) {
        if (realm.where(AllCarsDB.class).equalTo("name", name).findAll().size() > 0) {
            return String.valueOf(realm.where(AllCarsDB.class).equalTo("name", name).findFirst().getId());
        } else {
            return "";
        }
    }

    public String[] getExCarData() {
        String data[] = new String[carInputData.getExCarInputData().size() + 1];
        data[0] = "Select Old Car";
        for (int i = 0; i < carInputData.getExCarInputData().size(); i++) {
            data[i + 1] = getCarModelName(Integer.parseInt(carInputData.getExCarInputData().get(i).getModel_id()));
        }
        return data;
    }


    public String getScheduledTestDriveTime() {
        if (tvScheduledAtTestDrive.getTag(R.id.autoninja_date) != null) {
            Calendar cal = (Calendar) tvScheduledAtTestDrive.getTag(R.id.autoninja_date);
            System.out.println(TAG + "SQLTime:" + Util.getSQLDateTime(cal.getTime()));
            return Util.getSQLDateTime((cal.getTime()));
        }
        return "";
    }


    private void setCarActivityTarget(int from) {
        String carId = getCarModelId(carSpinner.getSelectedItem().toString());
        if (from == 0) {
            //TestDrive //Proposal
            for (int i = 0; i < carInputData.getNewCarInputData().size(); i++) {
                if (carInputData.getNewCarInputData().get(i).getModel_id().equalsIgnoreCase(carId)) {
                    carInputData.getNewCarInputData().get(i).setIs_activity_target(true);
                    break;
                }
            }
        } else if (from == 1) {
            //Evaluation
            for (int i = 0; i < carInputData.getExCarInputData().size(); i++) {
                if (carInputData.getExCarInputData().get(i).getModel_id().equalsIgnoreCase(carId)) {
                    carInputData.getExCarInputData().get(i).setIs_activity_target(true);
                    break;
                }
            }

        }
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    private String getAssigneeTL(){
        if(Util.isNotNull(getAssignedDseId())) {
            TeamData realmData = realm.where(TeamData.class).equalTo("user_id", getAssignedDseId()).findFirst();
            if(realmData!=null) {
                UsersDB realmDataUser = realm.where(UsersDB.class).equalTo("id", realmData.getTeam_leader_id()).findFirst();
                if(realmDataUser!=null) {
                    System.out.println("User ID " + realmDataUser.getId());
                    assigneeId = realmDataUser.getId();
                    return realmDataUser.getName();
                }

            }
        }
        return "";

    }
}
