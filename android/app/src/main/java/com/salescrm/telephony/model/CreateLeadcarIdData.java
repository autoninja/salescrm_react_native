package com.salescrm.telephony.model;

/**
 * Created by akshata on 9/12/16.
 */

public class CreateLeadcarIdData {

    public LeadCarDetails lead_car_details;

    private String lead_id;

    public LeadCarDetails getLead_car_details() {
        return lead_car_details;
    }

    public void setLead_car_details(LeadCarDetails lead_car_details) {
        this.lead_car_details = lead_car_details;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [leadCarId = " + lead_car_details + ", lead_id = " + lead_id + "]";
    }

    public class LeadCarDetails {
        private String carVariant;

        private String carFuelType;

        private String carColorId;

        private String carModel;

        public String getCarVariant() {
            return carVariant;
        }

        public void setCarVariant(String carVariant) {
            this.carVariant = carVariant;
        }

        public String getCarFuelType() {
            return carFuelType;
        }

        public void setCarFuelType(String carFuelType) {
            this.carFuelType = carFuelType;
        }

        public String getCarColorId() {
            return carColorId;
        }

        public void setCarColorId(String carColorId) {
            this.carColorId = carColorId;
        }

        public String getCarModel() {
            return carModel;
        }

        public void setCarModel(String carModel) {
            this.carModel = carModel;
        }

        @Override
        public String toString() {
            return "ClassPojo [carVariant = " + carVariant + ", carFuelType = " + carFuelType + ", carColorId = " + carColorId + ", carModel = " + carModel + "]";
        }
    }
}