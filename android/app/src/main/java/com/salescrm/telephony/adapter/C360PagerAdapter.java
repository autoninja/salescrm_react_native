package com.salescrm.telephony.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.salescrm.telephony.fragments.ActionsFragment;

import com.salescrm.telephony.fragments.DetailsFragment;
import com.salescrm.telephony.fragments.NewCarsFragment;

/**
 * Created by akshata on 31/5/16.
 */
public class C360PagerAdapter extends FragmentPagerAdapter {
    int mNumOfTabs;

    public C360PagerAdapter(FragmentManager fm, int mNumOfTabs) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
    }
    @Override
    public Fragment getItem(int position) {

        System.out.println("TAB POSITION CALLED"+position);
        switch (position) {
            case 0:
               // SalesCRMApplication.sendEventToCleverTap("Mob Tab Action Fragment");
                ActionsFragment tab1 = new ActionsFragment();
                return tab1;
            case 1:
                NewCarsFragment tab2 = new NewCarsFragment();
                return tab2;
            case 2:
                DetailsFragment tab3=new DetailsFragment();
                return tab3;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {

        return mNumOfTabs;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
