package com.salescrm.telephony.adapter.gamification;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.salescrm.telephony.fragments.gamification.LeaderBoardDseBadgesFragment;
import com.salescrm.telephony.fragments.gamification.LeaderBoardDsePointsFragment;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.response.gamification.DSEPointsResponseModel;

/**
 * Created by bannhi on 27/2/18.
 */

public class DseProfileAdapter extends FragmentPagerAdapter {
    Context context;
    String userId;
    CallBack callBack;

    public DseProfileAdapter(Context context, FragmentManager fm, String userId, CallBack callBack) {
        super(fm);
        this.userId = userId;
        this.context = context;

        this.callBack = callBack;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return  LeaderBoardDsePointsFragment.newInstance(userId, callBack);
            case 1:
                 return LeaderBoardDseBadgesFragment.newInstance(userId);

        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Points";
            case 1:
                return "Badges";

        }
        return  "";
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return 2;
    }
}
