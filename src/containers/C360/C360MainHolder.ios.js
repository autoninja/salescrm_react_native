import React from 'react';
import C360MainScreen from './c360_main_screen';
import VehicleDBService from '../../storage/vehicle_db_service';
import UserInfoService from '../../storage/user_info_service';
import AppConfDBService from '../../storage/app_conf_db_service';
export default class C360MainHolder extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <C360MainScreen
                {...this.props}
                interestedVehicles={VehicleDBService.getInterestedVehicle()}
                allVehicles={VehicleDBService.getAllVehicle()}
                userRoles={UserInfoService.getRoles()}
                isClientTwoWheeler={(AppConfDBService.getVal('two_wheeler_conf') == 1 ? true : false)}
                il_flow={AppConfDBService.getVal('il_flow')}
                il_bank_flow={AppConfDBService.getVal('il_bank_flow')}
            />
        )
    }
}