package com.salescrm.telephony.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.EnquiryList.AssignedStageEnquiryListFragment;
import com.salescrm.telephony.fragments.EnquiryList.BookedStageEnquiryListFragment;
import com.salescrm.telephony.fragments.EnquiryList.ClosedStageEnquiryListFagment;
import com.salescrm.telephony.fragments.EnquiryList.EvaluationStageEnquiryListFragment;
import com.salescrm.telephony.fragments.EnquiryList.InvoicedStageEnquiryListFragment;
import com.salescrm.telephony.fragments.EnquiryList.TestDriveStageEnquiryListFragment;

/**
 * Created by Ravindra P on 04-12-2015.
 */
public class EnquiryListPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 6;

    public EnquiryListPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    // Returns total number of pages
    @Override
    public int getCount() {

        return DbUtils.isBike()? 3:6;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                //return AllEnquiryListFragment.newInstance(0, "All");
                return AssignedStageEnquiryListFragment.newInstance(0, "Assigned");
            case 1:
                return TestDriveStageEnquiryListFragment.newInstance(1, "TestDrive");
               // return NotContactedEnquiryListFragment.newInstance(1, "Not contacted");
            case 2:
                if(DbUtils.isBike()){
                    //return BookedStageEnquiryListFragment.newInstance(2, "Booked");
                    return ClosedStageEnquiryListFagment.newInstance(2, "Closed");
                }else {
                    return EvaluationStageEnquiryListFragment.newInstance(2, "Evaluation");
                    // return QualifiedEnquiryListFragment.newInstance(2, "Qualified");
                }
            case 3:
                /*if(DbUtils.isBike()){
                    return ClosedStageEnquiryListFagment.newInstance(3, "Closed");
                }else {
                    return BookedStageEnquiryListFragment.newInstance(3, "Booked");
                    // return AssignedEnquiryListFragment.newInstance(3, "Assigned");
                }*/
                return BookedStageEnquiryListFragment.newInstance(3, "Booked");
            case 4:
                return InvoicedStageEnquiryListFragment.newInstance(4, "Invoiced");
            case 5:
                return ClosedStageEnquiryListFagment.newInstance(5, "Closed");
            /*case 6:

            case 7:

                //return FinanceEnquiryListFragment.newInstance(7, "Finance");
            case 8:*/

                //return InvoicedEnquiryListFragment.newInstance(8, "Invoiced");
           /* case 9:
                return ClosedEnquiryListFragment.newInstance(9, "Closed");*/
            default:
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Unqualified";
            case 1:
                return "Fresh";
            case 2:
                return "Contacted";
            case 3:
                return "Negotiations";
            case 4:
                return "Booked";
            case 5:
                return "Closed";
            default:
                return "Closed";
        }
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}