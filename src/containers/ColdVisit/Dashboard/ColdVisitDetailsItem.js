import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";

export default class ColdVisitDetailsItem extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let data = this.props.data;
    return (
      <View
        style={{
          marginTop: 10,
          backgroundColor: "#eceff1",
          borderRadius: 6,
          padding: 10
        }}
      >
        <View style={{ flexDirection: "row", justifyContent: "center" }}>
          <Text
            style={{
              flex: 1,
              fontSize: 18,
              paddingRight: 10,
              paddingLeft: 4,
              color: "#45446d"
            }}
          >
            {data.customer_name}
          </Text>
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              width: 24,
              height: 24,
              borderRadius: 24 / 2,
              backgroundColor: "#2e5e86"
            }}
          >
            <TouchableOpacity
              hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
              onPress={() => this.props.call(data.mobile_number)}
            >
              <Image
                source={require("../../../images/ic_phone.png")}
                style={{ height: 16, width: 16 }}
              />
            </TouchableOpacity>
          </View>
        </View>
        <Text
          style={{
            fontSize: 17,
            paddingLeft: 4,
            marginTop: 6,
            color: "#494949"
          }}
        >
          {"Remarks: "}
          {data.remarks}
        </Text>
        <TouchableOpacity
          hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
          onPress={() => {
            this.props.openMap(
              data.customer_address,
              data.latitude,
              data.longitude
            );
          }}
        >
          <View
            style={{
              flexDirection: "row",
              marginTop: 6,
              alignItems: "center"
            }}
          >
            <Image
              source={require("../../../images/ic_location_black.png")}
              style={{ width: 20, height: 20 }}
            />
            <Text
              style={{
                paddingLeft: 4,
                fontSize: 16,
                color: "#2e5e86",
                textDecorationLine: "underline"
              }}
            >
              {data.customer_address + ""}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
