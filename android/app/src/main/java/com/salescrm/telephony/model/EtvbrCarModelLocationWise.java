package com.salescrm.telephony.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by prateek on 13/11/17.
 */

public class EtvbrCarModelLocationWise {

    @SerializedName("error")
    @Expose
    private Error error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Result {

        @SerializedName("total_abs")
        @Expose
        private TotalAbs totalAbs;
        @SerializedName("total_rel")
        @Expose
        private TotalRel totalRel;
        @SerializedName("locations")
        @Expose
        private List<Location> locations = null;

        public TotalAbs getTotalAbs() {
            return totalAbs;
        }

        public void setTotalAbs(TotalAbs totalAbs) {
            this.totalAbs = totalAbs;
        }

        public TotalRel getTotalRel() {
            return totalRel;
        }

        public void setTotalRel(TotalRel totalRel) {
            this.totalRel = totalRel;
        }

        public List<Location> getLocations() {
            return locations;
        }

        public void setLocations(List<Location> locations) {
            this.locations = locations;
        }
    }

    public class LocationAbs {

        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retails")
        @Expose
        private Integer retails;
        @SerializedName("lost_drop")
        @Expose
        private Integer lost_drop;
        @SerializedName("exchanged")
        @Expose
        private Integer exchanged;
        @SerializedName("in_house_financed")
        @Expose
        private Integer in_house_financed;
        @SerializedName("out_house_financed")
        @Expose
        private Integer out_house_financed;

        @SerializedName("pending_bookings")
        @Expose
        private Integer pending_bookings;

        @SerializedName("live_enquiries")
        @Expose
        private Integer live_enquiries;

        public Integer getIn_house_financed() {
            return in_house_financed;
        }

        public void setIn_house_financed(Integer in_house_financed) {
            this.in_house_financed = in_house_financed;
        }

        public Integer getOut_house_financed() {
            return out_house_financed;
        }

        public void setOut_house_financed(Integer out_house_financed) {
            this.out_house_financed = out_house_financed;
        }

        public Integer getExchanged() {
            return exchanged;
        }

        public void setExchanged(Integer exchanged) {
            this.exchanged = exchanged;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetails() {
            return retails;
        }

        public void setRetails(Integer retails) {
            this.retails = retails;
        }

        public Integer getLost_drop() {
            return lost_drop;
        }

        public void setLost_drop(Integer lost_drop) {
            this.lost_drop = lost_drop;
        }

        public Integer getPending_bookings() {
            return pending_bookings;
        }

        public void setPending_bookings(Integer pending_bookings) {
            this.pending_bookings = pending_bookings;
        }

        public Integer getLive_enquiries() {
            return live_enquiries;
        }

        public void setLive_enquiries(Integer live_enquiries) {
            this.live_enquiries = live_enquiries;
        }
    }

    public class LocationRel {

        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retails")
        @Expose
        private Integer retails;
        @SerializedName("lost_drop")
        @Expose
        private Integer lost_drop;
        @SerializedName("exchanged")
        @Expose
        private Integer exchanged;
        @SerializedName("in_house_financed")
        @Expose
        private Integer in_house_financed;
        @SerializedName("out_house_financed")
        @Expose
        private Integer out_house_financed;

        @SerializedName("pending_bookings")
        @Expose
        private Integer pending_bookings;

        @SerializedName("live_enquiries")
        @Expose
        private Integer live_enquiries;

        public Integer getIn_house_financed() {
            return in_house_financed;
        }

        public void setIn_house_financed(Integer in_house_financed) {
            this.in_house_financed = in_house_financed;
        }

        public Integer getOut_house_financed() {
            return out_house_financed;
        }

        public void setOut_house_financed(Integer out_house_financed) {
            this.out_house_financed = out_house_financed;
        }

        public Integer getExchanged() {
            return exchanged;
        }

        public void setExchanged(Integer exchanged) {
            this.exchanged = exchanged;
        }
        
        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetails() {
            return retails;
        }

        public void setRetails(Integer retails) {
            this.retails = retails;
        }

        public Integer getLost_drop() {
            return lost_drop;
        }

        public void setLost_drop(Integer lost_drop) {
            this.lost_drop = lost_drop;
        }

        public Integer getPending_bookings() {
            return pending_bookings;
        }

        public void setPending_bookings(Integer pending_bookings) {
            this.pending_bookings = pending_bookings;
        }

        public Integer getLive_enquiries() {
            return live_enquiries;
        }

        public void setLive_enquiries(Integer live_enquiries) {
            this.live_enquiries = live_enquiries;
        }
    }

    public class Location {

        @SerializedName("location_id")
        @Expose
        private Integer locationId;
        @SerializedName("location_abs")
        @Expose
        private LocationAbs locationAbs;
        @SerializedName("location_rel")
        @Expose
        private LocationRel locationRel;
        @SerializedName("car_models")
        @Expose
        private List<CarModel> carModels = null;

        public Integer getLocationId() {
            return locationId;
        }

        public void setLocationId(Integer locationId) {
            this.locationId = locationId;
        }

        public LocationAbs getLocationAbs() {
            return locationAbs;
        }

        public void setLocationAbs(LocationAbs locationAbs) {
            this.locationAbs = locationAbs;
        }

        public LocationRel getLocationRel() {
            return locationRel;
        }

        public void setLocationRel(LocationRel locationRel) {
            this.locationRel = locationRel;
        }

        public List<CarModel> getCarModels() {
            return carModels;
        }

        public void setCarModels(List<CarModel> carModels) {
            this.carModels = carModels;
        }

    }

    public class CarModel {

        @SerializedName("info")
        @Expose
        private Info info;
        @SerializedName("abs")
        @Expose
        private Abs abs;
        @SerializedName("rel")
        @Expose
        private Rel rel;

        public Info getInfo() {
            return info;
        }

        public void setInfo(Info info) {
            this.info = info;
        }

        public Abs getAbs() {
            return abs;
        }

        public void setAbs(Abs abs) {
            this.abs = abs;
        }

        public Rel getRel() {
            return rel;
        }

        public void setRel(Rel rel) {
            this.rel = rel;
        }

    }

    public class Info {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Abs {

        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retails")
        @Expose
        private Integer retails;
        @SerializedName("lost_drop")
        @Expose
        private Integer lost_drop;
        @SerializedName("exchanged")
        @Expose
        private Integer exchanged;
        @SerializedName("in_house_financed")
        @Expose
        private Integer in_house_financed;
        @SerializedName("out_house_financed")
        @Expose
        private Integer out_house_financed;

        @SerializedName("pending_bookings")
        @Expose
        private Integer pending_bookings;

        @SerializedName("live_enquiries")
        @Expose
        private Integer live_enquiries;

        public Integer getIn_house_financed() {
            return in_house_financed;
        }

        public void setIn_house_financed(Integer in_house_financed) {
            this.in_house_financed = in_house_financed;
        }

        public Integer getOut_house_financed() {
            return out_house_financed;
        }

        public void setOut_house_financed(Integer out_house_financed) {
            this.out_house_financed = out_house_financed;
        }
        public Integer getExchanged() {
            return exchanged;
        }

        public void setExchanged(Integer exchanged) {
            this.exchanged = exchanged;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetails() {
            return retails;
        }

        public void setRetails(Integer retails) {
            this.retails = retails;
        }

        public Integer getLost_drop() {
            return lost_drop;
        }

        public void setLost_drop(Integer lost_drop) {
            this.lost_drop = lost_drop;
        }

        public Integer getPending_bookings() {
            return pending_bookings;
        }

        public void setPending_bookings(Integer pending_bookings) {
            this.pending_bookings = pending_bookings;
        }

        public Integer getLive_enquiries() {
            return live_enquiries;
        }

        public void setLive_enquiries(Integer live_enquiries) {
            this.live_enquiries = live_enquiries;
        }
    }

    public class Rel {

        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retails")
        @Expose
        private Integer retails;
        @SerializedName("lost_drop")
        @Expose
        private Integer lost_drop;
        @SerializedName("exchanged")
        @Expose
        private Integer exchanged;
        @SerializedName("in_house_financed")
        @Expose
        private Integer in_house_financed;
        @SerializedName("out_house_financed")
        @Expose
        private Integer out_house_financed;

        @SerializedName("pending_bookings")
        @Expose
        private Integer pending_bookings;

        @SerializedName("live_enquiries")
        @Expose
        private Integer live_enquiries;

        public Integer getIn_house_financed() {
            return in_house_financed;
        }

        public void setIn_house_financed(Integer in_house_financed) {
            this.in_house_financed = in_house_financed;
        }

        public Integer getOut_house_financed() {
            return out_house_financed;
        }

        public void setOut_house_financed(Integer out_house_financed) {
            this.out_house_financed = out_house_financed;
        }

        public Integer getExchanged() {
            return exchanged;
        }

        public void setExchanged(Integer exchanged) {
            this.exchanged = exchanged;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetails() {
            return retails;
        }

        public void setRetails(Integer retails) {
            this.retails = retails;
        }

        public Integer getLost_drop() {
            return lost_drop;
        }

        public void setLost_drop(Integer lost_drop) {
            this.lost_drop = lost_drop;
        }

        public Integer getPending_bookings() {
            return pending_bookings;
        }

        public void setPending_bookings(Integer pending_bookings) {
            this.pending_bookings = pending_bookings;
        }

        public Integer getLive_enquiries() {
            return live_enquiries;
        }

        public void setLive_enquiries(Integer live_enquiries) {
            this.live_enquiries = live_enquiries;
        }
    }

    public class TotalAbs {

        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retails")
        @Expose
        private Integer retails;
        @SerializedName("lost_drop")
        @Expose
        private Integer lost_drop;
        @SerializedName("exchanged")
        @Expose
        private Integer exchanged;
        @SerializedName("in_house_financed")
        @Expose
        private Integer in_house_financed;
        @SerializedName("out_house_financed")
        @Expose
        private Integer out_house_financed;

        @SerializedName("pending_bookings")
        @Expose
        private Integer pending_bookings;

        @SerializedName("live_enquiries")
        @Expose
        private Integer live_enquiries;

        public Integer getIn_house_financed() {
            return in_house_financed;
        }

        public void setIn_house_financed(Integer in_house_financed) {
            this.in_house_financed = in_house_financed;
        }

        public Integer getOut_house_financed() {
            return out_house_financed;
        }

        public void setOut_house_financed(Integer out_house_financed) {
            this.out_house_financed = out_house_financed;
        }

        public Integer getExchanged() {
            return exchanged;
        }

        public void setExchanged(Integer exchanged) {
            this.exchanged = exchanged;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetails() {
            return retails;
        }

        public void setRetails(Integer retails) {
            this.retails = retails;
        }

        public Integer getLost_drop() {
            return lost_drop;
        }

        public void setLost_drop(Integer lost_drop) {
            this.lost_drop = lost_drop;
        }

        public Integer getPending_bookings() {
            return pending_bookings;
        }

        public void setPending_bookings(Integer pending_bookings) {
            this.pending_bookings = pending_bookings;
        }

        public Integer getLive_enquiries() {
            return live_enquiries;
        }

        public void setLive_enquiries(Integer live_enquiries) {
            this.live_enquiries = live_enquiries;
        }
    }

    public class TotalRel {

        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retails")
        @Expose
        private Integer retails;
        @SerializedName("lost_drop")
        @Expose
        private Integer lost_drop;
        @SerializedName("exchanged")
        @Expose
        private Integer exchanged;
        @SerializedName("in_house_financed")
        @Expose
        private Integer in_house_financed;
        @SerializedName("out_house_financed")
        @Expose
        private Integer out_house_financed;

        @SerializedName("pending_bookings")
        @Expose
        private Integer pending_bookings;

        @SerializedName("live_enquiries")
        @Expose
        private Integer live_enquiries;

        public Integer getIn_house_financed() {
            return in_house_financed;
        }

        public void setIn_house_financed(Integer in_house_financed) {
            this.in_house_financed = in_house_financed;
        }

        public Integer getOut_house_financed() {
            return out_house_financed;
        }

        public void setOut_house_financed(Integer out_house_financed) {
            this.out_house_financed = out_house_financed;
        }

        public Integer getExchanged() {
            return exchanged;
        }

        public void setExchanged(Integer exchanged) {
            this.exchanged = exchanged;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetails() {
            return retails;
        }

        public void setRetails(Integer retails) {
            this.retails = retails;
        }

        public Integer getLost_drop() {
            return lost_drop;
        }

        public void setLost_drop(Integer lost_drop) {
            this.lost_drop = lost_drop;
        }

        public Integer getPending_bookings() {
            return pending_bookings;
        }

        public void setPending_bookings(Integer pending_bookings) {
            this.pending_bookings = pending_bookings;
        }

        public Integer getLive_enquiries() {
            return live_enquiries;
        }

        public void setLive_enquiries(Integer live_enquiries) {
            this.live_enquiries = live_enquiries;
        }
    }
}
