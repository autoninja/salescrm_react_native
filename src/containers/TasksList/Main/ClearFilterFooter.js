import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from 'react-native';

import { connect } from 'react-redux';
import { updateTasksListFilter } from '../../../actions/tasks_list_actions';

class ClearFilterFooter extends Component {
    state = {
        filters: []
    }
    constructor(props) {
        super(props);
    }
    componentDidMount() {
    }



    render() {
        return (
            <TouchableOpacity
                onPress={() => {

                    this.props.navigation.navigate('AutoDialog', {
                        data: {
                            title: "Are you sure want to clear Filters?\n",
                            yes: "Yes",
                            no: "No"
                        },
                        onNoButtonPress: () => {

                        },
                        onYesButtonPress: () => {
                            this.props.updateFilters([], true);
                        },

                    });


                }}>
                <View style={{
                    backgroundColor: 'rgba(0,0,0,0.4)',
                    flexDirection: 'row', height: 48,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>

                    <Image style={{ height: 16, width: 16, margin: 10 }}
                        source={require('../../../images/ic_close_white.png')}
                    />
                    <Text style={{ color: '#fff', fontSize: 18 }}>Clear Filters</Text>

                </View>
            </TouchableOpacity >
        )
    }
}
const mapStateToProps = state => {
    return {
        filters: state.tasksListReducer.filters
    }
}
const mapDispatchToProps = dispatch => {
    return {
        updateFilters: (filters, filtersUpdate) => {
            dispatch(updateTasksListFilter({ filters, filtersUpdate }))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ClearFilterFooter)