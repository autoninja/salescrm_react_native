package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bharath on 2/2/17.
 */
public class DefaultFAId extends RealmObject{
    private String fAnsId;

    private String displayText;

    private String answerValue;

    public String getfAnsId() {
        return fAnsId;
    }

    public void setfAnsId(String fAnsId) {
        this.fAnsId = fAnsId;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public String getAnswerValue() {
        return answerValue;
    }

    public void setAnswerValue(String answerValue) {
        this.answerValue = answerValue;
    }
}
