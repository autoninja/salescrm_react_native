import { UPDATE_CURRENT_VIEW } from './main_header_types';


export const updateMainHeaderCurrentView = payload => {
  return {
    type: UPDATE_CURRENT_VIEW,
    payload: payload
  }
}
