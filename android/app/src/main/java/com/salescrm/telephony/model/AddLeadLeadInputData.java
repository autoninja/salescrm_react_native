package com.salescrm.telephony.model;

/**
 * Created by bharath on 1/9/16.
 */
public class AddLeadLeadInputData {
    private String enqSourceId;
    private String leadDate;
    private String expectedPurDate;
    private String modeOfPayment;
    private String sourceCategory;
    private String vinReg;

    public AddLeadLeadInputData() {
    }

    public AddLeadLeadInputData(String enqSourceId, String leadDate, String expectedPurDate, String modeOfPayment,String sourceCategory,String vinReg) {
        this.enqSourceId = enqSourceId;
        this.leadDate = leadDate;
        this.expectedPurDate = expectedPurDate;
        this.modeOfPayment = modeOfPayment;
        this.sourceCategory = sourceCategory;
        this.vinReg = vinReg;
    }

    public String getVinReg() {
        return vinReg;
    }

    public void setVinReg(String vinReg) {
        this.vinReg = vinReg;
    }

    public String getEnqSourceId() {
        return enqSourceId;
    }

    public void setEnqSourceId(String enqSourceId) {
        this.enqSourceId = enqSourceId;
    }

    public String getLeadDate() {
        return leadDate;
    }

    public void setLeadDate(String leadDate) {
        this.leadDate = leadDate;
    }

    public String getExpectedPurDate() {
        return expectedPurDate;
    }

    public void setExpectedPurDate(String expectedPurDate) {
        this.expectedPurDate = expectedPurDate;
    }

    public String getModeOfPayment() {
        return modeOfPayment;
    }

    public void setModeOfPayment(String modeOfPayment) {
        this.modeOfPayment = modeOfPayment;
    }

    public String getSourceCategory() {
        return sourceCategory;
    }

    public void setSourceCategory(String sourceCategory) {
        this.sourceCategory = sourceCategory;
    }
}
