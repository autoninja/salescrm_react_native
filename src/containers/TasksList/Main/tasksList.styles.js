import { StyleSheet, Platform } from 'react-native';
var Dimensions = require('Dimensions');
export default StyleSheet.create({

    MainContainer: {
        flex: 1,
    },
    container: {
        backgroundColor: '#fff',
        margin: 4,
        overflow: 'hidden'
    },
    cardMain: {
        flexWrap: "wrap",
        flexDirection: "row",
        backgroundColor: "#fff",
        marginTop: 0.4,
        shadowColor: "#bdbdbd",
        shadowOpacity: 0.4,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 0
        }
    },
    cardWrapper: {
        flexDirection: "row",
        flex: 1,
        flexWrap: "wrap",
        margin: 2,
        borderColor: "#bdbdbd",
        borderWidth: 0.5,
        borderRadius: 6,
        backgroundColor: "#fff",
        elevation: 2,
    },
    leadTagIndicator: {
        width: 6,
        borderBottomLeftRadius: 6,
        borderTopLeftRadius: 6
    },
    cardLeft: {
        flex: 4,
        padding: 4,
    },
    cardRight: {
        flex: 6,
        padding: 4,
    },
    body: {
        padding: 10,
        paddingTop: 0
    },
    children: {
        backgroundColor: '#E9EEFF',
        paddingTop: 10,
        marginTop: -6,
        marginLeft: 8,
        marginRight: 20,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6
    },
    childBody: {
        padding: 15
    },
    update: {
        backgroundColor: '#007FFF',
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6,
        height: 36,
        alignItems: 'center',
        justifyContent: 'center'

    },
    scheduledDate: {
        marginBottom: -4,
        fontSize: 30,
        padding: 0,
        textAlignVertical: 'bottom',
        color: "#007fff"
    },
    bottomFixed: {
        fontSize: 16,
        textAlignVertical: 'bottom',
        paddingLeft: 4,
        paddingRight: 4,
        color: '#9b9b9b'

    },
    taskName: {
        fontSize: 13,
        fontWeight: 'bold',
        paddingLeft: 4,
        textAlignVertical: 'bottom',
        color: "#007fff"
    },
    name: {
        textAlignVertical: 'bottom',
        fontSize: 20,
        flex: 1,
        color: '#45446d'

    },
    imageNav: {
        opacity: 0.5,
        height: 16,
        width: 20
    },
    stage: {
        fontSize: 13,
        color: '#007fff',
        marginLeft: 8
    },
    stageTag: {
        borderRadius: 3,
        fontSize: 10,
        backgroundColor: 'rgba(48,48,48,0.3)',
        paddingLeft: 4,
        paddingRight: 4,
        textAlign: 'center',
        textAlignVertical: 'center',
        marginLeft: 10,
        color: '#303030'
    },
    hr: {
        height: 1,
        marginTop: 10,
        marginBottom: 10,
        opacity: 0.4,
        backgroundColor: '#979797'
    },
    imageButton: {
        width: 40,
        height: 40,
        borderRadius: 40,
        backgroundColor: '#0076ff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    loader: {
        height: 36,
        width: 36,
        alignSelf: 'center'

    }


});
