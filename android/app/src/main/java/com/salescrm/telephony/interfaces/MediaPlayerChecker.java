package com.salescrm.telephony.interfaces;

/**
 * Created by nndra on 13-Sep-17.
 */

public interface MediaPlayerChecker {

    void onPlayerClose(boolean status);

}
