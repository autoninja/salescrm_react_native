package com.salescrm.telephony.db.change_lead_stages;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 23/12/16.
 */
public class LostReasons extends RealmObject {

    private String id;

    private String reason;

    public RealmList<ChildReasons> childReasons = new RealmList<>();

    public RealmList<ChildReasons> getChildReasons() {
        return childReasons;
    }

    public void setChildReasons(RealmList<ChildReasons> childReasons) {
        this.childReasons = childReasons;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
