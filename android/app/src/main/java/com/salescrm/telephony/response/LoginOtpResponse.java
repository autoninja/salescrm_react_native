package com.salescrm.telephony.response;

/**
 * Created by bharath on 11/7/16.
 */
public class LoginOtpResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }
    public class Result
    {
        private String token;

        private String success;

        private Info info;

        public String getToken ()
        {
            return token;
        }

        public void setToken (String token)
        {
            this.token = token;
        }

        public String getSuccess ()
        {
            return success;
        }

        public void setSuccess (String success)
        {
            this.success = success;
        }

        public Info getInfo ()
        {
            return info;
        }

        public void setInfo (Info info)
        {
            this.info = info;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [token = "+token+", success = "+success+", info = "+info+"]";
        }
    }
    public class Info
    {
        private Message[] message;

        private String cre_count;

        public Message[] getMessage ()
        {
            return message;
        }

        public void setMessage (Message[] message)
        {
            this.message = message;
        }

        public String getCre_count ()
        {
            return cre_count;
        }

        public void setCre_count (String cre_count)
        {
            this.cre_count = cre_count;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [message = "+message+", cre_count = "+cre_count+"]";
        }
    }
    public class Message
    {
        private String dealer;

        private String id;

        private String user_name;

        private String module;

        private String email;

        private String name;

        private boolean store_recording;

        private boolean otpStore;

        private String otp;

        private String mobile;

        public String getDealer ()
        {
            return dealer;
        }

        public void setDealer (String dealer)
        {
            this.dealer = dealer;
        }

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getUser_name ()
        {
            return user_name;
        }

        public void setUser_name (String user_name)
        {
            this.user_name = user_name;
        }

        public String getModule ()
        {
            return module;
        }

        public void setModule (String module)
        {
            this.module = module;
        }

        public String getEmail ()
        {
            return email;
        }

        public void setEmail (String email)
        {
            this.email = email;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public boolean getStore_recording ()
        {
            return store_recording;
        }

        public void setStore_recording (boolean store_recording)
        {
            this.store_recording = store_recording;
        }

        public boolean getOtpStore ()
        {
            return otpStore;
        }

        public void setOtpStore (boolean otpStore)
        {
            this.otpStore = otpStore;
        }

        public String getOtp ()
        {
            return otp;
        }

        public void setOtp (String otp)
        {
            this.otp = otp;
        }

        public String getMobile ()
        {
            return mobile;
        }

        public void setMobile (String mobile)
        {
            this.mobile = mobile;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [dealer = "+dealer+", id = "+id+", user_name = "+user_name+", module = "+module+", email = "+email+", name = "+name+", store_recording = "+store_recording+", otpStore = "+otpStore+", otp = "+otp+", mobile = "+mobile+"]";
        }
    }

}
