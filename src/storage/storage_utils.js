import Realm from 'realm';
import {realm} from './realm_schemas'
import { USER_ROLE_SALES_CONSULTANT, USER_ROLE_TEAM_LEAD, USER_ROLE_SALES_MANAGER, USER_ROLE_BRANCH_HEAD, USER_ROLE_OPERATIONS } from '../utils/user_roles'

let StorageUtils = {
  isGamificationEnabled: function() {
    let gameEnabled = false;
    let userRoleAccessGame = realm.objects('UserRoles').filtered(
      'id == '+USER_ROLE_SALES_CONSULTANT.id + ' || ' +
      'id == '+USER_ROLE_SALES_MANAGER.id + ' || ' +
      'id == '+USER_ROLE_BRANCH_HEAD.id + ' || ' +
      'id == '+USER_ROLE_OPERATIONS.id
    );
    let gameLocations= realm.objects('GamificationLocations').filtered('gamification == true');
    return (userRoleAccessGame.length>0 && gameLocations.length>0);
  },

};

module.exports = StorageUtils;
