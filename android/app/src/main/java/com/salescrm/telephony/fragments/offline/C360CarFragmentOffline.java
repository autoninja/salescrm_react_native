package com.salescrm.telephony.fragments.offline;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.offline.OfflineC360LeadProcessActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandModelsDB;
import com.salescrm.telephony.db.car.CarBrandsDB;
import com.salescrm.telephony.db.car.ExchangeCarBrandModelsDB;
import com.salescrm.telephony.db.create_lead.Ad_car_details;
import com.salescrm.telephony.db.create_lead.Car_details;
import com.salescrm.telephony.db.create_lead.Ex_car_details;
import com.salescrm.telephony.db.create_lead.Lead_data;
import com.salescrm.telephony.interfaces.AddLeadCommunication;
import com.salescrm.telephony.model.AddLeadCarInputData;
import com.salescrm.telephony.model.CreateLeadInputData;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.CarModelDetailsResponse;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

import static com.salescrm.telephony.utils.Util.showDatePicker;

/**
 * Created by bharath on 30/5/16.
 */
public class C360CarFragmentOffline extends Fragment implements View.OnClickListener, View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener {
    //EditText etCarInterestedVar;
    TextView tvCarExYear, tvCarAddYear;
    Button btAddCarInterested, btAddCarEx, btAddCarAdd;
    LinearLayout llCarInterestedParent, llCarInterestedSpinnerHolder, llHrCarInter;
    LinearLayout llCarExParent, llCarExSpinnerHolder, llHrCarEx;
    LinearLayout llCarAddParent, llCarAddSpinnerHolder, llHrCarAdd;
    TextInputEditText etExReg, etExRun, etAddReg, etAddRun;
    private AddLeadCommunication addLeadCommunication;
    private String TAG = "AddLeadCar";
    private AutoCompleteTextView autoTvInterCarBrand, autoTvInterCarModel, autoTvInterCarVariant, autoTvInterCarFuelType, autoTvInterCarColor, autoTvExCarBrand,
            autoTvExCarModel, autoTvAddCarBrand, autoTvAddCarModel;
    private Realm realm;
    private Preferences pref = null;
    private CarModelDetailsResponse.Result carModelDetailsResult;
    private ProgressDialog progressDialog;
    private Set<String> setCarVariants;
    private int interCarCount = 0, exCarCount = 0, addCarCount = 0;
    private List<CreateLeadInputData.Lead_data.Car_details> listNewCar;
    private List<CreateLeadInputData.Lead_data.Ex_car_details> listExCar;
    private List<CreateLeadInputData.Lead_data.Ad_car_details> listAddCar;
    private View view;

    private TextInputLayout inputLayoutAddBrand;
    private TextInputLayout inputLayoutInterCarBrand;
    private TextInputLayout inputLayoutExBrand;

    private RealmResults<CarBrandsDB> interCarBrandList;

    private RealmResults<CarBrandModelsDB> carModelList=null;
    private RealmResults<CarBrandModelVariantsDB> carVariantList=null;
    private RealmList<CarBrandModelsDB> carModelListEx=null;
    private RealmList<CarBrandModelsDB> carModelListAdd;
    private RealmResults<CarBrandModelVariantsDB> colorList=null;
    private RealmResults<ExchangeCarBrandModelsDB> carModelListOld;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_lead_car_details, container, false);

        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(getContext());
        setCarVariants = new HashSet<String>();
        listNewCar = new ArrayList<>();
        listAddCar = new ArrayList<>();
        listExCar = new ArrayList<>();


        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Getting car details. Please wait! ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);


        //------Ids for Interested Car Card

        llCarInterestedParent = (LinearLayout) view.findViewById(R.id.ll_lead_car_inter_container);
        llCarInterestedSpinnerHolder = (LinearLayout) view.findViewById(R.id.ll_car_inter_spinner_holder);
        llHrCarInter = (LinearLayout) view.findViewById(R.id.ll_hr_car_inter);

        inputLayoutInterCarBrand = (TextInputLayout) view.findViewById(R.id.input_layout_car_brand);
        autoTvInterCarBrand = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_add_lead_car_brand);
        autoTvInterCarModel = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_add_lead_car_model);
        autoTvInterCarVariant = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_lead_car_var);
        autoTvInterCarFuelType = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_lead_car_fuel_type);
        autoTvInterCarColor = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_lead_car_color);
        btAddCarInterested = (Button) view.findViewById(R.id.bt_add_lead_car_inter);


        //-------------------------------


        //------Ids for Exchange Car Card
        llCarExParent = (LinearLayout) view.findViewById(R.id.ll_lead_car_ex_container);
        llCarExSpinnerHolder = (LinearLayout) view.findViewById(R.id.ll_car_ex_spinner_holder);
        llHrCarEx = (LinearLayout) view.findViewById(R.id.ll_hr_car_ex);
        inputLayoutExBrand = (TextInputLayout) view.findViewById(R.id.input_layout_ex_brand);
        autoTvExCarBrand = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_add_lead_car_exp_brand);
        autoTvExCarModel = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_add_lead_car_exp_model);
        tvCarExYear = (TextView) view.findViewById(R.id.spinner_lead_car_ex_year);
        btAddCarEx = (Button) view.findViewById(R.id.bt_add_lead_car_ex);
        etExReg = (TextInputEditText) view.findViewById(R.id.et_lead_reg_ex_no);
        etExRun = (TextInputEditText) view.findViewById(R.id.et_lead_reg_ex_run);
        //------------------------------


        //-------Ids for Additional Car Card
        llCarAddParent = (LinearLayout) view.findViewById(R.id.ll_lead_car_add_container);
        llCarAddSpinnerHolder = (LinearLayout) view.findViewById(R.id.ll_car_add_spinner_holder);
        llHrCarAdd = (LinearLayout) view.findViewById(R.id.ll_hr_car_add);
        inputLayoutAddBrand = (TextInputLayout) view.findViewById(R.id.input_layout_add_brand);
        autoTvAddCarBrand = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_add_lead_car_add_brand);
        autoTvAddCarModel = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_add_lead_car_add_model);
//        spinnerCarAddModel = (Spinner) view.findViewById(R.id.spinner_lead_car_add_model);
        tvCarAddYear = (TextView) view.findViewById(R.id.spinner_lead_car_add_year);
        btAddCarAdd = (Button) view.findViewById(R.id.bt_add_lead_car_add);
        etAddReg = (TextInputEditText) view.findViewById(R.id.et_lead_reg_add_no);
        etAddRun = (TextInputEditText) view.findViewById(R.id.et_lead_reg_add_run);
        //----------------------------------

        //Show brand visibility:
        inputLayoutInterCarBrand.setVisibility(View.VISIBLE);
        inputLayoutExBrand.setVisibility(View.VISIBLE);
        inputLayoutAddBrand.setVisibility(View.VISIBLE);

        initAdapter();

        tvCarAddYear.setOnClickListener(this);
        tvCarExYear.setOnClickListener(this);

       /* autoTvInterCarBrand.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(getActivity());
                //progressDialog.show();
                autoTvInterCarModel.setText("");
                autoTvInterCarVariant.setText("");
                autoTvInterCarFuelType.setText("");
                autoTvInterCarColor.setText("");
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, getCarBrandModel(position));
                autoTvInterCarModel.setAdapter(adapter);
                autoTvInterCarModel.requestFocus();
            }
        });*/


       /* autoTvExCarBrand.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(getActivity());
                //progressDialog.show();
                autoTvExCarModel.setText("");
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, getCarBrandModelEx(position));
                autoTvExCarModel.setAdapter(adapter);
                autoTvExCarModel.requestFocus();
            }
        });*/

        /*autoTvAddCarBrand.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(getActivity());
                //progressDialog.show();
                autoTvAddCarModel.setText("");
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, getCarBrandModelAdd(position));
                autoTvAddCarModel.setAdapter(adapter);
                autoTvAddCarModel.requestFocus();
            }
        });*/


        autoTvInterCarModel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Util.HideKeyboard(getActivity());
                //progressDialog.show();
                autoTvInterCarVariant.setText("");
                autoTvInterCarFuelType.setText("");
                autoTvInterCarColor.setText("");
                carVariantList = null;
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, getCarBrandModelVariants());
                autoTvInterCarVariant.setAdapter(adapter);
                autoTvInterCarVariant.requestFocus();
            }
        });

        autoTvInterCarVariant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(getActivity());
                autoTvInterCarFuelType.setText("");
                autoTvInterCarColor.setText("");
                autoTvInterCarColor.setText("");
//                colorList=null;
                //setColorAndFuelType();
                // interCarVariantPosition =position;
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, setColorAndFuelType());
                autoTvInterCarColor.setAdapter(adapter);
                autoTvInterCarColor.requestFocus();

            }
        });

        autoTvExCarModel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(getActivity());
            }
        });

        autoTvAddCarModel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(getActivity());
            }
        });


        btAddCarInterested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (llCarInterestedSpinnerHolder.getVisibility() == View.VISIBLE) {
                    if(isInterestedCarModelValid(autoTvInterCarModel.getText().toString())&&isNewCarOtherDetailsValid()){
                        addChildView(0, btAddCarInterested, llCarInterestedParent, R.layout.lead_child_car_layout,
                                llCarInterestedSpinnerHolder, llHrCarInter);
                        autoTvInterCarModel.setText("");

                    }
                    else {
                        if (TextUtils.isEmpty(autoTvInterCarModel.getText())) {
                            showAlert("Add car model");
                        } else {
                            showAlert("Invalid entry");
                        }
                    }

                } else {
                    btAddCarInterested.setText("Add");
                    autoTvInterCarModel.setText("");
                    autoTvInterCarVariant.setAdapter(null);
                    autoTvInterCarVariant.setText("");
                    autoTvInterCarColor.setAdapter(null);
                    autoTvInterCarColor.setText("");
                    autoTvInterCarFuelType.setTag(null);
                    autoTvInterCarFuelType.setTag(R.id.val,null);
                    autoTvInterCarFuelType.setText("");
                    llCarInterestedSpinnerHolder.setVisibility(View.VISIBLE);
                }
            }
        });


        btAddCarEx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (llCarExSpinnerHolder.getVisibility() == View.VISIBLE) {
                    if(isExchangeCarModelValid(autoTvExCarModel.getText().toString())){
                        addChildView(1, btAddCarEx, llCarExParent, R.layout.lead_child_car_layout,
                                llCarExSpinnerHolder, llHrCarEx);
                        autoTvExCarModel.setText("");
                    }
                    else {
                        if(TextUtils.isEmpty(autoTvExCarModel.getText())) {
                            showAlert("Add car model");
                        } else {
                            showAlert("Invalid Car");
                        }
                    }


                } else {
                    btAddCarEx.setText("Add");
                    autoTvExCarModel.setText("");
                    tvCarExYear.setText("Bought In");
                    etExReg.setText("");
                    etExRun.setText("");

                    llCarExSpinnerHolder.setVisibility(View.VISIBLE);
                }
            }
        });


        btAddCarAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (llCarAddSpinnerHolder.getVisibility() == View.VISIBLE) {
                    if(isExchangeCarModelValid(autoTvAddCarModel.getText().toString())){
                        addChildView(2, btAddCarAdd, llCarAddParent, R.layout.lead_child_car_layout,
                                llCarAddSpinnerHolder, llHrCarAdd);
                        autoTvAddCarModel.setText("");
                    }
                    else {
                        if (TextUtils.isEmpty(autoTvAddCarModel.getText())) {
                            showAlert("Add car model");
                        } else {
                            showAlert("Invalid car model");
                        }
                    }


                } else {
                    btAddCarAdd.setText("Add");
                    autoTvAddCarBrand.setText("");
                    autoTvAddCarModel.setText("");
                    tvCarAddYear.setText("Bought In");
                    etAddReg.setText("");
                    etAddRun.setText("");
                    llCarAddSpinnerHolder.setVisibility(View.VISIBLE);
                }
            }
        });

        OfflineC360LeadProcessActivity.tvAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!autoTvInterCarModel.getText().toString().isEmpty()||
                        !autoTvExCarModel.getText().toString().isEmpty()
                        ||!autoTvAddCarModel.getText().toString().isEmpty()){
                    Toast.makeText(getContext(),"Car not added",Toast.LENGTH_SHORT).show();
                }
                else {

                    realm.beginTransaction();
                    Lead_data data = OfflineC360LeadProcessActivity.salesCRMRealmTable.getCreateLeadInputDataDB().getLead_data();
                    AddLeadCarInputData carCurrentData = getCarInputData();
                    if (data != null && carCurrentData != null) {
                        if (carCurrentData.getNewCarInputData().size() > 0) {
                            RealmList<Car_details> carDetailsList = new RealmList<Car_details>();
                            carDetailsList.addAll(data.getCar_details());

                            for (int i = 0; i < carCurrentData.getNewCarInputData().size(); i++) {
                                Car_details car_details = realm.createObject(Car_details.class); //
                                car_details.setBrand_id(carCurrentData.getNewCarInputData().get(i).getBrand_id());
                                car_details.setFuel_type_id(carCurrentData.getNewCarInputData().get(i).getFuel_type_id());
                                car_details.setColor_id(carCurrentData.getNewCarInputData().get(i).getColor_id());
                                car_details.setModel_id(carCurrentData.getNewCarInputData().get(i).getModel_id());
                                car_details.setVariant_id(carCurrentData.getNewCarInputData().get(i).getVariant_id());
                                car_details.setIs_activity_target(carCurrentData.getNewCarInputData().get(i).getIs_activity_target());
                                car_details.setIs_primary(carCurrentData.getNewCarInputData().get(i).getIs_primary());
                                car_details.setBrand_name(carCurrentData.getNewCarInputData().get(i).getBrand_name());
                                car_details.setModel_name(carCurrentData.getNewCarInputData().get(i).getModel_name());
                                car_details.setVariant_name(carCurrentData.getNewCarInputData().get(i).getVariant_name());
                                car_details.setColor_name(carCurrentData.getNewCarInputData().get(i).getColor_name());
                                car_details.setFuel_name(carCurrentData.getNewCarInputData().get(i).getFuel_name());
                                carDetailsList.add(car_details);
                            }
                            data.setCar_details(carDetailsList);
                        }

                        if (carCurrentData.getExCarInputData().size() > 0) {
                            RealmList<Ex_car_details> exCarDetailsList = new RealmList<Ex_car_details>();
                            exCarDetailsList.addAll(data.getEx_car_details());

                            for (int i = 0; i < carCurrentData.getExCarInputData().size(); i++) {
                                Ex_car_details ex_car_details = realm.createObject(Ex_car_details.class);

                                ex_car_details.setMilage(carCurrentData.getExCarInputData().get(i).getMilage());
                                ex_car_details.setKms_run(carCurrentData.getExCarInputData().get(i).getKms_run());
                                ex_car_details.setModel_id(carCurrentData.getExCarInputData().get(i).getModel_id());
                                ex_car_details.setIs_primary(carCurrentData.getExCarInputData().get(i).getIs_primary());
                                ex_car_details.setPurchase_date(carCurrentData.getExCarInputData().get(i).getPurchase_date());
                                ex_car_details.setIs_activity_target(carCurrentData.getExCarInputData().get(i).is_activity_target());
                                ex_car_details.setBrand_id(carCurrentData.getExCarInputData().get(i).getBrand_id());
                                ex_car_details.setBrand_name(carCurrentData.getExCarInputData().get(i).getBrand_name());
                                ex_car_details.setModel_name(carCurrentData.getExCarInputData().get(i).getModel_name());


                                exCarDetailsList.add(ex_car_details);
                            }
                            data.setEx_car_details(exCarDetailsList);
                        }

                        if (carCurrentData.getAddCarInputData().size() > 0) {
                            RealmList<Ad_car_details> adCarDetailsList = new RealmList<Ad_car_details>();
                            adCarDetailsList.addAll(data.getAd_car_details());

                            for (int i = 0; i < carCurrentData.getAddCarInputData().size(); i++) {
                                Ad_car_details ad_car_details = realm.createObject(Ad_car_details.class);

                                ad_car_details.setMilage(carCurrentData.getAddCarInputData().get(i).getMilage());
                                ad_car_details.setKms_run(carCurrentData.getAddCarInputData().get(i).getKms_run());
                                ad_car_details.setModel_id(carCurrentData.getAddCarInputData().get(i).getModel_id());
                                ad_car_details.setIs_primary(carCurrentData.getAddCarInputData().get(i).getIs_primary());
                                ad_car_details.setPurchase_date(carCurrentData.getAddCarInputData().get(i).getPurchase_date());
                                ad_car_details.setIs_activity_target(carCurrentData.getAddCarInputData().get(i).is_activity_target());
                                ad_car_details.setBrand_id(carCurrentData.getAddCarInputData().get(i).getBrand_id());
                                ad_car_details.setBrand_name(carCurrentData.getAddCarInputData().get(i).getBrand_name());
                                ad_car_details.setModel_name(carCurrentData.getAddCarInputData().get(i).getModel_name());


                                adCarDetailsList.add(ad_car_details);
                            }
                            data.setAd_car_details(adCarDetailsList);
                        }
                    }
                    realm.commitTransaction();
                    getActivity().onBackPressed();
                }
            }
        });
        return view;


    }

    private boolean isNewCarOtherDetailsValid() {
        if(!autoTvInterCarVariant.getText().toString().isEmpty()){
            if(carVariantList==null||carVariantList.where().equalTo("variant",autoTvInterCarVariant.getText().toString())
                    .findFirst()==null){
                Toast.makeText(getContext(),"Invalid car variant",Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if(!autoTvInterCarFuelType.getText().toString().isEmpty()){
            if(autoTvInterCarFuelType.getTag()==null){
                Toast.makeText(getContext(),"Invalid car fuel type",Toast.LENGTH_SHORT).show();
                return false;
            }
            if(autoTvInterCarFuelType.getTag(R.id.val)==null||
                    !autoTvInterCarFuelType.getText().toString().equalsIgnoreCase(autoTvInterCarFuelType.getTag(R.id.val).toString())){
                Toast.makeText(getContext(),"Invalid car fuel type",Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if(!autoTvInterCarColor.getText().toString().isEmpty()){
            if(colorList==null||colorList.where().equalTo("color",
                    autoTvInterCarColor.getText().toString()).findFirst()==null){
                Toast.makeText(getContext(),"Invalid car color",Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    private boolean isInterestedCarModelValid(String text) {
        CarBrandModelsDB realmData = realm.where(CarBrandModelsDB.class).equalTo("model_name", text).findFirst();
        return realmData != null;
    }
    private boolean isExchangeCarModelValid(String text) {
        ExchangeCarBrandModelsDB realmData = realm.where(ExchangeCarBrandModelsDB.class).equalTo("model_name", text).findFirst();
        return realmData != null;
    }


    private void initAdapter() {
        ArrayAdapter<String> brandsAdapterModel = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                getCarBrandModel());
//        System.out.println("Car Brand Size:"+getCarBrandModel().length);
        autoTvInterCarModel.setAdapter(brandsAdapterModel);
        autoTvInterCarBrand.setVisibility(View.GONE);
        autoTvExCarBrand.setVisibility(View.GONE);
        autoTvAddCarBrand.setVisibility(View.GONE);
        //  autoTvInterCarBrand.setAdapter(brandsAdapter);
        ArrayAdapter<String> modelAdapterOld = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, getCarBrandModelForOld());

        autoTvExCarModel.setAdapter(modelAdapterOld);
        autoTvAddCarModel.setAdapter(modelAdapterOld);

        autoTvInterCarBrand.setThreshold(0);
        autoTvExCarBrand.setThreshold(0);
        autoTvAddCarBrand.setThreshold(0);
        autoTvInterCarModel.setThreshold(0);
        autoTvInterCarVariant.setThreshold(0);
        autoTvInterCarFuelType.setThreshold(0);
        autoTvInterCarColor.setThreshold(0);
        autoTvExCarModel.setThreshold(0);
        autoTvAddCarModel.setThreshold(0);

        autoTvInterCarBrand.setOnFocusChangeListener(this);
        autoTvExCarBrand.setOnFocusChangeListener(this);
        autoTvAddCarBrand.setOnFocusChangeListener(this);
        autoTvInterCarModel.setOnFocusChangeListener(this);
        autoTvExCarModel.setOnFocusChangeListener(this);
        autoTvAddCarModel.setOnFocusChangeListener(this);
        autoTvInterCarVariant.setOnFocusChangeListener(this);
        autoTvInterCarFuelType.setOnFocusChangeListener(this);
        autoTvInterCarColor.setOnFocusChangeListener(this);

        autoTvInterCarBrand.setOnClickListener(this);
        autoTvExCarBrand.setOnClickListener(this);
        autoTvAddCarBrand.setOnClickListener(this);
        autoTvExCarModel.setOnClickListener(this);
        autoTvAddCarModel.setOnClickListener(this);

    }


    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);

    }


    private void showAlert(String alert) {
        Snackbar snackbar = Snackbar.make(view, alert, Snackbar.LENGTH_LONG)
                .setAction("Close", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    private AddLeadCarInputData getCarInputData() {
        System.out.println("ListExCar:Size" + listExCar.size());
        System.out.println("ListAddCar:Size" + listAddCar.size());
        return new AddLeadCarInputData("", listNewCar, listExCar, listAddCar);
    }


    public void addChildView(final int from, final Button bt, final ViewGroup parent, int layout, final LinearLayout spinnerHolder, final LinearLayout hr) {
        hr.setVisibility(View.VISIBLE);
        bt.setText("Add another car");
        spinnerHolder.setVisibility(View.GONE);
        LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v = vi.inflate(layout, null);
        TextView tvMain = (TextView) v.findViewById(R.id.tvLeadMain);
        final TextView tvSub = (TextView) v.findViewById(R.id.tvLeadSub);

        if (from == 0) {
            CreateLeadInputData.Lead_data.Car_details newCar = new CreateLeadInputData().new Lead_data().new Car_details();
            // System.out.println("Get List selection:"+autoTvInterCarModel.getListSelection());
            // System.out.println("Car model id:"+carModelList.get(interCarModelPosition).getModel_id()+"::mODEL name:"+carModelList.get(interCarModelPosition).getModel_name());
            newCar.setBrand_id(getCarBrandId(autoTvAddCarModel.getText().toString()));
            newCar.setBrand_name(autoTvInterCarBrand.getText().toString());
            newCar.setModel_id(carModelList.where().
                    equalTo("model_name",autoTvInterCarModel.getText().toString()).findFirst().getModel_id());
            newCar.setModel_name(autoTvInterCarModel.getText().toString());
            //  System.out.println("Variant"+carVariantList.get(interCarVariantPosition).getVariant_id()+"::Variant name:"+carVariantList.get(interCarVariantPosition).getVariant());

            if(carVariantList!=null&&carVariantList.where().equalTo("variant",autoTvInterCarVariant.getText().toString())
                    .findFirst()!=null){
                newCar.setVariant_id(carVariantList.where().equalTo("variant",autoTvInterCarVariant.getText().toString())
                        .findFirst().getVariant_id());
                newCar.setVariant_name(autoTvInterCarVariant.getText().toString());
            }
            if(getFuelTypeId()!=null){
                newCar.setFuel_type_id(getFuelTypeId());
                newCar.setFuel_name(autoTvInterCarFuelType.getText().toString());
            }
            if(colorList!=null&&colorList.where().equalTo("color",
                    autoTvInterCarColor.getText().toString()).findFirst()!=null){
                newCar.setColor_id(colorList.where().equalTo("color",
                        autoTvInterCarColor.getText().toString()).findFirst().getColor_id());
                newCar.setColor_name(autoTvInterCarColor.getText().toString());
            }

            //newCar.setNo_of_car(1);
            System.out.println("Car:Brand Id:"+newCar.getBrand_id());
            System.out.println("Car:Brand Name:"+newCar.getBrand_name());
            System.out.println("Car:Model Id:"+newCar.getModel_id());
            System.out.println("Car:Model Name:"+newCar.getModel_name());
            System.out.println("Car:Variant Id:"+newCar.getVariant_id());
            System.out.println("Car:Variant Name:"+newCar.getVariant_name());
            System.out.println("Car:Color Id:"+newCar.getColor_id());
            System.out.println("Car:Color Name:"+newCar.getColor_name());
            System.out.println("Car:Fuel Id:"+newCar.getFuel_type_id());
            System.out.println("Car:Fuel Name:"+newCar.getFuel_name());

            newCar.setIs_activity_target(false);
            if (interCarCount == 0) {
                newCar.setIs_primary(true);
            } else {
                newCar.setIs_primary(false);
            }

            System.out.println("Car:Count" + interCarCount);
            listNewCar.add(interCarCount, newCar);
            interCarCount++;
            if (TextUtils.isEmpty(autoTvInterCarVariant.getText())) {
                tvMain.setText(String.format(Locale.getDefault(), "%d. %s", parent.getChildCount() + 1, autoTvInterCarModel.getText().toString()));
            } else {
                tvMain.setText(String.format(Locale.getDefault(), "%d. %s", parent.getChildCount() + 1, autoTvInterCarVariant.getText().toString()));
            }
            if(TextUtils.isEmpty(autoTvInterCarFuelType.getText())){
                autoTvInterCarFuelType.setText("Unknown FuelType");
            }
            if(TextUtils.isEmpty(autoTvInterCarColor.getText())){
                autoTvInterCarColor.setText("Unknown Color");
            }
            tvSub.setText(String.format(Locale.getDefault(), "%s • %s", autoTvInterCarFuelType.getText().toString(), autoTvInterCarColor.getText().toString()));

        } else if (from == 1) {

            CreateLeadInputData.Lead_data.Ex_car_details ex_car_details = new CreateLeadInputData().new Lead_data().new Ex_car_details();
            ex_car_details.setBrand_id(getCarBrandId(autoTvExCarModel.getText().toString()));
            ex_car_details.setModel_id(carModelListOld.where().equalTo("model_name",
                    autoTvExCarModel.getText().toString()).findFirst().getModel_id());
            ex_car_details.setBrand_name(autoTvExCarBrand.getText().toString());
            ex_car_details.setModel_name(autoTvExCarModel.getText().toString());
            ex_car_details.setPurchase_date(getSqlDate(tvCarExYear));
            ex_car_details.setKms_run(etExRun.getText().toString());
            if (exCarCount == 0) {
                ex_car_details.setIs_primary(true);
            } else {
                ex_car_details.setIs_primary(false);
            }

            listExCar.add(exCarCount, ex_car_details);
            exCarCount++;
            tvMain.setText(String.format(Locale.getDefault(), "%d. %s", parent.getChildCount() + 1, autoTvExCarModel.getText().toString()));

            if(TextUtils.isEmpty(etExReg.getText())){
                etExReg.setText("Unknown Reg. No");
            }
            if(TextUtils.isEmpty(etExRun.getText())){
                etExRun.setText("Unknown Km");
            }
            tvSub.setText(String.format("%s • %s km", etExReg.getText().toString(), etExRun.getText().toString()));


        } else {

            CreateLeadInputData.Lead_data.Ad_car_details ad_car_details = new CreateLeadInputData().new Lead_data().new Ad_car_details();
            ad_car_details.setBrand_id(getCarBrandId(autoTvAddCarModel.getText().toString()));
            ad_car_details.setModel_id(carModelListOld.where().equalTo("model_name",
                    autoTvAddCarModel.getText().toString()).findFirst().getModel_id());
            ad_car_details.setBrand_name(autoTvAddCarBrand.getText().toString());
            ad_car_details.setModel_name(autoTvAddCarModel.getText().toString());

            ad_car_details.setPurchase_date(getSqlDate(tvCarAddYear));
            ad_car_details.setKms_run(etAddRun.getText().toString());
            if (addCarCount == 0) {
                ad_car_details.setIs_primary(true);
            } else {
                ad_car_details.setIs_primary(false);
            }
            listAddCar.add(addCarCount, ad_car_details);
            addCarCount++;
            tvMain.setText(String.format(Locale.getDefault(), "%d. %s", parent.getChildCount() + 1, autoTvAddCarModel.getText().toString()));

            if(TextUtils.isEmpty(etAddReg.getText())){
                etAddReg.setText("Unknown Reg. No");
            }
            if(TextUtils.isEmpty(etAddRun.getText())){
                etAddRun.setText("Unknown");
            }
            tvSub.setText(String.format("%s • %s km", etAddReg.getText().toString(), etAddRun.getText().toString()));
        }

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        parent.addView(v, lp);
        ImageButton btRemove = (ImageButton) v.findViewById(R.id.bt_remove_parent_lead);
        btRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.removeView((View) v.getParent());
                System.out.println(parent.getChildCount());
                if (parent.getChildCount() == 0) {
                    if (from == 0) {
                        autoTvInterCarModel.setText("");
                        autoTvInterCarVariant.setAdapter(null);
                        autoTvInterCarVariant.setText("");
                        autoTvInterCarColor.setAdapter(null);
                        autoTvInterCarColor.setText("");
                        autoTvInterCarFuelType.setTag(null);
                        autoTvInterCarFuelType.setTag(R.id.val,null);
                        autoTvInterCarFuelType.setText("");
                        llCarInterestedSpinnerHolder.setVisibility(View.VISIBLE);
                    } else if (from == 1) {
                        autoTvExCarBrand.setText("");
                        autoTvExCarModel.setText("");
                        //autoTvExCarModel.setAdapter(null);
                        tvCarExYear.setText("");
                        etExReg.setText("");
                        etExRun.setText("");
                    } else {
                        autoTvAddCarBrand.setText("");
                        autoTvAddCarModel.setText("");
                        //  autoTvAddCarModel.setAdapter(null);
                        tvCarAddYear.setText("");
                        etAddReg.setText("");
                        etAddRun.setText("");
                    }
                    hr.setVisibility(View.GONE);
                    spinnerHolder.setVisibility(View.VISIBLE);
                    bt.setText("Add");
                }

                if (from == 0) {
                    interCarCount--;
                    listNewCar.remove(interCarCount);
                } else if (from == 1) {
                    exCarCount--;
                    listExCar.remove(exCarCount);
                } else {
                    addCarCount--;
                    listAddCar.remove(addCarCount);
                }

            }
        });
    }

    private String getColorId() {
        if (autoTvInterCarColor.getTag() == null) {
            return null;

        } else {
            return autoTvInterCarColor.getTag().toString();
        }

    }

    private String getFuelTypeId() {
        if(autoTvInterCarFuelType.getTag(R.id.val)!=null&&
                autoTvInterCarFuelType.getText().toString().equalsIgnoreCase(autoTvInterCarFuelType.getTag(R.id.val).toString())){
            if (autoTvInterCarFuelType.getTag() == null) {
                return null;

            } else {
                return autoTvInterCarFuelType.getTag().toString();
            }
        }
        else {
            return null;
        }


    }


    @Override
    public void onAttach(Context context) {
        try {
            addLeadCommunication = (AddLeadCommunication) context;
        } catch (Exception e) {
            System.out.println("ClassCastException");
        }
        super.onAttach(context);
    }


    @Override
    public void onClick(View v) {
        if (v instanceof AutoCompleteTextView) {
            ((AutoCompleteTextView) v).showDropDown();
        }

        switch (v.getId()) {
            case R.id.spinner_lead_car_ex_year:
                showDatePicker(this, getActivity(), R.id.spinner_lead_car_ex_year, null, Calendar.getInstance());
                break;
            case R.id.spinner_lead_car_add_year:
                showDatePicker(this, getActivity(), R.id.spinner_lead_car_add_year, null, Calendar.getInstance());
                break;

        }

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus&& v instanceof AutoCompleteTextView && (((AutoCompleteTextView) v).getAdapter()!=null)) {
            ((AutoCompleteTextView) v).showDropDown();
        }
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);

        switch (Integer.parseInt(view.getTag())) {
            case R.id.spinner_lead_car_ex_year:
                tvCarExYear.setText(String.format(Locale.getDefault(), "%d %s %d", dayOfMonth, calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), year));
                tvCarExYear.setTag(R.id.autoninja_date, calendar);
                break;
            case R.id.spinner_lead_car_add_year:
                tvCarAddYear.setText(String.format(Locale.getDefault(), "%d %s %d", dayOfMonth, calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), year));
                tvCarAddYear.setTag(R.id.autoninja_date, calendar);
                break;
        }
    }

    private String getSqlDate(TextView tv) {
        if (tv.getTag(R.id.autoninja_date) != null) {
            return Util.getSQLDateTime(((Calendar) tv.getTag(R.id.autoninja_date)).getTime());
        } else {
            return "";
        }
    }

    private String[] getCarBrandModel() {
        carModelList = realm.where(CarBrandModelsDB.class)
                .equalTo("category", WSConstants.CAR_CATEGORY_BOTH)
                .findAll();

        List<String> data = new ArrayList<>();
        for (int i = 0; i < carModelList.size(); i++) {
            if(carModelList.get(i).getModel_name()!=null){
                data.add(carModelList.get(i).getModel_name());
            }
        }
        String[] arr = new String[data.size()];
        for(int i=0;i<data.size();i++){
            arr[i] = data.get(i);
        }
        return arr;
    }

    private String[] getCarBrandModelForOld() {
        carModelListOld = realm.where(ExchangeCarBrandModelsDB.class).equalTo("category",WSConstants.CAR_CATEGORY_BOTH).or()
                .equalTo("category",WSConstants.CAR_CATEGORY_OLD_CAR).findAll();
        List<String> data = new ArrayList<>();
        for (int i = 0; i < carModelListOld.size(); i++) {
            if(carModelListOld.get(i).getModel_name()!=null){
                data.add(carModelListOld.get(i).getModel_name());
            }

        }
        String[] arr = new String[data.size()];
        for(int i=0;i<data.size();i++){
            arr[i] = data.get(i);
        }
        return arr;
    }

   /* private String[] getCarBrandModelAdd() {
            carModelListAdd = carBrandList.where().getBrand_models();
            String[] arr = new String[carModelListAdd.size()];
            for (int i = 0; i < carModelListAdd.size(); i++) {
                arr[i] = carModelListAdd.get(i).getModel_name();
            }
            return arr;
    }*/


    private String[] getCarBrandModelVariants() {
        carVariantList = carModelList.where().equalTo("model_name",autoTvInterCarModel.getText().toString())
                .findFirst().getCar_variants().where()
                .distinct("variant_id");

        List<String> data = new ArrayList<>();


        for (int i = 0; i < carVariantList.size(); i++) {
            if(carVariantList.get(i).getVariant()!=null){
                data.add(carVariantList.get(i).getVariant());
            }

        }
        String[] arr = new String[data.size()];
        for(int i=0;i<data.size();i++){
            arr[i] = data.get(i);
        }

        return arr;
    }

    private String[] setColorAndFuelType() {
        colorList = carModelList.where().equalTo("model_name",autoTvInterCarModel.getText().toString())
                .findFirst().getCar_variants().where()
                .equalTo("variant",autoTvInterCarVariant.getText().toString()).findAll();
        CarBrandModelVariantsDB currentVariant = carVariantList.where().equalTo("variant",autoTvInterCarVariant.getText().toString()).findFirst();
        List<String> data = new ArrayList<>();
        for (int i = 0; i < colorList.size(); i++) {
            if(colorList.get(i).getColor()!=null){
                data.add(colorList.get(i).getColor());
            }

        }


        String[] arr = new String[data.size()];
        for(int i=0;i<data.size();i++){
            arr[i] = data.get(i);
        }
        //  autoTvInterCarFuelType.setText(carVariantList.where().equalTo("variant",autoTvInterCarVariant.getText().toString()).findFirst().getFuel_type());

        autoTvInterCarFuelType.setText(currentVariant.getFuel_type());
        autoTvInterCarFuelType.setTag(currentVariant.getFuel_type_id());
        autoTvInterCarFuelType.setTag(R.id.val,currentVariant.getFuel_type());
        return arr;
    }



    private int getCarBrandId(String text) {
        RealmResults<CarBrandsDB> realmData = realm.where(CarBrandsDB.class).findAll();
        for(int i=0;i<realmData.size();i++){
            for(int j=0;j<realmData.get(i).getBrand_models().size();j++){
                if(realmData.get(i).getBrand_models().get(j).getModel_name().equalsIgnoreCase(text)){
                    return  Util.getInt(realmData.get(i).getBrand_id());
                }
            }
        }
        return -1;
    }


}
