import React, { Component } from 'react'
import { View, Text, Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Alert} from 'react-native'

import styles from '../styles/styles'

export default class LiveEFItemCarModel extends Component {
  constructor(props) {
    super(props);
    this.state ={showImagePlaceHolder:true};
  }

  render() {


    console.log('showPercentage:'+this.props.showPercentage);
    even = this.props.index %2 == 0;
    if(this.props.showPercentage) {
      return (
      <View style={{flexDirection: 'row', height:60, backgroundColor:(even?'#0000003c':'#2e5e86'), alignItems:'center'}}>

      <View style={{flex:1.5, alignItems:'center'}} >

      <Text style={{ color:'#FFFF00', padding:4}} >{this.props.info.car}</Text>


      </View>


      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.rel.r}</Text>
        </View>
      </View>



      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.rel.e}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.rel.f_i}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.rel.f_o}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#7ed321'}} >-</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

          <Text style={{ color:'#7ed321'}} >-</Text>
      </View>


      </View>
    );
    }
    else {
    return(
      <View style={{flexDirection: 'row', height:60, backgroundColor:(even?'#0000003c':'#2e5e86'), alignItems:'center'}}>

      <View style={{flex:1.5, alignItems:'center'}} >

        <Text style={{ color:'#FFFF00',padding:4}} >{this.props.info.car}</Text>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.abs.r}</Text>
        </View>

      </View>




      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.abs.e}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.abs.f_i}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff'}} >{this.props.abs.f_o}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#7ed321'}} >{this.props.abs.p_b}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

          <Text style={{ color:'#7ed321'}} >{this.props.abs.l_e}</Text>
      </View>


      </View>
    );
  }
  }
}
