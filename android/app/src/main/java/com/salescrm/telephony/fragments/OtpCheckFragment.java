package com.salescrm.telephony.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.salescrm.telephony.R;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.receiver.IncomingOtpSms;
import com.salescrm.telephony.response.OtpRequestResponse;
import com.salescrm.telephony.response.OtpValidateResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by Ravindra P on 17-03-2016.
 */
public class OtpCheckFragment extends Fragment implements TextWatcher, Callback,IncomingOtpSms.IncomingOtpSmsListener {

    private EditText etOtp;
    private TextView resendOtpBtn;
    private Button btSendOtp;
    private Preferences pref = null;
    private String otp;
    private View rootView;
    private FrameLayout frameLoader;

    //Get an instance of SmsRetrieverClient, used to start listening for a matching
    // SMS message.
    SmsRetrieverClient client = null;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.otpcheck_fragment, container, false);


        smsRetrieverRegister();



        pref = Preferences.getInstance();
        pref.load(getActivity());
        frameLoader = (FrameLayout) rootView.findViewById(R.id.login_frame_loader);
        etOtp = (EditText) rootView.findViewById(R.id.etOtp);
        etOtp.addTextChangedListener(this);
        etOtp.requestFocus();
        resendOtpBtn = (TextView) rootView.findViewById(R.id.tvResendOtp);
        btSendOtp = (Button) rootView.findViewById(R.id.btSubmitOtp);

        IncomingOtpSms.getInstance().registerListener(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            otp = bundle.getString("otp");
           // etOtp.setText(otp);
        }

        btSendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etOtp.getText().toString().length() == WSConstants.MAX_OTP_LENGTH) {
                    validateOTP();
                } else {
                    showAlert("Otp should be " + WSConstants.MAX_OTP_LENGTH + " digit", "Close", false);
                }
            }
        });
        resendOtpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOtp();
            }
        });
        return rootView;
    }

    private void smsRetrieverRegister() {
        if(getContext()!=null) {
            client = SmsRetriever.getClient(getContext());

            // Starts SmsRetriever, which waits for ONE matching SMS message until timeout
            // (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
            // action SmsRetriever#SMS_RETRIEVED_ACTION.
            Task<Void> task = client.startSmsRetriever();
            // Listen for success/failure of the start Task. If in a background thread, this
            // can be made blocking using Tasks.await(task, [timeout]);
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    // Successfully started retriever, expect broadcast intent
                    // ...
                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    // Failed to start retriever, inspect Exception for more details
                    // ...
                }
            });
        }
    }


    private void validateOTP() {
        ConnectionDetectorService cd = new ConnectionDetectorService(getActivity());
        if (cd.isConnectingToInternet()) {
            frameLoader.setVisibility(View.VISIBLE);
            btSendOtp.setClickable(false);

            String fcmID = "";
            if(pref.getFcmId()!= null) {
                fcmID = pref.getFcmId();
            }else{
                fcmID = "null";
            }

            ApiUtil.GetRestApiForCommon().validateOtp(pref.getUserMobile(), fcmID, etOtp.getText().toString(),  pref.getDeviceId(),this);

        } else {
            Toast.makeText(getActivity(), "No internet", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        System.out.println("called change");
        if (charSequence.length() == WSConstants.MAX_OTP_LENGTH) {
            Util.HideKeyboard(getActivity());
            validateOTP();
        }
    }


    @Override
    public void afterTextChanged(Editable editable) {
        System.out.println("Called");
        if (editable.toString().length() == 6) {
            Util.HideKeyboard(getActivity());
            //SubmitOtp();
        }
    }


    @Override
    public void success(Object o, Response response) {
        btSendOtp.setClickable(true);
        List<Header> headerList = response.getHeaders();
        for (Header header : headerList) {
            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                ApiUtil.UpdateAccessToken(header.getValue());
            }
        }
        frameLoader.setVisibility(View.GONE);
        final OtpValidateResponse otpValidateResponse = (OtpValidateResponse) o;
        if (otpValidateResponse!=null && otpValidateResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) &&
                otpValidateResponse.getResult()!=null) {
            pref.setAccessToken(otpValidateResponse.getResult().getToken());
            SalesCRMApplication.getBus().post(otpValidateResponse);

        } else if(otpValidateResponse!=null && Util.isNotNull(otpValidateResponse.getMessage())) {
            showAlert(otpValidateResponse.getMessage(), "Retry", true);
        }
        else {
            showAlert("Error validating otp", "Retry", true);
        }

    }


    @Override
    public void failure(RetrofitError error) {
        error.printStackTrace();
        frameLoader.setVisibility(View.GONE);
        btSendOtp.setClickable(true);
        //SalesCRMApplication.getCleverTapAPI().event.push("Mob-Login failure");
        showAlert("Something went wrong ;-)", "Retry", true);
    }

    protected void resendOtp() {
        ConnectionDetectorService cd = new ConnectionDetectorService(getActivity());
        if (cd.isConnectingToInternet()) {
            resendOtpBtn.setClickable(false);
            frameLoader.setVisibility(View.VISIBLE);
            //System.out.println("LoginFragment:"Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));LoginFragment.dummyIMEI
            ApiUtil.GetRestApiForCommon().login(pref.getUserMobile(),pref.getDeviceId(), new Callback<OtpRequestResponse>() {
                @Override
                public void success(OtpRequestResponse loginOtpResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }
                    frameLoader.setVisibility(View.GONE);
                    resendOtpBtn.setClickable(true);
                    OtpRequestResponse loginResponse = (OtpRequestResponse) loginOtpResponse;
                    if (loginResponse!=null && loginResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) &&
                            loginResponse.getResult() != null && loginResponse.getResult().isSuccess()) {
                            showAlert("New otp sent", "Close", false);

                    } else if(loginResponse!=null && Util.isNotNull(loginOtpResponse.getMessage())) {
                        showAlert(loginResponse.getMessage(), "Close", false);
                    }
                    else {
                        showAlert("Error in sending new otp","Close", false);
                    }

                }

                @Override
                public void failure(RetrofitError error) {
                    resendOtpBtn.setClickable(true);
                    frameLoader.setVisibility(View.GONE);
                }
            });
        } else {
            showAlert("No internet", "Close", false);
        }

    }

    public void showAlert(String message, String action, final boolean actionRetry) {
        Snackbar snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG)
                .setAction(action, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (actionRetry) {
                            resendOtp();
                        }
                    }
                });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();

    }

    @Override
    public void onOtpSmsReceived(String sms) {
        if(!isDetached()) {
            System.out.println("otp received:" + sms);
            etOtp.setText(sms);
        }
    }
}

