package com.salescrm.telephony.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.salescrm.telephony.fragments.EtvbrGmSmFragment;
import com.salescrm.telephony.fragments.TodayTasksSummaryMainFragment;
import com.salescrm.telephony.fragments.retExcFin.RefGmSmFragment;

/**
 * Created by prateek on 14/7/17.
 */

public class TeamDashboradGMPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 1;
    private int SmId;
    private int isItEtvbr;

    public TeamDashboradGMPagerAdapter(FragmentManager fm, int sm_id, int isItEtvbr) {
        super(fm);
        this.SmId = sm_id;
        this.isItEtvbr = isItEtvbr;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){

            case 0:
                if(isItEtvbr == 0) {
                    return EtvbrGmSmFragment.newInstance(SmId, "ETVBR");
                }else {
                    return RefGmSmFragment.newInstance(SmId, "ETVBR");
                }
            case 1:
                return new TodayTasksSummaryMainFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }


}
