package com.salescrm.telephony.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.SaveTargetAdapter;
import com.salescrm.telephony.db.team_dashboard_gm_db.Absolute;
import com.salescrm.telephony.db.team_dashboard_gm_db.ResultEtvbr;
import com.salescrm.telephony.db.team_dashboard_gm_db.ResultSM;
import com.salescrm.telephony.db.team_dashboard_gm_db.TeamSM;
import com.salescrm.telephony.model.TargetSectionModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.SetTargetResponse;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.WSConstants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bannhi on 24/8/17.
 */

public class SaveTargetActivity extends AppCompatActivity {
    private SaveTargetAdapter targetsRecyclerAdapter;
    AlertDialog alertDialog;
    TextView retail_total_tv,exch_total_tv, fin_total_tv;
    RecyclerView recyclerView;
    // private SwipeRefreshLayout swipeRefreshLayout;
    private int lastTLId = 0;
    ImageView setTargetButton;
    private String startDateString = "";
    private String endDateString = "";
    private Preferences pref;

    private RelativeLayout rel_loading_frame;
    private ImageView ibCloseFilter;
    Realm realm;
    private RealmList<ResultEtvbr> branchManagerDBRealmList = new RealmList<>();
    private RealmList<ResultSM> salesManagerDBRealmList = new RealmList<>();
    private RealmList<TeamSM> teamLeadsDBRealmList = new RealmList<>();
    private RealmList<Absolute> dseDBRealmList = new RealmList<>();
    private RealmList<Absolute> dseDBFullRealmList = new RealmList<>();
    private int exchangeTotal,retailTotal,financeTotal = 0;
    String month = "";
    String location = "";
    String locationId = "0";
    int targetMonth = 0;
    private ArrayList<TargetSectionModel> targetSectionModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_edit_target_activity);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerViewEtvbr);
        retail_total_tv = (TextView)findViewById(R.id.retail_no) ;
        exch_total_tv = (TextView)findViewById(R.id.exch_no) ;
        fin_total_tv = (TextView)findViewById(R.id.fin_no) ;
        Bundle bundle = getIntent().getExtras();
        month = bundle.getString("month");
        location = bundle.getString("location");
        locationId = bundle.getString("locationID");
        Calendar calendar = Calendar.getInstance();
        String title = location+" - "+new SimpleDateFormat("MMM yyyy").format(calendar.getTime());
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_toolbar_filter);
        if (month.equalsIgnoreCase("current")) {
            toolbar.setTitle(title);
            targetMonth = 0;
        } else {
            calendar.add(Calendar.MONTH, 1);
            targetMonth = 1;
            title = "Set Target - "+ new SimpleDateFormat("MMM yyyy").format(calendar.getTime());
            toolbar.setTitle(title);
        }

        setSupportActionBar(toolbar);

        setTargetButton = (ImageView) findViewById(R.id.set_target_button);
        rel_loading_frame = (RelativeLayout) findViewById(R.id.rel_loading_frame);
        ibCloseFilter = (ImageView) findViewById(R.id.ibCloseFilter);

        ibCloseFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        pref = Preferences.getInstance();
        pref.load(this);
        realm = Realm.getDefaultInstance();
        branchManagerDBRealmList.clear();


        setTargetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveNewTargets();
            }
        });
        displayData();

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    private void saveNewTargets() {
        for(int i =0; i < SaveTargetAdapter.targetItemArrayList.size();i++){
            System.out.println("USER ID"+SaveTargetAdapter.targetItemArrayList.get(i).getUser_id()+
                    "RETAIL"+SaveTargetAdapter.targetItemArrayList.get(i).getRetail()+
                    "BOOKING"+SaveTargetAdapter.targetItemArrayList.get(i).getBookings()+
                    "ENQUIRY"+SaveTargetAdapter.targetItemArrayList.get(i).getEnquiries());
        }
        SaveTargetAdapter temp = new SaveTargetAdapter();
        SaveTargetAdapter.TargetArray targets = temp.new TargetArray();
        targets.setTargets(SaveTargetAdapter.targetItemArrayList);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).setTargets(targets, new Callback<SetTargetResponse>() {
            @Override
            public void success(SetTargetResponse setTargetResponse, Response response) {
                //pref.setTargetMonth(1);
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(setTargetResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(SaveTargetActivity.this,0);
                }

                if(setTargetResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)){
                    if (!month.isEmpty() && month.equalsIgnoreCase("current")) {
                        pref.setTargetMonth(0);
                    } else {
                        pref.setTargetMonth(1);
                    }

                   finish();
                }

            }


            @Override
            public void failure(RetrofitError error) {
              //  pref.setTargetMonth(1);
               // Util.showToast(getApplicationContext(), "Server not responding - "+ error.getMessage(), Toast.LENGTH_SHORT);
                finish();
            }
        });
        //call api

    }

    public void displayData() {
        boolean add = false;
        rel_loading_frame.setVisibility(View.GONE);
        RealmResults<ResultEtvbr> branchManagerDBRealmResults = realm.where(ResultEtvbr.class)
                .findAllSorted("userId", Sort.ASCENDING);

        if (branchManagerDBRealmResults != null) {
            for (int k = 0; k < branchManagerDBRealmResults.size(); k++) {
                salesManagerDBRealmList= branchManagerDBRealmResults.get(k).getSalesManager();
                for(int s = 0; s< salesManagerDBRealmList.size();s++){
                    teamLeadsDBRealmList =  salesManagerDBRealmList.get(s).getTeams();
                    for(int t = 0; t< teamLeadsDBRealmList.size();t++){
                        // if(teamLeadsDBRealmList.get(t).getTeamId()!=null && !teamLeadsDBRealmList.get(t).getTeamName().equalsIgnoreCase("total") ){
                       /* TargetSectionModel temp = new TargetSectionModel();
                        temp.setSectionPostion(dseDBFullRealmList.size());
                        temp.setTeamLeadName(teamLeadsDBRealmList.get(t).getTeamName());
                        targetSectionModelArrayList.add(temp);*/
                        int position = dseDBFullRealmList.size();
                        dseDBRealmList =  teamLeadsDBRealmList.get(t).getAbs();
                        for (int d = 0; d < dseDBRealmList.size(); d++) {
                            if (dseDBRealmList.get(d).getLocationId() == Integer.parseInt(locationId)){
                                add = true;
                                dseDBFullRealmList.add(dseDBRealmList.get(d));
                                if (dseDBRealmList.get(d).getBookings_target() != null) {
                                    exchangeTotal += dseDBRealmList.get(d).getExchange_target();
                                }
                                if (dseDBRealmList.get(d).getEnquiries_target() != null) {
                                    financeTotal += dseDBRealmList.get(d).getFinance_target();
                                }
                                if (dseDBRealmList.get(d).getRetail_target() != null) {
                                    retailTotal += dseDBRealmList.get(d).getRetail_target();
                                }
                            }

                        }
                        if(add) {
                            TargetSectionModel temp = new TargetSectionModel();
                            temp.setSectionPostion(position);
                            temp.setTeamLeadName(teamLeadsDBRealmList.get(t).getTeamName());
                            targetSectionModelArrayList.add(temp);
                            add = false;
                        }



                    }
                }
            }

        }
        exch_total_tv.setText(""+exchangeTotal);
        fin_total_tv.setText(""+financeTotal);
        retail_total_tv.setText(""+retailTotal);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        targetsRecyclerAdapter = new SaveTargetAdapter(this, dseDBFullRealmList,
                targetSectionModelArrayList, exch_total_tv, retail_total_tv, fin_total_tv, exchangeTotal, financeTotal, retailTotal, targetMonth);

        recyclerView.setAdapter(targetsRecyclerAdapter);
    }

}
