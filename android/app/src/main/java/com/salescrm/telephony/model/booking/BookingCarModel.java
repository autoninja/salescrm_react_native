package com.salescrm.telephony.model.booking;

import com.salescrm.telephony.model.Item;
import com.salescrm.telephony.utils.Util;

import java.util.List;

/**
 * Created by bharath on 22/2/18.
 */

public class BookingCarModel {

    private Integer brandId;
    private String brandName;

    private Integer modelId;
    private String modelName;

    private Integer variantId;
    private String variantName;

    private Integer fuelTypeId;
    private String fuelName;

    private Integer colorTypeId;
    private String colorName;

    private Integer quantity = 1; //Default 1
    private List<Item> modelList;
    private List<Item> variantList;
    private List<Item> colorList;
    private List<Item> fuelTypeList;

    private String vinNumber;
    private String invoiceMobileNumber;
    private String invoiceDate;

    private String bookingAmount;
    private String deliveryDate;
    private String deliveryChallan;


    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Integer getVariantId() {
        return variantId;
    }

    public void setVariantId(Integer variantId) {
        this.variantId = variantId;
    }

    public String getVariantName() {
        return variantName;
    }

    public void setVariantName(String variantName) {
        this.variantName = variantName;
    }

    public Integer getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(Integer fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public String getFuelName() {
        return fuelName;
    }

    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    public Integer getColorTypeId() {
        return colorTypeId;
    }

    public void setColorTypeId(Integer colorTypeId) {
        this.colorTypeId = colorTypeId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setModelList(List<Item> modelList) {
        this.modelList = modelList;
    }

    public List<Item> getModelList() {
        return modelList;
    }

    public void setVariantList(List<Item> variantList) {
        this.variantList = variantList;
    }

    public List<Item> getVariantList() {
        return variantList;
    }

    public void setColorList(List<Item> colorList) {
        this.colorList = colorList;
    }

    public List<Item> getColorList() {
        return colorList;
    }

    public void setFuelTypeList(List<Item> fuelTypeList) {
        this.fuelTypeList = fuelTypeList;
    }

    public List<Item> getFuelTypeList() {
        return fuelTypeList;
    }

    public String getBookingAmount() {
        return bookingAmount;
    }

    public void setBookingAmount(String bookingAmount) {
        this.bookingAmount = bookingAmount;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }

    public String getInvoiceMobileNumber() {
        return invoiceMobileNumber;
    }

    public void setInvoiceMobileNumber(String invoiceMobileNumber) {
        this.invoiceMobileNumber = invoiceMobileNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public String getDeliveryDateAsSql() {
        return Util.getSQLDateTime(deliveryDate);
    }

    public void setDeliveryChallan(String deliveryChallan) {
        this.deliveryChallan = deliveryChallan;
    }

    public String getDeliveryChallan() {
        return deliveryChallan;
    }

    public void copy(BookingCarModel bookingCarModel) {
        this.brandId = bookingCarModel.getBrandId();
        this.brandName = bookingCarModel.getBrandName();
        this.modelId = bookingCarModel.getModelId();
        this.modelName = bookingCarModel.getModelName();
        this.variantId = bookingCarModel.getVariantId();
        this.variantName = bookingCarModel.getVariantName();
        this.fuelTypeId = bookingCarModel.getFuelTypeId();
        this.fuelName = bookingCarModel.getFuelName();
        this.colorTypeId = bookingCarModel.getColorTypeId();
        this.colorName = bookingCarModel.getColorName();
        this.modelList = bookingCarModel.getModelList();
        this.variantList = bookingCarModel.getVariantList();
        this.colorList = bookingCarModel.getColorList();
        this.fuelTypeList = bookingCarModel.getFuelTypeList();
        this.deliveryDate = bookingCarModel.getDeliveryDate();
        this.deliveryChallan = bookingCarModel.getDeliveryChallan();
        this.bookingAmount = bookingCarModel.getBookingAmount();
        this.vinNumber = bookingCarModel.vinNumber;
        this.invoiceMobileNumber = bookingCarModel.getInvoiceMobileNumber();
        this.invoiceDate = bookingCarModel.getInvoiceDate();
    }


}
