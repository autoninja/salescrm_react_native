import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native'





export default class TabBarBottom extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (

        <View style={{marginTop:10}}>

            <View style={{ width: '100%', height: 0.3, backgroundColor: '#607D8B', marginTop: 15}}/>

            <Text  keyName='LeaderBoard' title = 'Leader Board' imageSource={require('../images/ic_leader_board.png')} navigationProps={ this.props.navigation }/>
       </View>
    );
  }
}
