package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 31/8/16.
 */
public class AllCarsDB extends RealmObject {
    @PrimaryKey
    private int id;
    private String name;
    private String category;

    public AllCarsDB() {
    }

    public AllCarsDB(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
