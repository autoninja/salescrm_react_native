package com.salescrm.telephony.sync;

import android.content.Context;
import android.widget.Toast;

import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.db.AddAndExchangeCarSyncDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.interfaces.SyncListener;
import com.salescrm.telephony.model.AddExchangeCarModel;
import com.salescrm.telephony.model.AddMultipleCarModel;
import com.salescrm.telephony.model.Cars;
import com.salescrm.telephony.model.CreateLeadcarIdData;
import com.salescrm.telephony.model.Model;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AddCarsNewResponse;
import com.salescrm.telephony.response.AddExchangeCarResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 9/5/17.
 */

public class SyncCar implements FetchC360OnCreateLeadListener {
    private final RealmResults<AddAndExchangeCarSyncDB> allSyncCarList;
//    private final ArrayList<String> leadIdList;
    private SyncListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private int leadIdCount=0;
    private String TAG = SyncCar.class.getSimpleName()+":";
    private String generatedLeadId="";

    public SyncCar(SyncListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        this.pref.load(context);
        /*leadIdList = new ArrayList<>();
        RealmResults<AddAndExchangeCarSyncDB> data = realm.where(AddAndExchangeCarSyncDB.class).equalTo("is_synced", false).distinct("leadId");
        for(int i=0;i<data.size();i++){
            leadIdList.add(data.get(i).getLead_id());
        }*/
        allSyncCarList = realm.where(AddAndExchangeCarSyncDB.class).equalTo("is_synced",false).findAll();

    }
    public void sync(){
        if(allSyncCarList.size()>0){
            submitCarDetails(allSyncCarList.get(0));
        }
        else {
            listener.onCarSync();
        }
    }

    private void submitCarDetails(final AddAndExchangeCarSyncDB carDb) {
        //onSuccess increase the leadListCount

        if (new ConnectionDetectorService(context).isConnectingToInternet()) {
            //Sync one by one
            if(carDb.getAddType()==0){
                CreateLeadcarIdData createcardata = new CreateLeadcarIdData();
                createcardata.setLead_id(carDb.getLeadId());
                CreateLeadcarIdData.LeadCarDetails carInputData= createcardata.new LeadCarDetails();
                carInputData.setCarColorId(carDb.getColorId());
                carInputData.setCarVariant(carDb.getVariantId());
                carInputData.setCarModel(carDb.getModelId());
                carInputData.setCarFuelType(carDb.getFuelTypeId());
                createcardata.setLead_car_details(carInputData);

                ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).Addnewcar(createcardata, new Callback<AddCarsNewResponse>() {
                    @Override
                    public void success(AddCarsNewResponse generalResponse, Response response) {
                        if (!generalResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                            System.out.println(TAG+"Success:0" + generalResponse.getMessage());
                            updateSyncStatus(carDb,false,generalResponse.getMessage());
                            //showAlert(validateOtpResponse.getMessage());
                        } else {
                            if (generalResponse.getResult()!=null) {
                                System.out.println(TAG+"Success:1" + generalResponse.getMessage());
                                updateSyncStatus(carDb,true,generalResponse.getMessage());
                            } else {
                                // relLoader.setVisibility(View.GONE);
                                updateSyncStatus(carDb,false,generalResponse.getMessage());
                                System.out.println(TAG+"Success:2" + generalResponse.getMessage());
                                //showAlert(validateOtpResponse.getMessage());
                            }
                        }
                        //sendPendingAddCarFromDb();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        updateSyncStatus(carDb,false,error.getResponse().getReason());

                    }
                });
            }
            else  {
                AddExchangeCarModel temp = new AddExchangeCarModel();
                ArrayList<Cars> cars = new ArrayList<>();
                Model model = new Model(carDb.getModelId());
                cars.add(new Cars(model, carDb.getKms_run(), carDb.getYear(), carDb.getReg_no()));
                temp.setLead_id(carDb.getLeadId());
                temp.setLead_last_updated(realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", Util.getInt(carDb.getLeadId()))
                        .findAllSorted("leadLastUpdated", Sort.DESCENDING).first().getLeadLastUpdated());
                temp.setCars(cars);
                ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).AddExchangeCar(temp, new Callback<AddExchangeCarResponse>() {
                    @Override
                    public void success(final AddExchangeCarResponse generalResponse, Response response) {
                        if (!generalResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                            System.out.println(TAG+"Success:0" + generalResponse.getMessage());
                            updateSyncStatus(carDb,false,generalResponse.getMessage());
                            //showAlert(validateOtpResponse.getMessage());
                        } else {
                            if (generalResponse.getResult()!=null) {
                                System.out.println(TAG+"Success:1" + generalResponse.getMessage());
                                updateSyncStatus(carDb,true,generalResponse.getMessage());
                            } else {
                                // relLoader.setVisibility(View.GONE);
                                updateSyncStatus(carDb,false,generalResponse.getMessage());
                                System.out.println(TAG+"Success:2" + generalResponse.getMessage());
                                //showAlert(validateOtpResponse.getMessage());
                            }
                        }
                        //sendPendingAddCarFromDb();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        updateSyncStatus(carDb,false,error.getResponse().getReason());

                    }
                });
            }


        /*
            AddMultipleCarModel addMultipleCarModel = new AddMultipleCarModel();
            addMultipleCarModel.setLead_id(leadIdList.get(leadIdCount));
            System.out.println(TAG+"leadId"+leadIdList.get(leadIdCount));
            System.out.println(TAG+"Lead last updated::"+realm.where(SalesCRMRealmTable.class)
                    .equalTo("leadId", Util.getInt(leadIdList.get(leadIdCount)))
                    .findAllSorted("leadLastUpdated", Sort.DESCENDING).first().getLead_last_updated());

            addMultipleCarModel.setLead_last_updated(realm.where(SalesCRMRealmTable.class)
                    .equalTo("leadId", Util.getInt(leadIdList.get(leadIdCount)))
                    .findAllSorted("leadLastUpdated", Sort.DESCENDING).first().getLead_last_updated());

            addMultipleCarModel.setInterestedCars(getInterestedCars(addAndExchangeCarSyncDBs));
            addMultipleCarModel.setExchangeCars(getExchangeCars(addAndExchangeCarSyncDBs));
            addMultipleCarModel.setAdditionalCars(null);
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).AddMultipleCar(addMultipleCarModel, new Callback<AddMultipleCarResponse>() {
                @Override
                public void success(AddMultipleCarResponse generalResponse, Response gener) {
                    if (!generalResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        System.out.println(TAG+"Success:0" + generalResponse.getMessage());
                        updateSyncStatus(addAndExchangeCarSyncDBs,false,generalResponse.getMessage());
                        //showAlert(validateOtpResponse.getMessage());
                    } else {
                        if (generalResponse.getResult()!=null) {
                            System.out.println(TAG+"Success:1" + generalResponse.getMessage());
                            updateSyncStatus(addAndExchangeCarSyncDBs,true,generalResponse.getMessage());
                        } else {
                            // relLoader.setVisibility(View.GONE);
                            updateSyncStatus(addAndExchangeCarSyncDBs,false,generalResponse.getMessage());
                            System.out.println(TAG+"Success:2" + generalResponse.getMessage());
                            //showAlert(validateOtpResponse.getMessage());
                        }
                    }

                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println(TAG+"Failure");
                    updateSyncStatus(addAndExchangeCarSyncDBs,false,error.getResponse().getReason());
                }
            });*/



        }
        else {
            listener.onCarSync();
        }




    }


    private void updateSyncStatus(final AddAndExchangeCarSyncDB addAndExchangeCarSyncDBs, final boolean b, String message) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                addAndExchangeCarSyncDBs.setResult(b);
                addAndExchangeCarSyncDBs.setIs_synced(true);

            }
        });
        fetchC360(addAndExchangeCarSyncDBs.getLeadId());
        Toast.makeText(context, "Car details synced for for Lead Id:"+addAndExchangeCarSyncDBs.getLeadId(), Toast.LENGTH_SHORT).show();

    }
    private void fetchC360(String leadId) {
        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(leadId);
        new FetchC360InfoOnCreateLead(this,context,leadData,pref.getAppUserId()).call(true);

    }
    @Override
    public void onFetchC360OnCreateLeadSuccess() {
        sync();

    }

    @Override
    public void onFetchC360OnCreateLeadError() {
        sync();
    }



    private List<AddMultipleCarModel.InterestedCars> getInterestedCars(RealmResults<AddAndExchangeCarSyncDB> addAndExchangeCarSyncDBs) {
        List<AddMultipleCarModel.InterestedCars> list = new ArrayList<>();
        for (AddAndExchangeCarSyncDB  carDb:addAndExchangeCarSyncDBs){
            if(carDb.getAddType()==0) {
                AddMultipleCarModel.InterestedCars interestedCars = new AddMultipleCarModel().new InterestedCars();
                interestedCars.setCarColorId(carDb.getColorId());
                interestedCars.setCarFuelType(carDb.getFuelTypeId());
                interestedCars.setCarModel(carDb.getModelId());
                interestedCars.setCarVariant(carDb.getVariantId());
                interestedCars.setColor(carDb.getColorName());
                interestedCars.setFuel_type(carDb.getFuelTypeName());
                interestedCars.setModel(carDb.getModelName());
                interestedCars.setVariant(carDb.getVariantName());
                list.add(interestedCars);
            }
        }
        return list;
    }

    private List<AddMultipleCarModel.OtherCars> getExchangeCars(RealmResults<AddAndExchangeCarSyncDB> addAndExchangeCarSyncDBs) {
        List<AddMultipleCarModel.OtherCars> list = new ArrayList<>();
        for (AddAndExchangeCarSyncDB  carDb:addAndExchangeCarSyncDBs) {
            if (carDb.getAddType() == 1) {
                AddMultipleCarModel.OtherCars exchangeCar = new AddMultipleCarModel().new OtherCars();

                AddMultipleCarModel.OtherCars.Model  model = exchangeCar.new Model();
                model.setCategory("");
                model.setId(carDb.getModelId());
                model.setCode("");
                model.setName(carDb.getModelName());

                exchangeCar.setKms_run(carDb.getKms_run());
                exchangeCar.setName(carDb.getModelName());
                exchangeCar.setPurchase_date(carDb.getYear());
                exchangeCar.setReg_no(carDb.getReg_no());

                exchangeCar.setModel(model);
                list.add(exchangeCar);
            }
        }
        return list;
    }







}

