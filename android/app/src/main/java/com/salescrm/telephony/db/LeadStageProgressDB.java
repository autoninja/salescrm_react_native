package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bharath on 13/1/17.
 */
public class LeadStageProgressDB extends RealmObject {
    private int stageId;

    private String name;
    private Integer width;
    private String barClass;

    public int getStageId() {
        return stageId;
    }

    public void setStageId(int stageId) {
        this.stageId = stageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public String getBarClass() {
        return barClass;
    }

    public void setBarClass(String barClass) {
        this.barClass = barClass;
    }
}
