package com.salescrm.telephony.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by prateek on 10/6/17.
 */

public class EtvbrModelPojo {
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("error")
    @Expose
    private List<Error> error = null;
    @SerializedName("teams")
    @Expose
    private List<Team> teams = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<Error> getError() {
        return error;
    }

    public void setError(List<Error> error) {
        this.error = error;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public class Error {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("details")
        @Expose
        private String details;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

    }

    public class Team {

        @SerializedName("teamid")
        @Expose
        private Integer teamid;
        @SerializedName("teamName")
        @Expose
        private Integer teamName;
        @SerializedName("tlName")
        @Expose
        private String tlName;
        @SerializedName("dateTo")
        @Expose
        private String dateTo;
        @SerializedName("dateFrom")
        @Expose
        private String dateFrom;
        @SerializedName("abs")
        @Expose
        private List<Ab> abs = null;
        @SerializedName("rel")
        @Expose
        private List<Rel> rel = null;

        public Integer getTeamid() {
            return teamid;
        }

        public void setTeamid(Integer teamid) {
            this.teamid = teamid;
        }

        public Integer getTeamName() {
            return teamName;
        }

        public void setTeamName(Integer teamName) {
            this.teamName = teamName;
        }

        public String getTlName() {
            return tlName;
        }

        public void setTlName(String tlName) {
            this.tlName = tlName;
        }

        public String getDateTo() {
            return dateTo;
        }

        public void setDateTo(String dateTo) {
            this.dateTo = dateTo;
        }

        public String getDateFrom() {
            return dateFrom;
        }

        public void setDateFrom(String dateFrom) {
            this.dateFrom = dateFrom;
        }

        public List<Ab> getAbs() {
            return abs;
        }

        public void setAbs(List<Ab> abs) {
            this.abs = abs;
        }

        public List<Rel> getRel() {
            return rel;
        }

        public void setRel(List<Rel> rel) {
            this.rel = rel;
        }

        public class Ab {

            @SerializedName("img")
            @Expose
            private String img;
            @SerializedName("userId")
            @Expose
            private Integer userId;
            @SerializedName("username")
            @Expose
            private String username;
            @SerializedName("enq")
            @Expose
            private Integer enq;
            @SerializedName("tdrive")
            @Expose
            private Integer tdrive;
            @SerializedName("booking")
            @Expose
            private Integer booking;
            @SerializedName("visit")
            @Expose
            private Integer visit;
            @SerializedName("retail")
            @Expose
            private Integer retail;

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public Integer getUserId() {
                return userId;
            }

            public void setUserId(Integer userId) {
                this.userId = userId;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public Integer getEnq() {
                return enq;
            }

            public void setEnq(Integer enq) {
                this.enq = enq;
            }

            public Integer getTdrive() {
                return tdrive;
            }

            public void setTdrive(Integer tdrive) {
                this.tdrive = tdrive;
            }

            public Integer getBooking() {
                return booking;
            }

            public void setBooking(Integer booking) {
                this.booking = booking;
            }

            public Integer getVisit() {
                return visit;
            }

            public void setVisit(Integer visit) {
                this.visit = visit;
            }

            public Integer getRetail() {
                return retail;
            }

            public void setRetail(Integer retail) {
                this.retail = retail;
            }

        }

        public class Rel {

            @SerializedName("img")
            @Expose
            private String img;
            @SerializedName("userId")
            @Expose
            private Integer userId;
            @SerializedName("username")
            @Expose
            private String username;
            @SerializedName("enq")
            @Expose
            private Integer enq;
            @SerializedName("tdrive")
            @Expose
            private Integer tdrive;
            @SerializedName("booking")
            @Expose
            private Integer booking;
            @SerializedName("visit")
            @Expose
            private Integer visit;
            @SerializedName("retail")
            @Expose
            private Integer retail;

            public String getImg() {
                return img;
            }

            public void setImg(String img) {
                this.img = img;
            }

            public Integer getUserId() {
                return userId;
            }

            public void setUserId(Integer userId) {
                this.userId = userId;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public Integer getEnq() {
                return enq;
            }

            public void setEnq(Integer enq) {
                this.enq = enq;
            }

            public Integer getTdrive() {
                return tdrive;
            }

            public void setTdrive(Integer tdrive) {
                this.tdrive = tdrive;
            }

            public Integer getBooking() {
                return booking;
            }

            public void setBooking(Integer booking) {
                this.booking = booking;
            }

            public Integer getVisit() {
                return visit;
            }

            public void setVisit(Integer visit) {
                this.visit = visit;
            }

            public Integer getRetail() {
                return retail;
            }

            public void setRetail(Integer retail) {
                this.retail = retail;
            }

        }

    }
}
