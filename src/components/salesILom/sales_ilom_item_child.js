import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, Alert } from 'react-native'

import CallStat from './call_stat'
export default class SalesILomItemChild extends Component {
  constructor(props) {
    super(props);
    this.state = { expand: false, showImagePlaceHolder: true };
  }

  render() {
    let imageSource;
    let placeHolder;
    let headerColor = '#2D4F8B';
    let paddingLeft = 0;
    let viewHeight = 60;
    let headerFontSize = 15;
    if (this.props.isUnderTl) {
      headerColor = '#808080';
      paddingLeft = 4;
      viewHeight = 50;
      headerFontSize = 13;
    }
    if (this.props.from_location) {
      imageSource = require('../../images/ic_location.png');
      placeHolder = null;

    }
    else {
      imageSource = { uri: this.props.info.dp_url ? this.props.info.dp_url : 'null' };
      placeHolder = <View style={{ position: 'absolute', display: (this.state.showImagePlaceHolder ? 'flex' : 'none'), width: 36, height: 36, borderRadius: 36 / 2, backgroundColor: '#FF8000', alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#fff' }}>{this.props.info.name.charAt(0).toUpperCase()}</Text></View>;;
    }



    if (this.props.showPercentage) {

      return (

        <View>
          <View style={{ marginTop: 4, marginBottom: 4, flexDirection: 'row', alignItems: 'center' }}>
            <Text style={{
              flex: 1, fontSize: headerFontSize, color: headerColor,
              paddingLeft
            }}>{this.props.info.name}</Text>
             <CallStat call_stats = {this.props.call_stats}/>
          </View>
          <View style={{ flexDirection: 'row', height: viewHeight, backgroundColor: '#fff', borderColor: '#e0e0e0', borderWidth: 0.5, alignItems: 'center' }}>


            <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
              {placeHolder}
              <TouchableOpacity
                style={{ position: 'absolute', }}
                onLongPress={() => {
                  this.props.onLongPress('user_info',
                    { info: { name: this.props.info.name, dp_url: this.props.info.dp_url } })
                }}>
                <Image
                  source={imageSource}
                  style={{ width: 36, height: 36, borderRadius: 36 / 2, }}
                />
              </TouchableOpacity>
            </View>






            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#2D4F8B' }} >{this.props.rel.callback}</Text>
              </View>


            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#2D4F8B' }} >{this.props.rel.prospect}</Text>
              </View>

            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#2D4F8B' }} >{this.props.rel.sale_confirmed}</Text>
              </View>


            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#2D4F8B' }} >{this.props.rel.il_done}</Text>
              </View>


            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
            }}>

              <Text style={{ color: 'red' }} >{this.props.rel.l}</Text>
            </View>


          </View>
        </View>
      );
    }
    else {

      return (

        <View>
          <View style={{ marginTop: 4, marginBottom: 4, flexDirection: 'row', alignItems: 'center' }}>
            <Text style={{
              flex: 1, fontSize: headerFontSize, color: headerColor,
              paddingLeft
            }}>{this.props.info.name}</Text>
             <CallStat call_stats = {this.props.call_stats}/>
          </View>
          <View style={{ flexDirection: 'row', height: viewHeight, backgroundColor: '#fff', borderColor: '#e0e0e0', borderWidth: 0.5, alignItems: 'center' }}>

            <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
              {placeHolder}
              <TouchableOpacity
                style={{ position: 'absolute', }}
                onLongPress={() => {
                  this.props.onLongPress('user_info',
                    { info: { name: this.props.info.name, dp_url: this.props.info.dp_url } })
                }}>
                <Image
                  source={imageSource}
                  style={{ width: 36, height: 36, borderRadius: 36 / 2, }}
                />
              </TouchableOpacity>
            </View>


            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Assigned', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                  <Text style={{ color: '#2D4F8B', textDecorationLine: 'underline' }} >{this.props.abs.assigned}</Text>
                </TouchableOpacity>
              </View>

            </View>




            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Call back', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>


                  <Text style={{ color: '#2D4F8B', textDecorationLine: 'underline' }} >{this.props.abs.callback}</Text>

                </TouchableOpacity>
              </View>


            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Prospect', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                  <Text style={{ color: '#2D4F8B', textDecorationLine: 'underline' }} >{this.props.abs.prospect}</Text>
                </TouchableOpacity>
              </View>

            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Sale Confirmed', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                  <Text style={{ color: '#2D4F8B', textDecorationLine: 'underline' }} >{this.props.abs.sale_confirmed}</Text>
                </TouchableOpacity>
              </View>

            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'IL Done', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                  <Text style={{ color: '#2D4F8B', textDecorationLine: 'underline' }} >{this.props.abs.il_done}</Text>
                </TouchableOpacity>
              </View>

            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
            }}>
              <TouchableOpacity
                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Lost / Not Eligible', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>


                <Text style={{ color: 'red', textDecorationLine: 'underline' }} >{this.props.abs.l}</Text>

              </TouchableOpacity>
            </View>


          </View>
        </View>
      );
    }
  }
}
