package com.salescrm.telephony.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.FilterActivityTypeFragment;
import com.salescrm.telephony.fragments.FilterBuyerTypeFragment;
import com.salescrm.telephony.fragments.FilterCarModelsFragment;
import com.salescrm.telephony.fragments.FilterDateFragment;
import com.salescrm.telephony.fragments.FilterEnquiryAgeFragment;
import com.salescrm.telephony.fragments.FilterEnquirySourceFragment;
import com.salescrm.telephony.fragments.FilterEnquiryStageFragment;
import com.salescrm.telephony.fragments.FilterTagsFragment;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.FilterDataChangeListener;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;

import java.util.HashMap;

/**
 * Created by Ravindra P on 30-05-2016.
 */
public class FiltersActivity extends AppCompatActivity implements FilterDataChangeListener, AutoDialogClickListener {

    private String[] mobileArray = {"Tasks Type", "Tags", "Vehicle Models", "Lead Source", "Ageing", "Lead Stage",
            "Buyer Type", "Date"};
    private String[] filterData = {"Tasks Type", "Tags", "Vehicle Models", "Lead Source", "Ageing", "Lead Stage",
            "Buyer Type", "Date"};
    private ListView listView;
    private int lastSelectedListItem = 0;
    private int colorActive, colorInactive;
    private CustomAdapter adapter;
    private TextView btClearAll, btApplyFilter;
    private FilterSelectionHolder filterSelectionHolder;
    private ImageButton ibCloseFilter;
    private int fragmentPosition = 0;
    private FilterSelectionHolder filterSelectionHolderPreviousData;
    private Preferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);
        pref = Preferences.getInstance();
        pref.load(this);
        filterSelectionHolderPreviousData = new FilterSelectionHolder();
        filterSelectionHolder = FilterSelectionHolder.getInstance(this);
        for (int i = 0; i < filterSelectionHolder.getAllMapData().size(); i++) {
            SparseArray<String> selectedValues = new SparseArray<>();
            for (int j = 0; j < filterSelectionHolder.getAllMapData().valueAt(i).size(); j++) {
                selectedValues.put(filterSelectionHolder.getAllMapData().valueAt(i).keyAt(j), filterSelectionHolder.getAllMapData().valueAt(i).valueAt(j));
            }
            filterSelectionHolderPreviousData.setMapDataNoCallBack(filterSelectionHolder.getAllMapData().keyAt(i),
                    selectedValues);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_toolbar_filter);
        setSupportActionBar(toolbar);
        listView = (ListView) findViewById(R.id.lvfilter_item_list);
        btClearAll = (TextView) findViewById(R.id.bt_filter_clear);
        btApplyFilter = (TextView) findViewById(R.id.bt_filter_apply);
        ibCloseFilter = (ImageButton) findViewById(R.id.ibCloseFilter);

        ibCloseFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btClearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!filterSelectionHolder.isEmpty()) {
                    clearFilter();
                }
            }
        });

        btApplyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyChange
                if (filterSelectionHolder.isEmpty()) {
                    clearFilter();
                } else {
                    logEvent();
                    FiltersActivity.this.finish();
                }
            }
        });
        adapter = new CustomAdapter(this, mobileArray);

        listView.setAdapter(adapter);


        colorActive = ContextCompat.getColor(getApplicationContext(), R.color.blue);
        colorInactive = ContextCompat.getColor(getApplicationContext(), R.color.filter_text_inactive_color);


        if (getIntent().getExtras() != null) {
            addFragments(getIntent().getExtras().getInt("fragmentPosition"));
            init(getIntent().getExtras().getInt("fragmentPosition"));
        } else {
            addFragments(0);
            init(0);
        }
        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                addFragments(position);

                for (int i = 0; i < parent.getChildCount(); i++) {
                    View row = parent.getChildAt(i);
                    FrameLayout frameLayout = (FrameLayout) row.findViewById(R.id.label_filter_background);
                    TextView listTitle = (TextView) row.findViewById(R.id.label);
                    listTitle.setTextColor(colorInactive);
                    listTitle.setTypeface(null, Typeface.NORMAL);
                    frameLayout.setBackgroundColor(getResources().getColor(R.color.filter_gray));
                }

                TextView listTitle = (TextView) view.findViewById(R.id.label);
                listTitle.setTextColor(colorActive);
                listTitle.setTypeface(null, Typeface.BOLD);
                view.setBackgroundColor(Color.WHITE);

                //   Toast.makeText(FiltersActivity.this, "You selected : " + position, Toast.LENGTH_SHORT).show();
                lastSelectedListItem = position;
            }
        });

    }

    private void logEvent() {
        StringBuilder filterSelectedCombination = new StringBuilder();
        for (int i = 0; i < filterSelectionHolder.getAllMapData().size(); i++) {
            int insideKey = filterSelectionHolder.getAllMapData().keyAt(i);

            filterSelectedCombination.append(Util.getFilterLogKey(insideKey));

            if (i != filterSelectionHolder.getAllMapData().size() - 1) {
                filterSelectedCombination.append(",");
            }


        }
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_FILTER_KEY_TYPE,
                filterSelectedCombination.toString());
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_FILTERS, hashMap);

    }

    private void clearFilter() {

        //ClearFilter Dialog
        new AutoDialog(this, new AutoDialogModel("Are you sure want to clear filter?", "Yes", "No")).show();


    }

    private void init(final int pos) {
        listView.post(new Runnable() {
            @Override
            public void run() {
                View row = listView.getChildAt(pos);
                TextView listTitle = (TextView) row.findViewById(R.id.label);
                listTitle.setTextColor(colorActive);
                listTitle.setTypeface(null, Typeface.BOLD);
                listView.getChildAt(pos).setBackgroundColor(Color.WHITE);
                lastSelectedListItem = pos;
            }
        });
        changeFilterListSelected();
    }

    void addFragments(int position) {
        fragmentPosition = position;
        android.support.v4.app.Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new FilterActivityTypeFragment();
                break;
            case 1:
                fragment = new FilterTagsFragment();
                break;
            case 2:
                fragment = new FilterCarModelsFragment();
                break;
            case 3:
                fragment = new FilterEnquirySourceFragment();
                break;
            case 4:
                fragment = new FilterEnquiryAgeFragment();
                break;
            case 5:
                fragment = new FilterEnquiryStageFragment();
                break;
            case 6:
                fragment = new FilterBuyerTypeFragment();
                break;
            case 7:
                fragment = new FilterDateFragment();
                break;
            default:
                break;

        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_filter_value_container, fragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (!getIntent().getExtras().getBoolean(WSConstants.IS_FILTER_ALREADY_APPLIED)) {
            filterSelectionHolder.clearAll();
        } else {
            filterSelectionHolder.setAllMapData(filterSelectionHolderPreviousData.getAllMapData());
        }
        super.onBackPressed();

    }

    @Override
    public void onFilterDataChanged() {
        changeFilterListSelected();
    }

    private void changeFilterListSelected() {
        System.arraycopy(filterData, 0, mobileArray, 0, filterData.length);
        for (int i = 0; i < filterSelectionHolder.getAllMapData().size(); i++) {
            //For number use below { Ex: (1) }
            // mobileArray[filterSelectionHolder.getAllMapData().keyAt(i)] = filterData[filterSelectionHolder.getAllMapData().keyAt(i)] + " (<font color='#2196f3'>"+filterSelectionHolder.getMapData(filterSelectionHolder.getAllMapData().keyAt(i)).size()+"</font>)";
            mobileArray[filterSelectionHolder.getAllMapData().keyAt(i)] = filterData[filterSelectionHolder.getAllMapData().keyAt(i)] + "<font color='#2196f3'> •</font>";
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
        filterSelectionHolder.clearAll();
        FiltersActivity.this.finish();
    }

    @Override
    public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {

    }
}

class CustomAdapter extends ArrayAdapter<String> {

    Context context;
    private String[] resource;

    CustomAdapter(Context context, String[] resource) {
        super(context, R.layout.filters_activity_list_item_view, resource);
        this.context = context;
        this.resource = resource;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.filters_activity_list_item_view, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.label);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textView.setText(Html.fromHtml(resource[position]));
        return convertView;
    }

    static class ViewHolder {
        TextView textView;
    }


}
