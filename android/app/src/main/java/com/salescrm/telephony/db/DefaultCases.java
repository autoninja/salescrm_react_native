package com.salescrm.telephony.db;

import android.content.Intent;

import io.realm.RealmObject;

/**
 * Created by bharath on 2/2/17.
 */
public class DefaultCases extends RealmObject{
    private String id;
    private String dependent_question_answer_id;
    private Integer dependent_question_id;
    private String possible_answer_id;
    private Integer sequence;
    private Integer target_question_id;
    private Integer is_edit;

    public Integer getIs_edit() {
        return is_edit;
    }

    public void setIs_edit(Integer is_edit) {
        this.is_edit = is_edit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDependent_question_answer_id() {
        return dependent_question_answer_id;
    }

    public void setDependent_question_answer_id(String dependent_question_answer_id) {
        this.dependent_question_answer_id = dependent_question_answer_id;
    }

    public Integer getDependent_question_id() {
        return dependent_question_id;
    }

    public void setDependent_question_id(Integer dependent_question_id) {
        this.dependent_question_id = dependent_question_id;
    }

    public String getPossible_answer_id() {
        return possible_answer_id;
    }

    public void setPossible_answer_id(String possible_answer_id) {
        this.possible_answer_id = possible_answer_id;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getTarget_question_id() {
        return target_question_id;
    }

    public void setTarget_question_id(Integer target_question_id) {
        this.target_question_id = target_question_id;
    }
}
