package com.salescrm.telephony.fragments.EnquiryList;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.CommonEnquiryListAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.db.EnquiryListStageTotalDb;
import com.salescrm.telephony.interfaces.OnLoadLeadListListener;
import com.salescrm.telephony.model.AllLeadEnquiryListResponse;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by prateek on 16/10/17.
 */

public class InvoicedStageEnquiryListFragment extends Fragment implements Callback{

    static FrameLayout frame_layout;
    static FragmentManager fm;
    static FragmentTransaction ft;
    private static RecyclerView mRecyclerView;
    private static RelativeLayout progressRelativeLayout;
    CommonEnquiryListAdapter enquiryListAdapter;
    View dimLayer;
    CoordinatorLayout coordinatorLayout;
    // Store instance variables
    private String title;
    private int page;
    //RealmList<LeadListDetails> leadListDetails = new RealmList<>();
    Realm realm;
    TextView tvNoLeadMsg;
    private List<AllLeadEnquiryListResponse.LeadListDetails> leadListDetails;
    private Preferences pref;
    private List<AllLeadEnquiryListResponse.LeadListDetails> leadList2;
    private int startPage = 0, endPage = 50;
    private FilterSelectionHolder filterSelectionHolder;
    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RelativeLayout relLoading;
    protected FragmentActivity mActivity;
    private boolean API_ALREADY_CALLED = false;
    // newInstance constructor for creating fragment with arguments
    public static InvoicedStageEnquiryListFragment newInstance(int page, String title) {
        InvoicedStageEnquiryListFragment fragmentFirst = new InvoicedStageEnquiryListFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (FragmentActivity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
        if(API_ALREADY_CALLED == false) {
            setupAdapterWithoutFilter();
        }
    }

    private void setupAdapterWithoutFilter() {
        if(new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            progressBar.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            mSwipeRefreshLayout.setVisibility(View.GONE);
            API_ALREADY_CALLED = true;
            makeFirstApiCall();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SalesCRMApplication.getBus().unregister(this);
        realm.close();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_base_enquiry_list, container, false);
        realm = Realm.getDefaultInstance();
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.progressRecycler);
        fm = getFragmentManager();
        pref = Preferences.getInstance();
        filterSelectionHolder = FilterSelectionHolder.getInstance();
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        relLoading = (RelativeLayout) rootView.findViewById(R.id.rel_loading_frame);
        leadListDetails = new ArrayList<>();

        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        tvNoLeadMsg = (TextView) rootView.findViewById(R.id.tv_no_lead);
        leadList2 = new ArrayList<>();
        relLoading.setVisibility(View.VISIBLE);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                if(new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
                    relLoading.setVisibility(View.VISIBLE);
                    getAllLeadList();
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        return rootView;
    }

    private void getAllLeadList() {
        makeFirstApiCall();
    }

    private void makeFirstApiCall() {
        ArrayList<String> leadData1 = new ArrayList<>();
        leadData1.clear();
        leadData1.add("8");
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getAllLeadListWithFilter(null, leadData1, null, WSConstants.ENQUIRY_LIST_LOCATION_ID,
                null, "0", "50", "0", Util.createJsonObject(filterSelectionHolder, realm),  new Callback<AllLeadEnquiryListResponse>() {
            @Override
            public void success(AllLeadEnquiryListResponse allLeadEnquiryListResponse, Response response) {
                progressBar.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                relLoading.setVisibility(View.GONE);
                API_ALREADY_CALLED = false;
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(allLeadEnquiryListResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }

                if (allLeadEnquiryListResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    if(allLeadEnquiryListResponse.getResult() != null && allLeadEnquiryListResponse.getResult().getLeadDetails() != null) {
                        if(allLeadEnquiryListResponse.getResult().getCount() != null){
                            //SalesCRMApplication.getBus().post(new LeadCountSpreader(4, "Invoiced", Integer.parseInt(allLeadEnquiryListResponse.getResult().getCount())));
                        }
                        populateView(allLeadEnquiryListResponse.getResult());
                    }

                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressBar.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                relLoading.setVisibility(View.GONE);
            }
        });
    }

    private void populateView(AllLeadEnquiryListResponse.Result result) {
        if(leadListDetails!= null){
            leadListDetails.clear();
        }
        leadListDetails.addAll(result.getLeadDetails());
        if(leadListDetails.size() == 0){
            tvNoLeadMsg.setText("No enquiry on this stage:)");
            tvNoLeadMsg.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            mSwipeRefreshLayout.setVisibility(View.GONE);
        }else{
            tvNoLeadMsg.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        }
        enquiryListAdapter = new CommonEnquiryListAdapter(leadListDetails, mActivity, mRecyclerView, realm, CleverTapConstants.EVENT_MY_ENQUIRES_TAB_INVOICED);
        mRecyclerView.setAdapter(enquiryListAdapter);

        startPage =0;
        endPage = 50;
        int totalCount = realm.where(EnquiryListStageTotalDb.class).findFirst()!= null ? realm.where(EnquiryListStageTotalDb.class).findFirst().getInvoiced() : 0;
        if(totalCount >= 50) {
            enquiryListAdapter.setOnLoadMoreListener(new OnLoadLeadListListener() {
                @Override
                public void onLoadMoreLeads() {

                    WSConstants.RECYCLER_STATUS = false;
                    startPage = endPage;
                    endPage = startPage + 50;
                    System.out.println("My Data:- " + startPage + "*" + endPage);
                    ArrayList<String> leadData = new ArrayList<>();
                    leadData.clear();
                    leadData.add("8");
                    ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getAllLeadListWithFilter(null, leadData, null, WSConstants.ENQUIRY_LIST_LOCATION_ID,
                            null, startPage + "", endPage + "", "0",
                            Util.createJsonObject(filterSelectionHolder, realm), InvoicedStageEnquiryListFragment.this);
                }
            });
        }else {
            WSConstants.RECYCLER_STATUS = true;
        }
    }

    @Override
    public void success(Object o, Response response) {
        AllLeadEnquiryListResponse allLeadEnquiryListResponse = (AllLeadEnquiryListResponse) o;
        List<Header> headerList = response.getHeaders();
        for (Header header : headerList) {
            if (header.getName().equalsIgnoreCase("Authorization")) {
                ApiUtil.UpdateAccessToken(header.getValue());
            }
        }

        if(allLeadEnquiryListResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
            ApiUtil.InvalidUserLogout(getActivity(),0);
        }

        if (allLeadEnquiryListResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            if (allLeadEnquiryListResponse.getResult() != null && allLeadEnquiryListResponse.getResult().getLeadDetails() != null) {
                leadList2.clear();
                //SalesCRMApplication.getBus().post(new LeadCountSpreader(4, "Invoiced", Integer.parseInt(allLeadEnquiryListResponse.getResult().getCount())));
                if(allLeadEnquiryListResponse.getResult().getLeadDetails().size()== 50){
                    leadList2.addAll(allLeadEnquiryListResponse.getResult().getLeadDetails());
                    enquiryListAdapter.setLoding();
                }else{
                    leadList2.addAll(allLeadEnquiryListResponse.getResult().getLeadDetails());
                    WSConstants.RECYCLER_STATUS = true;
                }
                leadListDetails.addAll(leadList2);
                enquiryListAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void failure(RetrofitError error) {

    }
}
