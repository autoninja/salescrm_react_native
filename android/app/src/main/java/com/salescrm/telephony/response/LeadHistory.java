package com.salescrm.telephony.response;

import com.salescrm.telephony.model.FormSubmissionInputDataServer;

import java.util.List;

/**
 * Created by prateek on 4/10/16.
 */
public class LeadHistory {

    private String statusCode;

    private String message;

    private List<Result> result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public List<Result> getResult ()
    {
        return result;
    }

    public void setResult (List<Result> result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    public class Result
    {
        private String remarks_edited;

        private String icon_type;

        private List<AllHistoryObject> allHistoryObject;

        private String logged_time;

        private String title;

        private String edit_flag;

        private String remarks;

        private String log_type;

        private String log_id;

        private LocationData location_data;

        public String getLogged_time() {
            return logged_time;
        }

        public void setLogged_time(String logged_time) {
            this.logged_time = logged_time;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getRemarks_edited ()
        {
            return remarks_edited;
        }

        public void setRemarks_edited (String remarks_edited)
        {
            this.remarks_edited = remarks_edited;
        }

        public String getIcon_type ()
        {
            return icon_type;
        }

        public void setIcon_type (String icon_type)
        {
            this.icon_type = icon_type;
        }

        public List<AllHistoryObject> getAllHistoryObject ()
        {
            return allHistoryObject;
        }

        public void setAllHistoryObject(List<AllHistoryObject> allHistoryObject)
        {
            this.allHistoryObject = allHistoryObject;
        }

        public String getEdit_flag ()
        {
            return edit_flag;
        }

        public void setEdit_flag (String edit_flag)
        {
            this.edit_flag = edit_flag;
        }

        public String getRemarks ()
        {
            return remarks;
        }

        public void setRemarks (String remarks)
        {
            this.remarks = remarks;
        }

        public String getLog_type ()
        {
            return log_type;
        }

        public void setLog_type (String log_type)
        {
            this.log_type = log_type;
        }

        public String getLog_id ()
        {
            return log_id;
        }

        public void setLog_id (String log_id)
        {
            this.log_id = log_id;
        }

        public LocationData getLocation_data() {
            return location_data;
        }

        public void setLocation_data(LocationData location_data) {
            this.location_data = location_data;
        }

        public class AllHistoryObject
        {
            private String value;

            private String key;

            public String getValue ()
            {
                return value;
            }

            public void setValue (String value)
            {
                this.value = value;
            }

            public String getKey ()
            {
                return key;
            }

            public void setKey (String key)
            {
                this.key = key;
            }
        }
        public class LocationData {
            private String locality;
            private String latitude;
            private String longitude;

            public String getLocality() {
                return locality;
            }

            public void setLocality(String locality) {
                this.locality = locality;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }
        }
    }
}
