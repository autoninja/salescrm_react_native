import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, NativeModules, TouchableWithoutFeedback, KeyboardAvoidingView, ScrollView, TextInput, BackHandler, Modal
} from 'react-native'
import C360CancelRescheduleActivity from './c360_activity_cancel_reschedule';

const styles = StyleSheet.create({
    title: {
        color: '#ffffff',
        textAlign: 'center',
        fontSize: 14,
        alignSelf: 'center',
    },
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        padding: 20

    },
    button_view: {
        width: 250,
        backgroundColor: '#2196f3',
        padding: 5,
        alignSelf: 'center',
        borderColor: '#2196f3',
        margin: 15
    }
});
export default class C360MarkExchangeCarActivityOption extends Component {
    constructor(props) {
        super(props);
        this.state = {
            exchangeCarDetail: this.props.exchangeCarDetail,
            isInnerModalVisible: false,
            viewId: '',
        }
    }

    render() {

        let selectedOptions = this.props.exchangeCarDetail;


        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled={Platform.OS === 'ios'}>
                <View style={{ flex: 1, alignItems: 'center' }}>

                    <Modal
                        visible={this.props.isMarkActivityOptionOpen}
                        animationType={'slide'}
                        transparent={true}
                        onRequestClose={() => this.props.closeMarkExchangeCarActivityOption()}
                    >

                        <View style={{ flex: 1, backgroundColor: '#2e3286', alignItems: 'center', padding: 10 }}>
                            <TouchableOpacity style={{ marginRight: 20, marginTop: 20, alignSelf: 'flex-end' }} onPress={() => this.props.closeMarkExchangeCarActivityOption()}>
                                <Image
                                    style={{ width: 20, height: 20, }}
                                    source={require('../../images/ic_close_white.png')} />
                            </TouchableOpacity>
                            <View style={{ flex: 1, justifyContent: 'center', padding: 10, alignSelf: 'center' }}>
                                <TouchableOpacity style={styles.button_view} onPress={() => this.performEvaluationDone()}>
                                    <Text style={styles.title}>{(selectedOptions && selectedOptions.activityDetails.activity == 10) ? "EVALUATION DONE" : "PRICE QUOTED"}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.button_view} onPress={() => this.openFurtherFolloUp()}>
                                    <Text style={styles.title}>FURTHER FOLLOWUP NEEDED</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.button_view} onPress={() => this.openCancelEvaluation()}>
                                    <Text style={styles.title}>{(selectedOptions && selectedOptions.activityDetails.activity == 10) ? "CANCEL EVALUATION" : "CUSTOMER DENIED"}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <C360CancelRescheduleActivity
                            isInnerModalVisible={this.state.isInnerModalVisible}
                            viewId={this.state.viewId}
                            onCloseInnerModal={this.onCloseInnerModal}
                            performCancelOrReschedule={this.performCancelOrReschedule}
                        />

                    </Modal>
                </View>
            </KeyboardAvoidingView>);
    }

    performEvaluationDone() {
        this.props.markExchangeCarActivity()
    }

    openFurtherFolloUp() {
        this.setState({ viewId: 1, isInnerModalVisible: true })
    }

    openCancelEvaluation() {
        this.setState({ viewId: 2, isInnerModalVisible: true })
    }

    onCloseInnerModal = () => {
        this.setState({ isInnerModalVisible: false })
    }

    performCancelOrReschedule = (data, viewId) => {
        this.setState({ isInnerModalVisible: false })
        this.props.cancelOrRescheduleActivity(data, viewId)
    }
}