import React, { Component } from 'react'
import { View, Text } from 'react-native'
export default class CallStat extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let call_duration = 0;
        let calls_attempted = 0;
        let calls_contacted = 0;
        if(!this.props.call_stats) {
            return(
                <View/>
            )
        }
        else {
            call_duration = this.props.call_stats.call_duration
            calls_attempted = this.props.call_stats.calls_attempted?this.props.call_stats.calls_attempted:0
            calls_contacted = this.props.call_stats.calls_contacted?this.props.call_stats.calls_contacted:0
        }
        let minTag = "0 Min";
        if(call_duration) {
            if(call_duration == 60) {
                minTag = "1 Hr";
            }
            else if(call_duration>60) {
                minTag = parseFloat((call_duration/60).toFixed(2))+" Hrs";
            }
            else if(call_duration<=1){
                minTag = parseFloat(call_duration.toFixed(2)) + " Min";
            }
            else {
                minTag = parseFloat(call_duration.toFixed(2)) + " Mins";
            }
        }
        return(
            <View  style={{flexDirection:'row', alignItems:'center', justifyContent:'center',padding:3, }}>
                <Text style={{fontSize:12, color:'#007FFF',}}>{calls_contacted}/{calls_attempted} Contacted - {minTag}</Text>
            </View>
        )
    }
}