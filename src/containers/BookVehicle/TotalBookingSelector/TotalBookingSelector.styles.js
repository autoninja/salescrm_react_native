import React from "react";
import { StyleSheet } from "react-native";
export default styles = StyleSheet.create({
    linearGradient: {
        padding: 20,
        paddingBottom: 24,
        borderRadius: 10,
    },
    main: {
        backgroundColor: '#000000',
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 80,
        paddingBottom: 80,
        flex: 1,
        justifyContent: 'center'

    },
    header: {
        flexDirection: "row",
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    title: {
        flex: 1,
        padding: 6,
        color: "#eeeeee",
        fontSize: 18,
        textAlign: 'center',
    },
    closeButton: {
        position: "absolute",
        right: 8,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
        height: 24,
        width: 24,
        borderRadius: 14
    },
    vehicleCountButton: {
        backgroundColor: '#007fff',
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
        height: 20,
        width: 20,
        borderRadius: 20
    },
    line: {
        backgroundColor: "#303030",
    },
    list: {
        borderRadius: 10,
        marginTop: 20,
        marginStart: 10,
        marginEnd: 10,
        marginBottom: 10,
        backgroundColor: 'rgba(255,255,255,0.3)'
    },
    mainAction: {
        flexDirection: 'row',
        borderRadius: 8,
        padding: 20, flex: 1,
        backgroundColor: 'rgba(255,255,255,0.3)',
        justifyContent: 'center',
        margin: 2,
        alignItems: 'center'
    },
    alert: {
        position: "absolute",
        textAlign: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.6)',
        color: '#FF0000',
        padding: 6,
        paddingLeft: 10,
        paddingRight: 10,
        bottom: 40,
        fontSize: 15,
        alignSelf: 'center',
        borderRadius: 6,
        minWidth: 120
    },

});
