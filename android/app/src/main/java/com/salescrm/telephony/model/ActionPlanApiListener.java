package com.salescrm.telephony.model;

/**
 * Created by bharath on 23/8/16.
 */
public class ActionPlanApiListener {
    private int from;

    public ActionPlanApiListener(int from) {
        this.from = from;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }
}
