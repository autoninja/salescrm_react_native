package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by prateek on 6/12/16.
 */

public class SMSResult extends RealmObject{

    private String content;

    private String id;

    private String notification_cat_id;

    private String is_deleted;

    private String updated_at;

    private String is_active;

    private String name;

    private String created_at;

    private String is_manual;

    private String recipient_type;

    private String notification_type_id;

    private String recipient_type_id;

    public String getContent ()
    {
        return content;
    }

    public void setContent (String content)
    {
        this.content = content;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getNotification_cat_id ()
    {
        return notification_cat_id;
    }

    public void setNotification_cat_id (String notification_cat_id)
    {
        this.notification_cat_id = notification_cat_id;
    }

    public String getIs_deleted ()
    {
        return is_deleted;
    }

    public void setIs_deleted (String is_deleted)
    {
        this.is_deleted = is_deleted;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getIs_active ()
    {
        return is_active;
    }

    public void setIs_active (String is_active)
    {
        this.is_active = is_active;
    }


    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getIs_manual ()
    {
        return is_manual;
    }

    public void setIs_manual (String is_manual)
    {
        this.is_manual = is_manual;
    }

    public String getRecipient_type ()
    {
        return recipient_type;
    }

    public void setRecipient_type (String recipient_type)
    {
        this.recipient_type = recipient_type;
    }

    public String getNotification_type_id ()
    {
        return notification_type_id;
    }

    public void setNotification_type_id (String notification_type_id)
    {
        this.notification_type_id = notification_type_id;
    }

    public String getRecipient_type_id() {
        return recipient_type_id;
    }

    public void setRecipient_type_id(String recipient_type_id) {
        this.recipient_type_id = recipient_type_id;
    }
}
