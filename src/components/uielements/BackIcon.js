import React, { Component } from 'react';
import { View, Image, TouchableOpacity, NativeModules, Platform } from 'react-native';
import { NavigationActions } from 'react-navigation'
import NavigationService from '../../navigation/NavigationService'


class BackIcon extends Component {

    popBackStack = () => {
        if (this.props.isFromTask) {
            this.props.navigationProps.goBack(null);
        }
        else if (Platform.OS == "ios" && this.props.navigationProps.dangerouslyGetParent().state.index <= 0) {
            NavigationService.navigate('Home');
        }
        else {
            this.props.navigationProps.goBack(null);
        }

        // this.props.navigationProps.dispatch(NavigationActions.back())
    };

    render() {
        let image;
        if (this.props.invert) {
            image = <Image
                source={require('../../images/ic_back_black.png')}
                style={{ width: 24, height: 24, marginLeft: 20 }}
            />
        }
        else {
            image = <Image
                source={require('../../images/ic_back_white.png')}
                style={{ width: 24, height: 24, marginLeft: 20 }}
            />
        }
        return (
            <View style={{ flexDirection: 'row' }}>

                <TouchableOpacity
                    onPress={this.popBackStack.bind(this)}
                    hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                >
                    {image}
                </TouchableOpacity>

            </View>
        );
    }

}

export default BackIcon;
