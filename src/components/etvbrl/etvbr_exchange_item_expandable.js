import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'


//import ETVBRItemChild from './etvbr_item_child'
import ETVBRExchangeItemChild from './etvbr_exchange_item_child';

export default class ETVBRItemExpandable extends Component {
    constructor(props) {
        super(props);
        this.state = { expand: false, showImagePlaceHolder: true };
    }
    _etvbrLongPress(from, info) {
        this.props.onLongPress(from, info)
        console.log(info)
    }
    render() {

        const { evaluation_manager } = this.props;
        //console.log("Hakuna", evaluators);
        let imageSource;
        let placeHolder
        if (this.props.from_location) {
            imageSource = require('../../images/ic_location.png');
            placeHolder = null;

        }
        else {
            imageSource = { uri: this.props.info.dp_url };
            placeHolder = <View style={{ position: 'absolute', display: (this.state.showImagePlaceHolder ? 'flex' : 'none'), width: 36, height: 36, borderRadius: 36 / 2, backgroundColor: '#FF8000', alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#fff' }}>{this.props.info.name.charAt(0).toUpperCase()}</Text></View>;;
        }


        let sc_views = []
        for (var i = 0; i < evaluation_manager.evaluators.length; i++) {
            let evaluator = evaluation_manager.evaluators[i];
            sc_views.push(

                <ETVBRExchangeItemChild
                    isUnderTl={true}
                    onLongPress={this._etvbrLongPress.bind(this)}
                    showPercentage={this.props.showPercentage} key={evaluator.info.id}

                    abs={
                        {
                            e: evaluator.abs.enquiries,
                            ed: evaluator.abs.evaluations,
                            pq: evaluator.abs.price_negotiations,
                            r: evaluator.abs.retails,

                        }}

                    rel={
                        {
                            e: evaluator.rel.enquiries,
                            ev: evaluator.rel.evaluations,
                            pq: evaluator.rel.price_negotiations,
                            r: evaluator.rel.retails,

                        }}



                    info={
                        {
                            dp_url: evaluator.info.dp_url,
                            name: evaluator.info.name,
                            user_id: evaluator.info.id,
                            location_id: this.props.info.location_id,
                            user_role_id: "28",
                        }
                    }

                ></ETVBRExchangeItemChild>)

        }

        if (this.props.showPercentage) {


            return (

                <View>
                    <TouchableOpacity onPress={() => this.setState({ expand: !this.state.expand })}>
                        <View style={{ marginTop: 10 }}>
                            <View style={{ marginTop: 4, marginBottom: 4, flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ flex: 1, fontSize: 15, color: '#2D4F8B', }}>{this.props.info.name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#eceff1', alignItems: 'center', borderRadius: 6 }}>


                                <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
                                    {placeHolder}
                                    <TouchableOpacity
                                        style={{ position: 'absolute', }}
                                        onPress={() => this.setState({ expand: !this.state.expand })}
                                        onLongPress={() => {
                                            this.props.onLongPress('user_info',
                                                { info: { name: this.props.info.name, dp_url: this.props.info.dp_url } })
                                        }}>
                                        <Image
                                            source={imageSource}
                                            style={{ width: 36, height: 36, borderRadius: 36 / 2, }}
                                        />
                                    </TouchableOpacity>
                                </View>







                                <View style={{
                                    flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                                    borderLeftWidth: 0.3
                                }}>
                                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#303030' }} >{this.props.rel.ed}</Text>
                                    </View>


                                </View>

                                <View style={{
                                    flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                                    borderLeftWidth: 0.3
                                }}>
                                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#303030' }} >{this.props.rel.pq}</Text>
                                    </View>

                                </View>

                                <View style={{
                                    flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                                    borderLeftWidth: 0.3
                                }}>
                                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: '#303030' }} >{this.props.rel.r}</Text>
                                    </View>


                                </View>




                            </View>
                        </View>
                    </TouchableOpacity>
                    {
                        this.state.expand && (
                            <View>
                                {sc_views}
                            </View>
                        )
                    }

                </View>
            );

        }
        else {


            return (

                <View>
                    <TouchableOpacity onPress={() => this.setState({ expand: !this.state.expand })}>
                        <View style={{ marginTop: 10 }}>
                            <View style={{ marginTop: 4, marginBottom: 4, flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ flex: 1, fontSize: 15, color: '#2D4F8B', }}>{this.props.info.name}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#eceff1', alignItems: 'center', borderRadius: 6 }}>

                                <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
                                    {placeHolder}
                                    <TouchableOpacity
                                        style={{ position: 'absolute', }}
                                        onPress={() => this.setState({ expand: !this.state.expand })}
                                        onLongPress={() => {
                                            this.props.onLongPress('user_info',
                                                { info: { name: this.props.info.name, dp_url: this.props.info.dp_url } })
                                        }}>
                                        <Image
                                            source={imageSource}
                                            style={{ width: 36, height: 36, borderRadius: 36 / 2, }}
                                        />
                                    </TouchableOpacity>
                                </View>

                                <View style={{
                                    flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                                    borderLeftWidth: 0.3
                                }}>
                                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity
                                            hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                            onPress={() => this.setState({ expand: !this.state.expand })}
                                            onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Enquiries', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                                            <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.e}</Text>
                                        </TouchableOpacity>
                                    </View>

                                </View>




                                <View style={{
                                    flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                                    borderLeftWidth: 0.3
                                }}>
                                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity
                                            hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                            onPress={() => this.setState({ expand: !this.state.expand })}
                                            onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'evaluations', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                                            <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.ed}</Text>
                                        </TouchableOpacity>
                                    </View>

                                </View>

                                <View style={{
                                    flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                                    borderLeftWidth: 0.3
                                }}>
                                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity
                                            hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                            onPress={() => this.setState({ expand: !this.state.expand })}
                                            onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'price_negotiations', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                                            <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.pq}</Text>
                                        </TouchableOpacity>
                                    </View>

                                </View>

                                <View style={{
                                    flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                                    borderLeftWidth: 0.3
                                }}>
                                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableOpacity
                                            hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                            onPress={() => this.setState({ expand: !this.state.expand })}
                                            onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Procure', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                                            <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.r}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>


                            </View>
                        </View>
                    </TouchableOpacity>

                    {this.state.expand && (<View>{sc_views}</View>)}
                </View>
            );
        }
    }
}
