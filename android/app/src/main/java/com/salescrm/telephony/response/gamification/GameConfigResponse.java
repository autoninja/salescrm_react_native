package com.salescrm.telephony.response.gamification;

import com.salescrm.telephony.response.Error;

import java.util.List;

/**
 * Created by bharath on 02/07/18.
 */

public class GameConfigResponse {
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private List<GameLocation> locations;

        private String date;

        public List<GameLocation> getLocations() {
            return locations;
        }

        public void setLocations(List<GameLocation> locations) {
            this.locations = locations;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public class GameLocation {
            private Integer id;
            private String name;
            private boolean gamification;
            private boolean targets_locked;
            private String start_date;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public boolean isGamification() {
                return gamification;
            }

            public void setGamification(boolean gamification) {
                this.gamification = gamification;
            }

            public boolean isTargets_locked() {
                return targets_locked;
            }

            public void setTargets_locked(boolean targets_locked) {
                this.targets_locked = targets_locked;
            }

            public String getStart_date() {
                return start_date;
            }

            public void setStart_date(String start_date) {
                this.start_date = start_date;
            }
        }


    }
}
