package com.salescrm.telephony.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.interfaces.TeamDashboardSwipedListener;
import com.salescrm.telephony.model.TeamDashboardSwiped;
import com.salescrm.telephony.preferences.Preferences;

import java.lang.reflect.Field;
import java.util.HashMap;

public class RNLostDropMainFragment extends Fragment implements DefaultHardwareBackBtnHandler, TeamDashboardSwipedListener {
    private ReactRootView mReactRootView;
    private ReactInstanceManager mReactInstanceManager = null;
    private Preferences pref;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedLayout = inflater.inflate(R.layout.fragment_rn_lost_drop_main, container, false);
        mReactRootView = inflatedLayout.findViewById(R.id.react_root_view_lost_drop_main);
        return inflatedLayout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        pref = Preferences.getInstance();
        pref.load(context);
        //   mReactRootView = new ReactRootView(getContext());
        if (getActivity() != null) {
            mReactInstanceManager = ((SalesCRMApplication) getActivity().getApplication())
                    .getReactNativeHost()
                    .getReactInstanceManager();
        }


    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ;
        Bundle initialProps = new Bundle();
        initialProps.putBoolean("fromAndroid", true);
        initialProps.putString("token", pref.getAccessToken());
        initialProps.putString("module", "LOST_DROP_ANALYSIS");
        if (mReactInstanceManager != null && mReactRootView != null) {
            mReactRootView.startReactApplication(
                    mReactInstanceManager,
                    "NinjaCRMSales",
                    initialProps
            );
        }

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }

    }

    private TeamDashboardSwiped teamDashboardSwiped = TeamDashboardSwiped.getInstance(this,2);
    @Override
    public void onTeamDashboardSwiped(int position, int tabCount) {
        if(position == 2) {
            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_LOST_DROP_ANALYSIS);
            CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
