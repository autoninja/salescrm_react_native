package com.salescrm.telephony.db;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by bharath on 11/12/16.
 */
public class UserCarBrands extends RealmObject {
    private String brand_id;
    private RealmList<UserCarBrand> carBrand;

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public RealmList<UserCarBrand> getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(RealmList<UserCarBrand> carBrand) {
        this.carBrand = carBrand;
    }
}
