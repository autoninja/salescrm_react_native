package com.salescrm.telephony.adapter.gamification;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.gamification.DsePointsActivity;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.gamification.ConsultantLeaderBoardResponse;
import com.salescrm.telephony.utils.CircleTransform;
import com.salescrm.telephony.utils.Util;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

/**
 * Created by bannhi on 14/12/17.
 */

public class ConsultantDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    Activity activity;
    AlertDialog alertDialog;
    private Preferences pref;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_ITEM = 1;
    private ConsultantLeaderBoardResponse.Categories categories;
    private Drawable drawableBooster;
    public ConsultantDetailsAdapter(Activity activity, ConsultantLeaderBoardResponse.Categories cats) {
        this.activity = activity;
        this.pref = Preferences.getInstance();
        categories = cats;
        pref.load(activity);
        drawableBooster = VectorDrawableCompat.create(activity.getResources(), R.drawable.ic_bolt_svg, activity.getTheme());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = View.inflate(parent.getContext(), R.layout.cons_game_row, null);
        return new ConsultantItemHolder(view);

    }
    @Override
    public int getItemViewType (int position) {
        return TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
       if(holder instanceof ConsultantItemHolder){
           //display data
           ((ConsultantItemHolder) holder).serial_number_tv.setText(""+categories.getUsers().get(position).getRank());
           ((ConsultantItemHolder) holder).dse_name_tv.setText(categories.getUsers().get(position).getName());
           if(categories.getUsers().get(position).getSpotlight()==1){
               ((ConsultantItemHolder) holder).bgMain.setBackgroundColor(Color.parseColor("#384C69"));
               ((ConsultantItemHolder) holder).goarrow.setImageResource(R.drawable.arrow_grey_right);
           }else{
               ((ConsultantItemHolder) holder).bgMain.setBackgroundColor(Color.parseColor("#FF0D284D"));
               ((ConsultantItemHolder) holder).goarrow.setImageResource(R.drawable.blue_arrow);
           }
           ((ConsultantItemHolder) holder).bgMain.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Intent intent = new Intent(activity, DsePointsActivity.class);
                   intent.putExtra("user_id",""+categories.getUsers().get(position).getUser_id());
                   intent.putExtra("name",categories.getUsers().get(position).getName());
                   intent.putExtra("image",categories.getUsers().get(position).getPhoto_url());
                   intent.putExtra("points", Util.getInt(categories.getUsers().get(position).getPoints()));
                   intent.putExtra("spotlight", categories.getUsers().get(position).getSpotlight());
                   activity.startActivity(intent);
                   }
           });


           ((ConsultantItemHolder) holder).text_user.setText
                   (String.format("%s", categories.getUsers().get(position).getName().toUpperCase().substring(0, 1)));

           if(Util.isNotNull(categories.getUsers().get(position).getPhoto_url())) {
               Glide.with(activity).
                       load(categories.getUsers().get(position).getPhoto_url())
                       .asBitmap()
                       .centerCrop()
                       .listener(new RequestListener<String, Bitmap>() {
                           @Override
                           public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                               ((ConsultantItemHolder) holder).imgUser.setVisibility(View.GONE);
                               ((ConsultantItemHolder) holder).text_user.setVisibility(View.VISIBLE);
                               e.printStackTrace();
                               return false;
                           }

                           @Override
                           public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                               ((ConsultantItemHolder) holder).imgUser.setVisibility(View.VISIBLE);
                               ((ConsultantItemHolder) holder).text_user.setVisibility(View.GONE);
                               return false;
                           }
                       })
                       .into(new BitmapImageViewTarget(((ConsultantItemHolder) holder).imgUser) {
                           @Override
                           protected void setResource(Bitmap resource) {
                               if(resource!=null ) {
                                   RoundedBitmapDrawable circularBitmapDrawable =
                                           RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                                   circularBitmapDrawable.setCircular(true);
                                   ((ConsultantItemHolder) holder).imgUser.setImageDrawable(circularBitmapDrawable);
                               }
                           }
                       });
           }
           else {
               ((ConsultantItemHolder) holder).imgUser.setVisibility(View.GONE);
               ((ConsultantItemHolder) holder).text_user.setVisibility(View.VISIBLE);

           }


           System.out.println("Booster - "+categories.getUsers().get(position).getBooster_multiplier());

            if(categories.getUsers().get(position).getBooster_multiplier()!=null && categories.getUsers().get(position).getBooster_multiplier()>1) {
               ((ConsultantItemHolder) holder).tv_booster.setVisibility(View.VISIBLE);
               ((ConsultantItemHolder) holder).tv_booster.setText("   "+categories.getUsers().get(position).getBooster_multiplier()+"x");
                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    ((ConsultantItemHolder) holder).tv_booster.setBackgroundDrawable(drawableBooster);
                }else{
                    ((ConsultantItemHolder) holder).tv_booster.setBackground(drawableBooster);
                }
            }
            else {
                ((ConsultantItemHolder) holder).tv_booster.setVisibility(View.GONE);
            }
            ((ConsultantItemHolder) holder).tv_dse_pts.setText(""+categories.getUsers().get(position).getPoints());
            ((ConsultantItemHolder) holder).goarrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //not yet decided.
                }
            });
            if(categories.getUsers().get(position).getBadges()!=null
                    && categories.getUsers().get(position).getBadges().size()>0){
                ((ConsultantItemHolder) holder).badge_one.setVisibility(View.GONE);
                ((ConsultantItemHolder) holder).badge_two.setVisibility(View.GONE);
                ((ConsultantItemHolder) holder).badge_three.setVisibility(View.GONE);
                ((ConsultantItemHolder) holder).badge_four.setVisibility(View.GONE);
                ((ConsultantItemHolder) holder).badge_five.setVisibility(View.GONE);
                for(int i=0;i< categories.getUsers().get(position).getBadges().size();i++) {
                    ConsultantLeaderBoardResponse.Users.UserBadges badgeData = categories.getUsers().get(position).getBadges().get(i);
                    if(i==0) {
                        ((ConsultantItemHolder) holder).badge_one.setVisibility(View.VISIBLE);
                        ((ConsultantItemHolder) holder).badge_one.setImageResource(getBadgeIcon(badgeData.getBadge_id()));
                    }
                    else if(i==1){
                        ((ConsultantItemHolder) holder).badge_two.setVisibility(View.VISIBLE);
                        ((ConsultantItemHolder) holder).badge_two.setImageResource(getBadgeIcon(badgeData.getBadge_id()));
                    }else if(i==2){
                        ((ConsultantItemHolder) holder).badge_three.setVisibility(View.VISIBLE);
                        ((ConsultantItemHolder) holder).badge_three.setImageResource(getBadgeIcon(badgeData.getBadge_id()));
                    }
                    else if(i==3){
                        ((ConsultantItemHolder) holder).badge_four.setVisibility(View.VISIBLE);
                        ((ConsultantItemHolder) holder).badge_four.setImageResource(getBadgeIcon(badgeData.getBadge_id()));

                    }
                    else if(i==4){
                        ((ConsultantItemHolder) holder).badge_five.setVisibility(View.VISIBLE);
                        ((ConsultantItemHolder) holder).badge_five.setImageResource(getBadgeIcon(badgeData.getBadge_id()));

                    }

                }
            }else{
                ((ConsultantItemHolder) holder).badge_one.setVisibility(View.GONE);
                ((ConsultantItemHolder) holder).badge_two.setVisibility(View.GONE);
                ((ConsultantItemHolder) holder).badge_three.setVisibility(View.GONE);
                ((ConsultantItemHolder) holder).badge_four.setVisibility(View.GONE);
                ((ConsultantItemHolder) holder).badge_five.setVisibility(View.GONE);
            }

       }
    }

    private int getBadgeIcon(int badgeId){
        switch(badgeId){
            case 1:
                return R.drawable.badge_bulb;
            case 2:
                return R.drawable.badge_car;
            case 3:
                return R.drawable.badge_target;
            case 4:
                return R.drawable.badge_fire;
            case 5:
                return R.drawable.badge_explorer;
            case 6:
                return R.drawable.badge_achiever;
            case 7:
                return R.drawable.badge_dynamo;
            case 8:
                return R.drawable.badge_guru;
            case 9:
                return R.drawable.badge_trophy;


         }
        return 0;
    }
    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        if(categories.getUsers()!=null) {
            return categories.getUsers().size();
        }
        return 0;
    }


    private class ConsultantItemHolder extends RecyclerView.ViewHolder {

        TextView dse_name_tv, text_user,serial_number_tv,tv_booster,tv_dse_pts;
        ImageView imgUser;
        RelativeLayout bgMain;
        LinearLayout rll_right;
        ImageView badge_one, badge_two,badge_three,badge_four,badge_five,goarrow;
        ConsultantItemHolder(View itemView) {
            super(itemView);
            serial_number_tv = (TextView) itemView.findViewById(R.id.serial_number);
            dse_name_tv = (TextView) itemView.findViewById(R.id.dse_name);
            imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            badge_one = (ImageView) itemView.findViewById(R.id.badge_one) ;
            badge_two = (ImageView) itemView.findViewById(R.id.badge_two) ;
            badge_three = (ImageView) itemView.findViewById(R.id.badge_three) ;
            badge_four = (ImageView) itemView.findViewById(R.id.badge_four) ;
            badge_five = (ImageView) itemView.findViewById(R.id.badge_five) ;
            goarrow = (ImageView) itemView.findViewById(R.id.go_arrow) ;
            text_user = (TextView) itemView.findViewById(R.id.text_user);
            tv_booster = (TextView) itemView.findViewById(R.id.gamification_dse_multiplier_count);
            tv_dse_pts = (TextView) itemView.findViewById(R.id.dse_pts);
            bgMain = (RelativeLayout) itemView.findViewById(R.id.rel_leader_board_dse_item_main);
            rll_right = (LinearLayout) itemView.findViewById(R.id.rightll);

        }
    }
}
