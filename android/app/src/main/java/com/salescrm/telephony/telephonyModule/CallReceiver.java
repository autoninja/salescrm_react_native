package com.salescrm.telephony.telephonyModule;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.CallLog;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.salescrm.telephony.BuildConfig;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.AllMobileNumbersSearch;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.telephonyModule.db.TelephonyDbHandler;
import com.salescrm.telephony.telephonyModule.recorder.AudioRecording;
import com.salescrm.telephony.telephonyModule.service.SyncRecordInBackground;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;


public class CallReceiver extends PhonecallReceiver {

    private static final String TAG = "CallReceiverTelephony:";
    private static Preferences pref = null;
    private static MediaRecorder myRecorder;
    private static String outputFile;
    private static String fileName, imeiNumber;
    // private static String startDate, endDate;
    private static long seconds;
    //private static final String AUDIO_RECORDER_FOLDER = "Ninja_Crm";
    public static final String DATABASE_NAME = "NinjaCrm.db";
    private static long start_time, end_time;
    private static String incomingOrOutgoing = "O";
    private static String recordingType;
    private Realm realm = Realm.getDefaultInstance();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
    Cursor cur;
    int triggrCall = 0;
    private static String subNumber;
    private static long currentLeadId = 0;
    private AllMobileNumbersSearch mobileNumbers;


    // private static boolean useApi3;
    private static AudioRecording mAudioRecording;

    @Override
    protected void onIncomingCallStarted(final Context ctx, final String number, final String start) {

    }
/*    private void callAcr(Context context){
        if(Build.VERSION.SDK_INT >= 23) {
            useApi3 = DeviceHelper.useApi3();
            Log.i(TAG, "Remaining license time in seconds  " + Native.getExpiry(Util.getLong(pref.getAcrSerial()), pref.getAcrLicenseKey()));
            Log.i(TAG, "Package and cert check result  " + Native.checkPackageAndCert(context));
            *//* Fix android 7.1 issues. Must be called before start7. Not implemented for API3 and would crash if used together with start3()*//*
            if (DeviceHelper.isAndroid71FixRequired()) {
                DeviceHelper.sleepForAndroid71();
                Native.fixAndroid71(Native.FIX_ANDROID_7_1_ON);
            }
        }
    }*/

    @Override
    protected void startRecordingIncomingCalls(final Context ctx, final String number, final String start) {
        Log.d("PhoneCallReceiver", "IncomingCallStarted() - " + number);
        pref = Preferences.getInstance();
        pref.load(ctx);

        //Call ACR
        //callAcr(ctx);

        subNumber = number;
        pref.setmStart(start);
        incomingOrOutgoing = "I";
        recordingType = ".3gp";
        try {
            if (subNumber.startsWith("0")) {
                subNumber = number.substring(1);
            }
            if (subNumber.contains("+91")) {
                subNumber = number.substring(3);
            }
            subNumber = subNumber.replaceAll("\\s+", "");
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }

        realm = Realm.getDefaultInstance();
        mobileNumbers = realm.where(AllMobileNumbersSearch.class)
                .contains("mobileNumber", subNumber)
                .findFirst();
        System.out.println("TELEPHONY mobileNumbers 0- " + mobileNumbers);

        System.out.println(TAG + "Incoming Call");
        System.out.println(TAG + "Mobile Number:" + subNumber);
        System.out.println(TAG + "MobileNumberDataInDb:" + mobileNumbers);

        start_time = System.currentTimeMillis();
        if (mobileNumbers != null && !pref.isClientILom() && !pref.isClientILBank()) {
            System.out.println(TAG + "Went Inside Recordings");
            ////////////////////subbu's code//////////////////////////////////
            android.os.Handler mHandler1 = new android.os.Handler();
            mHandler1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        pref.setCalledNumber(subNumber);
                        currentLeadId = mobileNumbers.getLeadId();
                        start_time = System.currentTimeMillis();
                        System.out.println("TELEPHONY mobileNumbers 1- " + number);
                        if (mobileNumbers != null) {
                            startRecording(ctx, subNumber, start);

                            pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_INCOMING_STARTED,
                                    subNumber,
                                    subNumber,
                                    pref.getCalledNumber(),
                                    currentLeadId + "",
                                    "Mobile Number : " + mobileNumbers.getMobileNumber()
                                            + "-Lead Id:" + mobileNumbers.getLeadId()
                                            + "-Name:" + mobileNumbers.getName());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }, 100L);
            /////////////////////subbu's code ends///////////////////////////////
        } else if (pref.isCalledHappened()) {
            String numberTriggeredAndSearched = subNumber;
            String cdrNumber = pref.getCalledNumber();
            String cdrLeadId = currentLeadId + "";

            stopRecording(ctx, subNumber, -1, null);

            pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_INCOMING_ENDED_FORCE,
                    numberTriggeredAndSearched,
                    numberTriggeredAndSearched,
                    cdrNumber,
                    cdrLeadId,
                    null
            );
        }

    }

    @Override
    protected void onOutgoingCallStarted(final Context ctx, final String number, final String start,
                                         boolean recordWhenHotlineNull) {
        incomingOrOutgoing = "O";
        recordingType = ".3gp";
        pref = Preferences.getInstance();
        pref.load(ctx);

        //Call ACR
        // callAcr(ctx);

        start_time = System.currentTimeMillis();
        pref.setmStart(start);
        subNumber = number;

        try {
            if(!pref.isClientILom() && !pref.isClientILBank()) {
                    if (subNumber.startsWith("0")) {
                        subNumber = number.substring(1);
                    }
                    if (subNumber.contains("+91")) {
                        subNumber = number.substring(3);
                    }
            }
            subNumber = subNumber.replaceAll("\\s+", "");

        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }
        realm = Realm.getDefaultInstance();
        mobileNumbers = realm.where(AllMobileNumbersSearch.class)
                .contains("mobileNumber", subNumber)
                .findFirst();
        System.out.println("TELEPHONY mobileNumbers 00- " + mobileNumbers);
        System.out.println(TAG + "Outgoing Call");
        System.out.println(TAG + "Mobile Number:" + subNumber);
        System.out.println(TAG + "MobileNumberDataInDb:" + mobileNumbers);
        System.out.println(TAG + "HOTLINE Number:"+pref.getHotlineNumber());

        boolean iLomCheck = pref.isClientILom()
                && Util.isNotNull(pref.getHotlineNumber())
                && subNumber.contains(pref.getHotlineNumber().split("\\D+")[0]);
        boolean iLomBankCheck = pref.isClientILBank()
                && Util.isNotNull(pref.getHotlineNumber())
                && subNumber.contains(pref.getHotlineNumber().split("\\D+")[0]);
        boolean iLomCheckHotlineNull = pref.isClientILom() && recordWhenHotlineNull;
        boolean iLomBankCheckHotlineNull = pref.isClientILBank() && recordWhenHotlineNull;
        if(iLomCheck || iLomBankCheck || iLomCheckHotlineNull || iLomBankCheckHotlineNull){
            System.out.println(TAG + "Went Inside Recordings");
            android.os.Handler mHandler1 = new android.os.Handler();
            mHandler1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        pref.setCalledNumber(subNumber);
                        currentLeadId = Util.getLongDefaultZero(pref.getCallingLeadId());
                        pref = Preferences.getInstance();
                        pref.load(ctx);
                        start_time = System.currentTimeMillis();
                        System.out.println("TELEPHONY LEAD ID BEFORE RECORDING" + pref.getmTelephonyLeadID());
                        startRecording(ctx, subNumber, start);
                        pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_OUTGOING_STARTED,
                                subNumber,
                                subNumber,
                                pref.getCalledNumber(),
                                currentLeadId + "",
                                "Mobile Number : " + subNumber
                                        + "-Lead Id:" + currentLeadId
                                        + "-Name: I_LOM" );

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }, 100L);

            ////////////////////////////////////////////////////
            /**
             * required when data is required to sync
             */
        }
        else if (!pref.isClientILom() && !pref.isClientILBank() && mobileNumbers != null) {
            System.out.println(TAG + "Went Inside Recordings");
            ///  ctx.startService(new Intent(ctx, SyncRecordInBackground.class));
            ////////////////////subbu's code//////////////////////////////////
            android.os.Handler mHandler1 = new android.os.Handler();
            mHandler1.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        pref.setCalledNumber(subNumber);
                        currentLeadId = mobileNumbers.getLeadId();
                        pref = Preferences.getInstance();
                        pref.load(ctx);
                        start_time = System.currentTimeMillis();
                        System.out.println("TELEPHONY LEAD ID BEFORE RECORDING" + pref.getmTelephonyLeadID());
                        if (mobileNumbers != null) {
                            startRecording(ctx, subNumber, start);
                            pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_OUTGOING_STARTED,
                                    subNumber,
                                    subNumber,
                                    pref.getCalledNumber(),
                                    currentLeadId + "",
                                    "Mobile Number : " + mobileNumbers.getMobileNumber()
                                            + "-Lead Id:" + mobileNumbers.getLeadId()
                                            + "-Name:" + mobileNumbers.getName());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }, 100L);

            ////////////////////////////////////////////////////
            /**
             * required when data is required to sync
             */
        } else if (pref.isCalledHappened()) {

            String numberTriggeredAndSearched = subNumber;
            String cdrNumber = pref.getCalledNumber();
            String cdrLeadId = currentLeadId + "";

            stopRecording(ctx, subNumber, -1, null);

            pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_OUTGOING_ENDED_FORCE,
                    numberTriggeredAndSearched,
                    numberTriggeredAndSearched,
                    cdrNumber,
                    cdrLeadId,
                    null);

        }


    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String number, String start, String end) {
        Log.d("PhoneCallReceiver", "IncomingcallEnd()");
        end_time = System.currentTimeMillis();
        // endDate = end;
        pref = Preferences.getInstance();
        pref.load(ctx);
        pref.setmEnd(end);
        long callDuration = end_time - start_time;
        seconds = TimeUnit.MILLISECONDS.toSeconds(callDuration);
        pref.setmDuration(seconds);
        // pref.setInOut("00");
        if (pref.isLogedIn() && pref.isCalledHappened() && ctx != null && WSConstants.TELEPHONY_ENABLE) {

            String cdrNumber = pref.getCalledNumber();
            String cdrLeadId = currentLeadId + "";

            stopRecording(ctx, number, 0, null);

            pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_INCOMING_ENDED,
                    number,
                    null,
                    cdrNumber,
                    cdrLeadId,
                    null
            );
        }
    }

    private void deleteCallLog(final Context context, final String mobileNumber) {
        System.out.println("mobile number to delete:" + mobileNumber);
        String searchNumber = mobileNumber;
        try {
            if (searchNumber.startsWith("0")) {
                searchNumber = searchNumber.substring(1);
            }
            if (searchNumber.contains("+91")) {
                searchNumber = searchNumber.substring(3);
            }
            searchNumber = searchNumber.replaceAll("\\s+", "");

        } catch (Exception ne) {
            ne.printStackTrace();
        }
        final String finalSearchNumber = searchNumber;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                System.out.println("Searched number: " + finalSearchNumber);
                if (Realm.getDefaultInstance().where(AllMobileNumbersSearch.class)
                        .equalTo("mobileNumber", finalSearchNumber)
                        .findFirst() != null) {
                    System.out.println("To delete mobile number:" + mobileNumber);
                    String queryString = "NUMBER=" + mobileNumber;
                    System.out.println("Number of rows deleted::" +
                            context.getContentResolver().delete(CallLog.Calls.CONTENT_URI, queryString, null));

                }
            }
        }, 5000);

    }

    @Override
    protected void onOutgoingCallEnded(Context ctx, String number, String start, String end, String scheduledActivityId) {
        Preferences pref = null;
        Log.d("PhoneCallReceiver", "OutgoingcallEnd()");
        Log.d("PhoneCallReceiver", "scheduledActivityId:"+scheduledActivityId);
        end_time = System.currentTimeMillis();
        // endDate = end;
        pref = Preferences.getInstance();
        pref.load(ctx);
        pref.setmEnd(end);
        long callDuration = end_time - start_time;
        seconds = TimeUnit.MILLISECONDS.toSeconds(callDuration);
        pref.setmDuration(seconds);
        if (pref.isLogedIn() & pref.isCalledHappened() && ctx != null && WSConstants.TELEPHONY_ENABLE) {


            String cdrNumber = pref.getCalledNumber();
            String cdrLeadId = currentLeadId + "";

            stopRecording(ctx, number, 1, scheduledActivityId);

            pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_OUTGOING_ENDED,
                    number,
                    null,
                    cdrNumber,
                    cdrLeadId,
                    null);
        }
    }

    @Override
    protected void onMissedCall(Context ctx, String number, String start) {
        pref = Preferences.getInstance();
        pref.load(ctx);
        // pref.setmInOut("00");
        Log.d("PhoneCallReceiver", "Misscall()");
        if (pref.isLogedIn() && pref.isCalledHappened() && ctx != null && WSConstants.TELEPHONY_ENABLE) {

            String cdrNumber = pref.getCalledNumber();
            String cdrLeadId = currentLeadId + "";

            stopRecording(ctx, number, -1, null);
            pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_MISSED_CALL_ENDED,
                    number,
                    null,
                    cdrNumber,
                    cdrLeadId,
                    null);
        }
    }

    private void startRecording(final Context ctx, final String number, final String start) throws IOException {
        /**
         * showing the dialog to identify the called customer
         */

     /*   if(!pref.getmTelephonyLeadID().equalsIgnoreCase("0")) {

            Intent activitIintent = new Intent(ctx, GcmDialog.class);
            activitIintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activitIintent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            activitIintent.putExtra("mobile", number);
            ctx.startActivity(activitIintent);

        }*/
        pref.setCalledHappened(true);
        pref.setmFilePath("");
        pref.setFileName("");
        if(Build.VERSION.SDK_INT< Build.VERSION_CODES.P) {
            try {
                recordCall(start, number);
                myRecorder.prepare();
                myRecorder.start();
            } catch (Exception ex) {
                Log.e("CallReceiver", "** Exception Normal Recording....");
                deleteFileIfExceptionCame(outputFile);


                if (Build.VERSION.SDK_INT >= 24 || pref.isIsDeviceSupportedForNLL()) {
                    try {
                        System.out.println("Record Library invoking Started::");

                        setFileName(start, number, ".aac");
                        //New audio record technique
                        mAudioRecording = new AudioRecording();
                        AudioRecording.OnAudioRecordListener onRecordListener = new AudioRecording.OnAudioRecordListener() {

                            @Override
                            public void onRecordFinished() {
                                Log.d("MAIN", "onFinish ");
                                System.out.println("Record Library success :in Audio Record:");

                            }

                            @Override
                            public void onError(int e) {
                                Log.d("MAIN", "onError " + e);
                                mAudioRecording = null;
                                deleteFileIfExceptionCame(outputFile);
                                Crashlytics.log(Log.ERROR, "AUDIO_RECORD_ENCODING", "Failed on Error Stage2_" + Build.MODEL + '_' + Build.VERSION.SDK_INT);
                                Crashlytics.logException(new Exception("AUDIO_RECORD_ENCODING | ERROR:" + e + "Failed on Error Stage2_" + Build.MODEL + '_' + Build.VERSION.SDK_INT));
                                System.out.println("Record Library invoking failed:in Audio Record:");
                                recordingException(ctx, start, number);
                            }

                            @Override
                            public void onRecordingStarted() {
                                Log.d("MAIN", "onStart ");

                            }
                        };


                        mAudioRecording.setOnAudioRecordListener(onRecordListener);
                        mAudioRecording.setFile(outputFile);
                        mAudioRecording.startRecording(ctx, pref);

                    } catch (Exception ex1) {

                        Log.e(TAG, "onError: ", ex1);
                        try {
                            mAudioRecording.stopRecording(true);
                            mAudioRecording = null;
                        } catch (Exception exStop) {
                            Crashlytics.log(Log.ERROR, "AUDIO_RECORD_ENCODING", "Failed to stop3_" + Build.MODEL + '_' + Build.VERSION.SDK_INT);
                            Crashlytics.logException(exStop);
                        } finally {
                            mAudioRecording = null;
                        }
                        deleteFileIfExceptionCame(outputFile);
                        System.out.println("Record Library invoking failed::");
                        new Handler(ctx.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                deleteFileIfExceptionCame(outputFile);
                                recordingException(ctx, start, number);
                            }
                        });

                    }
                } else {
                    deleteFileIfExceptionCame(outputFile);
                    recordingException(ctx, start, number);
                }

            }
        }

    }


    private void deleteFileIfExceptionCame(String outputFile) {
        try {
            File file = new File(outputFile);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception ignored) {

        }
    }

    private void recordingException(Context ctx, String start, String number) {
        try {
            System.out.println("Exception Recording");
            setFileName(start, number, ".3gp");
            myRecorder.reset();

            myRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            myRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            myRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_WB);
           /* myRecorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
            myRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);*/
            myRecorder.setMaxDuration(60 * 60 * 1000);

            try {
                AudioManager audiomanager = (AudioManager) ctx.getSystemService("audio");
                int i = audiomanager.getRouting(2);
                audiomanager.setMode(2);
                audiomanager.setMicrophoneMute(false);
                int j = audiomanager.getStreamMaxVolume(0);
                if (j < 0)
                    j = 1;
                int k = j / 2 + 1;
                audiomanager.setStreamVolume(0, k, 0);
                audiomanager.setRouting(2, 11, 15);
            } catch (Exception ignored) {

            }


            myRecorder.setOutputFile(outputFile);
            try {
                myRecorder.prepare();
                myRecorder.start();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sync(Context ctx, String scheduledActivityId) {
        final String uuid = "" + UUID.randomUUID().toString() + "" + pref.getCalledNumber() + "" + formatter.format(new Date());


        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                System.out.println("TELEPHONY LEAD ID IS " + currentLeadId);
                System.out.println("TELEPHONY subNumber - " + pref.getCalledNumber() + " - " + subNumber);

                if (!pref.getCalledNumber().equalsIgnoreCase("")) {
                    int nextId = TelephonyDbHandler.getInstance().insertIntoDB(realm,
                            pref.getmaxId(),
                            uuid,
                            scheduledActivityId,
                            "" + currentLeadId,
                            incomingOrOutgoing,
                            pref.getCalledNumber(),
                            "" + pref.getmStart(),
                            pref.getmEnd(),
                            pref.getmDuration(),
                            "unknown",
                            pref.getmFilePath(),
                            pref.getFileName(),
                            Build.VERSION.SDK_INT>=Build.VERSION_CODES.P?1:0,
                            0,
                            1,
                            Util.getNowTimeString());
                    pref.setMaxId(nextId);

                    pref.setmTelephonyLeadID("0");
                    currentLeadId = 0;
                    pref.setIsCallOnGoing(false);
                    System.out.println("TELEPHONY LEAD ID AFTER INSERT IS " + currentLeadId);
                    realm.close();

                }
            }
        });
        realm.close();
        pref.setCalledHappened(false);
        pref.setCalledNumber("");
        pref.setCallingLeadId("");
        // pref.setmCallId("00");
        try {
            ctx.startService(new Intent(ctx, SyncRecordInBackground.class));
        } catch (Exception e) {
            try {
                if (Build.VERSION.SDK_INT >= 28) {
                    ctx.startForegroundService(new Intent(ctx, SyncRecordInBackground.class));
                }
            } catch (Exception exc) {

            }

        }


    }

    private void pushCleverTap() {

        try {
            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put(CleverTapConstants.EVENT_ZERO_BYTE_RECORDINGS_KEY_BRAND, Build.MANUFACTURER);
            hashMap.put(CleverTapConstants.EVENT_ZERO_BYTE_RECORDINGS_KEY_MODEL, Build.MODEL);
            hashMap.put(CleverTapConstants.EVENT_ZERO_BYTE_RECORDINGS_KEY_OS, Build.VERSION.SDK_INT);
            CleverTapPush.pushEvent(CleverTapConstants.EVENT_ZERO_BYTE_RECORDINGS, hashMap);
        } catch (Exception e) {
            e.printStackTrace();

        }


    }

    private void stopRecording(final Context ctx, String number, int inOut, String scheduledActivityId) {
        realm = Realm.getDefaultInstance();
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.P) {
            try {
                if (seconds > 20 && incomingOrOutgoing.equals("O")) {
                    pref.setmOutgoingCalls(pref.getmOutgoingCalls() + 1);
                }
            } catch (NullPointerException ne) {
                ne.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            try {
                if (incomingOrOutgoing.equals("I")) {
                    pref.setmIncomingCalls(pref.getmIncomingCalls() + 1);
                    //TelephonyDbHandler.getInstance().getLeadFromMobileNumber(number, ctx);
                    // updateIncomingCalls(myRealm);
                }
            } catch (NullPointerException ne) {
                ne.fillInStackTrace();
            }

            PackageInfo pInfo = null;
            try {
                pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if (mAudioRecording != null) {
                mAudioRecording.stopRecording(false);
                mAudioRecording = null;
                System.out.println("Stopped");
            } else {
                try {
                    if (null != myRecorder) {
                        myRecorder.stop();
                        myRecorder.reset();
                        myRecorder.release();
                        myRecorder = null;
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    myRecorder = null;
                    e.printStackTrace();
                } catch (RuntimeException e) {
                    e.printStackTrace();
                    myRecorder = null;
                    e.printStackTrace();
                } catch (Exception e) {
                    myRecorder = null;
                    e.printStackTrace();
                } finally {
                    myRecorder = null;
                }
            }

            if (new File(outputFile).length() == 0) {
                pushCleverTap();
            } else {
                System.out.println("Files are fine no need to push");
            }
        }


        sync(ctx, scheduledActivityId);



    }

    private String getFilename(String date2, String number) {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, WSConstants.AUDIO_RECORDER_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }
        return (file.getAbsolutePath() + "/" + date2 + "_" + pref.getCalledNumber() + "_" + pref.getImeiNumber() + "_"
                + incomingOrOutgoing + "R" + recordingType);
    }

    private void setFileName(String start, String number, String ext) {
        recordingType = ext;
        outputFile = getFilename(start, number);
        pref.setmFilePath(outputFile);
        fileName = "" + start + "_" + pref.getCalledNumber() + "_" + pref.getImeiNumber() + "_" + incomingOrOutgoing
                + "R" + recordingType;
        pref.setFileName(fileName);
    }

    private void recordCall(String date2, String number) {

        setFileName(date2, number, ".3gp");
       /* try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        myRecorder = new MediaRecorder();
        myRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);
        myRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        System.out.println("ex");
        myRecorder.setOutputFile(outputFile);
        myRecorder.setMaxDuration(60 * 60 * 1000);
    }

    private void pushCleverTapTelephony(String eventType,
                                        String triggeredNumber,
                                        String searchedNumber,
                                        String cdrNumber,
                                        String cdrLeadId,
                                        String dbResult
    ) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_TYPE, eventType);
        hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_CDR_SYNC_PERCENTAGE, TelephonyDbHandler.getInstance().getCDRSyncPercentage(realm));
        hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_FILE_SYNC_PERCENTAGE, TelephonyDbHandler.getInstance().getFileSyncPercentage(realm));


        if (Util.isNotNull(triggeredNumber)) {
            hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_TRIGGERED_NUMBER, triggeredNumber);
        }
        if (Util.isNotNull(searchedNumber)) {
            hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_SEARCHED_NUMBER, searchedNumber);
        }
        if (Util.isNotNull(cdrNumber)) {
            hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_CDR_NUMBER, cdrNumber);
        }
        if (Util.isNotNull(cdrLeadId)) {
            hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_CDR_LEAD_ID, cdrLeadId);
        }
        if (Util.isNotNull(dbResult)) {
            hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_DB_RESULT, dbResult);
        }
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TELEPHONY, hashMap);

    }


}