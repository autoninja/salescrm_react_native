package com.salescrm.telephony.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.preferences.Preferences;

public class ForceUpdateActivity extends AppCompatActivity {

    /* private RadioGroup radioUpdateGroup;
     private RadioButton radioOptionButton;*/
    private TextView update;
    private Intent i;
    private Preferences pref;
    private TextView tvUpdateNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_force_update);

        tvUpdateNotice = (TextView) findViewById(R.id.tv_update_changes);

        pref = Preferences.getInstance();
        pref.load(this);
       /* radioUpdateGroup = (RadioGroup) findViewById(R.id.radioAppUpdateGroup);*/
        update = (TextView) findViewById(R.id.btnAppUpdate);
        tvUpdateNotice.setText(pref.getNewUpdateNotice());
        update.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.salescrm.telephony")));
                } catch (ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.salescrm.telephony")));
                }

            }

        });

    }
}
