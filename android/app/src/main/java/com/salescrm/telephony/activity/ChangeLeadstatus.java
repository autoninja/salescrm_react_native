package com.salescrm.telephony.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.salescrm.telephony.R;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AddNoteResponse;
import com.salescrm.telephony.utils.ApiUtil;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by akshata on 30/5/16.
 */
public class ChangeLeadstatus extends AppCompatActivity implements Callback {

    Button savebtnaddnote;
    Button save_btn_add_note;
    private Preferences pref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_note);
        Toolbar toolbar_add_note = (Toolbar) findViewById(R.id.toolbar_add_note);
        setSupportActionBar(toolbar_add_note);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        pref = Preferences.getInstance();
        pref.load(getApplicationContext());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        save_btn_add_note=(Button)findViewById(R.id.savebtnaddnote);
        save_btn_add_note.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                call_note_api();
            }
        });
    }
    @Override
    public void success(Object o, Response response) {
        List<Header> headerList = response.getHeaders();
        for(Header header : headerList) {
            Log.e("Action Plan", "Action -  "+header.getName() + " - " + header.getValue());
            if(header.getName().equalsIgnoreCase("Authorization")){
                ApiUtil.UpdateAccessToken( header.getValue());
            }
        }
        AddNoteResponse addnote=(AddNoteResponse) o;
    }
    public void call_note_api(){
        System.out.println("Pref aCCESS tOKEN:"+pref.getAccessToken());
    }

    @Override
    public void failure(RetrofitError error) {
        Log.e("Getnote", "note - " + error.getMessage());
    }

}
