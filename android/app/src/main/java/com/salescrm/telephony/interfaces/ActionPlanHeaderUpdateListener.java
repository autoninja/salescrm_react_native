package com.salescrm.telephony.interfaces;

/**
 * Created by bharath on 7/6/17.
 */

public interface ActionPlanHeaderUpdateListener{
    void onActionPlanHeaderUpdateRequired(int from);
    void onFilterAppliedUpdateText(int pos,String text);
}