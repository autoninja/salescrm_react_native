package com.salescrm.telephony.model;

import android.text.TextUtils;

/**
 * Created by bharath on 13/7/16.
 */
public class SearchInputData {
    private String mobile;
    private String enqNumber;
    private String customerName;
    private long leadID;
    private String emailId;

    public SearchInputData(String mobile, String enqNumber, String customerName, long leadID, String emailId) {
        this.mobile = mobile;
        this.enqNumber = enqNumber;
        this.customerName = customerName;
        this.leadID = leadID;
        this.emailId = emailId;
        checkEmpty();
    }

    private void checkEmpty() {
        if(TextUtils.isEmpty(mobile)){
            mobile = "-1";
        }
        if(TextUtils.isEmpty(enqNumber)){
            enqNumber = "-1";
        }
        if(TextUtils.isEmpty(customerName)){
            customerName = "-1";
        }
        if(TextUtils.isEmpty(emailId)){
            emailId = "-1";
        }
    }


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEnqNumber() {
        return enqNumber;
    }

    public void setEnqNumber(String enqNumber) {
        this.enqNumber = enqNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public long getLeadID() {
        return leadID;
    }

    public void setLeadID(int leadID) {
        this.leadID = leadID;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
