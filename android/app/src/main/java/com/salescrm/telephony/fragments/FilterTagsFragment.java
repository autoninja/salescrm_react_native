package com.salescrm.telephony.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.FilterActivityTypeCustomAdapter;
import com.salescrm.telephony.dataitem.FilterActivityTypeModel;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ravindra P on 03-06-2016.
 */
public class FilterTagsFragment extends Fragment {

    private View rootView;
    private ListView listView;
    private int lastSelectedListItem = 0;
    private String[] items;
    private List<FilterActivityTypeModel> modelItems;
    private FilterActivityTypeCustomAdapter adapter;
    FilterSelectionHolder selectionHolder;
    private SparseArray<String> selectedValues;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.filter_activity_type_fragment, container, false);
        listView = (ListView) rootView.findViewById(R.id.lvfilter_activity_type_item_list);

        selectionHolder = FilterSelectionHolder.getInstance();

        modelItems = new ArrayList<>();
        showAdapter();
        if (selectedValues == null) {
            selectedValues = new SparseArray<String>();
        }

        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // System.out.println("Data from other Fragment:"+filterSelectionHolder.getData());

                FilterActivityTypeModel model = modelItems.get(position);
                if (model.getValue() == 1) {
                    model.setValue(0);

                    if (selectedValues.get(position) != null) {
                        selectedValues.remove(position);
                        selectionHolder.setMapData(1, selectedValues);
                        if (selectedValues.size() == 0) {
                            selectionHolder.removeMapData(1);
                        }
                    }

                } else {
                    model.setValue(1);
                    selectedValues.put(position, model.getName());
                    selectionHolder.setMapData(1, selectedValues);
                }
                modelItems.set(position, model);
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
                lastSelectedListItem = position;
            }
        });


        return rootView;

    }

    private void showAdapter() {
        modelItems.add(new FilterActivityTypeModel(WSConstants.LEAD_TAG_HOT, 0,WSConstants.LEAD_TAG_HOT_ID));
        modelItems.add(new FilterActivityTypeModel(WSConstants.LEAD_TAG_WARM, 0,WSConstants.LEAD_TAG_WARM_ID));
        modelItems.add(new FilterActivityTypeModel(WSConstants.LEAD_TAG_COLD, 0,WSConstants.LEAD_TAG_COLD_ID));
        //modelItems.add(new FilterActivityTypeModel(WSConstants.LEAD_TAG_OVERDUE, 0,WSConstants.LEAD_TAG_OVERDUE_ID));
        adapter = new FilterActivityTypeCustomAdapter(getActivity(), modelItems);

        selectedValues = selectionHolder.getMapData(1);
        if (selectedValues != null) {
            for (int i = 0; i < selectedValues.size(); i++) {
                FilterActivityTypeModel model = modelItems.get(selectedValues.keyAt(i));
                if (model != null) {
                    model.setValue(1);
                    modelItems.set(selectedValues.keyAt(i), model);
                }
            }
        }

        listView.setAdapter(adapter);

    }
}
