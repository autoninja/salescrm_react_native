import React, { Component } from 'react';
import {
    Platform, View, Text, Image, TouchableOpacity, Dimensions, BackHandler
} from 'react-native'
import ImageView from 'react-native-image-view';
import styles from './CreateEnquiry.styles'

const w = Dimensions.get('window');

export default class ViewImageScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //data: this.getData(),
            selectedIndex: this.getSelectedIndex(),
            isImageViewVisible: true,
        }
    }

    close() {
        console.log("close getting Called")
        this.props.closeImageScreen();
    }

    getData() {
        return this.props.data;
    }

    getSelectedIndex() {
        return this.props.selectedIndex;
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        return true;
    }

    render() {
        //let { data } = this.state;
        let imageArray = [];
        let dataArray = this.props.data;
        // for(i=0; i<dataArray.length; i++){

        // }
        dataArray.map((item) => {
            if (item.includes('http')) {
                imageArray.push({ source: { uri: item }, width: 806, height: 720, })
            } else {
                imageArray.push({ source: { uri: 'data:image/jpeg;base64,' + item }, width: 806, height: 720, })
            }
        });

        let item = this.props.data[this.state.selectedIndex];
        //console.log("openImageGallery will close", item);
        return (
            // <View style={{ flex: 1 }}>

            //     <View style={{ backgroundColor: '#fff' }}>
            //         <TouchableOpacity
            //             onPress={() => this.close()}
            //         >
            //             <Image source={require('../../../images/ic_close_black.png')} style={{ alignSelf: 'center', width: 30, height: 30 }} />
            //         </TouchableOpacity>
            //     </View>

            //     <View style={{ flex: 1 }}>
            //         {/* {(item.includes('http')) ?
            //             <Image
            //                 source={{ uri: item + `?w=${w.width * 2}&buster=${Math.random()}` }}
            //                 style={{ width: w.width, height: w.width }}
            //                 resizeMode="cover" />
            //             : <Image source={{ uri: 'data:image/jpeg;base64,' + item + `?w=${w.width * 2}&buster=${Math.random()}` }}
            //                 style={{ width: w.width, height: w.width }}
            //                 resizeMode="cover" />} */}
            //         {(item.includes('http')) ?
            //             <Image source={{ uri: item }} style={{ alignSelf: 'center', width: "100%", height: "100%" }} />
            //             : <Image source={{ uri: 'data:image/jpeg;base64,' + item }} style={{ alignSelf: 'center', width: "100%", height: "100%" }} />}

            //     </View>
            // </View>
            <View style={{ flex: 1 }}>
                <ImageView
                    //glideAlways={false}
                    isPinchZoomEnabled={false}
                    images={imageArray}
                    imageIndex={this.state.selectedIndex}
                    isVisible={this.state.isImageViewVisible}
                    onClose={() => this.close()}
                //renderFooter={(currentImage) => (<View><Text>My footer</Text></View>)}
                />
            </View>
        );
    }
}