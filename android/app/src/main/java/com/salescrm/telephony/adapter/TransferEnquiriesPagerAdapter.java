package com.salescrm.telephony.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.salescrm.telephony.fragments.transferenquiry.TransferEnquiriesDistributeFragment;
import com.salescrm.telephony.fragments.transferenquiry.TransferEnquiriesFiltersFragment;
import com.salescrm.telephony.fragments.transferenquiry.TransferEnquiriesUsersFromFragment;
import com.salescrm.telephony.fragments.transferenquiry.TransferEnquiriesUsersToFragment;

/**
 * Created by bharath on 14/8/17.
 */

public class TransferEnquiriesPagerAdapter extends FragmentStatePagerAdapter{

    public TransferEnquiriesPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:return new TransferEnquiriesUsersFromFragment();
            case 1:return new TransferEnquiriesFiltersFragment();
            case 2:return  new TransferEnquiriesUsersToFragment();
            case 3:return new TransferEnquiriesDistributeFragment();
        }
        return null;
    }
    @Override
    public int getCount() {
        return 4;
    }
}
