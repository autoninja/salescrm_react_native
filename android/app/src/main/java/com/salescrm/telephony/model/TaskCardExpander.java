package com.salescrm.telephony.model;

/**
 * Created by bharath on 20/6/17.
 */

public class TaskCardExpander {
    int position;
    int scheduledActivityId;
    boolean isOpen =false;

    public TaskCardExpander(int position, int scheduledActivityId, boolean isOpen) {
        this.position = position;
        this.scheduledActivityId = scheduledActivityId;
        this.isOpen = isOpen;
    }

    public TaskCardExpander() {

    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getScheduledActivityId() {
        return scheduledActivityId;
    }

    public void setScheduledActivityId(int scheduledActivityId) {
        this.scheduledActivityId = scheduledActivityId;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }
}
