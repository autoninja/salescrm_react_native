package com.salescrm.telephony.telephonyModule.model;

/**
 * Created by bannhi on 28/3/17.
 */

public class RecordingsModel {

    private String id;
    private String lead_id;
    private String called_number;
    private String call_start_time;
    private String call_end_time;
    private String call_duration;
    private String Call_hangup_cause;
    private String file_name;
    private String file_created_on;
    private String uuid;
    private String caller_number;
    private String imei_number;
    private String last_modified;
    private String call_type;
    private String call_id;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public String getCalled_number() {
        return called_number;
    }

    public void setCalled_number(String called_number) {
        this.called_number = called_number;
    }

    public String getCall_start_time() {
        return call_start_time;
    }

    public void setCall_start_time(String call_start_time) {
        this.call_start_time = call_start_time;
    }

    public String getCall_end_time() {
        return call_end_time;
    }

    public void setCall_end_time(String call_end_time) {
        this.call_end_time = call_end_time;
    }

    public String getCall_duration() {
        return call_duration;
    }

    public void setCall_duration(String call_duration) {
        this.call_duration = call_duration;
    }

    public String getCall_hangup_cause() {
        return Call_hangup_cause;
    }

    public void setCall_hangup_cause(String call_hangup_cause) {
        Call_hangup_cause = call_hangup_cause;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_created_on() {
        return file_created_on;
    }

    public void setFile_created_on(String file_created_on) {
        this.file_created_on = file_created_on;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCaller_number() {
        return caller_number;
    }

    public void setCaller_number(String caller_number) {
        this.caller_number = caller_number;
    }

    public String getImei_number() {
        return imei_number;
    }

    public void setImei_number(String imei_number) {
        this.imei_number = imei_number;
    }

    public String getLast_modified() {
        return last_modified;
    }

    public void setLast_modified(String last_modified) {
        this.last_modified = last_modified;
    }

    public String getCall_type() {
        return call_type;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    public String getCall_id() {
        return call_id;
    }

    public void setCall_id(String call_id) {
        this.call_id = call_id;
    }
}
