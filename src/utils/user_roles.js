export const USER_ROLE_SALES_CONSULTANT = { id:4, name: 'Sales Consultant' };
export const USER_ROLE_TEAM_LEAD = { id:6, name: 'DSE Team Lead' };
export const USER_ROLE_SALES_MANAGER = { id:8, name: 'Sales Manager' };
export const USER_ROLE_BRANCH_HEAD = { id:21, name: 'Branch Head' };
export const USER_ROLE_OPERATIONS = { id:30, name: 'Operations' };
