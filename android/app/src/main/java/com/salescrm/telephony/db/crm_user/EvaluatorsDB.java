package com.salescrm.telephony.db.crm_user;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bannhi on 24/6/17.
 */

public class EvaluatorsDB extends RealmObject{

    String evaluatorName;
    @PrimaryKey
    String evaluatorId;


    public String getEvaluatorName() {
        return evaluatorName;
    }

    public void setEvaluatorName(String evaluatorName) {
        this.evaluatorName = evaluatorName;
    }

    public String getEvaluatorId() {
        return evaluatorId;
    }

    public void setEvaluatorId(String evaluatorId) {
        this.evaluatorId = evaluatorId;
    }


}
