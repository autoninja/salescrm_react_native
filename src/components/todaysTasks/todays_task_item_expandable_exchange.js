import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'


import TodaysTaskItemChildExchange from './todays_task_item_child_exchange'

export default class TodaysTaskItemExpandableExchange extends Component {
    constructor(props) {
        super(props);
        this.state = { expand: false, showImagePlaceHolder: true };
    }
    _etvbrLongPress(from, info) {
        this.props.onLongPress(from, info)
        console.log(info)
    }
    render() {

        let evaluation_managers = this.props.evaluation_managers;
        let imageSource;
        let placeHolder

        imageSource = { uri: evaluation_managers.info.dp_url };
        //console.log("evaluation_managers.info.name", evaluation_managers.info.name);
        placeHolder = <View style={{ position: 'absolute', display: (this.state.showImagePlaceHolder ? 'flex' : 'none'), width: 36, height: 36, borderRadius: 36 / 2, backgroundColor: '#FF8000', alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#fff' }}>{(evaluation_managers.info.name) ? evaluation_managers.info.name.charAt(0).toUpperCase() : ""}</Text></View>;;


        let sc_views = []
        for (var i = 0; i < evaluation_managers.evaluators.length; i++) {
            var sc = evaluation_managers.evaluators[i];
            sc_views.push(

                <TodaysTaskItemChildExchange
                    openTasksList={this.props.openTasksList.bind(this)}
                    onLongPress={this.props.onLongPress.bind(this)}
                    key={sc.info.id}

                    abs={
                        {
                            ed: sc.info.abs.evaluations,
                            pq: sc.info.abs.price_negotiations,
                            pending_total_count: sc.info.abs.pending_total_count,

                        }}

                    info={
                        {
                            id: sc.info.id,
                            dp_url: sc.info.dp_url,
                            name: sc.info.name,
                            user_id: sc.info.id,
                            location_id: this.props.info.location_id,
                            user_role_id: 4,
                        }
                    }


                ></TodaysTaskItemChildExchange>)

        }

        return (
            <View style={{ marginBottom: 8 }}>
                <TouchableOpacity onPress={() => this.setState({ expand: !this.state.expand })}>
                    <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#eceff1', borderColor: '#bdbdbd', borderWidth: 0.3, borderRadius: 6, alignItems: 'center' }}>

                        <View style={{ flex: 0.7, alignItems: 'center' }} >

                            <View style={{ flex: 0.7, alignItems: 'center', justifyContent: 'center' }}>
                                {placeHolder}
                                <TouchableOpacity
                                    onPress={() => this.setState({ expand: !this.state.expand })}
                                    style={{ position: 'absolute', }}
                                    onLongPress={() => this.props.onLongPress('user_info', { info: { name: this.props.info.name, dp_url: this.props.info.dp_url } })}>
                                    <Image
                                        source={imageSource}
                                        style={{ width: 36, height: 36, borderRadius: 36 / 2, }}
                                    />
                                </TouchableOpacity>
                            </View>





                        </View>





                        <View style={{
                            flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                            borderLeftWidth: 0.3
                        }}>
                            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#303030' }} >{this.props.abs.ed}</Text>
                            </View>


                        </View>

                        <View style={{
                            flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                            borderLeftWidth: 0.3
                        }}>
                            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#303030' }} >{this.props.abs.pq}</Text>
                            </View>

                        </View>


                        <View style={{
                            flex: 1, height: '100%', borderLeftColor: '#cfd8dc',
                            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
                        }}>

                            <Text style={{ color: '#F5A623' }} >{this.props.abs.pending_total_count}</Text>
                        </View>
                        {
                            /*
                            <View style= {{flex:1,height:'100%',borderLeftColor: '#cfd8dc',
                              borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                                <TouchableOpacity  hitSlop={{top: 10, bottom: 10, left: 10, right: 10}} onLongPress={ () => this.props.onLongPress('Uncalled Tasks',{info: this.props.info})}>
                                  <Text style={{ color:'red',textDecorationLine: 'underline',
                                    textDecorationColor: '#F20407'}} >{this.props.abs.uncalled_tasks}</Text>
                                </TouchableOpacity>
                            </View>
                            */
                        }





                    </View>
                </TouchableOpacity>

                {this.state.expand && (
                    <View>
                        {sc_views}
                    </View>
                )}
            </View>
        );
    }
}
