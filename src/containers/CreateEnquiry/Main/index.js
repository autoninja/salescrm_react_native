import React, { Component } from 'react';
import Geolocation from '@react-native-community/geolocation';
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import {
  DeviceEventEmitter,
  Dimensions,
  AppRegistry,
  StyleSheet,
  Text,
  View, Picker, Platform, Button, FlatList, Alert,
  ScrollView, TouchableOpacity, Image, Switch, ActivityIndicator,
  RefreshControl, NativeModules,
  TouchableWithoutFeedback, TextInput, KeyboardAvoidingView, NavigationActions
} from 'react-native';
import Toast, { DURATION } from 'react-native-easy-toast'

import CreateLeadExpander from '../../../components/createLead/create_lead_expander'
import CreateLeadButtonSelector from '../../../components/createLead/create_lead_button_selector'
import CreateLeadTagSelector from '../../../components/createLead/create_lead_tag_selector'
import ItemPicker from '../../../components/createLead/item_picker'
import CustomTextInput from '../../../components/createLead/custom_text_input'
import CustomButton from '../../../components/createLead/custom_button'
import PickerList from '../../../utils/PickerList'
import CustomDateTimePicker from '../../../utils/DateTimePicker'
import RadioButton from '../../../components/uielements/RadioButton';
import Utils from '../../../utils/Utils'
import CreateEnquiryModel from './CreateEnquiryModel'

import RestClient from '../../../network/rest_client'
import styles from './CreateEnquiry.styles'
//import ExchangeCar from './exchange_car';
import NavigationService from '../../../navigation/NavigationService'

const CUSTOMER_TYPES = [
  { id: 'corporate_individual', server_id: 1, text: 'Corporate(Individual)' },
  { id: 'individual', server_id: 2, text: 'Individual' },
  { id: 'government', server_id: 3, text: 'Government' },
  { id: 'corporate_company', server_id: 4, text: 'Corporate(Company)' },
  { id: 'fleet_individual', server_id: 5, text: 'Fleet(Individual)' },
  { id: 'fleet_company', server_id: 6, text: 'Fleet(Company)' }
]

const CUSTOMER_TITLES = [
  'Mr',
  'Miss',
  'M/S',
  'Dr',
]
const CUSTOMER_ICONS = [
  { iconActive: require('../../../images/mr_white.png'), iconInActive: require('../../../images/mr.png') },
  { iconActive: require('../../../images/miss_white.png'), iconInActive: require('../../../images/miss.png') },
  { iconActive: require('../../../images/ms_white.png'), iconInActive: require('../../../images/ms.png') },
  { iconActive: require('../../../images/doctor_white.png'), iconInActive: require('../../../images/doctor.png') }
]
const ADDRESS_TYPES = [
  { id: 1, text: 'Office address' },
  { id: 2, text: 'Home address' }
]
const MODE_OF_PAYMENTS = [
  { id: 'full_cash', text: 'Full Cash' },
  { id: 'company_lease_plan', text: 'Company Lease Plan' },
  { id: 'in_house_loan', text: 'In House Loan' },
  { id: 'out_house_loan', text: 'Out House Loan' }
]
const LEAD_TAG_COLORS = [
  { id: 6, text: 'Hot', color: '#f7412d' },
  { id: 7, text: 'Warm', color: '#ff9900' },
  { id: 8, text: 'Cold', color: '#00a7f7' }
]

const ACTIVITY_IDS = {
  FOLLOWUP_CALL_ID: 1,
  HOME_VISIT_ID: 2,
  SHOWROOM_VISIT_ID: 3,
  CONDUCT_TEST_DRIVE_SHOWROOM_ID: 5,
  CONDUCT_TEST_DRIVE_HOME_ID: 6,
  EVALUATION_REQUEST_ID: 10,
  LOST_DROP_ID: 24,
  FIRST_CALL_ID: 33,
  CRE_FOLLOW_UP_CALL: 53,
}


export default class CreateEnquiry extends Component {

  constructor(props) {
    super(props);
    this.state = {
      enquiryFields: [
        { id: 'buyer_type', title: 'Buyer Type', isExpanded: false, data: { showroomLocation: this.getLocations().length == 1 ? this.getLocations()[0] : null } },
        { id: 'customer_details', title: 'Customer Details', isExpanded: false, data: { primaryMobile: this.getPrimaryMobile(), customerAddressType: ADDRESS_TYPES[0], secondoryMobile: [], secondoryEmail: [] } },
        { id: 'lead_info', title: 'Lead Type', isExpanded: false, data: {} },
        { id: 'interested_vehicle', title: 'Interested  Vehicle', isExpanded: false, data: [{}] },
        { id: 'extra_vehicle', title: '', isExpanded: false, data: [{}] },
        { id: 'assign_dse', title: 'Assign SC', isExpanded: false, data: { isClientTwoWheeler: this.isClientTwoWheeler(), salesConsultant: this.autoPopulateConsultant() } }
      ],
      showItemPickerFilter: false,
      showItemPicker: false,
      dataItemPicker: {},
      interestedVehicle: this.getInterestedVehicle(),
      allVehicles: this.getAllVehicle(),
      dateTimePickerVisibility: false,
      dateTimePickerPayload: {},
      locations: this.getLocations(),
      leadSourceCategories: this.getLeadSourceCategories(),
      leadSourceMain: this.getLeadSourceMain(),
      showEnquirySourceCategory: this.isShowEnquirySourceCategory(),
      loading: false,
      mapRegion: null,
      lastLat: null,
      lastLong: null,
      currentLocationHolder: "Getting your location, please wait",
      addressFetchingFromLatLong: true,
      isReverseAddressValid: false,
      hasEvaluationManager: this.getLocations().length == 1 ? this.getEvaluationManager(this.getLocations()[0]) : false,
    }

    //console.log("userLocations", this.props.userLocations)
    //console.log("evaluationManagers", this.props.evaluationManagers)
  }
  watchID = null;
  componentDidMount() {
    if (this.props.fromAndroid) {
      NativeModules.ReactNativeToAndroid.componentDidMount("CREATE_LEAD");
    }
    if (Platform.OS == 'ios') {
      // this.watchID = navigator.geolocation.watchPosition((position) => {
      //   // Create the object to update this.state.mapRegion through the onRegionChange function
      //   let region = {
      //     latitude: position.coords.latitude,
      //     longitude: position.coords.longitude,
      //     latitudeDelta: 0.00922 * 1.5,
      //     longitudeDelta: 0.00421 * 1.5
      //   }
      //   this.onRegionChange(region, region.latitude, region.longitude);
      //   console.error(region);
      // }, (error) => console.error(error));
      Geolocation.requestAuthorization();
      Geolocation.getCurrentPosition(
        position => {
          let region = {
            latitude: parseFloat(position.coords.latitude),
            longitude: parseFloat(position.coords.longitude),
            latitudeDelta: 0.00922 * 1.5,
            longitudeDelta: 0.00421 * 1.5
          }
          this.onRegionChange(region, region.latitude, region.longitude);
          this.requestAddressFromLatLong(region.latitude + ',' + region.longitude)
        },
        error => Alert.alert('Error Getting the location, please enable the permission'),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
      );
      // this.watchID = Geolocation.watchPosition(position => {
      //   const lastPosition = JSON.stringify(position);
      //   this.setState({ lastPosition });
      // });

    }
    else {
      this.subscription = DeviceEventEmitter.addListener('onGettingLocation', function (e: Event) {

        if (e) {
          let region = {
            latitude: parseFloat(e.LATITUDE),
            longitude: parseFloat(e.LONGITUDE),
            latitudeDelta: 0.00922 * 1.5,
            longitudeDelta: 0.00421 * 1.5
          }
          this.onRegionChange(region, region.latitude, region.longitude);
          this.requestAddressFromLatLong(region.latitude + ',' + region.longitude)
        }
      }.bind(this));
    }




  }


  componentWillUnmount() {
    if (Platform.OS == 'ios') {
      //this.watchID != null && Geolocation.clearWatch(this.watchID);
    }
    if (this.props.fromAndroid) {
      NativeModules.ReactNativeToAndroid.componentWillUnmount("CREATE_LEAD");
    }
  }

  onRegionChange(region, lastLat, lastLong) {
    this.setState({
      mapRegion: region,
      // If there are no new values set the current ones
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong
    });
  }
  getPrimaryMobile() {
    if (Platform.OS == "ios") {
      return this.props.navigation.getParam('primaryMobile')
    }
    else {
      return this.props.primaryMobile + '';
    }
  }
  autoPopulateConsultant() {
    let user = null;
    if (this.getLocations().length == 1) {
      let salesConsultants = this.getSalesConsultantsItems(this.getLocations()[0]);
      salesConsultants.map((salesConsultant) => {
        if (salesConsultant.id == global.appUserId) {
          user = salesConsultant;
          return true;
        }
      })
    }
    return user;
  }


  getSalesConsultantsItems(locationParam) {
    let salesConsultants = [];
    let location
    if (locationParam) {
      location = locationParam;
    }
    else if (this.state && this.state.enquiryFields) {
      location = this.state.enquiryFields[0].data.showroomLocation;
    }
    if (location) {
      let teamUser = null;
      this.props.teams.map((team) => {
        if (team.location_id == location.id) {
          teamUser = team;
          return;
        }
      });
      if (teamUser) {
        teamUser.teams.map((team) => {
          let id = team.user_id;
          let team_leader_id = team.team_leader_id;
          let team_leader_name = "";
          for (var i = 0; i < teamUser.users.length; i++) {
            if (team_leader_id == teamUser.users[i].id) {
              team_leader_name = teamUser.users[i].name;
              break;
            }
          }
          for (var i = 0; i < teamUser.users.length; i++) {
            if (id == teamUser.users[i].id) {
              salesConsultants.push({ id, text: teamUser.users[i].name, role_id: teamUser.users[i].role_id, team_leader_id, team_leader_name });
              break;
            }
          }
        })
      }
    }
    return salesConsultants
  }

  getActivityItems() {
    let actvitiesItem = [];
    let activities = this.props.activities;
    let isClientTwoWheeler = this.isClientTwoWheeler();
    activities.map((item) => {
      if (isClientTwoWheeler) {
        if (item.id == ACTIVITY_IDS.FIRST_CALL_ID) {
          actvitiesItem.push({ id: item.id, text: item.name });
        }
      }
      else {
        if (item.id != ACTIVITY_IDS.LOST_DROP_ID
          && item.id != ACTIVITY_IDS.EVALUATION_REQUEST_ID
          && item.id != ACTIVITY_IDS.CRE_FOLLOW_UP_CALL) {
          actvitiesItem.push({ id: item.id, text: item.name });
        }
      }

    })
    return actvitiesItem;
  }

  getShowRoomVisitItems() {
    let showroomItems = [];
    let showroomItemsDB = this.props.allLocations;
    showroomItemsDB.map((showroom) => {
      showroomItems.push({ id: showroom.id, text: showroom.name });
    })
    return showroomItems;
  }
  getTestDriveCars() {
    let testDriveCarItems = [];
    let { enquiryFields } = this.state;
    enquiryFields[3].data.map((vehicle) => {
      if (vehicle && vehicle.vehicleModel) {
        testDriveCarItems.push({ id: vehicle.vehicleModel.id, text: vehicle.vehicleModel.text });
      }
    })
    return testDriveCarItems;
  }
  isShowEnquirySourceCategory() {
    return this.props.isShowEnquirySourceCategory;
  }

  isClientTwoWheeler() {
    return this.props.isClientTwoWheeler;
  }

  isVinNumberMandatory() {
    let { enquiryFields } = this.state;
    return enquiryFields[2].data.enquirySource && enquiryFields[2].data.enquirySource.id == '37';
  }

  getLocations() {
    if (this.props.fromAndroid) {

    }
    else {

    }
    return this.props.userLocations;

  }

  getLeadSourceCategories() {
    return this.props.enquirySourceCategories;
  }

  getLeadSourceCategoriesItems() {
    let { leadSourceCategories, enquiryFields } = this.state;
    let list = [];
    if (this.getEventItems().length > 0 && enquiryFields[2].data.eventSelected) {

      let eventsMain = this.props.activeEvents;
      let locationId = enquiryFields[0].data.showroomLocation ? enquiryFields[0].data.showroomLocation.id : null;
      eventsMain.map((event) => {
        if (event.locationId == locationId) {
          event.eventsLeadSourceCategories.map((eventSourceCategory) => {
            let leadSourcesTemp = [];
            eventSourceCategory.leadSources.map((leadSource) => {
              leadSource.events.map((eventItem) => {
                if (eventItem.id == enquiryFields[2].data.eventSelected.id) {
                  //Select LeadSource Categories
                  leadSourcesTemp.push({ id: leadSource.id, name: leadSource.name })
                }
              })
            })
            if (leadSourcesTemp.length > 0) {
              list.push({
                id: eventSourceCategory.id,
                text: eventSourceCategory.name,
                payload: leadSourcesTemp
              })
            }
          })
        }
      })
    }
    else {
      leadSourceCategories.map((item) => {
        list.push({ id: item.id, text: item.name, payload: item.subEnquirySource })
        //console.log("Payload SubEnquirySource");
        //console.log("Payload SubEnquirySource" + JSON.stringify(item));
      })
    }

    return list;
  }
  getLeadSourceItems() {
    let { leadSourceMain, showEnquirySourceCategory, enquiryFields } = this.state;
    let list = [];

    if (showEnquirySourceCategory && enquiryFields[2].data.enquirySourceCategory && enquiryFields[2].data.enquirySourceCategory.payload) {
      enquiryFields[2].data.enquirySourceCategory.payload.map((item) => {
        list.push({ id: item.id, text: item.name })
      })
    }
    else if (!showEnquirySourceCategory) {
      leadSourceMain.map((item) => {
        list.push({ id: item.id, text: item.name })
      });
    }
    return list;
  }

  getEventItems() {
    let { enquiryFields } = this.state;
    let eventItems = [];
    let eventsMain = this.props.activeEvents;
    let locationId = enquiryFields[0].data.showroomLocation ? enquiryFields[0].data.showroomLocation.id : null;
    if (eventsMain && this.isShowEnquirySourceCategory()
      && locationId) {
      eventsMain.map((event) => {
        if (event.locationId == locationId) {
          event.eventsLeadSourceCategories.map((eventSourceCategory) => {
            eventSourceCategory.leadSources.map((leadSource) => {
              leadSource.events.map((eventItem) => {
                if (eventItems.map(item => item.id).indexOf(eventItem.id) == -1) {
                  eventItems.push({ id: eventItem.id, text: eventItem.name });
                }
              })
            })
          })
        }
      })
    }
    return eventItems;
  }

  getLeadSourceMain() {
    return this.props.enquirySourceMain;
  }

  getInterestedVehicle() {
    if (this.props.fromAndroid) {

    }
    return this.props.interestedVehicle;
  }
  getMainLeadSource() {

  }
  getAllVehicle() {
    if (this.props.fromAndroid) {

    }
    return this.props.allVehicles;
  }

  getShowRoomItems() {
    let { locations } = this.state;
    return locations;
  }
  getInterestedVehicleModelsItems() {
    let { interestedVehicle } = this.state;
    let list = [];
    if (interestedVehicle) {
      interestedVehicle.brand_models.map((model) => {
        list.push({ id: model.model_id, text: model.model_name, payload: { brand_id: interestedVehicle.brand_id, brand_name: interestedVehicle.brand_name } });
      })
    }
    return list;
  }
  getAllVehicleBrandItems() {
    let { allVehicles } = this.state;
    let list = [];
    if (allVehicles) {
      allVehicles.map((brand) => {
        list.push({ id: brand.brand_id, text: brand.brand_name, models: brand.brand_models });
        //console.log("Brand Models. SIze:" + brand.brand_models.length);
      });
    }
    return list;
  }
  getAllVehicleModelsItems(extraVehicle) {
    let list = [];
    if (extraVehicle && extraVehicle.vehicleBrand) {
      extraVehicle.vehicleBrand.models.map((model) => {
        list.push({ id: model.model_id, text: model.model_name, payload: { brand_id: extraVehicle.vehicleBrand.id } });
      });
    }
    return list;
  }
  getInterestedVehicleVariantsItems(modelItem) {
    let { interestedVehicle, enquiryFields } = this.state;
    let list = [];
    if (interestedVehicle && modelItem) {
      interestedVehicle.brand_models.map((model) => {
        if (model.model_id == modelItem.id) {
          model.car_variants.map((variant) => {
            if (list.map(item => item.id).indexOf(variant.variant_id) == -1) {
              list.push({ id: variant.variant_id, text: variant.variant, payload: variant });
            }
          })
        }
      })
    }
    return list;
  }


  getInterestedVehcileColorsItems(modelItem, varaintItem) {
    let { interestedVehicle, enquiryFields } = this.state;
    let list = [];
    if (interestedVehicle && modelItem && varaintItem) {
      interestedVehicle.brand_models.map((model) => {
        if (model.model_id == modelItem.id) {
          model.car_variants.map((variant) => {
            if (variant.variant_id == varaintItem.id) {
              list.push({ id: variant.color_id, text: variant.color });
            }
          })
        }
      })
    }
    return list;
  }

  setBuyerType(buyerType) {
    var enquiryFields = this.state.enquiryFields;
    if (!enquiryFields[0].data.buyerType || enquiryFields[0].data.buyerType != buyerType) {
      enquiryFields[0].data.buyerType = buyerType;
      enquiryFields[4].data = [{}];
      this.setState({ enquiryFields });
    }
  }

  setCustomerTitle(title) {
    var enquiryFields = this.state.enquiryFields;
    enquiryFields[1].data.customerTitle = title;
    this.setState({ enquiryFields });
  }

  showPicker(data, isSearchShown) {
    let showItemPickerFilter = false || isSearchShown;
    this.setState({ showItemPicker: true, dataItemPicker: data, showItemPickerFilter })
  }


  getPincodeDetails(pincode) {
    var enquiryFields = this.state.enquiryFields;
    enquiryFields[1].data.pincodeDetails = [];
    enquiryFields[1].data.isPincodeValid = false;
    this.setState({ pincodeFetching: true, enquiryFields });
    let apiKey = this.props.govtAPIKeys[Utils.getRandomInt(0, this.props.govtAPIKeys.length - 1)];
    //console.log("API KEY:" + apiKey)
    new RestClient().getPincodeDetailsInternal(pincode).then((data) => {

      if (data && data.result && data.result.pindata && data.result.pindata.length > 0) {
        let pinData = data.result.pindata;
        enquiryFields[1].data.customerState = pinData[0].statename;
        enquiryFields[1].data.customerCity = pinData[0].districtname;
        if (pinData.length == 1) {
          enquiryFields[1].data.customerLocality = { text: pinData[0].locality }
        }
        enquiryFields[1].data.pincodeDetails = pinData;
        enquiryFields[1].data.isPincodeValid = true;
        this.setState({ pincodeFetching: false, enquiryFields })
      }
      else {
        this.showAlert('Please enter a valid pincode');
        this.setState({ pincodeFetching: false })
      }

    }).catch(error => {
      console.log(error);
      if (!error.response) {
        this.showAlert('No Interent Connection');
      }
      else {
        this.showAlert('Please enter a valid pincode');
      }
      this.setState({ pincodeFetching: false });
    });

  }

  getVisitEditText(item) {
    return (
      <CustomTextInput
        hint='Address *'
        text={item.data.visitAddress}
        onInputTextChanged={(from, text, payload) => {
          let { enquiryFields } = this.state;
          enquiryFields[5].data.visitAddress = text
          this.setState(enquiryFields);
        }} />
    )
  }

  getViews(item, index) {
    if (!item.isExpanded) {
      return <View />
    }
    switch (item.id) {
      case 'customer_details': {
        let customerType = "Customer type *";
        let modeOfPayment = "Mode of payment";
        let customerAddressType = "Address Type";
        if (item.data.customerAddressType) {
          customerAddressType = item.data.customerAddressType.text;
        }
        if (item.data.customerType) {
          customerType = item.data.customerType.text;
        }
        if (item.data.modeOfPayment && item.data.modeOfPayment.text) {
          modeOfPayment = item.data.modeOfPayment.text;
        }
        let pincodeDetails = item.data.pincodeDetails;
        let customerLocality = "Select Locality *";
        let customerLocalities = [];
        if (item.data.customerLocality) {
          customerLocality = item.data.customerLocality.text;
        }

        if (pincodeDetails && pincodeDetails.length > 0) {
          pincodeDetails.map((record) => {
            customerLocalities.push({ text: record.locality });
          });
        }
        if (item.isExpanded) {
          return (
            <View style={[{ display: (item.isExpanded ? 'flex' : 'none') }, styles.field]}>
              <CustomTextInput
                editable={false}
                selectTextOnFocus={false}
                keyboardType='numeric'
                from='phone_number'
                maxLength={10}
                hint={"Phone number"} text={item.data.primaryMobile}
                onInputTextChanged={this.onInputTextChanged.bind(this)} />
              {item.data.secondoryMobile.map((item, i) => {
                return (
                  <CustomTextInput
                    key={i + ''}
                    keyboardType='numeric'
                    showRemove={true}
                    from='secondary_mobile' maxLength={10}
                    hint={"Secondary number"}
                    text={item}
                    onInputTextChanged={(from, text, payload) => {
                      let enquiryFields = this.state.enquiryFields;
                      enquiryFields[1].data.secondoryMobile[i] = text;
                      this.setState({ enquiryFields });
                    }}
                    onRemoveEditText={() => {
                      let enquiryFields = this.state.enquiryFields;
                      enquiryFields[1].data.secondoryMobile.splice(i, 1);
                      this.setState({ enquiryFields });
                    }}
                  />
                )
              })}
              <Text style={styles.floatRight} onPress={() => {
                if (item.data.secondoryMobile.length == 0
                  || item.data.secondoryMobile[item.data.secondoryMobile.length - 1].length == 10) {
                  let enquiryFields = this.state.enquiryFields;
                  enquiryFields[1].data.secondoryMobile.push('');
                  this.setState({ enquiryFields });
                }
                else {
                  this.showAlert("Please add a valid number");
                }

              }}> Add Secondary number</Text>
              <ItemPicker title={customerType} onClick={this.showPicker.bind(this, { title: 'Customer type', fromId: 'customer_type', listItems: CUSTOMER_TYPES })} />
              <ScrollView style={{ marginTop: 20 }} horizontal={true} showsHorizontalScrollIndicator={false}>
                {CUSTOMER_TITLES.map((customerTitle, customerIndex) => {
                  return (<CreateLeadButtonSelector key={customerTitle} icons={CUSTOMER_ICONS[customerIndex]} style={{ width: 100 }} id={customerTitle} title={customerTitle} selected={item.data.customerTitle} onClick={this.setCustomerTitle.bind(this)} />)
                })}
              </ScrollView>
              <CustomTextInput hint='First name * ' from='customer_name_first' text={item.data.customerNameFirst} onInputTextChanged={this.onInputTextChanged.bind(this)} />
              {(!item.data.customerType || item.data.customerType.id != 'corporate_company') && <CustomTextInput hint='Last name' from='customer_name_last' text={item.data.customerNameLast} onInputTextChanged={this.onInputTextChanged.bind(this)} />}
              <CustomTextInput keyboardType='numeric' hint='Age' from='customer_age' maxLength={3} text={item.data.customerAge} onInputTextChanged={this.onInputTextChanged.bind(this)} />
              <CustomTextInput hint='Email *' autoCapitalize='none' keyboardType='email-address' from='customer_email' text={item.data.customerEmail} onInputTextChanged={this.onInputTextChanged.bind(this)} />

              {item.data.secondoryEmail.map((item, i) => {
                return (
                  <CustomTextInput
                    key={i + ''}
                    keyboardType='email-address'
                    autoCapitalize='none'
                    showRemove={true}
                    from='secondary_email'
                    hint={"Secondary email"}
                    text={item}
                    onInputTextChanged={(from, text, payload) => {
                      let enquiryFields = this.state.enquiryFields;
                      enquiryFields[1].data.secondoryEmail[i] = text;
                      this.setState({ enquiryFields });
                    }}
                    onRemoveEditText={() => {
                      let enquiryFields = this.state.enquiryFields;
                      enquiryFields[1].data.secondoryEmail.splice(i, 1);
                      this.setState({ enquiryFields });
                    }}
                  />
                )
              })}
              <Text style={styles.floatRight} onPress={() => {
                let regEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                let enquiryFields = this.state.enquiryFields;
                if (regEmail.test(enquiryFields[1].data.customerEmail)
                  && (enquiryFields[1].data.secondoryEmail.length == 0 || (enquiryFields[1].data.secondoryEmail.length > 0 &&
                    regEmail.test(enquiryFields[1].data.secondoryEmail[enquiryFields[1].data.secondoryEmail.length - 1])))) {
                  enquiryFields[1].data.secondoryEmail.push('');
                  this.setState({ enquiryFields });
                }
                else {
                  this.showAlert("Please add a valid email to add more")
                }

              }}> Add Secondary Email</Text>

              <CustomTextInput hint={this.isCustomerTypeCompany() ? 'Company *' : 'Company'} from='customer_company' text={item.data.customerCompany} onInputTextChanged={this.onInputTextChanged} />
              <CustomTextInput hint={this.isCustomerTypeCompany() ? 'Designation *' : 'Designation'} from='customer_designation' text={item.data.customerDesignation} onInputTextChanged={this.onInputTextChanged} />
              <View style={[styles.shadowsView]}>
                <View style={[styles.shadowsViewInner, { backgroundColor: '#eceff1' }]}>
                  <ItemPicker title={customerAddressType} onClick={this.showPicker.bind(this, { fromId: 'customer_address_type', listItems: ADDRESS_TYPES })} />
                  <View>

                    <CustomTextInput editable={!this.state.pincodeFetching} keyboardType='numeric' hint='Pincode *' from='customer_pincode' maxLength={6} text={item.data.customerPincode} onInputTextChanged={this.onInputTextChanged} />
                    <CustomTextInput hint='Address' from='customer_address' text={item.data.customerAddress} onInputTextChanged={this.onInputTextChanged} />
                    <ItemPicker title={customerLocality} onClick={this.showPicker.bind(this, { fromId: 'customer_locality', listItems: customerLocalities })} />
                    <CustomTextInput editable={false} hint='City' from='customer_city' text={item.data.customerCity} onInputTextChanged={this.onInputTextChanged} />
                    <CustomTextInput editable={false} hint='State' from='customer_state' text={item.data.customerState} onInputTextChanged={this.onInputTextChanged} />
                    {this.state.pincodeFetching && <View style={[styles.loading, { position: 'absolute' }]}>
                      <ActivityIndicator size="small" color="#007fff" />
                    </View>}
                  </View>
                </View>
              </View>
              <ItemPicker title={item.data.expectedBuyingDate ? Utils.getReadableDate(item.data.expectedBuyingDate) : 'Expected buying date'}
                onClick={() => {
                  this.setState({
                    dateTimePickerPayload: { from: 'expected_buying_date' },
                    dateTimePickerVisibility: true, dateTimePickerMinimumDate: new Date()
                  })
                }}
              />
              <ItemPicker title={modeOfPayment} onClick={this.showPicker.bind(this, { title: 'Mode of payment', fromId: 'mode_of_payment', listItems: MODE_OF_PAYMENTS })} />
            </View>
          )
        }

      }
      case 'buyer_type': {
        //console.log("buyerTypeId " + this.state.enquiryFields[0].data.buyerType)

        let showroomLocation = "Location *"
        if (item.data.showroomLocation) {
          showroomLocation = item.data.showroomLocation.text;
        }
        let { hasEvaluationManager } = this.state;
        let enquiryFields = this.state.enquiryFields;
        //console.log("hasEvaluationManagerrrrrrr", hasEvaluationManager)
        return (
          <View style={[{ display: (item.isExpanded ? 'flex' : 'none') }, styles.field]}>
            <ItemPicker title={showroomLocation} onClick={this.showPicker.bind(this, { title: 'Location', fromId: 'showroom_location', listItems: this.getShowRoomItems() })} />
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
              <CreateLeadButtonSelector id="1"
                disabled={false}
                icons={{ iconActive: require('../../../images/first_time_white.png'), iconInActive: require('../../../images/first_time.png') }}
                title="First Time" selected={item.data.buyerType} onClick={this.setBuyerType.bind(this)} />
              <CreateLeadButtonSelector id="3"
                disabled={hasEvaluationManager}
                title="Exchange"
                icons={{ iconActive: require('../../../images/exchange_white.png'), iconInActive: require('../../../images/exchange.png') }}
                selected={item.data.buyerType} onClick={this.setBuyerType.bind(this)} />
            </View>
            <View style={{ flexDirection: 'row' }}>
              <CreateLeadButtonSelector id="2"
                disabled={false}
                title="Additional"
                icons={{ iconActive: require('../../../images/additional_white.png'), iconInActive: require('../../../images/additional.png') }}
                selected={item.data.buyerType} onClick={this.setBuyerType.bind(this)} />
              <View style={{ flex: 1 }} />
            </View>
            <View style={{ flexDirection: 'row', marginTop: 10 }}>
              {LEAD_TAG_COLORS.map((leadColor) => {
                return (<CreateLeadTagSelector key={leadColor.id + ''} selected={item.data.leadTagId} item={leadColor} onClick={(item) => {
                  //let enquiryFields = this.state.enquiryFields;
                  enquiryFields[0].data.leadTagId = item.id;
                  this.setState({ enquiryFields });
                }} />)
              })}
            </View>
          </View>
        )
      }

      case 'lead_info': {
        let enquirySourceCategory = "Lead source category *"
        let enquirySource = "Lead source *"
        let eventSelectedName = "Event Lead ? Select Event name";
        let vinNumber = "";
        let showEnquirySourceCategory = this.state.showEnquirySourceCategory;
        let eventItems = this.getEventItems();

        let currentLocationHolder = this.state.currentLocationHolder;

        if (item.data.vinNumber) {
          vinNumber = item.data.vinNumber;
        }
        if (item.data.enquirySourceCategory) {
          enquirySourceCategory = item.data.enquirySourceCategory.text;
        }
        if (item.data.enquirySource) {
          enquirySource = item.data.enquirySource.text;
        }
        if (item.data.eventSelected) {
          eventSelectedName = item.data.eventSelected.text;
        }
        return (
          <View style={[{ display: (item.isExpanded ? 'flex' : 'none') }, styles.field]}>
            {eventItems.length > 0 &&
              <ItemPicker
                title={eventSelectedName}
                onClick={this.showPicker.bind(this, { title: 'Event', fromId: 'event_selected', listItems: eventItems })}
                showClearButton={item.data.eventSelected ? true : false}
                hideDownArrow={item.data.eventSelected ? true : false}
                onClear={() => {
                  let { enquiryFields } = this.state;
                  enquiryFields[2].data.eventSelected = null
                  enquiryFields[2].data.enquirySourceCategory = null;
                  enquiryFields[2].data.enquirySource = null;
                  enquiryFields[2].data.vinNumber = "";
                  this.setState(enquiryFields);
                }}
              />
            }
            {showEnquirySourceCategory &&
              <ItemPicker disabled={eventItems > 0 && !item.data.eventSelected} title={enquirySourceCategory} onClick={this.showPicker.bind(this, { title: 'Lead source category', fromId: 'enquiry_source_category', listItems: this.getLeadSourceCategoriesItems() })} />
            }
            <ItemPicker disabled={showEnquirySourceCategory && !item.data.enquirySourceCategory} title={enquirySource} onClick={this.showPicker.bind(this, { title: 'Lead source', fromId: 'enquiry_source', listItems: this.getLeadSourceItems() })} />
            {this.isVinNumberMandatory() &&
              <CustomTextInput
                hint='Vin number *'
                from='vin number'
                text={vinNumber}
                onInputTextChanged={(from, text, payload) => {
                  let { enquiryFields } = this.state;
                  enquiryFields[2].data.vinNumber = text
                  this.setState(enquiryFields);
                }} />
            }
            <View style={styles.mapViewHolder}>
              <View style={{ flexDirection: 'row' }}>
                {this.state.addressFetchingFromLatLong && (<ActivityIndicator style={{ marginLeft: 10 }} size="small" color="#007fff" />)}

                <Text style={styles.mapViewLocation}>{currentLocationHolder}</Text>
              </View>
              <MapView
                scrollEnabled={false}
                style={styles.mapView}
                region={this.state.mapRegion}
              >
                {
                  this.state.lastLat &&
                  (<Marker
                    coordinate={{
                      latitude: this.state.lastLat,
                      longitude: this.state.lastLong
                    }}
                  />)
                }

              </MapView>
              {
                // <Marker
                //   coordinate={{
                //   latitude: (this.state.lastLat + 0.00050) || -36.82339,
                //   longitude: (this.state.lastLong + 0.00050) || -73.03569,
                // }}>
                // <View>
                //     <Text style={{color: '#000'}}>
                //       { this.state.lastLong } / { this.state.lastLat }
                //     </Text>
                // </View>
                // </Marker>
              }
            </View>
          </View>
        )
      }
      case 'interested_vehicle': {
        let flatListInterestedVehicle;
        const VEHICLE_WIDTH = Dimensions.get('window').width - 50;
        return (
          <View style={[{ display: (item.isExpanded ? 'flex' : 'none') }, styles.field]}>
            <FlatList
              ref={ref => flatListInterestedVehicle = ref}
              getItemLayout={(data, pos) => ({ index: pos, length: VEHICLE_WIDTH, offset: VEHICLE_WIDTH * pos })}
              horizontal={true}
              style={{ flex: 1, width: '100%', }}
              data={item.data}
              renderItem={({ item: atom, index: i }) => {
                let model = "Model *";
                let variant = "Variant";
                let fuelType = "Fuel Type";
                let color = "Color/Description";
                //console.log("I::" + i);
                if (atom) {
                  //console.log("Atom");
                  //console.log(JSON.stringify(atom));
                  if (atom.vehicleModel) {
                    model = atom.vehicleModel.text;
                  }
                  if (atom.vehicleVariant) {
                    variant = atom.vehicleVariant.text;
                  }
                  if (atom.vehicleFuelType) {
                    fuelType = atom.vehicleFuelType.text;
                  }
                  if (atom.vehicleColor) {
                    color = atom.vehicleColor.text;
                  }
                }

                return (
                  <View styles={{ marginLeft: 10 }}>

                    <View style={[styles.shadowsView, { width: VEHICLE_WIDTH }]}>
                      <View style={[styles.shadowsViewInner]}>
                        <TouchableOpacity
                          disabled={i == 0}
                          style={{ alignSelf: 'flex-end' }}
                          onPress={() => {
                            let { enquiryFields } = this.state;
                            if (enquiryFields[3].data[i].vehicleModel && enquiryFields[5].data.testDriveVehicle) {
                              enquiryFields[5].data.testDriveVehicle = null;
                            }
                            enquiryFields[3].data.splice(i, 1)
                            this.setState(enquiryFields);
                            flatListInterestedVehicle.scrollToIndex({ animated: true, index: enquiryFields[3].data.length - 1 });
                          }}>

                          <Text style={{
                            backgroundColor: i == 0 ? '#fff' : '#FF0000', alignSelf: 'flex-end',
                            paddingLeft: 5, paddingRight: 5, borderRadius: 10, color: '#fff', fontSize: 12,
                            marginRight: 5, marginTop: 5
                          }}>Remove
                      </Text>

                        </TouchableOpacity>
                        <ItemPicker style={{ marginTop: -5 }} title={model} onClick={this.showPicker.bind(this, { title: 'Vehicle model', fromId: 'vehicle_model', payload: { index: i }, listItems: this.getInterestedVehicleModelsItems() }, true)} />
                        <ItemPicker disabled={atom && atom.vehicleModel ? false : true} title={variant} onClick={this.showPicker.bind(this, { title: 'Select variant', fromId: 'vehicle_variant', payload: { index: i }, listItems: this.getInterestedVehicleVariantsItems(atom ? atom.vehicleModel : null) })} />
                        <ItemPicker disabled={atom && atom.vehicleVariant ? false : true} title={color} onClick={this.showPicker.bind(this, { title: 'Select color', fromId: 'vehicle_color', payload: { index: i }, listItems: this.getInterestedVehcileColorsItems(atom ? atom.vehicleModel : null, atom ? atom.vehicleVariant : null) })} />
                        <ItemPicker disabled={atom && atom.vehicleVariant ? false : true} title={fuelType} hideDownArrow={true} onClick={() => { }} />
                      </View>
                    </View>
                  </View>
                )
              }}
              extraData={this.state.enquiryFields[3].data}
              keyExtractor={(item, index) => index.toString()}
            />
            <Text style={styles.floatRight} onPress={() => {
              if (item.data[item.data.length - 1].vehicleModel) {
                let { enquiryFields } = this.state;
                enquiryFields[3].data.push({});
                this.setState(enquiryFields);
                flatListInterestedVehicle.scrollToEnd({ animated: true });
              }

            }}> Add Another vehicle</Text>
          </View>
        )
      }
        break;

      case 'extra_vehicle': {
        let flatListInterestedVehicle;
        const VEHICLE_WIDTH = Dimensions.get('window').width - 50;
        return (
          <View style={[{ display: (item.isExpanded ? 'flex' : 'none') }, styles.field]}>
            <FlatList
              ref={ref => flatListInterestedVehicle = ref}
              getItemLayout={(data, pos) => ({ index: pos, length: VEHICLE_WIDTH, offset: VEHICLE_WIDTH * pos })}
              horizontal={true}
              style={{ flex: 1, width: '100%', }}
              data={item.data}
              renderItem={({ item: extraVehicleItem, index: i }) => {
                let brand = "Brand *";
                let model = "Model *";
                let year = "Purchaced date";
                let regNumber = "";
                let kmRun = "";
                //console.log("I::" + i);
                if (extraVehicleItem) {
                  if (extraVehicleItem.vehicleBrand) {
                    brand = extraVehicleItem.vehicleBrand.text;
                  }
                  if (extraVehicleItem.vehicleModel) {
                    model = extraVehicleItem.vehicleModel.text;
                  }
                  if (extraVehicleItem.vehicleYear) {
                    year = Utils.getReadableDate(extraVehicleItem.vehicleYear);
                  }
                  if (extraVehicleItem.vehicleRegNumber) {
                    regNumber = extraVehicleItem.vehicleRegNumber;
                  }
                  if (extraVehicleItem.vehicleKMRun) {
                    kmRun = extraVehicleItem.vehicleKMRun;
                  }
                }

                return (
                  <View styles={{ marginLeft: 10 }}>

                    <View style={[styles.shadowsView, { width: VEHICLE_WIDTH }]}>
                      <View style={[styles.shadowsViewInner]}>
                        <TouchableOpacity
                          disabled={i == 0}
                          style={{ alignSelf: 'flex-end' }}
                          onPress={() => {
                            let { enquiryFields } = this.state;
                            enquiryFields[4].data.splice(i, 1)
                            this.setState(enquiryFields);
                            flatListInterestedVehicle.scrollToIndex({ animated: true, index: enquiryFields[4].data.length - 1 });
                          }}>

                          <Text style={{
                            backgroundColor: i == 0 ? '#fff' : '#FF0000', alignSelf: 'flex-end',
                            paddingLeft: 5, paddingRight: 5, borderRadius: 10, color: '#fff', fontSize: 12,
                            marginRight: 5, marginTop: 5
                          }}>Remove
                      </Text>

                        </TouchableOpacity>
                        <ItemPicker style={{ marginTop: -5 }} title={brand} onClick={this.showPicker.bind(this, { title: 'Select brand', fromId: 'vehicle_extra_brand', payload: { index: i }, listItems: this.getAllVehicleBrandItems() }, true)} />
                        <ItemPicker disabled={extraVehicleItem && extraVehicleItem.vehicleBrand ? false : true} title={model} onClick={this.showPicker.bind(this, { title: 'Select model', fromId: 'vehicle_extra_model', payload: { index: i }, listItems: this.getAllVehicleModelsItems(item.data[i]) }, true)} />
                        <ItemPicker disabled={extraVehicleItem && extraVehicleItem.vehicleModel ? false : true} title={year} hideDownArrow={true}
                          onClick={() => {
                            this.setState({
                              dateTimePickerPayload: { from: 'vehicle_extra_year', index: i },
                              dateTimePickerVisibility: true, dateTimePickerMaximumDate: new Date()
                            })
                          }}
                        />
                        <View style={{ marginLeft: 10, marginRight: 10 }}>
                          <TextInput
                            style={{ paddingTop: 10, paddingBottom: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}
                            editable={extraVehicleItem && extraVehicleItem.vehicleModel ? true : false}
                            placeholder='Register Number'
                            value={regNumber}
                            onChangeText={(text) => {
                              let { enquiryFields } = this.state;
                              enquiryFields[4].data[i].vehicleRegNumber = text
                              this.setState(enquiryFields);
                            }}
                          />
                          <TextInput
                            style={{ paddingTop: 10, paddingBottom: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, marginBottom: 10, color: '#303030' }}
                            editable={extraVehicleItem && extraVehicleItem.vehicleModel ? true : false}
                            placeholder='KM Run'
                            keyboardType='numeric'
                            value={kmRun}
                            onChangeText={(text) => {
                              let { enquiryFields } = this.state;
                              enquiryFields[4].data[i].vehicleKMRun = text
                              this.setState(enquiryFields);


                            }}
                          />
                        </View>
                      </View>
                    </View>
                  </View>
                )
              }}
              extraData={this.state.enquiryFields[4].data}
              keyExtractor={(item, index) => index.toString()}
            />
            <Text style={styles.floatRight} onPress={() => {
              if (item.data[item.data.length - 1].vehicleModel) {
                let { enquiryFields } = this.state;
                enquiryFields[4].data.push({});
                this.setState(enquiryFields);
                flatListInterestedVehicle.scrollToEnd({ animated: true });
              }

            }}> Add Another vehicle</Text>
          </View>
        )
      }

      case 'assign_dse': {
        let salesConsultant = "Sales Consultant *";
        let activitySelected = "Select Activity *";
        let scheduledAt = "Scheduled at *";
        let remarks = "";
        let actvityId = 0;
        let visitType = item.data.visitType;
        let showroomLocation = "Select showroom *";
        let testDriveVehcile = "Select testdrive vehicle *";
        //console.log("Visit Type" + visitType);
        let maxDate = new Date();
        maxDate.setDate(maxDate.getDate() + 10);
        if (item.data.remarks) {
          remarks = item.data.remarks;
        }
        if (item.data.salesConsultant) {
          salesConsultant = item.data.salesConsultant.text;
        }
        if (item.data.activity) {
          activitySelected = item.data.activity.text;
          actvityId = item.data.activity.id;
        }
        if (item.data.showroomLocation) {
          showroomLocation = item.data.showroomLocation.text;
        }
        if (item.data.testDriveVehicle) {
          testDriveVehcile = item.data.testDriveVehicle.text;
        }
        if (item.data.activityScheduledDate) {
          scheduledAt = Utils.getReadableDateTime(item.data.activityScheduledDate)
        }

        return (
          <View style={[{ display: (item.isExpanded ? 'flex' : 'none') }, styles.field]}>
            <ItemPicker title={salesConsultant} onClick={this.showPicker.bind(this, { title: 'Sales consultant', fromId: 'sales_consultant', listItems: this.getSalesConsultantsItems() })} />
            <ItemPicker
              disabled={item.data.salesConsultant ? false : true}
              title={activitySelected} onClick={this.showPicker.bind(this, { fromId: 'activity', listItems: this.getActivityItems() })} />
            {(actvityId == ACTIVITY_IDS.HOME_VISIT_ID || actvityId == ACTIVITY_IDS.CONDUCT_TEST_DRIVE_HOME_ID) &&
              <View style={{ padding: 10, marginTop: 10 }}>
                <Text style={{ color: '#303030', fontSize: 16, fontWeight: 'bold' }}>{actvityId == ACTIVITY_IDS.HOME_VISIT_ID ? 'Visit Adress' : 'Visit Type'}</Text>
                <RadioButton selected={visitType == 1} style={{ marginTop: 10 }} title={'Home'}
                  onClick={() => {
                    let { enquiryFields } = this.state;
                    enquiryFields[5].data.visitType = 1;
                    if (enquiryFields[1].data.customerAddressType.id == ADDRESS_TYPES[1].id) {
                      enquiryFields[5].data.visitAddress = enquiryFields[1].data.customerAddress;
                    }
                    else {
                      enquiryFields[5].data.visitAddress = null;
                    }
                    this.setState({ enquiryFields });
                  }} />
                {visitType == 1 && this.getVisitEditText(item)}
                <RadioButton selected={visitType == 2} style={{ marginTop: 10 }} title={'Office'}
                  onClick={() => {

                    let { enquiryFields } = this.state;
                    enquiryFields[5].data.visitType = 2;

                    if (enquiryFields[1].data.customerAddressType.id == ADDRESS_TYPES[0].id) {
                      enquiryFields[5].data.visitAddress = enquiryFields[1].data.customerAddress;
                    }
                    else {
                      enquiryFields[5].data.visitAddress = null;
                    }
                    this.setState({ enquiryFields });
                  }}
                />
                {visitType == 2 && this.getVisitEditText(item)}
                <RadioButton selected={visitType == 3} style={{ marginTop: 10 }} title={'Other'}
                  onClick={() => {
                    let { enquiryFields } = this.state;
                    enquiryFields[5].data.visitType = 3;
                    enquiryFields[5].data.visitAddress = null;
                    this.setState({ enquiryFields });
                  }}
                />
                {visitType == 3 && this.getVisitEditText(item)}


              </View>
            }

            {(actvityId == ACTIVITY_IDS.SHOWROOM_VISIT_ID || actvityId == ACTIVITY_IDS.CONDUCT_TEST_DRIVE_SHOWROOM_ID) &&
              <View>
                <ItemPicker
                  title={showroomLocation}
                  onClick={this.showPicker.bind(this, {
                    title: 'Showroom', fromId: 'activity_showroom_location',
                    listItems: this.getShowRoomVisitItems()
                  })} />

              </View>
            }
            {(actvityId == ACTIVITY_IDS.CONDUCT_TEST_DRIVE_SHOWROOM_ID || actvityId == ACTIVITY_IDS.CONDUCT_TEST_DRIVE_HOME_ID) &&
              <View>
                <ItemPicker
                  title={testDriveVehcile}
                  onClick={this.showPicker.bind(this, {
                    title: 'Testdrive vehicle', fromId: 'activity_test_drive_vehcile',
                    listItems: this.getTestDriveCars()
                  })} />

              </View>
            }

            <ItemPicker
              disabled={item.data.salesConsultant && item.data.activity ? false : true}
              title={scheduledAt}
              onClick={() => {
                this.setState({
                  dateTimePickerPayload: { from: 'activity_scheduled_date' },
                  dateTimePickerVisibility: true, datePickerMode: 'datetime',
                  dateTimePickerMinimumDate: new Date(),
                  dateTimePickerMaximumDate: maxDate
                }
                )
              }}
            />
            <CustomTextInput
              editable={item.data.salesConsultant && item.data.activity ? true : false}
              hint='Remarks *'
              text={remarks}
              onInputTextChanged={(from, text, payload) => {
                let { enquiryFields } = this.state;
                enquiryFields[5].data.remarks = text
                this.setState(enquiryFields);
              }} />
          </View>
        )
      }
      default:
        return (
          <View style={{ display: (item.isExpanded ? 'flex' : 'none') }}>
            <Text>{item.title}</Text>
          </View>
        )
    }
  }

  expandField(index) {
    var enquiryFields = this.state.enquiryFields;
    for (var i = 0; i < enquiryFields.length; i++) {
      if (i == index) {
        enquiryFields[index].isExpanded = !enquiryFields[index].isExpanded;
      }
      else {
        enquiryFields[i].isExpanded = false;
      }
    }

    this.setState({ enquiryFields });
  }

  isCustomerTypeCompany() {
    let isCompany = false;
    if (this.state.enquiryFields[1].data.customerType) {
      let id = this.state.enquiryFields[1].data.customerType.id;
      isCompany = (id == 'corporate_individual' || id == 'corporate_company' || id == 'fleet_company')
    }
    return isCompany;
  }

  getEvaluationManager = (item) => {
    let evaluationManagersList = this.props.evaluationManagers;
    for (var i = 0; i < evaluationManagersList.length - 1; i++) {
      console.log("item.id == evaluationManagersList.locationId)", item.id, evaluationManagersList[i].hasEvaluationManager)
      if (item.id == evaluationManagersList[i].locationId) {
        return !evaluationManagersList[i].hasEvaluationManager;
      }
    }
    return true;
  }

  onSelect = (fromId, item, payload) => {
    var enquiryFields = this.state.enquiryFields;
    var { hasEvaluationManager } = this.state;
    if (fromId == "customer_type") {
      enquiryFields[1].data.customerType = item;

      if (item.id == 'individual') {
        enquiryFields[1].data.customerAddressType = ADDRESS_TYPES[1];
        enquiryFields[1].data.customerTitle = CUSTOMER_TITLES[0];
      }
      else if (item.id == 'corporate_company') {
        enquiryFields[1].data.customerAddressType = ADDRESS_TYPES[0];
        enquiryFields[1].data.customerTitle = CUSTOMER_TITLES[2];
        enquiryFields[1].data.customerNameLast = null;
      }
      else {
        enquiryFields[1].data.customerAddressType = ADDRESS_TYPES[0];
        enquiryFields[1].data.customerTitle = CUSTOMER_TITLES[0];
      }
    }
    if (fromId == "customer_address_type") {
      enquiryFields[1].data.customerAddressType = item;
    }
    else if (fromId == "customer_locality") {
      enquiryFields[1].data.customerLocality = item;
    }
    else if (fromId == 'mode_of_payment') {
      enquiryFields[1].data.modeOfPayment = item;
    }
    else if (fromId == 'showroom_location') {
      enquiryFields[0].data.showroomLocation = item;

      //console.log("getEvaluationManager", this.getEvaluationManager(item));
      //console.log("kolma", item); 
      hasEvaluationManager = this.getEvaluationManager(item);
      enquiryFields[0].data.buyerType = null;
      if (this.getLocations().length > 1) {
        enquiryFields[5].data.salesConsultant = null;
        enquiryFields[5].data.activity = null;
        enquiryFields[5].data.visitType = null;
        enquiryFields[5].data.showroomLocation = null;
        enquiryFields[5].data.activityScheduledDate = null;
        enquiryFields[5].data.remarks = null;

        enquiryFields[2].data.eventSelected = null
        enquiryFields[2].data.enquirySourceCategory = null;
        enquiryFields[2].data.enquirySource = null;
        enquiryFields[2].data.vinNumber = "";
      }

    }
    else if (fromId == 'enquiry_source_category') {
      enquiryFields[2].data.enquirySourceCategory = item;
      enquiryFields[2].data.enquirySource = null;
      enquiryFields[2].data.vinNumber = "";
      //enquiryFields[2].data.eventSelected = null;
    }
    else if (fromId == 'enquiry_source') {
      enquiryFields[2].data.enquirySource = item;
      enquiryFields[2].data.vinNumber = "";
      //enquiryFields[2].data.eventSelected = null;
    }
    // else if (fromId == 'event_selected') {
    //   enquiryFields[2].data.eventSelected = item;
    // }
    else if (fromId == 'event_selected') {
      enquiryFields[2].data.eventSelected = item;
      enquiryFields[2].data.enquirySourceCategory = null;
      enquiryFields[2].data.enquirySource = null;
      enquiryFields[2].data.vinNumber = "";
    }
    else if (fromId == 'vehicle_model') {
      console.log('Payload');
      console.log(payload);
      enquiryFields[3].data[payload.index].vehicleModel = item;
      enquiryFields[3].data[payload.index].vehicleVariant = null;
      enquiryFields[3].data[payload.index].vehicleFuelType = null
      enquiryFields[3].data[payload.index].vehicleColor = null;
      enquiryFields[5].data.testDriveVehicle = null;
    }
    else if (fromId == 'vehicle_variant') {
      enquiryFields[3].data[payload.index].vehicleVariant = item;
      enquiryFields[3].data[payload.index].vehicleFuelType = { id: item.payload.fuel_type_id, text: item.payload.fuel_type }
      enquiryFields[3].data[payload.index].vehicleColor = null;
    }
    else if (fromId == 'vehicle_fuel_type') {
      enquiryFields[3].data[payload.index].vehicleFuelType = item;
    }
    else if (fromId == 'vehicle_color') {
      enquiryFields[3].data[payload.index].vehicleColor = item;
    }
    else if (fromId == 'vehicle_extra_brand') {
      enquiryFields[4].data[payload.index].vehicleBrand = item;
      enquiryFields[4].data[payload.index].vehicleModel = null;
      enquiryFields[4].data[payload.index].vehicleYear = '';
      enquiryFields[4].data[payload.index].vehicleRegNumber = ''
      enquiryFields[4].data[payload.index].vehicleKMRun = '';
    }
    else if (fromId == 'vehicle_extra_model') {
      enquiryFields[4].data[payload.index].vehicleModel = item;
      enquiryFields[4].data[payload.index].vehicleYear = '';
      enquiryFields[4].data[payload.index].vehicleRegNumber = ''
      enquiryFields[4].data[payload.index].vehicleKMRun = '';
    }
    else if (fromId == 'sales_consultant') {
      enquiryFields[5].data.salesConsultant = item;
      enquiryFields[5].data.activity = null;
      enquiryFields[5].data.visitType = null;
      enquiryFields[5].data.showroomLocation = null;
      enquiryFields[5].data.activityScheduledDate = null;
      enquiryFields[5].data.remarks = null;
    }
    else if (fromId == 'activity') {
      enquiryFields[5].data.activity = item;
      enquiryFields[5].data.visitType = null;
      enquiryFields[5].data.showroomLocation = null;
      enquiryFields[5].data.testDriveVehicle = null;
      enquiryFields[5].data.activityScheduledDate = null;
      enquiryFields[5].data.remarks = null;
      enquiryFields[5].data.disableScheduledDateSelector = false;
      if (enquiryFields[5].data.isClientTwoWheeler
        && item.id == ACTIVITY_IDS.FIRST_CALL_ID
        && this.props.FIRST_CALL_ADVANCE_DAYS > 0) {
        enquiryFields[5].data.disableScheduledDateSelector = true;
        let scheduledDate = new Date();
        scheduledDate.setHours(10);
        scheduledDate.setMinutes(0);
        scheduledDate.setSeconds(0);
        scheduledDate.setMilliseconds(0);
        scheduledDate.setDate(scheduledDate.getDate() + this.props.FIRST_CALL_ADVANCE_DAYS);
        enquiryFields[5].data.activityScheduledDate = scheduledDate
      }

    }
    else if (fromId == 'activity_showroom_location') {
      enquiryFields[5].data.showroomLocation = item;
    }
    else if (fromId == 'activity_test_drive_vehcile') {
      enquiryFields[5].data.testDriveVehicle = item;
    }
    this.setState({
      enquiryFields,
      showItemPicker: false,
      hasEvaluationManager
    })
  }

  onInputTextChanged = (fromId, text) => {
    let enquiryFields = this.state.enquiryFields;
    switch (fromId) {
      case 'phone_number':
        enquiryFields[1].data.primaryMobile = text;
        break;
      case 'customer_name_first':
        enquiryFields[1].data.customerNameFirst = text;
        break;
      case 'customer_name_last':
        enquiryFields[1].data.customerNameLast = text;
        break;
      case 'customer_age':
        enquiryFields[1].data.customerAge = text;
        break;
      case 'customer_email':
        enquiryFields[1].data.customerEmail = text;
        break;
      case 'customer_company':
        enquiryFields[1].data.customerCompany = text;
        break;
      case 'customer_designation':
        enquiryFields[1].data.customerDesignation = text;
        break;
      case 'customer_address':
        enquiryFields[1].data.customerAddress = text;
        enquiryFields[5].data.visitAddress = text;
        break;
      case 'customer_city':
        enquiryFields[1].data.customerCity = text;
        break;
      case 'customer_state':
        enquiryFields[1].data.customerState = text;
        break;
      case 'customer_pincode':
        enquiryFields[1].data.customerPincode = text;
        enquiryFields[1].data.isPincodeValid = false;
        enquiryFields[1].data.customerState = "";
        enquiryFields[1].data.customerCity = "";
        enquiryFields[1].data.customerLocality = "";
        enquiryFields[1].data.pincodeDetails = [];
        if (text.length == 6) {
          this.getPincodeDetails(text);
        }
        break;
    }
    this.setState({
      enquiryFields
    })
  }

  onCancel = () => {
    this.setState({
      showItemPicker: false
    });
  }

  isCustomerInfoValid(isShowAlert) {
    let isValid = true;
    var regEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    let { enquiryFields } = this.state;
    let customerInfo = enquiryFields[1].data

    isValid = isValid && customerInfo.primaryMobile.length == 10;

    if (customerInfo.secondoryMobile.length > 0) {
      if (customerInfo.secondoryMobile[customerInfo.secondoryMobile.length - 1].length == 10) {
        isValid = isValid && true;
      }
      else {
        isValid = isValid && false;
        if (isShowAlert) {
          this.showAlert("Invalid secondary mobile number");
          return;
        }
      }
    }
    if (customerInfo.customerType) {
      isValid = isValid && true;
    }
    else {
      isValid = isValid && false;
      if (isShowAlert) {
        this.showAlert("Please select customer type");
        return;
      }
    }
    if (customerInfo.customerTitle) {
      isValid = isValid && true;
    }
    else {
      isValid = isValid && false;
      if (isShowAlert) {
        this.showAlert("Please select title for customer");
        return;
      }
    }

    if (customerInfo.customerNameFirst && !customerInfo.customerNameFirst.isEmpty()) {
      isValid = isValid && true;
    }
    else {
      isValid = isValid && false;
      if (isShowAlert) {
        this.showAlert("Please enter customer name");
        return;
      }
    }
    if (customerInfo.customerEmail && regEmail.test(customerInfo.customerEmail)) {
      isValid = isValid && true;
    }
    else {
      isValid = isValid && false;
      if (isShowAlert) {
        this.showAlert("Please enter a valid email");
        return;
      }
    }

    if (customerInfo.secondoryEmail.length > 0) {
      if (regEmail.test(customerInfo.secondoryEmail[customerInfo.secondoryEmail.length - 1])) {
        isValid = isValid && true;
      }
      else {
        isValid = isValid && false;
        if (isShowAlert) {
          this.showAlert("Invalid secondary email");
          return;
        }
      }
    }
    if (this.isCustomerTypeCompany()) {
      if (customerInfo.customerCompany && !customerInfo.customerCompany.isEmpty()) {
        isValid = isValid && true;
      }
      else {
        isValid = isValid && false;
        if (isShowAlert) {
          this.showAlert("Please enter a company name");
          return;
        }
      }
      if (customerInfo.customerDesignation && !customerInfo.customerDesignation.isEmpty()) {
        isValid = isValid && true;
      }
      else {
        isValid = isValid && false;
        if (isShowAlert) {
          this.showAlert("Please enter customer designation");
          return;
        }
      }
    }
    //Address Validation
    isValid = isValid && customerInfo.isPincodeValid && customerInfo.customerLocality;


    return isValid;
  }
  isBuyerTypeValid() {
    let isValid = false;
    let { enquiryFields } = this.state;
    let buyerInfo = enquiryFields[0].data;
    if (buyerInfo.showroomLocation && buyerInfo.buyerType && buyerInfo.leadTagId) {
      isValid = true;
    }
    return isValid;
  }

  isLeadInfoValid() {
    let { enquiryFields, lastLat, lastLong } = this.state;
    let leadInfo = enquiryFields[2].data;
    let isValid = leadInfo.enquirySource ? true : false;
    if (this.state.showEnquirySourceCategory) {
      isValid = isValid && leadInfo.enquirySourceCategory ? true : false;
      if (this.isVinNumberMandatory()) {
        isValid = isValid && (leadInfo.vinNumber ? true : false) && !leadInfo.vinNumber.isEmpty();
      }
      // if(this.getEventItems().length>0) {
      //   isValid = isValid && leadInfo.eventSelected?true:false;
      // }
    }
    isValid = isValid && lastLat && lastLong ? true : false;
    return isValid;
  }

  isInterestedCarValid() {
    let { enquiryFields } = this.state;
    let cars = enquiryFields[3].data;
    return cars.length > 0 && cars[0].vehicleModel;
  }

  isExtraCarValid() {
    let { enquiryFields } = this.state;
    let cars = enquiryFields[4].data;
    return cars.length > 0 && cars[0].vehicleModel;
  }

  isActivityValid() {
    let { enquiryFields } = this.state;
    let activityData = enquiryFields[5].data;
    let isValid = false;
    if (activityData) {
      if (activityData.salesConsultant
        && activityData.activity
        && activityData.activityScheduledDate
        && activityData.remarks
        && !activityData.remarks.isEmpty()) {
        if (activityData.activity.id == ACTIVITY_IDS.HOME_VISIT_ID) {
          isValid = activityData.visitType && activityData.visitAddress && !(activityData.visitAddress.isEmpty())
        }
        else if (activityData.activity.id == ACTIVITY_IDS.SHOWROOM_VISIT_ID) {
          isValid = activityData.showroomLocation;
        }
        else if (activityData.activity.id == ACTIVITY_IDS.CONDUCT_TEST_DRIVE_SHOWROOM_ID) {
          isValid = activityData.showroomLocation && activityData.testDriveVehicle;
        }
        else if (activityData.activity.id == ACTIVITY_IDS.CONDUCT_TEST_DRIVE_HOME_ID) {
          isValid = activityData.testDriveVehicle && activityData.visitType
            && activityData.visitAddress && !(activityData.visitAddress.isEmpty())
        }
        else {
          isValid = activityData.activity.id == ACTIVITY_IDS.FOLLOWUP_CALL_ID
            || activityData.activity.id == ACTIVITY_IDS.FIRST_CALL_ID;
        }
      }
    }
    return isValid;
  }

  getSuccessMark(item) {
    let success = false;
    if (item) {
      switch (item.id) {
        case 'customer_details':
          success = this.isCustomerInfoValid();
          break;
        case 'buyer_type':
          success = this.isBuyerTypeValid();
          break;
        case 'lead_info':
          success = this.isLeadInfoValid();
          break;
        case 'interested_vehicle':
          success = this.isInterestedCarValid();
          break;
        case 'extra_vehicle':
          success = this.isExtraCarValid();
          break;
        case 'assign_dse':
          success = this.isActivityValid();
          break;
      }
    }
    return success;
  }

  showAlert(alert) {
    this.refs.alert.show(alert);
  }

  onDateSelect(date, payload) {
    let { enquiryFields } = this.state;
    if (payload && payload.from == 'vehicle_extra_year') {
      enquiryFields[4].data[payload.index].vehicleYear = date;
    }
    else if (payload && payload.from == 'expected_buying_date') {
      enquiryFields[1].data.expectedBuyingDate = date;
    }
    else if (payload && payload.from == 'activity_scheduled_date') {
      enquiryFields[5].data.activityScheduledDate = date;
    }
    this.setState({
      enquiryFields,
      dateTimePickerVisibility: false,
      datePickerMode: 'date',
      dateTimePickerMinimumDate: undefined,
      dateTimePickerMaximumDate: undefined
    });
  }

  createEnquiry() {
    //console.log("Enquiry Model Building...");

    let { currentLocationHolder, isReverseAddressValid, lastLat, lastLong } = this.state;
    let location_data = {
      locality: isReverseAddressValid ? currentLocationHolder : "",
      latitude: lastLat,
      longitude: lastLong,
    }
    if (this.state.enquiryFields[0].data.buyerType == '3') {
      this.props.navigation.navigate('ExchangeCar', { allVehicles: this.props.allVehicles, enquiryField: this.state.enquiryFields, userLocation: this.getLocations(), fromAndroid: this.props.fromAndroid, location_data });
    } else {
      try {
        let inputData = CreateEnquiryModel.getInputDataServer(this.state.enquiryFields, location_data);
        //console.log(JSON.stringify(inputData));
        this.setState({ loading: !this.state.loading });
        new RestClient().createEnquiry(inputData).then((data) => {
          this.setState({ loading: false })
          //console.log(data);
          if (data) {
            if (data.statusCode == '2002' && data.result) {
              if (Platform.OS == "android") {
                let firstName = JSON.stringify(inputData.lead_data.customer_details.first_name);
                let lastName = JSON.stringify(inputData.lead_data.customer_details.last_name);
                let title = JSON.stringify(inputData.lead_data.customer_details.title);
                let leadTagId = JSON.stringify(inputData.lead_data.lead_tag_id);
                NativeModules.ReactNativeToAndroid.onEnquiryCreate(JSON.stringify(data.result),
                  JSON.stringify(data.scheduled_activity_id),
                  firstName,
                  lastName,
                  title,
                  leadTagId
                );
              }
              else {
                NavigationService.resetScreen("C360MainScreen", { leadId: data.result });
              }
            }
            else if (data.message) {
              this.showAlert(data.message);
            }
            else {
              this.showAlert('Error creating lead');
            }

          }
          else {
            this.showAlert('Error creating lead');
          }
          this.setState({ loading: false });

        }).catch(error => {
          console.log(error);
          if (!error.response) {
            this.showAlert('No Interent Connection');
          }
          else {
            this.showAlert('Error creating lead');
          }
          this.setState({ loading: false });
        });
      }
      catch (e) {
        this.showAlert(e + '');
      }
    }
  }
  requestAddressFromLatLong(latlong) {
    this.setState({
      currentLocationHolder: 'Getting your location, please wait',
      addressFetchingFromLatLong: true,
      isReverseAddressValid: false
    })
    let params = {
      appId: this.props.HERE_MAP_APP_ID,
      appCode: this.props.HERE_MAP_APP_CODE,
      CSVLatLong: latlong
    }
    new RestClient().getReverseGeoCode(params).then((data) => {
      console.log(JSON.stringify(data))
      if (data
        && data.Response
        && data.Response.View
        && data.Response.View.length > 0
        && data.Response.View[0]
        && data.Response.View[0].Result
        && data.Response.View[0].Result.length > 0
        && data.Response.View[0].Result[0]
        && data.Response.View[0].Result[0].Location
        && data.Response.View[0].Result[0].Location.Address
        && data.Response.View[0].Result[0].Location.Address.Label
      ) {
        let currentLocationHolder = data.Response.View[0].Result[0].Location.Address;
        this.setState({
          currentLocationHolder: currentLocationHolder.Label + ', ' + currentLocationHolder.PostalCode,
          addressFetchingFromLatLong: false, isReverseAddressValid: true
        })
      }
      else {
        this.setState({
          currentLocationHolder: 'Failed to get the address, Tap to retry',
          addressFetchingFromLatLong: false
        })
      }

    }).catch(error => {
      console.error(error);
      this.setState({
        currentLocationHolder: 'Failed to get the address, Tap to retry',
        addressFetchingFromLatLong: false
      });
    });
  }
  render() {
    //const { navigate } = this.props.navigation;
    //console.log("this.props.navigation", this.props.navigation)
    let { showItemPicker, dataItemPicker } = this.state;



    const { enquiryFields } = this.state;
    let isBuyerTypeExchangeOrAdditional = enquiryFields[0].data.buyerType == '2';

    let createEnquiryStatus = this.isCustomerInfoValid()
      && this.isBuyerTypeValid()
      && this.isLeadInfoValid()
      && this.isInterestedCarValid()
      && this.isActivityValid();
    if (isBuyerTypeExchangeOrAdditional) {
      createEnquiryStatus = createEnquiryStatus && this.isExtraCarValid();
    }
    return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled={Platform.OS === 'ios'}>
        <View style={{ flex: 1 }}>
          <View style={styles.container}>
            <View style={styles.main}>
              <Toast
                ref="alert"
                style={{ backgroundColor: 'red' }}
                position='top'
                fadeInDuration={1000}
                fadeOutDuration={1000}
                opacity={0.8}
                textStyle={{ color: 'white' }}
              />
              <PickerList
                showFilter={this.state.showItemPickerFilter}
                visible={showItemPicker}
                onSelect={this.onSelect}
                onCancel={this.onCancel}
                data={dataItemPicker}
              />
              <CustomDateTimePicker
                minimumDate={this.state.dateTimePickerMinimumDate}
                maximumDate={this.state.dateTimePickerMaximumDate}
                dateTimePickerVisibility={this.state.dateTimePickerVisibility}
                datePickerMode={this.state.datePickerMode}
                payload={this.state.dateTimePickerPayload}
                handleDateTimePicked={(date, payload) => {

                  this.onDateSelect(date, payload);
                }}
                hideDateTimePicked={() => {
                  this.setState({
                    dateTimePickerVisibility: false,
                    datePickerMode: 'date',
                    dateTimePickerMinimumDate: undefined,
                    dateTimePickerMaximumDate: undefined
                  })
                }}
              />

              <ScrollView>
                {
                  enquiryFields.map((item, index) => {
                    let marginTop = 0;
                    if (index != 0) {
                      marginTop = 1;
                    }

                    if (item.id == 'extra_vehicle') {
                      if (isBuyerTypeExchangeOrAdditional) {
                        if (enquiryFields[0].data.buyerType == '2') {
                          item.title = "Additional Vehicle"
                        }

                        return (
                          <View key={index}>
                            <CreateLeadExpander item={item} showSuccessMark={this.getSuccessMark(item)} key={index} style={{ marginTop }} onClick={this.expandField.bind(this, index)} />
                            <View>
                              {this.getViews(item, index)}
                            </View>
                          </View>
                        )
                      }

                    }
                    else {
                      return (
                        <View key={index}>
                          <CreateLeadExpander item={item} showSuccessMark={this.getSuccessMark(item)} key={index} style={{ marginTop }} onClick={this.expandField.bind(this, index)} />
                          <View>
                            {this.getViews(item, index)}
                          </View>
                        </View>
                      )
                    }

                  })
                }

              </ScrollView>

            </View>
            <TouchableOpacity
              disabled={!createEnquiryStatus}
              //this.props.navigation.navigate('ExchangeCar')
              onPress={() => this.createEnquiry()}
            //  onPress={() => this.props.navigation.navigate('ExchangeCar', { allVehicles: this.props.allVehicles, enquiryField: this.state.enquiryFields, userLocation: this.getLocations(), fromAndroid: this.props.fromAndroid })}
            >
              <View style={[styles.footer, createEnquiryStatus ? styles.footerValid : styles.footerInvalid]}>
                {(this.state.enquiryFields[0].data.buyerType == '3') ?
                  <Text style={styles.footerText}>Add Exchange car details</Text>
                  : <Text style={styles.footerText}>Create Enquiry</Text>
                }
              </View>
            </TouchableOpacity>
          </View>
          {this.state.loading && <View style={styles.loading}>
            <ActivityIndicator size="large" color="#007fff" />
          </View>}
        </View>
      </KeyboardAvoidingView>

    )
  }
}


String.prototype.isEmpty = function () {
  return (this.length === 0 || !this.trim());
};
