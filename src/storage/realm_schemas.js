import Realm from 'realm';
export const DEALERSHIPS_SCHEMA_NAME = 'dealership';

const DEALERSHIP_SCHEME = {
  name: DEALERSHIPS_SCHEMA_NAME,
  primaryKey: 'dealer_id',
  properties: {
    dealer_id: 'int',
    dealer_name: 'string',
    dealer_db_name: 'string',
    user_id: 'int',
    user_name: 'string?',
    user_dp_url: 'string?',
    selected: 'bool',
    updated_at: 'date',
  }
}

const USER_ROLES_SCHEMA = {
  name: 'UserRoles',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string'
  }
};
const USER_LOCATIONS_SCHEMA = {
  name: 'UserLocations',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string'
  }
};

const USER_INFO_SCHEMA = {
  name: 'UserInfo',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string',
    user_name: 'string',
    mobile: 'string',
    email: 'string?',
    dealer: 'string?',
    dp_url: 'string?',
    team_id: 'int?',
    brand_id: 'int',
    brand_name: 'string',
    roles: 'UserRoles[]',
    locations: 'UserLocations[]',
    updated_at: 'date',
  }
};

const GAME_LOCATION_SCHEMA = {
  name: 'GamificationLocations',
  properties: {
    id: 'int',
    name: 'string',
    gamification: 'bool',
    targets_locked: 'bool',
    start_date: 'string',
  }
};

const VEHICLE_MODELS_VARIANTS_SCHEMA = {
  name: 'VehicleModelsVariantsDB',
  properties: {
    variant_id: 'int?',
    variant: 'string?',
    fuel_type_id: 'int?',
    fuel_type: 'string?',
    color_id: 'int?',
    color: 'string?',
  }
};

const VEHICLE_MODELS_SCHEMA = {
  name: 'VehicleModelsDB',
  properties: {
    model_id: 'int?',
    model_name: 'string?',
    category: 'string?',
    car_variants: 'VehicleModelsVariantsDB[]'
  }
};

const VEHICLE_SCHEMA = {
  name: 'VehicleDB',
  primaryKey: 'brand_id',
  properties: {
    brand_id: 'int',
    brand_name: 'string?',
    brand_models: 'VehicleModelsDB[]'

  }
};

const APP_CONF_SCHEMA = {
  name: 'AppConfDB',
  primaryKey: 'id',
  properties: {
    id: 'int',
    key: 'string?',
    value: 'string?',
    is_edit: 'string?',
    is_active: 'string?'
  }
};

const SHOWROOM_LOCATION_SCHEMA = {
  name: 'ShowRoomLocationDB',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string?'
  }
};

const ACTIVITY_SCHEMA = {
  name: 'ActvitiyDB',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string?'
  }
};

const SUB_ENQUIRY_SOURCE_SCHEMA = {
  name: 'SubEnquirySourceDB',
  properties: {
    id: 'int',
    name: 'string?'
  }
};

const ENQUIRY_SOURCE_CATEGORY_SCHEMA = {
  name: 'EnquirySourceCategoryDB',
  properties: {
    id: 'int',
    name: 'string?',
    subEnquirySource: 'SubEnquirySourceDB[]'
  }
};

const ENQUIRY_SOURCE_MAIN_SCHEMA = {
  name: 'EnquirySourceMainDB',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string?',
  }
}

const TEAM_SCHEMA = {
  name: 'TeamDB',
  properties: {
    manager_id: 'int?',
    team_leader_id: 'int?',
    user_id: 'int?'
  }
}

const USER_SCHEMA = {
  name: 'UserDB',
  properties: {
    id: 'int?',
    name: 'string?',
    mobile_number: 'string?',
    role_id: 'int?'
  }
}

const TEAM_USERS_SCHEMA = {
  name: 'TeamUserDB',
  primaryKey: 'location_id',
  properties: {
    location_id: 'int',
    teams: 'TeamDB[]',
    users: 'UserDB[]',
  }
}
const EVENT_SCHEMA = {
  name: 'EventDB',
  properties: {
    id: 'int',
    name: 'string',
    start_date: 'string?',
    end_date: 'string?'
  }
}
const EVENT_LEAD_SOURCE_SCHEMA = {
  name: 'EventLeadSourceDB',
  properties: {
    id: 'int',
    name: 'string',
    events: 'EventDB[]'
  }
}
const EVENT_LEAD_SOURCE_CATEGORY_SCHEMA = {
  name: 'EventLeadSourceCategoryDB',
  properties: {
    id: 'int',
    name: 'string?',
    leadSources: 'EventLeadSourceDB[]'
  }
}

const EVENTS_MAIN_SCHEMA = {
  name: "EventsMainDB",
  properties: {
    locationId: 'int',
    locationName: 'string',
    eventsLeadSourceCategories: 'EventLeadSourceCategoryDB[]'
  }
}

const EVENT_CATEGORIES = {
  name: 'EventCategoriesDB',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string?'
  }
};
const EVALUATION_MANAGERS_CONF_SCHEMA = {
  name: 'EvaluationManagersConfDB',
  properties: {
    locationId: 'int',
    hasEvaluationManager: 'bool'
  }
};


// const GAMIFICATION_CONFIGURATION = {
//   name: 'GamificationConfig',
//   properties:{
//       date:'string',
//       locations:'GamificationLocations[]'
//   }
// };

export const realm = new Realm({
  schema: [DEALERSHIP_SCHEME, USER_ROLES_SCHEMA, USER_LOCATIONS_SCHEMA, USER_INFO_SCHEMA, GAME_LOCATION_SCHEMA,
    VEHICLE_MODELS_VARIANTS_SCHEMA, VEHICLE_MODELS_SCHEMA, VEHICLE_SCHEMA, APP_CONF_SCHEMA, SHOWROOM_LOCATION_SCHEMA, ACTIVITY_SCHEMA,
    ENQUIRY_SOURCE_MAIN_SCHEMA, ENQUIRY_SOURCE_CATEGORY_SCHEMA, SUB_ENQUIRY_SOURCE_SCHEMA,
    TEAM_USERS_SCHEMA, USER_SCHEMA, TEAM_SCHEMA,
    EVENT_CATEGORIES,
    EVENTS_MAIN_SCHEMA, EVENT_LEAD_SOURCE_CATEGORY_SCHEMA, EVENT_LEAD_SOURCE_SCHEMA, EVENT_SCHEMA,
    EVALUATION_MANAGERS_CONF_SCHEMA
  ],

  deleteRealmIfMigrationNeeded: true,
});
