package com.salescrm.telephony.model;

/**
 * Created by bharath on 9/6/17.
 */

public class DseModelTask {
    int dseId;
    int dseRoleId;
    String dseName;
    String dsePhotoUrl;

    public DseModelTask(int dseId, int dseRoleId, String dseName, String dsePhotoUrl) {
        this.dseId = dseId;
        this.dseRoleId = dseRoleId;
        this.dseName = dseName;
        this.dsePhotoUrl = dsePhotoUrl;
    }

    public int getDseId() {
        return dseId;
    }

    public void setDseId(int dseId) {
        this.dseId = dseId;
    }

    public int getDseRoleId() {
        return dseRoleId;
    }

    public void setDseRoleId(int dseRoleId) {
        this.dseRoleId = dseRoleId;
    }

    public String getDseName() {
        return dseName;
    }

    public void setDseName(String dseName) {
        this.dseName = dseName;
    }

    public String getDsePhotoUrl() {
        return dsePhotoUrl;
    }

    public void setDsePhotoUrl(String dsePhotoUrl) {
        this.dsePhotoUrl = dsePhotoUrl;
    }
}
