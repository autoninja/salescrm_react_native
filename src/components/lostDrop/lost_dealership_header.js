import React, { Component } from 'react'
import { View, Text, } from 'react-native'

export default class LostDealershipHeader extends Component {
  render() {
    return (
      <View>
      <View style= {{
        alignItems:'center',
        flexDirection: 'row',
        padding:10,
        marginTop:20,
        alignItems:'center'}}>

        <View style={{justifyContent: "center",alignItems: "center", flex:3,}}>
          <Text style= {{color:'#6C759B', fontSize:14, fontWeight:'500'}}>Distributor  </Text>
        </View>
        <View style={{justifyContent: "center",alignItems: "center", flex:2}}>
          <Text style= {{color:'#6C759B', fontSize:14, fontWeight:'500', textAlign:'center'}}>Leads Lost</Text>
        </View>
        <View style={{justifyContent: "center",alignItems: "center", flex:3}}>
          <Text style= {{color:'#F20000', fontSize:14, fontWeight:'500'}}>Loss  </Text>
        </View>
        <View style={{justifyContent: "center",alignItems: "center", flex:2}}>
          <Text style= {{color:'#6C759B', fontSize:14, fontWeight:'500'}}>Reason  </Text>
        </View>

      </View>
      <View style= {{backgroundColor:'#8C8C8C', height:1, marginTop:10, width:'100%'}}/>
      </View>
    );
  }
}
