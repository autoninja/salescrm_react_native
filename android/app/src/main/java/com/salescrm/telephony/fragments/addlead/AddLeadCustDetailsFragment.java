package com.salescrm.telephony.fragments.addlead;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddLeadActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.interfaces.AddLeadActivityToFragmentListener;
import com.salescrm.telephony.interfaces.AddLeadCommunication;
import com.salescrm.telephony.model.AddLeadCustomerInputData;
import com.salescrm.telephony.model.CreateLeadInputData;
import com.salescrm.telephony.model.ViewPagerSwiped;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.CheckNumberIsPrimaryResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bharath on 30/5/16.
 */
public class AddLeadCustDetailsFragment extends Fragment implements AddLeadActivityToFragmentListener{
    Spinner spinnerGender, spinnerAdrType, spinnerCustomerType;
    ImageButton btAddLeadNumber, btAddLeadEmail;
    TextInputLayout textILMobileNo, textILEmailID;
    TextInputEditText primaryMobileNo;
    TextInputEditText primaryEmailId;

    TextInputEditText etFirstName, etLastName, etAge, etAddress, etCity, etPinCode,etCompany,etDesignation;
    private int typeOfAddress=WSConstants.ADDRESS_TYPE_OFFICE;
    private AddLeadCommunication addLeadCommunication;
    private String TAG = "AddLeadCustDetailsFrag";
    private View view;
    private ImageButton btPrimaryMobile;
    private ImageButton btPrimaryEmail;
    private LinearLayout llNumberContainer, llEmailContainer;
    private List<EditText> etMobileNumbers, etEmailIds;
    private Preferences pref = null;
    private String prevPrimaryNumber="";
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_lead_cust_details, container, false);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        prevPrimaryNumber = pref.getAddSearchNumber();
        spinnerGender = (Spinner) view.findViewById(R.id.spinner_add_lead_mr);
        spinnerCustomerType = (Spinner) view.findViewById(R.id.spinner_customer_type);
        spinnerAdrType = (Spinner) view.findViewById(R.id.spinner_add_lead_adr_type);
        btAddLeadNumber = (ImageButton) view.findViewById(R.id.bt_lead_add_mobile);
        btAddLeadEmail = (ImageButton) view.findViewById(R.id.bt_lead_add_email);
        textILMobileNo = (TextInputLayout) view.findViewById(R.id.input_layout_number);
        textILEmailID = (TextInputLayout) view.findViewById(R.id.input_layout_lead_email);
        primaryMobileNo = (TextInputEditText) view.findViewById(R.id.add_lead_primary_mob);
        primaryEmailId = (TextInputEditText) view.findViewById(R.id.add_lead_primary_email);
        etFirstName = (TextInputEditText) view.findViewById(R.id.et_lead_first_name);
        etLastName = (TextInputEditText) view.findViewById(R.id.et_lead_last_name);
        etAge = (TextInputEditText) view.findViewById(R.id.et__lead_age);
        etAddress = (TextInputEditText) view.findViewById(R.id.add_lead_primary_address);
        etCity = (TextInputEditText) view.findViewById(R.id.et_lead_city);
        etPinCode = (TextInputEditText) view.findViewById(R.id.et_lead_pin);
        etDesignation = (TextInputEditText) view.findViewById(R.id.et_lead_designation);
        etCompany = (TextInputEditText) view.findViewById(R.id.et_lead_company);
        btPrimaryEmail = (ImageButton) view.findViewById(R.id.ibPrimaryEmail);
        btPrimaryMobile = (ImageButton) view.findViewById(R.id.ibPrimaryNumber);
        llNumberContainer = (LinearLayout) view.findViewById(R.id.ll_lead_number_container);
        llEmailContainer = (LinearLayout) view.findViewById(R.id.ll_lead_email_container);
        etMobileNumbers = new ArrayList<>();
        etEmailIds = new ArrayList<>();

        setSpinnerArrayAdapter(spinnerGender, WSConstants.LEAD_GENDERS);
        setSpinnerArrayAdapter(spinnerAdrType, new String[]{"Office address", "Home"});
        setSpinnerListAdapter(spinnerCustomerType, WSConstants.customerType);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Util.HideKeyboard(getContext());
                spinnerCustomerType.performClick();
                etFirstName.clearFocus();
            }
        },100);

        etFirstName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if(etLastName.isEnabled()) {
                        etLastName.requestFocus();
                    }
                    else {
                        etAge.requestFocus();
                    }
                    return true;
                }
                return false;
            }
        });


        spinnerCustomerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1 || position == 4 || position == 6) {
                    etDesignation.setHint(getResources().getString(R.string.designation_input)+"*");
                    etCompany.setHint(getResources().getString(R.string.company)+"*");
                }else{
                    etDesignation.setHint(getResources().getString(R.string.designation_input));
                    etCompany.setHint(getResources().getString(R.string.company));
                }
                if(position == 2){
                    spinnerAdrType.setSelection(1);
                    spinnerGender.setSelection(0);
                    etLastName.setEnabled(true);
                }else if(position == 4){
                    spinnerAdrType.setSelection(0);
                    spinnerGender.setSelection(2);
                    etLastName.setEnabled(false);
                }else{
                    spinnerAdrType.setSelection(0);
                    spinnerGender.setSelection(0);
                    etLastName.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        primaryMobileNo.setText(pref.getAddSearchNumber());

        primaryMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (textILMobileNo.isErrorEnabled()) {
                    textILMobileNo.setErrorEnabled(false);
                    textILMobileNo.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btPrimaryMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isValidMobile(primaryMobileNo.getText().toString())) {
                    showAlert(primaryMobileNo.getText().toString() + " is a primary mobile number");
                }
            }
        });
        btPrimaryEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isValidEmail(primaryEmailId.getText().toString())) {
                    showAlert(primaryEmailId.getText().toString() + " is a primary email id");

                }
            }
        });
        btAddLeadEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Util.isValidEmail(primaryEmailId.getText().toString())) {
                    if (isChildEmailIdValid()) {
                        ViewGroup viewGroup = (ViewGroup) textILEmailID.getParent();
                        createTextInputEditText((ViewGroup) viewGroup.getParent(), primaryEmailId);
                    }
                } else {
                    showAlert("Enter a valid email-id");
                    primaryEmailId.requestFocus();
                }
            }
        });

        btAddLeadNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Util.isValidMobile(primaryMobileNo.getText().toString())) {
                    if (isChildMobileNumbersValid()) {
                        ViewGroup viewGroup = (ViewGroup) textILMobileNo.getParent();
                        createTextInputEditText((ViewGroup) viewGroup.getParent(), primaryMobileNo);
                    }
                    // textILMobileNo.setErrorEnabled(false);
                } else {
                   /* textILMobileNo.setErrorEnabled(true);
                    textILMobileNo.setError("Gone");*/
                    showAlert("Enter 10 digit valid number");
                    primaryMobileNo.requestFocus();
                }
            }
        });
        return view;


    }


    private boolean isChildEmailIdValid() {
        String prevText = primaryEmailId.getText().toString();
        if (llEmailContainer.getChildCount() == 1) {
            return true;
        } else {
            for (int i = 1; i < llEmailContainer.getChildCount(); i++) {
                RelativeLayout relCurrent = (RelativeLayout) llEmailContainer.getChildAt(i);
                EditText currentEditText = (EditText) relCurrent.findViewById(R.id.et_current_text);
                if (Util.isValidEmail(currentEditText.getText().toString()) || currentEditText.getText().toString().equals(prevText)) {
                    currentEditText.requestFocus();
                    return false;
                }
                prevText = currentEditText.getText().toString();

            }
        }
        return true;

    }

    private boolean isChildMobileNumbersValid() {
        String prevText = primaryMobileNo.getText().toString();
        if (llNumberContainer.getChildCount() == 1) {
            return true;
        } else {
            for (int i = 1; i < llNumberContainer.getChildCount(); i++) {
                RelativeLayout relCurrent = (RelativeLayout) llNumberContainer.getChildAt(i);
                EditText currentEditText = (EditText) relCurrent.findViewById(R.id.et_current_text);
                if (!Util.isValidMobile(currentEditText.getText().toString()) || currentEditText.getText().toString().equals(prevText)) {
                    currentEditText.requestFocus();
                    return false;
                }
                prevText = currentEditText.getText().toString();

            }
        }
        return true;

    }

    /**
     * string array
     * @param spinner
     * @param data
     */
    public void setSpinnerArrayAdapter(Spinner spinner, String data[]) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, data);
        spinner.setAdapter(adapter);
    }

    /**
     *
     * @param spinner
     * @param data
     */
    public void setSpinnerListAdapter(Spinner spinner, List<String> data) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, data);
        spinner.setAdapter(adapter);
        spinner.setSelection(0);
    }
    @Override
    public void onAttach(Context context) {
        try {
            addLeadCommunication = (AddLeadCommunication) context;
        } catch (Exception e) {
            System.out.println("ClassCastException");
        }
        super.onAttach(context);
    }

    public void createTextInputEditText(final ViewGroup parent, final TextInputEditText editText) {
        System.out.println("Parent:+" + parent.getId());
        LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.edit_text_with_close, null);
        ImageButton btRemove = (ImageButton) v.findViewById(R.id.bt_remove_parent);
        ImageButton btChangePrimary = (ImageButton) v.findViewById(R.id.iv_change_primary);
        final EditText currentEditText = (EditText) v.findViewById(R.id.et_current_text);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        parent.addView(v, lp);
        btRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Item Just removed");
                parent.removeView((View) v.getParent());
                editText.requestFocus();
            }
        });
        if (editText.getId() == primaryMobileNo.getId()) {
            etMobileNumbers.add(currentEditText);
            currentEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
            currentEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
            currentEditText.setHint("Secondary Mobile");
        } else {
            etEmailIds.add(currentEditText);
            currentEditText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            currentEditText.setHint("Secondary Email-Id");

        }

        currentEditText.requestFocus();
        btChangePrimary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean flag = false;
                if (editText.getId() == primaryMobileNo.getId()) {
                    if (Util.isValidMobile(currentEditText.getText().toString())) {
                        flag = true;
                    } else {
                        showAlert("Enter a valid 10 digit number to switch primary");

                    }
                } else {
                    if (Util.isValidEmail(currentEditText.getText().toString())) {
                        flag = true;
                    } else {
                        showAlert("Enter a valid  email-id  to switch primary");
                    }
                }
                if (flag) {
                    String temp = currentEditText.getText().toString();
                    currentEditText.setText(editText.getText());
                    editText.setText(temp);
                    editText.requestFocus();
                    showAlert("Primary switched");
                } else {
                    currentEditText.requestFocus();
                }

            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);

    }

    @Subscribe
    public void onViewPagerMoved(ViewPagerSwiped viewPagerSwiped) {
        List<CreateLeadInputData.Lead_data.Emails> tempEmailList = getCustomerInputData().getEmails();
        if (viewPagerSwiped.getOldPosition() == 0 && viewPagerSwiped.getNewPosition() == 1) {
            System.out.println(TAG + ":ViewPagerTryToMoving");

           /* if(getCustomerType()==0){
                showAlert("Please select a customer type");
            }*/if (Util.isValidMobile(primaryMobileNo.getText().toString())){
                if(!prevPrimaryNumber.equals(primaryMobileNo.getText().toString())){
                    if(new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                        checkPrimaryActiveApi(primaryMobileNo.getText().toString(), viewPagerSwiped.getOldPosition());
                    }
                }
            }else if(!Util.isValidMobile(primaryMobileNo.getText().toString())){
                primaryMobileNo.requestFocus();
                textILMobileNo.setErrorEnabled(true);
                textILMobileNo.setError("Enter a valid mobile number");
                showAlert("Enter a valid mobile number");
            }

            if(getCustomerType().equalsIgnoreCase("0")){
                showAlert("Please select a customer type");
            }
            else if(!Util.isNotNull(etFirstName.getText().toString())){
                showAlert("Please fill first name");
                etFirstName.requestFocus();
            }
            else if(Util.isValidMobile(primaryMobileNo.getText().toString()) && !Util.isValidEmail(primaryEmailId.getText().toString())) {
                showAlert("Enter a valid email-id");
                primaryEmailId.requestFocus();
            }/*else if(tempEmailList== null || tempEmailList.size()==0 || tempEmailList.isEmpty()){
                    showAlert("Please add an email id");
            }*/else if(getCustomerType()!=null && (getCustomerType().equalsIgnoreCase("1") || getCustomerType().equalsIgnoreCase("4")
                     || getCustomerType().equalsIgnoreCase("6"))
                    && etDesignation.getText().toString().equalsIgnoreCase("")
                    && etCompany.getText().toString().equalsIgnoreCase("")){
                showAlert("Please fill both designation and company");

            }else{
                AddLeadActivity.customerInputData = getCustomerInputData();
                addLeadCommunication.onCustomerDetailsEdited(getCustomerInputData(),viewPagerSwiped.getOldPosition());
            }

        }
    }

    private AddLeadCustomerInputData getCustomerInputData() {
        return new AddLeadCustomerInputData(String.valueOf(spinnerGender.getSelectedItem()),getCustomerType(),etFirstName.getText().toString().trim(), etLastName.getText().toString().trim(),
                etAge.getText().toString().trim(),
                getMobileNumbers(),
                getEmailIds(),
                etAddress.getText().toString().trim(),
                etCity.getText().toString().trim(),
                etPinCode.getText().toString().trim(),
                getAddressType(),
                etCompany.getText().toString().trim(),
                etDesignation.getText().toString().trim()
        );
    }

    private List<CreateLeadInputData.Lead_data.Emails> getEmailIds() {
        List<CreateLeadInputData.Lead_data.Emails> list = new ArrayList<>();
        if(!TextUtils.isEmpty(primaryEmailId.getText().toString().trim())) {
            CreateLeadInputData.Lead_data.Emails temp = new CreateLeadInputData().new Lead_data().new Emails();
            temp.setAddress(primaryEmailId.getText().toString());
            temp.setIs_primary(true);
            list.add(temp);
        }
        for (int i = 0; i < etEmailIds.size(); i++) {
            if (Util.isValidEmail(etEmailIds.get(i).getText().toString())) {
                CreateLeadInputData.Lead_data.Emails currentEmail =new CreateLeadInputData().new Lead_data().new Emails();
                currentEmail.setAddress(etEmailIds.get(i).getText().toString());
                currentEmail.setIs_primary(false);
                list.add(currentEmail);
            }
        }


        return list;
    }

    private List<CreateLeadInputData.Lead_data.Mobile_numbers> getMobileNumbers() {
        List<CreateLeadInputData.Lead_data.Mobile_numbers> list = new ArrayList<>();
        CreateLeadInputData.Lead_data.Mobile_numbers temp =new CreateLeadInputData().new Lead_data().new Mobile_numbers();
        temp.setNumber(primaryMobileNo.getText().toString());
        temp.setIs_primary(true);
        list.add(temp);
        for (int i = 0; i < etMobileNumbers.size(); i++) {
            if (Util.isValidMobile(etMobileNumbers.get(i).getText().toString())) {
                CreateLeadInputData.Lead_data.Mobile_numbers currentNumber =new CreateLeadInputData().new Lead_data().new Mobile_numbers();
                currentNumber.setNumber(etMobileNumbers.get(i).getText().toString());
                currentNumber.setIs_primary(false);
                list.add(currentNumber);
            }
        }
        return list;
    }

    /**
     * to get the selected gender
     * @return
     */
    private String getGenders() {
        if (String.valueOf(spinnerGender.getSelectedItem()).equals("Mr")) {
            return "Male";
        } else if (String.valueOf(spinnerGender.getSelectedItem()).equals("Miss")) {
            return "Female";
        } else {
            //Wiil return for Mrs
            return null;
        }
    }

    /**
     * get the selected customer type
     * @return
     */
    private String getCustomerType() {
       return ""+spinnerCustomerType.getSelectedItemPosition();
       // return String.valueOf(spinnerCustomerType.getSelectedItem());
    }
    private int getAddressType() {
        if (String.valueOf(spinnerAdrType.getSelectedItem()).equalsIgnoreCase("Office address")) {
            return WSConstants.ADDRESS_TYPE_OFFICE;
        } else {
            return WSConstants.ADDRESS_TYPE_HOME;
        }
    }

    public void showAlert(String alert) {
        Snackbar snackbar = Snackbar.make(view, alert, Snackbar.LENGTH_LONG)
                .setAction("Close", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    private void checkPrimaryActiveApi(final String number,final int position) {
        progressDialog =new ProgressDialog(getContext());
        progressDialog.setMessage("Checking number primary or not. Please wait! ");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).CheckNumberPrimary(number, new Callback<CheckNumberIsPrimaryResponse>() {
            @Override
            public void success(CheckNumberIsPrimaryResponse o, Response response) {
                progressDialog.hide();
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(o.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }

                if (!o.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    System.out.println(TAG + " Success:0" + o.getMessage());
                    showAlert("Something went wrong !! try again!");
                    //showAlert(validateOtpResponse.getMessage());
                } else {
                    if (o.getResult() ==1) {
                       // addLeadCommunication.onCustomerDetailsEdited(getCustomerInputData(),position);
                        prevPrimaryNumber = number;
                        System.out.println(TAG + " Success:1" + o.getMessage());


                    } else {
                        primaryMobileNo.requestFocus();
                        textILMobileNo.setErrorEnabled(true);
                        textILMobileNo.setError("Can not be used as primary!");
                        showAlert(number+" Can not be used as primary!");
                        System.out.println(TAG + " Success:2" + o.getMessage());
                        //showAlert(validateOtpResponse.getMessage());
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showAlert("Something went wrong !! try again!");
                progressDialog.hide();
            }
        });
    }
}
