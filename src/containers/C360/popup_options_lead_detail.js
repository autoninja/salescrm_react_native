import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, NativeModules, TouchableWithoutFeedback, KeyboardAvoidingView, ScrollView, TextInput, BackHandler, Modal
} from 'react-native'
import DateTimePicker from 'react-native-modal-datetime-picker';
//import Modal from "react-native-modal";


const styles = StyleSheet.create({
    top_view: {
        flex: 1,
        alignItems: 'center',
    },
    lead_detail_view: {
        margin: 5,
        alignSelf: 'stretch',
    },
    customer_detail_view: {
        margin: 5,
        alignSelf: 'stretch',
    },
    lead_detail: {
        padding: 10,
        backgroundColor: '#fff'
    },
    heading: {
        fontSize: 16,
        color: '#000000',
        fontWeight: 'bold',
        margin: 5
    },
    title: {
        fontSize: 16,
        color: '#000000',
        margin: 5
    },
    grey_line: {
        height: 1,
        backgroundColor: "#808080",
        marginBottom: 10,
        marginTop: 10
    },
    save: {
        backgroundColor: "#007fff"
    },
    cancel: {
        backgroundColor: "#808080"
    },
    listItem: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
        paddingBottom: 20,
        fontSize: 16,
        color: '#303030'
    },
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        padding: 20

    }
})

const customer_type = ["Corporate(Individual)", "Individual", "Government",
    "Corporate(Company)", "Fleet(Individual)", "Fleet(Company)"]

const lead_mode_of_payment = ["Full Cash", "Company Lease Plan", "In House Loan", "Out House Loan"];

const lead_title = ["Mr", "Miss", "M/S", "Dr"];

export default class SelectPopUpItem extends Component {
    constructor(props) {
        super(props);

    }

    generatePopup(pickerId) {
        switch (pickerId) {
            case "type_of_customer":
                return (
                    <View style={{ backgroundColor: '#ffffff' }}>
                        <FlatList
                            data={customer_type}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity onPress={() => this.props.selectedData(item)}>
                                        <View>
                                            <Text style={styles.listItem}>{item}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                            <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080', padding: 10 }} onPress={() => this.props.onSelectPopUpItemCancel()}>
                                <Text style={{ textAlign: 'center', color: "#fff" }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            case "mode_of_payment":
                return (
                    <View style={{ backgroundColor: '#ffffff' }}>
                        <FlatList
                            data={lead_mode_of_payment}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity onPress={() => this.props.selectedData(item)}>
                                        <View>
                                            <Text style={styles.listItem}>{item}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                            <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080', padding: 10 }} onPress={() => this.props.onSelectPopUpItemCancel()}>
                                <Text style={{ textAlign: 'center', color: "#fff" }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            case "title":
                return (
                    <View style={{ backgroundColor: '#ffffff' }}>
                        <FlatList
                            data={lead_title}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity onPress={() => this.props.selectedData(item)}>
                                        <View>
                                            <Text style={styles.listItem}>{item}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                            <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080', padding: 10 }} onPress={() => this.props.onSelectPopUpItemCancel()}>
                                <Text style={{ textAlign: 'center', color: "#fff" }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            case "residence_locality":
                return (
                    <View style={{ backgroundColor: '#ffffff' }}>
                        <FlatList
                            data={this.props.residence_locality_array}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity onPress={() => this.props.selectedData(item.locality)}>
                                        <View>
                                            <Text style={styles.listItem}>{item.locality}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                            <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080', padding: 10 }} onPress={() => this.props.onSelectPopUpItemCancel()}>
                                <Text style={{ textAlign: 'center', color: "#fff" }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )

            case "office_locality":
                return (
                    <View style={{ backgroundColor: '#ffffff' }}>
                        <FlatList
                            data={this.props.office_locality_array}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity onPress={() => this.props.selectedData(item.locality)}>
                                        <View>
                                            <Text style={styles.listItem}>{item.locality}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                            <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080', padding: 10 }} onPress={() => this.props.onSelectPopUpItemCancel()}>
                                <Text style={{ textAlign: 'center', color: "#fff" }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )
        }
    }

    render() {
        var pickerId = this.props.pickerId;
        console.log("ppppicker", pickerId)

        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                {/* <Modal isVisible={this.props.isPickerVisible}>
                    {this.generatePopup(pickerId)}
                </Modal> */}
                <Modal
                    visible={this.props.isPickerVisible}
                    animationType={'slide'}
                    transparent={true}
                    onRequestClose={() => this.props.onSelectPopUpItemCancel()}
                >
                    <View style={styles.overlay}>
                        {this.generatePopup(pickerId)}
                    </View>
                </Modal>
            </View>
        )
    }
}