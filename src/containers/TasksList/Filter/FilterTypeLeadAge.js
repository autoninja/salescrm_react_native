import React, { Component } from "react";
import { View, Text, Dimensions } from 'react-native';
import VerticalSlider from "rn-vertical-slider";
export default class FilterTypeLeadAge extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let { data } = this.props;
        return (
            <View style={{ padding: 10, alignItems: 'center', justifyContent: 'center', alignSelf: 'center' }}>

                <VerticalSlider
                    style={{ flex: 1, alignSelf: 'center' }}
                    value={data[0].title}
                    disabled={false}
                    min={0}
                    max={100}
                    onChange={(value: number) => {
                        this.props.onChange(value);
                        console.log("CHANGE", value);
                    }}
                    onComplete={(value: number) => {
                        //console.log("COMPLETE", value);
                    }}
                    width={30}
                    height={Dimensions.get('window').height / 2}
                    step={1}
                    borderRadius={5}
                    minimumTrackTintColor={"#007fff"}
                    maximumTrackTintColor={"#ebebeb"}
                    showBallIndicator
                    ballIndicatorColor={"#007fff"}
                    ballIndicatorTextColor={"white"}
                />
                <Text style={{ marginTop: 20, fontSize: 13 }}>Slide to change the value</Text>
            </View >
        )
    }
}