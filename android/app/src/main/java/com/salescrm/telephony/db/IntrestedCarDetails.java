package com.salescrm.telephony.db;

import com.salescrm.telephony.db.car.CarDmsDataDB;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 19-08-2016.
 */
public class IntrestedCarDetails extends RealmObject {

    public static final String INTRESTEDCARDETAILS = "IntrestedCarDetails";

    /**
     * new columns
     *
     */
    private String fuelId;
    private String fuelType;
    /**
     * end
     */
    @PrimaryKey
    private int intrestedLeadCarId;
    private long leadId;
    private String intrestedCarName;
    private String intrestedCarVariantCode;
    private String intrestedCarVariantId;
    private String intrestedCarColorId;
    private String intrestedCarModelId;
    private String intrestedModelName;
    private int intrestedCarIsPrimary;
    private int testDriveStatus;
    private String color;
    private String variant;
    public RealmList<CarDmsDataDB> carDmsDataList = new RealmList<>();
    public RealmList<IntrestedCarActivityGroup> intrestedCarActivityGroup = new RealmList<>();


    public String getIntrestedModelName() {
        return intrestedModelName;
    }

    public void setIntrestedModelName(String intrestedModelName) {
        this.intrestedModelName = intrestedModelName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public int getTestDriveStatus() {
        return testDriveStatus;
    }

    public void setTestDriveStatus(int testDriveStatus) {
        this.testDriveStatus = testDriveStatus;
    }

    public RealmList<CarDmsDataDB> getCarDmsDataList() {
        return carDmsDataList;
    }

    public void setCarDmsDataList(RealmList<CarDmsDataDB> carDmsDataList) {
        this.carDmsDataList = carDmsDataList;
    }

    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public int getIntrestedLeadCarId() {

        return intrestedLeadCarId;
    }

    public void setIntrestedLeadCarId(int intrestedLeadCarId) {
        this.intrestedLeadCarId = intrestedLeadCarId;
    }

    public String getIntrestedCarName() {
        return intrestedCarName;
    }

    public void setIntrestedCarName(String intrestedCarName) {
        this.intrestedCarName = intrestedCarName;
    }

    public String getIntrestedCarVariantCode() {
        return intrestedCarVariantCode;
    }

    public void setIntrestedCarVariantCode(String intrestedCarVariantCode) {
        this.intrestedCarVariantCode = intrestedCarVariantCode;
    }

    public String getIntrestedCarVariantId() {
        return intrestedCarVariantId;
    }

    public void setIntrestedCarVariantId(String intrestedCarVariantId) {
        this.intrestedCarVariantId = intrestedCarVariantId;
    }

    public String getIntrestedCarColorId() {
        return intrestedCarColorId;
    }

    public void setIntrestedCarColorId(String intrestedCarColorId) {
        this.intrestedCarColorId = intrestedCarColorId;
    }

    public String getIntrestedCarModelId() {
        return intrestedCarModelId;
    }

    public void setIntrestedCarModelId(String intrestedCarModelId) {
        this.intrestedCarModelId = intrestedCarModelId;
    }


    public int getIntrestedCarIsPrimary() {
        return intrestedCarIsPrimary;
    }

    public void setIntrestedCarIsPrimary(int intrestedCarIsPrimary) {
        this.intrestedCarIsPrimary = intrestedCarIsPrimary;
    }


    public String getFuelId() {
        return fuelId;
    }

    public void setFuelId(String fuelId) {
        this.fuelId = fuelId;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

}
