package com.salescrm.telephony.luckywheel;

import android.animation.Animator;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

import com.salescrm.telephony.R;
import com.salescrm.telephony.utils.Util;

import java.util.List;

/**
 * Created by nndra on 01-Mar-18.
 */

public class WheelView extends View {
    private final int DEFAULT_PADDING = 10, DEFAULT_ROTATION_TIME = 7000;

    private RectF range = new RectF();
    private Paint archPaint;
    private int padding, radius, center, mWheelBackground;
    private List<WheelItem> mWheelItems;
    private OnLuckyWheelReachTheTarget mOnLuckyWheelReachTheTarget;
    final float scale = getContext().getResources().getDisplayMetrics().density;
    int screenSize = getResources().getConfiguration().screenLayout &
            Configuration.SCREENLAYOUT_SIZE_MASK;

    public WheelView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public WheelView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initComponents() {
        //arc paint object
        archPaint = new Paint();
        archPaint.setAntiAlias(true);
        archPaint.setDither(true);
        //rect rang of the arc
        range = new RectF(padding, padding, padding + radius, padding + radius);
    }

    /**
     * Get the angele of the target
     *
     * @return Number of angle
     */
    private float getAngleOfIndexTarget(int target) {
        return (360 / mWheelItems.size()) * target;
    }

    /**
     * Function to set wheel background
     *
     * @param wheelBackground Wheel background color
     */
    public void setWheelBackgoundWheel(int wheelBackground) {
        mWheelBackground = wheelBackground;
        invalidate();
    }

    /**
     * Function to set wheel listener
     *
     * @param onLuckyWheelReachTheTarget target reach listener
     */
    public void setWheelListener(OnLuckyWheelReachTheTarget onLuckyWheelReachTheTarget) {
        mOnLuckyWheelReachTheTarget = onLuckyWheelReachTheTarget;
    }

    /**
     * Function to add wheels items
     *
     * @param wheelItems Wheels model item
     */
    public void addWheelItems(List<WheelItem> wheelItems) {
        mWheelItems = wheelItems;
        invalidate();
    }

    /**
     * Function to draw wheel background
     *
     * @param canvas Canvas of draw
     */
    private void drawWheelBackground(Canvas canvas) {
        Paint backgroundPainter = new Paint();
        backgroundPainter.setAntiAlias(true);
        backgroundPainter.setDither(true);
        backgroundPainter.setColor(mWheelBackground);
        canvas.drawCircle(center, center, center, backgroundPainter);
    }

    /**
     * Function to draw image in the center of arc
     *
     * @param canvas    Canvas to draw
     * @param tempAngle Temporary angle
     * @param bitmap    Bitmap to draw
     */
    private void drawImage(Canvas canvas, float tempAngle, Bitmap bitmap) {
        //get every arc img width and angle
        int imgWidth = radius / mWheelItems.size();
        float angle = (float) ((tempAngle + 360 / mWheelItems.size() / 2) * Math.PI / 180);
        //calculate x and y
        int x = (int) (center + radius / 2 / 2 / 2 * Math.cos(angle));
        int y = (int) (center + radius / 2 / 2 / 2 * Math.sin(angle));
        //create arc to draw
        Rect rect = new Rect(x - imgWidth / 3, y - imgWidth / 3, x + imgWidth / 3, y + imgWidth / 3);
        //rotate main bitmap
        Matrix matrix = new Matrix();
        matrix.postRotate(45);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        canvas.drawBitmap(rotatedBitmap, null, rect, null);
    }

    /**
     * Function to rotate wheel to target
     *
     * @param target target number
     */
    public void rotateWheelToTarget(int target) {

        float wheelItemCenter = 270 - getAngleOfIndexTarget(target) + (360 / mWheelItems.size()) / 2;
        animate().setInterpolator(new DecelerateInterpolator())
                .setDuration(DEFAULT_ROTATION_TIME)
                .rotation((360 * 5) + wheelItemCenter)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (mOnLuckyWheelReachTheTarget != null) {
                            mOnLuckyWheelReachTheTarget.onReachTarget();
                        }
                        clearAnimation();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }

    /**
     * Function to rotate to zero angle
     *
     * @param target target to reach
     */
    public void resetRotationLocationToZeroAngle(final int target) {
        animate().setDuration(0)
                .rotation(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                rotateWheelToTarget(target);
                clearAnimation();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#0F2B51"));

        drawWheelBackground(canvas);
        initComponents();

        float tempAngle = 0;
        float sweepAngle = 360 / mWheelItems.size();

        Typeface plain = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL);
        //Typeface plain = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Regular.ttf");
        Typeface bold = Typeface.create(plain, Typeface.BOLD);

        for (int i = 0; i < mWheelItems.size(); i++) {
            archPaint.setColor(mWheelItems.get(i).color);
            canvas.drawArc(range, tempAngle, sweepAngle, true, archPaint);
            if(mWheelItems.get(i).boosterSign.equalsIgnoreCase("1")) {
                drawImage(canvas, tempAngle, mWheelItems.get(i).bitmap);
            }

            if(mWheelItems.get(i).title.contains(",")){
                String[] splittedTitle1 = mWheelItems.get(i).title.split(",");
                drawTextTop(canvas, tempAngle, splittedTitle1[0].trim(), paint, sweepAngle, bold);
                drawText1(canvas, tempAngle, splittedTitle1[1].trim(), paint, sweepAngle, bold);
                if(mWheelItems.get(i).description.contains(",")) {
                    String[] splittedTitle2 = mWheelItems.get(i).description.split(",");
                    drawText2(canvas, tempAngle, splittedTitle2[0], paint, sweepAngle, plain);
                    drawText3(canvas, tempAngle, splittedTitle2[1], paint, sweepAngle, plain);
                }else {
                    drawText2(canvas, tempAngle, mWheelItems.get(i).description, paint, sweepAngle, plain);
                }
            }else {
                drawText1(canvas, tempAngle, mWheelItems.get(i).title, paint, sweepAngle, bold);
                if(mWheelItems.get(i).description.contains(",")) {
                    String[] splittedTitle2 = mWheelItems.get(i).description.split(",");
                    drawText2(canvas, tempAngle, splittedTitle2[0], paint, sweepAngle, plain);
                    drawText3(canvas, tempAngle, splittedTitle2[1], paint, sweepAngle, plain);
                }else {
                    drawText2(canvas, tempAngle, mWheelItems.get(i).description, paint, sweepAngle, plain);
                }
            }
            tempAngle += sweepAngle;
        }

        drawCenterImage(canvas, getResources().getDrawable(R.mipmap.ic_spin_center_circle));
    }

    private void drawCenterImage(Canvas canvas, Drawable drawable) {
        Bitmap bitmap = drawableToBitmap(drawable);
        bitmap = Bitmap.createScaledBitmap(bitmap, 80, 80, false);
        canvas.drawBitmap(bitmap, getMeasuredWidth() / 2 - bitmap.getWidth() / 2,
                getMeasuredHeight() / 2 - bitmap.getHeight() / 2, null);
    }

    private void drawTextTop(Canvas canvas, float tempAngle, String title, Paint paint, float sweepAngle, Typeface bold) {
        Path path = new Path();
        path.addArc(range,tempAngle,sweepAngle);
        float textWidth = paint.measureText(title);
        //int hOffset = (int) (radius * Math.PI / mWheelItems.size()/2-textWidth/2);
        int vOffset = radius/2/6;
        paint.setTextSize(textSize(16));
        paint.setTypeface(bold);
        paint.setTextAlign(Paint.Align.CENTER);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            paint.setLetterSpacing(0.1f);  // setLetterSpacing is only available from LOLLIPOP and on
            canvas.drawTextOnPath(title, path, 0, vOffset, paint);
        }else {
            String titleString = "";
            for(int i =0; i < title.length(); i++) {
                titleString += title.charAt(i)+ " ";
            }
            canvas.drawTextOnPath(titleString, path, 0, vOffset, paint);
        }
    }

    private void drawText1(Canvas canvas, float tempAngle, String title, Paint paint, float sweepAngle, Typeface bold) {
        Path path = new Path();
        path.addArc(range,tempAngle,sweepAngle);
        float textWidth = paint.measureText(title);
        //int hOffset = (int) (radius * Math.PI / mWheelItems.size()/2-textWidth/2);
        int vOffset = radius/2/4;
        paint.setTextSize(textSize(16));
        paint.setTypeface(bold);
        paint.setTextAlign(Paint.Align.CENTER);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            paint.setLetterSpacing(0.1f);  // setLetterSpacing is only available from LOLLIPOP and on
            canvas.drawTextOnPath(title, path, 0, vOffset, paint);
        }else {
            String titleString = "";
            for(int i =0; i < title.length(); i++) {
                titleString += title.charAt(i)+ " ";
            }
            canvas.drawTextOnPath(titleString, path, 0, vOffset, paint);
        }
    }

    private void drawText2(Canvas canvas, float tempAngle, String title, Paint paint, float sweepAngle, Typeface plain) {
        Path path = new Path();
        path.addArc(range,tempAngle,sweepAngle);
        float textWidth = paint.measureText(title);
        //int hOffset = (int) (radius * Math.PI / mWheelItems.size()/2-textWidth/2);
        int vOffset = radius/2/4;
        paint.setTextSize(textSize(12));
        paint.setTypeface(plain);
        paint.setTextAlign(Paint.Align.CENTER);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            paint.setLetterSpacing(0.1f);  // setLetterSpacing is only available from LOLLIPOP and on
            canvas.drawTextOnPath(title, path, 0, vOffset + 40, paint);
        }else {
            String titleString = "";
            for(int i =0; i < title.length(); i++) {
                titleString += title.charAt(i)+ " ";
            }
            canvas.drawTextOnPath(titleString, path, 0, vOffset + 40, paint);
        }
    }

    private void drawText3(Canvas canvas, float tempAngle, String title, Paint paint, float sweepAngle, Typeface plain) {
        Path path = new Path();
        path.addArc(range,tempAngle,sweepAngle);
        float textWidth = paint.measureText(title);
        //int hOffset = (int) (radius * Math.PI / mWheelItems.size()/2-textWidth/2);
        int vOffset = radius/2/4;
        paint.setTextSize(textSize(12));
        paint.setTypeface(plain);
        paint.setTextAlign(Paint.Align.CENTER);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            paint.setLetterSpacing(0.1f);  // setLetterSpacing is only available from LOLLIPOP and on
            canvas.drawTextOnPath(title, path, 0, vOffset + 70, paint);
        }else {
            String titleString = "";
            for(int i =0; i < title.length(); i++) {
                titleString += title.charAt(i)+ " ";
            }
            canvas.drawTextOnPath(titleString, path, 0, vOffset + 70, paint);
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = Math.min(getMeasuredWidth(), getMeasuredHeight());
        padding = getPaddingLeft() == 0 ? DEFAULT_PADDING : getPaddingLeft();
        radius = width - padding * 2;
        center = width / 2;
        setMeasuredDimension(width, width);

    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public float textSize(float value){
        switch (screenSize){
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                value = value + 1;
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                value = value - 1;
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                value = value - 2;
                break;
        }
        final float GESTURE_THRESHOLD_DIP = value;
        int mGestureThreshold = (int) (GESTURE_THRESHOLD_DIP * scale + 0.5f);
        return mGestureThreshold;
    }
}