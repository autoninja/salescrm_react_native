package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class GameLocationDB extends RealmObject {
    @PrimaryKey
    private int id;

    private String name;
    private boolean gamification;
    private boolean targets_locked;
    private String start_date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isGamification() {
        return gamification;
    }

    public void setGamification(boolean gamification) {
        this.gamification = gamification;
    }

    public boolean isTargets_locked() {
        return targets_locked;
    }

    public void setTargets_locked(boolean targets_locked) {
        this.targets_locked = targets_locked;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }
}
