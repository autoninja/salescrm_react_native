package com.salescrm.telephony.db.teams;

import io.realm.RealmObject;

/**
 * Created by bharath on 9/12/16.
 */

public class TeamMainDB extends RealmObject {
    private int manager_id;
    private int team_leader_id;
    private int user_id;

    public TeamMainDB() {
    }

    public TeamMainDB(int manager_id, int team_leader_id, int user_id) {
        this.manager_id = manager_id;
        this.team_leader_id = team_leader_id;
        this.user_id = user_id;
    }

    public int getManager_id() {
        return manager_id;
    }

    public void setManager_id(int manager_id) {
        this.manager_id = manager_id;
    }

    public int getTeam_leader_id() {
        return team_leader_id;
    }

    public void setTeam_leader_id(int team_leader_id) {
        this.team_leader_id = team_leader_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
