package com.salescrm.telephony.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.salescrm.telephony.response.Error;

import java.util.List;

public class BookingAllocationModel {
    @SerializedName("error")
    @Expose
    private Error error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Result{
        private List<VinAllocationStatus> vin_allocation_statuses = null;

        public List<VinAllocationStatus> getVin_allocation_statuses() {
            return vin_allocation_statuses;
        }

        public void setVin_allocation_statuses(List<VinAllocationStatus> vin_allocation_statuses) {
            this.vin_allocation_statuses = vin_allocation_statuses;
        }
    }

    public class VinAllocationStatus{
        private String id;
        private String name;
        private List<String> role_ids = null;

        public List<String> getRoleId() {
            return role_ids;
        }

        public void setRoleId(List<String> roleId) {
            this.role_ids = roleId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
