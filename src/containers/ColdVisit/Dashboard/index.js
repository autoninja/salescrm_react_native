import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  NativeModules
} from "react-native";
import {
  NavigationEvents,
  createMaterialTopTabNavigator,
  createAppContainer
} from "react-navigation";
import ColdVisitDashboardChild from "./ColdVisitDashboardChild";
import styles from "./ColdVisitDashboard.styles";
import Toast from "react-native-easy-toast";
import RestClient from "../../../network/rest_client";
export default class ColdVisitDashboard extends Component {
  constructor(props) {
    super(props);
    let endDate = new Date();
    let startDate = new Date();
    startDate.setDate(1);
    this.state = {
      isApiCalledAtleastOnce: false,
      loading: true,
      response: [],
      dateRange: { startDate, endDate },
      initDate: {
        startDate: global_filter_date.startDateStr,
        endDate: global_filter_date.endDateStr
      }
    };
  }
  componentDidMount() {
    this._mounted = true;
    this.getColdVisits();
  }
  componentWillUnmount() {
    this._mounted = false;
  }

  onLongPress(from, data) {
    let { navigation } = this.props;
    if (from === "user_info") {
      navigation.navigate("UserInfo", {
        name: data.info.name,
        dp_url: data.info.dp_url
      });
    } else if (from == "cold_visit_details") {
      navigation.navigate("ColdVisitDashboardDetails", {
        data
      });
    }
  }

  showDateRangePicker(navigation) {
    navigation.navigate("DateRangePickerDialog", {
      initDate: this.state.dateRange,
      onFilterApply: (s, e) => {
        if (s && e) {
          let start = s.split("-");
          let end = e.split("-");

          let localDateRange = {};
          localStartDate = new Date();
          localStartDate.setDate(start[2]);
          localStartDate.setMonth(start[1] - 1);
          localStartDate.setYear(start[0]);
          localDateRange.startDate = localStartDate;

          localEndDate = new Date();
          localEndDate.setDate(end[2]);
          localEndDate.setMonth(end[1] - 1);
          localEndDate.setYear(end[0]);
          localDateRange.endDate = localEndDate;
          this.setState({
            dateRange: localDateRange,
            initDate: {
              startDate: global_filter_date.startDateStr,
              endDate: global_filter_date.endDateStr
            }
          });
          this.getColdVisits();
          // CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrDate });
        }
      }
    });
  }
  showAlert(alert) {
    if (Platform.OS === "android") {
      NativeModules.ReactNativeToAndroid.showToast(alert);
    }
  }

  getColdVisits() {
    this.setState({ loading: true, error: false, response: [] });
    new RestClient()
      .getColdVisitCounts({
        start_date: global.global_filter_date.startDateStr,
        end_date: global.global_filter_date.endDateStr
      })
      .then(data => {
        if (!this._mounted) {
          return;
        }
        if (data) {
          if (
            data.statusCode == 2002 &&
            data.result &&
            data.result.data &&
            data.result.data.length > 0
          ) {
            this.setState({
              loading: false,
              response: data.result.data,
              user_role_id: data.result.user_role_id
            });
          } else if (
            data.result &&
            data.result.data &&
            data.result.data.length == 0
          ) {
            this.setState({ loading: false, error: true });
          } else if (data.message) {
            this.setState({ loading: false, error: true });
            this.showAlert(data.message);
          } else {
            this.setState({ loading: false, error: true });
            this.showAlert("Failed to get the response! Try Again");
          }
        } else {
          this.setState({ loading: false, error: true });
          this.showAlert("Failed to get the response! Try Again");
        }
        //this.getColdVisitDetails();
      })
      .catch(error => {
        if (!this._mounted) {
          return;
        }
        this.setState({ loading: false, error: true });
        if (!error.status) {
          this.refs.toast.show("Connection Error");
        }
      });
  }

  getTabs() {
    let { user_role_id, response } = this.state;
    if (response.length == 0) {
      return <View />;
    } else {
      let tabs = {};
      for (var i = 0; i < response.length; i++) {
        let data = response[i];
        tabs[data.location_name + "(" + data.count + ")"] = {
          screen: props => (
            <ColdVisitDashboardChild
              isUserBH={this.props.isUserBH}
              isUserSM={this.props.isUserSM}
              isUserTL={this.props.isUserTL}
              isUserSC={this.props.isUserSC}
              onLongPress={this.onLongPress.bind(this)}
              refresh={this.getColdVisits.bind(this)}
              data={data}
              user_role_id={user_role_id}
            />
          )
        };
      }
      return createAppContainer(
        createMaterialTopTabNavigator(tabs, {
          backBehavior: "none",
          swipeEnabled: true,
          tabBarOptions: {
            style: { backgroundColor: "#fff", elevation: 0 },
            tabStyle: {
              height: 48
            },
            labelStyle: {
              fontSize: 14
            },
            inactiveTintColor: "#808080",
            activeTintColor: "#2e5e86",
            upperCaseLabel: false,
            indicatorStyle: { height: 2, backgroundColor: "#2e5e86" },
            scrollEnabled: true
          }
        })
      );
    }
  }

  render() {
    let { user_role_id, response } = this.state;
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#fff"
          }}
        >
          <ActivityIndicator
            animating={this.state.loading}
            style={{ flex: 1, alignSelf: "center" }}
            color="#2e5e86"
            size="small"
            hidesWhenStopped={true}
          />
        </View>
      );
    }

    let startDateData = global.global_filter_date.startDateStr.split("-");
    let endDateData = global.global_filter_date.endDateStr.split("-");

    let date =
      ("0" + startDateData[2]).slice(-2) +
      "/" +
      ("0" + startDateData[1]).slice(-2) +
      "-" +
      ("0" + endDateData[2]).slice(-2) +
      "/" +
      ("0" + endDateData[1]).slice(-2);

    let TAB_VIEW = this.getTabs();
    return (
      <View style={styles.main}>
        <Toast ref="toast" position="top" opacity={0.8} />
        <View
          style={{
            marginTop: 10,
            flexDirection: "row",
            height: 30
          }}
        >
          <View
            style={{
              flex: 1 / 2,
              height: 30,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-start"
            }}
          >
            <View
              style={{
                backgroundColor: "#F4F4F4",
                borderWidth: 1,
                borderColor: "#D0D8E4",
                borderRadius: 6,
                justifyContent: "center"
              }}
            >
              <Text
                style={{
                  fontSize: 14,
                  padding: 4,
                  color: "#1B143C",
                  textAlign: "center"
                }}
              >
                {date}
              </Text>
            </View>
            <TouchableOpacity
              onPress={
                () => this.showDateRangePicker(this.props.navigation)
                //this.getColdVisitDetails()
              }
            >
              <Image
                source={require("../../../images/ic_calender_blue.png")}
                style={[styles.icon, { marginLeft: 10 }]}
              />
            </TouchableOpacity>
          </View>
        </View>

        {this.state.error && (
          <View
            style={{
              flex: 1,
              alignItems: "center",
              backgroundColor: "#fff",
              justifyContent: "center"
            }}
          >
            <Text>No data from server</Text>
            <TouchableOpacity
              onPress={() => {
                this.getColdVisits();
              }}
            >
              <Text
                style={{
                  marginTop: 10,
                  padding: 10,
                  borderColor: "#c4c4c4",
                  borderRadius: 4,
                  borderWidth: 1
                }}
              >
                {" "}
                Reload{" "}
              </Text>
            </TouchableOpacity>
          </View>
        )}
        {response.length > 1 && <TAB_VIEW />}
        {response.length == 1 && (
          <View style={{ marginTop: 10, flex: 1 }}>
            <ColdVisitDashboardChild
              isUserBH={this.props.isUserBH}
              isUserSM={this.props.isUserSM}
              isUserTL={this.props.isUserTL}
              isUserSC={this.props.isUserSC}
              onLongPress={this.onLongPress.bind(this)}
              refresh={this.getColdVisits.bind(this)}
              data={response[0]}
              user_role_id={user_role_id}
            />
          </View>
        )}

        <View></View>
      </View>
    );
  }
}
