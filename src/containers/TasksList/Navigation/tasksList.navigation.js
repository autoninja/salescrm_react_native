import React from "react";
import {
    createStackNavigator,
    createMaterialTopTabNavigator,
    createBottomTabNavigator,
    createAppContainer
} from "react-navigation";

import HamburgerIcon from "../../../components/uielements/HamburgerIcon";
import BackIcon from "../../../components/uielements/BackIcon";
import HeaderRight from "../../../components/uielements/HeaderRight";
import TasksListMain from '../Main'
import Search from "../../../containers/Search/index";
import SearchResult from "../../../containers/Search/SearchResult";
import TaskListFilter from '../../../containers/TasksList/Filter'
import AutoDialog from '../../../components/uielements/AutoDialog'
import TasksTab from '../Main/TasksTabChecking'

const getNavigationWithTitle = (navigation, title) => {
    return {
        headerStyle: {
            backgroundColor: "#2e5e86",
            elevation: 0
        },
        headerLeft: <BackIcon navigationProps={navigation} />,
        title: title,
        headerBackTitle: null,
        headerTintColor: "#fff",
        headerTitleStyle: {
            fontWeight: 'bold',
            width: '100%',
            textAlign: 'left',
        },
    };
};

const getTabs = () => {
    return (createMaterialTopTabNavigator({
        Today: {
            screen: props => (
                <TasksTab
                    {...props}
                    tabIndex={0}
                    when={'pending'}
                    order={'DESC'}
                    doneLeadStatus={0}
                />)
        }
    }, {
        backBehavior: "none",
        swipeEnabled: true,
        tabBarOptions: {
            style: { backgroundColor: "transperent", elevation: 0 },
            tabStyle: {
                height: 36
            },
            labelStyle: {
                fontSize: 13,
                fontWeight: 'bold'
            },
            inactiveTintColor: "#81a8c8",
            activeTintColor: "#fff",
            upperCaseLabel: false,
            indicatorStyle: { height: 0, backgroundColor: "#2e5e86" },
        }
    }));
}

getTitle = (navigation) => {
    let title = "My Tasks";
    if (navigation && navigation.getParam("name") && navigation.getParam("id") != global.appUserId) {
        title = navigation.getParam("name") + "'s Tasks";
    }
    return title;
};

export const getTaskListNavigator = (parentNavigation, isFromDashboard, info) => {
    return (
        createAppContainer(
            createStackNavigator(
                {
                    TaskList: {
                        screen: props => <TasksListMain {...props} info={info} />,
                        navigationOptions: ({ navigation }) => ({

                            title: getTitle(navigation),

                            headerLeft: isFromDashboard ? <BackIcon navigationProps={navigation} isFromTask={true} /> : <HamburgerIcon navigationProps={navigation} />,

                            headerRight: (
                                <HeaderRight navigationProps={navigation} showFilter={true} />
                            ),

                            headerStyle: {
                                backgroundColor: "#2e5e86",
                                elevation: 0
                            },
                            headerTransperent: true,
                            headerTintColor: "#fff",
                            headerTitleStyle: {
                                textAlign: "left"
                            }
                        })
                    },
                    Search: {
                        screen: Search,
                        navigationOptions: ({ navigation }) => getNavigationWithTitle(navigation, "Add/ Search lead")
                    },
                    SearchResult: {
                        screen: props => (
                            <SearchResult
                                {...props}
                                uniqueAcrossDealerShip={true}
                                addLeadEnabled={true}
                                coldVisitEnabled={true}
                                showLeadCompetitor={true}
                            />
                        ),
                        navigationOptions: ({ navigation }) => getNavigationWithTitle(navigation, "Search Result")
                    },
                    TaskListFilter: {
                        screen: props => (
                            <TaskListFilter
                                {...props}
                                vehcileModel={info.vehcileModel}
                                enquirySource={info.enquirySource}
                            />
                        ),
                        navigationOptions: ({ navigation }) => ({
                            header: null
                        })
                    },
                    AutoDialog: {
                        screen: AutoDialog,
                        navigationOptions: ({ navigation }) => ({
                            header: null
                        })
                    }
                },
                {
                    cardStyle: {
                        backgroundColor: "rgba(255,255,255,0.3)",
                        opacity: 1
                    },
                    mode: "modal"
                },

            )
        )
    )
}