import React, { Component } from "React";
import { View, TouchableOpacity, Text } from "react-native";
export default class Button extends Component {
  render() {
    return (
      <TouchableOpacity
        disabled={this.props.disabled}
        style={{ width: "100%", alignItems: "center" }}
        onPress={() => this.props.onPress()}
      >
        <View
          style={{
            height: 40,
            width: this.props.width ? this.props.width : 240,
            justifyContent: "center",
            marginTop: 20,
            backgroundColor: this.props.disabled ? "#b4b5b8" : "#007fff",
            borderRadius: 6
          }}
        >
          <Text
            adjustsFontSizeToFit={true}
            numberOfLines={1}
            style={{
              textAlign: "center",
              fontSize: this.props.fontSize ? this.props.fontSize : 18,
              textAlignVertical: "center",
              color: this.props.disabled ? "#808080" : "#fff"
            }}
          >
            {this.props.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}
