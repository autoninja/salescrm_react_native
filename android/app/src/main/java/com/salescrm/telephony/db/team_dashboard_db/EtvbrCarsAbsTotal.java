package com.salescrm.telephony.db.team_dashboard_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by prateek on 20/9/17.
 */

public class EtvbrCarsAbsTotal extends RealmObject {
    @SerializedName("enqTotal")
    @Expose
    private Integer enqTotal;
    @SerializedName("tdTotal")
    @Expose
    private Integer tdTotal;
    @SerializedName("visitsTotal")
    @Expose
    private Integer visitsTotal;
    @SerializedName("bookTotal")
    @Expose
    private Integer bookTotal;
    @SerializedName("retailTotal")
    @Expose
    private Integer retailTotal;

    public Integer getEnqTotal() {
        return enqTotal;
    }

    public void setEnqTotal(Integer enqTotal) {
        this.enqTotal = enqTotal;
    }

    public Integer getTdTotal() {
        return tdTotal;
    }

    public void setTdTotal(Integer tdTotal) {
        this.tdTotal = tdTotal;
    }

    public Integer getVisitsTotal() {
        return visitsTotal;
    }

    public void setVisitsTotal(Integer visitsTotal) {
        this.visitsTotal = visitsTotal;
    }

    public Integer getBookTotal() {
        return bookTotal;
    }

    public void setBookTotal(Integer bookTotal) {
        this.bookTotal = bookTotal;
    }

    public Integer getRetailTotal() {
        return retailTotal;
    }

    public void setRetailTotal(Integer retailTotal) {
        this.retailTotal = retailTotal;
    }
}
