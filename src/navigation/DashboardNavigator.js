import React from "react";
import { TouchableOpacity, Text, Image, View } from "react-native";
import {
  createStackNavigator,
  createMaterialTopTabNavigator,
  createBottomTabNavigator,
  createAppContainer
} from "react-navigation";

import TodaysTasksMain from "../containers/Dashboard/TodaysTasks/index";
import ETVBRLDashboard from "../containers/Dashboard/ETVBRL/ETVBRLDashboard";
import ETVBRLDashboardChild from "../containers/Dashboard/ETVBRL/ETVBRLDashboardChild";
import LiveEFDashboard from "../containers/Dashboard/LiveEF/LiveEFDashboard";
import LiveEFDashboardChild from "../containers/Dashboard/LiveEF/LiveEFDashboardChild";
import ETVBRFilters from "../containers/Dashboard/Filters/etvbr_filters";
import ETVBRFiltersChild from "../containers/Dashboard/Filters/etvbr_filters_child";
import LeadSourceCarModelDistribution from "../containers/Dashboard/LeadSourceCarModelDistribution";
import UserInfo from "../containers/UserInfo/user_info";
import LostDrop from "../containers/Dashboard/LostDrop/Main";
import LostDetailsNewUsed from "../containers/Dashboard/LostDrop/LostDetailsNewUsed";
import DropDetailsReasons from "../containers/Dashboard/LostDrop/DropDetailsReasons";
import LostDetailsOEMLevel from "../containers/Dashboard/LostDrop/LostDetailsOEMLevel";
import LostDetailsOEMDealerReasons from "../containers/Dashboard/LostDrop/LostDetailsOEMDealerReasons";
import LostDropFilters from "../containers/Dashboard/LostDrop/LostDropFilters";
import LostDropFiltersChild from "../containers/Dashboard/LostDrop/LostDropFilters/lost_drop_filters_child";
import EventsEtvbr from "../containers/Dashboard/Events/events_etvbr_main";
import EventsEtvbrChild from "../containers/Dashboard/Events/events_etvbr_child";
import ETVBRDetails from "../containers/Dashboard/ETVBRLDetails/etvbr_details";
import AddEvents from "../containers/Dashboard/Events/addEvents";
import DateRangePickerDialog from "../components/date_range_picker";
import HamburgerIcon from "../components/uielements/HamburgerIcon";
import BackIcon from "../components/uielements/BackIcon";
import HeaderRight from "../components/uielements/HeaderRight";
import C360MainHolder from "../containers/C360/C360MainHolder";
import ETVBRExchange from "../containers/Dashboard/UsedCarDashboard/ETVBRExchange";
import ETVBRLExchangeChild from "../containers/Dashboard/UsedCarDashboard/ETVBRExchangeChild";
import TodaysTasksExchangeMain from "../containers/Dashboard/UsedCarTodaysTasks/index";
import Search from "../containers/Search/index";
import SearchResult from "../containers/Search/SearchResult";
import { getTaskListNavigator } from '../containers/TasksList/Navigation/tasksList.navigation'

//ILom Components
import TodaysTasksMainILom from "../containers/Dashboard/TodaysTasksILom/index";
import SalesILomDashboard from "../containers/Dashboard/SalesILom/SalesILomDashboard";
import SalesILomDashboardChild from "../containers/Dashboard/SalesILom/SalesILomDashboardChild";

//IBankLom
import TodaysTasksMainILomBank from "../containers/Dashboard/TodaysTasksILomBank/index";
import SalesILomBankDashboard from "../containers/Dashboard/SalesILomBank/SalesILomBankDashboard";
import SalesILomBankDashboardChild from "../containers/Dashboard/SalesILomBank/SalesILomBankDashboardChild";
//Booking
import BookBike from '../containers/BookBike'
//BookCar
import BookVehicleInit from '../containers/BookVehicle/Init/BookVehicleInit'

const getNavigation = (navigation) => {
  return {
    headerStyle: {
      backgroundColor: "#2e5e86",
      elevation: 0
    },
    headerLeft: <BackIcon navigationProps={navigation} />,
    headerBackTitle: null,
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold",
      textAlign: "left"
    }
  };
};

const getNavigationWithTitle = (navigation, title) => {
  return {
    headerStyle: {
      backgroundColor: "#2e5e86",
      elevation: 0
    },
    headerLeft: <BackIcon navigationProps={navigation} />,
    title: title,
    headerBackTitle: null,
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: 'bold',
      width: '100%',
      textAlign: 'left',
    },
  };
};

const getInitialRouteName = notification => {
  if (notification && notification.name == "eod_summary") {
    //Set date to end date to todays date;
    let endDate = new Date();
    let startDate = new Date();
    let startDateStr =
      startDate.getFullYear() +
      "-" +
      ("0" + (startDate.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + startDate.getDate()).slice(-2);
    let endDateStr =
      endDate.getFullYear() +
      "-" +
      ("0" + (endDate.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + endDate.getDate()).slice(-2);
    global.global_filter_date = { startDateStr, endDateStr };

    return "ETVBRL";
  }
  return "TodaysTasks";
};

const getETVBRMainTabs = (isUserBH, isUserOps, isUserSM) => {
  return createMaterialTopTabNavigator(
    {
      ETVBRL: {
        screen: ETVBRLDashboard,

        navigationOptions: ({ navigation }) => ({
          title: "ETVBRL"
        })
      },
      LiveEF: {
        screen: LiveEFDashboard,
        navigationOptions: ({ navigation }) => ({
          title: "Live / E-F"
        })
      },
      ...(isUserBH || isUserOps || isUserSM
        ? {
          Events: {
            screen: EventsEtvbr,
            navigationOptions: ({ navigation }) => ({
              title: "Events"
            })
          }
        }
        : {})
    },
    {
      backBehavior: "none",
      tabBarContainer: {
        flexDirection: "row",
        justifyContent: "right",
        alignItems: "center"
      },
      tabBarOptions: {
        style: {
          backgroundColor: "#fff",
          elevation: 0,
          borderBottomColor: "#e0e0e0",
          borderBottomWidth: 1
        },
        upperCaseLabel: false,
        tabStyle: {
          height: 48,
          alignSelf: "center"
        },
        labelStyle: {
          fontSize: 14,
          textAlign: "center",
          color: "#2e5e86"
        },
        indicatorStyle: { height: 4, marginLeft: 4, backgroundColor: "#2e5e86" }
      }
    }
  );
};

const getDashBoardWithNewAndExchange = (showtab, isOnlyNewCar, isOnlyUsedCar, isUserBH, isUserOps, isUserEM, isUserEV, isUserSM, isUserTL, isUserSC, isUserVF, info) => {

  if (info.isClientILom) {
    return createMaterialTopTabNavigator(
      {
        TodaysTasks: {
          screen: props => (
            <TodaysTasksMainILom
              {...props}
              notification={info.notification}
              isUserBH={isUserBH}
              isUserOps={isUserOps}
              isUserSM={isUserSM}
            />
          ),
          navigationOptions: ({ navigation }) => ({
            title: "Today's Tasks"
          })
        },
        SalesILomDashboard: {
          screen: SalesILomDashboard,
          navigationOptions: ({ navigation }) => ({
            title: "Sales"
          })
        }
      },
      {
        backBehavior: "none",
        tabBarContainer: {
          flexDirection: "row",
          justifyContent: "right",
          alignItems: "center"
        },
        tabBarOptions: {
          style: { backgroundColor: "#2e5e86", elevation: 0 },
          upperCaseLabel: false,
          tabStyle: {
            height: 48,
            alignSelf: "center"
          },
          labelStyle: {
            fontSize: 14,
            textAlign: "center"
          },
          indicatorStyle: {
            height: 4,
            marginBottom: 2,
            marginLeft: 4,
            backgroundColor: "#fff"
          }
        }
      }
    )
  }
  if (info.isClientILBank) {
    return createMaterialTopTabNavigator(
      {
        TodaysTasks: {
          screen: props => (
            <TodaysTasksMainILomBank
              {...props}
              notification={info.notification}
              isUserBH={isUserBH}
              isUserOps={isUserOps}
              isUserSM={isUserSM}
            />
          ),
          navigationOptions: ({ navigation }) => ({
            title: "Today's Tasks"
          })
        },
        SalesILomBankDashboard: {
          screen: SalesILomBankDashboard,
          navigationOptions: ({ navigation }) => ({
            title: "Sales"
          })
        }
      },
      {
        backBehavior: "none",
        tabBarContainer: {
          flexDirection: "row",
          justifyContent: "right",
          alignItems: "center"
        },
        tabBarOptions: {
          style: { backgroundColor: "#2e5e86", elevation: 0 },
          upperCaseLabel: false,
          tabStyle: {
            height: 48,
            alignSelf: "center"
          },
          labelStyle: {
            fontSize: 14,
            textAlign: "center"
          },
          indicatorStyle: {
            height: 4,
            marginBottom: 2,
            marginLeft: 4,
            backgroundColor: "#fff"
          }
        }
      }
    )
  }
  if (showtab) {
    return createBottomTabNavigator({
      NEWCAR: {
        screen: createMaterialTopTabNavigator(
          {
            TodaysTasks: {
              screen: props => (
                <TodaysTasksMain
                  {...props}
                  notification={info.notification}
                  isUserBH={isUserBH}
                  isUserOps={isUserOps}
                  isUserSM={isUserSM}
                />
              ),

              navigationOptions: ({ navigation }) => ({
                title: "Today's Tasks"
              })
            },
            ETVBRL: {
              screen: getETVBRMainTabs(isUserBH, isUserOps, isUserSM),

              navigationOptions: ({ navigation }) => ({
                title: "ETVBRL"
              })
            },
            ...(isUserBH || isUserOps || isUserSM
              ? {
                LostDrop: {
                  screen: LostDrop,
                  navigationOptions: ({ navigation }) => ({
                    title: "Lost / Drop"
                  })
                }
              }
              : {})
          },
          {
            initialRouteName: getInitialRouteName(info.notification),
            backBehavior: "none",
            tabBarContainer: {
              flexDirection: "row",
              justifyContent: "right",
              alignItems: "center"
            },
            tabBarOptions: {
              style: { backgroundColor: "#2e5e86", elevation: 0 },
              upperCaseLabel: false,
              tabStyle: {
                height: 48,
                alignSelf: "center"
              },
              labelStyle: {
                fontSize: 14,
                textAlign: "center"
              },
              indicatorStyle: {
                height: 4,
                marginBottom: 2,
                marginLeft: 4,
                backgroundColor: "#fff"
              }
            }
          }
        ),
        navigationOptions: () => ({
          title: `New Car`,
          headerBackTitle: null,
        }),
      },
      USEDCAR: {
        screen: createMaterialTopTabNavigator(
          {
            TodaysTasks: {
              screen: props => (
                <TodaysTasksExchangeMain
                  {...props}
                  notification={info.notification}
                  isUserBH={isUserBH}
                  isUserEM={isUserEM}
                />
              ),

              navigationOptions: ({ navigation }) => ({
                title: "Today's Tasks"
              })
            },
            ETVBRL: {
              screen: ETVBRExchange,

              navigationOptions: ({ navigation }) => ({
                title: "Performance"
              })
            }
          },
          {
            initialRouteName: getInitialRouteName(info.notification),
            backBehavior: "none",
            tabBarContainer: {
              flexDirection: "row",
              justifyContent: "right",
              alignItems: "center"
            },
            tabBarOptions: {
              style: { backgroundColor: "#2e5e86", elevation: 0 },
              upperCaseLabel: false,
              tabStyle: {
                height: 48,
                alignSelf: "center"
              },
              labelStyle: {
                fontSize: 14,
                textAlign: "center"
              },
              indicatorStyle: {
                height: 4,
                marginBottom: 2,
                marginLeft: 4,
                backgroundColor: "#fff"
              }
            }
          }
        ), navigationOptions: () => ({
          title: `Used Car`,
          headerBackTitle: null,
        }),
      }
    }, {
      initialRouteName: 'NEWCAR',
      tabBarOptions: {
        style: { backgroundColor: "#2e5e86", elevation: 0 },
        upperCaseLabel: false,
        activeTintColor: '#fff',
        tabStyle: {
          height: 48,
          alignContent: 'center'
        },
        labelStyle: {
          fontSize: 16,
          textAlign: "center",
          alignContent: 'center',
          marginBottom: 10
        },

      }
    })
  } else {
    if (isOnlyUsedCar) {
      return createMaterialTopTabNavigator(
        {
          TodaysTasks: {
            screen: props => (
              <TodaysTasksExchangeMain
                {...props}
                notification={info.notification}
                isUserBH={isUserBH}
                isUserEM={isUserEM}
              />
            ),

            navigationOptions: ({ navigation }) => ({
              title: "Today's Tasks"
            })
          },
          ETVBRL: {
            screen: ETVBRExchange,

            navigationOptions: ({ navigation }) => ({
              title: "Performance"
            })
          }
        },
        {
          initialRouteName: getInitialRouteName(info.notification),
          backBehavior: "none",
          tabBarContainer: {
            flexDirection: "row",
            justifyContent: "right",
            alignItems: "center"
          },
          tabBarOptions: {
            style: { backgroundColor: "#2e5e86", elevation: 0 },
            upperCaseLabel: false,
            tabStyle: {
              height: 48,
              alignSelf: "center"
            },
            labelStyle: {
              fontSize: 14,
              textAlign: "center"
            },
            indicatorStyle: {
              height: 4,
              marginBottom: 2,
              marginLeft: 4,
              backgroundColor: "#fff"
            }
          }
        }
      )
    }
    else {
      return createMaterialTopTabNavigator(
        {
          TodaysTasks: {
            screen: props => (
              <TodaysTasksMain
                {...props}
                notification={info.notification}
                isUserBH={isUserBH}
                isUserOps={isUserOps}
                isUserSM={isUserSM}
              />
            ),

            navigationOptions: ({ navigation }) => ({
              title: "Today's Tasks"
            })
          },
          ETVBRL: {
            screen: getETVBRMainTabs(isUserBH, isUserOps, isUserSM),

            navigationOptions: ({ navigation }) => ({
              title: "ETVBRL"
            })
          },
          ...(isUserBH || isUserOps || isUserSM
            ? {
              LostDrop: {
                screen: LostDrop,
                navigationOptions: ({ navigation }) => ({
                  title: "Lost / Drop"
                })
              }
            }
            : {})
        },
        {
          initialRouteName: getInitialRouteName(info.notification),
          backBehavior: "none",
          tabBarContainer: {
            flexDirection: "row",
            justifyContent: "right",
            alignItems: "center"
          },
          tabBarOptions: {
            style: { backgroundColor: "#2e5e86", elevation: 0 },
            upperCaseLabel: false,
            tabStyle: {
              height: 48,
              alignSelf: "center"
            },
            labelStyle: {
              fontSize: 14,
              textAlign: "center"
            },
            indicatorStyle: {
              height: 4,
              marginBottom: 2,
              marginLeft: 4,
              backgroundColor: "#fff"
            }
          }
        }
      )

    }
  }
}

export const getDashboardNavigator = (
  parentNavigation,
  isUserBH,
  isUserOps,
  isUserSM,
  isUserEM,
  isUserEV,
  isUserTL,
  isUserSC,
  isUserVF,
  info,
) => {
  //let ETVBRLMain = getETVBRMainTabs(isUserBH, isUserOps, isUserSM);
  let keepBottomOptionVisible = (isUserBH || isUserOps || isUserEM || isUserEV)
    && (isUserBH || isUserOps || isUserSM || isUserTL || isUserSC);
  let onlyNewCarDashboard = (isUserSM || isUserTL || isUserSC) && !(isUserEM || isUserEV)
  let onlyUsedCarDashboard = (isUserEM || isUserEV) && !(isUserSM || isUserTL || isUserSC);
  return createAppContainer(
    createStackNavigator(
      {
        TeamDashboard: {
          screen: getDashBoardWithNewAndExchange(keepBottomOptionVisible, onlyNewCarDashboard, onlyUsedCarDashboard
            , isUserBH, isUserOps, isUserEM, isUserEV, isUserSM, isUserTL, isUserSC, isUserVF, info),

          navigationOptions: ({ navigation }) => ({
            title: "Dashboard",

            headerBackTitle: null,

            headerLeft: <HamburgerIcon navigationProps={parentNavigation} />,

            headerRight: (
              <HeaderRight isUserBH={isUserBH} isClientILom={info.isClientILom} isClientILBank={info.isClientILBank} navigationProps={navigation} />
            ),

            headerStyle: {
              backgroundColor: "#2e5e86",
              elevation: 0
            },
            headerTransperent: true,
            headerTintColor: "#fff",
            headerTitleStyle: {
              width: "100%",
              textAlign: "left"
            }
          })
        },
        ETVBRLDashboardChild: {
          screen: ETVBRLDashboardChild,
          navigationOptions: ({ navigation }) => getNavigation(navigation)
        },
        LiveEFDashboardChild: {
          screen: LiveEFDashboardChild,
          navigationOptions: ({ navigation }) => getNavigation(navigation)
        },
        LeadSourceCarModelDistribution: {
          screen: LeadSourceCarModelDistribution,
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        SalesILomDashboardChild: {
          screen: SalesILomDashboardChild,
          navigationOptions: ({ navigation }) => getNavigation(navigation)
        },
        SalesILomBankDashboardChild: {
          screen: SalesILomBankDashboardChild,
          navigationOptions: ({ navigation }) => getNavigation(navigation)
        },
        UserInfo: {
          screen: UserInfo,
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        DateRangePickerDialog: {
          screen: DateRangePickerDialog,
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        ETVBRFilters: {
          screen: ETVBRFilters,
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        ETVBRFiltersChild: {
          screen: ETVBRFiltersChild,
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        ETVBRDetails: {
          screen: ETVBRDetails,
          navigationOptions: ({ navigation }) => getNavigation(navigation)
        },
        C360MainScreen: {
          screen: props => (
            <C360MainHolder
              {...props}
              interestedVehicles={info.vehcileModel}
              userRoles={[{ id: 4 }]}
              allVehicles={[]}
              isClientTwoWheeler={info.isClientTwoWheeler}
              il_flow={info.isClientILom}
              il_bank_flow={info.isClientILBank}
            />
          ),
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        BookCar: {
          screen: BookVehicleInit,
          navigationOptions: ({ navigation }) => ({
            header: null,
          }),
        },
        BookBike: {
          screen: BookBike,
          navigationOptions: ({ navigation }) => ({
            header: null,
          }),
        },
        EventsEtvbrChild: {
          screen: EventsEtvbrChild,
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        AddEvents: {
          screen: props => (
            <AddEvents
              {...props}
              allLocations={info.allLocations}
              enquirySourceCategories={info.enquirySourceCategories}
              eventCategories={info.eventCategories}
            />
          ),
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        LostDetailsNewUsed: {
          screen: LostDetailsNewUsed,
          navigationOptions: ({ navigation }) => getNavigation(navigation)
        },
        DropDetailsReasons: {
          screen: DropDetailsReasons,
          navigationOptions: ({ navigation }) => ({
            title: "Drop (Reason)",
            headerBackTitle: null,
            headerLeft: <BackIcon navigationProps={navigation} />,
            headerTransperent: true,
            headerTintColor: "#fff",
            headerTitleStyle: {
              fontWeight: "bold",
              width: "100%",
              textAlign: "left"
            }
          })
        },
        LostDetailsOEMLevel: {
          screen: LostDetailsOEMLevel,
          navigationOptions: ({ navigation }) => getNavigation(navigation)
        },
        LostDetailsOEMDealerReasons: {
          screen: LostDetailsOEMDealerReasons,
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        LostDropFilters: {
          screen: LostDropFilters,
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        LostDropFiltersChild: {
          screen: LostDropFiltersChild,
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        ETVBRLExchangeChild: {
          screen: ETVBRLExchangeChild,
          navigationOptions: ({ navigation }) => getNavigation(navigation)
        },
        Search: {
          screen: Search,
          navigationOptions: ({ navigation }) => getNavigationWithTitle(navigation, "Add/ Search lead")
        },
        SearchResult: {
          screen: props => (
            <SearchResult
              {...props}
              uniqueAcrossDealerShip={info.uniqueAcrossDealerShip}
              addLeadEnabled={info.addLeadEnabled}
              coldVisitEnabled={info.coldVisitEnabled}
              showLeadCompetitor={info.showLeadCompetitor}
            />
          ),
          navigationOptions: ({ navigation }) => getNavigationWithTitle(navigation, "Search Result")
        },
        TasksList: {
          screen: getTaskListNavigator(null, true,
            {
              uniqueAcrossDealerShip: info.uniqueAcrossDealerShip,
              addLeadEnabled: info.addLeadEnabled,
              coldVisitEnabled: info.coldVisitEnabled,
              showLeadCompetitor: info.showLeadCompetitor,
              vehcileModel: info.vehcileModel,
              enquirySource: info.enquirySource,
              isClientTwoWheeler: info.isClientTwoWheeler,
              isClientILom: info.isClientILom,
              isClientILBank: info.isClientILBank,
              hotlineNo: info.hotlineNo,
            }),
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        }

      },

      {
        cardStyle: {
          backgroundColor: "#303030BE",
          opacity: 1
        },
        mode: "modal"
      },
      {
        initialRouteName: "Search"
      }
    )
  );
};