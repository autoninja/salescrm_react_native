import React, { Component } from 'react'
import { Platform, View, Text, Image, TouchableOpacity, YellowBox, StatusBar,  FlatList, ActivityIndicator, Dimensions } from 'react-native'

import ETVBRFiltersItemHasChildren from '../../../components/filters/etvbr_filters_item_has_children'
import ItemCheckBox from '../../../components/filters/item_checkbox'

export default class ETVBRFiltersChild extends Component {

  constructor(props) {

    super(props);
    this.state = {loading:true, is_select_all:false, update:false};



    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);

  }


  closeButtonClickHandler(navigation) {
    this.props.navigation.state.params.onGoBack();
    navigation.goBack();
    console.log('closeButtonClickHandler');
  }

  _selectAllFilter(filter, is_select_all) {
    console.log('_selectAllFilter'+is_select_all);
    addAllETVBRFilterItem(filter,is_select_all);
    this.setState({is_select_all});
  }

  _update() {
    this.setState({update:!this.state.update});
  }

  render() {
    const { navigation } = this.props;
    let filter = navigation.getParam('filter', {});



    return   <View style={{width:(Dimensions.get('window').width)*.85,backgroundColor:'#fff', flex:1, paddingTop: (Platform.OS) === 'ios' ? 20 : 10,}}>
      <View style={{flex:1}}>
      <View style={{flexDirection:'row', padding:16, justifyContent:'space-between'}}>
      <TouchableOpacity onPress ={this.closeButtonClickHandler.bind(this,this.props.navigation)}>
      <Image source={require('../../../images/ic_back_black.png')} style={{width:24, height:24}}/>
        </TouchableOpacity>
      <Text style= {{fontSize:18, color:'#494949', fontWeight:'bold'}}> {filter.name} </Text>
      <Text style= {{fontSize:17, color:'#494949', }}> {} </Text>
      </View>
      <View style={{backgroundColor:'#cccccc', height:1, width:'100%'}}/>

      <FlatList
      extraData={this.state}
      data = {filter.values}
      renderItem={({item, index}) =>{
        if(item.has_children){
            return <ETVBRFiltersItemHasChildren key_name={filter.key} filter = {filter} item={item}/>
        }
        else {
          let header;
          if(index==0) {
            header = <ItemCheckBox key_name={filter.key} select_all selectAllFilter = {this._selectAllFilter.bind(this,filter)}/>;
          }
          return <View>
          {header}
          <ItemCheckBox update = {this._update.bind(this)} key_name={filter.key} id={item.id} title={item.name} is_select_all={this.state.is_select_all} />
          </View>
        }

      }}
      keyExtractor={(item, index) => index+''}
      />


      </View>

      </View>

  }
}
