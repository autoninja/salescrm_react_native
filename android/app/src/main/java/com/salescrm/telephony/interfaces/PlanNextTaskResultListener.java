package com.salescrm.telephony.interfaces;

/**
 * Created by bharath on 23/6/17.
 */

public interface PlanNextTaskResultListener {
    void onPlanNextTaskResult(int answerId);
}
