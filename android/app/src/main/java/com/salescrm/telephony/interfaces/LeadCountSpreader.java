package com.salescrm.telephony.interfaces;

/**
 * Created by prateek on 23/3/17.
 */

public class LeadCountSpreader {

    private int position;
    private int count;
    private String title;

    public LeadCountSpreader(int position, String title, int count){
        this.position = position;
        this.title = title;
        this.count = count;
    }

    public String getTitle(){
        return title;
    }

    public int getPosition(){
        return position;
    }

    public int getCount(){
        return count;
    }

}
