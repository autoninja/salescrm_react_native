import React, { Component } from 'react';

import { TouchableWithoutFeedback,Alert, Image, StyleSheet, View, Text, TouchableOpacity, FlatList, ActivityIndicator, ScrollView } from 'react-native';

import DateRangePicker from '../utils/DateRangePicker';
import LayoutHr from './layout_hr'
import styles from '../styles/styles'
import * as async_storage from '../storage/async_storage'

import { StackActions, NavigationActions } from 'react-navigation';

import { CheckBox } from 'react-native-elements'

import DealershipService from '../storage/dealership_service'
import DealershipModel from '../models/dealership_model'

import ImagePicker from 'react-native-image-picker';
import Toast, {DURATION} from 'react-native-easy-toast'

import RestClient from '../network/rest_client'



let dealerList = DealershipService.findAll();
const options = {
  title: 'Select Photo',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export default class SwitchDealership extends Component {

  // More info on all the options is below in the API Reference... just some common use cases shown here

  showImagePicker() {
    /**
 * The first arg is the options object for customization (it can also be null or omitted for default options),
 * The second arg is the callback which sends object: response (more info in the API Reference)
 */
ImagePicker.showImagePicker(options, (response) => {
  //console.log('Response = ', response);
  this.setState({
    isImageLoading : true,
  });
  if (response.didCancel) {
    this.setState({
      isImageLoading : false,
    });
    console.log('User cancelled image picker');
  } else if (response.error) {
    console.log('ImagePicker Error: ', response.error);
    this.setState({
      isImageLoading : false,
    });
  } else {

    // You can also display the image using data:
    // const source = { uri: 'data:image/jpeg;base64,' + response.data };

    console.log();

    this.setState({
      dpUrl:null,
      pickedImage : response,
    });
  }
});
  }
  constructor(props) {
  super(props);
  let checkbox= [];
  let userDp = null;
  let isGotUserDp = false;
  var selectedId = "";

  this.state = {checkbox, loading:false, isImageLoading: true, dpUrl:null, pickedImage:null};

  async_storage.getDealershipSelectedId().then((id)=> {
      if(id) {
      selectedId = id;
    }
    dealerList.forEach(function(entry){
      if(entry.user_dp_url && !isGotUserDp) {
        userDp = entry.user_dp_url;
        isGotUserDp = true;
      }
      checkbox.push({id:entry.dealer_id,selected:entry.dealer_id == selectedId, title:entry.dealer_name, dp_url:entry.user_dp_url});
    })
    this.setState({checkbox, dpUrl:userDp});

  })

  }

  logout() {
    var logout = StackActions.reset({
       index: 0, // <-- currect active route from actions array
       key:null,
       actions: [
         NavigationActions.navigate({ routeName: 'Login' }),
       ],
     });
     this.props.navigation.dispatch(logout);
  }

  capitalizeFirstLetter(string) {
    if(!string) {
      return 'Default';
    }
    return string.charAt(0).toUpperCase() + string.slice(1);

  }
  checkBoxPressed(index) {
    console.log('index:'+index);

    let dataCheckBoxes = this.state.checkbox;
    let userDp = null;
    for (var i = 0; i < dataCheckBoxes.length; i++) {
      if(i == index) {
        dataCheckBoxes[i].selected = true;
        userDp = dataCheckBoxes[i].dp_url;
      }
      else {
        dataCheckBoxes[i].selected = false;;
      }

    }
    console.log(dataCheckBoxes);
    this.setState({checkbox:dataCheckBoxes, dpUrl:userDp, isImageLoading: true, pickedImage : null});
  }

  isAnyCheckBoxSelected() {
    for (var i = 0; i < this.state.checkbox.length; i++) {
      if(this.state.checkbox[i].selected) {
        if(this.state.pickedImage || this.state.dpUrl) {
          return true;
        }

      }
    }
    return false;
  }
  proceed() {
    let dataCheckBoxes = this.state.checkbox;
    for (var i = 0; i < dataCheckBoxes.length; i++) {
      if(dataCheckBoxes[i].selected) {
        this.setState({loading:true});
        new RestClient().switchDealership({
          dealer_id: dataCheckBoxes[i].id,}).then( (data) => {
                this.setState({ loading: false});
                if(data) {
                  this.setState({ loading: true});
                  if(data.statusCode == '2002' && data.result) {
                      console.log(JSON.stringify(data));
                      global.token = data.result;
                      async_storage.setToken(data.result);

                      if(this.state.pickedImage) {
                      this.setState({ loading: true});
                      new RestClient().uploadImage({
                        base64_image:this.state.pickedImage.data}).then( (data) => {
                              console.log("Response came");
                              this.setState({ loading: false});
                              if(data) {
                                if(data.statusCode == '2002' && data.result) {
                                      async_storage.setDealershipSelectedId(dataCheckBoxes[i].id+'').then(()=>{
                                        async_storage.setLastOpenedDate(0).then(()=>{
                                          this.resetNavigation('SplashScreen');
                                        });
                                    });

                                }
                                else if(data.message) {
                                    this.refs.toast.show(data.message);
                                }
                                else {
                                    this.refs.toast.show('Error uploading photo');
                                }

                              }
                              else {
                                  this.refs.toast.show('Error uploading photo');
                              }

                          }).catch(error => {
                            console.log(error);

                                if (!error.status) {
                                    this.refs.toast.show('No Internet Connection');
                                }

                              else {
                                  this.refs.toast.show('Error uploading photo');
                              }

                            this.setState({ loading: false})});
                          }
                          else {
                            async_storage.setDealershipSelectedId(dataCheckBoxes[i].id+'').then(()=>{
                                async_storage.setLastOpenedDate(0).then(()=>{
                                 this.resetNavigation('SplashScreen');
                          });
                                 
                          });
                            
                          }


                  }
                  else if(data.message) {
                      this.refs.toast.show(data.message);
                  }
                  else {
                      this.refs.toast.show('Error while selecting dealership');
                  }

                }
                else {
                    this.refs.toast.show('Error while selecting dealership');
                }

            }).catch(error => {
              if (error.response && error.response.status == 401) {
                Alert.alert(
              'Failed',
              'Please try again',
              [
                {text: 'Logout', onPress: () => this.logout()},
                {text: 'Retry', onPress: () =>  this.proceed()},
              ],
              { cancelable: false }
            )
              }
              else {

                  if (!error.status) {
                      this.refs.toast.show('No Internet Connection');
                  }

              }
              this.setState({ loading: false})});

        return;
      }
    }


  }


 resetNavigation() {
   resetAction = StackActions.reset({
      index: 0, // <-- currect active route from actions array
      key:null,
      actions: [
        NavigationActions.navigate({ routeName: 'SplashScreen' }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }

  render () {

    let imageSource;
    let placeHolder;

    console.log("Data");
    console.log();
    if(this.state.dpUrl) {
      imageSource =   {uri :this.state.dpUrl};
    }
    else if(this.state.pickedImage) {
      imageSource =   {uri :this.state.pickedImage.uri};
    }
    else {
      imageSource = require( '../images/no_profile_img.png');
    }
    console.log(this.state.dealerList);
    let navigation = this.props.navigation;
    let title = navigation.getParam('title','Select Dealership');
    let action = navigation.getParam('action','Proceed');

    let bgBackgroundColor = this.isAnyCheckBoxSelected()? '#007fff':'#cccccc';

    return (
              <TouchableWithoutFeedback onPress = {()=>this.props.navigation.goBack()}>
                <View style={styles.Alert_Main_View}>

                      <Toast
                          ref="toast"
                           style={{backgroundColor:'red'}}
                           position='bottom'
                           fadeInDuration={750}
                           fadeOutDuration={750}
                           opacity={0.8}
                           textStyle={{color:'white'}}
                        />


                      <View style = {{backgroundColor:'#fff', borderRadius:6,}}>

                      <View style = {{marginTop:-35,borderRadius:70/2,height:70,width:70, backgroundColor:'#fff',alignSelf:'center',justifyContent:'center'}}>
                      <ActivityIndicator
                            animating={this.state.isImageLoading}
                            style={{flex: 1,   alignSelf:'center',}}
                            color="#007fff"
                            size="large"
                            hidesWhenStopped={true}
                            style={{alignSelf:'center',position:'absolute',display:(this.state.isImageLoading?'flex':'none'),}}

                      />
                      <TouchableOpacity onPress = {()=>this.showImagePicker()}>

                      <Image
                        onLoadEnd={() => this.setState({isImageLoading : false})}
                       source={imageSource}
                       style={{alignSelf:'center',width:64, height:64,borderRadius:64/2}}/>
                      </TouchableOpacity>
                      </View>
                      <Text style = {{color:'#303030', fontSize:20, marginLeft:20, marginTop:10,marginBottom:20, fontSize:18,textAlign:'center'}}> {title} </Text>
                      <LayoutHr style={{marginTop:4}}/>


                      <FlatList
                      style={{height:'40%'}}
                      extraData = {this.state}
                      data = {this.state.checkbox}
                      renderItem={({item, index}) =>
                      <CheckBox
                        title={this.capitalizeFirstLetter(item.title)}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={item.selected}
                        checkedColor ='#007fff'
                        textStyle = {{ color:'#303030', fontWeight:'normal'}}
                        containerStyle= {{backgroundColor:'#fff', borderWidth:0, margin:4}}
                        onPress={() => {
                          this.checkBoxPressed(index);
                        }
                        }
                        />
                      }
                      keyExtractor={(item, index) => index+''}
                      />



                     <TouchableOpacity disabled = {!this.isAnyCheckBoxSelected()} style={{}} onPress ={()=> {
                       this.proceed();

                  }}>
                      <View style={{height:48,justifyContent: "center", backgroundColor:bgBackgroundColor, borderBottomLeftRadius:6, borderBottomRightRadius:6}}>

                      <Text adjustsFontSizeToFit={true}
                           numberOfLines={1}
                      style={{alignSelf:'center',display:(this.state.loading?'none':'flex'), textAlign:'center', fontSize:18,textAlignVertical: 'center', color:'#fff'}}>
                      {action}

                      </Text>

                      <ActivityIndicator
                            animating={this.state.loading}
                            style={{flex: 1,   alignSelf:'center',}}
                            color="#fff"
                            size="large"
                            hidesWhenStopped={true}
                            style={{display:(this.state.loading?'flex':'none')}}

                      />




                      </View>

                      </TouchableOpacity>






                    </View>

                    </View>
                    </TouchableWithoutFeedback>

    );

  }

}
