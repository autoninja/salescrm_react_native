package com.salescrm.telephony.presenter;

import com.salescrm.telephony.model.booking.BookingMainModel;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.Calendar;

/**
 * Created by bharath on 14/3/18.
 */

public class BookingSubmitPresenter {

    private BookingSubmitView bookingSubmitView;
    private BookingMainModel bookingMainModel;


    public BookingSubmitPresenter(BookingSubmitView bookingSubmitView,
                                  BookingMainModel bookingMainModel) {
        this.bookingSubmitView = bookingSubmitView;
        this.bookingMainModel = bookingMainModel;
    }

    public BookingMainModel getBookingMainModel() {
        return bookingMainModel;
    }

    public void updateDate(Calendar calendar) {

        bookingMainModel.setDate(calendar);

        bookingSubmitView.updateDate(calendar.get(Calendar.DAY_OF_MONTH)
                + "/"
                + (calendar.get(Calendar.MONTH)+1)
                + "/"
                + calendar.get(Calendar.YEAR)
                + "  "
                + Util.getAmPmTime(calendar));
        isAllComplete();

    }

    public void init() {
        if (bookingMainModel.getDate()!=null ){
            bookingSubmitView.updateDate(bookingMainModel.getDate().get(Calendar.DAY_OF_MONTH)
                    + "/"
                    + (bookingMainModel.getDate().get(Calendar.MONTH)+1)
                    + "/"
                    + bookingMainModel.getDate().get(Calendar.YEAR)
                    + "  "
                    + Util.getAmPmTime(bookingMainModel.getDate()));
        }

        bookingSubmitView.init(bookingMainModel);
        isAllComplete();
    }

    public void updateRemarks(String data) {
        bookingMainModel.setRemarks(data);
        isAllComplete();
    }

    private void isAllComplete() {
        switch (bookingMainModel.getStageId()) {
            case WSConstants.CarBookingStages.BOOK_CAR:
            case WSConstants.CarBookingStages.CAR_BOOKED_HALF_PAYMENT:
                bookingSubmitView.isEnableSubmitBookingDetails(isValid(bookingMainModel.getDateAsSQLDate())
                        && isValid(bookingMainModel.getRemarks()));
                break;
            case WSConstants.CarBookingStages.CAR_BOOKED_FULL_PAYMENT_NO:
            case WSConstants.CarBookingStages.CUSTOM_INVOICED_DELIVERY_PENDING_NO:
                bookingSubmitView.isEnableSubmitBookingDetails(isValid(bookingMainModel.getDateAsSQLDate())
                        && isValid(bookingMainModel.getRemarks()));
                break;
        }

    }

    private boolean isValid(String text) {
        return text != null && !text.trim().isEmpty();
    }

    public interface BookingSubmitView {
        void init(BookingMainModel bookingMainModel);

        void isEnableSubmitBookingDetails(boolean isAllComplete);

        void updateDate(String date);
    }
}
