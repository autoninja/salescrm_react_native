package com.salescrm.telephony.model.booking;

/**
 * Created by bharath on 22/2/18.
 */

public class BookingCustomerModel {
    private String bookingName;
    private String dateOfBirth;
    private String address;
    private String pinCode;
    private String careOfType;
    private String careOf;
    private int customerType;
    private String panNumber;
    private String aadhaar;
    private String gst;
    private String buyerType;

    public String getBookingName() {
        return bookingName;
    }

    public void setBookingName(String bookingName) {
        this.bookingName = bookingName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getCareOfType() {
        return careOfType;
    }

    public void setCareOfType(String careOfType) {
        this.careOfType = careOfType;
    }

    public String getCareOf() {
        return careOf;
    }

    public void setCareOf(String careOf) {
        this.careOf = careOf;
    }

    public int getCustomerType() {
        return customerType;
    }

    public void setCustomerType(int customerType) {
        this.customerType = customerType;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getAadhaar() {
        return aadhaar;
    }

    public void setAadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getBuyerType() {
        return buyerType;
    }

    public void setBuyerType(String buyerType) {
        this.buyerType = buyerType;
    }

    public void copy(BookingCustomerModel bookingCustomerModel) {
        this.bookingName = bookingCustomerModel.getBookingName();
        this.dateOfBirth = bookingCustomerModel.getDateOfBirth();
        this.address = bookingCustomerModel.getAddress();
        this.pinCode = bookingCustomerModel.getPinCode();
        this.careOfType = bookingCustomerModel.getCareOfType();
        this.careOf = bookingCustomerModel.getCareOf();
        this.customerType = bookingCustomerModel.getCustomerType();
        this.panNumber = bookingCustomerModel.getPanNumber();
        this.aadhaar = bookingCustomerModel.getAadhaar();
        this.gst = bookingCustomerModel.getGst();
        this.buyerType = bookingCustomerModel.getBuyerType();
    }
}
