package com.salescrm.telephony.activity.gamification;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.model.gamification.GameData;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.FabricUtils;
import com.salescrm.telephony.utils.Util;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

/**
 * Created by prateek on 15/6/18.
 */

public class BadgesNotificationActivity extends AppCompatActivity implements View.OnClickListener{
    private RelativeLayout unlockedBadge1Rl;
    private RelativeLayout unlockedBadge2Rl;
    private TextView tv1GoToBadges, tv1Cancel;
    private TextView tv2GoToBadges, tv2Cancel;
    private TextView badge1Title, badge2Title;
    private GameData gameData;
    private Preferences pref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_game_badge);
        pref = Preferences.getInstance();
        pref.load(this);
        unlockedBadge1Rl = (RelativeLayout) findViewById(R.id.unlocked_badge1_rl);
        unlockedBadge2Rl = (RelativeLayout) findViewById(R.id.unlocked_badge2_rl);
        tv1GoToBadges = (TextView) findViewById(R.id.unlocked_badge1_tv_go_to_badges);
        tv2GoToBadges = (TextView) findViewById(R.id.unlocked_badge2_tv_go_to_badges);
        tv1Cancel = (TextView) findViewById(R.id.unlocked_badge1_tv_cancel);
        tv2Cancel = (TextView) findViewById(R.id.unlocked_badge2_tv_cancel);
        badge1Title = (TextView) findViewById(R.id.unlocked_badge1_title_two);
        badge2Title = (TextView) findViewById(R.id.unlocked_badge2_title_two);
        System.out.println("badges list: "+getIntent().getStringExtra("badges_notification"));
        if(getIntent().getStringExtra("badges_notification") != null
                && !getIntent().getStringExtra("badges_notification").equalsIgnoreCase(""))
        gameData = new Gson().fromJson(getIntent().getStringExtra("badges_notification"), GameData.class);
        if(gameData.getBadge_details().size() > 0){
            if(gameData.getBadge_details().size() == 1){
                badge1Title.setText(gameData.getBadge_details().get(0).getName());
                unlockedBadge2Rl.setVisibility(View.INVISIBLE);
            }else if(gameData.getBadge_details().size() == 2){
                badge1Title.setText(gameData.getBadge_details().get(0).getName());
                badge2Title.setText(gameData.getBadge_details().get(1).getName());
                unlockedBadge2Rl.setVisibility(View.VISIBLE);
            }

        }
        tv1GoToBadges.setOnClickListener(this);
        tv2GoToBadges.setOnClickListener(this);
        tv1Cancel.setOnClickListener(this);
        tv2Cancel.setOnClickListener(this);
        pushCleverTap();
    }

    private void pushCleverTap() {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_LEADER_BOARD_KEY_TYPE,
                CleverTapConstants.EVENT_LEADER_BOARD_TYPE_BADGES_EARNED);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_LEADER_BOARD, hashMap);
        //Fabric Events
        FabricUtils.sendEvent(pref, "LeaderBoard-"+pref.getDealerName(),
                "Type", CleverTapConstants.EVENT_LEADER_BOARD_TYPE_BADGES_EARNED);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.unlocked_badge1_tv_go_to_badges:
            case R.id.unlocked_badge2_tv_go_to_badges:
                Intent intent = new Intent(BadgesNotificationActivity.this, DsePointsActivity.class);
                intent.putExtra("from_view_badges", true);
                startActivity(intent);
                finish();
                break;
            case R.id.unlocked_badge1_tv_cancel:
            case R.id.unlocked_badge2_tv_cancel:
                finish();
                break;
        }
    }
}
