package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by bharath on 15/2/18.
 */

public class LeadMobileResponse {
    private String message;

    private String statusCode;

    private List<Result> result;

    private Error error;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public class Result {
        private List<String> mobiles;

        private String lead_id;

        public List<String> getMobiles() {
            return mobiles;
        }

        public void setMobiles(List<String> mobiles) {
            this.mobiles = mobiles;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [mobiles = " + mobiles + ", lead_id = " + lead_id + "]";
        }
    }


}
