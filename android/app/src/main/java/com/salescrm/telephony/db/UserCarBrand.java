package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bharath on 11/12/16.
 */
public class UserCarBrand extends RealmObject{
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
