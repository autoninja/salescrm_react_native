package com.salescrm.telephony.db.team_dashboard_gm_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by prateek on 13/7/17.
 */

public class Absolute extends RealmObject{

    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("location_id")
    @Expose
    private Integer locationId;
    @SerializedName("Sales Manager")
    @Expose
    private String salesManager;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("dp_url")
    @Expose
    private String dpUrl;
    @SerializedName("enquiries")
    @Expose
    private Integer enquiries;
    @SerializedName("tdrives")
    @Expose
    private Integer tdrives;
    @SerializedName("visits")
    @Expose
    private Integer visits;
    @SerializedName("bookings")
    @Expose
    private Integer bookings;
    @SerializedName("retail")
    @Expose
    private Integer retail;

    @SerializedName("enquiries_target")
    @Expose
    private Integer enquiries_target;
    @SerializedName("tdrives_target")
    @Expose
    private Integer tdrives_target;
    @SerializedName("visits_target")
    @Expose
    private Integer visits_target;
    @SerializedName("bookings_target")
    @Expose
    private Integer bookings_target;
    @SerializedName("retail_target")
    @Expose
    private Integer retail_target;
    @SerializedName("exchange_target")
    @Expose
    private Integer exchange_target;
    @SerializedName("finance_target")
    @Expose
    private Integer finance_target;

    public Integer getExchange_target() {
        return exchange_target;
    }

    public void setExchange_target(Integer exchange_target) {
        this.exchange_target = exchange_target;
    }

    public Integer getFinance_target() {
        return finance_target;
    }

    public void setFinance_target(Integer finance_target) {
        this.finance_target = finance_target;
    }


    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getSalesManager() {
        return salesManager;
    }

    public void setSalesManager(String salesManager) {
        this.salesManager = salesManager;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDpUrl() {
        return dpUrl;
    }

    public void setDpUrl(String dpUrl) {
        this.dpUrl = dpUrl;
    }

    public Integer getEnquiries() {
        return enquiries;
    }

    public void setEnquiries(Integer enquiries) {
        this.enquiries = enquiries;
    }

    public Integer getTdrives() {
        return tdrives;
    }

    public void setTdrives(Integer tdrives) {
        this.tdrives = tdrives;
    }

    public Integer getVisits() {
        return visits;
    }

    public void setVisits(Integer visits) {
        this.visits = visits;
    }

    public Integer getBookings() {
        return bookings;
    }

    public void setBookings(Integer bookings) {
        this.bookings = bookings;
    }

    public Integer getRetail() {
        return retail;
    }

    public void setRetail(Integer retail) {
        this.retail = retail;
    }

    public Integer getEnquiries_target() {
        return enquiries_target;
    }

    public void setEnquiries_target(Integer enquiries_target) {
        this.enquiries_target = enquiries_target;
    }

    public Integer getTdrives_target() {
        return tdrives_target;
    }

    public void setTdrives_target(Integer tdrives_target) {
        this.tdrives_target = tdrives_target;
    }

    public Integer getVisits_target() {
        return visits_target;
    }

    public void setVisits_target(Integer visits_target) {
        this.visits_target = visits_target;
    }

    public Integer getBookings_target() {
        return bookings_target;
    }

    public void setBookings_target(Integer bookings_target) {
        this.bookings_target = bookings_target;
    }

    public Integer getRetail_target() {
        return retail_target;
    }

    public void setRetail_target(Integer retail_target) {
        this.retail_target = retail_target;
    }
}
