package com.salescrm.telephony.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by prateek on 11/10/17.
 */

public class EnquiryListStageTotal {
    @SerializedName("error")
    @Expose
    private Error error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }


    public class Result {

        @SerializedName("all")
        @Expose
        private Integer all;
        @SerializedName("notcontacted")
        @Expose
        private Integer notcontacted;
        @SerializedName("qualified")
        @Expose
        private Integer qualified;
        @SerializedName("assigned")
        @Expose
        private Integer assigned;
        @SerializedName("testdrive")
        @Expose
        private Integer testdrive;
        @SerializedName("booked")
        @Expose
        private Integer booked;
        @SerializedName("invoiced")
        @Expose
        private Integer invoiced;
        @SerializedName("finance")
        @Expose
        private Integer finance;
        @SerializedName("closed")
        @Expose
        private Integer closed;
        @SerializedName("evaluation")
        @Expose
        private Integer evaluation;

        public Integer getAll() {
            return all;
        }

        public void setAll(Integer all) {
            this.all = all;
        }

        public Integer getNotcontacted() {
            return notcontacted;
        }

        public void setNotcontacted(Integer notcontacted) {
            this.notcontacted = notcontacted;
        }

        public Integer getQualified() {
            return qualified;
        }

        public void setQualified(Integer qualified) {
            this.qualified = qualified;
        }

        public Integer getAssigned() {
            return assigned;
        }

        public void setAssigned(Integer assigned) {
            this.assigned = assigned;
        }

        public Integer getTestdrive() {
            return testdrive;
        }

        public void setTestdrive(Integer testdrive) {
            this.testdrive = testdrive;
        }

        public Integer getBooked() {
            return booked;
        }

        public void setBooked(Integer booked) {
            this.booked = booked;
        }

        public Integer getInvoiced() {
            return invoiced;
        }

        public void setInvoiced(Integer invoiced) {
            this.invoiced = invoiced;
        }

        public Integer getFinance() {
            return finance;
        }

        public void setFinance(Integer finance) {
            this.finance = finance;
        }

        public Integer getClosed() {
            return closed;
        }

        public void setClosed(Integer closed) {
            this.closed = closed;
        }

        public Integer getEvaluation() {
            return evaluation;
        }

        public void setEvaluation(Integer evaluation) {
            this.evaluation = evaluation;
        }

    }
}
