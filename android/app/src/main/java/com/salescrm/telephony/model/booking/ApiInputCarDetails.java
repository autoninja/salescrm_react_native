package com.salescrm.telephony.model.booking;

/**
 * Created by bharath on 19/3/18.
 */

public class ApiInputCarDetails {
    private Integer model_id, variant_id, fuel_type_id, color_id;
    private Integer quantity;

    public ApiInputCarDetails(Integer model_id, Integer variant_id, Integer fuel_type_id, Integer color_id, Integer quantity) {
        this.model_id = model_id;
        this.variant_id = variant_id;
        this.fuel_type_id = fuel_type_id;
        this.color_id = color_id;
        this.quantity = quantity;
    }

    public Integer getModel_id() {
        return model_id;
    }

    public void setModel_id(Integer model_id) {
        this.model_id = model_id;
    }

    public Integer getVariant_id() {
        return variant_id;
    }

    public void setVariant_id(Integer variant_id) {
        this.variant_id = variant_id;
    }

    public Integer getFuel_type_id() {
        return fuel_type_id;
    }

    public void setFuel_type_id(Integer fuel_type_id) {
        this.fuel_type_id = fuel_type_id;
    }

    public Integer getColor_id() {
        return color_id;
    }

    public void setColor_id(Integer color_id) {
        this.color_id = color_id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}

