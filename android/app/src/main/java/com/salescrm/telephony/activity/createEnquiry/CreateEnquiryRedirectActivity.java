package com.salescrm.telephony.activity.createEnquiry;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.model.CreateLeadInputDataServer;
import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;

import io.realm.Realm;

public class CreateEnquiryRedirectActivity extends AppCompatActivity {
    private String generatedLeadId;
    private ProgressDialog progressDialog;
    private Preferences pref;
    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(this);

        Intent intent = getIntent();
        String leadId = intent.getStringExtra("leadId");
        String scheduledActivityId = intent.getStringExtra("scheduledActivityId");
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Redirecting to C360 !!!.. Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        fetchC360(leadId);
        createAllotDseTask(leadId,
                Util.getInt(scheduledActivityId),
                intent.getStringExtra("firstName"),
                intent.getStringExtra("lastName"),
                intent.getStringExtra("title"),
                intent.getStringExtra("leadTagId")
                );
    }

    private void fetchC360(String leadId) {
        progressDialog.setMessage("Redirecting to C360 !!!.. Please wait");
        progressDialog.show();
        this.generatedLeadId = leadId;

        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(leadId);
        new FetchC360InfoOnCreateLead(new FetchC360OnCreateLeadListener() {
            @Override
            public void onFetchC360OnCreateLeadSuccess() {
                progressDialog.dismiss();
                pref.setLeadID("" + generatedLeadId);
                SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", Util.getInt(generatedLeadId)).findFirst();
                if (salesCRMRealmTable != null) {
                    pref.setScheduledActivityId(salesCRMRealmTable.getScheduledActivityId());

                }

                startActivity(new Intent(CreateEnquiryRedirectActivity.this, C360Activity.class));
                CreateEnquiryRedirectActivity.this.finish();
            }

            @Override
            public void onFetchC360OnCreateLeadError() {
                progressDialog.dismiss();
                Util.showToast(getApplicationContext(), "Failed to fetch C360 information", Toast.LENGTH_SHORT);
                CreateEnquiryRedirectActivity.this.finish();

            }
        }, getApplicationContext(), leadData, pref.getAppUserId()).call(true);

    }

    private void createAllotDseTask(String leadId, Integer scheduled_activity_id,
                                    String firsName, String lastName,
                                    String title,
                                    String leadTagId){
        realm.beginTransaction();
        SalesCRMRealmTable salesCRMRealmTable = new SalesCRMRealmTable();
        salesCRMRealmTable.setScheduledActivityId(scheduled_activity_id);
        salesCRMRealmTable.setLeadId(Long.parseLong(leadId));
        salesCRMRealmTable.setFirstName(firsName);
        salesCRMRealmTable.setLastName(lastName);
        salesCRMRealmTable.setTitle(title);
        salesCRMRealmTable.setActivityId(WSConstants.TaskActivityName.ALLOT_DSE_ID);
        salesCRMRealmTable.setActivityScheduleDate(Util.getDateTime(0));
        salesCRMRealmTable.setActivityName(WSConstants.TaskActivityName.ALLOT_DSE);
        salesCRMRealmTable.setIsLeadActive(1);
        salesCRMRealmTable.setLeadTagsName(leadTagId);
        salesCRMRealmTable.setDone(true);
        salesCRMRealmTable.setSync(true);
        realm.copyToRealmOrUpdate(salesCRMRealmTable);
        realm.commitTransaction();

    }
}
