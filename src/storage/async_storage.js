import { AsyncStorage } from "react-native"

let _storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    console.log('Error saving data');
  }
}
let _retrieveData = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      // We have data!!
      return value;
    }
  } catch (error) {
    // Error retrieving data
    console.log('Error retirving data');
  }
  return null;
}

export const isLoggedIn = () => {
  return _retrieveData('is_logged_in');
}

export const setLoggedIn = (is_logged_in) => {
  return _storeData('is_logged_in', is_logged_in);
}

export const getDealershipSelectedId = () => {
  return _retrieveData('dealership_selected_id');
}

export const setDealershipSelectedId = (dealership_selected_id) => {
  return _storeData('dealership_selected_id', dealership_selected_id);
}

export const getUserMobileNumber = () => {
  return _retrieveData('app_user_mobile_number');
}

export const setAppUserMobileNumber = (mobile) => {
  return _storeData('app_user_mobile_number', mobile);
}

export const getToken = () => {
  return _retrieveData('token');
}

export const setToken = (token) => {
  return _storeData('token', token);
}
export const getLastOpenedDate = () => {
  return _retrieveData('last_opened_date');
}

export const setLastOpenedDate = (date) => {
  return _storeData('last_opened_date', date + '');
}
export const getAppUserId = () => {
  return _retrieveData('app_user_id');
}

export const setAppUserId = (id) => {
  return _storeData('app_user_id', id);
}

export const getSelectedGameLocationId = () => {
  return _retrieveData('selected_game_location_id');
}

export const setSelectedGameLocationId = (id) => {
  return _storeData('selected_game_location_id', id);
}
export const getSystemDate = () => {
  return _retrieveData('system_date');
}

export const setSystemDate = (date) => {
  return _storeData('system_date', date);
}
export const getGameIntroSeen = () => {
  return _retrieveData('game_intro_seen');
}

export const setGameIntroSeen = (seen) => {
  return _storeData('game_intro_seen', seen);
}
export const getHereMapAppId = () => {
  return _retrieveData('here_map_app_id');
}

export const setHereMapAppId = (id) => {
  return _storeData('here_map_app_id', id);
}
export const getHereMapAppCode = () => {
  return _retrieveData('here_map_app_code');
}

export const setHereMapAppCode = (id) => {
  return _storeData('here_map_app_code', id);
}
export const getGovtApiKeys = () => {
  return _retrieveData('gov_data_api_keys');
}

export const setGovtApiKeys = (keys) => {
  return _storeData('gov_data_api_keys', keys);
}
export const deleteAll = () => {
  AsyncStorage.clear();
}
