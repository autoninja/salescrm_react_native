import React, { Component } from 'react';
import { createStackNavigator, createMaterialTopTabNavigator } from 'react-navigation';
import TeamDashboard from  '../components/team_dashboard';
import TodaysTasks from  '../components/todays_tasks';
import TeamDashboardChild from '../components/team_dashboard_child';
import UserInfo from '../components/user_info';
import ETVBRDetails from '../components/etvbr_details';
import ETVBRFilters from '../components/etvbr_filters';
import ETVBRFiltersChild from '../components/etvbr_filters_child';
import DateRangePickerDialog from '../components/date_range_picker';
import HamburgerIcon from '../components/uielements/HamburgerIcon';
import BackIcon from '../components/uielements/BackIcon';
import LoginRedirector from '../components/login_redirector';

const FirstActivity_StackNavigator = createStackNavigator({
        TeamDashboard: {
            screen: createMaterialTopTabNavigator(
                {
                    Home: {
                        screen: TeamDashboard,
                        navigationOptions: ({ navigation }) => ({
                            title: 'ETVBRL',
                        }),
                    },
                    TodaysTasks: {
                        screen: TodaysTasks,
                        navigationOptions: ({ navigation }) => ({
                            title: "Today's Tasks",
                        }),
                    },
                },
                {
                    tabBarContainer: {
                        flexDirection: 'row',
                        justifyContent: 'right',
                        alignItems:'center'
                    },
                    tabBarOptions: {
                        style:{
                            backgroundColor:'#2e5e86'
                        },
                        tabStyle: {
                            width: 200,
                            paddingVertical: 5,
                            alignSelf:'center'
                        },
                        indicatorStyle:{
                            backgroundColor:'#fff'
                        }
                    }
                }
            ),
            navigationOptions: ({ navigation }) => ({

                title: 'Team Dashboard',

                headerBackTitle : null,

                headerLeft : <HamburgerIcon navigationProps={ navigation }/>,

                headerStyle: {
                    backgroundColor: '#2e5e86'
                },
                headerTransperent: true,
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                    width: '100%',
                    textAlign: 'left',
                },
            })
        },
        TeamDashboardChild: {
            screen : TeamDashboardChild,
            navigationOptions: ({ navigation }) => ({
                headerStyle: {
                    backgroundColor: '#2e5e86'
                },
                headerLeft : <BackIcon navigationProps = {navigation} />,
                headerBackTitle : null,
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                    width: '100%',
                    textAlign: 'left',
                },
            })
        },
        UserInfo: {
            screen : UserInfo,
            navigationOptions: ({ navigation }) => ({
                header: null
            })
        },
        DateRangePickerDialog: {
            screen : DateRangePickerDialog,
            navigationOptions: ({ navigation }) => ({
                header: null
            })
        },
        ETVBRDetails: {
            screen : ETVBRDetails,

            navigationOptions: ({ navigation }) => ({

                headerBackTitle : null,

                headerLeft : <BackIcon navigationProps={ navigation }/>,

                headerStyle: {
                    backgroundColor: '#2e5e86'
                },
                headerTransperent: true,
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                    width: '100%',
                    textAlign: 'left',
                    paddingRight:60,
                },
            })
        },
        ETVBRFilters: {
            screen : ETVBRFilters,
            navigationOptions: ({ navigation }) => ({
                header: null,
            }),

        },
        LoginRedirector: {
            screen : LoginRedirector,
            navigationOptions: ({ navigation }) => ({
                header: null,
            }),

        },
        ETVBRFiltersChild: {
            screen : ETVBRFiltersChild,
            navigationOptions: ({ navigation }) => ({
                header: null,
            }),

        },
    },
    {
        cardStyle: {
            backgroundColor: '#303030BE',
            opacity: 1,
        },
        mode:'modal'
    },
    {
        initialRouteName: 'ETVBRFilters',
    }
);

export default FirstActivity_StackNavigator;