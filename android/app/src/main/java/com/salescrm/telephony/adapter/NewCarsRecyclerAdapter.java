package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.activity.FormRenderingActivity;
import com.salescrm.telephony.activity.RNBookBikeActivity;
import com.salescrm.telephony.activity.RNBookVehicleActivity;
import com.salescrm.telephony.activity.createEnquiry.RNCreateLeadActivity;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.db.car.AllInterestedCars;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.NewCarsFragment;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.interfaces.NewCarsProgressLoader;
import com.salescrm.telephony.interfaces.PlanNextTaskPostBookingListener;
import com.salescrm.telephony.interfaces.PlanNextTaskResultListener;
import com.salescrm.telephony.model.C360InformationModel;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.model.StandardPickerModel;
import com.salescrm.telephony.model.booking.BookingMainModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.services.service_handlers.CarsFragmentServiceHandler;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.FabricUtils;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.PostBookingTestDriveVisitDonePicker;
import com.salescrm.telephony.views.booking.CarBookingFormMain;
import com.salescrm.telephony.views.booking.CarBookingQuantityForm;
import com.salescrm.telephony.views.booking.CarBookingSubmitForm;
import com.salescrm.telephony.views.CustomDialogs;
import com.salescrm.telephony.views.StandardPicker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by bannhi on 13/6/17.
 */

public class NewCarsRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final boolean isLeadActive;
    RealmList<AllInterestedCars> allInterestedCarsRealmList = new RealmList<>();
    AllInterestedCars allInterestedCar;
    Activity activity;
    String locationId = "0";
    String leadSourceId = "0";
    String leadLastUpdated = "";
    String leadId = "";
    ProgressDialog progressDialog;
    Preferences pref;
    private NewCarsProgressLoader mNewCarsProgressLoader;
    private FormSubmissionInputData formSubmissionInputData;
    private FragmentManager childFragmentManager;
    private Realm realm;

    public NewCarsRecyclerAdapter(FragmentManager childFragmentManager, FormSubmissionInputData formSubmissionInputData,
                                  RealmList<AllInterestedCars> allInterestedCarsRealmList, String locId,
                                  String leadSrId, String leadLastUpdated, String leadId, Activity activity, boolean isLeadActive) {

        this.childFragmentManager = childFragmentManager;
        this.activity = activity;
        this.allInterestedCarsRealmList = allInterestedCarsRealmList;
        this.locationId = locId;
        this.leadSourceId = leadSrId;
        this.leadLastUpdated = leadLastUpdated;
        this.leadId = leadId;
        this.formSubmissionInputData = formSubmissionInputData;
        this.isLeadActive = isLeadActive;

        C360InformationModel.getInstance().setLocationId(locId);
        C360InformationModel.getInstance().setLeadSourceId(leadSrId);
        C360InformationModel.getInstance().setFormSubmissionInput(formSubmissionInputData);
        C360InformationModel.getInstance().setLeadId(leadId);
        C360InformationModel.getInstance().setLeadLastUpdated(leadLastUpdated);

        pref = Preferences.getInstance();
        pref.load((Context) activity);
        realm = Realm.getDefaultInstance();

    }

    private void bookBike(){
        Intent bookBikeActivity = new Intent(activity, RNBookBikeActivity.class);
        bookBikeActivity.putExtra("leadId",leadId);
        bookBikeActivity.putExtra("leadLastUpdated",leadLastUpdated);
        bookBikeActivity.putExtra("formSubmissionInputData", new Gson().toJson(formSubmissionInputData));
        activity.startActivity(bookBikeActivity);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.new_car_card, null);
        mNewCarsProgressLoader = new NewCarsFragment();
        System.out.println("NEW CARS ADAPTER CALLED");
        return new CarItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RecyclerView.ViewHolder iViewHolder = holder;

        if (holder instanceof CarItemHolder && allInterestedCarsRealmList != null && allInterestedCarsRealmList.get(position) != null &&
                allInterestedCarsRealmList.get(position).isValid()) {
            if (allInterestedCarsRealmList.get(position).getIntrestedCarIsPrimary() == 0) {
                ((CarItemHolder) iViewHolder).carStarIb.setImageResource(R.drawable.ic_star_border_black_24dp);

            } else if (allInterestedCarsRealmList.get(position).getIntrestedCarIsPrimary() == 1) {
                ((CarItemHolder) iViewHolder).carStarIb.setImageResource(R.drawable.ic_filled_star_img_24);

            }
            ((CarItemHolder) iViewHolder).carNameTv.setText(allInterestedCarsRealmList.get(position).getIntrestedCarName());
            ((CarItemHolder) iViewHolder).carNameTv.setText(allInterestedCarsRealmList.get(position).getIntrestedCarName());
            ((CarItemHolder) iViewHolder).bookingFlagTv.setText(allInterestedCarsRealmList.get(position).getIntrestedCarColorId());
            ((CarItemHolder) iViewHolder).bookingIdTv.setText("Booking ID- " + allInterestedCarsRealmList.get(position).getBooking_amount());
            ((CarItemHolder) iViewHolder).carEditIb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isLeadActive) {
                        handleCarEditClick(allInterestedCarsRealmList.get(position));
                    }
                }
            });
            ((CarItemHolder) iViewHolder).carStarIb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isLeadActive) {
                        if (allInterestedCarsRealmList.get(position).getIsPrimary() == 0) {
                            ((CarItemHolder) iViewHolder).carStarIb.setImageResource(R.drawable.ic_filled_star_img_24);
                            handleCarStarClick(allInterestedCarsRealmList.get(position));
                        }
                    }

                }
            });
            if (DbUtils.isBike()) {

                ((CarItemHolder) iViewHolder).tvBookingDetails.setVisibility(View.GONE);
                ((CarItemHolder) iViewHolder).tvTdVisit.setVisibility(View.GONE);
                ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);
                ((CarItemHolder) iViewHolder).carEditIb.setVisibility(View.GONE);
                ((CarItemHolder) iViewHolder).bookingFlagTv.setVisibility(View.GONE);
                ((CarItemHolder) iViewHolder).bookingIdTv.setVisibility(View.GONE);
                ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.GONE);
            }
            else if(DbUtils.isClientTwoWheeler() || pref.isTwoWheelerClient()) {
                ((CarItemHolder) iViewHolder).tvBookingDetails.setVisibility(View.GONE);
                ((CarItemHolder) iViewHolder).tvTdVisit.setVisibility(View.GONE);
                ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.VISIBLE);
                ((CarItemHolder) iViewHolder).carEditIb.setVisibility(View.GONE);
                ((CarItemHolder) iViewHolder).bookingFlagTv.setVisibility(View.GONE);
                ((CarItemHolder) iViewHolder).bookingIdTv.setVisibility(View.GONE);
                ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.GONE);
            }
            else {
                if(Util.getInt(allInterestedCarsRealmList.get(position).getCar_stage_id()) == WSConstants.CarBookingStages.BOOK_CAR) {
                    ((CarItemHolder) iViewHolder).carEditIb.setVisibility(View.VISIBLE);
                }
                else {
                    ((CarItemHolder) iViewHolder).carEditIb.setVisibility(View.GONE);
                }
                ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.VISIBLE);
                ((CarItemHolder) iViewHolder).tvBookingDetails.setVisibility(View.VISIBLE);
                ((CarItemHolder) iViewHolder).tvTdVisit.setVisibility(View.VISIBLE);
                ((CarItemHolder) iViewHolder).bookingFlagTv.setVisibility(View.VISIBLE);
                ((CarItemHolder) iViewHolder).bookingIdTv.setVisibility(View.VISIBLE);
                ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.VISIBLE);

            }

            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(DbUtils.isClientTwoWheeler() || pref.isTwoWheelerClient()) {
                        bookBike();
                    }
                    else {
                        handleStageChangeBtnClick(allInterestedCarsRealmList.get(position), position);
                    }
                }
            });
            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(activity, RNBookVehicleActivity.class);
                    intent.putExtra("lead_id", allInterestedCarsRealmList.get(position).getLeadId()+"");
                    intent.putExtra("lead_car_id",allInterestedCarsRealmList.get(position).getIntrestedLeadCarId()+"");
                    intent.putExtra("model_name", allInterestedCarsRealmList.get(position).getModel());
                    intent.putExtra("stage_id", allInterestedCarsRealmList.get(position).getCar_stage_id()+"");
                    intent.putExtra("booking_id", allInterestedCarsRealmList.get(position).getBooking_id()+"");
                    intent.putExtra("action", "update_booking");
                    activity.startActivity(intent);
                    return false;
                }
            });

            ((CarItemHolder) iViewHolder).cancelBookingTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    HashMap<String, Object> hashMap = new HashMap<String, Object>();
                    hashMap.put(CleverTapConstants.EVENT_BOOKING_KEY_TYPE,
                            CleverTapConstants.EVENT_BOOKING_TYPE_CANCEL);
                    CleverTapPush.pushEvent(CleverTapConstants.EVENT_BOOKING, hashMap);

                    handleCancelBookingClick(allInterestedCarsRealmList.get(position), iViewHolder);
                }
            });

            ((CarItemHolder) iViewHolder).cancelBookingTv.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(activity, RNBookVehicleActivity.class);
                    intent.putExtra("lead_id", allInterestedCarsRealmList.get(position).getLeadId()+"");
                    intent.putExtra("lead_car_id",allInterestedCarsRealmList.get(position).getIntrestedLeadCarId()+"");
                    intent.putExtra("model_name", allInterestedCarsRealmList.get(position).getModel());
                    intent.putExtra("stage_id", allInterestedCarsRealmList.get(position).getCar_stage_id()+"");
                    intent.putExtra("booking_id", allInterestedCarsRealmList.get(position).getBooking_id()+"");
                    intent.putExtra("action", "cancel_booking");
                    activity.startActivity(intent);
                    return false;
                }
            });


            ((CarItemHolder)iViewHolder).tvBookingDetails.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(activity, RNBookVehicleActivity.class);
                    intent.putExtra("lead_id", allInterestedCarsRealmList.get(position).getLeadId()+"");
                    intent.putExtra("lead_car_id",allInterestedCarsRealmList.get(position).getIntrestedLeadCarId()+"");
                    intent.putExtra("model_name", allInterestedCarsRealmList.get(position).getModel());
                    intent.putExtra("stage_id", allInterestedCarsRealmList.get(position).getCar_stage_id()+"");
                    intent.putExtra("booking_id", allInterestedCarsRealmList.get(position).getBooking_id()+"");
                    intent.putExtra("action", "details_booking");
                    activity.startActivity(intent);
                    return false;
                }
            });

            ((CarItemHolder) iViewHolder).tvBookingDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    HashMap<String, Object> hashMap = new HashMap<String, Object>();
                    hashMap.put(CleverTapConstants.EVENT_BOOKING_KEY_TYPE,
                            CleverTapConstants.EVENT_BOOKING_TYPE_DETAILS);
                    CleverTapPush.pushEvent(CleverTapConstants.EVENT_BOOKING, hashMap);

                    C360InformationModel.getInstance().setAllInterestedCars(allInterestedCarsRealmList.get(position));
                    Intent intent = new Intent(activity, CarBookingFormMain.class);
                    intent.putExtra("stage_id", WSConstants.CarBookingStages.CUSTOM_CAR_BOOKED_EDIT);
                    activity.startActivity(intent);
                }
            });

            ((CarItemHolder) iViewHolder).tvTdVisit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new PostBookingTestDriveVisitDonePicker(activity, new PlanNextTaskPostBookingListener() {
                        @Override
                        public void onPlanNextTaskPostBookingResult(int activityId, int answerId) {
                            if(isInternetAvailable()) {
                                Intent formRenderingActivityIntent = new Intent(activity, FormRenderingActivity.class);
                                formRenderingActivityIntent.putExtra("form_title", "Update");
                                formRenderingActivityIntent.putExtra("form_action", "Send Otp");
                                formRenderingActivityIntent.putExtra("action_id", WSConstants.FormAction.PRE_DONE);
                                formRenderingActivityIntent.putExtra("activity_id", activityId);
                                formRenderingActivityIntent.putExtra("from_c360", true);
                                formRenderingActivityIntent.putExtra("from_post_booking", true);
                                formRenderingActivityIntent.putExtra("scheduled_activity_id", WSConstants.DEFAULT_SCHEDULED_ACTIVITY_ID_POST_BOOKING);
                                formRenderingActivityIntent.putExtra("answer_id",answerId);
                                formRenderingActivityIntent.putExtra("lead_car_id",allInterestedCarsRealmList.get(position).getIntrestedLeadCarId()+"");
                                activity.startActivity(formRenderingActivityIntent);
                                if (activity instanceof Activity) {
                                    ((Activity) (activity)).finish();
                                }
                            }
                            else {
                                Util.showToast(activity,"No internet connection",Toast.LENGTH_SHORT);
                            }
                        }
                    }).show();
                    CleverTapPush.pushEvent(CleverTapConstants.EVENT_POST_BOOKING_TD_VISIT);
                }
            });

            populateData(iViewHolder, allInterestedCarsRealmList.get(position));


            SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                    .equalTo("leadId", Util.getInt(pref.getLeadID()))
                    .findFirst();
            if (salesCRMRealmTable != null) {
                if (salesCRMRealmTable.getIsLeadActive() == 0) {
                    ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);
                    ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.GONE);
                }
            }


        }
    }

    private boolean isInternetAvailable() {
        return new ConnectionDetectorService(activity).isConnectingToInternet();
    }


    @Override
    public int getItemCount() {
        return allInterestedCarsRealmList.size();
    }

    private void populateData(RecyclerView.ViewHolder iViewHolder, AllInterestedCars allInterestedCar) {

        int stageId = 0;
        boolean showTD = false;
        if (allInterestedCar.getCar_stage_id() != null && !allInterestedCar.getCar_stage_id().isEmpty()) {
            try {
                stageId = Integer.parseInt(allInterestedCar.getCar_stage_id());
                //  stageId = 3;
                switch (stageId) {
                    case WSConstants.CarBookingStages.BOOK_CAR:
                        //case 0: ie clicked on book car-> call book car api
                        ((CarItemHolder) iViewHolder).bookingIdTv.setVisibility(View.GONE);
                        if (pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.DSE) || pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.TEAM_LEAD)
                                ||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.MANAGER)||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.BRANCH_HEAD)
                                ||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.OPERATIONS)) {
                            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setText("Book Vehicle");
                        } else {
                            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);
                        }
                        ((CarItemHolder) iViewHolder).tvBookingDetails.setVisibility(View.GONE);
                        if(pref.isShowPostBookingTdVisit()) {
                            ((CarItemHolder) iViewHolder).tvTdVisit.setVisibility(View.VISIBLE);
                        }
                        else {
                            ((CarItemHolder) iViewHolder).tvTdVisit.setVisibility(View.GONE);

                        }
                        ((CarItemHolder) iViewHolder).flaggedBlock.setVisibility(View.GONE);
                        ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.GONE);
                        if (allInterestedCar.getTestDriveConducted() == 1) {
                            ((CarItemHolder) iViewHolder).stageTv.setText("Test Drive: Conducted");
                            ((CarItemHolder) iViewHolder).stageIcon.setVisibility(View.VISIBLE);
                        } else {
                            ((CarItemHolder) iViewHolder).stageTv.setText("Test Drive: Not Conducted");
                            ((CarItemHolder) iViewHolder).stageIcon.setVisibility(View.GONE);
                        }
                        showTD = true;
                        break;
                    case WSConstants.CarBookingStages.CAR_BOOKED_HALF_PAYMENT:
                        //case 1: half payment received. button shows full payment recieved. click hence call full payment api
                        ((CarItemHolder) iViewHolder).flaggedBlock.setVisibility(View.VISIBLE);
                        ((CarItemHolder) iViewHolder).bookingFlagTv.setText("BOOKED");
                        ((CarItemHolder) iViewHolder).bookingIdTv.setText("Booking ID- "+allInterestedCar.getBooking_number());
                        ((CarItemHolder) iViewHolder).stageTv.setVisibility(View.GONE);
                        if (pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.DSE) || pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.TEAM_LEAD)
                                ||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.MANAGER)||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.BRANCH_HEAD)
                                ||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.OPERATIONS)) {
                            ((CarItemHolder) iViewHolder).stageIcon.setVisibility(View.GONE);
                            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setText("Full Payment Recieved");
                            ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.VISIBLE);
                            ((CarItemHolder) iViewHolder).cancelBookingTv.setText("Cancel Booking");
                        } else {
                            ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.GONE);
                            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);
                        }
                        showTD = false;
                        break;
                    case WSConstants.CarBookingStages.INVOICE_PENDING:
                        ((CarItemHolder) iViewHolder).flaggedBlock.setVisibility(View.VISIBLE);
                        ((CarItemHolder) iViewHolder).bookingFlagTv.setText("BOOKED");
                        ((CarItemHolder) iViewHolder).stageIcon.setVisibility(View.GONE);
                        ((CarItemHolder) iViewHolder).bookingIdTv.setText("Booking ID- "+allInterestedCar.getBooking_number());
                        if (pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.DSE) || pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.TEAM_LEAD)
                                ||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.MANAGER)||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.BRANCH_HEAD)||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.OPERATIONS)) {
                            ((CarItemHolder) iViewHolder).stageTv.setText("Sent for Invoice");
                            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);
                            ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.VISIBLE);
                            ((CarItemHolder) iViewHolder).cancelBookingTv.setText("Cancel Booking");
                        } else if (pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.BILLING_EXECUTIVE)) {
                            ((CarItemHolder) iViewHolder).stageTv.setText("Full Payment Recieved");
                            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.VISIBLE);
                            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setText("UPDATE INVOICE");
                            ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.GONE);
                            //((CarItemHolder) iViewHolder).cancelBookingTv.setText("Cancel Invoice");
                        }
                        showTD = false;
                        break;
                    case WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING:
                        if (pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.DSE) || pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.TEAM_LEAD)
                                ||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.MANAGER)||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.BRANCH_HEAD)||pref.getRoleId().equalsIgnoreCase(WSConstants.ROLES.OPERATIONS)) {
                            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.VISIBLE);
                            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setText("Update Delivery");
                            ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.VISIBLE);
                            ((CarItemHolder) iViewHolder).cancelBookingTv.setText("Cancel Booking");
                        } else {
                            ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.VISIBLE);
                            ((CarItemHolder) iViewHolder).cancelBookingTv.setText("Cancel Booking");
                            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);
                        }
                        ((CarItemHolder) iViewHolder).flaggedBlock.setVisibility(View.VISIBLE);
                        ((CarItemHolder) iViewHolder).bookingFlagTv.setText("INVOICED");
                        ((CarItemHolder) iViewHolder).bookingFlagTv.setBackgroundResource(R.drawable.invoiced_tag);
                        ((CarItemHolder) iViewHolder).bookingIdTv.setText(String.format("Booking ID- %s%s", allInterestedCar.getBooking_number(), allInterestedCar.getInvoiceDetails() == null ? "" : "\nInvoice ID- " + allInterestedCar.getInvoiceDetails().getInvoice_number()));
                        ((CarItemHolder) iViewHolder).stageTv.setText("Delivery Due: " + allInterestedCar.getExpected_delivery_date());
                        ((CarItemHolder) iViewHolder).stageIcon.setVisibility(View.GONE);
                        showTD = false;
                        break;
                    case WSConstants.CarBookingStages.DELIVERED:

                        //delivered: no button press
                        ((CarItemHolder) iViewHolder).tvTdVisit.setVisibility(View.GONE);
                        ((CarItemHolder) iViewHolder).flaggedBlock.setVisibility(View.VISIBLE);
                        ((CarItemHolder) iViewHolder).bookingFlagTv.setText("DELIVERED");
                        ((CarItemHolder) iViewHolder).bookingFlagTv.setBackgroundResource(R.drawable.delivered_tag);
                        ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.GONE);
                        ((CarItemHolder) iViewHolder).bookingIdTv.setText("Booking ID- "+allInterestedCar.getBooking_number());
                        ((CarItemHolder) iViewHolder).stageTv.setText("Delivery Date: " + allInterestedCar.getExpected_delivery_date());
                        ((CarItemHolder) iViewHolder).stageIcon.setVisibility(View.GONE);
                        ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);
                        showTD = false;
                        break;
                    case WSConstants.CarBookingStages.CAR_CANCELLED:
                        ((CarItemHolder) iViewHolder).tvBookingDetails.setVisibility(View.GONE);
                        ((CarItemHolder) iViewHolder).tvTdVisit.setVisibility(View.GONE);
                        ((CarItemHolder) iViewHolder).bookingIdTv.setVisibility(View.GONE);
                        ((CarItemHolder) iViewHolder).flaggedBlock.setVisibility(View.VISIBLE);
                        ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.GONE);
                        ((CarItemHolder) iViewHolder).bookingFlagTv.setText("CANCELLED");
                        ((CarItemHolder) iViewHolder).bookingFlagTv.setBackgroundResource(R.drawable.cancelled_tag);
                        ((CarItemHolder) iViewHolder).bookingIdTv.setText("Booking ID- "+allInterestedCar.getBooking_number());
                        ((CarItemHolder) iViewHolder).stageTv.setVisibility(View.GONE);
                        ((CarItemHolder) iViewHolder).stageIcon.setVisibility(View.GONE);
                        ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);
                        showTD = false;
                        break;


                }

               /* if (DbUtils.isBike() || DbUtils.isClientTwoWheeler() || pref.isTwoWheelerClient()) {
                    ((CarItemHolder) iViewHolder).flaggedBlock.setVisibility(View.GONE);
                    ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);
                    *//*((CarItemHolder) iViewHolder).stageTv.setVisibility(View.GONE);
                    ((CarItemHolder) iViewHolder).stageIcon.setVisibility(View.GONE);*//*
                }*/
                if (!showTD) {
                    ((CarItemHolder) iViewHolder).stageTv.setVisibility(View.GONE);
                    ((CarItemHolder) iViewHolder).stageIcon.setVisibility(View.GONE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(isJustEvaluatorOrEvaluatorManager()){
            ((CarItemHolder) iViewHolder).tvBookingDetails.setEnabled(false);
            ((CarItemHolder) iViewHolder).tvBookingDetails.setVisibility(View.GONE);
            ((CarItemHolder) iViewHolder).carEditIb.setEnabled(false);
            ((CarItemHolder) iViewHolder).carEditIb.setVisibility(View.GONE);
            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setEnabled(false);
            ((CarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);
            ((CarItemHolder) iViewHolder).tvTdVisit.setVisibility(View.GONE);
            ((CarItemHolder) iViewHolder).cancelBookingTv.setVisibility(View.GONE);
        }

    }

    private boolean isJustEvaluatorOrEvaluatorManager() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if ((!userRoles.get(i).getId().equalsIgnoreCase(WSConstants.ROLES.DSE) || !userRoles.get(i).getId().equalsIgnoreCase(WSConstants.ROLES.TEAM_LEAD)
                        || !userRoles.get(i).getId().equalsIgnoreCase(WSConstants.ROLES.MANAGER)|| !userRoles.get(i).getId().equalsIgnoreCase(WSConstants.ROLES.BRANCH_HEAD)
                        || !userRoles.get(i).getId().equalsIgnoreCase(WSConstants.ROLES.OPERATIONS))) {
                    if((userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR)
                            || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR_MANAGER))) {
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * handling click on edit button
     */
    private void handleCarEditClick(AllInterestedCars allInterestedCar) {
        CustomDialogs.getInstance(activity, allInterestedCar, mNewCarsProgressLoader).showEditCarDialog();
    }

    /**
     * handling click on star button
     */
    private void handleCarStarClick(AllInterestedCars allInterestedCar) {
        //Toast.makeText(activity, "Star Clicked", Toast.LENGTH_LONG).show();
        CarsFragmentServiceHandler.getInstance(activity).markCarPrimary(pref.getAccessToken(), "" + allInterestedCar.getLeadId(),
                "" + allInterestedCar.getIntrestedLeadCarId(), leadLastUpdated);

    }

    /**
     * handling change of stage
     */
    private void handleStageChangeBtnClick(final AllInterestedCars allInterestedCar, int pos) {
        if(allInterestedCar == null || !allInterestedCar.isValid()){
            Util.showToast(activity, "Please close the app and try again",Toast.LENGTH_SHORT);
            return;
        }
        int stageId = 0;
        boolean done = false;
        C360InformationModel.getInstance().setAllInterestedCars(allInterestedCar);
        if (allInterestedCar.getCar_stage_id() != null && !allInterestedCar.getCar_stage_id().isEmpty()) {
            try {
                stageId = Integer.parseInt(allInterestedCar.getCar_stage_id());
                // stageId = 3;
                switch (stageId) {
                    case WSConstants.CarBookingStages.BOOK_CAR:
                        if (!locationId.equalsIgnoreCase("0") && !locationId.equalsIgnoreCase("") && !locationId.isEmpty()) {
                                        for (int i = 0; i < allInterestedCarsRealmList.size(); i++) {
                                            if (i != pos && Integer.parseInt(allInterestedCarsRealmList.get(i).getCar_stage_id()) != WSConstants.CarBookingStages.BOOK_CAR
                                                    && Integer.parseInt(allInterestedCarsRealmList.get(i).getCar_stage_id()) != WSConstants.CarBookingStages.CAR_CANCELLED) {
//                                                CustomDialogs.getInstance(activity, allInterestedCar, mNewCarsProgressLoader).
//                                                        showAlrdyInvoicedDialog(locationId, leadSourceId, formSubmissionInputData,leadLastUpdated);

                                                List<StandardPickerModel> standardPickerModels = new ArrayList<>();
                                                standardPickerModels.add(new StandardPickerModel(WSConstants.StandardPickerIds.YES, R.drawable.ic_check_24dp, "YES"));
                                                standardPickerModels.add(new StandardPickerModel(WSConstants.StandardPickerIds.NO, R.drawable.ic_close_no_bg_white_24dp, "NO"));
                                                final int finalStageId = stageId;
                                                new StandardPicker().show(activity, "One vehicle is already in booking process. Do you still want to proceed?",
                                                        standardPickerModels,
                                                        new StandardPicker.StandardPickerListener() {
                                                            @Override
                                                            public void onSelectStandardPickerItem(StandardPickerModel standardPickerModel) {

                                                                if (standardPickerModel.getId() == WSConstants.StandardPickerIds.YES) {
                                                                    CarBookingQuantityForm.getInstance().show(activity, allInterestedCar.getModel(), new CarBookingQuantityForm.CarBookingQuantitySelectListener() {
                                                                        @Override
                                                                        public void onCarQuantitySelect(int quantity) {
                                                                            Intent intent = new Intent(activity, CarBookingFormMain.class);
                                                                            intent.putExtra("booking_quantity", quantity);
                                                                            intent.putExtra("stage_id", WSConstants.CarBookingStages.BOOK_CAR);
                                                                            activity.startActivity(intent);
                                                                        }
                                                                    });

                                                                }


                                                            }
                                                        });
                                                done = true;
                                                break;
                                            }
                                        }
                                        if (!done) {
                                            CarBookingQuantityForm.getInstance().show(activity, allInterestedCar.getModel(), new CarBookingQuantityForm.CarBookingQuantitySelectListener() {
                                                @Override
                                                public void onCarQuantitySelect(int quantity) {
                                                    Intent intent = new Intent(activity, CarBookingFormMain.class);
                                                    intent.putExtra("booking_quantity", quantity);
                                                    intent.putExtra("stage_id", WSConstants.CarBookingStages.BOOK_CAR);
                                                    activity.startActivity(intent);
                                                }
                                            });
                                            }

                                    }
                        else {

                            Toast.makeText(activity, "Please assign a DSE before booking a car.", Toast.LENGTH_LONG).show();
                        }

                        break;
                    case WSConstants.CarBookingStages.CAR_BOOKED_HALF_PAYMENT:
                        List<StandardPickerModel> standardPickerModels = new ArrayList<>();
                        standardPickerModels.add(new StandardPickerModel(WSConstants.StandardPickerIds.YES, R.drawable.ic_check_24dp, "YES"));
                        standardPickerModels.add(new StandardPickerModel(WSConstants.StandardPickerIds.NO, R.drawable.ic_close_no_bg_white_24dp, "NO"));
                        new StandardPicker().show(activity,
                                "Full Payment Received?",
                                standardPickerModels,
                                new StandardPicker.StandardPickerListener() {
                                    @Override
                                    public void onSelectStandardPickerItem(StandardPickerModel standardPickerModel) {
                                        if (standardPickerModel.getId() == WSConstants.StandardPickerIds.YES) {
//                                    CustomDialogs.getInstance(activity, allInterestedCar,mNewCarsProgressLoader).showFullPaymentRecievedConfirmationDialog(allInterestedCar,
//                                    Integer.parseInt(locationId.trim()), Integer.parseInt(leadSourceId),leadLastUpdated.trim());

                                           /* new CarBookingFormDelete().show(activity,childFragmentManager, allInterestedCar, new CarBookingFormDelete.CarBookingListener() {
                                                @Override
                                                public void onCarBookingSubmit(BookingMainModel bookingMainModel) {
                                                    CarsFragmentServiceHandler.getInstance(activity).fullPaymentReceived( pref.getAccessToken(),allInterestedCar.getCarId(),
                                                            CarsFragmentServiceHandler.getInstance(activity).createFullPaymentReceivedInputObject(
                                                            bookingMainModel.getRemarks(),
                                                            Util.getInt(allInterestedCar.getBooking_id()),
                                                            allInterestedCar.getBooking_number(),
                                                            allInterestedCar.getIntrestedLeadCarId(),
                                                            Util.getInt(leadSourceId),
                                                            Util.getInt(locationId),
                                                            allInterestedCar.getLeadId(),
                                                            Util.getInt(allInterestedCar.getCar_stage_id()),
                                                            bookingMainModel.getDateAsSQLDate(),
                                                            pref.getAccessToken(),
                                                            allInterestedCar.getCarId(),
                                                            Util.getInt(allInterestedCar.getEnquiry_id()),
                                                            leadLastUpdated, bookingMainModel.getApiInputDetails()));
                                                }
                                            });*/


                                            Intent intent = new Intent(activity, CarBookingFormMain.class);
                                            intent.putExtra("stage_id", WSConstants.CarBookingStages.CAR_BOOKED_HALF_PAYMENT);
                                            activity.startActivity(intent);

                                        } else if (standardPickerModel.getId() == WSConstants.StandardPickerIds.NO) {

                                            CarBookingSubmitForm.getInstance().show(
                                                    activity,
                                                    WSConstants.CarBookingStages.CAR_BOOKED_FULL_PAYMENT_NO,
                                                    new CarBookingSubmitForm.CarBookingSubmitFormListener() {
                                                        @Override
                                                        public void onCarBookingSubmitForm(BookingMainModel bookingMainModel) {
                                                            mNewCarsProgressLoader.progressButtonClicked();
                                                            CarsFragmentServiceHandler.getInstance(activity).rescheduleTask(pref.getAccessToken(),
                                                                    Util.getInt(allInterestedCar.getBooking_id()),
                                                                    allInterestedCar.getIntrestedLeadCarId(),
                                                                    allInterestedCar.getLeadId(),
                                                                    Util.getInt(allInterestedCar.getCar_stage_id()),
                                                                    allInterestedCar.getCarId(),
                                                                    Util.getInt(allInterestedCar.getEnquiry_id()),
                                                                    0,
                                                                    bookingMainModel.getDateAsSQLDate(),
                                                                    bookingMainModel.getRemarks());
                                                        }
                                                    });


                                        }


                                    }
                                });
                        break;
                    case WSConstants.CarBookingStages.INVOICE_PENDING:
                        // if dse login no button press
                        CustomDialogs.getInstance(activity, allInterestedCar, mNewCarsProgressLoader)
                                .showUpdateInvoiceConfirmationDialog(locationId.trim(), leadLastUpdated.trim(), new CallBack() {
                                    @Override
                                    public void onCallBack() {
                                        //Confirm FullPayment Received
                                        Intent intent = new Intent(activity, CarBookingFormMain.class);
                                        intent.putExtra("stage_id", WSConstants.CarBookingStages.INVOICE_PENDING);
                                        activity.startActivity(intent);
                                    }

                                    @Override
                                    public void onCallBack(Object data) {

                                    }

                                    @Override
                                    public void onError(String error) {

                                    }
                                });
                        //if acount login click on update invoice button code to be written
                        break;
                    case WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING:


                        List<StandardPickerModel> standardPickerModelsPending = new ArrayList<>();
                        standardPickerModelsPending.add(new StandardPickerModel(WSConstants.StandardPickerIds.YES, R.drawable.ic_check_24dp, "YES"));
                        standardPickerModelsPending.add(new StandardPickerModel(WSConstants.StandardPickerIds.NO, R.drawable.ic_close_no_bg_white_24dp, "NO"));
                        new StandardPicker().show(activity,
                                "Have You Delivered The Car ?",
                                standardPickerModelsPending,
                                new StandardPicker.StandardPickerListener() {
                                    @Override
                                    public void onSelectStandardPickerItem(StandardPickerModel standardPickerModel) {
                                        if (standardPickerModel.getId() == WSConstants.StandardPickerIds.YES) {

                                            Intent intent = new Intent(activity, CarBookingFormMain.class);
                                            intent.putExtra("stage_id", WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING);
                                            activity.startActivity(intent);

                                        } else if (standardPickerModel.getId() == WSConstants.StandardPickerIds.NO) {

                                            CarBookingSubmitForm.getInstance().show(
                                                    activity,
                                                    WSConstants.CarBookingStages.CUSTOM_INVOICED_DELIVERY_PENDING_NO,
                                                    new CarBookingSubmitForm.CarBookingSubmitFormListener() {
                                                        @Override
                                                        public void onCarBookingSubmitForm(BookingMainModel bookingMainModel) {
                                                            mNewCarsProgressLoader.progressButtonClicked();
                                                            int invoiceId = 0;
                                                            if (allInterestedCar.getInvoiceDetails() != null) {
                                                                invoiceId = allInterestedCar.getInvoiceDetails().getInvoiceId();
                                                            }
                                                            CarsFragmentServiceHandler.getInstance(activity).rescheduleTask(pref.getAccessToken(),
                                                                    Util.getInt(allInterestedCar.getBooking_id()),
                                                                    allInterestedCar.getIntrestedLeadCarId(),
                                                                    allInterestedCar.getLeadId(),
                                                                    Util.getInt(allInterestedCar.getCar_stage_id()),
                                                                    allInterestedCar.getCarId(),
                                                                    Util.getInt(allInterestedCar.getEnquiry_id()),
                                                                    invoiceId,
                                                                    bookingMainModel.getDateAsSQLDate(),
                                                                    bookingMainModel.getRemarks());

                                                        }
                                                    });


                                        }


                                    }
                                });
                        break;


                    case WSConstants.CarBookingStages.DELIVERED:
                        // no button
                        break;
                    case WSConstants.CarBookingStages.CAR_CANCELLED:
                        // no button press
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }

    /**
     * to to cancel a booking
     *
     * @param allInterestedCar
     */
    private void handleCancelBookingClick(final AllInterestedCars allInterestedCar, RecyclerView.ViewHolder iViewHolder) {

        int bookingId = 0, leadCarId = 0, locId = 0, stageId = 0, enquiryId = 0, invoiceId = 0;
        long leadId = 0;
        String presentCarId = "0";
        if (allInterestedCar.getBooking_id() != null && !allInterestedCar.getBooking_id().isEmpty()) {
            bookingId = Integer.parseInt(allInterestedCar.getBooking_id());
        }
        if (allInterestedCar.getCar_stage_id() != null && !allInterestedCar.getCar_stage_id().isEmpty()) {
            stageId = Integer.parseInt(allInterestedCar.getCar_stage_id());
        }
        leadCarId = allInterestedCar.getIntrestedLeadCarId();
        leadId = allInterestedCar.getLeadId();
        locId = Integer.parseInt(locationId);
        invoiceId = allInterestedCar.getInvoiceDetails().getInvoiceId();
        if (allInterestedCar.getCarId() != null && !allInterestedCar.getCarId().isEmpty()) {
            presentCarId = allInterestedCar.getCarId();
        }
        if (allInterestedCar.getEnquiry_id() != null && !allInterestedCar.getEnquiry_id().isEmpty()) {
            enquiryId = Integer.parseInt(allInterestedCar.getEnquiry_id());
        }

        if (((CarItemHolder) iViewHolder).cancelBookingTv.getText().toString().trim().equalsIgnoreCase("Cancel Booking")) {
            CustomDialogs.getInstance((Activity) activity, allInterestedCar, mNewCarsProgressLoader).showCancelReasonDialog(bookingId,
                    leadCarId, locId, leadId, stageId, presentCarId, enquiryId, pref.getAccessToken(), invoiceId==0?null:invoiceId+"", leadLastUpdated);
        } else {
            CustomDialogs.getInstance((Activity) activity, allInterestedCar, mNewCarsProgressLoader).showCancelInvoiceDialog(bookingId,
                    leadCarId, locId, leadId, stageId, presentCarId, enquiryId, invoiceId, pref.getAccessToken(), leadLastUpdated);
        }
    }

    private class CarItemHolder extends RecyclerView.ViewHolder {

        //public   View parentLayout;
        TextView carNameTv, bookingFlagTv, bookingIdTv, cancelBookingTv, stageTv, tvBookingDetails, tvTdVisit;
        Button stageChangeBtnTv;
        ImageButton carStarIb, carEditIb;
        ImageView stageIcon;
        RelativeLayout flaggedBlock;
        RelativeLayout rlProgressLoader;

        public CarItemHolder(View itemView) {
            super(itemView);
            // parentLayout = itemView;
            carNameTv = (TextView) itemView.findViewById(R.id.car_name);
            bookingFlagTv = (TextView) itemView.findViewById(R.id.bookingFlagTv);
            bookingIdTv = (TextView) itemView.findViewById(R.id.booking_id);
            cancelBookingTv = (TextView) itemView.findViewById(R.id.cancelBookingTv);
            stageTv = (TextView) itemView.findViewById(R.id.stage_text);
            stageIcon = (ImageView) itemView.findViewById(R.id.stage_icon);
            stageChangeBtnTv = (Button) itemView.findViewById(R.id.btn_nxt_stage);
            flaggedBlock = (RelativeLayout) itemView.findViewById(R.id.flagged_block);
            tvBookingDetails = (TextView) itemView.findViewById(R.id.tv_booking_details);
            tvTdVisit = itemView.findViewById(R.id.tv_td_visit);

            // rlProgressLoader = (RelativeLayout) view.findViewById(R.id.new_car_frag_progress_loader);

            carStarIb = (ImageButton) itemView.findViewById(R.id.car_star);
            carEditIb = (ImageButton) itemView.findViewById(R.id.car_edit);


        }

    }


}
