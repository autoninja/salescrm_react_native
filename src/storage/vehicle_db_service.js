import { realm } from './realm_schemas';
import UserInfoService from './user_info_service';
import { VehicleModel, VehicleModelsModel, VehicleModelsVariantModel } from '../models/vehicle_model'

const VehicleDBService = {
  findAll: function () {
    return realm.objects('VehicleDB');
  },
  save: function (vehicles) {
    realm.write(() => {
      vehicles.map((vehicle) => {
        realm.create('VehicleDB', vehicle, true);
      })
    })
    let vehicleDB = realm.objects('VehicleDB');
    console.log('VehicleDB.length', vehicleDB.length);
    console.log(JSON.stringify(vehicleDB));
  },
  getInterestedVehicle: () => {
    let vehicle = null;
    let userData = UserInfoService.findFirst();
    if (userData) {
      let vehicleDB = realm.objects('VehicleDB').filtered('brand_id==' + userData.brand_id);
      if (vehicleDB && vehicleDB.length > 0) {
        let vehicleModels = [];
        vehicleDB[0].brand_models.map((model) => {
          if (model.category == 'BOTH') {
            let vehicleVariants = [];
            model.car_variants.map((variant) => {
              vehicleVariants.push(
                new VehicleModelsVariantModel(variant.variant_id, variant.variant,
                  variant.fuel_type_id, variant.fuel_type, variant.color_id, variant.color))
            })
            vehicleModels.push(new VehicleModelsModel(model.model_id, model.model_name, model.category, vehicleVariants))
          }
        })
        vehicle = new VehicleModel(vehicleDB[0].brand_id, vehicleDB[0].brand_name, vehicleModels);
      }
    }

    return vehicle;

  },
  getVehicleVariants: (modelId) => {
    let vehicleVariants = [];
    let userData = UserInfoService.findFirst();
    if (userData) {
      let vehicleDB = realm.objects('VehicleDB').filtered('brand_id==' + userData.brand_id);
      if (vehicleDB && vehicleDB.length > 0) {
        vehicleDB[0].brand_models.map((model) => {
          if (model.model_id == modelId) {
            model.car_variants.map((variant) => {
              vehicleVariants.push(
                new VehicleModelsVariantModel(variant.variant_id, variant.variant,
                  variant.fuel_type_id, variant.fuel_type, variant.color_id, variant.color))
            })
          }
        })
      }
    }

    return vehicleVariants;

  },
  getAllVehicle: () => {
    let vehicles = [];
    let vehicleDB = realm.objects('VehicleDB');
    if (vehicleDB && vehicleDB.length > 0) {

      vehicleDB.map((brand) => {
        let vehicleModels = [];

        brand.brand_models.map((model) => {
          if (model.category == 'BOTH' || model.category == 'OLD_CAR') {
            let vehicleVariants = [];
            model.car_variants.map((variant) => {
              vehicleVariants.push(
                new VehicleModelsVariantModel(variant.variant_id, variant.variant,
                  variant.fuel_type_id, variant.fuel_type, variant.color_id, variant.color))
            })
            vehicleModels.push(new VehicleModelsModel(model.model_id, model.model_name, model.category, vehicleVariants))
          }
        })

        vehicles.push(new VehicleModel(brand.brand_id, brand.brand_name, vehicleModels));
      })
    }
    return vehicles;

  },
  deleteAll: function () {
    realm.write(() => {
      realm.delete(realm.objects('VehicleModelsVariantsDB'));
      realm.delete(realm.objects('VehicleModelsDB'));
      realm.delete(realm.objects('VehicleDB'));
    })
  },
}

export default VehicleDBService;
