import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  ActivityIndicator,
  Alert,
  TouchableOpacity
} from "react-native";
import {
  createMaterialTopTabNavigator,
  createAppContainer
} from "react-navigation";
import LeadSourceDistribution from "./LeadSourceDistribution";
import CarModelDistribution from "./CarModelDistribution";
import RestClient from "../../../network/rest_client";
import styles from "./LeadSourceCarModelDistribution.styles";

export default class LeadSourceCarModelDistribution extends Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: false, data: null };
  }
  componentDidMount() {
    this.getLeadSourceAndModelInfo();
  }

  openLeads() {}
  getLeadSourceAndModelInfo() {
    let { navigation } = this.props;
    let start_date = navigation.getParam("start_date");
    let end_date = navigation.getParam("end_date");
    let location_id = navigation.getParam("location_id");
    let user_id = navigation.getParam("user_id");
    let role_id = navigation.getParam("role_id");

    this.setState({ isLoading: true });
    new RestClient()
      .getETVBRLeadSourceModelWise({
        start_date,
        end_date,
        location_id,
        user_id,
        role_id,
        filters: encodeURIComponent(
          JSON.stringify(global.global_etvbr_filters_selected)
        )
      })
      .then(responseJson => {
        console.log("called -----" + JSON.stringify(responseJson));
        if (responseJson.statusCode == 2002) {
          this.setState({
            isLoading: false,
            data: responseJson.result
          });
        } else if (responseJson.statusCode == 4000) {
          this.setState({
            isLoading: false
          });
          return Alert.alert(responseJson.message);
        }
      })
      .catch(error => {
        this.setState({
          isLoading: false
        });
        return Alert.alert("Someting went wrong");
      });
  }
  renderTabs(navigation) {
    let isFromLiveEF = navigation.getParam("isFromLiveEF", false);
    var tabs = {
      "Lead Source": {
        screen: props => (
          <LeadSourceDistribution
            isFromLiveEF={isFromLiveEF}
            total={
              this.state.data
                ? {
                    abs: this.state.data.total_abs,
                    rel: this.state.data.total_rel
                  }
                : null
            }
            leadSourceWise={this.state.data ? this.state.data.source_wise : []}
            onClick={this.openLeads.bind(this)}
          />
        )
      },
      "Vehicle Model": {
        screen: props => (
          <CarModelDistribution
            isFromLiveEF={isFromLiveEF}
            total={
              this.state.data
                ? {
                    abs: this.state.data.total_abs,
                    rel: this.state.data.total_rel
                  }
                : null
            }
            modelWise={this.state.data ? this.state.data.model_wise : []}
            onClick={this.openLeads.bind(this)}
          />
        )
      }
    };
    let TAB_VIEW = createAppContainer(
      createMaterialTopTabNavigator(tabs, {
        swipeEnabled: true,
        animationEnabled: true,
        backBehavior: "none",
        tabBarOptions: {
          style: {
            backgroundColor: "#fff",
            elevation: 0,
            shadowColor: "transparent",
            justifyContent: "center"
          },
          tabStyle: {
            height: 50
          },
          labelStyle: {
            fontSize: 15,
            textAlign: "center"
          },
          inactiveTintColor: "#808080",
          activeTintColor: "#2e5e86",
          upperCaseLabel: false,

          indicatorStyle: {
            backgroundColor: "#2e5e86",
            height: 1
          }
        }
      })
    );
    return <TAB_VIEW />;
  }
  render() {
    let { navigation } = this.props;
    let imageSource = { uri: navigation.getParam("dp_url") };
    let name = navigation.getParam("name", "");
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={{ flexDirection: "row", alignItems: "center", flex: 1 }}>
            <View
              style={{
                height: 38,
                width: 38,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <TouchableOpacity onLongPress={() => {}}>
                <View
                  style={{ alignItems: "center", justifyContent: "center" }}
                >
                  <View style={styles.imagePlaceHolder}>
                    <Text style={{ color: "#2F5C86" }}>
                      {name.length > 1 ? name.charAt(0).toUpperCase() : ""}
                    </Text>
                  </View>
                  <Image source={imageSource} style={styles.userImage} />
                </View>
              </TouchableOpacity>
            </View>
            <Text style={styles.headerText}>{name}</Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
          >
            <Image
              style={[styles.closeButton]}
              source={require("../../../images/ic_close_white.png")}
            />
          </TouchableOpacity>
        </View>
        {this.state.isLoading && (
          <View
            style={[
              styles.container,
              { display: this.state.isLoading ? "flex" : "none" }
            ]}
          >
            <ActivityIndicator
              animating={this.state.isLoading}
              style={{ flex: 1, alignSelf: "center" }}
              color="#2e5e86"
              size="small"
              hidesWhenStopped={true}
            />
          </View>
        )}
        {!this.state.isLoading && this.renderTabs(navigation)}
      </View>
    );
  }
}
