package com.salescrm.telephony.db.etvbr_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by prateek on 8/11/17.
 */

public class TeamLeader extends RealmObject {

    @SerializedName("info")
    @Expose
    private Info info;
    @SerializedName("sales_consultants")
    @Expose
    private RealmList<SalesConsultant> sales_consultants = null;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public RealmList<SalesConsultant> getSalesConsultants() {
        return sales_consultants;
    }

    public void setSalesConsutants(RealmList<SalesConsultant> salesConsultants) {
        this.sales_consultants = salesConsultants;
    }
}
