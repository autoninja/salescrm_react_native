package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by Ravindra P on 08-09-2016.
 */
public class FilterActivityAndStagesResponse
{
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private List<LeadStages> leadStages;

        private List<Activities> activities;

        private List<CustomerTypes> customerTypes;

        public List<LeadStages> getLeadStages ()
        {
            return leadStages;
        }

        public void setLeadStages (List<LeadStages> leadStages)
        {
            this.leadStages = leadStages;
        }

        public List<Activities> getActivities ()
        {
            return activities;
        }

        public void setActivities (List<Activities> activities)
        {
            this.activities = activities;
        }

        public List<CustomerTypes> getCustomerTypes() {
            return customerTypes;
        }

        public void setCustomerTypes(List<CustomerTypes> customerTypes) {
            this.customerTypes = customerTypes;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [leadStages = "+leadStages+", activities = "+activities+"]";
        }

        public class LeadStages
        {
            private String stage;

            private String stage_id;

            public String getStage ()
            {
                return stage;
            }

            public void setStage (String stage)
            {
                this.stage = stage;
            }

            public String getStage_id ()
            {
                return stage_id;
            }

            public void setStage_id (String stage_id)
            {
                this.stage_id = stage_id;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [stage = "+stage+", stage_id = "+stage_id+"]";
            }
        }

        public class Activities
        {
            private String id;

            private String name;

            public String getId ()
            {
                return id;
            }

            public void setId (String id)
            {
                this.id = id;
            }

            public String getName ()
            {
                return name;
            }

            public void setName (String name)
            {
                this.name = name;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [id = "+id+", name = "+name+"]";
            }
        }

        public class CustomerTypes {
            private String id;

            private String type;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }
    }

}
