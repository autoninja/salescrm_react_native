package com.salescrm.telephony.db.create_lead;


import com.salescrm.telephony.utils.Util;

import java.util.Date;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 15/9/16.
 */
public class CreateLeadInputDataDB extends RealmObject{

    @PrimaryKey
    private int scheduledActivityId;
    private boolean is_synced=false;
    private Date dataCreatedDate= Util.getDateTime(0);

    public boolean is_synced() {
        return is_synced;
    }

    public void setIs_synced(boolean is_synced) {
        this.is_synced = is_synced;
    }

    public int getScheduledActivityId() {
        return scheduledActivityId;
    }

    public void setScheduledActivityId(int scheduledActivityId) {
        this.scheduledActivityId = scheduledActivityId;
    }

    public Date getDataCreatedDate() {
        return dataCreatedDate;
    }

    public void setDataCreatedDate(Date dataCreatedDate) {
        this.dataCreatedDate = dataCreatedDate;
    }

    private Activity_data activity_data;

    private Lead_data lead_data;

    private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Activity_data getActivity_data() {
        return activity_data;
    }

    public void setActivity_data(Activity_data activity_data) {
        this.activity_data = activity_data;
    }

    public Lead_data getLead_data() {
        return lead_data;
    }

    public void setLead_data(Lead_data lead_data) {
        this.lead_data = lead_data;
    }

    @Override
    public String toString() {
        return "ClassPojo [activity_data = " + activity_data + ", lead_data = " + lead_data + "]";
    }


}
