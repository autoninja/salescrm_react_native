package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by bharath on 1/9/17.
 */

public class TeamShuffleModel {
    private static final TeamShuffleModel teamShuffleModel = new TeamShuffleModel();
    private Item userTeamShuffle;

    public static TeamShuffleModel getInstance() {
        return teamShuffleModel;
    }

    public Item getUserTeamShuffle() {
        return userTeamShuffle;
    }

    public void setUserTeamShuffle(Item userTeamShuffle) {
        this.userTeamShuffle = userTeamShuffle;
    }

    public class Item {
        private Integer parentId;
        private int type; //1-SM 2-TL 3-DSE
        private String title;
        private Integer id;
        private boolean isClicked;
        private List<Item> childItem;
        private String imgUrl;
        private Integer teamId;
        private Integer locationId;

        public Item() {
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public boolean isClicked() {
            return isClicked;
        }

        public void setClicked(boolean clicked) {
            isClicked = clicked;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public List<Item> getChildItem() {
            return childItem;
        }

        public void setChildItem(List<Item> childItem) {
            this.childItem = childItem;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public Integer getTeamId() {
            return teamId;
        }

        public void setTeamId(Integer teamId) {
            this.teamId = teamId;
        }

        public Integer getParentId() {
            return parentId;
        }

        public void setParentId(Integer parentId) {
            this.parentId = parentId;
        }

        public Integer getLocationId() {
            return locationId;
        }

        public void setLocationId(Integer locationId) {
            this.locationId = locationId;
        }
    }
}
