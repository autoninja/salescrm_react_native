package com.salescrm.telephony.views.booking;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.model.booking.BookingMainModel;
import com.salescrm.telephony.presenter.BookingSubmitPresenter;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

/**
 * Created by bharath on 14/3/18.
 */

public class CarBookingSubmitForm implements BookingSubmitPresenter.BookingSubmitView {
    private static final CarBookingSubmitForm ourInstance = new CarBookingSubmitForm();
    private BookingSubmitPresenter bookingSubmitPresenter;
    private TextView tvDateHeader;
    private TextView tvDateValue;
    private TextView tvRemarks;
    private TextView etRemarks;
    private Button btAction;
    private Calendar calendarMax =null;

    public static CarBookingSubmitForm getInstance() {
        return ourInstance;
    }

    private CarBookingSubmitForm() {
    }
    public void show(final Context context,
                     int stageId,
                     final CarBookingSubmitFormListener carBookingSubmitFormListener) {
        BookingMainModel bookingMainModel = new BookingMainModel();
        bookingMainModel.setStageId(stageId);
        show(context, bookingMainModel, carBookingSubmitFormListener);
    }
    public void show(final Context context,
                     BookingMainModel bookingMainModel,
                     final CarBookingSubmitFormListener carBookingSubmitFormListener) {
        bookingSubmitPresenter = new BookingSubmitPresenter(this, bookingMainModel);

        final Dialog dialog = new Dialog(context, R.style.AppTheme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if( dialog.getWindow()!=null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
        }
        dialog.setContentView(R.layout.booking_car_submit_form);
        tvDateHeader = (TextView) dialog.findViewById(R.id.tv_booking_submit_date_header);
        tvDateValue = (TextView) dialog.findViewById(R.id.tv_booking_submit_date_value);
        tvRemarks = (TextView) dialog.findViewById(R.id.tv_booking_submit_remarks_header);
        etRemarks = (TextView) dialog.findViewById(R.id.et_booking_submit_remarks_value);
        btAction = (Button) dialog.findViewById(R.id.bt_booking_submit_action);

        bookingSubmitPresenter.init();



        tvDateValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Util.showDatePicker(new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {
                        final Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        Util.showTimePicker(new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                calendar.set(Calendar.MINUTE, minute);
                                calendar.set(Calendar.SECOND, second);
                                bookingSubmitPresenter.updateDate(calendar);
                            }
                        }, (Activity) context, v.getId() );


                    }
                }, (Activity) context, v.getId(), null, Calendar.getInstance(),calendarMax);
            }
        });

        etRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                bookingSubmitPresenter.updateRemarks(s.toString().trim());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        dialog.findViewById(R.id.bt_booking_submit_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                carBookingSubmitFormListener.onCarBookingSubmitForm(bookingSubmitPresenter.getBookingMainModel());
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }

    @Override
    public void init(BookingMainModel bookingMainModel) {
        switch (bookingMainModel.getStageId()) {
            case WSConstants.CarBookingStages.BOOK_CAR:
                tvDateHeader.setText("Post Booking Follow Up Date");
                calendarMax = Calendar.getInstance();
                calendarMax.add(Calendar.DAY_OF_MONTH, 10);
                tvRemarks.setText("Scheme offered");
                etRemarks.setHint("Add scheme offered");
                break;
            case WSConstants.CarBookingStages.CAR_BOOKED_HALF_PAYMENT:
                tvDateHeader.setText("Expected Delivery Date");
                calendarMax = null;
                break;
            case WSConstants.CarBookingStages.CAR_BOOKED_FULL_PAYMENT_NO:
                tvDateHeader.setText(" Post Booking Follow Up Date");
                calendarMax = Calendar.getInstance();
                calendarMax.add(Calendar.DAY_OF_MONTH, 10);
                break;
            case WSConstants.CarBookingStages.CUSTOM_INVOICED_DELIVERY_PENDING_NO:
                tvDateHeader.setText("Expected Delivery Date");
                calendarMax = Calendar.getInstance();
                calendarMax.add(Calendar.DAY_OF_MONTH, 10);
                break;

        }
        if(bookingMainModel.getRemarks()!=null) {
            etRemarks.setText(bookingMainModel.getRemarks());
        }
    }

    @Override
    public void isEnableSubmitBookingDetails(boolean isAllComplete) {
        btAction.setEnabled(isAllComplete);
        btAction.setTextColor(isAllComplete?Color.WHITE : Color.parseColor("#64ffffff"));

    }

    @Override
    public void updateDate(String date) {
        tvDateValue.setText(date);
    }

    public interface CarBookingSubmitFormListener{
        void onCarBookingSubmitForm(BookingMainModel bookingMainModel);
    }
}
