import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    container: { flex: 1 },
    head: { height: 40, borderBottomWidth: 1, borderBottomColor: '#7c7c7c', paddingLeft: 5},
    text: { margin: 6, color: '#2e5e86' },
    row: { flexDirection: 'row', height : 50, borderBottomWidth: 1, borderBottomColor: '#ececec'},
    button: {width: '50%', paddingTop: 5, height: 50, borderRadius: 0},
    bottom: {
        flex: 1,
        justifyContent: 'flex-end'
    }
});
