import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image } from 'react-native';
import TodayTasksHeader from '../../../components/todaysTasks/todays_tasks_header'
import TodaysTaskItemExpandable from '../../../components/todaysTasks/todays_task_item_expandable'
import TodaysTaskItemChild from '../../../components/todaysTasks/todays_task_item_child'
import TodayTaskItemResult from '../../../components/todaysTasks/todays_task_item_result'
import TodaysTasksFooter from '../../../components/todaysTasks/todays_tasks_footer'

export default class TodaysTasksChild extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isRefreshing: false };
  }

  refresh() {
    this.setState({ isRefreshing: true });
    this.props.refresh();
  }
  render() {
    //console.error(JSON.stringify(this.props.data.team_leaders));
    let mainView = null;
    if (this.props.data) {
      let dataArray = this.props.data.team_leaders;
      let locationId = this.props.data.location_id;
      let locationData = this.props.data.location_abs;
      if (this.props.expandable) {

        mainView = <FlatList
          showsVerticalScrollIndicator={false}
          data={dataArray}
          renderItem={({ item, index }) => {

            let renderView = <TodaysTaskItemExpandable
              openTasksList={this.props.openTasksList.bind(this)}
              onLongPress={this.props.onLongPress.bind(this)}
              team_leader={item}
              info={
                {
                  id: item.info.id,
                  dp_url: item.info.dp_url,
                  name: item.info.name,
                  location_id: locationId,
                  user_role_id: 6
                }
              }
              abs={
                {
                  calls: item.info.abs.calls,
                  tdrives: item.info.abs.tdrives,
                  visits: item.info.abs.visits,
                  post_bookings: item.info.abs.post_bookings,
                  deliveries: item.info.abs.deliveries,
                  pending_total_count: item.info.abs.pending_total_count,
                  uncalled_tasks: item.info.abs.uncalled_tasks,
                }
              }
            />
            if (index == dataArray.length - 1) {
              //Return with total & bottomView
              return (
                <View>
                  {renderView}
                  <TodayTaskItemResult
                    abs={
                      {
                        calls: locationData.calls,
                        tdrives: locationData.tdrives,
                        visits: locationData.visits,
                        post_bookings: locationData.post_bookings,
                        deliveries: locationData.deliveries,
                        pending_total_count: locationData.pending_total_count,
                        uncalled_tasks: locationData.uncalled_tasks,
                      }
                    }
                  />
                  <TodaysTasksFooter />
                </View>);
            }
            else {
              return (renderView);
            }

          }
          }
          keyExtractor={(item, index) => index.toString()}
          onRefresh={() => this.refresh()}
          refreshing={this.state.isRefreshing}
        />
      }
      else {
        mainView = <FlatList
          showsVerticalScrollIndicator={false}
          data={dataArray[0].sales_consultants}
          renderItem={({ item, index }) => {

            let renderView = <TodaysTaskItemChild
              openTasksList={this.props.openTasksList.bind(this)}
              onLongPress={this.props.onLongPress.bind(this)}
              info={
                {
                  id: item.info.id,
                  dp_url: item.info.dp_url,
                  name: item.info.name,
                  location_id: locationId,
                  user_role_id: 4
                }
              }
              abs={
                {
                  calls: item.info.abs.calls,
                  tdrives: item.info.abs.tdrives,
                  visits: item.info.abs.visits,
                  post_bookings: item.info.abs.post_bookings,
                  deliveries: item.info.abs.deliveries,
                  pending_total_count: item.info.abs.pending_total_count,
                  uncalled_tasks: item.info.abs.uncalled_tasks,
                }
              }
            />
            if (dataArray[0].sales_consultants.length == 1) {
              //Just salesConsultant
              //Return with bottom view
              return (<View>{renderView}<TodaysTasksFooter /></View>);
            }
            else if (index == dataArray[0].sales_consultants.length - 1) {
              //Return with total & bottomView
              return (
                <View>
                  {renderView}
                  <TodayTaskItemResult
                    abs={
                      {
                        calls: dataArray[0].info.abs.calls,
                        tdrives: dataArray[0].info.abs.tdrives,
                        visits: dataArray[0].info.abs.visits,
                        post_bookings: dataArray[0].info.abs.post_bookings,
                        deliveries: dataArray[0].info.abs.deliveries,
                        pending_total_count: dataArray[0].info.abs.pending_total_count,
                        uncalled_tasks: dataArray[0].info.abs.uncalled_tasks,
                      }
                    }
                  />
                  <TodaysTasksFooter />
                </View>);
            }
            else {
              return (renderView);
            }

          }
          }
          keyExtractor={(item, index) => index.toString()}
          onRefresh={() => this.refresh()}
          refreshing={this.state.isRefreshing}
        />

      }
    }

    return (

      <View style={{ padding: 4, flex: 1 }}>
        <TodayTasksHeader />
        <View style={{ flex: 1, marginTop: 6 }}>
          {mainView}
        </View>
      </View>
    );
  }
}
