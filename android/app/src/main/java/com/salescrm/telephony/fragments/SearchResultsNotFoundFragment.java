package com.salescrm.telephony.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddSearchActivity;
import com.salescrm.telephony.activity.RNColdVisitActivity;
import com.salescrm.telephony.db.AppConfigDB;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.model.SearchInputData;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;

/**
 * Created by bharath on 30/5/16.
 */
public class SearchResultsNotFoundFragment extends Fragment {

    TextView btAddSearch, btAddColdVisit;
    OnStartAddLeadActivity onStartAddLeadActivity;
    SearchResultsFragment searchResultsFragment;
    private String mobileNumber;
    private boolean addLeadEnabled = true;

    public static SearchResultsNotFoundFragment newInstance(SearchInputData data) {
        SearchResultsNotFoundFragment fragment = new SearchResultsNotFoundFragment();
        Bundle args = new Bundle();
        if(data!=null) {
            args.putString(WSConstants.SRCMRealDbFields.MOBILE, data.getMobile());
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            onStartAddLeadActivity = (OnStartAddLeadActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {

    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_search_result_not_found, container, false);
        getActivity().setTitle(getString(R.string.results));
        btAddSearch = rootView.findViewById(R.id.bt_add_lead);
        btAddColdVisit = rootView.findViewById(R.id.bt_add_cold_visit);
        mobileNumber = getArguments().getString(WSConstants.SRCMRealDbFields.MOBILE, "-1");
        System.out.println("mobile number:"+mobileNumber);
        addLeadEnabled = DbUtils.isAddLeadEnabled() ;
        if(mobileNumber==null|| mobileNumber.isEmpty()|| mobileNumber.equals("-1") || !addLeadEnabled){
            rootView.findViewById(R.id.ll_search_result_buttons).setVisibility(View.GONE);
        }
        btAddSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onStartAddLeadActivity!=null){
                    onStartAddLeadActivity.startAddLeadActivity(true);

                }
            }
        });
        if(DbUtils.isAppConfEnabled(WSConstants.AppConfig.COLD_VISIT_ENABLED)){
            rootView.findViewById(R.id.frame_add_cold_visit).setVisibility(View.VISIBLE);

            btAddColdVisit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onStartAddLeadActivity!=null){
                        onStartAddLeadActivity.startAddColdVisitActivity(true);

                    }
                }
            });

        }
        else {
            rootView.findViewById(R.id.frame_add_cold_visit).setVisibility(View.GONE);
        }
        ImageButton imageViewBackToSearch = (ImageButton) rootView.findViewById(R.id.imageViewBackToSearch);
        imageViewBackToSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return rootView;
    }

    public interface OnStartAddLeadActivity {
        void startAddLeadActivity(boolean val);
        void startAddColdVisitActivity(boolean val);
    }

}
