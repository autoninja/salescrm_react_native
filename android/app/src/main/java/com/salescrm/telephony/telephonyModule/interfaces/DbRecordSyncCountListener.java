package com.salescrm.telephony.telephonyModule.interfaces;

/**
 * Created by bharath on 5/3/18.
 */

public interface DbRecordSyncCountListener {
    void dbRecordSyncCountQueryResult(int count);
}
