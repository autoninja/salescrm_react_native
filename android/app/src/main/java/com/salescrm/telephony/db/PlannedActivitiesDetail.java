package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by Ravindra P on 29-08-2016.
 */
public class PlannedActivitiesDetail extends RealmObject {

    public static final String PLANNEDACTIVITYDETAILS = "PlannedActivitiesDetail";


    private String plannedActivitiesDetailKeys;
    private String plannedActivitiesDetailValues;
    private String plannedActivitiesDetailIconTypes;

    public String getPlannedActivitiesDetailKeys() {
        return plannedActivitiesDetailKeys;
    }

    public void setPlannedActivitiesDetailKeys(String plannedActivitiesDetailKeys) {
        this.plannedActivitiesDetailKeys = plannedActivitiesDetailKeys;
    }

    public String getPlannedActivitiesDetailValues() {
        return plannedActivitiesDetailValues;
    }

    public void setPlannedActivitiesDetailValues(String plannedActivitiesDetailValues) {
        this.plannedActivitiesDetailValues = plannedActivitiesDetailValues;
    }

    public String getPlannedActivitiesDetailIconTypes() {
        return plannedActivitiesDetailIconTypes;
    }

    public void setPlannedActivitiesDetailIconTypes(String plannedActivitiesDetailIconTypes) {
        this.plannedActivitiesDetailIconTypes = plannedActivitiesDetailIconTypes;
    }
}
