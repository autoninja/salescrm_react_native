package com.salescrm.telephony.offline;

import android.content.Context;

import com.google.gson.Gson;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.EvaluationManagerResult;
import com.salescrm.telephony.db.EvaluationManagerResultData;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.model.EvaluationManagerModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.concurrent.Callable;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FetchEvaluationManager implements Callback<EvaluationManagerModel> {

    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;

    public FetchEvaluationManager(OfflineSupportListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }

    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getEvaluationManagerExist(this);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.EVALUATION_MANAGER);

        }
    }

    @Override
    public void success(EvaluationManagerModel data, Response response) {
        Util.updateHeaders(response.getHeaders());

        if (data.getResult() != null) {
            System.out.println("Success:1" + data.getMessage());
            System.out.println("BookingAllocation: " + new Gson().toJson(data.getResult()));
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    insertData(realm, data);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    listener.onFetchEvaluationManager(data);
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.EVALUATION_MANAGER);
                }
            });


        } else {
            // relLoader.setVisibility(View.GONE);
            System.out.println("Success:2" + data.getMessage());
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.EVALUATION_MANAGER);
            //showAlert(validateappConfResponse.getMessage());
        }
    }

    private void insertData(Realm realm, EvaluationManagerModel data) {
        realm.delete(EvaluationManagerResult.class);
        realm.delete(EvaluationManagerResultData.class);
        EvaluationManagerResult evaluationManagerResult = new EvaluationManagerResult();
        for(int i = 0; i<data.getResult().size(); i++){
            EvaluationManagerResultData evaluationManagerResultData = new EvaluationManagerResultData();
            evaluationManagerResultData.setLocationId(data.getResult().get(i).getLocation_id());
            evaluationManagerResultData.setHasEvaluationManager(data.getResult().get(i).isHas_evaluation_managers());
            evaluationManagerResult.getEvaluationManagerResultData().add(evaluationManagerResultData);
        }
        realm.copyToRealm(evaluationManagerResult);
    }

    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.EVALUATION_MANAGER);
    }
}
