import { StyleSheet, Platform } from 'react-native';

export default styles = StyleSheet.create( {
    container: {
        justifyContent: 'center',
        flex:1,
        backgroundColor: '#fff',
        marginTop: (Platform.OS) === 'ios' ? 20 : 0,
      },
    header: {
      height:64,
      backgroundColor:'#2F5C86',
      flexDirection:'row',
      alignSelf: 'stretch',
      alignItems:'center',
      padding:10,

    },
    closeButton: {
      width: 20,
      height: 20,
      marginRight:10,
    },
    imagePlaceHolder: {
      position:'absolute',
      width:38, height:38,
      borderRadius: 38/2,
      backgroundColor:'#fff',
      alignItems:'center',
      justifyContent:'center'
    },
    userImage: {
      position:'absolute',
      height: 36,
      width:36,
      borderRadius: 36/2
    },

    headerText: {
      fontSize:18,
      fontWeight:'bold',
      color:'#fff',
      marginLeft:10,
      marginRight:10
    }

      
});