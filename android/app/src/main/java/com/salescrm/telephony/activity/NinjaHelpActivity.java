package com.salescrm.telephony.activity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.AllotDseDB;
import com.salescrm.telephony.db.FormAnswerDB;
import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.services.SyncDataService;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by bharath on 20/1/17.
 */

public class NinjaHelpActivity extends AppCompatActivity implements AutoDialogClickListener {
    private Toolbar toolbar;
    private CoordinatorLayout coordinatorLayout;
    private TextView tvSyncStatus,tvSyncStatusTask,tvSyncStatusLead,tvSyncStatusAllotDse;
    private Realm realm;
    private int createLeadTotal,formTotal,allotDseTotal;
    private RealmResults<CreateLeadInputDataDB> createLeadInputDataDBList;
    private RealmResults<FormAnswerDB> formAnswerList;
    private RealmResults<AllotDseDB> allotDseList;
    private ProgressBar progressSync;
    private Button btSyncNow,btRefreshOffline;
    private Preferences pref = null;
    private TextView tvLastUpdated;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ninja_help);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(NinjaHelpActivity.this);
        populateRealmResult();
        toolbar = (Toolbar) findViewById(R.id.app_toolbar);
        tvSyncStatus = (TextView)findViewById(R.id.tv_sync_status);
        tvSyncStatusTask = (TextView)findViewById(R.id.tv_sync_status_task);
        tvSyncStatusLead = (TextView)findViewById(R.id.tv_sync_status_lead);
        tvSyncStatusAllotDse = (TextView)findViewById(R.id.tv_sync_status_allot_dse);
        tvLastUpdated = (TextView)findViewById(R.id.tv_offline_status);
        progressSync = (ProgressBar)findViewById(R.id.sync_progress_bar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout_search);
        btSyncNow = (Button) findViewById(R.id.bt_sync_now);
        btRefreshOffline = (Button) findViewById(R.id.bt_update_now);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.help));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        createLeadTotal =  realm.where(CreateLeadInputDataDB.class).findAll().size();
        formTotal =  realm.where(FormAnswerDB.class).findAll().size();
        allotDseTotal =  realm.where(AllotDseDB.class).findAll().size();

        tvLastUpdated.setText(String.format("Last updated : %s", Util.parseDateAsAgo(pref.getBasicOfflineLastUpdated())));

        createLeadInputDataDBList.addChangeListener(new RealmChangeListener<RealmResults<CreateLeadInputDataDB>>() {
            @Override
            public void onChange(RealmResults<CreateLeadInputDataDB> element) {
                updateUi();
                redirectToOfflineFetch();
            }
        });
        formAnswerList.addChangeListener(new RealmChangeListener<RealmResults<FormAnswerDB>>() {
            @Override
            public void onChange(RealmResults<FormAnswerDB> element) {
                updateUi();
                redirectToOfflineFetch();
            }
        });
        allotDseList.addChangeListener(new RealmChangeListener<RealmResults<AllotDseDB>>() {
            @Override
            public void onChange(RealmResults<AllotDseDB> element) {
                updateUi();
                redirectToOfflineFetch();
            }
        });
        updateUi();
        btSyncNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(new Intent(NinjaHelpActivity.this, SyncDataService.class));
                }
                else {
                    startService(new Intent(NinjaHelpActivity.this, SyncDataService.class));
                }
                btSyncNow.setEnabled(false);
                btSyncNow.setVisibility(View.GONE);
            }
        });
        btRefreshOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()){
                    showDialog();
                }
                else {
                    Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });
        Bundle data = getIntent().getExtras();
        if(data!=null) {
            if (data.getBoolean("is_from_offline")) {
               forceSync();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void redirectToOfflineFetch(){
        if(getSyncedCount()==getTotal()){
            Bundle data = getIntent().getExtras();
            if(data!=null) {
                if (data.getBoolean("is_from_offline")) {
                    Intent i=new Intent(NinjaHelpActivity.this,SplashActivity.class);
                    i.putExtra(WSConstants.REFRESH_FROM_HELP,true);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
            }
        }

    }

    private void showDialog() {
        new AutoDialog(this,new AutoDialogModel("This operation will refresh Car,Dse and other fundamental element without effecting Tasks.","Refresh","Cancel")).show();
    }

    private void populateRealmResult(){
        createLeadInputDataDBList = realm.where(CreateLeadInputDataDB.class).equalTo("is_synced", true).findAll();
        formAnswerList = realm.where(FormAnswerDB.class).equalTo("is_synced", true).findAll();
        allotDseList = realm.where(AllotDseDB.class).equalTo("is_synced", true).findAll();

    }
    private int getSyncedCount(){
        return (createLeadInputDataDBList.size()+
                formAnswerList.size()+
                allotDseList.size());

    }
    private int getTotal(){
        return (createLeadTotal+formTotal+allotDseTotal);
    }
    private void updateUi(){
        tvSyncStatus.setText("Sync Status ("+getSyncedCount()+"/"+getTotal()+")");
        tvSyncStatusTask.setText("Task ("+formAnswerList.size()+"/"+formTotal+")");
        tvSyncStatusLead.setText("Lead ("+createLeadInputDataDBList.size()+"/"+createLeadTotal+")");
        tvSyncStatusAllotDse.setText("Allot Dse ("+allotDseList.size()+"/"+allotDseTotal+")");
        progressSync.setVisibility(View.VISIBLE);
        progressSync.setMax(getTotal() * 100);
        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(progressSync, "progress", progressSync.getProgress(), getSyncedCount() * 100);
        progressAnimator.setDuration(800);
        progressAnimator.setInterpolator(new DecelerateInterpolator());
        progressAnimator.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Util.isServiceRunning(SyncDataService.class,getApplicationContext())){
            btSyncNow.setEnabled(false);
            btSyncNow.setVisibility(View.GONE);
        }
        else {
            btSyncNow.setEnabled(true);
            btSyncNow.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
        refresh();
    }

    private void refresh() {
        pref.setRefreshOfflineData(true);
        Intent i=new Intent(NinjaHelpActivity.this,SplashActivity.class);
        i.putExtra(WSConstants.REFRESH_FROM_HELP,true);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        NinjaHelpActivity.this.finish();
    }

    @Override
    public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {

    }

    @Override
    public void onBackPressed() {
        Bundle data = getIntent().getExtras();
        if(data!=null){
            if(data.getBoolean("is_from_offline")){
                Intent i=new Intent(NinjaHelpActivity.this,SplashActivity.class);
                i.putExtra(WSConstants.REFRESH_FROM_HELP,true);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
            else {
                super.onBackPressed();
            }
        }
        else {
            super.onBackPressed();
        }

    }
    private void forceSync() {
        if(!Util.isServiceRunning(SyncDataService.class,getApplicationContext())){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(new Intent(NinjaHelpActivity.this,SyncDataService.class));
            }
            else {
                startService(new Intent(NinjaHelpActivity.this,SyncDataService.class));
            }

        }
        Intent intent = new Intent(WSConstants.NINJA_BROADCAST_MESSAGE_FORCE_SYNC);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

    }
}
