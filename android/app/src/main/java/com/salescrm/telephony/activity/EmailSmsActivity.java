package com.salescrm.telephony.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.EmailSmsPagerAdapter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.SendMailObjects;
import com.salescrm.telephony.db.SendSMSObjects;
import com.salescrm.telephony.fragments.EmailsFragment;
import com.salescrm.telephony.fragments.SmsFragment;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.GetAllSmsEmailResponse;
import com.salescrm.telephony.response.SendEmailResponse;
import com.salescrm.telephony.response.SendsmsResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.WSConstants;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class EmailSmsActivity extends AppCompatActivity implements Callback, SmsFragment.OnDataPassSMS, EmailsFragment.OnDataPassEmail {
    final Context context = this;
    Button sendBtn;
    ViewPager viewPager;
    TabLayout tabLayout;
    GetAllSmsEmailResponse AllSmsEmailResponse;
    String phone, smsResult, typeID;
    String address, mailType, body, subject, leadId, customer;
    private SendMailObjects sendMailObjects;
    private SendSMSObjects sendSMSObjects;
    private Realm realm;
    private RealmResults<SendMailObjects> realmResultsSendMailObjectses;
    private RealmResults<SendSMSObjects> realmResultsSendSMSObjectses;
    private EmailSmsPagerAdapter adapter;
    private ConnectionDetectorService connectionDetectorService;
    private int emailSyncSize, smsSyncSize;
    private String fileAttachment;
    private View parentLayout;
    private Snackbar snackbar;
    private View viewSnacbar;
    private TextView tvSnacbar;
    private ImageView ibBack;
    private ProgressBar emailSmsProgressbar;
    private TextView tvFormTitle;
    private String pdfLink = "";
    private boolean fromProforma;
    private boolean isClientILom = true;

    @Override
    public void success(Object o, Response response) {
    }

    @Override
    public void failure(RetrofitError error) {

    }

    private Preferences pref;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void disableScreenCapture() {
        if(pref.isScreenshotEnabled()) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Preferences.getInstance();
        pref.load(getApplicationContext());
        disableScreenCapture();
        setContentView(R.layout.email_sms);
        parentLayout = findViewById(R.id.pager);
        viewPager = (ViewPager) findViewById(R.id.pager);
        emailSmsProgressbar = (ProgressBar) findViewById(R.id.email_sms_progressbar);
        ibBack = (ImageView) findViewById(R.id.ib_form_back_activity);
        realm = Realm.getDefaultInstance();
        realm = Realm.getDefaultInstance();
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tvFormTitle = (TextView)findViewById(R.id.tv_form_title);
        isClientILom = pref.isClientILom();
        if(isClientILom) {
            tabLayout.addTab(tabLayout.newTab().setText("SMS"));
            tvFormTitle.setText("SMS");
            tabLayout.setVisibility(View.INVISIBLE);
            pdfLink = "";
            fromProforma =false;
        }
        else {
            if((getIntent().getBooleanExtra("from_proforma", false))) {
                tabLayout.addTab(tabLayout.newTab().setText("Email"));
                tvFormTitle.setText("Email");
                tabLayout.setVisibility(View.GONE);
                pdfLink = pref.getProformaPdfLink();
                fromProforma =true;
            }else{
                tabLayout.addTab(tabLayout.newTab().setText("Email"));
                tabLayout.addTab(tabLayout.newTab().setText("Sms"));
                tvFormTitle.setText("Email/Sms");
                pref.setProformaPdfLink("");
                pdfLink = "";
                tabLayout.setVisibility(View.VISIBLE);
                fromProforma = false;
            }
        }

        connectionDetectorService = new ConnectionDetectorService(EmailSmsActivity.this);
        // if internet connection available
        if (connectionDetectorService.isConnectingToInternet()) {
            realmResultsSendMailObjectses = realm.where(SendMailObjects.class).equalTo("mailSync", 1).findAll();
            if (realmResultsSendMailObjectses != null) {
                emailSyncSize = realmResultsSendMailObjectses.size() - 1;
                //sendPendingDbMail();
            }
            realmResultsSendSMSObjectses = realm.where(SendSMSObjects.class).equalTo("smsSync", 1).findAll();
            if (realmResultsSendSMSObjectses != null) {
                smsSyncSize = realmResultsSendSMSObjectses.size() - 1;
                //sendPendingDbSMS();
            }
        } else {

        }
        if (connectionDetectorService.isConnectingToInternet()) {
            callAllSmsEmail();
        } else {
            populateViewpager();
        }
        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //populateViewpager();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    setTypeID("2");
                } else if (position == 1) {
                    setTypeID("1");
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        sendBtn = (Button) findViewById(R.id.send_btn);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EmailSmsActivity.this);
                alertDialogBuilder.setMessage("Are you sure you want to send?");
                alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        callApi();
                        if(fromProforma) {
                            HashMap<String, Object> hashMap = new HashMap<String, Object>();
                            hashMap.put(CleverTapConstants.EVENT_PROFORMA_INVOICE_KEY_TYPE,
                                    CleverTapConstants.EVENT_PROFORMA_INVOICE_TYPE_SENT);
                            hashMap.put(CleverTapConstants.EVENT_PROFORMA_INVOICE_KEY_SENT_TYPE,
                                    CleverTapConstants.EVENT_PROFORMA_INVOICE_SENT_TYPE_EMAIL);
                            CleverTapPush.pushEvent(CleverTapConstants.EVENT_PROFORMA_INVOICE, hashMap);

                        }

                    }
                });
                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finish();

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

    }

    private void sendPendingDbSMS() {
        if (smsSyncSize >= 0) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).Sendsms(realmResultsSendMailObjectses.get(smsSyncSize).getLeadId(), realmResultsSendSMSObjectses.get(smsSyncSize).getPhone(),
                    realmResultsSendSMSObjectses.get(smsSyncSize).getSmsResult(), new Callback<SendsmsResponse>() {
                        @Override
                        public void success(SendsmsResponse sendsmsResponse, Response response) {
                            realm.beginTransaction();
                            realmResultsSendSMSObjectses.get(smsSyncSize).setSmsSync(0);
                            realm.commitTransaction();
                            smsSyncSize--;
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                Log.e("sendsmsresponse", "sendsmsresponse -  " + header.getName() + " - " + header.getValue());
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }

                            if(sendsmsResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                                ApiUtil.InvalidUserLogout(EmailSmsActivity.this,0);
                            }

                            sendPendingDbSMS();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.e("sendsmsresponse", "sendsmsresponse - " + error.getMessage());


                        }
                    });
        } else {
            return;
        }
    }


    private void sendPendingDbMail() {
        if (emailSyncSize >= 0) {
            TypedFile typedFile = null;
           /* try {
                File myFile = new File(realmResultsSendMailObjectses.get(emailSyncSize).getAttachment());
                typedFile = new TypedFile("multipart/form-data", myFile);
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).SendEmail(realmResultsSendMailObjectses.get(emailSyncSize).getAddress(), realmResultsSendMailObjectses.get(emailSyncSize).getMailType(),
                    realmResultsSendMailObjectses.get(emailSyncSize).getBody(), realmResultsSendMailObjectses.get(emailSyncSize).getSubject(),
                    typedFile, realmResultsSendMailObjectses.get(emailSyncSize).getLeadId(),
                    realmResultsSendMailObjectses.get(emailSyncSize).getCustomer(), pdfLink, new Callback<SendEmailResponse>() {
                        @Override
                        public void success(SendEmailResponse sendEmailResponse, Response response) {
                            realm.beginTransaction();
                            realmResultsSendMailObjectses.get(emailSyncSize).setMailSync(0);
                            realm.commitTransaction();
                            emailSyncSize--;
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                Log.e("sendemailresponse", "sendemailresponse -  " + header.getName() + " - " + header.getValue());
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }
                            if(sendEmailResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                                ApiUtil.InvalidUserLogout(EmailSmsActivity.this,0);
                            }
                            sendPendingDbMail();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            emailSyncSize--;
                            sendPendingDbMail();
                        }
                    });
        } else {
            return;
        }
    }


    public void callApi() {

        System.out.println("SMSResultData phone:- " + phone + "  Content:- " + smsResult + "  typeId :-" + typeID);
        System.out.println("EmailFile:- " + fileAttachment);

        String type = typeID;
        System.out.println("TypeID :- " + type);
        if (type.equalsIgnoreCase(WSConstants.NOTIFICATION_TYPE_EMAIL+"")) {
            System.out.println("Mail Block:-  " + type);
            if (!address.equalsIgnoreCase("")) {
                if (connectionDetectorService.isConnectingToInternet()) {
                    callEmailAPI();
                    cleverTapPush(CleverTapConstants.EVENT_COMMUNICATION_EMAIL);
                } else {

                    realm.beginTransaction();
                    sendMailObjects = realm.createObject(SendMailObjects.class);
                    sendMailObjects.setMailSync(1);
                    sendMailObjects.setSubject(subject);
                    sendMailObjects.setAddress(address);
                    sendMailObjects.setBody(body);
                    sendMailObjects.setCustomer(customer);
                    sendMailObjects.setLeadId(leadId);
                    sendMailObjects.setAttachment(fileAttachment);
                    sendMailObjects.setMailType(mailType);
                    realm.commitTransaction();
                }
                callSnacbar("done");
            } else {
                callSnacbar("No email address");
            }
        } else if (type.equalsIgnoreCase(WSConstants.NOTIFICATION_TYPE_SMS+"")) {
            System.out.println("SMS Block:-  " + type);
            if (connectionDetectorService.isConnectingToInternet()) {
                callSmsAPI();
                cleverTapPush(CleverTapConstants.EVENT_COMMUNICATION_SMS);
            } else {
                realm.beginTransaction();
                sendSMSObjects = realm.createObject(SendSMSObjects.class);
                sendSMSObjects.setSmsSync(1);
                sendSMSObjects.setPhone(phone);
                sendSMSObjects.setSmsResult(smsResult);
                realm.commitTransaction();
            }
            callSnacbar("done");
        } else {
            //do nothing
        }
    }

    private void cleverTapPush(String communicationType) {

        if(getIntent()!=null){
            boolean isFromMyTask = getIntent().getBooleanExtra("from_my_task",false);
            CleverTapPush.pushCommunicationEvent(communicationType, isFromMyTask);
        }
    }

    private void callSnacbar(String message){
        snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_SHORT);
        viewSnacbar = snackbar.getView();
        tvSnacbar = (TextView) viewSnacbar.findViewById(android.support.design.R.id.snackbar_text);
        tvSnacbar.setGravity(Gravity.CENTER_HORIZONTAL);
        tvSnacbar.setTextColor(Color.WHITE);
        viewSnacbar.setBackgroundColor(Color.BLACK);
        snackbar.show();
    }

    private TypedFile makeFile(String uri){
        // this will make file which is required by Retrofit.
        File file = new File(Environment.getExternalStorageDirectory() + "/proformapdf/" + new File(uri).getName());
        TypedFile typedFile = new TypedFile("application/pdf", file);
        return typedFile;
    }

    public File getFile(String url) {
        String filename = String.valueOf(url.hashCode());
        // String filename = URLEncoder.encode(url);
        File f = new File(getCacheDir(), filename);
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    private void callEmailAPI() {

       // TypedFile typedFile = makeFile(pref.getProformaPdfLink());
        TypedFile typedFile = null;

        /*try {
            File myFile = new File(fileAttachment);
            typedFile = new TypedFile("multipart/form-data", myFile);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).SendEmail(address, mailType, body, subject, typedFile, leadId, customer, pdfLink, new Callback<SendEmailResponse>() {
            @Override
            public void success(SendEmailResponse sendEmailResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    Log.e("sendsmsresponse", "sendsmsresponse -  " + header.getName() + " - " + header.getValue());
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(sendEmailResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)){
                    if(getIntent().getBooleanExtra("from_proforma", false)) {
                        pref.setProformaSent(true);
                        ProformaPdfActivity.getInstance().finish();
                        EmailSmsActivity.this.finish();
                    }else {
                        EmailSmsActivity.this.finish();
                    }
                }
                if(sendEmailResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(EmailSmsActivity.this,0);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println("hulululu :- Failed" + error.getBody());
            }
        });
    }


    public void callSmsAPI() {
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).Sendsms(pref.getLeadID(), phone, smsResult, new Callback<SendsmsResponse>() {
            @Override
            public void success(SendsmsResponse sendsmsResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    Log.e("sendsmsresponse", "sendsmsresponse -  " + header.getName() + " - " + header.getValue());
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(sendsmsResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(EmailSmsActivity.this,0);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("sendsmsresponse", "sendsmsresponse - " + error.getMessage());


            }
        });
    }

    public void DoneHello(View view) {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_dialogfilter);
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();

        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public void callAllSmsEmail() {
        emailSmsProgressbar.setVisibility(View.VISIBLE);
        boolean PROFORMA = false;
        if(getIntent().getBooleanExtra("from_proforma", false)){
            PROFORMA = getIntent().getBooleanExtra("from_proforma", false);
        }
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetAllTemplates(PROFORMA, new Callback<GetAllSmsEmailResponse>() {
            @Override
            public void success(GetAllSmsEmailResponse getAllSmsEmailResponse, Response response) {
                emailSmsProgressbar.setVisibility(View.GONE);
                Log.e("Getallsmsemail", "Success - ");
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(getAllSmsEmailResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(EmailSmsActivity.this,0);
                }

                if (getAllSmsEmailResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    AllSmsEmailResponse = getAllSmsEmailResponse;
                    populateViewpager();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                emailSmsProgressbar.setVisibility(View.GONE);
                Log.e("Getallsmsemail", "getallsmsemail - " + error.getMessage());

            }
        });
    }


    private void populateViewpager() {
        adapter = new EmailSmsPagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount(), AllSmsEmailResponse, isClientILom);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    @Override
    public void onDataPassSMS(String phone, String smsResult, String typeID) {
        this.phone = phone;
        this.smsResult = smsResult;
        this.typeID = typeID;
    }

    @Override
    public void onDataPassEmail(String address, String mailType, String body, String subject, String attachment, String leadId, String customer, String typeID) {
        this.address = address;
        this.mailType = mailType;
        this.body = body;
        this.subject = subject;
        this.fileAttachment = attachment;
        this.leadId = leadId;
        this.customer = customer;
        this.typeID = typeID;
    }

    public void setTypeID(String typeID) {
        this.typeID = typeID;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(getIntent().getBooleanExtra("from_proforma", false) ||
                getIntent().getBooleanExtra("from_my_task", false)){
            this.finish();
        }
        else{
            Intent in = new Intent(EmailSmsActivity.this, C360Activity.class);
            startActivity(in);
            this.finish();
        }
    }

}