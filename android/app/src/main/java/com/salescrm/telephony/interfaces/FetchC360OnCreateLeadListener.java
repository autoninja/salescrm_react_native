package com.salescrm.telephony.interfaces;

/**
 * Created by bharath on 28/12/16.
 */
public interface FetchC360OnCreateLeadListener {
    void onFetchC360OnCreateLeadSuccess();
    void onFetchC360OnCreateLeadError();
}
