import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image} from 'react-native';
import { createStackNavigator } from 'react-navigation'
import { createMaterialTopTabNavigator } from 'react-navigation'
import { TabNavigator } from 'react-navigation'
import { StackActions, NavigationActions } from 'react-navigation';
import ConsultantDetails from './leaderboard_consultant_detail'

const img = "https://tineye.com/images/widgets/mona.jpg";

class Explorers extends React.Component {

	constructor(props)
		{
		    super(props);

		    this.state = {
		    	explorersArray: explorers
			}
		}

	render () {
	    return (
	    	/*<View style={{flex: 1, alignItems : 'center', backgroundColor:'#2e5e86',justifyContent: 'center'}}>
	    	<Text>Explorers</Text>
	    	</View>*/
	    	<FlatList
		    	ItemSeparatorComponent = {this.FlatListItemSeparator}
		    	data={this.state.explorersArray}
		    	renderItem= {({item, index}) =>(
		    		<TouchableOpacity onPress={ () => this.props.navigation.navigate('ConsultantDetails')}>
			    		<View style={{flex : 10, height: 50,flexDirection: 'row', alignItems : 'center', justifyContent: 'center', backgroundColor: '#09273b'}}>
				    		 <View style={{flex : 1, alignItems : 'center', justifyContent: 'center'}}>
				    		 	<Text style={{color: '#fff'}}> {item.rank}</Text>
				    		 </View>
				    		 <Image
		                          style={{width: 36, height: 36, borderRadius: 36/2, alignItems: 'center'}}
		                          source={{uri: img}}
		                      />
		                      <View style={{flex : 3, alignItems : 'flex-start', justifyContent: 'center', paddingLeft: 15}}>
				    		 	<Text style={{color: '#fff'}}> {item.name} </Text>
				    		 </View>

				    		 <View style={{flex : 4, alignItems : 'center', justifyContent: 'center'}}>
				    		 	 
				    		 </View>

				    		 <Image
							        style={{ width: 24, height: 24, marginRight: 15}}
		                          	source={require('../images/blue_arrow.png')}
							        />

			    		</View>
		    		</TouchableOpacity>
		    		)}
		    	keyExtractor= {(item, index) => index.toString() }
		    	/>
		    	);
	  }
	}

class Achievers extends React.Component {

	constructor(props)
		{
		    super(props);

		    this.state = {
		    	achieversArray: achievers
			}
		}
	render () {
		return (
			//this.props.navigation.navigate('leaderboard_consultant_detail'),
		    <FlatList
		    	ItemSeparatorComponent = {this.FlatListItemSeparator}
		    	data={this.state.achieversArray}
		    	renderItem= {({item, index}) =>(
		    		<View style={{flex : 10, height: 50,flexDirection: 'row', alignItems : 'center', justifyContent: 'center', backgroundColor: '#09273b'}}>
			    		 <View style={{flex : 1, alignItems : 'center', justifyContent: 'center'}}>
			    		 	<Text style={{color: '#fff'}}> {item.rank}</Text>
			    		 </View>
			    		 <Image
	                          style={{width: 36, height: 36, borderRadius: 36/2, alignItems: 'center'}}
	                          source={{uri: img}}
	                      />
	                      <View style={{flex : 3, alignItems : 'flex-start', justifyContent: 'center', paddingLeft: 15}}>
			    		 	<Text style={{color: '#fff'}}> {item.name} </Text>
			    		 </View>

			    		 <View style={{flex : 4, alignItems : 'center', justifyContent: 'center'}}>
			    		 	
			    		 </View>
			    		 <Image
						        style={{ width: 24, height: 24, marginRight: 15}}
	                          	source={require('../images/blue_arrow.png')}
						        />
		    		</View>
		    		)}
		    	keyExtractor= {(item, index) => index.toString() }
		    	/>
		    	);
	  }
}

class Dynamos extends React.Component {
	constructor(props)
		{
		    super(props);

		    this.state = {
		    	dynamosArray: dynamos
			}
		}
	render () {
		 return (
		    <FlatList
		    	ItemSeparatorComponent = {this.FlatListItemSeparator}
		    	data={this.state.dynamosArray}
		    	renderItem= {({item, index}) =>(
		    		<View style={{flex : 10, height: 50,flexDirection: 'row', alignItems : 'center', justifyContent: 'center', backgroundColor: '#09273b'}}>
			    		 <View style={{flex : 1, alignItems : 'center', justifyContent: 'center'}}>
			    		 	<Text style={{color: '#fff'}}> {item.rank}</Text>
			    		 </View>
			    		 <Image
	                          style={{width: 36, height: 36, borderRadius: 36/2, alignItems: 'center'}}
	                          source={{uri: img}}
	                      />
	                      <View style={{flex : 3, alignItems : 'flex-start', justifyContent: 'center', paddingLeft: 15}}>
			    		 	<Text style={{color: '#fff'}}> {item.name} </Text>
			    		 </View>

			    		 <View style={{flex : 4, alignItems : 'center', justifyContent: 'center'}}>
			    		 	
			    		 </View>
			    		 <Image
						        style={{ width: 24, height: 24, marginRight: 15}}
	                          	source={require('../images/blue_arrow.png')}
						        />
		    		</View>
		    		)}
		    	keyExtractor= {(item, index) => index.toString() }
		    	/>
		    	);
	  }
}

class Gurus extends React.Component {
	constructor(props)
		{
		    super(props);

		    this.state = {
		    	gurusArray: gurus
			}
		}
	render () {
		 return (
		    <FlatList
		    	ItemSeparatorComponent = {this.FlatListItemSeparator}
		    	data={this.state.gurusArray}
		    	renderItem= {({item, index}) =>(
		    		<View style={{flex : 10, height: 50,flexDirection: 'row', alignItems : 'center', justifyContent: 'center', backgroundColor: '#09273b'}}>
			    		 <View style={{flex : 1, alignItems : 'center', justifyContent: 'center'}}>
			    		 	<Text style={{color: '#fff'}}> {item.rank}</Text>
			    		 </View>
			    		 <Image
	                          style={{width: 36, height: 36, borderRadius: 36/2, alignItems: 'center'}}
	                          source={{uri: img}}
	                      />
	                      <View style={{flex : 3, alignItems : 'flex-start', justifyContent: 'center', paddingLeft: 15}}>
			    		 	<Text style={{color: '#fff'}}> {item.name} </Text>
			    		 </View>

			    		 <View style={{flex : 4, alignItems : 'center', justifyContent: 'center'}}>
			    		 	
			    		 </View>
			    		 <Image
						        style={{ width: 24, height: 24, marginRight: 15}}
	                          	source={require('../images/blue_arrow.png')}
						        />
		    		</View>
		    		)}
		    	keyExtractor= {(item, index) => index.toString() }
		    	/>
		    	);
	  }
}


FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#fff",

        }}
      />
    );
  }

const Tabs = createMaterialTopTabNavigator({
			  Explorers: {screen:Explorers},
			  Achievers: {screen:Achievers},
			  Dynamos: {screen:Dynamos},
			  Gurus: {screen:Gurus}
	},
		{
	    initialRouteName: 'Explorers',
	    swipeEnabled: true,
	    animationEnabled: true,
	    tabBarOptions: {
	      style: {
	        height: 50,
	        backgroundColor: '#2e5e86',
	      },
	      labelStyle:{
	      	fontSize:13
	      },
	     inactiveTintColor:'#fff',
	     activeTintColor:'#F5A623',
	     upperCaseLabel:false,
	     indicatorStyle:{opacity: 0},

	     }
  	});

const LeaderBoardStackNavigator = createStackNavigator({
	LeaderBoardTab : { screen: Tabs},
	ConsultantDetails:{
        screen : ConsultantDetails,
      }
});

export default class LeaderBoardHome extends React.Component {

	constructor(props)
	{
	    super(props);

	    this.state = {
	    	isLoading: true,
	    	selected:0
		}
	}

	_openConsultant(value){
		console.log(value);
		//this.makeRemoteRequest();
		this.setState({selected:value});
	}

	 makeRemoteRequest = () => {
    	this.setState({ isLoading: true});
    }

	render(){

		/*if (this.state.isLoading) {
	      return (
	        <View style={{flex: 1, backgroundColor:'#fff', alignItems : 'center', justifyContent: 'center'}}>
	          <ActivityIndicator />
	        </View>
	      );
	    }*/

		return (
			
			<View style={{flex: 1, backgroundColor:'#fff',justifyContent: 'center'}}>
		        <View style={{flex: 8}}>
		        	{<LeaderBoardStackNavigator/>}
		        </View>
		        <View style={{flex: 1, alignItems : 'center', backgroundColor:'#fff',justifyContent: 'flex-end', flexDirection: 'row'}}>
		        	
			         <View style={{flex: 1, alignItems : 'center', backgroundColor:'#fff',justifyContent: 'center', flexDirection: 'column' }}>
			         <TouchableOpacity onPress = {this._openConsultant.bind(this, 0)}>
			         	<View style = {{alignItems: 'center'}}>
				          	<Image
						        style={{ width: 24, height: 24, alignItems: 'center'}}
	                          	source={require('../images/ic_team_active.png')}
						        />
				         	<Text style= {(this.state.selected == 0)? {color:'#2e5e86', fontWeight: 'bold' }:{color: '#000000', fontWeight: 'normal'}}>Consultant</Text>
			         	</View>
			         	</TouchableOpacity>
			         </View>
			         
			        
			         <View style={{flex: 1, alignItems : 'center', backgroundColor:'#fff',justifyContent: 'center', flexDirection: 'column'}}>
			         <TouchableOpacity onPress = {this._openConsultant.bind(this, 1)}>
			         	<View style = {{alignItems: 'center'}}>
				         	<Image
						        style={{ width: 24, height: 24, alignItems: 'center'}}
	                          	source={require('../images/ic_team_active.png')}
						        />
				         	<Text style= {(this.state.selected == 1)? {color:'#2e5e86', fontWeight: 'bold' }:{color: '#000000', fontWeight: 'normal'}}>Team</Text>
			         	</View>
			         	</TouchableOpacity>
			         </View>
			         
			        
			         <View style={{flex: 1, alignItems : 'center', backgroundColor:'#fff',justifyContent: 'center', flexDirection: 'column'}}>
			         <TouchableOpacity onPress = {this._openConsultant.bind(this, 2)}>
			         	<View style = {{alignItems: 'center'}}>
				         	<Image
						        style={{ width: 24, height: 24, alignItems: 'center'}}
	                          	source={require('../images/ic_team_active.png')}
						        />
				         	<Text style= { (this.state.selected == 2)? {color:'#2e5e86', fontWeight: 'bold' }:{color: '#000000', fontWeight: 'normal'}}>Wall of fame</Text>
			         	</View>
			         	</TouchableOpacity>
			         </View>
			         
		        </View>
	        </View>
	        );
	}

}

const explorers = [
    { "rank":1, "name":"Abhi", "avatar": "", "trophy":"" },
    { "rank":2, "name":"Bobby", "avatar": "", "trophy":"" },
    { "rank":3, "name":"Chetan", "avatar": "", "trophy":"" },
    { "rank":4, "name":"David", "avatar": "", "trophy":"" },
    { "rank":5, "name":"Ehsan", "avatar": "", "trophy":"" }];
const achievers = [
    { "rank":1, "name":"Ronaldo", "avatar": "", "trophy":"" },
    { "rank":2, "name":"Bobby", "avatar": "", "trophy":"" },
    { "rank":3, "name":"Messi", "avatar": "", "trophy":"" },
    { "rank":4, "name":"Zalatan", "avatar": "", "trophy":"" },
    { "rank":5, "name":"Ramos", "avatar": "", "trophy":"" }];
 const dynamos = [
    { "rank":1, "name":"Khaleel", "avatar": "", "trophy":"" },
    { "rank":2, "name":"Virat", "avatar": "", "trophy":"" },
    { "rank":3, "name":"Shikhar", "avatar": "", "trophy":"" },
    { "rank":4, "name":"David", "avatar": "", "trophy":"" },
    { "rank":5, "name":"Pujara", "avatar": "", "trophy":"" }];
 const gurus = [
    { "rank":1, "name":"Serena", "avatar": "", "trophy":"" },
    { "rank":2, "name":"Nadal", "avatar": "", "trophy":"" },
    { "rank":3, "name":"Chetan", "avatar": "", "trophy":"" },
    { "rank":4, "name":"Sania", "avatar": "", "trophy":"" },
    { "rank":5, "name":"Sachin", "avatar": "", "trophy":"" }];
