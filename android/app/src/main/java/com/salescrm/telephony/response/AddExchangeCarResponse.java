package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by prateek on 20/12/16.
 */

public class AddExchangeCarResponse {

    private String statusCode;

    private String message;

    private List<Result> result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {

        private String kms_run;

        private String lead_exchange_car_id;

        private String purchase_date;

        private String car_model_id;

        private String reg_no;

        public String getKms_run ()
        {
            return kms_run;
        }

        public void setKms_run (String kms_run)
        {
            this.kms_run = kms_run;
        }

        public String getLead_exchange_car_id ()
        {
            return lead_exchange_car_id;
        }

        public void setLead_exchange_car_id (String lead_exchange_car_id)
        {
            this.lead_exchange_car_id = lead_exchange_car_id;
        }

        public String getPurchase_date ()
        {
            return purchase_date;
        }

        public void setPurchase_date (String purchase_date)
        {
            this.purchase_date = purchase_date;
        }

        public String getCar_model_id ()
        {
            return car_model_id;
        }

        public void setCar_model_id (String car_model_id)
        {
            this.car_model_id = car_model_id;
        }

        public String getReg_no ()
        {
            return reg_no;
        }

        public void setReg_no (String reg_no)
        {
            this.reg_no = reg_no;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [kms_run = "+kms_run+", lead_exchange_car_id = "+lead_exchange_car_id+", purchase_date = "+purchase_date+", car_model_id = "+car_model_id+", reg_no = "+reg_no+"]";
        }
    }
    public class Error {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }
}
