package com.salescrm.telephony.model;

import android.util.SparseArray;

import com.salescrm.telephony.interfaces.FilterDataChangeListener;

/**
 * Created by bharath on 17/10/16.
 */

public class FilterSelectionHolder {
    private static final FilterSelectionHolder holder = new FilterSelectionHolder();
    public static FilterDataChangeListener listener;
    private SparseArray<SparseArray<String>> mapData = new SparseArray<>();

    public static FilterSelectionHolder getInstance(FilterDataChangeListener filterDataChangeListener) {
        listener = filterDataChangeListener;
        return holder;
    }

    public static FilterSelectionHolder getInstance() {
        return holder;
    }

    public void setMapData(Integer key, SparseArray<String> hashMap) {
        this.mapData.put(key, hashMap);
        if (listener != null) {
            listener.onFilterDataChanged();
        }
    }

    public void setMapDataNoCallBack(Integer key, SparseArray<String> hashMap) {
        this.mapData.put(key, hashMap);
    }

    public void removeMapData(Integer key) {
        this.mapData.remove(key);
        if (listener != null) {
            listener.onFilterDataChanged();
        }
    }

    public int getCount() {
        return mapData.size();
    }

    public SparseArray<String> getMapData(Integer key) {
        return mapData.get(key);
    }

    public SparseArray<SparseArray<String>> getAllMapData() {
        return mapData;
    }

    public void setAllMapData(SparseArray<SparseArray<String>> mapData) {
         this.mapData=mapData;
    }


    public void clearAll() {
        mapData.clear();
        if (listener != null) {
            listener.onFilterDataChanged();
        }
    }

    public boolean isEmpty() {
        return mapData.size() == 0;
    }
}
