package com.salescrm.telephony.adapter.offline;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;

import com.salescrm.telephony.fragments.offline.ActionsFragmentOffline;
import com.salescrm.telephony.fragments.offline.CarsFragmentOffline;
import com.salescrm.telephony.fragments.offline.CustomerDetailsFragmentOffline;
import com.salescrm.telephony.fragments.offline.FabC360FragmentOffline;

/**
 * Created by bharath on 28/12/16.
 */
public class C360PagerAdapterOffline extends FragmentPagerAdapter {
    int mNumOfTabs;
    private int scheduledActivityId;

    public C360PagerAdapterOffline(FragmentManager fm, int mNumOfTabs,int scheduledActivityId) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
        this.scheduledActivityId = scheduledActivityId;
    }
    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("scheduledActivityId", scheduledActivityId);

        switch (position) {
            case 0:
                ActionsFragmentOffline fragmentOffline =new ActionsFragmentOffline();
                fragmentOffline.setArguments(bundle);
              return fragmentOffline;
            case 1:
                CarsFragmentOffline carsFragmentOffline =new CarsFragmentOffline();
                carsFragmentOffline.setArguments(bundle);
               return carsFragmentOffline;
            case 2:
                CustomerDetailsFragmentOffline customerDetailsFragmentOffline =new CustomerDetailsFragmentOffline();
                customerDetailsFragmentOffline .setArguments(bundle);
                return customerDetailsFragmentOffline;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {

        return mNumOfTabs;
    }
}
