"use strict";
import React, { Component } from "react";
import { View, Text, TextInput, Animated } from "react-native";
export default class TextInputLayout extends Component {
  constructor(props) {
    super(props);
    this.state = { isFocused: false, error: false, transalteY: new Animated.Value(0) };
  }
  doTransalte = (toValue) => {
    Animated.timing(this.state.transalteY, {
      toValue: toValue,
      duration: 200,
      useNativeDriver: true
    }).start();
  }
  render() {
    const yVal = this.state.transalteY.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -15]
    })
    const animStyle = {
      transform: [{
        translateY: yVal
      }]
    }
    let color = "#CFCFCF";
    if (this.state.error) {
      color = "#FF0000";
    } else if (this.state.isFocused) {
      color = "#007FFF";
    }
    return (
      <View style={{ paddingTop: 10, height: 60, }}>

        <Animated.View style={[styles.ball, animStyle]}>
          <Text
            style={{
              fontSize: 11,
              color,
              height: 16,
              position: 'absolute'
            }}
          >
            {this.state.isFocused || this.state.text ? this.props.hint : ""}
          </Text>
        </Animated.View>


        <TextInput
          style={[
            {
              borderColor: color,
              fontSize: 16,
              paddingTop: 4,
              paddingBottom: 4,
              paddingLeft: 0,
              height: 30,
              borderBottomWidth: this.state.isFocused ? 2 : 1
            }
          ]}
          onChangeText={text => {
            this.setState({ text });
            this.props.onTextChange(text);
          }}
          onFocus={() => {
            this.setState({ isFocused: true, error: false });
            this.doTransalte(1);
          }}
          onBlur={() => {
            let error = this.state.error;
            if (
              this.state.text &&
              this.props.pattern &&
              !this.props.pattern.test(this.state.text)
            ) {
              error = true;
            }
            this.setState({ isFocused: false, error });
            if (!this.state.text) {
              this.doTransalte(0);
            }

          }}
          placeholder={!this.state.isFocused ? this.props.hint : ""}
          placeholderTextColor="#CFCFCF"
          value={this.state.text}
          {...this.props}
        />
      </View>
    );
  }
}
