package com.salescrm.telephony.model.booking;

/**
 * Created by bharath on 19/3/18.
 */

public class ApiInputCustomerDetails {
    private String booking_name, dob, pan, address, aadhaar, pin_code, care_of_type, care_of, buyer_type_id, customer_type_id, gst;

    public ApiInputCustomerDetails(String booking_name, String dob, String pan, String address, String aadhaar, String pin_code, String care_of_type, String care_of, String buyer_type_id, String customer_type_id, String gst) {
        this.booking_name = booking_name;
        this.dob = dob;
        this.pan = pan;
        this.address = address;
        this.aadhaar = aadhaar;
        this.pin_code = pin_code;
        this.care_of_type = care_of_type;
        this.care_of = care_of;
        this.buyer_type_id = buyer_type_id;
        this.customer_type_id = customer_type_id;
        this.gst = gst;
    }

    public String getBooking_name() {
        return booking_name;
    }

    public void setBooking_name(String booking_name) {
        this.booking_name = booking_name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAadhaar() {
        return aadhaar;
    }

    public void setAadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
    }

    public String getPin_code() {
        return pin_code;
    }

    public void setPin_code(String pin_code) {
        this.pin_code = pin_code;
    }

    public String getCare_of_type() {
        return care_of_type;
    }

    public void setCare_of_type(String care_of_type) {
        this.care_of_type = care_of_type;
    }

    public String getCare_of() {
        return care_of;
    }

    public void setCare_of(String care_of) {
        this.care_of = care_of;
    }

    public String getBuyer_type_id() {
        return buyer_type_id;
    }

    public void setBuyer_type_id(String buyer_type_id) {
        this.buyer_type_id = buyer_type_id;
    }

    public String getCustomer_type_id() {
        return customer_type_id;
    }

    public void setCustomer_type_id(String customer_type_id) {
        this.customer_type_id = customer_type_id;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }
}
