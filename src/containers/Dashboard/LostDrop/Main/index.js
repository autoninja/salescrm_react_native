import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, Platform, Button, FlatList, Alert, ScrollView, TouchableOpacity, Image, Switch, ActivityIndicator, RefreshControl, NativeModules, TouchableWithoutFeedback } from 'react-native';
//import { VictoryPie } from 'react-native-svg'
import { VictoryPie } from "victory-native";
import Svg from 'react-native-svg';
import { NavigationEvents } from "react-navigation";
import Toast, {DURATION} from 'react-native-easy-toast'

import RestClient from '../../../../network/rest_client'
import LostDropRevenueLoss from '../../../../components/lostDrop/lost_drop_revenue_loss'
import styles from './LostDrop.styles'

import { Dimensions } from 'react-native';
import { withNavigationFocus } from "react-navigation";

import CleverTapPush from '../../../../clevertap/CleverTapPush'
import {eventTeamDashboard} from '../../../../clevertap/CleverTapConstants';
const { width } = Dimensions.get('window');


global.global_lost_drop_filters_selected=[];

window.getLostDropFilterSelectedCount= function() {
    return global.global_lost_drop_filters_selected.length;
};


class LostDrop extends Component {
  constructor(props) {
    super(props);

    var endDate = new Date();
    var startDate =  new Date();
    startDate.setDate(1);

    this.state = {
    isLoading:true,
    isApiCalledAtleastOnce : false,
    result:{},
    dateRange:{startDate, endDate},
    initDate:{startDate:global_filter_date.startDateStr, endDate:global_filter_date.endDateStr},
    dateRangeVisibilty : false,
    lostchartstate : false,
    dropchartstate :false,
     simpleData : [
        { label: "A", y: 1,},
        { label: "A", y: 1,},
        { label: "A", y: 1,},
        { label: "A", y: 1,},
        { label: "A", y: 1,},
        { label: "A", y: 1,},
      ],
    colors : [
      '#ffa600',
      '#ff6e54',
      '#dd5182',
      '#955196',
      '#444e86',
      '#003f5c',
    ],


    };
  }

  componentDidMount() {
    this.getLostDropAnalysis();
    this._mounted = true;
  }
  componentWillUnmount () {
    this._mounted = false;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      if(this.props.isFocused) {
        CleverTapPush.pushEvent(eventTeamDashboard.event, {'Dashboard Type': eventTeamDashboard.keyType.valuesType.lostDropAnalysis});

      } 
    }
  }

  _showLostDropFilter(navigation){
    navigation.navigate('LostDropFilters', {onGoBack:()=> {
      this._onRefresh();
    }})
  }
  _showDateRangePicker(navigation) {
  navigation.navigate('DateRangePickerDialog',{initDate :this.state.dateRange, onFilterApply:(s, e)=>{

    if(s&&e) {
        start = s.split('-');
        end = e.split('-')

        localDateRange = {};

        localStartDate = new Date();
        localStartDate.setDate(start[2]);
        localStartDate.setMonth(start[1]-1);
        localStartDate.setYear(start[0]);
        localDateRange.startDate = localStartDate;

        localEndDate = new Date();
        localEndDate.setDate(end[2]);
        localEndDate.setMonth(end[1]-1);
        localEndDate.setYear(end[0]);
        localDateRange.endDate = localEndDate;

        endDate = new Date();

        console.log("showDateRangePickerrr"+s+':::'+e);
        this.setState({dateRange:localDateRange,initDate:{startDate:global_filter_date.startDateStr, endDate:global_filter_date.endDateStr}});
        this._onRefresh();
        //CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type' : eventTeamDashboard.keyType.valuesType.etvbrDate });

    }

  }})

  console.log("showDateRangePickerrr");
  }
  updateOnFocusChange(){
    if(this.state.initDate.startDate != global.global_filter_date.startDateStr ||
    this.state.initDate.endDate != global.global_filter_date.endDateStr) {
     this._onRefresh();
    }
    //this.pushCleverTapEvent({'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrLocation});
  }

  _onRefresh() {
    this.setState({ isLoading: true, initDate:{startDate:global_filter_date.startDateStr, endDate:global_filter_date.endDateStr}}, function() { this.getLostDropAnalysis() });
  }

  getLostDropAnalysis() {
    new RestClient().getLostDropAnalysis({
      start_date:global.global_filter_date.startDateStr,
      end_date:global.global_filter_date.endDateStr,
      filters:encodeURIComponent(JSON.stringify(global.global_lost_drop_filters_selected))})
      .then( (data) => {
        if(!this._mounted){
          return;
        }
        console.log(JSON.stringify(data));

        if(!this.state.isApiCalledAtleastOnce) {
          //this.pushCleverTapEvent({'Dashboard Type': eventTeamDashboard.keyType.valuesType.liveEFTeams});
        }

        this.setState({lostchartstate : false,
          dropchartstate :false,
          isApiCalledAtleastOnce:true,
          isLoading: false,
          result: data.result,})

      }).catch(error => {
        //console.error(error);
        if(!this._mounted){
          return;
        }
        this.setState({ lostchartstate : false,
          dropchartstate :false,
          isApiCalledAtleastOnce:true,
          isLoading: false,
          result:{}});
        if (!error.status) {
            this.refs.toast.show('Connection Error');
        }
      });
  }



    lostChartState(segments){
      if(!segments || segments.length ==0) {
          this.refs.toast.show("No data");
          return;
      }
      let dropState = this.state.dropchartstate;
      if(dropState) {
        dropState =!dropState;
      }
      this.setState({lostchartstate:!this.state.lostchartstate, dropchartstate:dropState})
    }

    dropChartState(segments){
      if(!segments || segments.length ==0) {
          this.refs.toast.show("No data");
          return;
      }
      let lostState = this.state.lostchartstate;
      if(lostState) {
        lostState =!lostState;
      }
      this.setState({dropchartstate:!this.state.dropchartstate, lostchartstate:lostState})
    }

    gotToDetails(segments, id, navigation, modelData, color) {
      if(!segments || segments.length ==0) {
          this.refs.toast.show("No data");
          return;
      }
      // if(global.fromAndroid) {
      //   NativeModules.ReactNativeToAndroid.navigateToLostDropDetails(id+"", global.global_filter_date.startDateStr,
      //     global.global_filter_date.endDateStr,
      //     JSON.stringify(global.global_lost_drop_filters_selected),
      //     JSON.stringify(modelData),
      //     color
      //   );
      // }
      switch(id) {
        case 0:
            navigation.navigate("LostDetailsNewUsed",{ modelData, color });
          break;
        case 1:
            navigation.navigate("DropDetailsReasons", { modelData, color });
          break;
        }


    }

    getWrapView(segments, colors, id) {
      let modelNames = [];
      for(let i=0;i<segments.length;i++) {
        modelNames.push(

           <TouchableWithoutFeedback key={segments[i].id}  disabled = {segments[i].id==0} onPress = {this.gotToDetails.bind(this, segments, id, this.props.navigation, segments[i], colors[i]) }>
              <View style={[styles.wrapButton, {backgroundColor:colors[i]}]}>
                <Text numberOfLines={1} style={styles.wrapText} key={segments[i].id}>{segments[i].name+" "}</Text>
              </View>
           </TouchableWithoutFeedback>

        )
      }
      return (<View style={styles.wrapView}>{modelNames}</View>)
    }
  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  render() {

    if(this.state.isLoading){
    return(
      <View style = {{display:(this.state.isLoading?'flex':'none'), flex:1,justifyContent: 'center',
        alignItems: 'center',backgroundColor:'#fff'}}>

      <ActivityIndicator
            animating={this.state.isLoading}
            style={{flex: 1,   alignSelf:'center',}}
            color="#2e5e86"
            size="large"
            hidesWhenStopped={true}

      />

      <NavigationEvents
        onWillFocus={payload => {
          this.updateOnFocusChange();
        }}
      />

      </View>

    )
   }

    let startDateData  =  global.global_filter_date.startDateStr.split('-');
    let endDateData  =  global.global_filter_date.endDateStr.split('-');

    date=("0" + startDateData[2]).slice(-2)+'/'+("0" + startDateData[1]).slice(-2)
    +' - '+("0" + endDateData[2]).slice(-2)+'/'+("0" + endDateData[1]).slice(-2);

    let navigation = this.props.navigation;
    let totalLost = 0;
    let revenueLossInLost = 0;
    let lostSegments = [];
    let lostSegmentsPie = [];
    let lostSegmentsColors = [];
    let totalDrop = 0;
    let revenueLossInDrop = 0;
    let dropSegments = [];
    let dropSegmentsPie = [];
    let dropSegmentsColors = [];
    if(this.state.result) {
      let result = this.state.result;
      if(result.lost) {
        totalLost = result.lost.total;
        revenueLossInLost = result.lost.loss_in_revenue;
        lostSegments = result.lost.segments?result.lost.segments:[];
        for(var i=0; i<lostSegments.length; i++) {
          lostSegmentsPie.push({label:lostSegments[i].rel+'%'+' ('+lostSegments[i].abs+')', y:lostSegments[i].rel})
          if(lostSegments[i].id == 0) {
            lostSegmentsColors.push('#B8B8B8');
          }
          else {
            lostSegmentsColors.push(i<this.state.colors.length?this.state.colors[i]:getRandomColor());
          }

        }
      }
      if(result.drop) {
        totalDrop = result.drop.total;
        revenueLossInDrop = result.drop.loss_in_revenue;
        dropSegments = result.drop.segments?result.drop.segments:[];
        for(var i=0;i<dropSegments.length;i++) {
          dropSegmentsPie.push({label:dropSegments[i].rel+'%'+' ('+dropSegments[i].abs+')', y:dropSegments[i].rel})
          if(dropSegments[i].id == 0) {
            dropSegmentsColors.push('#B8B8B8');
          }
          else {
            dropSegmentsColors.push(i<this.state.colors.length?this.state.colors[i]:getRandomColor());
          }
        }
      }
    }

    return (
      <View style={{backgroundColor:'#fff', flex:1}}>
      <Toast
      ref="toast"
      style={{backgroundColor:'red'}}
      position='top'
      fadeInDuration={750}
      fadeOutDuration={750}
      opacity={0.8}
      textStyle={{color:'white'}}
      />
      <ScrollView contentContainerStyle={{flexGrow: 1, backgroundColor: '#fff'}} showsVerticalScrollIndicator={false}
      refreshControl={
        <RefreshControl
          refreshing={this.state.isLoading}
          onRefresh={this._onRefresh.bind(this)}
        />}
      >
      <NavigationEvents
        onWillFocus={payload => {
          this.updateOnFocusChange();
        }}
      />
      <View style={styles.container}>
        <View style={{
         flexDirection: 'row',
         height:60, }}>
         <View style={{flex:1/2, height:30, margin:5, flexDirection :'row', alignItems:'center',justifyContent:'flex-start'}}>
           <View style={{backgroundColor:'#F4F4F4', borderWidth:1, borderColor:'#D0D8E4',borderRadius:6, justifyContent:'center'}}>
            <Text style ={{ fontSize:17, padding:4, color:'#1B143C', textAlign:'center'}}>{date}</Text>
           </View>
           <TouchableOpacity onPress = {this._showDateRangePicker.bind(this,this.props.navigation) }>
             <Image source={require('../../../../images/ic_calender_blue.png')}
             style={[styles.icon, {marginLeft:10}]}
              />
            </TouchableOpacity>
         </View>

        <View style={{flexDirection:'row',  height:30,  margin:5, flex:1/2, alignItems:'center',justifyContent:'flex-end',}} >
          {
          //   <View style={{alignItems:'center'}}>
          //   <Image source={require('../../../../images/ic_download.png')}
          //       style={[styles.icon]}/>
          //   <Text style={{color:'#2e5e86', fontSize:14}}>Report</Text>
          // </View>
        }
          <TouchableOpacity onPress = {this._showLostDropFilter.bind(this, this.props.navigation)}>
            <View style={{flexDirection:'row'}}>
              <Image source={require('../../../../images/ic_etvbr_filter.png')}
              style={[styles.icon, {marginRight:10,}]}/>

              <View style={{display:(isLostDropFilterSelected()?'flex':'none'),marginLeft:-14,marginTop:-6,backgroundColor:'#FFCA28', height:16, width:16, alignItems:'center', borderRadius:10, justifyContent: "center",alignItems: "center"}}>
                <Text adjustsFontSizeToFit={true}
                   numberOfLines={1} style={{textAlign:'center', fontSize:8,textAlignVertical: 'center', color:'#303030'}}>{
                     getLostDropFilterSelectedCount()
                   }</Text>
              </View>

            </View>
          </TouchableOpacity>
          </View>
        </View>

        <View style={styles.shadowsView}>
          <View style={styles.innerView}>
          <TouchableOpacity style={{padding: 5, width: '100%',}} onPress={()=> this.lostChartState(lostSegments)}>
            <View style={styles.innerRow}>
              <Text style={styles.textTitle}>Lost</Text>
              <TouchableOpacity onPress={()=> this.gotToDetails(lostSegments, 0, this.props.navigation)} >
                <Text style={styles.textTitleclickable}>{totalLost}</Text>
              </TouchableOpacity>
            </View>

          </TouchableOpacity>

          {(this.state.lostchartstate)?
            <View style={{flex:1, justifyContent: 'center', alignItems: 'center', width:'100%'}}>
              <View style= {styles.line}/>
              <Svg  width={width * 2} height={280}  style={{ alignItems:'center', justifyContent:'center',}}>
                <VictoryPie
                  height={280}
	                width={width * 2}
                  innerRadius={50}
                  standalone={false}
                  data={lostSegmentsPie}
                  style={{ labels: { fontSize: 11, fill: "#303030", fontWeight:'bold', fontFamily: "Roboto", textAlign:'center' }, alignSelf:'center'}}
                  colorScale={lostSegmentsColors}
                  />
              </Svg>
              {this.getWrapView(lostSegments, lostSegmentsColors, 0)}
              <LostDropRevenueLoss loss={revenueLossInLost} style={{marginTop:10, marginBottom:10}}/>

            </View>:null}

          </View>
        </View>
        <View style={styles.shadowsView}>
          <View style={styles.innerView}>
          <TouchableOpacity style={{padding: 5, width: '100%'}} onPress={()=> this.dropChartState(dropSegments)}>
            <View style={styles.innerRow}>
              <Text style={styles.textTitle}>Drop</Text>
              <TouchableOpacity onPress={()=> this.gotToDetails(dropSegments, 1, this.props.navigation)}>
              <Text style={styles.textTitleclickable}>{totalDrop}</Text>
              </TouchableOpacity>
            </View>


          </TouchableOpacity>
            {(this.state.dropchartstate)?

            <View style={{flex:1, justifyContent: 'center', alignItems: 'center', width: '100%'}}>
              <View style= {styles.line}/>
                <Svg  width={width * 2} height={280} style={{ alignItems:'center', justifyContent:'center',}}>
                  <VictoryPie
                    height={280}
                    width={width * 2}
                    innerRadius={50}
                    standalone={false}
                    data={dropSegmentsPie}
                    style={{ labels: { fontSize: 12, fill: "#303030", fontWeight:'bold', fontFamily: "Roboto", textAlign:'center' }, alignSelf:'center'}}
                    colorScale={dropSegmentsColors}
                    />
                    </Svg>
                {this.getWrapView(dropSegments, dropSegmentsColors, 1)}
                <LostDropRevenueLoss loss={revenueLossInDrop} style={{marginTop:10, marginBottom:10}} />

            </View>:null}
          </View>
        </View>
      </View>
      </ScrollView>
      </View>
    )
  }
}

export default withNavigationFocus(LostDrop)
