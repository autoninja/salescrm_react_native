package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.db.SalesCRMRealmTable;

import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.RetrofitError;

/**
 * Created by bharath on 7/1/17.
 */
public interface AutoFetchFormListener {
    void onAutoFormsDataFetched(boolean b, RealmResults<SalesCRMRealmTable> data);
    void onAutoFormsDataError(RetrofitError error,int from);
}
