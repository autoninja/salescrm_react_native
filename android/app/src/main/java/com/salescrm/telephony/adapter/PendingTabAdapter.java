package com.salescrm.telephony.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.salescrm.telephony.fragments.PendingDetailsFragment;
import com.salescrm.telephony.utils.WSConstants;

/**
 * Created by bannhi on 12/9/17.
 */

public class PendingTabAdapter extends FragmentPagerAdapter {

    public PendingTabAdapter(FragmentManager fm) {
        super(fm);
    }

    /**
     * This method may be called by the ViewPager to obtain a title string
     * to describe the specified page. This method may return null
     * indicating no title for this page. The default implementation returns
     * null.
     *
     * @param position The position of the title requested
     * @return A title for the requested page
     */
    @Override
    public CharSequence getPageTitle(int position)
    {
         //return "Bangalore";
            return WSConstants.Locations.get(position).getLocName();

    }

    @Override
    public Fragment getItem(int position) {

        System.out.println("GET ITEM IS CALLEd"+position);
        return new PendingDetailsFragment().newInstance(position,WSConstants.LAST_PENDING_TAB);
    }

    @Override
    public int getCount() {
            return WSConstants.Locations.size();

        //return 20;
    }
    public void updateView(){
        this.notifyDataSetChanged();
    }


}
