
class EventsMain {
  constructor(locationId, locationName, eventsLeadSourceCategories) {
    this.locationId = locationId;
    this.locationName = locationName;
    this.eventsLeadSourceCategories = eventsLeadSourceCategories;
  }
}

class EventsLeadSourceCategory {
  constructor(id, name, leadSources) {
    this.id = id;
    this.name = name;
    this.leadSources = leadSources;
  }
}

class EventsLeadSource {
  constructor(id, name, events) {
    this.id = id;
    this.name = name;
    this.events = events;
  }
}

class Events {
  constructor(id, name, start_date, end_date) {
    this.id = id;
    this.name = name;
    this.start_date = start_date;
    this.end_date = end_date;
  }
}


module.exports = { EventsMain, EventsLeadSourceCategory, EventsLeadSource, Events };
