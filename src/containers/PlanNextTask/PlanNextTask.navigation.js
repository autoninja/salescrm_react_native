import React from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import PlanNextTask from './PlanNextTask'
import OtpPicker from './OtpPicker'
import BackIcon from '../../components/uielements/BackIcon';
export const getPlanNextNavigator = (info)=> createAppContainer(createStackNavigator({
    PlanNextTask : { 
        screen: props => (
            <PlanNextTask {...props} info={info}/>
        ),
        navigationOptions: ({ navigation }) => ({
        header: null
        })
    },
    OtpPicker: {
        screen: props => (
            <OtpPicker
            {...props}
            info={info}
            />
        ),
        navigationOptions: ({ navigation }) => ({
            title: "Submit OTP",
            headerBackTitle: null,
            headerLeft: <BackIcon navigationProps={navigation} invert={true} />,
            headerStyle: {
                backgroundColor: '#fff',
                elevation: 0,
            },
            headerTransperent: true,
            headerTintColor: '#303030',
            headerTitleStyle: {
                fontWeight: 'bold',
                width: '100%',
                textAlign: 'left',
            },
        })
    }
}, {
    initialRouteName: "PlanNextTask",
    cardStyle: {
        backgroundColor: "#303030BE",
        opacity: 1
      },
      mode: "modal"
}
));
