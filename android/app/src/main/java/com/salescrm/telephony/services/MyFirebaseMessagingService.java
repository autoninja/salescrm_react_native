package com.salescrm.telephony.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

//import com.clevertap.android.sdk.CleverTapAPI;
//import com.clevertap.android.sdk.NotificationInfo;
import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.NotificationInfo;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.HomeActivity;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.model.ExternalNotificationModel;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Map;

import io.realm.Realm;

/**
 * Created by prateek on 4/11/16.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private int NOTIFICATION_CAT;
    private int NOTIFICATION_TYPE;
    private String MESSAGE = "";
    private String LEAD_ID ="";

    FCMNotification fcmNotification;
    private String title;
    private String content;
    private String TAG = getClass().getSimpleName()+" : ";
    private Integer activityId, leadId, scheduledActivityId, locationId;
    private String firebaseEvent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        try {
        if (remoteMessage != null) {
            //Just to see the complete JSON
            Map<String, String> params = remoteMessage.getData();
            JSONObject object = new JSONObject(params);
            Log.e(TAG, object.toString());

            //Debug copy
          /*  ClipboardManager clipboard = (ClipboardManager) getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Copied Text",  object.toString());
            clipboard.setPrimaryClip(clip);
            Util.showToast(getApplicationContext(),"API Response copied to clipboard",Toast.LENGTH_LONG);*/


            String responseObj = object.toString();
            if (responseObj != null) {
                responseObj = responseObj.replace("\"{", "{").replace("}\"", "}").replace("\\\"", "\"");
                System.out.println(TAG + "Split Response:" + responseObj);
                ExternalNotificationModel notificationModel = new Gson().fromJson(responseObj, ExternalNotificationModel.class);
                if (notificationModel != null && notificationModel.getBody() != null) {
                    System.out.println(TAG + "Notification Model:Title" + notificationModel.getTitle());
                    title = notificationModel.getTitle();
                    content = notificationModel.getMessage();
                    activityId = getInt(notificationModel.getBody().getActivity_id());
                    leadId = getInt(notificationModel.getBody().getLead_id());
                    scheduledActivityId = getInt(notificationModel.getBody().getScheduled_activity_id());
                    firebaseEvent = notificationModel.getEvent();
                    locationId = getInt(notificationModel.getBody().getLocation_id());
                    if (firebaseEvent.equalsIgnoreCase(WSConstants.FirebaseEvent.ACTIVITY_DONE)) {
                        updateDb(scheduledActivityId);
                    } else {
                        Util.showAppNotification(getApplicationContext(), leadId,
                                content, scheduledActivityId, title, Calendar.getInstance().getTimeInMillis(), activityId, true, firebaseEvent, locationId);
                    }
                } else {
                    System.out.println(TAG + "Notification Model:NULL");

                }
            }

            Bundle extras = new Bundle();
            for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {
                extras.putString(entry.getKey(), entry.getValue());
            }
            NotificationInfo info = CleverTapAPI.getNotificationInfo(extras);
            if (info.fromCleverTap) {
                CleverTapAPI.createNotification(getApplicationContext(), extras);
            } else {

                title = remoteMessage.getData().get("title");
                content = remoteMessage.getData().get("message");

                if (remoteMessage.getData().containsKey("notification_cat") && remoteMessage.getData().containsKey("notification_cat")) {
                    if (remoteMessage.getData().get("notification_cat") != null && remoteMessage.getData().get("notification_type") != null) {
                        NOTIFICATION_CAT = Integer.parseInt(remoteMessage.getData().get("notification_cat"));
                        NOTIFICATION_TYPE = Integer.parseInt(remoteMessage.getData().get("notification_type"));
                        MESSAGE = remoteMessage.getData().get("message");
                        title = remoteMessage.getData().get("title");
                        if (remoteMessage.getData().get("lead_id") != null && !remoteMessage.getData().get("lead_id").equalsIgnoreCase("")) {
                            LEAD_ID = Integer.parseInt(remoteMessage.getData().get("lead_id")) + "";

                        }
                        fcmNotification = new FCMNotification(NOTIFICATION_CAT, NOTIFICATION_TYPE, MESSAGE, LEAD_ID);
                    }
                } else {
                    System.out.println("Keys are missing");
                }
            }
        }
        }catch (Throwable t){
            t.printStackTrace();
        }
    }

    private void updateDb(Integer scheduledActivityId) {
        if(scheduledActivityId!=null) {
            Realm realm = Realm.getDefaultInstance();
            SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
            if(salesCRMRealmTable!=null && salesCRMRealmTable.isValid()){
                realm.beginTransaction();
                salesCRMRealmTable.setDone(true);
                realm.commitTransaction();
            }
            if(!realm.isClosed()) {
                realm.close();
            }
        }
    }
    private int getInt(Integer integer){
        return integer == null? -1 : integer;
    }
}
