package com.salescrm.telephony.model;

import com.salescrm.telephony.model.booking.ApiInputBookingDetails;

public class EditCarModel {
    private int booking_id;
    private int enquiry_id;
    private int location_id;
    private long lead_id;
    private int lead_car_id;
    private int lead_source_id;
    private String lead_last_updated;
    private ApiInputBookingDetails booking_details;

    public EditCarModel(int booking_id, int enquiry_id, int location_id, long lead_id, int lead_car_id, int lead_source_id, String lead_last_updated, ApiInputBookingDetails booking_details) {
        this.booking_id = booking_id;
        this.enquiry_id = enquiry_id;
        this.location_id = location_id;
        this.lead_id = lead_id;
        this.lead_car_id = lead_car_id;
        this.lead_source_id = lead_source_id;
        this.lead_last_updated = lead_last_updated;
        this.booking_details = booking_details;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public long getLead_id() {
        return lead_id;
    }

    public void setLead_id(long lead_id) {
        this.lead_id = lead_id;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(int booking_id) {
        this.booking_id = booking_id;
    }

    public int getEnquiry_id() {
        return enquiry_id;
    }

    public void setEnquiry_id(int enquiry_id) {
        this.enquiry_id = enquiry_id;
    }

    public int getLead_car_id() {
        return lead_car_id;
    }

    public void setLead_car_id(int lead_car_id) {
        this.lead_car_id = lead_car_id;
    }

    public int getLead_source_id() {
        return lead_source_id;
    }

    public void setLead_source_id(int lead_source_id) {
        this.lead_source_id = lead_source_id;
    }

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }

    public ApiInputBookingDetails getBooking_details() {
        return booking_details;
    }

    public void setBooking_details(ApiInputBookingDetails booking_details) {
        this.booking_details = booking_details;
    }
}
