package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by bharath on 3/11/16.
 */
public class FormSubmissionResponse {
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public class Result{
        private String lead_id;
        private String lead_last_updated;
        private boolean submission_status;
        private GameData game_data;

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getLead_last_updated() {
            return lead_last_updated;
        }

        public void setLead_last_updated(String lead_last_updated) {
            this.lead_last_updated = lead_last_updated;
        }

        public boolean isSubmission_status() {
            return submission_status;
        }

        public void setSubmission_status(boolean submission_status) {
            this.submission_status = submission_status;
        }

        public GameData getGame_data ()
        {
            return game_data;
        }

        public void setGame_data (GameData game_data)
        {
            this.game_data = game_data;
        }
    }
    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class GameData {
        private List<BadgeDetails> badge_details;

        private String points_earned;

        private String activity;

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public List<BadgeDetails> getBadge_details ()
        {
            return badge_details;
        }

        public void setBadge_details (List<BadgeDetails> badge_details)
        {
            this.badge_details = badge_details;
        }

        public String getPoints_earned ()
        {
            return points_earned;
        }

        public void setPoints_earned (String points_earned)
        {
            this.points_earned = points_earned;
        }
    }

    public class BadgeDetails
    {
        private String id;

        private String name;

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }
    }
}