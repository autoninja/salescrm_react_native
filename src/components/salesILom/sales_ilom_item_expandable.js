import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import CallStat from './call_stat'

import SalesILomItemChild from './sales_ilom_item_child'

export default class SalesILomItemExpandable extends Component {
  constructor(props) {
    super(props);
    this.state = { expand: false, showImagePlaceHolder: true };
  }
  _etvbrLongPress(from, info) {
    this.props.onLongPress(from, info)
    console.log(info)
  }
  render() {

    const { team_leader } = this.props;
    let imageSource;
    let placeHolder
    if (this.props.from_location) {
      imageSource = require('../../images/ic_location.png');
      placeHolder = null;

    }
    else {
      imageSource = { uri: this.props.info.dp_url };
      placeHolder = <View style={{ position: 'absolute', display: (this.state.showImagePlaceHolder ? 'flex' : 'none'), width: 36, height: 36, borderRadius: 36 / 2, backgroundColor: '#FF8000', alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#fff' }}>{this.props.info.name.charAt(0).toUpperCase()}</Text></View>;;
    }


    let sc_views = []
    for (var i = 0; i < team_leader.sales_consultants.length; i++) {
      let sc = team_leader.sales_consultants[i];
      sc_views.push(

        <SalesILomItemChild
          isUnderTl={true}
          leadSourceCarModelWiseETVBR=
          {() => {
            this.props.leadSourceCarModelWiseETVBRInner(
              sc.info.name,
              sc.info.dp_url,
              this.props.info.location_id,
              sc.info.id,
              sc.info.user_role_id)
          }}
          onLongPress={this._etvbrLongPress.bind(this)}
          showPercentage={this.props.showPercentage} key={sc.info.id}
          call_stats = {
            {
              call_duration: sc.info.abs.call_duration,
              calls_contacted: sc.info.abs.calls_contacted,
              calls_attempted: sc.info.abs.calls_attempted
            }
          }
          abs={
            {
              e: sc.info.abs.enquiries,
              t: sc.info.abs.tdrives,
              v: sc.info.abs.visits,
              b: sc.info.abs.bookings,
              r: sc.info.abs.retails,
              l: sc.info.abs.lost_drop,
              assigned: sc.info.abs.enquiries,
              callback: sc.info.abs.tdrives,
              prospect: sc.info.abs.visits,
              sale_confirmed: sc.info.abs.bookings,
              il_done: sc.info.abs.retails,

            }}

          rel={
            {
              e: sc.info.rel.enquiries,
              t: sc.info.rel.tdrives,
              v: sc.info.rel.visits,
              b: sc.info.rel.bookings,
              r: sc.info.rel.retails,
              l: sc.info.rel.lost_drop,
              assigned:  sc.info.rel.enquiries,
              callback:  sc.info.rel.tdrives,
              prospect:  sc.info.rel.visits,
              sale_confirmed:  sc.info.rel.bookings,
              il_done:  sc.info.rel.retails,

            }}



          info={
            {
              dp_url: sc.info.dp_url,
              name: sc.info.name,
              user_id: sc.info.id,
              location_id: this.props.info.location_id,
              user_role_id: sc.info.user_role_id,
            }
          }

          targets={
            {
              e: sc.info.targets.enquiries,
              t: sc.info.targets.tdrives,
              v: sc.info.targets.visits,
              b: sc.info.targets.bookings,
              r: sc.info.targets.retails,
              l: sc.info.targets.lost_drop,
            }
          }

        ></SalesILomItemChild>)

    }

    if (this.props.showPercentage) {


      return (

        <View>
          <TouchableOpacity onPress={() => this.setState({ expand: !this.state.expand })}>
            <View style={{ marginTop: 10 }}>
              <View style={{ marginTop: 4, marginBottom: 4, flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ flex: 1, fontSize: 15, color: '#2D4F8B', }}>{this.props.info.name}</Text>
                <CallStat call_stats = {this.props.call_stats}/>
              </View>
              <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#eceff1', alignItems: 'center', borderRadius: 6 }}>


                <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
                  {placeHolder}
                  <TouchableOpacity
                    style={{ position: 'absolute', }}
                    onPress={() => this.setState({ expand: !this.state.expand })}
                    onLongPress={() => {
                      this.props.onLongPress('user_info',
                        { info: { name: this.props.info.name, dp_url: this.props.info.dp_url } })
                    }}>
                    <Image
                      source={imageSource}
                      style={{ width: 36, height: 36, borderRadius: 36 / 2, }}
                    />
                  </TouchableOpacity>
                </View>







                <View style={{
                  flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                  borderLeftWidth: 0.3
                }}>
                  <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#303030' }} >{this.props.rel.callback}</Text>
                  </View>


                </View>

                <View style={{
                  flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                  borderLeftWidth: 0.3
                }}>
                  <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#303030' }} >{this.props.rel.prospect}</Text>
                  </View>

                </View>

                <View style={{
                  flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                  borderLeftWidth: 0.3
                }}>
                  <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#303030' }} >{this.props.rel.sale_confirmed}</Text>
                  </View>

                </View>

                <View style={{
                  flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                  borderLeftWidth: 0.3
                }}>
                  <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: '#303030' }} >{this.props.rel.il_done}</Text>
                  </View>


                </View>

                <View style={{
                  flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                  borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
                }}>

                  <Text style={{ color: 'red' }} >{this.props.rel.l}</Text>
                </View>


              </View>
            </View>
          </TouchableOpacity>
          {
            this.state.expand && (
              <View>
                {sc_views}
              </View>
            )
          }

        </View>
      );

    }
    else {


      return (

        <View>
          <TouchableOpacity onPress={() => this.setState({ expand: !this.state.expand })}>
            <View style={{ marginTop: 10 }}>
              <View style={{ marginTop: 4, marginBottom: 4, flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ flex: 1, fontSize: 15, color: '#2D4F8B', }}>{this.props.info.name}</Text>
                <CallStat call_stats = {this.props.call_stats}/>
              </View>
              <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#eceff1', alignItems: 'center', borderRadius: 6 }}>

                <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
                  {placeHolder}
                  <TouchableOpacity
                    style={{ position: 'absolute', }}
                    onPress={() => this.setState({ expand: !this.state.expand })}
                    onLongPress={() => {
                      this.props.onLongPress('user_info',
                        { info: { name: this.props.info.name, dp_url: this.props.info.dp_url } })
                    }}>
                    <Image
                      source={imageSource}
                      style={{ width: 36, height: 36, borderRadius: 36 / 2, }}
                    />
                  </TouchableOpacity>
                </View>

                <View style={{
                  flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                  borderLeftWidth: 0.3
                }}>
                  <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                      hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                      onPress={() => this.setState({ expand: !this.state.expand })}
                      onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Assigned', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                      <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.assigned}</Text>
                    </TouchableOpacity>
                  </View>


                </View>




                <View style={{
                  flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                  borderLeftWidth: 0.3
                }}>
                  <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                      hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                      onPress={() => this.setState({ expand: !this.state.expand })}
                      onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Call back', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                      <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.callback}</Text>
                    </TouchableOpacity>
                  </View>

                </View>

                <View style={{
                  flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                  borderLeftWidth: 0.3
                }}>
                  <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                      hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                      onPress={() => this.setState({ expand: !this.state.expand })}
                      onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Prospect', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                      <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.prospect}</Text>
                    </TouchableOpacity>
                  </View>

                </View>

                <View style={{
                  flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                  borderLeftWidth: 0.3
                }}>
                  <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                      hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                      onPress={() => this.setState({ expand: !this.state.expand })}
                      onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Sale Confirmed', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                      <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.sale_confirmed}</Text>
                    </TouchableOpacity>
                  </View>

                </View>

                <View style={{
                  flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                  borderLeftWidth: 0.3
                }}>
                  <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                      hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                      onPress={() => this.setState({ expand: !this.state.expand })}
                      onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'IL Done', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                      <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.il_done}</Text>
                    </TouchableOpacity>
                  </View>

                </View>

                <View style={{
                  flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
                  borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
                }}>
                  <TouchableOpacity
                    hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                    onPress={() => this.setState({ expand: !this.state.expand })}
                    onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Lost / Not Eligible', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                    <Text style={{ color: 'red', textDecorationLine: 'underline' }} >{this.props.abs.l}</Text>
                  </TouchableOpacity>
                </View>


              </View>
            </View>
          </TouchableOpacity>

          {this.state.expand && (<View>{sc_views}</View>)}
        </View>
      );
    }
  }
}
