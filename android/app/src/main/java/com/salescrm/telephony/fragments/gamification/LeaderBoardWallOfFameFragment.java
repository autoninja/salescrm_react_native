package com.salescrm.telephony.fragments.gamification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.google.android.flexbox.FlexboxLayout;
import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.gamification.WallOfFameResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.services.service_handlers.GamificationServiceHandler;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import java.lang.reflect.Field;

public class LeaderBoardWallOfFameFragment extends Fragment {

    TextView tv_team_lead_icon, tv_team_wall_month, tv_team_wall_title, tv_team_name;
    TextView tv_dse_wall_month, tv_dse_wall_title;
    ImageView img_team_lead_icon;
    private BroadcastReceiver broadcastReceiver;
    int numberOfDsesPerRow = 4;
    FrameLayout sectionOne, sectionTwo;
    TextView sectionZero;
    Preferences pref;
    private FlexboxLayout flexTeamUsers, flexBestConsultant;
    //private SwipeRefreshLayout mSwipeRefreshLayout;
    private RelativeLayout relLoading;

    public LeaderBoardWallOfFameFragment() {
        // Required empty public constructor
    }

    public static LeaderBoardWallOfFameFragment newInstance(String param1, String param2) {
        LeaderBoardWallOfFameFragment fragment = new LeaderBoardWallOfFameFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStop() {
        super.onStop();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rView = inflater.inflate(R.layout.fragment_wall_of_fame, container, false);

        pref = Preferences.getInstance();
        pref.load(getActivity());

        flexTeamUsers = rView.findViewById(R.id.flex_wall_of_team_users);
        flexTeamUsers.setFlexWrap(FlexboxLayout.FLEX_WRAP_WRAP);

        flexBestConsultant = rView.findViewById(R.id.flex_wall_of_consultant);
        flexBestConsultant.setFlexWrap(FlexboxLayout.FLEX_WRAP_WRAP);

        tv_team_wall_month = (TextView) rView.findViewById(R.id.team_wall_month);
        tv_team_wall_title = (TextView) rView.findViewById(R.id.team_wall_txt);
        tv_team_lead_icon = (TextView) rView.findViewById(R.id.wall_of_fame_team_txt);
        img_team_lead_icon = (ImageView) rView.findViewById(R.id.wall_of_fame_team_img);
        tv_team_name = (TextView) rView.findViewById(R.id.team_team_name);
        tv_dse_wall_month = (TextView) rView.findViewById(R.id.wall_dse_month);
        tv_dse_wall_title = (TextView) rView.findViewById(R.id.wall_dse_txt);
        sectionOne = (FrameLayout) rView.findViewById(R.id.section_one);
        sectionTwo = (FrameLayout) rView.findViewById(R.id.section_two);
        sectionZero = (TextView) rView.findViewById(R.id.section_zero);
        relLoading = (RelativeLayout) rView.findViewById(R.id.gamification_rel_loading_frame);
        if (new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
            relLoading.setVisibility(View.VISIBLE);
            GamificationServiceHandler.getInstance
                    (getActivity()).fetchWallOfFameData(pref.getAccessToken(), pref.getmGameLocationId(), Util.getMonthYear(pref.getSelectedGameDate()));
        } else {
            sectionZero.setVisibility(View.VISIBLE);
            sectionOne.setVisibility(View.GONE);
            sectionTwo.setVisibility(View.GONE);
            sectionZero.setText("Please Enable Internet Connection");
            Toast.makeText(getActivity(), "Please Enable Internet Connection", Toast.LENGTH_LONG).show();
        }
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle args = intent.getBundleExtra("DATA");
                int homeActivity = args.getInt("homeActivity");
                if (homeActivity == 1) {
                    if (new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
                        relLoading.setVisibility(View.VISIBLE);
                        sectionZero.setVisibility(View.GONE);
                        sectionOne.setVisibility(View.GONE);
                        sectionTwo.setVisibility(View.GONE);
                        GamificationServiceHandler.getInstance(getActivity())
                                .fetchWallOfFameData(pref.getAccessToken(), pref.getmGameLocationId(), Util.getMonthYear(pref.getSelectedGameDate()));
                    } else {
                        sectionZero.setVisibility(View.VISIBLE);
                        sectionOne.setVisibility(View.GONE);
                        sectionTwo.setVisibility(View.GONE);
                        sectionZero.setText("Please Enable Internet Connection");
                        Toast.makeText(getActivity(), "Please Enable Internet Connection", Toast.LENGTH_LONG).show();
                    }

                }
            }
        };
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(WSConstants.wallOfFameDataFetchedIntent));
        return rView;

    }

    @Subscribe
    public void wallOfFameOttoNotify(final WallOfFameResponse wallOfFameResponse) {
        relLoading.setVisibility(View.GONE);
        WallOfFameResponse realResponse = wallOfFameResponse.getWallOfFameResponse();
        //realResponse = null;
        if (realResponse != null && realResponse.getResult() != null) {
            if(realResponse.getResult().getBest_consultant() != null) {
                addBestConsultantUsers(realResponse.getResult().getBest_consultant());
            }
            else {
                sectionOne.setVisibility(View.GONE);
            }

            if(realResponse.getResult().getBest_team() != null) {
                addTeamUsers(realResponse.getResult().getBest_team());
            }

            else {
                sectionTwo.setVisibility(View.GONE);
            }


            sectionZero.setVisibility(View.GONE);
        } else {
            sectionZero.setVisibility(View.VISIBLE);
            sectionOne.setVisibility(View.GONE);
            sectionTwo.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "Coming Soon", Toast.LENGTH_LONG).show();
        }

    }

    private void addTeamUsers(WallOfFameResponse.Best_team best_team) {
        tv_team_wall_month.setText(best_team.getMonth());
        tv_team_wall_title.setText(best_team.getTitle());
        tv_team_name.setText(String.format("Team\n%s", best_team.getTeam_leader_name()));
        tv_team_lead_icon.setText(best_team.getTeam_leader_name().toUpperCase().substring(0, 1));
        setImage(best_team.getTeam_leader_photo_url(), tv_team_lead_icon, img_team_lead_icon);
        flexTeamUsers.removeAllViews();
        for (int i = 0; i < (best_team.getTeam_users()==null?0:best_team.getTeam_users().size()); i++) {
            WallOfFameResponse.Team_users teamUserData = best_team.getTeam_users().get(i);
            final View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_wall_of_fame_user, null, false);
            final TextView txtImgDse = view.findViewById(R.id.wall_of_fame_team_lead_txt);
            final ImageView imgDse = view.findViewById(R.id.wall_of_fame_team_lead_img);
            txtImgDse.setText(teamUserData.getUser_name().toUpperCase().substring(0, 1));
            setImage(teamUserData.getUser_photo_url(), txtImgDse, imgDse);
            flexTeamUsers.addView(view);
        }


    }

    private void addBestConsultantUsers(WallOfFameResponse.Best_consultant best_consultant) {
        tv_dse_wall_title.setText(best_consultant.getTitle());
        tv_dse_wall_month.setText(best_consultant.getMonth());
        flexBestConsultant.removeAllViews();
        for (int i = 0; i < best_consultant.getBest_dses().size(); i++) {
            WallOfFameResponse.Best_dses bestTeam = best_consultant.getBest_dses().get(i);

            final View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_wall_of_best_consultant, null, false);

            final TextView txtImgDse = view.findViewById(R.id.tv_wall_of_fame_consultant_txt);
            final ImageView imgDse = view.findViewById(R.id.img_wall_of_fame_consultant);
            final TextView tvCategory = view.findViewById(R.id.tv_wall_of_fame_consultant_category);
            final TextView tvUserName = view.findViewById(R.id.tv_wall_of_fame_consultant_name);


            txtImgDse.setText(bestTeam.getName().toUpperCase().substring(0, 1));
            setImage(bestTeam.getPhoto_url(), txtImgDse, imgDse);
            tvCategory.setText(bestTeam.getCategory());
            tvUserName.setText(bestTeam.getName());
            flexBestConsultant.addView(view);
        }


    }

    private void setImage(String photoUrl, final TextView txtImgDse, final ImageView imgDse) {
        if (Util.isNotNull(photoUrl)) {
            Glide.with(this).
                    load(photoUrl)
                    .asBitmap()
                    .centerCrop()
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                            imgDse.setVisibility(View.GONE);
                            txtImgDse.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            imgDse.setVisibility(View.VISIBLE);
                            txtImgDse.setVisibility(View.INVISIBLE);
                            return false;
                        }
                    })
                    .into(new BitmapImageViewTarget(imgDse) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            if (resource != null) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                imgDse.setImageDrawable(circularBitmapDrawable);
                            }
                        }
                    });
        } else {
            imgDse.setVisibility(View.GONE);
            txtImgDse.setVisibility(View.INVISIBLE);

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }


}
