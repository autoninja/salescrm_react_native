import Utils from '../../../utils/Utils'
import { BOOKING_STAGES } from '../BookingUtils/BookingUtils'

let getUserSelectedBookingDetails = (formData, stageId, action) => {
    let userSelectedValue = {
        invoice_details: {
            invoice_number: '',
            invoice_name: '',
            invoice_address: '',
            invoice_amount: '0',
            invoice_date: '',
            invoice_mob_no: '',
            vin_no: ''
        },
        delivery_details: {
            delivery_date: "",
            delivery_number: "",
            invoice_id: "",
            invoice_mob_no: "",
            invoice_name: "",
            invoice_number: "",
            vin_no: ""
        }
    };
    let bookingDetails = [];
    formData.map((sections) => {
        let car_details = {
            quantity: 1
        };
        let customer_details = {

        };
        let price_breakup = {};
        let discounts = {};
        let payment_details = {};
        let vin_allocation_details = {}
        sections.map((section) => {
            section.items.map((item) => {
                if (item.value) {
                    if (item.server_id) {
                        let value;
                        switch (item.view) {
                            case "picker":
                                value = item.value.id;
                                break;
                            case "edit_text":
                            case "date_picker":
                            case "radio":
                                value = item.value;
                                break;
                        }
                        switch (section.id) {
                            case "vehicle":
                                if (item.server_id == "color_id") {
                                    car_details["fuel_type_id"] = item.value.payload.fuel_type_id;
                                }
                                car_details[item.server_id] = value;
                                break;
                            case "customer":
                                customer_details[item.server_id] = value;
                                break;
                            case "price_breakup":
                                price_breakup[item.server_id] = value;
                                break;
                            case "discount":
                                discounts[item.server_id] = value;
                                break;
                            case "exchange_finance":
                                payment_details[item.server_id] = value;
                                break;
                        }
                    }
                    else {
                        if (item.key == "booking_amount") {
                            price_breakup[item.key] = item.value;
                        }
                        else if (item.key == "allocation") {
                            vin_allocation_details["vin_allocation_status_id"] = item.value.id;
                            vin_allocation_details["vin_allocation_status"] = item.value.text;
                        }
                        else if (item.key == "allocation_vin_number") {
                            vin_allocation_details["vin_no"] = item.value.id;
                        }
                        else if (item.key == "invoice_date"
                            || item.key == "invoice_mob_no"
                            || item.key == "vin_no") {
                            userSelectedValue.invoice_details[item.key] = item.value;
                        }
                        else if (item.key == "delivery_date"
                            || item.key == "delivery_number") {
                            userSelectedValue.delivery_details[item.key] = item.value;
                        }

                    }
                }
            })
        })
        let vin_allocation_status_id = vin_allocation_details ?
            vin_allocation_details.vin_allocation_status_id : null
        bookingDetails.push({
            car_details,
            customer_details,
            price_breakup,
            discounts,
            payment_details,
            vin_allocation_details,
            vin_allocation_status_id
        })
    });
    if (stageId == BOOKING_STAGES.STAGE_INIT && action == "update_booking") {
        userSelectedValue.booking_details = bookingDetails;
    }
    else {
        userSelectedValue.booking_details = bookingDetails[0];
    }
    return userSelectedValue;
}
let ServerModel = {
    getSeverData: (formData, info, followupInfo) => {
        let { c360Information, stageId, leadCarId, action, booking_id } = info;
        let { booking_details, invoice_details, delivery_details } = getUserSelectedBookingDetails(formData, stageId, action);
        let serverData = {
            booking_details
        }
        let dmsData;
        if (c360Information) {
            if (c360Information.interestedCarsDetails) {
                c360Information.interestedCarsDetails.map((interestedCar) => {
                    if (interestedCar.lead_car_id == leadCarId) {
                        if (interestedCar.carDmsData && interestedCar.carDmsData.length > 0) {
                            dmsData = interestedCar.carDmsData.filter((dms => dms.booking_id == booking_id))[0];
                        }
                    }
                });
            }
        }
        if (action == "update_booking") {
            serverData.location_id = c360Information.customerDetails.lead_location.id;
            serverData.lead_source_id = c360Information.customerDetails.lead_source_id;
            serverData.lead_last_updated = c360Information.customerDetails.lead_last_updated;
            serverData.lead_id = c360Information.customerDetails.lead_id;
            serverData.lead_car_stage_id = stageId;
            serverData.lead_car_id = leadCarId;
            switch (parseInt(stageId)) {
                case BOOKING_STAGES.STAGE_INIT:
                    //booking stage;
                    delete serverData["lead_car_id"];
                    break;
                case BOOKING_STAGES.STAGE_BOOKED:
                    //fullPayment
                    if (dmsData) {
                        serverData.booking_no = dmsData.booking_number;
                        serverData.booking_id = dmsData.booking_id;
                        serverData.enquiry_id = dmsData.enquiry_id;
                        if (!serverData.booking_details.vin_allocation_status_id) {
                            serverData.booking_details.vin_allocation_details = dmsData.booking_details.vin_allocation_details;
                            serverData.booking_details.vin_allocation_status_id = dmsData.booking_details.vin_allocation_status_id;
                        }
                    }
                    break;
                case BOOKING_STAGES.STAGE_FULL_PAYMENT_RECEIVED:
                    //invoicing
                    delete serverData["lead_source_id"];
                    if (dmsData) {
                        serverData.booking_id = dmsData.booking_id;
                        serverData.booking_number = dmsData.booking_number;
                        serverData.enquiry_id = dmsData.enquiry_id;
                        serverData.expected_delivery_date = dmsData.expected_delivery_date;

                        //vin allocation details
                        if (!serverData.booking_details.vin_allocation_status_id) {
                            serverData.booking_details.vin_allocation_details = dmsData.booking_details.vin_allocation_details;
                            serverData.booking_details.vin_allocation_status_id = dmsData.booking_details.vin_allocation_status_id;
                        }
                        if (!serverData.booking_details.payment_details.exchanged) {
                            serverData.booking_details.payment_details.exchanged = "0";
                        }

                    }
                    serverData.invoice_details = invoice_details;
                    break;
                case BOOKING_STAGES.STAGE_INVOICED:
                    //delivery
                    serverData.delivery_details = delivery_details;
                    delete serverData["lead_source_id"];

                    if (dmsData) {

                        serverData.booking_id = dmsData.booking_id;
                        serverData.booking_number = dmsData.booking_number;
                        serverData.enquiry_id = dmsData.enquiry_id;

                        serverData.delivery_details.invoice_id = dmsData.invoice_id;
                        serverData.delivery_details.invoice_mob_no = dmsData.invoice_mob_no;
                        serverData.delivery_details.invoice_name = dmsData.invoice_name;
                        serverData.delivery_details.invoice_number = dmsData.invoice_number;
                        serverData.delivery_details.vin_no = dmsData.invoice_vin_no;

                        //vin allocation details
                        //vin allocation details
                        if (!serverData.booking_details.vin_allocation_status_id) {
                            serverData.booking_details.vin_allocation_details = dmsData.booking_details.vin_allocation_details;
                            serverData.booking_details.vin_allocation_status_id = dmsData.booking_details.vin_allocation_status_id;
                        }
                        if (!serverData.booking_details.payment_details.exchanged) {
                            serverData.booking_details.payment_details.exchanged = "0";
                        }
                    }
                    break;
                case BOOKING_STAGES.STAGE_DELIVERED:
                    //this won't come;
                    stageId = BOOKING_STAGES.STAGE_FULL_PAYMENT_RECEIVED;
                    break;
            }
        }
        else if (action == "details_booking") {
            serverData.location_id = c360Information.customerDetails.lead_location.id;
            serverData.lead_source_id = c360Information.customerDetails.lead_source_id;
            serverData.lead_last_updated = c360Information.customerDetails.lead_last_updated;
            serverData.lead_id = c360Information.customerDetails.lead_id;

            serverData.lead_car_id = leadCarId;
            if (dmsData) {
                serverData.booking_id = dmsData.booking_id;
                serverData.enquiry_id = dmsData.enquiry_id;
                if (!serverData.booking_details.vin_allocation_status_id) {
                    serverData.booking_details.vin_allocation_details = dmsData.booking_details.vin_allocation_details;
                    serverData.booking_details.vin_allocation_status_id = dmsData.booking_details.vin_allocation_status_id;
                }
            }


        }
        return serverData;
    }
}
module.exports = ServerModel 