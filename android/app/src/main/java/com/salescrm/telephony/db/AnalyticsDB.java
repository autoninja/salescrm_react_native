package com.salescrm.telephony.db;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by bannhi on 25/9/17.
 */

public class AnalyticsDB  extends RealmObject{

    private int locId;
    private String locName;
    private int todayCount;
    private int lastWkCount;
    private RealmList<WeeklyData> weeklyData;

    public int getLocId() {
        return locId;
    }

    public void setLocId(int locId) {
        this.locId = locId;
    }

    public String getLocName() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName = locName;
    }

    public int getTodayCount() {
        return todayCount;
    }

    public void setTodayCount(int todayCount) {
        this.todayCount = todayCount;
    }

    public int getLastWkCount() {
        return lastWkCount;
    }

    public void setLastWkCount(int lastWkCount) {
        this.lastWkCount = lastWkCount;
    }

    public RealmList<WeeklyData> getWeeklyData() {
        return weeklyData;
    }

    public void setWeeklyData(RealmList<WeeklyData> weeklyData) {
        this.weeklyData = weeklyData;
    }
}
