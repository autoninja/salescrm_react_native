import { UPDATE_TEAM_DASHBOARD } from '../actions/team_dashboard_types';

const initialState = {
  update_etvbr: false,
  update_live_ef: false
};

const teamDashboardFiltersReducer = (state = initialState, action) => {
  switch(action.type) {
    case UPDATE_TEAM_DASHBOARD:
      return {
        ...state,
        update_etvbr: action.payload.update_etvbr,
        update_live_ef: action.payload.update_live_ef
      };
    default:
      return state;
  }
}

export default teamDashboardFiltersReducer;
