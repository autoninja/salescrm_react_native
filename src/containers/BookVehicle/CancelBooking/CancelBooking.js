import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import NavigationService from '../../../navigation/NavigationService';
import { BOOKING_ROUTES } from '../BookingUtils/BookingUtils'
import RadioButton from '../../../components/uielements/RadioButton';
import RestClient from '../../../network/rest_client'
import Loader from '../../../components/uielements/Loader'
import Toast, { DURATION } from "react-native-easy-toast";
let styles = StyleSheet.create({
    linearGradient: {
        padding: 16,
        paddingBottom: 24,
        borderRadius: 10,
        backgroundColor: "#fff",
        borderRadius: 5,

    },
    main: {
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 80,
        paddingBottom: 80,
        flex: 1,
        justifyContent: 'center'

    },
    header: {
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        padding: 6,
        color: "#303030",
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    item: {
        marginTop: 14,
        color: "#7886C6",
        fontSize: 16,
        textAlign: 'center',
    },
    mainButton: {
        margin: 4,
        flex: 1, paddingTop: 12,
        paddingBottom: 12,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    mainActionText: {
        textAlign: 'center',
        color: '#fff', fontSize: 16,
        flexWrap: 'wrap', textTransform: 'uppercase'
    },

});

export default class CancelBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            requireRefreshC360: false,
            loading: false,
            selectedReason: null,
            reasons: [
                {
                    key: 'model_change',
                    title: 'Model Change',
                },
                {
                    key: 'plan_postponed',
                    title: 'Plan Postponed',
                },
                {
                    key: 'issue_in_price',
                    title: 'Issue In Price',
                }
            ]
        }
    }
    showAlert(msg) {
        this.refs.toast.show(msg);
    }
    onApiError(msg) {
        this.setState({ loading: false });
        this.showAlert(msg);
    }

    onConfirm() {
        let { selectedReason } = this.state;

        this.setState({ loading: true, requireRefreshC360: true });
        let { c360Information, stageId, leadCarId, booking_id } = this.props.info;

        let data = {
            booking_id: '',
            lead_car_id: leadCarId,
            location_id: '',
            enquiry_id: '',
            lead_id: '',
            lead_car_stage_id: stageId,
            cancel_reason: selectedReason.title,
            lead_last_updated: ''
        }
        if (c360Information) {
            if (c360Information.customerDetails) {
                data.location_id = c360Information.customerDetails.lead_location.id;
                data.lead_id = parseInt(c360Information.customerDetails.lead_id);
                data.lead_last_updated = c360Information.customerDetails.lead_last_updated;
            }
            if (c360Information.interestedCarsDetails) {
                c360Information.interestedCarsDetails.map((interestedCar) => {
                    if (interestedCar.lead_car_id == leadCarId) {
                        if (interestedCar.carDmsData && interestedCar.carDmsData.length > 0) {
                            let dmsData = interestedCar.carDmsData.filter((dms => dms.booking_id == booking_id))[0];
                            data.booking_id = dmsData.booking_id;
                            data.enquiry_id = dmsData.enquiry_id;
                        }
                    }
                });
            }
        }

        new RestClient().cancelBooking(data).then((data) => {
            if (data
                && data.statusCode == "2002"
                && data.result) {
                this.setState({
                    loading: false
                })
                this.showAlert("Booking Cancelled");
                this.props.callbacks.goBack();
                this.props.callbacks.onApiSubmission();
            }
            else if (data && data.message) {
                this.onApiError(data.message);
            }
            else {
                this.onApiError("Error cancelling booking");
            }
        }).
            catch((error) => {
                this.onApiError("Error cancelling booking");
            })
    }


    render() {
        let { selectedReason, reasons, loading, requireRefreshC360 } = this.state;
        return (
            <View style={styles.main}>
                <View
                    style={styles.linearGradient}>
                    <View style={{ flexWrap: 'wrap' }}>
                        <Text style={styles.title}>Select Reason to Cancel</Text>
                        {reasons.map((reason) => {
                            return (
                                <RadioButton
                                    key={reason.key}
                                    selected={reason.key == (selectedReason ? selectedReason.key : null)}
                                    style={{ marginTop: 10, marginStart: 20, }}
                                    title={reason.title}
                                    onClick={() => {
                                        this.setState({ selectedReason: reason });
                                    }}
                                />
                            )
                        })
                        }

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 16,
                        }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.callbacks.goBack();
                                    if (requireRefreshC360) {
                                        this.props.callbacks.onApiSubmission();
                                    }
                                }}
                                style={[
                                    styles.mainButton,
                                    {
                                        backgroundColor: '#808080',
                                    }
                                ]}
                            >
                                <Text style={styles.mainActionText}>Close</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                disabled={selectedReason ? false : true}
                                onPress={() => this.onConfirm()}
                                style={[
                                    styles.mainButton,
                                    {
                                        backgroundColor: selectedReason ? '#007FFF' : '#cfcfcf',
                                    }]}
                            >
                                <Text style={styles.mainActionText}>Save</Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                </View>
                {loading && <Loader isLoading={loading} />}
                <Toast
                    ref="toast"
                    style={{ backgroundColor: "#808080" }}
                    position="top"
                    fadeInDuration={750}
                    fadeOutDuration={750}
                    opacity={0.8}
                    textStyle={{ color: "white" }}
                />

            </View>
        )
    }
}