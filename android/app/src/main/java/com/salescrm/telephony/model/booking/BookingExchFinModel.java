package com.salescrm.telephony.model.booking;

public class BookingExchFinModel {

    private String finance_type;
    private String exchanged;

    public String getFinance() {
        return finance_type;
    }

    public void setFinance(String finance) {
        this.finance_type = finance;
    }

    public String getExchange() {
        return exchanged;
    }

    public void setExchange(String exchange) {
        this.exchanged = exchange;
    }
}
