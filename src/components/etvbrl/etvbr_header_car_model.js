import React, { Component } from 'react'
import { View, Text, } from 'react-native'

export default class ETVBRHeaderCarModel extends Component {

  render() {
    if(this.props.showPercentage) {
      return(
        <View style={{ flexDirection: 'row',
        height:36,
        backgroundColor:'#5E7692', marginTop:10, alignItems:'center', borderRadius:6}}>

        <View style={{flex:1.5}} />

        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
          borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
            <Text style={{ color:'#fff',  textAlign:'center'}} >E%</Text>
        </View>
        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
            borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
              <Text style={{ color:'#fff',  textAlign:'center'}} >T%</Text>
        </View>

        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
              <Text style={{ color:'#fff',  textAlign:'center'}} >V%</Text>
        </View>

        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
                borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                  <Text style={{ color:'#fff',  textAlign:'center'}} >B%</Text>
        </View>
        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
                  borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                    <Text style={{ color:'#fff',  textAlign:'center'}} >R%</Text>
        </View>
        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
                    borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                      <Text style={{ color:'#fff',  textAlign:'center'}} >L%</Text>
        </View>

        </View>
      )
    }
    else {
    return(
      <View style={{ flexDirection: 'row',
      height:36,
      backgroundColor:'#5E7692', marginTop:10, alignItems:'center', borderRadius:6}}>

      <View style={{flex:1.5}} />
      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
          <Text style={{ color:'#fff',  textAlign:'center'}} >E</Text>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
          borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
            <Text style={{ color:'#fff',  textAlign:'center'}} >T</Text>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
      borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
            <Text style={{ color:'#fff',  textAlign:'center'}} >V</Text>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
              borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                <Text style={{ color:'#fff',  textAlign:'center'}} >B</Text>
      </View>
      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
                borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                  <Text style={{ color:'#fff',  textAlign:'center'}} >R</Text>
      </View>
      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
                  borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                    <Text style={{ color:'#fff',  textAlign:'center'}} >L</Text>
      </View>

      </View>
    )
  }
  }
}
