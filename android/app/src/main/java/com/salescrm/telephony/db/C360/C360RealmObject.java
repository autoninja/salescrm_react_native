package com.salescrm.telephony.db.C360;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bannhi on 4/5/17.
 */

public class C360RealmObject extends RealmObject {

    @PrimaryKey
    private int leadId;
    private String gender;
    private String title;
    private String firstName;
    private String lastName;
    private String age;
    private String buyerType;
    private String modeOfPayment;
    private String expectedClosingDate;
    private String typeOfCustomerId;
    private String locationName;
    private String locationId;
    private String dseName;
    private String dseId;
    private String crmSource;
    private String crmId;
    private String residenceAddress;
    private String residencePin;
    private String officeAddress;
    private String officePin;
    private String occupation;
    private String designation;
    private String companyName;
    private String leadLastUpdated;
    private String enqSrcId;
    private String buyerTypeId;

    private RealmList<MobileNumbers> mobileNumbers = new RealmList<>();
    private RealmList<EmailIds> emailIds = new RealmList<>();
  //  private boolean synced;
    private boolean crmSourceSynced = true;
    private boolean buyerTypeSynced = true;
    private boolean customerDetailsSynced= true;
    private boolean mobileSync;
    private boolean emailSync;

   // private boolean createdOffline;

    public int getLeadId() {
        return leadId;
    }

    public void setLeadId(int leadId) {
        this.leadId = leadId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBuyerType() {
        return buyerType;
    }

    public void setBuyerType(String buyerType) {
        this.buyerType = buyerType;
    }

    public String getModeOfPayment() {
        return modeOfPayment;
    }

    public void setModeOfPayment(String modeOfPayment) {
        this.modeOfPayment = modeOfPayment;
    }

    public String getExpectedClosingDate() {
        return expectedClosingDate;
    }

    public void setExpectedClosingDate(String expectedClosingDate) {
        this.expectedClosingDate = expectedClosingDate;
    }

    public String getTypeOfCustomerId() {
        return typeOfCustomerId;
    }

    public void setTypeOfCustomerId(String typeOfCustomerId) {
        this.typeOfCustomerId = typeOfCustomerId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getDseName() {
        return dseName;
    }

    public void setDseName(String dseName) {
        this.dseName = dseName;
    }

    public String getDseId() {
        return dseId;
    }

    public void setDseId(String dseId) {
        this.dseId = dseId;
    }

    public String getCrmSource() {
        return crmSource;
    }

    public void setCrmSource(String crmSource) {
        this.crmSource = crmSource;
    }

    public String getCrmId() {
        return crmId;
    }

    public void setCrmId(String crmId) {
        this.crmId = crmId;
    }

    /*public boolean isSynced() {
        return synced;
    }*/

   /* public void setSynced(boolean synced) {
        this.synced = synced;
    }
*/
   /* public boolean isCreatedOffline() {
        return createdOffline;
    }

    public void setCreatedOffline(boolean createdOffline) {
        this.createdOffline = createdOffline;
    }*/

    public String getResidenceAddress() {
        return residenceAddress;
    }

    public void setResidenceAddress(String residenceAddress) {
        this.residenceAddress = residenceAddress;
    }

    public String getResidencePin() {
        return residencePin;
    }

    public void setResidencePin(String residencePin) {
        this.residencePin = residencePin;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public String getOfficePin() {
        return officePin;
    }

    public void setOfficePin(String officePin) {
        this.officePin = officePin;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public RealmList<MobileNumbers> getMobileNumbers() {
        return mobileNumbers;
    }

    public void setMobileNumbers(RealmList<MobileNumbers> mobileNumbers) {
        this.mobileNumbers = mobileNumbers;
    }

    public RealmList<EmailIds> getEmailIds() {
        return emailIds;
    }

    public void setEmailIds(RealmList<EmailIds> emailIds) {
        this.emailIds = emailIds;
    }

    public boolean isCrmSourceSynced() {
        return crmSourceSynced;
    }

    public void setCrmSourceSynced(boolean crmSourceSynced) {
        this.crmSourceSynced = crmSourceSynced;
    }

    public boolean isBuyerTypeSynced() {
        return buyerTypeSynced;
    }

    public void setBuyerTypeSynced(boolean buyerTypeSynced) {
        this.buyerTypeSynced = buyerTypeSynced;
    }

    public boolean isCustomerDetailsSynced() {
        return customerDetailsSynced;
    }

    public void setCustomerDetailsSynced(boolean customerDetailsSynced) {
        this.customerDetailsSynced = customerDetailsSynced;
    }

    public boolean isMobileSync() {
        return mobileSync;
    }

    public void setMobileSync(boolean mobileSync) {
        this.mobileSync = mobileSync;
    }

    public boolean isEmailSync() {
        return emailSync;
    }

    public void setEmailSync(boolean emailSync) {
        this.emailSync = emailSync;
    }

    public String getLeadLastUpdated() {
        return leadLastUpdated;
    }

    public void setLeadLastUpdated(String leadLastUpdated) {
        this.leadLastUpdated = leadLastUpdated;
    }

    public String getEnqSrcId() {
        return enqSrcId;
    }

    public void setEnqSrcId(String enqSrcId) {
        this.enqSrcId = enqSrcId;
    }

    public String getBuyerTypeId() {
        return buyerTypeId;
    }

    public void setBuyerTypeId(String buyerTypeId) {
        this.buyerTypeId = buyerTypeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
