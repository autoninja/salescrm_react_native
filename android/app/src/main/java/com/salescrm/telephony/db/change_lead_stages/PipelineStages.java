package com.salescrm.telephony.db.change_lead_stages;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 23/12/16.
 */
public class PipelineStages extends RealmObject {


    private String pipeline_id;

    private String pipeline_name;

    public RealmList<Stages> stages = new RealmList<>();

    public RealmList<Stages> getStages() {
        return stages;
    }

    public void setStages(RealmList<Stages> stages) {
        this.stages = stages;
    }

    public String getPipeline_name() {
        return pipeline_name;
    }

    public void setPipeline_name(String pipeline_name) {
        this.pipeline_name = pipeline_name;
    }

    public String getPipeline_id() {
        return pipeline_id;
    }

    public void setPipeline_id(String pipeline_id) {
        this.pipeline_id = pipeline_id;
    }
}
