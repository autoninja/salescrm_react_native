package com.salescrm.telephony.utils;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;

/**
 * Created by nndra on 07-Nov-17.
 */

public class ApiErrorHandler implements ErrorHandler {

    @Override
    public Throwable handleError(RetrofitError cause) {

        if(cause.getResponse() != null) {
            System.out.println("ApiErrorHandler - " + cause.getResponse().getStatus());

            if (cause.getResponse().getStatus() == 401) {
                //ApiUtil.InvalidUserLogout(HomeActivity.this,0);
                System.out.println("ApiErrorHandler - " + cause.getResponse().getStatus());
            }
        }

        return cause;
    }
}