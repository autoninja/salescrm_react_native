package com.salescrm.telephony.activity.gamification;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.gamification.GamificationTeamProfileAdapter;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.gamification.GamificationTeamProfileResponse;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GamificationTeamLeaderProfileActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RelativeLayout relLoading,back;
    private Preferences pref;
    private GamificationTeamProfileAdapter gamificationTeamProfileAdapter;
    private String leadId,avgPoints;
    private int calledFrom = 0;
    private TextView txtUserNameInitials,tv_team_pts,txtLeadName;
    private ImageView imgProfile;
    private String userName ="";
    private String profileUrl="";

    // private LinearLayout llGamificationRecyclerFooter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pref = Preferences.getInstance();
        pref.load(this);
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            leadId = extras.getString("team_leader_id");
            calledFrom = extras.getInt("called_from", 0);
            avgPoints = extras.getString("POINTS");
            userName = extras.getString("USER_NAME");
            profileUrl = extras.getString("PROFILE_URL");
        }


        System.out.println("Profile - "+leadId +" - "+calledFrom);

        if(calledFrom == 1) {
            setContentView(R.layout.gamification_team_profile);

            recyclerView = (RecyclerView) findViewById(R.id.recycler_gamification_team_profile);
            relLoading = (RelativeLayout) findViewById(R.id.rel_loading_frame);
            back = (RelativeLayout) findViewById(R.id.team_gamification_img_back);
            tv_team_pts = (TextView) findViewById(R.id.txt_team_pts);
            txtLeadName = (TextView) findViewById(R.id.txt_gamification_team_profile_lead_name);
            txtUserNameInitials = (TextView) findViewById(R.id.team_gamification_text_user);
            imgProfile = (ImageView) findViewById(R.id.team_gamification_img_user);
           // llGamificationRecyclerFooter = (LinearLayout) findViewById(R.id.ll_gamification_recycler_footer);


            relLoading.setVisibility(View.VISIBLE);

            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        finishAfterTransition();
                    }
                    else {
                        finish();
                    }
                }
            });
            init();
            makeTeamProfileApi();
        }else{

        }






    }

    private void init() {
        changeStatusBarColor(Color.parseColor("#FF0F2B51"));
        tv_team_pts.setText(avgPoints);
        txtLeadName.setText(userName);
        txtUserNameInitials.setText(userName.toUpperCase().substring(0, 1));
        System.out.println("profile URL:"+profileUrl);
        if(Util.isNotNull(profileUrl)) {
            Glide.with(this).
                    load(profileUrl)
                    .asBitmap()
                    .centerCrop()
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                            imgProfile.setVisibility(View.GONE);
                            txtUserNameInitials.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            imgProfile.setVisibility(View.VISIBLE);
                            txtUserNameInitials.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(new BitmapImageViewTarget(imgProfile) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            if(resource!=null ) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                imgProfile.setImageDrawable(circularBitmapDrawable);
                            }
                        }
                    });
        }
        else {
            imgProfile.setVisibility(View.GONE);
            txtUserNameInitials.setVisibility(View.VISIBLE);

        }



    }

    private void makeTeamProfileApi() {

        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GamificationTeamProfile(leadId, Util.getMonthYear(pref.getSelectedGameDate()), new Callback<GamificationTeamProfileResponse>() {
            @Override
            public void success(GamificationTeamProfileResponse gamificationTeamProfileResponse, Response response) {

                if(gamificationTeamProfileResponse.getResult() != null)
                callAdapter(gamificationTeamProfileResponse.getResult());

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    private void callAdapter(GamificationTeamProfileResponse.Result result) {

        /*if (result.getTeam_leader_photo_url() != null && !result.getTeam_leader_photo_url().isEmpty()) {
            imgProfile.setVisibility(View.VISIBLE);
            txtUserNameInitials.setVisibility(View.GONE);
            Picasso.with(this).load(result.getTeam_leader_photo_url())
                    .placeholder(R.drawable.ic_person_white_48dp)
                    .error(R.drawable.ic_person_white_48dp)
                    .transform(new CircleTransform())
                    .into(imgProfile);
        } else {
            imgProfile.setVisibility(View.GONE);
            txtUserNameInitials.setVisibility(View.VISIBLE);
            txtUserNameInitials.setText(""+result.getTeam_leader_name().toUpperCase().substring(0, 1));
        }*/
        tv_team_pts.setText(avgPoints);
        System.out.println("Profile - "+result.getTeam_leader_name());
        txtLeadName.setText(result.getTeam_leader_name());


        relLoading.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        gamificationTeamProfileAdapter = new GamificationTeamProfileAdapter(GamificationTeamLeaderProfileActivity.this,result.getPoints(),avgPoints );
        recyclerView.setAdapter(gamificationTeamProfileAdapter);

    }

    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(color);
        }
    }



}
