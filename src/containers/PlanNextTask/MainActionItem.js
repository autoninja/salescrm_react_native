import React, { Component } from "react";
import { TouchableOpacity, Image, Text } from 'react-native'

export default class MainActionItem extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        let pntModel = this.props.pntModel;
        return (
            <TouchableOpacity
                onPress={() => this.props.onPress()}
                style={[{
                    flexWrap: 'wrap',
                    marginStart: 10,
                    marginEnd: 10,
                    borderRadius: 8,
                    backgroundColor: '#4C6794',
                    paddingTop: 16,
                    paddingBottom: 16,
                    justifyContent: 'center',
                    alignItems: 'center',
                    flexDirection: 'row'
                }]}>
                <Image
                    source={pntModel.image}
                    style={{ width: 36, height: 36 }}
                    resizeMode={"contain"}
                />
                <Text style={{ color: '#fff', fontSize: 16, marginStart: 20, flexWrap: 'wrap' }}>{pntModel.title}</Text>
            </TouchableOpacity>
        )
    }
}