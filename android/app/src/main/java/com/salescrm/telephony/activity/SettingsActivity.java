package com.salescrm.telephony.activity;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.common.LifecycleState;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.shell.MainReactPackage;
import com.clevertap.react.CleverTapPackage;
import com.google.gson.Gson;
import com.horcrux.svg.SvgPackage;
import com.microsoft.appcenter.reactnative.analytics.AppCenterReactNativeAnalyticsPackage;
import com.microsoft.appcenter.reactnative.appcenter.AppCenterReactNativePackage;
import com.microsoft.codepush.react.CodePush;
import com.oblador.vectoricons.VectorIconsPackage;
import com.salescrm.telephony.BuildConfig;
import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.reactNative.ReactNativeToAndroidPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.zmxv.RNSound.RNSoundPackage;

public class SettingsActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {
    private ReactRootView mReactRootView;
    private ReactInstanceManager mReactInstanceManager;
    private Preferences pref;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pref = Preferences.getInstance();
        pref.load(this);

        setContentView(R.layout.activity_settings);
        setSupportActionBar((Toolbar) findViewById(R.id.app_toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("SMS / Email");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();
        Bundle initialProps = new Bundle();
        initialProps.putBoolean("fromAndroid", true);
        initialProps.putString("token",pref.getAccessToken());
        if(getIntent()!=null && getIntent().getExtras()!=null && getIntent().getExtras().getString("MODULE")!=null) {
            initialProps.putString("module", getIntent().getExtras().getString("MODULE"));
        }
        mReactRootView = findViewById(R.id.react_root_view_settings);
        mReactRootView.startReactApplication(
                mReactInstanceManager,
                "NinjaCRMSales",
                initialProps
        );

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause(this);
        }

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }




    @Override
    protected void onResume() {
        super.onResume();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
    }


    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
