package com.salescrm.telephony.adapter.RefAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.DetailsOfEtvbr;
import com.salescrm.telephony.activity.EtvbrImgDialog;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.db.etvbr_location.SalesConsultant;
import com.salescrm.telephony.db.ref_location.RefLocation;
import com.salescrm.telephony.db.ref_location.RefSalesConsultant;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.EtvbrLocationChildFragment;
import com.salescrm.telephony.fragments.EtvbrLocationFragment;
import com.salescrm.telephony.fragments.retExcFin.RefLocationChildFragment;
import com.salescrm.telephony.fragments.retExcFin.RefLocationFragment;
import com.salescrm.telephony.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by prateek on 9/7/17.
 */

public class RefSMCardAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private Realm realm;
    private RefLocation locationResult;
    private RealmList<RefSalesConsultant> salesConsutants;

    public RefSMCardAdpter(FragmentActivity activity, Realm realm, RefLocation resultSMs) {
        this.context = activity;
        this.realm = realm;
        this.locationResult = resultSMs;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.ref_sm_card_adapter, parent, false);
        return new RefSMCardAdpter.EtvbrSMCardHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final RecyclerView.ViewHolder iViewHolder = holder;
        ((EtvbrSMCardHolder) holder).llinearLayout.setVisibility(View.GONE);

        if(RefLocationFragment.isLocationAvailable()) {
            if (RefLocationChildFragment.PERCENT) {
                viewCardsForRel(holder, position);
            } else {
                viewCardsForAbsolute(holder, position);
            }
        }else{
            if (RefLocationFragment.PERCENT) {
                viewCardsForRel(holder, position);
            } else {
                viewCardsForAbsolute(holder, position);
            }
        }
    }

    private void viewCardsForAbsolute(RecyclerView.ViewHolder holder, int position) {
        final EtvbrSMCardHolder cardHolder = (EtvbrSMCardHolder) holder;
        final int pos = position;
        cardHolder.cardView.setVisibility(View.VISIBLE);
        cardHolder.llinearLayout.setVisibility(View.GONE);


        cardHolder.tvTargetFinCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetFinOutCard.setVisibility(View.GONE);
        cardHolder.tvTargetPBCard.setVisibility(View.GONE);
        cardHolder.tvTargetLECard.setVisibility(View.GONE);
        cardHolder.tvTargetExchCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetRetailCard.setVisibility(View.VISIBLE);
        cardHolder.tvRetailCard.setVisibility(View.VISIBLE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 0);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        if(pos < locationResult.getSalesManagers().get(0).getTeamLeaders().size()) {
            //absolute = results.get(position).getAbs();

            cardHolder.tvRetailCard.setPaintFlags(cardHolder.tvRetailCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getRetails());

            cardHolder.tvExchCard.setPaintFlags(cardHolder.tvExchCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvExchCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getExchanged());

            cardHolder.tvFinCard.setPaintFlags(cardHolder.tvExchCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvFinCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getIn_house_financed());

            cardHolder.tvFinOutCard.setPaintFlags(cardHolder.tvExchCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvFinOutCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getOut_house_financed());


            cardHolder.tvPBCard.setPaintFlags(cardHolder.tvExchCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvPBCard.setText(getReadableData(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getPending_bookings()));



            cardHolder.tvLECard.setPaintFlags(cardHolder.tvExchCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvLECard.setText(getReadableData(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getLive_enquiries()));

            cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
            cardHolder.llRetailsCard.setVisibility(View.VISIBLE);

            cardHolder.tvTargetRetailCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getTargets().getRetails());
            cardHolder.tvTargetExchCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getTargets().getExchanged());
            cardHolder.tvTargetFinCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getTargets().getIn_house_financed());
            cardHolder.tvTargetFinOutCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getTargets().getOut_house_financed());
            cardHolder.tvTargetPBCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getTargets().getPending_bookings());
            cardHolder.tvTargetLECard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getTargets().getLive_enquiries());

            if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl().equalsIgnoreCase("")){
                cardHolder.tvNameCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.refFooter.setVisibility(View.GONE);
                cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                String url = locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl();
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUserCard);
            }else if (locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().equalsIgnoreCase("")) {
                cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.refFooter.setVisibility(View.GONE);
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvNameCard.setText(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().charAt(0) + "");
            } else {
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                cardHolder.tvNameCard.setText("N");
            }

            cardHolder.llInner.setBackgroundColor(Color.WHITE);
            cardHolder.tvExchCard.setTextColor(Color.BLACK);
            cardHolder.tvFinCard.setTextColor(Color.BLACK);
            cardHolder.tvFinOutCard.setTextColor(Color.BLACK);
            cardHolder.tvPBCard.setTextColor(Color.BLACK);
            cardHolder.tvLECard.setTextColor(Color.BLACK);
            cardHolder.tvRetailCard.setTextColor(Color.BLACK);

            cardHolder.tvFinCard.setTypeface(null, Typeface.NORMAL);;
            cardHolder.tvFinOutCard.setTypeface(null, Typeface.NORMAL);;
            cardHolder.tvPBCard.setTypeface(null, Typeface.NORMAL);;
            cardHolder.tvLECard.setTypeface(null, Typeface.NORMAL);;
            cardHolder.tvExchCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvRetailCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvUserTotalCard.setTypeface(null, Typeface.NORMAL);

            cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
            cardHolder.viewExchCard.setVisibility(View.VISIBLE);
            cardHolder.viewFinCard.setVisibility(View.VISIBLE);
            cardHolder.viewFinOutCard.setVisibility(View.VISIBLE);
            cardHolder.viewPBCard.setVisibility(View.VISIBLE);
            cardHolder.viewLECard.setVisibility(View.VISIBLE);

            cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
            cardHolder.tvTargetExchCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
            cardHolder.tvTargetFinCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
            cardHolder.tvTargetFinOutCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
            cardHolder.tvTargetPBCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
            cardHolder.tvTargetLECard.setBackgroundColor(Color.parseColor("#C1C1C1"));
            cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#C1C1C1"));

            cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });

            cardHolder.tvRetailCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });

            cardHolder.tvExchCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });

            cardHolder.tvFinCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });

            cardHolder.tvFinOutCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });
            cardHolder.tvPBCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });
            cardHolder.tvLECard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });

            cardHolder.imgUserCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            cardHolder.tvNameCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getRetails(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getExchanged(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(6,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getIn_house_financed(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvFinOutCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(7,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getOut_house_financed(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });   cardHolder.tvPBCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(8,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getPending_bookings(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });   cardHolder.tvLECard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(9,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getLive_enquiries(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });
        }else{

            cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));

            cardHolder.tvRetailCard.setPaintFlags(cardHolder.tvRetailCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getAbs().getRetails());

            cardHolder.tvExchCard.setPaintFlags(cardHolder.tvExchCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvExchCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getExchanged());

            cardHolder.tvFinCard.setPaintFlags(cardHolder.tvExchCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvFinCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getIn_house_financed());

            cardHolder.tvFinOutCard.setPaintFlags(cardHolder.tvExchCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvFinOutCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getOut_house_financed());


            cardHolder.tvPBCard.setPaintFlags(cardHolder.tvExchCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvPBCard.setText(getReadableData(locationResult.getSalesManagers().get(0).getInfo().getAbs().getPending_bookings()));


            cardHolder.tvLECard.setPaintFlags(cardHolder.tvExchCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvLECard.setText(getReadableData(locationResult.getSalesManagers().get(0).getInfo().getAbs().getLive_enquiries()));

            cardHolder.tvTargetRetailCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getTargets().getRetails());
            cardHolder.tvTargetExchCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getTargets().getExchanged());
            cardHolder.tvTargetFinCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getTargets().getIn_house_financed());
            cardHolder.tvTargetFinOutCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getTargets().getOut_house_financed());
            cardHolder.tvTargetPBCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getTargets().getPending_bookings());
            cardHolder.tvTargetLECard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getTargets().getLive_enquiries());

            cardHolder.tvRetailCard.setVisibility(View.VISIBLE);
            cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
            cardHolder.tvTargetTitleCard.setVisibility(View.VISIBLE);
            cardHolder.llRetailsCard.setVisibility(View.VISIBLE);

            cardHolder.tvNameCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
            cardHolder.refFooter.setVisibility(View.VISIBLE);
            cardHolder.imgUserCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setText("Achieved");

            cardHolder.tvTargetTitleCard.setText("Target");

            cardHolder.tvExchCard.setTextColor(Color.WHITE);
            cardHolder.tvFinCard.setTextColor(Color.WHITE);
            cardHolder.tvFinOutCard.setTextColor(Color.WHITE);
            cardHolder.tvPBCard.setTextColor(Color.WHITE);
            cardHolder.tvLECard.setTextColor(Color.WHITE);
            cardHolder.tvRetailCard.setTextColor(Color.WHITE);
            cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);

            cardHolder.tvExchCard.setTypeface(null, Typeface.BOLD);;
            cardHolder.tvFinCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvFinOutCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvPBCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvLECard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvRetailCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvUserTotalCard.setTypeface(null, Typeface.BOLD);

            cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
            cardHolder.viewExchCard.setVisibility(View.INVISIBLE);
            cardHolder.viewFinCard.setVisibility(View.INVISIBLE);
            cardHolder.viewFinOutCard.setVisibility(View.INVISIBLE);
            cardHolder.viewPBCard.setVisibility(View.INVISIBLE);
            cardHolder.viewLECard.setVisibility(View.INVISIBLE);

            cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetFinCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetFinOutCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetPBCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetLECard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetExchCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#8C94AB"));

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,"All TL's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getRetails(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,"All TL's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getExchanged(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(6,"All TL's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getIn_house_financed(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvFinOutCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(7,"All TL's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getOut_house_financed(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvPBCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(8,"All TL's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getPending_bookings(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvLECard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(9,"All TL's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getLive_enquiries(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

        }
    }

    private void viewCardsForRel(RecyclerView.ViewHolder holder, int position) {
        final EtvbrSMCardHolder cardHolder = (EtvbrSMCardHolder) holder;
        final int pos = position;
        cardHolder.llinearLayout.setVisibility(View.GONE);

        cardHolder.cardView.setVisibility(View.VISIBLE);
        cardHolder.llinearLayout.setVisibility(View.GONE);
        cardHolder.tvTargetRetailCard.setVisibility(View.GONE);
        cardHolder.tvTargetExchCard.setVisibility(View.GONE);
        cardHolder.tvTargetFinCard.setVisibility(View.GONE);
        cardHolder.tvTargetFinOutCard.setVisibility(View.GONE);
        cardHolder.tvTargetPBCard.setVisibility(View.GONE);
        cardHolder.tvTargetLECard.setVisibility(View.GONE);
        cardHolder.viewTargetCard.setVisibility(View.GONE);
        cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
        cardHolder.llRetailsCard.setVisibility(View.GONE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 6);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);


        if(pos < locationResult.getSalesManagers().get(0).getTeamLeaders().size()) {
            //rels = results.get(position).getRel();

            cardHolder.tvRetailCard.setPaintFlags(0);
            cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getRetails());

            cardHolder.tvExchCard.setPaintFlags(0);
            cardHolder.tvExchCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getExchanged());

            cardHolder.tvFinCard.setPaintFlags(0);
            cardHolder.tvFinCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getIn_house_financed());

            cardHolder.tvFinOutCard.setPaintFlags(0);
            cardHolder.tvFinOutCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getOut_house_financed());

            cardHolder.tvPBCard.setPaintFlags(0);
            cardHolder.tvPBCard.setText(getReadableData(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getPending_bookings()));

            cardHolder.tvLECard.setPaintFlags(0);
            cardHolder.tvLECard.setText(getReadableData(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getLive_enquiries()));

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvFinOutCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvPBCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvLECard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.viewReatilCard.setVisibility(View.GONE);
            cardHolder.tvRetailCard.setVisibility(View.GONE);
            cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
            cardHolder.llRetailsCard.setVisibility(View.GONE);

            if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl().equalsIgnoreCase("")){
                cardHolder.tvNameCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.refFooter.setVisibility(View.GONE);
                cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                String url = locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl();
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUserCard);
            }else if (locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().equalsIgnoreCase("")) {
                cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.refFooter.setVisibility(View.GONE);
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvNameCard.setText(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().charAt(0) + "");
            } else {
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                cardHolder.tvNameCard.setText("N");
            }

            cardHolder.llInner.setBackgroundColor(Color.WHITE);
            cardHolder.tvExchCard.setTextColor(Color.BLACK);
            cardHolder.tvFinCard.setTextColor(Color.BLACK);
            cardHolder.tvFinOutCard.setTextColor(Color.BLACK);
            cardHolder.tvPBCard.setTextColor(Color.BLACK);
            cardHolder.tvLECard.setTextColor(Color.BLACK);
            cardHolder.tvRetailCard.setTextColor(Color.BLACK);

            cardHolder.tvFinCard.setTypeface(null, Typeface.NORMAL);;
            cardHolder.tvExchCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvRetailCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvFinOutCard.setTypeface(null, Typeface.NORMAL);;
            cardHolder.tvPBCard.setTypeface(null, Typeface.NORMAL);;
            cardHolder.tvLECard.setTypeface(null, Typeface.NORMAL);;

            cardHolder.viewExchCard.setVisibility(View.VISIBLE);
            cardHolder.viewFinCard.setVisibility(View.VISIBLE);
            cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
            cardHolder.viewFinOutCard.setVisibility(View.VISIBLE);
            cardHolder.viewPBCard.setVisibility(View.VISIBLE);
            cardHolder.viewLECard.setVisibility(View.VISIBLE);

            cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        viewForRel(cardHolder, pos);
                    }
                }
            });

            cardHolder.imgUserCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            cardHolder.tvNameCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

        }else{
            cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));


            cardHolder.tvRetailCard.setPaintFlags(0);
            cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getRel().getRetails());

            cardHolder.tvExchCard.setPaintFlags(0);
            cardHolder.tvExchCard.setText(""+ locationResult.getSalesManagers().get(0).getInfo().getRel().getExchanged());

            cardHolder.tvFinCard.setPaintFlags(0);
            cardHolder.tvFinCard.setText(""+ locationResult.getSalesManagers().get(0).getInfo().getRel().getIn_house_financed());

            cardHolder.tvFinOutCard.setPaintFlags(0);
            cardHolder.tvFinOutCard.setText(""+ locationResult.getSalesManagers().get(0).getInfo().getRel().getOut_house_financed());


            cardHolder.tvPBCard.setPaintFlags(0);
            cardHolder.tvPBCard.setText(getReadableData(locationResult.getSalesManagers().get(0).getInfo().getRel().getPending_bookings()));


            cardHolder.tvLECard.setPaintFlags(0);
            cardHolder.tvLECard.setText(getReadableData(locationResult.getSalesManagers().get(0).getInfo().getRel().getLive_enquiries()));

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvFinOutCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvPBCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvLECard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });


            cardHolder.tvRetailCard.setVisibility(View.GONE);
            cardHolder.viewReatilCard.setVisibility(View.GONE);

            cardHolder.tvNameCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
            cardHolder.refFooter.setVisibility(View.VISIBLE);
            cardHolder.imgUserCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setText("Total%");


            cardHolder.tvExchCard.setTextColor(Color.WHITE);
            cardHolder.tvFinCard.setTextColor(Color.WHITE);
            cardHolder.tvFinOutCard.setTextColor(Color.WHITE);
            cardHolder.tvPBCard.setTextColor(Color.WHITE);
            cardHolder.tvLECard.setTextColor(Color.WHITE);
            cardHolder.tvRetailCard.setTextColor(Color.WHITE);
            cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);

            cardHolder.tvExchCard.setTypeface(null, Typeface.BOLD);;
            cardHolder.tvFinCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvFinOutCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvPBCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvLECard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvRetailCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvUserTotalCard.setTypeface(null, Typeface.BOLD);

            cardHolder.viewExchCard.setVisibility(View.INVISIBLE);
            cardHolder.viewFinCard.setVisibility(View.INVISIBLE);
            cardHolder.viewFinOutCard.setVisibility(View.INVISIBLE);
            cardHolder.viewPBCard.setVisibility(View.INVISIBLE);
            cardHolder.viewLECard.setVisibility(View.INVISIBLE);
            cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
        }
    }

    private void viewForAbsolute(RecyclerView.ViewHolder holder, int pos) {
        final EtvbrSMCardHolder cardHolder = (EtvbrSMCardHolder) holder;
        final int position = pos;
        cardHolder.llinearLayout.removeAllViews();
        salesConsutants = locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getSalesConsultants();
        for (int a = 0; a < salesConsutants.size(); a++) {
            final int positionInner = a;
            LinearLayout ll = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.ref_sm_inner_layout, null, false);

            ((TextView) ll.findViewById(R.id.txt_retail)).setPaintFlags(cardHolder.tvRetailCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_retail)).setText(""+salesConsutants.get(a).getInfo().getAbs().getRetails());

            ((TextView) ll.findViewById(R.id.txt_exch)).setPaintFlags(cardHolder.tvRetailCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_exch)).setText(""+salesConsutants.get(a).getInfo().getAbs().getExchanged());

            ((TextView) ll.findViewById(R.id.txt_fin)).setPaintFlags(cardHolder.tvRetailCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_fin)).setText(""+salesConsutants.get(a).getInfo().getAbs().getIn_house_financed());

            ((TextView) ll.findViewById(R.id.txt_fin_out)).setPaintFlags(cardHolder.tvRetailCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_fin_out)).setText(""+salesConsutants.get(a).getInfo().getAbs().getOut_house_financed());

            ((TextView) ll.findViewById(R.id.txt_pending_bookings)).setPaintFlags(cardHolder.tvRetailCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_pending_bookings)).setText(""+salesConsutants.get(a).getInfo().getAbs().getPending_bookings());

            ((TextView) ll.findViewById(R.id.txt_live_enquiries)).setPaintFlags(cardHolder.tvRetailCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_live_enquiries)).setText(""+salesConsutants.get(a).getInfo().getAbs().getLive_enquiries());

            ((TextView) ll.findViewById(R.id.txt_retail)).setVisibility(View.VISIBLE);
            ((View) ll.findViewById(R.id.retail_view)).setVisibility(View.VISIBLE);
            ((TextView) ll.findViewById(R.id.tv_target_title)).setVisibility(View.INVISIBLE);
            ((LinearLayout) ll.findViewById(R.id.retail_ll)).setVisibility(View.VISIBLE);
            ((TextView) ll.findViewById(R.id.txt_target_retail)).setText(""+salesConsutants.get(a).getInfo().getTargets().getRetails());
            //((TextView) ll.findViewById(R.id.txt_target_exch)).setText(""+salesConsutants.get(a).getInfo().getTargets().getExchanged());
            //((TextView) ll.findViewById(R.id.txt_target_fin)).setText(""+salesConsutants.get(a).getInfo().getTargets().getFinanced());
            ((TextView) ll.findViewById(R.id.txt_target_retail)).setVisibility(View.VISIBLE);
            ((View) ll.findViewById(R.id.target_view)).setVisibility(View.VISIBLE);
            ((TextView) ll.findViewById(R.id.txt_target_fin_out)).setVisibility(View.GONE);
//            ((TextView) ll.findViewById(R.id.txt_target_pending_bookings)).setVisibility(View.GONE);
 //           ((TextView) ll.findViewById(R.id.txt_target_live_enquiries)).setVisibility(View.GONE);
            ((RelativeLayout) ll.findViewById(R.id.rl_frame_layout)).setPadding(0, 6, 0, 0);

            if(salesConsutants.get(a).getInfo().getDpUrl() != null && !salesConsutants.get(a).getInfo().getDpUrl().equalsIgnoreCase("")){
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.GONE);
                String url = salesConsutants.get(a).getInfo().getDpUrl();
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(((ImageView) ll.findViewById(R.id.img_user)));
            }else if (salesConsutants.get(positionInner).getInfo().getName() != null && !salesConsutants.get(positionInner).getInfo().getName().equalsIgnoreCase("")) {
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.GONE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setText(""+salesConsutants.get(positionInner).getInfo().getName().charAt(0));
            } else {
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.GONE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setText("N");
            }

            ((ImageView) ll.findViewById(R.id.img_user)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(salesConsutants.get(positionInner).getInfo().getDpUrl() != null && !salesConsutants.get(positionInner).getInfo().getDpUrl().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", salesConsutants.get(positionInner).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", salesConsutants.get(positionInner).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.text_user)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(salesConsutants.get(positionInner).getInfo().getName() != null && !salesConsutants.get(positionInner).getInfo().getName().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", salesConsutants.get(positionInner).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", salesConsutants.get(positionInner).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.txt_retail)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getRetails(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.txt_exch)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getExchanged(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.txt_fin)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(6,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getIn_house_financed(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.txt_fin_out)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(7,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getOut_house_financed(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });
            ((TextView) ll.findViewById(R.id.txt_pending_bookings)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(8,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getPending_bookings(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.txt_live_enquiries)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(9,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getLive_enquiries(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });
            cardHolder.llinearLayout.addView(ll);
        }
    }


    private void viewForRel(RecyclerView.ViewHolder holder, int pos) {
        EtvbrSMCardHolder cardHolder = (EtvbrSMCardHolder) holder;
        final int position = pos;
        cardHolder.llinearLayout.removeAllViews();
        salesConsutants = locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getSalesConsultants();
        for (int a = 0; a < salesConsutants.size(); a++) {
            final int positionInner = a;
            LinearLayout ll = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.etvbr_sm_inner_layout, null, false);
            ((TextView) ll.findViewById(R.id.txt_retail)).setText(""+salesConsutants.get(a).getInfo().getRel().getRetails());
            ((TextView) ll.findViewById(R.id.txt_exch)).setText(""+salesConsutants.get(a).getInfo().getRel().getExchanged());
            ((TextView) ll.findViewById(R.id.txt_fin)).setText(""+salesConsutants.get(a).getInfo().getRel().getIn_house_financed());
            ((TextView) ll.findViewById(R.id.txt_fin_out)).setText(""+salesConsutants.get(a).getInfo().getRel().getOut_house_financed());
            ((TextView) ll.findViewById(R.id.txt_pending_bookings)).setText(getReadableData(salesConsutants.get(a).getInfo().getRel().getPending_bookings()));
            ((TextView) ll.findViewById(R.id.txt_live_enquiries)).setText(getReadableData(salesConsutants.get(a).getInfo().getRel().getLive_enquiries()));
            ((TextView) ll.findViewById(R.id.txt_retail)).setVisibility(View.GONE);
            ((View)ll.findViewById(R.id.retail_view)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.tv_target_title)).setVisibility(View.GONE);
            ((LinearLayout) ll.findViewById(R.id.retail_ll)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.txt_target_retail)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.txt_target_exch)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.txt_target_fin)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.txt_target_fin_out)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.txt_target_pending_bookings)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.txt_target_live_enquiries)).setVisibility(View.GONE);
            ((View) ll.findViewById(R.id.target_view)).setVisibility(View.GONE);
            ((RelativeLayout) ll.findViewById(R.id.rl_frame_layout)).setPadding(0, 6, 0, 6);

            if(salesConsutants.get(a).getInfo().getDpUrl() != null && !salesConsutants.get(a).getInfo().getDpUrl().equalsIgnoreCase("")){
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.GONE);
                String url = salesConsutants.get(a).getInfo().getDpUrl();
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(((ImageView) ll.findViewById(R.id.img_user)));
            }else if (salesConsutants.get(a).getInfo().getName() != null && !salesConsutants.get(a).getInfo().getName().equalsIgnoreCase("")) {
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.GONE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setText(""+salesConsutants.get(a).getInfo().getName().charAt(0));
            } else {
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.GONE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setText("N");
            }


            ((ImageView) ll.findViewById(R.id.img_user)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(salesConsutants.get(positionInner).getInfo().getDpUrl() != null && !salesConsutants.get(positionInner).getInfo().getDpUrl().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", salesConsutants.get(positionInner).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", salesConsutants.get(positionInner).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.text_user)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(salesConsutants.get(positionInner).getInfo().getName() != null && !salesConsutants.get(positionInner).getInfo().getName().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", salesConsutants.get(positionInner).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", salesConsutants.get(positionInner).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            cardHolder.llinearLayout.addView(ll);
        }

    }

    @Override
    public int getItemCount() {
        if(locationResult.isValid()
                && locationResult.getSalesManagers().isValid()
                && locationResult.getSalesManagers().get(0).getTeamLeaders().isValid()) {
            return (locationResult.getSalesManagers().get(0).getTeamLeaders().size() > 0) ? locationResult.getSalesManagers().get(0).getTeamLeaders().size() + 1 : 0;
        }else{
            return 0;
        }
    }

    public class EtvbrSMCardHolder extends RecyclerView.ViewHolder {

        ImageView imgUser, imgUserCard;
        TextView tvNameCard, tvRetailCard, tvExchCard, tvFinCard, tvFinOutCard, tvPBCard, tvLECard;
        TextView tvName, tvEnquiry, tvTestDrive, tvVisit, tvBooking, tvRetail;
        View viewReatilCard, viewExchCard, viewFinCard, viewFinOutCard, viewPBCard, viewLECard;
        View viewEnquiry, viewTd, viewVisit, viewBooking, viewReatil;
        LinearLayout llinearLayout;
        LinearLayout llInner;
        TextView tvUserTotalCard;
        TextView tvUserTotal;
        CardView cardView;
        LinearLayout llRetailsCard, llExchCard, llFinCard, llFinOutCard, llPBCard, llLECard;
        TextView tvTargetTitleCard;
        TextView tvTargetRetailCard, tvTargetExchCard, tvTargetFinCard, tvTargetFinOutCard, tvTargetPBCard, tvTargetLECard;
        View viewTargetCard;
        RelativeLayout rlFrameLayoutCard;
        private LinearLayout refFooter;

        public EtvbrSMCardHolder(View itemView) {
            super(itemView);
            imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            tvName = (TextView) itemView.findViewById(R.id.text_user);
            tvEnquiry = (TextView) itemView.findViewById(R.id.txt_enquiry);
            tvTestDrive = (TextView) itemView.findViewById(R.id.txt_td);
            tvVisit = (TextView) itemView.findViewById(R.id.txt_visit);
            tvBooking = (TextView) itemView.findViewById(R.id.txt_booking);
            tvRetail = (TextView) itemView.findViewById(R.id.txt_retail);
            //llaLinearLayout = (LinearLayout) itemView.findViewById(R.id.ll_today_summary_main);
            tvUserTotal = (TextView) itemView.findViewById(R.id.text_user_total);
            viewEnquiry = (View) itemView.findViewById(R.id.enquiry_view);
            viewTd = (View) itemView.findViewById(R.id.td_view);
            viewVisit = (View) itemView.findViewById(R.id.visit_view);
            viewBooking = (View) itemView.findViewById(R.id.booking_view);
            viewReatil = (View) itemView.findViewById(R.id.retail_view);
            llinearLayout = (LinearLayout) itemView.findViewById(R.id.llinearLayout);
            llInner = (LinearLayout) itemView.findViewById(R.id.card_ll);

            imgUserCard = (ImageView) itemView.findViewById(R.id.img_user_card);
            tvNameCard = (TextView) itemView.findViewById(R.id.text_user_card);
            tvRetailCard = (TextView) itemView.findViewById(R.id.txt_retail_card);
            tvUserTotalCard = (TextView) itemView.findViewById(R.id.text_user_total_card);
            viewReatilCard = (View) itemView.findViewById(R.id.retail_view_card);
            cardView = (CardView) itemView.findViewById(R.id.cardview_etvbr);
            tvTargetTitleCard = (TextView) itemView.findViewById(R.id.tv_target_title_card);
            tvTargetRetailCard = (TextView) itemView.findViewById(R.id.txt_target_retail_card);
            viewTargetCard = (View) itemView.findViewById(R.id.target_view_card);
            rlFrameLayoutCard = (RelativeLayout) itemView.findViewById(R.id.rl_frame_layout_card);
            llRetailsCard = (LinearLayout) itemView.findViewById(R.id.retail_ll_card);
            viewExchCard = (View) itemView.findViewById(R.id.exch_view_card);
            viewFinCard = (View) itemView.findViewById(R.id.fin_view_card);
            viewFinOutCard = (View) itemView.findViewById(R.id.fin_out_view_card);
            viewPBCard = (View) itemView.findViewById(R.id.pending_bookings_view_card);
            viewLECard = (View) itemView.findViewById(R.id.live_enquiries_view_card);
            llExchCard = (LinearLayout) itemView.findViewById(R.id.exch_ll_card);
            llFinCard = (LinearLayout) itemView.findViewById(R.id.fin_ll_card);
            llFinOutCard = (LinearLayout) itemView.findViewById(R.id.fin_out_ll_card);
            llPBCard = (LinearLayout) itemView.findViewById(R.id.pending_bookings_ll_card);
            llLECard = (LinearLayout) itemView.findViewById(R.id.live_enquiries_ll_card);
            tvTargetExchCard = (TextView) itemView.findViewById(R.id.txt_target_exch__card);
            tvTargetFinCard = (TextView) itemView.findViewById(R.id.txt_target_fin_card);
            tvTargetFinOutCard = (TextView) itemView.findViewById(R.id.txt_target_fin_out_card);
            tvTargetPBCard = (TextView) itemView.findViewById(R.id.txt_target_pending_bookings_card);
            tvTargetLECard = (TextView) itemView.findViewById(R.id.txt_target_live_enquiries_card);
            tvExchCard = (TextView) itemView.findViewById(R.id.txt_exch_card);
            tvFinCard = (TextView) itemView.findViewById(R.id.txt_fin_card);
            tvFinOutCard = (TextView) itemView.findViewById(R.id.txt_fin_out_card);
            tvPBCard = (TextView) itemView.findViewById(R.id.txt_pending_bookings_card);
            tvLECard = (TextView) itemView.findViewById(R.id.txt_live_enquiries_card);
            refFooter = itemView.findViewById(R.id.ref_footer);
        }
    }

    private void goToDetailsOfEtvbr(int position, String title_name, String user_id, String etvbrtotal, String user_role_id, String user_location_id) {

        Intent etvbrDetails =  null;

        switch (position){
            case 4:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Retails");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 5:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Exchanged");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 6:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "In House Financed");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 7:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Out House Financed");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 8:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Pending Bookings");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 9:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Live Enquiries");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
        }
        if(etvbrDetails != null)
            context.startActivity(etvbrDetails);
    }

    private String getReadableData(Integer data) {
        if(data==null) {
            return "--";
        }
        return ""+data;
    }

}
