package com.salescrm.telephony.fragments.addlead;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddLeadActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.crm_user.TeamUserDB;
import com.salescrm.telephony.db.crm_user.UserNewCarDB;
import com.salescrm.telephony.db.crm_user.UsersDB;
import com.salescrm.telephony.interfaces.AddLeadCommunication;
import com.salescrm.telephony.model.AddLeadDSEInputData;
import com.salescrm.telephony.model.AddLeadInputPrimaryElements;
import com.salescrm.telephony.model.CreateLeadInputData;
import com.salescrm.telephony.model.ViewPagerSwiped;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.LeadTeamUserResponse;
import com.salescrm.telephony.response.SalesManagerDseResponse;
import com.salescrm.telephony.response.TeamLeadsAndDSEsResponse;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

import static android.text.TextUtils.isEmpty;

/**
 * Created by bharath on 30/5/16.
 */
public class AddLeadDseFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener {

    TextView tvDseHot, tvDseCold, tvDseWarm;
    private AddLeadCommunication addLeadCommunication;
    private String TAG = "AddLeadDSE";
    private AutoCompleteTextView spinnerDseBranch, autoTvSalesManager, autoTvTeamLead, autoTvDseName;
    private Realm realm;
    private Preferences pref = null;
    private String enqSourceId;
    private String primaryCarId;
    private ProgressDialog progressDialog;
    private SalesManagerDseResponse salesManagerDseData;
    private TeamLeadsAndDSEsResponse teamLeadDseData;
    private int apiCallCount = 0;
    private int hotColor, warmColor, coldColor;
    private TextView[] tvColors;
    private int allColor[];
    private int cardColor;
    private TextView tvLeadTagWarning;
    private RelativeLayout relLeadTagHolder;
    private int errorColor;
    private String leadColorTag;
    private LeadTeamUserResponse teamUserData;
    private Map<String, String> mangerMap, teamLeadMap, dseMap;
    private ArrayAdapter<LocationWiseUserItem> dseAdapter;
    private LocationWiseUserItem dseSelectedItem;
    private int leadNewCarId = 0;
    private int leadSource = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lead_dse, container, false);

        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(getContext());

        mangerMap = new HashMap<String, String>();
        teamLeadMap = new HashMap<String, String>();
        dseMap = new HashMap<String, String>();


        hotColor = ContextCompat.getColor(getContext(), R.color.hot);
        warmColor = ContextCompat.getColor(getContext(), R.color.warm);
        coldColor = ContextCompat.getColor(getContext(), R.color.cold);
        cardColor = ContextCompat.getColor(getContext(), R.color.card_default_color);
        errorColor = ContextCompat.getColor(getContext(), R.color.colorError);


        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);

        tvLeadTagWarning = (TextView) view.findViewById(R.id.tv_lead_stage_error);
        relLeadTagHolder = (RelativeLayout) view.findViewById(R.id.rel_lead_tag);
        tvDseCold = (TextView) view.findViewById(R.id.tv_lead_cold);
        tvDseHot = (TextView) view.findViewById(R.id.tv_lead_hot);
        tvDseWarm = (TextView) view.findViewById(R.id.tv_lead_warm);
        spinnerDseBranch = (AutoCompleteTextView) view.findViewById(R.id.spinner_lead_dse_branch);
        autoTvSalesManager = (AutoCompleteTextView) view.findViewById(R.id.spinner_lead_sales_man);
        autoTvTeamLead = (AutoCompleteTextView) view.findViewById(R.id.spinner_lead_team_lead);
        autoTvDseName = (AutoCompleteTextView) view.findViewById(R.id.spinner_lead_dse_name);

        allColor = new int[]{hotColor, warmColor, coldColor};
        tvColors = new TextView[]{tvDseHot, tvDseWarm, tvDseCold};

        init();

        autoTvDseName.setThreshold(0);
        autoTvDseName.setOnFocusChangeListener(this);
        autoTvDseName.setOnClickListener(this);
        autoTvDseName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(TextUtils.isEmpty(autoTvDseName.getText())){

                    autoTvDseName.showDropDown();
                }
                return false;
            }
        });

        populateAdapters(getSalesConsultants());
        populateInitDse();

        autoTvDseName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(getActivity());
                dseSelectedItem = dseAdapter.getItem(position);
            }
        });


        tvDseHot.setOnClickListener(this);
        tvDseCold.setOnClickListener(this);
        tvDseWarm.setOnClickListener(this);

        return view;


    }

    private void populateInitDse() {
        UserDetails userDetails = realm.where(UserDetails.class).findFirst();
        if(userDetails!=null && pref.getRoleId().equalsIgnoreCase(WSConstants.DSE_ROLE_ID) && getUserLocationIds().length>0) {
            autoTvDseName.setText(userDetails.getName());
            dseSelectedItem = new LocationWiseUserItem();
            dseSelectedItem.setLocationId(getUserLocationIds()[0]);
            dseSelectedItem.setId(userDetails.getUserId());
            dseSelectedItem.setText(userDetails.getName());
            //populateDataOnDseSelect();
        }
    }


    private int getPrimaryCarId() {
        if (AddLeadActivity.carInputData == null) {
            return 0;
        }
        for (int i = 0; i < AddLeadActivity.carInputData.getNewCarInputData().size(); i++) {
            if (AddLeadActivity.carInputData.getNewCarInputData().get(i).getIs_primary()) {
                return Util.getInt(AddLeadActivity.carInputData.getNewCarInputData().get(i).getModel_id());
            }
        }
        return 0;
    }


    private Integer[] getUserLocationIds() {
        RealmResults<UserLocationsDB> userLocations = realm.where(UserLocationsDB.class).findAll();
        Integer[] arr = new Integer[userLocations==null?0:userLocations.size()];
        if(userLocations ==null){
            return arr;
        }
        for (int i = 0; i < userLocations.size(); i++) {
            arr[i] = Util.getInt(userLocations.get(i).getLocation_id());
        }
        return arr;
    }


    public void init() {
        for (int i = 0; i < allColor.length; i++) {
            GradientDrawable drawable = (GradientDrawable) tvColors[i].getBackground();
            drawable.setStroke(2, allColor[i]);
            drawable.setColor(cardColor);
            tvColors[i].setTextColor(allColor[i]);
        }


    }

    public void initClicked(TextView view, int color) {
        if (!relLeadTagHolder.isEnabled()) {
            makeNormal();
        }
        GradientDrawable drawable = (GradientDrawable) view.getBackground();
        drawable.setStroke(2, color);
        drawable.setColor(color);
        view.setTextColor(Color.WHITE);

    }


    @Override
    public void onClick(View v) {
        if (v instanceof AutoCompleteTextView) {
            if (v.getId() == R.id.spinner_lead_dse_branch) {
                ((AutoCompleteTextView) v).showDropDown();
            } else if (!TextUtils.isEmpty(spinnerDseBranch.getText())) {
                ((AutoCompleteTextView) v).showDropDown();
            }

        }
        switch (v.getId()) {
            case R.id.tv_lead_hot:
                leadColorTag = WSConstants.LEAD_TAG_HOT;
                setDseTextNormal((TextView) v, hotColor);
                break;
            case R.id.tv_lead_cold:
                leadColorTag = WSConstants.LEAD_TAG_COLD;
                setDseTextNormal((TextView) v, coldColor);
                break;
            case R.id.tv_lead_warm:
                leadColorTag = WSConstants.LEAD_TAG_WARM;
                setDseTextNormal((TextView) v, warmColor);
                break;
        }
    }

    public void setDseTextNormal(TextView view, int color) {
        init();
        initClicked(view, color);
    }

    @Override
    public void onAttach(Context context) {
        try {
            addLeadCommunication = (AddLeadCommunication) context;
        } catch (Exception e) {
            System.out.println("ClassCastException");
        }
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);

    }

    @Subscribe
    public void onViewPagerMoved(ViewPagerSwiped viewPagerSwiped) {
        if (viewPagerSwiped.getOldPosition() == 3 && viewPagerSwiped.getNewPosition() == 4) {
            System.out.println(TAG + "ViewPagerTryToMoving");

            if(autoTvDseName.getText().toString().equalsIgnoreCase("")){
                AddLeadActivity.showAlert("Please select Dse Name");
            }else if (TextUtils.isEmpty(leadColorTag)) {
                showAlert();
            } else if (isDataValid()) {
                AddLeadActivity.dseInputData = getDSEInputData();
                addLeadCommunication.onAssignLeadDseEdited(getDSEInputData(), viewPagerSwiped.getOldPosition());
            }
        }
    }

    private boolean isDataValid() {

        if (!autoTvDseName.getText().toString().equalsIgnoreCase("")) {
            if (getDseId().equalsIgnoreCase("")) {
                AddLeadActivity.showAlert("Invalid Dse Name");
                return false;
            }
        }
        return true;
    }

    private AddLeadDSEInputData getDSEInputData() {

        return new AddLeadDSEInputData(dseSelectedItem.getLocationId()+"", getUserDetails(), getLeadTagId());
    }

    private List<CreateLeadInputData.Lead_data.User_details> getUserDetails() {
        List<CreateLeadInputData.Lead_data.User_details> list = new ArrayList<>();
        CreateLeadInputData.Lead_data.User_details user_detailsDseLead = new CreateLeadInputData().new Lead_data().new User_details();

        if (!TextUtils.isEmpty(getDseId())) {
            user_detailsDseLead.setRole_id(WSConstants.DSE_ROLE_ID);
            user_detailsDseLead.setUser_id(getDseId());
            list.add(user_detailsDseLead);
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.println("List data at :" + i + ":::UserId:" + list.get(i).getUser_id() + ":::RoleId:" + list.get(i).getRole_id());
            //System.out.println(list.get(i).getRole_id());
        }
        return list;

    }


    private String getLeadTagId() {
        if (leadColorTag.equalsIgnoreCase(WSConstants.LEAD_TAG_COLD)) {
            return WSConstants.LEAD_TAG_COLD_ID;
        } else if (leadColorTag.equalsIgnoreCase(WSConstants.LEAD_TAG_WARM)) {
            return WSConstants.LEAD_TAG_WARM_ID;
        } else {
            return WSConstants.LEAD_TAG_HOT_ID;
        }

    }


    private String getDseId() {
        if (dseSelectedItem != null && autoTvDseName.getText().toString().equalsIgnoreCase(dseSelectedItem.getText())) {
            return String.valueOf(dseSelectedItem.getId());
        }
        return "";
    }





    @Subscribe
    public void onAddLeadPrimaryElements(AddLeadInputPrimaryElements data) {
        if (data.getEnqSourceId() != null) {
            this.enqSourceId = data.getEnqSourceId();
        }

        if (data.getPrimaryCarId() != null) {
            this.primaryCarId = data.getPrimaryCarId();
        }

    }

    private void populateAdapters(List<LocationWiseUserItem> data) {

        List<LocationWiseUserItem> updateLocationWiseUserItems = new ArrayList<>();
        for (int i=0; i<data.size(); i++) {
            UsersDB userData = realm.where(UsersDB.class).equalTo("id", data.get(i).getId()+"").findFirst();
            if(userData != null && Util.isNotNull(userData.getName())) {
                LocationWiseUserItem updatedData = data.get(i);
                updatedData.setText(userData.getName());
                updateLocationWiseUserItems.add(updatedData);

            }
        }
        dseAdapter = new ArrayAdapter<LocationWiseUserItem>(getContext(), android.R.layout.simple_list_item_1, updateLocationWiseUserItems);
        autoTvDseName.setAdapter(dseAdapter);

    }



    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus && v instanceof AutoCompleteTextView && isEmpty(((AutoCompleteTextView) v).getText().toString())) {
            if (v.getId() == R.id.spinner_lead_dse_branch) {
                ((AutoCompleteTextView) v).showDropDown();
            } else if (!TextUtils.isEmpty(spinnerDseBranch.getText())) {
                ((AutoCompleteTextView) v).showDropDown();
            }
        }
    }

    private void showAlert() {
        relLeadTagHolder.setEnabled(false);
        relLeadTagHolder.setBackgroundColor(errorColor);
        tvLeadTagWarning.setVisibility(View.VISIBLE);
    }

    private void makeNormal() {
        relLeadTagHolder.setEnabled(true);
        relLeadTagHolder.setBackgroundColor(cardColor);
        tvLeadTagWarning.setVisibility(View.GONE);
    }

    private List<LocationWiseUserItem> getSalesConsultants() {
        List<LocationWiseUserItem> locationWiseUserItems = new ArrayList<>();
        RealmResults<TeamUserDB> teamUsers = realm.where(TeamUserDB.class)
                .in("locationId", getUserLocationIds())
                .findAll();

        for(int i=0;i<teamUsers.size();i++) {
            RealmResults<UserNewCarDB> userNewCarDBS = teamUsers.get(i).getUserNewCarDBList()
                    .where()
                    .equalTo("leadNewCar", leadNewCarId)
                    .equalTo("leadSource", leadSource).findAll();

            for(int j=0;j<userNewCarDBS.size();j++) {

                for(int k=0; k<userNewCarDBS.get(j).getTeam_data().size() ;k++){
                    LocationWiseUserItem locationWiseUserItem = new LocationWiseUserItem();
                    locationWiseUserItem.setLocationId(teamUsers.get(i).getLocationId());

              //      locationWiseUserItem.setText(userNewCarDBS.get(j).getTeam_data().get(k).getUser_id());
                    locationWiseUserItem.setId(Util.getInt(userNewCarDBS.get(j).getTeam_data().get(k).getUser_id()));
                    locationWiseUserItems.add(locationWiseUserItem);
                }
            }
        }
        return locationWiseUserItems;
    }


    private class LocationWiseUserItem {
        private String mText;
        private int mId;
        private Integer locationId;

        public Integer getLocationId() {
            return locationId;
        }

        public void setLocationId(Integer locationId) {
            this.locationId = locationId;
        }

        public int getId() {

            return mId;
        }

        public void setId(int id) {

            this.mId = id;
        }

        public String getText() {

            return mText;
        }

        public void setText(String text) {

            this.mText = text;
        }

        @Override
        public String toString() {

            return this.mText;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        progressDialog.hide();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }
}
