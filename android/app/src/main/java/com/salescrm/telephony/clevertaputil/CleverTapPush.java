package com.salescrm.telephony.clevertaputil;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.preferences.Preferences;

import java.util.HashMap;

public class CleverTapPush {
    public static final String TAG = "CLEVER_TAP_PUSH ";

    public static void pushProfile(Preferences preferences) {
        try {
            HashMap<String, Object> profileUpdate = new HashMap<String, Object>();
            profileUpdate.put("Name", preferences.getDseName());
            profileUpdate.put("UserName", preferences.getUserName());
            profileUpdate.put("Identity", preferences.getUserName() + "_" + preferences.getDealerName());
            profileUpdate.put("Phone", preferences.getUserMobile());
            profileUpdate.put("MobileNumber", preferences.getUserMobile());
            profileUpdate.put("Email", preferences.getEmail());
            profileUpdate.put("IMEI", preferences.getImeiNumber());

            profileUpdate.put("Roles", DbUtils.getRolesName());

            profileUpdate.put("Location", DbUtils.getLocationsName());

            profileUpdate.put("Dealer", preferences.getDealerName());
            profileUpdate.put("Photo", preferences.getUserProfilePicUrl());
            SalesCRMApplication.cleverTapAPI.pushProfile(profileUpdate);
            System.out.println(TAG + "Profile:" + profileUpdate.toString());
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    public static void pushEvent(String eventName, HashMap<String, Object> attributes) {
        try {
            System.out.println(TAG + eventName + " " + attributes.toString());
            SalesCRMApplication.cleverTapAPI.pushEvent(eventName, attributes);
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }
    public static void pushEvent(String eventName) {
        try {
            System.out.println(TAG + eventName);
            SalesCRMApplication.cleverTapAPI.pushEvent(eventName);
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    /**
     *
     * @param communicationType {Call, SMS, Email, Whatsapp}
     * @param isMyTask {True ? MyTask:C360}
     */
    public static void pushCommunicationEvent(String communicationType, boolean isMyTask){
        try {
            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put(communicationType, isMyTask?"MyTask" : "C360");
            System.out.println(TAG + CleverTapConstants.EVENT_COMMUNICATION + " " + hashMap.toString());
            SalesCRMApplication.cleverTapAPI.pushEvent(CleverTapConstants.EVENT_COMMUNICATION, hashMap);
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    /**
     *
     * @param communicationType {Call, SMS, Email, Whatsapp}
     * @param isMyTask {True ? MyTask:C360}
     */
    public static void pushCommunicationEvent(String communicationType, boolean isMyTask, String payload){
        try {
            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put(communicationType, isMyTask?"MyTask" : "C360" +","+ payload+"");
            System.out.println(TAG + CleverTapConstants.EVENT_COMMUNICATION + " " + hashMap.toString());
            SalesCRMApplication.cleverTapAPI.pushEvent(CleverTapConstants.EVENT_COMMUNICATION, hashMap);
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    public static void pushNotificationEvent(String type, String from, String eventName){

    }
}
