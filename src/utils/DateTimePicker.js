import React, { Component } from 'react'
import DateTimePicker from 'react-native-modal-datetime-picker';


export default class CustomDateTimePicker extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    let mode = "date";
    if (this.props.datePickerMode == 'datetime') {
      mode = 'datetime';
    }
    return (
      <DateTimePicker
        {...this.props}
        mode={mode}
        is24Hour={false}
        isVisible={this.props.dateTimePickerVisibility}
        onConfirm={(date) => { this.props.handleDateTimePicked(date, this.props.payload) }}
        onCancel={this.props.hideDateTimePicked}
      />
    )
  }
}
