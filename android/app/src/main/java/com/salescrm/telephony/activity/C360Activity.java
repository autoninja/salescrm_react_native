package com.salescrm.telephony.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.C360PagerAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.CustomerEmailId;
import com.salescrm.telephony.db.CustomerPhoneNumbers;
import com.salescrm.telephony.db.DseDetails;
import com.salescrm.telephony.db.ExchangeStatusdb;
import com.salescrm.telephony.db.IntrestedCarActivityGroup;
import com.salescrm.telephony.db.IntrestedCarActivityGroupStages;
import com.salescrm.telephony.db.IntrestedCarDetails;
import com.salescrm.telephony.db.LeadStageProgressDB;
import com.salescrm.telephony.db.PlannedActivities;
import com.salescrm.telephony.db.PlannedActivitiesAction;
import com.salescrm.telephony.db.PlannedActivitiesDetail;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.car.AdditionalCarsDetails;
import com.salescrm.telephony.db.car.AllInterestedCars;
import com.salescrm.telephony.db.car.CarDmsDataDB;
import com.salescrm.telephony.db.car.CarsDBHandler;
import com.salescrm.telephony.db.car.ExchangeCarDetails;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.dbOperation.LeadMobileMappingDbOperations;
import com.salescrm.telephony.fragments.FabFragment;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.AutoFetchFormListener;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.model.C360InformationModel;
import com.salescrm.telephony.model.C360RestartModel;
import com.salescrm.telephony.model.booking.ApiInputBookingDetails;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AllLeadCustomerDetailsResponse;
import com.salescrm.telephony.response.SearchResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;
import com.salescrm.telephony.views.BalloonAnimation;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by akshata on 25/5/16.
 */
public class C360Activity extends AppCompatActivity implements Callback, AutoFetchFormListener,FabFragment.ButtonClickListenerC360 {
    TextView tvC360NameMr, tvC360Name, tvC360LeadAge, tvC360Car;
    private ImageButton ibActionBack;
    private FloatingActionButton fabC360;
    private Preferences pref;
    private long leadId;
    private Realm realm;
    private ImageView tvC360Indicator;
    private RelativeLayout relC360OvalHolder;
    private TextView tvC360LeadStage;
    private AppBarLayout appBarC360;
    private RelativeLayout relLoading;
    private TabLayout tabLayout;
    private C360PagerAdapter adapter;
    public static ViewPager viewPager;
    private static boolean isLeadActive = true;
    private Drawable drawableCurrent, drawableActive, drawableInactive;

    public static AllLeadCustomerDetailsResponse.Result.Customer_details cardDetailResponseData;

    private String customerID;

    private SearchResponse.Result searchResponse;

    private SalesCRMRealmTable salesCRMRealmTable;

    private List<ImageView> ovalImageList;
    private boolean fromNotificationTray;
    private int tabPosition = 0;
    private boolean isCreatedTemp = false;
    private boolean switchToCarsTab;
    private boolean fabFragmentAdded;
    private boolean isClientILom;
    private boolean isClientILBank;
    private TextView tvLeadAgeTag;
    private TextView tvEditPolicy;
    private TextView tvLeadId;
    private TextView tvPolicyEndDate;
    private RelativeLayout llLeadId;
    private RelativeLayout relPolicyDetails;
    private TextView tvOTBTag;
    private TextView tvOTB;
    private TextView tvPolicyEndDateTag;

    private void disableScreenCapture() {
        if(pref.isScreenshotEnabled()) {
           getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Preferences.getInstance();
        pref.load(this);

        disableScreenCapture();

        setContentView(R.layout.activity_c360);
        realm = Realm.getDefaultInstance();

        isClientILom = pref.isClientILom();
        isClientILBank = pref.isClientILBank();
        ovalImageList = new ArrayList<>();
        appBarC360 = (AppBarLayout) findViewById(R.id.toolbar_card_detail_card);
        tvC360NameMr = (TextView) findViewById(R.id.tv_c360_person_name_mr);
        tvC360Name = (TextView) findViewById(R.id.tv_c360_person_name);
        tvC360LeadAge = (TextView) findViewById(R.id.tv_c360_lead_age);
        tvC360Car = (TextView) findViewById(R.id.tv_c360_car);
        tvC360Indicator = (ImageView) findViewById(R.id.tv_c360_indicator);
        relC360OvalHolder = (RelativeLayout) findViewById(R.id.rel_c360_progress_oval);
        tvC360LeadStage = (TextView) findViewById(R.id.tv_c360_stage);
        ibActionBack = (ImageButton) findViewById(R.id.img_c360_go_back);

        relLoading = (RelativeLayout) findViewById(R.id.rel_loading_frame);
        viewPager = (ViewPager) findViewById(R.id.pageractioncard);
        tabLayout = (TabLayout) findViewById(R.id.tab_layoutactioncard);
        fabC360 = (FloatingActionButton) findViewById(R.id.card_float);
        tvLeadAgeTag = (TextView) findViewById(R.id.tv_c360_ageing_tag);
        tvEditPolicy = (TextView) findViewById(R.id.tv_edit_policy);
        tvLeadId = (TextView) findViewById(R.id.text_c_360_lead_id);
        tvPolicyEndDate = (TextView) findViewById(R.id.text_policy_end_date);
        tvPolicyEndDateTag = (TextView) findViewById(R.id.tv_policy_end_date_tag);
        llLeadId = findViewById(R.id.ll_i_lom_lead_details);
        relPolicyDetails = (RelativeLayout) findViewById(R.id.rel_i_lom_policy_details);
        tvOTBTag = findViewById(R.id.text_c_360_otb_tag);
        tvOTB = findViewById(R.id.text_c_360_otb);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"));
        tabLayout.addTab(tabLayout.newTab().setText("Tasks"));
        if(isClientILom) {
            tvC360LeadAge.setVisibility(View.INVISIBLE);
            tvLeadAgeTag.setVisibility(View.INVISIBLE);
            tvC360Car.setVisibility(View.GONE);
            tvEditPolicy.setVisibility(View.VISIBLE);
            relPolicyDetails.setVisibility(View.VISIBLE);
            llLeadId.setVisibility(View.VISIBLE);
            tvOTB.setVisibility(View.GONE);
            tvOTBTag.setVisibility(View.GONE);
            tvPolicyEndDateTag.setVisibility(View.VISIBLE);
        }
        else if (isClientILBank) {
            tvC360LeadAge.setVisibility(View.INVISIBLE);
            tvLeadAgeTag.setVisibility(View.INVISIBLE);
            tvC360Car.setVisibility(View.GONE);
            tvEditPolicy.setVisibility(View.GONE);
            relPolicyDetails.setVisibility(View.VISIBLE);
            llLeadId.setVisibility(View.VISIBLE);
            tvOTB.setVisibility(View.VISIBLE);
            tvOTBTag.setVisibility(View.VISIBLE);
            tvPolicyEndDateTag.setVisibility(View.GONE);
        }
        else {
            tvC360LeadAge.setVisibility(View.VISIBLE);
            tvLeadAgeTag.setVisibility(View.VISIBLE);
            relPolicyDetails.setVisibility(View.GONE);
            llLeadId.setVisibility(View.GONE);
            tvC360Car.setVisibility(View.VISIBLE);
            tabLayout.addTab(tabLayout.newTab().setText("Vehicles"));
            tabLayout.addTab(tabLayout.newTab().setText("Details"));
            tvEditPolicy.setVisibility(View.GONE);
        }


        tabLayout.setBackgroundColor(Color.TRANSPARENT);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        drawableActive = ContextCompat.getDrawable(getApplicationContext(), R.drawable.progress_oval_active);
        drawableCurrent = ContextCompat.getDrawable(getApplicationContext(), R.drawable.progress_oval_current);
        drawableInactive = ContextCompat.getDrawable(getApplicationContext(), R.drawable.progress_oval_inactive);
        for (int i = 0; i < relC360OvalHolder.getChildCount(); i++) {
            if (relC360OvalHolder.getChildAt(i) instanceof ImageView && relC360OvalHolder.getChildAt(i).getId() != R.id.tv_c360_indicator) {
                ovalImageList.add((ImageView) relC360OvalHolder.getChildAt(i));
            }
        }

        if(pref.getCurrentDseId()!=null) {
            if (pref.getCurrentDseId().intValue() != pref.getAppUserId().intValue()) {
                isCreatedTemp = true;
            }
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            searchResponse = (SearchResponse.Result) extras.getSerializable("online_search_data");
            fromNotificationTray = extras.getBoolean(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY,false);
            tabPosition = extras.getInt(WSConstants.C360_TAB_POSITION,0);
            if(fromNotificationTray){
                pref.setLeadID(extras.getString(WSConstants.LEAD_ID));
            }
        }

        if (searchResponse == null) {
            leadId = Util.getLong(pref.getLeadID());
        } else {
            leadId = Util.getLong(searchResponse.getLeadId());
        }

        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                      .equalTo("leadId", leadId)
                   // .equalTo("scheduledActivityId", Util.getInt(pref.getmSheduleActivityID()))
                      .equalTo("isCreatedTemporary", isCreatedTemp).findFirst();

        if (new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
            if (salesCRMRealmTable != null) {
                init(false);
            }
            getAllDetailsForUser();
        } else {
            if (salesCRMRealmTable != null) {
                init(true);
            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            }
        }

        ibActionBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               backPressed();
            }
        });


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                //WSConstants.c360TabId = tab.getPosition();
                /*if (tab.getPosition() == 0) {
                    fabC360.hide();
                } else
                    fabC360.show();*/
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        fabC360.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(!isFinishing()) {
                    fabFragmentAdded = true;
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.rel_bottom_frame, new FabFragment()).commitAllowingStateLoss();
                    fabC360.hide();
                }

            }

        });




    }


    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
        if(pref.isReloadC360()) {
            pref.setReloadC360(false);
            Intent intent = getIntent();
            intent.putExtra(WSConstants.C360_TAB_POSITION,1);
            finish();
            overridePendingTransition(0,0);
            startActivity(intent);
            return;
        }

        if(pref.isShowBalloonAnimation()) {
            ((BalloonAnimation) findViewById(R.id.view_balloon_animation)).start();
            pref.setShowBalloonAnimation(false);
        }


        //  callBookingsRetro();

        // getAllDetailsForUser();
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    protected void onStop() {
        System.out.println("On Stop Triggered");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("On Destroy Triggered");
        pref.setC360Destroyed(true);
        //   adapter.notifyDataSetChanged();
        realm.close();

    }

    @Override
    public void onBackPressed() {
        backPressed();
    }

    public void backPressed() {
        if(fabFragmentAdded) {
            fabC360.show();
            try {
                getSupportFragmentManager().popBackStack();
            }
            catch (Exception ignored) {

            }
            fabFragmentAdded = false;
        }
        else {
            finish();
            if(fromNotificationTray){
                startActivity(new Intent(C360Activity.this,HomeActivity.class));
            }
        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void getAllDetailsForUser() {
        relLoading.setVisibility(View.VISIBLE);
        fabC360.hide();
        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(leadId + "");
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetAllLeadInformation(leadData, this);
    }

    @Override
    public void failure(RetrofitError arg0) {
        Log.e("CardDetailsActivity", "error - " + arg0.toString());
        if(!isFinishing()) {
            new AutoDialog(C360Activity.this, new AutoDialogClickListener() {
                @Override
                public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
                    getAllDetailsForUser();
                }

                @Override
                public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {
                    C360Activity.this.finish();
                }
            }, new AutoDialogModel("Error while fetching the data", "Try Again", "Close")).show();
        }

    }


    @Override
    public void success(Object arg0, Response arg1) {
        AllLeadCustomerDetailsResponse customerDetailsResponse = (AllLeadCustomerDetailsResponse) arg0;
        List<Header> headerList = arg1.getHeaders();
        for (Header header : headerList) {
            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                ApiUtil.UpdateAccessToken(header.getValue());
            }
        }

        if(customerDetailsResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
            ApiUtil.InvalidUserLogout(C360Activity.this,0);
        }

        if (!customerDetailsResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + customerDetailsResponse.getMessage());
            new AutoDialog(C360Activity.this, new AutoDialogClickListener() {
                @Override
                public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
                    getAllDetailsForUser();
                }

                @Override
                public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {
                    C360Activity.this.finish();
                }
            }, new AutoDialogModel("Error while fetching the data", "Try Again", "Close")).show();
        }else  {
            if (customerDetailsResponse.getResult() != null) {
                System.out.println("Success:1" + customerDetailsResponse.getMessage());
                if (customerDetailsResponse.getResult().size() > 0) {
                    if(customerDetailsResponse.getResult().get(0).getCustomer_details().isIs_view_allowed()) {
                        cardDetailResponseData = customerDetailsResponse.getResult().get(0).getCustomer_details();

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {

                                System.out.print("DSE Details Location2 - "+cardDetailResponseData.getCustomerDetails().getLead_location().getId()+"- "+cardDetailResponseData.getCustomerDetails().getLead_location().getName());
                                C360InformationModel.getInstance().setCustomerDetails(cardDetailResponseData);
                                pref.setBuyerType(cardDetailResponseData.getCustomerDetails().getBuyer_type_id()+"");
                                addDataToDB(realm, cardDetailResponseData);

                            }
                        });
                        init(true);
                        updateViews();
                        LeadMobileMappingDbOperations.updateCallingMobileNumbers(realm);
                    }
                    else {
                        new AutoDialog(C360Activity.this, new AutoDialogClickListener() {
                            @Override
                            public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
                                getAllDetailsForUser();
                            }

                            @Override
                            public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {
                                C360Activity.this.finish();
                            }
                        }, new AutoDialogModel("Sorry!! You're not authorized to view this lead ", "Try Again", "Close")).show();
                    }




                }

            } else {
                System.out.println("Success:2" + customerDetailsResponse.getMessage());
            }
        }
    }

    @Override
    public void onAutoFormsDataFetched(boolean b, RealmResults<SalesCRMRealmTable> data) {
        init(true);
        updateViews();
    }

    @Override
    public void onAutoFormsDataError(RetrofitError error, int from) {
        init(true);
        updateViews();
    }

    private void updateViews() {
        if(isFinishing()) {
            return;
        }
        relLoading.setVisibility(View.GONE);
        tabLayout.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.VISIBLE);
        if(isLeadActive) {
            fabC360.show();
        }
        else {
            fabC360.hide();
        }

    }

    public void addDataToDB(Realm realm, AllLeadCustomerDetailsResponse.Result.Customer_details cardDetailResponseData) {
        isLeadActive = cardDetailResponseData == null || cardDetailResponseData.getCustomerDetails() == null || Util.getInt(cardDetailResponseData.getCustomerDetails().getLead_active()) != 0;
        realm.delete(AllInterestedCars.class);
        RealmList<CustomerPhoneNumbers> customerPhoneNumbers = new RealmList<CustomerPhoneNumbers>();
       /* CustomerPhoneNumbers customerPhoneNumbers = new CustomerPhoneNumbers();*/
        if (cardDetailResponseData.getCustomerDetails().getMobile_nos() != null) {
            for (int i = 0; i < cardDetailResponseData.getCustomerDetails().getMobile_nos().size(); i++) {
                CustomerPhoneNumbers customerPhoneNumbersObj = new CustomerPhoneNumbers();
                customerPhoneNumbersObj.setLeadId(Long.parseLong(cardDetailResponseData.getCustomerDetails().getLead_id()));
                customerPhoneNumbersObj.setCustomerID(Integer.parseInt(cardDetailResponseData.getCustomerDetails().getCustomer_id()));
                customerPhoneNumbersObj.setPhoneNumber(Long.parseLong(cardDetailResponseData.getCustomerDetails().getMobile_nos().get(i).getNumber()));
                customerPhoneNumbersObj.setPhoneNumberID(cardDetailResponseData.getCustomerDetails().getMobile_nos().get(i).getId());
                customerPhoneNumbersObj.setPhoneNumberStatus(cardDetailResponseData.getCustomerDetails().getMobile_nos().get(i).getStatus());
                customerPhoneNumbersObj.setLead_phone_mapping_id(cardDetailResponseData.getCustomerDetails().getMobile_nos().get(i).getLead_phone_mapping_id());
                //  System.out.println("PHONE NUMBER ADDED"+cardDetailResponseData.getCustomer_details().getCustomerDetails().getPhone_nos().get(i).getNumber()+" id: "+cardDetailResponseData.getCustomer_details().getCustomerDetails().getPhone_nos().get(i).get());
                customerPhoneNumbers.add(customerPhoneNumbersObj);
            }
        }

        //realm.copyToRealmOrUpdate(customerPhoneNumbers);

        RealmList<CustomerEmailId> customerEmailId = new RealmList<CustomerEmailId>();

        if (cardDetailResponseData.getCustomerDetails().getEmail_ids() != null) {
            for (int i = 0; i < cardDetailResponseData.getCustomerDetails().getEmail_ids().size(); i++) {
                CustomerEmailId customerEmailIdObj = new CustomerEmailId();
                customerEmailIdObj.setLeadId(Long.parseLong(cardDetailResponseData.getCustomerDetails().getLead_id()));
                customerEmailIdObj.setCustomerID(Integer.parseInt(cardDetailResponseData.getCustomerDetails().getCustomer_id()));
                customerEmailIdObj.setEmail(cardDetailResponseData.getCustomerDetails().getEmail_ids().get(i).getAddress());
                customerEmailIdObj.setEmailID(cardDetailResponseData.getCustomerDetails().getEmail_ids().get(i).getId());
                customerEmailIdObj.setEmailStatus(cardDetailResponseData.getCustomerDetails().getEmail_ids().get(i).getStatus());
                customerEmailIdObj.setLead_email_mapping_id(cardDetailResponseData.getCustomerDetails().getEmail_ids().get(i).getLead_email_mapping_id());
                customerEmailId.add(customerEmailIdObj);
                //System.out.println("CUSTOMER ID"+cardDetailResponseData.getCustomer_details().getCustomerDetails().getCustomer_id()+"CUSTOMER PHONE NUMBER SIZE"+customerEmailId.size());
            }

        }


        RealmResults<PlannedActivities> deleteOldPlanned = realm.where(PlannedActivities.class).equalTo("leadID", Long.parseLong(cardDetailResponseData.getCustomerDetails().getLead_id())).findAll();
        if (deleteOldPlanned.size() > 0) {
            deleteOldPlanned.deleteAllFromRealm();
        }
        PlannedActivities plannedActivities = new PlannedActivities();

        pref.setShowPostBookingTdVisit(false);

        for (int i = 0; i < cardDetailResponseData.getPlannedActivities().size(); i++) {
            plannedActivities.setPlannedActivityScheduleId(Integer.parseInt(cardDetailResponseData.getPlannedActivities().get(i).getScheduled_activity_id()));
            plannedActivities.setLeadID(Long.parseLong(cardDetailResponseData.getCustomerDetails().getLead_id()));
            plannedActivities.setPlannedActivitiesIconType(cardDetailResponseData.getPlannedActivities().get(i).getIcon_type());
            plannedActivities.setPlannedActivitiesName(cardDetailResponseData.getPlannedActivities().get(i).getName());
            plannedActivities.setPlannedActivitiesDateTime(cardDetailResponseData.getPlannedActivities().get(i).getDate_time());
            plannedActivities.setActivityId(cardDetailResponseData.getPlannedActivities().get(i).getActivity_id());

            String activityId = cardDetailResponseData.getPlannedActivities().get(i).getActivity_id();
            if(Util.getInt(activityId) == WSConstants.TaskActivityName.NO_CAR_BOOKED_ID ||
                    Util.getInt(activityId) == WSConstants.TaskActivityName.RECEIVE_PAYMENT_ID ||
                    Util.getInt(activityId) == WSConstants.TaskActivityName.INVOICE_REQUEST_ID ||
                    Util.getInt(activityId) == WSConstants.TaskActivityName.DELIVERY_REQUEST_ID
                            ) {
                pref.setShowPostBookingTdVisit(true);

            }
            plannedActivities.plannedActivitiesDetail.clear();
           /* for (int j = 0; j < cardDetailResponseData.getPlannedActivities().get(i).getDetails().size(); j++) {*/
            for (int j = 0; j <(cardDetailResponseData.getPlannedActivities().get(i).getDetails()==null?0: cardDetailResponseData.getPlannedActivities().get(i).getDetails().size()); j++) {
                PlannedActivitiesDetail plannedActivitiesDetail = new PlannedActivitiesDetail();
                plannedActivitiesDetail.setPlannedActivitiesDetailIconTypes(cardDetailResponseData.getPlannedActivities().get(i).getDetails().get(j).getIcon_type());
                plannedActivitiesDetail.setPlannedActivitiesDetailKeys(cardDetailResponseData.getPlannedActivities().get(i).getDetails().get(j).getKey());
                plannedActivitiesDetail.setPlannedActivitiesDetailValues(cardDetailResponseData.getPlannedActivities().get(i).getDetails().get(j).getValue());
                //  realm.copyToRealmOrUpdate(plannedActivitiesDetail);
                plannedActivities.plannedActivitiesDetail.add(plannedActivitiesDetail);
                // plannedActivitiesDetail.deleteFromRealm();
            }

            // realm.copyToRealmOrUpdate(plannedActivitiesDetail);
            RealmList<PlannedActivitiesAction> plannedActivitiesAction = new RealmList<PlannedActivitiesAction>();
            plannedActivities.plannedActivitiesAction.clear();
            /*for (int k = 0; k < cardDetailResponseData.getPlannedActivities().get(i).getActions().size(); k++) {*/
            for (int k = 0; k <(cardDetailResponseData.getPlannedActivities().get(i).getActions()==null?0:cardDetailResponseData.getPlannedActivities().get(i).getActions().size()); k++) {
                PlannedActivitiesAction plannedActivitiesActionObj = new PlannedActivitiesAction();
                plannedActivitiesActionObj.setPlannedActivitiesAction(cardDetailResponseData.getPlannedActivities().get(i).getActions().get(k).getAction());
                plannedActivitiesActionObj.setPlannedActivitiesActionsIconType(cardDetailResponseData.getPlannedActivities().get(i).getActions().get(k).getIcon_type());
                plannedActivitiesActionObj.setPlannedActivitiesActionsId(Integer.parseInt(cardDetailResponseData.getPlannedActivities().get(i).getActions().get(k).getAction_id()));

                // realm.copyToRealmOrUpdate(plannedActivitiesAction);
                plannedActivitiesAction.add(plannedActivitiesActionObj);
                // plannedActivitiesAction.deleteFromRealm();
            }
            plannedActivities.plannedActivitiesAction.addAll(plannedActivitiesAction);
            //plannedActivities.plannedActivitiesAction.add(plannedActivitiesAction);
            realm.copyToRealmOrUpdate(plannedActivities);
        }


        // realm.copyToRealmOrUpdate(plannedActivitiesAction);


        if (cardDetailResponseData.getCustomerDetails().getDse_details() != null && cardDetailResponseData.getCustomerDetails().getDse_details().getDse_id() != null) {
            DseDetails dseDetails = new DseDetails();
            dseDetails.setCustomerID(Integer.parseInt(cardDetailResponseData.getCustomerDetails().getLead_id()));
            dseDetails.setDseDetailId(Integer.parseInt(cardDetailResponseData.getCustomerDetails().getDse_details().getId()));
            dseDetails.setDseId(cardDetailResponseData.getCustomerDetails().getDse_details().getDse_id());
            dseDetails.setDseMobile(cardDetailResponseData.getCustomerDetails().getDse_details().getDse_mobNo());
            dseDetails.setDseName(cardDetailResponseData.getCustomerDetails().getDse_details().getDse_name());

            realm.copyToRealmOrUpdate(dseDetails);
        }


        System.out.println("FirstSize" + cardDetailResponseData.getInterestedCarsDetails().size());

        for (int i = 0; i < cardDetailResponseData.getInterestedCarsDetails().size(); i++) {
            IntrestedCarDetails intrestedCarDetails = new IntrestedCarDetails();
            intrestedCarDetails.setLeadId(Long.parseLong(cardDetailResponseData.getCustomerDetails().getLead_id()));
            intrestedCarDetails.setIntrestedLeadCarId(Integer.parseInt(cardDetailResponseData.getInterestedCarsDetails().get(i).getLead_car_id()));
            intrestedCarDetails.setIntrestedCarName(cardDetailResponseData.getInterestedCarsDetails().get(i).getName());
            intrestedCarDetails.setIntrestedCarVariantCode(cardDetailResponseData.getInterestedCarsDetails().get(i).getVariant_code());
            intrestedCarDetails.setIntrestedCarVariantId(cardDetailResponseData.getInterestedCarsDetails().get(i).getVariant_id());
            intrestedCarDetails.setIntrestedCarColorId(cardDetailResponseData.getInterestedCarsDetails().get(i).getColor_id());
            intrestedCarDetails.setIntrestedCarModelId(cardDetailResponseData.getInterestedCarsDetails().get(i).getModel_id());
            intrestedCarDetails.setIntrestedModelName(cardDetailResponseData.getInterestedCarsDetails().get(i).getModel());
            intrestedCarDetails.setIntrestedCarIsPrimary(Integer.parseInt(cardDetailResponseData.getInterestedCarsDetails().get(i).getIs_primary()));
            intrestedCarDetails.setTestDriveStatus(Integer.parseInt(cardDetailResponseData.getInterestedCarsDetails().get(i).getTest_drive_status()));
            intrestedCarDetails.setColor(cardDetailResponseData.getInterestedCarsDetails().get(i).getColor());
            intrestedCarDetails.setVariant(cardDetailResponseData.getInterestedCarsDetails().get(i).getVariant());
            intrestedCarDetails.setFuelType(cardDetailResponseData.getInterestedCarsDetails().get(i).getFuel_type());
            intrestedCarDetails.setFuelId(cardDetailResponseData.getInterestedCarsDetails().get(i).getFuel_type_id());
            for (int j = 0; j < cardDetailResponseData.getInterestedCarsDetails().get(i).getCar_activity_groups().size(); j++) {//2

                IntrestedCarActivityGroup intrestedCarActivityGroup = new IntrestedCarActivityGroup();
                intrestedCarActivityGroup.setLeadId(Long.parseLong(cardDetailResponseData.getCustomerDetails().getLead_id()));
                intrestedCarActivityGroup.setIntrestedCarDetailActivityGroupName(cardDetailResponseData.getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getGroup_name());
                for (int k = 0; k < cardDetailResponseData.getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getActivity_stages().size(); k++) {

                    //realm.createObject(IntrestedCarActivityGroupStages.class);
                    IntrestedCarActivityGroupStages intrestedCarActivityGroupStages = new IntrestedCarActivityGroupStages();
                    intrestedCarActivityGroupStages.setLeadId(Long.parseLong(cardDetailResponseData.getCustomerDetails().getLead_id()));
                    intrestedCarActivityGroupStages.setIntrestedCarActivityStageID(cardDetailResponseData.getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getActivity_stages().get(k).getStage_id());
                    intrestedCarActivityGroupStages.setIntrestedCarStageBarClass(cardDetailResponseData.getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getActivity_stages().get(k).getBar_class());
                    intrestedCarActivityGroupStages.setIntrestedCarStageName(cardDetailResponseData.getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getActivity_stages().get(k).getName());
                    intrestedCarActivityGroupStages.setIntrestedCarStageWidth(cardDetailResponseData.getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getActivity_stages().get(k).getWidth());
                    intrestedCarActivityGroup.IntrestedCarActivityGroupStages.add(intrestedCarActivityGroupStages);

                }
                intrestedCarDetails.intrestedCarActivityGroup.add(intrestedCarActivityGroup);
            }
            List<ApiInputBookingDetails> apiInputBookingDetails = new ArrayList<>();
            for (int x = 0; x < cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().size(); x++) {
                apiInputBookingDetails.add(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getBooking_details());
                CarDmsDataDB carDmsDataDB = new CarDmsDataDB();

                carDmsDataDB.setCarId(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getEnquiry_id());
                carDmsDataDB.setBooking_number(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getBooking_number());
                System.out.println("ENQUIRY NUMBER BUG FIX" + cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getEnquiry_number());
                carDmsDataDB.setEnquiry_number(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getEnquiry_number());
                carDmsDataDB.setLocation_code(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getLocation_code());
                carDmsDataDB.setBooking_number(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getBooking_number());
                carDmsDataDB.setBooking_id(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getBooking_id());
                carDmsDataDB.setEnquiry_id(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getEnquiry_id());
                carDmsDataDB.setCar_stage_id(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getCar_stage_id());
                carDmsDataDB.setInvoice_number(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_number());
                carDmsDataDB.setInvoice_id(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_id());
                carDmsDataDB.setExpected_delivery_date(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getExpected_delivery_date());
                carDmsDataDB.setBooking_amount(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getBooking_amount());

                carDmsDataDB.setInvoice_name(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_name());
                carDmsDataDB.setInvoice_mobile_no(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_mob_no());
                carDmsDataDB.setInvoice_vin_no(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_vin_no());
                carDmsDataDB.setInvoice_date(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_date());
                carDmsDataDB.setBooking_amount(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getBooking_amount());
                carDmsDataDB.setDelivery_date(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getDelivery_date());
                carDmsDataDB.setDelivery_challan(cardDetailResponseData.getInterestedCarsDetails().get(i).getCarDmsData().get(x).getDelivery_number());
                intrestedCarDetails.carDmsDataList.add(carDmsDataDB);
            }

            realm.copyToRealmOrUpdate(intrestedCarDetails);
            CarsDBHandler.getInstance().createAllCarsList(realm, intrestedCarDetails,
                    cardDetailResponseData.getCustomerDetails().getName(), apiInputBookingDetails);


        }
        for (int i = 0; i < cardDetailResponseData.getExchangeCarsDetails().size(); i++) {
            ExchangeCarDetails exchangeCarDetails = new ExchangeCarDetails();
            exchangeCarDetails.setLeadId(Long.parseLong(cardDetailResponseData.getCustomerDetails().getLead_id()));
            exchangeCarDetails.setExchangecarname(cardDetailResponseData.getExchangeCarsDetails().get(i).getName());
            exchangeCarDetails.setLead_exchange_car_id(cardDetailResponseData.getExchangeCarsDetails().get(i).getLead_exchange_car_id());
            exchangeCarDetails.setExpected_price(cardDetailResponseData.getExchangeCarsDetails().get(i).getExpected_price());
            exchangeCarDetails.setMarket_price(cardDetailResponseData.getExchangeCarsDetails().get(i).getMarket_price());
            exchangeCarDetails.setModel_id(cardDetailResponseData.getExchangeCarsDetails().get(i).getModel_id());
            exchangeCarDetails.setPrice_quoted(cardDetailResponseData.getExchangeCarsDetails().get(i).getPrice_quoted());
            exchangeCarDetails.setPurchase_date(cardDetailResponseData.getExchangeCarsDetails().get(i).getPurchase_date());
            exchangeCarDetails.setKms_run(cardDetailResponseData.getExchangeCarsDetails().get(i).getKms_run());
            exchangeCarDetails.setReg_no(cardDetailResponseData.getExchangeCarsDetails().get(i).getReg_no());
            exchangeCarDetails.setCar_stage_id(cardDetailResponseData.getExchangeCarsDetails().get(i).getStage_id());
            exchangeCarDetails.setEvaluator_name(cardDetailResponseData.getExchangeCarsDetails().get(i).getEvaluator_name());
            exchangeCarDetails.setMake_year(cardDetailResponseData.getExchangeCarsDetails().get(i).getMake_year());
            exchangeCarDetails.setOwner_type(cardDetailResponseData.getExchangeCarsDetails().get(i).getOwner_type());
            if(cardDetailResponseData.getExchangeCarsDetails().get(i).getActivityDetails()!=null){
                //exchangeCarDetails.setEvaluator_name(cardDetailResponseData.getExchangeCarsDetails().get(i).getActivityDetails().getUser_name());
                exchangeCarDetails.setEvaluation_date_time(cardDetailResponseData.getExchangeCarsDetails().get(i).getActivityDetails().getScheduled_at());
                exchangeCarDetails.setEvaluation_remarks(cardDetailResponseData.getExchangeCarsDetails().get(i).getActivityDetails().getRemarks());
                exchangeCarDetails.setActivity_id(cardDetailResponseData.getExchangeCarsDetails().get(i).getActivityDetails().getActivity()+"");
                exchangeCarDetails.setScheduled_at(cardDetailResponseData.getExchangeCarsDetails().get(i).getActivityDetails().getScheduled_at());
                exchangeCarDetails.setRemarks(cardDetailResponseData.getExchangeCarsDetails().get(i).getActivityDetails().getRemarks());
                exchangeCarDetails.setScheduled_activity_id(cardDetailResponseData.getExchangeCarsDetails().get(i).getActivityDetails().getActivity_id()+"");
                exchangeCarDetails.setUser_name(cardDetailResponseData.getExchangeCarsDetails().get(i).getActivityDetails().getUser_name());
            }

            for (int j = 0; j < cardDetailResponseData.getExchangeCarsDetails().get(i).getExchange_status().length; j++) {
                ExchangeStatusdb exchangeStatusdb = new ExchangeStatusdb();
                exchangeStatusdb.setLeadId(Long.parseLong(cardDetailResponseData.getCustomerDetails().getLead_id()));
                exchangeStatusdb.setBar_class(cardDetailResponseData.getExchangeCarsDetails().get(i).getExchange_status()[j].getBar_class());
                exchangeStatusdb.setExchangestatusname(cardDetailResponseData.getExchangeCarsDetails().get(i).getExchange_status()[j].getName());
                exchangeStatusdb.setWidth(cardDetailResponseData.getExchangeCarsDetails().get(i).getExchange_status()[j].getWidth());
                exchangeStatusdb.setStage_id(cardDetailResponseData.getExchangeCarsDetails().get(i).getExchange_status()[j].getStage_id());
                exchangeCarDetails.exchangestatus.add(exchangeStatusdb);
            }
            realm.copyToRealmOrUpdate(exchangeCarDetails);

        }

        for (int i = 0; i < cardDetailResponseData.getAdditionalCarsDetails().size(); i++) {
            AdditionalCarsDetails additionalCarsDetails = new AdditionalCarsDetails();
            additionalCarsDetails.setLeadId(Long.parseLong(cardDetailResponseData.getCustomerDetails().getLead_id()));
            additionalCarsDetails.setName(cardDetailResponseData.getAdditionalCarsDetails().get(i).getName());
            additionalCarsDetails.setModel_id(cardDetailResponseData.getAdditionalCarsDetails().get(i).getModel_id());
            additionalCarsDetails.setPrice_quoted(cardDetailResponseData.getAdditionalCarsDetails().get(i).getPrice_quoted());
            additionalCarsDetails.setMarket_price(cardDetailResponseData.getAdditionalCarsDetails().get(i).getMarket_price());
            additionalCarsDetails.setExpected_price(cardDetailResponseData.getAdditionalCarsDetails().get(i).getExpected_price());
            additionalCarsDetails.setPurchase_date(cardDetailResponseData.getAdditionalCarsDetails().get(i).getPurchase_date());
            additionalCarsDetails.setKms_run(cardDetailResponseData.getAdditionalCarsDetails().get(i).getKms_run());
            additionalCarsDetails.setReg_no(cardDetailResponseData.getAdditionalCarsDetails().get(i).getReg_no());

            realm.copyToRealmOrUpdate(additionalCarsDetails);

        }


        if (salesCRMRealmTable == null) {
            System.out.print("DSE Details A - "+ salesCRMRealmTable);
            salesCRMRealmTable = new SalesCRMRealmTable();
            salesCRMRealmTable.setScheduledActivityId(Integer.parseInt(cardDetailResponseData.getCustomerDetails().getLead_id()));
            salesCRMRealmTable.setCreatedTemporary(true);
            salesCRMRealmTable.setActivityScheduleDate(Util.getDateTime(0));
        } else if(!salesCRMRealmTable.isValid()){
            salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                    .equalTo("leadId", leadId)
                    // .equalTo("scheduledActivityId", Util.getInt(pref.getmSheduleActivityID()))
                    .equalTo("isCreatedTemporary", isCreatedTemp).findFirst();
            if (salesCRMRealmTable != null) {
                salesCRMRealmTable.setCreatedTemporary(false);
            }
        }
        else {
            salesCRMRealmTable.setCreatedTemporary(false);
        }

            salesCRMRealmTable.setCustomerId(Integer.parseInt(C360Activity.cardDetailResponseData.getCustomerDetails().getCustomer_id()));
            salesCRMRealmTable.setCustomerName(C360Activity.cardDetailResponseData.getCustomerDetails().getFirst_name() + " " + C360Activity.cardDetailResponseData.getCustomerDetails().getLast_name());
            salesCRMRealmTable.setGender(C360Activity.cardDetailResponseData.getCustomerDetails().getGender());
            salesCRMRealmTable.setTitle(C360Activity.cardDetailResponseData.getCustomerDetails().getTitle());
            salesCRMRealmTable.setTemplateExist(C360Activity.cardDetailResponseData.getCustomerDetails().isTemplate_exist());
            String customerAge = C360Activity.cardDetailResponseData.getCustomerDetails().getCustomer_age();
            salesCRMRealmTable.setCustomerAge(customerAge.equals("") ? null : Integer.parseInt(customerAge) + "");
            if (C360Activity.cardDetailResponseData.getCustomerDetails().getCompany_name() != null) {
                salesCRMRealmTable.setCompanyName(C360Activity.cardDetailResponseData.getCustomerDetails().getCompany_name());
            }

            salesCRMRealmTable.setResidencePinCode(C360Activity.cardDetailResponseData.getCustomerDetails().getPin_code());
            salesCRMRealmTable.setResidenceAddress(C360Activity.cardDetailResponseData.getCustomerDetails().getAddress());
            salesCRMRealmTable.setResidenceLocality(C360Activity.cardDetailResponseData.getCustomerDetails().getLocality());
            salesCRMRealmTable.setResidenceCity(C360Activity.cardDetailResponseData.getCustomerDetails().getCity());
            salesCRMRealmTable.setResidenceState(C360Activity.cardDetailResponseData.getCustomerDetails().getState());

            salesCRMRealmTable.setOfficePinCode(C360Activity.cardDetailResponseData.getCustomerDetails().getOffice_pin_code());
            salesCRMRealmTable.setOfficeAddress(C360Activity.cardDetailResponseData.getCustomerDetails().getOffice_address());
            salesCRMRealmTable.setOfficeLocality(C360Activity.cardDetailResponseData.getCustomerDetails().getOffice_locality());
            salesCRMRealmTable.setOfficeCity(C360Activity.cardDetailResponseData.getCustomerDetails().getOffice_city());
            salesCRMRealmTable.setOfficeState(C360Activity.cardDetailResponseData.getCustomerDetails().getState());

            salesCRMRealmTable.setCompanyName(C360Activity.cardDetailResponseData.getCustomerDetails().getCompany_name());
            salesCRMRealmTable.setLeadId(Long.parseLong(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_id()));
            if (searchResponse != null) {
                salesCRMRealmTable.setLeadCarVariantName(searchResponse.getVariant_name());
                salesCRMRealmTable.setLeadTagsName(searchResponse.getTag_name());
            }

            if (C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details() != null) {
                salesCRMRealmTable.setLeadDseName(C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details().getDse_name());
                salesCRMRealmTable.setLeadDsemobileNumber(C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details().getDse_mobNo());
            }
            salesCRMRealmTable.setLeadAge(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_age());
            salesCRMRealmTable.setLeadSourceName(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_source());
            salesCRMRealmTable.setVinRegNo(C360Activity.cardDetailResponseData.getCustomerDetails().getRef_vin_reg_no());
            salesCRMRealmTable.setLeadSourceId(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_source_id());
            System.out.println("LEAD ID" + C360Activity.cardDetailResponseData.getCustomerDetails().getLead_id());
            if (C360Activity.cardDetailResponseData.getCustomerDetails().getLead_id().equalsIgnoreCase("109939")) {
                System.out.println("LEAD ID" + C360Activity.cardDetailResponseData.getCustomerDetails().getLead_id() +
                        "SERVER LAST LEAD UPDATED" + C360Activity.cardDetailResponseData.getCustomerDetails().getLead_last_updated());
            }
            salesCRMRealmTable.setLeadLastUpdated(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_last_updated());
            salesCRMRealmTable.setLeadExpectedClosingDateAsNormalString(C360Activity.cardDetailResponseData.getCustomerDetails().getClose_date());
            salesCRMRealmTable.setCustomerDesignation(C360Activity.cardDetailResponseData.getCustomerDetails().getDesignation());
            salesCRMRealmTable.setCustomerOccupation(C360Activity.cardDetailResponseData.getCustomerDetails().getOccupation());
            salesCRMRealmTable.setCustomerAddress(C360Activity.cardDetailResponseData.getCustomerDetails().getFull_address());
            salesCRMRealmTable.setOemSource(C360Activity.cardDetailResponseData.getCustomerDetails().getOem_source());
            salesCRMRealmTable.setCloseDate(C360Activity.cardDetailResponseData.getCustomerDetails().getClose_date());
            salesCRMRealmTable.setFirstName(C360Activity.cardDetailResponseData.getCustomerDetails().getFirst_name());
            salesCRMRealmTable.setLastName(C360Activity.cardDetailResponseData.getCustomerDetails().getLast_name());
            salesCRMRealmTable.setTypeOfCustomer(C360Activity.cardDetailResponseData.getCustomerDetails().getType_of_customer());
            salesCRMRealmTable.setModeOfPayment(C360Activity.cardDetailResponseData.getCustomerDetails().getMode_of_payment());
            salesCRMRealmTable.setBuyerType(C360Activity.cardDetailResponseData.getCustomerDetails().getBuyer_type_name());
            salesCRMRealmTable.setBuyerTypeId(C360Activity.cardDetailResponseData.getCustomerDetails().getBuyer_type_id());
       /* System.out.println("ENQUIRY NUMBER FROM RESPONSE C360"+cardDetailResponseData.getInterestedCarsDetails().get);
        salesCRMRealmTable.setEnqnumber(cardDetailResponseData.getCustomerDetails().getEnquiry_number());*/
            salesCRMRealmTable.customerPhoneNumbers = new RealmList<>();
            salesCRMRealmTable.customerPhoneNumbers.addAll(customerPhoneNumbers);
            salesCRMRealmTable.customerEmailId = new RealmList<>();
            salesCRMRealmTable.customerEmailId.addAll(customerEmailId);

            //new code added to keep the temp leads as temp
            salesCRMRealmTable.setCreatedTemporary(isCreatedTemp);


            RealmList<LeadStageProgressDB> leadStageProgressDBsList = new RealmList<>();
            for (int k = 0;
                 k < C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress().length;
                 k++) {

                AllLeadCustomerDetailsResponse.Result.Customer_details.CustomerDetails.Lead_stage_progress
                        currentData = C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress()[k];
                LeadStageProgressDB curProgressDB = realm.createObject(LeadStageProgressDB.class);
                curProgressDB.setStageId(Util.getInt(currentData.getStage_id()));
                curProgressDB.setName(currentData.getName());
                curProgressDB.setWidth(currentData.getWidth());
                curProgressDB.setBarClass(currentData.getBar_class());

                leadStageProgressDBsList.add(curProgressDB);

            }
            salesCRMRealmTable.setLeadStageProgressDB(leadStageProgressDBsList);

            for (int k = C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress().length - 1;
                 k >= 0; k--) {
                if (C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress()[k].getWidth() == 100) {
                    salesCRMRealmTable.setLeadStageId(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress()[k].getStage_id());
                    salesCRMRealmTable.setLeadStage(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress()[k].getName());
                    break;
                }
            }
            if (C360Activity.cardDetailResponseData.getCustomerDetails().getLead_tags().size() > 0) {
                salesCRMRealmTable.setLeadTagsName(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_tags().get(0).getName());
                salesCRMRealmTable.setLeadTagsColor(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_tags().get(0).getColor());
            }

            for (int k = 0; k < C360Activity.cardDetailResponseData.getInterestedCarsDetails().size(); k++) {
                if (C360Activity.cardDetailResponseData.getInterestedCarsDetails().get(k).getIs_primary().equalsIgnoreCase("1")) {
                    salesCRMRealmTable.setLeadCarModelId(C360Activity.cardDetailResponseData.getInterestedCarsDetails().get(k).getModel_id());
                    salesCRMRealmTable.setLeadCarVariantName(C360Activity.cardDetailResponseData.getInterestedCarsDetails().get(k).getVariant());
                    salesCRMRealmTable.setLeadCarModelName(C360Activity.cardDetailResponseData.getInterestedCarsDetails().get(k).getModel());
                    break;
                }
            }

            if (C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details() != null) {
                salesCRMRealmTable.setLeadDseName(C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details().getDse_name());
                salesCRMRealmTable.setLeadDsemobileNumber(C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details().getDse_mobNo());
            }
            salesCRMRealmTable.setIsLeadActive(Integer.parseInt(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_active()));
            if (Integer.parseInt(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_active()) == 0) {
                RealmResults<SalesCRMRealmTable> invalidData = realm.where(SalesCRMRealmTable.class).equalTo("leadId", Long.parseLong(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_id())).findAll();
                for (int z = 0; z < invalidData.size(); z++) {
                    invalidData.get(z).setIsLeadActive(0);
                }
            }
            salesCRMRealmTable.setLeadAge(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_age());
            System.out.print("DSE Details Location3 - "+ C360Activity.cardDetailResponseData.getCustomerDetails().getLead_location().getId()+"- "+ C360Activity.cardDetailResponseData.getCustomerDetails().getLead_location().getName());
            salesCRMRealmTable.setLeadLocationId(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_location().getId());
            salesCRMRealmTable.setLeadLocationName(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_location().getName());
            realm.copyToRealmOrUpdate(salesCRMRealmTable);


            insertToAll(customerPhoneNumbers,customerEmailId,cardDetailResponseData);

    }

    private void insertToAll(RealmList<CustomerPhoneNumbers> customerPhoneNumbers, RealmList<CustomerEmailId> customerEmailId, AllLeadCustomerDetailsResponse.Result.Customer_details cardDetailResponseData) {
        RealmResults<SalesCRMRealmTable> salesData = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId).findAll();
        for(int i=0;i<salesData.size();i++)
        {
            SalesCRMRealmTable salesCRMRealmTable = salesData.get(i);
            salesCRMRealmTable.setCustomerId(Integer.parseInt(C360Activity.cardDetailResponseData.getCustomerDetails().getCustomer_id()));
            salesCRMRealmTable.setCustomerName(C360Activity.cardDetailResponseData.getCustomerDetails().getFirst_name() + " " + C360Activity.cardDetailResponseData.getCustomerDetails().getLast_name());
            salesCRMRealmTable.setGender(C360Activity.cardDetailResponseData.getCustomerDetails().getGender());
            salesCRMRealmTable.setTitle(C360Activity.cardDetailResponseData.getCustomerDetails().getTitle());
            salesCRMRealmTable.setTemplateExist(C360Activity.cardDetailResponseData.getCustomerDetails().isTemplate_exist());
            String customerAge = C360Activity.cardDetailResponseData.getCustomerDetails().getCustomer_age();
            salesCRMRealmTable.setCustomerAge(customerAge.equals("") ? null : Integer.parseInt(customerAge) + "");
            if (C360Activity.cardDetailResponseData.getCustomerDetails().getCompany_name() != null) {
                salesCRMRealmTable.setCompanyName(C360Activity.cardDetailResponseData.getCustomerDetails().getCompany_name());
            }

            salesCRMRealmTable.setResidencePinCode(C360Activity.cardDetailResponseData.getCustomerDetails().getPin_code());
            salesCRMRealmTable.setResidenceAddress(C360Activity.cardDetailResponseData.getCustomerDetails().getAddress());
            salesCRMRealmTable.setResidenceLocality(C360Activity.cardDetailResponseData.getCustomerDetails().getLocality());
            salesCRMRealmTable.setResidenceCity(C360Activity.cardDetailResponseData.getCustomerDetails().getCity());
            salesCRMRealmTable.setResidenceState(C360Activity.cardDetailResponseData.getCustomerDetails().getState());

            salesCRMRealmTable.setOfficePinCode(C360Activity.cardDetailResponseData.getCustomerDetails().getOffice_pin_code());
            salesCRMRealmTable.setOfficeAddress(C360Activity.cardDetailResponseData.getCustomerDetails().getOffice_address());
            salesCRMRealmTable.setOfficeLocality(C360Activity.cardDetailResponseData.getCustomerDetails().getOffice_locality());
            salesCRMRealmTable.setOfficeCity(C360Activity.cardDetailResponseData.getCustomerDetails().getOffice_city());
            salesCRMRealmTable.setOfficeState(C360Activity.cardDetailResponseData.getCustomerDetails().getState());

            salesCRMRealmTable.setCompanyName(C360Activity.cardDetailResponseData.getCustomerDetails().getCompany_name());
            salesCRMRealmTable.setLeadId(Long.parseLong(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_id()));
            if (searchResponse != null) {
                salesCRMRealmTable.setLeadCarVariantName(searchResponse.getVariant_name());
                salesCRMRealmTable.setLeadTagsName(searchResponse.getTag_name());
            }

            if (C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details() != null) {
                salesCRMRealmTable.setLeadDseName(C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details().getDse_name());
                salesCRMRealmTable.setLeadDsemobileNumber(C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details().getDse_mobNo());
            }
            salesCRMRealmTable.setLeadAge(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_age());
            salesCRMRealmTable.setLeadSourceName(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_source());
            salesCRMRealmTable.setVinRegNo(C360Activity.cardDetailResponseData.getCustomerDetails().getRef_vin_reg_no());
            salesCRMRealmTable.setLeadSourceId(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_source_id());
            System.out.println("LEAD ID" + C360Activity.cardDetailResponseData.getCustomerDetails().getLead_id());
            if (C360Activity.cardDetailResponseData.getCustomerDetails().getLead_id().equalsIgnoreCase("109939")) {
                System.out.println("LEAD ID" + C360Activity.cardDetailResponseData.getCustomerDetails().getLead_id() +
                        "SERVER LAST LEAD UPDATED" + C360Activity.cardDetailResponseData.getCustomerDetails().getLead_last_updated());
            }
            salesCRMRealmTable.setLeadLastUpdated(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_last_updated());
            salesCRMRealmTable.setLeadExpectedClosingDateAsNormalString(C360Activity.cardDetailResponseData.getCustomerDetails().getClose_date());
            salesCRMRealmTable.setCustomerDesignation(C360Activity.cardDetailResponseData.getCustomerDetails().getDesignation());
            salesCRMRealmTable.setCustomerOccupation(C360Activity.cardDetailResponseData.getCustomerDetails().getOccupation());
            salesCRMRealmTable.setCustomerAddress(C360Activity.cardDetailResponseData.getCustomerDetails().getFull_address());
            salesCRMRealmTable.setOemSource(C360Activity.cardDetailResponseData.getCustomerDetails().getOem_source());
            salesCRMRealmTable.setCloseDate(C360Activity.cardDetailResponseData.getCustomerDetails().getClose_date());
            salesCRMRealmTable.setFirstName(C360Activity.cardDetailResponseData.getCustomerDetails().getFirst_name());
            salesCRMRealmTable.setLastName(C360Activity.cardDetailResponseData.getCustomerDetails().getLast_name());
            salesCRMRealmTable.setTypeOfCustomer(C360Activity.cardDetailResponseData.getCustomerDetails().getType_of_customer());
            salesCRMRealmTable.setModeOfPayment(C360Activity.cardDetailResponseData.getCustomerDetails().getMode_of_payment());
            salesCRMRealmTable.setBuyerType(C360Activity.cardDetailResponseData.getCustomerDetails().getBuyer_type_name());
            salesCRMRealmTable.setBuyerTypeId(C360Activity.cardDetailResponseData.getCustomerDetails().getBuyer_type_id());
       /* System.out.println("ENQUIRY NUMBER FROM RESPONSE C360"+cardDetailResponseData.getInterestedCarsDetails().get);
        salesCRMRealmTable.setEnqnumber(cardDetailResponseData.getCustomerDetails().getEnquiry_number());*/
            salesCRMRealmTable.customerPhoneNumbers = new RealmList<>();
            salesCRMRealmTable.customerPhoneNumbers.addAll(customerPhoneNumbers);
            salesCRMRealmTable.customerEmailId = new RealmList<>();
            salesCRMRealmTable.customerEmailId.addAll(customerEmailId);

            //new code added to keep the temp leads as temp
            salesCRMRealmTable.setCreatedTemporary(isCreatedTemp);


            RealmList<LeadStageProgressDB> leadStageProgressDBsList = new RealmList<>();
            for (int k = 0;
                 k < C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress().length;
                 k++) {

                AllLeadCustomerDetailsResponse.Result.Customer_details.CustomerDetails.Lead_stage_progress
                        currentData = C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress()[k];
                LeadStageProgressDB curProgressDB = realm.createObject(LeadStageProgressDB.class);
                curProgressDB.setStageId(Util.getInt(currentData.getStage_id()));
                curProgressDB.setName(currentData.getName());
                curProgressDB.setWidth(currentData.getWidth());
                curProgressDB.setBarClass(currentData.getBar_class());

                leadStageProgressDBsList.add(curProgressDB);

            }
            salesCRMRealmTable.setLeadStageProgressDB(leadStageProgressDBsList);

            for (int k = C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress().length - 1;
                 k >= 0; k--) {
                if (C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress()[k].getWidth() == 100) {
                    salesCRMRealmTable.setLeadStageId(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress()[k].getStage_id());
                    salesCRMRealmTable.setLeadStage(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_stage_progress()[k].getName());
                    break;
                }
            }
            if (C360Activity.cardDetailResponseData.getCustomerDetails().getLead_tags().size() > 0) {
                salesCRMRealmTable.setLeadTagsName(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_tags().get(0).getName());
                salesCRMRealmTable.setLeadTagsColor(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_tags().get(0).getColor());
            }

            for (int k = 0; k < C360Activity.cardDetailResponseData.getInterestedCarsDetails().size(); k++) {
                if (C360Activity.cardDetailResponseData.getInterestedCarsDetails().get(k).getIs_primary().equalsIgnoreCase("1")) {
                    salesCRMRealmTable.setLeadCarModelId(C360Activity.cardDetailResponseData.getInterestedCarsDetails().get(k).getModel_id());
                    salesCRMRealmTable.setLeadCarVariantName(C360Activity.cardDetailResponseData.getInterestedCarsDetails().get(k).getVariant());
                    salesCRMRealmTable.setLeadCarModelName(C360Activity.cardDetailResponseData.getInterestedCarsDetails().get(k).getModel());
                    break;
                }
            }

            if (C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details() != null) {
                salesCRMRealmTable.setLeadDseName(C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details().getDse_name());
                salesCRMRealmTable.setLeadDsemobileNumber(C360Activity.cardDetailResponseData.getCustomerDetails().getDse_details().getDse_mobNo());
            }
            salesCRMRealmTable.setIsLeadActive(Integer.parseInt(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_active()));
            if (Integer.parseInt(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_active()) == 0) {
                RealmResults<SalesCRMRealmTable> invalidData = realm.where(SalesCRMRealmTable.class).equalTo("leadId", Long.parseLong(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_id())).findAll();
                for (int z = 0; z < invalidData.size(); z++) {
                    invalidData.get(z).setIsLeadActive(0);
                }
            }
            salesCRMRealmTable.setLeadAge(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_age());
            System.out.print("DSE Details Location3 - "+ C360Activity.cardDetailResponseData.getCustomerDetails().getLead_location().getId()+"- "+ C360Activity.cardDetailResponseData.getCustomerDetails().getLead_location().getName());
            salesCRMRealmTable.setLeadLocationId(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_location().getId());
            salesCRMRealmTable.setLeadLocationName(C360Activity.cardDetailResponseData.getCustomerDetails().getLead_location().getName());
            realm.copyToRealmOrUpdate(salesCRMRealmTable);
        }
    }

    public void init(boolean isShowAdapter) {
        if(isFinishing()) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            if(isDestroyed()) {
                return;
            }
        }
        tvC360LeadStage.setText(salesCRMRealmTable.getLeadStage());
        if (isShowAdapter) {
            System.out.print("DSE Details Location4 - "+salesCRMRealmTable.getLeadLocationId()+"- "+salesCRMRealmTable.getLeadLocationName());

            pref.setC360Destroyed(false);
            if(switchToCarsTab) {
                if(adapter == null) {
                    switchToCarsTab = false;
                }
                else {
                    adapter.notifyDataSetChanged();
                }
            }
            else {
                adapter = new C360PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
                viewPager.setAdapter(adapter);
            }
            if(!switchToCarsTab && tabPosition<3) {
                viewPager.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if(tabLayout.getTabAt(tabPosition)!=null) {
                            tabLayout.getTabAt(tabPosition).select();
                            System.out.println("Tab selected:at"+tabPosition);
                        }
                        viewPager.setCurrentItem(tabPosition);
                        tabPosition = 0;

                    }
                }, 50);
            }
            switchToCarsTab = false;
            updateViews();

            setStageIndicator(salesCRMRealmTable.getLeadStageProgressDB());

        } else {
            if (salesCRMRealmTable.getLeadStageProgressDB().size() > 0) {
                System.out.print("DSE Details Location5 - "+salesCRMRealmTable.getLeadLocationId()+"- "+salesCRMRealmTable.getLeadLocationName());

                setStageIndicator(salesCRMRealmTable.getLeadStageProgressDB());
            }
        }


        if (searchResponse == null) {
            String name = salesCRMRealmTable.getFirstName()
                    + " " + salesCRMRealmTable.getLastName();
            if (Util.isNotNull(name)) {
                tvC360Name.setText(String.format("%s%s", name.substring(0, 1).toUpperCase(), name.substring(1)));
            }
            if (!Util.isNotNull(salesCRMRealmTable.getFirstName())) {
                tvC360Name.setText("Customer Name");
            }
        } else {
            String name = searchResponse.getFirstName()
                    + " " + searchResponse.getLastName();
            if (Util.isNotNull(name)) {
                tvC360Name.setText(String.format("%s%s", name.substring(0, 1).toUpperCase(), name.substring(1)));
            }
            if (!Util.isNotNull(searchResponse.getFirstName())) {
                tvC360Name.setText("Customer Name");

            }
        }
        if (Util.isNotNull(salesCRMRealmTable.getTitle())) {
            tvC360NameMr.setText(salesCRMRealmTable.getTitle()+".");
        }
        else {
            tvC360NameMr.setText("");
        }


        tvC360LeadAge.setText(salesCRMRealmTable.getLeadAge());
        System.out.println("Expected closing date:"+salesCRMRealmTable.getCloseDate());
        if (salesCRMRealmTable.getLeadCarVariantName() == null) {
            tvC360Car.setText(salesCRMRealmTable.getLeadCarModelName());
        } else {
            tvC360Car.setText(salesCRMRealmTable.getLeadCarVariantName());
        }
        if(isClientILom) {
            tvLeadId.setText(salesCRMRealmTable.getLeadId()+"");
            if(Util.isNotNull(salesCRMRealmTable.getCloseDate())) {
                tvPolicyEndDate.setText(salesCRMRealmTable.getCloseDate());
            }
            else {
                tvPolicyEndDate.setText("---");
            }
            tvEditPolicy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Util.openLink(C360Activity.this, salesCRMRealmTable.getCustomerAddress());
                }
            });
        }
        if(isClientILBank) {
            tvLeadId.setText(salesCRMRealmTable.getLeadId()+"");
            tvOTB.setText(salesCRMRealmTable.getResidencePinCode());
            tvPolicyEndDate.setText(salesCRMRealmTable.getCustomerAddress());
        }


        // System.out.println("inter" + indicatorPoint);
        if (salesCRMRealmTable.getLeadTagsName() != null) {
            if (salesCRMRealmTable.getLeadTagsName().equalsIgnoreCase(WSConstants.LEAD_TAG_HOT)) {
                appBarC360.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.hot));
            } else if (salesCRMRealmTable.getLeadTagsName().equalsIgnoreCase(WSConstants.LEAD_TAG_WARM)) {
                appBarC360.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.warm));
            } else if (salesCRMRealmTable.getLeadTagsName().equalsIgnoreCase(WSConstants.LEAD_TAG_COLD)) {
                appBarC360.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.cold));
            } else if (salesCRMRealmTable.getLeadTagsName().equalsIgnoreCase(WSConstants.LEAD_TAG_OVERDUE)) {
                appBarC360.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.overdue));

            }
        }


    }

    private void setStageIndicator(RealmList<LeadStageProgressDB> progressDBRealmList) {
        int indicatorPoint = 0;
        System.out.println("Progress Size:" + progressDBRealmList.size());
        System.out.println("sales:cu.no:" + salesCRMRealmTable.customerPhoneNumbers.size());
        if (progressDBRealmList.size() == 0) {
            tvC360Indicator.setVisibility(View.INVISIBLE);
            tvC360LeadStage.setBackgroundResource(R.drawable.tv_bg_error);
            tvC360LeadStage.setText("Error in lead\n Contact Autoninja");
            return;
        } else {
            tvC360Indicator.setVisibility(View.VISIBLE);
            tvC360LeadStage.setBackgroundResource(R.drawable.tv_enquiry_list_progress_type);
        }
        //{Indicator;
        //Indicator point should be zero to 8
        for (int i = 0; i < progressDBRealmList.size(); i++) {
            if (i < ovalImageList.size()) {
                ovalImageList.get(i).setVisibility(View.VISIBLE);
                if (progressDBRealmList.get(i).getWidth() == 100) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        ovalImageList.get(i).setBackground(drawableActive);
                    } else {
                        ovalImageList.get(i).setBackgroundDrawable(drawableActive);
                    }
                } else if (progressDBRealmList.get(i).getWidth() == 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        ovalImageList.get(i).setBackground(drawableInactive);
                    } else {
                        ovalImageList.get(i).setBackgroundDrawable(drawableInactive);
                    }
                }
            }

        }
        //finding indicator point
        for (int i = progressDBRealmList.size() - 1; i >= 0; i--) {
            if (progressDBRealmList.get(i).getWidth() == 100) {
                indicatorPoint = i;
                break;
            }
        }

       /* if(ovalImageList.size()<=progressDBRealmList.size()){
                if(indicatorPoint==progressDBRealmList.size()-1){
                   indicatorPoint=
                }

        }*/

        if (indicatorPoint >= WSConstants.LEAD_STAGE_MIN && indicatorPoint <= WSConstants.LEAD_STAGE_MAX) {

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvC360Indicator.getLayoutParams();
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.addRule(RelativeLayout.ALIGN_START, getResources().getIdentifier("oval_view_" + indicatorPoint, "id", getPackageName()));
            } else {
                params.addRule(RelativeLayout.ALIGN_LEFT, getResources().getIdentifier("oval_view_" + indicatorPoint, "id", getPackageName()));
            }*/
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.addRule(RelativeLayout.ALIGN_START, getResources().getIdentifier("oval_view_" + indicatorPoint, "id", getPackageName()));
            }
            params.addRule(RelativeLayout.ALIGN_LEFT, getResources().getIdentifier("oval_view_" + indicatorPoint, "id", getPackageName()));
            tvC360Indicator.setLayoutParams(params);


            //Indicator oval color(active and inactive color)}

        }

    }

    @Override
    public void onBookCarRequested(boolean flag) {
        viewPager.setCurrentItem(1);
        onBackPressed();
    }

    @Subscribe
    public void restart(C360RestartModel restartModel){
        Intent intent = C360Activity.this.getIntent();
        intent.putExtra(WSConstants.C360_TAB_POSITION,restartModel.getTabPosition());
        finish();
        overridePendingTransition(0,0);
        startActivity(intent);
    }
}
