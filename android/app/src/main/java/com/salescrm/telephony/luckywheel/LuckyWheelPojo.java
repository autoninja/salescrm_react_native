package com.salescrm.telephony.luckywheel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by prateek on 20/3/18.
 */

public class LuckyWheelPojo implements Serializable{

    private String lead_id;

    private String scheduled_activity_id;

    private Heading heading;

    private String selected_option;

    private List<Option> options = null;

    private List<DseUnderTeamLead> dse_under_me = null;

    public List<DseUnderTeamLead> getDse_under_me() {
        return dse_under_me;
    }

    public void setDse_under_me(List<DseUnderTeamLead> dse_under_me) {
        this.dse_under_me = dse_under_me;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public String getScheduled_activity_id() {
        return scheduled_activity_id;
    }

    public void setScheduled_activity_id(String scheduled_activity_id) {
        this.scheduled_activity_id = scheduled_activity_id;
    }

    public Heading getHeading() {
        return heading;
    }

    public void setHeading(Heading heading) {
        this.heading = heading;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public String getSelected_option() {
        return selected_option;
    }

    public void setSelected_option(String selected_option) {
        this.selected_option = selected_option;
    }

    public class Heading implements Serializable{

        private String title;

        private String description;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public class Option implements Serializable{

        private String option_id;
        private String title;
        private String description;
        private String color_code;
        private String booster_sign;
        private String spin_id;

        public String getOption_id() {
            return option_id;
        }

        public void setOption_id(String option_id) {
            this.option_id = option_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getColor_code() {
            return color_code;
        }

        public void setColor_code(String color_code) {
            this.color_code = color_code;
        }

        public String getBooster_sign() {
            return booster_sign;
        }

        public void setBooster_sign(String booster_sign) {
            this.booster_sign = booster_sign;
        }

        public String getSpin_id() {
            return spin_id;
        }

        public void setSpin_id(String spin_id) {
            this.spin_id = spin_id;
        }
    }

    public class DseUnderTeamLead implements Serializable{

        private String user_id;

        private String user_name;

        private String location_id;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }
    }
}
