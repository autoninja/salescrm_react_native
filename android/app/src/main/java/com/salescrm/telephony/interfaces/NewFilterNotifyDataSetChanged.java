package com.salescrm.telephony.interfaces;

/**
 * Created by prateek on 8/8/18.
 */

public class NewFilterNotifyDataSetChanged {

    boolean datasetchanged;

    public NewFilterNotifyDataSetChanged(boolean datasetchanged) {
        this.datasetchanged = datasetchanged;
    }

    public boolean isDatasetchanged() {
        return datasetchanged;
    }
}
