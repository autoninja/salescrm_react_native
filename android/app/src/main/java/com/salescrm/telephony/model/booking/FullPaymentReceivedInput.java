package com.salescrm.telephony.model.booking;

/**
 * Created by bharath on 22/3/18.
 */

public class FullPaymentReceivedInput {
    private String remarks;
    int booking_id;
    private String booking_no;
    private int lead_car_id;
    private int lead_source_id;
    int location_id;
    private long lead_id;
    private int lead_car_stage_id;
    private String exp_delivery_date;
    private int enquiry_id;
    private String lead_last_updated;
    private ApiInputBookingDetails booking_details;

    public FullPaymentReceivedInput(String strRemarkTitle, int bookingId, String bookingNumber, int leadCarId, int leadSrcId, int locationId, long leadId, int stageId, String expectedDeliveryDate, String accessToken, String presentCarId, int enquiryId, String leadLastUpdated, ApiInputBookingDetails booking_details) {
        this.remarks = strRemarkTitle;
        this.booking_id = bookingId;
        this.booking_no = bookingNumber;
        this.lead_car_id = leadCarId;
        this.lead_source_id = leadSrcId;
        this.location_id = locationId;
        this.lead_id = leadId;
        this.lead_car_stage_id = stageId;
        this.exp_delivery_date = expectedDeliveryDate;
        this.enquiry_id = enquiryId;
        this.lead_last_updated = leadLastUpdated;
        this.booking_details = booking_details;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(int booking_id) {
        this.booking_id = booking_id;
    }

    public String getBooking_no() {
        return booking_no;
    }

    public void setBooking_no(String booking_no) {
        this.booking_no = booking_no;
    }

    public int getLead_car_id() {
        return lead_car_id;
    }

    public void setLead_car_id(int lead_car_id) {
        this.lead_car_id = lead_car_id;
    }

    public int getLead_source_id() {
        return lead_source_id;
    }

    public void setLead_source_id(int lead_source_id) {
        this.lead_source_id = lead_source_id;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public long getLead_id() {
        return lead_id;
    }

    public void setLead_id(long lead_id) {
        this.lead_id = lead_id;
    }

    public int getLead_car_stage_id() {
        return lead_car_stage_id;
    }

    public void setLead_car_stage_id(int lead_car_stage_id) {
        this.lead_car_stage_id = lead_car_stage_id;
    }

    public String getExp_delivery_date() {
        return exp_delivery_date;
    }

    public void setExp_delivery_date(String exp_delivery_date) {
        this.exp_delivery_date = exp_delivery_date;
    }

    public int getEnquiry_id() {
        return enquiry_id;
    }

    public void setEnquiry_id(int enquiry_id) {
        this.enquiry_id = enquiry_id;
    }

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }

    public ApiInputBookingDetails getBooking_details() {
        return booking_details;
    }

    public void setBooking_details(ApiInputBookingDetails booking_details) {
        this.booking_details = booking_details;
    }
}
