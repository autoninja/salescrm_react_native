import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import LanguageItem from './LanguageItem'
import CustomTextInput from "../../components/createLead/custom_text_input";

export default class VerificationLanguagePicker extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {selectedLanguage:null, proposalId:null}
    }
    render() {
        let {selectedLanguage, proposalId} = this.state;
        let actionDisabled = selectedLanguage==null || proposalId==null || proposalId.length==0;
        return(
                <LinearGradient
                colors={["#2e5e86", "#2e3486"]}
                style={{padding:10,
                    paddingTop:20,
                    paddingBottom:20,
                    borderRadius:10,}}>
                        <View>
                        <Text style={
                            {color:'#fff', 
                        fontSize:18, 
                        padding:6,
                        textAlign:'center'
                        }}>Select Language</Text>

                        <ScrollView style={{padding:8}}>
                            <LanguageItem
                                onSelect={(languageCode)=> {
                                    this.setState({selectedLanguage:languageCode})
                                }}
                                selectedLanguage = {selectedLanguage}
                                languageCode={'EN'}
                                languageText={'English'}
                                style={{borderTopEndRadius:6, borderTopStartRadius:6,}}
                            />

                            <LanguageItem
                                onSelect={(languageCode)=> {
                                    this.setState({selectedLanguage:languageCode})
                                }}
                                selectedLanguage = {selectedLanguage}
                                languageCode={'HI'}
                                languageText={'Hindi\n(हिन्दी)'}
                                style={{borderBottomEndRadius:6, borderBottomStartRadius:6, marginTop:4,}}
                            />
                            <View style={{flexDirection:'row'}}>
                                <LanguageItem
                                    onSelect={(languageCode)=> {
                                        this.setState({selectedLanguage:languageCode})
                                    }}
                                    selectedLanguage = {selectedLanguage}
                                    languageCode={'TA'}
                                    languageText={'Tamil\n(தமிழ்)'}
                                    style={{flex:1,borderTopEndRadius:6, borderTopStartRadius:6, marginTop:4,}}
                                />
                                <LanguageItem
                                    onSelect={(languageCode)=> {
                                        this.setState({selectedLanguage:languageCode})
                                    }}
                                    selectedLanguage = {selectedLanguage}
                                    languageCode={'TE'}
                                    languageText={'Telugu\n(తెలుగు)'}
                                    style={{flex:1,borderTopEndRadius:6, borderTopStartRadius:6, marginTop:4, marginStart:4,}}
                                />
                            </View>

                            <View style={{flexDirection:'row', paddingBottom:20}}>
                                <LanguageItem
                                    onSelect={(languageCode)=> {
                                        this.setState({selectedLanguage:languageCode})
                                    }}
                                    selectedLanguage = {selectedLanguage}
                                    languageCode={'ML'}
                                    languageText={'Malayalam\n(മലയാളം)'}
                                    style={{flex:1,borderBottomEndRadius:6, borderBottomStartRadius:6, marginTop:4,}}
                                />
                                <LanguageItem
                                    onSelect={(languageCode)=> {
                                        this.setState({selectedLanguage:languageCode})
                                    }}
                                    selectedLanguage = {selectedLanguage}
                                    languageCode={'KN'}
                                    languageText={'Kannada\n(ಕನ್ನಡ)'}
                                    style={{flex:1,borderBottomEndRadius:6, borderBottomStartRadius:6, marginTop:4,marginStart:4}}
                                />
                            </View>
                            <CustomTextInput
                                lightMode={true}
                                showPlaceHolder={true}
                                header="Proposal ID"
                                hint="Enter proposal id"
                                text={proposalId}
                                onInputTextChanged={(id, text, payload) => {
                                    this.setState({ proposalId: text });
                                }}
                            />
                        </ScrollView>
                       
                        <View style={{flexDirection:'row', marginTop:20}}>
                            <TouchableOpacity 
                            onPress={()=> {
                                this.props.onCancel();
                            }}
                            style={{borderRadius:4,flex:1, backgroundColor:'#a1a1a1', padding:6, margin:4}}
                            >
                                <Text style={{color:"#fff", fontSize:18, textAlign:'center'}}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                            disabled={actionDisabled}
                            onPress={()=> {
                                this.props.onConfirm({selectedLanguage, proposalId});
                            }} 
                            style={{borderRadius:4, flex:1, backgroundColor:'#007fff', padding:6, margin:4}}>
                            <Text style={{color:actionDisabled?'#bab6b6':"#fff", fontSize:18, textAlign:'center'}}>Submit</Text>
                            </TouchableOpacity>
                        </View>
                         </View>  
                </LinearGradient>
        )
    }
}