package com.salescrm.telephony.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.salescrm.telephony.utils.Util;

public class DynamicLinkHandler extends AppCompatActivity {
    public static final String TAG = "DynamicLinkHandler";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            if(deepLink!=null && deepLink.getQueryParameter("lead_id")!=null) {
                                startSplashActivity(true, deepLink.getQueryParameter("lead_id"));
                            }
                            else {
                                startSplashActivity(false, null);
                            }

                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Util.showToast(getApplicationContext(), "Error getting the dynamic link", Toast.LENGTH_LONG);
                        startSplashActivity(false, null);
                    }
                });
    }
    void startSplashActivity(boolean startC360, String leadId){
        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("START_C360", startC360);
        intent.putExtra("LEAD_ID", leadId);
        startActivity(intent);
        finish();
    }
}
