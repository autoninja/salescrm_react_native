package com.salescrm.telephony.model;


import com.salescrm.telephony.db.car.AllInterestedCars;
import com.salescrm.telephony.response.AllLeadCustomerDetailsResponse;

/**
 * Created by bharath on 20/3/18.
 */

public class C360InformationModel {
    private static final C360InformationModel instance = new C360InformationModel();
    private  AllLeadCustomerDetailsResponse.Result.Customer_details customerDetails = null;
    private AllInterestedCars allInterestedCars;
    private int totalNumberOfBooking = 1;
    private String locationId;
    private FormSubmissionInputData formSubmissionInput;
    private String leadId;
    private String leadLastUpdated;
    private String leadSourceId;

    public static C360InformationModel getInstance() {
        return instance;
    }

    private C360InformationModel() {
    }


    public AllLeadCustomerDetailsResponse.Result.Customer_details getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(AllLeadCustomerDetailsResponse.Result.Customer_details customerDetails) {
        this.customerDetails = customerDetails;
    }

    public AllInterestedCars getAllInterestedCars() {
        return allInterestedCars;
    }

    public void setAllInterestedCars(AllInterestedCars allInterestedCars) {
        this.allInterestedCars = allInterestedCars;
    }

    public int getTotalNumberOfBooking() {
        return totalNumberOfBooking;
    }

    public void setTotalNumberOfBooking(int totalNumberOfBooking) {
        this.totalNumberOfBooking = totalNumberOfBooking;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public void setFormSubmissionInput(FormSubmissionInputData formSubmissionInput) {
        this.formSubmissionInput = formSubmissionInput;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public void setLeadLastUpdated(String leadLastUpdated) {
        this.leadLastUpdated = leadLastUpdated;
    }

    public void setLeadSourceId(String leadSourceId) {
        this.leadSourceId = leadSourceId;
    }

    public String getLocationId() {
        return locationId;
    }

    public FormSubmissionInputData getFormSubmissionInput() {
        return formSubmissionInput;
    }

    public String getLeadId() {
        return leadId;
    }

    public String getLeadLastUpdated() {
        return leadLastUpdated;
    }

    public String getLeadSourceId() {
        return leadSourceId;
    }
}
