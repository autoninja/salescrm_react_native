import React, { Component } from 'react'
import { View, Text, } from 'react-native'

export default class LiveEFFooter extends Component {

  render() {
    return (
      <View style={{
        marginTop: 40, alignItems: 'flex-start', borderRadius: 6, padding: 5
      }}>

        <View style={{
          flex: 1, flexDirection: 'row',
        }}>
          <Text style={{ color: '#546e7a', fontSize: 14, textAlign: 'center' }} >L - </Text>
          <Text style={{ color: '#546e7a', fontSize: 14, textAlign: 'center' }} >Lost / Not Eligible</Text>
        </View>

      </View>
    )
  }
}
