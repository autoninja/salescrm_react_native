package com.salescrm.telephony.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.salescrm.telephony.fragments.addlead.AddLeadActFragment;
import com.salescrm.telephony.fragments.addlead.AddLeadCarFragment;
import com.salescrm.telephony.fragments.addlead.AddLeadCustDetailsFragment;
import com.salescrm.telephony.fragments.addlead.AddLeadDetailsFragment;
import com.salescrm.telephony.fragments.addlead.AddLeadDseFragment;

/**
 * Created by bharath on 30/5/16.
 */
public class AddLeadPagerAdapter extends FragmentStatePagerAdapter {
    int count;
    Context context;

    public AddLeadPagerAdapter(FragmentManager fm, int count, Context context) {
        super(fm);
        this.count = count;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new AddLeadCustDetailsFragment();
            case 1:
                return new AddLeadDetailsFragment();
            case 2:
                return new AddLeadCarFragment();
            case 3:
                return new AddLeadDseFragment();
            case 4:
                return new AddLeadActFragment();
            default:
                return null;

        }

    }

    @Override
    public int getCount() {
        return count;
    }
}
