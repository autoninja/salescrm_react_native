package com.salescrm.telephony.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;



/**
 * Created by akshata on 12/4/16.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;
    // Database Name
    private static final String DATABASE_NAME = "TaskDB";
    private static final String TABLE_TASKS = "tasks";

    private static final String KEY_LEADID = "lead_id";
    private static final String KEY_SCHACTIVITY = "scheduled_activity_id";
    private static final String KEY_NUMBER = "number";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_ACTIVITYDESC = "activity_description";
    private static final String KEY_ACTIVITYTYPE = "activity_type";
    private static final String KEY_ACTIVITYNAME = "activity_name";
    private static final String KEY_ACTIVITYGRPID = "activity_group_id";
    private static final String KEY_ICONTYPE = "icon_type";
    private static final String KEY_LEADAGE = "lead_age";
    private static final String KEY_DATE = "date";
    private static final String KEY_LSOURCENAME = "lead_source_name";
    private static final String KEY_ECLOSINGDATE = "expected_closing_date";
    private static final String KEY_ALLOTEDDSE = "allotedDse";
    private static final String KEY_CUSTOMERID = "customer_id";
    private static final String KEY_VARIANTNAME = "variant_name";
    private static final String KEY_MODELNAME = "model_name";


    private static final String[] COLUMNS = {KEY_LEADID, KEY_SCHACTIVITY, KEY_NUMBER, KEY_NAME, KEY_EMAIL, KEY_ACTIVITYDESC, KEY_ACTIVITYTYPE, KEY_ACTIVITYNAME,
            KEY_ACTIVITYGRPID, KEY_ICONTYPE, KEY_LEADAGE, KEY_DATE, KEY_LSOURCENAME, KEY_ECLOSINGDATE, KEY_ALLOTEDDSE, KEY_CUSTOMERID, KEY_VARIANTNAME, KEY_MODELNAME
    };
    String CREATE_TASK_TABLE = "CREATE TABLE tasks ( " +
            "lead_id  INTEGER , " +
            "scheduled_activity_id  INTEGER , " +
            "number INTEGER , " + "name TEXT, " + "email TEXT, " + "activity_description TEXT, " + "activity_type TEXT, "
            + "activity_name TEXT, " + "activity_group_id INTEGER, " + "icon_type TEXT, " + "lead_age INTEGER , "
            + "date TEXT, " + "lead_source_name TEXT, " + "expected_closing_date TEXT, " + "allotedDse TEXT, " + "customer_id TEXT, " + "variant_name TEXT, " + "model_name TEXT )";


    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TASK_TABLE);
        Log.d("CREATE_TASK_TABLE", CREATE_TASK_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS tasks");

        this.onCreate(db);

    }

    public void addtask(String lead_id, String scheduled_activity_id, String number, String name, String email, String activity_description, String activity_type, String activity_name, String activity_group_id, String icon_type, String lead_age, String date, String lead_source_name, String expected_closing_date, String allotedDse, String customer_id, String variant_name, String model_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("lead_id", lead_id);
        values.put("scheduled_activity_id", scheduled_activity_id);
        values.put("number", number);
        values.put("name", name);
        values.put("email", email);
        values.put("activity_description", activity_description);
        values.put("activity_type", activity_type);
        values.put("activity_name", activity_name);
        values.put("activity_group_id", activity_group_id);
        values.put("icon_type", icon_type);
        values.put("lead_age", lead_age);
        values.put("date", date);
        values.put("lead_source_name", lead_source_name);
        values.put("expected_closing_date", expected_closing_date);
        values.put("allotedDse", allotedDse);
        values.put("customer_id", customer_id);
        values.put("variant_name", variant_name);
        values.put("model_name", model_name);
        db.insert(TABLE_TASKS, null, values);
        Log.d("Addtasks","addtask");
    }

    public void updatetask(String lead_id) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("lead_id", lead_id);
        Log.d("leadid",lead_id+" "+db.update(TABLE_TASKS, values, KEY_LEADID + " =?", new String[]{String.valueOf(lead_id)}));
        Log.d("KEYLEADID", String.valueOf(lead_id));
        System.out.println("KEYLEADID"+String.valueOf(lead_id));

    }

    public void deletetask(String number) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("number", number);
        Log.d("nuumberrr",number+" "+db.delete(TABLE_TASKS, KEY_NUMBER + " =?", new String[]{String.valueOf(number)}));
        System.out.println("KEY_NUMBER"+String.valueOf(number));
    }

    public  int getTotalCount(String number) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = null;
        c = db.rawQuery("SELECT count(*) from tasks where number  IS NULL or ''",null);
        c.moveToFirst();
        System.out.println("Count:" + c.getCount());

        return c.getCount();



    }
}
