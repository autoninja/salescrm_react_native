package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by bharath on 8/8/16.
 */
public class AddLeadElementsResponse {
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private EnqSource enqSource;

        private List<Brands> brands;

        private List<AllOldCars> allOldCars;

        private List<Locations> locations;

        private List<BuyerType> buyerType;

        private List<Activities> activities;

        private List<AllCars> allCars;

        public EnqSource getEnqSource() {
            return enqSource;
        }

        public void setEnqSource(EnqSource enqSource) {
            this.enqSource = enqSource;
        }

        public List<Brands> getBrands() {
            return brands;
        }

        public void setBrands(List<Brands> brands) {
            this.brands = brands;
        }

        public List<AllOldCars> getAllOldCars() {
            return allOldCars;
        }

        public void setAllOldCars(List<AllOldCars> allOldCars) {
            this.allOldCars = allOldCars;
        }

        public List<Locations> getLocations() {
            return locations;
        }

        public void setLocations(List<Locations> locations) {
            this.locations = locations;
        }

        public List<BuyerType> getBuyerType() {
            return buyerType;
        }

        public void setBuyerType(List<BuyerType> buyerType) {
            this.buyerType = buyerType;
        }

        public List<Activities> getActivities() {
            return activities;
        }

        public void setActivities(List<Activities> activities) {
            this.activities = activities;
        }

        public List<AllCars> getAllCars() {
            return allCars;
        }

        public void setAllCars(List<AllCars> allCars) {
            this.allCars = allCars;
        }

        @Override
        public String toString() {
            return "ClassPojo [enqSource = " + enqSource + ", brands = " + brands + ", allOldCars = " + allOldCars + ", locations = " + locations + ", buyerType = " + buyerType + ", activities = " + activities + ", allCars = " + allCars + "]";
        }


        //Activities
        public class Activities {
            private String id;

            private String category;

            private String name;


            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", category = " + category + ", name = " + name + "]";
            }
        }

        //Enquiry Source
        public class EnqSource
        {
            private List<Lead_sources> lead_sources;

            private List<Lead_source_categories> lead_source_categories;

            public List<Lead_sources> getLead_sources ()
            {
                return lead_sources;
            }

            public void setLead_sources (List<Lead_sources> lead_sources)
            {
                this.lead_sources = lead_sources;
            }

            public List<Lead_source_categories> getLead_source_categories ()
            {
                return lead_source_categories;
            }

            public void setLead_source_categories (List<Lead_source_categories> lead_source_categories)
            {
                this.lead_source_categories = lead_source_categories;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [lead_sources = "+lead_sources+", lead_source_categories = "+lead_source_categories+"]";
            }

            public class Lead_sources
            {
                private int id;

                private String name;

                public int getId ()
                {
                    return id;
                }

                public void setId (int id)
                {
                    this.id = id;
                }

                public String getName ()
                {
                    return name;
                }

                public void setName (String name)
                {
                    this.name = name;
                }

                @Override
                public String toString()
                {
                    return "ClassPojo [id = "+id+", name = "+name+"]";
                }
            }

            public class Lead_source_categories
            {
                private int id;

                private Lead_sources lead_sources;

                private String name;

                public int getId ()
                {
                    return id;
                }

                public void setId (int id)
                {
                    this.id = id;
                }

                public Lead_sources getLead_sources ()
                {
                    return lead_sources;
                }

                public void setLead_sources (Lead_sources lead_sources)
                {
                    this.lead_sources = lead_sources;
                }

                public String getName ()
                {
                    return name;
                }

                public void setName (String name)
                {
                    this.name = name;
                }

                @Override
                public String toString()
                {
                    return "ClassPojo [id = "+id+", lead_sources = "+lead_sources+", name = "+name+"]";
                }

                public class Lead_sources
                {

                    private List<Data> data;

                    public List<Data> getData ()
                    {
                        return data;
                    }

                    public void setData (List<Data> data)
                    {
                        this.data = data;
                    }

                    public class Data
                    {
                        private int id;

                        private String name;

                        public int getId ()
                        {
                            return id;
                        }

                        public void setId (int id)
                        {
                            this.id = id;
                        }

                        public String getName ()
                        {
                            return name;
                        }

                        public void setName (String name)
                        {
                            this.name = name;
                        }

                        @Override
                        public String toString()
                        {
                            return "ClassPojo [id = "+id+", name = "+name+"]";
                        }
                    }
                }

            }



        }

        //Locations
        public class Locations{
            private String id;

            private String name;

            private String code;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", name = " + name + ", code = " + code + "]";
            }
        }

        //Brands
        public class Brands {
            private String id;

            private String company;

            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCompany() {
                return company;
            }

            public void setCompany(String company) {
                this.company = company;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", company = " + company + ", name = " + name + "]";
            }
        }

        public class AllOldCars {
            private String id;

            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", name = " + name + "]";
            }
        }

        public class AllCars {
            private String id;

            private String name;
            private String category;

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", name = " + name + "]";
            }
        }

        public class BuyerType{
            private String id;

            private String type;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", type = " + type + "]";
            }
        }


    }
}