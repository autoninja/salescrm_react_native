import { UPDATE_EVENT_DASHBOARD } from './event_update_types'

export const updateEventDashboard = payload => {
    return {
        type: UPDATE_EVENT_DASHBOARD,
        payload: payload
      }
}