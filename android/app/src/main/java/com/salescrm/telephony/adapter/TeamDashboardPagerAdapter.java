package com.salescrm.telephony.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.salescrm.telephony.fragments.ETVBRLMainFragment;
import com.salescrm.telephony.fragments.EtvbrContainerFragment;
import com.salescrm.telephony.fragments.EtvbrLocationFragment;
import com.salescrm.telephony.fragments.RNLostDropMainFragment;
import com.salescrm.telephony.fragments.TodayTasksSummaryMainFragment;
import com.salescrm.telephony.fragments.retExcFin.RefContainerFragment;

/**
 * Created by prateek on 6/6/17.
 */

public class TeamDashboardPagerAdapter extends FragmentStatePagerAdapter {
    private int NUM_ITEMS = 2;
    private static boolean showPendingTab = false;

    public TeamDashboardPagerAdapter(FragmentManager fm, int i) {
        super(fm);
        NUM_ITEMS = i;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new TodayTasksSummaryMainFragment();
            case 1:
                return new ETVBRLMainFragment();
            case 2:
                return new RNLostDropMainFragment();
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return NUM_ITEMS;
    }


}