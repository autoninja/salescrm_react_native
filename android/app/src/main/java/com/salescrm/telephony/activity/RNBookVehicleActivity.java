package com.salescrm.telephony.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.createEnquiry.RNCreateLeadActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandModelsDB;
import com.salescrm.telephony.db.car.CarBrandsDB;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.dbOperation.TasksDbOperation;
import com.salescrm.telephony.model.C360InformationModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class RNBookVehicleActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {

    private ReactInstanceManager mReactInstanceManager;
    private Realm realm;
    private ReactRootView mReactRootView;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals("REACT_NATIVE_EXIT")) {
                String view = intent.getStringExtra("VIEW");
                if(Util.isNotNull(view) && view.equalsIgnoreCase("BOOK_VEHICLE")) {
                    finish();
                }
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        Preferences pref = Preferences.getInstance();
        Preferences.load(this);
        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();
        Bundle initialProps = new Bundle();
        initialProps.putString("token", Preferences.getAccessToken());
        initialProps.putString("module", "BOOK_VEHICLE");
        initialProps.putInt("appUserId", pref.getAppUserId());
        initialProps.putString("leadCarId", getIntent().getStringExtra("lead_car_id"));
        initialProps.putString("leadId", getIntent().getStringExtra("lead_id"));
        initialProps.putString("stageId", getIntent().getStringExtra("stage_id"));
        initialProps.putString("booking_id", getIntent().getStringExtra("booking_id"));
        initialProps.putString("modelName", getIntent().getStringExtra("model_name"));
        initialProps.putString("action", getIntent().getStringExtra("action"));
        initialProps.putString("c360Information", new Gson().toJson(C360InformationModel.getInstance().getCustomerDetails()));
        initialProps.putString("interestedVehicles", new Gson().toJson(getInterestedVehicleModel()));
        initialProps.putString("userRoles", DbUtils.getRolesJSON());
        mReactRootView = new ReactRootView(this);
        setContentView(mReactRootView);
        mReactRootView.startReactApplication(
                mReactInstanceManager,
                "NinjaCRMSales",
                initialProps
        );
        
    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }

    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
        try {
            registerReceiver(broadcastReceiver, new IntentFilter("REACT_NATIVE_EXIT"));

        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }
    private VehicleModel getInterestedVehicleModel() {
        UserDetails userDetails = realm.where(UserDetails.class).findFirst();
        if (userDetails != null && userDetails.getUserCarBrands().size() > 0) {
            CarBrandsDB brand = realm.where(CarBrandsDB.class).equalTo("brand_id", userDetails.getUserCarBrands().get(0).getBrand_id()).findFirst();
            if (brand != null) {
                List<VehicleModelsModel> models = new ArrayList<>();
                for (CarBrandModelsDB model : brand.getBrand_models()) {

                    if (model.getCategory().equalsIgnoreCase(WSConstants.CAR_CATEGORY_BOTH)) {

                        List<VehicleModelsVariantModel> variants = new ArrayList<>();
                        for (CarBrandModelVariantsDB variant : model.getCar_variants()) {
                            variants.add(new VehicleModelsVariantModel(
                                    Util.getInt(variant.getVariant_id()),
                                    variant.getVariant(),
                                    Util.getInt(variant.getFuel_type_id()),
                                    variant.getFuel_type(),
                                    Util.getInt(variant.getColor_id()),
                                    variant.getColor()));
                        }

                        models.add(new VehicleModelsModel(Util.getInt(model.getModel_id()),
                                model.getModel_name(), model.getCategory(), variants));
                    }
                }
                return new VehicleModel(Util.getInt(brand.getBrand_id()), brand.getBrand_name(), models);
            }

        }

        return null;
    }

    class VehicleModel {
        int brand_id;
        String brand_name;
        List<VehicleModelsModel> brand_models;

        VehicleModel(int brand_id, String brand_name, List<VehicleModelsModel> brand_models) {
            this.brand_id = brand_id;
            this.brand_name = brand_name;
            this.brand_models = brand_models;
        }
    }

    class VehicleModelsModel {
        int model_id;
        String model_name;
        String category;
        List<VehicleModelsVariantModel> car_variants;

        VehicleModelsModel(int model_id, String model_name, String category,
                           List<VehicleModelsVariantModel> car_variants) {
            this.model_id = model_id;
            this.model_name = model_name;
            this.category = category;
            this.car_variants = car_variants;
        }
    }

    class VehicleModelsVariantModel {
        int variant_id;
        String variant;
        int fuel_type_id;
        String fuel_type;
        int color_id;
        String color;

        VehicleModelsVariantModel(int variant_id, String variant,
                                  int fuel_type_id, String fuel_type, int color_id, String color) {
            this.variant_id = variant_id;
            this.variant = variant;
            this.fuel_type_id = fuel_type_id;
            this.fuel_type = fuel_type;
            this.color_id = color_id;
            this.color = color;
        }
    }

}