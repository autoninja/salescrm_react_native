package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by bannhi on 16/6/17.
 */

public class BookCarResponse {
    private String message;

    private String statusCode;

    private Result result;

    private Error error;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", statusCode = " + statusCode + ", result = " + result + ", error = " + error + "]";
    }

    public class Error {
        private String details;

        private String type;

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "ClassPojo [details = " + details + ", type = " + type + "]";
        }
    }

    public class Result {
        private Integer[] scheduled_activity_ids;
        private Integer action_id;
        private String lead_last_updated;
        private Car_details car_details;
        private GameData game_data;

        private Booking_details[] booking_details;

        public Car_details getCar_details() {
            return car_details;
        }

        public void setCar_details(Car_details car_details) {
            this.car_details = car_details;
        }

        public Booking_details[] getBooking_details() {
            return booking_details;
        }

        public void setBooking_details(Booking_details[] booking_details) {
            this.booking_details = booking_details;
        }

        public Integer[] getScheduled_activity_ids() {
            return scheduled_activity_ids;
        }

        public void setScheduled_activity_ids(Integer[] scheduled_activity_ids) {
            this.scheduled_activity_ids = scheduled_activity_ids;
        }

        public Integer getAction_id() {
            return action_id;
        }

        public void setAction_id(Integer action_id) {
            this.action_id = action_id;
        }

        public String getLead_last_updated() {
            return lead_last_updated;
        }

        public void setLead_last_updated(String lead_last_updated) {
            this.lead_last_updated = lead_last_updated;
        }

        public GameData getGame_data() {
            return game_data;
        }

        public void setGame_data(GameData game_data) {
            this.game_data = game_data;
        }

        @Override
        public String toString() {
            return "ClassPojo [car_details = " + car_details + ", booking_details = " + booking_details + "]";
        }
    }

    public class Car_details {
        private String color_id;

        private String lead_car_id;

        private String variant_id;

        private String mode_id;

        public String getColor_id() {
            return color_id;
        }

        public void setColor_id(String color_id) {
            this.color_id = color_id;
        }

        public String getLead_car_id() {
            return lead_car_id;
        }

        public void setLead_car_id(String lead_car_id) {
            this.lead_car_id = lead_car_id;
        }

        public String getVariant_id() {
            return variant_id;
        }

        public void setVariant_id(String variant_id) {
            this.variant_id = variant_id;
        }

        public String getMode_id() {
            return mode_id;
        }

        public void setMode_id(String mode_id) {
            this.mode_id = mode_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [color_id = " + color_id + ", lead_car_id = " + lead_car_id + ", variant_id = " + variant_id + ", mode_id = " + mode_id + "]";
        }
    }

    public class Booking_details {
        private String booking_id;

        private String enquiry_id;

        private String booking_number;

        private String next_stage_id;

        public String getBooking_id() {
            return booking_id;
        }

        public void setBooking_id(String booking_id) {
            this.booking_id = booking_id;
        }

        public String getEnquiry_id() {
            return enquiry_id;
        }

        public void setEnquiry_id(String enquiry_id) {
            this.enquiry_id = enquiry_id;
        }

        public String getBooking_number() {
            return booking_number;
        }

        public void setBooking_number(String booking_number) {
            this.booking_number = booking_number;
        }

        public String getNext_stage_id() {
            return next_stage_id;
        }

        public void setNext_stage_id(String next_stage_id) {
            this.next_stage_id = next_stage_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [booking_id = " + booking_id + ", enquiry_id = " + enquiry_id + ", booking_number = " + booking_number + ", next_stage_id = " + next_stage_id + "]";
        }
    }

    public class GameData {
        private List<BadgeDetails> badge_details;

        private String points_earned;

        private String activity;

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public List<BadgeDetails> getBadge_details ()
        {
            return badge_details;
        }

        public void setBadge_details (List<BadgeDetails> badge_details)
        {
            this.badge_details = badge_details;
        }

        public String getPoints_earned ()
        {
            return points_earned;
        }

        public void setPoints_earned (String points_earned)
        {
            this.points_earned = points_earned;
        }
    }

    public class BadgeDetails
    {
        private String id;

        private String name;

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }
    }
}
