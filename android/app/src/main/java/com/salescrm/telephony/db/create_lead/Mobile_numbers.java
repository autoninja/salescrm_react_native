package com.salescrm.telephony.db.create_lead;

import io.realm.RealmObject;

/**
 * Created by bharath on 8/12/16.
 */
public class Mobile_numbers extends RealmObject {
    private String number;

    private boolean is_primary;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public boolean getIs_primary() {
        return is_primary;
    }

    public void setIs_primary(boolean is_primary) {
        this.is_primary = is_primary;
    }

    @Override
    public String toString() {
        return "ClassPojo [number = " + number + ", is_primary = " + is_primary + "]";
    }
}