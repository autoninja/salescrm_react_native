package com.salescrm.telephony.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.DealershipsDB;
import com.salescrm.telephony.model.Item;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.SwitchDealershipResponse;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.CircleTransform;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SwitchDealershipsActivity extends AppCompatActivity {

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GALLERY_IMAGE_REQUEST_CODE = 200;
    private static int MARGIN;
    private final String IMAGE_NAME = "img_profile.jpg";
    private RadioGroup radioGroup;
    private List<Item> items = new ArrayList<>();
    private Realm realm;
    private boolean isFromValidateOtp;
    private ImageView imageViewProfile;
    private LinearLayout llLoader;
    private Uri uriFile = null;
    private Preferences pref = null;
    private String dpUrl = null;
    private int selectedDealerId;

    private int from;
    private RealmResults<DealershipsDB> data = null;

    private ProgressBar progressBarImageLoading;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(this);
        MARGIN = (int) Util.convertDpToPixel(8, this);
        if(getWindow()!=null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        }

        loadFromBundle();

        setContentView(R.layout.layout_switch_dealerships);
        radioGroup = findViewById(R.id.radio_group_dealers);
        imageViewProfile = findViewById(R.id.image_view_dealers);
        llLoader = findViewById(R.id.switch_dealership_frame_loader);
        progressBarImageLoading = findViewById(R.id.switch_dealership_progress_bar);


        init();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                selectedDealerId = i;
                System.out.println(i);
                dpUrl = "";
                pref.setDpUri("");
                pref.setDpTakenFromDevice(false);
                if(data !=null ) {
                    DealershipsDB realmData = data.where().equalTo("dealer_id", selectedDealerId).findFirst();
                    if(realmData!=null && Util.isNotNull(realmData.getUser_dp_url())) {
                        dpUrl = realmData.getUser_dp_url();
                        System.out.println("Dealer:"+realmData.getUser_dp_url());
                    };


                }
                setImageUrl(dpUrl);

            }
        });

        findViewById(R.id.bt_dealership_proceed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pref.getDpUri() == null || pref.getDpUri().isEmpty() ) {
                    if(dpUrl == null || dpUrl.isEmpty()) {
                        Util.showToast(SwitchDealershipsActivity.this, "Please add a photo", Toast.LENGTH_LONG);
                        return;
                    }

                }
                if (selectedDealerId > 0) {
                    llLoader.setVisibility(View.VISIBLE);
                    radioGroup.setEnabled(false);
                    imageViewProfile.setEnabled(false);
                    ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).switchDealership(selectedDealerId, "", new Callback<SwitchDealershipResponse>() {
                        @Override
                        public void success(SwitchDealershipResponse switchDealershipResponse, Response response) {
                            llLoader.setVisibility(View.GONE);
                            radioGroup.setEnabled(true);
                            imageViewProfile.setEnabled(true);
                            if (switchDealershipResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) && Util.isNotNull(switchDealershipResponse.getResult())) {
                                pref.setAccessToken(switchDealershipResponse.getResult());
                                pref.setDealershipSelected(true);
                                pref.setDealershipSelectedId(selectedDealerId);
                                pref.setRefreshOfflineData(true);
                                Intent intent = new Intent(SwitchDealershipsActivity.this, OfflineSupportActivity.class);
                                intent.putExtra("START_C360", isOpenC360());
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();

                            } else if (Util.isNotNull(switchDealershipResponse.getMessage())) {
                                Util.showToast(SwitchDealershipsActivity.this, switchDealershipResponse.getMessage(), Toast.LENGTH_LONG);
                            } else {
                                Util.showToast(SwitchDealershipsActivity.this, "Failed to switch dealership", Toast.LENGTH_LONG);

                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            radioGroup.setEnabled(true);
                            imageViewProfile.setEnabled(true);
                            llLoader.setVisibility(View.GONE);
                            Util.showToast(SwitchDealershipsActivity.this, "Failed to switch dealership", Toast.LENGTH_LONG);

                        }
                    });

                }
                else {
                    Util.showToast(SwitchDealershipsActivity.this, "Please select a dealer", Toast.LENGTH_LONG);

                }
            }
        });
        imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SwitchDealershipsActivity.this);
                final CharSequence imageArray[] = {"Camera", "Gallery"};
                builder.setTitle("Upload From:");
                builder.setItems(imageArray, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        Intent iIntent;
                        if (imageArray[position].equals("Camera")) {
                            iIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            uriFile = getOutputMediaFileUri();
                            iIntent.putExtra(MediaStore.EXTRA_OUTPUT, getOutputMediaFileUri());
                            startActivityForResult(iIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                        } else if (imageArray[position].equals("Gallery")) {
                            iIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            iIntent.setType("image/*");
                            startActivityForResult(iIntent, GALLERY_IMAGE_REQUEST_CODE);
                        }
                    }
                });
                builder.show();
            }
        });

    }

    private void loadFromBundle() {
        if(getIntent()!=null && getIntent().getExtras()!=null) {
            from = getIntent().getIntExtra("from", 0);
        }
    }

    public Uri getOutputMediaFileUri() {
        return FileProvider.getUriForFile(this, this.getPackageName() + ".provider", getOutputMediaFile());
    }

    private boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }


    private File getOutputMediaFile() {
        if (isExternalStorageAvailable()) {
            File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), WSConstants.MY_SALES_DIRECTORY);
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            File mediaFile = new File(mediaStorageDir.getPath() + File.separator + IMAGE_NAME);
            return mediaFile;
        } else {
            return null;
        }
    }


    private void init() {
        items.clear();
        data = realm.where(DealershipsDB.class).findAll();
        for (int i = 0; i < data.size(); i++) {
            AppCompatRadioButton radioButton = new AppCompatRadioButton(this);
            radioButton.setTextSize(18);
            radioButton.setTextColor(Color.parseColor("#303030"));
            radioButton.setId(data.get(i).getDealer_id());
            System.out.println("Id" + data.get(i).getDealer_id());
            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT,
                    RadioGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(MARGIN, MARGIN, MARGIN, MARGIN);
            radioButton.setLayoutParams(params);
            radioButton.setText(getProperName(data.get(i).getDealer_name()));

            if(data.get(i).getDealer_id() == pref.getDealershipSelectedId()) {
                selectedDealerId = data.get(i).getDealer_id();
            }
            radioButton.setChecked(data.get(i).getDealer_id() == pref.getDealershipSelectedId());

            items.add(new Item(data.get(i).getDealer_name(), data.get(i).getDealer_id()));
            radioGroup.addView(radioButton);
            if (Util.isNotNull(data.get(i).getUser_dp_url())) {
                dpUrl = data.get(i).getUser_dp_url();
                System.out.println("DP_URL:" + dpUrl);

            }
        }
        if (Util.isNotNull(dpUrl)) {
            setImageUrl(dpUrl);
        }


    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    private String getProperName(String name) {
        if (name == null || name.equalsIgnoreCase("")) {
            return "";
        }
        return String.format("%s%s", name.substring(0, 1).toUpperCase(),
                name.split(" ")[0].substring(1));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("Hello onActivityResult: " + data);
        if (resultCode == RESULT_OK) {
            Bitmap bitmap;
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                pref.setDpUri(uriFile.toString());
                setImageUrl(pref.getDpUri());
                try {
                    System.out.println("uriFile:"+uriFile.toString());
                  //  FileProvider.getUriForFile(this, this.getPackageName() + ".provider", getOutputMediaFile());
                   // bitmap = BitmapFactory.decodeFile(Uri.parse(uriFile.toString()).getPath());
                    bitmap = BitmapFactory.decodeFile(getOutputMediaFile().getPath());
                    System.out.println("BitMap:"+bitmap);
                    pref.setEncodedUri(encodeImage(bitmap));
                    pref.setDpTakenFromDevice(true);
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }
            } else if (requestCode == GALLERY_IMAGE_REQUEST_CODE) {
                uriFile = data.getData();
                pref.setDpUri(data.getData().toString());
                setImageUrl(pref.getDpUri());
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(Uri.parse(pref.getDpUri()));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    bitmap = BitmapFactory.decodeStream(imageStream);
                    pref.setEncodedUri(encodeImage(bitmap));
                    pref.setDpTakenFromDevice(true);
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 30, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }

    private void setImageUrl(String dpUri) {
        imageViewProfile.setVisibility(View.GONE);
        progressBarImageLoading.setVisibility(View.VISIBLE);
        Picasso.with(this).load(Uri.parse(dpUri)).memoryPolicy(MemoryPolicy.NO_CACHE).
                networkPolicy(NetworkPolicy.NO_CACHE).placeholder(R.drawable.ic_person_white_48dp).
                error(R.mipmap.no_profile_img).transform(new CircleTransform()).into(imageViewProfile, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                imageViewProfile.setVisibility(View.VISIBLE);
                progressBarImageLoading.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                imageViewProfile.setVisibility(View.VISIBLE);
                progressBarImageLoading.setVisibility(View.GONE);
            }
        });
    }

    private boolean isOpenC360() {
        boolean openC360 = false;
        if(getIntent()!=null && getIntent().getBooleanExtra("START_C360", false)) {
            openC360 = true;
        }
        return openC360;
    }

}
