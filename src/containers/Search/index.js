import React, { Component } from "react";
import { View, Image, Text, KeyboardAvoidingView, Alert } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import styles from "./Search.styles";
import TextInputLayout from "../../components/uielements/TextInputLayout";
import Button from "../../components/uielements/Button";
const mobile_pattern = /^[1-9]\d{9}/;
const email_pattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile_number: "",
      customer_name: "",
      lead_id: "",
      email_id: ""
    };
  }
  isValid() {
    let { mobile_number, customer_name, lead_id, email_id } = this.state;

    if (mobile_number.length > 0 && email_id.length > 0) {
      return mobile_pattern.test(mobile_number) && email_pattern.test(email_id);
    } else if (mobile_number.length > 0) {
      return mobile_pattern.test(mobile_number);
    } else if (email_id.length > 0) {
      return email_pattern.test(email_id);
    }
    return customer_name.length > 0 || lead_id.length > 0;
  }
  navigateToSearchResult(navigation) {
    let { mobile_number, customer_name, lead_id, email_id } = this.state;
    navigation.navigate("SearchResult", { info: { mobile_number, customer_name, lead_id, email_id } });
  }
  render() {
    let { navigation } = this.props;
    return (
      <LinearGradient
        colors={["#2e5e86", "#2e3486"]}
        style={styles.linearGradient}
      >
        <View>

          <View style={styles.content}>
            <Text
              style={{ color: "#808080", alignSelf: "center", fontSize: 16, paddingBottom: 20 }}
            >
              Enter any info to proceed
              </Text>
            <TextInputLayout
              onTextChange={text => {
                this.setState({ mobile_number: text });
              }}
              hint={"Mobile number(recommended)"}
              keyboardType={"number-pad"}
              maxLength={10}
              pattern={mobile_pattern}
            />
            <TextInputLayout
              onTextChange={text => {
                this.setState({ customer_name: text });
              }}
              hint={"Customer name"}
            />
            <TextInputLayout
              onTextChange={text => {
                this.setState({ lead_id: text });
              }}
              hint={"Lead ID"}
              keyboardType={"number-pad"}
            />
            <TextInputLayout
              onTextChange={text => {
                this.setState({ email_id: text });
              }}
              hint={"Email ID"}
              keyboardType={"email-address"}
              pattern={email_pattern}
            />
          </View>
          <Button
            disabled={!this.isValid()}
            title="Search"
            onPress={() => {
              this.navigateToSearchResult(navigation);
            }}
          />
        </View>
      </LinearGradient>
    );
  }
}
