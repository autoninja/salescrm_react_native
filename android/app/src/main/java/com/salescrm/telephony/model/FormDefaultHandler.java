package com.salescrm.telephony.model;

/**
 * Created by bharath on 9/2/17.
 */

public class FormDefaultHandler {
    private Integer fromId;
    private String  fromOldValue;
    private Integer toId;
    private String  toChangedValue;

    public FormDefaultHandler(Integer fromId, String fromOldValue, Integer toId, String toChangedValue) {
        this.fromId = fromId;
        this.fromOldValue = fromOldValue;
        this.toId = toId;
        this.toChangedValue = toChangedValue;
    }

    public Integer getFromId() {
        return fromId;
    }

    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    public String getFromOldValue() {
        return fromOldValue;
    }

    public void setFromOldValue(String fromOldValue) {
        this.fromOldValue = fromOldValue;
    }

    public Integer getToId() {
        return toId;
    }

    public void setToId(Integer toId) {
        this.toId = toId;
    }

    public String getToChangedValue() {
        return toChangedValue;
    }

    public void setToChangedValue(String toChangedValue) {
        this.toChangedValue = toChangedValue;
    }
}
