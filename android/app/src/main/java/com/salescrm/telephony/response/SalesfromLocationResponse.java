package com.salescrm.telephony.response;

/**
 * Created by akshata on 29/11/16.
 */

public class SalesfromLocationResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }
    public class Result {
        private Dses[] dses;

        private Dses.Sales_managers[] sales_managers;

        public Dses[] getDses() {
            return dses;
        }

        public void setDses(Dses[] dses) {
            this.dses = dses;
        }

        public Dses.Sales_managers[] getSales_managers() {
            return sales_managers;
        }

        public void setSales_managers(Dses.Sales_managers[] sales_managers) {
            this.sales_managers = sales_managers;
        }

        @Override
        public String toString() {
            return "ClassPojo [dses = " + dses + ", sales_managers = " + sales_managers + "]";
        }

        public class Dses {
            private String id;

            private String name;

            private String team_id;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getTeam_id() {
                return team_id;
            }

            public void setTeam_id(String team_id) {
                this.team_id = team_id;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", name = " + name + ", team_id = " + team_id + "]";
            }

            public class Sales_managers {
                private String id;

                private String name;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                @Override
                public String toString() {
                    return "ClassPojo [id = " + id + ", name = " + name + "]";
                }

                public class Error {
                    private String details;

                    private String type;

                    public String getDetails() {
                        return details;
                    }

                    public void setDetails(String details) {
                        this.details = details;
                    }

                    public String getType() {
                        return type;
                    }

                    public void setType(String type) {
                        this.type = type;
                    }

                    @Override
                    public String toString() {
                        return "ClassPojo [details = " + details + ", type = " + type + "]";
                    }
                }
            }
        }
    }
}
