package com.salescrm.telephony.response;

/**
 * Created by akshata on 10/8/16.
 */
public class GetAllSmsEmailResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public class Result {
        private Template_data[] template_data;

        private String[] lead_data;

        private Dealer_data dealer_data;

        public Template_data[] getTemplate_data() {
            return template_data;
        }

        public void setTemplate_data(Template_data[] template_data) {
            this.template_data = template_data;
        }

        public String[] getLead_data() {
            return lead_data;
        }

        public void setLead_data(String[] lead_data) {
            this.lead_data = lead_data;
        }

        public Dealer_data getDealer_data() {
            return dealer_data;
        }

        public void setDealer_data(Dealer_data dealer_data) {
            this.dealer_data = dealer_data;
        }

        @Override
        public String toString() {
            return "ClassPojo [template_data = " + template_data + ", lead_data = " + lead_data + ", dealer_data = " + dealer_data + "]";
        }

        public class Dealer_data {
            private String custSupportEmailId;

            private String ccmEmailId;

            private String ccmMobNo;

            private String dealerCity;

            private String hotlineNo;

            private String dname;

            private String dbrand;

            private String crmMobNo;

            private String tdFeedbackLink;

            private String dealerWebsite;

            private String dealerAddress;

            private String pocName;

            public String getCustSupportEmailId() {
                return custSupportEmailId;
            }

            public void setCustSupportEmailId(String custSupportEmailId) {
                this.custSupportEmailId = custSupportEmailId;
            }

            public String getCcmEmailId() {
                return ccmEmailId;
            }

            public void setCcmEmailId(String ccmEmailId) {
                this.ccmEmailId = ccmEmailId;
            }

            public String getCcmMobNo() {
                return ccmMobNo;
            }

            public void setCcmMobNo(String ccmMobNo) {
                this.ccmMobNo = ccmMobNo;
            }

            public String getDealerCity() {
                return dealerCity;
            }

            public void setDealerCity(String dealerCity) {
                this.dealerCity = dealerCity;
            }

            public String getHotlineNo() {
                return hotlineNo;
            }

            public void setHotlineNo(String hotlineNo) {
                this.hotlineNo = hotlineNo;
            }

            public String getDname() {
                return dname;
            }

            public void setDname(String dname) {
                this.dname = dname;
            }

            public String getDbrand() {
                return dbrand;
            }

            public void setDbrand(String dbrand) {
                this.dbrand = dbrand;
            }

            public String getCrmMobNo() {
                return crmMobNo;
            }

            public void setCrmMobNo(String crmMobNo) {
                this.crmMobNo = crmMobNo;
            }

            public String getTdFeedbackLink() {
                return tdFeedbackLink;
            }

            public void setTdFeedbackLink(String tdFeedbackLink) {
                this.tdFeedbackLink = tdFeedbackLink;
            }

            public String getDealerWebsite() {
                return dealerWebsite;
            }

            public void setDealerWebsite(String dealerWebsite) {
                this.dealerWebsite = dealerWebsite;
            }

            public String getDealerAddress() {
                return dealerAddress;
            }

            public void setDealerAddress(String dealerAddress) {
                this.dealerAddress = dealerAddress;
            }

            public String getPocName() {
                return pocName;
            }

            public void setPocName(String pocName) {
                this.pocName = pocName;
            }

            @Override
            public String toString() {
                return "ClassPojo [custSupportEmailId = " + custSupportEmailId + ", ccmEmailId = " + ccmEmailId + ", ccmMobNo = " + ccmMobNo + ", dealerCity = " + dealerCity + ", hotlineNo = " + hotlineNo + ", dname = " + dname + ", dbrand = " + dbrand + ", crmMobNo = " + crmMobNo + ", tdFeedbackLink = " + tdFeedbackLink + ", dealerWebsite = " + dealerWebsite + ", dealerAddress = " + dealerAddress + ", pocName = " + pocName + "]";
            }
        }

        public class Template_data {
            private String content;

            private String id;

            private String subject;

            private String name;

            private String recipient_type;

            private String notification_type_id;

            private int recipient_type_id;

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getSubject() {
                return subject;
            }

            public void setSubject(String subject) {
                this.subject = subject;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getRecipient_type() {
                return recipient_type;
            }

            public void setRecipient_type(String recipient_type) {
                this.recipient_type = recipient_type;
            }

            public String getNotification_type_id() {
                return notification_type_id;
            }

            public void setNotification_type_id(String notification_type_id) {
                this.notification_type_id = notification_type_id;
            }

            @Override
            public String toString() {
                return "ClassPojo [content = " + content + ", id = " + id + ", subject = " + subject + ", name = " + name + ", recipient_type = " + recipient_type + ", notification_type_id = " + notification_type_id + "]";
            }

            public int getRecipient_type_id() {
                return recipient_type_id;
            }

            public void setRecipient_type_id(int recipient_type_id) {
                this.recipient_type_id = recipient_type_id;
            }
        }

    }

    public class Error {
        private String details;

        private String type;

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "ClassPojo [details = " + details + ", type = " + type + "]";
        }
    }
}
