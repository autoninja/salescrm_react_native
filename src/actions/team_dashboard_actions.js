import { UPDATE_TEAM_DASHBOARD } from './team_dashboard_types';


export const updateTeamDashboard = payload => {
  return {
    type: UPDATE_TEAM_DASHBOARD,
    payload: payload
  }
}
