package com.salescrm.telephony.activity.gamification;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.gamification.DseProfileAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.gamification.DSEPointsResponseModel;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.FabricUtils;
import com.salescrm.telephony.utils.Util;

import java.util.HashMap;
import java.util.Locale;

public class DsePointsActivity extends AppCompatActivity {

    private Preferences pref;
    private RelativeLayout back;
    ConnectionDetectorService connectionDetectorService;
    private String userId, dseName, photoURL = "";
    ImageView imgDse;
    TabLayout tabLayout;
    ViewPager viewPager;
    private Integer point;

    private int spotlight;

    private boolean isFromViewBadges;

    private TextView dse_name, total_title_points, txtImgDse, tvProfileViews;
    private int prevTab = -1;
    //private TextView tv_retail_points,tv_td_points,tv_visit_points,tv_zpd_points,tv_bonus_points,total_points;
    //private TextView tv_retail_total_points,tv_td_total_points, tv_visit_total_points, tv_zpd_total_points,tv_bonus_total_points;


    @Override
    protected void onStop() {
        super.onStop();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dse_points);
        pref = Preferences.getInstance();
        pref.load(this);
        tabLayout = findViewById(R.id.tablyt);
        viewPager = findViewById(R.id.viewpager);
        dse_name = findViewById(R.id.dse_name);
        imgDse = findViewById(R.id.img_dse_details);
        back = findViewById(R.id.team_gamification_img_back);
        txtImgDse = findViewById(R.id.imgtxt__dse_details);
        total_title_points = findViewById(R.id.total_points);
        tvProfileViews = findViewById(R.id.tv_profile_views);
        isFromViewBadges = getIntent().getBooleanExtra("from_view_badges", false);
        spotlight = getIntent().getIntExtra("spotlight", 1);
        if(isFromViewBadges) {
            userId = pref.getAppUserId()+"";
            dseName = pref.getDseName();
            photoURL = pref.getUserProfilePicUrl();
            point = -1;
            pref.setSelectedGameDate(pref.getSystemDate());
        }
        else {
            userId = getIntent().getStringExtra("user_id");
            dseName = getIntent().getStringExtra("name");
            photoURL = getIntent().getStringExtra("image");
            point = getIntent().getIntExtra("points", -1);
        }
        System.out.println("UserId:"+userId);
        System.out.println("dseName:"+dseName);
        System.out.println("photoURL:"+photoURL);

        init();

        findViewById(R.id.img_points_help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DsePointsActivity.this, PointsHelpActivity.class);
                intent.putExtra("user_id",userId);
                startActivity(intent);
                pushCleverTap(true);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void init() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(Color.parseColor("#FF0F2B51"));
        }
        dse_name.setText(dseName);
        txtImgDse.setText(String.format("%s", dseName.toUpperCase().substring(0, 1)));
        if(point !=-1) {
            total_title_points.setText(String.format(Locale.getDefault(),"%d", point));
        }

        txtImgDse.setText(dseName.toUpperCase().substring(0, 1));

        if(Util.isNotNull(photoURL)) {
            Glide.with(this).
                    load(photoURL)
                    .asBitmap()
                    .centerCrop()
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                            imgDse.setVisibility(View.GONE);
                            txtImgDse.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            imgDse.setVisibility(View.VISIBLE);
                            txtImgDse.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(new BitmapImageViewTarget(imgDse) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            if(resource!=null ) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                imgDse.setImageDrawable(circularBitmapDrawable);
                            }
                        }
                    });
        }
        else {
            imgDse.setVisibility(View.GONE);
            txtImgDse.setVisibility(View.VISIBLE);

        }


        DseProfileAdapter adapter =
                new DseProfileAdapter(this, getSupportFragmentManager(), userId, new CallBack() {
                    @Override
                    public void onCallBack() {

                    }

                    @Override
                    public void onCallBack(Object data) {
                        if(data!=null && data instanceof DSEPointsResponseModel.Result) {
                            total_title_points.setText(((DSEPointsResponseModel.Result)data).getTotal());
                            tvProfileViews.setText(String.format("%s Profile Views", ((DSEPointsResponseModel.Result) data).getViews()));

                        }

                    }

                    @Override
                    public void onError(String error) {

                    }
                });

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                changeTabTextStyle();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        tabLayout.setupWithViewPager(viewPager);
        if(isFromViewBadges) {
            viewPager.setCurrentItem(1);
            isFromViewBadges = false;
        }

        changeTabTextStyle();


    }

    private void changeTabTextStyle() {
        if(prevTab !=-1) {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(prevTab)))).getChildAt(1))).setTypeface(null, Typeface.NORMAL);
        }
        ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(tabLayout.getSelectedTabPosition())))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
        //((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(prevTab)))).getChildAt(1))).setTypeface(null, Typeface.NORMAL);
        prevTab=tabLayout.getSelectedTabPosition();
        pushCleverTap(false);

    }
    private void pushCleverTap(boolean isHelp) {
        String leaderBoardType;
        if(isHelp) {
            if (spotlight == 1) {
                leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_HELP_OWN;
            } else {
                leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_HELP_OTHERS;

            }
        }
        else {
            if (tabLayout.getSelectedTabPosition() == 0) {
                if (spotlight == 1) {
                    leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_CONSULTANT_POINT_BREAK_UP_OWN;
                } else {
                    leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_CONSULTANT_POINT_BREAK_UP_OTHERS;

                }
            } else {
                if (spotlight == 1) {
                    leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_BADGE_OWN;
                } else {
                    leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_BADGE_OTHERS;

                }
            }
        }
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_LEADER_BOARD_KEY_TYPE,
                leaderBoardType);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_LEADER_BOARD, hashMap);


        //Fabric Events
        FabricUtils.sendEvent(pref, "LeaderBoard-"+pref.getDealerName(),
                "Type", leaderBoardType);


    }

}
