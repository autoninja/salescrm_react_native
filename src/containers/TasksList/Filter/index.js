import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';
import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation'
import Moment from 'moment';

import { connect } from 'react-redux';
import { updateTasksListFilter } from '../../../actions/tasks_list_actions';
import styles from './tasksFilter.styles'
import tasksFilterStyles from "./tasksFilter.styles";
import RestClient from "../../../network/rest_client";
import FilterKeyItem from "./FilterKeyItem";
import FilterTypeCommon from './FilterTypeCommon';
import FilterTypeDate from './FilterTypeDate';
import FilterTypeLeadAge from './FilterTypeLeadAge';
import CustomDateTimePicker from '../../../utils/DateTimePicker'
import { getFilterInitialData } from './FilterInitialState';


class TaskListFilter extends Component {
    state = {
        filters: [],
        filtersUpdate: false
    }
    constructor(props) {
        super(props);
        this.lastIndex = 0;
        this.state = {
            filters: getFilterInitialData({ models: this.getVehcileModels(), enquirySource: this.getEnquirySources() }),
            loading: true,
        }
    }
    getVehcileModels() {
        let models = this.props.vehcileModel.brand_models;
        let childrens = [];
        if (models) {
            models.map((model) => {
                childrens.push({ id: model.model_id, title: model.model_name })
            })
        }
        return childrens;
    }
    getEnquirySources() {
        let enquirySource = this.props.enquirySource;
        let childrens = [];
        enquirySource.map((source) => {
            childrens.push({ id: source.id, title: source.name })
        })
        return childrens;
    }
    fetchFilterElements() {
        new RestClient().getFilterElements().then((data) => {
            if (data && data.result) {
                let { filters } = this.state;
                //activities
                filters[0].children = data.result.activities.map((item) => ({ id: item.id, title: item.name }))
                //leadStages
                filters[5].children = data.result.leadStages.map((item) => ({ id: item.stage_id, title: item.stage }))

                this.props.filters.map((selectedFilter) => {
                    for (i = 0; i < filters[selectedFilter.index].children.length; i++) {
                        selectedFilter.values.map((value) => {
                            if (selectedFilter.key == "date") {
                                if (value.key == "expected_buying_date_from") {
                                    filters[selectedFilter.index].children[0].selected = true;
                                    filters[selectedFilter.index].children[0].title = value.value;
                                }
                                else if (value.key == "expected_buying_date_to") {
                                    filters[selectedFilter.index].children[1].selected = true;
                                    filters[selectedFilter.index].children[1].title = value.value;

                                }
                                else if (value.key == "enquiry_date_from") {
                                    filters[selectedFilter.index].children[2].selected = true;
                                    filters[selectedFilter.index].children[2].title = value.value;

                                }
                                else if (value.key == "enquiry_date_to") {
                                    filters[selectedFilter.index].children[3].selected = true;
                                    filters[selectedFilter.index].children[3].title = value.value;

                                }
                            }
                            else if (selectedFilter.key == "lead_age" && filters[selectedFilter.index].children[i].id == value) {
                                //Exception for slider
                                filters[selectedFilter.index].children[i].selected = true;
                                filters[selectedFilter.index].children[i].title = selectedFilter.names[0];
                            }
                            else if (filters[selectedFilter.index].children[i].id == value) {
                                filters[selectedFilter.index].children[i].selected = true;
                            }

                        })
                    }
                });


                this.setState({ filters, loading: false });
            }

        }).catch((err) => {
            console.error(err);
        })
    }
    clearFilters() {
        this.props.navigation.navigate('AutoDialog', {
            data: {
                title: "Are you sure want to clear Filters?\n",
                yes: "Yes",
                no: "No"
            },
            onNoButtonPress: () => {

            },
            onYesButtonPress: () => {
                this.setState({
                    filters: getFilterInitialData({ models: this.getVehcileModels(), enquirySource: this.getEnquirySources() }),
                    loading: true,

                }, function () {
                    this.fetchFilterElements()
                });
                this.props.updateFilters([], false);
            },

        });
    }
    componentDidMount() {
        this.fetchFilterElements();
    }
    onFilterKeyPressed(index) {
        let { filters } = this.state;
        filters[index].selected = true;
        if (index != this.lastIndex) {
            filters[this.lastIndex].selected = false;
        }
        this.lastIndex = index;
        this.setState({ filters });
    }
    getSelectedFiltersView() {
        let { filters } = this.state;
        for (let i = 0; i < filters.length; i++) {
            if (filters[i].selected) {
                if (filters[i].view == "date_view") {
                    return (<FilterTypeDate
                        data={filters[i].children}
                        onPress={(id) => {
                            this.setState({
                                dateTimePickerPayload: { from: id },
                                dateTimePickerVisibility: true
                            })
                        }}
                    />)
                }
                else if (filters[i].view == "lead_age_view") {
                    return (
                        <FilterTypeLeadAge
                            onChange={(val) => {
                                let { filters } = this.state;
                                filters[i].children[0].selected = true;
                                this.setState({ filters });
                                if (val == 0) {
                                    filters[i].children[0].selected = false;
                                }
                                filters[i].children[0].title = val;
                                this.setState({ filters });
                            }}
                            data={filters[i].children}
                        />
                    )
                }
                else {
                    return (
                        <FilterTypeCommon
                            data={filters[i].children}
                            onPress={(index) => {
                                console.log(index, i);
                                let { filters } = this.state;
                                filters[i].children[index].selected = filters[i].children[index].selected ? false : true;
                                this.setState({ filters });
                            }}
                        />
                    )
                }

            }
        }
        return null;
    }

    onDateSelect(date, payload) {
        let { filters } = this.state;
        filters[6].children[payload.from].title = date;
        filters[6].children[payload.from].selected = true;
        this.setState({
            filters,
            dateTimePickerVisibility: false,
            dateTimePickerMinimumDate: undefined,
            dateTimePickerMaximumDate: undefined
        });
    }

    applyFilters() {

        let { filters } = this.state;
        let selectedFilters = [];
        filters.map((item, index) => {
            let values = [];
            let names = [];
            item.children.map((children) => {

                if (children.selected) {
                    if (item.id == "date") {
                        values.push({ key: children.id, value: children.title })
                    }
                    else {
                        names.push(children.title);
                        values.push(children.id)
                    }
                }

            })
            if (values.length > 0) {
                selectedFilters.push({ key: item.id, values, index, names });
            }
        });
        if (selectedFilters.length > 0) {
            this.props.navigation.goBack();
            this.props.updateFilters(selectedFilters, true);

        }

    }


    render() {
        let { filters, loading } = this.state;
        return (
            <View style={styles.MainContainer}>
                <View style={tasksFilterStyles.Header}>
                    <Text style={tasksFilterStyles.HeaderTitle}>Filter By </Text>
                    <TouchableOpacity
                        hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                    >
                        <Image style={tasksFilterStyles.Image}
                            source={require('../../../images/ic_close_black.png')}
                        />
                    </TouchableOpacity>
                </View>

                {loading && (
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ActivityIndicator
                            style={styles.loader}
                            color="#007fff"
                        />
                    </View>
                )}
                {!loading && (
                    <View style={tasksFilterStyles.Content}>
                        <CustomDateTimePicker
                            dateTimePickerVisibility={this.state.dateTimePickerVisibility}
                            datePickerMode={"date"}
                            payload={this.state.dateTimePickerPayload}
                            handleDateTimePicked={(date, payload) => {
                                this.onDateSelect(date, payload);
                            }}
                            hideDateTimePicked={() => {
                                this.setState({
                                    dateTimePickerVisibility: false,
                                    dateTimePickerMinimumDate: undefined,
                                    dateTimePickerMaximumDate: undefined
                                })
                            }}
                        />
                        <View style={tasksFilterStyles.ContentLeft}>
                            <FlatList
                                data={this.state.filters}
                                extraData={this.state}
                                renderItem={({ item, index }) => (
                                    <FilterKeyItem
                                        onPress={() => {
                                            this.onFilterKeyPressed(index);
                                        }}
                                        isLastItem={index == this.state.filters.length - 1 ? true : false}
                                        title={item.name}
                                        selected={item.selected}
                                        children={item.children}
                                    />
                                )}
                                keyExtractor={item => item.id}
                            />
                        </View>
                        <View style={tasksFilterStyles.ContentRight}>
                            {this.getSelectedFiltersView()}
                        </View>
                    </View>)}
                <View style={tasksFilterStyles.Footer}>
                    <TouchableOpacity
                        style={{ flex: 1 }}
                        onPress={() => {
                            this.clearFilters();
                        }}
                    >
                        <View style={[
                            tasksFilterStyles.FooterItem
                            , {
                                backgroundColor: '#808080'
                            }]
                        }>
                            <Text style={{ color: "#fff", fontSize: 18 }}>Clear All</Text>
                        </View>

                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ flex: 1 }}
                        onPress={() => {
                            this.applyFilters();
                        }}
                    >
                        <View style={[
                            tasksFilterStyles.FooterItem
                            , {
                                backgroundColor: '#007aff'
                            }]}>

                            <Text style={{ color: "#fff", fontSize: 18 }}>Apply</Text>


                        </View>
                    </TouchableOpacity>
                </View>

            </View >
        )
    }
}
const mapStateToProps = state => {
    return {
        filters: state.tasksListReducer.filters
    }
}
const mapDispatchToProps = dispatch => {
    return {
        updateFilters: (filters, filtersUpdate) => {
            dispatch(updateTasksListFilter({ filters, filtersUpdate }))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskListFilter)