import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import NavigationService from '../../../navigation/NavigationService';
import { BOOKING_ROUTES } from '../BookingUtils/BookingUtils'
import RestClient from '../../../network/rest_client'
import Loader from '../../../components/uielements/Loader'
import Toast, { DURATION } from "react-native-easy-toast";
let styles = StyleSheet.create({
    linearGradient: {
        padding: 16,
        paddingBottom: 24,
        borderRadius: 10,
        backgroundColor: "#fff",
        borderRadius: 5
    },
    main: {
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 80,
        paddingBottom: 80,
        flex: 1,
        justifyContent: 'center'

    },
    header: {
        flexDirection: "row",
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    title: {
        marginTop: 10,
        padding: 6,
        color: "#384CA8",
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        marginBottom: 10,
    },
    item: {
        marginTop: 14,
        color: "#7886C6",
        fontSize: 16,
        textAlign: 'center',
    },
    mainButton: {
        margin: 4,
        flex: 1, paddingTop: 12,
        paddingBottom: 12,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    closeButton: {
        position: "absolute",
        right: 8,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
        height: 24,
        width: 24,
        borderRadius: 14
    },
    mainActionText: {
        textAlign: 'center',
        color: '#fff', fontSize: 16,
        flexWrap: 'wrap', textTransform: 'uppercase'
    },

});

export default class UpdateInvoice extends Component {
    constructor(props) {
        super(props);
        this._userClickExit = false;
        this.state = {
            loading: false,
            requireRefreshC360: false
        }
    }

    getRichData() {
        let { c360Information, leadCarId, modelName, booking_id } = this.props.info;
        let richData = {
            customerName: "",
            modelName,
            bookingNumber: "No Booking Number"
        }
        if (c360Information) {
            if (c360Information.interestedCarsDetails) {
                c360Information.interestedCarsDetails.map((interestedCar) => {
                    if (interestedCar.lead_car_id == leadCarId &&
                        interestedCar.carDmsData && interestedCar.carDmsData.length > 0) {
                        carDmsData = interestedCar.carDmsData.filter((dms => dms.booking_id == booking_id))[0];
                        if (carDmsData && carDmsData.booking_number) {
                            richData.bookingNumber = carDmsData.booking_number;
                        }
                    }
                }
                )
            }
            if (c360Information.customerDetails && c360Information.customerDetails.name) {
                richData.customerName = c360Information.customerDetails.name;
            }
        }
        return richData;
    }
    onNotReceived() {
        this.setState({ loading: true, requireRefreshC360: true })
        let { c360Information, stageId, leadCarId, booking_id } = this.props.info;

        let data = {
            lead_car_id: leadCarId,
            lead_car_stage_id: stageId,
            booking_id: '',
            enquiry_id: '',
            lead_id: '',
            lead_last_updated: ''
        }
        if (c360Information) {
            if (c360Information.customerDetails) {
                data.lead_id = parseInt(c360Information.customerDetails.lead_id);
                data.lead_last_updated = c360Information.customerDetails.lead_last_updated;
            }
            if (c360Information.interestedCarsDetails) {
                c360Information.interestedCarsDetails.map((interestedCar) => {
                    if (interestedCar.lead_car_id == leadCarId) {
                        if (interestedCar.carDmsData && interestedCar.carDmsData.length > 0) {
                            dmsData = interestedCar.carDmsData.filter((dms => dms.booking_id == booking_id))[0];
                            data.booking_id = dmsData.booking_id;
                            data.enquiry_id = dmsData.enquiry_id;
                        }
                    }
                });
            }
        }
        new RestClient().cancelPayment(data).then((data) => {
            if (data
                && data.statusCode == "2002"
                && data.result) {
                this.setState({
                    loading: false
                })
                this.showAlert("Booking Cancelled");
                this.props.callbacks.goBack();
                this.props.callbacks.onApiSubmission();
            }
            else if (data
                && data.message) {
                this.onApiError(data.message);
            }
            else {
                this.onApiError("Failed to update, Please try again later");
            }
        }).
            catch((error) => {
                this.onApiError("Failed to update, Please try again later");
            })
    }
    onConfirm() {
        NavigationService.resetScreen(BOOKING_ROUTES.BookVehicleMain, {}, this.props.navigation);
    }

    showAlert(msg) {
        this.refs.toast.show(msg);
    }
    onApiError(msg) {
        this.setState({ loading: false });
        this.showAlert(msg);
    }

    render() {
        let { customerName, modelName, bookingNumber, requireRefreshC360 } = this.getRichData();
        let { loading } = this.state;
        return (
            <View style={styles.main}>
                <View
                    style={styles.linearGradient}>
                    <View style={{ flexWrap: 'wrap' }}>
                        <TouchableOpacity
                            hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                            style={styles.closeButton}
                            onPress={() => {
                                this.props.callbacks.goBack();
                                if (requireRefreshC360) {
                                    this.props.callbacks.onApiSubmission();
                                }

                            }}>
                            <Image
                                source={require('../../../images/ic_close_white.png')}
                                style={{ width: 16, height: 16 }}
                            />
                        </TouchableOpacity>
                        <Image
                            source={require('../../../images/payment_received_icon.png')}
                            style={{ alignSelf: 'center', marginTop: 36, width: 100, height: 100 }}
                        />
                        <Text style={styles.title}>Full Payment Received</Text>
                        <Text style={styles.item}>{bookingNumber}</Text>
                        <Text style={styles.item}>{customerName}</Text>
                        <Text style={styles.item}>{modelName}</Text>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 16,
                        }}>
                            <TouchableOpacity
                                onPress={() => this.onNotReceived()}
                                style={[
                                    styles.mainButton,
                                    {
                                        backgroundColor: '#C10F0F',
                                    }
                                ]}
                            >
                                <Text style={styles.mainActionText}>Not Received</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.onConfirm()}
                                style={[
                                    styles.mainButton,
                                    {
                                        backgroundColor: '#2196f3',
                                    }]}
                            >
                                <Text style={styles.mainActionText}>Confirm</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>

                {loading && <Loader isLoading={loading} />}
                <Toast
                    positionValue={200}
                    ref="toast"
                    style={{ backgroundColor: "#FF0000" }}
                    position="bottom"
                    fadeInDuration={1000}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: "white" }}
                />

            </View>
        )
    }
}