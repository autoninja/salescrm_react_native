package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.db.GameLocationDB;

public interface GameLocationPickerCallBack {
    void onPickGameLocation(GameLocationDB gameLocationDB);
}
