package com.salescrm.telephony.db.etvbr_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by prateek on 13/11/17.
 */

public class CarModelList extends RealmObject {

    @SerializedName("info")
    @Expose
    private InfoCarModel info;
    @SerializedName("abs")
    @Expose
    private AbsoluteCarModel abs;
    @SerializedName("rel")
    @Expose
    private RelationalCarModel rel;

    public InfoCarModel getInfo() {
        return info;
    }

    public void setInfo(InfoCarModel info) {
        this.info = info;
    }

    public AbsoluteCarModel getAbs() {
        return abs;
    }

    public void setAbs(AbsoluteCarModel abs) {
        this.abs = abs;
    }

    public RelationalCarModel getRel() {
        return rel;
    }

    public void setRel(RelationalCarModel rel) {
        this.rel = rel;
    }
}
