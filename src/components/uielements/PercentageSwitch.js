import React, { Component } from 'react';
import { Image } from 'react-native';
import { Switch } from 'react-native-switch';
export default class PercentageSwitch extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return(
            <Switch
                value={this.props.value}
                onValueChange={(val) =>{
                    this.props.onValueChange(val)
                }}
                disabled={false}
                activeText={'On'}
                inActiveText={'Off'}
                circleSize={24}
                barHeight={20}
                circleBorderWidth={0}
                circleActiveColor={'#007FFF'}
                circleInActiveColor={'#007FFF'}
                backgroundActive={'#2D4F8B'}
                backgroundInactive={'#E0E0E0'}
                renderInsideCircle={() => <Image resizeMode='contain' style={{height:20, width:20}}source={require('../../images/ic_percentage.png')} />} // custom component to render inside the Switch circle (Text, Image, etc.)
                changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
                innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} // style for inner animated circle for what you (may) be rendering inside the circle
                outerCircleStyle={{}} // style for outer animated circle
                renderActiveText={false}
                renderInActiveText={false}
                switchLeftPx={2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                switchRightPx={2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                switchWidthMultiplier={2} // multipled by the `circleSize` prop to calculate total width of the Switch
            />
        )
    }
}
