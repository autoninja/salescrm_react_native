import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, Platform, Button, TouchableOpacity, ScrollView, TouchableWithoutFeedback} from 'react-native';
//import { VictoryPie } from 'react-native-svg'
import { VictoryPie } from "victory-native";
import Svg from 'react-native-svg';
import LostDropRevenueLoss from '../../../../components/lostDrop/lost_drop_revenue_loss'

import { Dimensions } from 'react-native';
const { width } = Dimensions.get('window');

export default class LostDetailsTab extends Component {
  constructor(props) {
    super(props)
    this.state = {
      colors : [
        '#ffa600',
        '#ff6e54',
        '#dd5182',
        '#955196',
        '#444e86',
        '#003f5c',
         ]
    }
  }


  getWrapView(segments, colors, id) {
    let modelNames = [];
    for(let i=0;i<segments.length;i++) {

      modelNames.push(
         <TouchableWithoutFeedback key={segments[i].id} disabled= {segments[i].id==0} onPress = {()=>this.props.onClick(this.props.title, segments[i], colors[i]) }>
            <View style={[styles.wrapButton, {backgroundColor:colors[i]}]}>
              <Text numberOfLines={1} style={styles.wrapText} key={segments[i].id}>{segments[i].name+" "}</Text>
            </View>
         </TouchableWithoutFeedback>

      )
    }
    return (<View style={styles.wrapView}>{modelNames}</View>)
  }
  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  render() {
    let data = this.props.data;
    let revenueLoss = 0;
    let segments = [];
    let segmentsPie = [];
    let segmentsColors = [];

    if(data) {
        revenueLoss = data.loss_in_revenue;
        segments = data.segments?data.segments:[];
        for(var i=0; i<segments.length;i++) {
          segmentsPie.push({label:segments[i].rel+'%'+' ('+segments[i].abs+')', y:segments[i].rel})
          if(segments[i].id == 0) {
            segmentsColors.push('#808080');
          }
          else {
            segmentsColors.push(i<this.state.colors.length?this.state.colors[i]:this.getRandomColor());
          }

        }

    }

    return(
      <View style={{backgroundColor:'#fff', flex:1}}>
      {segments.length==0 && (
        <View style={{alignItems:'center', flex:1,justifyContent: 'center',}}>
          <Text style={{alignSelf:'center', fontSize:20}}> No Data </Text>
        </View>
      )}
      {segments.length>0 && (
        <ScrollView contentContainerStyle={{flexGrow: 1, backgroundColor: '#fff'}} showsVerticalScrollIndicator={false}>
        <View style={{flex:1, justifyContent: 'center', alignItems: 'center', width: '100%'}}>
          <Text style={{color:'#2e5e86', fontSize:18, marginTop:10, fontWeight:'600'}}>Brand wise  </Text>
          <Svg width={width * 2} height={300} style={{ alignItems:'center', justifyContent:'center',}}>
            <VictoryPie
              height={300}
              width={width * 2}
              standalone={false}
              data={segmentsPie}
              innerRadius={60}
              style={{ labels: { fontSize: 12, fill: "#303030", fontWeight:'bold', fontFamily: "Roboto", textAlign:'center' }, alignSelf:'center'}}
              colorScale={segmentsColors}
              />
          </Svg>
          {this.getWrapView(segments, segmentsColors)}
          <LostDropRevenueLoss loss={revenueLoss} style={{marginTop:10, marginBottom:10}}/>

        </View>
        </ScrollView>
      )}

      </View>
    )
  }
}


const styles = StyleSheet.create({
 wrapView: {
   marginTop:-10,
   flexWrap: 'wrap',
   flexDirection:'row',
   alignItems: 'flex-start',
   justifyContent:'center',
   backgroundColor:'#fff',
 },
 wrapButton: {
   backgroundColor:'#808080',
   paddingLeft:4,
   paddingRight:4,
   paddingTop:2,
   paddingBottom:2,
   margin:5,
   marginLeft:10,
   borderRadius:5,
   elevation:2,
   width:'40%',
   alignItems:'center'
 },
 wrapText: {
   color:'#fff',
   fontSize:16,
 }
});
