package com.salescrm.telephony.db.change_lead_stages;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 23/12/16.
 */
public class Stages extends RealmObject {


    private String stage_id;

    private String stage_name;

    private String category_id;

    private String car_model;

    public RealmList<NestedStage> nested_stages = new RealmList<>();

    public String getStage_name() {
        return stage_name;
    }

    public void setStage_name(String stage_name) {
        this.stage_name = stage_name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getStage_id() {
        return stage_id;
    }

    public void setStage_id(String stage_id) {
        this.stage_id = stage_id;
    }

    public RealmList<NestedStage> getNested_stages() {
        return nested_stages;
    }

    public void setNested_stages(RealmList<NestedStage> nested_stages) {
        this.nested_stages = nested_stages;
    }

    public String getCar_model() {
        return car_model;
    }

    public void setCar_model(String car_model) {
        this.car_model = car_model;
    }
}
