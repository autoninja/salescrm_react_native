package com.salescrm.telephony.response;


import java.io.Serializable;
import java.util.List;

/**
 * Created by Ravindra P on 13-12-2016.
 */

public class MobileAppDataResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result {
        private Integer force_update;
        private List<String> release_notes;
        private GameData game_data;
        private ACRLicense acr_license_details;
        private String[] acr_models;
        private AWSCredentials aws_credentials;
        private HereMapsCredentials here_maps_credentials;

        public HereMapsCredentials getHere_maps_credentials() {
            return here_maps_credentials;
        }

        public void setHere_maps_credentials(HereMapsCredentials here_maps_credentials) {
            this.here_maps_credentials = here_maps_credentials;
        }

        public AWSCredentials getAws_credentials() {
            return aws_credentials;
        }

        public void setAws_credentials(AWSCredentials aws_credentials) {
            this.aws_credentials = aws_credentials;
        }

        public ACRLicense getAcr_license_details() {
            return acr_license_details;
        }

        public void setAcr_license_details(ACRLicense acr_license_details) {
            this.acr_license_details = acr_license_details;
        }

        public GameData getGame_data() {
            return game_data;
        }

        public void setGame_data(GameData game_data) {
            this.game_data = game_data;
        }

        public Integer getForce_update() {
            return force_update;
        }

        public void setForce_update(Integer force_update) {
            this.force_update = force_update;
        }

        public List<String> getRelease_notes() {
            return release_notes;
        }

        public void setRelease_notes(List<String> release_notes) {
            this.release_notes = release_notes;
        }

        public String[] getAcr_models() {
            return acr_models;
        }

        public void setAcr_models(String[] acr_models) {
            this.acr_models = acr_models;
        }

    }

    public class GameData {
        private boolean booster;
        private String rank;
        private String time_left;
        private List<Points> points;
        private LuckyWheelContent spin_the_wheels;
        private List<BadgeDetails> badge_details;

        public String getDays_left() {
            return time_left;
        }

        public void setDays_left(String days_left) {
            this.time_left = days_left;
        }

        public List<BadgeDetails> getBadge_details() {
            return badge_details;
        }

        public void setBadge_details(List<BadgeDetails> badge_details) {
            this.badge_details = badge_details;
        }

        public boolean isBooster() {
            return booster;
        }

        public void setBooster(boolean booster) {
            this.booster = booster;
        }

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public List<Points> getPoints() {
            return points;
        }

        public void setPoints(List<Points> points) {
            this.points = points;
        }

        public LuckyWheelContent getSpin_the_wheels() {
            return spin_the_wheels;
        }

        public void setSpin_the_wheels(LuckyWheelContent spin_the_wheels) {
            this.spin_the_wheels = spin_the_wheels;
        }
    }

    public class ACRLicense{
        private String license;
        private String serial;

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getSerial() {
            return serial;
        }

        public void setSerial(String serial) {
            this.serial = serial;
        }
    }

    public class AWSCredentials{
        private String s3_user_name;
        private String s3_password;
        private String s3_bucket_name;

        public String getS3_user_name() {
            return s3_user_name;
        }

        public void setS3_user_name(String s3_user_name) {
            this.s3_user_name = s3_user_name;
        }

        public String getS3_password() {
            return s3_password;
        }

        public void setS3_password(String s3_password) {
            this.s3_password = s3_password;
        }

        public String getS3_bucket_name() {
            return s3_bucket_name;
        }

        public void setS3_bucket_name(String s3_bucket_name) {
            this.s3_bucket_name = s3_bucket_name;
        }
    }

    public class HereMapsCredentials {
        private String app_id;
        private String app_code;
        private String mode;

        public String getApp_id() {
            return app_id;
        }

        public void setApp_id(String app_id) {
            this.app_id = app_id;
        }

        public String getApp_code() {
            return app_code;
        }

        public void setApp_code(String app_code) {
            this.app_code = app_code;
        }

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }
    }


    public class Points{
        private String title;
        private String point;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPoint() {
            return point;
        }

        public void setPoint(String point) {
            this.point = point;
        }
    }

    public class BadgeDetails
    {
        private String id;

        private String name;

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }
    }

    public class LuckyWheelContent {

        private String lead_id;

        private String scheduled_activity_id;

        private Heading heading;

        private String selected_option;

        private List<Option> options = null;

        private List<DseUnderTeamLead> dse_under_me = null;

        public List<DseUnderTeamLead> getDse_under_me() {
            return dse_under_me;
        }

        public void setDse_under_me(List<DseUnderTeamLead> dse_under_me) {
            this.dse_under_me = dse_under_me;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public String getScheduled_activity_id() {
            return scheduled_activity_id;
        }

        public void setScheduled_activity_id(String scheduled_activity_id) {
            this.scheduled_activity_id = scheduled_activity_id;
        }

        public Heading getHeading() {
            return heading;
        }

        public void setHeading(Heading heading) {
            this.heading = heading;
        }

        public List<Option> getOptions() {
            return options;
        }

        public void setOptions(List<Option> options) {
            this.options = options;
        }

        public String getSelected_option() {
            return selected_option;
        }

        public void setSelected_option(String selected_option) {
            this.selected_option = selected_option;
        }
    }

    public class Heading {

        private String title;

        private String description;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public class Option {

        private Integer option_id;
        private String title;
        private String description;
        private String color_code;
        private Integer booster_sign;
        private Integer spin_id;

        public Integer getOption_id() {
            return option_id;
        }

        public void setOption_id(Integer option_id) {
            this.option_id = option_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getColor_code() {
            return color_code;
        }

        public void setColor_code(String color_code) {
            this.color_code = color_code;
        }

        public Integer getBooster_sign() {
            return booster_sign;
        }

        public void setBooster_sign(Integer booster_sign) {
            this.booster_sign = booster_sign;
        }

        public Integer getSpin_id() {
            return spin_id;
        }

        public void setSpin_id(Integer spin_id) {
            this.spin_id = spin_id;
        }
    }

    public class DseUnderTeamLead implements Serializable{
        private String user_id;

        private String user_name;

        private String location_id;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }
    }
}
