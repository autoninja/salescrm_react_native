package com.salescrm.telephony.response.gamification;

import java.util.ArrayList;

/**
 * Created by nndra on 30-Jan-18.
 */

public class GamificationSMProfileResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private String manager_name;

        private String manager_average_points;

        private String manager_photo_url;

        private String title;

        private Points points;

        public String getTitle ()
        {
            return title;
        }

        public void setTitle (String title)
        {
            this.title = title;
        }

        public String getManager_name() {
            return manager_name;
        }

        public void setManager_name(String manager_name) {
            this.manager_name = manager_name;
        }

        public String getManager_average_points() {
            return manager_average_points;
        }

        public void setManager_average_points(String manager_average_points) {
            this.manager_average_points = manager_average_points;
        }

        public String getManager_photo_url() {
            return manager_photo_url;
        }

        public void setManager_photo_url(String manager_photo_url) {
            this.manager_photo_url = manager_photo_url;
        }

        public Points getPoints() {
            return points;
        }

        public void setPoints(Points points) {
            this.points = points;
        }
    }

    public class Points
    {
        private ArrayList<Users> users;

        private String[] others;

        public ArrayList<Users> getUsers ()
        {
            return users;
        }

        public void setUsers (ArrayList<Users> users)
        {
            this.users = users;
        }

        public String[] getOthers ()
        {
            return others;
        }

        public void setOthers (String[] others)
        {
            this.others = others;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [users = "+users+", others = "+others+"]";
        }
    }

    public class Users
    {
        private String team_leader_name;

        private String team_leader_id;

        private String team_average_points;

        private String user_id;

        private String team_leader_photo_url;

        private String team_id;

        private String best_team_ever;

        private Integer above_average;

        private String team_name;

        private String spotlight;

        public String getTeam_leader_name() {
            return team_leader_name;
        }

        public void setTeam_leader_name(String team_leader_name) {
            this.team_leader_name = team_leader_name;
        }

        public String getTeam_leader_id() {
            return team_leader_id;
        }

        public void setTeam_leader_id(String team_leader_id) {
            this.team_leader_id = team_leader_id;
        }

        public String getTeam_average_points() {
            return team_average_points;
        }

        public void setTeam_average_points(String team_average_points) {
            this.team_average_points = team_average_points;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getTeam_leader_photo_url() {
            return team_leader_photo_url;
        }

        public void setTeam_leader_photo_url(String team_leader_photo_url) {
            this.team_leader_photo_url = team_leader_photo_url;
        }

        public String getTeam_id() {
            return team_id;
        }

        public void setTeam_id(String team_id) {
            this.team_id = team_id;
        }

        public String getBest_team_ever() {
            return best_team_ever;
        }

        public void setBest_team_ever(String best_team_ever) {
            this.best_team_ever = best_team_ever;
        }

        public Integer getAbove_average() {
            return above_average;
        }

        public void setAbove_average(Integer above_average) {
            this.above_average = above_average;
        }

        public String getTeam_name() {
            return team_name;
        }

        public void setTeam_name(String team_name) {
            this.team_name = team_name;
        }

        public String getSpotlight() {
            return spotlight;
        }

        public void setSpotlight(String spotlight) {
            this.spotlight = spotlight;
        }
    }

    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }


}