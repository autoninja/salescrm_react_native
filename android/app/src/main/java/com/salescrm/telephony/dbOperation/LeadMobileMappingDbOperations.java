package com.salescrm.telephony.dbOperation;

import com.salescrm.telephony.db.AllMobileNumbersSearch;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.response.AddMobileNumberResponse;
import com.salescrm.telephony.response.DeleteMobileResponse;
import com.salescrm.telephony.response.EditMobileNumberResponse;
import com.salescrm.telephony.utils.Util;

import io.realm.Realm;
import io.realm.RealmResults;

public class LeadMobileMappingDbOperations {

    public static void updateCallingMobileNumbers(Realm realm) {

        realm.beginTransaction();

        RealmResults<SalesCRMRealmTable> salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("isLeadActive", 1)
                .equalTo("createdOffline", false)
                .findAll();
        if (salesCRMRealmTable != null) {

            for (int i = 0; i < salesCRMRealmTable.size(); i++) {
                for (int j = 0; j < salesCRMRealmTable.get(i).customerPhoneNumbers.size(); j++) {
                    AllMobileNumbersSearch allMobileNumbersSearch = new AllMobileNumbersSearch();
                    String name = null ;
                    if(salesCRMRealmTable.get(i).getFirstName()!=null) {
                        name = salesCRMRealmTable.get(i).getFirstName() + " ";
                    }
                    if(salesCRMRealmTable.get(i).getLastName()!=null) {
                        name += salesCRMRealmTable.get(i).getLastName();
                    }
                    if(name!=null) {
                        allMobileNumbersSearch.setName(name);
                    }
                    if (salesCRMRealmTable.get(i) != null) {
                        allMobileNumbersSearch.setLeadId(salesCRMRealmTable.get(i).getLeadId());
                    } else {
                        allMobileNumbersSearch.setLeadId(0);
                    }
                    allMobileNumbersSearch.setDeleteStatus(0);
                    allMobileNumbersSearch.setMobileNumber("" + salesCRMRealmTable.get(i).customerPhoneNumbers.get(j).getPhoneNumber());
                    realm.copyToRealmOrUpdate(allMobileNumbersSearch);
                }
            }
        }
        realm.commitTransaction();
    }

    public static void updateCallingMobileNumbers(Realm realm, AddMobileNumberResponse addMobileResponse) {
        System.out.println("Udpate Lead Mobile Called :: New Number Added");
        if (addMobileResponse == null
                || addMobileResponse.getAddMobileNumberResponse() == null
                || addMobileResponse.getAddMobileNumberResponse().getResult() == null
                || addMobileResponse.getAddMobileNumberResponse().getResult().getMobileData() == null) {
            return;
        }
        realm.beginTransaction();

        for (int i = 0; i < addMobileResponse.getAddMobileNumberResponse().getResult().getMobileData().length; i++) {
            AllMobileNumbersSearch allMobileNumbersSearch = new AllMobileNumbersSearch();
            allMobileNumbersSearch.setName("");
            allMobileNumbersSearch.setDeleteStatus(0);
            allMobileNumbersSearch.setLeadId(Util.getLongDefaultZero(addMobileResponse.getAddMobileNumberResponse().getResult().getMobileData()[i].getLead_id()));
            allMobileNumbersSearch.setMobileNumber("" + addMobileResponse.getAddMobileNumberResponse().getResult().getMobileData()[i].getNumber());
            System.out.println("allMobileNumbersSearch" + allMobileNumbersSearch);
            System.out.println(allMobileNumbersSearch.toString());

            System.out.println(realm.where(AllMobileNumbersSearch.class)
                    .contains("mobileNumber", allMobileNumbersSearch.getMobileNumber())
                    .findFirst());

            realm.copyToRealmOrUpdate(allMobileNumbersSearch);
        }
        realm.commitTransaction();
    }

    public static void updateCallingMobileNumbers(Realm realm, EditMobileNumberResponse addMobileResponse) {
        if (addMobileResponse == null
                || addMobileResponse.getEditMobileNumberResponse() == null
                || addMobileResponse.getEditMobileNumberResponse().getResult() == null
                || addMobileResponse.getEditMobileNumberResponse().getResult().getMobileData() == null) {
            return;
        }
        realm.beginTransaction();

        for (int i = 0; i < addMobileResponse.getEditMobileNumberResponse().getResult().getMobileData().length; i++) {
            AllMobileNumbersSearch allMobileNumbersSearch = new AllMobileNumbersSearch();
            allMobileNumbersSearch.setName("");
            allMobileNumbersSearch.setDeleteStatus(0);
            allMobileNumbersSearch.setLeadId(Util.getLongDefaultZero(addMobileResponse.getEditMobileNumberResponse().getResult().getMobileData()[i].getLead_id()));
            allMobileNumbersSearch.setMobileNumber("" + addMobileResponse.getEditMobileNumberResponse().getResult().getMobileData()[i].getNumber());
            realm.copyToRealmOrUpdate(allMobileNumbersSearch);
        }
        realm.commitTransaction();
    }

    public static void updateCallingMobileNumbers(Realm realm, DeleteMobileResponse addMobileResponse) {
        if (addMobileResponse == null
                || addMobileResponse.getDeleteMobileResponse() == null
                || addMobileResponse.getDeleteMobileResponse().getMobile() == null) {
            return;
        }
        realm.beginTransaction();

        System.out.println("Details" + addMobileResponse.getDeleteMobileResponse().getMobile());
        AllMobileNumbersSearch allMobileNumbersSearch = realm.where(AllMobileNumbersSearch.class).equalTo("mobileNumber", addMobileResponse.getDeleteMobileResponse().getMobile()).findFirst();
        if (allMobileNumbersSearch != null) {
            allMobileNumbersSearch.deleteFromRealm();
        }
        realm.commitTransaction();
    }

    public static void copyOrUpdateMobileNumber(Realm realm, String mobile, long leadId) {
        //telephony
        if(Util.isNotNull(mobile)) {
            realm.beginTransaction();
            AllMobileNumbersSearch allMobileNumbersSearch = new AllMobileNumbersSearch();
            allMobileNumbersSearch.setLeadId(leadId);
            allMobileNumbersSearch.setMobileNumber(mobile);
            allMobileNumbersSearch.setDeleteStatus(0);
            realm.copyToRealmOrUpdate(allMobileNumbersSearch);
            realm.commitTransaction();
        }

    }


}
