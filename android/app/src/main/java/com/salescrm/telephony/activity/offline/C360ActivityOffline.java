package com.salescrm.telephony.activity.offline;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.graphics.drawable.VectorDrawableCompat;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.offline.C360PagerAdapterOffline;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;

import com.salescrm.telephony.fragments.FabFragment;
import com.salescrm.telephony.fragments.offline.FabC360FragmentOffline;
import com.salescrm.telephony.preferences.Preferences;

import com.salescrm.telephony.response.CustomerDetailsResponse;
import com.salescrm.telephony.response.SearchResponse;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by bharath on 28/12/16.
 */
public class C360ActivityOffline extends AppCompatActivity{

    TextView tvC360NameMr, tvC360Name, tvC360LeadAge, tvC360Car;
    private ImageButton ibActionBack;
    private FloatingActionButton fabC360;
    private String leadId;
    private Realm realm;
    private ImageView tvC360Indicator;
    private RelativeLayout relC360OvalHolder;
    private TextView tvC360LeadStage,tvC360Offline;
    private AppBarLayout appBarC360;
    private RelativeLayout relLoading;
    private TabLayout tabLayout;
    private C360PagerAdapterOffline adapter;
    private ViewPager viewPager;
    private String customerID;
    public static CustomerDetailsResponse.Result cardDetailResponseData;
    private SearchResponse.Result searchResponse;
    
    public  SalesCRMRealmTable salesCRMRealmTable;
    public  int scheduledActivityId;
    private CreateLeadInputDataDB createLeadDB;
   // ImageButton btOffline;
    private List<ImageView> ovalImageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_c360);
        realm = Realm.getDefaultInstance();
        ovalImageList = new ArrayList<ImageView>();
        appBarC360 = (AppBarLayout) findViewById(R.id.toolbar_card_detail_card);
      //  btOffline = (ImageButton) findViewById(R.id.img_c360_go_360);
        tvC360NameMr = (TextView) findViewById(R.id.tv_c360_person_name_mr);
        tvC360Name = (TextView) findViewById(R.id.tv_c360_person_name);
        tvC360LeadAge = (TextView) findViewById(R.id.tv_c360_lead_age);
        tvC360Car = (TextView) findViewById(R.id.tv_c360_car);
        tvC360Indicator = (ImageView) findViewById(R.id.tv_c360_indicator);
        relC360OvalHolder = (RelativeLayout) findViewById(R.id.rel_c360_progress_oval);
        tvC360LeadStage = (TextView) findViewById(R.id.tv_c360_stage);
        ibActionBack = (ImageButton) findViewById(R.id.img_c360_go_back);
        relLoading = (RelativeLayout) findViewById(R.id.rel_loading_frame);
        viewPager = (ViewPager) findViewById(R.id.pageractioncard);
        tabLayout = (TabLayout) findViewById(R.id.tab_layoutactioncard);
        fabC360 = (FloatingActionButton) findViewById(R.id.card_float);
        tvC360Offline = (TextView) findViewById(R.id.tv_c360_offline);

        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"));
        tabLayout.addTab(tabLayout.newTab().setText("Actions"));
        tabLayout.addTab(tabLayout.newTab().setText("Cars"));
        tabLayout.addTab(tabLayout.newTab().setText("Details"));
        tabLayout.setBackgroundColor(Color.TRANSPARENT);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        relLoading.setVisibility(View.GONE);
//        btOffline.setClickable(false);
//        btOffline.setEnabled(false);
//        btOffline.setImageDrawable(VectorDrawableCompat.create(getResources(), R.drawable.ic_offline_dark,getTheme()));
//
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            scheduledActivityId =extras.getInt("scheduled_activity_id");
            salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId",scheduledActivityId).findFirst();
            if(salesCRMRealmTable==null){
                return;
            }
            else {
                createLeadDB = salesCRMRealmTable.getCreateLeadInputDataDB();
                if(createLeadDB==null){
                    return;
                }
            }
        }
        else {
            return;
        }
        for (int i = 0; i < relC360OvalHolder.getChildCount(); i++) {
            if (relC360OvalHolder.getChildAt(i) instanceof ImageView && relC360OvalHolder.getChildAt(i).getId() != R.id.tv_c360_indicator) {
                ovalImageList.add((ImageView) relC360OvalHolder.getChildAt(i));
                relC360OvalHolder.getChildAt(i).setVisibility(View.VISIBLE);
            }
        }
        init();

        ibActionBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        fabC360.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                Bundle bundle = new Bundle();
                bundle.putInt("scheduledActivityId", scheduledActivityId);
                Fragment fragment = new FabC360FragmentOffline();
                fragment.setArguments(bundle);
                fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.rel_bottom_frame,fragment).commit();
                fabC360.hide();
            }

        });

    }


    public void onBackPressed() {
        fabC360.show();
        super.onBackPressed();

    }


    @Override
    protected void onResume() {
        super.onResume();
//        System.out.println("OnResume triggered");
    }

    private void updateViews() {
        relLoading.setVisibility(View.GONE);
        tabLayout.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.VISIBLE);
        fabC360.show();
    }

    public void init() {
        tvC360Indicator.setVisibility(View.VISIBLE);
        adapter = new C360PagerAdapterOffline(getSupportFragmentManager(), tabLayout.getTabCount(),scheduledActivityId);
        viewPager.setAdapter(adapter);
        updateViews();

        tvC360Name.setText(createLeadDB.getLead_data().getCustomer_details().getFirst_name()
                + " "+
                createLeadDB.getLead_data().getCustomer_details().getLast_name());
        if (Util.isNotNull(createLeadDB.getLead_data().getCustomer_details().getTitle())) {
            tvC360NameMr.setText(createLeadDB.getLead_data().getCustomer_details().getTitle());
        }
        else {
            tvC360NameMr.setText("Mr.");
        }



        tvC360LeadAge.setText(0+"");
        if(createLeadDB.getLead_data().getCar_details().size()>0) {
            tvC360Car.setText(createLeadDB.getLead_data().getCar_details().get(0).getModel_name());
        }
        tvC360LeadStage.setText("Not Contacted");

      

        // System.out.println("inter" + indicatorPoint);
        if (createLeadDB.getLead_data().getLead_tag_id().equalsIgnoreCase(WSConstants.LEAD_TAG_HOT_ID)) {
            appBarC360.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.hot));
        } else if (createLeadDB.getLead_data().getLead_tag_id().equalsIgnoreCase(WSConstants.LEAD_TAG_WARM_ID)) {
            appBarC360.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.warm));
        } else if (createLeadDB.getLead_data().getLead_tag_id().equalsIgnoreCase(WSConstants.LEAD_TAG_COLD_ID)) {
            appBarC360.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.cold));
        } else if (createLeadDB.getLead_data().getLead_tag_id().equalsIgnoreCase(WSConstants.LEAD_TAG_OVERDUE_ID)) {
            appBarC360.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.overdue));

        }
        setStageIndicator(0);
        
    }
    
    private void setStageIndicator(int indicatorPoint){

        //{Indicator;
        //Indicator point should be zero to 8
        if (indicatorPoint >= WSConstants.LEAD_STAGE_MIN && indicatorPoint <= WSConstants.LEAD_STAGE_MAX) {

            //{Indicator ImageView
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvC360Indicator.getLayoutParams();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                params.addRule(RelativeLayout.ALIGN_START, getResources().getIdentifier("oval_view_" + indicatorPoint, "id", getPackageName()));
            } else {
                params.addRule(RelativeLayout.ALIGN_LEFT, getResources().getIdentifier("oval_view_" + indicatorPoint, "id", getPackageName()));
            }
            tvC360Indicator.setLayoutParams(params);
            //Indicator ImageView}


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ovalImageList.get(indicatorPoint).setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.progress_oval_active));
            } else {
                ovalImageList.get(indicatorPoint).setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.progress_oval_active));
            }
        }
        
    }

}