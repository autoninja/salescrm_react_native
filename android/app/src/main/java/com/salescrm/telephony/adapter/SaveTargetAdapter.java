package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.salescrm.telephony.R;
import com.salescrm.telephony.db.AppConfigDB;
import com.salescrm.telephony.db.TargetsDB;
import com.salescrm.telephony.db.team_dashboard_gm_db.Absolute;
import com.salescrm.telephony.model.TargetSectionModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.CircleTransform;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import io.realm.Realm;
import io.realm.RealmList;

import static java.lang.Integer.parseInt;

/**
 * Created by bannhi on 24/8/17.
 */

public class SaveTargetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    RealmList<Absolute> allTargetsList = new RealmList<>();
    ArrayList<TargetSectionModel> targetSectionModelArrayList = new ArrayList<>();
    Activity activity;
    TextView exchange, retail, finance;
    double e, t, v, b, r, exch, fin = 0;
    int targetMonth = 0;
    int exch_total, retail_total, fin_total = 0;
    private Preferences pref;
    TargetsDB individualTarget;
    int inputB, inputE, inputR, inputV, inputTd, inputExch, inputFin = 0;
    public static ArrayList<TargetItem> targetItemArrayList = new ArrayList<TargetItem>();

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.set_target_row, null);
        return new SaveTargetAdapter.TargetItemHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        for (TargetSectionModel sectionModel : targetSectionModelArrayList) {
            if (position == sectionModel.getSectionPostion()) {
                ((SaveTargetAdapter.TargetItemHolder) holder).teamLeadName.setText(sectionModel.getTeamLeadName());
                ((SaveTargetAdapter.TargetItemHolder) holder).teamLeadName.setVisibility(View.VISIBLE);
                break;
            } else {
                ((SaveTargetAdapter.TargetItemHolder) holder).teamLeadName.setVisibility(View.GONE);
            }
        }

        //}
        String url = allTargetsList.get(position).getDpUrl();
        ((SaveTargetAdapter.TargetItemHolder) holder).text_user.setText("" + allTargetsList.get(position).getUserName().toUpperCase().substring(0, 1));
        ((SaveTargetAdapter.TargetItemHolder) holder).text_user.setVisibility(View.VISIBLE);
        ((SaveTargetAdapter.TargetItemHolder) holder).imgUser.setVisibility(View.GONE);


        if(!url.isEmpty()){
            Glide.with(activity).
                    load(url)
                    .asBitmap()
                    .centerCrop()
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                            ((SaveTargetAdapter.TargetItemHolder) holder).imgUser.setVisibility(View.GONE);
                            ((SaveTargetAdapter.TargetItemHolder) holder).text_user.setVisibility(View.VISIBLE);

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            ( (SaveTargetAdapter.TargetItemHolder) holder).imgUser.setVisibility(View.VISIBLE);
                            ( (SaveTargetAdapter.TargetItemHolder) holder).text_user.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(new BitmapImageViewTarget(((SaveTargetAdapter.TargetItemHolder) holder).imgUser) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            if(resource!=null && activity!=null) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                ((SaveTargetAdapter.TargetItemHolder) holder).imgUser.setImageDrawable(circularBitmapDrawable);
                            }
                        }
                    });



        }else {
            ((SaveTargetAdapter.TargetItemHolder) holder).imgUser.setVisibility(View.GONE);
            ((SaveTargetAdapter.TargetItemHolder) holder).text_user.setVisibility(View.VISIBLE);
            ((SaveTargetAdapter.TargetItemHolder) holder).text_user.setText("" + allTargetsList.get(position).getUserName().toUpperCase().substring(0, 1));
        }




        ((TargetItemHolder) holder).target_user_name_tv.setText("" + allTargetsList.get(position).getUserName().toUpperCase());
        ((TargetItemHolder) holder).enquiry_no_tv.setVisibility(View.GONE);
        ((TargetItemHolder) holder).retail_no_tv.setVisibility(View.GONE);
        ((TargetItemHolder) holder).booking_no_tv.setVisibility(View.GONE);
        ((TargetItemHolder) holder).exch_no_tv.setVisibility(View.GONE);
        ((TargetItemHolder) holder).fin_no_tv.setVisibility(View.GONE);
        ((TargetItemHolder) holder).visit_no_tv.setVisibility(View.GONE);
        ((TargetItemHolder) holder).td_no_tv.setVisibility(View.GONE);

        ((TargetItemHolder) holder).enquiry_no_edt.setVisibility(View.VISIBLE);
        ((TargetItemHolder) holder).retail_no_edt.setVisibility(View.VISIBLE);
        ((TargetItemHolder) holder).booking_no_edt.setVisibility(View.VISIBLE);
        ((TargetItemHolder) holder).visit_no_edt.setVisibility(View.VISIBLE);
        ((TargetItemHolder) holder).td_no_edt.setVisibility(View.VISIBLE);
        ((TargetItemHolder) holder).exch_no_edt.setVisibility(View.VISIBLE);
        ((TargetItemHolder) holder).fin_no_edt.setVisibility(View.VISIBLE);

        ((TargetItemHolder) holder).enquiry_no_edt.setTag(position);
        ((TargetItemHolder) holder).retail_no_edt.setTag(position);
        ((TargetItemHolder) holder).booking_no_edt.setTag(position);
        ((TargetItemHolder) holder).visit_no_edt.setTag(position);
        ((TargetItemHolder) holder).td_no_edt.setTag(position);
        ((TargetItemHolder) holder).exch_no_edt.setTag(position);
        ((TargetItemHolder) holder).fin_no_edt.setTag(position);
        ((TargetItemHolder) holder).enquiry_no_edt.setText("" + targetItemArrayList.get(position).getEnquiries());
        ((TargetItemHolder) holder).retail_no_edt.setText("" + targetItemArrayList.get(position).getRetail());
        ((TargetItemHolder) holder).booking_no_edt.setText("" + targetItemArrayList.get(position).getBookings());
        ((TargetItemHolder) holder).visit_no_edt.setText("" + targetItemArrayList.get(position).getVisits());
        ((TargetItemHolder) holder).td_no_edt.setText("" + targetItemArrayList.get(position).getTdrives());
        ((TargetItemHolder) holder).exch_no_edt.setText("" + targetItemArrayList.get(position).getExchange());
        ((TargetItemHolder) holder).fin_no_edt.setText("" + targetItemArrayList.get(position).getFinance());
        if (allTargetsList.get(position).getUserId() != null) {
            targetItemArrayList.get(position).setUser_id(allTargetsList.get(position).getUserId());
        }
        Date date = new Date();
        if (targetMonth == 0) {
            targetItemArrayList.get(position).setTargetdate(new SimpleDateFormat("yyyy-MM-dd").format(date));
        } else if (targetMonth == 1) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            targetItemArrayList.get(position).setTargetdate(new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()));
        }
        System.out.println("TARGET MONTH IS POSITION" + targetItemArrayList.get(position).getTargetdate());

        ///////////////////////////////////////////////enquiry///////////////////////////////////////////////////////
        ((TargetItemHolder) holder).exch_no_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    s = "0";
                }
                if (s != null && s.length() != 0) {
                    exch_total -= targetItemArrayList.get(position).getExchange();
                    exch_total += parseInt(s.toString());
                    exchange.setText("" + exch_total);
                    targetItemArrayList.get(position).setExchange(parseInt(s.toString()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        ///////////////////////////////////////////////retail///////////////////////////////////////////////////////
        ((TargetItemHolder) holder).retail_no_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                System.out.println("BEFORE TEXT IS CALLED: " + s.toString() + " START:" + start + " COUNT: " + count + " after: " + after);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                System.out.println("ON TEXT IS CALLED: " + s.toString() + "START: " + start + " COUNT: " + count + " before: " + before);
                if (s.toString().isEmpty()) {
                    s = "0";
                }
                if (s != null && s.length() != 0) {
                    inputR = parseInt(s.toString());
                    retail_total -= targetItemArrayList.get(position).getRetail();

                    inputExch = (int) Math.ceil((double) (inputR * exch));
                    inputFin = (int) Math.ceil((double) (inputR * fin));
                    inputE = (int) Math.ceil((double) (inputR * e));
                    inputB = (int) Math.ceil((double) (inputR * b));
                    inputTd = (int) Math.ceil((double) (inputR * t));
                    inputV = (int) Math.ceil((double) (inputR * v));


                    int storedRetail = targetItemArrayList.get(position).getRetail();
                    int storedExch = targetItemArrayList.get(position).getExchange();
                    int storedFin = targetItemArrayList.get(position).getFinance();
                    int storedEnq = targetItemArrayList.get(position).getEnquiries();
                    int storedTd = targetItemArrayList.get(position).getTdrives();
                    int storedVisit = targetItemArrayList.get(position).getVisits();
                    int storedBooking = targetItemArrayList.get(position).getBookings();


                    if(inputR!=0 && inputR!=storedRetail){
                        if((storedExch!=0 && storedExch==inputExch) || (storedExch ==0)){
                            ((TargetItemHolder) holder).exch_no_edt.setText("" + inputExch);
                           // targetItemArrayList.get(position).setEnquiries(inputE);
                        } else if(storedExch!=inputE && storedExch!=0){
                            ((TargetItemHolder) holder).exch_no_edt.setText(""+inputExch);
                           // targetItemArrayList.get(position).setEnquiries(inputE);
                        }
                    } else if(inputR==0){
                          if(storedRetail==0){
                              ((TargetItemHolder) holder).exch_no_edt.setText("0");
                          }else if(storedRetail!=0){
                              ((TargetItemHolder) holder).exch_no_edt.setText("" +storedExch);
                          }

                    }else if(inputR!=0 && inputR==storedRetail && storedExch==0){
                        ((TargetItemHolder) holder).exch_no_edt.setText("" + inputExch);
                    }
                     /////////////////////////book////////////////////////
                    if(inputR!=0 && inputR!=storedRetail){
                        if((storedFin!=0 && storedFin==inputFin) || (storedFin ==0)){
                            ((TargetItemHolder) holder).fin_no_edt.setText("" + inputFin);
                        } else if(storedFin!=inputFin && storedFin!=0){
                            ((TargetItemHolder) holder).fin_no_edt.setText(""+inputFin);
                        }
                    } else if(inputR==0){
                       if(storedRetail==0) {
                            ((TargetItemHolder) holder).fin_no_edt.setText("0");
                        }else if(storedRetail!=0){
                            ((TargetItemHolder) holder).fin_no_edt.setText("" +storedFin);
                        }
                    }else if(inputR!=0 && inputR==storedRetail && storedFin==0){
                        ((TargetItemHolder) holder).fin_no_edt.setText("" + inputFin);
                    }

                    /////////////////////////Enq////////////////////////
                    if(inputR!=0 && inputR!=storedRetail){
                        if((storedEnq!=0 && storedEnq==inputE) || (storedEnq ==0)){
                            ((TargetItemHolder) holder).enquiry_no_edt.setText("" + inputE);
                        } else if(storedEnq!=inputE && storedEnq!=0){
                            ((TargetItemHolder) holder).enquiry_no_edt.setText(""+inputE);
                        }
                    } else if(inputR==0){
                        if(storedRetail==0) {
                            ((TargetItemHolder) holder).enquiry_no_edt.setText("0");
                        }else if(storedRetail!=0){
                            ((TargetItemHolder) holder).enquiry_no_edt.setText("" +storedEnq);
                        }
                    }else if(inputR!=0 && inputR==storedRetail && storedEnq==0){
                        ((TargetItemHolder) holder).enquiry_no_edt.setText("" + inputE);
                    }

                    /////////////////////////Td////////////////////////
                    if(inputR!=0 && inputR!=storedRetail){
                        if((storedTd!=0 && storedTd==inputTd) || (storedTd ==0)){
                            ((TargetItemHolder) holder).td_no_edt.setText("" + inputTd);
                        } else if(storedTd!=inputTd && storedTd!=0){
                            ((TargetItemHolder) holder).td_no_edt.setText(""+inputTd);
                        }
                    } else if(inputR==0){
                        if(storedRetail==0) {
                            ((TargetItemHolder) holder).td_no_edt.setText("0");
                        }else if(storedRetail!=0){
                            ((TargetItemHolder) holder).td_no_edt.setText("" +storedTd);
                        }
                    }else if(inputR!=0 && inputR==storedRetail && storedTd==0){
                        ((TargetItemHolder) holder).td_no_edt.setText("" + inputTd);
                    }

                    /////////////////////////Visit////////////////////////
                    if(inputR!=0 && inputR!=storedRetail){
                        if((storedVisit!=0 && storedVisit==inputV) || (storedVisit ==0)){
                            ((TargetItemHolder) holder).visit_no_edt.setText("" + inputV);
                        } else if(storedVisit!=inputV && storedVisit!=0){
                            ((TargetItemHolder) holder).visit_no_edt.setText(""+inputV);
                        }
                    } else if(inputR==0){
                        if(storedRetail==0) {
                            ((TargetItemHolder) holder).visit_no_edt.setText("0");
                        }else if(storedRetail!=0){
                            ((TargetItemHolder) holder).visit_no_edt.setText("" +storedVisit);
                        }
                    }else if(inputR!=0 && inputR==storedRetail && storedVisit==0){
                        ((TargetItemHolder) holder).visit_no_edt.setText("" + inputV);
                    }

                    /////////////////////////Booking////////////////////////
                    if(inputR!=0 && inputR!=storedRetail){
                        if((storedBooking!=0 && storedBooking==inputB) || (storedBooking ==0)){
                            ((TargetItemHolder) holder).booking_no_edt.setText("" + inputB);
                        } else if(storedBooking!=inputB && storedBooking!=0){
                            ((TargetItemHolder) holder).booking_no_edt.setText(""+inputB);
                        }
                    } else if(inputR==0){
                        if(storedRetail==0) {
                            ((TargetItemHolder) holder).booking_no_edt.setText("0");
                        }else if(storedRetail!=0){
                            ((TargetItemHolder) holder).booking_no_edt.setText("" +storedBooking);
                        }
                    }else if(inputR!=0 && inputR==storedRetail && storedBooking==0){
                        ((TargetItemHolder) holder).booking_no_edt.setText("" + inputB);
                    }

                    targetItemArrayList.get(position).setRetail(inputR);
                    targetItemArrayList.get(position).setTdrives((int) Math.ceil((double) (inputR * t)));
                    targetItemArrayList.get(position).setVisits((int) Math.ceil((double) (inputR * v)));
                    targetItemArrayList.get(position).setEnquiries((int) Math.ceil((double) (inputR * e)));
                    targetItemArrayList.get(position).setBookings((int) Math.ceil((double) (inputR * b)));
                    retail_total += (inputR);
                    retail.setText("" + retail_total);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ///////////////////////////////////////////////booking///////////////////////////////////////////////////////
        ((TargetItemHolder) holder).fin_no_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    s = "0";
                }

                if (s != null && s.length() != 0) {
                    fin_total -= targetItemArrayList.get(position).getFinance();
                    fin_total += parseInt(s.toString());
                    finance.setText("" + fin_total);
                    targetItemArrayList.get(position).setFinance(parseInt(s.toString()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ((TargetItemHolder) holder).enquiry_no_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    s = "0";
                }

                if (s != null && s.length() != 0) {
                    targetItemArrayList.get(position).setEnquiries(parseInt(s.toString()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ((TargetItemHolder) holder).td_no_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    s = "0";
                }

                if (s != null && s.length() != 0) {
                    targetItemArrayList.get(position).setTdrives(parseInt(s.toString()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ((TargetItemHolder) holder).visit_no_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    s = "0";
                }

                if (s != null && s.length() != 0) {
                    targetItemArrayList.get(position).setVisits(parseInt(s.toString()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ((TargetItemHolder) holder).booking_no_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    s = "0";
                }

                if (s != null && s.length() != 0) {
                    targetItemArrayList.get(position).setBookings(parseInt(s.toString()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        //}
    }

    public SaveTargetAdapter(Activity activity, RealmList<Absolute> allTargetsList,
                             ArrayList<TargetSectionModel> targetSectionModelArrayList,
                             TextView exchange, TextView retail, TextView finance, int exch_total, int fin_total, int retail_total, int targetMonth) {
        this.allTargetsList = allTargetsList;
        this.activity = activity;
        this.targetMonth = targetMonth;
        pref = Preferences.getInstance();
        pref.load(this.activity);
        this.targetItemArrayList.clear();
        this.targetSectionModelArrayList = targetSectionModelArrayList;
        for (int i = 0; i < allTargetsList.size(); i++) {
            TargetItem target = new TargetItem();
            target.setBookings(allTargetsList.get(i).getBookings_target());
            target.setVisits(allTargetsList.get(i).getVisits_target());
            target.setTdrives(allTargetsList.get(i).getTdrives_target());
            target.setEnquiries(allTargetsList.get(i).getEnquiries_target());
            target.setRetail(allTargetsList.get(i).getRetail_target());
            target.setUser_id(allTargetsList.get(i).getUserId());
            target.setExchange(allTargetsList.get(i).getExchange_target());
            target.setFinance(allTargetsList.get(i).getFinance_target());

            Date date = new Date();
            if (targetMonth == 0) {
                target.setTargetdate(new SimpleDateFormat("yyyy-MM-dd").format(date));
            } else if (targetMonth == 1) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, 1);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                target.setTargetdate(new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()));
            }
            this.targetItemArrayList.add(target);
            System.out.println("TARGET MONTH IS" + target.getTargetdate());
        }
        this.exchange = exchange;
        this.retail = retail;
        this.finance = finance;
        this.exch_total = exch_total;
        this.fin_total = fin_total;
        this.retail_total = retail_total;
        setRatio();
    }

    public SaveTargetAdapter() {

    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return this.allTargetsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class TargetItemHolder extends RecyclerView.ViewHolder {

        TextView target_user_name_tv, teamLeadName,text_user;
        EditText retail_no_edt, booking_no_edt, enquiry_no_edt, td_no_edt, visit_no_edt, exch_no_edt, fin_no_edt;
        TextView retail_no_tv, booking_no_tv, enquiry_no_tv, td_no_tv, visit_no_tv, exch_no_tv, fin_no_tv;
        ImageView imgUser;

        public TargetItemHolder(View itemView) {
            super(itemView);
            // parentLayout = itemView;
            target_user_name_tv = (TextView) itemView.findViewById(R.id.target_user_name);
            teamLeadName = (TextView) itemView.findViewById(R.id.team_lead_tv);
            retail_no_edt = (EditText) itemView.findViewById(R.id.retail_no);
            booking_no_edt = (EditText) itemView.findViewById(R.id.booking_no);
            enquiry_no_edt = (EditText) itemView.findViewById(R.id.enquiry_no);

            retail_no_tv = (TextView) itemView.findViewById(R.id.retail_no_tv);
            booking_no_tv = (TextView) itemView.findViewById(R.id.booking_no_tv);
            enquiry_no_tv = (TextView) itemView.findViewById(R.id.enquiry_no_tv);
            imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            text_user =  (TextView) itemView.findViewById(R.id.text_user);
            td_no_tv = (TextView) itemView.findViewById(R.id.td_no_tv);
            visit_no_tv = (TextView) itemView.findViewById(R.id.visit_no_tv);
            exch_no_tv = (TextView) itemView.findViewById(R.id.exch_no_tv);
            fin_no_tv = (TextView) itemView.findViewById(R.id.fin_no_tv);
            td_no_edt = (EditText) itemView.findViewById(R.id.td_no);
            visit_no_edt = (EditText) itemView.findViewById(R.id.visit_no);
            exch_no_edt = (EditText) itemView.findViewById(R.id.exch_no);
            fin_no_edt = (EditText) itemView.findViewById(R.id.fin_no);
        }

    }

    public class TargetItem {
        int user_id;
        int enquiries;
        int tdrives;
        int visits;
        int bookings;
        int retail;
        int exchanged;
        int financed;
        String targetdate;

        public int getExchange() {
            return exchanged;
        }

        public void setExchange(int exchange) {
            this.exchanged = exchange;
        }

        public int getFinance() {
            return financed;
        }

        public void setFinance(int finance) {
            this.financed = finance;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(int enquiries) {
            this.enquiries = enquiries;
        }

        public int getTdrives() {
            return tdrives;
        }

        public void setTdrives(int tdrives) {
            this.tdrives = tdrives;
        }

        public int getVisits() {
            return visits;
        }

        public void setVisits(int visits) {
            this.visits = visits;
        }

        public int getBookings() {
            return bookings;
        }

        public void setBookings(int bookings) {
            this.bookings = bookings;
        }

        public int getRetail() {
            return retail;
        }

        public void setRetail(int retail) {
            this.retail = retail;
        }

        public String getTargetdate() {
            return targetdate;
        }

        public void setTargetdate(String targetdate) {
            this.targetdate = targetdate;
        }
    }

    public class TargetArray {
        ArrayList<TargetItem> targets;

        public ArrayList<TargetItem> getTargets() {
            return targets;
        }

        public void setTargets(ArrayList<TargetItem> targets) {
            this.targets = targets;
        }
    }

    private void setRatio() {
        Realm realm = Realm.getDefaultInstance();
        AppConfigDB appConfigDB = realm.where(AppConfigDB.class).equalTo("key", WSConstants.AppConfig.TARGET_RATIO_ETVBR).findFirst();
        if (appConfigDB != null) {
            String ratio = appConfigDB.getValue();
            StringTokenizer st = new StringTokenizer(ratio, ":");
            //while(st.hasMoreTokens()) {
            e = Double.parseDouble(st.nextToken());
            t = Double.parseDouble(st.nextToken());
            v = Double.parseDouble(st.nextToken());
            b = Double.parseDouble(st.nextToken());
            r = Double.parseDouble(st.nextToken());
            exch = Double.parseDouble(st.nextToken());
            fin = Double.parseDouble(st.nextToken());
            // }
            // r=2;
            if (r != 1) {
                e = e / r;
                t = t / r;
                v = v / r;
                b = b / r;
                exch = exch/r;
                fin = fin/r;
                r = 1;
            }
            System.out.println("RATIO: E:" + e + "T: " + t + "V: " + v + "B: " + b + "R: " + r);
            System.out.println("RATIO: Exch:" + exch + ", F: " + fin);

        }
    }

}

