import React, { Component } from 'react';
import { View, Text, TouchableOpacity} from 'react-native';

export default class LanguageItem extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return( 
            <TouchableOpacity
            onPress = {()=> {
                this.props.onSelect(this.props.languageCode)
            }}
            style={[
                {   
                    backgroundColor:this.props.languageCode==this.props.selectedLanguage?'#007fff':'rgba(255,255,255,0.2)',
                    padding:10,
                    height:64,
                    justifyContent:'center'
                },this.props.style]
            }
            >
                <Text 
                style={
                    {  
                        fontWeight:'bold',
                        color:'#fff', 
                        fontSize:15, textAlign:'center'
                    }
                    }
                >{this.props.languageText}</Text>
            </TouchableOpacity>
        )
    }
}