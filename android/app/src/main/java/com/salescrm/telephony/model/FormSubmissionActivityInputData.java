package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by bharath on 31/10/16.
 */

public class FormSubmissionActivityInputData {

    //{Add activity input data
    private String lead_last_updated;
    private String lead_id;
    private String scheduled_type;
    private String schedule_activity_id;
    //Add activity input data}

    private List<FormSubmissionInputData.Form_response> form_response;

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }

    public String getSchedule_activity_id() {
        return schedule_activity_id;
    }

    public void setSchedule_activity_id(String schedule_activity_id) {
        this.schedule_activity_id = schedule_activity_id;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public List<FormSubmissionInputData.Form_response> getForm_response() {
        return form_response;
    }

    public void setForm_response(List<FormSubmissionInputData.Form_response> form_response) {
        this.form_response = form_response;
    }

    @Override
    public String toString() {
        return "{\"form_response\":\""+form_response+"\",\"lead_last_updated\" : \"" + lead_last_updated + "\",\"lead_id\" : \"" + lead_id + "\"}";
    }

    public String getScheduled_type() {
        return scheduled_type;
    }

    public void setScheduled_type(String scheduled_type) {
        this.scheduled_type = scheduled_type;
    }
}