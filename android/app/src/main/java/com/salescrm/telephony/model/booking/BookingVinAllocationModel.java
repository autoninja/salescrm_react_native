package com.salescrm.telephony.model.booking;

public class BookingVinAllocationModel {

    private String vin_no;
    private String vin_allocation_status_id;
    private String vin_allocation_status;

    public String getVin_no() {
        return vin_no;
    }

    public void setVin_no(String vin_no) {
        this.vin_no = vin_no;
    }

    public String getVin_allocation_status_id() {
        return vin_allocation_status_id;
    }

    public void setVin_allocation_status_id(String vin_allocation_status_id) {
        this.vin_allocation_status_id = vin_allocation_status_id;
    }

    public String getVin_allocation_status() {
        return vin_allocation_status;
    }

    public void setVin_allocation_status(String vin_allocation_status) {
        this.vin_allocation_status = vin_allocation_status;
    }
}
