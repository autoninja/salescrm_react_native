package com.salescrm.telephony.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.db.DefaultCases;
import com.salescrm.telephony.db.FormAnswerDB;
import com.salescrm.telephony.db.FormObjectDb;
import com.salescrm.telephony.db.FormObjectQuestionChildren;
import com.salescrm.telephony.db.FormObjectQuestionValues;
import com.salescrm.telephony.db.FormResponseDB;
import com.salescrm.telephony.db.PlannedActivities;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.ValidationObject;
import com.salescrm.telephony.interfaces.FormUploadListener;
import com.salescrm.telephony.model.FormDefaultHandler;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.salescrm.telephony.utils.Util.showTimePicker;

/**
 * Created by bharath on 25/10/16.
 */

public class ViewGenerator implements DatePickerDialog.OnDateSetListener
        , TimePickerDialog.OnTimeSetListener {
    private final int paddingLeftDefault;
    private final NestedScrollView scrollView;
    //private final FormAnswerDB formAnswerDBConstant;
    private Context context;
    private ViewGroup parentFirst;
    private int paddingBottomDefault;
    private FormSubmissionInputData formSubmissionInputData;
    private List<FormSubmissionInputData.Form_response> form_responses;
    private FormUploadListener formUploadListener;
    private Realm realm;
    private boolean isFromDone;
    private AppCompatEditText currentDateEditor;
    private int answerCount = 0;
    private Set<Integer> checkBoxStack;
    private SparseArray<FormDefaultHandler> formDefaultHandler;
    private AppCompatCheckBox pntCheckBox = null;
    private boolean showTimeView = true;
    private int selectPntTaskAnswerValue = -1;


    public ViewGenerator(TextView btFormSubmit, final Context context, final ViewGroup parent,
                         NestedScrollView scrollView,
                         FormSubmissionInputData formSubmissionData, final boolean isFromDone, final int selectPntTaskAnswerValue) {
        this.context = context;
        this.realm = Realm.getDefaultInstance();
        this.formUploadListener = (FormUploadListener) context;
        this.parentFirst = parent;
        this.scrollView = scrollView;
        this.paddingBottomDefault = (int) Util.convertDpToPixel(8, context);
        this.paddingLeftDefault = (int) Util.convertDpToPixel(8, context);
        this.formSubmissionInputData = formSubmissionData;
        this.form_responses = new ArrayList<>();
        this.isFromDone = isFromDone;
        this.selectPntTaskAnswerValue = selectPntTaskAnswerValue;
        checkBoxStack = new HashSet<>();
        this.formDefaultHandler = new SparseArray<FormDefaultHandler>();
        //formAnswerDBConstant = realm.where(FormAnswerDB.class).equalTo("action_id", formSubmissionInputData.getAction_id() + "").equalTo("scheduled_activity_id", formSubmissionInputData.getScheduled_activity_id() + "").findFirst();
        btFormSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answerCount = 0;
                isFormFilled(parentFirst);
                List<FormSubmissionInputData.Form_response> current_form_responses = new ArrayList<FormSubmissionInputData.Form_response>();
                for (int i = 0; i < form_responses.size(); i++) {
                    if (realm.where(FormObjectQuestionValues.class).equalTo("fQId", form_responses.get(i).getName() + "")
                            .equalTo("ifVisibleMandatory", true).findFirst() != null) {
                        current_form_responses.add(form_responses.get(i));
                        if (form_responses.get(i).getValue() != null) {
                            System.out.println("Id of answer:" + form_responses.get(i).getName() +
                                    ":::Answer Value:" + form_responses.get(i).getValue().getAnswerValue() +
                                    ":::Display text:" + form_responses.get(i).getValue().getDisplayText());
                        }
                    }
                }
                System.out.println("Required form size:::" + answerCount);
                System.out.println("Form current size" + current_form_responses.size());
                if (current_form_responses.size() >= answerCount) {
                    System.out.println("Form -submission");
                    if (formUploadListener != null && checkPntMandatory()) {
                        formSubmissionInputData.setForm_response(form_responses);
                        if (isFromDone) {
                            realm.beginTransaction();
                            realm.where(FormAnswerDB.class).equalTo("action_id", formSubmissionInputData.getAction_id() + "").equalTo("scheduled_activity_id", formSubmissionInputData.getScheduled_activity_id() + "").findFirst().deleteFromRealm();
                            if (Util.getInt(formSubmissionInputData.getAction_id()) == WSConstants.FormAction.RESCHEDULE) {
                                SalesCRMRealmTable data = realm.where(SalesCRMRealmTable.class).equalTo("oldScheduledActivityId", Util.getInt(formSubmissionInputData.getScheduled_activity_id())).findFirst();
                                data.setFormAnswerDB(realm.createObjectFromJson(FormAnswerDB.class, new Gson().toJson(formSubmissionInputData)));

                            } else {
                                SalesCRMRealmTable data = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", Util.getInt(formSubmissionInputData.getScheduled_activity_id())).findFirst();
                                data.setFormAnswerDB(realm.createObjectFromJson(FormAnswerDB.class, new Gson().toJson(formSubmissionInputData)));

                            }
                            realm.commitTransaction();

                        }
                        formUploadListener.onClickFormUpload(formSubmissionInputData,selectPntTaskAnswerValue);
                    }

                } else {
                    System.out.println("Form -submission Error");
                    Toast.makeText(context, "Please enter all the required fields", Toast.LENGTH_LONG).show();
                }

                //showTestAnswerData();
            }
        });
    }

    private boolean checkPntMandatory() {
        if (pntCheckBox != null && pntCheckBox.getVisibility() == View.VISIBLE) {
            RealmResults<PlannedActivities> plannedActivitiesData = realm.where(PlannedActivities.class).equalTo("isDone", false).equalTo("leadID", Util.getInt(formSubmissionInputData.getLead_id())).findAll();
            System.out.println("Planned task size:" + (plannedActivitiesData == null ? 0 : plannedActivitiesData.size()));
            if (plannedActivitiesData != null && plannedActivitiesData.size() == 1 && !pntCheckBox.isChecked()) {
                Toast.makeText(context, "Please plan a next task", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }


    private void showTestAnswerData() {
        for (int i = 0; i < form_responses.size(); i++) {

            System.out.println("\n\n::Form Answer Data::");
            System.out.println("Name:" + form_responses.get(i).getName());
            System.out.print("Value :::");
            System.out.println("fAnsId:" + form_responses.get(i).getValue().getFAnsId());
            System.out.println("displayText:" + form_responses.get(i).getValue().getDisplayText());
            System.out.println("answerValue:" + form_responses.get(i).getValue().getAnswerValue());

        }
    }

    private void createChildView(final ViewGroup parent, final FormObjectQuestionValues questionChildren, int color, final int viewPosition) {
        if (color != -1) {
            parent.setBackgroundColor(color);

        }
        ViewType viewType;

        try {
            viewType = ViewType.valueOf(questionChildren.getFormInputType());
            if ((questionChildren.getFormInputType().equalsIgnoreCase("void"))) {
                viewType = ViewType.voidView;
            }
        } catch (IllegalArgumentException e) {
            viewType = ViewType.voidView;
        }
        int id = questionChildren.getQuestionId();
        String fqId = questionChildren.getfQId();
        String title = questionChildren.getTitle();
        String titleHeader = questionChildren.getTitle();
        if (questionChildren.isIfVisibleMandatory()) {
            if (Util.isNotNull(titleHeader)) {
                titleHeader = titleHeader + " *";
            }
        }
        switch (viewType) {
            case voidView:
                for (int i = 0; i < questionChildren.getQuestionChildren().size(); i++) {
                    for (int j = 0; j < questionChildren.getQuestionChildren().get(i).getValues().size(); j++) {
                        FormObjectQuestionValues currentQuestionChildren = questionChildren.getQuestionChildren().get(i).getValues().get(j);
                        LinearLayout linearLayoutVoid = new LinearLayout(context);
                        linearLayoutVoid.setOrientation(LinearLayout.VERTICAL);
                        parent.addView(linearLayoutVoid);
                        createChildView(linearLayoutVoid, currentQuestionChildren, -1, -1);


                    }


                }
                break;
            case text:
                // add text view
                final LinearLayout linearLayoutText = new LinearLayout(context);
                linearLayoutText.setOrientation(LinearLayout.VERTICAL);

                createHeaderTextView(linearLayoutText, titleHeader, questionChildren);

                AppCompatEditText etText = new AppCompatEditText(context);
                etText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                etText.setHint(title);
                etText.setMaxLines(1);
                etText.setId(id);
                etText.setTag(R.id.form_fq_id, fqId);
                etText.setTag(R.id.form_question_obj, questionChildren);

                changeInputTypeOfEditText(etText, questionChildren.getValidationObject());

                etText.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        removeFormResponseData(questionChildren.getfQId());
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (!s.toString().equalsIgnoreCase("")) {
                            addFormResponseData((questionChildren.getfQId()), "", s.toString(), "");
                        }
                    }
                });
                if (questionChildren.getDefaultFAId() != null && Util.isNotNull(questionChildren.getDefaultFAId().getAnswerValue())) {
                    etText.setText(questionChildren.getDefaultFAId().getAnswerValue());
                }
                //For answer from db
                if (isFromDone) {
                    removeFormResponseData(questionChildren.getfQId());
                    etText.setText(getTextForEditText(questionChildren));
                    addFormResponseData((questionChildren.getfQId()), "", getTextForEditText(questionChildren), "");
                }
                linearLayoutText.addView(etText);
                if (viewPosition >= 0) {
                    parent.addView(linearLayoutText, viewPosition);
                } else {
                    parent.addView(linearLayoutText);

                }
                break;

            case textarea:
                final LinearLayout linearLayoutTextArea = new LinearLayout(context);
                linearLayoutTextArea.setOrientation(LinearLayout.VERTICAL);

                createHeaderTextView(linearLayoutTextArea, titleHeader, questionChildren);
                AppCompatEditText et = new AppCompatEditText(context);
                et.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                et.setHint(title);
                et.setId(id);
                et.setTag(R.id.form_fq_id, fqId);
                et.setMinLines(1);
                et.setMaxLines(3);
                et.setTag(R.id.form_question_obj, questionChildren);

                changeInputTypeOfEditText(et, questionChildren.getValidationObject());


                et.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        removeFormResponseData(questionChildren.getfQId());
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (!s.toString().equalsIgnoreCase("")) {
                            addFormResponseData((questionChildren.getfQId()), "", s.toString(), "");
                        }
                    }
                });
                //Default value settting up
                if (questionChildren.getDefaultFAId() != null && Util.isNotNull(questionChildren.getDefaultFAId().getAnswerValue())) {
                    et.setText(questionChildren.getDefaultFAId().getAnswerValue());
                }

                //Answer from db
                if (isFromDone) {
                    removeFormResponseData(questionChildren.getfQId());
                    et.setText(getTextForEditText(questionChildren));
                    addFormResponseData((questionChildren.getfQId()), "", getTextForEditText(questionChildren), "");
                }
                linearLayoutTextArea.addView(et);
                if (viewPosition >= 0) {
                    parent.addView(linearLayoutTextArea, viewPosition);
                } else {
                    parent.addView(linearLayoutTextArea);

                }
                break;
            case radio:
                final LinearLayout linearLayout = new LinearLayout(context);
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                final RadioGroup radioGroup = new RadioGroup(context);
                radioGroup.setId(id);
                radioGroup.setTag(fqId);
                radioGroup.setTag(R.id.form_fq_id, fqId);
                if (viewPosition >= 0) {
                    radioGroup.setPadding(36, 0, 0, paddingBottomDefault);
                } else {
                    radioGroup.setPadding(0, 0, 0, paddingBottomDefault);
                }

                radioGroup.setTag(R.id.radio_obj, questionChildren);
                final boolean[] isRadioAdded = {false};
                Integer valHeader = 1;
                if (createHeaderTextView(radioGroup, titleHeader, questionChildren) != null) {
                    valHeader = 2;
                }
                radioGroup.setTag(R.id.val_header, valHeader);

                for (int i = 0; i < questionChildren.getAnswerChildren().size(); i++) {
                    if (questionChildren.getAnswerChildren().get(i).getAnswerValue() != null) {
                        final AppCompatRadioButton radioButton = new AppCompatRadioButton(context);
                        radioButton.setText(questionChildren.getAnswerChildren().get(i).getDisplayText().trim());
                        radioButton.setTag(R.id.radio_obj, questionChildren);
                        radioButton.setTag(R.id.radio_count, i);
                        radioButton.setId(new Random().nextInt(12121212));
                        radioButton.setTag(R.id.radio_answer_value, questionChildren.getAnswerChildren().get(i).getAnswerValue());
                        radioGroup.addView(radioButton);
                       /* if (isFromDone && i == getDropDownPosition(questionChildren)) {
                            radioButton.setChecked(true);
                        }*/
                        System.out.println("Radion button id:" + radioButton.getId());
                        System.out.println("Radion button id:" + radioButton.getText());
                    }

                }
                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        AppCompatRadioButton checkedRadioButton = (AppCompatRadioButton) group.findViewById(checkedId);
                        removeFormResponseDataRecursive(((FormObjectQuestionValues) checkedRadioButton.getTag(R.id.radio_obj)));
                        for (int k = linearLayout.getChildCount() - 1; k >= 0; k--) {
                            if (radioGroup.getId() == linearLayout.getChildAt(k).getId()) {
                                for (int l = radioGroup.getChildCount() - 1; l >= 0; l--) {
                                    if (!(radioGroup.getChildAt(l) instanceof AppCompatRadioButton)) {
                                        if (((FormObjectQuestionValues) radioGroup.getChildAt(l).getTag(R.id.radio_obj)) == null ||
                                                !((FormObjectQuestionValues) radioGroup.getChildAt(l).getTag(R.id.radio_obj)).getfQId()
                                                        .equalsIgnoreCase(((FormObjectQuestionValues) radioGroup.getTag(R.id.radio_obj)).getfQId())) {
                                            radioGroup.removeViewAt(l);
                                        }

                                    }
                                }
                            } else {
                                linearLayout.removeViewAt(k);

                            }
                        }
                        if (checkedRadioButton.isChecked()) {
                            changeUiToDefault();
                            RealmResults<DefaultCases> dataCurrent = questionChildren.getDefaultCases().sort("sequence", Sort.ASCENDING);
                            for (int i = 0; i < dataCurrent.size(); i++) {
                                System.out.println("Question Id: " + questionChildren.getQuestionId());
                                if (dataCurrent.get(i).getDependent_question_id().intValue()
                                        == questionChildren.getQuestionId().intValue()
                                        && checkedRadioButton.getTag(R.id.radio_answer_value).toString()
                                        .equalsIgnoreCase(dataCurrent.get(i).getDependent_question_answer_id())) {


                                    changeUiByQuestionId(checkedRadioButton.getId(),
                                            dataCurrent.get(i).getDependent_question_answer_id(),
                                            dataCurrent.get(i).getTarget_question_id(),
                                            dataCurrent.get(i).getPossible_answer_id(),
                                            dataCurrent.get(i).getIs_edit());

                                    System.out.println("::::::Default Cases::::: at " + dataCurrent.get(i).getSequence());
                                    System.out.println("getId:" + dataCurrent.get(i).getId());
                                    System.out.println("getDependent_question_id:" + dataCurrent.get(i).getDependent_question_id());
                                    System.out.println("getDependent_question_answer_id:" + dataCurrent.get(i).getDependent_question_answer_id());
                                    System.out.println("getSequence:" + dataCurrent.get(i).getSequence());
                                    System.out.println("getTarget_question_id:" + dataCurrent.get(i).getTarget_question_id());
                                    System.out.println("getPossible_answer_id:" + dataCurrent.get(i).getPossible_answer_id());
                                    System.out.println("::::::Default Cases:::::");

                                }

                            }

                            //For Show Answer from db
                            /*if(isFromDone) {
                                if (!isRadioAdded[0]&&getRadioPosition(questionChildren,radioGroup)>=0) {
                                    linearLayout.addView(radioGroup);
                                    if (viewPosition >= 0) {
                                        parent.addView(linearLayout,viewPosition);
                                    }
                                    else {
                                        parent.addView(linearLayout);
                                    }
                                    isRadioAdded[0] = true;
                                }
                            }*/

                            FormObjectQuestionValues formObjectValRadio = ((FormObjectQuestionValues) radioGroup.getTag(R.id.radio_obj));
                            System.out.println(formObjectValRadio == null);
                            addFormResponseData(((FormObjectQuestionValues) checkedRadioButton.getTag(R.id.radio_obj)).getfQId(),
                                    ((FormObjectQuestionValues) checkedRadioButton.getTag(R.id.radio_obj)).getAnswerChildren().get((Integer) checkedRadioButton.getTag(R.id.radio_count)).getfAnsId(),
                                    ((FormObjectQuestionValues) checkedRadioButton.getTag(R.id.radio_obj)).getAnswerChildren().get((Integer) checkedRadioButton.getTag(R.id.radio_count)).getAnswerValue(),
                                    ((FormObjectQuestionValues) checkedRadioButton.getTag(R.id.radio_obj)).getAnswerChildren().get((Integer) checkedRadioButton.getTag(R.id.radio_count)).getDisplayText(),
                                    questionChildren.getQuestionId());
                            if (formObjectValRadio != null && formObjectValRadio.getQuestionChildren().size() > (Integer) checkedRadioButton.getTag(R.id.radio_count)) {
                                for (int j = 0; j < ((FormObjectQuestionValues) checkedRadioButton.getTag(R.id.radio_obj)).getQuestionChildren().get((Integer) checkedRadioButton.getTag(R.id.radio_count)).getValues().size(); j++) {
                                    createChildView(radioGroup, ((FormObjectQuestionValues) checkedRadioButton.getTag(R.id.radio_obj)).getQuestionChildren().get((Integer) checkedRadioButton.getTag(R.id.radio_count)).getValues().get(j), -1,
                                            (Integer) checkedRadioButton.getTag(R.id.radio_count) + (Integer) radioGroup.getTag(R.id.val_header) + j);
                                }
                            }

                        }
                    }
                });

               /* if(isFromDone&&getRadioPosition(questionChildren,radioGroup)>=0) {
                    getRadioPosition(questionChildren,radioGroup,true);
                    if(!isRadioAdded[0]&&getRadioPosition(questionChildren,radioGroup)>=0) {
                        linearLayout.addView(radioGroup);
                        if (viewPosition >= 0) {
                            parent.addView(linearLayout,viewPosition);
                        }
                        else {
                            parent.addView(linearLayout);
                        }

                    }

                }*/

                linearLayout.addView(radioGroup);
                if (viewPosition >= 0) {
                    parent.addView(linearLayout, viewPosition);

                } else {
                    parent.addView(linearLayout);

                }
                /*if(isFromDone&&getRadioPosition(questionChildren,radioGroup)<0) {
                    linearLayout.addView(radioGroup);
                    if (viewPosition >= 0) {
                        parent.addView(linearLayout,viewPosition);
                    }
                    else {
                        parent.addView(linearLayout);
                    }
                }*/

                break;

            case dropdown:
                final LinearLayout linearLayoutDropDown = new LinearLayout(context);
                linearLayoutDropDown.setPadding(0, 0, 0, paddingBottomDefault);
                linearLayoutDropDown.setOrientation(LinearLayout.VERTICAL);
                createHeaderTextView(linearLayoutDropDown, titleHeader, questionChildren);
                final AppCompatSpinner dropdown = new AppCompatSpinner(context);
                dropdown.setId(id);
                dropdown.setTag(R.id.form_fq_id, fqId);
                dropdown.setTag(questionChildren);
                List<String> data = new ArrayList<>();
                data.add("<Select>");
                int defaultDropDownPosition = 0;
                for (int i = 0; i < questionChildren.getAnswerChildren().size(); i++) {
                    if (Util.isNotNull(questionChildren.getAnswerChildren().get(i).getDisplayText())) {
                        data.add(questionChildren.getAnswerChildren().get(i).getDisplayText());
                        if (questionChildren.getDefaultFAId() != null) {
                            if (questionChildren.getDefaultFAId().getAnswerValue() != null && questionChildren.getDefaultFAId().getAnswerValue()
                                    .equalsIgnoreCase(questionChildren.getAnswerChildren().get(i).getAnswerValue())) {
                                defaultDropDownPosition = i + 1;
                            }
                        }
                    }

                }
                dropdown.setAdapter(new ArrayAdapter<String>(context,
                        android.R.layout.simple_spinner_dropdown_item, data));
                dropdown.setSelection(defaultDropDownPosition);
                dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        changeUiToDefault();
                        removeFormResponseDataRecursive(questionChildren);
                        for (int i = linearLayoutDropDown.getChildCount() - 1; i > 1; i--) {
                            linearLayoutDropDown.removeViewAt(i);
                        }
                        RealmResults<DefaultCases> dataCurrent = questionChildren.getDefaultCases().sort("sequence", Sort.ASCENDING);
                        for (int i = 0; i < dataCurrent.size(); i++) {
                            System.out.println("Question Id: " + questionChildren.getQuestionId());
                            if (dataCurrent.get(i).getDependent_question_id().intValue()
                                    == questionChildren.getQuestionId().intValue()
                                    && position > 0
                                    && (position - 1) < questionChildren.getAnswerChildren().size() &&
                                    questionChildren.getAnswerChildren().get(position - 1).getAnswerValue()
                                            .equalsIgnoreCase(dataCurrent.get(i).getDependent_question_answer_id())) {


                                System.out.println("::::::Default Cases::::: at " + dataCurrent.get(i).getSequence());
                                System.out.println("getId:" + dataCurrent.get(i).getId());
                                System.out.println("getDependent_question_id:" + dataCurrent.get(i).getDependent_question_id());
                                System.out.println("getDependent_question_answer_id:" + dataCurrent.get(i).getDependent_question_answer_id());
                                System.out.println("getSequence:" + dataCurrent.get(i).getSequence());
                                System.out.println("getTarget_question_id:" + dataCurrent.get(i).getTarget_question_id());
                                System.out.println("getPossible_answer_id:" + dataCurrent.get(i).getPossible_answer_id());
                                System.out.println("::::::Default Cases:::::");
                                changeUiByQuestionId(
                                        questionChildren.getQuestionId(),
                                        dropdown.getSelectedItem().toString(),
                                        dataCurrent.get(i).getTarget_question_id(),
                                        dataCurrent.get(i).getPossible_answer_id(), dataCurrent.get(i).getIs_edit());

                            }

                        }
                        if (position > 0) {
                            //
                            addFormResponseData(questionChildren.getfQId(),
                                    questionChildren.getAnswerChildren().get(position - 1).getfAnsId(),
                                    questionChildren.getAnswerChildren().get(position - 1).getAnswerValue(),
                                    questionChildren.getAnswerChildren().get(position - 1).getDisplayText());

                            if (questionChildren.getQuestionChildren().size() > 0) {
                                for(int k=0; k< questionChildren.getQuestionChildren().size();k++) {
                                    if(questionChildren.getQuestionChildren().get(k).getKey().equalsIgnoreCase(questionChildren.getAnswerChildren().get(position - 1).getAnswerValue())) {
                                        FormObjectQuestionChildren currentQuestionChildren = questionChildren.getQuestionChildren().get(k);
                                        for (int j = 0; j < currentQuestionChildren.getValues().size(); j++) {
                                            createChildView(linearLayoutDropDown, currentQuestionChildren.getValues().get(j), -1, -1);
                                            populateData(null);
                                        }
                                    }
                                }

                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                // For Answer from db
                if (isFromDone && (getDropDownPosition(questionChildren) >= 0)) {
                    dropdown.setSelection(getDropDownPosition(questionChildren) + 1);
                } else if (questionChildren.getfQId().equalsIgnoreCase(WSConstants.FormQuestionId.SELECT_ACTIVITY + "")&&
                        selectPntTaskAnswerValue != -1
                        ) {
                    dropdown.setSelection(getDropDownPositionForPnt(questionChildren) + 1);
                    System.out.println("Drop down position::"+getDropDownPositionForPnt(questionChildren));
                } else if (questionChildren.getQuestionId()!=null && questionChildren.getQuestionId()== WSConstants.FormQuestionId.PNT_ACTIVITY&&
                        selectPntTaskAnswerValue != -1
                        ) {
                    dropdown.setSelection(getDropDownPositionForPnt(questionChildren) + 1);
                    System.out.println("Drop down position::"+getDropDownPositionForPnt(questionChildren));
                } else {
                    if (questionChildren.getAnswerChildren().size() == 1) {
                        if (Util.isNotNull(questionChildren.getAnswerChildren().get(0).getDisplayText())) {
                            dropdown.setSelection(1);
                        }
                    }
                }
                linearLayoutDropDown.addView(dropdown);
                if (viewPosition >= 0) {
                    parent.addView(linearLayoutDropDown, viewPosition);
                } else {
                    parent.addView(linearLayoutDropDown);
                }
                if (questionChildren.getHidden() != null && questionChildren.getHidden().equalsIgnoreCase("1")) {
                    dropdown.setVisibility(View.GONE);
                    linearLayoutDropDown.setVisibility(View.GONE);
                } else {
                    dropdown.setVisibility(View.VISIBLE);
                    linearLayoutDropDown.setVisibility(View.VISIBLE);

                }

                if(questionChildren.getEditable()!=null&&questionChildren.getEditable()==0){
                    dropdown.setEnabled(false);
                }

                break;

            case date_time:
                final LinearLayout linearLayoutDate = new LinearLayout(context);
                linearLayoutDate.setOrientation(LinearLayout.VERTICAL);
                createHeaderTextView(linearLayoutDate, titleHeader, questionChildren);

                AppCompatEditText etTime = new AppCompatEditText(context);
                if (isFromDone) {
                    removeFormResponseData(questionChildren.getfQId());
                    etTime.setText(getTextForEditText(questionChildren));
                    addFormResponseData((questionChildren.getfQId()), "", getTextForEditText(questionChildren), "");
                }
                etTime.setHint("Pick a date");
                etTime.setMaxLines(2);
                etTime.setFocusable(false);
                etTime.setId(questionChildren.getQuestionId());
                etTime.setTag(R.id.form_fq_id, fqId);
                etTime.setTag(R.id.form_question_obj, questionChildren);
                linearLayoutDate.addView(etTime);
                if (viewPosition >= 0) {
                    linearLayoutDate.setPadding(paddingLeftDefault, 0, 0, paddingBottomDefault);
                    parent.addView(linearLayoutDate, viewPosition);
                } else {
                    linearLayoutDate.setPadding(0, 0, 0, paddingBottomDefault);
                    parent.addView(linearLayoutDate);
                }
                //Default value settting up
                if (questionChildren.getDefaultFAId() != null && Util.isNotNull(questionChildren.getDefaultFAId().getAnswerValue())) {
                    Calendar calendarDefault = Calendar.getInstance();
                    calendarDefault.setTime(Util.getDate(questionChildren.getDefaultFAId().getAnswerValue()));
                    etTime.setTag(R.id.autoninja_date, calendarDefault);
                    etTime.setText(String.format(Locale.getDefault(), "%d %s %d", calendarDefault.get(Calendar.DAY_OF_MONTH),
                            calendarDefault.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3),
                            calendarDefault.get(Calendar.YEAR)));
                    etTime.append(String.format(Locale.getDefault(), ", %s", Util.getAmPmTime(calendarDefault)));
                    removeFormResponseData(questionChildren.getfQId());
                    addFormResponseData((questionChildren.getfQId()), "", Util.getSQLDateTime(calendarDefault.getTime()).split("\\.")[0], "");
                }


                etTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTimeView = true;
                        Calendar calenderDefault = Calendar.getInstance();
                        Calendar calenderMaxVal = Calendar.getInstance();
                        calenderMaxVal.add(Calendar.DAY_OF_MONTH,10);
                        if (questionChildren.getDefaultFAId() != null && Util.isNotNull(questionChildren.getDefaultFAId().getAnswerValue())) {
                            if (Util.getDate(questionChildren.getDefaultFAId().getAnswerValue()) != null) {
                                calenderDefault.setTime(Util.getDate(questionChildren.getDefaultFAId().getAnswerValue()));

                            }
                        }
                        Util.showDatePicker(ViewGenerator.this, (Activity) context, v.getId(), calenderDefault, Calendar.getInstance(), calenderMaxVal);
                    }
                });

                if(questionChildren.getEditable()!=null&&questionChildren.getEditable()==0){
                    etTime.setEnabled(false);
                }

                break;


            case date:
                final LinearLayout linearLayoutDateOnly = new LinearLayout(context);
                linearLayoutDateOnly.setOrientation(LinearLayout.VERTICAL);
                createHeaderTextView(linearLayoutDateOnly, titleHeader, questionChildren);

                AppCompatEditText etDate = new AppCompatEditText(context);
                if (isFromDone) {
                    removeFormResponseData(questionChildren.getfQId());
                    etDate.setText(getTextForEditText(questionChildren));
                    addFormResponseData((questionChildren.getfQId()), "", getTextForEditText(questionChildren), "");
                }
                etDate.setHint("Pick a date");
                etDate.setMaxLines(2);
                etDate.setFocusable(false);
                etDate.setId(questionChildren.getQuestionId());
                etDate.setTag(R.id.form_fq_id, fqId);
                etDate.setTag(R.id.form_question_obj, questionChildren);
                linearLayoutDateOnly.addView(etDate);
                if (viewPosition >= 0) {
                    linearLayoutDateOnly.setPadding(paddingLeftDefault, 0, 0, paddingBottomDefault);
                    parent.addView(linearLayoutDateOnly, viewPosition);
                } else {
                    linearLayoutDateOnly.setPadding(0, 0, 0, paddingBottomDefault);
                    parent.addView(linearLayoutDateOnly);
                }


                etDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTimeView = false;
                        if(questionChildren.getQuestionId()!=null &&
                                questionChildren.getQuestionId()==WSConstants.FormQuestionId.MANUFACTURING_YEAR){
                            Util.showDatePicker(ViewGenerator.this, (Activity) context, v.getId(),null, Calendar.getInstance());

                        }
                        else {
                            Util.showDatePicker(ViewGenerator.this, (Activity) context, v.getId(), Calendar.getInstance(), null);

                        }
                    }
                });
                break;


            case checkbox:
                //createHeaderTextView(parent,title,viewType);
                final AppCompatCheckBox checkBox = new AppCompatCheckBox(context);
                checkBox.setText(questionChildren.getTitle());
                checkBox.setTextSize(16);
                checkBox.setTag(questionChildren);
                checkBox.setTag(R.id.form_question_id, questionChildren.getQuestionId());
                checkBox.setId(questionChildren.getQuestionId());
                checkBox.setTag(R.id.form_fq_id, fqId);
                if (id == WSConstants.PNT_CHECKBOX_QUESTION_ID) {
                    pntCheckBox = checkBox;
                }
                final boolean[] isCheckboxAdded = {false};
                final LinearLayout linearLayoutCheckbox = new LinearLayout(context);
                if (viewPosition >= 0) {
                    linearLayoutCheckbox.setPadding(paddingLeftDefault, 0, 0, paddingBottomDefault);
                } else {
                    linearLayoutCheckbox.setPadding(0, 0, 0, paddingBottomDefault);
                }

                linearLayoutCheckbox.setOrientation(LinearLayout.VERTICAL);

                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        removeFormResponseDataRecursive(questionChildren);
                        changeUiToDefault();
                        if (isChecked) {
                            RealmResults<DefaultCases> dataCurrent = questionChildren.getDefaultCases().sort("sequence", Sort.ASCENDING);
                            for (int i = 0; i < dataCurrent.size(); i++) {
                                System.out.println("Question Id: " + questionChildren.getQuestionId());

                                if (dataCurrent.get(i).getDependent_question_id().intValue() ==
                                        questionChildren.getQuestionId().intValue()) {
                                    changeUiByQuestionId(questionChildren.getQuestionId(),
                                            dataCurrent.get(i).getDependent_question_answer_id(),
                                            dataCurrent.get(i).getTarget_question_id(),
                                            dataCurrent.get(i).getPossible_answer_id(),
                                            dataCurrent.get(i).getIs_edit());

                                    System.out.println("::::::Default Cases::::: at " + dataCurrent.get(i).getSequence());
                                    System.out.println("getId:" + dataCurrent.get(i).getId());
                                    System.out.println("getDependent_question_id:" + dataCurrent.get(i).getDependent_question_id());
                                    System.out.println("getDependent_question_answer_id:" + dataCurrent.get(i).getDependent_question_answer_id());
                                    System.out.println("getSequence:" + dataCurrent.get(i).getSequence());
                                    System.out.println("getTarget_question_id:" + dataCurrent.get(i).getTarget_question_id());
                                    System.out.println("getPossible_answer_id:" + dataCurrent.get(i).getPossible_answer_id());
                                    System.out.println("::::::Default Cases:::::");

                                }
                            }

                            //For Show Answer from db
                            if (isFromDone) {
                                if (!isCheckboxAdded[0] && (isCheckedInDB(questionChildren))) {
                                    linearLayoutCheckbox.addView(checkBox);
                                    if (viewPosition >= 0) {
                                        parent.addView(linearLayoutCheckbox, viewPosition);
                                    } else {
                                        parent.addView(linearLayoutCheckbox);
                                    }

                                    isCheckboxAdded[0] = true;
                                }
                            }
                            if (questionChildren.getAnswerChildren() != null && questionChildren.getAnswerChildren().size() > 0) {
                                addFormResponseData(questionChildren.getfQId(),
                                        questionChildren.getAnswerChildren().get(0).getfAnsId(),
                                        questionChildren.getAnswerChildren().get(0).getAnswerValue(),
                                        questionChildren.getAnswerChildren().get(0).getDisplayText(),
                                        questionChildren.getQuestionId());

                            } else {
                                addFormResponseData(questionChildren.getfQId(),
                                        "",
                                        "1",
                                        "Yes",
                                        questionChildren.getQuestionId());
                            }
                            if (((FormObjectQuestionValues) checkBox.getTag()).getQuestionChildren() != null && ((FormObjectQuestionValues) checkBox.getTag()).getQuestionChildren().size() > 0) {
                                for (int j = 0; j < ((FormObjectQuestionValues) checkBox.getTag()).getQuestionChildren().get(0).getValues().size(); j++) {
                                    createChildView(linearLayoutCheckbox, ((FormObjectQuestionValues) checkBox.getTag()).getQuestionChildren().get(0).getValues().get(j), -1, -1);
                                }
                            }
                            if (questionChildren.getQuestionId() == WSConstants.PNT_CHECKBOX_QUESTION_ID) {
                                //Scroll to bottom
                                scrollView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                                    }
                                });
                            }

                        } else {
                            if (questionChildren.getAnswerChildren() != null && questionChildren.getAnswerChildren().size() > 1) {
                                addFormResponseData(questionChildren.getfQId(),
                                        questionChildren.getAnswerChildren().get(1).getfAnsId(),
                                        questionChildren.getAnswerChildren().get(1).getAnswerValue(),
                                        questionChildren.getAnswerChildren().get(1).getDisplayText(),
                                        questionChildren.getQuestionId());


                            } else {
                                addFormResponseData(questionChildren.getfQId(),
                                        "",
                                        "0",
                                        "No",
                                        questionChildren.getQuestionId());
                            }

                            for (int k = linearLayoutCheckbox.getChildCount() - 1; k >= 0; k--) {
                                if (checkBox.getId() != linearLayoutCheckbox.getChildAt(k).getId()) {
                                    linearLayoutCheckbox.removeViewAt(k);
                                }
                            }
                            if (((FormObjectQuestionValues) checkBox.getTag()).getQuestionChildren() != null && ((FormObjectQuestionValues) checkBox.getTag()).getQuestionChildren().size() > 1) {

                                for (int j = 0; j < ((FormObjectQuestionValues) checkBox.getTag()).getQuestionChildren().get(1).getValues().size(); j++) {
                                    createChildView(linearLayoutCheckbox, ((FormObjectQuestionValues) checkBox.getTag()).getQuestionChildren().get(1).getValues().get(j), -1, -1);
                                }
                            }

                        }
                    }
                });

                //Show Answer Selected
                if (isFromDone) {
                    if (isCheckedInDB(questionChildren)) {
                        checkBox.setChecked(true);
                    }
                    if (!isCheckboxAdded[0] && (isCheckedInDB(questionChildren))) {
                        linearLayoutCheckbox.addView(checkBox);

                        if (viewPosition >= 0) {
                            parent.addView(linearLayoutCheckbox, viewPosition);
                        } else {
                            parent.addView(linearLayoutCheckbox);
                        }
                    }
                }
                if (!isFromDone) {
                    linearLayoutCheckbox.addView(checkBox);
                    if (viewPosition >= 0) {
                        parent.addView(linearLayoutCheckbox, viewPosition);
                    } else {
                        parent.addView(linearLayoutCheckbox);
                    }
                }
                if (isFromDone && !isCheckedInDB(questionChildren)) {
                    linearLayoutCheckbox.addView(checkBox);
                    if (viewPosition >= 0) {
                        parent.addView(linearLayoutCheckbox, viewPosition);
                    } else {
                        parent.addView(linearLayoutCheckbox);
                    }

                }

                break;

            case rating:
                final LinearLayout linearLayoutRating = new LinearLayout(context);
                linearLayoutRating.setPadding(0, 0, 0, paddingBottomDefault);
                linearLayoutRating.setOrientation(LinearLayout.VERTICAL);
                AppCompatRatingBar ratingBar = new AppCompatRatingBar(context);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                ratingBar.setLayoutParams(lp);
                ratingBar.setId(Integer.parseInt(questionChildren.getfQId()));
                ratingBar.setTag(R.id.form_fq_id, fqId);
                ratingBar.setTag(R.id.form_question_obj, questionChildren);
                ratingBar.setNumStars(questionChildren.getAnswerChildren().size());
                ratingBar.setStepSize(1);

                createHeaderTextView(linearLayoutRating, titleHeader, questionChildren);
                ratingBar.setOnRatingBarChangeListener(new AppCompatRatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                        removeFormResponseDataRecursive(questionChildren);
                        int position = (int) rating;
                        if (position - 1 >= 0) {
                            addFormResponseData(questionChildren.getfQId(),
                                    questionChildren.getAnswerChildren().get(position - 1).getfAnsId(),
                                    questionChildren.getAnswerChildren().get(position - 1).getAnswerValue(),
                                    questionChildren.getAnswerChildren().get(position - 1).getDisplayText());
                        }


                        for (int i = linearLayoutRating.getChildCount() - 1; i > 0; i--) {
                            FormObjectQuestionValues current = ((FormObjectQuestionValues) linearLayoutRating.getChildAt(i).getTag(R.id.form_question_obj));
                            if (ratingBar.getId() != linearLayoutRating.getChildAt(i).getId() &&
                                    current != null && current.getfQId().equalsIgnoreCase(questionChildren.getfQId())) {
                                linearLayoutRating.removeViewAt(i);
                            }

                        }
                        if (questionChildren.getQuestionChildren().size() > 0) {
                            for (int j = 0; j < questionChildren.getQuestionChildren().get(position).getValues().size(); j++) {
                                createChildView(linearLayoutRating, questionChildren.getQuestionChildren().get(position).getValues().get(j), -1, -1);
                            }
                        }

                    }
                });


                ratingBar.setRating(getRatingFromDB(questionChildren));
                linearLayoutRating.addView(ratingBar);

                if (viewPosition >= 0) {
                    parent.addView(linearLayoutRating, viewPosition);
                } else {
                    parent.addView(linearLayoutRating);
                }
                break;

        }
        //populateData(null);

    }

    private void changeInputTypeOfEditText(AppCompatEditText etText, ValidationObject validationObject) {
        if (validationObject != null) {
            if (validationObject.getDataType() != null && validationObject.getDataType().equalsIgnoreCase(WSConstants.VALIDATION_INPUT_NUMBER)) {
                etText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);

            }
            if (validationObject.getMax_val() != null) {
                etText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(validationObject.getMax_val())});
            }
        }
    }

    private void changeUiToDefault() {
        System.out.println("Sparse:Size" + formDefaultHandler.size());
        for (int i = formDefaultHandler.size() - 1; i >= 0; i--) {
            int key = formDefaultHandler.keyAt(i);
            // get the object by the key.
            FormDefaultHandler obj = formDefaultHandler.get(key);
            System.out.println("Sparse:Key" + key);
            System.out.println("Sparse:Obj To Id" + obj.getToId());
            System.out.println("Sparse:Obj To Changed Value" + obj.getToChangedValue());
            View viewGroupFrom = parentFirst.findViewById(obj.getFromId());
            if (viewGroupFrom != null) {
                if (viewGroupFrom instanceof AppCompatRadioButton) {
                    AppCompatRadioButton currentRadioButton = (AppCompatRadioButton) viewGroupFrom;
                    if (!currentRadioButton.isChecked()) {
                        View viewGroupTarget = parentFirst.findViewById(obj.getToId());
                        if (viewGroupTarget != null) {
                            if (viewGroupTarget instanceof AppCompatCheckBox) {
                                ((AppCompatCheckBox) viewGroupTarget).setVisibility(View.VISIBLE);
                                ((AppCompatCheckBox) viewGroupTarget).setChecked(false);
                                ((AppCompatCheckBox) viewGroupTarget).setEnabled(true);
                                formDefaultHandler.removeAt(i);
                                removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag());
                            } else if (viewGroupTarget instanceof AppCompatEditText) {
                                viewGroupTarget.setVisibility(View.VISIBLE);
                                ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.VISIBLE);
                                formDefaultHandler.removeAt(i);
                                removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag(R.id.form_question_obj));
                            } else if (viewGroupTarget instanceof AppCompatSpinner) {
                                if (((FormObjectQuestionValues) viewGroupTarget.getTag()).getHidden().equalsIgnoreCase("1")) {
                                    ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.GONE);
                                    (viewGroupTarget).setVisibility(View.GONE);
                                    (viewGroupTarget).setEnabled(true);
                                } else {
                                    ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.VISIBLE);
                                    (viewGroupTarget).setVisibility(View.VISIBLE);
                                    (viewGroupTarget).setEnabled(true);
                                }
                                formDefaultHandler.removeAt(i);
                                removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag());
                            }
                        }
                    }
                } else if (viewGroupFrom instanceof AppCompatSpinner) {
                    AppCompatSpinner fromSpinner = (AppCompatSpinner) viewGroupFrom;
                    if (!fromSpinner.getSelectedItem().toString().equalsIgnoreCase(obj.getFromOldValue())) {
                        View viewGroupTarget = parentFirst.findViewById(obj.getToId());
                        if (viewGroupTarget != null) {
                            if (viewGroupTarget instanceof AppCompatCheckBox) {
                                ((AppCompatCheckBox) viewGroupTarget).setVisibility(View.VISIBLE);
                                ((AppCompatCheckBox) viewGroupTarget).setChecked(false);
                                ((AppCompatCheckBox) viewGroupTarget).setEnabled(true);
                                formDefaultHandler.removeAt(i);
                                removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag());
                            } else if (viewGroupTarget instanceof AppCompatEditText) {
                                viewGroupTarget.setVisibility(View.VISIBLE);
                                ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.VISIBLE);
                                formDefaultHandler.removeAt(i);
                                removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag(R.id.form_question_obj));
                            } else if (viewGroupTarget instanceof AppCompatSpinner) {
                                if (((FormObjectQuestionValues) viewGroupTarget.getTag()).getHidden().equalsIgnoreCase("1")) {
                                    ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.GONE);
                                    (viewGroupTarget).setVisibility(View.GONE);
                                    (viewGroupTarget).setEnabled(true);
                                } else {
                                    ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.VISIBLE);
                                    (viewGroupTarget).setVisibility(View.VISIBLE);
                                    (viewGroupTarget).setEnabled(true);
                                }
                                formDefaultHandler.removeAt(i);
                                removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag());
                            }
                        }
                    }
                } else if (viewGroupFrom instanceof AppCompatCheckBox) {
                    System.out.println("Checbox text:" + ((AppCompatCheckBox) viewGroupFrom).getText());
                    AppCompatCheckBox currentCheckBox = (AppCompatCheckBox) viewGroupFrom;
                    if (!currentCheckBox.isChecked()) {
                        View viewGroupTarget = parentFirst.findViewById(obj.getToId());
                        if (viewGroupTarget != null) {
                            if (viewGroupTarget instanceof AppCompatCheckBox) {
                                ((AppCompatCheckBox) viewGroupTarget).setVisibility(View.VISIBLE);
                                ((AppCompatCheckBox) viewGroupTarget).setChecked(false);
                                ((AppCompatCheckBox) viewGroupTarget).setEnabled(true);
                                formDefaultHandler.removeAt(i);
                                removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag());
                            } else if (viewGroupTarget instanceof AppCompatEditText) {
                                viewGroupTarget.setVisibility(View.VISIBLE);
                                ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.VISIBLE);
                                formDefaultHandler.removeAt(i);
                                removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag(R.id.form_question_obj));
                            } else if (viewGroupTarget instanceof AppCompatSpinner) {
                                if (((FormObjectQuestionValues) viewGroupTarget.getTag()).getHidden().equalsIgnoreCase("1")) {
                                    ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.GONE);
                                    (viewGroupTarget).setVisibility(View.GONE);
                                    (viewGroupTarget).setEnabled(true);
                                } else {
                                    ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.VISIBLE);
                                    (viewGroupTarget).setVisibility(View.VISIBLE);
                                    (viewGroupTarget).setEnabled(true);
                                }
                                formDefaultHandler.removeAt(i);
                                removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag());
                            }
                        }
                    }
                }
            } else {
                View viewGroupTarget = parentFirst.findViewById(obj.getToId());
                if (viewGroupTarget != null) {
                    if (viewGroupTarget instanceof AppCompatCheckBox) {
                        ((AppCompatCheckBox) viewGroupTarget).setVisibility(View.VISIBLE);
                        ((AppCompatCheckBox) viewGroupTarget).setChecked(false);
                        ((AppCompatCheckBox) viewGroupTarget).setEnabled(true);
                        formDefaultHandler.removeAt(i);
                        removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag());
                    } else if (viewGroupTarget instanceof AppCompatEditText) {
                        viewGroupTarget.setVisibility(View.VISIBLE);
                        ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.VISIBLE);
                        formDefaultHandler.removeAt(i);
                        removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag(R.id.form_question_obj));
                    } else if (viewGroupTarget instanceof AppCompatSpinner) {
                        if (((FormObjectQuestionValues) viewGroupTarget.getTag()).getHidden().equalsIgnoreCase("1")) {
                            ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.GONE);
                            (viewGroupTarget).setVisibility(View.GONE);
                            (viewGroupTarget).setEnabled(true);
                        } else {
                            ((LinearLayout) (viewGroupTarget.getParent())).setVisibility(View.VISIBLE);
                            (viewGroupTarget).setVisibility(View.VISIBLE);
                            (viewGroupTarget).setEnabled(true);
                        }
                        formDefaultHandler.removeAt(i);
                        removeFormResponseDataRecursive((FormObjectQuestionValues) viewGroupTarget.getTag());
                    }
                }
            }
        }


    }

    private void changeUiByQuestionId(Integer fromId, String fromOldValue, Integer toId,
                                      String toChangedValue, Integer isEdit) {
        FormDefaultHandler formDefaultHandlerCurrent = new FormDefaultHandler(fromId, fromOldValue, toId, toChangedValue);
        if (formDefaultHandler.get(formDefaultHandlerCurrent.getFromId()) == null) {
            formDefaultHandler.put(formDefaultHandlerCurrent.getFromId(), formDefaultHandlerCurrent);
        }
        View viewGroup = parentFirst.findViewById(formDefaultHandlerCurrent.getToId());
        if (viewGroup != null) {
            System.out.println("View is not null.....");
            System.out.println("View is type .....:" + viewGroup.getClass());
            if (viewGroup instanceof AppCompatRadioButton) {
                System.out.println("View is text:" + ((AppCompatRadioButton) viewGroup).getText());
            }
            if (viewGroup instanceof AppCompatCheckBox) {
                checkBoxStack.add(formDefaultHandlerCurrent.getToId());
                if (formDefaultHandlerCurrent.getToChangedValue().equalsIgnoreCase("1")) {
                    ((AppCompatCheckBox) viewGroup).setVisibility(View.VISIBLE);
                    ((AppCompatCheckBox) viewGroup).setChecked(false);
                    ((AppCompatCheckBox) viewGroup).setChecked(true);
                } else if (formDefaultHandlerCurrent.getToChangedValue().equalsIgnoreCase("0")) {

                    ((AppCompatCheckBox) viewGroup).setChecked(false);
                    ((AppCompatCheckBox) viewGroup).setVisibility(View.VISIBLE);
                } else if (formDefaultHandlerCurrent.getToChangedValue().equalsIgnoreCase("-1")) {
                    ((AppCompatCheckBox) viewGroup).setChecked(false);
                    ((AppCompatCheckBox) viewGroup).setVisibility(View.INVISIBLE);
                }
            } else if (viewGroup instanceof AppCompatEditText) {
                checkBoxStack.add(formDefaultHandlerCurrent.getToId());
                if (formDefaultHandlerCurrent.getToChangedValue().equalsIgnoreCase("1")) {
                    viewGroup.setVisibility(View.VISIBLE);
                    ((LinearLayout) (viewGroup.getParent())).setVisibility(View.VISIBLE);

                } else if (formDefaultHandlerCurrent.getToChangedValue().equalsIgnoreCase("0")) {
                    viewGroup.setVisibility(View.VISIBLE);
                    ((LinearLayout) (viewGroup.getParent())).setVisibility(View.VISIBLE);
                } else if (formDefaultHandlerCurrent.getToChangedValue().equalsIgnoreCase("-1")) {
                    viewGroup.setVisibility(View.GONE);
                    LinearLayout current = (LinearLayout) ((AppCompatEditText) viewGroup).getParent();
                    current.setVisibility(View.GONE);
                }
                return;
            } else if (viewGroup instanceof AppCompatSpinner) {
                System.out.println("Dasta true");
                AppCompatSpinner spinner = (AppCompatSpinner) viewGroup;
                FormObjectQuestionValues questionValues = (FormObjectQuestionValues) spinner.getTag();
                if (formDefaultHandlerCurrent.getToChangedValue().equalsIgnoreCase("-1")) {
                    viewGroup.setVisibility(View.GONE);
                    LinearLayout current = (LinearLayout) spinner.getParent();
                    current.setVisibility(View.GONE);
                } else {
                    viewGroup.setVisibility(View.VISIBLE);
                    LinearLayout current = (LinearLayout) spinner.getParent();
                    current.setVisibility(View.VISIBLE);
                }
                if (questionValues != null) {
                    for (int j = 0; j < questionValues.getAnswerChildren().size(); j++) {

                        if (questionValues.getAnswerChildren().get(j).getAnswerValue()
                                .equalsIgnoreCase(formDefaultHandlerCurrent.getToChangedValue())) {
                            System.out.println("Dasta true");
                            spinner.setSelection(j + 1);
                            break;

                        }
                    }
                }
            }
            if (isEdit == null || isEdit == 0) {
                viewGroup.setEnabled(false);
            } else if (isEdit == 1) {
                viewGroup.setEnabled(true);
            }
        } else {
            System.out.println("View is null.....");
        }


    }

    private float getRatingFromDB(FormObjectQuestionValues questionChildren) {
        FormAnswerDB formAnswerDB = realm.where(FormAnswerDB.class).equalTo("action_id", formSubmissionInputData.getAction_id() + "").equalTo("scheduled_activity_id", formSubmissionInputData.getScheduled_activity_id() + "").equalTo("form_response.name", questionChildren.getfQId()).findFirst();
        if (formAnswerDB != null) {
            for (int i = 0; i < formAnswerDB.getForm_response().size(); i++) {
                if (questionChildren.getfQId().equalsIgnoreCase(formAnswerDB.getForm_response().get(i).getName())) {
                    return Float.parseFloat(formAnswerDB.getForm_response().get(i).getValue().getAnswerValue());
                }

                // return formAnswerDB.getForm_response().get(i).getValue().getAnswerValue();
            }
        }
        return 0;
    }

    private boolean isCheckedInDB(FormObjectQuestionValues questionChildren) {
        FormAnswerDB formAnswerDB = realm.where(FormAnswerDB.class).equalTo("action_id", formSubmissionInputData.getAction_id() + "").equalTo("scheduled_activity_id", formSubmissionInputData.getScheduled_activity_id() + "").equalTo("form_response.name", questionChildren.getfQId()).findFirst();
        if (formAnswerDB != null) {
            for (int i = 0; i < formAnswerDB.getForm_response().size(); i++) {
                String value = formAnswerDB.getForm_response().get(i).getValue().getAnswerValue();
                if (questionChildren.getAnswerChildren().size() == 0) {
                    return formAnswerDB.getForm_response().get(i).getValue().getAnswerValue().equalsIgnoreCase("1");
                } else {
                    for (int j = 0; j < questionChildren.getAnswerChildren().size(); j++) {
                        if (questionChildren.getAnswerChildren().get(j).getAnswerValue().equalsIgnoreCase(value)) {
                            return true;

                        }
                    }
                }
            }
        }
        return false;
    }

    private int getDropDownPosition(FormObjectQuestionValues questionChildren) {
        FormAnswerDB formAnswerDB = realm.where(FormAnswerDB.class).equalTo("action_id", formSubmissionInputData.getAction_id() + "").equalTo("scheduled_activity_id", formSubmissionInputData.getScheduled_activity_id() + "").equalTo("form_response.name", questionChildren.getfQId()).findFirst();
        if (formAnswerDB != null) {
            for (int i = 0; i < formAnswerDB.getForm_response().size(); i++) {
                String value = formAnswerDB.getForm_response().get(i).getValue().getDisplayText();
                for (int j = 0; j < questionChildren.getAnswerChildren().size(); j++) {
                    if (questionChildren.getAnswerChildren().get(j).getDisplayText().equalsIgnoreCase(value)) {
                        return j;

                    }
                }
            }
        }
        return -1;
    }

    private int getDropDownPositionForPnt(FormObjectQuestionValues questionChildren) {

        for (int j = 0; j < questionChildren.getAnswerChildren().size(); j++) {
            if (questionChildren.getAnswerChildren().get(j).getAnswerValue().equalsIgnoreCase(selectPntTaskAnswerValue + "")) {
                return j;

            }
        }

        return -1;
    }


    private int getRadioPosition(FormObjectQuestionValues questionChildren, RadioGroup radioGroup) {
        FormAnswerDB formAnswerDB = realm.where(FormAnswerDB.class).equalTo("action_id", formSubmissionInputData.getAction_id() + "")
                .equalTo("scheduled_activity_id", formSubmissionInputData.getScheduled_activity_id() + "")
                .equalTo("form_response.name", questionChildren.getfQId()).findFirst();
        if (formAnswerDB != null) {
            for (int i = 0; i < formAnswerDB.getForm_response().size(); i++) {
                String value = formAnswerDB.getForm_response().get(i).getValue().getDisplayText();
                String name = formAnswerDB.getForm_response().get(i).getName();
                for (int j = 0; j < radioGroup.getChildCount(); j++) {
                    if (radioGroup.getChildAt(j) instanceof AppCompatRadioButton) {
                        AppCompatRadioButton currentRadioButton = (AppCompatRadioButton) radioGroup.getChildAt(j);
                        if (currentRadioButton.getText().toString().equalsIgnoreCase(value)) {
                            return j;
                        }

                    }
                }
                /*for (int j = 0; j < radioGroup.getAnswerChildren().size(); j++) {
                    if (questionChildren.getAnswerChildren().get(j).getDisplayText().equalsIgnoreCase(value)) {
                        return j;

                    }
                }*/
            }
        }
        return -1;
    }

    private int getRadioPosition(FormObjectQuestionValues questionChildren, RadioGroup radioGroup, boolean val) {
        FormAnswerDB formAnswerDB = realm.where(FormAnswerDB.class).equalTo("action_id", formSubmissionInputData.getAction_id() + "").equalTo("scheduled_activity_id", formSubmissionInputData.getScheduled_activity_id() + "").equalTo("form_response.name", questionChildren.getfQId()).findFirst();
        if (formAnswerDB != null) {
            for (int i = 0; i < formAnswerDB.getForm_response().size(); i++) {
                String value = formAnswerDB.getForm_response().get(i).getValue().getDisplayText();
                String name = formAnswerDB.getForm_response().get(i).getName();
                System.out.println("Name:" + name);
                System.out.println("Value:" + value);
                for (int j = 0; j < radioGroup.getChildCount(); j++) {
                    if (radioGroup.getChildAt(j) instanceof AppCompatRadioButton) {
                        AppCompatRadioButton currentRadioButton = (AppCompatRadioButton) radioGroup.getChildAt(j);
                        if (currentRadioButton.getText().toString().equalsIgnoreCase(value)) {
                            currentRadioButton.setChecked(true);
                            return j;
                        }

                    }
                }
            }
        }
        return -1;
    }

    private String getTextForEditText(FormObjectQuestionValues questionChildren) {
        FormAnswerDB formAnswerDB = realm.where(FormAnswerDB.class).equalTo("action_id", formSubmissionInputData.getAction_id() + "").equalTo("scheduled_activity_id", formSubmissionInputData.getScheduled_activity_id() + "").equalTo("form_response.name", questionChildren.getfQId()).findFirst();
        if (formAnswerDB != null) {
            for (int i = 0; i < formAnswerDB.getForm_response().size(); i++) {
                if (questionChildren.getfQId().equalsIgnoreCase(formAnswerDB.getForm_response().get(i).getName())) {
                    return formAnswerDB.getForm_response().get(i).getValue().getAnswerValue();
                }

                // return formAnswerDB.getForm_response().get(i).getValue().getAnswerValue();
            }
        }
        return "";
    }

    private void removeFormResponseDataRecursive(FormObjectQuestionValues questionChildren) {
        if (questionChildren != null) {
            removeFormResponseData(questionChildren.getfQId());
            for (int i = 0; i < questionChildren.getQuestionChildren().size(); i++) {
                // System.out.println("Started at "+i);
                for (int j = 0; j < questionChildren.getQuestionChildren().get(i).getValues().size(); j++) {
                    FormObjectQuestionValues currentQuestionChildren = questionChildren.getQuestionChildren().get(i).getValues().get(j);
                    removeFormResponseDataRecursive(currentQuestionChildren);
                }
                //  System.out.println("Finished at "+i);


            }
        }
    }

    private void addFormResponseData(String name, String fAnsId, String answerValue, String displayText) {
        FormSubmissionInputData.Form_response form_response = new FormSubmissionInputData().new Form_response();
        form_response.setName(name);

        FormSubmissionInputData.Form_response.Value value = new FormSubmissionInputData().new Form_response().new Value();
        value.setFAnsId(fAnsId);
        value.setAnswerValue(answerValue);
        value.setDisplayText(displayText);

        form_response.setValue(value);
        form_responses.add(form_response);
        if(formUploadListener!=null){
            formUploadListener.onClickFormDataChangeListener(form_responses);
        }
    }
    private void addFormResponseData(String name, String fAnsId, String answerValue, String displayText,Integer questionId) {
        FormSubmissionInputData.Form_response form_response = new FormSubmissionInputData().new Form_response();
        form_response.setName(name);

        FormSubmissionInputData.Form_response.Value value = new FormSubmissionInputData().new Form_response().new Value();
        value.setFAnsId(fAnsId);
        value.setAnswerValue(answerValue);
        value.setDisplayText(displayText);
        value.setQuestionId(questionId);

        form_response.setValue(value);
        form_responses.add(form_response);
        if(formUploadListener!=null){
            formUploadListener.onClickFormDataChangeListener(form_responses);
        }
    }

    private void removeFormResponseData(String s) {
        for (int i = 0; i < form_responses.size(); i++) {
            if (form_responses.get(i).getName().equalsIgnoreCase(s)) {
                form_responses.remove(i);
            }
        }
        if(formUploadListener!=null){
            formUploadListener.onClickFormDataChangeListener(form_responses);
        }
    }

    private TextView createHeaderTextView(ViewGroup parent, String title) {
        title = title.trim();
        if (Util.isNotNull(title)) {
            TextView textView = new TextView(context);
            textView.setText(title);
            textView.setTypeface(null, Typeface.BOLD);
            parent.addView(textView);
            return textView;
        } else {
            return null;
        }
    }

    private TextView createHeaderTextView(ViewGroup parent, String title, int viewPosition) {

        title = title.trim();
        if (Util.isNotNull(title)) {
            TextView textView = new TextView(context);
            textView.setText(title);
            textView.setTypeface(null, Typeface.BOLD);
            if (viewPosition >= 0) {
                textView.setPadding(36, 0, 0, paddingBottomDefault);
                parent.addView(textView, viewPosition);
            } else {
                textView.setPadding(0, 0, 0, paddingBottomDefault);
                parent.addView(textView);
            }

            return textView;
        }
        return null;
    }

    private TextView createHeaderTextView(ViewGroup parent, String title, FormObjectQuestionValues data) {

        title = title.trim();
        if (Util.isNotNull(title)) {
            TextView textView = new TextView(context);
            textView.setText(title);
            textView.setTypeface(null, Typeface.BOLD);
            if (data != null) {
                textView.setTag(R.id.radio_obj, data);
            }
            parent.addView(textView);


            return textView;
        }
        return null;
    }

    public void createView(FormObjectQuestionValues questionChildren) {
        createChildView(parentFirst, questionChildren, -1, -1);

    }

    public void populateData(FormObjectDb formObjectDb) {
        if (isFromDone) {
            FormAnswerDB formAnswerDB = realm.where(FormAnswerDB.class)
                    .equalTo("action_id", formSubmissionInputData.getAction_id() + "")
                    .equalTo("scheduled_activity_id", formSubmissionInputData.getScheduled_activity_id() + "")
                    .findFirst();
            if (formAnswerDB != null) {
                System.out.println("---------------Printing data-------------");
                for (int i = 0; i < formAnswerDB.getForm_response().size(); i++) {
                    FormResponseDB currentData = formAnswerDB.getForm_response().get(i);
                    View view = parentFirst.findViewWithTag(currentData.getName());
                    if (view instanceof RadioGroup) {
                        System.out.println("View id:" + view.getId());
                        RadioGroup radioGroup = (RadioGroup) view;
                        if (radioGroup.getCheckedRadioButtonId() == -1) {
                            for (int j = 0; j < radioGroup.getChildCount(); j++) {
                                if (radioGroup.getChildAt(j) instanceof AppCompatRadioButton) {
                                    AppCompatRadioButton currentRadioButton = (AppCompatRadioButton) radioGroup.getChildAt(j);
                                    if (currentRadioButton.getText().toString().equalsIgnoreCase(currentData.getValue().getDisplayText())) {
                                        currentRadioButton.setChecked(true);
                                    }

                                }
                            }
                        }
                    }
                    System.out.println("form Answer::::for key" + currentData.getName());
                    System.out.println("form getAnswerValue:" + currentData.getValue().getAnswerValue());
                    System.out.println("form getFAnsId:" + currentData.getValue().getFAnsId());
                    System.out.println("form getDisplayText:" + currentData.getValue().getDisplayText());

                }
                System.out.println("---------------Printing data ends-------------");
            }
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        AppCompatEditText editText = (AppCompatEditText) parentFirst.findViewById(Integer.parseInt(view.getTag()));

        if (editText != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, monthOfYear, dayOfMonth);
            editText.setTag(R.id.autoninja_date, calendar);
            FormObjectQuestionValues questionChildren = ((FormObjectQuestionValues) editText.getTag(R.id.form_question_obj));
            currentDateEditor = editText;
            if (showTimeView) {
                editText.setText("");
                removeFormResponseData(questionChildren.getfQId());
                showTimePicker(ViewGenerator.this, (Activity) context, editText.getId());
            }
            else {
                editText.setText(String.format(Locale.getDefault(), "%d %s %d", calendar.get(Calendar.DAY_OF_MONTH), calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), calendar.get(Calendar.YEAR)));
                removeFormResponseData(questionChildren.getfQId());
                addFormResponseData((questionChildren.getfQId()), "", Util.getSQLDateTime(calendar.getTime()).split("\\.")[0], "");
            }

        }

    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        AppCompatEditText editText = currentDateEditor;
        {
            if (editText != null) {
                Calendar calDate = (Calendar) editText.getTag(R.id.autoninja_date);
                calDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calDate.set(Calendar.MINUTE, minute);
                calDate.set(Calendar.SECOND, second);
                editText.setTag(R.id.autoninja_date, calDate);
                editText.setText(String.format(Locale.getDefault(), "%d %s %d", calDate.get(Calendar.DAY_OF_MONTH),
                        calDate.getDisplayName(Calendar.MONTH, Calendar.LONG,
                                Locale.getDefault()).toUpperCase().substring(0, 3), calDate.get(Calendar.YEAR)));
                editText.append(String.format(Locale.getDefault(), ", %s", Util.getAmPmTime(calDate)));
                FormObjectQuestionValues questionChildren = ((FormObjectQuestionValues) editText.getTag(R.id.form_question_obj));
                removeFormResponseData(questionChildren.getfQId());
                addFormResponseData((questionChildren.getfQId()), "", Util.getSQLDateTime(calDate.getTime()).split("\\.")[0], "");

            }
        }


    }

    private void insertRealmDataIfExist() {

    }

    private void isFormFilled(ViewGroup parentFirst) {
        for (int i = parentFirst.getChildCount() - 1; i >= 0; i--) {
            final View child = parentFirst.getChildAt(i);
            if (child != null && child.getVisibility() == View.VISIBLE) {
                if (realm.where(FormObjectQuestionValues.class).equalTo("fQId", child.getTag(R.id.form_fq_id) + "").equalTo("ifVisibleMandatory", true).findFirst() != null) {
                    ++answerCount;
                    System.out.println("Id of required:" + child.getTag(R.id.form_fq_id));
                }
            }
            if (child instanceof ViewGroup) {
                isFormFilled((ViewGroup) child);
                // DO SOMETHING WITH VIEWGROUP, AFTER CHILDREN HAS BEEN LOOPED
            }
        }
    }


    private enum ViewType {
        text, textarea, radio, dropdown, date_time, date, checkbox, rating, voidView
    }
}