package com.salescrm.telephony.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.AddExchangecar;
import com.salescrm.telephony.db.AddAndExchangeCarSyncDB;
import com.salescrm.telephony.db.car.ExchangeCarBrandsDB;
import com.salescrm.telephony.db.car.ExchangeCarDetails;
import com.salescrm.telephony.db.IntrestedCarDetails;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.car.CarBrandModelsDB;
import com.salescrm.telephony.db.car.CarBrandsDB;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.AddExchangecarFragment;
import com.salescrm.telephony.fragments.AddcarFragment;
import com.salescrm.telephony.model.AddExchangeCarModel;
import com.salescrm.telephony.model.Cars;
import com.salescrm.telephony.model.CreateLeadcarIdData;
import com.salescrm.telephony.model.Model;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AddCarsNewResponse;
import com.salescrm.telephony.response.AddExchangeCarResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by akshata on 27/5/16.
 */
public class AddExchangeCarActivity extends AppCompatActivity implements AddExchangecarFragment.ExchangeCar {
    private Preferences pref;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Button save;
    private Realm realm;
    private RealmResults<CarBrandsDB> userCarBrand;
    private RealmResults<ExchangeCarBrandsDB> exchangeCarBrandsDB;
    public static String addcarcolorid = "", addcarmodelid = "",addfueltypeid="", addcarvariantid = "", syncTypeId = "0";
    public static String addCarModelName="",addCarVariantName="",addCarFuelTypeName="",addCarColorName="";
    private String brand = "", year = "", totalRun = "", regNumber = "", mExchangecarname = "";
    private ConnectionDetectorService connectionDetectorService;
    private AddAndExchangeCarSyncDB addAndExchangeCarSyncDB;
    private RealmResults<AddAndExchangeCarSyncDB> realmResultsAddCarSyncDBs, realmResultsExchangeCarSyncDBs;
    private int addCarSyncSize, exchangeCarSyncSize;
    private View parentLayout;
    private Snackbar snackbar;
    private View viewSnacbar;
    private TextView tvSnacbar;
    private ImageView ibBack;
    private SalesCRMRealmTable salesCRMRealmTable;
    private String mLead_last_updated = "";
    private TextView tvTitle;
    private boolean isBike;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exchange_addcars);
        pref = Preferences.getInstance();
        pref.load(getApplicationContext());
        realm = Realm.getDefaultInstance();
        save = (Button) findViewById(R.id.submit_btn);
        ibBack = (ImageView) findViewById(R.id.ib_form_back_activity);
        tvTitle = (TextView) findViewById(R.id.tv_form_title);
        connectionDetectorService = new ConnectionDetectorService(AddExchangeCarActivity.this);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout_exchange);
        isBike = DbUtils.isBike();
        tabLayout.addTab(tabLayout.newTab().setText(isBike?"Add Bike" : "Add car"));
        if(!isBike){
            //tabLayout.addTab(tabLayout.newTab().setText("Add exchange car"));
        }
        else {
            //tvTitle.setText("Add Bike");
        }

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setVisibility(View.INVISIBLE);
        viewPager = (ViewPager) findViewById(R.id.pageraddcar);
        parentLayout = (View) findViewById(R.id.pageraddcar);

        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findFirst();
        if (salesCRMRealmTable != null) {
            mLead_last_updated = salesCRMRealmTable.getLeadLastUpdated();
        }

        /*if (connectionDetectorService.isConnectingToInternet()) {
            realmResultsAddCarSyncDBs = realm.where(AddAndExchangeCarSyncDB.class).equalTo("addType", 0).equalTo("is_synced", true).findAll();
            if (realmResultsAddCarSyncDBs != null) {
                addCarSyncSize = realmResultsAddCarSyncDBs.size() - 1;
                sendPendingAddCarFromDb();
            }
            realmResultsExchangeCarSyncDBs = realm.where(AddAndExchangeCarSyncDB.class).equalTo("addType", 1).equalTo("is_synced", true).findAll();
            if (realmResultsExchangeCarSyncDBs != null) {
                exchangeCarSyncSize = realmResultsExchangeCarSyncDBs.size() - 1;
                sendPendingExchangeCarFromDb();
            }
        } else {

        }*/

        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        viewPager.setCurrentItem(0);
        setTypeID("0");
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    setTypeID("0");
                } else if (position == 1) {
                    setTypeID("1");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        userCarBrand = realm.where(CarBrandsDB.class).findAll();
        exchangeCarBrandsDB = realm.where(ExchangeCarBrandsDB.class).findAll();
        populatecarViewpager();
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!addcarmodelid.equalsIgnoreCase("-1")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddExchangeCarActivity.this);
                    alertDialogBuilder.setMessage("Are you sure you want to add this "+(isBike ? "bike":"car")+"?");
                    alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            makeApiCall();

                        }
                    });
                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                           /* Intent in = new Intent(AddExchangeCarActivity.this, C360Activity.class);
                            startActivity(in);
                            finish();*/
                            dialog.cancel();

                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else if (addcarmodelid.equalsIgnoreCase("-1")) {
                    Toast.makeText(getApplicationContext(), "Please select a model", Toast.LENGTH_LONG).show();
                }


            }
        });

    }

    private void sendPendingExchangeCarFromDb() {
        if (exchangeCarSyncSize > 0) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).AddExchangeCar(createExchangeObjectFromDB(), new Callback<AddExchangeCarResponse>() {
                @Override
                public void success(AddExchangeCarResponse addExchangeCarResponse, Response response) {

                    if(addExchangeCarResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                        ApiUtil.InvalidUserLogout(AddExchangeCarActivity.this,0);
                    }

                    if (addExchangeCarResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        realm.beginTransaction();
                        realmResultsExchangeCarSyncDBs.get(exchangeCarSyncSize).setIs_synced(true);
                        realm.commitTransaction();
                        exchangeCarSyncSize--;
                        updateExchangeCarToDB(addExchangeCarResponse);
                    }
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }

                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }

    private void sendPendingAddCarFromDb() {
        if (addCarSyncSize > 0) {
            CreateLeadcarIdData createcardata = new CreateLeadcarIdData();
            createcardata.setLead_id(pref.getLeadID());

            CreateLeadcarIdData.LeadCarDetails cardata = createcardata.new LeadCarDetails();
            cardata.setCarColorId(realmResultsAddCarSyncDBs.get(addCarSyncSize).getColorId());
            cardata.setCarVariant(realmResultsAddCarSyncDBs.get(addCarSyncSize).getVariantId());
            cardata.setCarModel(realmResultsAddCarSyncDBs.get(addCarSyncSize).getModelId());
            cardata.setCarFuelType("1");
            createcardata.setLead_car_details(cardata);

            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).Addnewcar(createcardata, new Callback<AddCarsNewResponse>() {
                @Override
                public void success(AddCarsNewResponse addCarsnewResponse, Response response) {

                    if(addCarsnewResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                        ApiUtil.InvalidUserLogout(AddExchangeCarActivity.this,0);
                    }

                    if (addCarsnewResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        realm.beginTransaction();
                        realmResultsAddCarSyncDBs.get(addCarSyncSize).setIs_synced(true);
                        realm.commitTransaction();
                        addCarSyncSize--;
                        updateInterestedCarToDB(addCarsnewResponse);
                    }
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }

                    }
                    //sendPendingAddCarFromDb();
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("musafir:-  Failed " + error.toString());
                }
            });
        }
    }

    private void callSnacbar(String message) {
        snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_SHORT);
        viewSnacbar = snackbar.getView();
        tvSnacbar = (TextView) viewSnacbar.findViewById(android.support.design.R.id.snackbar_text);
        tvSnacbar.setGravity(Gravity.CENTER_HORIZONTAL);
        tvSnacbar.setTextColor(Color.WHITE);
        viewSnacbar.setBackgroundColor(Color.BLACK);
        snackbar.show();
    }

    private void makeApiCall() {
        if (addcarcolorid.equalsIgnoreCase("-1")) {
            this.addcarcolorid = null;
        }
        if (addcarvariantid.equalsIgnoreCase("-1")) {
            this.addcarvariantid = null;
        }
        int type = Integer.parseInt(syncTypeId);
        if (type == WSConstants.TYPE_ADD_CAR) {
            AddCar addCar = new AddcarFragment();
            addCar.sendAddCarData();
            if (connectionDetectorService.isConnectingToInternet()) {
                if (!addcarmodelid.isEmpty() || isCarModelValid(addcarmodelid)) {
                    callAddCarAPI();
                } else {
                    callSnacbar("Add a "+(isBike? "bike":"car"));
                }
            } else {
                realm.beginTransaction();
                addAndExchangeCarSyncDB =realm.createObject(AddAndExchangeCarSyncDB.class);

                addAndExchangeCarSyncDB.setAddType(0);
                addAndExchangeCarSyncDB.setIs_synced(false);
                addAndExchangeCarSyncDB.setColorId(addcarcolorid);
                addAndExchangeCarSyncDB.setModelId(addcarmodelid);
                addAndExchangeCarSyncDB.setVariantId(addcarvariantid);
                addAndExchangeCarSyncDB.setLeadId(pref.getLeadID());
                addAndExchangeCarSyncDB.setModelName(addCarModelName);
                addAndExchangeCarSyncDB.setVariantName(addCarVariantName);
                addAndExchangeCarSyncDB.setFuelTypeName(addCarFuelTypeName);
                addAndExchangeCarSyncDB.setColorName(addCarColorName);
                realm.commitTransaction();
                Intent in = new Intent(AddExchangeCarActivity.this, C360Activity.class);
                startActivity(in);
                finish();
            }
            // callSnacbar("Done");
        } else if (type == WSConstants.TYPE_EXCHANGE_CAR) {
            if (connectionDetectorService.isConnectingToInternet()) {
                if (!addcarmodelid.isEmpty() || isCarModelValid(addcarmodelid)) {
                    callExchangeCarAPI();
                } else {
                    callSnacbar("Add a Exchange car.");
                }

            } else {
                realm.beginTransaction();
                addAndExchangeCarSyncDB = realm.createObject(AddAndExchangeCarSyncDB.class);

                addAndExchangeCarSyncDB.setAddType(1);
                addAndExchangeCarSyncDB.setIs_synced(false);
                addAndExchangeCarSyncDB.setModelId(addcarmodelid);
                addAndExchangeCarSyncDB.setKms_run(totalRun);
                addAndExchangeCarSyncDB.setReg_no(regNumber);
                addAndExchangeCarSyncDB.setYear(year);
                addAndExchangeCarSyncDB.setLeadId(pref.getLeadID());
                addAndExchangeCarSyncDB.setModelName(mExchangecarname);
                realm.commitTransaction();
                Intent in = new Intent(AddExchangeCarActivity.this, C360Activity.class);
                startActivity(in);
                finish();
            }
        } else {

        }
    }

    /**
     * create object for online
     *
     * @return
     */
    private AddExchangeCarModel createExchangeObject() {

        AddExchangeCarModel temp = new AddExchangeCarModel();
        ArrayList<Cars> cars = new ArrayList<>();
        Model model = new Model(addcarmodelid);
        cars.add(new Cars(model, totalRun, year, regNumber));
        temp.setLead_id(pref.getLeadID());
        temp.setLead_last_updated(mLead_last_updated);
        temp.setCars(cars);
        return temp;
    }

    /**
     * create object from DB
     *
     * @return
     */
    private AddExchangeCarModel createExchangeObjectFromDB() {

        AddExchangeCarModel temp = new AddExchangeCarModel();
        ArrayList<Cars> cars = new ArrayList<>();
        Model model = new Model(realmResultsExchangeCarSyncDBs.get(exchangeCarSyncSize).getModelId());
        cars.add(new Cars(model, realmResultsExchangeCarSyncDBs.get(exchangeCarSyncSize).getKms_run(),
                realmResultsExchangeCarSyncDBs.get(exchangeCarSyncSize).getYear(),
                realmResultsExchangeCarSyncDBs.get(exchangeCarSyncSize).getReg_no()));
        temp.setLead_id(pref.getLeadID());
        temp.setCars(cars);


        return temp;

    }

    private void callExchangeCarAPI() {

       /* ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).AddExchangeCar(pref.getLeadID(), addcarmodelid, new Callback<AddExchangeCarResponse>() {*/
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).AddExchangeCar(createExchangeObject(), new Callback<AddExchangeCarResponse>() {
            @Override
            public void success(final AddExchangeCarResponse addExchangeCarResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(addExchangeCarResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(AddExchangeCarActivity.this,0);
                }

                if (addExchangeCarResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    updateExchangeCarToDB(addExchangeCarResponse);
                    callSnacbar("Done");
                    Intent in = new Intent(AddExchangeCarActivity.this, C360Activity.class);
                    startActivity(in);
                    finish();

                }else  {
                    callSnacbar(addExchangeCarResponse.getMessage());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Exchange", "Exchange- " + error.toString());
                Toast.makeText(AddExchangeCarActivity.this,"There was a problem, Please try again.",Toast.LENGTH_LONG).show();
            }
        });
    }


    public void callAddCarAPI() {
        System.out.println("Bua:- " + new Gson().toJson(getLeadcarIdData()).toString());
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).Addnewcar(getLeadcarIdData(), new Callback<AddCarsNewResponse>() {
            @Override
            public void success(final AddCarsNewResponse carob, Response response) {
                System.out.println("add new car responce" + response.getBody());
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }

                }

                addcarmodelid = "";
                addcarcolorid = "";
                addfueltypeid = "";
                addcarvariantid = "";
                addCarModelName = "";
                addCarVariantName = "";
                addCarFuelTypeName = "";
                addCarColorName = "";

                if(carob.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(AddExchangeCarActivity.this,0);
                }

                if (carob.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    updateInterestedCarToDB(carob);
                    callSnacbar("Done");
                    Intent in = new Intent(AddExchangeCarActivity.this, C360Activity.class);
                    startActivity(in);
                    finish();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Getsavecars", "getcarsave - " + error.getMessage());
                Toast.makeText(AddExchangeCarActivity.this,"There was a problem, Please try again.",Toast.LENGTH_LONG).show();
            }
        });
    }


    private CreateLeadcarIdData getLeadcarIdData() {
        CreateLeadcarIdData newCarData = new CreateLeadcarIdData();
        newCarData.setLead_id(pref.getLeadID());
        CreateLeadcarIdData.LeadCarDetails newCar = newCarData.new LeadCarDetails();
        newCar.setCarColorId(this.addcarcolorid);
        newCar.setCarVariant(this.addcarvariantid);
        newCar.setCarModel(this.addcarmodelid);
        newCar.setCarFuelType(this.addfueltypeid);

        newCarData.setLead_car_details(newCar);
        //System.out.println(" SELECTED CAR COL" + addcarcolorid + "CAR VAriant" + addcarvariantid + "MODEL ID" + addcarmodelid + "LEaD CAR ID" + newCar);
        System.out.println("2. SELECTED MODEL ID=" + this.addcarmodelid + " SELECTED COLOR=" +
                this.addcarcolorid + " SELECTED VARIANT ID=" + this.addcarvariantid + "LEaD CAR ID= " + pref.getLeadID()+ "FUEL ID= "+this.addfueltypeid);


        return newCarData;
    }


    private void populatecarViewpager() {
        final AddExchangecar exchangecaradapter = new AddExchangecar
                (getSupportFragmentManager(), tabLayout.getTabCount(), userCarBrand, exchangeCarBrandsDB);
        viewPager.setAdapter(exchangecaradapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

   /* @Override
    public void sendAddCarData(String brand, String model, String color, String variant, String syncType) {

        this.addcarmodelid = model;
        this.addcarcolorid = color;
        this.addcarvariantid = variant;
        this.syncTypeId = syncType;

        System.out.println("SELECTED MODEL ID=" + this.addcarmodelid + " SELECTED COLOR=" + this.addcarcolorid + " SELECTED VARIANT ID=" + this.addcarvariantid);

    }*/


    @Override
    public void sendExchangeCarData(String brand, String model, String modelName, String year, String variant, String totalrun, String regNum) {
        this.brand = brand;
        this.addcarmodelid = model;
        this.year = year;
        this.addcarvariantid = variant;
        this.totalRun = totalrun;
        this.regNumber = regNum;
        this.mExchangecarname = modelName;
        System.out.println("SELECTED MODEL EXCHANGE ID=" + this.addcarmodelid + "BRAND" + brand + "YEAR" + this.year + "RUNS" + this.totalRun + "REG NUM" + this.regNumber + "SELECTED VARIANT ID=" + this.addcarvariantid);
    }

    public void setTypeID(String typeID) {
        this.syncTypeId = typeID;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent in = new Intent(AddExchangeCarActivity.this, C360Activity.class);
                startActivity(in);
                this.finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }


    private void updateInterestedCarToDB(final AddCarsNewResponse carob) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                IntrestedCarDetails intrestedCarDetails = new IntrestedCarDetails();
                intrestedCarDetails.setLeadId(Integer.parseInt(pref.getLeadID()));
                intrestedCarDetails.setIntrestedLeadCarId(Integer.parseInt(carob.getResult().getLead_car_id()));
                intrestedCarDetails.setIntrestedCarName(carob.getResult().getName());
                intrestedCarDetails.setIntrestedCarVariantCode(carob.getResult().getVariant_code());
                intrestedCarDetails.setIntrestedCarVariantId(carob.getResult().getVariant_id());
                intrestedCarDetails.setIntrestedCarColorId(carob.getResult().getColor_id());
                intrestedCarDetails.setIntrestedCarModelId(carob.getResult().getModel_id());
                intrestedCarDetails.setIntrestedCarIsPrimary(Integer.parseInt(carob.getResult().getIs_primary()));
                realm.copyToRealmOrUpdate(intrestedCarDetails);
            }
        });
    }

    private void updateExchangeCarToDB(final AddExchangeCarResponse addExchangeCarResponse) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (int i = 0; i < addExchangeCarResponse.getResult().size(); i++) {
                    ExchangeCarDetails exchangeCarDetails = new ExchangeCarDetails();
                    exchangeCarDetails.setLeadId(Integer.parseInt(pref.getLeadID()));
                    exchangeCarDetails.setExchangecarname(mExchangecarname);
                    exchangeCarDetails.setLead_exchange_car_id(addExchangeCarResponse.getResult().get(i).getLead_exchange_car_id());

                    Log.e("mExchangecarname", "mExchangecarname " + mExchangecarname);

                    exchangeCarDetails.setPurchase_date(addExchangeCarResponse.getResult().get(i).getPurchase_date());
                    exchangeCarDetails.setReg_no(addExchangeCarResponse.getResult().get(i).getReg_no());
                    exchangeCarDetails.setKms_run(addExchangeCarResponse.getResult().get(i).getKms_run());
                    exchangeCarDetails.setPurchase_date(addExchangeCarResponse.getResult().get(i).getPurchase_date());
                    exchangeCarDetails.setCar_stage_id("0");
                    realm.copyToRealmOrUpdate(exchangeCarDetails);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(AddExchangeCarActivity.this, C360Activity.class);
        startActivity(in);
        this.finish();
    }

    public interface AddCar {
        void sendAddCarData();
    }

    private boolean isCarModelValid(String text) {
        CarBrandModelsDB realmData = realm.where(CarBrandModelsDB.class).equalTo("model_id", text).findFirst();
        return realmData != null;
    }

}
