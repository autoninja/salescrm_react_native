package com.salescrm.telephony.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.gamification.BadgesNotificationActivity;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.locationHelper.LocationHelper;
import com.salescrm.telephony.offline.AutoFetchFormData;
import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.db.CustomerPhoneNumbers;
import com.salescrm.telephony.db.FormAnswerDB;
import com.salescrm.telephony.db.FormAnswerValueDB;
import com.salescrm.telephony.db.FormObjectDb;
import com.salescrm.telephony.db.FormObjectQuestionChildren;
import com.salescrm.telephony.db.FormObjectQuestionValues;
import com.salescrm.telephony.db.PlannedActivities;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.dbOperation.TasksDbOperation;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.AutoFetchFormListener;
import com.salescrm.telephony.interfaces.DialogCrm;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.interfaces.FormUploadListener;
import com.salescrm.telephony.interfaces.PlanNextTaskResultListener;
import com.salescrm.telephony.interfaces.TaskOtpResultListener;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.model.BookBikeModel;
import com.salescrm.telephony.model.FormObjectsInputs;
import com.salescrm.telephony.model.FormSubmissionActivityInputData;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.model.FormSubmissionInputDataServer;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.Activity_to_schedule;
import com.salescrm.telephony.response.BookingConfirmResponse;
import com.salescrm.telephony.response.FormObjectRetrofit;
import com.salescrm.telephony.response.FormSubmissionActivityResponse;
import com.salescrm.telephony.response.FormSubmissionResponse;
import com.salescrm.telephony.response.PostBookingFormResponse;
import com.salescrm.telephony.response.SplitFormObjectsRetrofit;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;
import com.salescrm.telephony.views.ViewGenerator;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bharath on 24/10/16.
 */
public class FormRenderingActivity extends AppCompatActivity implements FormUploadListener,
        Callback<FormObjectRetrofit>, AutoDialogClickListener, FetchC360OnCreateLeadListener, AutoFetchFormListener {
    private String TAG = "FormRenderingActivity:";
    private Preferences pref;
    private FormObjectRetrofit.Result.FormObject formObject;
    private Realm realm;
    private FormObjectDb formObjectDb;
    private ViewGenerator viewGenerator;
    private LinearLayout llParent;
    private int actionId;
    private int scheduledActivityId;
    private RelativeLayout relLoader;
    private TextView tvFormTitle;
    private TextView btFormAction;
    private ImageView ibBackActivity;
    private NestedScrollView nestedScrollView;
    private int apiCallCount = 0;
    private FormSubmissionInputData formSubmissionInputData;
    private ProgressDialog progressDialog;
    private int scheduledType = 1;
    private FormAnswerDB formAnswerDb;
    private boolean fromDone = false;
    private String generatedLeadId;
    private boolean fromC360 = false;
    private int dseId;
    private int taskTypeAnswerId = -1;
    private FormSubmissionInputData previousSubmissionInput;
    private boolean taskRescheduled = false;
    private String formAction = "Submit";
    private SalesCRMRealmTable salesCRMRealmTableCurrent;
    private boolean taskPNT =false;
    private boolean sendOtp = false;
    private int activityId;
    private boolean fromPostBooking = false;
    private String leadCarId = null;
    private LocationHelper locationHelper = null;
    private OtpDialogPlanNextTask otpDialogPlanNextTask = null;

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        //super.onSaveInstanceState(outState, outPersistentState);
    }
    private void disableScreenCapture() {
        if(pref.isScreenshotEnabled()) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Preference loading
        pref = Preferences.getInstance();
        pref.load(this);

        realm = Realm.getDefaultInstance();

        System.out.println("leadLast updated:");
        RealmResults<SalesCRMRealmTable> dataTesting = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findAllSorted("leadLastUpdated", Sort.DESCENDING);
        for (int i = 0; i < dataTesting.size(); i++) {
            System.out.println("Lead last updated:" + dataTesting.get(i).getLeadLastUpdated());
        }
        disableScreenCapture();
        setContentView(R.layout.dynamic_form_rendering);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating activity..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        tvFormTitle = (TextView) findViewById(R.id.tv_form_title);
        btFormAction = (TextView) findViewById(R.id.bt_form_action);
        relLoader = (RelativeLayout) findViewById(R.id.rel_form_loader);
        ibBackActivity = (ImageView) findViewById(R.id.ib_form_back_activity);
        llParent = (LinearLayout) findViewById(R.id.ll_form_render_parent);
        nestedScrollView = (NestedScrollView) findViewById(R.id.scroll_form_render_parent);
        formSubmissionInputData = new FormSubmissionInputData();
        ibBackActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
       /* RealmResults<SalesCRMRealmTable> data = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .equalTo("isCreatedTemporary",false)
                .equalTo("isDone", false)
                .equalTo("createdOffline",false).findAll();
        for(int i=0;i<data.size();i++){
            if(data.get(i).getIsLeadActive()!=1){
                btFormAction.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(),"Lead has been closed",Toast.LENGTH_SHORT).show();
                return;
            }
        }*/
        init();


    }

    private void initLocation() {
        locationHelper = LocationHelper.getInstance(this, new CallBack() {
            @Override
            public void onCallBack() {

            }

            @Override
            public void onCallBack(Object data) {
                if(data instanceof Location) {
                    Location location =  (Location) data;
                    if(otpDialogPlanNextTask!=null) {
                        otpDialogPlanNextTask.updateLocation(location);
                        locationHelper.abort();
                    }
                }
            }

            @Override
            public void onError(String error) {

            }
        }).listen();
    }

    private void init() {
        nestedScrollView.setVisibility(View.GONE);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            btFormAction.setText(bundle.getString("form_action", "Action"));
            formAction = bundle.getString("form_action", "Action");
            tvFormTitle.setText(bundle.getString("form_title", "Title"));
            actionId = bundle.getInt("action_id", -1);
            scheduledActivityId = bundle.getInt("scheduled_activity_id", pref.getScheduledActivityId());
            salesCRMRealmTableCurrent = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
            scheduledType = bundle.getInt("scheduled_type", 1);
            fromDone = bundle.getBoolean("from_done", false);
            fromC360 = bundle.getBoolean("from_c360", false);
            dseId = bundle.getInt("dseId", pref.getAppUserId());
            taskTypeAnswerId = bundle.getInt("answer_id", -1);
            activityId = bundle.getInt("activity_id", -1);
            fromPostBooking = bundle.getBoolean("from_post_booking", false);
            leadCarId = bundle.getString("lead_car_id", null);

            System.out.println("LeadCarID:"+leadCarId);
            System.out.println("taskTypeAnswerId::" + taskTypeAnswerId);
            System.out.println("Form type:" + actionId);
        }
        formSubmissionInputData.setLead_id(pref.getLeadID());

        formSubmissionInputData.setLead_last_updated(realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findAllSorted("leadLastUpdated", Sort.DESCENDING).first().getLeadLastUpdated());

        formSubmissionInputData.setAction_id(actionId + "");
        formSubmissionInputData.setScheduled_activity_id(scheduledActivityId + "");
        viewGenerator = new ViewGenerator(btFormAction, this, llParent, nestedScrollView, formSubmissionInputData, fromDone, taskTypeAnswerId);
        if(new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()){
            if(fromPostBooking) {
                postBookingProcess();
            }
            else {
                if (actionId == WSConstants.FormAction.POST_DONE) {
                    if (realm.where(FormObjectDb.class).equalTo("action_id", actionId).equalTo("scheduled_activity_id", scheduledActivityId).findFirst() != null) {
                        //initFormRendering();
                        getFormFromServer();
                    } else {
                        getFormFromServer();
                    }
                } else {
                    getFormFromServer();
                }
            }
        }
        else{
            Util.showToast(getApplicationContext(),"No internet connection",Toast.LENGTH_SHORT);
        }



    }

    private void postBookingProcess() {
        relLoader.setVisibility(View.VISIBLE);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getPostBookingForm(pref.getLeadID(), activityId + "", new Callback<PostBookingFormResponse>() {
            @Override
            public void success(final PostBookingFormResponse postBookingFormResponse, Response response) {
                relLoader.setVisibility(View.GONE);
                Util.updateHeaders(response.getHeaders());

                if(postBookingFormResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(FormRenderingActivity.this,0);
                }
                if(postBookingFormResponse.getStatusCode()!=null && postBookingFormResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_STATUS_OK+"") && postBookingFormResponse.getResult()!=null) {
                    System.out.println("We can render now..");
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                                    final SplitFormObjectsRetrofit.Result.FormObject.InnerFormObject preFormObject = postBookingFormResponse.getResult();

                                    if (preFormObject != null) {
                                        preFormObject.setLeadId(Util.getInt(pref.getLeadID()));

                                        preFormObject.setAction_id(WSConstants.FormAction.PRE_DONE);

                                        RealmResults<FormObjectDb> realmDelete = realm.where(FormObjectDb.class).equalTo("action_id",
                                                WSConstants.FormAction.PRE_DONE)
                                                .equalTo("scheduled_activity_id",
                                                        scheduledActivityId)
                                                .findAll();
                                        System.out.println("Form data:size:" + realmDelete.size());
                                        if (realmDelete.size() > 0) {
                                            realmDelete.deleteAllFromRealm();
                                        }

                                        preFormObject.setScheduled_activity_id(scheduledActivityId);
                                        realm.createObjectFromJson(FormObjectDb.class, new Gson().toJson(preFormObject));

                                        RealmResults<FormObjectDb> realmCount = realm.where(FormObjectDb.class).equalTo("action_id",
                                                WSConstants.FormAction.PRE_DONE)
                                                .equalTo("scheduled_activity_id",
                                                       scheduledActivityId)
                                                .findAll();
                                        System.out.println("Pre form count::" + realmCount.size());
                                    }


                        }
                    }, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            initFormRendering();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            System.out.println(error.toString());

                        }
                    });


                }
                else {
                    if (!isFinishing()) {
                        new AutoDialog(FormRenderingActivity.this, new AutoDialogModel("Failed to fetch form from server", "Retry", "Close")).show();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println("Error");
                relLoader.setVisibility(View.GONE);
                onErrorApiCallController(error);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void getFormFromServer() {
        relLoader.setVisibility(View.VISIBLE);
        System.out.println(TAG + "getFormFromServer Start"+ actionId);
        if (actionId == 0) {
            //AddActivity form
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetManualActivities(pref.getLeadID(), scheduledType + "", this);
        } else {
            //Form object form
            FormObjectsInputs formObjectsInputs = new FormObjectsInputs();
            List<FormObjectsInputs.Lead_data> lead_dataList = new ArrayList<>();
            FormObjectsInputs.Lead_data leadDataDone = new FormObjectsInputs().new Lead_data();
            leadDataDone.setAction_id(WSConstants.FormAction.PRE_DONE + "");
            leadDataDone.setLead_id(pref.getLeadID() + "");
            leadDataDone.setScheduled_activity_id(scheduledActivityId + "");
            lead_dataList.add(leadDataDone);
            formObjectsInputs.setLead_data(lead_dataList);
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetSplitFormObjects(formObjectsInputs, new Callback<SplitFormObjectsRetrofit>() {
                @Override
                public void success(final SplitFormObjectsRetrofit formObjectsRetrofit, Response response) {
                    relLoader.setVisibility(View.GONE);
                    Util.updateHeaders(response.getHeaders());

                     if(formObjectsRetrofit.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                        ApiUtil.InvalidUserLogout(FormRenderingActivity.this,0);
                    }

                    if (!formObjectsRetrofit.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        System.out.println(TAG + "Success:0" + formObjectsRetrofit.getMessage());
                    } else {
                        if (formObjectsRetrofit.getResult() != null) {
                            System.out.println(TAG + "Success:1" + formObjectsRetrofit.getMessage());


                            realm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {


                                    for (int i = 0; i < formObjectsRetrofit.getResult().size(); i++) {
                                        SplitFormObjectsRetrofit.Result.FormObject formObject = formObjectsRetrofit.getResult().get(i).getForm_object();
                                        if( formObject != null){
                                        final SplitFormObjectsRetrofit.Result.FormObject.InnerFormObject preFormObject = formObjectsRetrofit.getResult().get(i).getForm_object().getPre_form_object();
                                        final SplitFormObjectsRetrofit.Result.FormObject.InnerFormObject postFormObject = formObjectsRetrofit.getResult().get(i).getForm_object().getPost_form_object();

                                        if (preFormObject != null) {
                                            preFormObject.setLeadId(formObjectsRetrofit.getResult().get(i).getLead_id());

                                            preFormObject.setAction_id(WSConstants.FormAction.PRE_DONE);

                                            RealmResults<FormObjectDb> realmDelete = realm.where(FormObjectDb.class).equalTo("action_id",
                                                    WSConstants.FormAction.PRE_DONE)
                                                    .equalTo("scheduled_activity_id",
                                                            formObjectsRetrofit.getResult().get(i).getScheduled_activity_id())
                                                    .findAll();
                                            System.out.println("Form data:size:" + realmDelete.size());
                                            if (realmDelete.size() > 0) {
                                                realmDelete.deleteAllFromRealm();
                                            }

                                            preFormObject.setScheduled_activity_id(formObjectsRetrofit.getResult().get(i).getScheduled_activity_id());
                                            realm.createObjectFromJson(FormObjectDb.class, new Gson().toJson(preFormObject));

                                            RealmResults<FormObjectDb> realmCount = realm.where(FormObjectDb.class).equalTo("action_id",
                                                    WSConstants.FormAction.PRE_DONE)
                                                    .equalTo("scheduled_activity_id",
                                                            formObjectsRetrofit.getResult().get(i).getScheduled_activity_id())
                                                    .findAll();
                                            System.out.println("Pre form count::" + realmCount.size());
                                        }

                                        if (postFormObject != null) {
                                            postFormObject.setLeadId(formObjectsRetrofit.getResult().get(i).getLead_id());

                                            postFormObject.setAction_id(WSConstants.FormAction.POST_DONE);

                                            RealmResults<FormObjectDb> realmDelete = realm.where(FormObjectDb.class)
                                                    .equalTo("action_id", WSConstants.FormAction.POST_DONE)
                                                    .equalTo("scheduled_activity_id",
                                                            formObjectsRetrofit.getResult().get(i).getScheduled_activity_id())
                                                    .findAll();
                                            if (realmDelete.size() > 0) {
                                                realmDelete.deleteAllFromRealm();
                                            }
                                            postFormObject.setScheduled_activity_id(formObjectsRetrofit.getResult().get(i).getScheduled_activity_id());
                                            realm.createObjectFromJson(FormObjectDb.class, new Gson().toJson(postFormObject));

                                        }

                                    }
                                }
                                }
                            }, new Realm.Transaction.OnSuccess() {
                                @Override
                                public void onSuccess() {
                                    initFormRendering();
                                }
                            }, new Realm.Transaction.OnError() {
                                @Override
                                public void onError(Throwable error) {
                                    System.out.println(error.toString());

                                }
                            });


                        } else {
                            System.out.println(TAG + "Success:2" + formObjectsRetrofit.getMessage());

                        }
                    }


                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("Error");
                    relLoader.setVisibility(View.GONE);
                    onErrorApiCallController(error);
                }
            });

        }

    }

    private void initFormRendering() {
        llParent.removeAllViews();
        System.out.println(TAG + "realm after inserting..");
        System.out.println(TAG + "realm after inserting.. initFormRendering called");

        System.out.println(TAG + "Form data realm count:" + realm.where(FormObjectDb.class).findAll().size());

        formObjectDb = realm.where(FormObjectDb.class).equalTo("action_id", actionId).equalTo("scheduled_activity_id", scheduledActivityId).findFirst();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (formObjectDb != null) {
                    nestedScrollView.setVisibility(View.VISIBLE);
                    initCreateView(formObjectDb, formObjectDb.getQuestionChildren());
                }
            }
        });

        if (fromDone) {
            showTestAnswerData();
        }
    }


    private void initActivityFormRendering() {

        System.out.println(TAG + "realm after inserting..");
        System.out.println(TAG + "realm after inserting.. initActivityFormRendering called");

        System.out.println(TAG + "Form data realm count:" + realm.where(FormObjectDb.class).findAll().size());
        llParent.removeAllViews();
        formObjectDb = realm.where(FormObjectDb.class).equalTo("action_id", actionId).equalTo("leadId", Util.getInt(pref.getLeadID())).findFirst();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (formObjectDb != null) {
                    nestedScrollView.setVisibility(View.VISIBLE);
                    initCreateView(formObjectDb, formObjectDb.getQuestionChildren());
                }
            }
        });
        showTestAnswerData();
    }


    private void showTestAnswerData() {
        FormAnswerDB formAnswerDB = realm.where(FormAnswerDB.class).equalTo("action_id", actionId + "").equalTo("scheduled_activity_id", scheduledActivityId + "").findFirst();
        if (formAnswerDB != null) {
            System.out.println("Action Id:" + formAnswerDB.getAction_id());
            System.out.println("SCHEDULED_ID:" + formAnswerDB.getScheduled_activity_id());
            for (int i = 0; i < formAnswerDB.getForm_response().size(); i++) {
                System.out.println("Name:" + formAnswerDB.getForm_response().get(i).getName());
                FormAnswerValueDB value = formAnswerDB.getForm_response().get(i).getValue();
                if (value != null) {
                    System.out.println("Value Data:");
                    System.out.println("Answer Id:" + value.getFAnsId());
                    System.out.println("Answer Value:" + value.getAnswerValue());
                    System.out.println("Display Text:" + value.getDisplayText());
                }
            }
        }
    }

    private void showTestData(RealmList<FormObjectQuestionChildren> formObjectQuestionChildren) {
        if (formObjectQuestionChildren.size() > 0) {
            for (int i = 0; i < formObjectQuestionChildren.size(); i++) {
                //  System.out.println("Started at " + i);
                for (int j = 0; j < formObjectQuestionChildren.get(i).getValues().size(); j++) {
                    FormObjectQuestionValues currentQuestionChildren = formObjectQuestionChildren.get(i).getValues().get(j);
                    System.out.println(TAG + "In DB:" + j + ":Title:" + currentQuestionChildren.getTitle());
                    //  tvTesting.append(Html.fromHtml("<br/>" + currentQuestionChildren.getTitle() + "(<font color='red'>" + currentQuestionChildren.getFormInputType() + "<font>)" + "<br/>"));
                    //tvTesting.append("\nAnswer{\n");
                   /* for(int k=0;k<currentQuestionChildren.getAnswerChildren().size();k++){
                        Activity_to_schedule.AnswerChildren answerChildren = currentQuestionChildren.getAnswerChildren().get(k);
                        tvTesting.append(Html.fromHtml("<font color='blue'>"+answerChildren.getDisplayText()+"<font>"+"<br/>"));
                    }*/
                    //  tvTesting.append("}\n---------------------------\n");
//                    viewGenerator.createView(currentQuestionChildren.get);
                    showTestData(currentQuestionChildren.getQuestionChildren());
                }
                //   System.out.println("Finished at " + i);


            }
        }
    }

    private void initCreateView(FormObjectDb formObjectDb, RealmList<FormObjectQuestionChildren> formObjectQuestionChildren) {
        if (formObjectQuestionChildren.size() > 0) {
            for (int i = 0; i < formObjectQuestionChildren.size(); i++) {
                for (int j = 0; j < formObjectQuestionChildren.get(i).getValues().size(); j++) {
                    FormObjectQuestionValues currentQuestionChildren = formObjectQuestionChildren.get(i).getValues().get(j);
                    System.out.println(TAG + "In DB:" + j + ":Title:" + currentQuestionChildren.getTitle());
                    viewGenerator.createView(currentQuestionChildren);

                }


            }
            if (fromDone) {
                viewGenerator.populateData(formObjectDb);
            }
        }
        btFormAction.setVisibility(View.VISIBLE);
    }

    public void populateView() {
        // System.out.println(TAG + formObject.getQuestionChildren().size());
        createQuestionChildrenView(formObject.getQuestionChildren());
    }

    public void createQuestionChildrenView(List<Activity_to_schedule.Form_object.QuestionChildren> questionChildrens) {
        if (questionChildrens.size() > 0) {
            for (int i = 0; i < questionChildrens.size(); i++) {
                // System.out.println("Started at "+i);
                for (int j = 0; j < questionChildrens.get(i).getValues().size(); j++) {
                    Activity_to_schedule.Form_object.QuestionChildren.Values currentQuestionChildren = questionChildrens.get(i).getValues().get(j);
                    // System.out.println(TAG + ":"+j+":Title:" + currentQuestionChildren.getTitle());
                    //tvTestingFromServer.append(Html.fromHtml("<br/>" + currentQuestionChildren.getTitle() + "(<font color='red'>" + currentQuestionChildren.getFormInputType() + "<font>)" + "<br/>"));
                    //tvTestingFromServer.append("\nAnswer{\n");
                    for (int k = 0; k < currentQuestionChildren.getAnswerChildren().size(); k++) {
                        Activity_to_schedule.AnswerChildren answerChildren = currentQuestionChildren.getAnswerChildren().get(k);
                        //  tvTestingFromServer.append(Html.fromHtml("<font color='blue'>" + answerChildren.getDisplayText() + "<font>" + "<br/>"));
                    }
                    //tvTestingFromServer.append("}\n---------------------------\n");
                    // viewGenerator.createView(currentQuestionChildren.getFormInputType(),currentQuestionChildren.getFQId(),currentQuestionChildren.getTitle());

                    createQuestionChildrenView(currentQuestionChildren.getQuestionChildren());
                }
                //  System.out.println("Finished at "+i);


            }

        }

    }


    @Override
    public void success(FormObjectRetrofit formObjectRetrofit, Response response) {
        relLoader.setVisibility(View.GONE);
        List<Header> headerList = response.getHeaders();
        for (Header header : headerList) {
            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                ApiUtil.UpdateAccessToken(header.getValue());
            }
        }

        if(formObjectRetrofit.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
            ApiUtil.InvalidUserLogout(FormRenderingActivity.this,0);
        }

        if (!formObjectRetrofit.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println(TAG + "Success:0" + formObjectRetrofit.getMessage());

        } else {
            if (formObjectRetrofit.getResult() != null) {
                System.out.println(TAG + "Success:1" + formObjectRetrofit.getMessage());
                formObject = formObjectRetrofit.getResult().getForm_object();
                System.out.println("Form data" + formObjectRetrofit.getResult().getForm_object().getfQId());
                System.out.println("Data:" + new Gson().toJson(formObject));
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        formObject.setLeadId(Integer.parseInt(pref.getLeadID()));
                        formObject.setAction_id(actionId);
                        if (actionId != WSConstants.FormAction.ADD_ACTIVITY) {
                            formObject.setScheduled_activity_id(scheduledActivityId);
                        }
                        realm.createObjectFromJson(FormObjectDb.class, new Gson().toJson(formObject));
                    }
                });
                if (actionId == WSConstants.FormAction.ADD_ACTIVITY) {
                    initActivityFormRendering();
                } else {
                    initFormRendering();
                }

                // populateView();

            } else {
                System.out.println(TAG + "Success:2" + formObjectRetrofit.getMessage());

            }
        }

    }

    @Override
    public void failure(RetrofitError error) {
        System.out.println("Error");
        relLoader.setVisibility(View.GONE);
        onErrorApiCallController(error);

    }

    private void onErrorApiCallController(RetrofitError error) {
        if (error.getKind() == RetrofitError.Kind.NETWORK) {
            apiCallCount++;
            if (apiCallCount <= WSConstants.API_MAX_CALL) {
                Util.showToast(getApplicationContext(), "Failed to fetch form from server", Toast.LENGTH_SHORT);
            } else {
                apiCallCount = 0;
            }
        } else {
            if (!isFinishing()) {
                new AutoDialog(this, new AutoDialogModel("Failed to fetch form from server", "Retry", "Close")).show();

            }
            Util.systemPrint("Something happened in the server");
            apiCallCount = 0;
        }
    }

    @Override
    public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
        init();
    }

    @Override
    public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {
        FormRenderingActivity.this.finish();
    }

    @Override
    public void onClickFormUpload(final FormSubmissionInputData formSubmissionInputData, int selectedTaskTypeAnswerId) {
        System.out.println("Form Answer Insertion :: ");
        if (fromDone) {
            Toast.makeText(getApplicationContext(), "Form updated", Toast.LENGTH_SHORT).show();
            FormRenderingActivity.this.finish();
        } else {
            if (sendOtp || fromPostBooking) {
                String primaryNumber ="";
                RealmResults<CustomerPhoneNumbers> phRealmResult = realm.where(CustomerPhoneNumbers.class).equalTo("leadId", Integer.parseInt(formSubmissionInputData.getLead_id())).findAll();
                for(int i =0; i<phRealmResult.size(); i++){
                    if(phRealmResult.get(i).getPhoneNumberStatus().equalsIgnoreCase("PRIMARY")){
                        primaryNumber = ""+phRealmResult.get(i).getPhoneNumber();
                        break;
                    }
                }
                initLocation();
                otpDialogPlanNextTask =  new OtpDialogPlanNextTask(this, fromPostBooking, primaryNumber, activityId, formSubmissionInputData, new TaskOtpResultListener() {
                    @Override
                    public void onValidatingTaskOtp(boolean valid, int answerId, FormSubmissionInputDataServer.LocationData locationData) {
                        formSubmissionInputData.setLocation_data(locationData);
                        if(fromPostBooking) {
                            submitForm(formSubmissionInputData, true);
                        }
                        else {
                            callPlanNextTaskFlow(formSubmissionInputData, answerId);
                        }
                    }
                });
                otpDialogPlanNextTask.show();
               // -otp screen - enter otp - validate - if all fine then taskPNT = true
            } else if(taskPNT && !taskRescheduled) {
                previousSubmissionInput = new FormSubmissionInputData();
                previousSubmissionInput.setAction_id(formSubmissionInputData.getAction_id());
                previousSubmissionInput.setLead_id(formSubmissionInputData.getLead_id());
                previousSubmissionInput.setLead_last_updated(formSubmissionInputData.getLead_last_updated());
                previousSubmissionInput.setScheduled_activity_id(formSubmissionInputData.getScheduled_activity_id());
                previousSubmissionInput.setForm_response(formSubmissionInputData.getForm_response());
                new PlanNextTaskPicker(this, new PlanNextTaskResultListener() {
                    @Override
                    public void onPlanNextTaskResult(int answerId) {
                        callFormRenderingForPntDone(formSubmissionInputData, answerId);
                    }
                }).show();
            }
            else if (isTaskRescheduled(formSubmissionInputData) || selectedTaskTypeAnswerId != -1 || isEvaluationOrLostDropForm()) {
                if (previousSubmissionInput != null) {
                    List<FormSubmissionInputData.Form_response> finalForm = new ArrayList<>();
                    finalForm.addAll(previousSubmissionInput.getForm_response());
                    finalForm.addAll(formSubmissionInputData.getForm_response());
                    previousSubmissionInput.setForm_response(finalForm);
                }
                realm.beginTransaction();
                formAnswerDb = realm.createObjectFromJson(FormAnswerDB.class, new Gson().toJson(
                        previousSubmissionInput == null ? formSubmissionInputData : previousSubmissionInput));
                realm.commitTransaction();
                if (new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
                    submitForm(previousSubmissionInput == null ? formSubmissionInputData : previousSubmissionInput, false);
                } else {
                    Util.showToast(this, "No internet connections",Toast.LENGTH_LONG);
                }

            } else {
                previousSubmissionInput = new FormSubmissionInputData();
                previousSubmissionInput.setAction_id(formSubmissionInputData.getAction_id());
                previousSubmissionInput.setLead_id(formSubmissionInputData.getLead_id());
                previousSubmissionInput.setLead_last_updated(formSubmissionInputData.getLead_last_updated());
                previousSubmissionInput.setScheduled_activity_id(formSubmissionInputData.getScheduled_activity_id());
                previousSubmissionInput.setForm_response(formSubmissionInputData.getForm_response());
                new PlanNextTaskPicker(this, new PlanNextTaskResultListener() {
                    @Override
                    public void onPlanNextTaskResult(int answerId) {
                        callFormRenderingForPntDone(formSubmissionInputData,answerId);
                    }
                }, getActivityId()).show();
            }
        }
    }


    private boolean isEvaluationOrLostDropForm() {
        if(salesCRMRealmTableCurrent!=null){
            if(salesCRMRealmTableCurrent.getActivityId() == WSConstants.TaskActivityName.LOST_DROP_ID ||
                    salesCRMRealmTableCurrent.getActivityId() == WSConstants.TaskActivityName.EVALUATION_REQUEST_ID){
                return  true;
            }
        }
        else {
            if(activityId == WSConstants.TaskActivityName.LOST_DROP_ID ||
                    activityId ==  WSConstants.TaskActivityName.EVALUATION_REQUEST_ID ){
                return true;
            }
        }
        return false;
    }

    private int getActivityId() {
        if(salesCRMRealmTableCurrent!=null){
            return salesCRMRealmTableCurrent.getActivityId();
        }
        else {
            return activityId;
        }
    }

    @Override
    public void onClickFormDataChangeListener(List<FormSubmissionInputData.Form_response> form_responses) {

        for (int i = 0; i < form_responses.size(); i++) {
            FormSubmissionInputData.Form_response.Value data = form_responses.get(i).getValue();
            taskPNT = false;
            sendOtp = false;
            if (data != null) {
                if (data.getQuestionId()!=null &&
                        data.getQuestionId()== WSConstants.FormQuestionId.LOST_DROP_NO &&
                        data.getAnswerValue().equalsIgnoreCase("0")) {
                    taskPNT = true;
                    btFormAction.setText("Plan Next Task");
                    break;
                }else if(data.getQuestionId()!=null ){
                        if(data.getQuestionId()== WSConstants.FormQuestionId.VISIT_YES &&
                            data.getAnswerValue().equalsIgnoreCase("1") ||
                                (data.getQuestionId()== WSConstants.FormQuestionId.TESTDRIVE_YES &&
                                        data.getAnswerValue().equalsIgnoreCase("1")) ||
                            (data.getQuestionId() == WSConstants.FormQuestionId.OTHER_ACTIVITY &&
                            !data.getAnswerValue().equalsIgnoreCase("1"))){

                            if(DbUtils.isBike()){
                                taskPNT = true;
                                btFormAction.setText("Plan Next Task");
                            }else {
                                sendOtp = true;
                                btFormAction.setText("Send Otp");
                            }
                            break;
                    }
                }
            }
            System.out.println("Form Fanswer Id:");
            btFormAction.setText(formAction);
        }

        for (int i = 0; i < form_responses.size(); i++) {
            FormSubmissionInputData.Form_response.Value data = form_responses.get(i).getValue();
            if (data != null) {
                if (data.getQuestionId()!=null &&
                        data.getQuestionId()== WSConstants.FormQuestionId.RESCHEDULED &&
                        data.getAnswerValue().equalsIgnoreCase("1")) {
                    taskRescheduled = true;
                    btFormAction.setText("Reschedule");
                    break;
                }
            }
            System.out.println("Form Fanswer Id:");
            if(!taskPNT && !sendOtp) {
                btFormAction.setText(formAction);
            }
            taskRescheduled = false;
        }

    }

    private boolean isTaskRescheduled(FormSubmissionInputData formSubmissionInputData) {
        return taskRescheduled;
    }

    private void callPlanNextTaskFlow(final FormSubmissionInputData formSubmissionInputData, int answerId){
        previousSubmissionInput = new FormSubmissionInputData();
        previousSubmissionInput.setAction_id(formSubmissionInputData.getAction_id());
        previousSubmissionInput.setLead_id(formSubmissionInputData.getLead_id());
        previousSubmissionInput.setLead_last_updated(formSubmissionInputData.getLead_last_updated());
        previousSubmissionInput.setScheduled_activity_id(formSubmissionInputData.getScheduled_activity_id());
        previousSubmissionInput.setForm_response(formSubmissionInputData.getForm_response());
        previousSubmissionInput.setLocation_data(formSubmissionInputData.getLocation_data());
        new PlanNextTaskPicker(this, new PlanNextTaskResultListener() {
            @Override
            public void onPlanNextTaskResult(int answerId) {
                callFormRenderingForPntDone(formSubmissionInputData, answerId);
            }
        }).show();
    }
    private void callFormRenderingForPntDone(final FormSubmissionInputData formSubmissionInputData, int answerId) {
        if(answerId == WSConstants.FormAnswerId.BOOK_CAR_ID){

            Intent filterActivity = new Intent(FormRenderingActivity.this, C360Activity.class);
            filterActivity.putExtra(WSConstants.C360_TAB_POSITION,1);
            filterActivity.putExtra(WSConstants.C360_PREV_FORM_FOR_BOOK_CAR, new Gson().toJson(formSubmissionInputData));
            startActivity(filterActivity);
            finish();
        }
        else if(answerId == WSConstants.FormAnswerId.BOOK_RE) {
            new DialogCrm(this, new DialogCrm.DialogCrmListener() {
                @Override
                public void onDialogCrmYesClick() {
                    bookBike();
                }

                private void bookBike() {
                    progressDialog.setMessage("Please wait..");
                    progressDialog.show();
                    BookBikeModel bookBikeModel = new BookBikeModel();
                    bookBikeModel.setLead_id(formSubmissionInputData.getLead_id());
                    bookBikeModel.setLead_last_updated(formSubmissionInputData.getLead_last_updated());
                    bookBikeModel.setForm_object(formSubmissionInputData);

                    ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).bookBike(bookBikeModel, new Callback<BookingConfirmResponse>() {
                        @Override
                        public void success(BookingConfirmResponse o, Response response) {
                            progressDialog.dismiss();
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }
                            if(o.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                                ApiUtil.InvalidUserLogout(FormRenderingActivity.this,0);
                            }

                            if (!o.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                System.out.println("Success:0" + o.getMessage());
                                handleFailure();
                                fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));
                                //showAlert(validateOtpResponse.getMessage());
                            } else {
                                if (o.getResult() != null) {
                                        TasksDbOperation.getInstance().updateLeadLastUpdated(realm, formSubmissionInputData.getLead_id(), o.getResult().getLead_last_updated());
                                        realm.executeTransaction(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm realm) {
                                                //data.setUploaded(true);
                                                if(formAnswerDb!=null && formAnswerDb.isValid()) {
                                                    formAnswerDb.setIs_synced(true);
                                                }
                                                updateActionPlan();
                                            }
                                        });
                                        fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));

                                        // FormRenderingActivity.this.finish();
                                        Toast.makeText(getApplicationContext(), "Form has been submitted", Toast.LENGTH_LONG).show();

                                }
                                else {
                                    System.out.println("Success:2" + o.getMessage());
                                    handleFailure();
                                    fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));

                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            progressDialog.dismiss();
                            handleFailure();
                            fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));
                            //   Toast.makeText(getApplicationContext(),"Something went wrong", Toast.LENGTH_SHORT).show();
                            System.out.println("Something went wrong while submit form");

                        }
                    });

                }
            }).show();
        }
        else if(answerId == WSConstants.FormAnswerId.BOOKED) {
            Intent filterActivity = new Intent(FormRenderingActivity.this, C360Activity.class);
            filterActivity.putExtra(WSConstants.C360_TAB_POSITION,1);
            filterActivity.putExtra(WSConstants.C360_PREV_FORM_FOR_BOOK_CAR, new Gson().toJson(formSubmissionInputData));
            startActivity(filterActivity);
            finish();
        }
        else if(answerId == WSConstants.FormAnswerId.IL_RENEWED || answerId == WSConstants.FormAnswerId.SALES_CONFIRMED) {
            new ILomConfirmedProcess().show(this, this,
                    formSubmissionInputData.getLead_id(),
                    formSubmissionInputData.getScheduled_activity_id(),
                    answerId,
                    formSubmissionInputData,
                    fromC360?ILomConfirmedProcess.FROM_C_360 : ILomConfirmedProcess.FROM_FORM_RENDERING);
        }
        else if(answerId == WSConstants.FormAnswerId.VERIFICATION) {
            new ILSendToVerificationProcess().show(this, this,
                    formSubmissionInputData.getLead_id(),
                    formSubmissionInputData.getScheduled_activity_id(),
                    answerId,
                    formSubmissionInputData,
                    fromC360? ILSendToVerificationProcess.FROM_C_360 : ILSendToVerificationProcess.FROM_FORM_RENDERING);
        }
        else {
            Intent intent = getIntent();
            intent.putExtra("form_title", " Plan Next Task");
            intent.putExtra("form_action", "Submit");
            intent.putExtra("action_id", WSConstants.FormAction.POST_DONE);
            intent.putExtra("answer_id", answerId);
            setIntent(intent);
            init();
        }

    }


    private void submitForm(final FormSubmissionInputData formSubmissionInputData, boolean isPostBooking) {
        progressDialog.show();
        FormSubmissionActivityInputData formSubmissionActivityInputData = new FormSubmissionActivityInputData();
        formSubmissionActivityInputData.setLead_id(formSubmissionInputData.getLead_id());
        formSubmissionActivityInputData.setLead_last_updated(formSubmissionInputData.getLead_last_updated());
        formSubmissionActivityInputData.setSchedule_activity_id(formSubmissionInputData.getScheduled_activity_id());
        formSubmissionActivityInputData.setScheduled_type(scheduledType + "");
        formSubmissionActivityInputData.setForm_response(formSubmissionInputData.getForm_response());

        if (formSubmissionInputData.getAction_id().equalsIgnoreCase(WSConstants.FormAction.ADD_ACTIVITY + "")) {
            progressDialog.setMessage("Creating activity..");
            System.out.println("String with quote:" + formSubmissionActivityInputData.toString());
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).submitNewActivity(formSubmissionActivityInputData, new Callback<FormSubmissionActivityResponse>() {
                @Override
                public void success(FormSubmissionActivityResponse o, Response response) {
                    progressDialog.dismiss();
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }
                    if(o.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                        ApiUtil.InvalidUserLogout(FormRenderingActivity.this,0);
                    }

                    if (!o.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        System.out.println("Success:0" + o.getMessage());
                        //showAlert(validateOtpResponse.getMessage());
                        handleFailure();
                        fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));

                    } else {
                        if (o.getResult() != null) {
                            System.out.println("Success:1" + o.getMessage());
                            TasksDbOperation.getInstance().updateLeadLastUpdated(realm, o.getResult().getLead_id(), o.getResult().getLead_last_updated());
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    //data.setUploaded(true);
                                    System.out.println("Old scheduled next deleted");
                                    SalesCRMRealmTable scheduleNextTask = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
                                    if (scheduleNextTask != null) {
                                        if (scheduleNextTask.getActivityId() == WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY_ID ||
                                                scheduleNextTask.getActivityId() == WSConstants.TaskActivityName.ALLOT_DSE_ID) {
                                            formAnswerDb.setIs_synced(true);
                                            scheduleNextTask.setDone(true);
                                            if (formObjectDb.isValid()) {
                                                scheduleNextTask.setFormQuestionDB(formObjectDb);
                                            }
                                            scheduleNextTask.setFormAnswerDB(formAnswerDb);
                                            System.out.println("Old allot dse or scheduled next deleted");
                                        }

                                    }
                                }
                            });
                            fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));
                            Toast.makeText(getApplicationContext(), "New activity created", Toast.LENGTH_LONG).show();


                        } else {
                            // relLoader.setVisibility(View.GONE);
                            System.out.println("Success:2" + o.getMessage());
                            //Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                            handleFailure();
                            fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));
                            //showAlert(validateOtpResponse.getMessage());
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("Something went wrong while submit new task");
                    handleFailure();
                    fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));
                }
            });
        } else {

            String progressTitle = "Submitting form..";
            if (isTaskRescheduled(formSubmissionInputData)) {
                progressTitle = "Rescheduling task..";
            }

            FormSubmissionInputDataServer doneForm = new FormSubmissionInputDataServer();
            doneForm.setLead_id(formSubmissionInputData.getLead_id());
            doneForm.setLead_last_updated(formSubmissionInputData.getLead_last_updated());
            doneForm.setAction_id(formSubmissionInputData.getAction_id());

            if(isPostBooking) {
                doneForm.setScheduled_activity_id("-1");
                doneForm.setActivity_id(activityId+"");
                if(activityId == WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID || activityId == WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID) {
                    doneForm.setCategory_instance_id(leadCarId);
                }

            }
            else {
                doneForm.setScheduled_activity_id(formSubmissionInputData.getScheduled_activity_id());
                doneForm.setCategory_instance_id(null);
                doneForm.setActivity_id(null);
            }

            List<FormSubmissionInputDataServer.Form_response> form_responses = new ArrayList<>();
            for (int i = 0; i < formSubmissionInputData.getForm_response().size(); i++) {
                FormSubmissionInputDataServer.Form_response formResponseDone = new FormSubmissionInputDataServer().new Form_response();
                formResponseDone.setName(formSubmissionInputData.getForm_response().get(i).getName());

                FormSubmissionInputDataServer.Form_response.Value value =
                        new FormSubmissionInputDataServer().new Form_response().new Value();
                value.setFAnsId(formSubmissionInputData.getForm_response().get(i).getValue().getFAnsId());
                value.setDisplayText(formSubmissionInputData.getForm_response().get(i).getValue().getDisplayText());
                value.setAnswerValue(formSubmissionInputData.getForm_response().get(i).getValue().getAnswerValue());
                formResponseDone.setValue(value);
                form_responses.add(formResponseDone);
            }
            doneForm.setForm_response(form_responses);

            //Location Data
            doneForm.setLocation_data(formSubmissionInputData.getLocation_data());

            progressDialog.setMessage(progressTitle);
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).submitForm(doneForm, new Callback<FormSubmissionResponse>() {
                @Override
                public void success(final FormSubmissionResponse o, Response response) {
                    progressDialog.dismiss();
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }
                    if(o.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                        ApiUtil.InvalidUserLogout(FormRenderingActivity.this,0);
                    }

                    if (!o.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        System.out.println("Success:0" + o.getMessage());
                        handleFailure();
                        fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));
                        //showAlert(validateOtpResponse.getMessage());
                    } else {
                        if (o.getResult() != null) {
                            if (o.getResult().isSubmission_status()) {

                                TasksDbOperation.getInstance().updateLeadLastUpdated(realm, o.getResult().getLead_id(), o.getResult().getLead_last_updated());
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        //data.setUploaded(true);
                                        if(formAnswerDb!=null && formAnswerDb.isValid()) {
                                            formAnswerDb.setIs_synced(true);
                                        }
                                        updateActionPlan();
                                    }
                                });

                                if(o.getResult().getGame_data()!= null && pref.getmGameLocationId()!=-1) {
                                    if(o.getResult().getGame_data().getBadge_details()!= null && o.getResult().getGame_data().getBadge_details().size()!=0){
                                        final String jsonString = new Gson().toJson(o.getResult().getGame_data()).toString();
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                //fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));
                                                finish();
                                                callBadgesActivity(jsonString);
                                                //finish();
                                            }
                                        }, 2000);
                                    }
                                    if (o.getResult().getGame_data().getPoints_earned()!= null
                                            && !o.getResult().getGame_data().getPoints_earned().equalsIgnoreCase("0")) {
                                        showPointsToaster(o.getResult().getGame_data().getPoints_earned(), o.getResult().getGame_data().getActivity());
                                        fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));
                                    }

                                }else {
                                    fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));
                                }

                                // FormRenderingActivity.this.finish();
                                Toast.makeText(getApplicationContext(), "Form has been submitted- refresh action plan", Toast.LENGTH_LONG).show();
                            } else {
                                // relLoader.setVisibility(View.GONE);
                                System.out.println("Success:2" + o.getMessage());
                                handleFailure();
                                fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));

                                //    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                                //showAlert(validateOtpResponse.getMessage());
                            }
                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    progressDialog.dismiss();
                    handleFailure();
                    fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));
                    //   Toast.makeText(getApplicationContext(),"Something went wrong", Toast.LENGTH_SHORT).show();
                    System.out.println("Something went wrong while submit form");
                }
            });
        }


    }

    private void callBadgesActivity(String jsonString) {
        Intent intent = new Intent(getApplicationContext(), BadgesNotificationActivity.class);
        intent.putExtra("badges_notification", jsonString);
        startActivity(intent);
    }

    private void handleFailure() {
        Toast.makeText(getApplicationContext(), "Form will be refreshed, Try again", Toast.LENGTH_SHORT).show();
        realm.beginTransaction();
        SalesCRMRealmTable data = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
        if (data != null) {
            data.deleteFromRealm();
        }
        if (formAnswerDb != null && formAnswerDb.isValid()) {
            formAnswerDb.deleteFromRealm();
        }
        realm.commitTransaction();
    }

    private void updateActionPlan() {
        System.out.println("Updated action plan called");
        SalesCRMRealmTable data = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
        PlannedActivities plannedActivitiesData = realm.where(PlannedActivities.class).equalTo("plannedActivityScheduleId", scheduledActivityId).findFirst();
        if (data != null) {
            if (plannedActivitiesData != null) {
                plannedActivitiesData.setDone(true);
            }
            long leadID = data.getLeadId();
            if (actionId == WSConstants.FormAction.PRE_DONE || actionId == WSConstants.FormAction.POST_DONE) {
                data.setDone(true);
                if (!formObjectDb.isValid()) {
                    if (actionId == WSConstants.FormAction.ADD_ACTIVITY && realm.where(FormObjectDb.class).equalTo("action_id", actionId).equalTo("leadId", Util.getInt(pref.getLeadID())).findFirst() != null) {
                        formObjectDb = realm.where(FormObjectDb.class).equalTo("action_id", actionId).equalTo("leadId", Util.getInt(pref.getLeadID())).findFirst();
                    } else if (realm.where(FormObjectDb.class).equalTo("action_id", actionId).equalTo("scheduled_activity_id", scheduledActivityId).findFirst() != null) {
                        formObjectDb = realm.where(FormObjectDb.class).equalTo("action_id", actionId).equalTo("scheduled_activity_id", scheduledActivityId).findFirst();

                    }
                }
                data.setFormQuestionDB(formObjectDb);
                if (formAnswerDb!=null && formAnswerDb.isValid()) {
                    data.setFormAnswerDB(formAnswerDb);
                }
                System.out.println("data value: after changing:" + data.isDone());
            } else if (actionId == WSConstants.FormAction.RESCHEDULE) {
                SalesCRMRealmTable salesCRMRealmTable = new SalesCRMRealmTable();
                salesCRMRealmTable.setCustomerId(data.getCustomerId());
                salesCRMRealmTable.setCustomerNumber(data.getCustomerNumber());
                salesCRMRealmTable.setCustomerName(data.getCustomerName());
                salesCRMRealmTable.setCustomerEmail(data.getCustomerEmail());
                salesCRMRealmTable.setFirstName(data.getFirstName());
                salesCRMRealmTable.setLastName(data.getLastName());
                salesCRMRealmTable.setGender(data.getGender());
                salesCRMRealmTable.setTitle(data.getTitle());
                salesCRMRealmTable.setCustomerAge(data.getCustomerAge());

                salesCRMRealmTable.setResidencePinCode(data.getResidencePinCode());
                salesCRMRealmTable.setResidenceAddress(data.getResidenceAddress());
                salesCRMRealmTable.setResidenceLocality(data.getResidenceLocality());
                salesCRMRealmTable.setResidenceCity(data.getResidenceCity());
                salesCRMRealmTable.setResidenceState(data.getResidenceState());


                salesCRMRealmTable.setOfficePinCode(data.getOfficePinCode());
                salesCRMRealmTable.setOfficeAddress(data.getOfficeAddress());
                salesCRMRealmTable.setOfficeLocality(data.getOfficeLocality());
                salesCRMRealmTable.setOfficeCity(data.getOfficeCity());
                salesCRMRealmTable.setOfficeState(data.getOfficeState());

                System.out.println("Form rendering Company Before" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
                salesCRMRealmTable.setCompanyName(data.getCompanyName());
                System.out.println("Form rendering Company Before" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
                salesCRMRealmTable.setFormQuestionDB(formObjectDb);
                salesCRMRealmTable.setFormAnswerDB(formAnswerDb);
                salesCRMRealmTable.setLeadId(data.getLeadId());
                salesCRMRealmTable.setLeadCarVariantName(data.getLeadCarVariantName());
                salesCRMRealmTable.setLeadCarModelId(data.getLeadCarModelId());
                salesCRMRealmTable.setLeadCarModelName(data.getLeadCarModelName());
                salesCRMRealmTable.setLeadTagsName(data.getLeadTagsName());
                salesCRMRealmTable.setLeadTagsColor(data.getLeadTagsColor());
                salesCRMRealmTable.setLeadDseName(data.getLeadDseName());
                salesCRMRealmTable.setLeadDsemobileNumber(data.getLeadDsemobileNumber());
                salesCRMRealmTable.setLeadStageId(data.getLeadStageId());
                salesCRMRealmTable.setLeadStage(data.getLeadStage());
                salesCRMRealmTable.setLeadAge(data.getLeadAge());
                salesCRMRealmTable.setLeadSourceName(data.getLeadSourceName());
                salesCRMRealmTable.setVinRegNo(data.getVinRegNo());
                salesCRMRealmTable.setLeadSourceId(data.getLeadSourceId());
                salesCRMRealmTable.setLeadLastUpdated(data.getLeadLastUpdated());
                salesCRMRealmTable.setLeadExpectedClosingDateAsDate(data.getLeadExpectedClosingDate());
                salesCRMRealmTable.setScheduledActivityId(data.getScheduledActivityId() + (int) System.currentTimeMillis());
                salesCRMRealmTable.setOldScheduledActivityId(data.getScheduledActivityId());
                salesCRMRealmTable.setActivityId(data.getActivityId());
                salesCRMRealmTable.setActivityDescription(data.getActivityDescription());
                salesCRMRealmTable.setActivityCreationDate(data.getActivityCreationDate());
                salesCRMRealmTable.setActivityScheduleDate(data.getActivityScheduleDate());
                salesCRMRealmTable.setActivityTypeId(data.getActivityTypeId());
                salesCRMRealmTable.setActivityType(data.getActivityType());
                salesCRMRealmTable.setActivityName(data.getActivityName());
                salesCRMRealmTable.setIsLeadActive(1);
                salesCRMRealmTable.setActivityGroupId(data.getActivityGroupId());
                salesCRMRealmTable.setActivityIconType(data.getActivityIconType());
                salesCRMRealmTable.customerPhoneNumbers = data.customerPhoneNumbers;
                salesCRMRealmTable.plannedActivities = data.plannedActivities;
                salesCRMRealmTable.customerEmailId = data.customerEmailId;
                salesCRMRealmTable.setDone(true);
                realm.copyToRealmOrUpdate(salesCRMRealmTable);
                data.deleteFromRealm();
            }
        }


    }

    private void fetchC360(int leadId) {
        if (fromC360) {
            progressDialog.setMessage("Redirecting to C360 .. Please wait");
        } else {
            progressDialog.setMessage("Please wait..");

        }
        if (!isFinishing()) {
            progressDialog.show();
        }
        this.generatedLeadId = leadId + "";
        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(generatedLeadId);
        new FetchC360InfoOnCreateLead(this, getApplicationContext(), leadData, dseId).call(false);

    }

    private void showPointsToaster(String points, String activity){
        LayoutInflater llayout = getLayoutInflater();
        View layout = llayout.inflate(R.layout.game_toast_points,
                (ViewGroup) findViewById(R.id.custom_toast_layout));
        TextView tvPoint = (TextView) layout.findViewById(R.id.custom_toast_message_one);
        String pointString = "<b><big>+"+points+"</big></b><small> Points<small>";
        tvPoint.setText(Util.fromHtml(pointString));
        TextView tvTitleActivity = (TextView) layout.findViewById(R.id.tv_title_activity);
        tvTitleActivity.setText(activity);
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP|Gravity.FILL_HORIZONTAL, 0, 200);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();
    }

    @Override
    public void onFetchC360OnCreateLeadSuccess() {
        new AutoFetchFormData(this, getApplicationContext()).call(realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Util.getInt(generatedLeadId)).equalTo("isDone", false).findAll(), true);
    }

    @Override
    public void onFetchC360OnCreateLeadError() {
        onBackPressed();
        System.out.println(TAG + "Failed");
        Toast.makeText(getApplicationContext(), "Failed fetching form data", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAutoFormsDataFetched(boolean b, RealmResults<SalesCRMRealmTable> data) {
        onBackPressed();
    }

    @Override
    public void onAutoFormsDataError(RetrofitError error, int from) {
        onBackPressed();
        Toast.makeText(getApplicationContext(), "Failed fetching form data", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onBackPressed() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (fromC360) {
            Intent in = new Intent(FormRenderingActivity.this, C360Activity.class);
            startActivity(in);
            this.finish();
        } else {
            //super.onBackPressed();
            this.finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        realm.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(locationHelper!=null) {
            locationHelper.onActivityResult(requestCode, resultCode, data);
        }
    }
}
