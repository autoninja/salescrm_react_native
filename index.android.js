import React from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Image
} from "react-native";
import codePush from "react-native-code-push";
import { name as appName } from "./app.json";
import CommunicationSettings from "./src/containers/Settings/CommunicationSettings";
import CreateEnquiry from "./src/containers/CreateEnquiry/Main";
import Dashboard from "./src/containers/Dashboard";

import { Provider } from "react-redux";
import { Provider as PaperProvider } from "react-native-paper";
import configureStore from "./src/store";
import enableFontPatch from "./src/utils/enableFontPatch";
import ColdVisitForm from "./src/containers/ColdVisit/Form";
import { getColdVisitDashboardNavigator } from "./src/containers/ColdVisit/Dashboard/ColdVisitDashboard.navigation";
import { createStackNavigator, createAppContainer } from "react-navigation";
import ExchangeCar from "./src/containers/CreateEnquiry/Main/exchange_car";
import UpdateExchangeCarDetails from "./src/containers/CreateEnquiry/Main/update_exchange_car_details";
import BookBike from './src/containers/BookBike'
import Search from './src/containers/Search'
import SearchResult from './src/containers/Search/SearchResult'
import BackIcon from './src/components/uielements/BackIcon'
import BookVehicleInit from './src/containers/BookVehicle/Init/BookVehicleInit'
import { getFormRenderingNavigation } from './src/components/DynamicForms/FormRendering.navigation'
import C360MainScreen from './src/containers/C360/C360MainHolder'
import * as Sentry from "@sentry/react-native";

if (!__DEV__) {
  Sentry.init({
    dsn: "https://41c8cdb33fa746f6beaae1f6b3266e97@sentry.io/1761934"
  });
}

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE
};
try {
  enableFontPatch();
} catch (err) { }

const store = configureStore();

class NinjaCRMSalesAndroid extends React.Component {
  getNavigationWithTitle = (navigation, title) => {
    return {
      headerStyle: {
        backgroundColor: "#2e5e86",
        elevation: 0
      },
      headerLeft: <BackIcon navigationProps={navigation} />,
      title: title,
      headerBackTitle: null,
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontWeight: 'bold',
        width: '100%',
        textAlign: 'left',
      },
    };
  };
  getSearchRouter() {
    return (
      createAppContainer(
        createStackNavigator({
          Search: {
            screen: Search,
            navigationOptions: ({ navigation }) => this.getNavigationWithTitle(navigation, "Add/ Search lead")
          },
          SearchResult: {
            screen: props => (
              <SearchResult
                {...props}
                uniqueAcrossDealerShip={JSON.parse(this.props.uniqueAcrossDealerShip)}
                addLeadEnabled={JSON.parse(this.props.addLeadEnabled)}
                coldVisitEnabled={JSON.parse(this.props.coldVisitEnabled)}
                showLeadCompetitor={JSON.parse(this.props.showLeadCompetitor)}
              />
            ),
            navigationOptions: ({ navigation }) => this.getNavigationWithTitle(navigation, "Search Result")
          }
        })
      )
    )
  }
  getFourWheelerBookingRouter(info, interestedVehicles, userRoles) {
    return createAppContainer(
      createStackNavigator({
        BOOK_VEHICLE: {
          screen: props => (
            <BookVehicleInit
              {...props}
              interestedVehicles={interestedVehicles}
              userRoles={userRoles}
              info={info}
            />
          ),
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        }
      })
    )
  }
  getEnquiryRouter() {
    const ENQUIRY_STACK = createStackNavigator({
      AddEnquiry: {
        screen: props => (
          <CreateEnquiry
            {...props}
            token={this.props.token}
            fromAndroid={true}
            primaryMobile={JSON.parse(this.props.primaryMobile)}
            userLocations={JSON.parse(this.props.userLocations)}
            allLocations={JSON.parse(this.props.allLocations)}
            interestedVehicle={JSON.parse(this.props.interestedVehicle)}
            allVehicles={JSON.parse(this.props.allVehicles)}
            enquirySourceMain={JSON.parse(this.props.enquirySourceMain)}
            enquirySourceCategories={JSON.parse(
              this.props.enquirySourceCategories
            )}
            isClientTwoWheeler={this.props.isClientTwoWheeler}
            isShowEnquirySourceCategory={this.props.isShowEnquirySourceCategory}
            activities={JSON.parse(this.props.activities)}
            teams={JSON.parse(this.props.teams)}
            activeEvents={JSON.parse(this.props.activeEvents)}
            govtAPIKeys={JSON.parse(this.props.govtAPIKeys)}
            evaluationManagers={JSON.parse(this.props.evaluationManagers)}
            HERE_MAP_APP_ID={this.props.HERE_MAP_APP_ID}
            HERE_MAP_APP_CODE={this.props.HERE_MAP_APP_CODE}
            FIRST_CALL_ADVANCE_DAYS={this.props.FIRST_CALL_ADVANCE_DAYS}
          />
        ),

        navigationOptions: ({ navigation }) => ({
          title: "Add Enquiry"
        })
      },
      ExchangeCar: {
        screen: ExchangeCar,
        navigationOptions: ({ navigation }) => ({
          title: "Add Enquiry"
        })
      }
    });
    return createAppContainer(ENQUIRY_STACK);
  }

  constructor(props) {
    super(props);
    this.state = { updating: false, is_manually_closed: false };
  }
  codePushStatusDidChange(status) {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        if (this.state.updating) {
          this.setState({ updating: false });
        }
        console.log("Checking for updates.");
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        this.setState({ updating: true });
        console.log("Downloading package.");
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        this.setState({ updating: true });
        console.log("Installing update.");
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        if (this.state.updating) {
          this.setState({ updating: false });
        }
        console.log("Up-to-date.");
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        if (this.state.updating) {
          this.setState({ updating: false });
        }
        console.log("Update installed.");
        break;
    }
  }
  codePushDownloadDidProgress(progress) {
    console.log(
      progress.receivedBytes + " of " + progress.totalBytes + " received."
    );
  }
  render() {
    let endDate = new Date();
    let startDate = new Date();
    startDate.setDate(1);
    let startDateStr =
      startDate.getFullYear() +
      "-" +
      ("0" + (startDate.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + startDate.getDate()).slice(-2);
    let endDateStr =
      endDate.getFullYear() +
      "-" +
      ("0" + (endDate.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + endDate.getDate()).slice(-2);
    global.global_filter_date = { startDateStr, endDateStr };

    global.global_etvbr_filters_selected = [];
    global.global_lost_drop_filters_selected = [];
    global.fromAndroid = true;
    global.selectedGameLocationId = null;
    global.selectedGameDate = new Date();
    global.selectedTeamdashboardTab = 0;
    global.token = this.props.token;
    global.appUserId = this.props.appUserId;

    if (this.state.updating && !this.state.is_manually_closed) {
      return (
        <View
          style={{
            backgroundColor: "#fff",
            flex: 1,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignSelf: "center",
              alignItems: "center"
            }}
          >
            <ActivityIndicator
              animating={this.state.updating}
              style={{ alignSelf: "center" }}
              color="#007fff"
              size="small"
              hidesWhenStopped={true}
            />
            <Text style={{ marginTop: 8, fontSize: 16, color: "#303030" }}>
              {" "}
              Please wait we are updating the app ..
            </Text>
            <TouchableOpacity
              style={{ width: "100%", alignItems: "center" }}
              onPress={() => {
                this.setState({ is_manually_closed: true });
              }}
            >
              <View
                style={{
                  padding: 5,
                  justifyContent: "center",
                  marginTop: 8,
                  backgroundColor: "#808080",
                  borderRadius: 3
                }}
              >
                <Text
                  adjustsFontSizeToFit={true}
                  numberOfLines={1}
                  style={{
                    textAlign: "center",
                    fontSize: 12,
                    textAlignVertical: "center",
                    color: "#fff"
                  }}
                >
                  Do in background
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      );
    }

    if (this.props.module == "SMS_EMAIL_SETTINGS") {
      return (
        <PaperProvider>
          <CommunicationSettings />
        </PaperProvider>
      );
    } else if (this.props.module == "CREATE_LEAD") {
      global.appUserId = this.props.appUserId;
      let ENQUIRY_ROUTER = this.getEnquiryRouter();
      return <ENQUIRY_ROUTER />;
    } else if (this.props.module == "COLD_VISIT_FORM") {
      return (
        <ColdVisitForm
          fromAndroid={true}
          HERE_MAP_APP_ID={this.props.HERE_MAP_APP_ID}
          HERE_MAP_APP_CODE={this.props.HERE_MAP_APP_CODE}
          mobileNumber={this.props.mobileNumber}
          userLocations={JSON.parse(this.props.userLocations)}
        />
      );
    } else if (this.props.module == "COLD_VISIT_DASHBOARD") {
      let COLD_VISIT_DASHBOARD = getColdVisitDashboardNavigator(
        this.props.isUserBH || this.props.isUserOps,
        this.props.isUserSM,
        this.props.isUserTL,
        this.props.isUserSC,
        {
          uniqueAcrossDealerShip: this.props.uniqueAcrossDealerShip,
          addLeadEnabled: this.props.addLeadEnabled,
          coldVisitEnabled: this.props.coldVisitEnabled,
          showLeadCompetitor: this.props.showLeadCompetitor
        }
      );
      return (
        <Provider store={store}>
          <COLD_VISIT_DASHBOARD />
        </Provider>
      );
    } else if (this.props.module == "DASHBOARD") {
      // return (
      //   <Provider store={store}>
      //     <C360MainScreen token={this.props.token} leadId={6462} userRoles={[{ id: 11 }]} />
      //   </Provider>
      // );
      return (
        <Provider store={store}>
          <Dashboard
            fromAndroid={true}
            isUserBH={JSON.parse(this.props.isUserBH)}
            isUserOps={JSON.parse(this.props.isUserOps)}
            isUserSM={JSON.parse(this.props.isUserSM)}
            isUserEM={JSON.parse(this.props.isUserEM)}
            isUserEV={JSON.parse(this.props.isUserEV)}
            isUserTL={JSON.parse(this.props.isUserTL)}
            isUserSC={JSON.parse(this.props.isUserSC)}
            isUserVF={JSON.parse(this.props.isUserVF)}
            allLocations={JSON.parse(this.props.allLocations)}
            enquirySourceCategories={JSON.parse(
              this.props.enquirySourceCategories
            )}
            eventCategories={JSON.parse(this.props.eventCategories)}
            notification={JSON.parse(this.props.notification)}
            uniqueAcrossDealerShip={JSON.parse(this.props.uniqueAcrossDealerShip)}
            addLeadEnabled={JSON.parse(this.props.addLeadEnabled)}
            coldVisitEnabled={JSON.parse(this.props.coldVisitEnabled)}
            showLeadCompetitor={JSON.parse(this.props.showLeadCompetitor)}
            vehcileModel={JSON.parse(this.props.interestedVehicle ? this.props.interestedVehicle : null)}
            enquirySource={[]}
            isClientILom={this.props.isClientILom}
            isClientILBank={this.props.isClientILBank}
            isClientTwoWheeler={this.props.isClientTwoWheeler}
          />
        </Provider>
      );
    } else if (this.props.module == "C360ACTIVITY") {
      return (
        <Provider store={store}>
          <C360MainScreen token={this.props.token} leadId={this.props.leadId} />
        </Provider>
      );
    } else if (this.props.module == "FormRenderingActivity") {
      //console.log("FormRenderingActivity", this.props.scheduled_activity_id, this.props.leadId, this.props.action_id)
      let FORM_VIEW = getFormRenderingNavigation(
        {
          activityId: 6,
          mobileNumber: '123',
          leadId: this.props.leadId,
          formAction: this.props.form_action,
          formTitle: this.props.form_title,
          actionId: this.props.action_id,
          scheduledActivityId: this.props.scheduled_activity_id,
          dseId: this.props.dseId,
          activityId: this.props.activity_id,
          leadLastUpdated: this.props.lead_last_updated,
          postBookingDriveOrVisit: false,
          HERE_MAP_APP_ID: this.props.HERE_MAP_APP_ID,
          HERE_MAP_APP_CODE: this.props.HERE_MAP_APP_CODE,
          isClientRoyalEnfield: this.props.isClientRoyalEnfield,
          isClientTwoWheeler: this.props.isClientTwoWheeler,
          isClientILom: this.props.isClientILom,
          isClientILomBank: this.props.isClientILomBank,
          isUserVerifier: this.props.isUserVerifier,

        }
      );
      return <FORM_VIEW />

    } else if (this.props.module == "UPDATE_EXCHANGE_CAR_DETAIL") {
      return (
        <UpdateExchangeCarDetails
          token={this.props.token}
          fromAndroid={true}
          userLocations={JSON.parse(this.props.userLocations)}
          allVehicles={JSON.parse(this.props.allVehicles)}
          evaluationManagers={JSON.parse(this.props.evaluationManagers)}
          leadId={JSON.parse(this.props.leadId)}
          updateWithoutChangingActivity={
            this.props.updateWithoutChangingActivity
          }
        />
      );
    }
    else if (this.props.module == "ADD_EXCHANGE_CAR") {
      return (
        <ExchangeCar
          module={this.props.module}
          token={this.props.token}
          fromAndroid={true}
          userLocations={JSON.parse(this.props.userLocations)}
          allVehicles={JSON.parse(this.props.allVehicles)}
          evaluationManagers={JSON.parse(this.props.evaluationManagers)}
          leadId={this.props.leadId}
          updateWithoutChangingActivity={
            this.props.updateWithoutChangingActivity
          }
          leadLocationId={this.props.leadLocationId}
        />
      );
    }
    else if (this.props.module == "BOOK_BIKE") {
      return (
        <BookBike
          interestedVehicle={JSON.parse(this.props.interestedVehicle)}
          formSubmissionInputData={JSON.parse(this.props.formSubmissionInputData)}
          leadId={this.props.leadId}
          leadLastUpdated={this.props.leadLastUpdated}
        />)
    }
    else if (this.props.module == "SEARCH") {
      let SEARCH_ROUTER = this.getSearchRouter();
      return <SEARCH_ROUTER />
    }
    else if (this.props.module == "BOOK_VEHICLE") {
      let leadId = this.props.leadId;
      let leadCarId = this.props.leadCarId;
      let stageId = this.props.stageId;
      let modelName = this.props.modelName;
      let action = this.props.action;
      let interestedVehicles = JSON.parse(this.props.interestedVehicles)
      let c360Information = JSON.parse(this.props.c360Information);
      let userRoles = JSON.parse(this.props.userRoles);
      let booking_id = JSON.parse(this.props.booking_id);
      let BOOK_VEHICLE = this.getFourWheelerBookingRouter(
        {
          leadCarId,
          booking_id,
          leadId,
          stageId,
          modelName,
          c360Information,
          action
        },
        interestedVehicles,
        userRoles
      );
      return (
        <BOOK_VEHICLE />
      )
    }
    else {
      return (
        <View>
          <Text style={{ color: "#000" }}>Invalid Input</Text>
        </View>
      );
    }
  }
}

AppRegistry.registerComponent(appName, () =>
  codePush(codePushOptions)(NinjaCRMSalesAndroid)
);
