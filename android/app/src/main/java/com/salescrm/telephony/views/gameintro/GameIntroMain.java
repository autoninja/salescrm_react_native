package com.salescrm.telephony.views.gameintro;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.salescrm.telephony.R;
import com.salescrm.telephony.presenter.gameintro.GameIntroMainPresenter;
import com.salescrm.telephony.utils.Util;

public class GameIntroMain extends AppCompatActivity implements GameIntroMainPresenter.GameIntroMainView {
    private ViewPager viewPager;
    private GameIntroMainPresenter presenter;
    private Drawable drawableActive;
    private Drawable drawableInactive;
    private LinearLayout llBottom;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_intro_main);

        presenter = new GameIntroMainPresenter(this);
        drawableActive = ContextCompat.getDrawable(this,R.drawable.oval_active);
        drawableInactive = ContextCompat.getDrawable(this,R.drawable.oval_inactive);

        viewPager = findViewById(R.id.game_intro_main_view_pager);
        llBottom  = findViewById(R.id.game_intro_main_bottom);

        viewPager.setAdapter(new GameIntroViewPagerAdapter(getSupportFragmentManager()));
       // viewPager.setPadding((int) Util.convertDpToPixel(20,this), 0, (int) Util.convertDpToPixel(20,this), 0);
        viewPager.setClipToPadding(false);
        viewPager.setPageMargin((int) Util.convertDpToPixel(20,this));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                
            }

            @Override
            public void onPageSelected(int position) {
                presenter.updateBottomOval(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        presenter.init();

    }

    @Override
    public void init() {
    }

    @Override
    public void updateOval(int currentPosition, int lastPosition) {
        llBottom.getChildAt(currentPosition).setBackground(drawableActive);
        llBottom.getChildAt(lastPosition).setBackground(drawableInactive);
    }
}
