import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback
} from 'react-native'
import { getFormRenderingNavigation } from '../DynamicForms/FormRendering.navigation'
import AppConfDBService from '../../storage/app_conf_db_service';

export default class FormRenderingRouterIOS extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        let FORM_VIEW = getFormRenderingNavigation(
            {
                leadId: this.props.navigation.getParam('leadId'),
                formAction: this.props.navigation.getParam('form_action'),
                formTitle: this.props.navigation.getParam('form_title'),
                actionId: this.props.navigation.getParam('actionId'),
                scheduledActivityId: this.props.navigation.getParam('scheduledActivityId'),
                dseId: this.props.navigation.getParam('dseId'),
                activityId: this.props.navigation.getParam('activity_id'),
                leadLastUpdated: this.props.navigation.getParam('lead_last_updated'),
                postBookingDriveOrVisit: this.props.navigation.getParam('postBookingTDVisit', false),
                HERE_MAP_APP_ID: this.props.navigation.getParam('HERE_MAP_APP_ID'),
                HERE_MAP_APP_CODE: this.props.navigation.getParam('HERE_MAP_APP_CODE'),
                isClientRoyalEnfield: (AppConfDBService.getVal('bike_activity_conf') == 1 ? true : false),
                isClientTwoWheeler: (AppConfDBService.getVal('two_wheeler_conf') == 1 ? true : false),
                isClientILom: (AppConfDBService.getVal('il_flow') == 1 ? true : false),
                isClientILomBank: (AppConfDBService.getVal('il_bank_flow') == 1 ? true : false),
                isUserVerifier: false,
                navigation: this.props.navigation

            }
        );
        //console.error("Crashed....", AppConfDBService.getVal('bike_activity_conf'), AppConfDBService.getVal('two_wheeler_conf'))
        return (
            <FORM_VIEW />
        )
    }

}