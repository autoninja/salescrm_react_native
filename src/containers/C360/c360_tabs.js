import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback
} from 'react-native'
import C360Tasks from './c360_tasks'
import C360Cars from './c360_cars'
import C360Details from './c360_details'
import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation'



export default class C360Tabs extends Component {
    constructor(props) {
        super(props);
        //console.log("this.props.leadIdthis.props.leadId", this.props.interestedCarsDetails)
        this.state = {
            leadId: this.props.leadId,
            leadLastUpdated: this.props.leadDetails.lead_last_updated,
            interestedCarsDetails: this.props.interestedCarsDetails,
            exchangeCarsDetails: this.props.exchangeCarsDetails,
            additionalCarsDetails: this.props.additionalCarsDetails,
            il_flow: this.props.il_flow,
            il_bank_flow: this.props.il_bank_flow,
            bookingDetails: this.props.bookingDetails,
            isViewAllowed: this.props.isViewAllowed
        }
    }

    refresh360 = () => {
        this.props.refresh360(this.state.leadId);
    }
    getC360Tabs() {
        return (
            createAppContainer(createMaterialTopTabNavigator(
                (this.state.il_flow || this.state.il_bank_flow) ?
                    {
                        Tasks: {
                            screen: props => <C360Tasks {...props} userRoles={this.props.userRoles}
                                allVehicles={this.props.allVehicles} leadId={this.props.leadId} plannedActivities={this.props.plannedActivities} leadLastUpdated={this.state.leadLastUpdated} />,
                            navigationOptions: ({ navigation }) => ({
                                title: 'TASKS',
                            }),
                        },
                    } :
                    {
                        Tasks: {
                            screen: props => <C360Tasks {...props} userRoles={this.props.userRoles}
                                allVehicles={this.props.allVehicles} leadId={this.props.leadId} plannedActivities={this.props.plannedActivities} leadLastUpdated={this.state.leadLastUpdated} />,
                            navigationOptions: ({ navigation }) => ({
                                title: 'TASKS',
                            }),
                        },
                        Vehicles: {
                            screen: props => <C360Cars
                                {...props}
                                formSubmissionInputData={this.props.formSubmissionInputData}
                                userRoles={this.props.userRoles}
                                allVehicles={this.props.allVehicles}
                                interestedVehicles={this.props.interestedVehicles}
                                isClientTwoWheeler={this.props.isClientTwoWheeler}
                                leadId={this.props.leadId}
                                leadLastUpdated={this.state.leadLastUpdated}
                                interestedCarsDetails={this.state.interestedCarsDetails}
                                exchangeCarsDetails={this.state.exchangeCarsDetails}
                                additionalCarsDetails={this.state.additionalCarsDetails}
                                bookingDetails={this.state.bookingDetails}
                                leadDetails={this.props.leadDetails}
                                c360Information={this.props.c360Information}
                                isViewAllowed={this.state.isViewAllowed}
                                parentNavigation={this.props.navigation}
                                refresh360={this.refresh360}
                                plannedActivities={this.props.plannedActivities} />,

                            navigationOptions: ({ navigation }) => ({
                                title: 'VEHICLES',
                            }),
                        },
                        Details: {
                            screen: props => <C360Details {...props} userRoles={this.props.userRoles}
                                allVehicles={this.props.allVehicles} leadId={this.props.leadId} leadDetails={this.props.leadDetails} />,
                            navigationOptions: ({ navigation }) => ({
                                title: 'DETAILS',
                            }),
                        },
                    },
                {
                    backBehavior: "none",
                    initialRouteName: this.props.OPEN_TAB,
                    swipeEnabled: true,
                    animationEnabled: true,
                    tabBarOptions: {
                        style: {
                            height: 50,
                            backgroundColor: '#2e5e86',
                        },
                        labelStyle: {
                            fontSize: 15
                        },
                        inactiveTintColor: '#fff',
                        activeTintColor: '#F5A623',
                        upperCaseLabel: true,
                        indicatorStyle: { opacity: 0 },

                    }
                })
            ));

    }

    render() {
        const Tabs = this.getC360Tabs();
        //console.log("lllllllll", this.props.leadId)
        return (
            <View style={{ flex: 1, width: '100%' }}>
                <Tabs />
                {/* <C360Tasks 
                leadId = {this.props.leadId} 
                plannedActivities = {this.props.plannedActivities} /> */}
            </View>
        );
    }
}