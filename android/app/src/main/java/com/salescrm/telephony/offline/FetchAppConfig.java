package com.salescrm.telephony.offline;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.AppConfigDB;
import com.salescrm.telephony.db.GameLocationDB;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AppConfResponse;
import com.salescrm.telephony.response.ExternalAPIKeysResponse;
import com.salescrm.telephony.response.gamification.GameConfigResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 23/9/17.
 * Call this to get the user data and gamification data
 */
public class FetchAppConfig implements Callback<AppConfResponse> {
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;

    public FetchAppConfig(OfflineSupportListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }

    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getAppConfiguration(this);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.APP_CONFIG);

        }
    }


    @Override
    public void success(final AppConfResponse data, Response response) {
        Util.updateHeaders(response.getHeaders());

        if (!data.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + data.getMessage());
            //showAlert(validateappConfResponse.getMessage());
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.APP_CONFIG);

        } else {
            if (data.getResult() != null) {
                System.out.println("Success:1" + data.getMessage());
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        insertData(realm, data);
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).fetchGamificationConfig(new Callback<GameConfigResponse>() {
                            @Override
                            public void success(final GameConfigResponse configResponse, Response response) {
                                if (configResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                    realm.executeTransactionAsync(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            insertGameConfig(realm,configResponse);
                                        }
                                    }, new Realm.Transaction.OnSuccess() {
                                        @Override
                                        public void onSuccess() {
                                            //Get the externel url api keys
                                           getExternalApiKeys(data);
                                        }
                                    }, new Realm.Transaction.OnError() {
                                        @Override
                                        public void onError(Throwable error) {
                                            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.GAME_CONFIG);
                                        }
                                    });

                                } else {
                                    listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.GAME_CONFIG);
                                }


                            }

                            @Override
                            public void failure(RetrofitError error) {
                                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.GAME_CONFIG);
                            }
                        });
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.APP_CONFIG);
                    }
                });


            } else {
                // relLoader.setVisibility(View.GONE);
                System.out.println("Success:2" + data.getMessage());
                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.APP_CONFIG);
                //showAlert(validateappConfResponse.getMessage());
            }
        }
    }

    private void getExternalApiKeys(AppConfResponse data) {
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getExternalAPIKeys(new Callback<ExternalAPIKeysResponse>() {
            @Override
            public void success(final ExternalAPIKeysResponse configResponse, Response response) {
                if (configResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) && configResponse.getResult()!=null) {
                    pref.setGovtApiKeys(configResponse.getResult().getGov_data_api_keys());
                    listener.onAppConfFetched(data);
                } else {
                    listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.EXTERNAL_API);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.EXTERNAL_API);
            }
        });


    }

    private void insertGameConfig(Realm realm, GameConfigResponse configResponse) {
        pref.setmGameLocationId(-1);
        pref.setGameLocationName(null);
        pref.setSystemDate(System.currentTimeMillis()+"");
        realm.where(GameLocationDB.class).findAll().deleteAllFromRealm();
        System.out.println("System Date:Before:"+Util.getCalender(pref.getSystemDate()).getTime().toString());

        if(configResponse.getResult() == null) {
            return;
        }
        if(configResponse.getResult().getDate()!=null && Util.getDate(configResponse.getResult().getDate())!=null) {
            pref.setSystemDate(Util.getDate(configResponse.getResult().getDate()).getTime()+"");
        }

        System.out.println("System Date:After:"+Util.getCalender(pref.getSystemDate()).getTime().toString());


        for(int i=0;i<configResponse.getResult().getLocations().size();i++) {
            GameConfigResponse.Result.GameLocation gameLocation = configResponse.getResult().getLocations().get(i);
            if(gameLocation.getId()!=null) {

                GameLocationDB gameLocationDB = new GameLocationDB();
                gameLocationDB.setId(gameLocation.getId());
                gameLocationDB.setName(gameLocation.getName());
                gameLocationDB.setGamification(gameLocation.isGamification());
                gameLocationDB.setTargets_locked(gameLocation.isTargets_locked());
                gameLocationDB.setStart_date(gameLocation.getStart_date());
                realm.copyToRealmOrUpdate(gameLocationDB);
                System.out.println("Game Insert "+gameLocation.getName()
                        +":GameOn:"+gameLocation.isGamification()
                        +":Target Locked:"+gameLocation.isTargets_locked());
            }
            System.out.println("Game Insert Success");
        }
    }

    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.APP_CONFIG);
        System.out.println(error);

    }

    private void insertData(Realm realm, AppConfResponse appConfResponse) {
        realm.where(AppConfigDB.class).findAll().deleteAllFromRealm();
        pref.setTwoWheelerClient(false);
        pref.setClientILom(false);
        pref.setClientILBank(false);
        pref.setScreenshotEnabled(false);
        pref.setHotlineNumber("");
        RealmList<AppConfigDB> AppConfigDBList = new RealmList<>();
        for (int i = 0; i < (appConfResponse.getResult() == null ? 0 : appConfResponse.getResult().size()); i++) {

            if (appConfResponse.getResult().get(i).getKey().contains("telephony_enabled")) {
                if (appConfResponse.getResult().get(i).getValue().contains("1")) {
                    pref.setIsTelephonyNeeded(true);
                } else {
                    pref.setIsTelephonyNeeded(false);
                }
            }
            else if(appConfResponse.getResult().get(i).getKey().equalsIgnoreCase(WSConstants.AppConfig.TWO_WHEELER_CONF)) {
                if (appConfResponse.getResult().get(i).getValue().contains("1")) {
                    pref.setTwoWheelerClient(true);
                } else {
                    pref.setTwoWheelerClient(false);
                }
            }
            else if(appConfResponse.getResult().get(i).getKey().equalsIgnoreCase(WSConstants.AppConfig.I_LOM_FLOW)) {
                if (appConfResponse.getResult().get(i).getValue().contains("1")) {
                    pref.setClientILom(true);
                } else {
                    pref.setClientILom(false);
                }
            }
            else if(appConfResponse.getResult().get(i).getKey().equalsIgnoreCase(WSConstants.AppConfig.I_LOM_BANK_FLOW)) {
                if (appConfResponse.getResult().get(i).getValue().contains("1")) {
                    pref.setClientILBank(true);
                } else {
                    pref.setClientILBank(false);
                }
            }
            else if(appConfResponse.getResult().get(i).getKey().equalsIgnoreCase(WSConstants.AppConfig.SCREESHOT_ENABLED)) {
                if (appConfResponse.getResult().get(i).getValue().contains("1")) {
                    pref.setScreenshotEnabled(true);
                } else {
                    pref.setScreenshotEnabled(false);
                }
            } else if(appConfResponse.getResult().get(i).getKey().equalsIgnoreCase(WSConstants.AppConfig.HOTLINE_NUMBER)) {
                pref.setHotlineNumber(appConfResponse.getResult().get(i).getValue());
            }

            AppConfigDB appConfigDB = new AppConfigDB();
            appConfigDB.setId(appConfResponse.getResult().get(i).getId());
            appConfigDB.setIs_active(appConfResponse.getResult().get(i).getIs_active());
            appConfigDB.setIs_edit(appConfResponse.getResult().get(i).getIs_edit());
            appConfigDB.setKey(appConfResponse.getResult().get(i).getKey());
            appConfigDB.setValue(appConfResponse.getResult().get(i).getValue());
            AppConfigDBList.add(appConfigDB);


        }
        realm.copyToRealmOrUpdate(AppConfigDBList);
    }

}
