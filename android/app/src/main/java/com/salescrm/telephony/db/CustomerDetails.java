package com.salescrm.telephony.db;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 16-08-2016.
 */
public class CustomerDetails extends RealmObject {

    public static final String CUSTOMERDETAILS = "CustomerDetails";

    @PrimaryKey
    private int customerID;

    private int leadId;
    @Index
    private String customerMobileNumber;
    private String customerName;
    private String customerEmail;
    private String customerDesignation;
    private String customerAddress;
    private String typeOfCustomer;
    private String modeOfPayment;
    private String oemSource;
    private String buyerTypeId;
    private String closeDate;
    private String firstName;
    private String lastName;
    private String enqnumber;
    private String gender;
    private String residenceAddress;
    private String residencePinCode;
    private String officeAddress;
    private String officePinCode;
    private String customerAge;

    public RealmList<CustomerPhoneNumbers> customerPhoneNumbers = new RealmList<>();
    public RealmList<CustomerEmailId> customerEmailId = new RealmList<>();

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public int getLeadId() {
        return leadId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getResidenceAddress() {
        return residenceAddress;
    }

    public void setResidenceAddress(String residenceAddress) {
        this.residenceAddress = residenceAddress;
    }

    public String getResidencePinCode() {
        return residencePinCode;
    }

    public void setResidencePinCode(String residencePinCode) {
        this.residencePinCode = residencePinCode;
    }

    public String getEnqnumber() {
        return enqnumber;
    }

    public void setEnqnumber(String enqnumber) {
        this.enqnumber = enqnumber;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public String getOfficePinCode() {
        return officePinCode;
    }

    public void setOfficePinCode(String officePinCode) {
        this.officePinCode = officePinCode;
    }

    public String getCustomerAge() {
        return customerAge;
    }

    public void setCustomerAge(String customerAge) {
        this.customerAge = customerAge;
    }

    public void setLeadId(int leadId) {
        this.leadId = leadId;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(String customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerDesignation() {
        return customerDesignation;
    }

    public void setCustomerDesignation(String customerDesignation) {
        this.customerDesignation = customerDesignation;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getTypeOfCustomer() {
        return typeOfCustomer;
    }

    public void setTypeOfCustomer(String typeOfCustomer) {
        this.typeOfCustomer = typeOfCustomer;
    }

    public String getModeOfPayment() {
        return modeOfPayment;
    }

    public void setModeOfPayment(String modeOfPayment) {
        this.modeOfPayment = modeOfPayment;
    }

    public String getOemSource() {
        return oemSource;
    }

    public void setOemSource(String oemSource) {
        this.oemSource = oemSource;
    }

    public String getBuyerTypeId() {
        return buyerTypeId;
    }

    public void setBuyerTypeId(String buyerTypeId) {
        this.buyerTypeId = buyerTypeId;
    }
}
