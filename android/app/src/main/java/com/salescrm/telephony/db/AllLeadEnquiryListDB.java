package com.salescrm.telephony.db;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 17/2/17.
 */

public class AllLeadEnquiryListDB extends RealmObject {
    private String message;
    @PrimaryKey
    private String count;
    public RealmList<LeadListDetails> leadDetails = new RealmList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public RealmList<LeadListDetails> getLeadListDetailses() {
        return leadDetails;
    }

    public void setLeadListDetailses(RealmList<LeadListDetails> leadListDetailses) {
        this.leadDetails = leadListDetailses;
    }
}
