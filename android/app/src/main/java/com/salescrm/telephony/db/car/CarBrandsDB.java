package com.salescrm.telephony.db.car;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 14/12/16.
 */

public class CarBrandsDB extends RealmObject {

    @PrimaryKey
    private String brand_id;

    private RealmList<CarBrandModelsDB> brand_models;


    private String brand_name;

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public RealmList<CarBrandModelsDB> getBrand_models() {
        return brand_models;
    }

    public void setBrand_models(RealmList<CarBrandModelsDB> brand_models) {
        this.brand_models = brand_models;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

}
