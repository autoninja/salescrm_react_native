package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bannhi on 25/9/17.
 */

public class WeeklyData extends RealmObject {

    private int weekNo;
    private int avgPending;

    public int getWeekNo() {
        return weekNo;
    }

    public void setWeekNo(int weekNo) {
        this.weekNo = weekNo;
    }

    public int getAvgPending() {
        return avgPending;
    }

    public void setAvgPending(int avgPending) {
        this.avgPending = avgPending;
    }
}
