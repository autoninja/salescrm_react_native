package com.salescrm.telephony.model;

import com.salescrm.telephony.interfaces.TeamDashboardSwipedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bharath on 6/10/17.
 */

public class TeamDashboardSwiped {

    private static TeamDashboardSwipedListener[] listeners =new TeamDashboardSwipedListener[3];

    private static final TeamDashboardSwiped instance =new TeamDashboardSwiped();

    public static TeamDashboardSwiped getInstance(TeamDashboardSwipedListener teamDashboardSwipedListener,int position) {
        listeners[position] = teamDashboardSwipedListener;
        System.out.println("Inserting:::");
        return instance;
    }

    public static TeamDashboardSwiped getInstance() {
        return instance;
    }

    public boolean post(int position,int tabCount){
        boolean success=false;

        for(TeamDashboardSwipedListener listener : listeners){
           if(listener!=null){
               success = true;
               listener.onTeamDashboardSwiped(position,tabCount);

           }
       }
       return success;
    }
}
