package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.car.AdditionalCarsDetails;
import com.salescrm.telephony.fragments.NewCarsFragment;
import com.salescrm.telephony.interfaces.NewCarsProgressLoader;
import com.salescrm.telephony.preferences.Preferences;

import io.realm.Realm;
import io.realm.RealmList;

public class AdditionalCarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    RealmList<AdditionalCarsDetails> additionalCarsDetailsRealmList = new RealmList<>();
    Activity activity;
    Preferences pref;
    private Realm realm;
    private NewCarsProgressLoader mNewCarsProgressLoader;

    public AdditionalCarAdapter(RealmList<AdditionalCarsDetails> additionalCarsDetailsRealmList, Activity activity) {
        this.activity = activity;
        this.additionalCarsDetailsRealmList = additionalCarsDetailsRealmList;
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(activity);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = View.inflate(parent.getContext(), R.layout.additional_car_card, null);
        System.out.println("Additional ADAPTER CALLED");
        mNewCarsProgressLoader = new NewCarsFragment();
        return new AdditionalCarItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final RecyclerView.ViewHolder iViewHolder = holder;
        if (holder instanceof AdditionalCarItemHolder) {
            final AdditionalCarsDetails temp = additionalCarsDetailsRealmList.get(position);
            //System.out.println("getRoleId"+ pref.getRoleId());
            String purchaseDate = "", regNo = "",kms="", ownerType = "";
            if(purchaseDate!=null && !purchaseDate.isEmpty()){
                ((AdditionalCarAdapter.AdditionalCarItemHolder) iViewHolder).carNameTv.setText
                        (temp.getName() + "(" + purchaseDate + ")");
            }else{
                ((AdditionalCarAdapter.AdditionalCarItemHolder) iViewHolder).carNameTv.setText
                        (temp.getName());
            }


            //((AdditionalCarItemHolder) iViewHolder).car_details.setText(temp.getReg_no()+"");

            //populateData(iViewHolder, temp);
        }
    }

    private void populateData(RecyclerView.ViewHolder iViewHolder, AdditionalCarsDetails temp) {
    }

    @Override
    public int getItemCount() {
        return additionalCarsDetailsRealmList.size();
    }

    private class AdditionalCarItemHolder extends RecyclerView.ViewHolder {

        //public   View parentLayout;
//        TextView carNameTv, tvPriceQuoteValue, tvPriceExpectedValue, tvPriceMarketValue, stageTv,
//                stageChangeBtnTv, evaluatorName, evaluation_date_time, car_details;
//        ImageButton carEditIb;
//        ImageView stageIcon;
//        RelativeLayout priceLyt, evulationDetailsLyt;
//        TextView btnExchangeDetail;

        TextView carNameTv, car_details;

        public AdditionalCarItemHolder(View itemView) {
            super(itemView);
            // parentLayout = itemView;
            carNameTv = (TextView) itemView.findViewById(R.id.car_name);
            car_details = (TextView) itemView.findViewById(R.id.car_details);
            /*priceLyt = (RelativeLayout) itemView.findViewById(R.id.price_rll);
            // priceValue = (RelativeLayout) itemView.findViewById(R.id.price_value_block);

            evulationDetailsLyt = (RelativeLayout) itemView.findViewById(R.id.evaluation_details);
            evaluatorName = (TextView) itemView.findViewById(R.id.evaluator_name);
            evaluation_date_time = (TextView) itemView.findViewById(R.id.evaluation_date_time);

            tvPriceQuoteValue = (TextView) itemView.findViewById(R.id.price_quote_value);
            tvPriceMarketValue = (TextView) itemView.findViewById(R.id.market_price_value);
            tvPriceExpectedValue = (TextView) itemView.findViewById(R.id.price_expected_value);

            stageTv = (TextView) itemView.findViewById(R.id.stage_text);
            stageIcon = (ImageView) itemView.findViewById(R.id.stage_icon);
            stageChangeBtnTv = (TextView) itemView.findViewById(R.id.btn_nxt_stage);
            carEditIb = (ImageButton) itemView.findViewById(R.id.car_edit);
            btnExchangeDetail = (TextView) itemView.findViewById(R.id.tv_exchange_details);*/


        }

    }
}
