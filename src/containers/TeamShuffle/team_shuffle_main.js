import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback, ScrollView,
} from 'react-native'
import RestClient from '../../network/rest_client'
import Toast, { DURATION } from "react-native-easy-toast";
import UserDp from './user_dp';

const styles = StyleSheet.create({
    main: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        backgroundColor: "#2e5e86",
        alignItems: 'center',
        paddingTop: (Platform.OS) === 'ios' ? 20 : 5,
    },
    listItem: {
        paddingLeft: 7,
        paddingRight: 7,
        paddingTop: 7,
        paddingBottom: 7,
        fontSize: 18,
        color: '#ffffff',
        textAlign: 'center'
    },
    pop_up: {
        width: "100%",
        justifyContent: 'center',
        backgroundColor: "#2e5e86",
        alignItems: 'center',
        padding: 10,
        marginTop: 20,
        marginBottom: 20
    },
    popup_heading: {
        color: "#ffffff",
        fontSize: 20,
        marginBottom: 10
    },
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: '#000000',
        padding: 20,

    },
    popup_button_view: {
        width: 250,
        backgroundColor: '#53759D',
        borderRadius: 5,
        margin: 10,
    },
    manager_view: {
        backgroundColor: "#000000",
        flexDirection: "row",
        margin: 5,
        padding: 5,
        borderRadius: 2
    },
    manager_circle: {
        alignSelf: "center",
        width: 36, height: 36, borderRadius: 36 / 2,
        backgroundColor: '#0076ff', alignItems: 'center', justifyContent: 'center'
    },
    name_view: {
        flexDirection: 'column',
        marginLeft: 5,
        alignSelf: 'center',
    },
    lead_view: {
        backgroundColor: "#66b1f0",
        flexDirection: "row",
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 15,
        marginRight: 5,
        padding: 5,
        borderRadius: 2
    },
    lead_circle: {
        alignSelf: "center",
        width: 36, height: 36, borderRadius: 36 / 2,
        backgroundColor: '#00C853', alignItems: 'center', justifyContent: 'center'
    },
    user_back_view: {
        flex: 1,
        flexDirection: "row",
        flexWrap: 'wrap',
        marginLeft: 20,
        padding: 5,

    },
    user_circle: {
        alignSelf: "center",
        width: 36, height: 36, borderRadius: 36 / 2,
        backgroundColor: '#FFCA28', alignItems: 'center', justifyContent: 'center'
    },
    user_view: {
        backgroundColor: "#fff",
        flexDirection: "row",
        padding: 5,
        marginRight: 2,
        marginLeft: 2,
        marginTop: 2,
        marginBottom: 2,
        borderRadius: 2
    },
    move_grid: {
        flexDirection: "row",
        flexWrap: 'wrap',
        padding: 5,

    },
    moveGridColor: {
        backgroundColor: "#C0C0C0",
        flexDirection: "row",
        padding: 5,
        marginRight: 1,
        marginLeft: 1,
        marginTop: 1,
        marginBottom: 1,
        justifyContent: 'center'
    },
    moveGridColorSelected: {
        backgroundColor: "#838383",
        flexDirection: "row",
        padding: 5,
        marginRight: 1,
        marginLeft: 1,
        marginTop: 1,
        marginBottom: 1,
        justifyContent: 'center'
    },
    grid_name_view: {
        flexDirection: 'column',
        marginLeft: 5,
        justifyContent: 'center',
        alignItems: "center",
    },

})

export default class TeamSuffleMain extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isFetching: false,
            openLocationPopUp: (this.props.allLocations.length > 1) ? true : false,
            locationId: this.props.allLocations[0].id,
            managersList: [],
            selectedLead: '',
            selectedUser: '',
            parentId: '',
            openLeadModal: false,
            openUserModal: false,
            itemType: '',  //1-SM, 2-TL, 3-DSE
            isDeactivateUser: false,
            userIdforDeactivation: '',
            isUserSelectedFromGrid: false,
            selectedData: '',
            indexValue: { id: '', isSelected: false },
        }
    }


    onSelection(item) {
        //console.error("locationId:item.id", item)
        this.setState({ locationId: item.id, openLocationPopUp: false })
        this.getAllManagerHierarchyUsers(item.id)
    }

    componentWillMount() {
        if (this.props.allLocations.length == 1) {
            this.getAllManagerHierarchyUsers(this.state.locationId)
        }
    }

    getAllManagerHierarchyUsers(locationId) {
        this.setState({ isLoading: true, isFetching: true });
        //console.error('Called')
        new RestClient().getAllManagerHierarchyUsersApi({ location_id: locationId })
            .then((responseJson) => {
                //console.error('Called')
                //console.error("called -----" + JSON.stringify(responseJson))


                this.setState({
                    isLoading: false,
                    isFetching: false,
                    managersList: responseJson.result,
                }, function () {
                    // In this block you can do something with new state.
                    //console.log(responseJ
                    // const size  = responseJson.result.length;
                    // console.log(responseJson.result.length);
                });
            }).catch(error => {

                this.setState({ isLoading: false, isFetching: false });
            });
    }

    showAlert(msg) {
        this.refs.toast.show(msg);
    }

    onLeadPress(item, parentId, managers) {
        //console.log("OnLeadPress", JSON.stringify(item))
        if (managers <= 1) {
            this.showAlert("TL can't be moved as there is no other sales manager in this location")
        }
        else if (item.team_leader_id == parentId) {
            Alert.alert("",
                item.team_leader_name + " is also a Sales Manager, you can not move to another Sales Manager"
            )
            //this.showAlert(item.team_leader_name+" is also a Sales Manager, you can not move to another Sales Manager")
        } else {
            this.setState({ openLeadModal: true, selectedLead: item, parentId: parentId })
        }

    }

    onUserPress(item, parentId) {
        //console.log("OnUserPress", JSON.stringify(item))
        if (item.id == parentId) {
            Alert.alert("",
                item.name + " is also a TL, you can not move to another TL"
            )
            //this.showAlert(item.name+" is also a TL, you can not move to another TL")
        } else {
            this.setState({ openUserModal: true, selectedUser: item, parentId: parentId })
        }
    }

    userSelectedFromLeadGrid(team_leader_id, current_manager_id, new_manager_id) {
        let { indexValue } = this.state;
        let item = {
            team_leader_id: team_leader_id,
            current_manager_id: current_manager_id,
            new_manager_id: new_manager_id
        }
        let index = {
            id: new_manager_id,
            isSelected: (indexValue.id == new_manager_id) ? !indexValue.isSelected : true
        }
        this.setState({ isDeactivateUser: false, itemType: 2, selectedData: item, indexValue: index })
    }

    userSelectedFromDseGrid(user_id, team_id, same_location) {
        let { indexValue } = this.state;
        let item = {
            user_id: user_id,
            team_id: team_id,
            same_location: same_location
        }
        let index = {
            id: team_id,
            isSelected: (indexValue.id == team_id) ? !indexValue.isSelected : true
        }
        this.setState({ isDeactivateUser: false, itemType: 3, selectedData: item, indexValue: index })
    }


    cancelUserDialog() {
        this.setState({ openLeadModal: false, openUserModal: false, isDeactivateUser: false, selectedData: '', indexValue: { id: '', isSelected: false }, })
    }

    confirmLeadDialog() {

    }

    confirmUserDialog(typeId) {

        if (this.state.isDeactivateUser) {
            this.callDeactivateUser()
        } else {
            if (this.state.selectedData) {
                this.moveUser(typeId)
            } else {
                //this.showAlert("Select a user to proceed!!")
                Alert.alert(
                    'You need to select a user to proceed!!'
                )
            }
        }
    }

    deactivateUser(userId) {
        let { isDeactivateUser } = this.state
        this.setState({ isDeactivateUser: !isDeactivateUser, userIdforDeactivation: userId, selectedData: '', indexValue: { id: '', isSelected: false } })
    }

    callDeactivateUser() {
        let { userIdforDeactivation } = this.state;
        let { locationId } = this.state;
        this.setState({ isLoading: true, isFetching: true });
        new RestClient().deactivateUser({ id: userIdforDeactivation })
            .then((responseJson) => {
                //console.error('Called')
                //console.log("called Dea -----" + JSON.stringify(responseJson))


                this.setState({
                    isLoading: false,
                    isFetching: false,
                    openLeadModal: false,
                    openUserModal: false,
                    isDeactivateUser: false,
                    indexValue: { id: '', isSelected: false },
                })

                this.getAllManagerHierarchyUsers(locationId)

            }).catch(error => {
                this.setState({ isLoading: false, isFetching: false });
            });
    }

    moveUser(typeId) {
        let { locationId } = this.state
        let { selectedData } = this.state
        this.setState({ isLoading: true, isFetching: true });
        if (typeId == 3) {
            new RestClient().moveDse(selectedData)
                .then((responseJson) => {
                    //console.error('Called')
                    //console.log("moveDse  -----" + JSON.stringify(responseJson))


                    this.setState({
                        isLoading: false,
                        isFetching: false,
                        openLeadModal: false,
                        openUserModal: false,
                        isDeactivateUser: false
                    })

                    this.getAllManagerHierarchyUsers(locationId)

                }).catch(error => {

                    this.setState({ isLoading: false, isFetching: false });
                });
        }

        if (typeId == 2) {
            new RestClient().moveTeamLead(selectedData)
                .then((responseJson) => {
                    //console.error('Called')
                    //console.log("moveTeamLead -----" + JSON.stringify(responseJson))


                    this.setState({
                        isLoading: false,
                        isFetching: false,
                        openLeadModal: false,
                        openUserModal: false,
                        isDeactivateUser: false
                    })

                    this.getAllManagerHierarchyUsers(locationId)

                }).catch(error => {
                    this.setState({ isLoading: false, isFetching: false });

                });
        }
    }


    render() {
        //console.log("this.props.allLocations", this.props.allLocations.length)

        if (this.state.openLocationPopUp) {
            //console.log("this.props.allLocations", this.props.allLocations.length)
            return (

                <Modal
                    visible={this.props.openLocationPopUp}
                    animationType={'slide'}
                    transparent={true}
                    onRequestClose={() => null}
                >
                    <View style={styles.overlay}>
                        <View style={styles.pop_up}>

                            <Text style={styles.popup_heading}>Select a location</Text>

                            <FlatList
                                data={this.props.allLocations}
                                renderItem={({ item, index }) => {
                                    return (
                                        <TouchableOpacity onPress={() => this.onSelection(item)}>
                                            <View style={styles.popup_button_view}>
                                                <Text style={styles.listItem}>{item.name}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </View>
                </Modal>

            )
        } else {
            //this.getAllManagerHierarchyUsers();
            if (this.state.isLoading) {
                return (
                    <View style={{ flex: 1, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center' }}>
                        <ActivityIndicator />
                    </View>
                );
            }

            if (this.state.openLeadModal) {
                let { selectedLead } = this.state
                let { managersList } = this.state
                let { parentId } = this.state;
                let { indexValue } = this.state;
                let { isDeactivateUser } = this.state

                return (

                    <Modal
                        visible={this.state.openLeadModal}
                        animationType={'slide'}
                        transparent={true}
                        onRequestClose={() => null}
                    >
                        <View style={styles.overlay}>
                            <View style={styles.pop_up}>

                                <Text style={styles.popup_heading}>Move {selectedLead.team_leader_name} and his team to</Text>
                                <ScrollView>
                                    <View style={styles.move_grid}>
                                        {
                                            managersList.map((innerItem, key) => {
                                                //console.log("innerItem.manager_id == leadsManagerId", innerItem, parentId)
                                                if (innerItem.manager_id == parentId) {
                                                    return null;
                                                }
                                                else {
                                                    return (
                                                        <View key={key} style={{ width: "50%" }}>
                                                            <TouchableOpacity onPress={() => this.userSelectedFromLeadGrid(selectedLead.team_leader_id, parentId, innerItem.manager_id)}>
                                                                <View style={(indexValue.id == innerItem.manager_id && indexValue.isSelected) ? styles.moveGridColorSelected : styles.moveGridColor}>

                                                                    <View style={styles.grid_name_view}>
                                                                        <UserDp image_url={innerItem.image_url}
                                                                            name={innerItem.name}
                                                                            bgcolor='#0076ff' />
                                                                        <Text style={{ color: '#53759D', fontSize: 16, textAlign: 'center', alignSelf: 'center' }}>{innerItem.name.charAt(0).toUpperCase() + " " + innerItem.name.split(' ').slice(0, 1)}</Text>
                                                                    </View>
                                                                </View>
                                                            </TouchableOpacity>
                                                        </View>
                                                    )
                                                }


                                            })}
                                    </View>
                                </ScrollView>

                                <Text style={{ color: "#fff", fontSize: 14, margin: 3 }}>Or</Text>

                                <TouchableOpacity onPress={() => this.deactivateUser(selectedLead.team_leader_id)}>
                                    <View style={{ backgroundColor: (isDeactivateUser) ? "#FF0000" : "#C0C0C0", margin: 5, borderRadius: 5 }}>
                                        <Text style={{ color: (isDeactivateUser) ? "#FFFFFF" : "#FF0000", fontSize: 17, padding: 5 }}>Deactivate User</Text>
                                    </View>
                                </TouchableOpacity>

                                <View style={{ flexDirection: "row", marginTop: 5 }}>
                                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.cancelUserDialog()}>
                                        <View style={{ backgroundColor: '#909090' }}>
                                            <Text style={{ color: "#000", fontSize: 17, padding: 5, textAlign: 'center' }}>Cancel</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.confirmUserDialog(2)}>
                                        <View style={{ backgroundColor: "#0076ff" }}>
                                            <Text style={{ color: "#fff", fontSize: 17, padding: 5, textAlign: 'center' }}>Confirm</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>

                )
            }

            if (this.state.openUserModal) {
                let { selectedUser } = this.state
                let { managersList } = this.state
                let { parentId } = this.state;
                let { isDeactivateUser } = this.state
                let { indexValue } = this.state;
                return (

                    <Modal
                        visible={this.state.openUserModal}
                        animationType={'slide'}
                        transparent={true}
                        onRequestClose={() => null}
                    >
                        <View style={styles.overlay}>
                            <View style={styles.pop_up}>

                                <Text style={styles.popup_heading}>Move {selectedUser.name} to</Text>
                                <ScrollView>
                                    <View style={styles.move_grid}>
                                        {
                                            managersList.map((manager) => {
                                                //console.log("manager.manager_id", manager, parentId)
                                                return manager.team_leaders.map((teamlead, key) => {
                                                    if (teamlead.team_leader_id == parentId) {

                                                    } else {
                                                        //console.log("manager.team_leaders", teamlead.team_leader_name, parentId)

                                                        return (
                                                            <View key={key} style={{ width: "50%" }}>
                                                                <TouchableOpacity onPress={() => this.userSelectedFromDseGrid(selectedUser.id, teamlead.team_id, true)}>
                                                                    <View style={(indexValue.id == teamlead.team_id && indexValue.isSelected) ? styles.moveGridColorSelected : styles.moveGridColor}>

                                                                        <View style={styles.grid_name_view}>
                                                                            <UserDp image_url={teamlead.image_url}
                                                                                name={teamlead.team_leader_name}
                                                                                bgcolor='#0076ff' />
                                                                            <Text style={{ color: '#53759D', fontSize: 16, textAlign: 'center', alignSelf: 'center' }}>{teamlead.team_leader_name.charAt(0).toUpperCase() + " " + teamlead.team_leader_name.split(' ').slice(0, 1)}</Text>
                                                                        </View>
                                                                    </View>
                                                                </TouchableOpacity>
                                                            </View>
                                                        )
                                                    }
                                                })

                                            })}
                                    </View>
                                </ScrollView>

                                <Text style={{ color: "#fff", fontSize: 14, margin: 3 }}>Or</Text>

                                <TouchableOpacity onPress={() => this.deactivateUser(selectedUser.id)}>
                                    <View style={{ backgroundColor: (isDeactivateUser) ? "#FF0000" : "#C0C0C0", margin: 5, borderRadius: 5 }}>
                                        <Text style={{ color: (isDeactivateUser) ? "#FFFFFF" : "#FF0000", fontSize: 17, padding: 5 }}>Deactivate User</Text>
                                    </View>
                                </TouchableOpacity>

                                <View style={{ flexDirection: "row", marginTop: 5 }}>
                                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.cancelUserDialog()}>
                                        <View style={{ backgroundColor: '#909090' }}>
                                            <Text style={{ color: "#000", fontSize: 17, padding: 5, textAlign: 'center' }}>Cancel</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ flex: 1 }} onPress={() => this.confirmUserDialog(3)}>
                                        <View style={{ backgroundColor: "#0076ff" }}>
                                            <Text style={{ color: "#fff", fontSize: 17, padding: 5, textAlign: 'center' }}>Confirm</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>

                )
            }

            if (this.state.managersList.length != 0) {
                let { managersList } = this.state;
                let innerCount = 0;
                return (
                    <View style={styles.main}>

                        <Toast
                            ref="toast"
                            style={{ backgroundColor: "red" }}
                            position="top"
                            fadeInDuration={750}
                            fadeOutDuration={750}
                            opacity={0.8}
                            textStyle={{ color: "white" }}
                        />

                        <FlatList
                            data={managersList}
                            style={{ flex: 1, width: '100%' }}
                            renderItem={({ item, index }) => {
                                return (
                                    <View>
                                        <View style={styles.manager_view}>
                                            <UserDp image_url={item.image_url}
                                                name={item.name}
                                                bgcolor='#0076ff' />

                                            <View style={styles.name_view}>
                                                <Text style={{ color: '#fff', fontSize: 16 }}>{item.name}</Text>
                                                <Text style={{ color: '#fff', fontSize: 14 }}>Sales Manager</Text>
                                            </View>

                                        </View>

                                        {
                                            item.team_leaders.map((leadItem, key) => {
                                                return (
                                                    <View key={key}>
                                                        <TouchableOpacity onPress={() => this.onLeadPress(leadItem, item.manager_id, managersList.length)}>
                                                            <View style={styles.lead_view}>
                                                                <UserDp image_url={leadItem.image_url}
                                                                    name={leadItem.team_leader_name}
                                                                    bgcolor='#00C853' />

                                                                <View style={styles.name_view}>
                                                                    <Text style={{ color: '#000000', fontSize: 16 }}>{leadItem.team_leader_name}</Text>
                                                                    <Text style={{ color: '#53759D', fontSize: 14 }}>Team Lead</Text>
                                                                </View>

                                                            </View>
                                                        </TouchableOpacity>

                                                        <View style={styles.user_back_view}>
                                                            {
                                                                leadItem.users.map((innerItem, key) => {

                                                                    return (
                                                                        <View key={key} style={{ width: "50%" }}>
                                                                            <TouchableOpacity onPress={() => this.onUserPress(innerItem, leadItem.team_leader_id)}>
                                                                                <View style={styles.user_view}>
                                                                                    <UserDp image_url={innerItem.image_url}
                                                                                        name={innerItem.name}
                                                                                        bgcolor='#FFCA28' />
                                                                                    <View style={styles.name_view}>
                                                                                        <Text style={{ color: '#53759D', fontSize: 16 }}>{innerItem.name.charAt(0).toUpperCase() + " " + innerItem.name.split(' ').slice(0, 1)}</Text>
                                                                                        <Text style={{ color: '#53759D', fontSize: 14 }}>SC</Text>
                                                                                    </View>
                                                                                </View>
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    )


                                                                })}
                                                        </View>

                                                    </View>
                                                )
                                            })
                                        }
                                    </View>

                                )
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />

                    </View>
                )
            } else {
                return null;
            }
        }
    }
}