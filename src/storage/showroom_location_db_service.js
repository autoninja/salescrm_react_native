import { realm } from './realm_schemas';
import { ShowRoomLocationModel } from '../models/lead_elements_model'



const ShowRoomLocationDBService = {
  findAll: function() {
    return realm.objects('ShowRoomLocationDB');
  },
  getShowRooms : ()=> {
    let showRoomsDB = realm.objects('ShowRoomLocationDB');
    let showrooms = [];
    showRoomsDB.map((showRoomDB)=> {
      showrooms.push(new ShowRoomLocationModel(showRoomDB.id, showRoomDB.name));
    })
    return showrooms;
  },
  save: function(showRoomLocations) {
    realm.write(() => {
      showRoomLocations.map((location)=> {
        realm.create('ShowRoomLocationDB', location, true);
      })
    })
    let locations = realm.objects('ShowRoomLocationDB');
    console.log('locations.length', locations.length);
  },
  deleteAll: function() {
    realm.write(() => {
      realm.delete(realm.objects('ShowRoomLocationDB'));
    })
  },
}

export default ShowRoomLocationDBService;
