package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.model.DseModelTask;

/**
 * Created by bharath on 9/6/17.
 */

public interface TeamSummaryToTaskListener {
    void onDseTaskSummaryClicked(DseModelTask dseModelTask, boolean isUserTlOnly);
}
