package com.salescrm.telephony.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.HomeActivity;
import com.salescrm.telephony.db.FcmNotificationObject;

import io.realm.Realm;

/**
 * Created by prateek on 17/11/16.
 */

public class FCMNotification {

    private int NOTIFICATION_CAT;
    private int NOTIFICATION_TYPE;
    private String MESSAGE;
    private String leadID;
    Realm realm;
    FcmNotificationObject fcmNotificationObject;

    public FCMNotification(int NOTIFICATION_CAT, int NOTIFICATION_TYPE, String MESSAGE, String leadID) {

        this.NOTIFICATION_CAT = NOTIFICATION_CAT;
        this.NOTIFICATION_TYPE = NOTIFICATION_TYPE;
        this.MESSAGE = MESSAGE;
        this.leadID = leadID;
        // storing the data in the DB
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        fcmNotificationObject = realm.createObject(FcmNotificationObject.class);
        fcmNotificationObject.setNotifiacation_message(MESSAGE);
        fcmNotificationObject.setNotification_cat(NOTIFICATION_CAT);
        fcmNotificationObject.setNotification_type(NOTIFICATION_TYPE);
        fcmNotificationObject.setLeadId(leadID);
        fcmNotificationObject.setNotification_read(false);
        realm.commitTransaction();

        System.out.println("OnMessage: - constructor called"+"--- CAT:- "+NOTIFICATION_CAT+"  TYPE:- "+NOTIFICATION_TYPE+"  Message:- "+MESSAGE+" leadId:-"+leadID);
    }


    private void sendNotification() {

        System.out.println("OnMessage: - C: " + NOTIFICATION_CAT + "  T: " + NOTIFICATION_TYPE);


    }
}
