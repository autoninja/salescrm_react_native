package com.salescrm.telephony.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddToContactsActivity;
import com.salescrm.telephony.activity.AllotDseActivity;
import com.salescrm.telephony.activity.BookBikeProcess;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.activity.EmailSmsActivity;
import com.salescrm.telephony.activity.FormRenderingActivity;
import com.salescrm.telephony.activity.ILSendToVerificationProcess;
import com.salescrm.telephony.activity.ILomConfirmedProcess;
import com.salescrm.telephony.activity.PlanNextTaskPicker;
import com.salescrm.telephony.activity.ProformaInvoiceActivity;
import com.salescrm.telephony.activity.RNFormRenderingActivity;
import com.salescrm.telephony.activity.WhatsappActivity;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.dataitem.SectionModel;
import com.salescrm.telephony.db.FormObjectDb;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.dbOperation.CallStatDbOperation;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.PlanNextTaskResultListener;
import com.salescrm.telephony.interfaces.TaskCardExpanderListener;
import com.salescrm.telephony.model.TaskCardExpander;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by bharath on 28/9/16.
 */

public class ActionPlanRealmAdapter extends RealmRecyclerViewAdapter<SalesCRMRealmTable, ActionPlanRealmAdapter.ActionPlanRealmViewHolder> {


    private TaskCardExpanderListener expandCardListener;
    private boolean expand = false;
    private Context context;
    private List<SectionModel> sections;
    private Preferences pref = null;
    private TaskCardExpander expandPosition = null;
    private boolean isNotificationTrayTaskAllowedToView =true;
    private int tab;
    private Realm realm;
    private boolean isClientILom;

    public ActionPlanRealmAdapter(@NonNull Context context, @Nullable OrderedRealmCollection<SalesCRMRealmTable> data, boolean autoUpdate,
                                  List<SectionModel> sections) {
        super(context, data, true);
        this.context = context;
        this.pref = Preferences.getInstance();
        this.pref.load(context);
        this.sections = sections;
        realm  = Realm.getDefaultInstance();
        isClientILom = pref.isClientILom();
    }

    public ActionPlanRealmAdapter(@NonNull Context context,
                                  @Nullable OrderedRealmCollection<SalesCRMRealmTable> data, boolean autoUpdate,
                                  List<SectionModel> sections, TaskCardExpander expandPosition,TaskCardExpanderListener taskCardExpanderListener) {
        super(context, data, true);
        this.context = context;
        this.pref = Preferences.getInstance();
        this.pref.load(context);
        this.sections = sections;
        this.expandPosition = expandPosition;
        this.expandCardListener = taskCardExpanderListener;
        realm  = Realm.getDefaultInstance();
        isClientILom = pref.isClientILom();
    }

    @Override
    public ActionPlanRealmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = null;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.action_plan_card, parent, false);
        return new ActionPlanRealmViewHolder(itemLayoutView);
    }
    void openC360(String leadId, int scheduledActivityId) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(context!=null) {
                    pref.setLeadID(leadId);
                    pref.setScheduledActivityId(scheduledActivityId);
                    Intent intent = new Intent(context, C360Activity.class);
                    intent.putExtra(WSConstants.C360_TAB_POSITION, 1);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        }, 0);

    }

    @Override
    public void onBindViewHolder(final ActionPlanRealmViewHolder holder, final int position) {
        if (getData() != null && getData().get(position) != null) {
            final SalesCRMRealmTable actionPlanData = getData().get(position);
            if (sections != null) {
                for (SectionModel section : sections) {
                    if (position == section.getPosition()) {
                        holder.tvActionPlanHeader.setText(section.getTitle());
                        holder.tvActionPlanHeader.setVisibility(View.VISIBLE);
                        break;
                    } else {
                        holder.tvActionPlanHeader.setVisibility(View.GONE);
                    }
                }
            }

            if (actionPlanData.isCreatedOffline()) {
                System.out.println("Triggered:" + actionPlanData.getScheduledActivityId());
                holder.tvActionPlanPerName.setText(actionPlanData.getCreateLeadInputDataDB().getLead_data().getCustomer_details().getFirst_name());

            } else {
                holder.tvActionPlanMrMs.setText(Util.getGender(actionPlanData.getTitle()));
                if (Util.isNotNull(actionPlanData.getFirstName())) {
                    holder.tvActionPlanPerName.setText(String.format("%s%s %s", actionPlanData.getFirstName().substring(0, 1).toUpperCase(),
                            actionPlanData.getFirstName().substring(1),
                            actionPlanData.getLastName()));
                } else {
                    holder.tvActionPlanPerName.setText("Customer Name");
                }

                if (Util.isNotNull(actionPlanData.getResidenceAddress())) {
                    holder.tvActionPlanAddress.setVisibility(View.VISIBLE);
                    holder.addressViewHr.setVisibility(View.VISIBLE);
                    holder.tvActionPlanAddress.setText(actionPlanData.getResidenceAddress());
                } else if (Util.isNotNull(actionPlanData.getOfficeAddress())) {
                    holder.tvActionPlanAddress.setVisibility(View.VISIBLE);
                    holder.addressViewHr.setVisibility(View.VISIBLE);
                    holder.tvActionPlanAddress.setText(actionPlanData.getOfficeAddress());
                } else {
                    holder.tvActionPlanAddress.setVisibility(View.GONE);
                    holder.addressViewHr.setVisibility(View.GONE);
                    holder.tvActionPlanAddress.setText("No Address");

                }


                holder.tvActionPlanActionStage.setText(actionPlanData.getLeadStage());



                //Scheduled Time Format
                SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm,a", Locale.getDefault());
                if (actionPlanData.getActivityScheduleDate() != null) {
                    String formattedDate = dateFormatter.format(actionPlanData.getActivityScheduleDate().getTime());

                    String[] dateSplits = formattedDate.split(",");

                    if (dateSplits.length == 2) {
                        holder.tvActionPlanTime.setText(dateSplits[0]);
                        holder.tvActionPlanAmPm.setText(dateSplits[1].toLowerCase());
                    }

                }
                //Remarks Date Format
                SimpleDateFormat dateFormatterRemarks = new SimpleDateFormat("dd/MM,E", Locale.getDefault());
                if(actionPlanData.getActivityCreationDate()!=null ){
                    String formattedDate = dateFormatterRemarks.format(actionPlanData.getActivityCreationDate().getTime());

                    String[] dateSplits = formattedDate.split(",");

                    if (dateSplits.length == 2) {
                        holder.tvActionPlanDate.setText(dateSplits[0]);
                        holder.tvActionPlanDay.setText(dateSplits[1].toUpperCase());
                    }
                }

                holder.tvActionPlanAction.setText(actionPlanData.getActivityName());
                if (Util.isNotNull(actionPlanData.getActivityDescription())) {
              /*  holder.tvActionPlanRemarks.setVisibility(View.VISIBLE);
                holder.remarksViewHr.setVisibility(View.VISIBLE);
                holder.remarksDateLinear.setVisibility(View.VISIBLE);*/
                    holder.tvActionPlanRemarks.setText(String.format("Remarks : %s", actionPlanData.getActivityDescription()));
                } else {
               /* holder.tvActionPlanRemarks.setVisibility(View.GONE);
                holder.remarksViewHr.setVisibility(View.GONE);
                holder.remarksDateLinear.setVisibility(View.GONE);*/
                    holder.tvActionPlanRemarks.setText("Remarks : No data");
                }
                holder.tvActionPlanRemarks.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        Util.copy(context, actionPlanData.getActivityDescription());
                        Util.showToast(context, "Copied to clipboard", Toast.LENGTH_SHORT);
                        return false;
                    }
                });


                //Tag Color
                setTagColor(holder, actionPlanData.getLeadTagsName(), actionPlanData);

                if(pref.isClientILBank()){
                    holder.ibMail.setVisibility(View.GONE);
                    holder.ibWhatsapp.setVisibility(View.GONE);
                    String sourceString = "OTB : "+"<b>" + actionPlanData.getResidencePinCode() + "</b>" ;
                    //holder.tvOtb.setText(String.format("OTB : %s", actionPlanData.getResidencePinCode()));
                    holder.tvOtb.setText(Html.fromHtml(sourceString));
                    holder.tvOtb.setVisibility(View.VISIBLE);
                }else{
                    holder.tvOtb.setVisibility(View.GONE);
                }

                holder.ibMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pref.setLeadID("" + actionPlanData.getLeadId());
                        // SalesCRMApplication.sendEventToCleverTap("Mob Button Sms/Mail");
                        Intent in = new Intent(context, EmailSmsActivity.class);
                        in.putExtra("from_proforma", false);
                        in.putExtra("from_my_task",true);
                        context.startActivity(in);
                    }
                });

                holder.ibWhatsapp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //String phone = "9740176267";
                        //String ph = "9663088848";
                        //String ph = "9739703797";
                        String phone = actionPlanData.getCustomerNumber();
                        String customerName = actionPlanData.getFirstName()+" " + actionPlanData.getLastName();
                        //String customerName = "Pandu ranga";
                        if(!phone.equalsIgnoreCase("")) {
                            if (Util.checkContactAvailable(phone, context)) {
                                Intent intent = new Intent(context, WhatsappActivity.class);
                                intent.putExtra("phono", phone);
                                intent.putExtra("from_my_task",true);
                                context.startActivity(intent);
                            } else {
                                Intent intent = new Intent(context, AddToContactsActivity.class);
                                intent.putExtra("phono", phone);
                                intent.putExtra("customer_name", customerName);
                                intent.putExtra("from_proforma", false);
                                intent.putExtra("from_my_task",true);
                                context.startActivity(intent);
                            }
                        }
                    }
                });

            }
            if(actionPlanData.isTemplateExist()){
                holder.tvProformaBtn.setVisibility(View.VISIBLE);
                holder.tvProformaTry.setVisibility(View.VISIBLE);
                holder.tvProformaTry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(pref.getUsersTopRole()){
                            pref.setLeadID("" + actionPlanData.getLeadId());
                            if(new ConnectionDetectorService(context).isConnectingToInternet()) {
                                Intent proformaActivity = new Intent(context, ProformaInvoiceActivity.class);
                                proformaActivity.putExtra("phone", actionPlanData.getCustomerNumber()+"");
                                context.startActivity(proformaActivity);
                            }
                        }else {
                            Util.showToast(context, "Access Denied !!", Toast.LENGTH_LONG);
                        }
                        HashMap<String, Object> hashMap = new HashMap<String, Object>();
                        hashMap.put(CleverTapConstants.EVENT_PROFORMA_INVOICE_KEY_TYPE,
                                CleverTapConstants.EVENT_PROFORMA_INVOICE_TYPE_CLICK);
                        CleverTapPush.pushEvent(CleverTapConstants.EVENT_PROFORMA_INVOICE, hashMap);                        }
                });
                    holder.tvProformaBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(pref.getUsersTopRole()){
                                pref.setLeadID("" + actionPlanData.getLeadId());
                                if(new ConnectionDetectorService(context).isConnectingToInternet()) {
                                    Intent proformaActivity = new Intent(context, ProformaInvoiceActivity.class);
                                    proformaActivity.putExtra("phone", actionPlanData.getCustomerNumber()+"");
                                    context.startActivity(proformaActivity);
                                }
                            }else {
                                Util.showToast(context, "Access Denied !!", Toast.LENGTH_LONG);
                            }
                            HashMap<String, Object> hashMap = new HashMap<String, Object>();
                            hashMap.put(CleverTapConstants.EVENT_PROFORMA_INVOICE_KEY_TYPE,
                                    CleverTapConstants.EVENT_PROFORMA_INVOICE_TYPE_CLICK);
                            CleverTapPush.pushEvent(CleverTapConstants.EVENT_PROFORMA_INVOICE, hashMap);                        }
                    });

            }else{
                holder.tvProformaBtn.setVisibility(View.GONE);
                holder.tvProformaTry.setVisibility(View.GONE);
            }


            holder.ibCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pref.setLeadID("" + actionPlanData.getLeadId());
                    pref.setmTelephonyLeadID("" + actionPlanData.getLeadId());
                    System.out.println("TELEPHONY LEAD ID IN IB CALL" + pref.getmTelephonyLeadID());
                    pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                    if (!actionPlanData.isCreatedOffline()) {
                        String mobileNumber = "Not found";
                        if (actionPlanData.getCustomerNumber() != null) {
                            if(isClientILom || pref.isClientILBank()) {
                                callILom(actionPlanData.getCustomerNumber(), actionPlanData.getLeadId()+"", actionPlanData.getScheduledActivityId());
                            }
                            else {
                                Util.callNumber(actionPlanData.getCustomerNumber(), context,
                                        actionPlanData.getLeadId()+"",
                                        actionPlanData.getScheduledActivityId()+"");
                            }

                        } else {
                            for (int i = 0; i < actionPlanData.customerPhoneNumbers.size(); i++) {
                                if (actionPlanData.customerPhoneNumbers.get(i).getPhoneNumberStatus().equalsIgnoreCase("PRIMARY")) {
                                    mobileNumber = "" + actionPlanData.customerPhoneNumbers.get(i).getPhoneNumber();
                                }
                            }
                            if(isClientILom || pref.isClientILBank()) {
                                callILom(mobileNumber, actionPlanData.getLeadId()+"", actionPlanData.getScheduledActivityId());
                            }
                            else {
                                Util.callNumber(mobileNumber, context, actionPlanData.getLeadId()+"", actionPlanData.getScheduledActivityId()+"");
                            }

                        }

                       /* if (actionPlanData.getCustomerNumber() != null) {
                           // SalesCRMApplication.sendEventToCleverTap("Mob Button Call");
                            Util.setUserProfileCleverTap("Mob Button Call",null);
                            Util.callNumber(actionPlanData.getCustomerNumber(), context);
                        }*/
                    } else {

                    }

                }
            });
            holder.relActionPlanRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!actionPlanData.isCreatedOffline() && isJustEvaluatorOrEvaluatorManager()) {
                        pref.setLeadID("" + actionPlanData.getLeadId());
                        pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                        Intent intent = new Intent(context, C360Activity.class);
                        intent.putExtra(WSConstants.C360_TAB_POSITION,1);
                        context.startActivity(intent);
                    } else if(!actionPlanData.isCreatedOffline()){
                        pref.setLeadID("" + actionPlanData.getLeadId());
                        pref.setmSheduleActivityID(""+actionPlanData.getScheduledActivityId());
                        pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                        Intent filterActivity = new Intent(context, C360Activity.class);
                        filterActivity.putExtra("customerID", "" + actionPlanData.getCustomerId());
                        context.startActivity(filterActivity);
                    }else{

                    }
                }
            });

            holder.llTaskAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(context, "Huhuhu....."+actionPlanData.getActivityId(), Toast.LENGTH_SHORT).show();

//                    pref.setLeadID("" + actionPlanData.getLeadId());
//                    pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
//
//                    String pnt;
//                    if (actionPlanData.getActivityId() == WSConstants.TaskActivityName.LOST_DROP_ID ||
//                            actionPlanData.getActivityId() == WSConstants.TaskActivityName.EVALUATION_REQUEST_ID) {
//                        pnt = "Submit";
//                    } else {
//                        pnt = "Plan Next Task";
//                    }
//                    Intent rnFormRenderingActivityIntent = new Intent(context, RNFormRenderingActivity.class);
//                    rnFormRenderingActivityIntent.putExtra("form_title", "Update");
//                    rnFormRenderingActivityIntent.putExtra("form_action", pnt);
//                    rnFormRenderingActivityIntent.putExtra("action_id", WSConstants.FormAction.PRE_DONE);
//                    rnFormRenderingActivityIntent.putExtra("dseId", actionPlanData.getDseId());
//                    rnFormRenderingActivityIntent.putExtra("activity_id", actionPlanData.getActivityId());
//                    //rnFormRenderingActivityIntent.putExtra("formObject",""+realm.where(FormObjectDb.class).equalTo("action_id", actionPlanData.getActivityId()).equalTo("scheduled_activity_id", pref.getScheduledActivityId()).findFirst());
//                    context.startActivity(rnFormRenderingActivityIntent);


                    if (!actionPlanData.isCreatedOffline()) {
                        System.out.println("actionPlanData.getActivityId"+actionPlanData.getActivityId());
                        pref.setLeadID("" + actionPlanData.getLeadId());
                        pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                        if (actionPlanData.getActivityId() == WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY_ID) {
                            if(isInternetAvailable()) {
                                new PlanNextTaskPicker(context, new PlanNextTaskResultListener() {
                                    @Override
                                    public void onPlanNextTaskResult(int answerId) {
                                        if (answerId == WSConstants.FormAnswerId.BOOK_CAR_ID) {
                                            pref.setLeadID("" + actionPlanData.getLeadId());
                                            pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                                            Intent filterActivity = new Intent(context, C360Activity.class);
                                            filterActivity.putExtra("customerID", "" + actionPlanData.getCustomerId());
                                            filterActivity.putExtra(WSConstants.C360_TAB_POSITION, 1);
                                            context.startActivity(filterActivity);
                                        }
                                        else if(answerId == WSConstants.FormAnswerId.BOOK_RE) {
                                            pref.setLeadID("" + actionPlanData.getLeadId());
                                            pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                                            new BookBikeProcess().show(null, context,
                                                    actionPlanData.getLeadId()+"",
                                                    actionPlanData.getScheduledActivityId()+"", false);
                                        }
                                        else if(answerId == WSConstants.FormAnswerId.BOOKED) {
                                            pref.setLeadID("" + actionPlanData.getLeadId());
                                            pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                                            Intent filterActivity = new Intent(context, C360Activity.class);
                                            filterActivity.putExtra("customerID", "" + actionPlanData.getCustomerId());
                                            filterActivity.putExtra(WSConstants.C360_TAB_POSITION, 1);
                                            context.startActivity(filterActivity);
                                        }
                                        else if(answerId == WSConstants.FormAnswerId.IL_RENEWED || answerId == WSConstants.FormAnswerId.SALES_CONFIRMED) {
                                            new ILomConfirmedProcess().show(null,
                                                    context,
                                                    actionPlanData.getLeadId()+"",
                                                    actionPlanData.getScheduledActivityId()+"",
                                                    answerId,
                                                    null,
                                                    ILomConfirmedProcess.FROM_FORM_TASKS_LIST);
                                        } else if(answerId == WSConstants.FormAnswerId.VERIFICATION) {
                                            new ILSendToVerificationProcess().show(null,
                                                    context,
                                                    actionPlanData.getLeadId()+"",
                                                    actionPlanData.getScheduledActivityId()+"",
                                                    answerId,
                                                    null,
                                                    ILSendToVerificationProcess.FROM_FORM_TASKS_LIST);
                                        }
                                        else {
                                            Intent formRenderingActivityIntent = new Intent(context, FormRenderingActivity.class);
                                            formRenderingActivityIntent.putExtra("form_title", "Plan Next Task");
                                            formRenderingActivityIntent.putExtra("form_action", "Submit");
                                            formRenderingActivityIntent.putExtra("action_id", WSConstants.FormAction.ADD_ACTIVITY);
                                            formRenderingActivityIntent.putExtra("scheduled_type", 2);
                                            formRenderingActivityIntent.putExtra("dseId", actionPlanData.getDseId());
                                            formRenderingActivityIntent.putExtra("answer_id", answerId);
                                            formRenderingActivityIntent.putExtra("activity_id", actionPlanData.getActivityId());
                                            context.startActivity(formRenderingActivityIntent);
                                        }
                                    }
                                }).show();
                            }
                            else {
                               Util.showToast(context,"No internet connection",Toast.LENGTH_SHORT);
                            }

                        } else if (actionPlanData.getActivityId() == WSConstants.TaskActivityName.ALLOT_DSE_ID) {
                            //Open Allot dse form
                            Intent intent = new Intent(context, AllotDseActivity.class);
                            intent.putExtra("lead_id", actionPlanData.getLeadId());
                            intent.putExtra("lead_last_updated", actionPlanData.getLeadLastUpdated());
                            context.startActivity(intent);

                        }
                        else if(actionPlanData.getActivityId() == WSConstants.TaskActivityName.NO_CAR_BOOKED_ID ||
                                actionPlanData.getActivityId() == WSConstants.TaskActivityName.RECEIVE_PAYMENT_ID ||
                                actionPlanData.getActivityId() == WSConstants.TaskActivityName.INVOICE_REQUEST_ID ||
                                actionPlanData.getActivityId() == WSConstants.TaskActivityName.DELIVERY_REQUEST_ID ||
                                actionPlanData.getActivityId() == WSConstants.TaskActivityName.EVALUATION_REQUEST_ID ||
                                actionPlanData.getActivityId() == WSConstants.TaskActivityName.SUBMIT_FINAL_QUOTE
                                ){
                            pref.setLeadID("" + actionPlanData.getLeadId());
                            pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                            Intent intent = new Intent(context, C360Activity.class);
                            intent.putExtra(WSConstants.C360_TAB_POSITION,1);
                            context.startActivity(intent);
                        }
                        else {
                            if(isInternetAvailable()) {
                                String pnt;
                                if (actionPlanData.getActivityId() == WSConstants.TaskActivityName.LOST_DROP_ID ||
                                        actionPlanData.getActivityId() == WSConstants.TaskActivityName.EVALUATION_REQUEST_ID) {
                                    pnt = "Submit";
                                } else {
                                    pnt = "Plan Next Task";
                                }
                                Intent formRenderingActivityIntent = new Intent(context, FormRenderingActivity.class);
                                formRenderingActivityIntent.putExtra("form_title", "Update");
                                formRenderingActivityIntent.putExtra("form_action", pnt);
                                formRenderingActivityIntent.putExtra("action_id", WSConstants.FormAction.PRE_DONE);
                                formRenderingActivityIntent.putExtra("dseId", actionPlanData.getDseId());
                                formRenderingActivityIntent.putExtra("activity_id", actionPlanData.getActivityId());
                                context.startActivity(formRenderingActivityIntent);
                            }
                            else {
                                Util.showToast(context,"No internet connection",Toast.LENGTH_SHORT);
                            }
                        }
                        //SalesCRMApplication.sendEventToCleverTap("Mob Button Done");
                    }

                }
            });

            holder.ibReSchedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!actionPlanData.isCreatedOffline()) {
                        //SalesCRMApplication.sendEventToCleverTap("Mob Button Reschedule");
                        pref.setLeadID("" + actionPlanData.getLeadId());
                        pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                        Intent formRenderingActivityIntent = new Intent(context, FormRenderingActivity.class);
                        formRenderingActivityIntent.putExtra("form_title", "Reschedule");
                        formRenderingActivityIntent.putExtra("form_action", "Submit");
                        formRenderingActivityIntent.putExtra("action_id", WSConstants.FormAction.RESCHEDULE);
                        formRenderingActivityIntent.putExtra("activity_id", actionPlanData.getActivityId());
                        context.startActivity(formRenderingActivityIntent);
                    }

                }
            });

            if(isJustEvaluatorOrEvaluatorManager()){
                holder.relActionPlanLeft.setEnabled(false);
                holder.ibControlBottom.setVisibility(View.INVISIBLE);
            }else{
                holder.relActionPlanLeft.setEnabled(true);
                holder.ibControlBottom.setVisibility(View.VISIBLE);
            }

            holder.relActionPlanLeft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder.relActionPlanBottom.getVisibility() == View.VISIBLE) {

                        // mActivity.startActivity(new Intent(mActivity.getApplicationContext(), LeadDetailsActivity.class));
                      /*  if(expandCardListener!=null&&expandPosition!=null&&
                                expandPosition.getScheduledActivityId()==actionPlanData.getScheduledActivityId()){
                            expandCardListener.onExpandCard(expandPosition.getPosition(),expandPosition.getScheduledActivityId(),true);
                        }*/
                        expand = false;
                        // callCard1.setVisibility(View.GONE);
                        holder.ibControlBottom.setImageDrawable(VectorDrawableCompat.create(context.getResources(), R.drawable.ic_expand_more_24dp, context.getTheme()));
                        //   relActionPlanBottom.setVisibility(View.GONE);
                        Util.collapse(holder.relActionPlanBottom);
                    } else {

                        expand = true;

                        // callCard1.setVisibility(View.VISIBLE);
                        holder.ibControlBottom.setImageDrawable(VectorDrawableCompat.create(context.getResources(), R.drawable.ic_expand_less_24dp, context.getTheme()));
                        //  relActionPlanBottom.setVisibility(View.VISIBLE);
                        Util.expand(holder.relActionPlanBottom);
                    }

                }
            });

            if (expandPosition != null
                    && expandPosition.getPosition() >= 0
                    && expandPosition.getPosition() < getData().size()
                    && expandPosition.getPosition() ==position
                    && expandPosition.isOpen()
                    && isNotificationTrayTaskAllowedToView) {

                expand = true;
                isNotificationTrayTaskAllowedToView = false;
                // callCard1.setVisibility(View.VISIBLE);
                holder.ibControlBottom.setImageDrawable(VectorDrawableCompat.create(context.getResources(), R.drawable.ic_expand_less_24dp, context.getTheme()));
                //  relActionPlanBottom.setVisibility(View.VISIBLE);
                Util.expand(holder.relActionPlanBottom);

            }
            else {
                expand = false;
                // callCard1.setVisibility(View.VISIBLE);
                holder.ibControlBottom.setImageDrawable(VectorDrawableCompat.create(context.getResources(), R.drawable.ic_expand_more_24dp, context.getTheme()));
                //  relActionPlanBottom.setVisibility(View.VISIBLE);
                holder.relActionPlanBottom.setVisibility(View.GONE);
            }

            if(isClientILom) {
                holder.ibWhatsapp.setVisibility(View.GONE);
                holder.tvProformaBtn.setVisibility(View.GONE);
                holder.tvProformaTry.setVisibility(View.GONE);
                holder.tvEditPolicy.setVisibility(View.VISIBLE);
                holder.tvActionPlanAddress.setVisibility(View.GONE);
                holder.addressViewHr.setVisibility(View.GONE);

                holder.tvEditPolicy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(Util.isNotNull(actionPlanData.getCustomerAddress())
                                && URLUtil.isValidUrl(actionPlanData.getCustomerAddress())) {
                            openC360(actionPlanData.getLeadId()+"", actionPlanData.getScheduledActivityId());
                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Util.openLink(context, actionPlanData.getCustomerAddress());
                            }
                        }, 100);
                    }
                });
                if(actionPlanData.getLeadExpectedClosingDate()!=null) {
                    holder.tvActionPlanCar.setVisibility(View.VISIBLE);
                    holder.tvActionPlanCar.setText(Util.getDateFormatWithYear(actionPlanData.getLeadExpectedClosingDate()));
                }
                else {
                    holder.tvActionPlanCar.setVisibility(View.VISIBLE);
                    holder.tvActionPlanCar.setText("---");

                }
            }
            else {
                if (pref.isClientILBank()) {
                    holder.ibWhatsapp.setVisibility(View.GONE);
                    holder.tvProformaBtn.setVisibility(View.GONE);
                    holder.tvProformaTry.setVisibility(View.GONE);
                    holder.tvEditPolicy.setVisibility(View.GONE);
                    holder.tvActionPlanAddress.setVisibility(View.VISIBLE);
                    holder.addressViewHr.setVisibility(View.VISIBLE);
                    holder.tvActionPlanCar.setVisibility(View.GONE);
                    holder.carViewHr.setVisibility(View.GONE);
                    holder.tvActionPlanAddress.setTypeface(null, Typeface.BOLD);
                } else {
                    if (Util.isNotNull(actionPlanData.getLeadCarModelName())) {
                        holder.tvActionPlanCar.setVisibility(View.VISIBLE);
                        holder.carViewHr.setVisibility(View.VISIBLE);
                        if (actionPlanData.getLeadCarVariantName() == null) {
                            holder.tvActionPlanCar.setText(actionPlanData.getLeadCarModelName());
                        } else {
                            holder.tvActionPlanCar.setText(actionPlanData.getLeadCarVariantName());
                        }

                    } else {
                        holder.tvActionPlanCar.setVisibility(View.GONE);
                        holder.carViewHr.setVisibility(View.GONE);
                        holder.tvActionPlanCar.setText("No Car");
                    }

                    if (actionPlanData.isTemplateExist()) {
                        holder.tvProformaBtn.setVisibility(View.VISIBLE);
                        holder.tvProformaTry.setVisibility(View.VISIBLE);
                    } else {
                        holder.tvProformaBtn.setVisibility(View.GONE);
                        holder.tvProformaTry.setVisibility(View.GONE);
                    }
                    holder.ibWhatsapp.setVisibility(View.VISIBLE);
                    holder.tvEditPolicy.setVisibility(View.GONE);
                }
            }
        }


    }
    void createCallStat(String mobileNumber, String leadId, String scheduledAt) {
        long createdAt = System.currentTimeMillis();
        pref.setCallStatCreatedAt(createdAt);
        CallStatDbOperation.createOnStartCall(
                createdAt,
                mobileNumber,
                leadId,
                scheduledAt);
    }
    private void callILom(String mobileNumber, String callingLeadId, int scheduledActivityId) {
        String callingNumber = pref.getHotlineNumber()+mobileNumber;
        if(Util.isNotNull(pref.getHotlineNumber())) {
            callingNumber = callingNumber+ Uri.encode("#");
        }
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +callingNumber));
        intent.putExtra("com.android.phone.extra.slot", 0); //For sim 1
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show();
            return;
        }
        pref.setCallingLeadId(callingLeadId);
        createCallStat(callingNumber, callingLeadId, scheduledActivityId+"");
        CleverTapPush.pushCommunicationEvent(CleverTapConstants.EVENT_COMMUNICATION_CALL, true);
        context.startActivity(intent);
    }

    private void setTagColor(ActionPlanRealmViewHolder viewHolder, String tagName, SalesCRMRealmTable actionPlanData) {
        if (tagName != null) {
            if (tagName.contains(WSConstants.LEAD_TAG_HOT)) {
                viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.hot));
            } else if (tagName.contains(WSConstants.LEAD_TAG_WARM)) {
                viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.warm));
            } else if (tagName.contains(WSConstants.LEAD_TAG_COLD)) {
                viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cold));
            } else if (tagName.contains(WSConstants.LEAD_TAG_OVERDUE)) {
                viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.overdue));

            }
        }
        //background card color
        System.out.println("TODAY:"+ Util.getTime(0) + "ACTION DATE: "+
                actionPlanData.getActivityScheduleDate() + "DIFFERENCE: "+Util.getDifferenceBetweenDates(Util.getTime(0),actionPlanData.getActivityScheduleDate()));
        if(Util.getTime(0).after(actionPlanData.getActivityScheduleDate()) || actionPlanData.getActivityScheduleDate().before(Util.getTime(0))){
            System.out.println("DIFFERENCE is:"+Util.getDifferenceBetweenDates(Util.getTime(0),actionPlanData.getActivityScheduleDate())+ "ENTERED IF");
            viewHolder.relActionPlanLeft.setBackgroundColor(ContextCompat.getColor(context, R.color.pending));
            viewHolder.relActionPlanRight.setBackgroundColor(ContextCompat.getColor(context, R.color.pending));
            viewHolder.tvActionPlanAction.setTextColor(ContextCompat.getColor(context, R.color.hot));
            viewHolder.tvActionPlanAmPm.setTextColor(ContextCompat.getColor(context, R.color.textColorCardPer));
            viewHolder.tvActionPlanTime.setTextColor(ContextCompat.getColor(context, R.color.textColorCardPer));
            viewHolder.tvActionPlanHeader.setBackgroundColor(ContextCompat.getColor(context,R.color.pending_heading));

        }else{
            int[] attrs = new int[]{R.attr.selectableItemBackground};
            TypedArray typedArray = context.obtainStyledAttributes(attrs);
            int backgroundResource = typedArray.getResourceId(0, 0);
            viewHolder.relActionPlanLeft.setBackgroundResource(backgroundResource);
            viewHolder.relActionPlanRight.setBackgroundResource(backgroundResource);
            typedArray.recycle();
            viewHolder.tvActionPlanAction.setTextColor(ContextCompat.getColor(context, R.color.textColorPrimaryLight));
            viewHolder.tvActionPlanAmPm.setTextColor(ContextCompat.getColor(context, R.color.textColorPrimaryLight));
            viewHolder.tvActionPlanTime.setTextColor(ContextCompat.getColor(context, R.color.textColorPrimaryLight));
            viewHolder.tvActionPlanHeader.setBackgroundColor(ContextCompat.getColor(context,R.color.transparent));
        }




    }

    private boolean isJustEvaluatorOrEvaluatorManager() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if ((!userRoles.get(i).getId().equalsIgnoreCase(WSConstants.ROLES.DSE) || !userRoles.get(i).getId().equalsIgnoreCase(WSConstants.ROLES.TEAM_LEAD)
                        || !userRoles.get(i).getId().equalsIgnoreCase(WSConstants.ROLES.MANAGER)|| !userRoles.get(i).getId().equalsIgnoreCase(WSConstants.ROLES.BRANCH_HEAD)
                        || !userRoles.get(i).getId().equalsIgnoreCase(WSConstants.ROLES.OPERATIONS))) {
                    if((userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR)
                            || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR_MANAGER))) {
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }
        }
        return false;
    }


    class ActionPlanRealmViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public ImageView imgCardStatus, imgMultipleOptions;
        public int currentItem;
        RelativeLayout relActionPlanLeft, relActionPlanRight, relActionPlanBottom;
        ImageButton ibControlBottom, ib360Profile, ibMail, ibReSchedule, ibCall, ibWhatsapp;
        LinearLayout llTaskAction;
        TextView tvActionPlanTime, tvActionPlanAmPm, tvActionPlanPerName, tvActionPlanMrMs, tvActionPlanAction, tvActionPlanHeader;
        ////////------------------
        TextView tvActionPlanActionStage, tvActionPlanRemarks, tvActionPlanDate, tvActionPlanDay, tvActionPlanCar, tvActionPlanAddress;
        LinearLayout llActionButtonsHolder;
        ////////-----------------
        private CardView cardActionTag;
        private LinearLayout remarksDateLinear;
        private View carViewHr, addressViewHr, remarksViewHr;

        private LinearLayout llCall, llMail, llWhatsapp;
        private TextView tvProformaBtn, tvProformaTry, tvEditPolicy;
        private TextView tvOtb;

        ActionPlanRealmViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            view = itemLayoutView;

            //------------New Layout action_plan_card

//            cardViewTag=(CardView) itemLayoutView.findViewById(R.id.card_action_plan_tag);
            tvActionPlanHeader = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_header);
            relActionPlanLeft = (RelativeLayout) itemLayoutView.findViewById(R.id.rel_action_plan_left);
            relActionPlanRight = (RelativeLayout) itemLayoutView.findViewById(R.id.rel_action_plan_right);
            relActionPlanBottom = (RelativeLayout) itemLayoutView.findViewById(R.id.rel_action_plan_bottom);
            ibControlBottom = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_bottom_control);
            ib360Profile = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_go_360);
            ibMail = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_mail);
            ibCall = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_call);
            ibReSchedule = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_reschedule);
            llTaskAction = (LinearLayout) itemLayoutView.findViewById(R.id.ll_task_action);
            tvActionPlanTime = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_time);
            tvActionPlanAmPm = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_time_am);
            tvActionPlanPerName = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_name);
            tvActionPlanMrMs = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_name_mr);
            tvActionPlanAction = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_action);
            tvActionPlanActionStage = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_action_stage);
            tvActionPlanRemarks = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_remarks);
            tvActionPlanDate = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_date);
            tvActionPlanDay = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_day);
            tvActionPlanCar = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_car);
            tvActionPlanAddress = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_address);
            llActionButtonsHolder = (LinearLayout) itemLayoutView.findViewById(R.id.ll_action_plan_bottom_bts);
            cardActionTag = (CardView) itemLayoutView.findViewById(R.id.card_action_plan_top);
            carViewHr = itemLayoutView.findViewById(R.id.view_action_plan_hr_1);
            addressViewHr = itemLayoutView.findViewById(R.id.view_action_plan_hr_2);
            remarksViewHr = itemLayoutView.findViewById(R.id.view_action_plan_hr);
            remarksDateLinear = (LinearLayout) itemLayoutView.findViewById(R.id.linearLayout);

            llCall = (LinearLayout) itemLayoutView.findViewById(R.id.ll_action_plan_call);
            llMail = (LinearLayout) itemLayoutView.findViewById(R.id.ll_action_plan_mail);
            llWhatsapp = (LinearLayout) itemLayoutView.findViewById(R.id.ll_action_plan_whatsapp);
            ibWhatsapp = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_whatsapp);
            tvProformaBtn = (TextView) itemLayoutView.findViewById(R.id.tv_proforma_btn);
            tvProformaTry = itemLayoutView.findViewById(R.id.tv_proforma_new);
            tvEditPolicy = itemLayoutView.findViewById(R.id.tv_edit_policy);
            tvOtb = (TextView) itemLayoutView.findViewById(R.id.tv_otb);


            if(DbUtils.isBike()) {
                llMail.setVisibility(View.GONE);
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        0.5f
                );
                llCall.setLayoutParams(param);
            }



            //------------

//            view.setOnClickListener(new View.OnClickListener() {
//                @Override public void onClick(View v) {
//                    mlistener.AllTaskFragmentCallBack(taskResponse.get(currentItem).getLead_id());
//                }
//            });



          /*  relActionPlanRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent filterActivity = new Intent(mActivity, CardetailsActivity.class);
                    mActivity.startActivity(filterActivity);
                }
            });*/
        }


    }
    private boolean isInternetAvailable(){
        return new ConnectionDetectorService(context).isConnectingToInternet();
    }
}
