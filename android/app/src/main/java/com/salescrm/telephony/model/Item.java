package com.salescrm.telephony.model;

import com.salescrm.telephony.utils.Util;

/**
 * Created by bharath on 9/1/17.
 */

public class Item {

    private String mText;
    private int mId;

    public Item(String text, int id) {

        this.mText = text;
        this.mId = id;
    }
    public Item(String text, String id) {

        this.mText = text;
        this.mId = Util.getInt(id);
    }

    public int getId() {

        return mId;
    }

    public void setId(String id) {

        this.mId = Integer.parseInt(id);
    }

    public String getText() {

        return mText;
    }

    public void setText(String text) {

        this.mText = text;
    }

    @Override
    public String toString() {

        return this.mText;
    }
}