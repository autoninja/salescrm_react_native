package com.salescrm.telephony.offline;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.UserCarBrands;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandModelsDB;
import com.salescrm.telephony.db.car.CarBrandsDB;
import com.salescrm.telephony.db.car.ExchangeCarBrandsDB;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.CarBrandModelVariantsResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 9/12/16.
 * Call this to get the car model and variants data
 */
public class FetchCarModelAndVariants implements Callback<CarBrandModelVariantsResponse> {
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private RealmList<UserCarBrands> carBrands;

    public FetchCarModelAndVariants(OfflineSupportListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
        realm.beginTransaction();
        realm.where(CarBrandsDB.class).findAll().deleteAllFromRealm();
        realm.where(CarBrandModelsDB.class).findAll().deleteAllFromRealm();
        realm.where(CarBrandModelVariantsDB.class).findAll().deleteAllFromRealm();
        realm.where(ExchangeCarBrandsDB.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void call() {
        UserDetails userDetails = realm.where(UserDetails.class).findFirst();
        if(userDetails != null) {
            carBrands = userDetails.getUserCarBrands();
            ArrayList<String> data = new ArrayList<>();
            for (int i = 0; i < carBrands.size(); i++) {
                data.add(carBrands.get(i).getBrand_id());
            }
            ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
            if (cd.isConnectingToInternet()) {
                ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getModelsAndVariants(data, this);
            } else {

                System.out.println("No Internet Connection");
                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);

            }

        }
        else {
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);

        }

    }

    /**
     *
     */
    private void callForExchangeCars(){
        UserDetails userDetails = realm.where(UserDetails.class).findFirst();
        if(userDetails != null) {
            ArrayList<String> data = new ArrayList<>();
            data.add("-1");
            ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
            if (cd.isConnectingToInternet()) {
                ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getModelsAndVariants(data, new Callback<CarBrandModelVariantsResponse>() {
                            @Override
                            public void success(final CarBrandModelVariantsResponse carBrandModelVariantsResponse, Response response) {
                                Util.updateHeaders(response.getHeaders());
                                if (!carBrandModelVariantsResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                    listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);

                                } else {
                                    if (carBrandModelVariantsResponse.getResult() != null) {
                                        realm.executeTransactionAsync(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm realm) {
                                                insertExchangeCarsData(realm,carBrandModelVariantsResponse);
                                            }
                                        }, new Realm.Transaction.OnSuccess() {
                                            @Override
                                            public void onSuccess() {
                                                listener.onCarModelAndVariantsFetched(carBrandModelVariantsResponse);
                                            }
                                        }, new Realm.Transaction.OnError() {
                                            @Override
                                            public void onError(Throwable error) {
                                                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);
                                            }
                                        });

                                    } else {
                                        // relLoader.setVisibility(View.GONE);
                                        System.out.println("Success:2" + carBrandModelVariantsResponse.getMessage());
                                        //showAlert(validateOtpResponse.getMessage());
                                        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);

                                    }
                                }
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);
                            }
                        });

            } else {

                System.out.println("No Internet Connection");
                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);

            }

        }
        else {
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);

        }

    }


    @Override
    public void success(final CarBrandModelVariantsResponse data, Response response) {
        Util.updateHeaders(response.getHeaders());
        if (!data.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);

        } else {
            if (data.getResult() != null) {
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        insertData(realm,data);
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        callForExchangeCars();
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);
                    }
                });

            } else {
                // relLoader.setVisibility(View.GONE);
                System.out.println("Success:2" + data.getMessage());
                //showAlert(validateOtpResponse.getMessage());
                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);

            }
        }
    }

    private void insertData(Realm realm,CarBrandModelVariantsResponse data) {
        for (int i = 0; i < data.getResult().size(); i++) {
            if (data.getResult().get(i).getBrand_id() != null) {
                realm.createOrUpdateObjectFromJson(CarBrandsDB.class, new Gson().toJson(data.getResult().get(i)));
            }
        }

    }

    private void insertExchangeCarsData(Realm realm, CarBrandModelVariantsResponse data) {
        for (int i = 0; i < data.getResult().size(); i++) {
            if (data.getResult().get(i).getBrand_id() != null) {
               // realm.createObjectFromJson(ExchangeCarBrandsDB.class, new Gson().toJson(data.getResult().get(i)));
                realm.createOrUpdateObjectFromJson(ExchangeCarBrandsDB.class, new Gson().toJson(data.getResult().get(i)));

            }
        }

    }


    @Override
    public void failure(RetrofitError error) {
        System.out.println(error.getKind().toString());
        Toast.makeText(context,"Failed fetching car details",Toast.LENGTH_LONG).show();
        listener.onOfflineSupportApiError(error, WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS);
    }

}
