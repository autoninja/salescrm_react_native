package com.salescrm.telephony.sync;

import android.content.Context;
import android.widget.Toast;

import com.salescrm.telephony.offline.AutoFetchFormData;
import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.create_lead.Activity_data;
import com.salescrm.telephony.db.create_lead.Activity_owner;
import com.salescrm.telephony.db.create_lead.Ad_car_details;
import com.salescrm.telephony.db.create_lead.Car_details;
import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;
import com.salescrm.telephony.db.create_lead.Ex_car_details;
import com.salescrm.telephony.db.create_lead.User_details;
import com.salescrm.telephony.interfaces.AutoFetchFormListener;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.interfaces.SyncListener;
import com.salescrm.telephony.model.CreateLeadInputData;
import com.salescrm.telephony.model.CreateLeadInputDataServer;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.CreateLeadResponse;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bharath on 13/12/16.
 */

public class SyncCreateLead implements FetchC360OnCreateLeadListener, AutoFetchFormListener {

    public SyncListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private SalesCRMRealmTable salesCRMRealmTable;
    private String TAG = "SyncCreateLead";
    private RealmResults<CreateLeadInputDataDB> createLeadInputDataDBList;
    private String generatedLeadId;

    public SyncCreateLead(SyncListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
        createLeadInputDataDBList = realm.where(CreateLeadInputDataDB.class).equalTo("is_synced",false).findAll();
    }
    public void sync(){
        System.out.println("CreateLeadDB size:"+createLeadInputDataDBList.size());
        if(createLeadInputDataDBList.size()>0){
            //CreateLeadInputDBList automatically refreshed
            //Getting first item means it will get the old data first and size automatically reduced
            submitCreateLead(createLeadInputDataDBList.get(0));
        }
        else {
            listener.onCreateLeadSync();
        }


    }

    private void submitCreateLead(final CreateLeadInputDataDB createLeadInputDataDB) {
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).CreateLead(getCreateLeadInputData(createLeadInputDataDB), new Callback<CreateLeadResponse>() {
            @Override
            public void success(CreateLeadResponse o, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }
                if (!o.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    System.out.println("Success:0" + o.getMessage());
                    updateSyncStatus(null, createLeadInputDataDB,false,"Failed");
                    //showAlert(validateOtpResponse.getMessage());
                } else {
                    if (o.getResult() != null) {
                        System.out.println("Success:1" + o.getMessage());
                        System.out.println("Lead Id:" + o.getResult());
                        Toast.makeText(context, "Lead has been created : "+o.getResult(), Toast.LENGTH_SHORT).show();
                        updateSyncStatus(o.getResult(),createLeadInputDataDB,true,"Success");

                    } else {

                        updateSyncStatus(null, createLeadInputDataDB,false,"Failed");
                            // relLoader.setVisibility(View.GONE);
                        System.out.println("Success:2" + o.getMessage());
                            //showAlert(validateOtpResponse.getMessage());

                    }
                }

            }

            @Override
            public void failure(RetrofitError error) {
                updateSyncStatus(null, createLeadInputDataDB,false,"Failed");
                System.out.println(error.toString());


            }

        });
    }

    private CreateLeadInputDataServer getCreateLeadInputData(CreateLeadInputDataDB db) {

        CreateLeadInputDataServer inputData = new CreateLeadInputDataServer();
        CreateLeadInputDataServer.Lead_data lead_data = inputData.new Lead_data();


        //Customer Details
        CreateLeadInputDataServer.Lead_data.Customer_details customer_details = lead_data.new Customer_details();
        customer_details.setGender(db.getLead_data().getCustomer_details().getGender());
        customer_details.setTitle(db.getLead_data().getCustomer_details().getTitle());
        customer_details.setFirst_name(db.getLead_data().getCustomer_details().getFirst_name());
        customer_details.setLast_name(db.getLead_data().getCustomer_details().getLast_name());
        customer_details.setAge(String.valueOf(db.getLead_data().getCustomer_details().getAge()));
            customer_details.setOffice_address(db.getLead_data().getCustomer_details().getOffice_address());
            customer_details.setOffice_city(db.getLead_data().getCustomer_details().getOffice_city());
            customer_details.setOffice_pin_code(db.getLead_data().getCustomer_details().getOffice_pin_code());
            customer_details.setResidence_address(db.getLead_data().getCustomer_details().getResidence_address());
            customer_details.setCity(db.getLead_data().getCustomer_details().getCity());
            customer_details.setPin_code(db.getLead_data().getCustomer_details().getPin_code());
        customer_details.setOccupation(db.getLead_data().getCustomer_details().getOccupation());
        customer_details.setCompany_name(db.getLead_data().getCustomer_details().getCompany_name());

        //Set Customer details
        lead_data.setCustomer_details(customer_details);

        //Mobile Numbers
        List<CreateLeadInputData.Lead_data.Mobile_numbers> mobile_numbers = new ArrayList<>();

        for(int i=0;i<db.getLead_data().getMobile_numbers().size();i++){
            CreateLeadInputData.Lead_data.Mobile_numbers mobile_number =  new CreateLeadInputData().new Lead_data().new Mobile_numbers();
            mobile_number.setIs_primary(db.getLead_data().getMobile_numbers().get(i).getIs_primary());
            mobile_number.setNumber(db.getLead_data().getMobile_numbers().get(i).getNumber());

            mobile_numbers.add(mobile_number);
        }
        lead_data.setMobile_numbers(mobile_numbers);

        System.out.println("Customer Mobile Number");
        //Email Ids
        List<CreateLeadInputData.Lead_data.Emails> emailsList = new ArrayList<>();

        for(int i=0;i<db.getLead_data().getEmails().size();i++){
            CreateLeadInputData.Lead_data.Emails email =   new CreateLeadInputData().new Lead_data().new  Emails();
            email.setIs_primary(db.getLead_data().getEmails().get(i).getIs_primary());
            email.setAddress(db.getLead_data().getEmails().get(i).getAddress());

            emailsList.add(email);
        }
        lead_data.setEmails(emailsList);
        //Customer Details --------------------------------------------------


        //Lead Details Screen
        lead_data.setLead_enq_date(db.getLead_data().getLead_enq_date());
        lead_data.setLead_source_id(db.getLead_data().getLead_source_id());
        lead_data.setLead_source_category_id(db.getLead_data().getLead_source_category_id());
        lead_data.setRef_vin_reg_no(db.getLead_data().getRef_vin_reg_no());
        lead_data.setExp_closing_date(db.getLead_data().getExp_closing_date());
        lead_data.setMode_of_pay(db.getLead_data().getMode_of_pay());
        //Lead Details Screen------------------------------------------------

        //Car Details

        //NewCar
        RealmList<Car_details> newCarDB = db.getLead_data().getCar_details();
        List<CreateLeadInputDataServer.Lead_data.Car_details> newCarList = new ArrayList<>();

        for(int i=0;i<newCarDB.size();i++){
            CreateLeadInputDataServer.Lead_data.Car_details currentCar = lead_data.new Car_details();
            currentCar.setIs_activity_target(newCarDB.get(i).getIs_activity_target());
            currentCar.setIs_primary(newCarDB.get(i).getIs_primary());
            currentCar.setColor_id(newCarDB.get(i).getColor_id());
            currentCar.setFuel_type_id(newCarDB.get(i).getFuel_type_id());
            currentCar.setModel_id(newCarDB.get(i).getModel_id());
            currentCar.setVariant_id(newCarDB.get(i).getVariant_id());
            newCarList.add(currentCar);
        }
        lead_data.setCar_details(newCarList);

        //ExCar
        RealmList<Ex_car_details> exCarListDB = db.getLead_data().getEx_car_details();
        List<CreateLeadInputDataServer.Lead_data.Ex_car_details> exCarList = new ArrayList<>();

        for(int i=0;i<exCarListDB.size();i++){
            CreateLeadInputDataServer.Lead_data.Ex_car_details currentCar = lead_data.new Ex_car_details();
            currentCar.setIs_activity_target(exCarListDB.get(i).is_activity_target());
            currentCar.setIs_primary(exCarListDB.get(i).getIs_primary());
            currentCar.setMilage(exCarListDB.get(i).getMilage());
            currentCar.setKms_run(exCarListDB.get(i).getKms_run());
            currentCar.setModel_id(exCarListDB.get(i).getModel_id());
            currentCar.setPurchase_date(exCarListDB.get(i).getPurchase_date());
            exCarList.add(currentCar);
        }
        lead_data.setEx_car_details(exCarList);



        //AddCar
        RealmList<Ad_car_details> addCarListDB = db.getLead_data().getAd_car_details();
        List<CreateLeadInputDataServer.Lead_data.Ad_car_details> addCarList = new ArrayList<>();

        for(int i=0;i<addCarListDB.size();i++){
            CreateLeadInputDataServer.Lead_data.Ad_car_details currentCar = lead_data.new Ad_car_details();
            currentCar.setIs_activity_target(addCarListDB.get(i).is_activity_target());
            currentCar.setIs_primary(addCarListDB.get(i).getIs_primary());
            currentCar.setMilage(addCarListDB.get(i).getMilage());
            currentCar.setKms_run(addCarListDB.get(i).getKms_run());
            currentCar.setModel_id(addCarListDB.get(i).getModel_id());
            currentCar.setPurchase_date(addCarListDB.get(i).getPurchase_date());
            addCarList.add(currentCar);
        }
        lead_data.setAd_car_details(addCarList);
        //Car Details--------------------------------------------------------


        //Assign Lead to Dse
        if(db.getLead_data().getLocation_id()!=null&&db.getLead_data().getLocation_id()>0){
            lead_data.setLocation_id(db.getLead_data().getLocation_id());
        }
        System.out.println(lead_data.getLocation_id());

        RealmList<User_details> user_detailsDB = db.getLead_data().getUser_details();
        List<CreateLeadInputData.Lead_data.User_details> user_detailsList = new ArrayList<>();
        for (int i=0;i<user_detailsDB.size();i++){
            CreateLeadInputData.Lead_data.User_details cUser_details = new CreateLeadInputData().new Lead_data().new User_details();
            cUser_details.setRole_id(user_detailsDB.get(i).getRole_id());
            cUser_details.setUser_id(user_detailsDB.get(i).getUser_id());
            user_detailsList.add(cUser_details);
        }
        lead_data.setUser_details(user_detailsList);
        lead_data.setLead_tag_id(db.getLead_data().getLead_tag_id());
        //Assign Lead to Dse-------------------------------------------------


        //Activity

        Activity_data activity_dataDB = db.getActivity_data();
       if(activity_dataDB!=null){
           CreateLeadInputData.Activity_data activity_data = new CreateLeadInputData(). new Activity_data();

           Activity_owner activity_owner_db = activity_dataDB.getActivity_owner();
           if(activity_owner_db!=null){
               CreateLeadInputData.Activity_data.Activity_owner activity_owner = activity_data. new Activity_owner();
               activity_owner.setRole_id(activity_owner_db.getRole_id());
               activity_owner.setUser_id(activity_owner_db.getUser_id());
               activity_data.setActivity_owner(activity_owner);
           }
           activity_data.setVisit_type(activity_dataDB.getVisit_type());
           activity_data.setScheduledDateTime(activity_dataDB.getScheduledDateTime());
           activity_data.setActivity(activity_dataDB.getActivity());
           activity_data.setAddress(activity_dataDB.getAddress());
           activity_data.setVisit_time(activity_dataDB.getVisit_time());
           activity_data.setLocation_id(activity_dataDB.getLocation_id());
           inputData.setActivity_data(activity_data);
       }

        //Activity-----------------------------------------------------------

        //remarks
        inputData.setRemarks(db.getRemarks());
        //
        //Final Lead_Data
        inputData.setLead_data(lead_data);
        return inputData;

    }

    private void updateSyncStatus(String result, final CreateLeadInputDataDB data, boolean b, String error) {
        realm.beginTransaction();
        data.setIs_synced(true);
        System.out.println("Sc id:+"+data.getScheduledActivityId());
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("scheduledActivityId",data.getScheduledActivityId()).findFirst();
        if(salesCRMRealmTable!=null) {
            salesCRMRealmTable.setSync(true);
        }
        realm.commitTransaction();
        if(result!=null){
            fetchC360(result,data);
        }
        else {
            sync();
        }


    }

    private void fetchC360(String leadId, CreateLeadInputDataDB createLeadInputData) {
        this.generatedLeadId = leadId;
        boolean fetchActionPlanFirst=false;
        if(createLeadInputData.getActivity_data()==null
                ||createLeadInputData.getActivity_data().getActivity()==null
                ||createLeadInputData.getActivity_data().getActivity().equalsIgnoreCase("")){
            fetchActionPlanFirst =true;
        }
        System.out.println("Fetch First"+(fetchActionPlanFirst));
        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(leadId);
        new FetchC360InfoOnCreateLead(this,context,leadData,pref.getAppUserId()).call(fetchActionPlanFirst);

    }

    @Override
    public void onFetchC360OnCreateLeadSuccess() {
        new AutoFetchFormData(this,context).call(realm.where(SalesCRMRealmTable.class).equalTo("leadId",Util.getInt(generatedLeadId)).findAll(),true);
    }

    @Override
    public void onFetchC360OnCreateLeadError() {
        System.out.println(TAG+"Failed");
        sync();

    }
    @Override
    public void onAutoFormsDataFetched(boolean b, RealmResults<SalesCRMRealmTable> data) {
        Toast.makeText(context, "Lead Created", Toast.LENGTH_LONG).show();
        sync();
    }

    @Override
    public void onAutoFormsDataError(RetrofitError error, int from) {
       sync();
    }





}
