import React, { Component } from 'react'
import { Alert, View, Text, Image, } from 'react-native'
import styles from '../styles/styles'
import GameIntroWelcome from './game_intro_welcome'
import GameIntroTeam from './game_intro_team'
import GameIntroSC from './game_intro_sc'
import GameIntroPoints from './game_intro_points'
import GameIntroStart from './game_intro_start'

import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation'
import { StackActions, NavigationActions } from 'react-navigation';
import UserInfoService from '../storage/user_info_service'
import GamificationConfigurationService from '../storage/gamification_configuration_service'
import * as async_storage from '../storage/async_storage'

export default class GameIntro extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentScreen: 0,
    }
  }

  startGame() {
    async_storage.setGameIntroSeen("1");
    this.props.navigation.dispatch(StackActions.reset({
      index: 0, // <-- currect active route from actions array
      key: null,
      actions: [
        NavigationActions.navigate({ routeName: 'Home' }),
      ],
    }));
  }
  getViewBottom(screen) {
    var views = [];
    for (i = 0; i < 5; i++) {
      backgroundColor = "rgba(255, 255, 255, 0.7)";
      if (i == screen) {
        backgroundColor = "#fff"
      }
      views.push(<View key={i + ""} style={[{ backgroundColor: backgroundColor, marginLeft: 10 }, styles.circle]} />);
    }
    return views;
  }
  gameTabs() {

    let userInfo = UserInfoService.findFirst();
    let dealername = "";
    let bestTeam = "Be the Best Team";
    if (userInfo) {
      dealername = userInfo.dealer;
      if (dealername && dealername.length > 0) {
        dealername = dealername.substr(0, 1).toUpperCase() + dealername.split(" ")[0].substr(1);
      }
    }

    let gameConf = GamificationConfigurationService.getLocations();
    if (gameConf.length > 0) {
      bestTeam = bestTeam + " at\n" + gameConf[Math.floor((Math.random() * gameConf.length))].name;
    }
    let payload = { dealername, bestTeam }


    var order = ['GameIntroWelcome', 'GameIntroTeam', 'GameIntroSC', 'GameIntroPointSystem', 'GameIntroLetsStart'];
    return createAppContainer(createMaterialTopTabNavigator({
      GameIntroWelcome: {
        screen: props => <GameIntroWelcome payload={payload} bottomViews={this.getViewBottom(0)} />,
        navigationOptions: ({ navigation }) => ({
          tabBarVisible: false
        }),
      },
      GameIntroTeam: {
        screen: props => <GameIntroTeam payload={payload} bottomViews={this.getViewBottom(1)} />,
        navigationOptions: ({ navigation }) => ({
          tabBarVisible: false
        }),
      },
      GameIntroSC: {
        screen: props => <GameIntroSC bottomViews={this.getViewBottom(2)} />,
        navigationOptions: ({ navigation }) => ({
          tabBarVisible: false
        }),
      },
      GameIntroPointSystem: {
        screen: props => <GameIntroPoints bottomViews={this.getViewBottom(3)} />,
        navigationOptions: ({ navigation }) => ({
          tabBarVisible: false
        }),
      },
      GameIntroLetsStart: {
        screen: props => <GameIntroStart bottomViews={this.getViewBottom(4)} startGame={this.startGame.bind(this)} />,
        navigationOptions: ({ navigation }) => ({
          tabBarVisible: false
        }),
      },

    }, {
      tabBarContainer: { flexDirection: 'row', justifyContent: 'right', alignItems: 'center' },
      initialRouteName: order[this.state.currentScreen],
      order,

    }));
  }
  render() {
    const GAME_TABS = this.gameTabs();
    //const VIEW_BOTTOM = this.getViewBottom();
    return (

      <View style={styles.MainContainer}>

        <View style={{ flex: 1 }}>
          <GAME_TABS />
        </View>
      </View>
    )
  }
}
