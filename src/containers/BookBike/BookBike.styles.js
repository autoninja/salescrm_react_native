import { StyleSheet } from "react-native";
export default styles = StyleSheet.create({
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.8)',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30

  },
  main: {
    backgroundColor: '#fff',
    borderRadius: 5,
    width: '100%',
    maxHeight: '90%',
    paddingBottom: 20,
  },
  title: {
    fontSize: 18,
    color: '#303030',
    fontWeight: 'bold'
  },
  listItem: {

    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
    fontSize: 16,
    color: '#303030'
  },
  line: {
    backgroundColor: '#e0e0e0',
    height: 1,
  },
  lineMain: {
    marginTop: 15,
    backgroundColor: '#e0e0e0',
    height: 1,
    borderRadius: 5,
  },
  textHeader: {
    color: "#303030",
    fontSize: 16,
    fontWeight: "bold",
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 10
  }
});
