package com.salescrm.telephony.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.views.RangeBarVertical;

import java.util.Locale;

/**
 * Created by Ravindra P on 03-06-2016.
 */
public class FilterEnquiryAgeFragment extends Fragment implements RangeBarVertical.OnRangeBarChangeListener {

    private static final Integer FILTER_ENQUIRY_AGE_POS = 4;
    TextView tvMin, tvMax;
    FilterSelectionHolder selectionHolder;
    private View rootView;
    private ListView listView;
    private int lastSelectedListItem = 0;
    private RangeBarVertical rangeBarVertical;
    private RelativeLayout relativeLayout;
    private SparseArray<String> selectedValues;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.filter_enquiry_age_fragment, container, false);
        selectionHolder = FilterSelectionHolder.getInstance();

        tvMin = (TextView) rootView.findViewById(R.id.tv_filter_age_min);
        tvMax = (TextView) rootView.findViewById(R.id.tv_filter_age_max);
        relativeLayout = (RelativeLayout) rootView.findViewById(R.id.rel_filter_enq_age_parent);
        rangeBarVertical = new RangeBarVertical(getContext(), this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, tvMin.getId());
        params.addRule(RelativeLayout.ABOVE, tvMax.getId());
        relativeLayout.addView(rangeBarVertical, params);
        selectedValues = selectionHolder.getMapData(FILTER_ENQUIRY_AGE_POS);
        if (selectedValues == null) {
            selectedValues = new SparseArray<>();
        } else {
            //For minimum 0 as key
            if (selectedValues.get(0) != null) {
                System.out.println("CalledMin");
                rangeBarVertical.setMinimumProgress(Util.getInt(selectedValues.get(0)));
                tvMin.setText(String.format(Locale.getDefault(), "%s Days", selectedValues.get(0)));
            }

            //For max 1 as key
            if (selectedValues.get(1) != null) {
                System.out.println("CalledMax");
                rangeBarVertical.setMaximumProgress(Util.getInt(selectedValues.get(1)));
                tvMax.setText(String.format(Locale.getDefault(), "%s Days", selectedValues.get(1)));
            }

        }
        return rootView;

    }

    @Override
    public void onRangeBarChange(int min, int max) {

        selectedValues.put(0, min + "");
        selectedValues.put(1, max + "");
        selectionHolder.setMapData(FILTER_ENQUIRY_AGE_POS, selectedValues);

        tvMin.setText(String.format(Locale.getDefault(), "%d Days", min));
        tvMax.setText(String.format(Locale.getDefault(), "%d Days", max));
    }
}
