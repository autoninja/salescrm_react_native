package com.salescrm.telephony.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.FabricUtils;
import com.salescrm.telephony.utils.WSConstants;

import org.w3c.dom.Document;

import java.io.File;

/**
 * Created by prateek on 9/4/18.
 */

public class WhatsappActivity extends AppCompatActivity {
    private String phone = "";
    private Button btnSend;
    private TextView tvMobileNumber;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private TextView tvAttachment;
    private ImageView imageViewCancelAttachment;
    private LinearLayout attachmentLayout;
    private EditText edtMessage;
    private int DOC_SELECTED = 191;
    private int IMAGE_SELECTED = 181;
    private Intent intent;
    private Uri fileUri = null;
    //private Uri imageUri = null;
    private Toolbar toolbar;
    private TextView customTitle;
    private Preferences pref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.whatsapp_activity);
        pref = Preferences.getInstance();
        pref.load(this);
        toolbar = (Toolbar) findViewById(R.id.whatsapp_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        customTitle = (TextView) findViewById(R.id.custom_title);
        customTitle.setText("Whatsapp msg");
        btnSend = (Button) findViewById(R.id.btn_send);
        tvMobileNumber = (TextView) findViewById(R.id.mobile_number);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group_whatsapp);
        tvAttachment = (TextView) findViewById(R.id.tv_attachment);
        imageViewCancelAttachment = (ImageView) findViewById(R.id.img_attachment_cancel);
        attachmentLayout = (LinearLayout) findViewById(R.id.attachment_layout);
        edtMessage = (EditText) findViewById(R.id.edt_message);
        Drawable drawable = VectorDrawableCompat
                .create(this.getResources(), R.drawable.ic_attachment, null);
        tvAttachment.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        tvAttachment.setHint("Tap to attach!!");
        tvAttachment.setCompoundDrawablePadding(2);
        //default checked radio
        if(getIntent().getStringExtra("phono")!= null){
            phone = getIntent().getStringExtra("phono");
        }
        tvMobileNumber.setText(phone);
        radioGroup.check(R.id.rb_text);
        ((TextView) findViewById(R.id.rb_text)).setTextColor(Color.parseColor("#2196f3"));

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioButton = (RadioButton) findViewById(checkedId);
                switch (checkedId){
                    case (R.id.rb_doc):
                        fileUri = null;
                        docAssetVisibility();
                        break;
                    case (R.id.rb_img):
                        fileUri = null;
                        imageAssetVisibilty();
                        break;
                    case (R.id.rb_text):
                        textAssetVisibilty();
                        break;
                }
            }
        });
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioButton = (RadioButton) findViewById(selectedId);

                if(radioButton != null){
                    switch (radioButton.getId()){
                        case (R.id.rb_doc):
                            //toastMessage("Document clicked");
                            if(fileUri!=null) {
                                sendDoc();
                            }else {
                                toastMessage("File not selected");
                            }
                            break;
                        case (R.id.rb_img):
                            //toastMessage("Image clicked");
                            if(fileUri != null) {
                                sendImage();
                            }else {
                                toastMessage("Image not selected");
                            }
                            break;
                        case (R.id.rb_text):
                            //toastMessage("Text clicked");
                            if(edtMessage.getText()!= null && !edtMessage.getText().toString().equalsIgnoreCase("")) {
                                sendText();
                            }else {
                                toastMessage("Please write a message to send!!");
                            }
                            break;
                    }
                }else {
                    toastMessage("Please select type of message!!");
                }

                //sendText();
            }
        });

        imageViewCancelAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvAttachment.setText("");
                tvAttachment.setHint("Tap to attach!!");
                tvAttachment.setVisibility(View.VISIBLE);
                imageViewCancelAttachment.setVisibility(View.INVISIBLE);
                fileUri = null;
            }
        });

        tvAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(Intent.ACTION_GET_CONTENT);
                switch (radioButton.getId()){
                    case (R.id.rb_doc):
                        intent.setType("file/*");
                        startActivityForResult(intent, DOC_SELECTED);
                        break;
                    case (R.id.rb_img):
                        intent.setType("image/*");
                        startActivityForResult(intent, IMAGE_SELECTED);
                        break;
                }
            }
        });
    }
    private void textAssetVisibilty() {
        ((TextView) findViewById(R.id.rb_text)).setTextColor(Color.parseColor("#2196f3"));
        ((TextView) findViewById(R.id.rb_img)).setTextColor(Color.parseColor("#000000"));
        ((TextView) findViewById(R.id.rb_doc)).setTextColor(Color.parseColor("#000000"));
        attachmentLayout.setVisibility(View.GONE);
        edtMessage.setVisibility(View.VISIBLE);
    }

    private void imageAssetVisibilty() {
        ((TextView) findViewById(R.id.rb_text)).setTextColor(Color.parseColor("#000000"));
        ((TextView) findViewById(R.id.rb_img)).setTextColor(Color.parseColor("#2196f3"));
        ((TextView) findViewById(R.id.rb_doc)).setTextColor(Color.parseColor("#000000"));
        edtMessage.setVisibility(View.VISIBLE);
        attachmentLayout.setVisibility(View.VISIBLE);
        imageViewCancelAttachment.setVisibility(View.INVISIBLE);
        tvAttachment.setVisibility(View.VISIBLE);
        tvAttachment.setText("");
    }

    private void docAssetVisibility() {
        ((TextView) findViewById(R.id.rb_text)).setTextColor(Color.parseColor("#000000"));
        ((TextView) findViewById(R.id.rb_img)).setTextColor(Color.parseColor("#000000"));
        ((TextView) findViewById(R.id.rb_doc)).setTextColor(Color.parseColor("#2196f3"));
        attachmentLayout.setVisibility(View.VISIBLE);
        imageViewCancelAttachment.setVisibility(View.INVISIBLE);
        tvAttachment.setVisibility(View.VISIBLE);
        tvAttachment.setText("");
        edtMessage.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && data.getData()!= null) {
            if (requestCode == DOC_SELECTED) {
                fileUri = data.getData();
                tvAttachment.setVisibility(View.VISIBLE);
                tvAttachment.setText(""+fileName(fileUri));
                imageViewCancelAttachment.setVisibility(View.VISIBLE);

            } else if (requestCode == IMAGE_SELECTED) {
                fileUri = data.getData();
                tvAttachment.setVisibility(View.VISIBLE);
                tvAttachment.setText(""+fileName(fileUri));
                imageViewCancelAttachment.setVisibility(View.VISIBLE);
            }
        }
    }

    private String fileName(Uri fileUri) {
        File myFile = new File(this.fileUri.toString());
        String path = myFile.getAbsolutePath();
        String displayName = null;
        if (this.fileUri.toString().startsWith("content://")) {
            Cursor cursor = null;
            try {
                cursor = this.getContentResolver().query(this.fileUri, null, null, null, null);
                if (cursor != null && cursor.moveToFirst()) {
                    displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        } else if (fileUri.toString().startsWith("file://")) {
            displayName = myFile.getName();
        }
        return  displayName;
    }

    private void sendText() {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        System.out.println("phone number: "+phone);
        whatsappIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("91"+tvMobileNumber.getText()) + "@s.whatsapp.net");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, ""+edtMessage.getText());
        try {
            startActivity(whatsappIntent);
            edtMessage.setText("");
            WhatsappActivity.this.finish();
            cleverTapPush("Text");
        } catch (android.content.ActivityNotFoundException ex) {
            toastMessage("Whatsapp not available, Please install!!");
        }
    }

    private void sendDoc(){
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("*/*");
        whatsappIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("91"+tvMobileNumber.getText()) + "@s.whatsapp.net");
        whatsappIntent.setPackage("com.whatsapp");
        //whatsappIntent.putExtra(Intent.EXTRA_SUBJECT, "My Pdf");
        //whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Hello there, i'm sending messages across!!");
        whatsappIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        try {
            startActivity(whatsappIntent);
            WhatsappActivity.this.finish();
            cleverTapPush("Doc");
        } catch (android.content.ActivityNotFoundException ex) {
            toastMessage("Whatsapp not available, Please install!!");
        }
    }

    private void sendImage(){
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("image/*");
        whatsappIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("91"+tvMobileNumber.getText()) + "@s.whatsapp.net");
        whatsappIntent.setPackage("com.whatsapp");
        //whatsappIntent.putExtra(Intent.EXTRA_SUBJECT, "My Pdf");
        if(edtMessage.getText()!= null && !edtMessage.getText().toString().equalsIgnoreCase("")) {
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, "" + edtMessage.getText());
        }
        whatsappIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        try {
            startActivity(whatsappIntent);
            edtMessage.setText("");
            WhatsappActivity.this.finish();
            cleverTapPush("Image");
        } catch (android.content.ActivityNotFoundException ex) {
            toastMessage("Whatsapp not available, Please install!!");
        }
    }
    private void cleverTapPush(String type){
        boolean isFromMyTask = getIntent().getBooleanExtra("from_my_task", false);
        CleverTapPush.pushCommunicationEvent(CleverTapConstants.EVENT_COMMUNICATION_WHATSAPP, isFromMyTask, type);

    }

    private void toastMessage(String toast){
        Toast.makeText(WhatsappActivity.this,
                toast, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish(); // close this activity and return to previous activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
