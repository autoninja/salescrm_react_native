package com.salescrm.telephony.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.model.ShareWhatsappLog;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.FileDownloader;
import com.salescrm.telephony.utils.Util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by prateek on 27/5/18.
 */

public class ProformaPdfActivity extends AppCompatActivity implements View.OnClickListener {

    private WebView webViewProforma;
    private ProgressBar progressBarProforma;
    private TextView tvWhatsapp, tvEmail;
    private String phone = "";
    private String customerName = "";
    private String fileLink = "";
    private Preferences pref;
    private Toolbar toolbar;
    private TextView customTitle;
    private ImageView btnRefresh;
    private static ProformaPdfActivity proformaPdfActivityInstance;
    private LinearLayout llButtonContainer;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish(); // close this activity and return to previous activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!fileLink.equalsIgnoreCase("")) {
            webViewProforma.loadUrl("https://docs.google.com/gview?embedded=true&url=" + fileLink);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(pref.isProformaSent() && !getIntent().getBooleanExtra("from_logs", false)) {
            ProformaInvoiceActivity.getInstance().finish();
        }
    }

    public static ProformaPdfActivity getInstance(){
        return  proformaPdfActivityInstance;
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.proforma_pdf_activity);
        pref = Preferences.getInstance();
        pref.load(this);
        toolbar = (Toolbar) findViewById(R.id.preview_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        customTitle = (TextView) findViewById(R.id.custom_title);
        customTitle.setText("Invoice Preview");
        webViewProforma = (WebView) findViewById(R.id.webviewproforma);
        progressBarProforma = (ProgressBar) findViewById(R.id.progressbarproforma);
        tvWhatsapp = (TextView) findViewById(R.id.tv_whatsapp);
        tvEmail = (TextView) findViewById(R.id.tv_email);
        btnRefresh = (ImageView) findViewById(R.id.img_refresh);
        llButtonContainer = (LinearLayout) findViewById(R.id.ll_buttons_container);
        tvEmail.setOnClickListener(this);
        tvWhatsapp.setOnClickListener(this);
        btnRefresh.setOnClickListener(this);
        webViewProforma.getSettings().setJavaScriptEnabled(true);
        proformaPdfActivityInstance = this;
        webViewProforma.getSettings().setDisplayZoomControls(false);
        webViewProforma.getSettings().setBuiltInZoomControls(true);
        webViewProforma.getSettings().setSupportZoom(true);
        fileLink = pref.getProformaPdfLink() != null? pref.getProformaPdfLink():"";

        if(getIntent().getBooleanExtra("from_logs", false)){
            llButtonContainer.setVisibility(View.GONE);
        }else{
            llButtonContainer.setVisibility(View.VISIBLE);
            phone = getIntent().getStringExtra("phone") != null ? getIntent().getStringExtra("phone"): "";
            customerName = getIntent().getStringExtra("customer_name")!= null ?getIntent().getStringExtra("customer_name"): "";
        }

        webViewProforma.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                view.loadUrl("javascript:(function() { " +
                        "document.querySelector('[role=\"toolbar\"]').remove();})()");
                progressBarProforma.setVisibility(View.GONE);
                //swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                view.loadUrl("javascript:(function() { " +
                        "document.querySelector('[role=\"toolbar\"]').remove();})()");
            }
        });

        downloadPdf();
    }

    private void downloadPdf() {

        try {
            new DownloadFile().execute(fileLink, "ProformaInvoice.pdf");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/proformapdf/" + "ProformaInvoice.pdf");
        switch (v.getId()){
            case R.id.tv_whatsapp:
                if(pdfFile != null) {
                    shareWhatsappLog();
                    if (!phone.equalsIgnoreCase("")) {
                        if (Util.checkContactAvailable(phone, this)) {
                            sendFile(pdfFile);
                        } else {
                            Uri uri = FileProvider.getUriForFile(this, this.getPackageName() + ".provider", pdfFile);
                            Intent intent = new Intent(this, AddToContactsActivity.class);
                            intent.putExtra("phono", phone);
                            intent.putExtra("customer_name", customerName);
                            intent.putExtra("from_proforma", true);
                            intent.putExtra("pdf_file", uri.toString());
                            this.startActivity(intent);
                        }

                    } else {
                        showToast("Phone number not available");
                    }
                }else {
                    showToast("Could not download pdf");
                }
                break;
            case R.id.tv_email:
                    if(pdfFile != null) {
                        emailInvoicePdf(pdfFile);
                    }else {
                        showToast("Could not download pdf");
                    }
                break;

            case R.id.img_refresh:
                if(!fileLink.equalsIgnoreCase("")) {
                    progressBarProforma.setVisibility(View.VISIBLE);
                    webViewProforma.loadUrl("https://docs.google.com/gview?embedded=true&url=" + fileLink);
                }else {
                    showToast("Pdf link not received");
                }
        }
    }

    private void shareWhatsappLog() {
        ShareWhatsappLog shareWhatsappLog = new ShareWhatsappLog();
        shareWhatsappLog.setLead_id(pref.getLeadID());
        shareWhatsappLog.setVia("WHATSAPP");
        shareWhatsappLog.setTo("+91-"+phone);
        shareWhatsappLog.setPdf_url(fileLink);
        shareWhatsappLog.setTimestamp(""+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        System.out.println("shareWhatsappLog: "+ new Gson().toJson(shareWhatsappLog).toString());
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).shareLog(shareWhatsappLog, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void emailInvoicePdf(File pdfFile) {
        if(pdfFile != null) {
            Uri uri = Uri.fromFile(pdfFile);
            Intent intentEmail = new Intent(this, EmailSmsActivity.class);
            intentEmail.putExtra("from_proforma", true);
            intentEmail.putExtra("pdf_file", uri.toString());
            startActivity(intentEmail);
        }
    }

    private void sendFile(File pdfFile) {
        if(pdfFile != null) {
           // Uri uri = Uri.fromFile(pdfFile);
            Uri uri = FileProvider.getUriForFile(this, this.getPackageName() + ".provider", pdfFile);
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("*/*");
            whatsappIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("91" + phone) + "@s.whatsapp.net");
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_STREAM, uri);
            whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put(CleverTapConstants.EVENT_PROFORMA_INVOICE_KEY_TYPE,
                        CleverTapConstants.EVENT_PROFORMA_INVOICE_TYPE_SENT);
                hashMap.put(CleverTapConstants.EVENT_PROFORMA_INVOICE_KEY_SENT_TYPE,
                        CleverTapConstants.EVENT_PROFORMA_INVOICE_SENT_TYPE_WHATSAPP);
                CleverTapPush.pushEvent(CleverTapConstants.EVENT_PROFORMA_INVOICE, hashMap);
                startActivity(whatsappIntent);
                pref.setProformaSent(true);
                ProformaPdfActivity.this.finish();
            } catch (android.content.ActivityNotFoundException ex) {
                showToast("Whatsapp not available, Please install!!");
            }
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "proformapdf");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;
        }
    }

    public void showToast(String message){
        Util.showToast(ProformaPdfActivity.this, message, Toast.LENGTH_SHORT);
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
