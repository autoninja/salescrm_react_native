import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback,
} from 'react-native'

const styles = StyleSheet.create({
    manager_circle: {
        alignSelf: "center",
        width: 36, height: 36, borderRadius: 36 / 2,
        alignItems: 'center', justifyContent: 'center'
    },
})

export default class UserDp extends Component {
    constructor(props) {
        super(props);
        this.state = { showImagePlaceHolder: false };
    }

    render() {
        // if (this.props.name === 'chetan') {
        //     console.error(this.props.name, this.props.image_url)
        // }
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                {(this.state.showImagePlaceHolder || this.props.image_url == null) ?
                    <View style={[styles.manager_circle, { backgroundColor: this.props.bgcolor }]}>
                        <Text style={{ color: '#fff' }}>{this.props.name.charAt(0).toUpperCase()}</Text>
                    </View>

                    :
                    <Image
                        source={{ uri: this.props.image_url }}
                        onError={() => { this.setState({ showImagePlaceHolder: true }) }}
                        style={{ width: 36, height: 36, borderRadius: 36 / 2, }} />}
            </View>
        )
    }

}
