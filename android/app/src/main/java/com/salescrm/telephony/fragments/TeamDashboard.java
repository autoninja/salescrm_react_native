package com.salescrm.telephony.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.TeamDashboardPagerAdapter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.model.TeamDashboardSwiped;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.WSConstants;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import io.realm.Realm;

/**
 * Created by prateek on 6/6/17.
 */

public class TeamDashboard extends Fragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TeamDashboardPagerAdapter teamDashboradPagerAdapter;
    private int prevTab = -1;
    private Preferences pref;
    private int locationsSize = 0;
    private Realm realm;
    private Intent intentAddevent = new Intent("show_addEventsButtonForBroadcastReceiver");

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.team_dashboard, container, false);
        realm = Realm.getDefaultInstance();
        viewPager = (ViewPager) rootView.findViewById(R.id.dashboardviewpager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout_team_dashboard);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        pref = Preferences.getInstance();
        pref.load(getContext());
        setDate();
        //not using this Etvbr_Called_once, but dont clean it
        //WSConstants.ETVBR_CALLED_ONCE = false;
        /* new PendingDetailsFragment().fetchPendingStats(getActivity(),false);*/

        /*if(getArguments()!= null && getArguments().getBoolean("is_team_lead")){
            tabLayout.addTab(tabLayout.newTab().setText("ETVBR"));
            tabLayout.addTab(tabLayout.newTab().setText("Today's Tasks"));
            tabLayout.setVisibility(View.VISIBLE);
            teamDashboradPagerAdapter = new TeamDashboradPagerAdapter(getChildFragmentManager(), 2);
        }else {
            tabLayout.addTab(tabLayout.newTab().setText("ETVBR"));
            tabLayout.setVisibility(View.GONE);
            teamDashboradPagerAdapter = new TeamDashboradPagerAdapter(getChildFragmentManager(), 1);
        }*/
        locationsSize = DbUtils.getLocationsName().size();
        setTeamDashboardOptions();
        viewPager.setAdapter(teamDashboradPagerAdapter);
        populateViewpager();
        changeTabTextStyle(0);
        System.out.println("Oncreate:::TeamDashboard");
        switchTabOnNotification();
        return rootView;
    }

    private void setDate() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        String startDate = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        String endDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        String startDateTv = new SimpleDateFormat("dd/MM").format(calendar.getTime());
        String endDateTv = new SimpleDateFormat("dd/MM").format(date);
        pref.setShowDateEtvbr(startDateTv + "-" + endDateTv);
        pref.setStartdateEtvbr(startDate);
        pref.setEndDateEtvbr(endDate);
    }

    void setTeamDashboardOptions() {
        if (getArguments() == null) {
            return;
        }

        tabLayout.addTab(tabLayout.newTab().setText("Today's Tasks"));
        if (DbUtils.isBike()) {
            tabLayout.addTab(tabLayout.newTab().setText("ETB"));
        } else {
            tabLayout.addTab(tabLayout.newTab().setText("ETVBRL"));
        }
       // tabLayout.addTab(tabLayout.newTab().setText("Live / E-F"));

        if(DbUtils.isUserBranchHeadOperationsOrSM()) {
            tabLayout.addTab(tabLayout.newTab().setText("Lost/Drop"));
        }
        //tabLayout.addTab(tabLayout.newTab().setText("Pending"));
        tabLayout.setVisibility(View.VISIBLE);
        teamDashboradPagerAdapter = new TeamDashboardPagerAdapter(getChildFragmentManager(), DbUtils.isUserBranchHeadOperationsOrSM()?3:2);
    }

    private void populateViewpager() {
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                changeTabTextStyle(tab.getPosition());
                if(tab.getPosition()!=1){
                    intentAddevent.putExtra("showAddEvent", false);
                    getActivity().sendBroadcast(intentAddevent);
                }
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void changeTabTextStyle(int pos) {

        if (!TeamDashboardSwiped.getInstance().post(pos, tabLayout.getTabCount())) {
            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_TODAY_TASKS);
            CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
        }

        if (prevTab == -1) {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            prevTab = 0;

        } else if (prevTab != pos) {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(prevTab)))).getChildAt(1))).setTypeface(null, Typeface.NORMAL);
            prevTab = pos;
        }
    }

    private void switchTabOnNotification() {
        if (getActivity() != null
                && getArguments() != null
                && getActivity().getIntent().getExtras() != null
                && getActivity().getIntent().getExtras().getBoolean(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY)
                && getActivity().getIntent().getExtras().getString(WSConstants.NOTIFICATION_CLICKED_NAME, "")
                .equalsIgnoreCase(WSConstants.FirebaseEvent.EOD_SUMMARY)) {
            if (getArguments().getBoolean("is_branch_head")
                    || getArguments().getBoolean("is_sales_manager")) {
                viewPager.setCurrentItem(1);
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
