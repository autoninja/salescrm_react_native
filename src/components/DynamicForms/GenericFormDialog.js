import React, { Component } from 'react';
import { View, Text, TouchableOpacity} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import RadioButton from '../../components/uielements/RadioButton'

export default class GenericFormDialog extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {tenure:null}
    }
    render() {
        let actionDisabled = this.props.showTenure && this.state.tenure==null
        return(
                <LinearGradient
                colors={["#2e5e86", "#2e3486"]}
                style={{padding:10,
                    paddingTop:20,
                    paddingBottom:20,
                    borderRadius:10,}}>
                        <View>
                        <Text style={
                            {color:'#fff', 
                        fontSize:18, 
                        padding:6,
                        textAlign:'center'
                        }}>{this.props.title}</Text>
                        {this.props.showTenure && (
                            <View style={{padding:4}}>
                                <Text style={{color:"#eeeeee", fontSize:16}}>Select Tenure</Text>
                            <RadioButton 
                            style={{marginTop:4}}
                            lightMode={true}
                            selected={this.state.tenure == "1 Year"} title={"1 Year"} 
                            onClick={() => {
                               this.setState({tenure:"1 Year"});
                            }}
                        />	
                        <RadioButton 
                            style={{marginTop:4}}
                            lightMode={true}
                            selected={this.state.tenure == "2 Year"} title={"2 Year"} 
                            onClick={() => {
                               this.setState({tenure:"2 Year"});
                            }}
                        />	
                        <RadioButton 
                            style={{marginTop:4}}
                            lightMode={true}
                            selected={this.state.tenure == "3 Year"} title={"3 Year"} 
                            onClick={() => {
                               this.setState({tenure:"3 Year"});
                            }}
                        />	
                        </View>
                        )}
                        <View style={{flexDirection:'row', marginTop:20}}>
                            <TouchableOpacity 
                            onPress={()=> {
                                this.props.onCancel();
                            }}
                            style={{borderRadius:4,flex:1, backgroundColor:'#a1a1a1', padding:6, margin:4}}
                            >
                                <Text style={{color:"#fff", fontSize:18, textAlign:'center'}}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                            disabled={actionDisabled}
                            onPress={()=> {
                                this.props.onConfirm(this.state.tenure);
                            }} 
                            style={{borderRadius:4, flex:1, backgroundColor:'#007fff', padding:6, margin:4}}>
                            <Text style={{color:actionDisabled?'#bab6b6':"#fff", fontSize:18, textAlign:'center'}}>I Confirm</Text>
                            </TouchableOpacity>
                        </View>
                         </View>  
                </LinearGradient>
        )
    }
}