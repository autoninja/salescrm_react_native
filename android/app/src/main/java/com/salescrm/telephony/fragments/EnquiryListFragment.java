package com.salescrm.telephony.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.PlanNextTaskPicker;
import com.salescrm.telephony.activity.TransferEnquiriesActivity;
import com.salescrm.telephony.adapter.EnquiryListPagerAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.AllMobileNumbersSearch;
import com.salescrm.telephony.db.EnquiryListStageTotalDb;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.LeadCountSpreader;
import com.salescrm.telephony.interfaces.UserLocationPickerCallBack;
import com.salescrm.telephony.model.AllLeadEnquiryListResponse;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.model.EnquiryListStageTotal;
import com.salescrm.telephony.model.FilterDataChangedListener;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;
import com.salescrm.telephony.views.UserLocationPicker;
import com.squareup.otto.Subscribe;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by Ravindra P on 01-12-2015.
 */
public class EnquiryListFragment extends Fragment implements AutoDialogClickListener {
    private Preferences pref;
    private ViewPager viewPager;
    private EnquiryListPagerAdapter taskSimplePagerAdapter;
    private TabLayout tabLayout;
    private int prevTab = -1;
    private ConnectionDetectorService connectionDetectorService;
    private Realm realm;
    private RelativeLayout relLoading;
    private RelativeLayout relFooter;
    private FilterSelectionHolder filterSelectionHolder;
    private volatile boolean isOnDestroyCalled = false;
    private TextView tvTransferEnquiries;
    private FrameLayout frameTransferEnquiries;
    private boolean isBike;
    private RealmResults<UserLocationsDB> userLocationsDB;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            Bundle data = intent.getExtras();
            if (data != null) {
                boolean isConnected = data.getBoolean("isConnected");
                if (filterSelectionHolder.isEmpty()) {
                    if (isConnected) {
                        updateFooter(View.GONE, getString(R.string.your_offline), false);
                    } else {
                        updateFooter(View.VISIBLE, getString(R.string.your_offline), true);
                    }
                }

            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_enquiry_list, container, false);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(getActivity());
        filterSelectionHolder = FilterSelectionHolder.getInstance();
        isBike = DbUtils.isBike();
        relLoading = (RelativeLayout) rootView.findViewById(R.id.rel_loading_frame);
        viewPager = (ViewPager) rootView.findViewById(R.id.vpPager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout_enquiry_list);
        relFooter = (RelativeLayout) rootView.findViewById(R.id.rel_action_plan_footer);
        tvTransferEnquiries = (TextView) rootView.findViewById(R.id.tv_transfer_enq_action);
        frameTransferEnquiries = (FrameLayout) rootView.findViewById(R.id.frame_transfer_enq_action);
        WSConstants.FRAGMENT_ID = 3;
        userLocationsDB = realm.where(UserLocationsDB.class).findAll();

        if(DbUtils.isBike()){
            tabLayout.addTab(tabLayout.newTab().setText("Assigned("+0+")"));
            tabLayout.addTab(tabLayout.newTab().setText("Test Ride("+0+")"));
            //tabLayout.addTab(tabLayout.newTab().setText("Evaluation("+0+")"));
            //tabLayout.addTab(tabLayout.newTab().setText("Converted("+0+")"));
            //tabLayout.addTab(tabLayout.newTab().setText("Invoiced(" + 0 + ")"));
            tabLayout.addTab(tabLayout.newTab().setText("Closed(" + 0 + ")"));
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
        }else {
            tabLayout.addTab(tabLayout.newTab().setText("Assigned(" + 0 + ")"));
            tabLayout.addTab(tabLayout.newTab().setText("Test drive(" + 0 + ")"));
            tabLayout.addTab(tabLayout.newTab().setText("Evaluation(" + 0 + ")"));
            tabLayout.addTab(tabLayout.newTab().setText("Booked(" + 0 + ")"));
            tabLayout.addTab(tabLayout.newTab().setText("Invoiced(" + 0 + ")"));
            tabLayout.addTab(tabLayout.newTab().setText("Closed(" + 0 + ")"));
            tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        //tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        changeTabTextStyle(0);
        WSConstants.ENQUIRY_LIST_LOCATION_ID.clear();
        System.out.println("Hala Madrid 1 "+WSConstants.ENQUIRY_LIST_LOCATION_ID);
        if(new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            if(userLocationsDB != null && userLocationsDB.isValid() && userLocationsDB.size()>1) {
                setLocationPopUp(userLocationsDB);
            }else {
                callAdapter();
            }
        }
        if (isUserBranchHeadOrSm()) {
            if(DbUtils.isBike()){
                frameTransferEnquiries.setVisibility(View.GONE);
            }else {
                frameTransferEnquiries.setVisibility(View.VISIBLE);
                Drawable rightDrawable = VectorDrawableCompat
                        .create(getResources(), R.drawable.ic_transfer_24dp, null);

                tvTransferEnquiries.setCompoundDrawablesWithIntrinsicBounds(null, null, rightDrawable, null);
                rootView.findViewById(R.id.frame_transfer_enq_click_action).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new UserLocationPicker().show(getContext(), new UserLocationPickerCallBack() {
                            @Override
                            public void onPickLocation(UserLocationsDB locationsDB) {
                                if (locationsDB != null) {
                                    showTransferEnqDialog(locationsDB);
                                }
                            }
                        });

                    }
                });
            }
        } else {
            frameTransferEnquiries.setVisibility(View.GONE);
        }
        relFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filterSelectionHolder == null || !filterSelectionHolder.isEmpty()) {
                    clearFilter();
                }

            }
        });

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter(WSConstants.NINJA_BROADCAST_MESSAGE));

        pushCleverTap(0);

        return rootView;
    }

    private void setLocationPopUp(RealmResults<UserLocationsDB> userLocationsDB) {
        relLoading.setVisibility(View.VISIBLE);
        ArrayList<String> listOfLocationsName = new ArrayList<>();
        final ArrayList<String> listOfLocationsIds = new ArrayList<>();
        listOfLocationsName.clear();
        listOfLocationsIds.clear();
        for(int i = 0; i < userLocationsDB.size(); i++){
            listOfLocationsName.add(userLocationsDB.get(i).getName());
            listOfLocationsIds.add(userLocationsDB.get(i).getLocation_id());
        }
        final CharSequence[] sequences = listOfLocationsName.toArray(new CharSequence[listOfLocationsName.size()]);
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setCancelable(false)
                .setTitle("Select Locations: ")
                .setMultiChoiceItems(sequences, null, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int index, boolean isChecked) {
                        if(isChecked){
                            WSConstants.ENQUIRY_LIST_LOCATION_ID.add(listOfLocationsIds.get(index));
                        }else if(WSConstants.ENQUIRY_LIST_LOCATION_ID.contains(listOfLocationsIds.get(index))) {
                            WSConstants.ENQUIRY_LIST_LOCATION_ID.remove(listOfLocationsIds.get(index));
                        }
                    }
                }).setPositiveButton("Apply", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        callAdapter();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        WSConstants.ENQUIRY_LIST_LOCATION_ID.clear();
                        callAdapter();
                    }
                }).create();
                dialog.show();

    }

    private void callAdapter() {
        relLoading.setVisibility(View.VISIBLE);
        getLeadStagesTotal();
    }

    private void showTransferEnqDialog(final UserLocationsDB locationsDB) {
        final Dialog dialog = new Dialog(getContext(), R.style.AppTheme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
            //  dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }

        dialog.setContentView(R.layout.layout_tasks_picker);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tv_pnt_title);
        tvTitle.setText("Transfer enquiries by");
        GridView gridView = (GridView) dialog.findViewById(R.id.grid_view_pnt);
        dialog.findViewById(R.id.card_pnt_book_car).setVisibility(View.GONE);

        String[] imageCaption = new String[]{"Filters", "Lead ID"};
        int[] imgSource = new int[]{R.drawable.ic_filter_24dp, R.drawable.ic_filter_lead};

        final int[] fAnswerId = new int[]{0, 1};
        gridView.setAdapter(new PlanNextTaskPicker().new TaskPickerGridAdapter(getContext(), imgSource, imageCaption, fAnswerId));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openTransferEnqActivity(fAnswerId[position],locationsDB.getLocation_id());
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


    }

    private void openTransferEnqActivity(int from, String userLocation) {
        Intent intent = new Intent(getContext(), TransferEnquiriesActivity.class);
        intent.putExtra("lead_transfer_type", from);
        intent.putExtra("lead_transfer_user_location", userLocation);
        startActivity(intent);

    }

    private void clearFilter() {

        //ClearFilter Dialog
        new AutoDialog(getContext(), this, new AutoDialogModel("Are you sure want to clear filter?", "Yes", "No")).show();


    }

    /*private void getAllLeadList() {
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getAllLeadList(null, null, null, WSConstants.ENQUIRY_LIST_LOCATION_ID, null, "0", "450", "0", new Callback<AllLeadEnquiryListResponse>() {
            @Override
            public void success(final AllLeadEnquiryListResponse allLeadEnquiryListResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(allLeadEnquiryListResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }

                if (allLeadEnquiryListResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    if (allLeadEnquiryListResponse.getResult() != null && allLeadEnquiryListResponse.getResult().getLeadDetails() != null) {
                        if (!allLeadEnquiryListResponse.getResult().getCount().equalsIgnoreCase("")) {
                            pref.setLeadListCount(Integer.parseInt(allLeadEnquiryListResponse.getResult().getCount()));
                        }
                        realm.beginTransaction();
                        if(realm.where(LeadListDetails.class).findAll() != null) {
                            realm.delete(LeadListDetails.class);
                        }
                        realm.commitTransaction();

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                addLeadListToDb(realm, allLeadEnquiryListResponse.getResult());
                            }
                        });
                    }
                    //populateViewpager();
                    getSearchMobileNumbers(allLeadEnquiryListResponse.getResult());
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    private void addLeadListToDb(Realm realm, AllLeadEnquiryListResponse.Result result) {

        System.out.println("Size Of List: - " + pref.getLeadListCount() + ", " + new Gson().toJson(result).toString());
        //SalesCRMApplication.getBus().post(new LeadCountSpreader(0, "All", pref.getLeadListCount()));
        for (int i = 0; i < result.getLeadDetails().size(); i++) {
            LeadListDetails leadListDetails = new LeadListDetails();
            leadListDetails.setActivity_id(result.getLeadDetails().get(i).getActivity_id());
            leadListDetails.setStage_id(result.getLeadDetails().get(i).getStage_id());
            leadListDetails.setActivity_description(result.getLeadDetails().get(i).getActivity_description());
            leadListDetails.setActivity_group(result.getLeadDetails().get(i).getActivity_group());
            leadListDetails.setActivity_type(result.getLeadDetails().get(i).getActivity_type());
            leadListDetails.setActivity_group_id(result.getLeadDetails().get(i).getActivity_group_id());
            leadListDetails.setActivity_name(result.getLeadDetails().get(i).getActivity_name());
            leadListDetails.setActivity_type_id(result.getLeadDetails().get(i).getActivity_type_id());
            leadListDetails.setActivity_user_asignee(result.getLeadDetails().get(i).getActivity_user_asignee());
            leadListDetails.setAssigned_to_id(result.getLeadDetails().get(i).getAssigned_to_id());
            leadListDetails.setCategory(result.getLeadDetails().get(i).getCategory());
            leadListDetails.setCategory_instance_id(result.getLeadDetails().get(i).getCategory_instance_id());
            leadListDetails.setColor(result.getLeadDetails().get(i).getColor());
            leadListDetails.setCustomer_id(result.getLeadDetails().get(i).getCustomer_id());
            leadListDetails.setEmail(result.getLeadDetails().get(i).getEmail());
            leadListDetails.setFirst_name(result.getLeadDetails().get(i).getFirst_name());
            leadListDetails.setLast_name(result.getLeadDetails().get(i).getLast_name());
            leadListDetails.setLead_id(result.getLeadDetails().get(i).getLead_id());
            leadListDetails.setLead_source_id(result.getLeadDetails().get(i).getLead_source_id());
            leadListDetails.setLead_stage(result.getLeadDetails().get(i).getLead_stage());
            leadListDetails.setLead_tag_name(result.getLeadDetails().get(i).getLead_tag_name());
            leadListDetails.setNumber(result.getLeadDetails().get(i).getNumber());
            leadListDetails.setScheduled_activity_id(result.getLeadDetails().get(i).getScheduled_activity_id());
            leadListDetails.setScheduled_at(result.getLeadDetails().get(i).getScheduled_at());
            leadListDetails.setVariant_name(result.getLeadDetails().get(i).getVariant_name());
            leadListDetails.setGender(result.getLeadDetails().get(i).getGender());
            leadListDetails.setLead_creation_date_time(result.getLeadDetails().get(i).getLead_creation_date_time());
            leadListDetails.setLead_age(Integer.parseInt(result.getLeadDetails().get(i).getLead_age()));
            leadListDetails.setStage_age(Integer.parseInt(result.getLeadDetails().get(i).getStage_age()));
            leadListDetails.setBuyer_type(result.getLeadDetails().get(i).getBuyer_type());
            leadListDetails.setExpected_closing_date(result.getLeadDetails().get(i).getExpected_closing_date());
            leadListDetails.setLead_source_name(result.getLeadDetails().get(i).getLead_source_name());
            leadListDetails.setDse_name(result.getLeadDetails().get(i).getDse_name());
            leadListDetails.setClosing_reason(result.getLeadDetails().get(i).getClosing_reason());
            if (result.getLeadDetails().get(i).getStage_updated() != null) {
                leadListDetails.setStage_updated(Util.getDate(result.getLeadDetails().get(i).getStage_updated()));
            }
            leadListDetails.lead_stage_progress.clear();
            for (int j = 0; j < result.getLeadDetails().get(i).getLead_stage_progress().size(); j++) {
                LeadListStageProgress leadListStageProgress = new LeadListStageProgress();
                leadListStageProgress.setStage_id(Integer.parseInt(result.getLeadDetails().get(i).getLead_stage_progress().get(j).getStage_id()));
                leadListStageProgress.setWidth(Integer.parseInt(result.getLeadDetails().get(i).getLead_stage_progress().get(j).getWidth()));
                leadListStageProgress.setName(result.getLeadDetails().get(i).getLead_stage_progress().get(j).getName());
                leadListStageProgress.setBar_class(result.getLeadDetails().get(i).getLead_stage_progress().get(j).getBar_class());
                leadListDetails.lead_stage_progress.add(leadListStageProgress);
            }
            realm.copyToRealmOrUpdate(leadListDetails);
        }

        populateViewpager();
        // changeItemCountOnTab();

    }*/

    private void populateViewpager() {
        relLoading.setVisibility(View.GONE);
        changeItemCountOnTab();
        if (!isOnDestroyCalled && taskSimplePagerAdapter!= null && isAdded()) {
            taskSimplePagerAdapter.notifyDataSetChanged();
        }
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setCurrentItem(pref.getLastVisitedTab());
        if(DbUtils.isBike()){
            viewPager.setOffscreenPageLimit(3);
        }else {
            viewPager.setOffscreenPageLimit(6);
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //changeTabTextStyle(tab.getPosition());
                pref.setLastVisitedTab(tab.getPosition());
                viewPager.setCurrentItem(tab.getPosition());
                pushCleverTap(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void pushCleverTap(int tabPosition) {
        String tab = "";
        if(isBike) {
            switch (tabPosition) {
                case 0:
                    tab = CleverTapConstants.EVENT_MY_ENQUIRES_TAB_ASSIGNED;
                    break;
                case 1:
                    tab = CleverTapConstants.EVENT_MY_ENQUIRES_TAB_TEST_RIDE;
                    break;
                case 2:
                    tab = CleverTapConstants.EVENT_MY_ENQUIRES_TAB_CLOSED;
                    break;
            }
        }
        else {
            switch (tabPosition) {
                case 0:
                    tab = CleverTapConstants.EVENT_MY_ENQUIRES_TAB_ASSIGNED;
                    break;
                case 1:
                    tab = CleverTapConstants.EVENT_MY_ENQUIRES_TAB_TEST_DRIVE;
                    break;
                case 2:
                    tab = CleverTapConstants.EVENT_MY_ENQUIRES_TAB_EVALUATION;
                    break;
                case 3:
                    tab = CleverTapConstants.EVENT_MY_ENQUIRES_TAB_BOOKED;
                    break;
                case 4:
                    tab = CleverTapConstants.EVENT_MY_ENQUIRES_TAB_INVOICED;
                    break;
                case 5:
                    tab =CleverTapConstants.EVENT_MY_ENQUIRES_TAB_CLOSED;
                    break;
            }
        }
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_MY_ENQUIRES_KEY_TAB,tab);
        hashMap.put(CleverTapConstants.EVENT_MY_ENQUIRES_KEY_CLICK,CleverTapConstants.EVENT_MY_ENQUIRES_CLICK_NO);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_MY_ENQUIRES, hashMap);

    }

    private void changeItemCountOnTab() {

        EnquiryListStageTotalDb enquiryStageTotal = realm.where(EnquiryListStageTotalDb.class).findFirst();
        if(new ConnectionDetectorService(getActivity()).isConnectingToInternet() && enquiryStageTotal != null) {
            if(DbUtils.isBike()){
                tabLayout.getTabAt(0).setText("Assigned(" + enquiryStageTotal.getAssigned() + ")");
                tabLayout.getTabAt(1).setText("Test Ride(" + enquiryStageTotal.getTestdrive() + ")");
                //tabLayout.getTabAt(2).setText("Evaluation(" + enquiryStageTotal.getEvaluation() + ")");
                //tabLayout.getTabAt(2).setText("Converted(" + enquiryStageTotal.getBooked() + ")");
                //tabLayout.getTabAt(4).setText("Invoiced(" + enquiryStageTotal.getInvoiced() + ")");
                tabLayout.getTabAt(2).setText("Closed(" + enquiryStageTotal.getClosed() + ")");
            }else {
                tabLayout.getTabAt(0).setText("Assigned(" + enquiryStageTotal.getAssigned() + ")");
                tabLayout.getTabAt(1).setText("Test drive(" + enquiryStageTotal.getTestdrive() + ")");
                tabLayout.getTabAt(2).setText("Evaluation(" + enquiryStageTotal.getEvaluation() + ")");
                tabLayout.getTabAt(3).setText("Booked(" + enquiryStageTotal.getBooked() + ")");
                tabLayout.getTabAt(4).setText("Invoiced(" + enquiryStageTotal.getInvoiced() + ")");
                tabLayout.getTabAt(5).setText("Closed(" + enquiryStageTotal.getClosed() + ")");
            }
        }else{
            if(DbUtils.isBike()){
                tabLayout.getTabAt(0).setText("Assigned(" + enquiryStageTotal.getAssigned() + ")");
                tabLayout.getTabAt(1).setText("Test Ride(" + enquiryStageTotal.getTestdrive() + ")");
                //tabLayout.getTabAt(2).setText("Evaluation(" + enquiryStageTotal.getEvaluation() + ")");
                //tabLayout.getTabAt(2).setText("Converted(" + enquiryStageTotal.getBooked() + ")");
                //tabLayout.getTabAt(4).setText("Invoiced(" + enquiryStageTotal.getInvoiced() + ")");
                tabLayout.getTabAt(2).setText("Closed(" + enquiryStageTotal.getClosed() + ")");
            }else {
                tabLayout.getTabAt(0).setText("Assigned(" + 0 + ")");
                tabLayout.getTabAt(1).setText("Test drive(" + 0 + ")");
                tabLayout.getTabAt(2).setText("Evaluation(" + 0 + ")");
                tabLayout.getTabAt(3).setText("Booked(" + 0 + ")");
                tabLayout.getTabAt(4).setText("Invoiced(" + 0 + ")");
                tabLayout.getTabAt(5).setText("Closed(" + 0 + ")");
            }
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setupTabIcons();
    }

    public void changeIndicator(int x, int y) {

    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getResources().getStringArray(R.array.drawer_items)[2]);
        SalesCRMApplication.getBus().register(this);
        updateUi();

    }

    private void updateUi() {
        if (new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            //relLoading.setVisibility(View.VISIBLE);
            //callAdapter();
            //getLeadStagesTotal();
            //getAllLeadList();
        } else {
            populateViewpager();
        }
        //  callBookingsRetro();
        if (!filterSelectionHolder.isEmpty()) {
            updateFooter(View.VISIBLE, getString(R.string.clear_filter), false);
        } else if (!new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            updateFooter(View.VISIBLE, getString(R.string.your_offline), true);
        } else {
            updateFooter(View.GONE, getString(R.string.clear_filter), false);
        }
    }

    private void getLeadStagesTotal() {
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getAllLeadListWithFilter(null, null, null,
                WSConstants.ENQUIRY_LIST_LOCATION_ID, null,null, null, null,
                Util.createJsonObject(filterSelectionHolder, realm), true, new Callback<EnquiryListStageTotal>() {
            @Override
            public void success(final EnquiryListStageTotal enquryListStageTotal, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(enquryListStageTotal.getStatusCode().equals(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }

                if ((""+enquryListStageTotal.getStatusCode()).equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    if(enquryListStageTotal.getResult() != null){
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.delete(EnquiryListStageTotalDb.class);
                                addTotalToDb(enquryListStageTotal.getResult());
                            }
                        });
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    private void addTotalToDb(EnquiryListStageTotal.Result result) {
        EnquiryListStageTotalDb enquiryListStageTotalDb = new EnquiryListStageTotalDb();
        enquiryListStageTotalDb.setAll(result.getAll());
        enquiryListStageTotalDb.setNotcontacted(result.getNotcontacted());
        enquiryListStageTotalDb.setQualified(result.getQualified());
        enquiryListStageTotalDb.setAssigned(result.getAssigned());
        enquiryListStageTotalDb.setBooked(result.getBooked());
        enquiryListStageTotalDb.setTestdrive(result.getTestdrive());
        enquiryListStageTotalDb.setInvoiced(result.getInvoiced());
        enquiryListStageTotalDb.setFinance(result.getFinance());
        enquiryListStageTotalDb.setClosed(result.getClosed());
        enquiryListStageTotalDb.setEvaluation(result.getEvaluation());
        realm.copyToRealm(enquiryListStageTotalDb);
        if(getActivity() != null && isAdded()) {
            taskSimplePagerAdapter = new EnquiryListPagerAdapter(getChildFragmentManager());
            viewPager.setAdapter(taskSimplePagerAdapter);
            populateViewpager();
        }
    }

    @Subscribe
    public void onFilterDataChanged(FilterDataChangedListener filterDataChangedListener) {
        if (filterDataChangedListener.isUpdateRequired()) {
            updateUi();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isOnDestroyCalled = true;
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMessageReceiver);
        realm.close();
    }

    public void updateFooter(int visibility, String text, boolean isOffline) {
        relFooter.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            TextView textView = (TextView) relFooter.findViewById(R.id.tv_action_plan_footer);
            textView.setText(text);
            ImageView imageView = (ImageView) relFooter.findViewById(R.id.iv_action_plan_footer);
            if (isOffline) {
                Drawable vector = VectorDrawableCompat.create(getResources(), R.drawable.ic_offline, getActivity().getTheme());
                imageView.setImageDrawable(vector);
            } else if (!filterSelectionHolder.isEmpty()) {
                Drawable vector = VectorDrawableCompat.create(getResources(), R.drawable.ic_clear, getActivity().getTheme());
                imageView.setImageDrawable(vector);
            }
        }
    }

    public void changeTabTextStyle(int pos) {
        if (prevTab == -1) {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            prevTab = 0;

        } else {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(prevTab)))).getChildAt(1))).setTypeface(null, Typeface.NORMAL);
            prevTab = pos;
        }
    }

    @Subscribe
    public void leadCountEvent(LeadCountSpreader leadCountSpreader) {
        tabLayout.getTabAt(leadCountSpreader.getPosition()).setText(leadCountSpreader.getTitle() + "(" + leadCountSpreader.getCount() + ")");
    }

    @Override
    public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
        filterSelectionHolder.clearAll();
        callAdapter();
        if (!filterSelectionHolder.isEmpty()) {
            updateFooter(View.VISIBLE, getString(R.string.clear_filter), false);
        } else if (!new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            updateFooter(View.VISIBLE, getString(R.string.your_offline), true);
        } else {
            updateFooter(View.GONE, getString(R.string.clear_filter), false);
        }
        SalesCRMApplication.getBus().post(new FilterDataChangedListener(true));
    }

    @Override
    public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {

    }

    public boolean isUserBranchHeadOrSm() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_BRANCH_HEAD)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_OPERATIONS) || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLES.MANAGER+"")) {
                    return true;
                }
            }
        }
        return false;
    }

    public void getSearchMobileNumbers(AllLeadEnquiryListResponse.Result result) {

        realm.beginTransaction();

        RealmResults<SalesCRMRealmTable> salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).findAll();
        //RealmResults<LeadListDetails> leadListDetails = realm.where(LeadListDetails.class).findAll();
        AllMobileNumbersSearch allMobileNumbersSearch;

        if(salesCRMRealmTable != null) {

            for (int i = 0; i < salesCRMRealmTable.size(); i++) {
                allMobileNumbersSearch = new AllMobileNumbersSearch();
                allMobileNumbersSearch.setName(salesCRMRealmTable.get(i).getCustomerName());
                if(salesCRMRealmTable != null && salesCRMRealmTable.get(i) != null) {
                    allMobileNumbersSearch.setLeadId(salesCRMRealmTable.get(i).getLeadId());
                }else{
                    allMobileNumbersSearch.setLeadId(0);
                }
                for (int j = 0; j < salesCRMRealmTable.get(i).customerPhoneNumbers.size(); j++) {
                    allMobileNumbersSearch.setDeleteStatus(0);
                    allMobileNumbersSearch.setMobileNumber(""+salesCRMRealmTable.get(i).customerPhoneNumbers.get(j).getPhoneNumber());
                    System.out.println("TELEPHONY mobileNumbers Sales- "+salesCRMRealmTable.get(i).customerPhoneNumbers.get(j).getPhoneNumber());
                    realm.copyToRealmOrUpdate(allMobileNumbersSearch);
                }
            }
        }

        if(result.getLeadDetails() != null){

            for(int i=0;i<result.getLeadDetails().size();i++){
                allMobileNumbersSearch = new AllMobileNumbersSearch();
                allMobileNumbersSearch.setDeleteStatus(0);
                allMobileNumbersSearch.setLeadId(Long.parseLong(result.getLeadDetails().get(i).getLead_id()));
                allMobileNumbersSearch.setMobileNumber(result.getLeadDetails().get(i).getNumber());
                realm.copyToRealmOrUpdate(allMobileNumbersSearch);
                System.out.println("TELEPHONY mobileNumbers Lead- "+ Long.parseLong(result.getLeadDetails().get(i).getNumber()) );
            }
        }
        realm.commitTransaction();
    }
}
