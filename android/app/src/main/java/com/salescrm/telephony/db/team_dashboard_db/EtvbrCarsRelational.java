package com.salescrm.telephony.db.team_dashboard_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by prateek on 20/9/17.
 */

public class EtvbrCarsRelational extends RealmObject {
    @SerializedName("modelName")
    @Expose
    private String modelName;
    @SerializedName("modelId")
    @Expose
    private Integer modelId;
    @SerializedName("enquiries_per")
    @Expose
    private Integer enquiriesPer;
    @SerializedName("tdrives_per ")
    @Expose
    private Integer tdrivesPer;
    @SerializedName("visits_per ")
    @Expose
    private Integer visitsPer;
    @SerializedName("bookings_per ")
    @Expose
    private Integer bookingsPer;
    @SerializedName("retail_per ")
    @Expose
    private Integer retailPer;

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getEnquiriesPer() {
        return enquiriesPer;
    }

    public void setEnquiriesPer(Integer enquiriesPer) {
        this.enquiriesPer = enquiriesPer;
    }

    public Integer getTdrivesPer() {
        return tdrivesPer;
    }

    public void setTdrivesPer(Integer tdrivesPer) {
        this.tdrivesPer = tdrivesPer;
    }

    public Integer getVisitsPer() {
        return visitsPer;
    }

    public void setVisitsPer(Integer visitsPer) {
        this.visitsPer = visitsPer;
    }

    public Integer getBookingsPer() {
        return bookingsPer;
    }

    public void setBookingsPer(Integer bookingsPer) {
        this.bookingsPer = bookingsPer;
    }

    public Integer getRetailPer() {
        return retailPer;
    }

    public void setRetailPer(Integer retailPer) {
        this.retailPer = retailPer;
    }
}
