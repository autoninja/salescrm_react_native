const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
]
const _MS_PER_DAY = 1000 * 60 * 60 * 24;

let Utils = {
  getTodaysDate: function () {
    let today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd
    }
    if (mm < 10) {
      mm = '0' + mm
    }
    return yyyy + '-' + mm + '-' + dd;
  },


  getCurrentMonth: function () {
    let today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd
    }
    if (mm < 10) {
      mm = '0' + mm
    }
    return yyyy + '-' + mm + '-' + dd + '      ' + today;
  },

  getTimeAMPM: function (date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  },

  getReadableDateForGame: function (date) {
    if (date) {
      var month = date.getMonth() ? date.getMonth() : new Date().getMonth();
      return monthNames[month].substr(0, 3) + ' ' + (date.getFullYear() ? date.getFullYear() : new Date().getFullYear());
    }
    return monthNames[new Date().getMonth()].substr(0, 3) + ' ' + new Date().getFullYear();
  },

  getReadableDate: function (date) {
    if (date) {
      var month = date.getMonth() ? date.getMonth() : new Date().getMonth();
      return date.getDate() + ' ' + monthNames[month].substr(0, 3) + ' ' + (date.getFullYear() ? date.getFullYear() : new Date().getFullYear());
    }
    return new Date().getDate() + ' ' + monthNames[new Date().getMonth()].substr(0, 3) + ' ' + new Date().getFullYear();
  },
  parseReadableDate: function (date) {
    if (date) {
      var month = date.getMonth() ? date.getMonth() : new Date().getMonth();
      return date.getDate() + ' ' + monthNames[month].substr(0, 3) + ' ' + (date.getFullYear() ? date.getFullYear() : new Date().getFullYear());
    }
    return null;
  },
  getReadableDateTime: function (date) {
    if (date) {
      var month = date.getMonth() ? date.getMonth() : new Date().getMonth();
      return date.getDate()
        + ' ' + monthNames[month].substr(0, 3)
        + ' ' + (date.getFullYear() ? date.getFullYear() : new Date().getFullYear())
        + ', ' + this.getTimeAMPM(date);
    }
    return new Date().getDate() + ' ' + monthNames[new Date().getMonth()].substr(0, 3) + ' ' + new Date().getFullYear();
  },
  isValidDate: function (d) {
    return d instanceof Date && !isNaN(d);
  },
  getDate: function (serverDate) {
    //  console.error(serverDate);
    serverDate = serverDate.toString();
    if (serverDate) {
      serverDate = serverDate.split(' ')[0].split('-');
      if (serverDate && serverDate.length == 3) {
        return new Date(serverDate[0], (serverDate[1] - 1), serverDate[2], 0, 0, 0, 0);
      }
    }
    return new Date();
  },
  parseDate: function (serverDate) {
    //  console.error(serverDate);
    serverDate = serverDate.toString();
    if (serverDate) {
      serverDate = serverDate.split(' ')[0].split('-');
      if (serverDate && serverDate.length == 3) {
        return new Date(serverDate[0], (serverDate[1] - 1), serverDate[2], 0, 0, 0, 0);
      }
    }
    return null;
  },
  dateDiffInDays: (date1, date2) => {
    // Discard the time and time-zone information.
    let utc1 = Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate());
    let utc2 = Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate());

    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
  },
  getRandomInt: (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },
  getJsonFromUrl: (url) => {
    let query = url.substr(1);
    let result = {};
    query.split("&").forEach(function (part) {
      let item = part.split("=");
      result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
  },
  getNumbersAbbreviatedString: (value) => {
    var suffixes = ["", "k", "L"];
    var suffixNum = Math.floor(("" + value).length / 3);

    if (value >= 100000) {
      var shortValue = parseFloat((suffixNum != 0 ? ((value * 10) / Math.pow(1000, suffixNum)) : value).toPrecision(2));
      if (shortValue % 1 != 0) {
        shortValue = shortValue.toFixed(1);
      }
      return shortValue + suffixes[suffixNum];
    }

    if (value >= 1000) {
      var shortValue = parseFloat((suffixNum != 0 ? (value / Math.pow(1000, suffixNum)) : value).toPrecision(2));
      if (shortValue % 1 != 0) {
        shortValue = shortValue.toFixed(1);
      }
      return shortValue + suffixes[suffixNum];
    }

    return value
  },
  getModDate: (val) => {
    let date = new Date();
    if (val) {
      date.setDate(date.getDate() + val);
    }
    return date;
  }

};

module.exports = Utils;
