package com.salescrm.telephony.interfaces;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;

import io.realm.Realm;
import io.realm.RealmQuery;
import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by bharath on 28/12/17.
 */

public class BadgeUpdater {
    public static void updateBadge(Context context) {
        try {
            if (context == null) {
                context = SalesCRMApplication.GetAppContext();
            }
            Realm realm = Realm.getDefaultInstance();
            Preferences preferences = Preferences.getInstance();
            preferences.load(context);

            RealmQuery<SalesCRMRealmTable> realmQuery = realm.where(SalesCRMRealmTable.class)
                    .equalTo("isCreatedTemporary", false)
                    .lessThan("activityScheduleDate", Util.getTime(1))
                    .equalTo("isDone", false)
                    .equalTo("isLeadActive", 1)
                    .equalTo("dseId", preferences.getAppUserId())
                    .equalTo("createdOffline", false);
            System.out.println("Badge Count:"+realmQuery.count());
            ShortcutBadger.applyCount(context, (int)realmQuery.count());
        }
        catch (Exception e) {
            ShortcutBadger.removeCount(context);
        }

    }
}
