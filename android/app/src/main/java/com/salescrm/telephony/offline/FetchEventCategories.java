package com.salescrm.telephony.offline;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.events.EventsCategoryDb;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.EventCategoryResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FetchEventCategories implements Callback<EventCategoryResponse> {

    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;

    public FetchEventCategories(OfflineSupportListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }

    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getEventCategories(this);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.EVENT_CATEGORIES);
        }
    }

    @Override
    public void success(EventCategoryResponse eventCategoryResponse, Response response) {
        Util.updateHeaders(response.getHeaders());
        if (eventCategoryResponse.getStatusCode() != null && !eventCategoryResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + eventCategoryResponse.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.EVENT_CATEGORIES);
        } else {
            if (eventCategoryResponse.getResult() != null) {
                System.out.println("Success:1" + eventCategoryResponse.getMessage());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm1) {
                        insertData(realm1, eventCategoryResponse.getResult());

                    }
                });
                listener.onFetchEventCategories(eventCategoryResponse);
            } else {
                // relLoader.setVisibility(View.GONE);

                System.out.println("Success:2" + eventCategoryResponse.getMessage());
                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.EVENT_CATEGORIES);
                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }

    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.EVENT_CATEGORIES);
    }

    private void insertData(Realm realm, EventCategoryResponse.Result result) {
        realm.delete(EventsCategoryDb.class);
        RealmList<EventsCategoryDb> eventsCategoryDbs = new RealmList<>();
        for(int i=0; i< result.categories.size(); i++){
            EventsCategoryDb eventsCategory = new EventsCategoryDb();
            eventsCategory.setId(result.categories.get(i).getId());
            eventsCategory.setName(result.categories.get(i).getName());
            eventsCategoryDbs.add(eventsCategory);
        }
        realm.copyToRealm(eventsCategoryDbs);

    }
}
