
import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image} from 'react-native';
import { createStackNavigator, NavigationEvents } from 'react-navigation';
const img = "https://tineye.com/images/widgets/mona.jpg";

import {eventLeaderBoard} from '../clevertap/CleverTapConstants';
import CleverTapPush from '../clevertap/CleverTapPush'



export default class LeaderBoardConsultantsList extends React.Component {

	constructor(props)
		{
		    super(props);

		    this.state = {
		    	//isLoading: true,
		    	//isFetching:false,
			}
		}

	/*componentDidMount(){
     	 this.makeRemoteRequest();
 	 }

 	makeRemoteRequest = () => {
    	this.setState({ isLoading: true, isFetching: true });
    	new RestClient().getConsultantData({})*/


    	getImage(id, i){
    		switch(id){

    			case 1:
    				return <Image
		    				key={i}
							style={{ width: 15, height: 15, margin: 2}}
				            source={require('../images/badge_bulb.png')}
							/>;

				case 2:
    				return <Image
		    				key={i}
							style={{ width: 15, height: 15, margin: 2}}
				            source={require('../images/badge_car.png')}
							/>;
				case 3:
    				return <Image
		    				key={i}
							style={{ width: 15, height: 15, margin: 2}}
				            source={require('../images/badge_target.png')}
							/>;
				case 4:
    				return <Image
		    				key={i}
							style={{ width: 15, height: 15, margin: 2}}
				            source={require('../images/badge_fire.png')}
							/>;
				case 5:
    				return <Image
		    				key={i}
							style={{ width: 15, height: 15, margin: 2}}
				            source={require('../images/badge_explorer.png')}
							/>;
				case 6:
    				return <Image
		    				key={i}
							style={{ width: 15, height: 15, margin: 2}}
				            source={require('../images/badge_achiever.png')}
							/>;
				case 7:
    				return <Image
		    				key={i}
							style={{ width: 15, height: 15, margin: 2}}
				            source={require('../images/badge_dynamo.png')}
							/>;
				case 8:
    				return <Image
		    				key={i}
							style={{ width: 15, height: 15, margin: 2}}
				            source={require('../images/badge_guru.png')}
							/>;
				case 9:
    				return <Image
		    				key={i}
							style={{ width: 15, height: 15, margin: 2}}
				            source={require('../images/badge_trophy.png')}
							/>;

    		}
    	}

	logClevertap(eventName) {
		 CleverTapPush.pushEvent(eventLeaderBoard.event, { 'LeaderBoard Type' : eventName });

	 }

	render () {
			console.log(this.props.data);

			var dataList = this.props.category.users;
	    return (
	    	<View style= {{flex : 1, backgroundColor: '#2e5e86'}}>
				<NavigationEvents
					onWillFocus={payload => {
						if(this.props.category.spotlight == '1') {
								this.logClevertap(eventLeaderBoard.keyType.valuesType.CONSULTANT_LEADERBOARD_OWN);
						}
						else {
								this.logClevertap(eventLeaderBoard.keyType.valuesType.CONSULTANT_LEADERBOARD_OTHERS);
						}

					}}
				/>
	    	<FlatList
		    	//ItemSeparatorComponent = {this.FlatListItemSeparator}
		    	data={dataList}
		    	renderItem= {({item, index}) =>{
		    		let placeHolder = <View style={{position:'absolute',width:36, height:36, borderRadius: 36/2, alignItems:'center', backgroundColor: '#FF8000', justifyContent:'center'}}><Text style={{color:'#fff',alignItems: 'center', justifyContent:'center', textAlign: 'center'}}>{item.name.charAt(0).toUpperCase()}</Text></View>;
		    		return(
		    		<TouchableOpacity onPress={ () => this.props.onClick(item)}>
			    		<View style={{flex : 10, height: 50,flexDirection: 'row', alignItems : 'center', justifyContent: 'center', backgroundColor: '#0F2B52'}}>
				    		 <View style={{flex : 1, alignItems : 'center', justifyContent: 'center'}}>
				    		 	<Text style={{color: '#fff'}}>{item.rank}</Text>
				    		 </View>
				    		 <TouchableOpacity>
							  {placeHolder}
					    		 <Image
			                          style={{width: 36, height: 36, borderRadius: 36/2, alignItems: 'center'}}
			                          source={{uri: item.photo_url}}
			                      />
		                      </TouchableOpacity>
		                      <View style={{flex : 3, alignItems : 'flex-start', justifyContent: 'center', paddingLeft: 15}}>
				    		 	<Text style={{color: '#fff', fontSize: 13}}>{item.name}</Text>
				    		 </View>

				    		 <View style={{flex : 4, alignItems : 'center', justifyContent: 'center', flexDirection: 'row' }}>
		    						{
		    							item.badges.map((info, i) =>
		    								this.getImage(info.badge_id, i)
		    								)
		    						}
				    		 </View>

				    		 <Image
							        style={{ width: 12, height: 13, marginRight: 15}}
		                          	source={require('../images/ic_right_arrow.png')}
							        />

			    		</View>
		    			<View
						        style={{
						          height: 0.5,
						          width: "100%",
						          backgroundColor: "#d3d3d3",
						        }}
						      />
		    		</TouchableOpacity>
		    		)}}
		    	keyExtractor= {(item, index) => index.toString() }
		    	/>
		    	</View>
		    	);
	  }
	}
