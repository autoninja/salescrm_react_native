
class ShowRoomLocationModel {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}

class ActvityModel {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}

class EnquirySourceMainModel {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}

class EnquirySourceCategoryModel {
  constructor(id, name, subEnquirySource) {
    this.id = id;
    this.name = name;
    this.subEnquirySource = subEnquirySource;
  }
}

class SubEnquirySourceModel {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}

module.exports = {ShowRoomLocationModel, ActvityModel, EnquirySourceMainModel, EnquirySourceCategoryModel,SubEnquirySourceModel};
