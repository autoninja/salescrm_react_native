package com.salescrm.telephony.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bharath on 8/9/16.
 */
public class CreForActivityResponses { private String statusCode;

    private String message;

    private List<Result> result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public List<Result> getResult ()
    {
        return result;
    }

    public void setResult (List<Result> result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result implements Serializable
    {
        private String id;

        private String activity_id;

        private String role_id;

        private String name;

        private String location_id;

        private String lead_source_id;

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getActivity_id ()
        {
            return activity_id;
        }

        public void setActivity_id (String activity_id)
        {
            this.activity_id = activity_id;
        }

        public String getRole_id ()
        {
            return role_id;
        }

        public void setRole_id (String role_id)
        {
            this.role_id = role_id;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getLocation_id ()
        {
            return location_id;
        }

        public void setLocation_id (String location_id)
        {
            this.location_id = location_id;
        }

        public String getLead_source_id ()
        {
            return lead_source_id;
        }

        public void setLead_source_id (String lead_source_id)
        {
            this.lead_source_id = lead_source_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [id = "+id+", activity_id = "+activity_id+", role_id = "+role_id+", name = "+name+", location_id = "+location_id+", lead_source_id = "+lead_source_id+"]";
        }
    }
}
