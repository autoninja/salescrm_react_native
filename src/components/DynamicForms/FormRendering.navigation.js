import React from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import FormRenderMain from './form_rendering_main'
import PNTSubmission from './PNTSubmission'
import PlanNextTask from '../../containers/PlanNextTask/PlanNextTask'
import OtpPicker from '../../containers/PlanNextTask/OtpPicker'
import BackIcon from '../uielements/BackIcon';
import { TaskConstants } from '../../utils/Constants';

export const getFormRenderingNavigation = (info)=> createAppContainer(createStackNavigator({
    FormRendering: {
        screen: props => (<FormRenderMain 
            {...props}
            info={info}
           />),
           navigationOptions: ({ navigation }) => ({
            title: "Update",
            headerBackTitle: null,
            headerLeft: <BackIcon navigationProps={navigation} invert={true} />,
            headerStyle: {
                backgroundColor: '#fff',
                elevation: 0,
            },
            headerTransperent: true,
            headerTintColor: '#303030',
            headerTitleStyle: {
                fontWeight: 'bold',
                width: '100%',
                textAlign: 'left',
            },
        })
    },
    PlanNextTask : { 
        screen: props => (
            <PlanNextTask {...props} info={info}/>
        ),
        navigationOptions: ({ navigation }) => ({
        header: null
        })
    },
    PNTSubmission : { 
        screen: props => (
            <PNTSubmission {...props} info={info}/>
        ),
        navigationOptions: ({ navigation }) => ({
        header: null
        })
    },
    OtpPicker: {
        screen: props => (
            <OtpPicker
            {...props}
            info={info}
            />
        ),
        navigationOptions: ({ navigation }) => ({
            title: "Submit OTP",
            headerBackTitle: null,
            headerLeft: <BackIcon navigationProps={navigation} invert={true} />,
            headerStyle: {
                backgroundColor: '#fff',
                elevation: 0,
            },
            headerTransperent: true,
            headerTintColor: '#303030',
            headerTitleStyle: {
                fontWeight: 'bold',
                width: '100%',
                textAlign: 'left',
            },
        })
    }
}, {
    initialRouteName: info.activityId==TaskConstants.PLAN_NEXT_TASK.id?"PlanNextTask":"FormRendering",
    cardStyle: {
        backgroundColor: "#303030BE",
        opacity: 1
      },
      mode: "modal"
}
));
