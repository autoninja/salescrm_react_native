import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

export default class ItemPicker extends Component {

  constructor(props) {
    super(props);
  }
  render() {
    let title = "Title";
    title = this.props.title;
    let showHeaderText = this.props.showHeaderText;
    let headerText = this.props.headerText;
    let imageSourceExpand = require('../../images/ic_down_black.png');
    let disabled = false || this.props.disabled;
    let hideDownArrow = this.props.hideDownArrow;
    let showClearButton = this.props.showClearButton;
    let clickdisabled = false || this.props.clickdisabled

    let showError = this.props.showError;
    if (showError) {
      hideDownArrow = true;
    }

    return (
      <TouchableOpacity onPress={() => this.props.onClick()} disabled={disabled || clickdisabled}>
        <View style={[{ marginTop: 10 }, this.props.style]}>
          {showHeaderText && (
            <Text
              style={{
                color: "#303030",
                fontSize: 16,
                paddingLeft: 10,
                paddingRight: 10,
                fontWeight: "bold"
              }}
            >
              {headerText}
            </Text>
          )}
          <View style={[{
            flexDirection: 'row',
            justifyContent: 'space-between', alignItems: 'center', padding: 10, paddingLeft: 10, paddingRight: 10
          }, this.props.style]}>
            <View style={{ alignItems: 'center', flex: 1, justifyContent: 'flex-start', flexDirection: 'row' }}>
              <Text style={{ color: disabled ? '#808080' : '#303030', fontSize: 16, paddingRight: 10 }}>{title}</Text>

            </View>
            {hideDownArrow ||
              <Image
                source={imageSourceExpand}
                style={{ width: 24, height: 24, opacity: .2, }}
              />
            }
            {showClearButton &&
              <TouchableOpacity
                hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                onPress={() => this.props.onClear()}>
                <Image
                  source={require('../../images/ic_close_black.png')}
                  resizeMode='center'
                  style={{ width: 20, height: 20, }}
                />
              </TouchableOpacity>
            }
            {showError &&
              <TouchableOpacity
                hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                onPress={() => this.props.onClickErrorButton()}>
                <Image
                  source={require('../../images/ic_error_mark.png')}
                  resizeMode='center'
                  style={{ width: 20, height: 20, }}
                />
              </TouchableOpacity>
            }


          </View>
          <View style={{ height: 0.8, backgroundColor: '#cfcfcf' }} />
        </View>
      </TouchableOpacity>
    )
  }

}
