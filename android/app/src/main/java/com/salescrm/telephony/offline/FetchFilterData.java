package com.salescrm.telephony.offline;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.CustomerTypeDB;
import com.salescrm.telephony.db.FilterActivityType;
import com.salescrm.telephony.db.FilterEnquiryStage;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.FilterActivityAndStagesResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 6/12/16.
 * Call this to get the filter elements
 */
public class FetchFilterData implements Callback<FilterActivityAndStagesResponse> {
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;

    public FetchFilterData(OfflineSupportListener listener, Context context) {
        this.listener =listener;
        this.context = context;
      //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }
    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetFilterActivityAndStages(this);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.FETCH_FILTER_DATA);

        }
    }


    @Override
    public void success(final FilterActivityAndStagesResponse data, Response response) {
        Util.updateHeaders(response.getHeaders());

        if (!data.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + data.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.FETCH_FILTER_DATA);

        } else {
            if (data.getResult() != null) {
                System.out.println("Success:1" + data.getMessage());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        insertData(data);
                    }
                });
                listener.onFilterDataFetched(data);
            } else {
                // relLoader.setVisibility(View.GONE);
                System.out.println("Success:2" + data.getMessage());
                listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.FETCH_FILTER_DATA);
                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }

    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(error,WSConstants.OfflineAPIRequest.FETCH_FILTER_DATA);

    }

    private void insertData(FilterActivityAndStagesResponse data) {
        realm.where(FilterActivityType.class).findAll().deleteAllFromRealm();
        realm.where(FilterEnquiryStage.class).findAll().deleteAllFromRealm();
        realm.where(CustomerTypeDB.class).findAll().deleteAllFromRealm();
        FilterActivityType filterActivityType = new FilterActivityType();
        FilterEnquiryStage filterEnquiryStage = new FilterEnquiryStage();
        CustomerTypeDB customerTypeDB = new CustomerTypeDB();
        for (int i = 0; i < data.getResult().getActivities().size(); i++) {
            filterActivityType.setId(data.getResult().getActivities().get(i).getId());
            filterActivityType.setName(data.getResult().getActivities().get(i).getName());
            realm.copyToRealm(filterActivityType);
        }

        for (int i = 0; i < data.getResult().getLeadStages().size(); i++) {
            filterEnquiryStage.setStage_id(data.getResult().getLeadStages().get(i).getStage_id());
            filterEnquiryStage.setStage(data.getResult().getLeadStages().get(i).getStage());
            realm.copyToRealm(filterEnquiryStage);
        }

        for (int i = 0; i < data.getResult().getCustomerTypes().size(); i++) {
            customerTypeDB.setId(data.getResult().getCustomerTypes().get(i).getId());
            customerTypeDB.setType(data.getResult().getCustomerTypes().get(i).getType());
            realm.copyToRealm(customerTypeDB);
        }

    }

}
