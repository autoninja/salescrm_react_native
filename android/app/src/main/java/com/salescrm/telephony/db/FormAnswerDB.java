package com.salescrm.telephony.db;

import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.utils.Util;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by bharath on 31/10/16.
 */

public class FormAnswerDB  extends RealmObject{

    private boolean is_synced=false;
    private Date dataCreatedDate= Util.getDateTime(0);

    //{Add activity input data
    private String lead_last_updated;
    private String lead_id;
    //Add activity input data}



    private String action_id;
    private String scheduled_activity_id;


    public boolean is_synced() {
        return is_synced;
    }

    public void setIs_synced(boolean is_synced) {
        this.is_synced = is_synced;
    }

    public Date getDataCreatedDate() {
        return dataCreatedDate;
    }

    public void setDataCreatedDate(Date dataCreatedDate) {
        this.dataCreatedDate = Util.getDateTime(0);
    }

    private RealmList<FormResponseDB> form_response;

    public String getAction_id() {
        return action_id;
    }

    public void setAction_id(String action_id) {
        this.action_id = action_id;
    }

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }


    public String getScheduled_activity_id() {
        return scheduled_activity_id;
    }

    public void setScheduled_activity_id(String scheduled_activity_id) {
        this.scheduled_activity_id = scheduled_activity_id;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public RealmList<FormResponseDB> getForm_response() {
        return form_response;
    }

    public void setForm_response(RealmList<FormResponseDB> form_response) {
        this.form_response = form_response;
    }

    @Override
    public String toString() {
        return "ClassPojo [action_id = " + action_id + ", lead_last_updated = " + lead_last_updated + ", scheduled_activity_id = " + scheduled_activity_id + ", lead_id = " + lead_id + ", form_response = " + form_response + "]";
    }

}

