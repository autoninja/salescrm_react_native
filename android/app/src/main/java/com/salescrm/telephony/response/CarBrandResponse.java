package com.salescrm.telephony.response;

/**
 * Created by akshata on 8/9/16.
 */
public class CarBrandResponse {

    private String statusCode;

    private String message;

    private Result[] result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result[] getResult() {
        return result;
    }

    public void setResult(Result[] result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private String id;

        private String company;

        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "ClassPojo [id = " + id + ", company = " + company + ", name = " + name + "]";
        }

        public class Error {
            private String details;

            private String type;

            public String getDetails() {
                return details;
            }

            public void setDetails(String details) {
                this.details = details;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            @Override
            public String toString() {
                return "ClassPojo [details = " + details + ", type = " + type + "]";
            }
        }
    }
}
