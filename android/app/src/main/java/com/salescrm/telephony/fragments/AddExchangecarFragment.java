package com.salescrm.telephony.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddExchangeCarActivity;
import com.salescrm.telephony.db.AppConfigDB;
import com.salescrm.telephony.db.car.ExchangeCarBrandModelsDB;
import com.salescrm.telephony.db.car.ExchangeCarBrandsDB;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;


/**
 * Created by akshata on 27/5/16.
 */
public class AddExchangecarFragment extends Fragment implements com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener,View.OnFocusChangeListener,View.OnClickListener {

    static RealmResults<ExchangeCarBrandsDB> mUserCarBrand;
    static AppConfigDB appConfdDb;

    List<String> modelIdsList = new ArrayList<String>();
    List<String> modelNamesList = new ArrayList<>();

    Preferences pref;
    EditText date_calendar;
  //  Spinner spinnerModel, spinnerVariant;
    EditText edtTotalRun, edtRegNumber;
   // TextView brandName;
    private List<String> variantNamesList = new ArrayList<String>();
    private List<String> variantIdsList = new ArrayList<String>(); ;
    private ExchangeCar _exchangeCar;
    private String selectedBrandId = "", selectedModelId = "", selectedYear = "", selectedVariantId = "",
            selectedTotalRuns = "", selectedRegNum = "", selectedBrandName = "", selectedModelName ="";
   // private CarBrandsDB mCarBrandsDB;
    private ExchangeCarBrandModelsDB mCarBrandModelsDB;
    private RealmResults<ExchangeCarBrandModelsDB> allDistinctModelsDB;
    private Realm realm;
    private AutoCompleteTextView autoTvInterExchangeCarModel;
    private RealmResults<ExchangeCarBrandModelsDB> carModelListOld;


    public static AddExchangecarFragment newInstance(RealmResults<ExchangeCarBrandsDB> userCarBrand) {
        AddExchangecarFragment addexchange = new AddExchangecarFragment();
        mUserCarBrand = userCarBrand;
        return addexchange;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        AddExchangeCarActivity a;
        if (context instanceof AddExchangeCarActivity) {
            a = (AddExchangeCarActivity) context;
            try {
                _exchangeCar = (ExchangeCar) a;
            } catch (ClassCastException e) {
                throw new ClassCastException(a.toString() + " must implement ExchangeCar");
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view1 = inflater.inflate(R.layout.add_exchangecar, container, false);
        final FragmentActivity c = getActivity();
        pref = Preferences.getInstance();
        pref.load(getActivity());
        date_calendar = (EditText) view1.findViewById(R.id.date_calendar);
        //spinnerModel = (Spinner) view1.findViewById(R.id.spinner_model);
        autoTvInterExchangeCarModel = (AutoCompleteTextView) view1.findViewById(R.id.add_exchangecar_auto_tv_add_lead_car_model);
        //tvBrand = (Spinner) view1.findViewById(R.id.spinner_brand);
        //spinnerVariant = (Spinner) view1.findViewById(R.id.spinner_variant);
        //spinnerVariant.setVisibility(View.GONE);
        edtTotalRun = (EditText) view1.findViewById(R.id.edt_total_run);
        edtRegNumber = (EditText) view1.findViewById(R.id.edt_reg_number);
     //   brandName = (TextView) view1.findViewById(R.id.brandName);
        final List<String> listexchangecar = new ArrayList<String>();
        realm = Realm.getDefaultInstance();
        initAdapter();

        autoTvInterExchangeCarModel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedModelId = carModelListOld.where().
                        equalTo("model_name",autoTvInterExchangeCarModel.getText().toString())
                        .findFirst()
                        .getModel_id();
               // selectedModelId = modelIdsList.get(position);
                selectedModelName = carModelListOld.where().
                        equalTo("model_name",autoTvInterExchangeCarModel.getText().toString())
                        .findFirst()
                        .getModel_name();
               // if(!selectedBrandId.equalsIgnoreCase("-1") && !selectedModelId.equalsIgnoreCase("-1"))
                    // callAllcarvariants(selectedBrandId, selectedModelId);
                    sendData();
            }
        });


        edtTotalRun.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectedTotalRuns = edtTotalRun.getText().toString();
                sendData();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtRegNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectedRegNum = edtRegNumber.getText().toString();
                sendData();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        date_calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Util.showDatePicker(AddExchangecarFragment.this, getActivity(), R.id.date_calendar, WSConstants.TYPE_DATE_PICKER.ADD_EXCHANGE_CAR);


            }
        });

        appConfdDb = realm.where(AppConfigDB.class)
                .equalTo("key","dealerBrand")
                .findFirst();

        if(appConfdDb!=null){
            selectedBrandId = appConfdDb.getId();
            selectedBrandName = appConfdDb.getValue();
        }
      //  brandName.setText(selectedBrandName);
        sendData();
        if(!selectedBrandId.equalsIgnoreCase("-1"))
            callAllmodels();

      /*  spinnerModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedModelId = modelIdsList.get(i);
                selectedModelName = modelNamesList.get(i);
                if(!selectedBrandId.equalsIgnoreCase("-1") && !selectedModelId.equalsIgnoreCase("-1"))
               // callAllcarvariants(selectedBrandId, selectedModelId);
                sendData();
            }
        });*/

        return view1;
    }

    public void callAllmodels() {

        modelNamesList.clear();
        modelIdsList.clear();
        modelIdsList.add("-1");
        modelNamesList.add("<Select>");
        variantNamesList.clear();
        variantIdsList.clear();
        variantIdsList.add("-1");
        variantNamesList.add("<Select>");

        allDistinctModelsDB = realm.where(ExchangeCarBrandModelsDB.class)
                            .distinct("model_name");

        Iterator<ExchangeCarBrandModelsDB> itr = allDistinctModelsDB.iterator();
        while(itr.hasNext()){
            ExchangeCarBrandModelsDB temp = itr.next();
            modelNamesList.add(temp.getModel_name());
            modelIdsList.add(temp.getModel_id());
        }
/*
        ArrayAdapter<String> modelAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, modelNamesList);*/
       // modelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      //  spinnerModel.setAdapter(modelAdapter);


    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date_calendar.setText(String.format(Locale.getDefault(), "%d-%d-%d", dayOfMonth,((monthOfYear)+1), year));
        selectedYear = String.format(Locale.getDefault(), "%d-%d-%d", dayOfMonth, ((monthOfYear)+1), year);
        Log.e("selectedYear","- "+selectedYear);
        if(selectedYear.equalsIgnoreCase("01 jan 1970") || selectedYear.contains("1970")){
            selectedYear="";
        }
        sendData();

    }

    private void sendData() {
        Log.e("selectedYear","- "+selectedYear);
        if(selectedYear.equalsIgnoreCase("01 jan 1970") || selectedYear.contains("1970")){
            selectedYear="";
        }
        _exchangeCar.sendExchangeCarData(selectedBrandId, selectedModelId, selectedModelName, selectedYear, selectedVariantId, selectedTotalRuns, selectedRegNum);
    }

    public interface ExchangeCar {
        void sendExchangeCarData(String brand, String model, String modelName, String year, String variant, String totalrun, String regNum);
    }

    private void initAdapter() {
        ArrayAdapter<String> brandsAdapterModel = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                getCarBrandModelForOld());
        autoTvInterExchangeCarModel.setAdapter(brandsAdapterModel);

        autoTvInterExchangeCarModel.setThreshold(0);

        autoTvInterExchangeCarModel.setOnFocusChangeListener(this);
    }


    private String[] getCarBrandModelForOld() {
        carModelListOld = realm.where(ExchangeCarBrandModelsDB.class).equalTo("category",WSConstants.CAR_CATEGORY_BOTH).or()
                .equalTo("category",WSConstants.CAR_CATEGORY_OLD_CAR).findAll();
        List<String> data = new ArrayList<>();
        for (int i = 0; i < carModelListOld.size(); i++) {
            if(carModelListOld.get(i).getModel_name()!=null){
                data.add(carModelListOld.get(i).getModel_name());
            }

        }
        String[] arr = new String[data.size()];
        for(int i=0;i<data.size();i++){
            arr[i] = data.get(i);
        }
        return arr;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus&& v instanceof AutoCompleteTextView && (((AutoCompleteTextView) v).getAdapter()!=null)) {
            ((AutoCompleteTextView) v).showDropDown();
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof AutoCompleteTextView) {
            ((AutoCompleteTextView) v).showDropDown();
        }
    }



}
