import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image, ScrollView, ImageBackground} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { TabNavigator } from 'react-navigation'
import { createMaterialTopTabNavigator } from 'react-navigation'
import RestClient from '../network/rest_client'
import { NavigationEvents } from "react-navigation";

const img = "https://tineye.com/images/widgets/mona.jpg";
global._userId = 0;
global.spotlight = 0;

import {eventLeaderBoard} from '../clevertap/CleverTapConstants';
import CleverTapPush from '../clevertap/CleverTapPush'

class Points extends React.Component {

	constructor(props)
		{
		    super(props);

		    this.state = {
		    	selected:0,
		    	result: {},
			}
		}


	componentDidMount(){

					if(global._userId) {
						  this.makeRemoteRequest(global._userId);
							this.logClevertapPoints();
					}
					else {
							Alert.alert("Failed to get user info");
					}


  			}

	logClevertapPoints() {
		if(global.spotlight == '1') {
			CleverTapPush.pushEvent(eventLeaderBoard.event, { 'LeaderBoard Type' :  eventLeaderBoard.keyType.valuesType.CONSULTANT_POINT_BREAK_UP_OWN});
		}
		else {
			CleverTapPush.pushEvent(eventLeaderBoard.event, { 'LeaderBoard Type' :  eventLeaderBoard.keyType.valuesType.CONSULTANT_POINT_BREAK_UP_OTHERS});
		}

	}


  			makeRemoteRequest = (userId) => {
	  			this.setState({ isLoading: true});
	  			new RestClient().getDsePoints({userId,date:getSelectedGameDate()}).then( (data) => {
			      if(data.statusCode == 4001 || data.statusCode == 5001) {
			                  Alert.alert(
			                'Failed',
			                'Please try again',
			                [
			                  {text: 'Logout', onPress: () => this.logout()},
			                  {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
			                ],
			                { cancelable: false }
			                )
			                  return;
			                }
			               console.log("uwala: "+JSON.stringify(data));
			               this.setState({
							    		isLoading: false,
							    		result:data.result
										});


			          }).catch((error) => {
			            console.error("Error Team Details"+ error)
			            if (error.response && error.response.status == 401) {
			                Alert.alert(
			              'Failed',
			              'Please try again',
			              [
			                {text: 'Logout', onPress: () => this.logout()},
			                {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
			              ],
			              { cancelable: false }
			            )
			              }
			          });
			}

	render () {

		if (this.state.isLoading) {
		      return (
		        <View style={{flex: 1, backgroundColor:'#fff', alignItems : 'center', justifyContent: 'center'}}>
		          <ActivityIndicator />
		        </View>
		      );
   			 }
		return (
			<View style={{flex: 10, width: '100%'}}>
			<NavigationEvents
				onWillFocus={payload => {
					this.logClevertapPoints();

				}}
			/>
				{ (this.state.result.category_points && this.state.result.category_points.length>0) ?
			    	<View style={{width: '100%', height: 30, alignItems : 'center', backgroundColor:'#566781',justifyContent: 'center', flexDirection: 'row'}}>
				    	<Text style={{flex: 2,  color: '#fff', textAlign: 'center'}}>Activity</Text>
				    	<Text style={{flex: 1.5,	color: '#fff', textAlign: 'center'}}>Point x Count</Text>
				    	<Text style={{flex: 1, 	color: '#63ffa3', textAlign: 'right', marginRight: 5}}>Total Points</Text>
			    	</View> : null}

			    	{ (this.state.result.category_points && this.state.result.category_points.length>0) ?
			    	<ScrollView>
			    	<FlatList
			    		ItemSeparatorComponent = {this.FlatListItemSeparator}
	    				data = {this.state.result.category_points}
	    				renderItem={({item, index}) =>(
	    					<View style={{justifyContent: 'center'}}>
		    				<View style= {{justifyContent: 'space-between', alignItems: 'center' , margin: 5, flexDirection: 'row'}}>
		    					<Text style={{flex:2, color: '#88a6cf', fontSize: 14, marginLeft: 10, textAlign: 'left'}}> {item.name} </Text>
		    					<View style={{width: 0.5,height: '100%', backgroundColor: "#527DB9"}}/>
		    					<View style= {{flex:1.5, flexDirection: 'column'}}>
		    						{item.points.map((info, i) => <Text key={i} style={{color: '#88a6cf', fontSize: 14, textAlign: 'center'}}> {item.name=='Bonus'?'':info.count+'X'+info.value}  </Text>)}
		    					</View>
		    					<View style={{width: 0.5,height: '100%', backgroundColor: '#527DB9'}}/>
		    					<View style= {{flex:1, flexDirection: 'column'}}>
		    						{item.points.map((info, i) => <Text key={i} style={{color: '#63ffa3', fontSize: 14, marginRight: 10, textAlign: 'center'}}> {item.name=='Bonus'?item.sum:info.total} </Text>)}
		    					</View>
		    				</View>
		    				<View
								        style={{
								          height: 0.5,
								          width: "100%",
								          backgroundColor: "#527DB9",
								        }}
								      />
		    				</View>
		    				)}
	    				keyExtractor= {(item, index) => index.toString() }
	    			 />

	    			 <View style={{width: '100%', height: 30, alignItems : 'center', justifyContent: 'center', flexDirection: 'row'}}>
	    			 	<Text style={{flex: 2,  color: '#88a6cf', textAlign: 'right', fontSize: 16, fontWeight: 'bold', marginRight: 10}}></Text>
				    	<Text style={{flex: 1.5,  color: '#88a6cf', textAlign: 'center', fontSize: 16, fontWeight: 'bold'}}>Total</Text>
				    	<Text style={{flex: 1,	color: '#63ffa3', textAlign: 'center', fontSize: 16, fontWeight: 'bold', marginRight: 10}}>{this.state.result.total}</Text>
			    	</View>
			    	</ScrollView>

			    :<Text style={{flex: 2,  color: '#fff', textAlign: 'center', fontSize: 16, justifyContent: 'center'}}>No Data Available</Text>}

		    	</View>
			)}

}

class Badges extends React.Component {

	constructor(props)
		{
		    super(props);

		    this.state = {
		    	selected:0,
		    	result: [],

			}
		}

		componentDidMount(){

					if(global._userId) {
						  this.makeRemoteRequest(global._userId);
					}
					else {
							Alert.alert("Failed to get user info");
					}

  			}

				logClevertapBadges() {
					if(global.spotlight == '1') {
						CleverTapPush.pushEvent(eventLeaderBoard.event, { 'LeaderBoard Type' :  eventLeaderBoard.keyType.valuesType.BADGE_OWN});
					}
					else {
						CleverTapPush.pushEvent(eventLeaderBoard.event, { 'LeaderBoard Type' :  eventLeaderBoard.keyType.valuesType.BADGE_OTHERS});
					}

				}

  			makeRemoteRequest = (userId) => {
	  			this.setState({ isLoading: true});
	  			new RestClient().getDseBadges({userId,date:getSelectedGameDate()}).then( (data) => {
			      if(data.statusCode == 4001 || data.statusCode == 5001) {
			                  Alert.alert(
			                'Failed',
			                'Please try again',
			                [
			                  {text: 'Logout', onPress: () => this.logout()},
			                  {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
			                ],
			                { cancelable: false }
			                )
			                  return;
			                }
			               console.log("uwala: Badge "+JSON.stringify(data));
			               this.setState({
							    		isLoading: false,
							    		result:data.result
										});


			          }).catch((error) => {
			            console.error("Error Team Details"+ error)
			            if (error.response && error.response.status == 401) {
			                Alert.alert(
			              'Failed',
			              'Please try again',
			              [
			                {text: 'Logout', onPress: () => this.logout()},
			                {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
			              ],
			              { cancelable: false }
			            )
			              }
			          });
			}

	renderSwitch(param, count) {
	  switch(param) {

	    case 1:
	      return 	(count == 0)? <Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_bulb.png')}
			        />:<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_bulb_mulitple.png')}
			        />;;
		case 2:
	      return 	(count == 0)?<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_car.png')}
			        />:<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_car_multiple.png')}
			        />;
		case 3:
	      return 	(count == 0)?<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_target.png')}
			        />:<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_target_multiple.png')}
			        />;
		case 4:
	      return 	(count == 0)?<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_fire.png')}
			        />:<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_fire_multiple.png')}
			        />;
		case 5:
	      return 	(count == 0)?<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_explorer.png')}
			        />:<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/explorers_multi_medal_badge.png')}
			        />;
		case 6:
	      return 	(count == 0)?<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_achiever.png')}
			        />:<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/achievers_multi_medal_badge.png')}
			        />;
		case 7:
	      return 	(count == 0)?<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_dynamo.png')}
			        />:<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/dynamos_multi_medal_badge.png')}
			        />;
		case 8:
	      return 	(count == 0)?<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_guru.png')}
			        />:<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/multi_guru_medal_badge.png')}
			        />;
		case 9:
	      return 	<Image
				        style={{ width: 25, height: 42}}
                      	source={require('../images/badge_trophy.png')}
			        />;

	    default:
	      return null;
	  }
	}

	render () {

		if (this.state.isLoading) {
		      return (
		        <View style={{flex: 1, backgroundColor:'#fff', alignItems : 'center', justifyContent: 'center'}}>
		          <ActivityIndicator />
		        </View>
		      );
   			 }
		return (

			<View style={{flex: 10, width: '100%'}}>
			<NavigationEvents
				onWillFocus={payload => {
					this.logClevertapBadges();

				}}
			/>
				<ScrollView>
			    		<View style= {{height: 200,alignItems: 'center', justifyContent: 'center', paddingTop: 5}}>
				    		<ImageBackground style={{flex: 5, width: '100%',justifyContent: 'center'}}
				    				source={require('../images/shelf.png')}>

							    	 <FlatList
					    				data = {this.state.result}
					    				renderItem={({item, index}) =>(
						    				(item.badge_count == 0)?null: <View style= {{flex:5, justifyContent: 'center', alignItems: 'center', margin:2,flexDirection: 'column'}}>

						    					<Text style={{color: '#fff', fontSize: 12}}> {(item.badge_count == 1)?item.name : item.name+'('+item.count+')'}</Text>
							    				{(item.badge_count == 1)?this.renderSwitch(item.id, 0):this.renderSwitch(item.id, 1)}

						    				</View>)}
					    				numColumns={3}
					    				keyExtractor= {(item, index) => index.toString() }
					    			 />

					    	</ImageBackground>
				    	</View>
				    	<View style={{flexDirection: 'column', margin: 5}}>

				    		<View style={{height: 40,flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
						    		<View style= {{flex:1, alignItems: 'center', margin: 5}}>
						    		<Image
						    			style={{ width: 40, height: 40}}
							            source={require('../images/badge_bulb.png')}/>

						    		</View>
						    		<View style= {{flex:3, flexDirection: 'column'}}>
						    			<Text style={{color: '#fff', fontWeight: 'bold'}}>Early Adapter </Text>
						    			<Text style={{color: '#fff'}}>Most active user in first 2 weeks</Text>

						    		</View>
				    		</View>
				    		<View style={{height: 40, flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
					    		<View style= {{flex:1, alignItems: 'center', margin: 5}}>
								<Image
					    			style={{ width: 40, height: 40}}
						            source={require('../images/badge_car.png')}/>

					    		</View>
					    		<View style= {{flex:3, flexDirection: 'column'}}>
					    			<Text style={{color: '#fff', fontWeight: 'bold'}}>Test Drive Target</Text>
					    			<Text style={{color: '#fff'}}>Achieve Test Drive Target</Text>

					    		</View>
				    		</View>
				    		<View style={{height: 40,flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
					    		<View style= {{flex:1, alignItems: 'center', margin: 5}}>
					    		<Image
					    			style={{ width: 40, height: 40}}
						            source={require('../images/badge_target.png')}/>

					    		</View>
					    		<View style= {{flex:3, flexDirection: 'column'}}>
					    			<Text style={{color: '#fff', fontWeight: 'bold'}}>Month Target Achieved</Text>
					    			<Text style={{color: '#fff'}}>Achieve Month Retail Target</Text>

					    		</View>
				    		</View>
				    		<View style={{height: 40,flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
					    		<View style= {{flex:1, alignItems: 'center', margin: 5}}>
					    		<Image
					    			style={{ width: 40, height: 40}}
						            source={require('../images/badge_fire.png')}/>

					    		</View>
					    		<View style= {{flex:3, flexDirection: 'column'}}>
					    			<Text style={{color: '#fff', fontWeight: 'bold'}}>2X Target</Text>
					    			<Text style={{color: '#fff'}}>Reach Twice Your Retail Target</Text>

					    		</View>
				    		</View>
				    		<View style={{height: 40, flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
					    		<View style= {{flex:1, alignItems: 'center', margin: 5}}>
					    		<Image
					    			style={{ width: 23, height: 40}}
						            source={require('../images/badge_explorer.png')}/>

					    		</View>
					    		<View style= {{flex:3, flexDirection: 'column'}}>
					    			<Text style={{color: '#fff', fontWeight: 'bold'}}>Best Explorer</Text>
					    			<Text style={{color: '#fff'}}>Become the Best DSE in Explorers Group</Text>

					    		</View>
				    		</View>
				    		<View style={{height: 40, flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
					    		<View style= {{flex:1, alignItems: 'center', margin: 5}}>
					    		<Image
					    			style={{ width: 23, height: 40}}
						            source={require('../images/badge_achiever.png')}/>

					    		</View>
					    		<View style= {{flex:3, flexDirection: 'column'}}>
					    			<Text style={{color: '#fff', fontWeight: 'bold'}}>Best Achiever</Text>
					    			<Text style={{color: '#fff'}}>Become the best DSE in Achievers Group</Text>

					    		</View>
				    		</View>
				    		<View style={{height: 40, flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
					    		<View style= {{flex:1, alignItems: 'center', margin: 5}}>
					    		<Image
					    			style={{ width: 23, height: 40}}
						            source={require('../images/badge_dynamo.png')}/>

					    		</View>
					    		<View style= {{flex:3, flexDirection: 'column'}}>
					    			<Text style={{color: '#fff', fontWeight: 'bold'}}>Best Dynamo</Text>
					    			<Text style={{color: '#fff'}}>Become the best DSE in Dynamos Group</Text>

					    		</View>
				    		</View>
				    		<View style={{height: 40, flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
					    		<View style= {{flex:1, alignItems: 'center', margin: 5}}>
					    		<Image
					    			style={{ width: 23, height: 40}}
						            source={require('../images/badge_guru.png')}/>

					    		</View>
					    		<View style= {{flex:3, flexDirection: 'column'}}>
					    			<Text style={{color: '#fff', fontWeight: 'bold'}}>Best Guru</Text>
					    			<Text style={{color: '#fff'}}>Become the best DSE in Gurus Group</Text>

					    		</View>
				    		</View>
				    	</View>
				    </ScrollView>

		    	</View>
			)}

}

const InnerTab = createMaterialTopTabNavigator({
			  Points: {screen:Points},
			  Badges: {screen:Badges}
	},
	{
	    swipeEnabled: true,
	    animationEnabled: false,
	    tabBarOptions: {
	      style: {
	        height: 50,
	        backgroundColor: '#0F2B52',
	      },
	      labelStyle:{
	      	fontSize:17
	      },
	     inactiveTintColor:'#fff',
	     activeTintColor:'#F5A623',
	     upperCaseLabel:false,
	     indicatorStyle:{opacity: 0},

	     }
	});

export default class ConsultantDetails extends React.Component {

	constructor(props)
		{
		    super(props);

		    this.state = {
		    	selected:0,
		    	result: {},
		    	userPointDetails : {},
			}
		}



		_selectedOption(value){
			console.log(value);
			//this.makeRemoteRequest();
			this.setState({selected:value});
		}

		render () {
			var userPointDetails = this.props.navigation.getParam('item');
			let userName = "";
			let userPoint = 0;
			let userPhoto = "";
			if(userPointDetails) {
				userName = userPointDetails.name;
				userPoint = userPointDetails.points;
				userPhoto = userPointDetails.dse_photo_url;

			}
			global._userId = userPointDetails.user_id;
			global.spotlight = userPointDetails.spotlight;
			let placeHolder = <View style={{position:'absolute',width:66, height:66, borderRadius: 66/2,backgroundColor:'#FF8000', alignItems:'center', justifyContent:'center'}}><Text style={{color:'#fff', fontSize: 20}}>{userName.charAt(0).toUpperCase()}</Text></View>;;
		return (
			<View style={{flex: 1, alignItems : 'center', backgroundColor:'#0F2B52',justifyContent: 'center'}}>
				<View style={{flex: 2, width: '100%', backgroundColor:'#0F2B52',flexDirection: 'row', padding: 5}}>
					<TouchableOpacity onPress = { ()=> this.props.navigation.goBack(null)}>
						<View style={{flex:1, justifyContent: 'center' , alignItems: 'center', marginLeft: 5}}>
			    			<Image
						        style={{width: 12, height: 15, justifyContent: 'center', alignItems: 'center', marginTop: 10}}
		                      	source={require('../images/ic_left_arrow.png')}
						        />
					     </View>
				     </TouchableOpacity>
			        <View style={{flex:7, marginTop:20, flexDirection: 'row', marginLeft: 15, marginTop: 10,alignItems: 'center'}}>
			        	{placeHolder}
			        	<Image
		                  style={{width: 66, height: 66, borderRadius: 66/2, alignItems: 'center'}}
		                  source={{uri: userPhoto}}
		                />

		             <View style={{flexDirection: 'column', marginTop: 5, marginLeft: 5}}>
		             	<Text style={{color: '#fff', fontWeight: 'bold', fontSize: 15}}> {userName} </Text>
		             	<Text style={{color: '#fff', fontSize: 15}}> {userPoint+' Points'} </Text>
		             </View>
			        </View>
			        <TouchableOpacity onPress={ () => {
								this.props.navigation.navigate('LeaderBoardHelp')
								if(global.spotlight == '1') {
									CleverTapPush.pushEvent(eventLeaderBoard.event, { 'LeaderBoard Type' :  eventLeaderBoard.keyType.valuesType.HELP_OWN});
								}
								else {
									CleverTapPush.pushEvent(eventLeaderBoard.event, { 'LeaderBoard Type' :  eventLeaderBoard.keyType.valuesType.HELP_OTHERS});
								}
							} }>
			        	<View style={{flex:2, justifyContent: 'center', alignItems: 'center', marginTop: 10, marginRight: 10}}>
			             <Image
					        style={{width: 20, height: 20, alignItems: 'center'}}
	                      	source={require('../images/ic_help.png')}
					        />
					    </View>
				     </TouchableOpacity>
	    		</View>
	    		<View style={{flex: 10, width: '100%'}}>
	    			<InnerTab/>
	    		</View>

	    	</View>
			);
	}
}

FlatListItemSeparator = () => {
	    return (
	      <View
	        style={{
	          height: 1,
	          width: "100%",
	          backgroundColor: "#ffffff",
	        }}
	      />
	    );
  }
