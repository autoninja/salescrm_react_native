import React, { Component } from 'react'
import { View, Text, } from 'react-native'

export default class LostDropReasonItem extends Component {
  render() {

    let name;
    let leadLost;
    let loss;
    if(this.props.data) {
      name = this.props.data.name;
      leadLost = this.props.data.leads_lost;
      loss = this.props.data.loss;
    }

    return (
      <View style= {{
        flex: 8,
        alignItems:'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingTop:20,
        paddingLeft:10,
        paddingBottom:20,
        paddingRight:10,
        borderBottomColor:'#E4E4E4',
        borderBottomWidth:1,
        alignItems:'center'}}>

        <View style={{justifyContent: "center",alignItems: "flex-start", paddingTop:4, paddingBottom:4, flex:4, borderRightColor:'#E4E4E4', borderRightWidth:1}}>
          <Text style= {{color:'#6C759B', fontSize:15}}>{name?name.trim()+"  ":""}</Text>
        </View>
        <View style={{justifyContent: "center",alignItems: "center", paddingTop:4, paddingBottom:4,  flex:2, borderRightColor:'#E4E4E4', borderRightWidth:1}}>
          <Text style= {{color:'#6C759B', fontSize:15}}>{leadLost+"  "}</Text>
        </View>
        <View style={{justifyContent: "center",alignItems: "center", paddingTop:4, paddingBottom:4,  flex:2,}}>
          <Text style= {{color:'#F20000', fontSize:15, textAlign:'center'}}>&#8377;{" "+loss+" "}</Text>
        </View>

      </View>


    );
  }
}
