package com.salescrm.telephony.fragments;

import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.NewFilterAdapter;
import com.salescrm.telephony.adapter.NewFilterAdapterSecond;
import com.salescrm.telephony.interfaces.NewFilterCallFragmentListener;
import com.salescrm.telephony.model.NewFilter.Filter;
import com.salescrm.telephony.utils.WSConstants;

/**
 * Created by prateek on 31/7/18.
 */

public class NewFilterFragmentSecond extends Fragment {
    private RecyclerView recyclerView;
    private NewFilterAdapterSecond newFilterAdapter;
    private Drawable drawable;
    private NewFilterCallFragmentListener newFilterCallFragmentListener;
    private int position;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.new_filter_fragment_second, container, false);
        recyclerView = (RecyclerView) rView.findViewById(R.id.recycler_view_filter_second);
        newFilterCallFragmentListener = (NewFilterCallFragmentListener) getActivity();
        drawable = VectorDrawableCompat.create(getActivity().getResources(), R.drawable.ic_back_btn_filter, null);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        position = getArguments().getString("pos")!= null ? Integer.parseInt(getArguments().getString("pos")):0;
        System.out.println("HmaaraBjaaj: "+ new Gson().toJson(WSConstants.filterSelection).toString());
        callServer(position);
        return rView;
    }

    private void callServer(int position) {
        newFilterAdapter = new NewFilterAdapterSecond(getActivity(), position);
        recyclerView.setAdapter(newFilterAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        newFilterCallFragmentListener.setOptions(WSConstants.filters.get(position).getName(), drawable);
        newFilterCallFragmentListener.setApply("Apply", 1);
    }
}
