import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';

export default class AutoDialog extends Component {
    close() {
        this.props.navigation.goBack();

    }
    render() {
        let { title, yes, no } = this.props.navigation.getParam('data');
        return (
            <TouchableWithoutFeedback onPress={() => {
                this.close();
                this.props.navigation.state.params.onNoButtonPress();
            }}>
                <View style={styles.overlay}>
                    <TouchableWithoutFeedback onPress={() => {
                    }}>
                        <View style={styles.main}>
                            <Text style={styles.title} >{title}</Text>
                            <View style={{

                                flexDirection: 'row',
                                justifyContent: 'center',
                                marginTop: 14,
                            }}>
                                <View style={{ flex: 1, backgroundColor: '#808080', borderRadius: 4 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.close();
                                            this.props.navigation.state.params.onNoButtonPress();
                                        }}
                                    >
                                        <View style={{ alignSelf: 'center', padding: 2 }}>
                                            <Text style={{ color: "#fff", fontSize: 18, paddingTop: 4, paddingBottom: 4 }}>{no}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flex: 1, marginLeft: 10, backgroundColor: '#007fff', borderRadius: 4 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.close();
                                            this.props.navigation.state.params.onYesButtonPress();
                                        }}
                                    >
                                        <View style={{ alignSelf: 'center', padding: 2 }}>
                                            <Text style={{ color: "#fff", fontSize: 18, paddingTop: 4, paddingBottom: 4 }}>{yes}</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,0.5)',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 30

    },
    main: {
        backgroundColor: '#fff',
        borderRadius: 5,
        width: '100%',
        padding: 16,
        maxHeight: '90%',
    },
    title: {
        lineHeight: 22,
        fontSize: 16,
        color: '#303030',
        paddingTop: 10,
        paddingLeft: 2,
        paddingRight: 2,
        paddingBottom: 10,

    },

});
