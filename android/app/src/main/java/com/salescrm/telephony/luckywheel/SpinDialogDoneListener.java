package com.salescrm.telephony.luckywheel;

/**
 * Created by prateek on 27/6/18.
 */

public interface SpinDialogDoneListener {
    void onOkPressed();
}
