package com.salescrm.telephony.db.change_lead_stages;

import io.realm.RealmObject;

/**
 * Created by prateek on 30/12/16.
 */

public class SavedChangedLeadStageDb extends RealmObject {

    private String selcetedPipeline;
    private String selectedStage;
    private String remarks;
    private String leadLastUpdate;
    private String reason;
    private String subReason;
    private String model;
    private int syncId;
    private int leadId;
    private String closingType;

    public String getClosingType() {
        return closingType;
    }

    public void setClosingType(String closingType) {
        this.closingType = closingType;
    }

    public int getLeadId() {
        return leadId;
    }

    public void setLeadId(int leadId) {
        this.leadId = leadId;
    }

    public String getSelcetedPipeline() {
        return selcetedPipeline;
    }

    public void setSelcetedPipeline(String selcetedPipeline) {
        this.selcetedPipeline = selcetedPipeline;
    }

    public String getSelectedStage() {
        return selectedStage;
    }

    public void setSelectedStage(String selectedStage) {
        this.selectedStage = selectedStage;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getLeadLastUpdate() {
        return leadLastUpdate;
    }

    public void setLeadLastUpdate(String leadLastUpdate) {
        this.leadLastUpdate = leadLastUpdate;
    }

    public int getSyncId() {
        return syncId;
    }

    public void setSyncId(int syncId) {
        this.syncId = syncId;
    }

    public String getSubReason() {
        return subReason;
    }

    public void setSubReason(String subReason) {
        this.subReason = subReason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
