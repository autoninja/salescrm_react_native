package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.model.FormSubmissionInputDataServer;

/**
 * Created by bannhi on 24/11/17.
 */

public interface TaskOtpResultListener {
    void onValidatingTaskOtp(boolean valid, int answerId, FormSubmissionInputDataServer.LocationData locationData);
}
