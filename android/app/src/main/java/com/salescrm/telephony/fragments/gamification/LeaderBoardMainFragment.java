package com.salescrm.telephony.fragments.gamification;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.GameLocationDB;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.FabricUtils;
import com.salescrm.telephony.utils.Util;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import io.realm.Realm;

/**
 * Created by bannhi on 14/12/17.
 */

public class LeaderBoardMainFragment extends Fragment {

    static ConnectionDetectorService connectionDetectorService;
    static Preferences pref;
    static Realm realm;
    ImageView consultantIcon, teamIcon, wallOfFameIcon, smIcon;
    TextView consultantText, teamText, wallOfFameText, smText;
    private RelativeLayout rel_loading_frame;
    private FrameLayout fragmentContainer;
    private LinearLayout toggleSM, toggleLeft, toggleRight, toggleMiddle;
    private Fragment leaderBoardSMFragment;
    private Fragment leaderBoardTeamFragment;
    private Fragment leaderBoardDseFragment;
    private Fragment leaderBoardWallOfFameFragment;

    private Drawable drawableSMActive, drawableSMInActive,
            drawableTeamActive, drawableTeamInActive,
            drawableConsultantActive, drawableConsultantInActive,
            drawableWallOfFameActvive, drawableWallOfFameInActive;

    private ImageView imgMonthForward, imgMonthBackward;
    private TextView tvGameMonth;

    private Calendar selectedGameCalender;
    private Calendar systemTime;
    private int lastPosition = 0;
    private Fragment lastFragment = null;
    private LinearLayout llCalender;
    private LeaderBoardHomeCommunication leaderboardHomeCommunication = null;


    public static LeaderBoardMainFragment newInstance(int i, String pending) {
        Bundle args = new Bundle();
        args.putInt("someInt", i);
        args.putString("someTitle", pending);
        LeaderBoardMainFragment fragment = new LeaderBoardMainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.leaderboard_fragment_layout, container, false);


        leaderBoardSMFragment = new LeaderBoardSMFragment();
        leaderBoardTeamFragment = new LeaderBoardTeamFragment();
        leaderBoardDseFragment = new LeaderBoardDseFragment();
        leaderBoardWallOfFameFragment = new LeaderBoardWallOfFameFragment();

        drawableSMActive = VectorDrawableCompat.create(getResources(), R.drawable.ic_leader_board_manager_active, getActivity().getTheme());
        drawableSMInActive = VectorDrawableCompat.create(getResources(), R.drawable.ic_leader_board_manager_in_active, getActivity().getTheme());
        drawableTeamActive = VectorDrawableCompat.create(getResources(), R.drawable.ic_leader_board_team_active, getActivity().getTheme());
        drawableTeamInActive = VectorDrawableCompat.create(getResources(), R.drawable.ic_leader_board_team_in_active, getActivity().getTheme());
        drawableConsultantActive = VectorDrawableCompat.create(getResources(), R.drawable.ic_leader_board_consultant_active, getActivity().getTheme());
        drawableConsultantInActive = VectorDrawableCompat.create(getResources(), R.drawable.ic_leader_board_consultant_in_active, getActivity().getTheme());
        drawableWallOfFameActvive = VectorDrawableCompat.create(getResources(), R.drawable.ic_leader_board_wall_active, getActivity().getTheme());
        drawableWallOfFameInActive = VectorDrawableCompat.create(getResources(), R.drawable.ic_leader_board_wall_in_active, getActivity().getTheme());
        llCalender = rView.findViewById(R.id.ll_game_calender);

        rel_loading_frame = (RelativeLayout) rView.findViewById(R.id.rel_loading_frame);
        fragmentContainer = (FrameLayout) rView.findViewById(R.id.fragment_container);
        toggleSM = (LinearLayout) rView.findViewById(R.id.smll);
        toggleLeft = (LinearLayout) rView.findViewById(R.id.leftll);
        toggleRight = (LinearLayout) rView.findViewById(R.id.rightll);
        toggleMiddle = (LinearLayout) rView.findViewById(R.id.midll);
        consultantIcon = (ImageView) rView.findViewById(R.id.consultant_icon);
        teamIcon = (ImageView) rView.findViewById(R.id.team_icon);
        wallOfFameIcon = (ImageView) rView.findViewById(R.id.wall_of_fame_icon);
        smIcon = (ImageView) rView.findViewById(R.id.sm_icon);
        consultantText = (TextView) rView.findViewById(R.id.consultant_text);
        teamText = (TextView) rView.findViewById(R.id.team_text);
        smText = (TextView) rView.findViewById(R.id.sm_text);
        wallOfFameText = (TextView) rView.findViewById(R.id.wall_of_fame_text);

        tvGameMonth = rView.findViewById(R.id.tv_game_month_year);
        imgMonthBackward = rView.findViewById(R.id.image_game_month_year_backward);
        imgMonthForward = rView.findViewById(R.id.image_game_month_year_forward);

        systemTime = Util.getCalender(pref.getSystemDate());
        selectedGameCalender = Calendar.getInstance();

        selectedGameCalender.set(Calendar.DATE, systemTime.get(Calendar.DATE));
        selectedGameCalender.set(Calendar.MONTH, systemTime.get(Calendar.MONTH));
        selectedGameCalender.set(Calendar.YEAR, systemTime.get(Calendar.YEAR));

        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        rel_loading_frame.setVisibility(View.VISIBLE);

        changeGameDateAction();

        if (getArguments() != null && getArguments().getBoolean("FROM_TASK_BOTTOM_RANK", false)) {
            changeFragment(2);
        } else {
            changeFragment(0);
        }


        toggleSM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rel_loading_frame.setVisibility(View.VISIBLE);
                changeFragment(0);

            }
        });

        toggleLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rel_loading_frame.setVisibility(View.VISIBLE);
                changeFragment(1);
            }
        });
        toggleMiddle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rel_loading_frame.setVisibility(View.VISIBLE);
                changeFragment(2);
            }
        });
        toggleRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rel_loading_frame.setVisibility(View.VISIBLE);
                changeFragment(3);
            }
        });

        imgMonthForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedGameCalender.add(Calendar.MONTH, 1);
                changeGameDateAction();
                refreshFragment();
            }
        });

        imgMonthBackward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pushCleverTap(CleverTapConstants.EVENT_LEADER_BOARD_TYPE_PREVIOUS_MONTH);
                selectedGameCalender.add(Calendar.MONTH, -1);
                changeGameDateAction();
                refreshFragment();
            }
        });


        return rView;
    }

    private void refreshFragment() {
        if (lastFragment != null) {
            final FragmentTransaction ft = getChildFragmentManager().beginTransaction();
            ft.detach(lastFragment);
            ft.attach(lastFragment);
            ft.commit();
        }
    }

    private void changeFragment(int pos) {
        rel_loading_frame.setVisibility(View.GONE);
        Fragment fragment = null;
        if (pos == 0) {
            llCalender.setVisibility(View.VISIBLE);
            smIcon.setImageDrawable(drawableSMActive);
            consultantIcon.setImageDrawable(drawableConsultantInActive);
            teamIcon.setImageDrawable(drawableTeamInActive);
            wallOfFameIcon.setImageDrawable(drawableWallOfFameInActive);
            consultantText.setTextColor(Color.parseColor("#FF606060"));
            teamText.setTextColor(Color.parseColor("#FF606060"));
            wallOfFameText.setTextColor(Color.parseColor("#FF606060"));
            smText.setTextColor(Color.parseColor("#FF1E3B64"));
            consultantText.setTextSize(11);
            teamText.setTextSize(11);
            wallOfFameText.setTextSize(11);
            smText.setTextSize(13);
            pref.setLeaderBoardTab(pos);
            fragment = leaderBoardSMFragment;
            pushCleverTap(CleverTapConstants.EVENT_LEADER_BOARD_TYPE_MANAGER_LEADERBOARD);


        } else if (pos == 1) {
            llCalender.setVisibility(View.VISIBLE);
            smIcon.setImageDrawable(drawableSMInActive);
            consultantIcon.setImageDrawable(drawableConsultantInActive);
            teamIcon.setImageDrawable(drawableTeamActive);
            wallOfFameIcon.setImageDrawable(drawableWallOfFameInActive);
            consultantText.setTextColor(Color.parseColor("#FF606060"));
            teamText.setTextColor(Color.parseColor("#FF1E3B64"));
            wallOfFameText.setTextColor(Color.parseColor("#FF606060"));
            smText.setTextColor(Color.parseColor("#FF606060"));
            consultantText.setTextSize(11);
            teamText.setTextSize(13);
            wallOfFameText.setTextSize(11);
            smText.setTextSize(11);
            pref.setLeaderBoardTab(pos);
            fragment = leaderBoardTeamFragment;
            pushCleverTap(CleverTapConstants.EVENT_LEADER_BOARD_TYPE_TEAM_LEADERBOARD);


        } else if (pos == 2) {
            llCalender.setVisibility(View.VISIBLE);
            smIcon.setImageDrawable(drawableSMInActive);
            consultantIcon.setImageDrawable(drawableConsultantActive);
            teamIcon.setImageDrawable(drawableTeamInActive);
            wallOfFameIcon.setImageDrawable(drawableWallOfFameInActive);
            consultantText.setTextColor(Color.parseColor("#FF1E3B64"));
            teamText.setTextColor(Color.parseColor("#FF606060"));
            wallOfFameText.setTextColor(Color.parseColor("#FF606060"));
            smText.setTextColor(Color.parseColor("#FF606060"));
            consultantText.setTextSize(13);
            teamText.setTextSize(11);
            wallOfFameText.setTextSize(11);
            smText.setTextSize(11);
            pref.setLeaderBoardTab(pos);
            fragment = leaderBoardDseFragment;


        } else if (pos == 3) {
            llCalender.setVisibility(View.GONE);
            smIcon.setImageDrawable(drawableSMInActive);
            consultantIcon.setImageDrawable(drawableConsultantInActive);
            teamIcon.setImageDrawable(drawableTeamInActive);
            wallOfFameIcon.setImageDrawable(drawableWallOfFameActvive);
            consultantText.setTextColor(Color.parseColor("#FF606060"));
            teamText.setTextColor(Color.parseColor("#FF606060"));
            wallOfFameText.setTextColor(Color.parseColor("#FF1E3B64"));
            smText.setTextColor(Color.parseColor("#FF606060"));
            consultantText.setTextSize(11);
            teamText.setTextSize(11);
            wallOfFameText.setTextSize(13);
            smText.setTextSize(11);
            pref.setLeaderBoardTab(pos);
            fragment = leaderBoardWallOfFameFragment;
            pushCleverTap(CleverTapConstants.EVENT_LEADER_BOARD_TYPE_WALL_OF_FAME);

        }

        if (fragment != null) {
            FragmentTransaction ft = getChildFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment);
            ft.commit();
        }

        lastPosition = pos;
        lastFragment = fragment;
        if(leaderboardHomeCommunication!=null) {
            leaderboardHomeCommunication.onLeaderBoardTabChange(pos);
        }
    }

    private void changeGameDateAction() {
        tvGameMonth.setText(String.format(Locale.getDefault(), "%s %d",
                selectedGameCalender.getDisplayName(Calendar.MONTH,
                        Calendar.SHORT, Locale.getDefault()),
                selectedGameCalender.get(Calendar.YEAR)));

        imgMonthBackward.setEnabled(true);
        imgMonthForward.setEnabled(true);
        imgMonthBackward.setAlpha(1f);
        imgMonthForward.setAlpha(1f);

        //Compare with game start date!!
        GameLocationDB gameLocationDB = realm.where(GameLocationDB.class).equalTo("id", pref.getmGameLocationId()).findFirst();
        Calendar backwardCalender = Calendar.getInstance();

        if (gameLocationDB == null || Util.getDate(gameLocationDB.getStart_date()) == null) {
            backwardCalender.setTimeInMillis(systemTime.getTimeInMillis());
        } else {
            backwardCalender.setTimeInMillis(Util.getDate(gameLocationDB.getStart_date()).getTime());
        }
        if (selectedGameCalender.get(Calendar.MONTH) == backwardCalender.get(Calendar.MONTH) &&
                selectedGameCalender.get(Calendar.YEAR) == backwardCalender.get(Calendar.YEAR)) {
            imgMonthBackward.setEnabled(false);
            imgMonthBackward.setAlpha(0.4f);
        }
        if (selectedGameCalender.get(Calendar.MONTH) == systemTime.get(Calendar.MONTH) &&
                selectedGameCalender.get(Calendar.YEAR) == systemTime.get(Calendar.YEAR)) {
            imgMonthForward.setEnabled(false);
            imgMonthForward.setAlpha(0.4f);
        }

        pref.setSelectedGameDate(selectedGameCalender.getTimeInMillis() + "");
    }

    private void pushCleverTap(String leaderBoardType) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_LEADER_BOARD_KEY_TYPE,
                leaderBoardType);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_LEADER_BOARD, hashMap);

        //Fabric Events
        FabricUtils.sendEvent(pref, "LeaderBoard-" + pref.getDealerName(), "Type", leaderBoardType);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context!=null && context instanceof LeaderBoardHomeCommunication) {
            leaderboardHomeCommunication = (LeaderBoardHomeCommunication) context;
        }
    }

    public interface LeaderBoardHomeCommunication {
        void onLeaderBoardTabChange(int pos);
    }
}
