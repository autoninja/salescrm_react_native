export const getPNTModel = (info) => {
    let ROYAL_ENFIELD_PNT_MODEL = {
        gridItems:
            [
                {
                    title: "Follow Up",
                    image: require("../../images/pnt/ic_pnt_call.png"),
                    fAnswerId: FormAnswerIds.FOLLOW_UP_CALL_RE
                },
                {
                    title: "Test Ride",
                    image: require("../../images/pnt/ic_pnt_test_ride.png"),
                    fAnswerId: FormAnswerIds.TEST_RIDE_INIT_RE,
                    childModels: {
                        gridItems: [
                            {
                                title: "Test Ride Home",
                                image: require("../../images/pnt/ic_pnt_home.png"),
                                fAnswerId: FormAnswerIds.TEST_RIDE_HOME_RE
                            },
                            {
                                title: "Test Ride Showroom",
                                image: require("../../images/pnt/ic_pnt_showroom.png"),
                                fAnswerId: FormAnswerIds.TEST_RIDE_SHOWROOM_RE
                            },
                            {
                                title: "Test Ride Experiential",
                                image: require("../../images/pnt/ic_pnt_test_ride_exp.png"),
                                fAnswerId: FormAnswerIds.TEST_RIDE_EXP_RE
                            },
                            {
                                title: "Test Ride Demo Day",
                                image: require("../../images/pnt/ic_pnt_test_ride_demo.png"),
                                fAnswerId: FormAnswerIds.TEST_RIDE_DEMO_RE
                            },
                        ]
                    }
                },
                {
                    title: "Dropout",
                    image: require("../../images/pnt/ic_pnt_lost_drop.png"),
                    fAnswerId: FormAnswerIds.DROP_OUT_RE
                },
                {
                    title: "Converted",
                    image: require("../../images/pnt/ic_pnt_book_car.png"),
                    fAnswerId: FormAnswerIds.BOOK_RE
                },
            ]
    }
    let TWO_WHEELER_PNT_MODEL = {
        gridItems:
            [
                {
                    title: "Call From Consultant",
                    image: require("../../images/pnt/ic_pnt_arrange_call.png"),
                    fAnswerId: FormAnswerIds.ARRANGE_CALL
                },
                {
                    title: "Follow Up",
                    image: require("../../images/pnt/ic_pnt_call.png"),
                    fAnswerId: FormAnswerIds.FOLLOW_UP_CALL
                },
                {
                    title: "Lost Drop",
                    image: require("../../images/pnt/ic_pnt_lost_drop.png"),
                    fAnswerId: FormAnswerIds.LOST_DROP
                },
                {
                    title: "Booked",
                    image: require("../../images/pnt/ic_pnt_book_car.png"),
                    fAnswerId: FormAnswerIds.BOOKED
                },
            ]
    }
    let I_LOM_PNT_MODEL = {
        gridItems:
            [
                {
                    title: "Call Back",
                    image: require("../../images/pnt/ic_pnt_callback.png"),
                    fAnswerId: FormAnswerIds.CALL_BACK
                },
                {
                    title: "Prospect",
                    image: require("../../images/pnt/ic_pnt_prospects.png"),
                    fAnswerId: FormAnswerIds.PROSPECT
                },
                {
                    title: "IL Renewed",
                    image: require("../../images/pnt/ic_pnt_il_renewed.png"),
                    fAnswerId: FormAnswerIds.IL_RENEWED
                },
                {
                    title: "Not Eligible/Not Interested",
                    image: require("../../images/pnt/ic_pnt_lost_drop.png"),
                    fAnswerId: FormAnswerIds.LOST_NOT_ELIGIBLE
                },
            ],
        mainAction: {
            title: "Sales Confirmed",
            image: require("../../images/pnt/ic_pnt_book_car.png"),
            fAnswerId: FormAnswerIds.SALES_CONFIRMED
        }
    }
    let I_LOM_BANK_USER_VERIFIER_PNT_MODEL = {
        gridItems:
            [
                {
                    title: "Sales Confirmed",
                    image: require("../../images/pnt/ic_pnt_book_car.png"),
                    fAnswerId: FormAnswerIds.SALES_CONFIRMED
                },
                {
                    title: "Back to Sale",
                    image: require("../../images/pnt/ic_pnt_back_to_sale.png"),
                    fAnswerId: FormAnswerIds.BACK_TO_SALE
                }
            ]
    }
    let I_LOM_BANK_PNT_MODEL = {
        gridItems:
            [
                {
                    title: "Call Back",
                    image: require("../../images/pnt/ic_pnt_callback.png"),
                    fAnswerId: FormAnswerIds.CALL_BACK
                },
                {
                    title: "Prospect",
                    image: require("../../images/pnt/ic_pnt_prospects.png"),
                    fAnswerId: FormAnswerIds.PROSPECT
                },
                {
                    title: "Verification",
                    image: require("../../images/pnt/ic_pnt_verification.png"),
                    fAnswerId: FormAnswerIds.VERIFICATION
                },
                {
                    title: "Not Eligible/Not Interested",
                    image: require("../../images/pnt/ic_pnt_lost_drop.png"),
                    fAnswerId: FormAnswerIds.LOST_NOT_ELIGIBLE
                },
            ]
    }

    let POST_BOOKING_PNT_MODEL = {
        gridItems:
            [
                {
                    title: "Test Drive",
                    image: require("../../images/pnt/ic_pnt_test_drive.png"),
                    fAnswerId: FormAnswerIds.TEST_DRIVE_INIT,
                    childModels: {
                        gridItems: [
                            {
                                title: "Test Drive Home",
                                image: require("../../images/pnt/ic_pnt_home.png"),
                                fAnswerId: FormAnswerIds.TEST_DRIVE_HOME
                            },
                            {
                                title: "Test Drive Showroom",
                                image: require("../../images/pnt/ic_pnt_showroom.png"),
                                fAnswerId: FormAnswerIds.TEST_DRIVE_SHOW_ROOM
                            },
                        ]
                    }
                },
                {
                    title: "Visit",
                    image: require("../../images/pnt/ic_pnt_visit.png"),
                    fAnswerId: FormAnswerIds.HOME_INIT,
                    childModels: {
                        gridItems: [
                            {
                                title: "Home Visit",
                                image: require("../../images/pnt/ic_pnt_home.png"),
                                fAnswerId: FormAnswerIds.HOME_VISIT
                            },
                            {
                                title: "Showroom Visit",
                                image: require("../../images/pnt/ic_pnt_showroom.png"),
                                fAnswerId: FormAnswerIds.SHOWROOM_VISIT
                            },
                        ]
                    }
                },
            ]
    }
    let GENERIC_PNT_MODEL = {
        gridItems:
            [
                {
                    title: "Follow Up",
                    image: require("../../images/pnt/ic_pnt_call.png"),
                    fAnswerId: FormAnswerIds.FOLLOW_UP_CALL
                },
                {
                    title: "Test Drive",
                    image: require("../../images/pnt/ic_pnt_test_drive.png"),
                    fAnswerId: FormAnswerIds.TEST_DRIVE_INIT,
                    childModels: {
                        gridItems: [
                            {
                                title: "Test Drive Home",
                                image: require("../../images/pnt/ic_pnt_home.png"),
                                fAnswerId: FormAnswerIds.TEST_DRIVE_HOME
                            },
                            {
                                title: "Test Drive Showroom",
                                image: require("../../images/pnt/ic_pnt_showroom.png"),
                                fAnswerId: FormAnswerIds.TEST_DRIVE_SHOW_ROOM
                            },
                        ]
                    }
                },
                {
                    title: "Visit",
                    image: require("../../images/pnt/ic_pnt_visit.png"),
                    fAnswerId: FormAnswerIds.HOME_INIT,
                    childModels: {
                        gridItems: [
                            {
                                title: "Home Visit",
                                image: require("../../images/pnt/ic_pnt_home.png"),
                                fAnswerId: FormAnswerIds.HOME_VISIT
                            },
                            {
                                title: "Showroom Visit",
                                image: require("../../images/pnt/ic_pnt_showroom.png"),
                                fAnswerId: FormAnswerIds.SHOWROOM_VISIT
                            },
                        ]
                    }
                },
                {
                    title: "Lost/Drop",
                    image: require("../../images/pnt/ic_pnt_lost_drop.png"),
                    fAnswerId: FormAnswerIds.LOST_DROP
                },
            ],
        mainAction: {
            title: "Book Car",
            image: require("../../images/pnt/ic_pnt_book_car.png"),
            fAnswerId: FormAnswerIds.BOOK_CAR_ID
        }
    }

    let pntModel = GENERIC_PNT_MODEL;
    if (info) {
        if (info.postBookingDriveOrVisit) {
            pntModel = POST_BOOKING_PNT_MODEL;
        }
        else if (info.isClientRoyalEnfield) {
            pntModel = ROYAL_ENFIELD_PNT_MODEL;
        }
        else if (info.isClientTwoWheeler) {
            pntModel = TWO_WHEELER_PNT_MODEL;
        }
        else if (info.isClientILom) {
            pntModel = I_LOM_PNT_MODEL;
            if (info.activityId == ActivityId.PROSPECT_CALL) {
                pntModel.gridItems.map((item) => {
                    if (item.fAnswerId == FormAnswerIds.CALL_BACK) {
                        item.disabled = true;
                    }
                });
            }

        }
        else if (info.isClientILomBank) {
            if (info.isUserVerifier) {
                pntModel = I_LOM_BANK_USER_VERIFIER_PNT_MODEL;
            }
            else {
                pntModel = I_LOM_BANK_PNT_MODEL;
                if (info.activityId == ActivityId.PROSPECT_CALL) {
                    pntModel.gridItems.map((item) => {
                        if (item.fAnswerId == FormAnswerIds.CALL_BACK) {
                            item.disabled = true;
                        }
                    });
                }
                if (info.activityId == ActivityId.BACK_TO_SALE) {
                    pntModel.gridItems.map((item) => {
                        if (item.fAnswerId == FormAnswerIds.CALL_BACK
                            || item.fAnswerId == FormAnswerIds.PROSPECT) {
                            item.disabled = true;
                        }
                    });
                }
            }
        }
    }
    return (
        pntModel
    )
}
const ActivityId = {
    PROSPECT_CALL: 6,
    BACK_TO_SALE: 60
}
export const FormAnswerIds = {
    FOLLOW_UP_CALL: 1,
    DISABLED: -100,
    TEST_DRIVE_INIT: 65, //
    HOME_INIT: 23,

    LOST_DROP: 24,

    //For second view
    TEST_DRIVE_HOME: 6,
    TEST_DRIVE_SHOW_ROOM: 5,
    HOME_VISIT: 2,
    SHOWROOM_VISIT: 3,
    BOOK_CAR_ID: 10,



    //For RoyalEnfield
    FOLLOW_UP_CALL_RE: 1,
    TEST_RIDE_INIT_RE: -11,
    DROP_OUT_RE: 24,
    BOOK_RE: -10,

    //Two Wheeler
    FIRST_CALL: 1,
    ARRANGE_CALL: 60,
    BOOKED: -12,

    //Second View
    TEST_RIDE_HOME_RE: 6,
    TEST_RIDE_SHOWROOM_RE: 5,
    TEST_RIDE_EXP_RE: 57,
    TEST_RIDE_DEMO_RE: 58,
    //RoyalEnfield fields ends here


    //ILom Activities
    CALL_BACK: 1,
    PROSPECT: 6, //
    IL_RENEWED: -1,
    LOST_NOT_ELIGIBLE: 24,
    SALES_CONFIRMED: -2,
    VERIFICATION: -101,
    BACK_TO_SALE: 60,
}
