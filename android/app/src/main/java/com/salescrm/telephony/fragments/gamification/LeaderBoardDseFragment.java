package com.salescrm.telephony.fragments.gamification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.gamification.LeaderBoardTabAdapter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.interfaces.RefreshCallBack;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.gamification.ConsultantLeaderBoardResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.services.service_handlers.GamificationServiceHandler;
import com.salescrm.telephony.utils.FabricUtils;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by nndra on 04-Jan-18.
 */

public class LeaderBoardDseFragment extends Fragment implements TabLayout.OnTabSelectedListener {

    static  private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView section_zero;
    // private ProgressBar progress;
    private RelativeLayout relLoading;

    private BroadcastReceiver broadcastReceiver;

    //private RelativeLayout rel_loading_frame;
    static ConnectionDetectorService connectionDetectorService;
    static SwipeRefreshLayout swipeRefreshLayout;
    static Preferences pref;
    static LeaderBoardTabAdapter adapter;
    ConsultantLeaderBoardResponse consultantLeaderBoardResponse;
    private boolean isRefreshRequested;
    private CallBack callback = new CallBack() {
        @Override
        public void onCallBack() {

        }

        @Override
        public void onCallBack(Object data) {
            if(data instanceof ConsultantLeaderBoardResponse) {
                consultantLeaderBoardResponse = (ConsultantLeaderBoardResponse) data;
                relLoading.setVisibility(View.GONE);
                section_zero.setVisibility(View.GONE);
                if(getActivity()!=null && !isRemoving() && !isDetached() && isAdded()) {
                    if(consultantLeaderBoardResponse!=null && consultantLeaderBoardResponse.getResult()!=null && consultantLeaderBoardResponse.getResult().getCategories().size() == 0) {
                        if(isSelectedMonth()) {
                            section_zero.setVisibility(View.GONE);
                            String month = Util.getCalender(pref.getSystemDate()).getDisplayName(Calendar.MONTH,
                                    Calendar.LONG, Locale.getDefault());
                            tvTargetSet.setText("Game for "+month+" started !");
                            tvActivitiesSet.setText("All activities marked from "+month+" 1st will be taken care when we will allot points for month");
                            tvBoosterSet.setText("Booster period is ON till 10th "+month+" so all activities will give you double points!");
                            relGameJustStarted.setVisibility(View.VISIBLE);
                        }
                        else {
                            section_zero.setVisibility(View.VISIBLE);
                            section_zero.setText("No Data");
                        }
                        HashMap<String, Object> hashMap = new HashMap<String, Object>();
                        hashMap.put(CleverTapConstants.EVENT_LEADER_BOARD_KEY_TYPE,
                                CleverTapConstants.EVENT_LEADER_BOARD_TYPE_CONSULTANT_POINT_BREAK_UP_OWN);
                        CleverTapPush.pushEvent(CleverTapConstants.EVENT_LEADER_BOARD, hashMap);
                        //Fabric Events
                        FabricUtils.sendEvent(pref, "LeaderBoard-"+pref.getDealerName(),
                                "Type", CleverTapConstants.EVENT_LEADER_BOARD_TYPE_CONSULTANT_POINT_BREAK_UP_OWN);
                    }
                    else {
                        setDseAdapter();
                    }

                }
            }
        }

        @Override
        public void onError(String error) {

        }
    };
    private int prevTab = -1;
    private String monthYear;
    private RelativeLayout relGameJustStarted;
    private TextView tvTargetSet,tvActivitiesSet,tvBoosterSet;

    public static LeaderBoardDseFragment newInstance(int i, int pending) {
        Bundle args = new Bundle();
        args.putInt("someInt", i);
        args.putInt("someTitle", pending);
        LeaderBoardDseFragment fragment = new LeaderBoardDseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.game_cons_lyt, container, false);

        viewPager = (ViewPager) rView.findViewById(R.id.gamification_viewpager);
        tabLayout = (TabLayout) rView.findViewById(R.id.gamification_sliding_tabs);
        section_zero = (TextView) rView.findViewById(R.id.section_zero);
        relGameJustStarted = (RelativeLayout) rView.findViewById(R.id.rel_game_not_started);
        tvTargetSet = (TextView) rView.findViewById(R.id.tv_game_not_started_target_set);
        tvActivitiesSet = (TextView) rView.findViewById(R.id.tv_game_not_started_activities_details);
        tvBoosterSet = (TextView) rView.findViewById(R.id.tv_game_not_started_booster_details);


        //progress = (ProgressBar) rView.findViewById(R.id.pbProgress);
        relLoading = (RelativeLayout) rView.findViewById(R.id.gamification_rel_loading_frame);


        relGameJustStarted.setVisibility(View.GONE);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        monthYear = Util.getMonthYear(pref.getSelectedGameDate());

        if (pref.getmGameLocationId() == -1 ||  !Util.isNotNull(pref.getGameLocationName())) {
            if(DbUtils.getLocationsId().size()>0) {
                pref.setmGameLocationId(DbUtils.getLocationsId().get(0));
                pref.setGameLocationName(DbUtils.getLocationsName().get(0));
            }
        }


        //rel_loading_frame = (RelativeLayout) rView.findViewById(R.id.rel_loading_frame);

        /*broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle args = intent.getBundleExtra("DATA");
                consultantLeaderBoardResponse = (ConsultantLeaderBoardResponse) args.getSerializable("dseDataResponse");
                int homeActivity = args.getInt("homeActivity");
                System.out.println("HOME ACTIVITY"+homeActivity);
                if(homeActivity==1){
                    System.out.println("getActivity:"+getActivity());


                }


            }
        };*/
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(WSConstants.dseDataFetchedIntent));
        System.out.println("API_UTIL - prefGame locationID - "+pref.getmGameLocationId());
        callApi();
        return rView;
    }

    private void setDseAdapter(){
        adapter = new LeaderBoardTabAdapter(getChildFragmentManager(), new RefreshCallBack() {
            @Override
            public void onRefresh() {
                isRefreshRequested = true;
                callApi();
            }
        });
        int pos = 0;
        for(int i = 0 ; i < consultantLeaderBoardResponse.getResult().getCategories().size();i++) {
            adapter.setTab(consultantLeaderBoardResponse.getResult().getCategories().get(i).getName(),
                    consultantLeaderBoardResponse.getResult().getCategories());
            if(consultantLeaderBoardResponse.getResult().getCategories().get(i).getSpotlight()==1){
                pos = i;
            }
        }
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setCurrentItem(pos);
        tabLayout.removeOnTabSelectedListener(this);
        tabLayout.addOnTabSelectedListener(this);
        tabLayout.setupWithViewPager(viewPager);

        changeTabTextStyle();

        isRefreshRequested = false;

    }
    private void changeTabTextStyle() {
        try {
            if (prevTab != -1) {
                ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(prevTab)))).getChildAt(1))).setTypeface(null, Typeface.NORMAL);
            }
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(tabLayout.getSelectedTabPosition())))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
        }
        catch (Exception ignored) {

        }
            //((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(prevTab)))).getChildAt(1))).setTypeface(null, Typeface.NORMAL);
         prevTab=tabLayout.getSelectedTabPosition();

    }
    private void callApi() {
        if(new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            relLoading.setVisibility(View.VISIBLE);
            section_zero.setVisibility(View.GONE);
            GamificationServiceHandler.getInstance(getActivity())
                    .fetchConsultantData(pref.getAccessToken(), pref.getmGameLocationId(), monthYear, callback);
        }else{
            System.out.println("Error No Internet 1");
            section_zero.setVisibility(View.VISIBLE);
            section_zero.setText("Please Enable Internet Connection");
            Util.showToast(getContext(),"Please Enable Internet Connection",Toast.LENGTH_LONG);
        }
    }

    private void pushCleverTap() {
        String leaderBoardType;
        if(!isRefreshRequested && consultantLeaderBoardResponse!=null && consultantLeaderBoardResponse.getResult()!=null) {
            if(consultantLeaderBoardResponse.getResult().getCategories().size()>tabLayout.getSelectedTabPosition()) {
                ConsultantLeaderBoardResponse.Categories categoryData = consultantLeaderBoardResponse.getResult().getCategories().get(tabLayout.getSelectedTabPosition());
                if(categoryData.getSpotlight() == 1) {
                    leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_CONSULTANT_LEADERBOARD_OWN;
                }
                else {
                    leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_CONSULTANT_LEADERBOARD_OTHERS;
                }
                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put(CleverTapConstants.EVENT_LEADER_BOARD_KEY_TYPE,
                        leaderBoardType);
                CleverTapPush.pushEvent(CleverTapConstants.EVENT_LEADER_BOARD, hashMap);

                //Fabric Events
                FabricUtils.sendEvent(pref, "LeaderBoard-"+pref.getDealerName(),
                        "Type", leaderBoardType);

            }


        }




    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        changeTabTextStyle();
        pushCleverTap();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

    private boolean isSelectedMonth() {
        Calendar selectedCalender = Util.getCalender(pref.getSelectedGameDate());
        Calendar systemCalender = Util.getCalender(pref.getSystemDate());
        return selectedCalender.get(Calendar.MONTH) == systemCalender.get(Calendar.MONTH) && selectedCalender.get(Calendar.YEAR) == selectedCalender.get(Calendar.YEAR);
    }
}
