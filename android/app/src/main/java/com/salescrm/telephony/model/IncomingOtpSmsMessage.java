package com.salescrm.telephony.model;

/**
 * Created by bharath on 1/8/16.
 */
public class IncomingOtpSmsMessage {
    private String otp;

    public IncomingOtpSmsMessage(String s) {
        this.otp=s;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
