package com.salescrm.telephony.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.TeamShuffleGridDialogAdapter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.UserLocationPickerCallBack;
import com.salescrm.telephony.model.ApiGenericModelResponse;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.model.ManagerHierarchyUserModel;
import com.salescrm.telephony.model.TeamShuffleModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.CircleTransform;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;
import com.salescrm.telephony.views.GridLayoutItemDecorator;
import com.salescrm.telephony.views.UserLocationPicker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by bharath on 1/9/17.
 */

public class TeamShuffleFragment extends Fragment implements View.OnClickListener {
    private static final int ALERT_TYPE_PERMISSION = 2;
    private static final int ALERT_TYPE_MOVE_USER = 3;
    private static final int ALERT_TYPE_DEACTIVATE_USER = 4;
    private TeamShuffleModel teamShuffleModel;
    private LinearLayout llParent;
    private int colorLightBlack, colorGrey, colorWhite, colorBlue, colorBlack;
    private int leftMarginSalesConsultant;
    private int leftMarginTeamLead, topMarginTeamLead, textSizeSelected, textSizeNormal;
    private ProgressDialog progressDialog;
    private RelativeLayout relLoader;
    private Preferences pref;
    private int lastSelectedUserPosition = -1;
    private TeamShuffleGridDialogAdapter adapterDialog;
    private Realm realm;
    private boolean isUserSalesManager = false;
    private boolean isUserBranchHead = false;
    private boolean isDeactivateUser;
    private TeamShuffleModel.Item selectedToItem;
    private AdapterView<?> parentGridClicked = null;
    private UserLocationsDB userLocationsDB;
    private RelativeLayout frameAlert;
    private TextView tvAlertAction;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedLayout = inflater.inflate(R.layout.fragment_team_shuffle, container, false);
        pref = Preferences.getInstance();
        pref.load(getContext());
        realm = Realm.getDefaultInstance();

        isUserBranchHead = isUserBranchHead();
        isUserSalesManager = isUserSalesManager();


        llParent = (LinearLayout) inflatedLayout.findViewById(R.id.ll_team_shuffle);
        relLoader = (RelativeLayout) inflatedLayout.findViewById(R.id.rel_shuffle_user_loader);
        frameAlert = (RelativeLayout) inflatedLayout.findViewById(R.id.frame_team_shuffle_alert);
        tvAlertAction = (TextView) inflatedLayout.findViewById(R.id.tv_team_shuffle_alert_action);

        teamShuffleModel = TeamShuffleModel.getInstance();
        this.colorLightBlack = Color.parseColor("#FF252525");
        this.colorGrey = Color.parseColor("#FF656565");
        this.colorWhite = Color.parseColor("#FFFFFFFF");
        this.colorBlue = Color.parseColor("#FF2E5C86");
        this.colorBlack = Color.parseColor("#FF3F3F3F");
        this.leftMarginSalesConsultant = (int) Util.convertDpToPixel(20, getContext());
        this.leftMarginTeamLead = (int) Util.convertDpToPixel(10, getContext());
        this.topMarginTeamLead = (int) Util.convertDpToPixel(4, getContext());

        this.textSizeSelected = (int) Util.convertDpToPixel(19, getContext());
        this.textSizeNormal = (int) Util.convertDpToPixel(17, getContext());

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        // Create a grid layout with two columns

        tvAlertAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickLocation();
            }
        });
        pickLocation();

        return inflatedLayout;
    }

    private void pickLocation() {

        new UserLocationPicker().show(getContext(), new UserLocationPickerCallBack() {
            @Override
            public void onPickLocation(UserLocationsDB locationsDB) {
                userLocationsDB = locationsDB;
                init();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void init() {

        if (userLocationsDB == null) {
            frameAlert.setVisibility(View.VISIBLE);
            return;
        }

        frameAlert.setVisibility(View.GONE);
        llParent.removeAllViews();
        relLoader.setVisibility(View.VISIBLE);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getAllManagerHierarchyUsers(Util.getInt(userLocationsDB.getLocation_id()), new Callback<ManagerHierarchyUserModel>() {
            @Override
            public void success(ManagerHierarchyUserModel managerHierarchyUserModel, Response response) {
                Util.updateHeaders(response.getHeaders());
                if (managerHierarchyUserModel.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)
                        && managerHierarchyUserModel.getResult() != null) {
                    dismissProgressDialog();
                    showData(managerHierarchyUserModel.getResult());
                } else {
                    showErrorDialog("Error while loading user lists", 0);
                }
            }

            private void showData(List<ManagerHierarchyUserModel.Result> results) {
                TeamShuffleModel.Item itemMain = teamShuffleModel.new Item();
                itemMain.setType(0);
                List<TeamShuffleModel.Item> dataList = new ArrayList<>();
                if (isUserBranchHead()) {
                    for (int i = 0; i < results.size(); i++) {
                        dataList.add(getManagerList(results.get(i)));
                    }
                } else if (isUserSalesManager()) {
                    for (int i = 0; i < results.size(); i++) {
                        if (pref.getAppUserId() != null &&
                                results.get(i).getManager_id() != null &&
                                results.get(i).getManager_id().intValue() == pref.getAppUserId()) {
                            dataList.addAll(getTeamLeadList(results.get(i)));
                            break;
                        }
                    }
                } else {
                    showErrorDialog("There is some issue with permission!", ALERT_TYPE_PERMISSION);
                }
                itemMain.setChildItem(dataList);
                teamShuffleModel.setUserTeamShuffle(itemMain);
                if (getActivity() != null && !getActivity().isFinishing()) {
                    createViewDynamically(teamShuffleModel.getUserTeamShuffle(), llParent);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showErrorDialog("Error while loading user lists", 0);
            }
        });

    }

    private void showErrorDialog(String error, final int from) {
        dismissProgressDialog();
        new AutoDialog(getContext(), new AutoDialogClickListener() {
            @Override
            public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
                if (from == 0) {
                    init();
                }
            }


            @Override
            public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {

            }
        }, new AutoDialogModel(error, "Retry", "Close")).show();

    }

    private void showErrorDialog(String error, final int from, String yes, String no) {
        dismissProgressDialog();
        new AutoDialog(getContext(), new AutoDialogClickListener() {
            @Override
            public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
                if (from == 0) {
                    init();
                }
            }


            @Override
            public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {

            }
        }, new AutoDialogModel(error, yes, no)).show();

    }

    private void dismissProgressDialog() {
        relLoader.setVisibility(View.GONE);
        if (getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

    }

    private TeamShuffleModel.Item getManagerList(ManagerHierarchyUserModel.Result result) {
        TeamShuffleModel.Item itemMain = teamShuffleModel.new Item();
        itemMain.setType(1);
        itemMain.setTitle(result.getName());
        itemMain.setId(result.getManager_id());
        itemMain.setImgUrl(result.getImage_url());
        itemMain.setLocationId(result.getLocation_id());
        List<TeamShuffleModel.Item> userItemsTL = new ArrayList<>();
        for (int i = 0; i < result.getTeam_leaders().size(); i++) {
            TeamShuffleModel.Item itemTL = teamShuffleModel.new Item();
            itemTL.setType(2);
            itemTL.setParentId(result.getManager_id());
            itemTL.setTitle(result.getTeam_leaders().get(i).getTeam_leader_name());
            itemTL.setImgUrl(result.getTeam_leaders().get(i).getImage_url());
            itemTL.setTeamId(result.getTeam_leaders().get(i).getTeam_id());
            itemTL.setId(result.getTeam_leaders().get(i).getTeam_leader_id());
            itemTL.setLocationId(result.getLocation_id());
            //- ---- SC  ----
            List<TeamShuffleModel.Item> userItemsSC = new ArrayList<>();
            for (int j = 0; j < result.getTeam_leaders().get(i).getUsers().size(); j++) {
                TeamShuffleModel.Item itemSC = teamShuffleModel.new Item();
                itemSC.setType(3);
                itemSC.setParentId(result.getTeam_leaders().get(i).getTeam_leader_id());
                itemSC.setTitle(result.getTeam_leaders().get(i).getUsers().get(j).getName());
                itemSC.setImgUrl(result.getTeam_leaders().get(i).getUsers().get(j).getImage_url());
                itemSC.setId(result.getTeam_leaders().get(i).getUsers().get(j).getId());
                itemSC.setLocationId(result.getLocation_id());
                userItemsSC.add(itemSC);

            }
            itemTL.setChildItem(userItemsSC);
            //- ---- SC  ----

            userItemsTL.add(itemTL);

        }

        itemMain.setChildItem(userItemsTL);
        return itemMain;
    }

    private List<TeamShuffleModel.Item> getTeamLeadList(ManagerHierarchyUserModel.Result result) {

        List<TeamShuffleModel.Item> userItemsTL = new ArrayList<>();
        for (int i = 0; i < result.getTeam_leaders().size(); i++) {
            TeamShuffleModel.Item itemTL = teamShuffleModel.new Item();
            itemTL.setType(2);
            itemTL.setParentId(result.getManager_id());
            itemTL.setTitle(result.getTeam_leaders().get(i).getTeam_leader_name());
            itemTL.setImgUrl(result.getTeam_leaders().get(i).getImage_url());
            itemTL.setTeamId(result.getTeam_leaders().get(i).getTeam_id());
            itemTL.setId(result.getTeam_leaders().get(i).getTeam_leader_id());
            itemTL.setLocationId(result.getLocation_id());
            //- ---- SC  ----
            List<TeamShuffleModel.Item> userItemsSC = new ArrayList<>();
            for (int j = 0; j < result.getTeam_leaders().get(i).getUsers().size(); j++) {
                TeamShuffleModel.Item itemSC = teamShuffleModel.new Item();
                itemSC.setType(3);
                itemSC.setParentId(result.getTeam_leaders().get(i).getTeam_leader_id());
                itemSC.setTitle(result.getTeam_leaders().get(i).getUsers().get(j).getName());
                itemSC.setImgUrl(result.getTeam_leaders().get(i).getUsers().get(j).getImage_url());
                itemSC.setId(result.getTeam_leaders().get(i).getUsers().get(j).getId());
                itemSC.setLocationId(result.getLocation_id());
                userItemsSC.add(itemSC);

            }
            itemTL.setChildItem(userItemsSC);
            //- ---- SC  ----

            userItemsTL.add(itemTL);

        }

        return userItemsTL;
    }

    private View getUserCardView(TeamShuffleModel.Item item) {
        int layout = R.layout.layout_user_card_sales_consultant;

        switch (item.getType()) {
            case 1:
                layout = R.layout.layout_user_card_sales_manager;
                break;
            case 2:
                layout = R.layout.layout_user_card_team_lead;
                break;
            case 3:
                layout = R.layout.layout_user_card_sales_consultant;
                break;


        }
        View view = LayoutInflater.from(getContext()).inflate(layout, null, false);
        TextView tvName = (TextView) view.findViewById(R.id.tv_team_shuffle_user);
        TextView tvInitial = (TextView) view.findViewById(R.id.tv_team_shuffle_user_initial);
        setImage(item.getImgUrl(), (ImageView) view.findViewById(R.id.img_team_shuffle_user), tvInitial);
        tvName.setText(item.getTitle());
        tvInitial.setText(item.getTitle() != null && item.getTitle().length() > 0 ? String.valueOf(item.getTitle().charAt(0)) : "");
        return view;
    }

    private void createViewDynamically(TeamShuffleModel.Item item, LinearLayout parent) {
        //Sales Manager || //Team Lead
        if (item.getType() == 1 || item.getType() == 2) {
            View view = getUserCardView(item);
            parent.addView(view);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            if (item.getType() == 2) {
                params.topMargin = isUserBranchHead ? topMarginTeamLead : 0;
                params.bottomMargin = isUserBranchHead ? topMarginTeamLead : 0;
                params.leftMargin = isUserBranchHead ? leftMarginTeamLead : 0;
                view.setTag(item);
                if (isUserBranchHead) {
                    view.setOnClickListener(this);
                }
            }
        } else if (item.getType() == 3) {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    50f
            );
            View view = getUserCardView(item);
            view.setLayoutParams(param);
            view.setTag(item);
            view.setOnClickListener(this);
            parent.addView(view);

        }


        if (item.getChildItem() != null && item.getChildItem().size() > 0) {

            if (item.getChildItem().get(0).getType() == 3) {
                for (int i = 0; i < item.getChildItem().size(); i += 2) {

                    LinearLayout linearLayout = new LinearLayout(getContext());
                    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                    linearLayout.setWeightSum(100f);


                    createViewDynamically(item.getChildItem().get(i), linearLayout);
                    if (i + 1 < item.getChildItem().size()) {
                        createViewDynamically(item.getChildItem().get(i + 1), linearLayout);
                    }

                    llParent.addView(linearLayout);
                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) linearLayout.getLayoutParams();
                    params.leftMargin = isUserBranchHead ? leftMarginSalesConsultant : leftMarginTeamLead;
                }

            } else {
                for (TeamShuffleModel.Item itemData : item.getChildItem()) {
                    createViewDynamically(itemData, llParent);
                }
            }
        }

    }


    private void moveDseDialog(final TeamShuffleModel.Item item) {


        if (null == item) {
            Util.showToast(getContext(), "Please try later", Toast.LENGTH_SHORT);
            return;
        } else if (item.getId().intValue() == item.getParentId().intValue()) {
            if (item.getType() == 3) {
                showErrorDialog(item.getTitle() + " is also a TL, you can not move to another TL", -1, "Ok", "Close");
                return;
            } else if (item.getType() == 2) {
                showErrorDialog(item.getTitle() + " is also a Sales Manager, you can not move to another Sales Manager", -1, "Ok", "Close");
                return;
            }

        }
        final List<TeamShuffleModel.Item> itemDialog = new ArrayList<>();
        itemDialog.addAll(getDialogData(item));
        lastSelectedUserPosition = -1;
        isDeactivateUser = false;
        selectedToItem = null;

        final Dialog dialog = new Dialog(getContext(), R.style.AppTheme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }


        dialog.setContentView(R.layout.layout_move_user_dialog);

        TextView textView = (TextView) dialog.findViewById(R.id.tv_shuffle_user_selected);
        final TextView textViewDeactivate = (TextView) dialog.findViewById(R.id.tv_shuffle_user_action_deactivate);
        final CardView cardViewDeactivate = (CardView) dialog.findViewById(R.id.card_shuffle_user_action_deactivate);

        cardViewDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDeactivateUser = !isDeactivateUser;
                makeDefaultGridClicked(itemDialog);
                if (isDeactivateUser) {
                    cardViewDeactivate.setCardBackgroundColor(Color.parseColor("#FFD0021B"));
                    textViewDeactivate.setTextColor(Color.WHITE);
                } else {
                    cardViewDeactivate.setCardBackgroundColor(Color.parseColor("#FFD7D7D7"));
                    textViewDeactivate.setTextColor(Color.parseColor("#FFD0021B"));
                }
            }
        });

        dialog.findViewById(R.id.tv_shuffle_user_action_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.findViewById(R.id.tv_shuffle_user_action_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDeactivateUser) {
                    deactivateUser();
                } else if (selectedToItem != null) {
                    moveUser();
                } else {
                    Util.showToast(getContext(), "Please select the operation", Toast.LENGTH_SHORT);
                }


            }

            private void moveUser() {
                logEvent(CleverTapConstants.EVENT_SHUFFLE_TYPE_SHUFFLE);
                progressDialog.setMessage("Please wait..");
                progressDialog.show();
                if (item.getType() == 3) {
                    ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).moveUser(item.getId(), selectedToItem.getTeamId(), true, new Callback<ApiGenericModelResponse>() {
                        @Override
                        public void success(ApiGenericModelResponse moveUserResponse, Response response) {
                            Util.updateHeaders(response.getHeaders());
                            if (moveUserResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)
                                    && moveUserResponse.getResult() != null
                                    && moveUserResponse.getResult().equalsIgnoreCase("1")) {
                                dialog.dismiss();
                                dismissProgressDialog();
                                Util.showToast(getContext(), item.getTitle() + " has been successfully moved", Toast.LENGTH_LONG);
                                init();
                            } else {
                                showErrorDialogInside("Error while moving " + item.getTitle(), ALERT_TYPE_MOVE_USER);
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            showErrorDialogInside("Error while moving " + item.getTitle(), ALERT_TYPE_MOVE_USER);
                        }
                    });
                } else if (item.getType() == 2) {
                    ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).moveUser(item.getId(), item.getParentId(), selectedToItem.getId(), new Callback<ApiGenericModelResponse>() {
                        @Override
                        public void success(ApiGenericModelResponse moveUserResponse, Response response) {
                            Util.updateHeaders(response.getHeaders());
                            if (moveUserResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)
                                    && moveUserResponse.getResult() != null
                                    && moveUserResponse.getResult().equalsIgnoreCase("1")) {
                                dialog.dismiss();
                                dismissProgressDialog();
                                Util.showToast(getContext(), item.getTitle() + " has been successfully moved", Toast.LENGTH_LONG);
                                init();
                            } else {
                                showErrorDialogInside("Error while moving " + item.getTitle() + "\nMessage : " + moveUserResponse.getMessage(), ALERT_TYPE_MOVE_USER);
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            showErrorDialogInside("Error while moving " + item.getTitle(), ALERT_TYPE_MOVE_USER);
                        }
                    });
                }
            }

            private void showErrorDialogInside(String error, final int from) {
                dismissProgressDialog();
                new AutoDialog(getContext(), new AutoDialogClickListener() {
                    @Override
                    public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
                        if (from == ALERT_TYPE_MOVE_USER) {
                            moveUser();
                        } else if (from == ALERT_TYPE_DEACTIVATE_USER) {
                            deactivateUser();
                        }
                    }


                    @Override
                    public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {

                    }
                }, new AutoDialogModel(error, "Retry", "Close")).show();
            }

            private void deactivateUser() {
                logEvent(CleverTapConstants.EVENT_SHUFFLE_TYPE_DEACTIVATE_USER);
                progressDialog.setMessage("Deactivating " + item.getTitle() + "\nPlease wait..");
                progressDialog.show();
                ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).deactivateUser(item.getId(), new Callback<ApiGenericModelResponse>() {
                    @Override
                    public void success(ApiGenericModelResponse moveUserResponse, Response response) {
                        Util.updateHeaders(response.getHeaders());
                        if (moveUserResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)
                                && moveUserResponse.getResult() != null && moveUserResponse.getResult().equalsIgnoreCase("1")) {
                            dialog.dismiss();
                            dismissProgressDialog();
                            Util.showToast(getContext(), item.getTitle() + " has been successfully deactivated", Toast.LENGTH_LONG);
                            init();
                        } else {
                            showErrorDialogInside("Error while deactivating " + item.getTitle() + "\nMessage : " + moveUserResponse.getMessage(), ALERT_TYPE_DEACTIVATE_USER);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showErrorDialogInside("Error while deactivating " + item.getTitle(), ALERT_TYPE_DEACTIVATE_USER);
                    }
                });
            }
        });
        if (item.getType() == 2) {
            textView.setText(Html.fromHtml("Move <b>" + item.getTitle() + "</b> and his team to"));
        } else {
            textView.setText(Html.fromHtml("Move <b>" + item.getTitle() + "</b> to"));
        }
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view_view_shuffle_user);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new GridLayoutItemDecorator(
                getResources().getDimensionPixelSize(R.dimen.grid_list_spacing),
                2));
        adapterDialog = new TeamShuffleGridDialogAdapter(getContext(), itemDialog, new TeamShuffleGridDialogAdapter.TeamShuffleGridListener() {
            @Override
            public void onTeamShuffleDialogGridClicked(int position, TeamShuffleModel.Item item) {
                isDeactivateUser = false;
                cardViewDeactivate.setCardBackgroundColor(Color.parseColor("#FFD7D7D7"));
                textViewDeactivate.setTextColor(Color.parseColor("#FFD0021B"));
                if (lastSelectedUserPosition != position) {
                    makeDefaultGridClicked(itemDialog);
                }

                item.setClicked(!item.isClicked());
                if (item.isClicked()) {
                    selectedToItem = item;
                }
                itemDialog.set(position, item);
                adapterDialog.notifyItemChanged(position);
                lastSelectedUserPosition = position;
            }
        });
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapterDialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }

    private void makeDefaultGridClicked(List<TeamShuffleModel.Item> itemDialog) {
        if (lastSelectedUserPosition >= 0) {
            itemDialog.get(lastSelectedUserPosition).setClicked(false);
            adapterDialog.notifyItemChanged(lastSelectedUserPosition);
            lastSelectedUserPosition = -1;
            selectedToItem = null;

        }
    }

    private List<TeamShuffleModel.Item> getDialogData(TeamShuffleModel.Item item) {
        List<TeamShuffleModel.Item> itemDialog = new ArrayList<>();
        if (isUserBranchHead) {
            if (item.getType() == 3) {
                for (int i = 0; i < teamShuffleModel.getUserTeamShuffle().getChildItem().size(); i++) {
                    for (int j = 0; j < teamShuffleModel.getUserTeamShuffle().getChildItem().get(i).getChildItem().size(); j++) {
                        TeamShuffleModel.Item childItem =
                                teamShuffleModel.getUserTeamShuffle().getChildItem().get(i).getChildItem().get(j);
                        childItem.setClicked(false);
                        if (childItem.getId().intValue() != item.getParentId().intValue() && childItem.getLocationId().intValue() == item.getLocationId().intValue()) {
                            itemDialog.add(childItem);
                        }
                    }

                }

            } else if (item.getType() == 2) {
                for (int i = 0; i < teamShuffleModel.getUserTeamShuffle().getChildItem().size(); i++) {
                    TeamShuffleModel.Item childItem = teamShuffleModel.getUserTeamShuffle().getChildItem().get(i);
                    childItem.setClicked(false);
                    if (childItem.getId().intValue() != item.getParentId().intValue() && childItem.getLocationId().intValue() == item.getLocationId().intValue()) {
                        itemDialog.add(childItem);
                    }
                }
            }
        } else if (isUserSalesManager) {
            for (int i = 0; i < teamShuffleModel.getUserTeamShuffle().getChildItem().size(); i++) {
                TeamShuffleModel.Item childItem = teamShuffleModel.getUserTeamShuffle().getChildItem().get(i);
                childItem.setClicked(false);
                if (childItem.getId().intValue() != item.getParentId().intValue() && childItem.getLocationId().intValue() == item.getLocationId().intValue()) {
                    itemDialog.add(childItem);
                }

            }
        }
        return itemDialog;
    }

    @Override
    public void onClick(View v) {
        moveDseDialog((TeamShuffleModel.Item) v.getTag());
    }

    private void setImage(String url, final ImageView imageView, final TextView tvInitial) {
        imageView.setVisibility(View.GONE);
        tvInitial.setVisibility(View.VISIBLE);

        Glide.with(getContext()).
                load(url)
                .asBitmap()
                .centerCrop()
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                        imageView.setVisibility(View.GONE);
                        tvInitial.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        imageView.setVisibility(View.VISIBLE);
                        tvInitial.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                if(resource!=null && getContext()!=null) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    imageView.setImageDrawable(circularBitmapDrawable);
                }
            }
        });

        /*Picasso.with(getContext()).load(url)
                .transform(new CircleTransform()).into(imageView, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                imageView.setVisibility(View.VISIBLE);
                tvInitial.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                imageView.setVisibility(View.GONE);
                tvInitial.setVisibility(View.VISIBLE);
            }
        });*/
    }

    public boolean isUserBranchHead() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_BRANCH_HEAD)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_OPERATIONS)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isUserSalesManager() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.MANAGER_ROLE_ID)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void logEvent(String event) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_SHUFFLE_KEY_TYPE,
                event);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_SHUFFLE, hashMap);

    }

}
