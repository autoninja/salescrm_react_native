import React, { Component } from 'react'
import {View, Text, Image, TouchableOpacity,} from 'react-native'
import styles from '../styles/styles'

export default class GameIntroSC extends Component {

  constructor(props) {
    super(props);
  }
  componentDidMount () {
}
  render() {
    return(
  <View style={styles.MainContainer}>
      <View style= {styles.MainContainerYellow}>
        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>

          <Image source={require('../images/sc_leaderboard_top.png')}
          style={{flex:1,margin:30,}}
          resizeMode = 'contain'
           />

           <View style={{flex:1, alignItems:'center'}}>

           <View style={{flexDirection:'row', padding:10}}>
           <View style={{height:24, width:24, borderRadius:24/2, backgroundColor:'#fff', alignItems:'center', justifyContent:'center'}}>
           <Text style={{color:'#4B551E', fontSize:16}}>2</Text>
           </View>
           <Text style={{color:'#fff',fontWeight:'bold',fontSize:19, fontStyle:'italic', paddingLeft:10}}>Consultant Leaderboard</Text>
           </View>
           <Text style={{fontSize:18,color:'#fff', padding:10, textAlign:'center'}}>You only compete with consultants who have similar targets</Text>
           </View>

        </View>

        <View style={{flex:1, borderBottomLeftRadius:6, alignItems:'center',}}>
        <Image source={require('../images/sc_leaderboard.png')}
        style={{flex:1,margin:30,}}
        resizeMode = 'contain'
         />
        </View>

      </View>
      <View style={{alignItems: 'center', padding:10,flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
      {this.props.bottomViews}
      </View>
      </View>
    )
  }
}
