import { realm } from './realm_schemas'
import { ShowRoomLocationModel, ActvityModel, EnquirySourceMainModel, EnquirySourceCategoryModel, SubEnquirySourceModel } from '../models/lead_elements_model'

const EnquirySourceDBService = {
  save: function(enquirySourceMain, enquirySourceCategory) {
    realm.write(() => {
      enquirySourceMain.map((enquirySource)=> {
        realm.create('EnquirySourceMainDB', enquirySource, true);
      })

      enquirySourceCategory.map((enquirySourceCategoryItem)=> {
        realm.create('EnquirySourceCategoryDB', enquirySourceCategoryItem, true);
      })

    })
  },
  deleteAll: function() {
    realm.write(() => {
      realm.delete(realm.objects('EnquirySourceMainDB'));
      realm.delete(realm.objects('SubEnquirySourceDB'));
      realm.delete(realm.objects('EnquirySourceCategoryDB'));
    })
  },
  getEnquirySourceMain : ()=> {
    let enquirySources = [];
    let enquirySourceMainDB = realm.objects('EnquirySourceMainDB');
    enquirySourceMainDB.map((enquirySource)=> {
      enquirySources.push(new EnquirySourceMainModel(enquirySource.id, enquirySource.name));
    })
    return enquirySources;
  },
  getEnquirySourceCategories : ()=> {
    let enquirySourceCategories = [];
    let enquirySourceCategoriesDB = realm.objects('EnquirySourceCategoryDB');
    enquirySourceCategoriesDB.map((enqSource)=> {
      let subEnqSources = [];

      enqSource.subEnquirySource.map((subSource)=> {
        subEnqSources.push(new SubEnquirySourceModel(subSource.id, subSource.name));
      })
      enquirySourceCategories.push(new EnquirySourceCategoryModel(enqSource.id, enqSource.name, subEnqSources));
    })
    return enquirySourceCategories;
  }
};

export default EnquirySourceDBService;
