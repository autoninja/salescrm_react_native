import React, { Component } from "react";
import { View, TextInput, StyleSheet, Text } from "react-native";
export default class OTPInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: ["", "", "", ""],
      selectedField: 0
    };
  }
  onFocus(id) {
    this.setState({ selectedField: id });
  }
  onChangeText(id, text) {
    let { otp } = this.state;
    otp[id] = text;
    this.setState({ otp });
    if (text) {
      switch (id) {
        case 0:
          this.refs.input_1.focus();
          break;
        case 1:
          this.refs.input_2.focus();
          break;
        case 2:
          this.refs.input_3.focus();
          break;
      }
    } else {
      switch (id) {
        case 3:
          this.refs.input_2.focus();
          break;
        case 2:
          this.refs.input_1.focus();
          break;
        case 1:
          this.refs.input_0.focus();
          break;
      }
    }
    let otpVal = "";
    otp.map(val => {
      otpVal = otpVal.concat(val);
    });
    this.props.onOTPEntered(otpVal);
  }
  render() {
    let selectedField = this.state.selectedField;
    let title = this.props.title || "Enter OTP";
    return (
      <View style={styles.container}>
        <Text
          style={{
            padding: 10,
            color: "#303030",
            fontSize: 16,
            alignSelf: "center"
          }}
        >
          {title}
        </Text>
        <View style={styles.main}>
          {this.state.otp.map((otp, index) => {
            return (
              <TextInput
                ref={"input_" + index}
                onFocus={() => this.onFocus(index)}
                onChangeText={text => this.onChangeText(index, text)}
                maxLength={1}
                keyboardType="numeric"
                style={[
                  styles.input,
                  {
                    borderColor: "#007FFF",
                    borderWidth: selectedField == index ? 1 : 0
                  }
                ]}
                key={index + ""}
              />
            );
          })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    width: 40,
    height: 40,
    backgroundColor: "#EEE",
    marginLeft: 4,
    borderRadius: 4,
    textAlign: "center",
    marginRight: 4,
    padding: 6,
    fontSize: 16,
    marginTop: 10
  },
  main: {
    justifyContent: "center",
    alignContent: "center",
    flexDirection: "row"
  },
  container: {
    justifyContent: "center",
    alignContent: "center"
  }
});
