import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, FlatList, Platform } from 'react-native';
import LinearGradient from "react-native-linear-gradient";

import GridItem from './GridItem'
import MainActionItem from './MainActionItem'
import styles from './PlanNextTask.styles';
import { getPNTModel, FormAnswerIds } from './PNTModel';
import Toast, { DURATION } from 'react-native-easy-toast';
import RestClient from '../../network/rest_client'
import Loader from "../../components/uielements/Loader";
export default class PlanNextTask extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pntModel: getPNTModel(this.props.info),
            clickedForNextAction: false,
            childPNTModels: [],
        }
    }
    showAlert(message) {
        try {
            this.refs.toast.show(message);
        }
        catch (e) {

        }
    }
    onConfirmPnt(pntModel) {
        //C360 car is not done in ios
        let route;
        switch (pntModel.fAnswerId) {
            case FormAnswerIds.BOOK_CAR_ID:
            case FormAnswerIds.BOOK_RE:
            case FormAnswerIds.BOOKED:
            case FormAnswerIds.IL_RENEWED:
            case FormAnswerIds.SALES_CONFIRMED:
            case FormAnswerIds.VERIFICATION:
                route = "PNTSubmission"
                break;
            default:
                route = "FormRendering";
        }
        this.props.navigation.push(route, {
            should_render_post_form: true,
            post_form_object: this.props.navigation.getParam('post_form_object'),
            pntModel,
            previousFormAnswers: this.props.navigation.getParam('previousFormAnswers'),
            postBookingDriveOrVisit: this.props.info.postBookingDriveOrVisit
        })
    }
    onSelectItem(pntModel) {
        if (pntModel.disabled) {
            this.showAlert("Disbaled")
        }
        else {

            if (pntModel.childModels && pntModel.childModels.gridItems.length > 0) {
                this.setState({ clickedForNextAction: true, childPNTModels: pntModel.childModels });
            }
            //Also Client should not be royal enfield, ilom, ilombank
            else if (pntModel.fAnswerId == FormAnswerIds.LOST_DROP
                && !this.props.info.isClientILomBank
                && !this.props.info.isClientILom) {
                this.setState({ loading: true });
                new RestClient().exchangeLostDropCheck(this.props.info.leadId).then((data) => {
                    if (data && data.statusCode == "2002" && data.result && data.result.ld_flag) {
                        //Callback
                        this.onConfirmPnt(pntModel)
                    }
                    else {
                        this.showAlert("Failed to select lost drop")
                    }
                    this.setState({ loading: false })
                }).catch(error => {
                    this.showAlert("Failed to select lost drop")
                    this.setState({ loading: false });
                });
            }
            else {
                //Callback
                this.onConfirmPnt(pntModel)
            }
        }
    }
    getPntModelsNow() {
        let { pntModel, clickedForNextAction, childPNTModels } = this.state;
        if (clickedForNextAction) {
            return childPNTModels;
        }
        else {
            return pntModel;
        }
    }
    render() {
        let pntModel = this.getPntModelsNow();
        let backImage = require("../../images/ic_close_black.png");
        if (this.state.clickedForNextAction) {
            backImage = require("../../images/ic_back_black.png")
        }
        return (
            <View style={styles.main}>
                <LinearGradient
                    colors={["#2e5e86", "#2e3486"]}
                    style={styles.linearGradient}>
                    <View style={{ flexWrap: 'wrap' }}>

                        <View style={styles.header}>
                            <Text style={styles.title}>Plan Next Task</Text>
                            <TouchableOpacity
                                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                style={styles.closeButton}
                                onPress={() => {
                                    if (this.state.clickedForNextAction) {
                                        this.setState({ clickedForNextAction: false })
                                    }
                                    else {
                                        try {
                                            if (this.props.navigation.getParam('pntOpendFromForm')) {
                                                this.props.navigation.goBack(null)
                                            } else {
                                                this.props.info.navigation.goBack(null)
                                            }
                                        } catch (e) {

                                        }
                                    }
                                }}>
                                <Image
                                    source={backImage}
                                    style={{ width: 16, height: 16 }}
                                />
                            </TouchableOpacity>
                        </View>

                        <FlatList
                            showsVerticalScrollIndicator={false}
                            style={styles.list}
                            ItemSeparatorComponent={() => <View style={styles.line} />}
                            data={pntModel.gridItems}
                            numColumns='2'
                            renderItem={({ item, index }) => {
                                let gridView = <GridItem
                                    onPress={this.onSelectItem.bind(this, item)}
                                    pntModel={item}
                                    index={index}
                                    length={pntModel.gridItems.length}
                                />;
                                if (index == pntModel.gridItems.length - 1 && index % 2 == 0) {
                                    return (
                                        <View style={{ flexDirection: 'row', flex: 1 }}>
                                            {gridView}
                                            <View style={{ padding: 20, flex: 1 }} />
                                        </View>
                                    )
                                }
                                else {
                                    return (
                                        gridView
                                    )
                                }

                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                        {pntModel.mainAction && !this.state.clickedForNextAction && (
                            <MainActionItem pntModel={pntModel.mainAction}
                                onPress={this.onSelectItem.bind(this, pntModel.mainAction)}
                            />
                        )}

                    </View>
                    {this.state.loading && <Loader isLoading={this.state.loading} />}
                </LinearGradient>
                <Toast
                    ref="toast"
                    style={{ backgroundColor: 'red' }}
                    fadeInDuration={750}
                    fadeOutDuration={750}
                    opacity={0.8}
                    textStyle={{ color: 'white' }}
                />
            </View>
        )
    }
}