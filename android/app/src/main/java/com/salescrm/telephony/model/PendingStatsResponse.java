package com.salescrm.telephony.model;

/**
 * Created by bannhi on 25/9/17.
 */

public class PendingStatsResponse {
    private String statusCode;

    private String message;

    private Result[] result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result[] getResult() {
        return result;
    }

    public void setResult(Result[] result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }


    public class Result {
        private Users[] users;

        private String location_name;

        private Stats stats;

        private int location_id;

        public Users[] getUsers() {
            return users;
        }

        public void setUsers(Users[] users) {
            this.users = users;
        }

        public String getLocation_name() {
            return location_name;
        }

        public void setLocation_name(String location_name) {
            this.location_name = location_name;
        }

        public Stats getStats() {
            return stats;
        }

        public void setStats(Stats stats) {
            this.stats = stats;
        }

        public int getLocation_id() {
            return location_id;
        }

        public void setLocation_id(int location_id) {
            this.location_id = location_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [users = " + users + ", location_name = " + location_name + ", stats = " + stats + ", location_id = " + location_id + "]";
        }
    }

    public class Stats {
        private int today_count;

        private Weekly_data[] weekly_data;

        private int last_week_count;

        public int getToday_count() {
            return today_count;
        }

        public void setToday_count(int today_count) {
            this.today_count = today_count;
        }

        public Weekly_data[] getWeekly_data() {
            return weekly_data;
        }

        public void setWeekly_data(Weekly_data[] weekly_data) {
            this.weekly_data = weekly_data;
        }

        public int getLast_week_count() {
            return last_week_count;
        }

        public void setLast_week_count(int last_week_count) {
            this.last_week_count = last_week_count;
        }

        @Override
        public String toString() {
            return "ClassPojo [today_count = " + today_count + ", weekly_data = " + weekly_data + ", last_week_count = " + last_week_count + "]";
        }
    }

    public class Weekly_data {
        private int week_number;

        private int avg_pending;

        public int getWeek_number() {
            return week_number;
        }

        public void setWeek_number(int week_number) {
            this.week_number = week_number;
        }

        public int getAvg_pending() {
            return avg_pending;
        }

        public void setAvg_pending(int avg_pending) {
            this.avg_pending = avg_pending;
        }

        @Override
        public String toString() {
            return "ClassPojo [week_number = " + week_number + ", avg_pending = " + avg_pending + "]";
        }
    }

    public class Users {
        private int pending_count_difference;

        private int id;

        private int team_member;

        private String image;

        private int pending_count;

        private String name;

        private int visible_times;

        private int median_delay;

        private Calls[] calls;

        public int getPending_count_difference() {
            return pending_count_difference;
        }

        public void setPending_count_difference(int pending_count_difference) {
            this.pending_count_difference = pending_count_difference;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getTeam_member() {
            return team_member;
        }

        public void setTeam_member(int team_member) {
            this.team_member = team_member;
        }

        public int getPending_count() {
            return pending_count;
        }

        public void setPending_count(int pending_count) {
            this.pending_count = pending_count;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getVisible_times() {
            return visible_times;
        }

        public void setVisible_times(int visible_times) {
            this.visible_times = visible_times;
        }

        public int getMedian_delay() {
            return median_delay;
        }

        public void setMedian_delay(int median_delay) {
            this.median_delay = median_delay;
        }

        public Calls[] getCalls() {
            return calls;
        }

        public void setCalls(Calls[] calls) {
            this.calls = calls;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        @Override
        public String toString() {
            return "ClassPojo [pending_count_difference = " + pending_count_difference + ", id = " + id + ", pending_count = " + pending_count + ", name = " + name + ", visible_times = " + visible_times + ", median_delay = " + median_delay + ", calls = " + calls + "]";
        }
    }

    public class Error {
        private String details;

        private String type;

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "ClassPojo [details = " + details + ", type = " + type + "]";
        }
    }

    public class Calls {
        private int id;

        private String phone;

        private String name;

        private String image;

        private String role_name;

        private int role_id;


        public int getRole_id() {
            return role_id;
        }

        public void setRole_id(int role_id) {
            this.role_id = role_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getRole_name() {
            return role_name;
        }

        public void setRole_name(String role_name) {
            this.role_name = role_name;
        }

        @Override
        public String toString() {
            return "ClassPojo [id = " + id + ", phone = " + phone + ", name = " + name + ", image = " + image + ", role_name = " + role_name + "]";
        }
    }


}
