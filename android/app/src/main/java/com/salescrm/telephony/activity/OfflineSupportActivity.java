package com.salescrm.telephony.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.firebase.iid.FirebaseInstanceId;
import com.salescrm.telephony.BuildConfig;
import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.AllMobileNumbersSearch;
import com.salescrm.telephony.db.LeadListDetails;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.FetchLeadElementsListener;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.model.BookingAllocationModel;
import com.salescrm.telephony.model.EvaluationManagerModel;
import com.salescrm.telephony.model.NewFilter.NewFilter;
import com.salescrm.telephony.offline.FetchActionPlanData;
import com.salescrm.telephony.offline.FetchActiveEvents;
import com.salescrm.telephony.offline.FetchActivityUsersDetails;
import com.salescrm.telephony.offline.FetchAppConfig;
import com.salescrm.telephony.offline.FetchAwsCredentials;
import com.salescrm.telephony.offline.FetchBookingAllocationData;
import com.salescrm.telephony.offline.FetchC360Information;
import com.salescrm.telephony.offline.FetchCarModelAndVariants;
import com.salescrm.telephony.offline.FetchDealershipsData;
import com.salescrm.telephony.offline.FetchEtvbrFilters;
import com.salescrm.telephony.offline.FetchEvaluationManager;
import com.salescrm.telephony.offline.FetchEventCategories;
import com.salescrm.telephony.offline.FetchFilterData;
import com.salescrm.telephony.offline.FetchLeadElements;
import com.salescrm.telephony.offline.FetchLeadMobiles;
import com.salescrm.telephony.offline.FetchTeamUsersDetails;
import com.salescrm.telephony.offline.FetchUserData;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.ActionPlanResponse;
import com.salescrm.telephony.response.ActiveEventsResponse;
import com.salescrm.telephony.response.AddLeadElementsResponse;
import com.salescrm.telephony.response.AllLeadCustomerDetailsResponse;
import com.salescrm.telephony.response.AppConfResponse;
import com.salescrm.telephony.response.AppUserResponse;
import com.salescrm.telephony.response.CarBrandModelVariantsResponse;
import com.salescrm.telephony.response.CreForActivityResponses;
import com.salescrm.telephony.response.DealershipsResponse;
import com.salescrm.telephony.response.EventCategoryResponse;
import com.salescrm.telephony.response.FCMTokenRefreshResponse;
import com.salescrm.telephony.response.FilterActivityAndStagesResponse;
import com.salescrm.telephony.response.LeadMobileResponse;
import com.salescrm.telephony.response.LeadTeamUserResponse;
import com.salescrm.telephony.response.ScoreboardResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;
import com.salescrm.telephony.views.gameintro.GameIntroMain;

import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bharath on 5/12/16.
 */

public class OfflineSupportActivity extends AppCompatActivity implements OfflineSupportListener, FetchLeadElementsListener, AutoDialogClickListener {
    Preferences pref;
    private Realm realm;

    private void disableScreenCapture() {
        if(pref.isScreenshotEnabled()) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Preferences.getInstance();
        Preferences.load(getApplicationContext());
        disableScreenCapture();
        realm = Realm.getDefaultInstance();
        setContentView(R.layout.activity_offline_support);
        deleteDatabase();
        init();
    }

    private void deleteDatabase() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    realm.deleteAll();
                } catch (Exception ignored) {
                }
            }
        });
    }
    private void init() {
        if (new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
            getDealershipsData();
        } else {
            new AutoDialog(this, new AutoDialogModel("No internet connection", "Retry", "Re-Login", WSConstants.OfflineAPIRequest.INIT)).show();
        }
    }

    private void getUserDetails() {
        new FetchUserData(this, getApplicationContext()).call();
    }

    private void getDealershipsData() {
        new FetchDealershipsData(this, getApplicationContext()).call();
    }

    private void getActionPlanData() {
        Integer dseId = realm.where(UserDetails.class).findFirst() == null ? null : realm.where(UserDetails.class).findFirst().getUserId();
        new FetchActionPlanData(this, getApplicationContext(), dseId).call();
    }

    private void getC360Information() {
        new FetchC360Information(this, getApplicationContext()).call();
    }

    /**
     * Method to get app config data
     */
    private void getAppConf() {
        new FetchAppConfig(this, getApplicationContext()).call();
    }

    /**
     * Method to get app config data
     */
    private void getEtvbrFilters() {
        new FetchEtvbrFilters(this, getApplicationContext()).call();
    }

    private void getLeadMobiles() {
        new FetchLeadMobiles(this, getApplicationContext()).call();
    }


    //1:method for getting basic lead data
    private void getLeadElements() {
        new FetchLeadElements(this, getApplicationContext()).call();
    }

    //2:method for getting filter data
    private void getFilterData() {
        new FetchFilterData(this, getApplicationContext()).call();
    }

    //3:method for getting Team_user data
    private void getTeamUserData() {
        new FetchTeamUsersDetails(this, getApplicationContext()).call();
    }

    //4:method for getting Team_user data
    private void getActivityUsers() {
        new FetchActivityUsersDetails(this, getApplicationContext()).call();
    }

    //5:method for getting car data
    private void getCarModelAndVariants() {
        new FetchCarModelAndVariants(this, getApplicationContext()).call();
    }

    private void getActiveEvents() {
        new FetchActiveEvents(this, getApplicationContext()).call();
    }

    private void getEventCategories() {
        new FetchEventCategories(this, getApplicationContext()).call();
    }

    private void getBookingAllocationData() {
        new FetchBookingAllocationData(this, getApplicationContext()).call();
    }

    private void getEvaluationManager(){
        new FetchEvaluationManager(this, getApplicationContext()).call();
    }
    private void getAwsCredentials(){
        new FetchAwsCredentials(this, getApplicationContext()).callAsync();
    }


    @Override
    public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
        Preferences.load(getApplicationContext());
        switch (data.getFromId()) {
            case WSConstants.OfflineAPIRequest.INIT:
                init();
                break;
            case WSConstants.OfflineAPIRequest.DEALERSHIPS:
                getDealershipsData();
                break;
            case WSConstants.OfflineAPIRequest.USER_DATA:
                getUserDetails();
                break;
            case WSConstants.OfflineAPIRequest.ACTION_PLAN:
                getActionPlanData();
                break;
            case WSConstants.OfflineAPIRequest.C360:
                getC360Information();
                break;
            case WSConstants.OfflineAPIRequest.FETCH_LEAD_ELEMENTS:
                getLeadElements();
                break;
            case WSConstants.OfflineAPIRequest.FETCH_FILTER_DATA:
                getFilterData();
                break;
            case WSConstants.OfflineAPIRequest.TEAM_USERS:
                getTeamUserData();
                break;
            case WSConstants.OfflineAPIRequest.ACTIVITY_USERS:
                getActivityUsers();
                break;
            case WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS:
                getCarModelAndVariants();
                break;
            case WSConstants.OfflineAPIRequest.APP_CONFIG:
                getAppConf();
                break;
            case WSConstants.OfflineAPIRequest.EXTERNAL_API:
                getAppConf();
                break;
            case WSConstants.OfflineAPIRequest.LEAD_MOBILE:
                getLeadMobiles();
                break;
            case WSConstants.OfflineAPIRequest.GAME_CONFIG:
                getAppConf();
                break;
            case WSConstants.OfflineAPIRequest.ETVBR_FILTER:
                getEtvbrFilters();
                break;
            case WSConstants.OfflineAPIRequest.BOOKING_ALLOCATION:
                getBookingAllocationData();
                break;
            case WSConstants.OfflineAPIRequest.ACTIVE_EVENTS:
                getActiveEvents();
                break;
            case WSConstants.OfflineAPIRequest.EVENT_CATEGORIES:
                getEventCategories();
                break;
            case WSConstants.OfflineAPIRequest.EVALUATION_MANAGER:
                getEvaluationManager();
                break;
            case WSConstants.OfflineAPIRequest.AWS_CREDENTIALS:
                getAwsCredentials();
                break;
            default:
                getActionPlanData();
                break;

        }


    }


    @Override
    public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    realm.deleteAll();
                } catch (Exception ignored) {

                }
            }

        });
        Preferences.setIsLogedIn(false);
        Preferences.deleteAll(OfflineSupportActivity.this);
        OfflineSupportActivity.this.finish();
        startActivity(new Intent(this, SplashActivity.class));

    }

    @Override
    public void onOfflineSupportApiError(RetrofitError error, int fromId) {
        if (error != null) {
            System.out.println(error.toString());
        }
        String text;
        switch (fromId) {
            case WSConstants.OfflineAPIRequest.DEALERSHIPS:
                text = "Dealership";
                break;
            case WSConstants.OfflineAPIRequest.USER_DATA:
                text = "User";
                break;
            case WSConstants.OfflineAPIRequest.ACTION_PLAN:
                text = "Tasks";
                break;
            case WSConstants.OfflineAPIRequest.C360:
                text = "C360";
                break;
            case WSConstants.OfflineAPIRequest.SCOREBOARD:
                text = "FetchScoreBoardData";
                break;
            case WSConstants.OfflineAPIRequest.FETCH_LEAD_ELEMENTS:
                text = "lead basic";
                break;
            case WSConstants.OfflineAPIRequest.FETCH_FILTER_DATA:
                text = "filter";
                break;
            case WSConstants.OfflineAPIRequest.TEAM_USERS:
                text = "Team User";
                break;
            case WSConstants.OfflineAPIRequest.ACTIVITY_USERS:
                text = "Activity mapping to user";
                break;
            case WSConstants.OfflineAPIRequest.CAR_MODEL_VARIANTS:
                text = "Vehicles";
                break;
            case WSConstants.OfflineAPIRequest.ENQUIRY_DATA:
                text = "Enquiry Data";
                break;

            case WSConstants.OfflineAPIRequest.APP_CONFIG:
                text = "App Configuration";
                break;
            case WSConstants.OfflineAPIRequest.EXTERNAL_API:
                text = "External API Keys";
                break;
            case WSConstants.OfflineAPIRequest.GAME_CONFIG:
                text = "Game Configuration";
                break;
            case WSConstants.OfflineAPIRequest.LEAD_MOBILE:
                text = "Lead Mobile (Telephony)";
                break;
            case WSConstants.OfflineAPIRequest.ETVBR_FILTER:
                text = "Etvbr Filter";
                break;
            case WSConstants.OfflineAPIRequest.BOOKING_ALLOCATION:
                text = "Car Allocation";
                break;

            case WSConstants.OfflineAPIRequest.ACTIVE_EVENTS:
                text = "Active Events";
                break;
            case WSConstants.OfflineAPIRequest.EVENT_CATEGORIES:
                text = "Event Categories";
                break;
            case WSConstants.OfflineAPIRequest.EVALUATION_MANAGER:
                text = "Evaluation Manager";
                break;
            case WSConstants.OfflineAPIRequest.AWS_CREDENTIALS:
                text = "AWS Credentials";
                break;
            default:
                text = "the";
                break;

        }

        if (!isFinishing()) {
            new AutoDialog(this,
                    new AutoDialogModel("Something went wrong while fetching " + text + " information.", "Retry", "Re-Login",
                            fromId)).show();
        }

    }

    @Override
    public void onDealerShipFetched(DealershipsResponse dealershipsResponse) {
        if (!Preferences.isDealershipSelected()) {
            startActivity(new Intent(this, SwitchDealershipsActivity.class));
            OfflineSupportActivity.this.finish();
        } else {
            getUserDetails();
        }
    }

    @Override
    public void onUserDataFetched(AppUserResponse resp) {
        getActionPlanData();
    }

    @Override
    public void onActionPlanFetched(List<ActionPlanResponse.Result.Data> data) {
        getC360Information();
    }

    @Override
    public void onC360InformationFetched(AllLeadCustomerDetailsResponse data) {
        getAppConf();
    }

    @Override
    public void onAppConfFetched(AppConfResponse resp) {
        getEtvbrFilters();
    }

    @Override
    public void onEtvbrFiltersFetched(NewFilter response) {
        getBookingAllocationData();
    }

    @Override
    public void onBookingAllocationDataFetched(BookingAllocationModel response) {
        getLeadMobiles();
    }

    @Override
    public void onLeadMobileFetched(LeadMobileResponse response) {
        getLeadElements();
    }

    @Override
    public void onLeadElementsFetched(AddLeadElementsResponse addLeadElementsResponse) {
        getFilterData();
    }

    @Override
    public void onFilterDataFetched(FilterActivityAndStagesResponse data) {
        getTeamUserData();
    }

    @Override
    public void onTeamUsersFetched(LeadTeamUserResponse leadTeamUserResponse) {
        getActivityUsers();
    }

    @Override
    public void onActivityUsersFetched(CreForActivityResponses creForActivityResponse) {
        getCarModelAndVariants();
    }

    @Override
    public void onCarModelAndVariantsFetched(CarBrandModelVariantsResponse carBrandModelVariantsResponse) {
        getActiveEvents();
    }

    @Override
    public void onFetchActiveEvents(ActiveEventsResponse response) {
        getEventCategories();
    }

    @Override
    public void onFetchEventCategories(EventCategoryResponse response) {
        getEvaluationManager();
    }

    @Override
    public void onFetchEvaluationManager(EvaluationManagerModel response) {
        callHomeActivity();
        // getAwsCredentials();
    }

    @Override
    public void onFetchAwsCredentials() {
        callHomeActivity();
    }


    //Pretty old method!! Loved this screen, so not removed ScoreboardActivity
    @Override
    public void onScoreboardDataFetched(ScoreboardResponse data) {
    }

    @Override
    public void onFetchLeadElementsError(RetrofitError error) {
        if (!new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
            Toast.makeText(getApplicationContext(),
                    "Please switch on the internet connection and try again", Toast.LENGTH_LONG).show();
            new AutoDialog(this,
                    new AutoDialogModel("Please switch on the internet connection and try again.", "Retry", "no_button",
                            WSConstants.OfflineAPIRequest.FETCH_LEAD_ELEMENTS)).show();
        } else {
            new AutoDialog(this,
                    new AutoDialogModel("Something went wrong while fetching the data.", "Retry", "Exit",
                            WSConstants.OfflineAPIRequest.FETCH_LEAD_ELEMENTS)).show();
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public void getSearchMobileNumbers() {

        realm.beginTransaction();

        RealmResults<SalesCRMRealmTable> salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).findAll();
        RealmResults<LeadListDetails> leadListDetails = realm.where(LeadListDetails.class).findAll();
        AllMobileNumbersSearch allMobileNumbersSearch;

        if (salesCRMRealmTable != null) {

            for (int i = 0; i < salesCRMRealmTable.size(); i++) {
                allMobileNumbersSearch = new AllMobileNumbersSearch();
                allMobileNumbersSearch.setName(salesCRMRealmTable.get(i).getCustomerName());
                if (salesCRMRealmTable != null && salesCRMRealmTable.get(i) != null) {
                    allMobileNumbersSearch.setLeadId(salesCRMRealmTable.get(i).getLeadId());
                } else {
                    allMobileNumbersSearch.setLeadId(0);
                }

                for (int j = 0; j < salesCRMRealmTable.get(i).customerPhoneNumbers.size(); j++) {
                    allMobileNumbersSearch.setDeleteStatus(0);
                    allMobileNumbersSearch.setMobileNumber("" + salesCRMRealmTable.get(i).customerPhoneNumbers.get(j).getPhoneNumber());
                    System.out.println("TELEPHONY mobileNumbers Sales- " + salesCRMRealmTable.get(i).customerPhoneNumbers.get(j).getPhoneNumber());
                    realm.copyToRealmOrUpdate(allMobileNumbersSearch);
                }
            }
        }

        if (leadListDetails != null) {

            for (int i = 0; i < leadListDetails.size(); i++) {
                allMobileNumbersSearch = new AllMobileNumbersSearch();
                allMobileNumbersSearch.setDeleteStatus(0);
                allMobileNumbersSearch.setLeadId(Long.parseLong(leadListDetails.get(i).getLead_id()));
                allMobileNumbersSearch.setMobileNumber(leadListDetails.get(i).getNumber());
                realm.copyToRealmOrUpdate(allMobileNumbersSearch);
                System.out.println("TELEPHONY mobileNumbers Lead- " + Long.parseLong(leadListDetails.get(i).getNumber()));
            }
        }
        realm.commitTransaction();
    }


    private void callHomeActivity() {
        CleverTapPush.pushProfile(pref);
        sendAndGenerateFcmId();

        new Thread() {
            @Override
            public void run() {
                super.run();
                registerNotifications();
            }
        }.start();

        if(isOpenC360()) {
            pref.setAppLastOpenedDate(Util.getTime(0).getTime());
            pref.setIsLogedIn(true);
            pref.setAppLastOpenedDate(Util.getTime(0).getTime());
            pref.setVersionCode(BuildConfig.VERSION_CODE);
            startActivity(new Intent(OfflineSupportActivity.this, C360Activity.class));
        }
        else {
            if (DbUtils.isGamificationEnabled() && !Preferences.isGameIntroSeen()) {
                startActivity(new Intent(OfflineSupportActivity.this, GameIntroMain.class));
            } else {
                startActivity(new Intent(OfflineSupportActivity.this, HomeActivity.class));
            }
        }
        //getSearchMobileNumbers();
        OfflineSupportActivity.this.finish();
    }

    private void sendAndGenerateFcmId() {

        if ((new ConnectionDetectorService(this).isConnectingToInternet())) {

            if (Preferences.getFcmId() != null && !Preferences.getFcmId().equalsIgnoreCase("")) {
                makeFcmUpdateApiCall();
            } else {
                String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                if (refreshedToken != null && !refreshedToken.equalsIgnoreCase("")) {
                    Preferences.setFcmId(refreshedToken);
                    Preferences.setFcmSync(1);
                    makeFcmUpdateApiCall();
                } else {
                    ButtonClicked("FCMID NOT GENERATED");
                }
            }

        }
    }

    private void registerNotifications() {
        //6pm trigger low pending alert;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Util.registerNotification(getApplicationContext(),
                null,
                null,
                WSConstants.InAppNotificationType.LESS_TASK_COMPLETION,
                calendar.getTimeInMillis()
        );
    }

    private void ButtonClicked(String mButtonName) {
        if (Preferences.isFabricsNeeded()) {
            Answers.getInstance().logCustom(new CustomEvent(mButtonName)
                    //  .putCustomAttribute("Name", mButtonName)
                    .putCustomAttribute("Dealer Name", Preferences.getDealerName())
                    .putCustomAttribute("IMEI", Preferences.getImeiNumber())
                    .putCustomAttribute("User Name", Preferences.getUserName()));
        }
    }

    private void makeFcmUpdateApiCall() {

        ApiUtil.GetRestApiWithHeader(Preferences.getAccessToken()).refreshFcmToken(Preferences.getFcmId(), new Callback<FCMTokenRefreshResponse>() {
            @Override
            public void success(FCMTokenRefreshResponse fcmTokenRefreshResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if (fcmTokenRefreshResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)) {
                    ApiUtil.InvalidUserLogout(OfflineSupportActivity.this, 0);
                }

                if (fcmTokenRefreshResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    Preferences.setFcmSync(0);
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    private boolean isOpenC360() {
        boolean openC360 = false;
        if(getIntent()!=null && getIntent().getBooleanExtra("START_C360", false)) {
            openC360 = true;
        }
        return openC360;
    }
}
