package com.salescrm.telephony.clevertaputil;

public class CleverTapConstants {

    /*Tasks  with Tabs*/
    public static final String EVENT_MY_TASK = "My Task";
    public static final String EVENT_MY_TASK_KEY_TAB = "Tab";
    public static final String EVENT_MY_TASK_TAB_VALUE_TODAY = "Today";
    public static final String EVENT_MY_TASK_TAB_VALUE_FUTURE = "Future";
    public static final String EVENT_MY_TASK_TAB_VALUE_VALUE_DONE = "Done";
    /*--------*/

    /*Notification Sent*/
    public static final String EVENT_NOTIFICATION = "Notification";

    public static final String EVENT_NOTIFICATION_KEY_TYPE = "Type";
    public static final String EVENT_NOTIFICATION_TYPE_SENT = "Sent";
    public static final String EVENT_NOTIFICATION_TYPE_CLICK = "Click";

    public static final String EVENT_NOTIFICATION_KEY_FROM = "Triggered From";
    public static final String EVENT_NOTIFICATION_FROM_VALUE_IN_APP = "InApp";
    public static final String EVENT_NOTIFICATION_FROM_VALUE_FCM = "FCM";
    //Values will be dynamic for Notification Type
    public static final String EVENT_NOTIFICATION_KEY_EVENT = "Event Name";
    /*-------*/

    /*User communication*/
    public static final String EVENT_COMMUNICATION = "Communication";
    public static final String EVENT_COMMUNICATION_CALL = "Call";
    public static final String EVENT_COMMUNICATION_SMS = "SMS";
    public static final String EVENT_COMMUNICATION_EMAIL = "Email";
    public static final String EVENT_COMMUNICATION_WHATSAPP = "Whatsapp";
    /*-------*/

    /*TeamDashboard*/
    public static final String EVENT_TEAM_DASHBOARD="Team Dashboard";
    public static final String EVENT_TEAM_DASHBOARD_KEY_TYPE="Dashboard Type";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_LOCATIONS="ETVBR Locations";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_TEAMS="ETVBR Teams";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_TODAY_TASKS="Today's Task";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_PENDING_TEAMS="Pending Team";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_CARS="ETVBR Cars";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_PERCENTAGE="ETVBR Percentage";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_DATE = "ETVBR Date";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_PREVIOUS_DATE = "ETVBR Previous Date";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_PENDING_ANALYTICS="Pending Analytics";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_LOST_DROP_ANALYSIS="Lost Drop Analysis";


    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_LOCATIONS="EXCHFIN Locations";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_TEAMS="EXCHFIN Teams";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_CARS="EXCHFIN Cars";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_PERCENTAGE="EXCHFIN Percentage";
    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_DATE = "EXCHFIN Date";

    public static final String EVENT_TEAM_DASHBOARD_TYPE_VALUE_EVENTS = "Events";
    /*------*/

    /*Lead Search*/
    public static final String EVENT_LEAD_SEARCH="Lead Search";
    public static final String EVENT_LEAD_SEARCH_KEY_SEARCH_TYPE="Search Type";

    /*My Performance for sc*/
    public static final String EVENT_MY_PERFORMANCE="My Performance";

    /*My Performance for sc*/
    public static final String EVENT_MY_ENQUIRES="My Enquires";
    public static final String EVENT_MY_ENQUIRES_KEY_TAB = "Tab";
    public static final String EVENT_MY_ENQUIRES_KEY_CLICK = "Click";
    public static final String EVENT_MY_ENQUIRES_CLICK_NO = "NO";
    public static final String EVENT_MY_ENQUIRES_CLICK_YES = "YES";
    public static final String EVENT_MY_ENQUIRES_TAB_ASSIGNED = "Assigned";
    public static final String EVENT_MY_ENQUIRES_TAB_TEST_RIDE = "Test Ride";
    public static final String EVENT_MY_ENQUIRES_TAB_TEST_DRIVE = "Test Drive";
    public static final String EVENT_MY_ENQUIRES_TAB_EVALUATION = "Evaluation";
    public static final String EVENT_MY_ENQUIRES_TAB_BOOKED = "Booked";
    public static final String EVENT_MY_ENQUIRES_TAB_INVOICED = "Invoiced";
    public static final String EVENT_MY_ENQUIRES_TAB_CLOSED = "Closed";

    /*Recording Play*/
    public static final String EVENT_RECORDINGS = "Recordings Play";

    /*Booking*/
    public static final String EVENT_BOOKING = "Booking";
    public static final String EVENT_BOOKING_KEY_TYPE = "Type";
    public static final String EVENT_BOOKING_TYPE_DETAILS = "Details";
    public static final String EVENT_BOOKING_TYPE_CANCEL = "Cancel";

    /*Filters*/
    public static final String EVENT_FILTERS = "Filters";
    public static final String EVENT_FILTER_KEY_TYPE = "Filter Type";

    /*Logout*/
    public static final String EVENT_LOGOUT = "Logout";

    /*Logout*/
    public static final String FORCED_LOGOUT = "Forced Logout";

    /*Proforma Invoice*/
    public static final String EVENT_PROFORMA_INVOICE = "Proforma Invoice";
    public static final String EVENT_PROFORMA_INVOICE_KEY_TYPE = "Type";
    public static final String EVENT_PROFORMA_INVOICE_TYPE_CLICK = "Click";
    public static final String EVENT_PROFORMA_INVOICE_TYPE_SENT = "Sent";
    public static final String EVENT_PROFORMA_INVOICE_KEY_SENT_TYPE = "Sent Type";
    public static final String EVENT_PROFORMA_INVOICE_SENT_TYPE_WHATSAPP = "Whatsapp";
    public static final String EVENT_PROFORMA_INVOICE_SENT_TYPE_EMAIL = "Email";

    /*Transfer Enquiries*/
    public static final String EVENT_PROFORMA_TRANSFER_ENQUIRIES = "Transfer Enquiries";
    public static final String EVENT_PROFORMA_INVOICE_KEY_TRANSFER_ENQ_TYPE = "Transfer Type";
    public static final String EVENT_PROFORMA_INVOICE_TRANSFER_ENQ_FILTER = "Filter";
    public static final String EVENT_PROFORMA_INVOICE_TRANSFER_ENQ_LEAD_ID= "Lead Id";

    /*Shuffle*/
    public static final String EVENT_SHUFFLE = "Shuffle";
    public static final String EVENT_SHUFFLE_KEY_TYPE = "Type";
    public static final String EVENT_SHUFFLE_TYPE_SHUFFLE = "Shuffle";
    public static final String EVENT_SHUFFLE_TYPE_DEACTIVATE_USER = "Deactivate User";

    /*Target*/
    public static final String EVENT_TARGET = "Target";


    /*Leaderboard*/
    public static final String EVENT_LEADER_BOARD = "Leaderboard";
    public static final String EVENT_LEADER_BOARD_KEY_TYPE = "Leaderboard Type";
    public static final String EVENT_LEADER_BOARD_TYPE_MANAGER_LEADERBOARD = "Manager Leaderboard";
    public static final String EVENT_LEADER_BOARD_TYPE_MANAGER_BREAK_UP_OWN = "Manager Break Up Own";
    public static final String EVENT_LEADER_BOARD_TYPE_MANAGER_BREAK_UP_OTHERS = "Manager Break Up Others";
    public static final String EVENT_LEADER_BOARD_TYPE_TEAM_LEADERBOARD = "Team Leaderboard";
    public static final String EVENT_LEADER_BOARD_TYPE_TEAM_BREAK_UP_OWN = "Team Break Up Own";
    public static final String EVENT_LEADER_BOARD_TYPE_TEAM_BREAK_UP_OTHERS = "Team Break Up Others";
    public static final String EVENT_LEADER_BOARD_TYPE_CONSULTANT_LEADERBOARD_OWN = "Consultant Leaderboard Own";
    public static final String EVENT_LEADER_BOARD_TYPE_CONSULTANT_LEADERBOARD_OTHERS = "Consultant Leaderboard Others";
    public static final String EVENT_LEADER_BOARD_TYPE_CONSULTANT_POINT_BREAK_UP_OWN = "Consultant Point Break Up Own";
    public static final String EVENT_LEADER_BOARD_TYPE_CONSULTANT_POINT_BREAK_UP_OTHERS = "Consultant Point Break Up Others";
    public static final String EVENT_LEADER_BOARD_TYPE_BADGE_OWN = "Badge Own";
    public static final String EVENT_LEADER_BOARD_TYPE_BADGE_OTHERS = "Badge Others";
    public static final String EVENT_LEADER_BOARD_TYPE_HELP_OWN = "Help Own";
    public static final String EVENT_LEADER_BOARD_TYPE_HELP_OTHERS = "Help Others";
    public static final String EVENT_LEADER_BOARD_TYPE_WALL_OF_FAME = "Wall Of Fame";
    public static final String EVENT_LEADER_BOARD_TYPE_CLAIM_BONUS_RETAIL = "Claim Bonus Retail";
    public static final String EVENT_LEADER_BOARD_TYPE_CLAIM_BONUS_RETAIL_TARGET = "Claim Bonus Retail Target";
    public static final String EVENT_LEADER_BOARD_TYPE_CLAIM_BONUS_TEAM_ZERO_PENDING = "Claim Bonus Team Zero Pending";
    public static final String EVENT_LEADER_BOARD_TYPE_BADGES_EARNED = "Badges Earned";
    public static final String EVENT_LEADER_BOARD_TYPE_PREVIOUS_MONTH = "Previous Month";

    /*Post booking td/visit*/
    public static final String EVENT_POST_BOOKING_TD_VISIT = "Post Booking Td Visit";

    /*ETVBR Filters*/
    public static final String EVENT_ETVBR_FILTERS = "ETVBR Filters";
    public static final String EVENT_ETVBR_FILTERS_KEY_TYPE = "Type";

    /*Zero Bytes Recordings*/
    public static final String EVENT_ZERO_BYTE_RECORDINGS = "Zero Byte Recordings";
    public static final String EVENT_ZERO_BYTE_RECORDINGS_KEY_BRAND = "Brand";
    public static final String EVENT_ZERO_BYTE_RECORDINGS_KEY_MODEL = "Model";
    public static final String EVENT_ZERO_BYTE_RECORDINGS_KEY_OS = "OS API";

    /*TeamDashboard*/
    public static final String EVENT_CLICKS = "Clicks";
    public static final String EVENT_CLICKS_KEY_TYPE="Type";

    /*Telephony*/
    public static final String EVENT_TELEPHONY = "Telephony";
    public static final String EVENT_TELEPHONY_KEY_TYPE = "Type";
    public static final String EVENT_TELEPHONY_TYPE_STATUS = "Status";
    public static final String EVENT_TELEPHONY_TYPE_INCOMING_STARTED= "Incoming Started";
    public static final String EVENT_TELEPHONY_TYPE_INCOMING_ENDED= "Incoming Ended";
    public static final String EVENT_TELEPHONY_TYPE_INCOMING_ENDED_FORCE= "Incoming Ended Force";
    public static final String EVENT_TELEPHONY_TYPE_OUTGOING_STARTED = "Outgoing Started";
    public static final String EVENT_TELEPHONY_TYPE_OUTGOING_ENDED = "Outgoing Ended";
    public static final String EVENT_TELEPHONY_TYPE_OUTGOING_ENDED_FORCE = "Outgoing Ended Force";
    public static final String EVENT_TELEPHONY_TYPE_MISSED_CALL_ENDED = "Missed Call Ended";

    public static final String EVENT_TELEPHONY_TYPE_SYNC_CDR_START = "SYNC CDR Start";
    public static final String EVENT_TELEPHONY_TYPE_SYNC_CDR_SUCCESS = "SYNC CDR Success";
    public static final String EVENT_TELEPHONY_TYPE_SYNC_CDR_FAILED = "SYNC CDR Failed";

    public static final String EVENT_TELEPHONY_TYPE_SYNC_FILE_START = "SYNC File Start";
    public static final String EVENT_TELEPHONY_TYPE_SYNC_FILE_SUCCESS = "SYNC File Success";
    public static final String EVENT_TELEPHONY_TYPE_SYNC_FILE_FAILED = "SYNC File Failed";

    public static final String EVENT_TELEPHONY_TYPE_RECORDING_RESTRICTED = "Recording Restricted";

    public static final String EVENT_TELEPHONY_KEY_CDR_SYNC_PERCENTAGE = "CDR Sync Percentage";
    public static final String EVENT_TELEPHONY_KEY_FILE_SYNC_PERCENTAGE = "File Sync Percentage";

    public static final String EVENT_TELEPHONY_KEY_TRIGGERED_NUMBER = "Triggered Number";
    public static final String EVENT_TELEPHONY_KEY_SEARCHED_NUMBER = "Searched Number";
    public static final String EVENT_TELEPHONY_KEY_CDR_NUMBER = "CDR Number";
    public static final String EVENT_TELEPHONY_KEY_CDR_LEAD_ID = "CDR Lead Id";
    public static final String EVENT_TELEPHONY_KEY_CDR_DETAILS = "CDR Details";
    public static final String EVENT_TELEPHONY_KEY_DB_RESULT = "DB Result";
     public static final String EVENT_TELEPHONY_KEY_FAILURE_REASON = "Failed Reason";
    public static final String EVENT_TELEPHONY_FAILURE_REASON_NETWORK = "NETWORK";
    public static final String EVENT_TELEPHONY_FAILURE_REASON_STATUS_CODE = "Status Code";
    public static final String EVENT_TELEPHONY_FAILURE_REASON_ANDROID_PIE = "Android Pie";

    /*Settings*/
    public static final String EVENT_SETTINGS = "Settings";
    public static final String EVENT_SETTINGS_KEY_TYPE = "Type";
    public static final String EVENT_SETTINGS_TYPE_SMS_EMAIL = "SMS EMAIL";


    /*Events*/
    public static final String EVENT_DEALER_EVENTS = "Events";
    public static final String EVENT_DEALER_EVENTS_KEY_TYPE = "Type";
    public static final String EVENT_DEALER_EVENTS_TYPE_ADD_EVENT = "Add Event";
    public static final String EVENT_DEALER_EVENTS_TYPE_EDIT_EVENT = "Edit Event";


    //API Error
    public static final String EVENT_API_ERROR = "API ERROR";
    public static final String EVENT_API_ERROR_KEY_API = "Api";
    public static final String EVENT_API_ERROR_API_HERE_MAP = "Here Map";
    public static final String EVENT_API_ERROR_KEY_INPUT = "Input";
    public static final String EVENT_API_ERROR_KEY_RESPONSE = "Response";

    //Copy Remarks
    public static final String EVENT_COPY_REMARKS = "Copy Remarks";


}