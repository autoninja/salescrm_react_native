package com.salescrm.telephony.db.ref_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by prateek on 8/11/17.
 */

public class RefSalesManager extends RealmObject {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("info")
    @Expose
    private RefInfo info;
    @SerializedName("team_leaders")
    @Expose
    private RealmList<RefTeamLeader> team_leaders = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RefInfo getInfo() {
        return info;
    }

    public void setInfo(RefInfo info) {
        this.info = info;
    }

    public RealmList<RefTeamLeader> getTeamLeaders() {
        return team_leaders;
    }

    public void setTeamLeaders(RealmList<RefTeamLeader> teamLeaders) {
        this.team_leaders = teamLeaders;
    }
}
