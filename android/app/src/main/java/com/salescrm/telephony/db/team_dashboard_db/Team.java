package com.salescrm.telephony.db.team_dashboard_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 11/6/17.
 */

public class Team extends RealmObject {

    @SerializedName("teamid")
    @Expose
    @PrimaryKey
    private Integer teamid;
    @SerializedName("teamName")
    @Expose
    private Integer teamName;
    @SerializedName("tlName")
    @Expose
    private String tlName;
    @SerializedName("dateTo")
    @Expose
    private String dateTo;
    @SerializedName("dateFrom")
    @Expose
    private String dateFrom;
    @SerializedName("abs")
    @Expose
    private RealmList<Ab> abs = null;
    @SerializedName("rel")
    @Expose
    private RealmList<Rel> rel = null;

    public Integer getTeamid() {
        return teamid;
    }

    public void setTeamid(Integer teamid) {
        this.teamid = teamid;
    }

    public Integer getTeamName() {
        return teamName;
    }

    public void setTeamName(Integer teamName) {
        this.teamName = teamName;
    }

    public String getTlName() {
        return tlName;
    }

    public void setTlName(String tlName) {
        this.tlName = tlName;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public RealmList<Ab> getAbs() {
        return abs;
    }

    public void setAbs(RealmList<Ab> abs) {
        this.abs = abs;
    }

    public RealmList<Rel> getRel() {
        return rel;
    }

    public void setRel(RealmList<Rel> rel) {
        this.rel = rel;
    }
}
