package com.salescrm.telephony.interfaces;

public interface RefreshCallBack {
    void onRefresh();
}
