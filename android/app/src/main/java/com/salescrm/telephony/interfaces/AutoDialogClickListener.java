package com.salescrm.telephony.interfaces;

import android.view.View;

import com.salescrm.telephony.model.AutoDialogModel;

/**
 * Created by bharath on 28/10/16.
 */

public interface AutoDialogClickListener {
    void onAutoDialogYesButtonClick(View view, AutoDialogModel data);
    void onAutoDialogNoButtonClick(View view, AutoDialogModel data);

}
