import React from 'react';
import { createStackNavigator } from 'react-navigation';
import SettingsScreen from  './index';
import CommunicationSettings from '../CommunicationSettings';
import HamburgerIcon from '../../../components/uielements/HamburgerIcon';
import BackIcon from "../../../components/uielements/BackIcon";

const SettingsRouter = createStackNavigator(
    {
        Settings : {
            screen:SettingsScreen,
            navigationOptions: ({ navigation }) => ({
                title: 'Settings',
                headerBackTitle : null,
                headerLeft : <HamburgerIcon navigationProps={ navigation }/>,
                headerStyle: {
                    backgroundColor: '#2e5e86'
                },
                headerTransperent: true,
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                    width: '100%',
                    textAlign: 'left',
                },
            })
        },
        CommunicationSettings : {
            screen:CommunicationSettings,
            navigationOptions: ({ navigation }) => ({
                title: 'SMS / Email',
                headerBackTitle : null,
                headerLeft : <BackIcon navigationProps={ navigation }/>,
                headerStyle: {
                    backgroundColor: '#2e5e86'
                },
                headerTransperent: true,
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                    width: '100%',
                    textAlign: 'left',
                },
            })
        },
    },
    {
        initialRouteName: "Settings",
        initialRouteKey: "settings"
    }
);

export default SettingsRouter;