
 class AppConfModel{
  constructor(id, key, value, is_edit, is_active){
    this.id = id;
    this.key = key;
    this.value = value;
    this.is_edit = is_edit;
    this.is_active = is_active;
  }
}

module.exports = { AppConfModel };
