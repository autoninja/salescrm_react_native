package com.salescrm.telephony.utils;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.preferences.Preferences;

public class FabricUtils {

    public static void sendEvent(Preferences pref, String eventName, String customTypeName, String customTypeValue){
        System.out.println("Event Name:"+eventName+":prof:"+customTypeName+":value:"+customTypeValue);
        if (Answers.getInstance() != null && pref!=null) {
            Answers.getInstance().logCustom(new CustomEvent(eventName)
                    .putCustomAttribute("User Name", pref.getUserName())
                    .putCustomAttribute(customTypeName, customTypeValue)
                    .putCustomAttribute("Roles", DbUtils.getRolesCombination()));
        }
    }
    public static void sendEvent(Preferences pref, String eventName){
        if (Answers.getInstance() != null && pref!=null) {
            Answers.getInstance().logCustom(new CustomEvent(eventName)
                    .putCustomAttribute("Dealer Name", pref.getDealerName())
                    .putCustomAttribute("Roles", DbUtils.getRolesCombination()));
        }
    }

}
