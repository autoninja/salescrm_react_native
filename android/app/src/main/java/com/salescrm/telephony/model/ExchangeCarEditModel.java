package com.salescrm.telephony.model;

import com.salescrm.telephony.db.ExchangeStatusdb;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bannhi on 11/1/17.
 */

public class ExchangeCarEditModel {

    String lead_id;
    Car car;

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public class Car {
        private String expected_price;
        private String lead_exchange_car_id;
        private String market_price;
        private String model_id;
        private String name;
        private String newExpectedPrice;
        private String newMarketPrice;
        private String newPriceQuoted;
        private String price_quoted;
        private String purchase_date;
        private String edit;
        private String reg_no;
        private String kms_run;
        private ArrayList<ExchangeStatus> exchangeStatus;


        public String getReg_no() {
            return reg_no;
        }

        public void setReg_no(String reg_no) {
            this.reg_no = reg_no;
        }

        public String getKms_run() {
            return kms_run;
        }

        public void setKms_run(String kms_run) {
            this.kms_run = kms_run;
        }
        public String getExpected_price() {
            return expected_price;
        }

        public void setExpected_price(String expected_price) {
            this.expected_price = expected_price;
        }

        public String getLead_exchange_car_id() {
            return lead_exchange_car_id;
        }

        public void setLead_exchange_car_id(String lead_exchange_car_id) {
            this.lead_exchange_car_id = lead_exchange_car_id;
        }

        public String getMarket_price() {
            return market_price;
        }

        public void setMarket_price(String market_price) {
            this.market_price = market_price;
        }

        public String getModel_id() {
            return model_id;
        }

        public void setModel_id(String model_id) {
            this.model_id = model_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNewExpectedPrice() {
            return newExpectedPrice;
        }

        public void setNewExpectedPrice(String newExpectedPrice) {
            this.newExpectedPrice = newExpectedPrice;
        }

        public String getNewMarketPrice() {
            return newMarketPrice;
        }

        public void setNewMarketPrice(String newMarketPrice) {
            this.newMarketPrice = newMarketPrice;
        }

        public String getNewPriceQuoted() {
            return newPriceQuoted;
        }

        public void setNewPriceQuoted(String newPriceQuoted) {
            this.newPriceQuoted = newPriceQuoted;
        }

        public String getPrice_quoted() {
            return price_quoted;
        }

        public void setPrice_quoted(String price_quoted) {
            this.price_quoted = price_quoted;
        }

        public String getPurchase_date() {
            return purchase_date;
        }

        public void setPurchase_date(String purchase_date) {
            this.purchase_date = purchase_date;
        }


        public String getEdit() {
            return edit;
        }

        public void setEdit(String edit) {
            this.edit = edit;
        }

        public ArrayList<ExchangeStatus> getExchangeStatus() {
            return exchangeStatus;
        }

        public void setExchangeStatus(ArrayList<ExchangeStatus> exchangeStatus) {
            this.exchangeStatus = exchangeStatus;
        }


    }

    public class ExchangeStatus {
        private String stage_id;
        private String name;
        private String width;
        private String bar_clas;

        public String getStage_id() {
            return stage_id;
        }

        public void setStage_id(String stage_id) {
            this.stage_id = stage_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getBar_clas() {
            return bar_clas;
        }

        public void setBar_clas(String bar_clas) {
            this.bar_clas = bar_clas;
        }
    }
}
