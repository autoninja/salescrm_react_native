import React, { Component } from 'react'
import { Platform, View, Text, Image, TouchableOpacity, YellowBox, StatusBar,  FlatList, ActivityIndicator, Dimensions, Alert } from 'react-native'

import RestClient from '../../../network/rest_client'
import { StackActions, NavigationActions } from 'react-navigation';

import {eventETVBRFilters} from '../../../clevertap/CleverTapConstants';
import CleverTapPush from '../../../clevertap/CleverTapPush'

import { connect } from 'react-redux';
import { updateTeamDashboard } from '../../../actions/team_dashboard_actions';


import ETVBRFiltersItemMain from '../../../components/filters/etvbr_filters_item_main'


window.getETVBRFilterItem = function(key) {
    let l = 0;
    for(let i=0; i<global.global_etvbr_filters_selected.length; i++) {
        if(global.global_etvbr_filters_selected[i].key === key) {
            l = global.global_etvbr_filters_selected[i].values.length;
        }
    }
    return l;
};

window.isFilterSelected= function() {
    for(let i=0; i<global.global_etvbr_filters_selected.length; i++) {
        if(global.global_etvbr_filters_selected[i].values.length>0) {
            return true;
        }
    }
    return false;
};

window.getFilterSelectedCount= function() {
    return global.global_etvbr_filters_selected.length;
};

window.clearAllFilterSelected= function() {
    global.global_etvbr_filters_selected = [];
};

window.addOrRemoveETVBRFilterItem = function(key, val, sub_category, isAdd) {
    if(sub_category) {
        console.log('isAdd'+isAdd);
        console.log('key'+key);
        console.log('val'+val);
        console.log('SUB_CATEGORY:'+JSON.stringify(sub_category));
        let keyIndex = -1;
        for(let i = 0; i < global.global_etvbr_filters_selected.length; i++) {
            console.log('key:'+global.global_etvbr_filters_selected[i].key);
            if(global.global_etvbr_filters_selected[i].key === key) {
                keyIndex = i;
                break;
            }
        }

        if(keyIndex === -1) {
            global.global_etvbr_filters_selected.push({key,values:[]});
            keyIndex = global.global_etvbr_filters_selected.length-1;
        }



        let innerKeyIndex = -1;
        for(let i = 0; i < global.global_etvbr_filters_selected[keyIndex].values.length; i++) {
            console.log('ID:'+global.global_etvbr_filters_selected[keyIndex].values[i].id);
            if(global.global_etvbr_filters_selected[keyIndex].values[i].id === val) {
                innerKeyIndex = i;
                break;
            }
        }
        if(innerKeyIndex>-1) {
            //Add or remove here
            if(isAdd) {
                global.global_etvbr_filters_selected[keyIndex].values[innerKeyIndex].subcategories.push({id:sub_category.id})
            }
            else {
                //remove
                for(let i = 0; i < global.global_etvbr_filters_selected[keyIndex].values[innerKeyIndex].subcategories.length; i++) {

                    if(global.global_etvbr_filters_selected[keyIndex].values[innerKeyIndex].subcategories[i].id === sub_category.id) {
                        global.global_etvbr_filters_selected[keyIndex].values[innerKeyIndex].subcategories.splice(i,1);
                        break;
                    }
                }

                //if empty remove value:id also
                if(global.global_etvbr_filters_selected[keyIndex].values[innerKeyIndex].subcategories.length === 0) {
                    global.global_etvbr_filters_selected[keyIndex].values.splice(innerKeyIndex,1);
                }
            }
        }
        else if(isAdd){
            global.global_etvbr_filters_selected[keyIndex].values.push({id:val,subcategories:[{id:sub_category.id}]})
        }
        console.log(JSON.stringify(global.global_etvbr_filters_selected));
        return;
    }

    //global.global_etvbr_filters_selected.push({key,values:)
    console.log('ClickedL:'+key+val);
    let keyIndex = -1;
    for(let i = 0; i < global.global_etvbr_filters_selected.length; i++) {
        if(global.global_etvbr_filters_selected[i].key === key) {
            keyIndex = i;
            break;
        }
    }
    if(keyIndex>-1) {
        const valOfKey = global.global_etvbr_filters_selected[keyIndex];
        let removeIndex = -1;
        for(let i=0;i<valOfKey.values.length;i++) {
            if(valOfKey.values[i].id === val) {
                removeIndex = i;
                break;
            }
        }
        if(removeIndex>-1) {
            valOfKey.values.splice(removeIndex,1);
        }
        else {
            valOfKey.values.push({id:val});
        }
    }
    else{
        global.global_etvbr_filters_selected.push({key, values:[{id:val}]})
    }
    console.log(JSON.stringify(global.global_etvbr_filters_selected));
};

window.addAllETVBRFilterItem = function(filter, isAdd) {

    let keyIndex = -1;
    for(let i = 0; i < global.global_etvbr_filters_selected.length; i++) {
        if(global.global_etvbr_filters_selected[i].key === filter.key) {
            keyIndex = i;
            break;
        }
    }
    if(keyIndex>-1) {
        global.global_etvbr_filters_selected.splice(keyIndex,1);

    }
    if(isAdd) {
        const values = [];
        for(let i=0;i<filter.values.length;i++) {
            values.push({id:filter.values[i].id});
        }
        global.global_etvbr_filters_selected.push({key:filter.key, values})
    }
    console.log(JSON.stringify(global.global_etvbr_filters_selected));
};

window.addAllETVBRFilterItemWithSubCategories = function(key, firstVal,sub_category, isAdd) {
    let keyIndex = -1;
    for(let i = 0; i < global.global_etvbr_filters_selected.length; i++) {
        console.log('key:'+ global.global_etvbr_filters_selected[i].key);
        if(global.global_etvbr_filters_selected[i].key === key) {
            keyIndex = i;
            break;
        }
    }

    if(keyIndex === -1) {
        global.global_etvbr_filters_selected.push({key,values:[]});
        keyIndex = global.global_etvbr_filters_selected.length-1;
    }

    let innerKeyIndex = -1;
    for(let i = 0; i < global.global_etvbr_filters_selected[keyIndex].values.length; i++) {
        console.log('ID:'+global.global_etvbr_filters_selected[keyIndex].values[i].id);
        if(global.global_etvbr_filters_selected[keyIndex].values[i].id === firstVal) {
            innerKeyIndex = i;
            break;
        }
    }
    if(innerKeyIndex>-1) {
        global.global_etvbr_filters_selected[keyIndex].values.splice(innerKeyIndex,1);
    }

    if(isAdd) {
        const subcategories = [];
        let value={};
        for(let i=0;i<sub_category.length;i++) {
            subcategories.push({id:sub_category[i].id});
        }
        value =({id:firstVal,subcategories});
        global.global_etvbr_filters_selected[keyIndex].values.push(value);
    }
    else {
        for(let i = 0; i < global.global_etvbr_filters_selected[keyIndex].values.length; i++) {
            if(global.global_etvbr_filters_selected[keyIndex].values.id === firstVal) {
                global.global_etvbr_filters_selected[keyIndex].values.splice(i,1);
                break;
            }
        }
    }
    console.log(JSON.stringify(global.global_etvbr_filters_selected));
};

window.isFilterItemExists = function(key, val, sub_category) {
    if(sub_category) {
        let keyIndex = -1;
        for(let i = 0; i < global.global_etvbr_filters_selected.length; i++) {
            if(global.global_etvbr_filters_selected[i].key === key) {
                keyIndex = i;
                break;
            }
        }
        if(keyIndex>-1) {
            const valOfKey = global.global_etvbr_filters_selected[keyIndex];
            let removeIndex = -1;
            for(let i=0;i<valOfKey.values.length;i++) {
                if(valOfKey.values[i].id === val) {
                    removeIndex = i;
                    for(let j=0;j<valOfKey.values[i].subcategories.length;j++) {
                        if(valOfKey.values[i].subcategories[j].id === sub_category.id) {
                            return true;
                        }
                    }
                    return false;
                }
            }
        }
        return false;
    }
    let keyIndex = -1;
    for(let i = 0; i < global.global_etvbr_filters_selected.length; i++) {
        if(global.global_etvbr_filters_selected[i].key === key) {
            keyIndex = i;
            break;
        }
    }
    if(keyIndex>-1) {
        const valOfKey = global.global_etvbr_filters_selected[keyIndex];
        let removeIndex = -1;
        for(let i=0;i<valOfKey.values.length;i++) {
            if(valOfKey.values[i].id === val) {
                removeIndex = i;
                break;
            }
        }
        if(removeIndex>-1) {
            return true;
        }
    }
    return false;
};



class ETVBRFilters extends Component {

  state = {
    update_etvbr: false,
    update_live_ef: false
  }
  constructor(props) {

    super(props);
    this.state = {loading:true, filters:[],update:false, filter_applied:false};



    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);

  }
  componentDidMount() {
  this._mounted = true
  this.getETVBRFiltersData();
  }

 componentWillUnmount () {
   this._mounted = false
 }

 logout() {
    var logout = StackActions.reset({
       index: 0, // <-- currect active route from actions array
       key:null,
       actions: [
         NavigationActions.navigate({ routeName: 'Login' }),
       ],
     });
     this.props.navigation.dispatch(logout);
  }

  getETVBRFiltersData() {
    this.setState({ loading: true});


    new RestClient().getETVBRFilters().then( (data) => {
              if(data.statusCode===4001) {
                Alert.alert(
              'Failed',
              'Please try again',
              [
                {text: 'Logout', onPress: () => this.logout()},
                {text: 'Retry', onPress: () =>  this.getETVBRFiltersData()},
              ],
              { cancelable: false }
            )
                return;
              }
        if(this._mounted){
        this.setState({ loading: false, filters: data.result.filters});

        }

        }).catch((error) => {
          if (error.response && error.response.status == 401) {
            Alert.alert(
          'Failed',
          'Please try again',
          [
            {text: 'Logout', onPress: () => this.logout()},
            {text: 'Retry', onPress: () =>  this.getETVBRFiltersData()},
          ],
          { cancelable: false }
        )
          }
        });;


  }

  postCleverTap () {
    let selectedFilters = [];
    this.state.filters.map( function(item) {
        global.global_etvbr_filters_selected.map(function(selectedFilter) {
          if(item.key == selectedFilter.key) {
            selectedFilters.push(item.name);
          }
        });
    });

    let selectedFiltersStr = '[';
    if(selectedFilters.length>0) {
      selectedFilters.map((data, index) => {
          selectedFiltersStr = selectedFiltersStr + data;
          if(index < selectedFilters.length-1) {
            selectedFiltersStr  = selectedFiltersStr + ", ";
          }
      });
    }
    selectedFiltersStr  = selectedFiltersStr + ']';
    CleverTapPush.pushEvent(eventETVBRFilters.event, { 'Type' : selectedFiltersStr });

  }

  closeButtonClickHandler(navigation, isFilterApplied) {
  //  this.setState({filter_applied:true});
    if(isFilterApplied && this.state.filters && global.global_etvbr_filters_selected) {
      this.postCleverTap();
    }

    navigation.state.params.onGoBack()

    navigation.goBack();

    this.props.add(true, true);

    console.log('closeButtonClickHandler');


  }
  _onMainItemTouch(navigation, filter) {
    navigation.navigate('ETVBRFiltersChild',{ filter,
      onGoBack: () => this.setState({update:!this.state.update}),
    });
  }

  render() {
    const { navigation } = this.props;
    let clear='';
    let isFilter= isFilterSelected();
    if(isFilter) {
      clear = 'Clear';
    }
    if(this.state.loading) {
      return <View style={{width:(Dimensions.get('window').width)*.85,backgroundColor:'#fff', flex:1, paddingTop: (Platform.OS) === 'ios' ? 20 : 10,}}>


        <View style = {{display:(this.state.loading?'flex':'none'), flex:1,justifyContent: 'center',
          alignItems: 'center',}}>

        <ActivityIndicator
              animating={this.state.loading}
              style={{flex: 1,   alignSelf:'center',}}
              color="#303030"
              size="large"
              hidesWhenStopped={true}

        />
        </View>
        </View>
    }
    return   <View style={{width:(Dimensions.get('window').width)*.85,backgroundColor:'#fff', flex:1, paddingTop: (Platform.OS) === 'ios' ? 20 : 10,}}>
      <View style={{flex:1}}>
      <View style={{flexDirection:'row', padding:16, justifyContent:'space-between'}}>
      <TouchableOpacity onPress ={this.closeButtonClickHandler.bind(this,this.props.navigation, false)}>
      <Image source={require('../../../images/ic_close_black.png')} style={{width:24, height:24}}/>
        </TouchableOpacity>
      <Text style= {{fontSize:18, color:'#494949', fontWeight:'bold'}}> Filter By </Text>
      <TouchableOpacity disabled={!isFilter} onPress={()=> {
        clearAllFilterSelected();
        this.setState({update:!this.state.update})
      }}>
      <Text style= {{fontSize:17, color:'#494949', }}> {clear} </Text>
      </TouchableOpacity>
      </View>
      <View style={{backgroundColor:'#cccccc', height:1, width:'100%'}}/>

      <FlatList
      extraData = {this.state}
      data = {this.state.filters}
      renderItem={({item}) => <TouchableOpacity onPress = {this._onMainItemTouch.bind(this, this.props.navigation, item) }>
      <ETVBRFiltersItemMain key_name= {item.key} title= {item.name}/></TouchableOpacity>}
      keyExtractor={(item, index) => index+''}
      onRefresh={() => this.getETVBRFiltersData()}
      refreshing={this.state.loading}
      />


      </View>
      <TouchableOpacity disabled={!isFilter} onPress ={this.closeButtonClickHandler.bind(this,this.props.navigation, true)}>

      <View style={{height:56,justifyContent: "center",alignItems: "center", backgroundColor:isFilter?'#007fff':'#808080'}}>
      <Text adjustsFontSizeToFit={true}
           numberOfLines={1} style={{textAlign:'center', fontSize:18,textAlignVertical: 'center', color:'#fff'}}>
      Apply
      </Text>

      </View>
        </TouchableOpacity>

      </View>

  }
}

const mapStateToProps = state => {
return {
  update_etvbr: state.updateTeamDashboard.update_etvbr,
  update_live_ef: state.updateTeamDashboard.update_live_ef
}
}

const mapDispatchToProps = dispatch => {
return {
  add: (update_etvbr, update_live_ef) => {
    dispatch(updateTeamDashboard({update_etvbr, update_live_ef}))
  }
}
}

export default connect(mapStateToProps, mapDispatchToProps)(ETVBRFilters)
