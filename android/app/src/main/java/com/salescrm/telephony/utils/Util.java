package com.salescrm.telephony.utils;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.HomeActivity;
import com.salescrm.telephony.activity.NotificationRedirectActivity;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.dataitem.SectionModel;
import com.salescrm.telephony.db.FilterEnquiryStage;
import com.salescrm.telephony.db.PlannedActivitiesAction;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.dbOperation.CallStatDbOperation;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.PostBookingDbHandler;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.model.DateSectionTodayIndex;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.receiver.NotificationReceiver;
import com.salescrm.telephony.response.AllLeadCustomerDetailsResponse;
import com.salescrm.telephony.views.AutoDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;
import retrofit.client.Header;

/**
 * Created by bharath on 4/6/16.
 */
public class Util {
    private static String TAG = "Util";
    private static String[] formatStrings = {"dd MMM yyyy hh:mm aa", "yyyy-MM-dd hh:mm:ss", "yyyy-MM-dd HH:mm:ss.SSS"};
    public static HashMap<Integer, Integer> hashMapTagName = new HashMap<>();


    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);

    }

    public static void showTimePicker(Object o, Activity activity, int id) {
        Calendar now = Calendar.getInstance();
        TimePickerDialog timePicker = TimePickerDialog.newInstance(
                (com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener) o,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        timePicker.show(activity.getFragmentManager(), String.valueOf(id));
    }

    public static void showAlert(String alert, View view) {
        Snackbar snackbar = Snackbar.make(view, alert, Snackbar.LENGTH_LONG)
                .setAction("Close", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }
  /*  public static void showDatePicker(Object o, Activity activity, int id) {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                (com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener) o,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        dpd.show(activity.getFragmentManager(), String.valueOf(id));
    }*/

    public static void showDatePicker(Object o, Activity activity, int id, Calendar minCal, Calendar maxCal) {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                (com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener) o,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        if (minCal != null) {
            dpd.setMinDate(minCal);
        }
        if (maxCal != null) {
            dpd.setMaxDate(maxCal);
        }

        dpd.show(activity.getFragmentManager(), String.valueOf(id));
    }

    public static void showDatePickerPostBooking(Object o, Activity activity, int id, int TYPE, Calendar minCal, Calendar maxCal) {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                (com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener) o,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        if (minCal != null) {
            dpd.setMinDate(minCal);
        }
        if (maxCal != null) {
            dpd.setMaxDate(maxCal);
        }
        switch (TYPE) {
            case WSConstants.TYPE_DATE_PICKER.BOOKING_FOLLOW_UP_DATE:
                dpd.setMinDate(now);
                break;

        }
        dpd.show(activity.getFragmentManager(), String.valueOf(id));
    }

    public static void showDatePickerBirthDay(Object o, Activity activity, int id, Calendar maxCal) {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                (com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener) o,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        if (maxCal != null) {
            dpd.setMaxDate(maxCal);
        }
        dpd.show(activity.getFragmentManager(), String.valueOf(id));
    }

    public static void showDatePicker(Object o, Activity activity, int id, Calendar defaultCal, Calendar minCal, Calendar maxCal) {

        Calendar now = Calendar.getInstance();
        if (defaultCal != null) {
            now = defaultCal;
        }
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                (com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener) o,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        if (minCal != null) {
            dpd.setMinDate(minCal);
        }
        if (maxCal != null) {
            dpd.setMaxDate(maxCal);
        }

        dpd.show(activity.getFragmentManager(), String.valueOf(id));
    }

    public static void HideKeyboard(Context context) {
        // Check if no view has focus:
        View view = ((Activity) context).getCurrentFocus();
        System.out.println("hide.......................");
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static Date getNormalDate(String unixSeconds) {
        Date returnDate = new Date();
        try {
            Date date = new Date(Long.parseLong(unixSeconds) * 1000L); // *1000 is to convert seconds to milliseconds
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()); // the format of your date
            sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating (see comment at the bottom
            returnDate = sdf.parse(sdf.format(date));
        } catch (ParseException | NumberFormatException e1) {
            e1.printStackTrace();
            return returnDate;
        }
        return returnDate;
    }

    public static String getAmPmTime(Calendar cal) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault()); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating (see comment at the bottom
        return sdf.format(cal.getTime());
    }

    /**
     * when ==0(today), -1(yesterday), 1(tomorrow)
     *
     * @param when
     * @return
     */
    public static Date getTime(int when) {
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getDefault());
        c.add(Calendar.DATE, when);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    public static Date getDateTime(int when) {
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getDefault());
        c.add(Calendar.DATE, when);
        return c.getTime();
    }

    public static String getSQLDateTime(Date date) {
        return String.valueOf(new java.sql.Timestamp(date.getTime()));
    }
    //Format should be DD/MM/YY
    public static String getSQLDateTime(String str) {
        DateFormat format = new SimpleDateFormat("d/M/y", Locale.getDefault());
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(date!=null) {
            return String.valueOf(new java.sql.Timestamp(date.getTime()));
        }
        return null;
    }


    public static String getSQLDateTimeValidationCheck(String str) {
        if(str == null) {
            return "";
        }
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date dateValid = null;
            dateValid = format.parse(str);
            return str;
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        DateFormat format = new SimpleDateFormat("d/M/y", Locale.getDefault());
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(date!=null) {
            return String.valueOf(new java.sql.Timestamp(date.getTime()));
        }
        return null;
    }

    public static int getInt(String s) {
        try {
            return s == null || s.equals("") ? -1 : Integer.parseInt(s);
        }
        catch (Exception e) {
            return  -1;
        }
    }
    public static long getLongDefaultZero(String s) {
        return s == null || s.equals("") ? 0 : Long.parseLong(s);
    }

    public static long getLong(String s) {
        try {
            return s == null || s.equals("") ? -1 : Long.parseLong(s);
        }
        catch (Exception e) {
            return -1;
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidMobile(CharSequence target) {
        return target != null && target.length() == 10;
    }

    public static int getListSize(List list) {
        return list == null ? 0 : list.size();
    }


    public static void systemPrint(String s) {
        System.out.println(s);
    }

    public static void expand(final RelativeLayout v) {
        v.measure(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? RelativeLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final RelativeLayout v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static boolean isNotNull(String customerName) {
        return customerName != null && !TextUtils.isEmpty(customerName.trim());
    }
    public static boolean isNull(String data) {
        return data == null  || TextUtils.isEmpty(data.trim());
    }

    public static String getGender(String gender) {
        if (Util.isNotNull(gender)) return gender;
        else {
            return "";
        }
    }


    public static void callNumber(final String mobileNumber,
                                  final Context context,
                                  String leadId,
                                  String scheduledActivityId
    ) {
        new AutoDialog(context, new CallDialog(context, mobileNumber, leadId, scheduledActivityId), new AutoDialogModel("Do you want to call?\n\n" + mobileNumber)).show();

    }

    public static Calendar getCalender(String unixSeconds) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(Long.parseLong(unixSeconds));
        return cal;
    }
    public static String getMonthYear(String unixSeconds) {
        if(unixSeconds == null) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(Long.parseLong(unixSeconds));
        return (cal.get(Calendar.MONTH)+1)+"-"+(cal.get(Calendar.YEAR));

    }

    public static void updateHeaders(List<Header> headers) {
        for (Header header : headers) {
            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                System.out.println("Updating token:::"+header.getValue());
                ApiUtil.UpdateAccessToken(header.getValue());
            }
        }
    }

    public static String getAdvanceFilterKey(int insideKey) {
        String key = "";
        switch (insideKey) {
            case 0:
                key = "activityId";
                break;
            case 1:
                key = "leadTagsName";
                break;
            case 2:
                key = "leadCarModelId";
                break;
            case 3:
                key = "leadSourceName";
                break;
            case 4:
                key = "leadAge";
                break;
            case 5:
                key = "leadStage";
                break;
            case 6:
                key = "buyerType";
                break;
            case 7:
                key = "date";
                break;

        }
        return key;
    }

    public static String getFilterLogKey(int insideKey) {
        String key = "";
        switch (insideKey) {
            case 0:
                key = "Tasks Type";
                break;
            case 1:
                key = "Tags";
                break;
            case 2:
                key = "Car Models";
                break;
            case 3:
                key = "Lead Source";
                break;
            case 4:
                key = "Ageing";
                break;
            case 5:
                key = "Lead Stage";
                break;
            case 6:
                key = "Buyer Type";
                break;
            case 7:
                key = "Date";
                break;

        }
        return key;
    }

    public static String getAdvanceFilterKeyForLeadList(int insideKey) {
        String key = "";
        switch (insideKey) {
            case 0:
                key = "activity_id";
                break;
            case 1:
                key = "lead_tag_name";
                break;
            case 2:
                key = "variant_name";
                break;
            case 3:
                key = "lead_source_name";
                break;
            case 4:
                key = "lead_age";
                break;
            case 5:
                key = "lead_stage";
                break;
            case 6:
                key = "buyer_type";
                break;
            case 7:
                key = "date";
                break;

        }
        return key;
    }

    public static int getDifferenceBetweenDatesAll(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            return 0;
        }
        return (int) (Math.abs(date1.getTime() - date2.getTime())
                / (1000 * 60 * 60 * 24));
    }

    /**
     * get difference between past dates only
     *
     * @param todayDate
     * @param date2
     * @return
     */
    public static int getDifferenceBetweenDates(Date todayDate, Date date2) {
        if (todayDate == null || date2 == null) {
            return 0;
        }
        if (todayDate.after(date2) || todayDate.equals(date2)) {
            return (int) ((todayDate.getTime() - date2.getTime())
                    / (1000 * 60 * 60 * 24)) + 1;
        } else if (todayDate.before(date2)) {
            return (int) (date2.getTime() - todayDate.getTime())
                    / (1000 * 60 * 60 * 24);
        } else return 0;

    }


    public static Date getDate(String str) {
        if (str == null || str.equalsIgnoreCase("")) {
            return null;
        }
        DateFormat format = new SimpleDateFormat("y-M-d H:m:s", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            try{
                date = new SimpleDateFormat("y-M-d", Locale.ENGLISH).parse(str);
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        return date;
    }

    public static Date getDateAsJs(String str) {
        if (str == null || str.equalsIgnoreCase("")) {
            return null;
        }
        DateFormat format = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getAge(String age) {
        if (age == null) {
            return "-";
        } else if (getInt(age) <= 0) {
            return "-";
        } else {
            return age;
        }
    }

    public static boolean isActionsValid(List<AllLeadCustomerDetailsResponse.Result.Customer_details.PlannedActivities.Actions> actions) {
        for (int i = 0; i < actions.size(); i++) {
            if (actions.get(i).getAction_id().equalsIgnoreCase(WSConstants.FormAction.PRE_DONE + "")) {
                return true;
            }
        }
        return false;
    }

    public static boolean isActionsValid(RealmList<PlannedActivitiesAction> actions) {
        for (int i = 0; i < actions.size(); i++) {
            if (actions.get(i).getPlannedActivitiesActionsId() == WSConstants.FormAction.PRE_DONE) {
                return true;
            }
        }
        return false;
    }

    public static void showToast(Context context, String s, int lengthShort) {
        if (context != null) {
            Toast.makeText(context, s, lengthShort).show();
        }
    }

    public static Object convertViewToDrawable(View view) {
        int spec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(spec, spec);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap b = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        c.translate(-view.getScrollX(), -view.getScrollY());
        view.draw(c);
        view.setDrawingCacheEnabled(true);
        Bitmap cacheBmp = view.getDrawingCache();
        Bitmap viewBmp = cacheBmp.copy(Bitmap.Config.ARGB_8888, true);
        view.destroyDrawingCache();
        return new BitmapDrawable(viewBmp);

    }

    public static void postBookingDbReschedule(Realm realm, final Integer scheduledActivityId, final int actionId, final Date date) {
        if (scheduledActivityId != null) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    SalesCRMRealmTable dbRow = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
                    if (dbRow != null) {
                        if (actionId == 6) {
                            dbRow.setActivityScheduleDate(date);
                        }
                    }
                }
            });

        }
    }

    public static Date getDateFormatOnly(String str) {
        if (str == null || str.equalsIgnoreCase("")) {
            return null;
        }
        DateFormat format = new SimpleDateFormat("d-M-y", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static void call(final Context context, final Preferences pref, Realm realm) {
        if(context == null) {
            return;
        }
        if (pref.isFabricsNeeded()) {
            Answers.getInstance().logCustom(new CustomEvent("C360 Call")
                    .putCustomAttribute("Dealer Name", pref.getDealerName())
                    .putCustomAttribute("IMEI", pref.getImeiNumber())
                    .putCustomAttribute("User Name", pref.getUserName()));
        }

        SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findFirst();
        if (salesCRMRealmTable != null) {

            AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
            //builderSingle.setIcon(R.drawable.);
            builderSingle.setTitle("Select One Number to call:-");

            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.select_dialog_singlechoice);
            for (int i = 0; i < salesCRMRealmTable.customerPhoneNumbers.size(); i++) {
                arrayAdapter.add("" + salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumber());
            }


            builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pref.setmTelephonyLeadID("0");
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    String strName = arrayAdapter.getItem(which);

                    try {
                        pref.setmTelephonyLeadID(pref.getLeadID());
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + strName));
                        context.startActivity(callIntent);
                    } catch (ActivityNotFoundException activityException) {
                        pref.setmTelephonyLeadID("0");
                        Log.e("Calling a Phone Number", "Call failed", activityException);
                    }

                }
            });
            builderSingle.show();

        }
    }

    private static class CallDialog implements AutoDialogClickListener {
        String mobileNumber;
        String leadId;
        String scheduledActivityId;
        Context context;
        Preferences preferences;

        public CallDialog(Context context, String mobileNumber, String leadId, String scheduledActivityId) {
            this.mobileNumber = mobileNumber;
            this.context = context;
            this.leadId = leadId;
            this.scheduledActivityId = scheduledActivityId;
            this.preferences = Preferences.getInstance();
            this.preferences.load(context);
        }

        @Override
        public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mobileNumber));
            intent.putExtra("com.android.phone.extra.slot", 0); //For sim 1
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show();

                return;
            }
            long createdAt = System.currentTimeMillis();
            preferences.setCallStatCreatedAt(createdAt);
            CallStatDbOperation.createOnStartCall(
                    createdAt,
                    mobileNumber,
                    leadId,
                    scheduledActivityId);
            CleverTapPush.pushCommunicationEvent(CleverTapConstants.EVENT_COMMUNICATION_CALL, true);
            context.startActivity(intent);
        }

        @Override
        public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {
            Preferences pref = Preferences.getInstance();
            pref.load(context);
            pref.setmTelephonyLeadID("0");
        }
    }

    public static String getDateFormatForSections(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MM", Locale.getDefault()); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating (see comment at the bottom
        return sdf.format(date);
    }
    public static String getDateFormatWithYear(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault()); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating (see comment at the bottom
        return sdf.format(date);
    }

    public static DateSectionTodayIndex createDateSections(OrderedRealmCollection<SalesCRMRealmTable> data, int tab){
        int todayIndex = -1;
        List<SectionModel> sections =new ArrayList<>();
        Date prevDate =null;
        Date currentDate;

        for(int i=0;i<data.size();i++){
            if(data.get(i).getActivityScheduleDate()==null){
                return new DateSectionTodayIndex(sections,todayIndex);
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(data.get(i).getActivityScheduleDate().getTime());
            calendar.set(Calendar.HOUR_OF_DAY,0);
            calendar.set(Calendar.MINUTE,0);
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MILLISECOND,0);
            currentDate =calendar.getTime();
            StringBuilder title =new StringBuilder();
            if(tab!=WSConstants.TODAY_TAB){
                title.append(Util.getDateFormatForSections(data.get(i).getActivityScheduleDate()));
                if (data.get(i).getActivityScheduleDate().after(Util.getTime(-1)) && data.get(i).getActivityScheduleDate().before(Util.getTime(0))) {
                    //Yesterday
                    title.append(" (Yesterday)");
                } else if (data.get(i).getActivityScheduleDate().after(Util.getTime(0)) && data.get(i).getActivityScheduleDate().before(Util.getTime(1))) {
                    //Today
                    if(todayIndex<0){
                        todayIndex= i;
                    }
                    title.append(" (Today)");
                } else if (data.get(i).getActivityScheduleDate().after(Util.getTime(1)) && data.get(i).getActivityScheduleDate().before(Util.getTime(2))) {
                    //Tomorrow
                    title.append(" (Tomorrow)");

                }
            }else{
                int diff = Util.getDifferenceBetweenDates(Util.getTime(0),data.get(i).getActivityScheduleDate());
                if(diff>0){
                    if(diff == 1){
                        title.append("Pending ("+Util.getDifferenceBetweenDates(Util.getTime(0),data.get(i).getActivityScheduleDate())+" day)");
                    }else{
                        title.append("Pending ("+Util.getDifferenceBetweenDates(Util.getTime(0),data.get(i).getActivityScheduleDate())+" days)");
                    }

                } else if (data.get(i).getActivityScheduleDate().after(Util.getTime(-1)) && data.get(i).getActivityScheduleDate().before(Util.getTime(0))) {
                    //Yesterday
                    title.append(" (Yesterday)");
                } else if (data.get(i).getActivityScheduleDate().after(Util.getTime(0)) && data.get(i).getActivityScheduleDate().before(Util.getTime(1))) {
                    //Today
                    if(todayIndex<0){
                        todayIndex= i;
                    }
                    title.append(" (Today)");
                }


            }





            if(i==0){
                sections.add(new SectionModel(i,title.toString()));
            }
            else if(TimeUnit.MILLISECONDS.toDays((currentDate.getTime()-prevDate.getTime()))!=0){
                sections.add(new SectionModel(i,title.toString()));
            }
            prevDate = currentDate;

   /*         System.out.println("I:--------------------"+i);
            System.out.println("Current Date:"+currentDate);
            System.out.println("Prev Date:"+prevDate);
            System.out.println("Difference:"+ TimeUnit.MILLISECONDS.toDays((currentDate.getTime()-prevDate.getTime())));*/




        }
        return new DateSectionTodayIndex(sections,todayIndex);
    }

    public static String Calendar_date_time() {
        String currentTimeStamp = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date currentDate = new Date();
        System.out.println("Time here " + currentDate);
        Calendar calendar = Calendar.getInstance();
       /* calendar.setTime(currentDate);
        calendar.set(Calendar.HOUR);
        calendar.add(Calendar.MINUTE);*/
        currentTimeStamp = sdf.format(calendar.getTime());
        System.out.println("Time here " + currentTimeStamp);
        return currentTimeStamp;
    }

    public abstract class DoubleClickListener implements View.OnClickListener {

        private static final long DOUBLE_CLICK_TIME_DELTA = 300;//milliseconds

        long lastClickTime = 0;

        @Override
        public void onClick(View v) {
            long clickTime = System.currentTimeMillis();
            if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                onDoubleClick(v);
            } else {
                onSingleClick(v);
            }
            lastClickTime = clickTime;
        }

        public abstract void onSingleClick(View v);

        public abstract void onDoubleClick(View v);
    }

/*
    public static void setUserProfileCleverTap(String event, ArrayList<CleverTapUtil> extras){

        Preferences pref = Preferences.getInstance();
        HashMap<String, Object> prodViewAction = new HashMap<String, Object>();
        prodViewAction.put("Name", pref.getUserName());
        prodViewAction.put("Identity", pref.getDealerName()+"_"+pref.getUserName());
        prodViewAction.put("Phone", pref.getUserMobile());
        prodViewAction.put("Email", pref.getEmail());
        prodViewAction.put("Dealer", pref.getDealerName());
        prodViewAction.put("UserId", pref.getUserId());
        prodViewAction.put("Role", pref.getRole());
        if(extras!=null){
            Iterator<CleverTapUtil> itr = extras.iterator();
            while(itr.hasNext()){
                CleverTapUtil obj = itr.next();
                prodViewAction.put(obj.getKey(),obj.getValue());

            }
        }

      //  SalesCRMApplication.getCleverTapAPI().event.push(event, prodViewAction);
    }
*/
    public static boolean isServiceRunning(Class<?> serviceClass,Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    public static String parseDateAsAgo(long time) {
        return (String) DateUtils.getRelativeTimeSpanString(time,getDateTime(0).getTime(),  DateUtils.SECOND_IN_MILLIS);
    }

    /**
     * date to insert into telephony db
     * @return
     */
    public static String getNowTimeString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        return dateFormat.format(cal.getTime());
    }


    /**
     * to get 7 days old
     *
     * @return
     */
    public static String getSevenDaysDateString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);
        return dateFormat.format(cal.getTime());
    }


    public static Boolean isMemoryLow(Context context){
        Boolean isLowMemory = false;
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(memoryInfo);
        if(memoryInfo.lowMemory){
            isLowMemory = true;
        }

        return isLowMemory;


    }

    public static Date tryParse(String dateString) {
        for (String formatString : formatStrings) {
            try {
                return new SimpleDateFormat(formatString).parse(dateString);
            } catch (ParseException e) {
            }
        }

        return null;
    }

    public static void setNotification(Context context, String activityName, String customerName,
                                       long leadId, int scheduledActivityId, long scheduledTime, long actualScheduledTime, int activityId) {
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent();
        intent.setAction(WSConstants.START_NINJA_NOTIFY);
        intent.putExtra(WSConstants.ACTIVITY_NAME,activityName);
        intent.putExtra(WSConstants.LEAD_ID,leadId);
        intent.putExtra(WSConstants.SCHEDULED_ACTIVITY_ID,scheduledActivityId);
        intent.putExtra(WSConstants.SCHEDULED_DATE,actualScheduledTime);
        intent.putExtra(WSConstants.NOTIFICATION_CUSTOMER_NAME,customerName);
        intent.putExtra(WSConstants.NOTIFICATION_ACTIVITY_ID,activityId);
        intent.setClass(context, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,scheduledActivityId,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        System.out.println("Setting alarm");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarm.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, scheduledTime, pendingIntent);
            // alarm.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,calendar.getTimeInMillis(),10*1000,pendingIntent);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarm.setExact(AlarmManager.RTC_WAKEUP, scheduledTime, pendingIntent);
        } else {
            alarm.set(AlarmManager.RTC_WAKEUP, scheduledTime, pendingIntent);
        }
    }

    public static void registerNotification(Context context,
                                            String title,
                                            String content,
                                            int type,
                                            long scheduledTime) {
        if(context == null) {
            return;
        }

        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        if(alarm == null) {
            return;
        }
        Intent intent = new Intent();
        intent.setAction(WSConstants.InAppNotification.START_NINJA_NOTIFY_CUSTOM);
        intent.putExtra(WSConstants.InAppNotification.TITLE, title);
        intent.putExtra(WSConstants.InAppNotification.CONTENT, content);
        intent.putExtra(WSConstants.InAppNotification.TYPE, type);
        intent.setClass(context, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int) scheduledTime,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarm.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, scheduledTime, pendingIntent);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            alarm.setExact(AlarmManager.RTC_WAKEUP, scheduledTime, pendingIntent);
        } else {

            alarm.set(AlarmManager.RTC_WAKEUP, scheduledTime, pendingIntent);
        }
    }

    public static void deleteNotification(Context context,int leadId, int scheduledActivityId,long scheduledTime) {

        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent();
        intent.setAction(WSConstants.START_NINJA_NOTIFY);
        intent.putExtra(WSConstants.LEAD_ID,leadId);
        intent.putExtra(WSConstants.SCHEDULED_ACTIVITY_ID,scheduledActivityId);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, scheduledActivityId,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        pendingIntent.cancel();
        alarm.cancel(pendingIntent);
    }

    public static void showDatePicker(Object o, Activity activity, int id, int TYPE) {
        Calendar now = Calendar.getInstance();
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                (com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener) o,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        switch(TYPE){
            case WSConstants.TYPE_DATE_PICKER.BOOKING_FOLLOW_UP_DATE:
                dpd.setMinDate(now);
                break;

        }
        dpd.show(activity.getFragmentManager(), String.valueOf(id));
    }

    public static void showAppNotification(Context context, int leadId, String contentText, int scheduledActivityId, String title,
                                           long scheduledDate, int activityId,
                                           boolean isNotificationServer,String firebaseEvent, int locationId){

        int icon;
        if(WSConstants.NOTIFICATION_DRAWABLE.indexOfKey(activityId) < 0) {
            //Item does not exist.
            icon =  R.drawable.ic_notifications_white_24dp;
        }
        else {
             icon = WSConstants.NOTIFICATION_DRAWABLE.get(activityId,-1);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE,WSConstants.DEFAULT_NOTIFICATION_BEFORE_TIME);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationsUI.CHANNEL_ID)
                .setContentTitle(isNotificationServer?title:title+", "+new SimpleDateFormat("hh:mm a",  Locale.getDefault()).format(scheduledDate))
                .setContentText(contentText)
                .setAutoCancel(true)
                .setSmallIcon(icon)
                .setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.mipmap.ninja_crm)).getBitmap())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setColor(Color.parseColor("#007FFF"))
                .setPriority(Notification.PRIORITY_HIGH);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |
                PowerManager.ACQUIRE_CAUSES_WAKEUP |
                PowerManager.ON_AFTER_RELEASE, "WakeLock");
        wakeLock.acquire();


        Intent intent;
        String notificationType = "";
        String notificationTriggeredFrom ="";
        if(isNotificationServer){
            intent = new Intent(context,NotificationRedirectActivity.class);
            notificationType = firebaseEvent;
            notificationTriggeredFrom = "FCM";
        }
        else {
            intent = new Intent(context, HomeActivity.class);
            intent.putExtra(WSConstants.NOTIFICATION_CLICKED_NAME,title);
            notificationType = title;
            notificationTriggeredFrom = "InApp";
        }

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_TYPE, CleverTapConstants.EVENT_NOTIFICATION_TYPE_SENT);
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_FROM, notificationTriggeredFrom);
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_EVENT, notificationType);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_NOTIFICATION, hashMap);


        intent.putExtra(WSConstants.NOTIFICATION_TRIGGERED_FROM,isNotificationServer);
        intent.putExtra(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY,true);
        intent.putExtra(WSConstants.LEAD_ID,leadId);
        intent.putExtra(WSConstants.SCHEDULED_ACTIVITY_ID,scheduledActivityId);
        intent.putExtra(WSConstants.NOTIFICATION_ACTIVITY_ID,activityId);
        intent.putExtra(WSConstants.NOTIFICATION_SERVER,isNotificationServer);
        intent.putExtra(WSConstants.NOTIFICATION_LOCATION_ID,locationId);
        if(firebaseEvent!=null){
            intent.putExtra(WSConstants.NOTIFICATION_FIREBASE_EVENT,firebaseEvent);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent activity = PendingIntent.getActivity(context, scheduledActivityId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(activity);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(NotificationsUI.getNotificationChannel());
        }
        notificationManager.notify(scheduledActivityId, builder.build());

    }

    public static void postBookingDbHandler(Realm realm,PostBookingDbHandler listener, final Integer scheduledActivityId,final int actionId, int activityId) {
        if (scheduledActivityId != null) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    SalesCRMRealmTable dbRow = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
                    if (dbRow != null) {
                        if (actionId == 5) {
                            dbRow.setDone(true);
                        } else {
                            dbRow.deleteFromRealm();
                        }
                    }
                }
            });

        }
    }

    /**
     * Function to convert milliseconds time to
     * Timer Format
     * Hours:Minutes:Seconds
     * */
    public static String milliSecondsToTimer(long milliseconds){
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int)( milliseconds / (1000*60*60));
        int minutes = (int)(milliseconds % (1000*60*60)) / (1000*60);
        int seconds = (int) ((milliseconds % (1000*60*60)) % (1000*60) / 1000);
        // Add hours if there
        if(hours > 0){
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if(seconds < 10){
            secondsString = "0" + seconds;
        }else{
            secondsString = "" + seconds;}

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    /**
     * Function to get Progress percentage
     * @param currentDuration
     * @param totalDuration
     * */
    public static int getProgressPercentage(long currentDuration, long totalDuration){
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage =(((double)currentSeconds)/totalSeconds)*100;

        // return percentage
        return percentage.intValue();
    }

    /**
     * Function to change progress to timer
     * @param progress -
     * @param totalDuration
     * returns current duration in milliseconds
     * */
    public static int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double)progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }

    public static int gcd(int a, int b)
    {
        if (a == 0)
            return b;
        return gcd(b%a, a);
    }

    // Function to find gcd of array of
    // numbers
    public static int findGCD(int arr[])
    {
        int result = arr[0];
        for (int i=1; i<arr.length; i++)
            result = gcd(arr[i], result);

        return result;
    }

    public static int getLargestNumber(int arr[]){

        int largest = arr[0];

        for(int i=1; i< arr.length; i++)
        {
            if(arr[i] > largest)
                largest = arr[i];
        }
        return largest;
    }

    public static String createJsonObject(FilterSelectionHolder filterSelectionHolder, Realm realm) {
        //JSONObject jObjectParent = new JSONObject();
        JSONObject jObject = new JSONObject();
        for (int i = 0; i < filterSelectionHolder.getAllMapData().size(); i++) {
            int insideKey = filterSelectionHolder.getAllMapData().keyAt(i);
            String key = Util.getAdvanceFilterKeyForLeadList(insideKey);
            if (!TextUtils.isEmpty(key)) {
                try {
                    if (key.equalsIgnoreCase("activity_id")) {
                        StringBuilder sbActivityId = new StringBuilder();
                        for (int l = 0; l < filterSelectionHolder.getMapData(insideKey).size(); l++) {
                            //jsonObjectAge.put(key, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(l)));
                            if(l<filterSelectionHolder.getMapData(insideKey).size()-1) {
                                sbActivityId.append(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(l))+",");
                            }else{
                                sbActivityId.append(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(l))+"");
                            }
                        }
                        jObject.put("activity_id", sbActivityId.toString());
                    }else{
                        if (key.equalsIgnoreCase("date")) {
                            JSONObject jsonObjectDate = new JSONObject();
                            JSONObject jsonObjectEnquiryDate = new JSONObject();
                            JSONObject jsonObjectBuyingDate = new JSONObject();
                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                            for (int j = 0; j < filterSelectionHolder.getMapData(insideKey).size(); j++) {
                                int dateKey = filterSelectionHolder.getMapData(insideKey).keyAt(j);
                                Date dateVal = Util.getCalender(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j))).getTime();
                                if(dateKey == 0){
                                    jsonObjectBuyingDate.put("start_date", format1.format(dateVal));
                                }
                                if(dateKey == 1){
                                    jsonObjectBuyingDate.put("end_date", format1.format(dateVal));
                                }
                                if(dateKey == 2){
                                    jsonObjectEnquiryDate.put("start_date", format1.format(dateVal));
                                }
                                if(dateKey == 3){
                                    jsonObjectEnquiryDate.put("end_date", format1.format(dateVal));
                                }
                            }
                            if(jsonObjectBuyingDate.has("start_date") && jsonObjectBuyingDate.has("end_date")) {
                                jsonObjectDate.put("buying_date", jsonObjectBuyingDate);
                            }
                            if(jsonObjectEnquiryDate.has("start_date") && jsonObjectEnquiryDate.has("end_date")) {
                                jsonObjectDate.put("enquiry_date", jsonObjectEnquiryDate);
                            }
                            jObject.put(key, jsonObjectDate);
                        }if (key.equalsIgnoreCase("lead_age")) {
                            JSONObject jsonObjectAge = new JSONObject();
                            for (int k = 0; k < filterSelectionHolder.getMapData(insideKey).size(); k++) {
                                int valFrom = Integer.parseInt(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(k)));
                                int valTo = Integer.parseInt(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(k + 1)));
                                jsonObjectAge.put("start_age", valTo+"");
                                jsonObjectAge.put("end_age", valFrom+"");
                            }
                            jObject.put(key, jsonObjectAge);
                        }if(key.equalsIgnoreCase("lead_tag_name")){
                            setHashMapData();
                            StringBuilder sbTagName = new StringBuilder();
                            for (int l = 0; l < filterSelectionHolder.getMapData(insideKey).size(); l++) {
                                //jsonObjectAge.put(key, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(l)));
                                if(l<filterSelectionHolder.getMapData(insideKey).size()-1) {
                                    sbTagName.append(hashMapTagName.get(filterSelectionHolder.getMapData(insideKey).keyAt(l))+",");
                                }else{
                                    sbTagName.append(hashMapTagName.get(filterSelectionHolder.getMapData(insideKey).keyAt(l))+"");
                                }
                            }
                            jObject.put("lead_tag_id", sbTagName.toString());

                        }if(key.equalsIgnoreCase("variant_name")){
                            StringBuilder sbVarientName = new StringBuilder();
                            for (int l = 0; l < filterSelectionHolder.getMapData(insideKey).size(); l++) {
                                //jsonObjectAge.put(key, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(l)));
                                if(l<filterSelectionHolder.getMapData(insideKey).size()-1) {
                                    sbVarientName.append(filterSelectionHolder.getMapData(insideKey).keyAt(l)+",");
                                }else{
                                    sbVarientName.append(filterSelectionHolder.getMapData(insideKey).keyAt(l)+"");
                                }
                            }
                            jObject.put("model_id", sbVarientName.toString());
                        }if(key.equalsIgnoreCase("lead_source_name")){
                            StringBuilder sbLeadSourceName = new StringBuilder();
                            for (int l = 0; l < filterSelectionHolder.getMapData(insideKey).size(); l++) {
                                //jsonObjectAge.put(key, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(l)));
                                if(l<filterSelectionHolder.getMapData(insideKey).size()-1) {
                                    sbLeadSourceName.append(filterSelectionHolder.getMapData(insideKey).keyAt(l)+",");
                                }else{
                                    sbLeadSourceName.append(filterSelectionHolder.getMapData(insideKey).keyAt(l)+"");
                                }
                            }
                            jObject.put("lead_source_id", sbLeadSourceName.toString());
                        }if(key.equalsIgnoreCase("lead_stage")){
                            StringBuilder sbLeadStage = new StringBuilder();
                            for (int l = 0; l < filterSelectionHolder.getMapData(insideKey).size(); l++) {
                                //jsonObjectAge.put(key, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(l)));
                                if(l<filterSelectionHolder.getMapData(insideKey).size()-1) {
                                    sbLeadStage.append(realm.where(FilterEnquiryStage.class).equalTo("stage", filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(l))).findFirst().getStage_id()+",");
                                }else{
                                    sbLeadStage.append(realm.where(FilterEnquiryStage.class).equalTo("stage", filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(l))).findFirst().getStage_id()+"");
                                }
                            }
                            jObject.put("lead_stage_id", sbLeadStage.toString());
                        }if(key.equalsIgnoreCase("buyer_type")){
                            StringBuilder sbBuyerType = new StringBuilder();
                            for (int l = 0; l < filterSelectionHolder.getMapData(insideKey).size(); l++) {
                                //jsonObjectAge.put(key, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(l)));
                                if(l<filterSelectionHolder.getMapData(insideKey).size()-1) {
                                    sbBuyerType.append((filterSelectionHolder.getMapData(insideKey).keyAt(l)+1)+",");
                                }else{
                                    sbBuyerType.append((filterSelectionHolder.getMapData(insideKey).keyAt(l)+1)+"");
                                }
                            }
                            jObject.put("buyer_type_id", sbBuyerType.toString());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("MyJsonObject: "+jObject.toString());
       /* try {
            jObjectParent.put("filters", jObject);
        }catch (Exception e){

        }*/
        return jObject.toString();
    }

    private static void setHashMapData() {
        hashMapTagName.put(0, 6);
        hashMapTagName.put(1, 7);
        hashMapTagName.put(2, 8);
        hashMapTagName.put(3, 5);
    }

    public static boolean isFragmentSafe(Fragment frag) {

        return !(frag.isRemoving()
                || frag.getActivity() == null
                || frag.isDetached()
                || !frag.isAdded()
                || frag.getView() == null );
    }

    public static boolean checkContactAvailable(String ph, Context context) {

        Uri uriPhone = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(ph));
        Cursor cur = context.getContentResolver().query(uriPhone, null, null, null, null);
        try {
            if (cur != null && cur.moveToFirst()) {
                return true;
            }
        } finally {
            if (cur != null)
                cur.close();
        }
        return false;
    }

    public static String getPdfFileNameFromLink(String pdfLink){
        String fileName = pdfLink.substring(pdfLink.lastIndexOf('/') + 1);
        return ""+fileName;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }

    public static String getProperName(String name) {
        if (name == null || name.equalsIgnoreCase("")) {
            return "";
        }
        return String.format("%s%s", name.substring(0, 1).toUpperCase(),
                name.split(" ")[0].substring(1));
    }

    public static int getRandomNumber(int max, int min) {
        return new Random().nextInt((max - min) + 1) + min;
    }
    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }

    public static void openLink(Context context, String link) {
        if(link!= null && !link.equalsIgnoreCase("")) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                context.startActivity(browserIntent);
            } catch (ActivityNotFoundException a) {
                Util.showToast(context, "Error opening link", Toast.LENGTH_SHORT);
            }
        }else{
            Util.showToast(context, "Link not found", Toast.LENGTH_SHORT);
        }

    }
    public static void copy(Context context, String text) {
        try {
            ClipboardManager clipboard = (ClipboardManager)context. getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("label", text);
            clipboard.setPrimaryClip(clip);
            CleverTapPush.pushEvent(CleverTapConstants.EVENT_COPY_REMARKS);
        }
        catch (Exception e) {

        }

    }
}
