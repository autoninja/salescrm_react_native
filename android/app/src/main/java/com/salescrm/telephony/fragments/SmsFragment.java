package com.salescrm.telephony.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.EmailSmsActivity;
import com.salescrm.telephony.db.CustomerPhoneNumbers;
import com.salescrm.telephony.db.DealerDataDb;
import com.salescrm.telephony.db.PlannedActivities;
import com.salescrm.telephony.db.SMSResult;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class SmsFragment extends Fragment{
    private static  RealmResults<SMSResult> smsRealmResults;
    Preferences pref;
    private Realm realm;
    TextView sms1edtxt1;
    private SMSResult smsResult;
    private OnDataPassSMS onDataPassObject;
    String phoneNumber="", content="", typeId="";
    private DealerDataDb dealerDataDb;
    private UserDetails userDetails;
    private SalesCRMRealmTable salesCRMRealmTable;


    public static SmsFragment newInstance(RealmResults<SMSResult> SmsEmailResponse) {
        SmsFragment sms = new SmsFragment();
        return sms ;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity a;
        if(context instanceof EmailSmsActivity) {
            a = (EmailSmsActivity) context;
            try {
                onDataPassObject = (OnDataPassSMS) a;
            } catch (ClassCastException e) {
                throw new ClassCastException(a.toString() + " must implement OnDataPassSMS");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.sms, container, false);
        final FragmentActivity c = getActivity();
        pref = Preferences.getInstance();
        realm=Realm.getDefaultInstance();
        pref.load(getActivity());
        final Spinner sms11spinner1=(Spinner)view.findViewById(R.id.sms1spinner1);
        final Spinner sms11spinner2=(Spinner)view.findViewById(R.id.sms1spinner2);
        sms1edtxt1=(TextView) view.findViewById(R.id.sms1edtxt1);

        userDetails = realm.where(UserDetails.class).findFirst();
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();


        final List<String> listsms1 = new ArrayList<String>();
        final List<String> listsmsMobileNumbers= new ArrayList<String>();
        RealmResults<PlannedActivities> plannedActivities =realm.where(PlannedActivities.class)
                .equalTo("leadID", Integer.parseInt(pref.getLeadID())).equalTo("isDone", false).findAll();


        smsRealmResults = realm.where(SMSResult.class).equalTo("recipient_type_id", "1").findAll();
        final RealmResults<CustomerPhoneNumbers> phno = realm.where(CustomerPhoneNumbers.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findAll();


        if(phno != null && phno.size() > 0){
            for(int i=0;i<phno.size();i++){
                String number = Long.toString(phno.get(i).getPhoneNumber());
                if(pref.isClientILom()) {
                    if(phno.get(i).getPhoneNumberStatus().equalsIgnoreCase("SECONDARY") && Util.isNotNull(number) && number.length()>2) {
                        number = number.charAt(0)+"" + number.charAt(1) + "XXXXXXXX";
                        listsms1.add(number);
                        listsmsMobileNumbers.add(Long.toString(phno.get(i).getPhoneNumber()));
                    }
                }
                else {
                    listsms1.add(number);
                    listsmsMobileNumbers.add(Long.toString(phno.get(i).getPhoneNumber()));
                }
            }
        }else{
            if(pref.isClientILom()) {
                String number = Long.toString(salesCRMRealmTable.getSecondaryMobileNumber());
                number = number.charAt(0)+"" + number.charAt(1) + "XXXXXXXX";
                listsms1.add(number);
                listsmsMobileNumbers.add(Long.toString(salesCRMRealmTable.getSecondaryMobileNumber()));
            }

        }


        final List<String> listsms2 = new ArrayList<String>();
        if(smsRealmResults!=null)
        for(int j=0 ; j< smsRealmResults.size(); j++){

            if(smsRealmResults.get(j).getId().equals(WSConstants.SmsTypeName.VISIT_SCHEDULED_ID)){
                if(plannedActivities.where().equalTo("activityId",WSConstants.TaskActivityName.SHOWROOM_VISIT_ID+"").or()
                        .equalTo("activityId",WSConstants.TaskActivityName.HOME_VISIT_ID+"").findAll().size()>0){
                    listsms2.add(smsRealmResults.get(j).getName());
                }
            }
            else if(smsRealmResults.get(j).getId().equals(WSConstants.SmsTypeName.TEST_DRIVE_SCHEDULED_ID)){
                if(plannedActivities.where().equalTo("activityId",WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID+"").or()
                        .equalTo("activityId",WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID+"").findAll().size()>0){
                    listsms2.add(j,smsRealmResults.get(j).getName());
                }
            }
            else if(smsRealmResults.get(j).getId().equals(WSConstants.SmsTypeName.EVALUATION_SCHEDULED_ID)){
                if(plannedActivities.where().equalTo("activityId",WSConstants.TaskActivityName.EVALUATION_REQUEST_ID+"").findAll().size()>0){
                    listsms2.add(smsRealmResults.get(j).getName());
                }
            }
            else {
                listsms2.add(smsRealmResults.get(j).getName());

            }

        }
        final ArrayAdapter<String> dataAdaptersms1 = new ArrayAdapter<String>(getContext(),
                R.layout.spinner_item, listsms1);
        dataAdaptersms1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sms11spinner1.setAdapter(dataAdaptersms1);
        if(pref.isClientILom()) {
            sms11spinner1.setEnabled(false);
        }
        else {
            sms11spinner1.setEnabled(true);
        }
        sms11spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                phoneNumber = listsmsMobileNumbers.get(sms11spinner1.getSelectedItemPosition());
                sendSmsData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> dataAdaptersms = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, listsms2);
        dataAdaptersms.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sms11spinner2.setAdapter(dataAdaptersms);
        //String phone = sms11spinner1.getItemAtPosition(sms11spinner1.getSelectedItemPosition()).toString();
        sms11spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        String msgTemp = sms11spinner2.getSelectedItem().toString();
                        String  mCustomerNumber = "{{mobNo}}" ,mCustomerName;
                             if(salesCRMRealmTable.getCustomerNumber() != null)
                             {
                                mCustomerNumber = salesCRMRealmTable.getCustomerNumber();
                             }else{
                                 for(int i=0;i<salesCRMRealmTable.customerPhoneNumbers.size();i++){
                                     if(salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumberStatus().equalsIgnoreCase("PRIMARY")){
                                         mCustomerNumber = "" + salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumber();
                                     }
                                 }
                             }

                            if( salesCRMRealmTable.getCustomerName() != null){
                                mCustomerName = salesCRMRealmTable.getCustomerName();
                            }else{
                                mCustomerName = "{{custName}}";
                            }

                        smsResult = realm.where(SMSResult.class).endsWith("name", msgTemp).findFirst();
                        if(smsResult.getContent() != null){
                            dealerDataDb = realm.where(DealerDataDb.class).findFirst();
                            String subString1 = smsResult.getContent().replace("{{dealerName}}", dealerDataDb.getDname())
                                    .replace("{{locationName}}", salesCRMRealmTable.getLeadLocationName())
                                    .replace("{{policyRenewalLink}}", Util.isNotNull(salesCRMRealmTable.getOfficeAddress())
                                            ?salesCRMRealmTable.getOfficeAddress():salesCRMRealmTable.getCustomerAddress())
                                    .replace("{{carBrand}}", dealerDataDb.getDbrand())
                                    .replace("{{leadSourceMobNo}}", dealerDataDb.getCrmMobNo())
                                    .replace("{{ccmMobNo}}", dealerDataDb.getCcmMobNo())
                                    .replace("{{ccmEmailId}}", dealerDataDb.getCcmEmailId())
                                    .replace("{{dealerWebsite}}", dealerDataDb.getDealerWebsite())
                                    .replace("{{custSupportEmailId}}", dealerDataDb.getCustSupportEmailId())
                                    .replace("{{dseName}}", userDetails.getName())
                                    .replace("{{carModel}}", salesCRMRealmTable.getLeadCarModelName()==null?"":salesCRMRealmTable.getLeadCarModelName())
                                    .replace("{{testDriveDateTime}}",getScheduledDate(new String[]{WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID+"",
                                            WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID+""}) )
                                    .replace("{{dseMobNo}}", userDetails.getMobile())
                                    .replace("{{custName}}", mCustomerName )
                                    .replace("{{mobNo}}", mCustomerNumber)
                                    .replace("{{hotlineNo}}", dealerDataDb.getHotlineNo())
                                    .replace("{{pocMobNo}}", dealerDataDb.getCcmMobNo())
                                    .replace("{{pocName}}", userDetails.getName())
                                    .replace("{{evaluationDateTime}}", getScheduledDate(new String[]{WSConstants.TaskActivityName.EVALUATION_REQUEST_ID+""}))
                                    .replace("{{VisitDateTime}}", getScheduledDate(new String[]{WSConstants.TaskActivityName.HOME_VISIT_ID+"",
                                            WSConstants.TaskActivityName.SHOWROOM_VISIT_ID+""}));
                            content = subString1;
                        }
                        typeId = smsResult.getNotification_type_id();
                        sms1edtxt1.setText(content);
                        sendSmsData();
                    }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sms1edtxt1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                    content = sms1edtxt1.getText().toString();
                    sendSmsData();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

    private CharSequence getScheduledDate(String[] ids) {
        RealmResults<PlannedActivities> plannedActivities =realm.where(PlannedActivities.class)
                .equalTo("leadID", Integer.parseInt(pref.getLeadID())).equalTo("isDone", false).findAll();
        for(int i=0; i<plannedActivities.size(); i++){
            for (int j=0;j<ids.length;j++){
                if(plannedActivities.get(i).getActivityId().equals(ids[j])){
                    return plannedActivities.get(i).getPlannedActivitiesDateTime()+"";
                }
            }
        }
        return "";
    }


    private void sendSmsData() {
        onDataPassObject.onDataPassSMS(phoneNumber, content, typeId);
    }

    public interface OnDataPassSMS {
        public void onDataPassSMS(String phone, String smsResult, String typeID);
    }

}
