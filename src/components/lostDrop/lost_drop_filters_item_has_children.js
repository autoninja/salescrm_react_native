import React, { Component } from 'react'
import { View, Text, Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Alert} from 'react-native'

import { CheckBox } from 'react-native-elements'
import LostDropFiltersItemCheckBox from './lost_drop_filters_item_checkbox'

export default class LostDropFiltersItemHasChildren extends Component {
  constructor(props) {
    super(props);
      this.state= {expand:false, is_select_all:false,update:false};
  }
  _selectAllSubFilter(key, firstVal,childValue, is_select_all) {
    console.log('_selectAllFilter'+is_select_all);
    addAllLostDropFilterItemWithSubCategories(key, firstVal, childValue,is_select_all);
    this.setState({is_select_all});
  }
  _update() {
    this.setState({update:!this.state.update});
  }
  render() {
    let filter = this.props.filter;
    let item = this.props.item;
    let sub_categories = [];
    sub_categories.push(<LostDropFiltersItemCheckBox
    key = {'select_all'}
    select_all
    selectAllFilter = {this._selectAllSubFilter.bind(this,filter.key,item.id,item.subcategories)}
    />);
    for(var i=0;i<item.subcategories.length;i++) {
      var sub_category = item.subcategories[i];
      sub_categories.push(<LostDropFiltersItemCheckBox
        is_select_all={this.state.is_select_all}
        key_name={filter.key}
        id={item.id}
        sub_category={sub_category}
        update = {this._update.bind(this)}
        key = {sub_category.id+''}
        title={sub_category.name} />)
    }
    let imageSource
    if(this.state.expand) {
      imageSource = require( '../../images/ic_up_black.png');
    }
    else {
      imageSource = require( '../../images/ic_right_black.png');
    }
    return(
      <View>
        <TouchableOpacity onPress = {()=>this.setState({expand:!this.state.expand})}>
      <View style={{flexDirection:'row',marginTop:10,padding:16, backgroundColor:'#fff', justifyContent:'space-between',borderRadius:6}}>

      <Text style={{color:(isLostDropFilterItemExists(filter.key, item.id)?'#007fff':'#303030'),fontSize:18}}>{item.name+"  "}</Text>

      <Image source={imageSource} style={{width:24, height:24}}/>

      </View>

      </TouchableOpacity>
      <View style = {{display:(this.state.expand?'flex':'none')}}>
      {sub_categories}
      </View>

      <View style={{backgroundColor:'#cccccc', height:1, width:'100%'}}/>

      </View>
    );
  }
}
