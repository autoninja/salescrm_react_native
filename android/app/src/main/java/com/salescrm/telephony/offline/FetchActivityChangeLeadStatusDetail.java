package com.salescrm.telephony.offline;

import android.content.Context;

import com.google.gson.Gson;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.change_lead_stages.ChildReasons;
import com.salescrm.telephony.db.change_lead_stages.DroppedReasons;
import com.salescrm.telephony.db.change_lead_stages.LeadStagePipelineResult;
import com.salescrm.telephony.db.change_lead_stages.LostReasons;
import com.salescrm.telephony.db.change_lead_stages.NestedStage;
import com.salescrm.telephony.db.change_lead_stages.PipelineStages;
import com.salescrm.telephony.db.change_lead_stages.Stages;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.GetPipelineResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by prateek on 11/1/17.
 */

public class FetchActivityChangeLeadStatusDetail implements Callback<GetPipelineResponse> {
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private int callCount;
    private String TAG = "FetchFormData";

    public FetchActivityChangeLeadStatusDetail(OfflineSupportListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        this.callCount = 0;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }

    public void call(){

        //Not required now
        if(true){
         //   listener.onLeadStageDataFetched(null);
            return;
        }

        RealmResults<SalesCRMRealmTable> leads = realm.where(SalesCRMRealmTable.class).equalTo("createdOffline",false).distinct("leadId");
        List<String> leadList = new ArrayList<>();
        for(int l=0; l<leads.size(); l++){
            leadList.add(leads.get(l).getLeadId()+"");
        }
        if(leads.size() == 0){
         //   listener.onLeadStageDataFetched(null);
            return;
        }
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetAllStagingPipeline(leadList,this);
//            listener.onC360InformationFetched(null);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.LEAD_STAGES);
        }
    }

    @Override
    public void success(final GetPipelineResponse getPipelineResponse, Response response) {
        Util.updateHeaders(response.getHeaders());
        if (!getPipelineResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("LeadStage:Success:0:GetAllStagingPipeline:- " + getPipelineResponse.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.LEAD_STAGES);
        } else {
            if (getPipelineResponse.getResult() != null) {
                System.out.println("LeadStage:Success:1:GetAllStagingPipeline:- " + getPipelineResponse.getMessage());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        insertLeadStages(realm, getPipelineResponse);
                    }
                });
             //   listener.onLeadStageDataFetched(getPipelineResponse);
            } else {
                // relLoader.setVisibility(View.GONE);
                System.out.println("LeadStage :Success:2" + getPipelineResponse.getMessage());
                listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.LEAD_STAGES);
                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }

    @Override
    public void failure(RetrofitError error) {
        System.out.println(error.toString());
        System.out.println("LEADSTAGE:Failed");
        System.out.println("LEADSTAGE:Error Kind:" + error.getKind());
        listener.onOfflineSupportApiError(error, WSConstants.OfflineAPIRequest.LEAD_STAGES);
    }

    private void insertLeadStages(Realm realm, GetPipelineResponse getPipelineResponse) {
            //   Log.e("All","All - "+data.getResult().get(i).getCustomer_details().getCustomerDetails().getEmail_ids().size() + " ID "+data.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_id());
            //insertStages(realm, getPipelineResponse.getResult().get(i));
            LeadStagePipelineResult leadStagePipelineResult = new LeadStagePipelineResult();
            for (int p = 0; p < getPipelineResponse.getResult().size(); p++) {
                System.out.println("JSON Data:- "+new Gson().toJson(getPipelineResponse.getResult().get(p)).toString());
                leadStagePipelineResult.setLeadId(Integer.parseInt(getPipelineResponse.getResult().get(p).getLead_id()));
                leadStagePipelineResult.pipelineStages.clear();
                for (int i = 0; i < getPipelineResponse.getResult().get(p).getPipelineStages().size(); i++) {
                    PipelineStages pipelineStagesDb = new PipelineStages();
                    pipelineStagesDb.setPipeline_id(getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getPipeline_id());
                    pipelineStagesDb.setPipeline_name(getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getPipeline_name());
                    if (getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getStages() != null) {
                        pipelineStagesDb.stages.clear();
                        for (int j = 0; j < getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getStages().size(); j++) {
                            Stages stagesDb = new Stages();
                            if (getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getStages().get(j).getCar_model() != null) {
                                stagesDb.setCategory_id(getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getStages().get(j).getCategory_id());
                                stagesDb.setCar_model(getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getStages().get(j).getCar_model());
                                stagesDb.nested_stages.clear();
                                for (int k = 0; k < getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getStages().get(j).getNested_stages().size(); k++) {
                                    NestedStage nestedStageDb = new NestedStage();
                                    nestedStageDb.setStage_id(getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getStages().get(j).getNested_stages().get(k).getStage_id());
                                    nestedStageDb.setStage_name(getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getStages().get(j).getNested_stages().get(k).getStage_name());
                                    stagesDb.nested_stages.add(nestedStageDb);
                                }
                                pipelineStagesDb.stages.add(stagesDb);

                            } else {
                                stagesDb.setCategory_id(getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getStages().get(j).getCategory_id());
                                stagesDb.setStage_id(getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getStages().get(j).getStage_id());
                                stagesDb.setStage_name(getPipelineResponse.getResult().get(p).getPipelineStages().get(i).getStages().get(j).getStage_name());
                                pipelineStagesDb.stages.add(stagesDb);
                            }
                        }
                    }
                    //realm.copyToRealmOrUpdate(pipelineStagesDb);
                    leadStagePipelineResult.pipelineStages.add(pipelineStagesDb);
                }

                leadStagePipelineResult.lostReasons.clear();
                //LostReasons lostReasonsDb = new LostReasons();
                for (int i = 0; i < getPipelineResponse.getResult().get(p).getLostReasons().size(); i++) {
                    LostReasons lostReasonsDb = new LostReasons();
                    lostReasonsDb.setId(getPipelineResponse.getResult().get(p).getLostReasons().get(i).getId());
                    lostReasonsDb.setReason(getPipelineResponse.getResult().get(p).getLostReasons().get(i).getReason());
                    lostReasonsDb.childReasons.clear();
                    for (int j = 0; j < getPipelineResponse.getResult().get(p).getLostReasons().get(i).getChildReasons().size(); j++) {
                        ChildReasons childReasonsDb = new ChildReasons();
                        childReasonsDb.setId(getPipelineResponse.getResult().get(p).getLostReasons().get(i).getChildReasons().get(j).getId());
                        childReasonsDb.setReason(getPipelineResponse.getResult().get(p).getLostReasons().get(i).getChildReasons().get(j).getReason());
                        lostReasonsDb.childReasons.add(childReasonsDb);
                    }
                    //realm.copyToRealmOrUpdate(lostReasonsDb);
                    leadStagePipelineResult.lostReasons.add(lostReasonsDb);
                }


                //DroppedReasons droppedReasonsDb = new DroppedReasons();
                leadStagePipelineResult.droppedReasons.clear();
                for (int i = 0; i < getPipelineResponse.getResult().get(p).getDroppedReasons().size(); i++) {
                    DroppedReasons droppedReasonsDb = new DroppedReasons();
                    droppedReasonsDb.setId(getPipelineResponse.getResult().get(p).getDroppedReasons().get(i).getId());
                    droppedReasonsDb.setReason(getPipelineResponse.getResult().get(p).getDroppedReasons().get(i).getReason());
                    droppedReasonsDb.childReasons.clear();
                    for (int j = 0; j < getPipelineResponse.getResult().get(p).getDroppedReasons().get(i).getChildReasons().size(); j++) {
                        ChildReasons childReasonsDb = new ChildReasons();
                        childReasonsDb.setId(getPipelineResponse.getResult().get(p).getDroppedReasons().get(i).getChildReasons().get(j).getId());
                        childReasonsDb.setReason(getPipelineResponse.getResult().get(p).getDroppedReasons().get(i).getChildReasons().get(j).getReason());
                        droppedReasonsDb.childReasons.add(childReasonsDb);
                    }
                    //realm.copyToRealmOrUpdate(droppedReasonsDb);
                    leadStagePipelineResult.droppedReasons.add(droppedReasonsDb);
                }

                realm.copyToRealmOrUpdate(leadStagePipelineResult);
            }
        }
}

