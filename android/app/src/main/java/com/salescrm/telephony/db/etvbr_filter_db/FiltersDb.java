package com.salescrm.telephony.db.etvbr_filter_db;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by prateek on 8/8/18.
 */

public class FiltersDb extends RealmObject {

    private RealmList<ValuesDb> values = new RealmList<>();
    private String key;
    private String name;
    private boolean has_Children;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<ValuesDb> getValues() {
        return values;
    }

    public void setValues(RealmList<ValuesDb> values) {
        this.values = values;
    }

    public String getKey() {
        return key;
    }

    public Boolean getHas_Children() {
        return has_Children;
    }

    public void setHas_Children(Boolean has_Children) {
        this.has_Children = has_Children;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
