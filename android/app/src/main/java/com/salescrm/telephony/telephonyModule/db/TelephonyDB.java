package com.salescrm.telephony.telephonyModule.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bannhi on 15/3/17.
 */

public class TelephonyDB extends RealmObject {

    @PrimaryKey
    private int id;
    private String uuId;
    private String callId;
    private String leadId;
    private String call_type;
    private String calledNumber;
    private String startTime;
    private String endTime;
    private long duration;
    private String callHangUpCause;
    private String filePath;
    private String fileName;
    private String fileCreatedDate;
    private int fileSyncStatus;
    private int recordSyncStatus;
    private int moduleType;
    private String lastModifiedTime;
    private int deleteFlag;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUuId() {
        return uuId;
    }

    public void setUuId(String uuId) {
        this.uuId = uuId;
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getCall_type() {
        return call_type;
    }

    public void setCall_type(String call_type) {
        this.call_type = call_type;
    }

    public String getCalledNumber() {
        return calledNumber;
    }

    public void setCalledNumber(String calledNumber) {
        this.calledNumber = calledNumber;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getCallHangUpCause() {
        return callHangUpCause;
    }

    public void setCallHangUpCause(String callHangUpCause) {
        this.callHangUpCause = callHangUpCause;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileCreatedDate() {
        return fileCreatedDate;
    }

    public void setFileCreatedDate(String fileCreatedDate) {
        this.fileCreatedDate = fileCreatedDate;
    }

    public int getFileSyncStatus() {
        return fileSyncStatus;
    }

    public void setFileSyncStatus(int fileSyncStatus) {
        this.fileSyncStatus = fileSyncStatus;
    }

    public int getRecordSyncStatus() {
        return recordSyncStatus;
    }

    public void setRecordSyncStatus(int recordSyncStatus) {
        this.recordSyncStatus = recordSyncStatus;
    }

    public int getModuleType() {
        return moduleType;
    }

    public void setModuleType(int moduleType) {
        this.moduleType = moduleType;
    }

    public String getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(String lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
