package com.salescrm.telephony.telephonyModule.recorder;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by bharath on 3/1/18.
 */

public class RecorderWrapper {
    private static final int RECORDER_BPP = 16;
    private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
    private static final String AUDIO_RECORDER_FOLDER = ".Ninja_Crm";
    private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
    private static final String AUDIO_RECORDER_TEMP_FILE_1 = "record_temp_1.wav";
    private static final int RECORDER_SAMPLERATE = 48000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_STEREO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    public static AudioRecord recorder = null;
    private static int bufferSize = 0;
    private static String outputFile;
    private static RecorderWrapper instance = null;
    private static RecordingWrapperExceptionHandler exceptionHandler = null;
    private Thread recordingThread = null;
    private boolean isRecording = false;
    private boolean isExceptionCame = false;
    private Context context = null;


    private RecorderWrapper() throws Exception{
        if (bufferSize == 0) {
            bufferSize = AudioRecord.getMinBufferSize(8000,
                    AudioFormat.CHANNEL_CONFIGURATION_MONO,
                    AudioFormat.ENCODING_PCM_16BIT);
        }
        if (recorder == null) {
            recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                    RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, bufferSize);
        }

    }

    public static RecorderWrapper getInstance(RecordingWrapperExceptionHandler e) throws Exception{
        exceptionHandler = e;
        if (instance==null || recorder == null) {
            instance = new RecorderWrapper();
        }
        return instance;
    }

    public static RecorderWrapper getInstance() throws Exception{
        if (instance==null || recorder == null) {
            instance = new RecorderWrapper();
        }
        return instance;
    }

    private String getFilename() {
       /* String filepath = Environment.getExternalStorageDirectory().getPath()+"/Downloads";
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_WAV);
 */
        System.out.println("Output file:" + outputFile);
        return outputFile;
    }


    private String getTempFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER + "/Temp");
        if (!file.exists()) {
            file.mkdirs();
        }

        File tempFile = new File(filepath + "/Temp", AUDIO_RECORDER_TEMP_FILE);

        if (tempFile.exists())
            tempFile.delete();

        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
    }

    private String getTempFilenameM4A() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER + "/Temp");
        if (!file.exists()) {
            file.mkdirs();
        }

        File tempFile = new File(filepath + "/Temp", AUDIO_RECORDER_TEMP_FILE_1);

        if (tempFile.exists())
            tempFile.delete();

        return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE_1);
    }

    private void recordingChecker(final Context ctx) {
        final Handler h = new Handler();
        h.postDelayed(new Runnable() {

            @Override
            public void run() {
                // do stuff then
                // can call h again after work!
                AudioManager manager = (AudioManager) ctx.getSystemService(Context.AUDIO_SERVICE);
                if (!isExceptionCame) {
                    if (manager != null && manager.getMode() == AudioManager.MODE_IN_CALL) {
                        System.out.println("Call Checker True");
                        h.postDelayed(this, 60 * 1000);
                    } else {
                        stopRecording();
                        System.out.println("Call Checker False");
                    }
                } else {
                    System.out.println("Call Checker Exception Came::");
                }


            }
        }, 60 * 1000); // 1 second delay (takes millis)
    }

    public void startRecording(String data, final Context ctx) throws Exception {
        deleteTempFile();
        recordingChecker(ctx);
        context = ctx;
        outputFile = data;
        System.out.println("Output file:" + outputFile);
        int i = recorder.getState();
        if (i == 1)
            recorder.startRecording();

        isRecording = true;

        recordingThread = new Thread(new Runnable() {

            @Override
            public void run() {

                try {
                    writeAudioDataToFile();
                } catch (Exception e) {
                    //recordingThread = null;
                    System.out.println("");
                    e.printStackTrace();
                    System.out.println("Error Occurred In the Library");
                    outputFile = null;
                    isExceptionCame = true;
                    new Handler(ctx.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            stopRecording();
                            AudioRecord resetRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                                    RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, bufferSize);
                            if (resetRecord.getState() == 1)
                                resetRecord.stop();
                            resetRecord.release();
                            exceptionHandler.onRecorderWrapperException();
                        }
                    });
                    recordingThread = null;


                }
            }
        }, "AudioRecorder Thread");

        recordingThread.start();

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                if (outputFile != null) {
                    stopRecording();
                }
            }
        }, 60 * 60 * 1000);
    }

    private void writeAudioDataToFile() throws Exception {
        byte data[] = new byte[bufferSize];
        String filename = getTempFilename();
        FileOutputStream os = null;

        os = new FileOutputStream(filename);

        int read = 0;
        while (isRecording) {
                read = recorder.read(data, 0, bufferSize);
                if (AudioRecord.ERROR_INVALID_OPERATION != read) {
                    os.write(data);
                }
        }
        os.close();

    }

    public void stopRecording() {
        System.out.println("Recording stop in RecorderWrapper");
        if (null != recorder) {
            isRecording = false;

            int i = recorder.getState();
            if (i == 1)
                recorder.stop();
            recorder.release();

            recorder = null;
            recordingThread = null;
        } else {
            outputFile = null;
        }
        if (outputFile != null) {
            copyWaveFile(getTempFilename(), getTempFilenameM4A());
            new MediaEncoder().encode(getTempFilenameM4A(), getFilename());
            outputFile = null;
        }

        /*try {
            convertAudio(getFilename(),getTempFilenameM4A());
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        deleteTempFile();
    }

    private void deleteTempFile() {
        File file1 = new File(getTempFilename());
        File file2 = new File(getTempFilenameM4A());

        file1.delete();
        file2.delete();
        System.out.println("Temporary File Deleted before calling");
    }

    /*  private void convertAudio(String filename, String outputpath) throws IOException {
  // Set up MediaExtractor to read from the source.

          MediaExtractor extractor = new MediaExtractor();
          System.out.println("Path is:"+filename);
          extractor.setDataSource(filename);


          int trackCount = extractor.getTrackCount();

  // Set up MediaMuxer for the destination.
          MediaMuxer muxer;
          muxer = new MediaMuxer(outputpath, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
  // Set up the tracks.
          HashMap<Integer, Integer> indexMap = new HashMap<Integer, Integer>(trackCount);
          for (int i = 0; i < trackCount; i++) {
              extractor.selectTrack(i);
              MediaFormat format = extractor.getTrackFormat(i);
              format.setString(MediaFormat.KEY_MIME, MediaFormat.MIMETYPE_AUDIO_AMR_NB);

              int dstIndex = muxer.addTrack(format);
              indexMap.put(i, dstIndex);
          }
  // Copy the samples from MediaExtractor to MediaMuxer.
          boolean sawEOS = false;
          int bufferSize = 32000;
          int frameCount = 0;
          int offset = 100;
          ByteBuffer dstBuf = ByteBuffer.allocate(bufferSize);
          MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
  *//* if (degrees >= 0) {
    muxer.setOrientationHint(degrees);
}*//*
// Test setLocation out of bound cases

        muxer.start();
        while (!sawEOS) {
            bufferInfo.offset = offset;
            bufferInfo.size = extractor.readSampleData(dstBuf, offset);
            if (bufferInfo.size < 0) {

                sawEOS = true;
                bufferInfo.size = 0;
            } else {
                bufferInfo.presentationTimeUs = extractor.getSampleTime();
                bufferInfo.flags = extractor.getSampleFlags();
                int trackIndex = extractor.getSampleTrackIndex();
                muxer.writeSampleData(indexMap.get(trackIndex), dstBuf,
                        bufferInfo);
                extractor.advance();
                frameCount++;

            }
        }
        muxer.stop();
        muxer.release();

    }
*/
    private void copyWaveFile(String inFilename, String outFilename) {
        FileInputStream in = null;
        FileOutputStream out = null;
        long totalAudioLen = 0;
        long totalDataLen = totalAudioLen + 36;
        long longSampleRate = RECORDER_SAMPLERATE;
        int channels = 2;
        long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels / 8;

        byte[] data = new byte[bufferSize];

        try {
            in = new FileInputStream(inFilename);
            out = new FileOutputStream(outFilename);
            totalAudioLen = in.getChannel().size();
            totalDataLen = totalAudioLen + 36;

            //  AppLog.logString("File size: " + totalDataLen);

            WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
                    longSampleRate, channels, byteRate);

            while (in.read(data) != -1) {
                out.write(data);
            }

            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void WriteWaveFileHeader(
            FileOutputStream out, long totalAudioLen,
            long totalDataLen, long longSampleRate, int channels,
            long byteRate) throws IOException {

        byte[] header = new byte[44];

        header[0] = 'R'; // RIFF/WAVE header
        header[1] = 'I';
        header[2] = 'F';
        header[3] = 'F';
        header[4] = (byte) (totalDataLen & 0xff);
        header[5] = (byte) ((totalDataLen >> 8) & 0xff);
        header[6] = (byte) ((totalDataLen >> 16) & 0xff);
        header[7] = (byte) ((totalDataLen >> 24) & 0xff);
        header[8] = 'W';
        header[9] = 'A';
        header[10] = 'V';
        header[11] = 'E';
        header[12] = 'f'; // 'fmt ' chunk
        header[13] = 'm';
        header[14] = 't';
        header[15] = ' ';
        header[16] = 16; // 4 bytes: size of 'fmt ' chunk
        header[17] = 0;
        header[18] = 0;
        header[19] = 0;
        header[20] = 1; // format = 1
        header[21] = 0;
        header[22] = (byte) channels;
        header[23] = 0;
        header[24] = (byte) (longSampleRate & 0xff);
        header[25] = (byte) ((longSampleRate >> 8) & 0xff);
        header[26] = (byte) ((longSampleRate >> 16) & 0xff);
        header[27] = (byte) ((longSampleRate >> 24) & 0xff);
        header[28] = (byte) (byteRate & 0xff);
        header[29] = (byte) ((byteRate >> 8) & 0xff);
        header[30] = (byte) ((byteRate >> 16) & 0xff);
        header[31] = (byte) ((byteRate >> 24) & 0xff);
        header[32] = (byte) (2 * 16 / 8); // block align
        header[33] = 0;
        header[34] = RECORDER_BPP; // bits per sample
        header[35] = 0;
        header[36] = 'd';
        header[37] = 'a';
        header[38] = 't';
        header[39] = 'a';
        header[40] = (byte) (totalAudioLen & 0xff);
        header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
        header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
        header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

        out.write(header, 0, 44);
    }

    public interface RecordingWrapperExceptionHandler {
        void onRecorderWrapperException();
    }
}
