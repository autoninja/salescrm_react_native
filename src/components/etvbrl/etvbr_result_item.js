import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'


export default class ETVBRResultItem extends Component {
  render() {
    if(this.props.showPercentage) {
      return(


              <View style={{flexDirection: 'row', height:60, backgroundColor:'#fff', marginTop:10, alignItems:'center'}}>

              <View style={{flex:1.5,height:'100%'}} >
              <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#2D4F8B', fontWeight:'bold'}} >Total%</Text>
              </View>


              </View>


              <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#1B075A', fontWeight:'bold'}} >{this.props.rel.t}</Text>
                </View>


              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#1B075A', fontWeight:'bold'}} >{this.props.rel.v}</Text>
                </View>

              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#1B075A', fontWeight:'bold'}} >{this.props.rel.b}</Text>
                </View>

              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#1B075A', fontWeight:'bold'}} >{this.props.rel.r}</Text>
                </View>


              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
                borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

                  <Text style={{ color:'red', fontWeight:'bold'}} >{this.props.rel.l}</Text>
              </View>


              </View>
      );
    }
    else {
    return(


      <View style={{flexDirection: 'row', height:60, backgroundColor:'#fff', marginTop:10, alignItems:'center',}}>

      <View style={{flex:1.5,height:'100%'}} >
      <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
        <Text style={{ color:'#2D4F8B', fontWeight:'bold', }} >Total</Text>
      </View>

      <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center',}}>
        <Text style={{ color:'#303030'}} >Target</Text>
      </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Enquiries',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#1B075A',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.e}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030'}} >{this.props.targets.e}</Text>
        </View>
      </View>




      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Test Drives',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#1B075A',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.t}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030'}} >{this.props.targets.t}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Visits',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#1B075A',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.v}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030'}} >{this.props.targets.v}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Bookings',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#1B075A',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.b}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030'}} >{this.props.targets.b}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Retails',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#1B075A',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.r}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030'}} >{this.props.targets.r}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Lost Drop',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'red',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.l}</Text>
          </TouchableOpacity>
      </View>


      </View>

    );
  }
  }
}
