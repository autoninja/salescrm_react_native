package com.salescrm.telephony.model;

/**
 * Created by prateek on 9/1/17.
 */

public class ForwardStageData {
    private String lead_id;
    private String lead_last_updated;
    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public class Response {
        private String selected_pipeline;
        private String selected_stage;
        private String remarks;
        private String selected_car;
        private StageClosed closed;


        public String getSelected_pipeline() {
            return selected_pipeline;
        }

        public void setSelected_pipeline(String selected_pipeline) {
            this.selected_pipeline = selected_pipeline;
        }

        public String getSelected_stage() {
            return selected_stage;
        }

        public void setSelected_stage(String selected_stage) {
            this.selected_stage = selected_stage;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getSelected_car() {
            return selected_car;
        }

        public void setSelected_car(String selected_car) {
            this.selected_car = selected_car;
        }

        public StageClosed getClosed() {
            return closed;
        }

        public void setClosed(StageClosed closed) {
            this.closed = closed;
        }

        public class StageClosed {
            private String selected_closing_type;
            private ClosingType lost;
            private ClosingType dropped;

            public ClosingType getLost() {
                return lost;
            }

            public void setLost(ClosingType lost) {
                this.lost = lost;
            }

            public ClosingType getDropped() {
                return dropped;
            }

            public void setDropped(ClosingType dropped) {
                this.dropped = dropped;
            }

            public String getSelected_closing_type() {
                return selected_closing_type;
            }

            public void setSelected_closing_type(String selected_closing_type) {
                this.selected_closing_type = selected_closing_type;
            }


            public class ClosingType {
                private String selected_lead_closing_reason;
                private String selected_lead_closing_reason_main;

                public String getSelected_lead_closing_reason() {
                    return selected_lead_closing_reason;
                }

                public void setSelected_lead_closing_reason(String selected_lead_closing_reason) {
                    this.selected_lead_closing_reason = selected_lead_closing_reason;
                }

                public String getSelected_lead_closing_reason_main() {
                    return selected_lead_closing_reason_main;
                }

                public void setSelected_lead_closing_reason_main(String selected_lead_closing_reason_main) {
                    this.selected_lead_closing_reason_main = selected_lead_closing_reason_main;
                }
            }

        }
    }

}
