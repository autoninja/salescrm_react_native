import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  main: {
    flex: 1
  },
  field: {
    padding: 10
  },
  footer: {
    height: 60,
    backgroundColor: "#808080",
    alignItems: "center",
    justifyContent: "center"
  },
  footerInvalid: {
    backgroundColor: "#808080"
  },
  footerValid: {
    backgroundColor: "#007fff"
  },
  footerText: {
    fontSize: 16,
    color: "#fff",
    fontWeight: "bold"
  },
  floatRight: {
    marginTop: 10,
    fontSize: 16,
    color: "#007fff",
    paddingRight: 10,
    alignSelf: "flex-end"
  },
  line: {
    height: 1,
    backgroundColor: "#D5D5D5"
  },
  buttonSelected: {
    margin: 5,
    backgroundColor: "#4470FF",
    borderRadius: 5,
    height: 80,
    elevation: 5
  },
  buttonUnSelected: {
    margin: 5,
    backgroundColor: "#303030",
    borderRadius: 5,
    height: 80,
    elevation: 5
  },
  shadowsView: {
    shadowColor: "#808080",
    shadowOpacity: 0.4,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0
    }
  },
  shadowsViewInner: {
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
    elevation: 4,
    margin: 5
  },
  loading: {
    backgroundColor: "#fff",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
    opacity: 0.5,
    position: "absolute",
    alignSelf: "center",
    justifyContent: "center"
  },
  loadingOnUpdate: {
    backgroundColor: "#fff",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    alignSelf: "center",
    justifyContent: "center"
  },
  header: {
    flexDirection:'row',
    paddingTop:16,
    paddingBottom:16,
    paddingStart:16,
    paddingEnd:16,
    backgroundColor:'#fff',
    alignItems:'center',
    borderBottomWidth:1,
    borderBottomColor:"#CFCFCF"
  },
  headerStartText: {
    fontSize:16,
    color:'#2E5E86',
    paddingStart:16
  },
  headerStart: {
    alignSelf:'flex-start',
  },
  headerEnd: {
    marginStart:10,
    alignSelf:'flex-end',
    backgroundColor:'#007fff',
    borderRadius:4,
    padding:4
  },
  headerEndText: {
    color:'#fff',
    fontSize:12
  },
  close:{
    height:24,
    width:24,
  },
  sectionView: {
    backgroundColor:'#fff',
    paddingStart:8,
    paddingEnd:8,
    paddingTop:4,
    paddingBottom:8
  },
  radioHeader: {
    color: "#303030",
    fontSize: 16,
    paddingTop:10,
    paddingLeft: 10,
    paddingRight: 10,
    fontWeight: "bold"
  }
});
