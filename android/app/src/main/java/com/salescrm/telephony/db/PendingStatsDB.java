package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bannhi on 25/9/17.
 */

public class PendingStatsDB extends RealmObject{

    private int locId;
    private String locName;
    private String userName;
    private int userId;
    private String userImage;
    private String managerImage;
    private String tlImage;
    private int visibleTimes;
    private int pendingCount;
    private int pendingCountDiff;
    private int medianDelay;
    private String userPhone;
    private String tlName;
    private String tlNum;
    private String managerName;
    private String managerNum;
    private int team_member;


    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getManagerImage() {
        return managerImage;
    }

    public void setManagerImage(String managerImage) {
        this.managerImage = managerImage;
    }

    public String getTlImage() {
        return tlImage;
    }

    public void setTlImage(String tlImage) {
        this.tlImage = tlImage;
    }

    public int getLocId() {
        return locId;
    }

    public void setLocId(int locId) {
        this.locId = locId;
    }

    public String getLocName() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName = locName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getVisibleTimes() {
        return visibleTimes;
    }

    public void setVisibleTimes(int visibleTimes) {
        this.visibleTimes = visibleTimes;
    }

    public int getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(int pendingCount) {
        this.pendingCount = pendingCount;
    }

    public int getPendingCountDiff() {
        return pendingCountDiff;
    }

    public void setPendingCountDiff(int pendingCountDiff) {
        this.pendingCountDiff = pendingCountDiff;
    }

    public int getMedianDelay() {
        return medianDelay;
    }

    public void setMedianDelay(int medianDelay) {
        this.medianDelay = medianDelay;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getTlName() {
        return tlName;
    }

    public void setTlName(String tlName) {
        this.tlName = tlName;
    }

    public String getTlNum() {
        return tlNum;
    }

    public void setTlNum(String tlNum) {
        this.tlNum = tlNum;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerNum() {
        return managerNum;
    }

    public void setManagerNum(String managerNum) {
        this.managerNum = managerNum;
    }

    public int getTeam_member() {
        return team_member;
    }

    public void setTeam_member(int team_member) {
        this.team_member = team_member;
    }
}
