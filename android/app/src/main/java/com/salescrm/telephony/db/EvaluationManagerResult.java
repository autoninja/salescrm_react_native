package com.salescrm.telephony.db;


import io.realm.RealmList;
import io.realm.RealmObject;

public class EvaluationManagerResult extends RealmObject {

    public RealmList<EvaluationManagerResultData> evaluationManagerResultData = new RealmList<>();

    public RealmList<EvaluationManagerResultData> getEvaluationManagerResultData() {
        return evaluationManagerResultData;
    }

    public void setEvaluationManagerResultData(RealmList<EvaluationManagerResultData> evaluationManagerResultData) {
        this.evaluationManagerResultData = evaluationManagerResultData;
    }
}
