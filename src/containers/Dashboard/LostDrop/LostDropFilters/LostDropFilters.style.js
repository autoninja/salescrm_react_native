import { StyleSheet, Dimensions, Platform } from 'react-native';
export default StyleSheet.create({
    container: {
      flex: 1,
      flexWrap:'wrap',
      backgroundColor:'#ffffff',
      width:(Dimensions.get('window').width)*.85,
      backgroundColor:'#fff',
      flex:1,
      paddingTop: (Platform.OS) === 'ios' ? 20 : 10,
     },
});
