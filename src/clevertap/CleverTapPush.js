
const CleverTap = require('clevertap-react-native');




let CleverTapPush = {
  pushProfile: function(userInfo, dbRoles, dbLocations) {
   
    let roles = [];
    if(dbRoles) {
      dbRoles.map((user_role, index) => {
          roles.push(user_role.name);
      });
    }
    let locations = [];
    if(dbLocations) {
      dbLocations.map((location, index) => {
          locations.push(location.name);
      });
    }
    if(userInfo) {
     console.log('Clvertap profile pushed');
     CleverTap.profileSet(
       { 'Name': userInfo.name,
         'UserName' : userInfo.user_name,
         'Identity': userInfo.user_name+'_'+userInfo.dealer,
         'Phone' : userInfo.mobile,
         'MobileNumber': userInfo.mobile,
         'Email': userInfo.mobile.email,
         'Roles': roles,
         'Locations': locations,
         'Dealer': userInfo.dealer,
         'Photo': userInfo.dp_url,
       });
     }
     else {
       console.log('Clevertap profile not pushed');
     }
  },
  
  pushEvent: function(eventName, props) {
    if(eventName) {
      if(props) {
          CleverTap.recordEvent(eventName, props);
      }
      else {
          CleverTap.recordEvent(eventName);
      }
    }

    console.log("CLEVERTAP::"+eventName+":"+JSON.stringify(props));

  },
};

module.exports = CleverTapPush;
