import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image} from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation'
import { StackActions, NavigationActions } from 'react-navigation';
import { NavigationEvents } from "react-navigation";
import RestClient from '../network/rest_client'

import LeaderBoardConsultantsList from './leader_board_consultants_list'
import Explorers from './explorer'
import Achievers from './achievers'
import Dynamos from './dynamos'
import Gurus from './gurus'
import GamificationConfigurationService from '../storage/gamification_configuration_service'
import Utils from '../utils/Utils'

import {eventLeaderBoard} from '../clevertap/CleverTapConstants';
import CleverTapPush from '../clevertap/CleverTapPush'


export default class LeaderBoardConsultants extends React.Component {

	constructor(props)
		{
		    super(props);
				this.state = {
					isLoading : true,
					categories: [],
					isPreviousEnabled : true,
					isNextEnabled : true,
					selectedGameDate:"",

				 }


		}
		componentWillMount() {
				this.updateDatePicker();
				this.getConsultantDetatails();

		}

		componentDidMount() {
		this._mounted = true

		}

	 componentWillUnmount () {
		 this._mounted = false
	 }

	 logClevertap(eventName) {
		 CleverTapPush.pushEvent(eventLeaderBoard.event, { 'LeaderBoard Type' : eventName });

	 }

		openConsultantDetails(item) {
			console.log("Clicked"+JSON.stringify(item));
			let {navigation} = this.props;
			navigation.navigate('ConsultantDetails',{item})
			// setTimeout(
			// 	function(){
			// 	navigation.navigate('ConsultantDetails')
			// }, 3000);
		}

		logout() {
			var logout = StackActions.reset({
				 index: 0, // <-- currect active route from actions array
				 key:null,
				 actions: [
					 NavigationActions.navigate({ routeName: 'Login' }),
				 ],
			 });
			 this.props.navigation.dispatch(logout);
		}

		getConsultantDetatails() {
			this.setState({isLoading:true});
			new RestClient().getLeaderBoardConsultantData({date:getSelectedGameDate(),locationId:1}).then( (data) => {
	      if(data.statusCode===4001 || data.statusCode == 5001) {
	                  Alert.alert(
	                'Failed',
	                'Please try again',
	                [
	                  {text: 'Logout', onPress: () => this.logout()},
	                  {text: 'Retry', onPress: () =>  this.getConsultantDetatails()},
	                ],
	                { cancelable: false }
	                )
	                  return;
	                }
									if(data.result && data.result.categories) {
										this.setState({isLoading:false,categories:data.result.categories});
									}
									console.log(data)



	          }).catch((error) => {
	           console.error(error)
	            if (error.response && error.response.status == 401) {
	                Alert.alert(
	              'Failed',
	              'Please try again',
	              [
	                {text: 'Logout', onPress: () => this.logout()},
	                {text: 'Retry', onPress: () =>  this.getConsultantDetatails()},
	              ],
	              { cancelable: false }
	            )
	              }
	          });
		}
		_tabNavigator() {
			var tabs = {};
			for(var i=0;i<this.state.categories.length;i++) {
				var category = this.state.categories[i];
				tabs[category.name] = {screen:props=><LeaderBoardConsultantsList category = {category} onClick = {this.openConsultantDetails.bind(this)}/>}
			}
			return createMaterialTopTabNavigator(
							tabs,
			        {
			          swipeEnabled: true,
			          animationEnabled: true,
			          tabBarOptions: {
			            style: {
			              height: 50,
			              backgroundColor: '#2e5e86',
			            },
			            labelStyle:{
			              fontSize:14
			            },
			           inactiveTintColor:'#fff',
			           activeTintColor:'#F5A623',
			           upperCaseLabel:false,
			           indicatorStyle:{opacity: 0},

			           }
			    });
    }


	 updateDatePicker() {
		 let isNextEnabled = true;
		 let isPreviousEnabled = true;
		 if(global.selectedGameDate.getMonth() == global.systemDate.getMonth()
			&& global.selectedGameDate.getFullYear() == global.systemDate.getFullYear() ) {
			 isNextEnabled = false;
		 }
		 let gameStartDate = new Date();
		 if(Utils.getDate(GamificationConfigurationService.getSelectedGameLocation(selectedGameLocationId))) {
			 gameStartDate = Utils.getDate(GamificationConfigurationService.getSelectedGameLocation(selectedGameLocationId).start_date);
		 }
		 if(global.selectedGameDate.getMonth() == gameStartDate.getMonth()
			&& global.selectedGameDate.getFullYear() == gameStartDate.getFullYear() ) {
			 isPreviousEnabled = false;
		 }
		 let readableDate = Utils.getReadableDateForGame(new Date());
		 if(Utils.getReadableDateForGame(global.selectedGameDate)) {
			 readableDate = Utils.getReadableDateForGame(global.selectedGameDate);
		 }
		 this.setState({isNextEnabled:isNextEnabled, isPreviousEnabled : isPreviousEnabled, selectedGameDate :readableDate});

	 }

	 moveToPreviousMonth(){
		 if(this.state.isPreviousEnabled) {
				 var prevDate = global.selectedGameDate;
				 prevDate.setMonth(global.selectedGameDate.getMonth()-1)
				 global.selectedGameDate = prevDate;
				 this.updateDatePicker();
				 this.getConsultantDetatails();
				 this.logClevertap(eventLeaderBoard.keyType.valuesType.PREVIOUS_MONTH);
		 }

	 }

	 moveToNextMonth(){
		 if(this.state.isNextEnabled) {
			 var nextDate = global.selectedGameDate;
			 nextDate.setMonth(global.selectedGameDate.getMonth()+1)
			 global.selectedGameDate = nextDate;
			 this.updateDatePicker();
			 this.getConsultantDetatails();
		 }

	 }

	 render () {
		 let placeNoData = null;
		 if(this.state.categories && this.state.categories.length==0) {
			 placeNoData = (<View style={{flex:1, height:'100%', alignItems:'center', justifyContent:'center'}}><Text style={{color:'#fff', alignSelf:'center', fontSize:16}}>No Data</Text></View>) ;
		 }
	 if(this.state.isLoading){
	 return(
		 <View style = {{display:(this.state.isLoading?'flex':'none'), flex:1,justifyContent: 'center',
			 alignItems: 'center',backgroundColor:'#2e5e86'}}>

		 <ActivityIndicator
					 animating={this.state.isLoading}
					 style={{flex: 1,   alignSelf:'center',}}
					 color="#fff"
					 size="large"
					 hidesWhenStopped={true}

		 />

		 </View>

	 )
	}

		if(this.state.categories && this.state.categories.length==0) {
			return(
				<View style = {{flex: 1, backgroundColor: '#2e5e86'}}>
			<NavigationEvents
				onWillFocus={payload => {
					this.updateDatePicker();
					this.getConsultantDetatails();

				}}
			/>

			<View style={{width: '100%',flexDirection: 'row', padding: 5, alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
					<TouchableOpacity onPress={()=>this.moveToPreviousMonth()}>
						<Image
											style={{ width: 12, height: 15,}}
																source={require('../images/ic_left_arrow.png')}
											/>
					</TouchableOpacity>

							<Text style= {{color: '#fff', fontSize: 18, margin: 5}}> {this.state.selectedGameDate} </Text>

					<TouchableOpacity onPress={()=>this.moveToNextMonth()}>
							<Image
										style={{ width: 12, height: 15,}}
															source={require('../images/ic_right_arrow.png')}
										/>
					</TouchableOpacity>
				</View>

				{placeNoData}
			</View>

		)
		}
	 	const Tabs = this._tabNavigator();
		return (
				<View style = {{flex: 1, backgroundColor: '#2e5e86'}}>
				<NavigationEvents
					onWillFocus={payload => {
						this.updateDatePicker();
						this.getConsultantDetatails();

					}}
				/>

				<View style={{width: '100%',flexDirection: 'row', padding: 5, alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
						<TouchableOpacity onPress={()=>this.moveToPreviousMonth()}>
							<Image
								        style={{ width: 12, height: 15,}}
			                          	source={require('../images/ic_left_arrow.png')}
								        />
						</TouchableOpacity>

								<Text style= {{color: '#fff', fontSize: 18, margin: 5}}> {this.state.selectedGameDate} </Text>

						<TouchableOpacity onPress={()=>this.moveToNextMonth()}>
								<Image
							        style={{ width: 12, height: 15,}}
		                          	source={require('../images/ic_right_arrow.png')}
							        />
						</TouchableOpacity>
					</View>

	 			<Tabs/>

		    </View>
			);
	}
}
