package com.salescrm.telephony.adapter;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by Ravindra P on 01-12-2015.
 */
public class MyCustomPager extends ViewPager {
    private boolean swipeEnabled;

    public MyCustomPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.swipeEnabled = true;
        this.setOffscreenPageLimit(10);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.swipeEnabled) {
            return super.onTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.swipeEnabled) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    public void setPagingEnabled(boolean swipeEnabled) {
        this.swipeEnabled = swipeEnabled;
    }
}