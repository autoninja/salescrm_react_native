import React, { Component } from 'react'
import { View, Text, } from 'react-native'

export default class LostDropReasonHeader extends Component {
  render() {
    let lostOrDrop= "Leads Lost  ";
    if(this.props.isDrop) {
      lostOrDrop = "Leads Drop ";
    }
    return (
      <View>
      <View style= {{
        alignItems:'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding:10,
        marginTop:20,
        alignItems:'center'}}>

        <View style={{justifyContent: "center",alignItems: "flex-start", flex:4}}>
          <Text style= {{color:'#6C759B', fontSize:14, fontWeight:'500'}}>Reason  </Text>
        </View>
        <View style={{justifyContent: "center",alignItems: "center", flex:2}}>
          <Text style= {{color:'#6C759B', fontSize:14, fontWeight:'500'}}>{lostOrDrop}</Text>
        </View>
        <View style={{justifyContent: "center",alignItems: "center", flex:2}}>
          <Text style= {{color:'#F20000', fontSize:14, fontWeight:'500'}}>Loss  </Text>
        </View>


      </View>
      <View style= {{backgroundColor:'#8C8C8C', height:1, marginTop:10, width:'100%'}}/>
      </View>
    );
  }
}
