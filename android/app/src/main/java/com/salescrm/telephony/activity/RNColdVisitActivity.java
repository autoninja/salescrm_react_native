package com.salescrm.telephony.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.interfaces.ReactNativeToAndroidConnect;
import com.salescrm.telephony.locationHelper.LocationHelper;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;

import io.realm.Realm;
import io.realm.RealmResults;


public class RNColdVisitActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {

    private ReactInstanceManager mReactInstanceManager;
    private ReactRootView mReactRootView;
    private Preferences pref;
    private ProgressDialog progressDialog;
    private boolean isReactViewRendered;
    private Realm realm;
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent intent) {
                String action = intent.getAction();
                if (action != null && action.equals("ON_CREATE_COLD_VISIT_RNColdVisitActivity")) {
                   finish();
                }

            }
        };
    private LocationHelper locationHelper = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        Preferences.load(this);

        setContentView(R.layout.activity_rn_cold_visit);
        Toolbar toolbar = findViewById(R.id.rn_create_lead_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Redirecting to C360 !!!.. Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();
        Bundle initialProps = new Bundle();
        initialProps.putBoolean("fromAndroid", true);
        initialProps.putString("token", Preferences.getAccessToken());
        initialProps.putString("module", "COLD_VISIT_FORM");
        initialProps.putInt("appUserId", pref.getAppUserId());
        initialProps.putString("userLocations", new Gson().toJson(getLocationList()));
        initialProps.putString("mobileNumber", getIntent().getStringExtra("MOBILE_NUMBER"));
        initialProps.putString("HERE_MAP_APP_ID",pref.getHereMapAppId());
        initialProps.putString("HERE_MAP_APP_CODE", pref.getHereMapAppCode());
        mReactRootView = findViewById(R.id.react_root_view_cold_visit);
        mReactRootView.startReactApplication(
                mReactInstanceManager,
                "NinjaCRMSales",
                initialProps
        );
        initLocation();

        ReactNativeToAndroidConnect.setListener(new ReactNativeToAndroidConnect.ReactNativeToAndroidConnectListener() {
            @Override
            public void onRNComponentDidMount(String view) {
                if(view.equalsIgnoreCase("COLD_VISIT")) {
                    isReactViewRendered = true;
                }
            }

            @Override
            public void onRNComponentWillUnMount(String view) {
                if(view.equalsIgnoreCase("COLD_VISIT")) {
                    isReactViewRendered = false;
                }
            }
        });

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }


    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
        try {
            registerReceiver(broadcastReceiver, new IntentFilter("ON_CREATE_COLD_VISIT_RNColdVisitActivity"));
            registerReceiver(broadcastReceiver, new IntentFilter("RN_COMPONENT_DID_MOUNT"));
            registerReceiver(broadcastReceiver, new IntentFilter("RN_COMPONENT_WILL_UNMOUNT"));

        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (locationHelper != null) {
            locationHelper.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initLocation() {
        locationHelper = LocationHelper.getInstance(this, new CallBack() {
            @Override
            public void onCallBack() {

            }

            @Override
            public void onCallBack(Object data) {
                if (data instanceof Location) {
                    Location location = (Location) data;
                    new Handler(getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (mReactInstanceManager != null && mReactInstanceManager.getCurrentReactContext() != null && isReactViewRendered) {
                                WritableMap params = Arguments.createMap();
                                params.putString("LATITUDE", String.valueOf(location.getLatitude()));
                                params.putString("LONGITUDE", String.valueOf(location.getLongitude()));
                                sendEvent(mReactInstanceManager.getCurrentReactContext(),
                                        "onGettingLocationColdVisit", params);
                                locationHelper.abort();
                            }
                        }
                    });


                }
            }

            @Override
            public void onError(String error) {

            }
        }).listen();
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    private Locations[] getLocationList() {
        RealmResults<UserLocationsDB> data =
                realm.where(UserLocationsDB.class).distinct("location_id");
        Locations[] userLocations = new Locations[data != null ? data.size() : 0];
        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i) != null) {
                    userLocations[i] = new Locations(Util.getInt(data.get(i).getLocation_id()), data.get(i).getName());
                }
            }
        }

        return userLocations;

    }

    private class Locations {
        private int id;
        private String text;

        public Locations(int id, String text) {
            this.id = id;
            this.text = text;
        }
    }


}
