import { UPDATE_CURRENT_SCREEN } from './game_intro_types';


export const updateCurrentGameScreen = payload => {
  return {
    type: UPDATE_CURRENT_SCREEN,
    payload: payload
  }
}
