package com.salescrm.telephony.interfaces;

/**
 * Created by akshata on 27/6/16.
 */
public interface FabFragmentcommunication {
    void add_note();
    void change_lead_status();
    void add_activity();
    void add_car();
    void email_sms();
    void call();
    void startSearchActivity();
}
