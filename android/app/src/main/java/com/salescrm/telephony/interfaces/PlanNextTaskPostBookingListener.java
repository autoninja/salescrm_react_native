package com.salescrm.telephony.interfaces;

/**
 * Created by bharath on 23/6/17.
 */

public interface PlanNextTaskPostBookingListener {
    void onPlanNextTaskPostBookingResult(int activityId, int answerId);
}
