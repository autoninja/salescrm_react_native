import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image, ScrollView} from 'react-native';
import { createStackNavigator, NavigationEvents } from 'react-navigation';
import RestClient from '../network/rest_client'

import {eventLeaderBoard} from '../clevertap/CleverTapConstants';
import CleverTapPush from '../clevertap/CleverTapPush'

const styles = StyleSheet.create({
		RectangleShapeView: {
			flex:1,
			justifyContent: 'center',
			alignItems: 'center',
			width: '80%',
		    height: 30,
		    flexDirection: 'row',
		    backgroundColor: '#FFC107',
		    borderBottomLeftRadius:10,
		    borderBottomRightRadius: 10,
		    shadowColor: '#000000',
		  }

	});

const img = "https://tineye.com/images/widgets/mona.jpg";

export default class LeaderBoardWallOfFame extends React.Component {

	static navigationOptions = ({ navigation }) => {
    return {
      title: 'Wall of Fame',
    };
  };

	constructor(props)
		{
		    super(props);

		    this.state = {

		    	infoArray : someInfo,
		    	consultant : consultants,
		    	bestTeam : {},
		    	bestConsultant : {}

			}
		}
		logClevertap() {
			CleverTapPush.pushEvent(eventLeaderBoard.event, { 'LeaderBoard Type' : eventLeaderBoard.keyType.valuesType.WALL_OF_FAME });
		}

		componentDidMount(){
   		   this.makeRemoteRequest();
				 const { navigation } = this.props;

				 console.log('navigationL:'+navigation.getParam('isRefresh'))
  		}

  		makeRemoteRequest = () => {
  			this.setState({ isLoading: true});

  			new RestClient().getWallOfFame({date:getSelectedGameDate(),locationId:global.selectedGameLocationId}).then( (data) => {
		      if(data.statusCode == 4001 || data.statusCode == 5001) {
		                  Alert.alert(
		                'Failed',
		                'Please try again',
		                [
		                  {text: 'Logout', onPress: () => this.logout()},
		                  {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
		                ],
		                { cancelable: false }
		                )
		                  return;
		                }
		                console.log("loa loa "+JSON.stringify(data));
		               	let teams = [];
		               	let bestTeam = {};
		               	let bestConsultant = {};
		                if(data.result && data.result.best_team) {
		                	bestTeam = data.result.best_team;
		              	}

		              	if(data.result && data.result.best_consultant) {
		                	bestConsultant = data.result.best_consultant;
		              	}
		               this.setState({
						    		isLoading: false,
						    		bestTeam:bestTeam,
						    		bestConsultant : bestConsultant
									});
										//console.error(this.state.teams)

		          }).catch((error) => {
		            console.error("Error Team Details"+ error)
		            if (error.response && error.response.status == 401) {
		                Alert.alert(
		              'Failed',
		              'Please try again',
		              [
		                {text: 'Logout', onPress: () => this.logout()},
		                {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
		              ],
		              { cancelable: false }
		            )
		              }
		          });
			}
		moveToPreviousMonth(){
			Alert.alert('Previous Month');
		}

		moveToNextMonth(){
			Alert.alert("Next Month");
		}

		render () {

		return (
			<View style={{flex: 1, alignItems : 'center', backgroundColor:'#2e5e86',justifyContent: 'flex-start', padding: 10}}>

			<NavigationEvents
				onWillFocus={payload => {
					this.logClevertap();

				}}
			/>
				{/*<View style={{flex: 1, width: '100%', flexDirection: 'row', padding: 5, alignItems: 'center', justifyContent: 'center'}}>
					<TouchableOpacity onPress={()=>this.moveToPreviousMonth()}>
						<Image
							        style={{ width: 25, height: 25,}}
		                          	source={require('../images/ic_back_black.png')}
							        />
					</TouchableOpacity>

							<Text style= {{color: '#fff', fontSize: 18, margin: 5}}> December </Text>

					<TouchableOpacity onPress={()=>this.moveToNextMonth()}>
							<Image
						        style={{ width: 25, height: 25,}}
	                          	source={require('../images/ic_right_black.png')}
						        />
					</TouchableOpacity>
				</View>*/}
		    	<View style={{ flex:5, height: 50,width: '100%', alignItems : 'center', backgroundColor:'#53759D',justifyContent: 'center', margin: 10, borderRadius: 2}}>
		    		<View style={styles.RectangleShapeView}>
		    				<Image
						        style={{ width: 15, height: 15,}}
	                          	source={require('../images/star.png')}
						        />

							<Text> {(this.state.bestTeam)?this.state.bestTeam.title : null} </Text>

							<Image
						        style={{ width: 15, height: 15,}}
	                          	source={require('../images/star.png')}
						        />
		    		</View>

		    		<View style={{flex:4, flexDirection: 'row', alignItems: 'center'}}>
		    			<View style= {{flex:2, alignItems: 'flex-end'}}>
		    				<Text style={{color: '#fff', fontSize: 13, alignItems: 'flex-end' }}> {(this.state.bestTeam)?this.state.bestTeam.team_name: null} </Text>
		    				<Text style={{color: '#fff', fontSize: 13, alignItems: 'flex-end' }}> {(this.state.bestTeam)?this.state.bestTeam.team_leader_name: null} </Text>
		    			</View>

		    			<View style= {{flex:1, alignItems: 'center', marginLeft: 5}}>
		    				<Image
						        style={{ width: 46, height: 46,borderRadius: 46/2}}
	                          	source={{uri: this.state.bestTeam.team_leader_photo_url}}
						        />
		    			</View>

		    			<View style= {{flex:5, alignItems: 'center', justifyContent: 'center', marginTop: 5}}>
		    			{(this.state.bestTeam.team_users && this.state.bestTeam.team_users.length>0)?
		    				<FlatList
		    				data = {this.state.bestTeam.team_users}
		    				renderItem={({item, index}) =>(
			    				<View style= {{justifyContent: 'center', alignItems: 'center', margin: 5}}>
				    				<Image
								        style={{ width: 28, height: 28, borderRadius: 28/2}}
			                          	source={{uri:item.user_photo_url}}
							        />
			    				</View>)}
		    				numColumns={5}
		    				keyExtractor= {(item, index) => index.toString() }
		    				/>
		    				:null}
		    			</View>
		    		</View>


		    	</View>

		    	<View style={{flex:5, height: 50, width: '100%', alignItems : 'center', backgroundColor:'#53759D',justifyContent: 'center',  margin: 10, borderRadius: 2}}>
		    		<View style={styles.RectangleShapeView}>
		    			<Image
						        style={{ width: 15, height: 15,}}
	                          	source={require('../images/star.png')}
						        />
						<Text> {this.state.bestConsultant.title} </Text>

						<Image
						        style={{ width: 15, height: 15,}}
	                          	source={require('../images/star.png')}
						        />
		    		</View>

		    		<View style={{flex:4, flexDirection: 'row'}}>
		    		{(this.state.bestConsultant.best_dses)?this.state.bestConsultant.best_dses.map((info, i) =>
			    		<View key={i} style={{margin: 5, flexDirection: 'column', alignItems: 'center'}}>

			    			<Image
							        style={{ width: 46, height: 46,borderRadius: 46/2}}
		                          	source={{uri: info.photo_url}}
							        />
							<Text style={{color: '#fff', fontSize: 9}}> {info.category} </Text>
							<Text style={{color: '#fff', fontSize: 13}}> {info.name}} </Text>

			    		</View>
		    		 ): null }
		    		</View>

		    	</View>
		    	<View style={{flex:8}}>

		    	</View>
	    	</View>
			);
	}
}

const someInfo = ["Mobile Phones", "Restaurants", "Tv Channels", "Music"];
const consultants =
[{"name" :"Mobile"}];
