import React, { Component } from 'react'
import { View, Text, } from 'react-native'

export default class LostDropRevenueLoss extends Component {
  render() {
    let title = this.props.title?this.props.title:"Loss in Revenue";
    let lossValue = this.props.loss?this.props.loss:"---";
    return (
      <View style={[{flexDirection:'row', alignSelf:'center', marginTop:20, marginBottom:20, justifyContent:'center', alignItems:'center', padding:10, flexWrap:'wrap'}, this.props.style]}>
        <Text style={{color:'#303030', fontSize:14, textAlign:'center'}}>{title+' ≈ '}</Text>
        <Text style={{color:'#FF0000', fontSize:18, fontWeight:'700',}}>&#8377;{" "+lossValue+"  "}</Text>
      </View>
    );
  }
}
