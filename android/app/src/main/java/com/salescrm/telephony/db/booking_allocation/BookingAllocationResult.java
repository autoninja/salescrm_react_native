package com.salescrm.telephony.db.booking_allocation;

import io.realm.RealmList;
import io.realm.RealmObject;

public class BookingAllocationResult extends RealmObject {

    public RealmList<VinAlllocationStatus> vin_allocation_statuses = new RealmList<>();

    public RealmList<VinAlllocationStatus> getVinAlllocationStatuses() {
        return vin_allocation_statuses;
    }

    public void setVinAlllocationStatuses(RealmList<VinAlllocationStatus> vinAlllocationStatuses) {
        this.vin_allocation_statuses = vinAlllocationStatuses;
    }
}
