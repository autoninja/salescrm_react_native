package com.salescrm.telephony.response.gamification;

import java.util.ArrayList;

/**
 * Created by bannhi on 28/2/18.
 */

public class BadgesResponse {

    private String statusCode;

    private String message;

    private ArrayList<Result> result;

    private Error error;


    private BadgesResponse badgesResponse;

    public BadgesResponse(BadgesResponse badgesResponse){
        this.badgesResponse = badgesResponse;
    }

    public BadgesResponse getBadgesResponse() {
        return badgesResponse;
    }

    public void setBadgesResponse(BadgesResponse badgesResponse) {
        this.badgesResponse = badgesResponse;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private Integer id;

        private String description;

        private String name;

        private Integer badge_count;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public void setBadge_count(Integer badge_count) {
            this.badge_count = badge_count;
        }

        public String getDescription ()
        {
            return description;
        }

        public void setDescription (String description)
        {
            this.description = description;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public Integer getBadge_count() {
            return badge_count;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [id = "+id+", description = "+description+", name = "+name+", badge_count = "+badge_count+"]";
        }
    }

    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }
}
