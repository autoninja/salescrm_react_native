package com.salescrm.telephony.model.NewFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prateek on 5/8/18.
 */

public class EtvbrSelectedFilters {

    private List<Filters> filters = new ArrayList<>();

    public List<Filters> getFilters() {
        return filters;
    }

    public void setFilters(List<Filters> filters) {
        this.filters = filters;
    }

    public class Filters {

        private List<Values> values = new ArrayList<>();
        private String key;
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Values> getValues() {
            return values;
        }

        public void setValues(List<Values> values) {
            this.values = values;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }

    public class Values {

        private String id;
        private List<Subcategories> subcategories = new ArrayList<>();

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<Subcategories> getSubcategories() {
            return subcategories;
        }

        public void setSubcategories(List<Subcategories> subcategories) {
            this.subcategories = subcategories;
        }
    }

    public class Subcategories {

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

}
