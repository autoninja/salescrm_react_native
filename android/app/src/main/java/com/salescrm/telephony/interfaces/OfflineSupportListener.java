package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.model.AllLeadEnquiryListResponse;
import com.salescrm.telephony.model.BookingAllocationModel;
import com.salescrm.telephony.model.EvaluationManagerModel;
import com.salescrm.telephony.model.NewFilter.NewFilter;
import com.salescrm.telephony.response.ActionPlanResponse;
import com.salescrm.telephony.response.ActiveEventsResponse;
import com.salescrm.telephony.response.AddLeadElementsResponse;
import com.salescrm.telephony.response.AllLeadCustomerDetailsResponse;
import com.salescrm.telephony.response.AppConfResponse;
import com.salescrm.telephony.response.AppUserResponse;
import com.salescrm.telephony.response.CarBrandModelVariantsResponse;
import com.salescrm.telephony.response.CreForActivityResponses;
import com.salescrm.telephony.response.DealershipsResponse;
import com.salescrm.telephony.response.EventCategoryResponse;
import com.salescrm.telephony.response.FilterActivityAndStagesResponse;
import com.salescrm.telephony.response.GetPipelineResponse;
import com.salescrm.telephony.response.LeadMobileResponse;
import com.salescrm.telephony.response.LeadTeamUserResponse;
import com.salescrm.telephony.response.ScoreboardResponse;

import java.util.List;

import retrofit.RetrofitError;

/**
 * Created by bharath on 14/10/16.
 */

public interface OfflineSupportListener {

    //ActionPlan
    void onActionPlanFetched(List<ActionPlanResponse.Result.Data> data);

    //Forms and Activities
/*    void onFormsDataFetched(boolean success, List<ActionPlanResponse.Result.Data> data);
    void onActivitiesFetched(boolean success);*/

    //360Data
    void onC360InformationFetched(AllLeadCustomerDetailsResponse data);

    //FetchScoreBoardData
    void onScoreboardDataFetched(ScoreboardResponse data);

    //FetchLeadElements
    void onLeadElementsFetched(AddLeadElementsResponse addLeadElementsResponse);

    //FetchFilterData
    void onFilterDataFetched(FilterActivityAndStagesResponse data);

    //TeamUsers
    void onTeamUsersFetched(LeadTeamUserResponse leadTeamUserResponse);
    void onOfflineSupportApiError(RetrofitError error,int fromId);

    //TeamUsers
    void onActivityUsersFetched(CreForActivityResponses creForActivityResponse);

    //Car - variants:  model, model name {variants and fuel type}
    void onCarModelAndVariantsFetched(CarBrandModelVariantsResponse carBrandModelVariantsResponse);

/*    //LeadStageData
    void onLeadStageDataFetched(GetPipelineResponse resp);*/


    //UserData
    void onUserDataFetched(AppUserResponse resp);

    //AppConf
    void onAppConfFetched(AppConfResponse resp);

    //Lead Mobiles
    void onLeadMobileFetched(LeadMobileResponse response);

    //Lead Mobiles
    void onEtvbrFiltersFetched(NewFilter response);

    //Dealerships
    void onDealerShipFetched(DealershipsResponse dealershipsResponse);


    void onBookingAllocationDataFetched(BookingAllocationModel response);

    void onFetchActiveEvents(ActiveEventsResponse response);

    void onFetchEventCategories(EventCategoryResponse response);

    void onFetchEvaluationManager(EvaluationManagerModel response);

    void onFetchAwsCredentials();
}
