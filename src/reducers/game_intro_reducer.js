import { UPDATE_CURRENT_SCREEN } from '../actions/game_intro_types';

const initialState = {
  currentScreen: 0
};

const gameIntroReducer = (state = initialState, action) => {
  switch(action.type) {
    case UPDATE_CURRENT_SCREEN:
      return {
        ...state,
        currentScreen: action.payload,
      };
    default:
      return state;
  }
}

export default gameIntroReducer;
