import React, { Component } from 'react';

import { Platform, StyleSheet, View, Text, Modal, Button, TouchableOpacity, Alert } from 'react-native';

import DateRangePicker from '../utils/DateRangePicker';
import styles from '../styles/styles'

export default class DateRangePickerDialog extends Component {

  constructor(props) {
  super(props);
  let initDate =  this.props.navigation.getParam('initDate');
  var startDate = initDate.startDate;
  var endDate =  initDate.endDate;
  let startDateStr = global.global_filter_date.startDateStr?global.global_filter_date.startDateStr:startDate.getFullYear()+'-'+("0" + (startDate.getMonth() + 1)).slice(-2)+'-'+("0" + startDate.getDate()).slice(-2);
  let endDateStr = global.global_filter_date.endDateStr?global.global_filter_date.endDateStr:endDate.getFullYear()+'-'+("0" + (endDate.getMonth() + 1)).slice(-2)+'-'+("0" + endDate.getDate()).slice(-2);

  this.state = {startDateStr: startDateStr, endDateStr : endDateStr};

  }

  _applyOrCloseFilter(navigation,s, e) {
      navigation.state.params.onFilterApply(s, e);
      navigation.goBack();
  }


  render () {
    let navigation = this.props.navigation;


    return (




                      <View style={styles.Alert_Main_View}>

                      <View style = {{backgroundColor:'#303030',paddingTop:20, borderRadius:6,}}>
                      <Text style = {{color:'#fff', marginLeft:20, marginBottom:20, fontSize:18, fontWeight:'bold'}}> Select Date Range </Text>

                      <DateRangePicker

                     initialRange={[this.state.startDateStr, this.state.endDateStr]}
                     onSuccess={(s, e) => {

                       this.setState({startDateStr : s, endDateStr:e});



                     }}
                     theme={{ markColor: '#2e5e86', markTextColor: 'white' }}/>

                     <View style= {{flexDirection:'row', alignItems:'center',height:60}}>
                     <TouchableOpacity style = {{flex:1, alignItems:'center'}} onPress = { ()=> this._applyOrCloseFilter(this.props.navigation)}>
                     <Text style={{color:'#fff', fontSize:16}}> Close </Text>
                     </TouchableOpacity>

                     <TouchableOpacity style = {{flex:1,  alignItems:'center'}} onPress = { ()=> {

                       if(this.state.startDateStr.split('-')[1] === this.state.endDateStr.split('-')[1]) {
                        global.global_filter_date.startDateStr = this.state.startDateStr;
                        global.global_filter_date.endDateStr = this.state.endDateStr;
                        this._applyOrCloseFilter(this.props.navigation,this.state.startDateStr, this.state.endDateStr)
                       }
                       else {
                           alert('Please select date from same month');
                       }

                     }}>
                      <Text style={{color:'#fff', fontSize:16}}> Apply </Text>
                     </TouchableOpacity>
                     </View>


                    </View>

                    </View>

    );
  }

}
