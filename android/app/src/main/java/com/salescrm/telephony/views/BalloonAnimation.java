package com.salescrm.telephony.views;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;

import com.salescrm.telephony.R;

import java.util.Random;

public class BalloonAnimation extends FrameLayout {
    private Context context;
    public BalloonAnimation(@NonNull Context context) {
        super(context);
        this.context = context;
    }


    public BalloonAnimation(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void start() {
        this.removeAllViews();
        this.setBackgroundColor(Color.TRANSPARENT);
        final View viewInflate = LayoutInflater.from(context).inflate(R.layout.layout_balloons, this, false);
        final View view = viewInflate.findViewById(R.id.ll_balloon_holder);
        this.addView(viewInflate);

        this.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                    final View childView = ((ViewGroup) view).getChildAt(i);
                    setAnimation(view,childView);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    BalloonAnimation.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    BalloonAnimation.this.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });


    }

    private void setAnimation(final View viewHolder, final View childView) {
        Animation animation = new TranslateAnimation(0, 0, 0,
                -(childView.getTop() + childView.getHeight()));
        Random r = new Random();
        int low = 3000;
        int high = 5000;
        int result = r.nextInt(high - low) + low;
        animation.setDuration(result);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                childView.setVisibility(INVISIBLE);
                boolean isAllInvisible =false;
                for (int i = 0; i < ((ViewGroup) viewHolder).getChildCount(); i++) {
                    final View childView = ((ViewGroup) viewHolder).getChildAt(i);
                    if(childView.getVisibility() == INVISIBLE) {
                        isAllInvisible = true;
                    }
                    else {
                        isAllInvisible = false;
                        break;

                    }
                }
                if(isAllInvisible) {
                    BalloonAnimation.this.removeAllViews();
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        childView.startAnimation(animation);

    }

}
