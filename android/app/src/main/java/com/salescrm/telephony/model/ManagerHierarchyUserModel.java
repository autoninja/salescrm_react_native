package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by bharath on 4/9/17.
 */

public class ManagerHierarchyUserModel {
    private String statusCode;

    private String message;

    private List<Result> result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private List<Team_leaders> team_leaders;

        private String location_name;

        private Integer manager_id;

        private String name;

        private Integer location_id;

        private String image_url;

        public List<Team_leaders> getTeam_leaders() {
            return team_leaders;
        }

        public void setTeam_leaders(List<Team_leaders> team_leaders) {
            this.team_leaders = team_leaders;
        }

        public String getLocation_name() {
            return location_name;
        }

        public void setLocation_name(String location_name) {
            this.location_name = location_name;
        }

        public Integer getManager_id() {
            return manager_id;
        }

        public void setManager_id(Integer manager_id) {
            this.manager_id = manager_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getLocation_id() {
            return location_id;
        }

        public void setLocation_id(Integer location_id) {
            this.location_id = location_id;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        @Override
        public String toString() {
            return "ClassPojo [team_leaders = " + team_leaders + ", location_name = " + location_name + ", manager_id = " + manager_id + ", name = " + name + ", location_id = " + location_id + "]";
        }
    }

    public class Team_leaders {
        private String image_url;

        private List<Users> users;

        private Integer team_leader_id;

        private String team_leader_name;

        private Integer team_id;

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public List<Users> getUsers() {
            return users;
        }

        public void setUsers(List<Users> users) {
            this.users = users;
        }

        public Integer getTeam_leader_id() {
            return team_leader_id;
        }

        public void setTeam_leader_id(Integer team_leader_id) {
            this.team_leader_id = team_leader_id;
        }

        public String getTeam_leader_name() {
            return team_leader_name;
        }

        public void setTeam_leader_name(String team_leader_name) {
            this.team_leader_name = team_leader_name;
        }

        public Integer getTeam_id() {
            return team_id;
        }

        public void setTeam_id(Integer team_id) {
            this.team_id = team_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [users = " + users + ", team_leader_id = " + team_leader_id + ", team_leader_name = " + team_leader_name + ", team_id = " + team_id + "]";
        }

        public class Users {
            private Integer id;

            private String image_url;

            private String name;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getImage_url() {
                return image_url;
            }

            public void setImage_url(String image_url) {
                this.image_url = image_url;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", image_url = " + image_url + ", name = " + name + "]";
            }
        }
    }
}
