const CUSTOMER_TYPES = [
    { id: 1, text: 'Corporate(Individual)' },
    { id: 2, text: 'Individual' },
    { id: 3, text: 'Government' },
    { id: 4, text: 'Corporate(Company)' },
    { id: 5, text: 'Fleet(Individual)' },
    { id: 6, text: 'Fleet(Company)' }
]
const CUSTOMER_TYPES_ACTIONS_INDIVIDUAL = [
    {
        key: 'customer_aadhaar',
        visible: true
    },
    {
        key: 'company_gst',
        visible: false
    },
    {
        key: 'customer_care_of',
        visible: true
    },
    {
        key: 'customer_care_of_value',
        visible: true
    }
]
const CUSTOMER_TYPES_ACTIONS_COMPANY = [
    {
        key: 'customer_aadhaar',
        visible: false
    },
    {
        key: 'company_gst',
        visible: true
    },
    {
        key: 'customer_care_of',
        visible: false
    },
    {
        key: 'customer_care_of_value',
        visible: false
    }
]
const CARE_OF_TYPES = [
    { id: 'S/O', text: 'S/O' },
    { id: 'D/O', text: 'D/O' },
    { id: 'W/O', text: 'W/O' },
]
const FINANCE_TYPES = [{
    index: 0,
    key: 'In House Loan',
    title: 'In House Finance',
},
{
    index: 1,
    key: 'Out House Loan',
    title: 'Out House Finance',
},
{
    index: 2,
    key: 'Full Cash',
    title: 'No Finance',
}
]
const EXCHANGE_ITEMS = [{
    index: 0,
    key: '1',
    title: 'Yes',
},
{
    index: 1,
    key: '0',
    title: 'No',
},
]
const CUSTOMER_TYPE_ACTION_VALUES = [
    {
        value: CUSTOMER_TYPES[0].id,
        actions: CUSTOMER_TYPES_ACTIONS_INDIVIDUAL
    },
    {
        value: CUSTOMER_TYPES[1].id,
        actions: CUSTOMER_TYPES_ACTIONS_INDIVIDUAL
    },
    {
        value: CUSTOMER_TYPES[2].id,
        actions: CUSTOMER_TYPES_ACTIONS_INDIVIDUAL
    },
    {
        value: CUSTOMER_TYPES[3].id,
        actions: CUSTOMER_TYPES_ACTIONS_COMPANY
    },
    {
        value: CUSTOMER_TYPES[4].id,
        actions: CUSTOMER_TYPES_ACTIONS_INDIVIDUAL
    },
    {
        value: CUSTOMER_TYPES[5].id,
        actions: CUSTOMER_TYPES_ACTIONS_COMPANY
    },
]
const BUYER_TYPE_EXCHANGE_CAR = 3

const VIN_ALLOCATION_IDS = {
    TO_BE_ALLOCATED: 1,
    UN_ALLOCATED: 2,
    ALLOCATED: 3,
    TO_ORDER: 4
}
const VIN_ALLOCATION_ACTION_ALLOCATED = [
    {
        key: 'allocation_vin_number',
        visible: true
    },
]
const VIN_ALLOCATION_ACTION_NO_ALLOCATED = [
    {
        key: 'allocation_vin_number',
        visible: false
    },
]
const VIN_ALLOCATION_ACTION_VALUES = [
    {
        value: VIN_ALLOCATION_IDS.TO_BE_ALLOCATED,
        actions: VIN_ALLOCATION_ACTION_NO_ALLOCATED
    },
    {
        value: VIN_ALLOCATION_IDS.UN_ALLOCATED,
        actions: VIN_ALLOCATION_ACTION_NO_ALLOCATED
    },
    {
        value: VIN_ALLOCATION_IDS.ALLOCATED,
        actions: VIN_ALLOCATION_ACTION_ALLOCATED
    },
    {
        value: VIN_ALLOCATION_IDS.TO_ORDER,
        actions: VIN_ALLOCATION_ACTION_NO_ALLOCATED
    }
]

module.exports = {
    CUSTOMER_TYPES,
    CARE_OF_TYPES,
    FINANCE_TYPES,
    EXCHANGE_ITEMS,
    BUYER_TYPE_EXCHANGE_CAR,
    VIN_ALLOCATION_IDS,
    CUSTOMER_TYPE_ACTION_VALUES,
    VIN_ALLOCATION_ACTION_VALUES
}