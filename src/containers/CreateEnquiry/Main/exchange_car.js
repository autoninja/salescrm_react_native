import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback, KeyboardAvoidingView, ScrollView, TextInput
} from 'react-native'
import styles from './CreateEnquiry.styles'
import ItemPicker from '../../../components/createLead/item_picker'
import CreateLeadExpander from '../../../components/createLead/create_lead_expander'
import PickerList from '../../../utils/PickerList'
import Utils from '../../../utils/Utils'
import CustomDateTimePicker from '../../../utils/DateTimePicker'
import ImagePicker from 'react-native-image-picker';
import CreateEnquiryModel from './CreateEnquiryModel'
import CreateEnquiryModelWithExchangeCar from './CreateEnquiryModelWithExchangeCar'
import AddExchangeCarModel from './add_exchange_car_model'
import RestClient from '../../../network/rest_client'
import Toast, { DURATION } from 'react-native-easy-toast'
import ViewImageScreen from './view_image_screen'

import CleverTapPush from '../../../clevertap/CleverTapPush'
import { fetchVehicleDetails } from '../../../clevertap/CleverTapConstants';

import NavigationService from '../../../navigation/NavigationService'

const innerStyles = StyleSheet.create({
    radioSelected: {
        borderColor: '#00a7f7'
    },
    radioUnseleceted: {
        backgroundColor: '#000'
    }
});

const imageStyle = StyleSheet.create({
    imageOverlay: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
    },
    container: {
        backgroundColor: '#e1e4e8',
        width: 50,
        height: 50
    },
});

const childStyles = StyleSheet.create({
    reg_button_enabled: {
        backgroundColor: '#007fff', flex: 2, height: 30, justifyContent: 'center', marginTop: 25, marginLeft: 5, borderRadius: 3
    },
    reg_button_disabled: {
        backgroundColor: '#808080', flex: 2, height: 30, justifyContent: 'center', marginTop: 25, marginLeft: 5, borderRadius: 3
    }
});

const options = {
    title: 'Select Photo',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};


export default class ExchangeCar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            exchange_car_fields: [
                { id: 'vehicle_detail', title: 'Exchange vehicle details', isExpanded: false, data: {} },
                { id: 'evaluator_request', title: 'Evaluator Request', isExpanded: false, data: {} },
                { id: 'inspection', title: 'Inspection', isExpanded: false, data: {} },
                { id: 'price', title: 'Quote Price', isExpanded: false, data: {} },
            ],
            showItemPickerFilter: false,
            showItemPicker: false,
            dataItemPicker: {},
            carModelAndVariantType: [],
            allVehicles: this.getAllVehicle(),
            is_under_insurance: false,
            is_doc_verified: false,
            vehicle_condition: '',
            dateTimePickerVisibility: false,
            dateTimePickerPayload: {},
            car_images_array: [],
            enquiryField: this.getEnquiryField(),
            //userLocation: this.getUserLocation(),
            evaluatorsList: [],
            loading: false,
            openImageGallery: false,
            selectedIndex: 0,
            location_data: this.getLocationData(),
            regButtonEnabled: true,

        }
    }

    componentDidMount() {
        //this.fillUpRegDetail();
        var { enquiryField } = this.state;
        let locationId = (this.props.navigation != null) ? enquiryField[0].data.showroomLocation.id : this.props.leadLocationId
        new RestClient().getAllEvaluators({ location_id: locationId }).then((data) => {
            if (data.result) {
                this.setState({ evaluatorsList: data.result })
            }
        }).catch(error => {
            //console.log(error);
            if (!error.response) {
                this.showAlert('No Interent Connection');
            }
        });
    }

    getRegNoDetails(regNo) {
        if (regNo) {
            var registrationNo = regNo.trim();
            //var enquiryFields = this.state.enquiryFields;
            this.setState({ loading: true });
            new RestClient().getRegDetails({ reg_no: registrationNo }).then((data) => {
                console.log("RegNo", data);
                if (data && data.result && data.result.owner) {
                    //this.showAlert("Reg detail received!")
                    //enquiryFields[1].data.customerNameFirst = data.result.owner;
                    this.fillUpRegDetail(data.result);
                    this.setState({ loading: false, regButtonEnabled: false })
                }
                else {
                    this.showAlert(data.message);
                    this.setState({ loading: false, regButtonEnabled: true })
                }

            }).catch(error => {
                //console.log(error);
                if (!error.response) {
                    this.showAlert('No Interent Connection');
                }
                else {
                    this.showAlert('Please enter a valid reg no.');
                }
                this.setState({ loading: false });
            });
            CleverTapPush.pushEvent(fetchVehicleDetails.event);
        }
    }

    fillUpRegDetail(vehicle_detail) {
        let { exchange_car_fields } = this.state;
        let { is_under_insurance } = this.state;
        if (vehicle_detail != '') {
            var vehicleDetails = vehicle_detail;
            exchange_car_fields[0].data = {
                vehicleBrand: { text: this.getBrandName(vehicleDetails.brand_id) ? this.getBrandName(vehicleDetails.brand_id) : "", id: vehicleDetails.brand_id },
                vehicleModel: { text: vehicleDetails.model ? vehicleDetails.model : "", id: vehicleDetails.model_id ? vehicleDetails.model_id : "" },
                vehicleVariant: vehicleDetails.variant ? vehicleDetails.variant : "",
                vehicleColor: vehicleDetails.color ? vehicleDetails.color : "",
                year: { text: vehicleDetails.reg_year ? vehicleDetails.reg_year : "" },
                //kmRun: data.result.vehicle_details.kms ? data.result.vehicle_details.kms : '',
                reg_no: vehicleDetails.reg_no ? vehicleDetails.reg_no : "",
                vin_no: vehicleDetails.vin_no ? vehicleDetails.vin_no : "",
                vehicleFuelType: vehicleDetails.fuel_type ? vehicleDetails.fuel_type : "",
                // ownerType: {
                //     text: data.result.vehicle_details.owner_type ? data.result.vehicle_details.owner_type : ''
                // }
            };

            exchange_car_fields[2].data = {
                is_under_insurance: vehicleDetails.ins_upto ? vehicleDetails.ins_upto : '',
            }

            is_under_insurance = vehicleDetails.ins_upto ? vehicleDetails.ins_upto : ''

            this.setState({ exchange_car_fields, is_under_insurance })
        }
    }

    getRegDetails() {
        if (this.props.navigation != null && Object.keys(this.props.navigation.getParam('regDetails')).length !== 0) {
            //console.log("regDetails true", this.props.navigation.getParam('regDetails'))
            return this.props.navigation.getParam('regDetails');
        } else {
            //console.log("regDetails false", this.props.navigation.getParam('regDetails'))
            return '';
        }
    }

    showAlert(alert) {
        this.refs.alert.show(alert);
    }

    getYearsList() {
        let maxOffset = 30;
        let currentYear = (new Date()).getFullYear();
        let list = [];
        for (let x = 0; x <= maxOffset; x++) {
            list.push({ text: currentYear - x })
        }
        //console.log("moment=", allYears)
        return list;
    }

    getInsuranceYear() {
        let maxOffset = 7;
        let currentYear = (new Date()).getFullYear() - 2;
        let list = [];
        for (let x = 0; x <= maxOffset; x++) {
            //console.log(x + currentYear)
            list.push({ text: x + currentYear })
        }
        return list;
    }


    createEnquiry() {
        //console.log("Are you attempting createEnquiry", this.state.exchange_car_fields)

        let inputData = CreateEnquiryModelWithExchangeCar.getInputDataServer(this.state.enquiryField, this.state.exchange_car_fields, this.state.location_data);
        //console.log("How are u doin?? ", inputData)
        try {
            //let inputData = CreateEnquiryModel.getInputDataServer(this.state.enquiryFields);
            //console.log("inputData", JSON.stringify(this.state.exchange_car_fields[2].data.car_images_array[0]));
            this.setState({ loading: !this.state.loading });
            new RestClient().createEnquiry(inputData).then((data) => {
                this.setState({ loading: false })
                //console.log(data);
                if (data) {
                    if (data.statusCode == '2002' && data.result) {
                        if (Platform.OS == "android") {
                            let firstName = JSON.stringify(inputData.lead_data.customer_details.first_name);
                            let lastName = JSON.stringify(inputData.lead_data.customer_details.last_name);
                            let title = JSON.stringify(inputData.lead_data.customer_details.title);
                            let leadTagId = JSON.stringify(inputData.lead_data.lead_tag_id);
                            //console.log("Chiraggg " + firstName, lastName, title, leadTagId);
                            NativeModules.ReactNativeToAndroid.onEnquiryCreate(JSON.stringify(data.result),
                                JSON.stringify(data.scheduled_activity_id),
                                firstName,
                                lastName,
                                title,
                                leadTagId
                            );
                        }
                        else {
                            NavigationService.resetScreen("C360MainScreen", { leadId: data.result });
                        }
                    }
                    else if (data.message) {
                        this.showAlert(data.message);
                    }
                    else {
                        this.showAlert('Error creating lead');
                    }

                }
                else {
                    this.showAlert('Error creating lead');
                }
                this.setState({ loading: false });

            }).catch(error => {
                //console.log(error);
                if (!error.response) {
                    this.showAlert('No Interent Connection');
                }
                else {
                    this.showAlert('Error creating lead');
                }
                this.setState({ loading: false });
            });
        }
        catch (e) {
            this.showAlert(e + '');
        }
    }

    addExchangeCar() {
        let leadId = this.props.leadId;
        this.setState({ loading: true });
        let inputData = AddExchangeCarModel.getInputDataServer(this.state.exchange_car_fields, leadId);
        //console.log("How are u doin?? ", inputData)
        try {
            //let inputData = CreateEnquiryModel.getInputDataServer(this.state.enquiryFields);
            //console.log("inputData", JSON.stringify(this.state.exchange_car_fields[2].data.car_images_array[0]));
            new RestClient().addExchangeCar(inputData).then((data) => {
                this.setState({ loading: false })
                //console.log(data);
                if (data) {
                    if (data.statusCode == '2002' && data.result) {
                        if (this.isThisAndroid()) {
                            NativeModules.ReactNativeToAndroid.onAddedExchangeCar(data.result.lead_id, data.result.scheduled_activity_id + '');
                        }
                    }
                    else if (data.message) {
                        this.showAlert(data.message);
                    }
                    else {
                        this.showAlert('Error Adding Exchange Car');
                    }

                }
                else {
                    this.showAlert('Error Adding Exchange Car');
                }
                this.setState({ loading: false });

            }).catch(error => {
                //console.log(error);
                if (!error.response) {
                    this.showAlert('No Interent Connection');
                }
                else {
                    this.showAlert('Error Adding Exchange Car');
                }
                this.setState({ loading: false });
            });
        }
        catch (e) {
            this.showAlert(e + '');
            this.setState({ loading: false });
        }
    }

    expandField(index) {
        var exchange_car_fields = this.state.exchange_car_fields;
        for (var i = 0; i < exchange_car_fields.length; i++) {
            if (i == index) {
                exchange_car_fields[index].isExpanded = !exchange_car_fields[index].isExpanded;
            }
            else {
                exchange_car_fields[i].isExpanded = false;
            }
        }

        this.setState({ exchange_car_fields });
    }

    showPicker(data, isSearchShown) {
        let showItemPickerFilter = false || isSearchShown;
        this.setState({ showItemPicker: true, dataItemPicker: data, showItemPickerFilter })
    }

    getSuccessMark(item) {
        let success = 'gone';
        if (item) {
            switch (item.id) {
                case 'vehicle_detail':
                    success = this.isVehicleDetailValid();
                    //success = true;
                    break;
                case 'evaluator_request':
                    success = this.isEvaluatorRequestValid();
                    //success = true;
                    break;
                case 'inspection':
                    success = this.isInspectionValid();
                    //success = true;
                    break;
                case 'price':
                    success = this.isPriceValid();
                    //success = true;
                    break;
            }
        }
        return success;
    }

    isVehicleDetailValid() {
        let { data } = this.state.exchange_car_fields[0];
        if (data.vehicleBrand && data.vehicleModel && data.vehicleColor && data.vehicleVariant && data.vehicleFuelType
            && data.year && data.kmRun && data.ownerType ) {
            if(data.vin_no !== undefined && data.vin_no.includes('*')){
                return false;
            }else{
                return true;
            }
        }
        return false;
    }

    isEvaluatorRequestValid() {
        let { data } = this.state.exchange_car_fields[1];
        if (data.evaluation_date && data.evaluator && data.remarks) {
            return true;
        }
        return false;
    }

    isInspectionValid() {
        // let { data } = this.state.exchange_car_fields[2];
        // if (data.is_under_insurance || data.is_doc_verified || data.vehicle_condition || data.car_images_array) {
        //     return true;
        // }

        return 'gone';
    }

    isPriceValid() {
        // let { data } = this.state.exchange_car_fields[3];
        // if (data.expected_price || data.quoted_price || data.comments_extra) {
        //     return true;
        // }
        return 'gone';
    }

    getAllVehicle() {
        if (this.props.navigation != null) {
            return this.props.navigation.getParam('allVehicles', null);
        } else {
            return this.props.allVehicles;
        }
    }

    getEnquiryField() {
        if (this.props.navigation != null) {
            return this.props.navigation.getParam('enquiryField', null);
        } else {
            return null;
        }
    }
    getLocationData() {
        if (this.props.navigation != null) {
            return this.props.navigation.getParam('location_data', null);
        } else {
            return null;
        }
    }

    getBrandName(brand_id) {
        let { allVehicles } = this.state;
        if (brand_id && allVehicles) {
            for (var i = 0; i < allVehicles.length; i++) {

                if (brand_id == allVehicles[i].brand_id) {
                    return allVehicles[i].brand_name;
                }
            }
        }
        return '';
    }

    // getUserLocation() {
    //     return this.props.navigation.getParam('userLocation', null)
    // }

    isThisAndroid() {
        if (this.props.navigation) {
            return this.props.navigation.getParam('fromAndroid', null);
        } else {
            return this.props.fromAndroid;
        }
    }

    getAllVehicleBrandItems() {
        let { allVehicles } = this.state;
        let list = [];
        if (allVehicles) {
            allVehicles.map((brand) => {
                list.push({ id: brand.brand_id, text: brand.brand_name, models: brand.brand_models });
                //console.log("Brand Models. SIze:" + brand.brand_models.length);
            });
        }
        return list;
    }

    getAllVehicleModelsItems(vehicleBrand) {
        let { allVehicles } = this.state;
        //console.log("hyu", vehicleBrand)
        //console.log("vehicleBrand.id", vehicleBrand.id)
        let list = [];
        if (vehicleBrand && vehicleBrand.id && vehicleBrand.models) {
            vehicleBrand.models.map((model) => {
                list.push({ id: model.model_id, text: model.model_name, variants: model.car_variants });
            });
        } else {
            for (var i = 0; i < allVehicles.length; i++) {
                //console.log("allVehicles[i].brand_id", allVehicles[i].brand_id)
                if (vehicleBrand && vehicleBrand.id && (vehicleBrand.id == allVehicles[i].brand_id)) {
                    allVehicles[i].brand_models.map((model) => {
                        list.push({ id: model.model_id, text: model.model_name, variants: model.car_variants });
                    })
                    return list;
                }
            }
        }
        return list;
    }

    getAllVehicleVariants(vehicleModel) {
        //console.log("hyu", vehicleModel)
        let list = [];
        if (vehicleModel && vehicleModel.variants) {
            vehicleModel.variants.map((variants) => {
                list.push({ id: variants.variant_id, text: variants.variant });
            });
        }
        return list;
    }

    getEvaluatorsList(evaluators) {
        //console.log("Hellloooooo " + JSON.stringify(evaluators))
        let list = []
        if (evaluators) {
            evaluators.map((data) => {
                list.push({ id: data.id, text: data.name });
            })
        }
        return list;
    }

    isUnderInsurance = (value) => {
        //console.log("isUnderInsurance", value)
        var { is_under_insurance } = this.state;
        var { exchange_car_fields } = this.state;
        is_under_insurance = value;
        exchange_car_fields[2].data.is_under_insurance = value;
        this.setState({ exchange_car_fields, is_under_insurance });
    }

    isDocVerified = (value) => {
        // console.log("isDocVerified", value)
        var { is_doc_verified } = this.state;
        var { exchange_car_fields } = this.state;
        is_doc_verified = value;
        exchange_car_fields[2].data.is_doc_verified = value;
        this.setState({ exchange_car_fields, is_doc_verified });
    }

    vehicleCondition = (value) => {
        var { vehicle_condition } = this.state;
        var { exchange_car_fields } = this.state;
        vehicle_condition = value;
        exchange_car_fields[2].data.vehicle_condition = value;

        this.setState({ exchange_car_fields, vehicle_condition });
    }


    addCarPhoto() {
        let { exchange_car_fields } = this.state;
        ImagePicker.showImagePicker(options, (response) => {
            //console.error("GGGGGGGG")
            // console.log('Response = Kulchaa');
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                // You can also display the image using data:
                //const source = { uri: 'data:image/jpeg;base64,' + response.data };

                let { car_images_array } = this.state;

                //console.log("car_images_array", imagearray.length)
                //car_images_array.push(source);
                car_images_array.push(response.data)
                exchange_car_fields[2].data.car_images_array = car_images_array;

                this.setState({
                    car_images_array, exchange_car_fields
                })
            }
        })
    }

    openImage(index) {
        let { openImageGallery } = this.state;
        let { selectedIndex } = this.state;
        this.setState({ openImageGallery: !openImageGallery, selectedIndex: index })
    }

    deleteImage(index) {
        let { car_images_array } = this.state;
        let { exchange_car_fields } = this.state;
        //console.log("imagesCount before delete", exchange_car_fields[2].data.car_images_array.length)
        car_images_array.splice(index, 1);
        exchange_car_fields[2].data.car_images_array = car_images_array;
        this.setState({ car_images_array, exchange_car_fields });
    }



    getViews(item, index) {
        if (!item.isExpanded) {
            return <View />
        }
        switch (item.id) {
            case 'vehicle_detail':
                let brandName = "Vehicle Brand *";
                let modelName = "Vehicle Model *";
                let variant = "";
                let color = "";
                let kmRun = "";
                let year = "Year *";
                let ownerType = "Owner Type *";
                let regNo = "";
                let vinNo = "";
                let fuelType = "";
                let ownerTypeList = [{ "text": "First" }, { "text": "Second" }, { "text": "Third" }]
                let { vehicleBrand } = this.state.exchange_car_fields[0].data;
                let { vehicleModel } = this.state.exchange_car_fields[0].data;
                let { vehicleVariant } = this.state.exchange_car_fields[0].data;
                let { data } = this.state.exchange_car_fields[0];

                if (vehicleBrand && vehicleBrand.text) {
                    brandName = vehicleBrand.text;
                }

                if (vehicleModel && vehicleModel.text) {
                    modelName = vehicleModel.text;
                }
                if (vehicleVariant) {
                    variant = vehicleVariant;
                }

                if (data.vehicleColor) {
                    color = data.vehicleColor;
                }

                if (data.year) {
                    year = data.year.text;
                }

                if (data.kmRun) {
                    kmRun = data.kmRun;
                }

                if (data.ownerType) {
                    ownerType = data.ownerType.text;
                }

                if (data.reg_no) {
                    regNo = data.reg_no
                }

                if (data.vin_no) {
                    vinNo = data.vin_no
                }

                if (data.vehicleFuelType) {
                    fuelType = data.vehicleFuelType;
                }

                if (item.isExpanded) {
                    // console.log("vehicleBrand", vehicleBrand ? false : true)
                    return (
                        <View style={[{ display: (item.isExpanded ? 'flex' : 'none') }, styles.field]}>

                            <View style={{ flexDirection: 'row', alignItems: 'center', margin: 5 }}>
                                <View style={{ flex: 5 }}>
                                    <TextInput
                                        style={{ padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}
                                        placeholder='Used Car Reg Num'
                                        value={regNo}
                                        maxLength={10}
                                        autoCapitalize="characters"
                                        onChangeText={(text) => {
                                            let regPattern = /^[a-z0-9]+$/i
                                            let { exchange_car_fields } = this.state;
                                            if (text.length == 0) {
                                                exchange_car_fields[0].data.reg_no = text
                                                this.setState({ exchange_car_fields, regButtonEnabled: true });
                                            } else if (regPattern.test(text)) {
                                                exchange_car_fields[0].data.reg_no = text
                                                this.setState({ exchange_car_fields, regButtonEnabled: true });
                                            } else {
                                                this.setState({ regButtonEnabled: true })
                                                this.showAlert("Please enter valid registration no.")
                                            }
                                        }}
                                    />
                                </View>
                                <TouchableOpacity
                                    disabled={!this.state.regButtonEnabled}
                                    style={this.state.regButtonEnabled ? childStyles.reg_button_enabled : childStyles.reg_button_disabled}
                                    onPress={() => { this.getRegNoDetails(data.reg_no) }}>
                                    <Text style={{ textAlign: 'center', color: '#fff' }}>Fetch Details</Text>
                                </TouchableOpacity>
                            </View>

                            <ItemPicker title={brandName} onClick={this.showPicker.bind(this, { title: 'Vehicle Model', fromId: 'vehicle_brand', listItems: this.getAllVehicleBrandItems() }, true)} />
                            <ItemPicker disabled={vehicleBrand ? false : true} title={modelName}
                                onClick={this.showPicker.bind(this, { title: 'Select model', fromId: 'vehicle_model', listItems: this.getAllVehicleModelsItems(vehicleBrand) }, true)} />

                            <TextInput
                                style={{ padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}
                                //editable={vehicleModel ? true : false}
                                placeholder='Variant *'
                                value={variant}
                                onChangeText={(text) => {
                                    let { exchange_car_fields } = this.state;
                                    exchange_car_fields[0].data.vehicleVariant = text
                                    this.setState(exchange_car_fields);
                                }}
                            />

                            {/* <ItemPicker disabled={vehicleModel ? false : true} title={variant}
                                onClick={this.showPicker.bind(this, { title: 'Select Variant', fromId: 'vehicle_variant', listItems: this.getAllVehicleVariants(vehicleModel) }, true)} /> */}

                            <TextInput
                                style={{ padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}
                                //editable={vehicleModel ? true : false}
                                placeholder='Color/Description *'
                                value={color}
                                onChangeText={(text) => {
                                    let { exchange_car_fields } = this.state;
                                    exchange_car_fields[0].data.vehicleColor = text
                                    this.setState(exchange_car_fields);
                                }}
                            />

                            <TextInput
                                style={{ padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}
                                //editable={vehicleModel ? true : false}
                                placeholder='Fuel Type *'
                                value={fuelType}
                                onChangeText={(text) => {
                                    let { exchange_car_fields } = this.state;
                                    exchange_car_fields[0].data.vehicleFuelType = text
                                    this.setState(exchange_car_fields);
                                }}
                            />

                            {/* <TextInput
                                style={{ padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}
                                //editable={vehicleModel ? true : false}
                                placeholder='Year'
                                keyboardType='numeric'
                                value={year}
                                onChangeText={(text) => {
                                    let { exchange_car_fields } = this.state;
                                    exchange_car_fields[0].data.year = text
                                    this.setState(exchange_car_fields);
                                }}
                            /> */}


                            <ItemPicker title={year}
                                onClick={this.showPicker.bind(this, { title: 'Select Year', fromId: 'year_type', listItems: this.getYearsList() }, false)} />

                            <TextInput
                                style={{ padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}
                                //editable={vehicleModel ? true : false}
                                placeholder='KMs Run *'
                                keyboardType='numeric'
                                value={kmRun}
                                onChangeText={(text) => {
                                    let { exchange_car_fields } = this.state;
                                    exchange_car_fields[0].data.kmRun = text
                                    this.setState(exchange_car_fields);
                                }}
                            />
                            {/* <TextInput
                                style={{ padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}
                                //editable={vehicleModel ? true : false}
                                placeholder='Owner Type'
                                value={ownerType}
                                onChangeText={(text) => {
                                    let { exchange_car_fields } = this.state;
                                    exchange_car_fields[0].data.ownerType = text
                                    this.setState(exchange_car_fields);
                                }}
                            /> */}

                            <ItemPicker title={ownerType}
                                onClick={this.showPicker.bind(this, { title: 'Owner Type', fromId: 'owner_type', listItems: ownerTypeList }, false)} />


                            <TextInput
                                style={(data.vin_no !== undefined && data.vin_no.includes('*')) ? { padding: 10, flex: 1, borderColor: '#FF0000', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }:
                                    { padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}
                                //editable={vehicleModel ? true : false}
                                placeholder='Used Car Vin Num'
                                value={vinNo}
                                maxLength={17}
                                autoCapitalize="characters"
                                onChangeText={(text) => {
                                    let pattern = /^[a-z0-9*]+$/i
                                    let { exchange_car_fields } = this.state;

                                    if (text.length == 0) {
                                        exchange_car_fields[0].data.vin_no = text
                                        this.setState(exchange_car_fields);
                                    } else if (pattern.test(text)) {
                                        exchange_car_fields[0].data.vin_no = text
                                        this.setState(exchange_car_fields);
                                    } else {
                                        this.showAlert("Please enter valid vin no.")
                                    }
                                    
                                }}
                            />
                        </View>
                    )
                }
                break;
            case 'evaluator_request':
                let evaluationDate = "Evaluation Date *";
                let evaluatorName = "Select Evaluator";
                let { evaluatorsList } = this.state;
                let { evaluator } = this.state.exchange_car_fields[1].data;
                let { remarks } = this.state.exchange_car_fields[1].data;
                let maxDate = new Date();
                maxDate.setDate(maxDate.getDate() + 10);
                //console.log("evaluatorsL" + JSON.stringify(this.state.evaluatorsList));

                if (item.data.evaluation_date) {
                    evaluationDate = Utils.getReadableDateTime(item.data.evaluation_date)
                }

                if (evaluator) {
                    evaluatorName = evaluator.text;
                }

                if (item.isExpanded) {
                    return (
                        <View style={[{ display: (item.isExpanded ? 'flex' : 'none') }, styles.field]}>
                            <ItemPicker title={evaluatorName}
                                onClick={this.showPicker.bind(this, { title: 'Select Evaluator', fromId: 'evaluator_request_name', listItems: this.getEvaluatorsList(evaluatorsList) }, false)} />

                            <ItemPicker
                                //disabled={item.data.salesConsultant && item.data.activity ? false : true}
                                title={evaluationDate}
                                onClick={() => {
                                    this.setState({
                                        dateTimePickerPayload: { from: 'evaluator_request_date' },
                                        dateTimePickerVisibility: true, datePickerMode: 'datetime',
                                        dateTimePickerMinimumDate: new Date(),
                                        dateTimePickerMaximumDate: maxDate
                                    }
                                    )
                                }}
                            />

                            <Text
                                style={{ padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}>
                                Old Car Evaluation Request
                                </Text>


                            <TextInput
                                style={{ padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}
                                placeholder='Remarks *'
                                value={remarks}
                                onChangeText={(text) => {
                                    let { exchange_car_fields } = this.state;
                                    exchange_car_fields[1].data.remarks = text
                                    this.setState(exchange_car_fields);
                                }}
                            />
                        </View>
                    )
                }
                break;
            case 'inspection':

                //let { car_images_array } = this.state;

                //console.log("car_images_array", this.state.car_images_array)

                let { is_under_insurance } = this.state;
                let underInsurance = "Select";
                //console.log("is_under_insurance", is_under_insurance)
                if (is_under_insurance) {
                    underInsurance = Utils.getReadableDate(new Date(is_under_insurance));
                }

                let maxDateInsurance = new Date();
                maxDateInsurance.setFullYear(maxDateInsurance.getFullYear() + 5);

                let minDateInsurance = new Date();
                minDateInsurance.setFullYear(minDateInsurance.getFullYear() - 2);

                if (item.isExpanded) {
                    return (
                        <View style={[{ display: (item.isExpanded ? 'flex' : 'none') }, styles.field]}>

                            <View style={{ flexDirection: 'row', padding: 10 }}>
                                <Text style={{ flex: 1, fontSize: 16 }}>Doc Verified</Text>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <TouchableOpacity style={{ marginLeft: 10 }} onPress={() => this.isDocVerified(true)}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{
                                                height: 20,
                                                width: 20,
                                                borderRadius: 10,
                                                borderWidth: 2,
                                                alignItems: 'center',
                                                borderColor: (this.state.is_doc_verified) ? '#00a7f7' : '#000',
                                                justifyContent: 'center',
                                            }}>
                                                {(this.state.is_doc_verified) ?
                                                    <View style={{
                                                        height: 10,
                                                        width: 10,
                                                        borderRadius: 5,
                                                        backgroundColor: '#00a7f7',
                                                    }} />
                                                    : null
                                                }
                                            </View>
                                            <Text style={{ marginLeft: 10, fontSize: 14 }}>Yes</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ marginLeft: 10 }} onPress={() => this.isDocVerified(false)} >
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{
                                                height: 20,
                                                width: 20,
                                                borderRadius: 10,
                                                borderWidth: 2,
                                                borderColor: (this.state.is_doc_verified) ? '#000' : '#00a7f7',
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                            }}>
                                                {(!this.state.is_doc_verified) ?
                                                    <View style={{
                                                        height: 10,
                                                        width: 10,
                                                        borderRadius: 5,
                                                        backgroundColor: '#00a7f7',
                                                    }} />
                                                    : null
                                                }
                                            </View>
                                            <Text style={{ marginLeft: 10, fontSize: 14 }}>No</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', padding: 10 }}>
                                <Text style={{ flex: 1, fontSize: 16, marginTop: 15 }}>Insurance Upto</Text>
                                <View style={{ flex: 1.5, marginTop: -10 }}>
                                    <ItemPicker
                                        //disabled={item.data.salesConsultant && item.data.activity ? false : true}
                                        title={underInsurance}
                                        onClick={() => {
                                            this.setState({
                                                dateTimePickerPayload: { from: 'insurance_year' },
                                                dateTimePickerVisibility: true,
                                                dateTimePickerMinimumDate: minDateInsurance,
                                                dateTimePickerMaximumDate: maxDateInsurance
                                            }
                                            )
                                        }}
                                    />
                                </View>
                            </View>

                            {/* <ItemPicker title={underInsurance}
                                onClick={this.showPicker.bind(this, { title: 'Insurance Upto', fromId: 'insurance_year', listItems: this.getInsuranceYear() }, false)} /> */}
                            {/* <View
                                style={styles.line}
                            /> */}
                            <View style={{ flexDirection: 'column', padding: 10 }}>
                                <Text style={{ flex: 1, fontSize: 16 }}>Vehicle Condition</Text>
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <TouchableOpacity style={{ flex: 1, padding: 10 }} onPress={() => this.vehicleCondition('Good')}>
                                        <View style={{
                                            height: 30,
                                            borderRadius: 25,
                                            borderWidth: 2,
                                            borderColor: '#76c328',
                                            backgroundColor: (this.state.vehicle_condition == 'Good') ? '#76c328' : 'transparent',
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>

                                            <Text style={{ color: (this.state.vehicle_condition == 'Good') ? '#fff' : '#76c328' }}>Good</Text>

                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ flex: 1, padding: 10 }} onPress={() => this.vehicleCondition('OK')}>
                                        <View style={{
                                            height: 30,
                                            borderRadius: 25,
                                            borderWidth: 2,
                                            borderColor: '#FFCA28',
                                            backgroundColor: (this.state.vehicle_condition == 'OK') ? '#FFCA28' : 'transparent',
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>

                                            <Text style={{ color: (this.state.vehicle_condition == 'OK') ? '#fff' : '#FFCA28' }}>OK</Text>

                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ flex: 1, padding: 10 }} onPress={() => this.vehicleCondition('Bad')}>
                                        <View style={{
                                            height: 30,
                                            borderRadius: 25,
                                            borderColor: '#F44336',
                                            borderWidth: 2,
                                            backgroundColor: (this.state.vehicle_condition == 'Bad') ? '#F44336' : 'transparent',
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>

                                            <Text style={{ color: (this.state.vehicle_condition == 'Bad') ? '#fff' : '#F44336' }}>Bad</Text>

                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View
                                style={styles.line}
                            />
                            <View style={{ flexDirection: 'column', padding: 10 }}>
                                <Text style={{ flex: 1, fontSize: 16 }}>Photos</Text>
                                {/* {console.log("car_images_arrayyyyy", this.state.car_images_array.length)} */}
                                <View style={{ flexDirection: 'column', marginTop: 10 }}>
                                    <ScrollView horizontal={true}>
                                        <TouchableOpacity onPress={() => this.addCarPhoto()}>
                                            <Image source={require('../../../images/no_profile_img.png')} style={{ alignSelf: 'center', width: 50, height: 50, marginTop: 1 }} />
                                        </TouchableOpacity>
                                        {(this.state.car_images_array && this.state.car_images_array.length > 0) ?
                                            this.state.car_images_array.map((item, index) => {
                                                // { console.log("car_images_arrayyyyy", item) }
                                                return (
                                                    <TouchableOpacity style={{ paddingLeft: 3, paddingRight: 3 }}
                                                        onPress={() => this.openImage(index)}
                                                        onLongPress={() => this.deleteImage(index)}
                                                        key={index} >
                                                        <View style={imageStyle.container}>
                                                            {(item.includes('http')) ?
                                                                <Image source={{ uri: item }} style={{ alignSelf: 'center', width: 50, height: 50 }} />
                                                                : <Image source={{ uri: 'data:image/jpeg;base64,' + item }} style={{ alignSelf: 'center', width: 50, height: 50 }} />}
                                                        </View>
                                                    </TouchableOpacity>
                                                )
                                            })

                                            : null
                                        }
                                        {/* <Image source={this.state.car_images_array[0]} style={{ alignSelf: 'center', width: 50, height: 50, }} /> */}
                                    </ScrollView>
                                </View>
                            </View>
                        </View>
                    )
                }
                break;
            case 'price':
                let expectedPrice = ''
                let quotePrice = ''
                let { expected_price } = this.state.exchange_car_fields[3].data
                let { quoted_price } = this.state.exchange_car_fields[3].data
                if (expected_price) {
                    expectedPrice = expected_price;
                }
                if (quoted_price) {
                    quotePrice = quoted_price;
                }


                if (item.isExpanded) {
                    return (
                        <View style={[{ display: (item.isExpanded ? 'flex' : 'none') }, styles.exchPricefield]}>
                            <TextInput
                                style={{ padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, marginLeft: 10, marginRight: 10, color: '#303030' }}
                                placeholder='Expected price'
                                keyboardType='numeric'
                                value={expectedPrice}
                                onChangeText={(text) => {
                                    let { exchange_car_fields } = this.state;
                                    exchange_car_fields[3].data.expected_price = text
                                    this.setState(exchange_car_fields);
                                }}
                            />
                            {/* <View
                                style={[styles.line, { marginTop: 10 }]}
                            />
                            <View style={{ flexDirection: 'row', marginTop: 10, marginLeft: 10, marginRight: 10 }}>
                                <Text style={{ flex: 1, fontSize: 18 }}>Car Dekho Price</Text>
                                <Text style={{ flex: 1, fontSize: 18, textAlign: 'center' }}>430000</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 10, marginBottom: 5, marginLeft: 10, marginRight: 10 }}>
                                <Text style={{ flex: 1, fontSize: 18 }}>Cars24 Price</Text>
                                <Text style={{ flex: 1, fontSize: 18, textAlign: 'center' }}>410000</Text>
                            </View> */}
                            {/* <View
                                style={styles.line}
                            /> */}
                            <TextInput
                                style={{ padding: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, marginLeft: 10, marginRight: 10, color: '#303030' }}
                                placeholder='Quote price'
                                keyboardType='numeric'
                                value={quotePrice}
                                onChangeText={(text) => {
                                    let { exchange_car_fields } = this.state;
                                    exchange_car_fields[3].data.quoted_price = text
                                    this.setState(exchange_car_fields);
                                }}
                            />
                        </View>
                    )
                }

                break;
        }
    }

    onCancel = () => {
        this.setState({
            showItemPicker: false
        });
    }

    onSelect = (fromId, item) => {
        var exchange_car_fields = this.state.exchange_car_fields;
        //console.log("apunka.. ", item);

        if (fromId == 'vehicle_brand') {
            //console.log('Payload', item);
            //console.log(payload);
            exchange_car_fields[0].data.vehicleBrand = item;
            exchange_car_fields[0].data.vehicleModel = null;
        }

        if (fromId == 'vehicle_model') {
            //console.log('Payload', item);
            //console.log(payload);
            exchange_car_fields[0].data.vehicleModel = item;
        }

        if (fromId == 'vehicle_variant') {
            //console.log('Payload', item);
            //console.log(payload);
            exchange_car_fields[0].data.vehicleVariant = item;
        }

        if (fromId == 'owner_type') {
            //console.log('Payload', item);
            //console.log(payload);
            exchange_car_fields[0].data.ownerType = item;
        }

        if (fromId == 'year_type') {
            exchange_car_fields[0].data.year = item
        }

        if (fromId == 'evaluator_request_name') {
            exchange_car_fields[1].data.evaluator = item;
            exchange_car_fields[1].data.activity_id = 10;
        }


        this.setState({
            exchange_car_fields,
            showItemPicker: false
        })
    }

    onDateSelect(date, payload) {
        let { exchange_car_fields } = this.state;
        let { is_under_insurance } = this.state;
        if (payload && payload.from == 'evaluator_request_date') {
            exchange_car_fields[1].data.evaluation_date = date;
        }
        if (payload && payload.from == 'insurance_year') {
            exchange_car_fields[2].data.is_under_insurance = date;
            is_under_insurance = Utils.getReadableDate(date);
        }
        this.setState({
            is_under_insurance,
            exchange_car_fields,
            dateTimePickerVisibility: false,
            datePickerMode: 'date',
            dateTimePickerMinimumDate: undefined,
            dateTimePickerMaximumDate: undefined
        });
    }

    closeImage = () => {
        console.log("openImageGallery will close")
        let { openImageGallery } = this.state;
        this.setState({ openImageGallery: !openImageGallery })
    }

    render() {
        const { navigation } = this.props;
        const { exchange_car_fields } = this.state;
        let { showItemPicker, dataItemPicker } = this.state;
        const isAllMandatoryFieldsDone = this.isVehicleDetailValid() && this.isEvaluatorRequestValid();

        if (this.state.openImageGallery) {
            let { exchange_car_fields } = this.state;
            let { selectedIndex } = this.state;
            let images = exchange_car_fields[2].data.car_images_array;
            return (
                <ViewImageScreen
                    data={images}
                    selectedIndex={selectedIndex}
                    closeImageScreen={this.closeImage}
                />
            )
        }

        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled={Platform.OS === 'ios'}>
                <View style={{ flex: 1 }}>
                    <View style={styles.container}>
                        <View style={styles.main}>
                            <Toast
                                ref="alert"
                                style={{ backgroundColor: 'red' }}
                                position='top'
                                fadeInDuration={1000}
                                fadeOutDuration={1000}
                                opacity={0.8}
                                textStyle={{ color: 'white' }}
                            />
                            <PickerList
                                showFilter={this.state.showItemPickerFilter}
                                visible={showItemPicker}
                                onSelect={this.onSelect}
                                onCancel={this.onCancel}
                                data={dataItemPicker}
                            />

                            <CustomDateTimePicker
                                minimumDate={this.state.dateTimePickerMinimumDate}
                                maximumDate={this.state.dateTimePickerMaximumDate}
                                dateTimePickerVisibility={this.state.dateTimePickerVisibility}
                                datePickerMode={this.state.datePickerMode}
                                payload={this.state.dateTimePickerPayload}
                                handleDateTimePicked={(date, payload) => {

                                    this.onDateSelect(date, payload);
                                }}
                                hideDateTimePicked={() => {
                                    this.setState({
                                        dateTimePickerVisibility: false,
                                        datePickerMode: 'date',
                                        dateTimePickerMinimumDate: undefined,
                                        dateTimePickerMaximumDate: undefined
                                    })
                                }}
                            />

                            <ScrollView>
                                {
                                    exchange_car_fields.map((item, index) => {
                                        let marginTop = 0;
                                        if (index != 0) {
                                            marginTop = 1;
                                        }

                                        return (
                                            <View key={index}>
                                                <CreateLeadExpander item={item} showSuccessMark={this.getSuccessMark(item)} key={index} style={{ marginTop }} onClick={this.expandField.bind(this, index)} />
                                                <View>
                                                    {this.getViews(item, index)}
                                                </View>
                                            </View>
                                        )
                                    })
                                }
                            </ScrollView>
                        </View>
                        <TouchableOpacity
                            disabled={!isAllMandatoryFieldsDone}
                            //this.props.navigation.navigate('ExchangeCar')
                            onPress={() => (this.props.navigation != null) ? this.createEnquiry() : this.addExchangeCar()}
                        >
                            <View style={[styles.footer, isAllMandatoryFieldsDone ? styles.footerValid : styles.footerInvalid]}>
                                <Text style={styles.footerText}> {(this.props.navigation != null) ? "Create Enquiry" : "Add Exchange Vehicle"}</Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                    {this.state.loading && <View style={styles.loading}>
                        <ActivityIndicator size="large" color="#007fff" />
                    </View>}
                </View>
            </KeyboardAvoidingView>
        );
    }

}
