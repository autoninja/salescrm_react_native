package com.salescrm.telephony.offline;

import android.content.Context;

import com.google.gson.Gson;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.booking_allocation.BookingAllocationResult;
import com.salescrm.telephony.db.booking_allocation.VinAlllocationStatus;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.model.BookingAllocationModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FetchBookingAllocationData implements Callback<BookingAllocationModel> {

    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;

    public FetchBookingAllocationData(OfflineSupportListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }

    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).fetchBookingAllocationData(this);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.BOOKING_ALLOCATION);

        }
    }

    @Override
    public void success(final BookingAllocationModel data, Response response) {
        Util.updateHeaders(response.getHeaders());

        if (data.getResult() != null) {
            System.out.println("Success:1" + data.getMessage());
            System.out.println("BookingAllocation: " + new Gson().toJson(data.getResult()));
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    insertData(realm, data);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    listener.onBookingAllocationDataFetched(data);
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.BOOKING_ALLOCATION);
                }
            });


        } else {
            // relLoader.setVisibility(View.GONE);
            System.out.println("Success:2" + data.getMessage());
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.BOOKING_ALLOCATION);
            //showAlert(validateappConfResponse.getMessage());
        }

    }

    private void insertData(Realm realm, BookingAllocationModel data) {
            realm.delete(BookingAllocationResult.class);
            realm.delete(VinAlllocationStatus.class);
            BookingAllocationResult bookingAllocationResult = new BookingAllocationResult();
            for(int i = 0; i<data.getResult().getVin_allocation_statuses().size(); i++){
                VinAlllocationStatus vinAlllocationStatus = new VinAlllocationStatus();
                vinAlllocationStatus.setId(data.getResult().getVin_allocation_statuses().get(i).getId());
                vinAlllocationStatus.setName(data.getResult().getVin_allocation_statuses().get(i).getName());
                vinAlllocationStatus.getVinAllocationRoleIds().addAll(data.getResult().getVin_allocation_statuses().get(i).getRoleId());
                bookingAllocationResult.getVinAlllocationStatuses().add(vinAlllocationStatus);
            }
            realm.copyToRealm(bookingAllocationResult);

    }

    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.BOOKING_ALLOCATION);
    }
}
