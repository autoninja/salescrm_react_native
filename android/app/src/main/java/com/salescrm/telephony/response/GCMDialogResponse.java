package com.salescrm.telephony.response;

/**
 * Created by bannhi on 20/4/17.
 */

public class GCMDialogResponse {

    String dialog;

    public String getDialog() {
        return dialog;
    }

    public void setDialog(String dialog) {
        this.dialog = dialog;
    }
}
