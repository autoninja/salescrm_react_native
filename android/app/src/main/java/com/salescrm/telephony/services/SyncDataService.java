package com.salescrm.telephony.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.AddAndExchangeCarSyncDB;
import com.salescrm.telephony.db.AllotDseDB;
import com.salescrm.telephony.db.FormAnswerDB;
import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;
import com.salescrm.telephony.interfaces.SyncListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.sync.SyncAllotDse;
import com.salescrm.telephony.sync.SyncC360Data;
import com.salescrm.telephony.sync.SyncCar;
import com.salescrm.telephony.sync.SyncCreateLead;
import com.salescrm.telephony.sync.SyncFormData;
import com.salescrm.telephony.utils.NotificationsUI;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.RetrofitError;

public class SyncDataService extends Service implements SyncListener{

    private Realm realm;
    private static final String TAG ="SyncService:" ;
    private Preferences pref = null;

    public SyncDataService() {
    }


    @Override
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26) {
            int NOTIFICATION_ID = NotificationsUI.BACKGROUND_ID;
            startForeground(NOTIFICATION_ID, new Notification.Builder(this, NotificationsUI.CHANNEL_ID_BACKGROUND).build());

        }
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        if(new ConnectionDetectorService(this).isConnectingToInternet()){
            if(pref.isSyncEnabled()){
                startSync();
            }
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(networkChangeReceiver, intentFilter);
    }

    BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            pref.load(getApplicationContext());
           if(intent.getAction().equalsIgnoreCase("android.net.conn.CONNECTIVITY_CHANGE")) {
               if(new ConnectionDetectorService(context).isConnectingToInternet()){
                   System.out.println(TAG+"Connected" + intent.getAction());
                   if(pref.isSyncEnabled()){
                       startSync();
                   }
               }
               else {
                   System.out.println(TAG+"Not Connected" + intent.getAction());
               }

            }

        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }





    private void startSync() {

        /*
        * if(realm.where(AllotDseDB.class).equalTo("is_synced",false).findAll().size()>0) {
            syncAllotDse();
        }
        else  if(realm.where(CreateLeadInputDataDB.class).equalTo("is_synced",false).findAll().size()>0) {
            syncCreateLead();
        }
        else  if(realm.where(FormAnswerDB.class).equalTo("is_synced",false).findAll().size()>0) {
            syncForm();
        }
        else if(realm.where(AddAndExchangeCarSyncDB.class).equalTo("is_synced",false).findAll().size()>0){
            syncCar();
        }*/


        /*if(realm.where(CreateLeadInputDataDB.class).equalTo("is_synced",false).findAll().size()>0) {
            syncCreateLead();
        }
        else if(realm.where(AddAndExchangeCarSyncDB.class).equalTo("is_synced",false).findAll().size()>0){
            syncCar();
        }
        else {
            stopSelfService();
        }*/

        stopSelfService();

        //syncCustomer360();


    }

    /*private void syncCustomer360(){
        new SyncC360Data(getApplicationContext()).syncBuyerTypeDetails();
        new SyncC360Data(getApplicationContext()).syncCrmSourceDetails();
        new SyncC360Data(getApplicationContext()).syncCustomerDetails();
        new SyncC360Data(getApplicationContext()).syncMobileNumbers();
        new SyncC360Data(getApplicationContext()).syncEmailIds();
    }*/
    private void syncAllotDse() {
        new SyncAllotDse(this,getApplicationContext()).sync();
    }

    private void syncCreateLead() {
        new SyncCreateLead(this,getApplicationContext()).sync();
    }

    private void syncForm() {
        new SyncFormData(this,getApplicationContext()).sync();
    }

    private void syncCar() {
        new SyncCar(this,getApplicationContext()).sync();
    }



    @Override
    public void onAllotDseSync() {
        System.out.println("Allot dse sync");
        syncCreateLead();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkChangeReceiver);
        realm.close();
    }

    @Override
    public void onOfflineSupportApiError(RetrofitError error, int fromId) {

    }

    @Override
    public void onCreateLeadSync() {
        syncCar();
    }

    @Override
    public void onFormSync() {

    }
    private void stopSelfService() {
        System.out.println("Service Stopped");
        try {
            this.stopSelf();
        }
        catch (Exception e) {

        }
    }
    @Override
    public void onCarSync(){
       stopSelfService();
    }


}
