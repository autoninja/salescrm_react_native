import { StyleSheet, Platform } from 'react-native';
export default StyleSheet.create({

    MainContainer: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: (Platform.OS) === 'ios' ? 20 : 10,
    },
    Header: {
        flexDirection: 'row',
        height: 48,
        justifyContent: 'space-between',
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 6

    },
    HeaderTitle: {
        color: "#626262",
        fontSize: 20,
        fontWeight: 'bold',
    },
    Image: {
        height: 20,
        width: 20
    },
    Content: {
        flex: 1,
        flexDirection: 'row',
    },
    ContentLeft: {
        flex: 1,
        backgroundColor: '#ebebeb'
    },
    ContentRight: {
        flex: 1,
    },
    Footer: {
        flexDirection: 'row',
        justifyContent: 'center',
        height: 56
    },
    FooterItem: {
        flex: 1,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
    },
    loader: {
        height: 36,
        width: 36,
        alignSelf: 'center'

    }

});
