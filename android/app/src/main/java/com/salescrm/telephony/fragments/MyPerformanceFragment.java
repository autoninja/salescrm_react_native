package com.salescrm.telephony.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.ScoreboardDsePagerAdapter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.preferences.Preferences;

/**
 * Created by prateek on 22/8/17.
 */

public class MyPerformanceFragment extends Fragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ScoreboardDsePagerAdapter scoreboardDsePagerAdapter;
    private Preferences preferences;
    private int prevTab = -1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.team_dashboard, container, false);
        preferences = Preferences.getInstance();
        preferences.load(getActivity());
        viewPager = (ViewPager) rootView.findViewById(R.id.dashboardviewpager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout_team_dashboard);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_MY_PERFORMANCE);
        tabLayout.addTab(tabLayout.newTab().setText("Scoreboard"));
        tabLayout.setVisibility(View.GONE);
        scoreboardDsePagerAdapter = new ScoreboardDsePagerAdapter(getChildFragmentManager(), 1);
        viewPager.setAdapter(scoreboardDsePagerAdapter);
        populateViewpager();
        return rootView;
    }

    private void populateViewpager() {
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                changeTabTextStyle(tab.getPosition());
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    public void changeTabTextStyle(int pos) {
        if (prevTab == -1) {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            prevTab = 0;

        } else {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            //((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(prevTab)))).getChildAt(1))).setTypeface(null, Typeface.NORMAL);
            prevTab=pos;
        }
    }
}
