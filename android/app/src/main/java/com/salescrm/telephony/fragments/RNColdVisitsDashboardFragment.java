package com.salescrm.telephony.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.EnqSourceCategoryDB;
import com.salescrm.telephony.db.EnqSubInnerSourceDB;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.events.EventsCategoryDb;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.TeamDashboardSwipedListener;
import com.salescrm.telephony.model.NotificationModel;
import com.salescrm.telephony.model.TeamDashboardSwiped;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.WSConstants;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class RNColdVisitsDashboardFragment extends Fragment implements DefaultHardwareBackBtnHandler {
    private ReactRootView mReactRootView;
    private ReactInstanceManager mReactInstanceManager = null;
    private Preferences pref;
    private Realm realm;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedLayout = inflater.inflate(R.layout.fragment_rn_lost_drop_main, container, false);
        mReactRootView = inflatedLayout.findViewById(R.id.react_root_view_lost_drop_main);
        return inflatedLayout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        pref = Preferences.getInstance();
        pref.load(context);
        realm = Realm.getDefaultInstance();
        //   mReactRootView = new ReactRootView(getContext());
        if (getActivity() != null) {
            mReactInstanceManager = ((SalesCRMApplication) getActivity().getApplication())
                    .getReactNativeHost()
                    .getReactInstanceManager();
        }


    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ;
        Bundle initialProps = new Bundle();
        initialProps.putBoolean("fromAndroid", true);
        initialProps.putString("token", pref.getAccessToken());
        initialProps.putInt("appUserId", pref.getAppUserId());
        initialProps.putString("module", "COLD_VISIT_DASHBOARD");
        initialProps.putBoolean("isUserBH", DbUtils.isUserBranchHead());
        initialProps.putBoolean("isUserOps", DbUtils.isUserOperations());
        initialProps.putBoolean("isUserSM", DbUtils.isUserSalesManager());
        initialProps.putBoolean(("isUserTL"), DbUtils.isUserTeamLead());
        initialProps.putBoolean(("isUserSC"), DbUtils.isUserSalesConsultant());
        initialProps.putBoolean("uniqueAcrossDealerShip",DbUtils.isAppConfEnabled(WSConstants.AppConfig.UNIQUE_ACROSS_DEALERSHIP));
        initialProps.putBoolean("addLeadEnabled",DbUtils.isAddLeadEnabled());
        initialProps.putBoolean("coldVisitEnabled",DbUtils.isAppConfEnabled(WSConstants.AppConfig.COLD_VISIT_ENABLED));
        initialProps.putBoolean("showLeadCompetitor",DbUtils.isAppConfEnabled(WSConstants.AppConfig.SHOW_LEAD_COMPETITOR));
        if (mReactInstanceManager != null && mReactRootView != null) {
            mReactRootView.startReactApplication(
                    mReactInstanceManager,
                    "NinjaCRMSales",
                    initialProps
            );
        }
        getActivity().getIntent().putExtra(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY, false);

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }





    class Item {
        String id;
        String name;

        public Item(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}