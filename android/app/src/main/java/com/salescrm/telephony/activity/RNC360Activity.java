package com.salescrm.telephony.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.SearchResponse;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.BalloonAnimation;


public class RNC360Activity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {
    private FloatingActionButton fabC360;
    private Preferences pref;
    private long leadId;
    private boolean fromNotificationTray;
    private int tabPosition = 0;
    private SearchResponse.Result searchResponse;
    private ReactRootView mReactRootView;
    private ReactInstanceManager mReactInstanceManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_c360_rn);
        pref = Preferences.getInstance();
        pref.load(this);

        fabC360 = (FloatingActionButton) findViewById(R.id.card_float);
        mReactRootView = findViewById(R.id.react_root_view_360);

        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            searchResponse = (SearchResponse.Result) extras.getSerializable("online_search_data");
            fromNotificationTray = extras.getBoolean(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY,false);
            tabPosition = extras.getInt(WSConstants.C360_TAB_POSITION,0);
            if(fromNotificationTray){
                pref.setLeadID(extras.getString(WSConstants.LEAD_ID));
            }
        }

        if (searchResponse == null) {
            leadId = Util.getLong(pref.getLeadID());
        } else {
            leadId = Util.getLong(searchResponse.getLeadId());
        }

        Bundle initialProps = new Bundle();
        initialProps.putString("leadId", leadId+"");
        initialProps.putString("token", pref.getAccessToken());
        initialProps.putString("module", "C360ACTIVITY");
        mReactRootView.startReactApplication(mReactInstanceManager, "NinjaCRMSales", initialProps);

        fabC360.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                /*if(!isFinishing()) {
                    fabFragmentAdded = true;
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.rel_bottom_frame, new FabFragment()).commitAllowingStateLoss();
                    fabC360.hide();
                }*/

            }

        });




    }


    @Override
    public void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
        SalesCRMApplication.getBus().register(this);
        if(pref.isReloadC360()) {
            pref.setReloadC360(false);
            /*Intent intent = getIntent();
            intent.putExtra(WSConstants.C360_TAB_POSITION,1);
            finish();
            overridePendingTransition(0,0);
            startActivity(intent);*/
            return;
        }

        if(pref.isShowBalloonAnimation()) {
            ((BalloonAnimation) findViewById(R.id.view_balloon_animation)).start();
            pref.setShowBalloonAnimation(false);
        }


        //  callBookingsRetro();

        // getAllDetailsForUser();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    protected void onStop() {
        System.out.println("On Stop Triggered");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        pref.setC360Destroyed(true);


    }

    @Override
    public void onBackPressed() {
        finish();
        if(fromNotificationTray){
            startActivity(new Intent(RNC360Activity.this,HomeActivity.class));
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }
}
