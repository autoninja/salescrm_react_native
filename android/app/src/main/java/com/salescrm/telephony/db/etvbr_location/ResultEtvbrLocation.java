package com.salescrm.telephony.db.etvbr_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 8/11/17.
 */

public class ResultEtvbrLocation extends RealmObject {

    @PrimaryKey
    @SerializedName("role_id")
    @Expose
    private Integer role_id;
    @SerializedName("total_abs")
    @Expose
    private TotalAbs total_abs;
    @SerializedName("total_rel")
    @Expose
    private TotalRel total_rel;
    @SerializedName("total_targets")
    @Expose
    private TotalTargets total_targets;
    @SerializedName("locations")
    @Expose
    private RealmList<Location> locations = null;

    public Integer getRoleId() {
        return role_id;
    }

    public void setRoleId(Integer roleId) {
        this.role_id = roleId;
    }

    public TotalAbs getTotalAbs() {
        return total_abs;
    }

    public void setTotalAbs(TotalAbs totalAbs) {
        this.total_abs = totalAbs;
    }

    public TotalRel getTotalRel() {
        return total_rel;
    }

    public void setTotalRel(TotalRel totalRel) {
        this.total_rel = totalRel;
    }

    public TotalTargets getTotalTargets() {
        return total_targets;
    }

    public void setTotalTargets(TotalTargets totalTargets) {
        this.total_targets = totalTargets;
    }

    public RealmList<Location> getLocations() {
        return locations;
    }

    public void setLocations(RealmList<Location> locations) {
        this.locations = locations;
    }
}
