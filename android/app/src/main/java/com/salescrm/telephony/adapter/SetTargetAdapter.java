package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.salescrm.telephony.R;
import com.salescrm.telephony.db.TargetsDB;
import com.salescrm.telephony.db.team_dashboard_gm_db.Absolute;
import com.salescrm.telephony.model.TargetSectionModel;
import com.salescrm.telephony.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import io.realm.RealmList;

/**
 * Created by bannhi on 11/8/17.
 */

public class SetTargetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //boolean setClicked = false;
    RealmList<Absolute> allTargetsList = new RealmList<>();
    Activity activity;
    ArrayList<TargetSectionModel> targetSectionModelArrayList= new ArrayList<>();
    TargetsDB individualTarget;
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.set_target_row, null);
        return new SetTargetAdapter.TargetItemHolder(view);
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RecyclerView.ViewHolder iViewHolder = holder;
      /*  if(holder instanceof TargetItemHolder && allTargetsList!=null && allTargetsList.get(position)!=null &&
                allTargetsList.get(position).isValid()){*/
        if (targetSectionModelArrayList != null) {
            for (TargetSectionModel sectionModel : targetSectionModelArrayList) {
                if (position == sectionModel.getSectionPostion()) {
                    ((TargetItemHolder) iViewHolder).teamLeadName.setText(sectionModel.getTeamLeadName());
                    ((TargetItemHolder) iViewHolder).teamLeadName.setVisibility(View.VISIBLE);
                    break;
                } else {
                    ((TargetItemHolder) iViewHolder).teamLeadName.setVisibility(View.GONE);
                }
            }

        }
            String url = allTargetsList.get(position).getDpUrl();
        ((TargetItemHolder) iViewHolder).text_user.setText("" + allTargetsList.get(position).getUserName().toUpperCase().substring(0, 1));
        ((TargetItemHolder) iViewHolder).text_user.setVisibility(View.VISIBLE);
        ((TargetItemHolder) iViewHolder).imgUser.setVisibility(View.GONE);


        if(!url.isEmpty()){
                Glide.with(activity).
                        load(url)
                        .asBitmap()
                        .centerCrop()
                        .listener(new RequestListener<String, Bitmap>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                ((TargetItemHolder) iViewHolder).imgUser.setVisibility(View.GONE);
                                ((TargetItemHolder) iViewHolder).text_user.setVisibility(View.VISIBLE);

                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                ( (TargetItemHolder) iViewHolder).imgUser.setVisibility(View.VISIBLE);
                                ( (TargetItemHolder) iViewHolder).text_user.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(new BitmapImageViewTarget(((TargetItemHolder) iViewHolder).imgUser) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                if(resource!=null && activity!=null) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    ((TargetItemHolder) iViewHolder).imgUser.setImageDrawable(circularBitmapDrawable);
                                }
                            }
                        });



            }else {
                ((TargetItemHolder) iViewHolder).imgUser.setVisibility(View.GONE);
                ((TargetItemHolder) iViewHolder).text_user.setVisibility(View.VISIBLE);
                ((TargetItemHolder) iViewHolder).text_user.setText("" + allTargetsList.get(position).getUserName().toUpperCase().substring(0, 1));
            }




            ((TargetItemHolder) iViewHolder).target_user_name_tv.setText(""+allTargetsList.get(position).getUserName().toUpperCase());
            ((TargetItemHolder) iViewHolder).enquiry_no_tv.setVisibility(View.VISIBLE);
            Drawable drawable = ContextCompat.getDrawable(activity.getApplicationContext(), R.drawable.round_gray_border);
            ((TargetItemHolder) iViewHolder).background_ll.setBackground(drawable);
            ((TargetItemHolder) iViewHolder).retail_no_tv.setVisibility(View.VISIBLE);
            ((TargetItemHolder) iViewHolder).enquiry_no_tv.setVisibility(View.VISIBLE);
            ((TargetItemHolder) iViewHolder).booking_no_tv.setVisibility(View.VISIBLE);
            ((TargetItemHolder) iViewHolder).td_no_tv.setVisibility(View.VISIBLE);
            ((TargetItemHolder) iViewHolder).visit_no_tv.setVisibility(View.VISIBLE);
            ((TargetItemHolder) iViewHolder).exch_no_tv.setVisibility(View.VISIBLE);
            ((TargetItemHolder) iViewHolder).fin_no_tv.setVisibility(View.VISIBLE);
            ((TargetItemHolder) iViewHolder).enquiry_no_edt.setVisibility(View.GONE);
            ((TargetItemHolder) iViewHolder).retail_no_edt.setVisibility(View.GONE);
            ((TargetItemHolder) iViewHolder).td_no_edt.setVisibility(View.GONE);
            ((TargetItemHolder) iViewHolder).visit_no_edt.setVisibility(View.GONE);
            ((TargetItemHolder) iViewHolder).exch_no_edt.setVisibility(View.GONE);
            ((TargetItemHolder) iViewHolder).fin_no_edt.setVisibility(View.GONE);

            ((TargetItemHolder) iViewHolder).enquiry_no_tv.setText(""+allTargetsList.get(position).getEnquiries_target());
            ((TargetItemHolder) iViewHolder).retail_no_tv.setText(""+allTargetsList.get(position).getRetail_target());
            ((TargetItemHolder) iViewHolder).booking_no_tv.setText(""+allTargetsList.get(position).getBookings_target());
            ((TargetItemHolder) iViewHolder).td_no_tv.setText(""+allTargetsList.get(position).getTdrives_target());
            ((TargetItemHolder) iViewHolder).visit_no_tv.setText(""+allTargetsList.get(position).getVisits_target());
            ((TargetItemHolder) iViewHolder).exch_no_tv.setText(""+allTargetsList.get(position).getExchange_target());
            ((TargetItemHolder) iViewHolder).fin_no_tv.setText(""+allTargetsList.get(position).getFinance_target());
       /* }else {
            System.out.println("PRINT ELSE");
        }*/

    }
    public SetTargetAdapter(Activity activity, RealmList<Absolute> allTargetsList,
                            ArrayList<TargetSectionModel> targetSectionModelArrayList){
        this.allTargetsList = allTargetsList;
        this.activity = activity;
        this.targetSectionModelArrayList = targetSectionModelArrayList;
       // this.setClicked = setClicked;
    }
    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return this.allTargetsList.size();
    }

    private class TargetItemHolder extends RecyclerView.ViewHolder {

        TextView target_user_name_tv,text_user;
        EditText retail_no_edt, booking_no_edt, enquiry_no_edt, td_no_edt, visit_no_edt, exch_no_edt, fin_no_edt;
        TextView retail_no_tv, booking_no_tv, enquiry_no_tv, td_no_tv, visit_no_tv, exch_no_tv, fin_no_tv;
        ImageView imgUser;
        LinearLayout background_ll;
        TextView teamLeadName;

        public TargetItemHolder(View itemView) {
            super(itemView);
            // parentLayout = itemView;
            target_user_name_tv = (TextView) itemView.findViewById(R.id.target_user_name);
            text_user =  (TextView) itemView.findViewById(R.id.text_user);
            teamLeadName = (TextView) itemView.findViewById(R.id.team_lead_tv);
            retail_no_edt = (EditText) itemView.findViewById(R.id.retail_no);
            booking_no_edt = (EditText) itemView.findViewById(R.id.booking_no);
            enquiry_no_edt = (EditText) itemView.findViewById(R.id.enquiry_no);

            retail_no_tv = (TextView) itemView.findViewById(R.id.retail_no_tv);
            booking_no_tv = (TextView) itemView.findViewById(R.id.booking_no_tv);
            enquiry_no_tv = (TextView) itemView.findViewById(R.id.enquiry_no_tv);
            background_ll= (LinearLayout)itemView.findViewById(R.id.background_ll);
            imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            td_no_tv = (TextView) itemView.findViewById(R.id.td_no_tv);
            visit_no_tv = (TextView) itemView.findViewById(R.id.visit_no_tv);
            exch_no_tv = (TextView) itemView.findViewById(R.id.exch_no_tv);
            fin_no_tv = (TextView) itemView.findViewById(R.id.fin_no_tv);
            td_no_edt = (EditText) itemView.findViewById(R.id.td_no);
            visit_no_edt = (EditText) itemView.findViewById(R.id.visit_no);
            exch_no_edt = (EditText) itemView.findViewById(R.id.exch_no);
            fin_no_edt = (EditText) itemView.findViewById(R.id.fin_no);
        }

    }
}
