import React, { Component } from 'react'
import { View, Text, } from 'react-native'

export default class LiveEFHeaderCarModel extends Component {

  render() {
    if(this.props.showPercentage) {
      return(
        <View style={{ flexDirection: 'row',
        height:36,
        backgroundColor:'#5E7692', marginTop:10, alignItems:'center', borderRadius:6}}>

        <View style={{flex:1.5}} />

        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
          borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
            <Text style={{ color:'#fff',  textAlign:'center'}} >Retail%</Text>
        </View>
        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
            borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
              <Text style={{ color:'#fff',  textAlign:'center'}} >Exch%</Text>
        </View>

        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
              <Text style={{ color:'#fff',  textAlign:'center'}} >Fin/I%</Text>
        </View>

        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
                borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                  <Text style={{ color:'#fff',  textAlign:'center'}} >Fin/O%</Text>
        </View>
        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
                  borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                    <Text style={{ color:'#7ed321',  textAlign:'center'}} >PB%</Text>
        </View>
        <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
                    borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                      <Text style={{ color:'#7ed321',  textAlign:'center'}} >LE%</Text>
        </View>

        </View>
      )
    }
    else {
    return(
      <View style={{ flexDirection: 'row',
      height:36,
      backgroundColor:'#5E7692', marginTop:10, alignItems:'center', borderRadius:6}}>

      <View style={{flex:1.5}} />
      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
          <Text style={{ color:'#fff',  textAlign:'center'}} >Retail</Text>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
          borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
            <Text style={{ color:'#fff',  textAlign:'center'}} >Exch</Text>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
      borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
            <Text style={{ color:'#fff',  textAlign:'center'}} >Fin/I</Text>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
              borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                <Text style={{ color:'#fff',  textAlign:'center'}} >Fin/O</Text>
      </View>
      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
                borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                  <Text style={{ color:'#7ed321',  textAlign:'center'}} >PB</Text>
      </View>
      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
                  borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                    <Text style={{ color:'#7ed321',  textAlign:'center'}} >LE</Text>
      </View>

      </View>
    )
  }
  }
}
