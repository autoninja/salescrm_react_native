import React, { Component } from 'react'
import { Platform, View, Text, Image, TouchableOpacity, YellowBox, FlatList, ActivityIndicator } from 'react-native'
import PercentageSwitch from '../../../components/uielements/PercentageSwitch'


import LiveEFItem from '../../../components/liveEF/live_ef_item'
import LiveEFItemExpandable from '../../../components/liveEF/live_ef_item_expandable'
import LiveEFItemChild from '../../../components/liveEF/live_ef_item_child'
import LiveEFResultItem from '../../../components/liveEF/live_ef_result_item'
import LiveEFHeader from '../../../components/liveEF/live_ef_header'
import Toast from 'react-native-easy-toast'

import RestClient from '../../../network/rest_client'
import {eventTeamDashboard} from '../../../clevertap/CleverTapConstants';
import CleverTapPush from '../../../clevertap/CleverTapPush'

export default class LiveEFDashboardChild extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title', 'Team Details'),
    };
  };
  constructor(props) {

    super(props);


    this.state ={show_percentage:false, isLoading: false, bottomItem:{visibility:false, selected : 1},
     result:{}, show_car_percentage:false, location_selected:this.props.navigation.getParam('location_selected',0), carModelResult:{}}


    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);

  }

componentDidMount(){
    CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type' : eventTeamDashboard.keyType.valuesType.liveEFTeams });
  }

    _onRefresh(itemSelected, clickedEvent) {
      if(clickedEvent) {
      if(itemSelected == 1 ) {
        CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type' : eventTeamDashboard.keyType.valuesType.liveEFTeams });

      }
      else if (itemSelected == 2) {
        CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type' : eventTeamDashboard.keyType.valuesType.liveEFCars });

      }
      }
    let selectedItem = 1;
    if(itemSelected) {
      selectedItem = itemSelected;
    }
    this.setState({ isLoading:(selectedItem==2), bottomItem:{selected:selectedItem} });
 }

  _showDetailsOnSalesManager(navigation, title, sales_manager, from, location_id) {
    navigation.push('LiveEFDashboardChild', {
      title,
      sales_manager,
      from,
      location_id,
      location_selected : this.state.location_selected
    });
  console.log("_showDetailsOnSalesManager");
  }

  _etvbrLongPress(navigation, from,data) {
    if(from === 'user_info') {
     navigation.navigate('UserInfo', {
        name: data.info.name,
        dp_url:data.info.dp_url

      });
    }
    else if(from == 'etvbr_details') {
      navigation.navigate('ETVBRDetails', {
         data
       });
    }

console.log("_etvbrLongPress"+from);
console.log(data);
}
leadSourceCarModelWiseETVBR(navigation, name, dp_url, location_id, user_id, role_id) {
  navigation.navigate('LeadSourceCarModelDistribution', 
  {
    name,
    dp_url,
    start_date:global.global_filter_date.startDateStr,
    end_date:global.global_filter_date.endDateStr,
    location_id,
    user_id,
    role_id,
    isFromLiveEF:true
  })
  console.log('leadSourceCarModelWiseETVBR');
}

     render()
     {

        if(this.state.isLoading){
     return(
       <View style = {{display:(this.state.isLoading?'flex':'none'), flex:1,justifyContent: 'center',
         alignItems: 'center',backgroundColor:'#fff'}}>

       <ActivityIndicator
             animating={this.state.isLoading}
             style={{flex: 1,   alignSelf:'center',}}
             color="#2e5e86"
             size="small"
             hidesWhenStopped={true}

       />
       </View>

     )
    }
        const { navigation } = this.props;

        const from = navigation.getParam('from');

        const show_bottom = navigation.getParam('showBottom', false);

        const location = navigation.getParam('location', null);

        const sales_managers = location?location.sales_managers:null;

        //For sales_manager click
        const salesManager = navigation.getParam('sales_manager');
        const teamLeaders = salesManager?salesManager.team_leaders:null;

        const parent_role_id = navigation.getParam('parent_role_id');

        const location_id = navigation.getParam('location_id');



        let list;
        console.log('from:'+from+this.state.show_percentage);
        if(from == 'location') {

          list =
          <FlatList
            showsVerticalScrollIndicator={false}
           extraData={this.state}
           data = {sales_managers}
           renderItem={({item, index}) => {

             let mainView =  <TouchableOpacity onPress = {this._showDetailsOnSalesManager.bind(this, this.props.navigation, item.info.name+'\'s Team', item, 'sales_manager', location_id) }>
                               <LiveEFItem
                               
                               leadSourceCarModelWiseETVBR = 
                               {this.leadSourceCarModelWiseETVBR.bind(this,
                               navigation, 
                               item.info.name, 
                               item.info.dp_url, 
                               location_id,
                               item.info.id,
                               item.info.user_role_id)}
                                onLongPress = {this._etvbrLongPress.bind(this,this.props.navigation)}
                                onPress = {this._showDetailsOnSalesManager.bind(this, this.props.navigation, item.info.name+'\'s Team', item, 'sales_manager', location_id)}
                               showPercentage =  {this.state.show_percentage} location={item}
                               abs = {
                                 {
                                   r: item.info.abs.retails,
                                   e: item.info.abs.exchanged,
                                   f_i: item.info.abs.in_house_financed,
                                   f_o: item.info.abs.out_house_financed,
                                   p_b: item.info.abs.pending_bookings,
                                   l_e: item.info.abs.live_enquiries,

                                   }}

                                   rel = {
                                     {
                                       r: item.info.rel.retails,
                                       e: item.info.rel.exchanged,
                                       f_i: item.info.rel.in_house_financed,
                                       f_o: item.info.rel.out_house_financed,
                                       p_b: item.info.rel.pending_bookings,
                                       l_e: item.info.rel.live_enquiries,

                                       }}


                                   info = {
                                     {
                                       dp_url : item.info.dp_url,
                                       name: item.info.name,
                                       user_id:item.info.id,
                                       location_id:location_id,
                                       user_role_id:item.info.user_role_id,
                                     }
                                   }

                                   targets = {
                                     {
                                       r: item.info.targets.retails,
                                       e: item.info.targets.exchanged,
                                       f_i: item.info.targets.in_house_financed,
                                       f_o: item.info.targets.out_house_financed,
                                       p_b: item.info.targets.pending_bookings,
                                       l_e: item.info.targets.live_enquiries,
                                       }
                                 }

                                   ></LiveEFItem>
                             </TouchableOpacity>;

             if(index == sales_managers.length - 1) {
               return (<View>
                     {mainView}

                     <LiveEFResultItem
                     showPercentage = {this.state.show_percentage}
                     onLongPress = {this._etvbrLongPress.bind(this,this.props.navigation)}
                     abs = {
                       {
                         r: location.location_abs.retails,
                         e: location.location_abs.exchanged,
                         f_i: location.location_abs.in_house_financed,
                         f_o: location.location_abs.out_house_financed,
                         p_b: location.location_abs.pending_bookings,
                         l_e: location.location_abs.live_enquiries,
                         }
                       }

                       rel = {
                         {
                           r: location.location_rel.retails,
                           e: location.location_rel.exchanged,
                           f_i: location.location_rel.in_house_financed,
                           f_o: location.location_rel.out_house_financed,
                           p_b: location.location_rel.pending_bookings,
                           l_e: location.location_rel.live_enquiries,
                           }
                         }

                       info = {
                         {
                           user_id:global.appUserId,
                           name:'All SM',
                           location_id:location_id,
                           user_role_id:parent_role_id,
                         }
                       }
                         targets = {
                           {
                             r: location.location_targets.retails,
                             e: location.location_targets.exchanged,
                             f_i: location.location_targets.in_house_financed,
                             f_o: location.location_targets.out_house_financed,
                             p_b: location.location_targets.pending_bookings,
                             l_e: location.location_targets.live_enquiries,
                             }
                       }

                     ></LiveEFResultItem>

                  </View> );
             }
             else {
               return ( mainView);
             }


         }}
           keyExtractor={(item, index) => index+''}
           />
        }
        else if(from == 'mutilple_location_tl') {
          //show scs for location tl
          console.log("MulipleLocationTL")
           let scData = [];
           if(sales_managers && sales_managers[0].team_leaders) {
             scData = sales_managers[0].team_leaders[0];
           }
           list =
           <FlatList
           showsVerticalScrollIndicator={false}
           data = {scData.sales_consultants}
           renderItem={({item, index}) => {
 
             let mainView =  <TouchableOpacity >
 
                               <LiveEFItemChild
                               leadSourceCarModelWiseETVBR = 
                               {this.leadSourceCarModelWiseETVBR.bind(this,
                               navigation, 
                               item.info.name, 
                               item.info.dp_url, 
                               location_id,
                               item.info.id,
                               item.info.user_role_id)}
                                 onLongPress = {this._etvbrLongPress.bind(this,this.props.navigation)}
                               showPercentage = {this.state.show_percentage}
                               abs = {
                                 {
                                   r: item.info.abs.retails,
                                   e: item.info.abs.exchanged,
                                   f_i: item.info.abs.in_house_financed,
                                   f_o: item.info.abs.out_house_financed,
                                   p_b: item.info.abs.pending_bookings,
                                   l_e: item.info.abs.live_enquiries,
                                   }
                                 }
                                 rel = {
                                   {
                                     r: item.info.rel.retails,
                                     e: item.info.rel.exchanged,
                                     f_i: item.info.rel.in_house_financed,
                                     f_o: item.info.rel.out_house_financed,
                                     p_b: item.info.rel.pending_bookings,
                                     l_e: item.info.rel.live_enquiries,
                                     }
                                   }
                                 info = {
                                   {
                                   dp_url : item.info.dp_url,
                                   name: item.info.name,
                                   user_id:item.info.id,
                                   location_id,
                                   user_role_id:item.info.user_role_id,
                                   }
                                 }
                                   targets = {
                                     {
                                       r: item.info.targets.retails,
                                       e: item.info.targets.exchanged,
                                       f_i: item.info.targets.in_house_financed,
                                       f_o: item.info.targets.out_house_financed,
                                       p_b: item.info.targets.pending_bookings,
                                       l_e: item.info.targets.live_enquiries,
                                       }
                                 }></LiveEFItemChild>
                             </TouchableOpacity>;
 
             if(index == scData.sales_consultants.length - 1) {
               return (<View>
                     {mainView}
                     <LiveEFResultItem
                     onLongPress = {this._etvbrLongPress.bind(this,this.props.navigation)}
                     showPercentage = {this.state.show_percentage}
 
                     abs = {
                       {
                         
                         r: scData.info.abs.retails,
                         e: scData.info.abs.exchanged,
                         f_i: scData.info.abs.in_house_financed,
                         f_o: scData.info.abs.out_house_financed,
                         p_b: scData.info.abs.pending_bookings,
                         l_e: scData.info.abs.live_enquiries,
                         }
                       }
                       rel = {
                         {
                         r: scData.info.rel.retails,
                         e: scData.info.rel.exchanged,
                         f_i: scData.info.rel.in_house_financed,
                         f_o: scData.info.rel.out_house_financed,
                         p_b: scData.info.rel.pending_bookings,
                         l_e: scData.info.rel.live_enquiries,
                           }
                         }
                       info = {
                         {
                           user_id:scData.info.id,
                           name:'Team\'s Data',
                           location_id:location_id,
                          user_role_id:scData.info.user_role_id,
                         }
                       }
                         targets = {
                           {
                             r: scData.info.targets.retails,
                             e: scData.info.targets.exchanged,
                             f_i: scData.info.targets.in_house_financed,
                             f_o: scData.info.targets.out_house_financed,
                             p_b: scData.info.targets.pending_bookings,
                             l_e: scData.info.targets.live_enquiries,
                             }
                       }
 
                     ></LiveEFResultItem>
                  </View> );
             }
             else {
               return ( mainView);
             }
 
 
         }}
           keyExtractor={(item, index) => index+''}
           />
         }
        else if(from == 'sales_manager'){
          list = <FlatList
          showsVerticalScrollIndicator={false}
           extraData={this.state}
           data = {teamLeaders}
           renderItem={({item, index}) => {

             let mainViewExpandable = <LiveEFItemExpandable
             leadSourceCarModelWiseETVBRInner = {
              this.leadSourceCarModelWiseETVBR.bind(this, navigation)
            }
             leadSourceCarModelWiseETVBR = 
             {this.leadSourceCarModelWiseETVBR.bind(this,
             navigation, 
             item.info.name, 
             item.info.dp_url, 
             location_id,
             item.info.id,
             item.info.user_role_id)}
             onLongPress = {this._etvbrLongPress.bind(this,this.props.navigation)}
             showPercentage = {this.state.show_percentage}
             team_leader={item} abs = {
                                 {
                                   r: item.info.abs.retails,
                                   e: item.info.abs.exchanged,
                                   f_i: item.info.abs.in_house_financed,
                                   f_o: item.info.abs.out_house_financed,
                                   p_b: item.info.abs.pending_bookings,
                                   l_e: item.info.abs.live_enquiries,

                                   }}

                                   rel = {
                                     {
                                       r: item.info.rel.retails,
                                       e: item.info.rel.exchanged,
                                       f_i: item.info.rel.in_house_financed,
                                       f_o: item.info.rel.out_house_financed,
                                       p_b: item.info.rel.pending_bookings,
                                       l_e: item.info.rel.live_enquiries,

                                       }}

                                   info = {
                                     {
                                       dp_url : item.info.dp_url,
                                       name: item.info.name,
                                       user_id:item.info.id,
                                       location_id:location_id,
                                       user_role_id:item.info.user_role_id,
                                     }
                                   }

                                   targets = {
                                     {
                                       r: item.info.targets.retails,
                                       e: item.info.targets.exchanged,
                                       f_i: item.info.targets.in_house_financed,
                                       f_o: item.info.targets.out_house_financed,
                                       p_b: item.info.targets.pending_bookings,
                                       l_e: item.info.targets.live_enquiries,
                                       }
                                 }

                                   ></LiveEFItemExpandable>;

             if(index == teamLeaders.length - 1) {
               return (<View>
                     {mainViewExpandable}

                     <LiveEFResultItem
                      onLongPress = {this._etvbrLongPress.bind(this,this.props.navigation)}
                     showPercentage = {this.state.show_percentage}
                     abs = {
                       {
                         r: salesManager.info.abs.retails,
                         e: salesManager.info.abs.exchanged,
                         f_i: salesManager.info.abs.in_house_financed,
                         f_o: salesManager.info.abs.out_house_financed,
                         p_b: salesManager.info.abs.pending_bookings,
                         l_e: salesManager.info.abs.live_enquiries,
                         }
                       }
                       rel = {
                         {
                           r: salesManager.info.rel.retails,
                           e: salesManager.info.rel.exchanged,
                           f_i: salesManager.info.rel.in_house_financed,
                           f_o: salesManager.info.rel.out_house_financed,
                           p_b: salesManager.info.rel.pending_bookings,
                           l_e: salesManager.info.rel.live_enquiries,

                           }}
                       info = {
                         {
                           user_id:salesManager.info.id,
                           name:salesManager.info.name+'\'s Data',
                           location_id:location_id,
                           user_role_id:salesManager.info.user_role_id,
                         }
                       }
                         targets = {
                           {
                             r: salesManager.info.targets.retails,
                             e: salesManager.info.targets.exchanged,
                             f_i: salesManager.info.targets.in_house_financed,
                             f_o: salesManager.info.targets.out_house_financed,
                             p_b: salesManager.info.targets.pending_bookings,
                             l_e: salesManager.info.targets.live_enquiries,
                             }
                       }

                     ></LiveEFResultItem>

                  </View> );
             }
             else {
               return ( mainViewExpandable);
             }


         }}
           keyExtractor={(item, index) => index+''}
           />
        }

        return(

        <View style ={{flex:1,
  paddingTop: (Platform.OS) === 'ios' ? 20 : 10,
  backgroundColor: '#fff'}} >
         <Toast
              ref="toast"
              position='top'
              opacity = {0.8}          
              />

        <View style = { {display:(this.state.bottomItem.selected == 1 ?'flex':'none'),flex:1,
          paddingBottom: 10,
  paddingLeft : 10,
  paddingRight:10,}} >

        <View style={{
        flexDirection: 'row',
        height:30,
        }}>


      <View style={{flex:1/2, height:30, flexDirection :'row', justifyContent:'flex-start'}}>
      <View style={{backgroundColor:'#F4F4F4', borderWidth:1, borderColor:'#D0D8E4',borderRadius:6, justifyContent:'center'}}>
       <Text style ={{ fontSize:14, padding:4,  color:'#1B143C',textAlign:'center'}}>{date}</Text>
      </View>
      </View>

       <View style={{flexDirection:'row',  height:30, flex:1/2, justifyContent:'flex-end',}} >
       <PercentageSwitch
       value={ this.state.show_percentage }
       onValueChange={(show_percentage) => {
         this.setState({show_percentage});
          if(show_percentage) {
            CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type' : eventTeamDashboard.keyType.valuesType.liveEFPercentage });
          }
  }}/>
        </View>

     </View>

     <LiveEFHeader showPercentage = {this.state.show_percentage}/>
     {list}


           </View>

                 
  

           </View>
        );
     }
  }
