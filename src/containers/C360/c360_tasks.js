import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback, Slider
} from 'react-native'
import RestClient from '../../network/rest_client'
import Moment from 'moment';
var Sound = require('react-native-sound');
import C360LeadHistoryRecordings from './c360_lead_history_recordings';
import { connect } from 'react-redux';
import NavigationService from '../../navigation/NavigationService'
var recordings = [];
import C360PlannedTaskChild from '../C360/c360_tasks_planned_task_child'


const styles = StyleSheet.create({
    view_plannedtask_upper: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'stretch',
        flexDirection: 'row',
    },
    view_left_progress_line: {
        height: '100%',
        paddingLeft: 5,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'column',
    },
    view_straight_line: {
        width: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    view_circle: {
        width: 7,
        height: 7,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        borderRadius: 7 / 2,
        backgroundColor: '#fff'
    },
    view_plannedtask_inner_first: {
        flex: 2,
        height: '100%',
        padding: 5,
        marginLeft: 5,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'column',
    },
    view_plannedtask_inner_second: {
        flex: 8,
        margin: 2,
        paddingBottom: 5,
        paddingTop: 5,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
        alignItems: 'flex-start',
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 2,
    },
    view_plannedtask_map_render: {
        justifyContent: 'space-around',
        alignItems: 'flex-start',
        flexDirection: 'row',
    },
    view_lead_history_inner_second: {
        flex: 8,
        margin: 2,
        paddingBottom: 5,
        paddingTop: 5,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
        alignItems: 'flex-start',
        flexDirection: 'column',
        backgroundColor: '#B3B7D3',
        borderRadius: 2,
    },
    view_lead_history_inner_recording_view: {
        flex: 8,
        margin: 2,
        paddingBottom: 5,
        paddingTop: 5,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
        alignItems: 'flex-start',
        flexDirection: 'column',
        backgroundColor: '#B3B7D3',
        borderRadius: 2,
    },
    outer_heading_text: {
        margin: 7,
        fontSize: 14,
        color: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center'
    }
})


class C360Tasks extends Component {
    state = {
        closeRecodringPlayer: false,
    }
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false,
            isLoading: false,
            results: '',
            leadHistoryArray: this.getPlannedActivities(),
            expandCard: false
        };

    }

    componentDidMount() {
        //console.log("helloooooo", this.props.plannedActivities)
        //console.log("called getLeadInfo"+JSON.stringify(this.props.plannedActivities))
        console.log("lllleadId", this.props.leadId)
        this.makeRemoteRequest(this.props.leadId);
    }

    componentDidUpdate(prevProps) {
        console.log("state.closeRecodringPlayer Changed", this.props.closeRecodringPlayer)
        if (this.props.closeRecodringPlayer != prevProps.closeRecodringPlayer) {
            console.log("state.closeRecodringPlayer Tasks inner Diff")
            if (this.props.closeRecodringPlayer) {
                console.log("state.closeRecodringPlayer Tasks inner Nope")
                this.releaseRecordings();
                this.props.closeRecodringPlayerFunc();
            }
        }
    }

    componentWillUnmount() {
        console.log('Unmounted');
        this.releaseRecordings();
    }

    makeRemoteRequest = (leadId) => {
        this.setState({ isLoading: true, isFetching: true });
        new RestClient().getLeadHistory({
            lead_id: leadId
        }).then((responseJson) => {
            console.log("called -----" + JSON.stringify(responseJson))


            this.setState({
                isLoading: false,
                isFetching: false,
                results: responseJson.result,
                leadHistoryArray: this.getLeadHistory(responseJson.result),


            }, function () {
                // In this block you can do something with new state.
                //console.log(responseJ
                // const size  = responseJson.result.length;
                // console.log(responseJson.result.length);
            });
        }).catch(error => {


        });
    }

    updateRecordings(index, recording) {
        recordings[index + ''] = recording;
        console.log('Recording Size:' + recordings.length);
    }

    releaseRecordings() {
        for (var i = 0; i < recordings.length; i++) {
            if (recordings) {
                try {
                    recordings[i].release();
                    //recordings[i] = null;
                }
                catch (err) {
                }
            }
        }
    }

    getPlannedActivities() {
        let plannedActivities = this.props.plannedActivities;
        let plannedActivitiesReversedArray = [...plannedActivities].reverse();
        return plannedActivitiesReversedArray;
    }

    getLeadHistory = (responseArray) => {
        let plannedActivities = this.props.plannedActivities;
        let plannedActivitiesReversedArray = [...plannedActivities].reverse();
        let leadHistoryArray = [...plannedActivitiesReversedArray, ...responseArray];
        console.log("leadHistory inner  " + JSON.stringify(leadHistoryArray))

        return leadHistoryArray;
    }

    _openCloseActionCard = (item, index) => {
        this.setState({ expandCard: !this.state.expandCard })
    }

    _markDone = (item, itemInner) => {
        NavigationService.navigate("FormRenderingRouter", {
            leadId: this.props.leadId, scheduledActivityId: item.scheduled_activity_id, actionId: itemInner.action_id,
            form_title: "Update", lead_last_updated: this.props.leadLastUpdated, activity_id: item.activity_id
        });
    }


    render() {

        //console.log("leadHistory N  "+this.state.leadHistoryArray.length+", "+this.props.plannedActivities+", "+JSON.stringify(this.state.leadHistoryArray))
        //console.log("this.props.plannedActivities", this.props.plannedActivities);
        let plannedActivities = [...this.props.plannedActivities].reverse();
        //console.log("planned length, "+plannedActivities.length)
        //let { expandCard } = this.state;
        return (
            <View style={{ flex: 1, justifyContent: 'center', margin: 5, }}>
                <FlatList

                    data={this.state.leadHistoryArray}
                    extraData={this.state}
                    renderItem={({ item, index }) => {
                        if (plannedActivities.length > 0 && index < plannedActivities.length) {
                            {
                                if (item.details) {
                                    // if(index == 0){
                                    //     console.log("arrayItme", item)
                                    // }
                                    return <C360PlannedTaskChild
                                        listItem={item}
                                        index={index}
                                        plannedActivities={plannedActivities}
                                        markDone={this._markDone}
                                    />
                                }
                            }
                        } else {
                            if (this.state.isLoading) {
                                return (
                                    <View style={{ flex: 1, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center' }}>
                                        <ActivityIndicator />
                                    </View>
                                );
                            }

                            if (this.state.results != 0) {
                                return (
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                        {(index == 0 || index == plannedActivities.length) ? <Text style={styles.outer_heading_text}>Past Tasks</Text> : null}

                                        <View style={styles.view_plannedtask_upper}>
                                            <View style={styles.view_left_progress_line}>
                                                <View style={styles.view_circle} />
                                                {(index < this.state.leadHistoryArray.length - 1) ? <View style={styles.view_straight_line} /> : null}
                                            </View>

                                            <View style={styles.view_plannedtask_inner_first}>
                                                <Text style={{ color: '#fff', fontSize: 10 }}>Day {Math.floor((new Date(Moment(item.logged_time).format('YYYY-MM-DD')).getTime() - new Date(Moment(this.state.leadHistoryArray[this.state.leadHistoryArray.length - 1].logged_time).format('YYYY-MM-DD')).getTime()) / 86400000 + 1)}</Text>
                                                <Text style={{ color: '#fff', fontSize: 8 }}>{Moment(item.logged_time).format('ddd DD/MM')}</Text>
                                                <Text style={{ color: '#fff', fontSize: 8 }}>{Moment(item.logged_time).format('hh:mm A')}</Text>
                                            </View>
                                            {(item.log_type === 'call') ?
                                                <View style={styles.view_lead_history_inner_recording_view}>

                                                    <C360LeadHistoryRecordings
                                                        updateRecordings={this.updateRecordings.bind(this, index)}
                                                        leadDetails={item} />

                                                </View>

                                                :
                                                <View style={styles.view_lead_history_inner_second}>

                                                    <Text style={{ color: '#4a4a4a', fontSize: 14, fontWeight: '500' }}>{item.title} </Text>
                                                    {(item.remarks && item.remarks != "") ? <Text style={{ flex: 1, flexWrap: 'wrap', color: '#4a4a4a', fontSize: 13 }}>Remarks: {item.remarks} </Text> : null}

                                                    {item.allHistoryObject.map((item, index) => {
                                                        if (item.key === 'Link') {

                                                        } else {
                                                            return (
                                                                <View style={styles.view_plannedtask_map_render} key={index}>

                                                                    <Text style={{ color: '#4a4a4a', fontSize: 13 }}>{item.key}: </Text>
                                                                    <Text style={{ flex: 1, flexWrap: 'wrap', color: '#4a4a4a', fontSize: 13 }}>
                                                                        {(item.key === 'Old Date' || item.key === 'New Date') ?
                                                                            Moment(item.value).format('DD MMM YYYY') + " " + Moment(item.value).format('hh:mm A') : item.value}
                                                                    </Text>
                                                                </View>
                                                            )
                                                        }
                                                    })}
                                                </View>}

                                        </View>


                                    </View>
                                )
                            }
                        }
                    }

                    }
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}

const mapStateToProps = state => {
    console.log("state.closeRecodringPlayer", state.closeRecodringPlayer)
    return {

        closeRecodringPlayer: state.closeRecodringPlayer.closeRecodringPlayer
    }
}

const mapDispatchToProps = dispatch => {
    console.log("state.closeRecodringPlayer  dddddd")
    return {
        closeRecodringPlayerFunc: () => dispatch({ type: 'OPEN_RECORD_PLAYING' })
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(C360Tasks)