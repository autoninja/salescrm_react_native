package com.salescrm.telephony.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.salescrm.telephony.response.Error;

import java.util.List;

/**
 * Created by prateek on 20/9/17.
 */

public class EtvbrCarsPojo {
    @SerializedName("error")
    @Expose
    private Error error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private ResultEtvbrCars result;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultEtvbrCars getResult() {
        return result;
    }

    public void setResult(ResultEtvbrCars result) {
        this.result = result;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class ResultEtvbrCars{
        @SerializedName("abs")
        @Expose
        private List<EtvbrCarsAbsolute> abs = null;
        @SerializedName("rel")
        @Expose
        private List<EtvbrCarsRelational> rel = null;
        @SerializedName("abs_total")
        @Expose
        private EtvbrCarsAbsTotal absTotal;
        @SerializedName("rel_total")
        @Expose
        private EtvbrCarsRelTotal relTotal;

        public List<EtvbrCarsAbsolute> getAbs() {
            return abs;
        }

        public void setAbs(List<EtvbrCarsAbsolute> abs) {
            this.abs = abs;
        }

        public List<EtvbrCarsRelational> getRel() {
            return rel;
        }

        public void setRel(List<EtvbrCarsRelational> rel) {
            this.rel = rel;
        }

        public EtvbrCarsAbsTotal getAbsTotal() {
            return absTotal;
        }

        public void setAbsTotal(EtvbrCarsAbsTotal absTotal) {
            this.absTotal = absTotal;
        }

        public EtvbrCarsRelTotal getRelTotal() {
            return relTotal;
        }

        public void setRelTotal(EtvbrCarsRelTotal relTotal) {
            this.relTotal = relTotal;
        }
    }

    public class EtvbrCarsAbsolute{
        @SerializedName("modelName")
        @Expose
        private String modelName;
        @SerializedName("modelId")
        @Expose
        private Integer modelId;
        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retail")
        @Expose
        private Integer retail;

        public String getModelName() {
            return modelName;
        }

        public void setModelName(String modelName) {
            this.modelName = modelName;
        }

        public Integer getModelId() {
            return modelId;
        }

        public void setModelId(Integer modelId) {
            this.modelId = modelId;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetail() {
            return retail;
        }

        public void setRetail(Integer retail) {
            this.retail = retail;
        }
    }

    public class EtvbrCarsRelational{
        @SerializedName("modelName")
        @Expose
        private String modelName;
        @SerializedName("modelId")
        @Expose
        private Integer modelId;
        @SerializedName("enquiries_per")
        @Expose
        private Integer enquiriesPer;
        @SerializedName("tdrives_per")
        @Expose
        private Integer tdrivesPer;
        @SerializedName("visits_per")
        @Expose
        private Integer visitsPer;
        @SerializedName("bookings_per")
        @Expose
        private Integer bookingsPer;
        @SerializedName("retail_per")
        @Expose
        private Integer retailPer;

        public String getModelName() {
            return modelName;
        }

        public void setModelName(String modelName) {
            this.modelName = modelName;
        }

        public Integer getModelId() {
            return modelId;
        }

        public void setModelId(Integer modelId) {
            this.modelId = modelId;
        }

        public Integer getEnquiriesPer() {
            return enquiriesPer;
        }

        public void setEnquiriesPer(Integer enquiriesPer) {
            this.enquiriesPer = enquiriesPer;
        }

        public Integer getTdrivesPer() {
            return tdrivesPer;
        }

        public void setTdrivesPer(Integer tdrivesPer) {
            this.tdrivesPer = tdrivesPer;
        }

        public Integer getVisitsPer() {
            return visitsPer;
        }

        public void setVisitsPer(Integer visitsPer) {
            this.visitsPer = visitsPer;
        }

        public Integer getBookingsPer() {
            return bookingsPer;
        }

        public void setBookingsPer(Integer bookingsPer) {
            this.bookingsPer = bookingsPer;
        }

        public Integer getRetailPer() {
            return retailPer;
        }

        public void setRetailPer(Integer retailPer) {
            this.retailPer = retailPer;
        }
    }

    public class EtvbrCarsRelTotal{
        @SerializedName("enqTotal")
        @Expose
        private Integer enqTotal;
        @SerializedName("tdTotal")
        @Expose
        private Integer tdTotal;
        @SerializedName("visitsTotal")
        @Expose
        private Integer visitsTotal;
        @SerializedName("bookTotal")
        @Expose
        private Integer bookTotal;
        @SerializedName("retailTotal")
        @Expose
        private Integer retailTotal;

        public Integer getEnqTotal() {
            return enqTotal;
        }

        public void setEnqTotal(Integer enqTotal) {
            this.enqTotal = enqTotal;
        }

        public Integer getTdTotal() {
            return tdTotal;
        }

        public void setTdTotal(Integer tdTotal) {
            this.tdTotal = tdTotal;
        }

        public Integer getVisitsTotal() {
            return visitsTotal;
        }

        public void setVisitsTotal(Integer visitsTotal) {
            this.visitsTotal = visitsTotal;
        }

        public Integer getBookTotal() {
            return bookTotal;
        }

        public void setBookTotal(Integer bookTotal) {
            this.bookTotal = bookTotal;
        }

        public Integer getRetailTotal() {
            return retailTotal;
        }

        public void setRetailTotal(Integer retailTotal) {
            this.retailTotal = retailTotal;
        }
    }

    public class EtvbrCarsAbsTotal{
        @SerializedName("enqTotal")
        @Expose
        private Integer enqTotal;
        @SerializedName("tdTotal")
        @Expose
        private Integer tdTotal;
        @SerializedName("visitsTotal")
        @Expose
        private Integer visitsTotal;
        @SerializedName("bookTotal")
        @Expose
        private Integer bookTotal;
        @SerializedName("retailTotal")
        @Expose
        private Integer retailTotal;

        public Integer getEnqTotal() {
            return enqTotal;
        }

        public void setEnqTotal(Integer enqTotal) {
            this.enqTotal = enqTotal;
        }

        public Integer getTdTotal() {
            return tdTotal;
        }

        public void setTdTotal(Integer tdTotal) {
            this.tdTotal = tdTotal;
        }

        public Integer getVisitsTotal() {
            return visitsTotal;
        }

        public void setVisitsTotal(Integer visitsTotal) {
            this.visitsTotal = visitsTotal;
        }

        public Integer getBookTotal() {
            return bookTotal;
        }

        public void setBookTotal(Integer bookTotal) {
            this.bookTotal = bookTotal;
        }

        public Integer getRetailTotal() {
            return retailTotal;
        }

        public void setRetailTotal(Integer retailTotal) {
            this.retailTotal = retailTotal;
        }
    }
}
