package com.salescrm.telephony.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.salescrm.telephony.response.Error;

import java.util.List;

/**
 * Created by prateek on 16/6/17.
 */

public class EtvbrPojo {

    @SerializedName("error")
    @Expose
    private Error error = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("result")
    @Expose
    private Results result = null;

    @SerializedName("userId")
    @Expose
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Results getResult() {
        return result;
    }

    public void setResult(Results result) {
        this.result = result;
    }

    public class Results{
        @SerializedName("result")
        @Expose
        private List<Result> result = null;

        public List<Result> getResult() {
            return result;
        }

        public void setResult(List<Result> result) {
            this.result = result;
        }
    }

   /* public class Error {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("details")
        @Expose
        private String details;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

    }*/

    public class Ab {

        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("user_name")
        @Expose
        private String userName;
        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retail")
        @Expose
        private Integer retail;
        @SerializedName("dp_url")
        @Expose
        private String dpUrl;
        @SerializedName("enquiries_target")
        @Expose
        private Integer enquiriesTarget;
        @SerializedName("tdrives_target")
        @Expose
        private Integer tdrivesTarget;
        @SerializedName("visits_target")
        @Expose
        private Integer visitsTarget;
        @SerializedName("bookings_target")
        @Expose
        private Integer bookingsTarget;
        @SerializedName("retail_target")
        @Expose
        private Integer retailTarget;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetail() {
            return retail;
        }

        public void setRetail(Integer retail) {
            this.retail = retail;
        }

        public String getDpUrl() {
            return dpUrl;
        }

        public void setDpUrl(String dpUrl) {
            this.dpUrl = dpUrl;
        }

        public Integer getEnquiriesTarget() {
            return enquiriesTarget;
        }

        public void setEnquiriesTarget(Integer enquiriesTarget) {
            this.enquiriesTarget = enquiriesTarget;
        }

        public Integer getTdrivesTarget() {
            return tdrivesTarget;
        }

        public void setTdrivesTarget(Integer tdrivesTarget) {
            this.tdrivesTarget = tdrivesTarget;
        }

        public Integer getVisitsTarget() {
            return visitsTarget;
        }

        public void setVisitsTarget(Integer visitsTarget) {
            this.visitsTarget = visitsTarget;
        }

        public Integer getBookingsTarget() {
            return bookingsTarget;
        }

        public void setBookingsTarget(Integer bookingsTarget) {
            this.bookingsTarget = bookingsTarget;
        }

        public Integer getRetailTarget() {
            return retailTarget;
        }

        public void setRetailTarget(Integer retailTarget) {
            this.retailTarget = retailTarget;
        }
    }

    public class Rel {

        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("user_name")
        @Expose
        private String userName;
        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retail")
        @Expose
        private Integer retail;
        @SerializedName("dp_url")
        @Expose
        private String dpUrl;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetail() {
            return retail;
        }

        public void setRetail(Integer retail) {
            this.retail = retail;
        }

        public String getDpUrl() {
            return dpUrl;
        }

        public void setDpUrl(String dpUrl) {
            this.dpUrl = dpUrl;
        }

    }

    public class Result {

        @SerializedName("team_id")
        @Expose
        private Integer teamId;
        @SerializedName("team_name")
        @Expose
        private String teamName;
        @SerializedName("abs")
        @Expose
        private List<Ab> abs = null;
        @SerializedName("rel")
        @Expose
        private List<Rel> rel = null;
        @SerializedName("abs_total")
        @Expose
        private AbsTotal absTotal;
        @SerializedName("rel_total")
        @Expose
        private RelTotal relTotal;

        public AbsTotal getAbsTotal() {
            return absTotal;
        }

        public void setAbsTotal(AbsTotal absTotal) {
            this.absTotal = absTotal;
        }

        public RelTotal getRelTotal() {
            return relTotal;
        }

        public void setRelTotal(RelTotal relTotal) {
            this.relTotal = relTotal;
        }

        public Integer getTeamId() {
            return teamId;
        }

        public void setTeamId(Integer teamId) {
            this.teamId = teamId;
        }

        public String getTeamName() {
            return teamName;
        }

        public void setTeamName(String teamName) {
            this.teamName = teamName;
        }

        public List<Ab> getAbs() {
            return abs;
        }

        public void setAbs(List<Ab> abs) {
            this.abs = abs;
        }

        public List<Rel> getRel() {
            return rel;
        }

        public void setRel(List<Rel> rel) {
            this.rel = rel;
        }

    }

    public class RelTotal {
        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retail")
        @Expose
        private Integer retail;

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetail() {
            return retail;
        }

        public void setRetail(Integer retail) {
            this.retail = retail;
        }
    }

    public class AbsTotal {

        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retail")
        @Expose
        private Integer retail;
        @SerializedName("enquiries_target")
        @Expose
        private Integer enquiriesTarget;
        @SerializedName("tdrives_target")
        @Expose
        private Integer tdrivesTarget;
        @SerializedName("visits_target")
        @Expose
        private Integer visitsTarget;
        @SerializedName("bookings_target")
        @Expose
        private Integer bookingsTarget;
        @SerializedName("retail_target")
        @Expose
        private Integer retailTarget;

        public Integer getEnquiriesTarget() {
            return enquiriesTarget;
        }

        public void setEnquiriesTarget(Integer enquiriesTarget) {
            this.enquiriesTarget = enquiriesTarget;
        }

        public Integer getTdrivesTarget() {
            return tdrivesTarget;
        }

        public void setTdrivesTarget(Integer tdrivesTarget) {
            this.tdrivesTarget = tdrivesTarget;
        }

        public Integer getVisitsTarget() {
            return visitsTarget;
        }

        public void setVisitsTarget(Integer visitsTarget) {
            this.visitsTarget = visitsTarget;
        }

        public Integer getBookingsTarget() {
            return bookingsTarget;
        }

        public void setBookingsTarget(Integer bookingsTarget) {
            this.bookingsTarget = bookingsTarget;
        }

        public Integer getRetailTarget() {
            return retailTarget;
        }

        public void setRetailTarget(Integer retailTarget) {
            this.retailTarget = retailTarget;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetail() {
            return retail;
        }

        public void setRetail(Integer retail) {
            this.retail = retail;
        }

    }
}
