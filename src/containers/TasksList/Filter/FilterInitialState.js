export const getFilterInitialData = (info) => {
    return (
        [
            {
                id: 'task_type',
                name: 'Tasks Type',
                selected: true,
                view: 'default_view',
                children: []
            },
            {
                id: 'tags',
                name: 'Tags',
                selected: false,
                view: 'default_view',
                children: [
                    { id: '1', title: "Hot" },
                    { id: '2', title: "Warm" },
                    { id: '3', title: "Cold" },
                ]

            },
            {
                id: 'vehicle_models',
                name: 'Vehicle Models',
                selected: false,
                view: 'default_view',
                children: info.models
            },
            {
                id: 'lead_source',
                name: 'Lead Source',
                selected: false,
                view: 'default_view',
                children: info.enquirySource
            },
            {
                id: 'lead_age',
                name: 'Ageing',
                selected: false,
                view: 'lead_age_view',
                children: [
                    { id: '0', title: 0 },
                ]
            },
            {
                id: 'lead_stage',
                name: 'Lead Stage',
                selected: false,
                view: 'default_view',
                children: []
            },
            {
                id: 'date',
                name: 'Expected Buying Date',
                selected: false,
                view: 'date_view',
                children: [
                    { id: 'expected_buying_date_from', title: "", payload: null },
                    { id: 'expected_buying_date_to', title: "", payload: null },
                    { id: 'enquiry_date_from', title: "", payload: null },
                    { id: 'enquiry_date_to', title: "", payload: null },
                ]
            }

        ])
} 