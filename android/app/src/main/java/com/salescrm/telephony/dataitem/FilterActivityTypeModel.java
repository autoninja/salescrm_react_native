package com.salescrm.telephony.dataitem;

/**
 * Created by Ravindra P on 02-06-2016.
 */
public class FilterActivityTypeModel {
    private String name;
    private int value; /* 0 checkbox disable, 1 checkbox enable */
    private String id;

    public FilterActivityTypeModel(String name, int value, String id) {
        this.id = id;
        this.name = name;
        this.value = value;
    }
    public FilterActivityTypeModel(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public FilterActivityTypeModel(int value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}