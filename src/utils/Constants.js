const TaskConstants = {
    HOME_VISIT :{
        title :"Home Visit",
        id : 2
    },
    SHOWROOM_VISIT :{
        title :"Home Visit",
        id : 3
    },
    TEST_DRIVE_HOME :{
        title :"Test Drive Home",
        id : 6
    },
    TEST_DRIVE_SHOWROOM :{
        title :"Test Drive Showroom",
        id : 5
    },
    LOST_DROP :{
        title :"Lost Drop",
        id : 24
    },
    EVALUATION_REQUEST :{
        title :"Evaluation Request",
        id : 10
    },
    PLAN_NEXT_TASK: {
        title:"Plan Next Task",
        id:23
    }

}
const FormQuestionIdConstants = {
        SELECT_ACTIVITY  : 351,
        SELECT_ACTIVITY_POST_BOOKING : 743,
        MANUFACTURING_YEAR : 99,
        RESCHEDULED : 71,
        LOST_DROP_NO  : 95,
        VISIT_YES : 72,
        TESTDRIVE_YES : 74,
        OTHER_ACTIVITY : 148,
        PNT_ACTIVITY : 2,
}
module.exports = {TaskConstants, FormQuestionIdConstants};