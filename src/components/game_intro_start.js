import React, { Component } from 'react'
import { Platform,ImageBackground, View, Text, TouchableHighlight, } from 'react-native'
import styles from '../styles/styles'

export default class GameIntroStart extends Component {

  constructor(props) {
    super(props);
  }
  componentDidMount () {

  }
  render() {
    var letsStart = 'Let\'s Start';
    return(
  <View style={styles.MainContainer}>
      <View style= {[styles.MainContainerYellow, {backgroundColor:'transparent', borderRadius:6}]}>
      <ImageBackground
        source={require('../images/game_intro_get_started.png')}
        style={{width: '100%', height: '100%', borderRadius:6}}
      >

      <Text onPress={() => this.props.startGame()} style={{flex:1,position:'absolute',bottom:10,width:'100%', textDecorationLine:'underline', fontSize:20, color:'#fff', textAlign:'center'}} > {letsStart} </Text>


      </ImageBackground>

      </View>
      <View style={{alignItems: 'center', padding:10,flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
      {this.props.bottomViews}
      </View>
      </View>
    )
  }
}
