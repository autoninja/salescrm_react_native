import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    TouchableWithoutFeedback,
    TouchableOpacity,
    ScrollView,
    Alert
} from "react-native";


export default class SearchResultItem extends Component {
    constructor(props) {
        super(props);
    }
    _onLongPressButton() {
        console.log("Long press worked");
        Alert.alert("You long-pressed the button!");
    }
    render() {
        let searchResult = this.props.searchResult;
        let bgColor = "#f7412d";
        if (searchResult.tag_name === "Cold") {
            bgColor = "#00a7f7";
        } else if (searchResult.tag_name == "Warm") {
            bgColor = "#ff9900";
        }
        return (
            <TouchableOpacity
                disabled={!searchResult.is_view_allowed}
                onPress={() =>
                    this.props.openC360(
                        searchResult.leadId,
                    )
                }
                style={{ marginTop: 10 }}
            >
                <View
                    style={{
                        flexDirection: "row",
                        backgroundColor: "#fff",
                        borderRadius: 6
                    }}
                >
                    <View
                        style={{
                            flexDirection: "row",
                            width: 4,
                            backgroundColor: bgColor,
                            borderBottomLeftRadius: 6,
                            borderTopLeftRadius: 6
                        }}
                    />
                    <View
                        style={{
                            paddingRight: 8,
                            paddingLeft: 8,
                            paddingTop: 6,
                            paddingBottom: 6
                        }}
                    >
                        <View style={{ flexDirection: "row" }}>
                            <Text
                                style={{
                                    fontSize: 16,
                                    color: "#9b9b9b",
                                    marginTop: 4,
                                    height: 24
                                }}
                            >
                                {" "}
                                {"Mr."}{" "}
                            </Text>
                            <View>
                                <Text style={{ fontSize: 20, color: "#45446d" }}>
                                    {searchResult.firstName + " " + searchResult.lastName}
                                </Text>
                                <Text style={{ fontSize: 14, marginTop: 2, color: "#45446d" }}>
                                    {searchResult.dsename ? "Assigned: " + searchResult.dsename : "Not Assigned"}
                                </Text>
                                <Text style={{ fontSize: 14, marginTop: 2, color: "#45446d" }}>
                                    {"Lead Status: " + searchResult.status + " (" + (searchResult.active == 0 ? "Closed" : "Open") + ")"}
                                </Text>
                                <Text style={{ fontSize: 14, marginTop: 2, color: "#45446d" }}>
                                    {"Lead Stage: " + searchResult.stage_name}
                                </Text>
                                <Text style={{ fontSize: 14, marginTop: 2, color: "#45446d" }}>
                                    {"Location: " + searchResult.location_name}
                                </Text>
                                {searchResult.activities_data && searchResult.activities_data.length > 0 && (
                                    <View
                                        style={{
                                            flexDirection: "row",
                                            marginTop: 6,
                                            alignItems: "center"
                                        }}
                                    >
                                        <Image
                                            source={require("../../images/ic_next_grey.png")}
                                            style={{ width: 24, height: 24 }}
                                        />
                                        <Text style={{ fontSize: 15, color: "#45446d" }}>
                                            {" "}
                                            {searchResult.activities_data[0].scheduled_activity_name}{" "}
                                        </Text>
                                    </View>
                                )}

                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}
