import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback
} from 'react-native'
import Moment from 'moment';

const styles = StyleSheet.create({
    view_plannedtask_upper: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'stretch',
        flexDirection: 'row',
    },
    view_left_progress_line: {
        height: '100%',
        paddingLeft: 5,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'column',
    },
    view_straight_line: {
        width: 1,
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    view_circle: {
        width: 7,
        height: 7,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        borderRadius: 7 / 2,
        backgroundColor: '#fff'
    },
    view_plannedtask_inner_first: {
        flex: 2,
        height: '100%',
        padding: 5,
        marginLeft: 5,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        flexDirection: 'column',
    },
    view_plannedtask_inner_second: {
        flex: 8,
        margin: 2,
        paddingBottom: 5,
        paddingTop: 5,
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
        alignItems: 'flex-start',
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 2,
    },
    view_plannedtask_map_render: {
        justifyContent: 'space-around',
        alignItems: 'flex-start',
        flexDirection: 'row',
    },
    outer_heading_text: {
        margin: 7,
        fontSize: 14,
        color: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center'
    }
})

export default class C360PlannedTaskChild extends Component {
    constructor(props) {
        super(props);

        this.state = {
            expandCard: false
        }
    }

    _openCloseActionCard = () => {
        this.setState({ expandCard: !this.state.expandCard })
    }

    markDone = (item, itemInner) => {
        this.props.markDone(item, itemInner)
    }

    render() {
        let item = this.props.listItem;
        let plannedActivities = this.props.plannedActivities;
        let index = this.props.index
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                {(index == 0) ? <Text style={styles.outer_heading_text}>Planned Tasks</Text> : null}
                <View style={[styles.view_plannedtask_upper, { backgroundColor: (this.state.expandCard) ? "#fff" : 'transparent', borderRadius: 2 }]}>
                    {(this.state.expandCard) ? null : <View style={styles.view_left_progress_line}>
                        <View style={styles.view_circle} />
                        {(index < plannedActivities.length - 1) ? <View style={styles.view_straight_line} /> : null}
                    </View>}
                    <View style={styles.view_plannedtask_inner_first}>
                        {(this.state.results && this.state.results.length > 0) ?
                            <Text style={{ color: (this.state.expandCard) ? '#000000' : '#fff', fontSize: 10 }}>Day {Math.floor((new Date(Moment(item.date_time).format('YYYY-MM-DD')).getTime() - new Date(Moment(this.state.leadHistoryArray[this.state.leadHistoryArray.length - 1].logged_time).format('YYYY-MM-DD')).getTime()) / 86400000 + 1)}</Text>
                            : <Text style={{ color: (this.state.expandCard) ? '#000000' : '#fff', fontSize: 10 }}>Day {Math.floor((new Date(Moment(item.date_time).format('YYYY-MM-DD')).getTime() - new Date(Moment(plannedActivities[plannedActivities.length - 1].date_time).format('YYYY-MM-DD')).getTime()) / 86400000 + 1)}</Text>
                        }
                        <Text style={{ color: (this.state.expandCard) ? '#000000' : '#fff', fontSize: 8 }}>{Moment(item.date_time).format('ddd DD/MM')}</Text>
                        <Text style={{ color: (this.state.expandCard) ? '#000000' : '#fff', fontSize: 8 }}>{Moment(item.date_time).format('hh:mm A')}</Text>
                    </View>

                    <View style={styles.view_plannedtask_inner_second}>
                        <View style={{ alignItems: 'flex-start', flex: 7 }}>
                            <Text style={{ color: '#4a4a4a', fontSize: 14, fontWeight: '500' }}>{item.name} </Text>
                            {item.details.map((item, index) => {
                                return (
                                    <View style={styles.view_plannedtask_map_render} key={index}>
                                        <Text style={{ color: '#4a4a4a', fontSize: 12 }}>{item.key}: </Text>
                                        <Text style={{ flex: 1, flexWrap: 'wrap', color: '#4a4a4a', fontSize: 12 }}>{item.value}</Text>
                                    </View>
                                )
                            })}
                        </View>
                        {(item.actions && item.actions.length > 0) ?
                            item.actions.map((itemInner, indexInner) => {
                                if (itemInner.action_id == "6" || itemInner.action_id == "5") {
                                    return <View style={{ backgroundColor: '#fff', flex: 1 }}>
                                        <TouchableOpacity onPress={this._openCloseActionCard}>
                                            {(this.state.expandCard) ? <Image
                                                style={{ width: 24, height: 24, alignItems: 'center', margin: 10 }}
                                                source={require('../../images/ic_up_black.png')}
                                            /> : <Image
                                                    style={{ width: 24, height: 24, alignItems: 'center', margin: 10 }}
                                                    source={require('../../images/ic_down_black.png')}
                                                />
                                            }
                                        </TouchableOpacity>
                                    </View>
                                }
                            })
                            : ((item.activity_id == "23" || item.activity_id == "22") && item.activity_id != "10" && item.activity_id != "11" && item.activity_id != "7") ?
                                <View style={{ backgroundColor: '#fff', flex: 1 }}>
                                    <TouchableOpacity onPress={this._openCloseActionCard}>
                                        {(this.state.expandCard) ? <Image
                                            style={{ width: 24, height: 24, alignItems: 'center', margin: 10 }}
                                            source={require('../../images/ic_up_black.png')}
                                        /> : <Image
                                                style={{ width: 24, height: 24, alignItems: 'center', margin: 10 }}
                                                source={require('../../images/ic_down_black.png')}
                                            />
                                        }

                                    </TouchableOpacity>
                                </View> : null}
                    </View>

                </View>
                {(this.state.expandCard) ? <View style={{ backgroundColor: '#E9EEFF', justifyContent: 'center', flexDirection: 'row', marginTop: -1, marginBottom: 4 }}>
                    {(item.actions && item.actions.length > 0) ?
                        item.actions.map((itemInner, indexInner) => {
                            if (itemInner.action_id == "5") {
                                return <View style={{ flex: 1, alignItems: 'center' }}>
                                    <TouchableOpacity onPress={() => this.markDone(item, itemInner)}>
                                        <Image
                                            style={{ width: 24, height: 24, alignItems: 'center', borderRadius: 24 / 2, margin: 5, padding: 5, backgroundColor: '#0076ff' }}
                                            source={require('../../images/ic_check_mark.png')}
                                        />
                                        <Text style={{ color: "#000000", fontSize: 10, marginBottom: 2 }}>Update</Text>
                                    </TouchableOpacity>
                                </View>
                            }
                            // if (itemInner.action_id == "6") {
                            //     return <View style={{ flex: 1, alignItems: 'center' }}>
                            //         <TouchableOpacity onPress={() => this.markDone(item, itemInner)}>
                            //             <Image
                            //                 style={{ width: 24, height: 24, alignItems: 'center', borderRadius: 24 / 2, margin: 5, padding: 5, backgroundColor: '#0076ff' }}
                            //                 source={require('../../images/ic_help.png')}
                            //             />

                            //         </TouchableOpacity>
                            //     </View>
                            // }
                        })
                        : ((item.activity_id == "23" || item.activity_id == "22") && item.activity_id != "10" && item.activity_id != "11" && item.activity_id != "7") ?
                            <View style={{ flex: 1, alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => this.markDone(item, item.action[0])}>
                                    <Image
                                        style={{ width: 24, height: 24, alignItems: 'center', borderRadius: 24 / 2, margin: 5, padding: 5, backgroundColor: '#0076ff' }}
                                        source={require('../../images/ic_check_mark.png')}
                                    />

                                </TouchableOpacity>
                            </View> : null}
                </View> : null}
            </View>
        );
    }
}