package com.salescrm.telephony.offline;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.ScoreboardDB;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.ScoreboardResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 6/12/16.
 * Call this to get the scoreboard
 */
public class FetchScoreboardData implements Callback<ScoreboardResponse> {
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private StringBuilder ids;

    public FetchScoreboardData(OfflineSupportListener listener, Context context) {
        this.listener =listener;
        this.context = context;
      //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }
    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ids = new StringBuilder();
            RealmList<UserRolesDB> rolesList =realm.where(UserDetails.class).findFirst()==null?null:realm.where(UserDetails.class).findFirst().getUserRoles();
            for (int i = 0; i < (rolesList == null ? 0 : rolesList.size()); i++) {
                ids.append(rolesList.get(i).getId());
                if (i < rolesList.size() - 1) {
                    ids.append(",");
                }
            }
            if(realm.where(UserDetails.class).findFirst()!=null) {
                listener.onScoreboardDataFetched(null);
              //  ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).FetchScoreBoardData(realm.where(UserDetails.class).findFirst().getUserId() + "", ids.toString(), this);
            }
            else {
                Toast.makeText(context,"You need to login again for security purpose",Toast.LENGTH_LONG).show();
                listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.SCOREBOARD);
            }
            } else {
            listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.SCOREBOARD);
            System.out.println("No Internet Connection");
        }
    }


    @Override
    public void success(final ScoreboardResponse data, Response response) {
        Util.updateHeaders(response.getHeaders());

        if (!data.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + data.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.SCOREBOARD);
        } else {
            if (data.getResult() != null) {
                System.out.println("Success:1" + data.getMessage());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        insertData(data);
                    }
                });
                listener.onScoreboardDataFetched(data);
            } else {
                // relLoader.setVisibility(View.GONE);
                System.out.println("Success:2" + data.getMessage());
                listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.SCOREBOARD);

                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }

    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(error,WSConstants.OfflineAPIRequest.SCOREBOARD);

    }

    private void insertData(ScoreboardResponse data) {
        data.getResult().setUser_id(ids.toString());
        realm.createOrUpdateObjectFromJson(ScoreboardDB.class, new Gson().toJson(data.getResult()));
    }

}
