package com.salescrm.telephony.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AllotDseActivity;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.activity.FormRenderingActivity;
import com.salescrm.telephony.activity.offline.C360ActivityOffline;
import com.salescrm.telephony.dataitem.SectionModel;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by bharath on 27-06-2016.
 */
public class DoneActionPlanRealmAdapter extends RealmRecyclerViewAdapter<SalesCRMRealmTable, DoneActionPlanRealmAdapter.DoneActionPlanRealViewHolder> {

    private boolean expand = false;
    private Context context;
    private List<SectionModel> sections;
    private Preferences pref = null;
    private Realm realm;

    public DoneActionPlanRealmAdapter(@NonNull Context context, @Nullable OrderedRealmCollection<SalesCRMRealmTable> data, boolean autoUpdate, List<SectionModel> sections) {
        super(context, data, true);
        this.context = context;
        this.realm = Realm.getDefaultInstance();
        this.pref = Preferences.getInstance();
        this.pref.load(context);
        this.sections = sections;
    }

    @Override
    public DoneActionPlanRealViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = null;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.action_plan_card, parent, false);
        return new DoneActionPlanRealViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(DoneActionPlanRealViewHolder holder, int position) {
        if (getData() != null && getData().get(position) != null) {
            final SalesCRMRealmTable actionPlanData = getData().get(position);
            // searchResponse holder. = activityDetails;
            // System.out.println("Lead:Id:"+actionPlanData.getLead_id());

            if (sections != null) {
                for (SectionModel section : sections) {
                    if (position == section.getPosition()) {
                        holder.tvActionPlanHeader.setText(section.getTitle());
                        holder.tvActionPlanHeader.setVisibility(View.VISIBLE);
                        break;
                    } else {
                        holder.tvActionPlanHeader.setVisibility(View.GONE);
                    }
                }
            }

            holder.ibControlBottom.setVisibility(View.INVISIBLE);
            holder.relLeadDetails.setVisibility(View.GONE);
            if (actionPlanData.getCreateLeadInputDataDB() != null && actionPlanData.getCreateLeadInputDataDB().is_synced()) {
                holder.frameOffline.setVisibility(View.GONE);
                holder.relActionPlanBottom.setVisibility(View.GONE);
            }
            else if (actionPlanData.getFormAnswerDB() != null && actionPlanData.getFormAnswerDB().is_synced()) {
                holder.frameOffline.setVisibility(View.GONE);
                holder.relActionPlanBottom.setVisibility(View.GONE);
            } else if (actionPlanData.getAllotDseAnswer() != null && actionPlanData.getAllotDseAnswer().is_synced()) {
                holder.frameOffline.setVisibility(View.GONE);
                holder.relActionPlanBottom.setVisibility(View.GONE);
            }
            else if(pref.getAppUserId().intValue()!=actionPlanData.getDseId().intValue()){
                holder.frameOffline.setVisibility(View.GONE);
                holder.relActionPlanBottom.setVisibility(View.GONE);
            }
            else if(actionPlanData.getFormAnswerDB() == null){
                holder.frameOffline.setVisibility(View.GONE);
                holder.relActionPlanBottom.setVisibility(View.GONE);
            }
            else {
                String title = "View Update";
                if(actionPlanData.getCreateLeadInputDataDB()!=null){
                  title = "Lead Created Offline";
                }
                else if (actionPlanData.getActivityId()==WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY_ID) {
                    title = "New Task Created";
                } else if (actionPlanData.getActivityId()==WSConstants.TaskActivityName.ALLOT_DSE_ID) {
                    title = "Dse Allotted";
                } else if (actionPlanData.getFormAnswerDB() != null) {
                    if (Util.getInt(actionPlanData.getFormAnswerDB().getAction_id()) == WSConstants.FormAction.PRE_DONE) {
                        System.out.println("Done Action Id:" + actionPlanData.getScheduledActivityId());
                        System.out.println("Done Action Id OLD:" + actionPlanData.getOldScheduledActivityId());
                        title = "Form submitted";
                    } else if (Util.getInt(actionPlanData.getFormAnswerDB().getAction_id()) == WSConstants.FormAction.RESCHEDULE) {
                        title = "Task Rescheduled";
                    }
                }
                holder.tvOfflineTaskName.setText(title);
                holder.frameOffline.setVisibility(View.VISIBLE);
                holder.relActionPlanBottom.setVisibility(View.VISIBLE);
            }

            holder.llActionPlanMain.setBackgroundColor(Color.parseColor("#D5D9E6"));
//             // holder.ibReSchedule.setBackground(R.drawable.mail_background_oval);

            if(actionPlanData.isCreatedOffline()
                    && actionPlanData.getCreateLeadInputDataDB()!=null
                    && actionPlanData.getCreateLeadInputDataDB().getLead_data()!=null){
                holder.tvActionPlanMrMs.setText(Util.getGender(actionPlanData.getCreateLeadInputDataDB().getLead_data().getCustomer_details().getTitle()));
                String name = actionPlanData.getCreateLeadInputDataDB().getLead_data().getCustomer_details().getFirst_name()
                        +" "+actionPlanData.getCreateLeadInputDataDB().getLead_data().getCustomer_details().getLast_name();
                if(!Util.isNotNull(actionPlanData.getCreateLeadInputDataDB().getLead_data().getCustomer_details().getFirst_name())){
                    name = "Customer Name";
                }
                if (Util.isNotNull(name)) {
                    holder.tvActionPlanPerName.setText(String.format("%s%s", name.substring(0, 1).toUpperCase(), name.substring(1)));
                }
                setTagColorId(holder,actionPlanData.getCreateLeadInputDataDB().getLead_data().getLead_tag_id());
                if(actionPlanData.getCreateLeadInputDataDB().getActivity_data().getScheduledDateTime()!=null){
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm,a,dd/MM,E", Locale.getDefault());
                    String formattedDate = dateFormatter.format(Util.getDate(actionPlanData.getCreateLeadInputDataDB().getActivity_data().getScheduledDateTime()));
                    String[] dateSplits = formattedDate.split(",");
                    if (dateSplits.length == 4) {
                        holder.tvActionPlanTime.setText(dateSplits[0]);
                        holder.tvActionPlanAmPm.setText(dateSplits[1].toLowerCase());
                       /* holder.tvActionPlanDate.setText(dateSplits[2]);
                        holder.tvActionPlanDay.setText(dateSplits[3].toUpperCase());*/
                    }

                }
                else {
                    SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm,a,dd/MM,E", Locale.getDefault());
                    String formattedDate = dateFormatter.format(actionPlanData.getCreateLeadInputDataDB().getDataCreatedDate().getTime());
                    String[] dateSplits = formattedDate.split(",");
                    if (dateSplits.length == 4) {
                        holder.tvActionPlanTime.setText(dateSplits[0]);
                        holder.tvActionPlanAmPm.setText(dateSplits[1].toLowerCase());
                      /*  holder.tvActionPlanDate.setText(dateSplits[2]);
                        holder.tvActionPlanDay.setText(dateSplits[3].toUpperCase());*/
                    }
                }
                if(actionPlanData.getCreateLeadInputDataDB().getActivity_data().getActivityName()!=null){
                    holder.tvActionPlanAction.setText(actionPlanData.getCreateLeadInputDataDB().getActivity_data().getActivityName());
                }
                else if(actionPlanData.getCreateLeadInputDataDB().getLead_data().getUser_details().size()==0){
                    holder.tvActionPlanAction.setText(WSConstants.TaskActivityName.ALLOT_DSE);
                }
                else {
                    holder.tvActionPlanAction.setText(WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY);
                }

            }

            else {
            holder.tvActionPlanMrMs.setText(Util.getGender(actionPlanData.getTitle()));
                String name = actionPlanData.getFirstName()
                        +" "+actionPlanData.getLastName();
                if(!Util.isNotNull(actionPlanData.getFirstName())){
                    name = "Customer Name";
                }
                if (Util.isNotNull(name)) {
                    holder.tvActionPlanPerName.setText(String.format("%s%s", name.substring(0, 1).toUpperCase(), name.substring(1)));
                }
                System.out.println("LeadId:"+actionPlanData.getLeadId());

            if (Util.isNotNull(actionPlanData.getResidenceAddress())) {
                holder.tvActionPlanAddress.setVisibility(View.VISIBLE);
                holder.addressViewHr.setVisibility(View.VISIBLE);
                holder.tvActionPlanAddress.setText(actionPlanData.getResidenceAddress());
            } else if (Util.isNotNull(actionPlanData.getOfficeAddress())) {
                holder.tvActionPlanAddress.setVisibility(View.VISIBLE);
                holder.addressViewHr.setVisibility(View.VISIBLE);
                holder.tvActionPlanAddress.setText(actionPlanData.getOfficeAddress());

            } else {
                holder.tvActionPlanAddress.setVisibility(View.GONE);
                holder.addressViewHr.setVisibility(View.GONE);
                holder.tvActionPlanAddress.setText("No Address");

            }


            holder.tvActionPlanActionStage.setText(actionPlanData.getLeadStage());

            if (Util.isNotNull(actionPlanData.getLeadCarModelName())) {
                holder.tvActionPlanCar.setVisibility(View.VISIBLE);
                holder.carViewHr.setVisibility(View.VISIBLE);
                //holder.tvActionPlanCar.setText(actionPlanData.getLeadCarModelName());
                if(actionPlanData.getLeadCarVariantName()==null){
                    holder.tvActionPlanCar.setText(actionPlanData.getLeadCarModelName());
                }else{
                    holder.tvActionPlanCar.setText(actionPlanData.getLeadCarVariantName());
                }
            } else {
                holder.tvActionPlanCar.setVisibility(View.GONE);
                holder.carViewHr.setVisibility(View.GONE);
                holder.tvActionPlanCar.setText("No Car");
            }

            SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm,a,dd/MM,E", Locale.getDefault());
            String formattedDate = dateFormatter.format(actionPlanData.getActivityScheduleDate().getTime());
            String[] dateSplits = formattedDate.split(",");
            if (dateSplits.length == 4) {
                holder.tvActionPlanTime.setText(dateSplits[0]);
                holder.tvActionPlanAmPm.setText(dateSplits[1].toLowerCase());
                holder.tvActionPlanDate.setText(dateSplits[2]);
                holder.tvActionPlanDay.setText(dateSplits[3].toUpperCase());
            }

                setTagColor(holder, actionPlanData.getLeadTagsName());
            holder.tvActionPlanAction.setText(actionPlanData.getActivityName());

        }

            //Tag Color


            holder.relActionPlanRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(actionPlanData.isCreatedOffline()) {
                        Intent intent = new Intent(context, C360ActivityOffline.class);
                        intent.putExtra("scheduled_activity_id", actionPlanData.getScheduledActivityId());
                        context.startActivity(intent);
                    }
                    else{
                        pref.setLeadID("" + actionPlanData.getLeadId());
                        pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                        if(actionPlanData.getOldScheduledActivityId()>0){
                            pref.setScheduledActivityId(actionPlanData.getOldScheduledActivityId());
                        }
                        Intent c360Intent = new Intent(context, C360Activity.class);
                        context.startActivity(c360Intent);
                    }
                }
            });

            holder.relActionPlanBottom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(actionPlanData.isCreatedOffline()){
                        Intent intent = new Intent(context, C360ActivityOffline.class);
                        intent.putExtra("scheduled_activity_id",actionPlanData.getScheduledActivityId());
                        context.startActivity(intent);
                    }
                    else {
                        pref.setLeadID("" + actionPlanData.getLeadId());
                        pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                        if (actionPlanData.getActivityId()==WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY_ID) {
                            Intent formRenderingActivityIntent = new Intent(context, FormRenderingActivity.class);
                            formRenderingActivityIntent.putExtra("form_title", "Add activity");
                            formRenderingActivityIntent.putExtra("form_action", "Save");
                            formRenderingActivityIntent.putExtra("action_id", WSConstants.FormAction.ADD_ACTIVITY);
                            formRenderingActivityIntent.putExtra("scheduled_type", 2);
                            formRenderingActivityIntent.putExtra("from_done", true);
                            context.startActivity(formRenderingActivityIntent);
                        } else if (actionPlanData.getActivityId()==WSConstants.TaskActivityName.ALLOT_DSE_ID) {
                            //Open Allot dse form
                            Intent intent = new Intent(context, AllotDseActivity.class);
                            intent.putExtra("lead_id", actionPlanData.getLeadId());
                            intent.putExtra("lead_last_updated", actionPlanData.getLeadLastUpdated());
                            intent.putExtra("from_done", true);
                            context.startActivity(intent);

                        } else {
                            if (actionPlanData.getFormAnswerDB() != null) {

                                switch (actionPlanData.getFormQuestionDB().getAction_id()) {
                                    case WSConstants.FormAction.PRE_DONE:
                                        //Form submitted
                                        pref.setScheduledActivityId(actionPlanData.getScheduledActivityId());
                                        Intent formRenderingActivityIntent = new Intent(context, FormRenderingActivity.class);
                                        formRenderingActivityIntent.putExtra("form_title", "Done");
                                        formRenderingActivityIntent.putExtra("form_action", "Update");
                                        formRenderingActivityIntent.putExtra("action_id", WSConstants.FormAction.PRE_DONE);
                                        formRenderingActivityIntent.putExtra("from_done", true);
                                        context.startActivity(formRenderingActivityIntent);
                                        break;
                                    case WSConstants.FormAction.RESCHEDULE:
                                        pref.setLeadID("" + actionPlanData.getLeadId());
                                        pref.setScheduledActivityId(actionPlanData.getOldScheduledActivityId());
                                        Intent formRenderingActivityIntentSc = new Intent(context, FormRenderingActivity.class);
                                        formRenderingActivityIntentSc.putExtra("form_action", "Update");
                                        formRenderingActivityIntentSc.putExtra("form_title", "Reschedule");
                                        formRenderingActivityIntentSc.putExtra("action_id", WSConstants.FormAction.RESCHEDULE);
                                        formRenderingActivityIntentSc.putExtra("from_done", true);
                                        context.startActivity(formRenderingActivityIntentSc);
                                        break;
                                }
                            } else {
                                Toast.makeText(context, "Sorry you can't view now", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                }
            });

            holder.ibReSchedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                }
            });







    }
    }

   /* //Firebase Analytics
    private void callAnalytics(String key, String value, String event) {
        Bundle params = new Bundle();
        params.putString(key, value);
        SalesCRMApplication.getFirebaseAnalytics().logEvent(event, params);
    }*/

    private void setBackground(ImageButton imageButton, Drawable drawable, boolean b) {
        imageButton.setEnabled(b);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            imageButton.setBackground(drawable);
        } else {
            imageButton.setBackgroundDrawable(drawable);
        }
    }

    private void setTagColor(DoneActionPlanRealViewHolder viewHolder, String tagName) {
        if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_HOT)) {
            viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.hot));
        } else if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_WARM)) {
            viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.warm));
        } else if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_COLD)) {
            viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cold));
        } else if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_OVERDUE)) {
            viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.overdue));

        }
    }
    private void setTagColorId(DoneActionPlanRealViewHolder viewHolder, String tagID) {
        // System.out.println("inter" + indicatorPoint);
        if (tagID.equalsIgnoreCase(WSConstants.LEAD_TAG_HOT_ID)) {
             viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.hot));
        } else if (tagID.equalsIgnoreCase(WSConstants.LEAD_TAG_WARM_ID)) {
             viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.warm));
        } else if (tagID.equalsIgnoreCase(WSConstants.LEAD_TAG_COLD_ID)) {
             viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cold));
        } else if (tagID.equalsIgnoreCase(WSConstants.LEAD_TAG_OVERDUE_ID)) {
             viewHolder.cardActionTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.overdue));

        }
    }



    class DoneActionPlanRealViewHolder extends RecyclerView.ViewHolder {
        private final RelativeLayout relLeadDetails;
        private final RelativeLayout frameOffline;
        public View view;
        public ImageView imgCardStatus, imgMultipleOptions;
        public int currentItem;
        RelativeLayout relActionPlanLeft, relActionPlanRight, relActionPlanBottom;
        ImageButton ibControlBottom, ib360Profile, ibMail, ibReSchedule, ibCall, ibDone;
        TextView tvReSchedule,tvDone,tvOfflineTaskName;
        TextView tvActionPlanTime, tvActionPlanAmPm, tvActionPlanPerName, tvActionPlanMrMs, tvActionPlanAction, tvActionPlanHeader;
        ////////------------------
        TextView tvActionPlanActionStage, tvActionPlanRemarks, tvActionPlanDate, tvActionPlanDay, tvActionPlanCar, tvActionPlanAddress;
        LinearLayout llActionButtonsHolder,llActionPlanMain;
        ////////-----------------
        private CardView cardActionTag;
        private LinearLayout remarksDateLinear;
        private View carViewHr, addressViewHr,remarksViewHr;

        DoneActionPlanRealViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            view = itemLayoutView;

            //------------New Layout action_plan_card

//            cardViewTag=(CardView) itemLayoutView.findViewById(R.id.card_action_plan_tag);
            tvActionPlanHeader = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_header);
            relActionPlanLeft = (RelativeLayout) itemLayoutView.findViewById(R.id.rel_action_plan_left);
            relActionPlanRight = (RelativeLayout) itemLayoutView.findViewById(R.id.rel_action_plan_right);
            relActionPlanBottom = (RelativeLayout) itemLayoutView.findViewById(R.id.rel_action_plan_bottom);
            ibControlBottom = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_bottom_control);
            ib360Profile = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_go_360);
            ibMail = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_mail);
            ibCall = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_call);
            ibReSchedule = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_reschedule);
            tvReSchedule = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_reschedule);
            ibDone = (ImageButton) itemLayoutView.findViewById(R.id.img_action_plan_done);
            tvDone = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_done);
            tvActionPlanTime = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_time);
            tvActionPlanAmPm = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_time_am);
            tvActionPlanPerName = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_name);
            tvActionPlanMrMs = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_name_mr);
            tvActionPlanMrMs = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_name_mr);
            tvActionPlanAction = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_action);
            tvActionPlanActionStage = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_action_stage);
            tvActionPlanRemarks = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_remarks);
            tvActionPlanDate = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_date);
            tvActionPlanDay = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_day);
            tvActionPlanCar = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_car);
            tvActionPlanAddress = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_address);
            llActionButtonsHolder = (LinearLayout) itemLayoutView.findViewById(R.id.ll_action_plan_bottom_bts);
            llActionPlanMain = (LinearLayout) itemLayoutView.findViewById(R.id.ll_action_plan_main);
            cardActionTag = (CardView) itemLayoutView.findViewById(R.id.card_action_plan_top);
            carViewHr = itemLayoutView.findViewById(R.id.view_action_plan_hr_1);
            addressViewHr = itemLayoutView.findViewById(R.id.view_action_plan_hr_2);
            remarksViewHr = itemLayoutView.findViewById(R.id.view_action_plan_hr);
            remarksDateLinear = (LinearLayout) itemLayoutView.findViewById(R.id.linearLayout);
            relLeadDetails = (RelativeLayout) itemLayoutView.findViewById(R.id.rel_action_plan_details);
            frameOffline = (RelativeLayout) itemLayoutView.findViewById(R.id.frame_action_plan_offline);
            tvOfflineTaskName = (TextView) itemLayoutView.findViewById(R.id.tv_action_plan_offline);


        }


    }
}