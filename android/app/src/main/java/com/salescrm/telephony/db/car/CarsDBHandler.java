package com.salescrm.telephony.db.car;

import com.salescrm.telephony.db.IntrestedCarDetails;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.crm_user.EvaluatorsDB;
import com.salescrm.telephony.model.ExchangeCarEditModel;
import com.salescrm.telephony.model.booking.ApiInputBookingDetails;
import com.salescrm.telephony.model.booking.ApiInputCarDetails;
import com.salescrm.telephony.model.booking.ApiInputCustomerDetails;
import com.salescrm.telephony.model.booking.ApiInputDiscount;
import com.salescrm.telephony.model.booking.ApiInputExchFinDetails;
import com.salescrm.telephony.model.booking.ApiInputPriceBreakup;
import com.salescrm.telephony.model.booking.ApiInputVinAllocationDetails;
import com.salescrm.telephony.views.CustomDialogs;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by bannhi on 16/6/17.
 */

public class CarsDBHandler {
    //Context context;
    AllInterestedCars allInterestedCar;
    static CarsDBHandler newInstance = new CarsDBHandler();

    public static CarsDBHandler getInstance() {
        //  context = ctx;
        if (newInstance == null)
            newInstance = new CarsDBHandler();
        return newInstance;
    }

    private CarsDBHandler() {

    }

    /**
     * update exchange editted car
     */
    public void updateExchangeEdittedCar(Realm realm, ExchangeCarEditModel exchangeCarEditModel) {
        ExchangeCarDetails exchangeCarDetailObj = realm.where(ExchangeCarDetails.class)
                .equalTo("lead_exchange_car_id", exchangeCarEditModel.getCar().getLead_exchange_car_id())
                .findFirst();

        if (exchangeCarDetailObj != null) {
            exchangeCarDetailObj.setExpected_price(exchangeCarEditModel.getCar().getExpected_price());
            exchangeCarDetailObj.setMarket_price(exchangeCarEditModel.getCar().getMarket_price());
            exchangeCarDetailObj.setPrice_quoted(exchangeCarEditModel.getCar().getPrice_quoted());
            exchangeCarDetailObj.setPurchase_date(exchangeCarEditModel.getCar().getPurchase_date());
            exchangeCarDetailObj.setKms_run(exchangeCarEditModel.getCar().getKms_run());
            exchangeCarDetailObj.setReg_no(exchangeCarEditModel.getCar().getReg_no());

        }
        realm.copyToRealmOrUpdate(exchangeCarDetailObj);
    }

    public void updateDbAfterBookCar(Realm realm, String enquiryId, String bookingNumber, String bookingId, String nextStageId,
                                     String carId, String amtRecvd) {
        allInterestedCar = realm.where(AllInterestedCars.class)
                .equalTo("carId", carId)
                .findFirst();
        allInterestedCar.setEnquiry_id(enquiryId);
        allInterestedCar.setBooking_id(bookingId);
        allInterestedCar.setBooking_number(bookingNumber);
        allInterestedCar.setBooking_amount(amtRecvd);
        if (!nextStageId.isEmpty()) {
            allInterestedCar.setCar_stage_id(nextStageId.trim());
        }


    }

    /**
     * @param realm
     * @param selectedVariant
     * @param selectedVariantId
     * @param selectedColorId
     * @param selectedColor
     * @param selectedFuelId
     * @param selectedFuel
     * @param presentCarId
     * @param selectedBookingId
     * @param amtRecvd
     */
    public void updateEdittedCarToDB(Realm realm, String selectedVariant, String selectedVariantId, String selectedColorId,
                                     String selectedColor, final String selectedFuelId, final String selectedFuel,
                                     String presentCarId, String selectedBookingId, final String amtRecvd) {

        allInterestedCar = realm.where(AllInterestedCars.class)
                .equalTo("carId", presentCarId)
                .findFirst();

        if (allInterestedCar != null) {
            allInterestedCar.setIntrestedCarVariantCode(selectedVariant);
            allInterestedCar.setIntrestedCarVariantId(selectedVariantId);
            allInterestedCar.setIntrestedCarColorId(selectedColorId);
            allInterestedCar.setBooking_id(selectedBookingId);
            allInterestedCar.setFuelId(selectedFuelId);
            allInterestedCar.setFuelType(selectedFuel);
            allInterestedCar.setBooking_amount(amtRecvd);

        }
        //realm.copyToRealmOrUpdate(allInterestedCar);

    }

    /**
     * create master list for cars to be shown
     *
     * @param realm
     * @param apiInputBookingDetails
     */

    public void createAllCarsList(Realm realm, IntrestedCarDetails intrestedCarDetails, String customerName,
                                  List<ApiInputBookingDetails> apiInputBookingDetails) {
        String primaryKeyCarId = "0";
        // for(int i = 0; i< intrestedCarDetailsRealmList.size(); i ++){
        //  IntrestedCarDetails intrestedCarDetails = intrestedCarDetailsRealmList.get(i);

        AllInterestedCars allInterestedCars = new AllInterestedCars();
        allInterestedCars.setFuelType(intrestedCarDetails.getFuelType());
        allInterestedCars.setFuelId(intrestedCarDetails.getFuelId());
        allInterestedCars.setIntrestedLeadCarId(intrestedCarDetails.getIntrestedLeadCarId());
        allInterestedCars.setLeadId(intrestedCarDetails.getLeadId());
        allInterestedCars.setIntrestedCarName(intrestedCarDetails.getIntrestedCarName());
        allInterestedCars.setIntrestedCarVariantCode(intrestedCarDetails.getIntrestedCarVariantCode());
        allInterestedCars.setIntrestedCarVariantId(intrestedCarDetails.getIntrestedCarVariantId());
        allInterestedCars.setIntrestedCarColorId(intrestedCarDetails.getIntrestedCarColorId());
        allInterestedCars.setIntrestedCarModelId(intrestedCarDetails.getIntrestedCarModelId());
        allInterestedCars.setIntrestedCarIsPrimary(intrestedCarDetails.getIntrestedCarIsPrimary());
        allInterestedCars.setTestDriveConducted(intrestedCarDetails.getTestDriveStatus());
        allInterestedCars.setCustomerName(customerName);
        allInterestedCars.setModel(intrestedCarDetails.getIntrestedModelName());
        allInterestedCars.setColor(intrestedCarDetails.getColor());
        allInterestedCars.setVariant(intrestedCarDetails.getVariant());
        allInterestedCars.setIsPrimary(intrestedCarDetails.getIntrestedCarIsPrimary());
        if (intrestedCarDetails.getCarDmsDataList() != null && intrestedCarDetails.getCarDmsDataList().size() > 0) {

            for (int k = 0; k < intrestedCarDetails.getCarDmsDataList().size(); k++) {
                CarDmsDataDB carDmsDataDB = intrestedCarDetails.getCarDmsDataList().get(k);
                primaryKeyCarId = intrestedCarDetails.getIntrestedLeadCarId() + carDmsDataDB.getEnquiry_id();
                allInterestedCars.setCarId(primaryKeyCarId);
                allInterestedCars.setEnquiry_id(carDmsDataDB.getEnquiry_id());
                allInterestedCars.setEnquiry_number(carDmsDataDB.getEnquiry_number());
                allInterestedCars.setBooking_id(carDmsDataDB.getBooking_id());
                allInterestedCars.setBooking_number(carDmsDataDB.getBooking_number());
                allInterestedCars.setLocation_code(carDmsDataDB.getLocation_code());
                allInterestedCars.setExpected_delivery_date(carDmsDataDB.getExpected_delivery_date());
                allInterestedCars.setCar_stage_id(carDmsDataDB.getCar_stage_id());
                allInterestedCars.setBooking_amount(carDmsDataDB.getBooking_amount());
                allInterestedCars.setDeliveryDate(carDmsDataDB.getDelivery_date());
                allInterestedCars.setDeliveryChallan(carDmsDataDB.getDelivery_challan());

                if( k< apiInputBookingDetails.size()) {
                    ApiInputBookingDetails bookingDetails = apiInputBookingDetails.get(k);
                    if (bookingDetails != null) {

                        allInterestedCars.setVin_allocation_status_id(bookingDetails.getVin_allocation_status_id());
                        allInterestedCars.setVin_no(bookingDetails.getVin_no());
                        allInterestedCars.setVin_allocation_status(bookingDetails.getVin_allocation_status());

                        ApiInputCustomerDetails apiInputCustomerDetails = bookingDetails.getCustomer_details();
                        ApiInputPriceBreakup price_breakup = bookingDetails.getPrice_breakup();
                        ApiInputDiscount discounts = bookingDetails.getDiscounts();
                        ApiInputExchFinDetails exchFin = bookingDetails.getPayment_details();
                        ApiInputVinAllocationDetails vinAllocationDetails = bookingDetails.getVin_allocation_details();

                        if (apiInputCustomerDetails != null) {
                            BookingCustomerDB bookingCustomerDB = new BookingCustomerDB();
                            bookingCustomerDB.setBookingId(carDmsDataDB.getBooking_id());
                            bookingCustomerDB.setBookingName(apiInputCustomerDetails.getBooking_name());
                            bookingCustomerDB.setDateOfBirth(apiInputCustomerDetails.getDob());
                            bookingCustomerDB.setAddress(apiInputCustomerDetails.getAddress());
                            bookingCustomerDB.setPinCode(apiInputCustomerDetails.getPin_code());
                            bookingCustomerDB.setCareOfType(apiInputCustomerDetails.getCare_of_type());
                            bookingCustomerDB.setCareOf(apiInputCustomerDetails.getCare_of());
                            bookingCustomerDB.setCustomerType(0); /*Invalid from the API*/
                            bookingCustomerDB.setPanNumber(apiInputCustomerDetails.getPan());
                            bookingCustomerDB.setAadhaar(apiInputCustomerDetails.getAadhaar());
                            bookingCustomerDB.setGst(apiInputCustomerDetails.getGst());
                            bookingCustomerDB.setBuyerType(apiInputCustomerDetails.getBuyer_type_id()); /*Invalid coming from the API*/
                            allInterestedCars.setBookingCustomerDB(bookingCustomerDB);
                        }

                        if(price_breakup!=null) {
                            BookingPriceBreakupDB bookingPriceBreakupDB = new BookingPriceBreakupDB();
                            bookingPriceBreakupDB.setBookingId(carDmsDataDB.getBooking_id());
                            bookingPriceBreakupDB.setExShowRoomPrice(price_breakup.getEx_showroom());
                            bookingPriceBreakupDB.setInsurance(price_breakup.getInsurance());
                            bookingPriceBreakupDB.setRegistration(price_breakup.getRegistration());
                            bookingPriceBreakupDB.setAccessories(price_breakup.getAccessories_sold());
                            bookingPriceBreakupDB.setExtendedWarranty(price_breakup.getEw());
                            bookingPriceBreakupDB.setOthers(price_breakup.getOthers());
                            allInterestedCars.setBookingPriceBreakupDB(bookingPriceBreakupDB);

                        }

                        if(discounts!=null) {
                            BookingDiscountDB bookingDiscountDB = new BookingDiscountDB();
                            bookingDiscountDB.setBookingId(carDmsDataDB.getBooking_id());
                            bookingDiscountDB.setCorporate(discounts.getCorporate());
                            bookingDiscountDB.setExchangeBonus(discounts.getExchange_bonus());
                            bookingDiscountDB.setLoyaltyBonus(discounts.getLoyalty_bonus());
                            bookingDiscountDB.setOemScheme(discounts.getOem_scheme());
                            bookingDiscountDB.setDealer(discounts.getDealer());
                            bookingDiscountDB.setAccessories(discounts.getFree_accessories());
                            allInterestedCars.setBookingDiscountDB(bookingDiscountDB);

                        }
                        if(exchFin!=null) {
                            BookingExchFinDB bookingExchFinDB = new BookingExchFinDB();
                            bookingExchFinDB.setExchange(exchFin.getExchange());
                            bookingExchFinDB.setFinance(exchFin.getFinance());
                            allInterestedCars.setExchFinDB(bookingExchFinDB);

                        }

                        if(vinAllocationDetails!= null){
                            BookingAllocationDB bookingAllocationDB = new BookingAllocationDB();
                            bookingAllocationDB.setVin_no(vinAllocationDetails.getVin_no());
                            bookingAllocationDB.setVin_allocation_status(vinAllocationDetails.getVin_allocation_status());
                            bookingAllocationDB.setVin_allocation_status_id(vinAllocationDetails.getVin_allocation_status_id());
                            allInterestedCars.setBookingAllocationDB(bookingAllocationDB);
                        }

                    }
                }

             /*   if (allInterestedCars.getInvoiceDetails() != null) {
                    allInterestedCars.getInvoiceDetails().setInvoice_number(carDmsDataDB.getInvoice_number());

                    if (carDmsDataDB.getInvoice_id() != null && !carDmsDataDB.getInvoice_id().isEmpty()) {
                        allInterestedCars.getInvoiceDetails().setInvoiceId(Integer.parseInt(carDmsDataDB.getInvoice_id()));
                    }
                    allInterestedCars.getInvoiceDetails().setInvoice_date(carDmsDataDB.getInvoice_date());
                    allInterestedCars.getInvoiceDetails().setInvoice_name(carDmsDataDB.getInvoice_name());
                    allInterestedCars.getInvoiceDetails().setInvoice_mob_no(carDmsDataDB.getInvoice_mobile_no());
                    allInterestedCars.getInvoiceDetails().setVin_no(carDmsDataDB.getInvoice_vin_no());


                } else {

                }*/
                InvoiceDetailsDB invoiceDetails = new InvoiceDetailsDB();
                if (carDmsDataDB.getInvoice_id() != null && !carDmsDataDB.getInvoice_id().isEmpty()) {
                    invoiceDetails.setInvoiceId(Integer.parseInt(carDmsDataDB.getInvoice_id()));
                }
                invoiceDetails.setInvoice_number(carDmsDataDB.getInvoice_number());
                invoiceDetails.setInvoice_name(carDmsDataDB.getInvoice_name());
                invoiceDetails.setInvoice_date(carDmsDataDB.getInvoice_date());

                invoiceDetails.setInvoice_mob_no(carDmsDataDB.getInvoice_mobile_no());
                invoiceDetails.setVin_no(carDmsDataDB.getInvoice_vin_no());
                allInterestedCars.setInvoiceDetails(invoiceDetails);


                realm.copyToRealmOrUpdate(allInterestedCars);
            }
        } else {
            allInterestedCars.setCar_stage_id("0");
            primaryKeyCarId = "" + intrestedCarDetails.getIntrestedLeadCarId();
            allInterestedCars.setCarId(primaryKeyCarId);
            realm.copyToRealmOrUpdate(allInterestedCars);
        }


        // }
    }

   /*
     * getting all possible car variants
     * @param realm
     * @param modelId
     * @return
     */

    public String[] getCarVariants(Realm realm, String modelId) {
        CarBrandModelsDB mCarBrandModelsDB = realm.where(CarBrandModelsDB.class)
                .equalTo("model_id", modelId)
                .findFirst();
        List<String> carVariantNamesTempList = new ArrayList<String>();
       // String[] carVariantNamesList = new String[mCarBrandModelsDB.getCar_variants().size()];
        CustomDialogs.variantIdsList.clear();
        for (int k = 0; k < mCarBrandModelsDB.getCar_variants().size(); k++) {
            if(!CustomDialogs.variantIdsList.contains(mCarBrandModelsDB.getCar_variants().get(k).getVariant_id())) {
                carVariantNamesTempList.add(mCarBrandModelsDB.getCar_variants().get(k).getVariant());
                CustomDialogs.variantIdsList.add(mCarBrandModelsDB.getCar_variants().get(k).getVariant_id());
            }

        }
        String[] carVariantNamesList = new String[carVariantNamesTempList.size()];
        for (int j = 0; j < carVariantNamesTempList.size(); j++) {
            carVariantNamesList[j] = (carVariantNamesTempList.get(j));
        }
        return carVariantNamesList;
    }


    /**
     * getting all possible car colors
     *
     * @param realm
     * @param modelId
     * @return
     */
    public String[] getColors(Realm realm, String modelId, String selectedVariantId) {
        System.out.println("COLOR 0 - "+ modelId + " "+ selectedVariantId);
        CarBrandModelsDB mCarBrandModelsDB = realm.where(CarBrandModelsDB.class)
                .equalTo("model_id", modelId)
                .findFirst();
        System.out.println("COLOR 1 - "+mCarBrandModelsDB.getCar_variants().toString());
        List<String> colorTempList = new ArrayList<String>();
        colorTempList.clear();
        CustomDialogs.colorIdsList.clear();
        for (int i = 0; i < mCarBrandModelsDB.getCar_variants().size(); i++) {
            if (mCarBrandModelsDB.getCar_variants().get(i).getVariant_id().equalsIgnoreCase(selectedVariantId) &&
                    (mCarBrandModelsDB.getCar_variants().get(i).getColor()!=null &&
                            !mCarBrandModelsDB.getCar_variants().get(i).getColor().isEmpty()
                            && i < mCarBrandModelsDB.getCar_variants().size() ) ) {
                System.out.println("COLOR 00 - "+mCarBrandModelsDB.getCar_variants().get(i).getColor().toString());
                colorTempList.add(mCarBrandModelsDB.getCar_variants().get(i).getColor());
                CustomDialogs.colorIdsList.add(mCarBrandModelsDB.getCar_variants().get(i).getColor_id());
           }
        }
        //)
                   // && !CustomDialogs.colorIdsList.contains(mCarBrandModelsDB.getCar_variants().get(i).getColor_id()
        System.out.println("COLOR 3 - "+colorTempList.toString());
        String[] colorsNameList = new String[colorTempList.size()];
        for (int k = 0; k < colorTempList.size(); k++) {
            colorsNameList[k] = (colorTempList.get(k));
        }
        System.out.println("COLOR 4 - "+colorsNameList.toString());
        return colorsNameList;
    }


    /**
     * getting all possible car fuel types
     *
     * @param realm
     * @param modelId
     * @return
     */
    public String[] getFuelList(Realm realm, String modelId, String selectedVariantId) {
        CarBrandModelsDB mCarBrandModelsDB = realm.where(CarBrandModelsDB.class)
                .equalTo("model_id", modelId)
                .findFirst();
        CustomDialogs.fuelIdsList.clear();
        List<String> fuelTypeTempList = new ArrayList<String>();
        for (int i = 0; i < mCarBrandModelsDB.getCar_variants().size(); i++) {
            if (mCarBrandModelsDB.getCar_variants().get(i).getVariant_id().equalsIgnoreCase(selectedVariantId)
                    && !fuelTypeTempList.contains(mCarBrandModelsDB.getCar_variants().get(i).getFuel_type())) {
                fuelTypeTempList.add(mCarBrandModelsDB.getCar_variants().get(i).getFuel_type());
                CustomDialogs.fuelIdsList.add(mCarBrandModelsDB.getCar_variants().get(i).getFuel_type_id());
            }
        }

        String[] fuelNamesList = new String[fuelTypeTempList.size()];
        for (int k = 0; k < fuelTypeTempList.size(); k++) {
            fuelNamesList[k] = (fuelTypeTempList.get(k));
        }
        return fuelNamesList;
    }

    public String[] getEvaluatorNameList(Realm realm) {

        RealmList<EvaluatorsDB> evaluatorsDBList = new RealmList<>();
        evaluatorsDBList.addAll(realm.where(EvaluatorsDB.class)
                .findAll());
        String[] evaluarorNameList = new String[evaluatorsDBList.size()];
        CustomDialogs.evaluatorIdList.clear();
        for (int k = 0; k < evaluatorsDBList.size(); k++) {
            evaluarorNameList[k] = (evaluatorsDBList.get(k).getEvaluatorName());
            CustomDialogs.evaluatorIdList.add(evaluatorsDBList.get(k).getEvaluatorId());

        }
        return evaluarorNameList;
    }

    public String[] getShowroomsForEvaluation(Realm realm) {
        RealmList<UserLocationsDB>locationsDBList = new RealmList<>();
        locationsDBList.addAll(realm.where(UserLocationsDB.class)
                .distinct("location_id"));

        String[] showRoomList = new String[locationsDBList.size()];
        CustomDialogs.showRoomIdList.clear();
        for (int k = 0; k < locationsDBList.size(); k++) {
            showRoomList[k] = (locationsDBList.get(k).getName());
            CustomDialogs.showRoomIdList.add(locationsDBList.get(k).getLocation_id());

        }
        return showRoomList;

    }
}
