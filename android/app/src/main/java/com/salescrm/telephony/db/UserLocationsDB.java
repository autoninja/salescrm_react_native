package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bharath on 17/11/16.
 */
public class UserLocationsDB extends RealmObject{

    private String location_id;

    private String name;

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+location_id+", name = "+name+"]";
    }
}
