package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 16-08-2016.
 */
public class CustomerPhoneNumbers extends RealmObject {

    public static final String CUSTOMERPHONENUMBERS = "CustomerPhoneNumbers";

    @PrimaryKey
    private String phoneNumberID;
    private int customerID;
    private long phoneNumber;
    private Long leadId;
    private String phoneNumberStatus;
    private String lead_phone_mapping_id;

    public String getLead_phone_mapping_id() {
        return lead_phone_mapping_id;
    }

    public void setLead_phone_mapping_id(String lead_phone_mapping_id) {
        this.lead_phone_mapping_id = lead_phone_mapping_id;
    }


    public String getPhoneNumberID() {
        return phoneNumberID;
    }

    public void setPhoneNumberID(String phoneNumberID) {
        this.phoneNumberID = phoneNumberID;
    }

    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumberStatus() {
        return phoneNumberStatus;
    }

    public void setPhoneNumberStatus(String phoneNumberStatus) {
        this.phoneNumberStatus = phoneNumberStatus;
    }
}
