import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'


export default class SalesILomResultItem extends Component {
  render() {
    if (this.props.showPercentage) {
      return (


        <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#fff', marginTop: 10, alignItems: 'center' }}>

          <View style={{ flex: 1.5, height: '100%' }} >
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#2D4F8B', fontWeight: 'bold' }} >Total%</Text>
            </View>


          </View>


          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#1B075A', fontWeight: 'bold' }} >{this.props.rel.callback}</Text>
            </View>


          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#1B075A', fontWeight: 'bold' }} >{this.props.rel.prospect}</Text>
            </View>

          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#1B075A', fontWeight: 'bold' }} >{this.props.rel.sale_confirmed}</Text>
            </View>

          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#1B075A', fontWeight: 'bold' }} >{this.props.rel.il_done}</Text>
            </View>


          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>

            <Text style={{ color: 'red', fontWeight: 'bold' }} >{this.props.rel.l}</Text>
          </View>


        </View>
      );
    }
    else {
      return (


        <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#fff', marginTop: 10, alignItems: 'center', }}>

          <View style={{ flex: 1.5, height: '100%' }} >
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#2D4F8B', fontWeight: 'bold', }} >Total</Text>
            </View>
          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity
                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Assigned', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                <Text style={{ color: '#1B075A', textDecorationLine: 'underline', fontWeight: 'bold' }} >{this.props.abs.assigned}</Text>
              </TouchableOpacity>
            </View>

          </View>




          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity
                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Call back', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                <Text style={{ color: '#1B075A', textDecorationLine: 'underline', fontWeight: 'bold' }} >{this.props.abs.callback}</Text>
              </TouchableOpacity>
            </View>

          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity
                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Prospect', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                <Text style={{ color: '#1B075A', textDecorationLine: 'underline', fontWeight: 'bold' }} >{this.props.abs.prospect}</Text>
              </TouchableOpacity>
            </View>

           
          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity
                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Sale Confirmed', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                <Text style={{ color: '#1B075A', textDecorationLine: 'underline', fontWeight: 'bold' }} >{this.props.abs.sale_confirmed}</Text>
              </TouchableOpacity>
            </View>

           
          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <TouchableOpacity
                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'IL Done', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                <Text style={{ color: '#1B075A', textDecorationLine: 'underline', fontWeight: 'bold' }} >{this.props.abs.il_done}</Text>
              </TouchableOpacity>
            </View>

           
          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <TouchableOpacity
              hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
              onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Lost / Not Eligible', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

              <Text style={{ color: 'red', textDecorationLine: 'underline', fontWeight: 'bold' }} >{this.props.abs.l}</Text>
            </TouchableOpacity>
          </View>


        </View>

      );
    }
  }
}
