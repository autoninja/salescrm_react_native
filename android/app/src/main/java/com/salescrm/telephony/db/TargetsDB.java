package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bannhi on 11/8/17.
 */

public class TargetsDB extends RealmObject{

    int tlId;
    String tlName;
    int dseId;
    int retailAchieved;
    int retailNo;
    int bookingsAchieved;
    int bookingNo;
    int enquiriesAchieved;
    int enquiryNo;

    public int getTlId() {
        return tlId;
    }

    public void setTlId(int tlId) {
        this.tlId = tlId;
    }

    public String getTlName() {
        return tlName;
    }

    public void setTlName(String tlName) {
        this.tlName = tlName;
    }

    public int getDseId() {
        return dseId;
    }

    public void setDseId(int dseId) {
        this.dseId = dseId;
    }



    public int getRetailAchieved() {
        return retailAchieved;
    }

    public void setRetailAchieved(int retailAchieved) {
        this.retailAchieved = retailAchieved;
    }

    public int getBookingsAchieved() {
        return bookingsAchieved;
    }

    public void setBookingsAchieved(int bookingsAchieved) {
        this.bookingsAchieved = bookingsAchieved;
    }

    public int getEnquiriesAchieved() {
        return enquiriesAchieved;
    }

    public void setEnquiriesAchieved(int enquiriesAchieved) {
        this.enquiriesAchieved = enquiriesAchieved;
    }

    public void setUserId(int userId) {
        this.dseId = userId;
    }

    public int getRetailNo() {
        return retailNo;
    }

    public void setRetailNo(int retailNo) {
        this.retailNo = retailNo;
    }

    public int getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(int bookingNo) {
        this.bookingNo = bookingNo;
    }

    public int getEnquiryNo() {
        return enquiryNo;
    }

    public void setEnquiryNo(int enquiryNo) {
        this.enquiryNo = enquiryNo;
    }
}
