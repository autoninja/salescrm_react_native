package com.salescrm.telephony.offline;

import android.content.Context;

import com.google.gson.Gson;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.etvbr_filter_db.EtvbrFilterDb;
import com.salescrm.telephony.db.etvbr_filter_db.FiltersDb;
import com.salescrm.telephony.db.etvbr_filter_db.SubcategoriesDb;
import com.salescrm.telephony.db.etvbr_filter_db.ValuesDb;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.model.NewFilter.Filter;
import com.salescrm.telephony.model.NewFilter.NewFilter;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.List;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 23/9/17.
 * Call this to get the Etvbr Filters
 */
public class FetchEtvbrFilters implements Callback<NewFilter> {
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;

    public FetchEtvbrFilters(OfflineSupportListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }

    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).fetchFilters(this);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ETVBR_FILTER);

        }
    }


    @Override
    public void success(final NewFilter data, Response response) {
        Util.updateHeaders(response.getHeaders());

            if (data.getResult() != null) {
                System.out.println("Success:1" + data.getMessage());
                System.out.println("EtvbrFilters: " + new Gson().toJson(data.getResult()));
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        insertData(realm, data);
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        listener.onEtvbrFiltersFetched(data);
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ETVBR_FILTER);
                    }
                });


            } else {
                // relLoader.setVisibility(View.GONE);
                System.out.println("Success:2" + data.getMessage());
                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ETVBR_FILTER);
                //showAlert(validateappConfResponse.getMessage());
            }
    }



    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ETVBR_FILTER);

    }

    private void insertData(Realm realm, NewFilter newFilter) {
        WSConstants.API_NOT_CALLED = true;
        emptyDb(realm);
        setDataToDb(realm, newFilter.getResult().getFilters());
    }

    private void setDataToDb(Realm realm, List<Filter> newFilter) {
        EtvbrFilterDb etvbrFilterDb = new EtvbrFilterDb();
        for(int i =0; i<newFilter.size(); i++){
            FiltersDb filters = new FiltersDb();
            filters.setKey(newFilter.get(i).getKey());
            filters.setName(newFilter.get(i).getName());
            for (int j = 0; j<newFilter.get(i).getValues().size(); j++){
                ValuesDb values = new ValuesDb();
                values.setId(newFilter.get(i).getValues().get(j).getId()+"");
                values.setName(newFilter.get(i).getValues().get(j).getName());
                values.setHas_children(newFilter.get(i).getValues().get(j).getHasChildren());
                if(newFilter.get(i).getValues().get(j).getHasChildren()) {
                    for (int k = 0; k < newFilter.get(i).getValues().get(j).getSubcategories().size(); k++) {
                        SubcategoriesDb subcategories = new SubcategoriesDb();
                        subcategories.setId(newFilter.get(i).getValues().get(j).getSubcategories().get(k).getId() + "");
                        subcategories.setName(newFilter.get(i).getValues().get(j).getSubcategories().get(k).getName());
                        values.getSubcategories().add(subcategories);
                    }
                }
                filters.getValues().add(values);
            }
            etvbrFilterDb.getFilters().add(filters);
        }

        realm.copyToRealm(etvbrFilterDb);
    }

    private void emptyDb(Realm realm) {
        realm.delete(EtvbrFilterDb.class);
        realm.delete(FiltersDb.class);
        realm.delete(ValuesDb.class);
        realm.delete(SubcategoriesDb.class);
    }

}
