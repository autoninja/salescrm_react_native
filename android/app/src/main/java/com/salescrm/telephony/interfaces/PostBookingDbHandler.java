package com.salescrm.telephony.interfaces;

/**
 * Created by bharath on 3/7/17.
 */

public interface PostBookingDbHandler {
   void onPostBookingDbHandler(int postBookingType);
}
