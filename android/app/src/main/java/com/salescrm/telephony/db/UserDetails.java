package com.salescrm.telephony.db;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 16-08-2016.
 */
public class UserDetails extends RealmObject {

    public static final String USERDETAILS = "UserDetails";

    @PrimaryKey
    private int userId;

    private String name;
    private String userName;
    private String mobile;
    private String email;
    private String module;
    private String dealer;
    private String otp;
    private boolean storeRecording;
    private boolean otpStore;
    private int crmType;
    private String imgProfileUrl;

    public String getImgProfileUrl() {
        return imgProfileUrl;
    }

    public void setImgProfileUrl(String imgProfileUrl) {
        this.imgProfileUrl = imgProfileUrl;
    }

    private RealmList<UserCarBrands> userCarBrands;

    private RealmList<UserRolesDB> userRoles;

    private RealmList<UserLocationsDB> userLocations;

    public RealmList<UserCarBrands> getUserCarBrands() {
        return userCarBrands;
    }

    public void setUserCarBrands(RealmList<UserCarBrands> userCarBrands) {
        this.userCarBrands = userCarBrands;
    }

    public RealmList<UserRolesDB> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(RealmList<UserRolesDB> userRoles) {
        this.userRoles = userRoles;
    }

    public RealmList<UserLocationsDB> getUserLocations() {
        return userLocations;
    }

    public void setUserLocations(RealmList<UserLocationsDB> userLocations) {
        this.userLocations = userLocations;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public boolean isStoreRecording() {
        return storeRecording;
    }

    public void setStoreRecording(boolean storeRecording) {
        this.storeRecording = storeRecording;
    }

    public boolean isOtpStore() {
        return otpStore;
    }

    public void setOtpStore(boolean otpStore) {
        this.otpStore = otpStore;
    }

    public int getCrmType() {
        return crmType;
    }

    public void setCrmType(int crmType) {
        this.crmType = crmType;
    }
}