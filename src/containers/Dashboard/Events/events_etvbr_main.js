import React, { Component } from 'react'
import {
	Platform, View, Text, Image, Switch, TouchableOpacity, Button,
	Alert, ActivityIndicator, FlatList, StyleSheet, Modal, Picker, NativeModules, TouchableWithoutFeedback,
	DeviceEventEmitter
} from 'react-native'
import { withNavigationFocus } from "react-navigation";
import RestClient from '../../../network/rest_client'
import EventsEtvbrChild from './events_etvbr_child'
import styless from '../../../styles/styles'
import Moment from 'moment';
import PercentageSwitch from '../../../components/uielements/PercentageSwitch'
import { connect } from 'react-redux';
import { updateMainHeaderCurrentView } from '../../../actions/main_header_actions';
import { updateEventDashboard } from '../../../actions/event_update_actions';
import { eventTeamDashboard } from '../../../clevertap/CleverTapConstants';
import { NavigationEvents } from "react-navigation";
import CleverTapPush from '../../../clevertap/CleverTapPush'

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		padding: 4,
		backgroundColor: '#fff'
	},
	etvbrTitle: {
		flex: 1,
		textAlign: 'center',
	},
	locationTitle: {
		flex: 1,
		textAlign: 'left',
		fontSize: 16,
		color: '#007fff',
	},
	etvbrNumbers: {
		textAlign: 'center',
		fontSize: 18,
		textDecorationLine: 'underline',
	},
	etvbrNumberHolder: {
		flex: 1,
		borderLeftColor: '#CFCFCF',
		borderLeftWidth: 0.3,
		justifyContent: 'center',
		alignItems: 'center'
	},
	etvbrTotal: {
		flex: 1,
		textAlign: 'center',
		fontSize: 18,
		fontWeight: 'bold',
		color: '#5E7692',
	},
	etvbrTotalTitle: {
		flex: 1,
		textAlign: 'left',
		fontSize: 16,
		fontWeight: 'bold',
		color: '#5E7692',
		paddingLeft: 5,
	},
	changeCategory: {
		textAlign: 'right',
		fontSize: 16,
		fontWeight: 'bold',
		color: '#2e5e86',
		textDecorationLine: 'underline',
	},
	overlay: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		backgroundColor: 'rgba(0,0,0,0.8)',
		alignItems: 'center',
		justifyContent: 'center',
		padding: 30

	},
	main: {
		backgroundColor: '#fff',
		borderRadius: 5,
		width: '100%',
		maxHeight: '90%',
		padding: 10,
	},
});

const months = [{ "name": "Jan", "value": "01" }, { "name": "Feb", "value": "02" }, { "name": "Mar", "value": "03" }, { "name": "Apr", "value": "04" },
{ "name": "May", "value": "05" }, { "name": "Jun", "value": "06" }, { "name": "Jul", "value": "07" }, { "name": "Aug", "value": "08" },
{ "name": "Sep", "value": "09" }, { "name": "Oct", "value": "10" }, { "name": "Nov", "value": "11" }, { "name": "Dec", "value": "12" }];

const years = ["2019", "2020", "2021"];

FlatListItemSeparator = () => {
	return (
		<View
			style={{
				height: 1,
				width: "100%",
				backgroundColor: "#C0C0C0",

			}}
		/>
	);
}

straightLine = () => {
	return (
		<View
			style={{
				height: 2,
				width: "100%",
				backgroundColor: "#C0C0C0",

			}}
		/>
	);
}

class EventsEtvbr extends Component {

	state = {
		current_view: 'EVENTS_ETVBR',
		isEventTabActive: true,
		update: false,
	}

	constructor(props) {
		super(props);
		DeviceEventEmitter.addListener('onUpdateEvent', (event) => {
			this.makeRemoteRequest(Moment(new Date()).format('MMM') + " " + Moment(new Date()).format('YYYY'));
		});
		this.state = {
			isFetching: false,
			isLoading: false,
			eventResults: null,
			eventsWithLocationArray: [],
			//resultDummy:resultHardCoded,
			isEventChangedPopUpOpen: false,
			eventCategoryIndex: 0,
			show_event_percentage: false,
			isCalendarOpen: false,
			selectedMonth: Moment(new Date()).format('MMM'),
			selectedYear: Moment(new Date()).format('YYYY'),
			selectedMonthIndex: '0',
			selectedMonthYearToRoll: Moment(new Date()).format('YYYY') + "-" + Moment(new Date()).format('MM'),
			selectedMonthYearToShow: Moment(new Date()).format('MMM') + " " + Moment(new Date()).format('YYYY'),
			tempSelectedMonth: Moment(new Date()).format('MMM'),
			tempSelectedYear: Moment(new Date()).format('YYYY'),
			months: months,
			years: years,
		};
	}

	componentDidMount() {
		console.log("Daata " + "Guddu")
		for (var i = 0; i < months.length; i++) {
			console.log("Daata " + Moment(new Date()).format('MMM') + ", " + months[i].name)
			if (Moment(new Date()).format('MMM') === (months[i].name)) {
				this.setState({ selectedMonthIndex: i })
			}
		}
		this.makeRemoteRequest(Moment(new Date()).format('YYYY') + "-" + Moment(new Date()).format('MM'));
	}
	componentDidUpdate(prevProps) {
		if (prevProps.isFocused !== this.props.isFocused) {
			if (this.props.isFocused) {
				this.props.setCurrentView('EVENTS_ETVBR_ACTIVE', true);
				CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type': eventTeamDashboard.keyType.valuesType.event_dashboard });
				if (this.props.update) {
					console.log("onUpdateOrCreateEvent")
					this.makeRemoteRequest(Moment(new Date()).format('YYYY') + "-" + Moment(new Date()).format('MM'));
					this.props.updateEventDashboard(false);
				}
			}
			else {
				this.props.setCurrentView('EVENTS_ETVBR_INACTIVE', false);
			}
		}
		if (this.props.update != prevProps.update) {
			if (this.props.update) {
				//console.log("onUpdateOrCreateEvent")
				//	this.makeRemoteRequest(Moment(new Date()).format('YYYY')+"-"+Moment(new Date()).format('MM'));
				//	this.props.updateEventDashboard(false);
			}

		}


	}
	updateOnFocusChange() {
		//CleverTapPush.pushEvent(eventTeamDashboard.event, {'Dashboard Type': eventTeamDashboard.keyType.valuesType.event_dashboard});
	}


	makeRemoteRequest = (rollDate) => {
		console.log("called Billa -----" + rollDate)
		this.setState({ isLoading: true, isFetching: true });
		new RestClient().getEventEtvbr({
			start_date: rollDate,
			filters: encodeURIComponent(JSON.stringify(global.global_etvbr_filters_selected))
		}).then((responseJson) => {
			console.log("called -----" + JSON.stringify(responseJson))


			this.setState({
				isLoading: false,
				isFetching: false,
				isEventChangedPopUpOpen: false,
				isCalendarOpen: false,
				eventCategoryIndex: 0,
				eventCategoryText: '',
				eventResults: responseJson.result,
				eventsWithLocationArray: responseJson.result.locations ? responseJson.result.locations : [],

			}, function () {
				// In this block you can do something with new state.
				//console.log(responseJ
				// const size  = responseJson.result.length;
				// console.log(responseJson.result.length);
			});
		}).catch(error => {


		});
	}

	_goToEventsEtvbrChild(cameFromLocationEvent, index) {
		// if(this.props.fromAndroid) {
		//     NativeModules.ReactNativeToAndroid.navigateToEventsChild(JSON.stringify({showButtons:cameFromLocationEvent, locationArray:this.state.eventResults.locations[index], date:this.state.selectedMonthYearToRoll}));
		// }
		// else {
		//   this.props.navigation.navigate('EventsEtvbrChild',  {showButtons:cameFromLocationEvent, locationArray:this.state.eventResults.locations[index], date:this.state.selectedMonthYearToRoll});

		// }
		this.props.navigation.navigate('EventsEtvbrChild', { showButtons: cameFromLocationEvent, locationArray: this.state.eventResults.locations[index], date: this.state.selectedMonthYearToRoll });

	}

	_editEvents(eventId, isEventActive, isDead) {
		console.log("lova " + eventId + ", " + isEventActive + ", " + isDead)
		if (isDead === 'false') {
			// if(this.props.fromAndroid) {
			//               NativeModules.ReactNativeToAndroid.openRNEditEvents(eventId, isEventActive);
			//    }
			this.props.navigation.navigate('AddEvents', { eventId, isEventActive, isEdit: true })
		}
	}

	_closePopUp = () => {
		//console.log("hellooo Event"+this.state.isEventChangedPopUpOpen)
		this.setState({ isEventChangedPopUpOpen: !this.state.isEventChangedPopUpOpen });
	}

	_selectedEventCategory = (index) => {
		this.setState({ isEventChangedPopUpOpen: !this.state.isEventChangedPopUpOpen, eventCategoryIndex: index });
	}

	_calendarPopUp = () => {
		//console.log("hellooo Event"+this.state.isEventChangedPopUpOpen)
		this.setState({
			isCalendarOpen: !this.state.isCalendarOpen,
			selectedMonth: this.state.tempSelectedMonth,
			selectedYear: this.state.tempSelectedYear
		});
	}

	_showETVBRFilterScreen(navigation) {
		navigation.navigate('ETVBRFilters', {
			onGoBack: () => {

			}
		})
	}

	rollSelectedDate = () => {
		console.log("rollSelectedDate: " + this.state.selectedYear + ", " + this.state.months[this.state.selectedMonthIndex].value)
		this.setState({
			isCalendarOpen: !this.state.isCalendarOpen,
			tempSelectedMonth: this.state.selectedMonth, tempSelectedYear: this.state.selectedYear,
			selectedMonthYearToRoll: this.state.selectedYear + "-" + this.state.months[this.state.selectedMonthIndex].value,
			selectedMonthYearToShow: this.state.selectedMonth + " " + this.state.selectedYear
		}, this.makeRemoteRequest(this.state.selectedYear + "-" + this.state.months[this.state.selectedMonthIndex].value));

		console.log("-- --k-" + this.state.selectedMonthYearToRoll)
	}

	getDetails(navigation, clicked_val, locationId, eventId, name) {
		var data = { info: { start_date: this.state.selectedMonthYearToRoll, clicked_val, locationId, eventId, title: name, isItEvents: true } }
		navigation.navigate('ETVBRDetails', { data, isEvents: true });
		//console.log("hoa "+text+", "+locationId+", "+eventId+""+", "+name)
		// if(this.props.fromAndroid) {

		//                 NativeModules.ReactNativeToAndroid.eventEtvbrDetails(text, locationId+"", eventId+"", name, this.state.selectedMonthYearToRoll);
		//               }
		//               else {
		//                 //this.props.navigation.goBack()
		// 							}


	}

	render() {
		if (this.state.isLoading) {
			return (
				<View style={{ flex: 1, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center' }}>
					<ActivityIndicator />
				</View>
			);
		}

		console.log("myDateFormat " + Moment(new Date()).format('MMM'))

		if (this.state.eventResults && this.state.eventResults.locations && this.state.eventResults.locations.length > 0 && this.state.isEventChangedPopUpOpen) {
			return (

				<Modal
					animationType="slide"
					transparent={true}
					visible={this.state.isEventChangedPopUpOpen}
					onRequestClose={() => this._closePopUp()}>
					<TouchableWithoutFeedback onPress={() => {
						// this.setState({filter:''})
						// this.props.onCancel()
						this._closePopUp();
					}}>
						<View style={styles.overlay}>
							<TouchableWithoutFeedback onPress={() => {

							}}>
								<View style={[styles.main]}>
									<FlatList
										ItemSeparatorComponent={this.FlatListItemSeparator}
										data={this.state.eventResults.locations[0].event_categories}
										renderItem={({ item, index }) =>
											<TouchableOpacity onPress={this._selectedEventCategory.bind(this, index)}>
												<Text style={{ color: '#000000', fontWeight: 'normal', textAlign: 'left', fontSize: 18, paddingTop: 10, paddingBottom: 10 }}>{item.event_category_name}</Text>
											</TouchableOpacity>

										}
										keyExtractor={(item, index) => index.toString()}
									/>
								</View>
							</TouchableWithoutFeedback>
						</View>
					</TouchableWithoutFeedback>
				</Modal>

			);
		}

		if (this.state.isCalendarOpen) {
			let pickerStyle;
			if (Platform.OS == 'android') {
				pickerStyle = { height: 50, width: 200 }
			}
			else if (Platform.OS == 'ios') {
				pickerStyle = { flex: 1 }
			}
			return (

				<Modal
					animationType="slide"
					transparent={true}
					visible={this.state.isCalendarOpen}
					onRequestClose={() => this._calendarPopUp()}>
					<TouchableWithoutFeedback onPress={() => {
						// this.setState({filter:''})
						// this.props.onCancel()
						this._calendarPopUp();
					}}>
						<View style={styles.overlay}>
							<TouchableWithoutFeedback onPress={() => {

							}}>
								<View style={[styles.main]}>
									<View style={{ margin: 30, flexDirection: Platform.OS == 'android' ? 'column' : 'row' }}>

										<Picker
											style={pickerStyle}
											mode="dropdown"
											selectedValue={this.state.selectedMonth}
											//onValueChange={(value, index)=>{this.monthChanged.bind(this, value)}}> //add your function to handle picker state change
											onValueChange={(itemValue, itemIndex) =>

												this.setState({ selectedMonth: itemValue, selectedMonthIndex: itemIndex })
											}>
											{this.state.months.map((item, index) => {
												return (<Picker.Item label={item.name} value={item.name} key={index} />)
											})}
										</Picker>

										<Picker
											style={pickerStyle}
											mode="dropdown"
											selectedValue={this.state.selectedYear}
											//onValueChange={(value, index)=>{this.yearChanged.bind(this, value)}}> //add your function to handle picker state change
											onValueChange={(itemValue, itemIndex) =>
												this.setState({ selectedYear: itemValue })
											}>
											{this.state.years.map((item, index) => {
												return (<Picker.Item label={item} value={item} key={index} />)
											})}
										</Picker>
										<View style={{ alignItems: 'center', justifyContent: 'center' }}>
											<Button
												style={{ alignSelf: 'center', }}
												onPress={this.rollSelectedDate}
												title="Submit"
												color="#2e5e86" />
										</View>
									</View>


								</View>
							</TouchableWithoutFeedback>
						</View>
					</TouchableWithoutFeedback>
				</Modal>

			);
		}

		if (this.state.eventResults && this.state.eventResults.locations && this.state.eventResults.locations.length > 0) {
			return (
				//console.log("show_event_percentage "+this.state.show_event_percentage)

				<View style={styles.container}>
					<NavigationEvents
						onWillFocus={payload => {
							this.updateOnFocusChange();
						}}
					/>
					<View style={{ width: '100%', alignItems: 'center', paddingTop: 8, paddingBottom: 8, justifyContent: 'center', flexDirection: 'row' }}>
						<View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start' }}>
							<Text style={{ textAlign: 'center', color: '#5E7692', fontSize: 16, flex: 1, }}>{this.state.selectedMonthYearToShow}</Text>
							{<TouchableOpacity onPress={this._calendarPopUp}>
								<Image
									style={{ width: 24, height: 24, alignItems: 'center' }}
									source={require('../../../images/ic_calender_blue.png')}
								/>
							</TouchableOpacity>}
						</View>
						<View style={{ flex: 2, alignSelf: 'flex-end', alignItems: 'flex-end', marginRight: 10 }}>
							<PercentageSwitch
								value={this.state.show_event_percentage}
								onValueChange={(show_event_percentage) => {
									this.setState({ show_event_percentage });
								}}
							/>
						</View>

						{/*<TouchableOpacity onPress = {this._showETVBRFilterScreen.bind(this, this.props.navigation)}>
							   <View style={{flexDirection:'row', marginRight: 10}}>

								   <Image source={require('../images/ic_etvbr_filter.png')}
									  style={[styless.icon, {marginLeft:10}]}/>


									   <View style={{display:(isFilterSelected()?'flex':'none'),marginLeft:-14,marginTop:-6,backgroundColor:'#FFCA28', height:16, width:16, alignItems:'center', borderRadius:10, justifyContent: "center",alignItems: "center"}}>
								    	<Text adjustsFontSizeToFit={true}
								         	numberOfLines={1} style={{textAlign:'center', fontSize:8,textAlignVertical: 'center', color:'#303030'}}>{getFilterSelectedCount()}</Text>
								    </View>
							    </View>
						     </TouchableOpacity>*/}

					</View>
					{(this.state.eventResults.locations.length == 1) ?
						<View style={{ width: '100%', alignItems: 'center', padding: 10, justifyContent: 'space-between', flexDirection: 'row', borderWidth: 1, borderColor: '#C0C0C0', backgroundColor: '#dae7f2' }}>
							<Text style={{ alignItems: 'center', fontSize: 18 }}> {this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_name}</Text>
							<TouchableOpacity onPress={this._closePopUp}>
								<Text style={styles.changeCategory}>Change</Text>
							</TouchableOpacity>
						</View> : null}

					<View style={{ width: '100%', alignItems: 'center', backgroundColor: '#CFCFCF', justifyContent: 'center', flexDirection: 'row', borderWidth: 1, borderColor: '#C0C0C0', height: 40 }}>
						<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage) ? 'E%' : 'E'}</Text>
						<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage) ? 'T%' : 'T'}</Text>
						<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage) ? 'V%' : 'V'}</Text>
						<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage) ? 'B%' : 'B'}</Text>
						<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage) ? 'R%' : 'R'}</Text>
						<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage) ? 'L%' : 'L'}</Text>
					</View>

					{(this.state.eventResults.locations.length > 1) ?

						<View style={{ flex: 1, width: '100%', justifyContent: 'center' }}>

							<FlatList
								data={this.state.eventResults.locations}
								extraData={this.state}
								renderItem={({ item, index }) => {
									return (
										<View style={{ marginTop: 5, marginBottom: 5 }}>
											<TouchableOpacity onPress={this._goToEventsEtvbrChild.bind(this, true, index)}>
												<View style={{ flex: 1, marginLeft: 5 }}>
													<Text style={styles.locationTitle} >{item.loation_name}</Text>
												</View>
											</TouchableOpacity>
											{(this.state.show_event_percentage) ?
												<View style={{ width: '100%', marginTop: 5, alignItems: 'center', borderColor: '#CFCFCF', borderWidth: 0.5, justifyContent: 'center', flexDirection: 'row', backgroundColor: '#fff', height: 40, }}>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }]}>{item.location_rel.enquiries}</Text>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }, { textDecorationLine: 'none' }]}>{item.location_rel.tdrives}</Text>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }]}>{item.location_rel.visits}</Text>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }]}>{item.location_rel.bookings}</Text>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }]}>{item.location_rel.retails}</Text>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }]}>{item.location_rel.lost_drop}</Text>
														</View>
													</View>

												</View>
												:
												<View style={{ width: '100%', marginTop: 5, alignItems: 'center', borderColor: '#CFCFCF', borderWidth: 0.5, justifyContent: 'center', flexDirection: 'row', backgroundColor: '#fff', height: 40, }}>

													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Enquiries', item.location_id, null, item.loation_name)}>
																<Text style={styles.etvbrNumbers}>{item.location_abs.enquiries}</Text>
															</TouchableOpacity>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Test Drives', item.location_id, null, item.loation_name)}>
																<Text style={styles.etvbrNumbers}>{item.location_abs.tdrives}</Text>
															</TouchableOpacity>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Visits', item.location_id, null, item.loation_name)}>
																<Text style={styles.etvbrNumbers}>{item.location_abs.visits}</Text>
															</TouchableOpacity>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Bookings', item.location_id, null, item.loation_name)}>
																<Text style={styles.etvbrNumbers}>{item.location_abs.bookings}</Text>
															</TouchableOpacity>
														</View>
													</View>

													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Retails', item.location_id, null, item.loation_name)}>
																<Text style={styles.etvbrNumbers}>{item.location_abs.retails}</Text>
															</TouchableOpacity>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Lost Drop', item.location_id, null, item.loation_name)}>
																<Text style={styles.etvbrNumbers}>{item.location_abs.lost_drop}</Text>
															</TouchableOpacity>
														</View>
													</View>
												</View>
											}

											{(this.state.eventResults.locations.length - 1 == index)
												? <View style={{ width: '100%', marginTop: 10, marginBottom: 4 }}>
													<Text style={styles.etvbrTotalTitle}>Total</Text>
													{(this.state.show_event_percentage) ?
														<View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', flexDirection: 'row', height: 40 }}>

															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.total_rel.enquiries}</Text>
															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.total_rel.tdrives}</Text>
															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.total_rel.visits}</Text>
															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.total_rel.bookings}</Text>
															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.total_rel.retails}</Text>
															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.total_rel.lost_drop}</Text>
														</View>
														: <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', flexDirection: 'row', height: 40 }}>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Enquiries', null, null, 'All Enquiries')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.total_abs.enquiries}</Text>
															</TouchableOpacity>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Test Drives', null, null, 'All Test Drive')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.total_abs.tdrives}</Text>
															</TouchableOpacity>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Visits', null, null, 'All Visits')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.total_abs.visits}</Text>
															</TouchableOpacity>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Bookings', null, null, 'All Bookings')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.total_abs.bookings}</Text>
															</TouchableOpacity>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Retails', null, null, 'All Retails')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.total_abs.retails}</Text>
															</TouchableOpacity>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Lost Drop', null, null, 'All Lost Drop')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.total_abs.lost_drop}</Text>
															</TouchableOpacity>
														</View>}
												</View>
												: null}
										</View>
									);
								}
								}
								keyExtractor={(item, index) => index + ''}
							/>

						</View>
						: /*<View style={{ flex:10, width: '100%', alignItems: 'center', justifyContent: 'center'}}>
						 <EventsEtvbrChild message={{showButtons:false, locationArray:this.state.eventResults.locations[0]}}/>
					  </View>*/
						<View style={{ flex: 1, width: '100%', justifyContent: 'center' }}>
							<FlatList

								data={this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].events}
								extraData={this.state}
								renderItem={({ item, index }) => {
									let locationId = this.state.eventResults.locations[0].location_id;
									return (
										<View style={{ marginTop: 5, marginBottom: 5 }}>
											{console.log("item.loation_name " + item.event_category_name)}
											<TouchableOpacity onPress={this._editEvents.bind(this, item.info.id + '', item.info.is_active, item.info.is_dead)}>
												<View style={{ flex: 1, marginLeft: 5 }}>
													<Text style={styles.locationTitle} >{item.info.name}</Text>
												</View>
											</TouchableOpacity>
											{(this.state.show_event_percentage) ?
												<View style={{ width: '100%', marginTop: 5, alignItems: 'center', borderColor: '#CFCFCF', borderWidth: 0.5, justifyContent: 'center', flexDirection: 'row', backgroundColor: '#fff', height: 40, }}>

													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }]}>{item.rel.enquiries}</Text>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }]}>{item.rel.tdrives}</Text>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }]}>{item.rel.visits}</Text>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }]}>{item.rel.bookings}</Text>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }]}>{item.rel.retails}</Text>
														</View>
													</View>
													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<Text style={[styles.etvbrNumbers, { textDecorationLine: 'none' }]}>{item.rel.lost_drop}</Text>
														</View>
													</View>


												</View>
												: <View style={{ width: '100%', marginTop: 5, alignItems: 'center', borderColor: '#CFCFCF', borderWidth: 0.5, justifyContent: 'center', flexDirection: 'row', backgroundColor: '#fff', height: 40, }}>

													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Enquiries', locationId, item.info.id, item.info.name)}>
																<Text style={styles.etvbrNumbers}>{item.abs.enquiries}</Text>
															</TouchableOpacity>
														</View>
													</View>

													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Test Drives', locationId, item.info.id, item.info.name)}>
																<Text style={styles.etvbrNumbers}>{item.abs.tdrives}</Text>
															</TouchableOpacity>
														</View>
													</View>

													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center' }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Visits', locationId, item.info.id, item.info.name)}>
																<Text style={styles.etvbrNumbers}>{item.abs.visits}</Text>
															</TouchableOpacity>
														</View>
													</View>

													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Bookings', locationId, item.info.id, item.info.name)}>
																<Text style={styles.etvbrNumbers}>{item.abs.bookings}</Text>
															</TouchableOpacity>
														</View>
													</View>

													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Retails', locationId, item.info.id, item.info.name)}>
																<Text style={styles.etvbrNumbers}>{item.abs.retails}</Text>
															</TouchableOpacity>
														</View>
													</View>

													<View style={styles.etvbrNumberHolder}>
														<View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
															<TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', }} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Lost Drop', locationId, item.info.id, item.info.name)}>
																<Text style={styles.etvbrNumbers}>{item.abs.lost_drop}</Text>
															</TouchableOpacity>
														</View>
													</View>







												</View>}

											{(this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].events.length - 1 == index)
												? <View style={{ width: '100%', marginTop: 10, marginBottom: 4 }}>
													<Text style={styles.etvbrTotalTitle}>Total</Text>
													{(this.state.show_event_percentage) ?
														<View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', flexDirection: 'row', height: 40 }}>
															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_rel.enquiries}</Text>
															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_rel.tdrives}</Text>
															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_rel.visits}</Text>
															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_rel.bookings}</Text>
															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_rel.retails}</Text>
															<Text style={[styles.etvbrTotal, { textDecorationLine: 'none' }]}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_rel.lost_drop}</Text>
														</View>
														:
														<View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', flexDirection: 'row', height: 40 }}>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Enquiries', this.state.eventResults.locations[0].location_id, null, 'All Enquiries')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_abs.enquiries}</Text>
															</TouchableOpacity>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Test Drives', this.state.eventResults.locations[0].location_id, null, 'All Test Drive')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_abs.tdrives}</Text>
															</TouchableOpacity>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Visits', this.state.eventResults.locations[0].location_id, null, 'All Visits')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_abs.visits}</Text>
															</TouchableOpacity>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Bookings', this.state.eventResults.locations[0].location_id, null, 'All Bookings')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_abs.bookings}</Text>
															</TouchableOpacity>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Retails', this.state.eventResults.locations[0].location_id, null, 'All Retails')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_abs.retails}</Text>
															</TouchableOpacity>
															<TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 7, }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Lost Drop', this.state.eventResults.locations[0].location_id, null, 'All Lost Drop')}*/>
																<Text style={styles.etvbrTotal}>{this.state.eventResults.locations[0].event_categories[this.state.eventCategoryIndex].event_category_abs.lost_drop}</Text>
															</TouchableOpacity>
														</View>}
												</View>
												: null}
										</View>
									);
								}
								}
								keyExtractor={(item, index) => index + ''}
							/>
						</View>
					}

				</View>
			);
		}/*else if(this.state.eventResults && this.state.eventResults.locations.length == 1){
				return(
					<EventsEtvbrChild/>
					);
			}*/else {
			return (
				<View style={styles.container}>
					<NavigationEvents
						onWillFocus={payload => {
							this.updateOnFocusChange();
						}}
					/>
					<View style={{ width: '100%', alignItems: 'center', paddingTop: 8, paddingBottom: 8, justifyContent: 'center', flexDirection: 'row' }}>
						<View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start' }}>
							<Text style={{ textAlign: 'center', color: '#5E7692', fontSize: 16, flex: 1, }}>{this.state.selectedMonthYearToShow}</Text>
							{<TouchableOpacity onPress={this._calendarPopUp}>
								<Image
									style={{ width: 24, height: 24, alignItems: 'center' }}
									source={require('../../../images/ic_calender_blue.png')}
								/>
							</TouchableOpacity>}
						</View>
						<View style={{ flex: 2 }}>

						</View>
					</View>
					<View style={{ flex: 8, alignItems: 'center', justifyContent: 'center' }}>
						<Text> No Data Found!!</Text>
					</View>
				</View>
			);
		}
	}

}

const resultHardCoded = {
	"error": {}, "message": "", "result":
	{
		"total_abs": { "enquiries": 72, "dms_enquiries": 0, "assigned": 72, "tdrives": 1, "visits": 0, "bookings": 1, "retails": 0, "lost_drop": 37, "live_enquiries": 134, "above_30d_enquiries": 74, "pending_bookings": 3, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 },
		"total_rel": { "enquiries": 100, "tdrives": 1, "visits": 0, "bookings": 1, "retails": 0, "lost_drop": 51, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 },
		"locations": [{
			"location_id": 1, "loation_name": "S P Road", "location_abs": { "enquiries": 72, "dms_enquiries": 0, "assigned": 72, "tdrives": 1, "visits": 0, "bookings": 1, "retails": 0, "lost_drop": 37, "live_enquiries": 134, "above_30d_enquiries": 74, "pending_bookings": 3, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 },
			"location_rel": { "enquiries": 100, "tdrives": 1, "visits": 0, "bookings": 1, "retails": 0, "lost_drop": 51, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 },
			"event_categories": [{
				"event_category_id": 1, "event_category_name": "Mall", "event_category_abs": { "enquiries": 72, "dms_enquiries": 0, "assigned": 72, "tdrives": 1, "visits": 0, "bookings": 1, "retails": 0, "lost_drop": 37, "live_enquiries": 134, "above_30d_enquiries": 74, "pending_bookings": 3, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 },
				"event_category_rel": { "enquiries": 100, "tdrives": 1, "visits": 0, "bookings": 1, "retails": 0, "lost_drop": 51, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 }, "events": [{
					"info": { "id": 1, "name": "test" }, "abs": { "enquiries": 72, "dms_enquiries": 0, "assigned": 72, "tdrives": 1, "visits": 0, "bookings": 1, "retails": 0, "lost_drop": 37, "pending_bookings": 3, "live_enquiries": 134, "above_30d_enquiries": 74, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 },
					"rel": { "enquiries": 100, "tdrives": 1, "visits": 0, "bookings": 1, "retails": 0, "lost_drop": 51, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 }
				}, {
					"info": { "id": 2, "name": "admin" }, "abs": { "enquiries": 0, "dms_enquiries": 0, "assigned": 0, "tdrives": 0, "visits": 0, "bookings": 0, "retails": 0, "lost_drop": 0, "pending_bookings": 0, "live_enquiries": 0, "above_30d_enquiries": 0, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 },
					"rel": { "enquiries": 0, "tdrives": 0, "visits": 0, "bookings": 0, "retails": 0, "lost_drop": 0, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 }
				}]
			}]
		},
		{
			"location_id": 2, "loation_name": "Jubilee Hills", "location_abs": { "enquiries": 0, "dms_enquiries": 0, "assigned": 0, "tdrives": 0, "visits": 0, "bookings": 0, "retails": 0, "lost_drop": 0, "live_enquiries": 0, "above_30d_enquiries": 0, "pending_bookings": 0, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 }, "location_rel": { "enquiries": 0, "tdrives": 0, "visits": 0, "bookings": 0, "retails": 0, "lost_drop": 0, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 },
			"event_categories": [{
				"event_category_id": 1, "event_category_name": "Mall", "event_category_abs": { "enquiries": 0, "dms_enquiries": 0, "assigned": 0, "tdrives": 0, "visits": 0, "bookings": 0, "retails": 0, "lost_drop": 0, "live_enquiries": 0, "above_30d_enquiries": 0, "pending_bookings": 0, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 }, "event_category_rel": { "enquiries": 0, "tdrives": 0, "visits": 0, "bookings": 0, "retails": 0, "lost_drop": 0, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 },
				"events": [{ "info": { "id": 1, "name": "test" }, "abs": { "enquiries": 2, "dms_enquiries": 0, "assigned": 0, "tdrives": 0, "visits": 0, "bookings": 0, "retails": 0, "lost_drop": 0, "pending_bookings": 0, "live_enquiries": 0, "above_30d_enquiries": 0, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 }, "rel": { "enquiries": 200, "tdrives": 0, "visits": 0, "bookings": 0, "retails": 0, "lost_drop": 0, "exchanged": 0, "in_house_financed": 0, "out_house_financed": 0 } }]
			}]
		}]
	}, "statusCode": 2002
};



//const locationArray = [{}];

const mapStateToProps = state => {
	return {
		current_view: state.updateMainHeaderCurrentView.current_view,
		isEventTabActive: state.updateMainHeaderCurrentView.isEventTabActive,
		update: state.updateEventDasboard.update,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		setCurrentView: (current_view, isEventTabActive) => {
			dispatch(updateMainHeaderCurrentView({ current_view, isEventTabActive }))
		},
		updateEventDashboard: (update) => {
			dispatch(updateEventDashboard({ update }))
		}
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(EventsEtvbr))
