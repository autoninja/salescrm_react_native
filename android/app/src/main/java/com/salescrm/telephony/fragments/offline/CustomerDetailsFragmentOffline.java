package com.salescrm.telephony.fragments.offline;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;

import android.support.v4.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.salescrm.telephony.R;

import com.salescrm.telephony.activity.offline.C360AllotDseActivityOffline;

import com.salescrm.telephony.db.EnqSourceDB;
import com.salescrm.telephony.db.LocationsDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;
import com.salescrm.telephony.db.create_lead.Customer_details;
import com.salescrm.telephony.db.create_lead.User_details;
import com.salescrm.telephony.db.crm_user.UsersDB;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Locale;

import io.realm.Realm;

/**
 * Created by bharath on 28/12/16.
 */
public class CustomerDetailsFragmentOffline extends Fragment implements DatePickerDialog.OnDateSetListener {
    private View view;
    private Realm realm;
    private TextView tvLocation,tvDse,tvCrmSource,tvExpectedClosingDate,tvAddDse,tvHeaderBuyingDate;
    Spinner spinnerModeOfPayment;
    private TextView tvFirstName,tvLastName,tvGender,tvAge,tvAddress;
    private LinearLayout llMobiles,llEmails;
    private CreateLeadInputDataDB data;
    private int scheduledActivityId;
    private TextView tvEditCustomerSimple;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.customer_details_offline, container, false);
        realm = Realm.getDefaultInstance();
        tvLocation = (TextView) view.findViewById(R.id.tv_c360_location);
        tvDse = (TextView) view.findViewById(R.id.tv_c360_dse);
        tvAddDse = (TextView) view.findViewById(R.id.tv_c360_add_dse);
        tvCrmSource = (TextView) view.findViewById(R.id.tv_c360_enq_source);
        spinnerModeOfPayment=(Spinner) view.findViewById(R.id.spinner_c360_mode_of_payment);
        tvExpectedClosingDate=(TextView) view.findViewById(R.id.tv_c360_exp_closing_date);
        tvHeaderBuyingDate=(TextView) view.findViewById(R.id.tv_c360_edit_buying_date);
        tvHeaderBuyingDate.setCompoundDrawablesWithIntrinsicBounds(null,null,
                ContextCompat.getDrawable(getContext(),R.drawable.ic_edit_black_24dp),null);
        tvEditCustomerSimple = (TextView) view.findViewById(R.id.tv_c360_edit_simple_offline);
        tvEditCustomerSimple.setCompoundDrawablesWithIntrinsicBounds(null,null,
                ContextCompat.getDrawable(getContext(),R.drawable.ic_edit_black_24dp),null);
        tvFirstName=(TextView) view.findViewById(R.id.tv_c360_first_name);
        tvLastName=(TextView) view.findViewById(R.id.tv_c360_last_name);
        tvGender=(TextView) view.findViewById(R.id.tv_c360_gender);
        tvAge=(TextView) view.findViewById(R.id.tv_c360_age);
        tvAddress=(TextView) view.findViewById(R.id.tv_c360_address);
        llMobiles = (LinearLayout)view.findViewById(R.id.ll_c360_mobiles);
        llEmails = (LinearLayout)view.findViewById(R.id.ll_c360_emails);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            scheduledActivityId = bundle.getInt("scheduledActivityId",0);
            SalesCRMRealmTable realmData = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
            if(realmData!=null){
                data = realmData.getCreateLeadInputDataDB();
            }
        }




        return view;
    }

    @Override
    public void onResume() {
        view.invalidate();
        init();
        super.onResume();

    }

    private void init() {
        tvLocation.setText(getLocationName(data.getLead_data().getLocation_id()));
        tvDse.setText(getDseName());
        tvCrmSource.setText(getCrmSource(Integer.valueOf(data.getLead_data().getLead_source_id())));
        setModeOfPayment();
        tvExpectedClosingDate.setText(data.getLead_data().getExp_closing_date().isEmpty()?
                "-":data.getLead_data().getExp_closing_date().split(" ")[0]);
        tvFirstName.setText(data.getLead_data().getCustomer_details().getFirst_name().isEmpty()?
                "-":data.getLead_data().getCustomer_details().getFirst_name());
        tvLastName.setText(data.getLead_data().getCustomer_details().getLast_name().isEmpty()?
                "-":data.getLead_data().getCustomer_details().getLast_name());
        tvGender.setText(!Util.isNotNull(data.getLead_data().getCustomer_details().getGender())?
                "-":data.getLead_data().getCustomer_details().getGender().toUpperCase());
        tvAge.setText(Util.getAge(data.getLead_data().getCustomer_details().getAge()));
        setupDynamicViewMobile();
        setupDynamicViewEmail();

        if(data.getLead_data().getCustomer_details().getResidence_address()==null&&data.getLead_data().getCustomer_details().getOffice_address()==null){
            tvAddress.setText("-");
        }
        else if(data.getLead_data().getCustomer_details().getResidence_address()==null){
            if(data.getLead_data().getCustomer_details().getOffice_address().isEmpty()){
                tvAddress.setText("-");
            }
            else {
                tvAddress.setText(data.getLead_data().getCustomer_details().getOffice_address() + "\n" + data.getLead_data().getCustomer_details().getOffice_pin_code());
            }
        }
        else {
            if(data.getLead_data().getCustomer_details().getResidence_address().isEmpty()) {
                tvAddress.setText("-");
            }
            else {
                tvAddress.setText(data.getLead_data().getCustomer_details().getResidence_address() + "\n" + data.getLead_data().getCustomer_details().getPin_code());
            }
        }
        tvHeaderBuyingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.showDatePicker(CustomerDetailsFragmentOffline.this, getActivity(), R.id.tv_c360_exp_closing_date, Calendar.getInstance(), null);
            }
        });
        tvEditCustomerSimple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeCustomerDataSimple();
            }
        });

    }


    private void setModeOfPayment() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, WSConstants.LEAD_MODE_OF_PAYMENTS);
        spinnerModeOfPayment.setAdapter(adapter);
        int spinnerPosition = 0;
        if(data.getLead_data().getMode_of_pay().equalsIgnoreCase( WSConstants.LEAD_MODE_OF_PAYMENTS[1])){
            spinnerPosition =1;
        }
        else if(data.getLead_data().getMode_of_pay().equalsIgnoreCase( WSConstants.LEAD_MODE_OF_PAYMENTS[2])){
            spinnerPosition =2;
        }
        else if(data.getLead_data().getMode_of_pay().equalsIgnoreCase( WSConstants.LEAD_MODE_OF_PAYMENTS[3])){
            spinnerPosition =3;
        }
        else if(data.getLead_data().getMode_of_pay().equalsIgnoreCase( WSConstants.LEAD_MODE_OF_PAYMENTS[4])){
            spinnerPosition =4;
        }
        spinnerModeOfPayment.setSelection(spinnerPosition);
        spinnerModeOfPayment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    realm.beginTransaction();
                    data.getLead_data().setMode_of_pay(
                            spinnerModeOfPayment.getSelectedItemPosition()==0?"":spinnerModeOfPayment.getSelectedItem().toString());
                    realm.commitTransaction();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }



    private String getDseName() {
        User_details realmData = data.getLead_data().getUser_details().where().equalTo("role_id", WSConstants.DSE_ROLE_ID).findFirst();
        if(realmData!=null){
            tvAddDse.setCompoundDrawablesWithIntrinsicBounds(null,null,null,null);
           return realm.where(UsersDB.class).equalTo("id",realmData.getUser_id()).findFirst().getName();

        }
        else {
            tvAddDse.setCompoundDrawablesWithIntrinsicBounds(null,null, ContextCompat.getDrawable(getContext(),R.drawable.ic_edit_black_24dp),null);

            tvAddDse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   //
                    Intent intent = new Intent(getContext(), C360AllotDseActivityOffline.class);
                    intent.putExtra("scheduledActivityId",scheduledActivityId);
                    startActivity(intent);
                }
            });

            return "-";
        }
    }

    private String getLocationName(Integer location_id) {
        if(location_id==null){
            return "-";
        }
        LocationsDB realmData = realm.where(LocationsDB.class).equalTo("id", location_id).findFirst();
        if(realmData!=null){
            return realmData.getName();
        }
        else {
            return "-";
        }
    }

    private String getCrmSource(Integer location_id) {
        EnqSourceDB realmData = realm.where(EnqSourceDB.class).equalTo("id", location_id).findFirst();
        if(realmData!=null){
            return realmData.getName();
        }
        else {
            return "-";
        }
    }
    private void setupDynamicViewMobile(){

        for(int i=0;i<llMobiles.getChildCount();i++){
            if(llMobiles.getChildAt(i).getId()!=R.id.ll_c360_mobiles_init){
                llMobiles.removeViewAt(i);
            }
        }
        for(int i=0;i<llEmails.getChildCount();i++){
            if(llEmails.getChildAt(i).getId()!=R.id.ll_c360_emails_init){
                llEmails.removeViewAt(i);
            }
        }
        if(data.getLead_data().getMobile_numbers().size()==0){
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.textview_star_layout, null);
            ImageView iv = (ImageView) v.findViewById(R.id.iv_c360_dynamic);
            TextView tv = (TextView) v.findViewById(R.id.tv_c360_dynamic);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            tv.setText("-");
            iv.setVisibility(View.INVISIBLE);
            llMobiles.addView(v,lp);
        }
        for(int i=0;i<data.getLead_data().getMobile_numbers().size();i++){
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.textview_star_layout, null);
            ImageView iv = (ImageView) v.findViewById(R.id.iv_c360_dynamic);
            TextView tv = (TextView) v.findViewById(R.id.tv_c360_dynamic);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            if(data.getLead_data().getMobile_numbers().get(i).getIs_primary()){
                iv.setVisibility(View.VISIBLE);
            }
            else {
                iv.setVisibility(View.INVISIBLE);
            }
            tv.setText(data.getLead_data().getMobile_numbers().get(i).getNumber());
            llMobiles.addView(v,lp);
        }
    }
    private void setupDynamicViewEmail(){
        if(data.getLead_data().getEmails().size()==0){
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.textview_star_layout, null);
            ImageView iv = (ImageView) v.findViewById(R.id.iv_c360_dynamic);
            TextView tv = (TextView) v.findViewById(R.id.tv_c360_dynamic);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            tv.setText("-");
            iv.setVisibility(View.INVISIBLE);
            llEmails.addView(v,lp);
        }
        for(int i=0;i<data.getLead_data().getEmails().size();i++){
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.textview_star_layout, null);
            ImageView iv = (ImageView) v.findViewById(R.id.iv_c360_dynamic);
            TextView tv = (TextView) v.findViewById(R.id.tv_c360_dynamic);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            if(data.getLead_data().getEmails().get(i).getIs_primary()){
                iv.setVisibility(View.VISIBLE);
            }
            else {
                iv.setVisibility(View.INVISIBLE);
            }
            tv.setText(data.getLead_data().getEmails().get(i).getAddress());
            llEmails.addView(v,lp);
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        tvExpectedClosingDate.setText(String.format(Locale.getDefault(), "%d %s %d", dayOfMonth , calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), year));
        tvExpectedClosingDate.setTag(R.id.autoninja_date, calendar);
        realm.beginTransaction();
        data.getLead_data().setExp_closing_date(Util.getSQLDateTime(((Calendar)tvExpectedClosingDate.getTag(R.id.autoninja_date)).getTime()));
        realm.commitTransaction();

    }
    private void changeCustomerDataSimple(){

            final Customer_details dataCustomer = data.getLead_data().getCustomer_details();
            final Dialog dialog = new Dialog(getContext());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            if( dialog.getWindow()!=null){
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
            dialog.setContentView(R.layout.dialog_simple_customer_edit_offline);
        final Spinner spinnerGender = (Spinner) dialog.findViewById(R.id.spinner_add_lead_mr);
        final TextInputEditText etFirstName = (TextInputEditText) dialog.findViewById(R.id.et_lead_first_name);
        final TextInputEditText etLastName = (TextInputEditText) dialog.findViewById(R.id.et_lead_last_name);
        final TextInputEditText etLeadAge = (TextInputEditText) dialog.findViewById(R.id.et_lead_age);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, WSConstants.LEAD_GENDERS);
        spinnerGender.setAdapter(adapter);
        if (dataCustomer.getGender().equalsIgnoreCase("male")) {
            spinnerGender.setSelection(0);
        } else {
            spinnerGender.setSelection(1);
        }

        etFirstName.setText(dataCustomer.getFirst_name());
        etLastName.setText(dataCustomer.getLast_name());
        etLeadAge.setText(dataCustomer.getAge());
        TextView tvYes = (TextView) dialog.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvNo = (TextView) dialog.findViewById(R.id.bt_custom_dialog_no);

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //your business logic
                dialog.dismiss();
            }
        });
        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.beginTransaction();

                dataCustomer.setAge(etLeadAge.getText().toString());
                dataCustomer.setFirst_name(etFirstName.getText().toString());
                dataCustomer.setLast_name(etLastName.getText().toString());
                String textGender ="";
                if (String.valueOf(spinnerGender.getSelectedItem()).equals("Mr")) {
                    textGender=  "male";
                } else if (String.valueOf(spinnerGender.getSelectedItem()).equals("Miss")) {
                    textGender= "female";
                } else {
                    //Wiil return for Mrs
                    textGender= "female";
                }
                dataCustomer.setGender(textGender);
                realm.commitTransaction();
                dialog.dismiss();
                init();

            }
        });


            dialog.setCanceledOnTouchOutside(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
    }
}