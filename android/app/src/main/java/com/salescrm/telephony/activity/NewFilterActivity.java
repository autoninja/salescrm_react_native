package com.salescrm.telephony.activity;

import android.app.Fragment;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.etvbr_filter_db.EtvbrFilterDb;
import com.salescrm.telephony.db.etvbr_filter_db.FiltersDb;
import com.salescrm.telephony.fragments.NewFilterFragmentFirst;
import com.salescrm.telephony.interfaces.NewFilterNotifyDataSetChanged;
import com.salescrm.telephony.model.NewFilter.EtvbrFilterApply;
import com.salescrm.telephony.interfaces.NewFilterCallFragmentListener;
import com.salescrm.telephony.model.NewFilter.EtvbrSelectedFilters;
import com.salescrm.telephony.model.NewFilter.Filter;
import com.salescrm.telephony.model.NewFilter.Filters;
import com.salescrm.telephony.model.NewFilter.Subcategories;
import com.salescrm.telephony.model.NewFilter.Values;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by prateek on 30/7/18.
 */

public class NewFilterActivity extends AppCompatActivity implements NewFilterCallFragmentListener, View.OnClickListener{

    private Preferences pref;
    private RelativeLayout rel_loading_frame;
    private int FRAGMENT_ID = 0;
    private int ACTION_APPLY = 0;
    private TextView tvClear;
    private TextView tvTitle;
    private ImageView imgBack;
    private TextView tvApply;
    private Realm realm;
    private EtvbrFilterDb etvbrFilterDb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_filter_activity);
        realm = Realm.getDefaultInstance();
        etvbrFilterDb = realm.where(EtvbrFilterDb.class).findFirst();
        rel_loading_frame = (RelativeLayout) findViewById(R.id.rel_loading_frame);
        rel_loading_frame.setVisibility(View.VISIBLE);
        tvClear = (TextView) findViewById(R.id.btn_clear);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        imgBack = (ImageView) findViewById(R.id.btn_cancel);
        tvApply = (TextView) findViewById(R.id.btn_apply);
        pref = Preferences.getInstance();
        if(etvbrFilterDb != null) {
            //WSConstants.API_NOT_CALLED = true;
            if (WSConstants.API_NOT_CALLED) {
                setData(etvbrFilterDb);
                WSConstants.API_NOT_CALLED = false;
            } else {
                rel_loading_frame.setVisibility(View.GONE);
                callFragment(0, new NewFilterFragmentFirst());
            }
        }

        tvApply.setOnClickListener(this);
        tvClear.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    private void setData(EtvbrFilterDb etvbrFilterDb) {
        setFilterDataMain(etvbrFilterDb);
        setfilterRequestData(etvbrFilterDb.getFilters());
        rel_loading_frame.setVisibility(View.GONE);
        callFragment(0, new NewFilterFragmentFirst());
    }

    private void setFilterDataMain(EtvbrFilterDb etvbrFilterDb) {
        WSConstants.filters.clear();
        for(int i = 0; i < etvbrFilterDb.getFilters().size(); i++){
            Filter filters = new Filter();
            filters.setKey(etvbrFilterDb.getFilters().get(i).getKey());
            filters.setName(etvbrFilterDb.getFilters().get(i).getName());
            for (int j = 0; j<etvbrFilterDb.getFilters().get(i).getValues().size(); j++){
                Filter.Value values = filters. new Value();
                values.setId(etvbrFilterDb.getFilters().get(i).getValues().get(j).getId()+"");
                values.setName(etvbrFilterDb.getFilters().get(i).getValues().get(j).getName());
                values.setHasChildren(etvbrFilterDb.getFilters().get(i).getValues().get(j).getHas_children());
                if(etvbrFilterDb.getFilters().get(i).getValues().get(j).getHas_children()) {
                    for (int k = 0; k < etvbrFilterDb.getFilters().get(i).getValues().get(j).getSubcategories().size(); k++) {
                        Filter.Subcategory subcategories = filters. new Subcategory();
                        subcategories.setId(etvbrFilterDb.getFilters().get(i).getValues().get(j).getSubcategories().get(k).getId() + "");
                        subcategories.setName(etvbrFilterDb.getFilters().get(i).getValues().get(j).getSubcategories().get(k).getName());
                        values.getSubcategories().add(subcategories);
                    }
                }
                filters.getValues().add(values);
            }
            WSConstants.filters.add(filters);
        }
    }

    private void setfilterRequestData(RealmList<FiltersDb> newFilter) {
        WSConstants.filterSelection.getFilters().clear();
        for(int i =0; i<newFilter.size(); i++){
            Filters filters = new Filters();
            filters.setKey(newFilter.get(i).getKey());
            filters.setName(newFilter.get(i).getName());
            for (int j = 0; j<newFilter.get(i).getValues().size(); j++){
                Values values = new Values();
                values.setId(newFilter.get(i).getValues().get(j).getId()+"");
                values.setHas_children(newFilter.get(i).getValues().get(j).getHas_children());
                values.setSet_checked(false);
                if(newFilter.get(i).getValues().get(j).getHas_children()) {
                    for (int k = 0; k < newFilter.get(i).getValues().get(j).getSubcategories().size(); k++) {
                        Subcategories subcategories = new Subcategories();
                        subcategories.setId(newFilter.get(i).getValues().get(j).getSubcategories().get(k).getId() + "");
                        subcategories.setSet_checked(false);
                        values.getSubcategories().add(subcategories);
                    }
                }
                filters.getValues().add(values);
            }
            WSConstants.filterSelection.getFilters().add(filters);
        }
    }


    private void callFragment(int i, Fragment fragment) {
        FRAGMENT_ID = i;
        setClickebles();
        getFragmentManager().beginTransaction()
                .replace(R.id.filter_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void setClickebles() {
        //if (WSConstants.selectedFilters.getFilters().size() > 0) {
            if (FRAGMENT_ID != 0) {
                tvClear.setVisibility(View.INVISIBLE);
            } else {
                tvClear.setVisibility(View.VISIBLE);
            }
        /*}else {
            tvClear.setVisibility(View.INVISIBLE);
        }*/
    }

    @Override
    public void replaceFragment(int i, Fragment fragment) {
        callFragment(i, fragment);
    }

    @Override
    public void setOptions(String title, Drawable drawable) {
            tvTitle.setText(title);
            imgBack.setImageDrawable(drawable);
    }

    @Override
    public void setApply(String text, int action_apply) {
        ACTION_APPLY = action_apply;
        tvApply.setText(text);
        setApplyButtonVisibility();
    }

    private void setApplyButtonVisibility() {
        if(ACTION_APPLY == 0){
            tvApply.setVisibility(View.VISIBLE);
            if(checkSelectedFilters(false, WSConstants.filterSelection.getFilters())){
                tvApply.setBackgroundColor(getResources().getColor(R.color.search_text_color));
                tvApply.setEnabled(true);
                //tvClear.setTypeface(null, Typeface.BOLD);
                //tvClear.setTextColor(getResources().getColor(R.color.search_text_color));
            }else {
                //tvClear.setTypeface(null, Typeface.NORMAL);
                //tvClear.setTextColor(getResources().getColor(R.color.gray));
                tvApply.setBackgroundColor(getResources().getColor(R.color.hr));
                tvApply.setEnabled(false);
            }

            if(checkSelectedFilters(false, WSConstants.filterSelection.getFilters()) || getFilterNumbers()>0){
                tvClear.setTypeface(null, Typeface.BOLD);
                tvClear.setEnabled(true);
                tvClear.setTextColor(getResources().getColor(R.color.search_text_color));
            }else {
                tvClear.setTypeface(null, Typeface.NORMAL);
                tvClear.setTextColor(getResources().getColor(R.color.gray));
                tvClear.setEnabled(false);
            }
        }else {
            tvApply.setVisibility(View.GONE);
        }
    }

    private int getFilterNumbers() {
        int value = 0;
        for (int i = 0; i < WSConstants.selectedFilters.getFilters().size(); i++) {
            if (!WSConstants.selectedFilters.getFilters().get(i).getValues().isEmpty()) {
                value++;
            }
        }
        return value;
    }

    private boolean checkSelectedFilters(boolean selected, List<Filters> filters){
        for (int i =0; i < filters.size(); i++) {
            for (int j = 0; j < filters.get(i).getValues().size(); j++) {
                if (filters.get(i).getValues().get(j).isHas_children()) {
                    for (int k = 0; k < filters.get(i).getValues().get(j).getSubcategories().size(); k++) {
                        if (filters.get(i).getValues().get(j).getSubcategories().get(k).isSet_checked()) {
                            selected = true;
                        }
                    }
                } else {
                    if (filters.get(i).getValues().get(j).isSet_checked()) {
                        selected = true;
                    }
                }
            }
        }
        return selected;
    }

    @Override
    public void onBackPressed() {
        /*if(FRAGMENT_ID ==  0) {
            finish();
            overridePendingTransition(R.anim.close_start, R.anim.close_end);
        }else{
            callFragment(0, new NewFilterFragmentFirst());
        }*/
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_apply:
                finalSelectedData();
                logCleverTap();
                killActivity(true);
                //System.out.println("Printing... "+ new Gson().toJson(WSConstants.selectedFilters) );
                break;
            case R.id.btn_clear:
                WSConstants.selectedFilters.getFilters().clear();
                unSelectSelectedOption();
                setApplyButtonVisibility();
                tvClear.setTypeface(null, Typeface.NORMAL);
                tvClear.setTextColor(getResources().getColor(R.color.gray));
                SalesCRMApplication.getBus().post(new NewFilterNotifyDataSetChanged(true));
                break;
            case R.id.btn_cancel:
                if(FRAGMENT_ID ==  0) {
                    killActivity(true);
                }else{
                    callFragment(0, new NewFilterFragmentFirst());
                }
                break;
        }
    }

    private void unSelectSelectedOption() {
        for(int i =0; i<WSConstants.filterSelection.getFilters().size(); i++){
            for (int j = 0; j<WSConstants.filterSelection.getFilters().get(i).getValues().size(); j++) {
                WSConstants.filterSelection.getFilters().get(i).getValues().get(j).setSet_checked(false);
                if (WSConstants.filterSelection.getFilters().get(i).getValues().get(j).getSubcategories() != null) {
                    for (int k = 0; k < WSConstants.filterSelection.getFilters().get(i).getValues().get(j).getSubcategories().size(); k++) {
                        Subcategories subcategories = new Subcategories();
                        subcategories.setId(WSConstants.filterSelection.getFilters().get(i).getValues().get(j).getSubcategories().get(k).getId() + "");
                        WSConstants.filterSelection.getFilters().get(i).getValues().get(j).getSubcategories().get(k).setSet_checked(false);
                    }
                }

            }
        }
    }

    private void killActivity(boolean bool) {
        finish();
        overridePendingTransition(R.anim.close_start, R.anim.close_end);
        SalesCRMApplication.getBus().post(new EtvbrFilterApply(bool));
    }

    private void logCleverTap() {
        try {
            List<String> filterSelectedCombination = new ArrayList<>();
            for (int i = 0; i < WSConstants.selectedFilters.getFilters().size(); i++) {
                if (!WSConstants.selectedFilters.getFilters().get(i).getValues().isEmpty()) {
                    filterSelectedCombination.add(WSConstants.filterSelection.getFilters().get(i).getName());
                }
            }
            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put(CleverTapConstants.EVENT_ETVBR_FILTERS_KEY_TYPE,
                    filterSelectedCombination.toString());
            CleverTapPush.pushEvent(CleverTapConstants.EVENT_ETVBR_FILTERS, hashMap);
        }
        catch (Exception ignored) {

        }
    }

    private void finalSelectedData() {
        WSConstants.selectedFilters.getFilters().clear();
        boolean isSubCatogoriesSelected;
        for(int i =0; i<WSConstants.filterSelection.getFilters().size(); i++){
            EtvbrSelectedFilters.Filters filters = WSConstants.selectedFilters.new Filters();
            filters.setKey(WSConstants.filterSelection.getFilters().get(i).getKey());
            for(int j =0; j<WSConstants.filterSelection.getFilters().get(i).getValues().size(); j++){
                EtvbrSelectedFilters.Values values = WSConstants.selectedFilters.new Values();
                if(WSConstants.filterSelection.getFilters().get(i).getValues().get(j).isHas_children()){
                    values.setId(WSConstants.filterSelection.getFilters().get(i).getValues().get(j).getId()+"");
                    isSubCatogoriesSelected = false;
                    for (int k = 0; k < WSConstants.filterSelection.getFilters().get(i).getValues().get(j).getSubcategories().size(); k++) {
                        EtvbrSelectedFilters.Subcategories subcategories = WSConstants.selectedFilters. new Subcategories();
                        if(WSConstants.filterSelection.getFilters().get(i).getValues().get(j).getSubcategories().get(k).isSet_checked()) {
                            subcategories.setId(WSConstants.filterSelection.getFilters().get(i).getValues().get(j).getSubcategories().get(k).getId() + "");
                            isSubCatogoriesSelected = true;
                            values.getSubcategories().add(subcategories);
                        }else{

                        }
                    }
                    if(isSubCatogoriesSelected) {
                        filters.getValues().add(values);
                    }else {

                    }
                }else{
                    if(WSConstants.filterSelection.getFilters().get(i).getValues().get(j).isSet_checked() == true){
                        values.setId(WSConstants.filterSelection.getFilters().get(i).getValues().get(j).getId());
                        values.setSubcategories(null);
                        filters.getValues().add(values);
                    }else{

                    }
                }
            }
            WSConstants.selectedFilters.getFilters().add(filters);
        }
    }
}
