package com.salescrm.telephony.activity.offline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;
import com.salescrm.telephony.fragments.offline.C360AddTaskFragmentOffline;
import com.salescrm.telephony.fragments.offline.C360CarFragmentOffline;

import io.realm.Realm;

/**
 * Created by bharath on 30/12/16.
 */

public class OfflineC360LeadProcessActivity extends AppCompatActivity{
    private String title;
    private String action;
    private CreateLeadInputDataDB dataDB;
    private int scheduledActivityId;
    public static SalesCRMRealmTable salesCRMRealmTable;
    private Realm realm;
    private TextView tvTitle;
    public static Button tvAction;
    private int from;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offline_c360_activity);
        realm = Realm.getDefaultInstance();
        tvTitle = (TextView) findViewById(R.id.tv_offline_c360_process_title);
        tvAction = (Button) findViewById(R.id.bt_add_lead_action);
        init();
    }

    private void init() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            scheduledActivityId = extras.getInt("scheduledActivityId");
            title =  extras.getString("title","Title");
            action =  extras.getString("action","Save");
            from = extras.getInt("from", -1);
            salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId",scheduledActivityId).findFirst();
            tvTitle.setText(title);
            tvAction.setText(action);
            showFragment();

        }
    }

    private void showFragment() {
        switch (from){
            case 0:
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.frame_offline_c360_process,new C360CarFragmentOffline()).commit();
                break;
            case 1:
                getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.frame_offline_c360_process,new C360AddTaskFragmentOffline()).commit();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        OfflineC360LeadProcessActivity.this.finish();
    }

    public void finishActivity(View view) {
        onBackPressed();
    }
}
