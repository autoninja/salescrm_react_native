package com.salescrm.telephony.db.crm_user;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 9/12/16.
 */

public class TeamUserDB extends RealmObject {
    @PrimaryKey
    private int locationId;

    private RealmList<UserNewCarDB> userNewCarDBList;

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public RealmList<UserNewCarDB> getUserNewCarDBList() {
        return userNewCarDBList;
    }

    public void setUserNewCarDBList(RealmList<UserNewCarDB> userNewCarDBList) {
        this.userNewCarDBList = userNewCarDBList;
    }
}
