package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by bharath on 10/12/16.
 */
public class GetCreActivitiesInput {
    private List<Integer> activities;
    private  List<Integer> leadSources;
    private List<Integer> locations;
    private String match;

    public List<Integer> getActivities() {
        return activities;
    }

    public void setActivities(List<Integer> activities) {
        this.activities = activities;
    }

    public List<Integer> getLeadSources() {
        return leadSources;
    }

    public void setLeadSources(List<Integer> leadSources) {
        this.leadSources = leadSources;
    }

    public List<Integer> getLocations() {
        return locations;
    }

    public void setLocations(List<Integer> locations) {
        this.locations = locations;
    }

    public String getMatch() {
        return match;
    }

    public void setMatch(String match) {
        this.match = match;
    }
}
