import React, { Component } from "react";
import {
    Slider,
    View,
    Text,
    Image,
    TouchableWithoutFeedback,
    TouchableOpacity,
    ScrollView,
    Alert,
    ActivityIndicator,
    TouchableHighlight,
    Linking
} from "react-native";
import Moment from 'moment';
import styles from './tasksList.styles';
import DateUtils from '../../../utils/DateUtils';
import Utils from '../../../utils/Utils'

export default class TaskListCard extends Component {
    constructor(props) {
        super(props);
        this.icons = {     //Step 2
            'up': require('../../../images/ic_up_black.png'),
            'down': require('../../../images/ic_down_black.png'),
            'right': require('../../../images/ic_right_black.png')
        };
        this.state = {
            expanded: false
        }
    }
    toggle() {
        if (!this.state.expanded) {
            this.props.scrollToIndex();
        }
        this.setState({ expanded: !this.state.expanded })
    }

    getCardData(task) {
        let scheduledTime = "--:--";
        let scheduledAMPM = "";
        let remarks = "Remarks : --";
        let remarksDate = "--/--";
        let taskName = "No Task";
        let title = "";
        let name = "Customer Name";
        let stageName = "--";
        let carName = "No Car";
        let address = "No Address";
        let tagColor = "#f7412d";
        let bgMain = "#fff";
        let taskColor = "#0076ff";
        let scheduledDateColor = "#0076ff"
        let leadId;
        let mobileNumber;
        let scheduledActivityId;
        let proformaTemplateExist;
        let otb = "";
        let policyEndDate = "";
        let editPolicyLink = "";
        if (this.props.tabIndex == 2) {
            bgMain = "#d4d3cf";
        }
        if (task) {
            if (task.activity) {
                if (task.activity.activity_scheduled_date) {
                    let scheduledTimeVal = Moment(task.activity.activity_scheduled_date).format('LT');
                    scheduledTime = scheduledTimeVal.split(" ")[0];
                    scheduledAMPM = scheduledTimeVal.split(" ")[1];
                    let today = new Date();
                    today.setHours("0");
                    today.setMinutes("0");
                    today.setSeconds("0");
                    today.setMilliseconds("0");
                    if (this.props.tabIndex != 2 && DateUtils.isBefore(task.activity.activity_scheduled_date, today)) {
                        bgMain = "#FFCACA";
                        taskColor = "#f7412d";
                        scheduledDateColor = "#45446d";

                    }

                }
                scheduledActivityId = task.activity.scheduled_activity_id;
                if (task.activity.activity_description) {
                    remarks = "Remarks : " + task.activity.activity_description;
                }
                if (task.activity.activity_creation_date) {
                    remarksDate = Moment(task.activity.activity_creation_date).format('DD/MM')
                        + "\n"
                        + Moment(task.activity.activity_creation_date).format('ddd').toUpperCase();
                }

                if (task.activity.activity_name) {
                    taskName = task.activity.activity_name;
                }


            }
            if (task.customer) {
                if (task.customer.title) {
                    title = task.customer.title;
                }
                if (task.customer.name) {
                    name = task.customer.name;
                }
                if (task.customer.customer_address) {
                    address = task.customer.customer_address;
                    editPolicyLink = task.customer.customer_address;
                }
                else if (task.customer.office_address) {
                    address = task.customer.office_address;
                }
                if (task.customer.mobile_number) {
                    mobileNumber = task.customer.mobile_number;
                }
                if (task.customer.residence_pin_code) {
                    otb = task.customer.residence_pin_code;
                }
            }
            if (task.lead_car) {
                if (task.lead_car.variant_name) {
                    carName = task.lead_car.variant_name;
                }
                else if (task.lead_car.model_name) {
                    carName = task.lead_car.model_name;
                }
            }
            if (task.lead) {
                if (task.lead.lead_stage) {
                    stageName = task.lead.lead_stage.stage;
                }
                if (task.lead.lead_tags) {
                    if (task.lead.lead_tags.tag_names == "Hot") {
                        tagColor = "#f7412d";
                    }
                    else if (task.lead.lead_tags.tag_names == "Warm") {
                        tagColor = "#ff9900";
                    }
                    else if (task.lead.lead_tags.tag_names == "Cold") {
                        tagColor = "#00a7f7";
                    }
                }
                if (task.lead.expected_closing_date) {
                    try {
                        policyEndDate = Utils.parseReadableDate(Utils.parseDate(task.lead.expected_closing_date));
                    } catch (error) {

                    }
                }
                leadId = task.lead.lead_id;
            }
            proformaTemplateExist = task.template_exist;

        }
        return {
            bgMain,
            taskColor,
            scheduledDateColor,
            tagColor,
            scheduledTime,
            scheduledAMPM,
            taskName,
            title,
            name,
            stageName,
            remarks,
            remarksDate,
            carName,
            address,
            leadId,
            mobileNumber,
            scheduledActivityId,
            proformaTemplateExist,
            otb,
            policyEndDate,
            editPolicyLink,

        }
    }

    render() {
        let info = this.props.info;
        let isClientILom = false;
        let isClientILBank = false;
        let isClientTwoWheeler;
        let hotlineNo;
        if (info) {
            isClientILom = info.isClientILom ? true : false;
            isClientILBank = info.isClientILBank ? true : false;
            isClientTwoWheeler = info.isClientTwoWheeler ? true : false;
            hotlineNo = info.hotlineNo;
        }
        let task = this.props.task;
        let icon = this.icons['down'];
        if (this.state.expanded) {
            icon = this.icons['up'];   //Step 4
        }
        let tabIndex = this.props.tabIndex;
        let {
            bgMain,
            taskColor,
            scheduledDateColor,
            tagColor,
            scheduledTime,
            scheduledAMPM,
            taskName,
            title,
            name,
            stageName,
            remarks,
            remarksDate,
            carName,
            address,
            leadId,
            mobileNumber,
            scheduledActivityId,
            proformaTemplateExist,
            otb,
            policyEndDate,
            editPolicyLink
        } = this.getCardData(task);

        let PRE_FORM = '5'
        return (
            <View style={{ marginTop: 4 }}>
                <View
                    style={[styles.cardWrapper, { backgroundColor: bgMain }]}
                >
                    <View
                        style={[
                            styles.leadTagIndicator,
                            {
                                backgroundColor: tagColor,
                            }]}
                    />


                    <View
                        style={{
                            margin: 4,
                            paddingRight: 4,
                            flexDirection: "row",
                            flex: 10
                        }}>

                        <View style={styles.cardLeft}>
                            <TouchableOpacity
                                disabled={tabIndex == 2}
                                onPress={() => { this.toggle() }}
                            >
                                <View style={{ flexDirection: 'row', height: 48, paddingBottom: 8, }}>
                                    <Text style={[styles.scheduledDate, { color: scheduledDateColor }]}>{scheduledTime}</Text>

                                    <Text style={[styles.bottomFixed, { color: scheduledDateColor, alignSelf: 'center' }]}>{scheduledAMPM}</Text>


                                </View>
                                <Text style={[styles.taskName, { color: taskColor }]}>{taskName}</Text>
                                {tabIndex != 2 ? (
                                    <Image
                                        style={[styles.imageNav, { marginTop: 6 }]}
                                        source={icon}
                                    ></Image>
                                ) : <View style={{ height: 14, marginTop: 6 }} />}

                            </TouchableOpacity>
                        </View>

                        <View style={styles.cardRight}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.onClickAction("navigate_c360", { leadId, scheduledActivityId })
                                }}
                            >
                                <View style={{ flexDirection: 'row', height: 48, paddingBottom: 8, paddingTop: 8 }}>

                                    <Text style={[styles.bottomFixed, { marginTop: 4 }]}>{title}</Text>

                                    <Text numberOfLines={1} style={styles.name}>{name}</Text>

                                    <Image
                                        style={[styles.imageNav, { alignSelf: 'flex-end', marginBottom: 4 }]}
                                        source={this.icons['right']}
                                    />
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.stage}>{stageName}</Text>
                                    <Text style={styles.stageTag}>STAGE</Text>
                                </View>



                            </TouchableOpacity>
                        </View>


                    </View>

                </View>
                {this.state.expanded && this.props.tabIndex != 2 && (
                    <View style={styles.children}>
                        <View style={styles.childBody}>
                            <View style={{ flexDirection: 'row', }}>
                                <Text style={{ flex: 1, alignSelf: 'center', fontSize: 16, color: '#494949' }}>{remarks}</Text>
                                <Text style={{ textAlign: 'center', fontSize: 18, color: '#494949', lineHeight: 22, }}>{remarksDate}</Text>
                            </View>
                            <View style={styles.hr} />
                            <View style={{ flexDirection: 'row', }}>

                                {!isClientILom && !isClientILBank && (
                                    <Text style={{ flex: 1, alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#494949' }}>{carName}</Text>
                                )}
                                {isClientILom && (
                                    <View style={{
                                        flex: 1,
                                        flexDirection: 'row',
                                    }}>

                                        <Text style={{ flex: 1, fontSize: 14, fontWeight: 'bold', alignSelf: 'flex-start' }}>{policyEndDate}</Text>

                                        <TouchableOpacity
                                            onPress={() => {
                                                Linking.openURL(editPolicyLink);
                                            }}
                                            style={{ alignSelf: 'flex-end', }}>
                                            <Text style={{
                                                fontSize: 13, paddingTop: 2, paddingBottom: 2, paddingStart: 8, paddingEnd: 8,
                                                borderRadius: 2,
                                                backgroundColor: '#F44336', color: '#fff'
                                            }}>Edit Policy</Text>
                                        </TouchableOpacity>
                                    </View>
                                )}
                                {isClientILBank && (
                                    <View>
                                        <Text style={{ flex: 1, alignSelf: 'center', fontSize: 16, fontWeight: 'bold', color: '#494949' }}>{address}</Text>
                                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                            <Text style={{ fontSize: 14 }}>{"OTB: "}</Text>
                                            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{otb}</Text>
                                        </View>
                                    </View>
                                )}
                                {proformaTemplateExist && (
                                    <View style={{ backgroundColor: '#007fff', height: 22, alignItems: 'center', alignSelf: 'center', padding: 2, paddingLeft: 4, paddingRight: 4, borderRadius: 3 }}>
                                        <TouchableOpacity
                                        >
                                            <Text style={{ color: '#fff', fontSize: 13, textAlign: 'center' }}>Proforma Invoice</Text>
                                        </TouchableOpacity>
                                    </View>
                                )}

                            </View>
                            {!isClientILom && !isClientILBank && (
                                <View>
                                    <View style={styles.hr} />
                                    <Text style={{ flex: 1, fontSize: 16, color: '#494949', lineHeight: 20, }}>{address}</Text>
                                </View>
                            )}

                            <View style={styles.hr} />
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 10, paddingRight: 10 }}>
                                    {!isClientILBank && (
                                        <TouchableOpacity>
                                            <View style={[styles.imageButton, { backgroundColor: '#007fff' }]}>
                                                <Image style={{ width: 24, height: 24 }} source={require('../../../images/ic_task_mail.png')} />
                                            </View>
                                        </TouchableOpacity>
                                    )}
                                    {!isClientILom && !isClientILBank && (
                                        <TouchableOpacity style={{ marginLeft: 20 }}>
                                            <View style={[styles.imageButton, { backgroundColor: '#fff' }]}>

                                                <Image style={{ width: 28, height: 28 }} source={require('../../../images/ic_tasks_whatsapp.png')} />

                                            </View>
                                        </TouchableOpacity>
                                    )}

                                </View>
                                <View style={{ flex: 1, alignSelf: 'flex-end', alignItems: 'flex-end' }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            if (isClientILom || isClientILBank) {
                                                if (hotlineNo) {
                                                    let mobileNumberILom = hotlineNo + mobileNumber + "%23";
                                                    Linking.openURL(`tel:${mobileNumberILom}`);
                                                }
                                            }
                                            else {
                                                this.props.onClickAction("call", { mobileNumber })
                                            }
                                        }}
                                    >
                                        <View style={[styles.imageButton, { backgroundColor: '#00C853' }]}>

                                            <Image style={{ width: 24, height: 24 }} source={require('../../../images/ic_task_call.png')} />

                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        <TouchableOpacity
                            onPress={() => {
                                this.props.onClickAction("update", {
                                    scheduledActivityId, leadId, actionId: PRE_FORM, form_title: "Update",
                                    leadLastUpdated: task.lead.lead_last_updated, activityId: task.activity.activity_id
                                })
                            }}
                        >
                            <View style={styles.update}>
                                <Text style={{ color: '#fff', fontSize: 18 }}>Update</Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                )
                }

            </View>

        );
    }
}
