import React, { Component } from 'react';
import { View, Text, YellowBox } from 'react-native';
import styles from '../styles/styles';

class SecondActivity extends Component {

    constructor(props) {
        super(props);
        YellowBox.ignoreWarnings([
            'Warning: componentWillMount is deprecated',
            'Warning: componentWillReceiveProps is deprecated',
        ]);
    }

    render()
    {
        return(
            <View style = { styles.MainContainer }>
                <Text style={{fontSize: 23}}> Logout - 2 </Text>
            </View>
        );
    }
}

export default SecondActivity;