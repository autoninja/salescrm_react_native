package com.salescrm.telephony.db.team_dashboard_gm_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by prateek on 29/8/17.
 */

public class AbsTotalSm extends RealmObject {
    @SerializedName("enquiries")
    @Expose
    private Integer enquiries;
    @SerializedName("tdrives")
    @Expose
    private Integer tdrives;
    @SerializedName("visits")
    @Expose
    private Integer visits;
    @SerializedName("bookings")
    @Expose
    private Integer bookings;
    @SerializedName("retail")
    @Expose
    private Integer retail;
    @SerializedName("enquiries_target")
    @Expose
    private Integer enquiriesTarget;
    @SerializedName("tdrives_target")
    @Expose
    private Integer tdrivesTarget;
    @SerializedName("visits_target")
    @Expose
    private Integer visitsTarget;
    @SerializedName("bookings_target")
    @Expose
    private Integer bookingsTarget;
    @SerializedName("retail_target")
    @Expose
    private Integer retailTarget;

    public Integer getEnquiriesTarget() {
        return enquiriesTarget;
    }

    public void setEnquiriesTarget(Integer enquiriesTarget) {
        this.enquiriesTarget = enquiriesTarget;
    }

    public Integer getTdrivesTarget() {
        return tdrivesTarget;
    }

    public void setTdrivesTarget(Integer tdrivesTarget) {
        this.tdrivesTarget = tdrivesTarget;
    }

    public Integer getVisitsTarget() {
        return visitsTarget;
    }

    public void setVisitsTarget(Integer visitsTarget) {
        this.visitsTarget = visitsTarget;
    }

    public Integer getBookingsTarget() {
        return bookingsTarget;
    }

    public void setBookingsTarget(Integer bookingsTarget) {
        this.bookingsTarget = bookingsTarget;
    }

    public Integer getRetailTarget() {
        return retailTarget;
    }

    public void setRetailTarget(Integer retailTarget) {
        this.retailTarget = retailTarget;
    }

    public Integer getEnquiries() {
        return enquiries;
    }

    public void setEnquiries(Integer enquiries) {
        this.enquiries = enquiries;
    }

    public Integer getTdrives() {
        return tdrives;
    }

    public void setTdrives(Integer tdrives) {
        this.tdrives = tdrives;
    }

    public Integer getVisits() {
        return visits;
    }

    public void setVisits(Integer visits) {
        this.visits = visits;
    }

    public Integer getBookings() {
        return bookings;
    }

    public void setBookings(Integer bookings) {
        this.bookings = bookings;
    }

    public Integer getRetail() {
        return retail;
    }

    public void setRetail(Integer retail) {
        this.retail = retail;
    }
}
