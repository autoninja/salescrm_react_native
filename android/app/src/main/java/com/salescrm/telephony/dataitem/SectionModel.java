package com.salescrm.telephony.dataitem;

/**
 * Created by bharath on 27/6/16.
 */
public class SectionModel {
    private int position;
    private String title;

    public SectionModel(int position, String title) {
        this.position = position;
        this.title = title;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
