package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by bharath on 8/9/16.
 */
public class TeamLeadsAndDSEsResponse {
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private List<Dses> dses;

        private List<Dse_team_leads> dse_team_leads;

        public List<Dses> getDses() {
            return dses;
        }

        public void setDses(List<Dses> dses) {
            this.dses = dses;
        }

        public List<Dse_team_leads> getDse_team_leads() {
            return dse_team_leads;
        }

        public void setDse_team_leads(List<Dse_team_leads> dse_team_leads) {
            this.dse_team_leads = dse_team_leads;
        }

        @Override
        public String toString() {
            return "ClassPojo [dses = " + dses + ", dse_team_leads = " + dse_team_leads + "]";
        }

        public class Dses {
            private String id;

            private String mobile_number;

            private String name;

            private String team_id;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getMobile_number() {
                return mobile_number;
            }

            public void setMobile_number(String mobile_number) {
                this.mobile_number = mobile_number;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getTeam_id() {
                return team_id;
            }

            public void setTeam_id(String team_id) {
                this.team_id = team_id;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", mobile_number = " + mobile_number + ", name = " + name + ", team_id = " + team_id + "]";
            }
        }

        public class Dse_team_leads {
            private String id;

            private String name;

            private String team_id;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getTeam_id() {
                return team_id;
            }

            public void setTeam_id(String team_id) {
                this.team_id = team_id;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", name = " + name + ", team_id = " + team_id + "]";
            }
        }

    }


}