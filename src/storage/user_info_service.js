import { realm } from './realm_schemas'

const UserInfoService = {
  findAll: function () {
    return realm.objects('UserInfo');
  },
  findFirst: function () {
    let realObj = realm.objects('UserInfo');
    if (realObj) {
      return realObj[0];
    }
    return null;
  },
  getRoles: function () {
    let realmObj = realm.objects('UserRoles');
    let userRoles = [];
    if (realmObj) {
      realmObj.map((role, index) => {
        userRoles.push(role);
      });
      return userRoles;
    }
    return null;
  },
  isUserBranchHead: function () {
    let realmObj = realm.objects('UserRoles').filtered('id == 21');
    return realmObj.length > 0;
  },
  isUserOperations: function () {
    let realmObj = realm.objects('UserRoles').filtered('id == 30');
    return realmObj.length > 0;
  },
  isUserSalesManager: function () {
    let realmObj = realm.objects('UserRoles').filtered('id == 8');
    return realmObj.length > 0;
  },
  isUserTeamLead: function () {
    let realmObj = realm.objects('UserRoles').filtered('id == 6');
    return realmObj.length > 0;
  },
  isUserSalesConsultant: function () {
    let realmObj = realm.objects('UserRoles').filtered('id == 4');
    return realmObj.length > 0;
  },
  isUserEvaluator: function () {
    let realmObj = realm.objects('UserRoles').filtered('id == 28');
    return realmObj.length > 0;
  },
  isUserEvaluatorManager: function () {
    let realmObj = realm.objects('UserRoles').filtered('id == 31');
    return realmObj.length > 0;
  },
  getLocations: function () {
    let realmObj = realm.objects('UserLocations');
    let userLocations = [];
    if (realmObj) {
      realmObj.map((role, index) => {
        userLocations.push(role);
      });
      return userLocations;
    }
    return null;
  },
  getLocationsList: function () {
    let realmObj = realm.objects('UserLocations');
    let userLocations = [];
    if (realmObj) {
      realmObj.map((location, index) => {
        userLocations.push({ id: location.id, text: location.name, name: location.name });
      });
      return userLocations;
    }
    return null;
  },

  save: function (user_info) {
    realm.write(() => {
      let user = realm.create('UserInfo', user_info, true);
      // for (var i = 0; i < user_info.brands.length; i++) {
      //     user.brands.push(realm.create('UserBrands', {id:user_info.brands[i].brand_id, name:users_info.brands[i].brand_name}, true));
      // }

    })
    let userInfo = realm.objects('UserInfo');
    console.log('userInfo.length', userInfo.length);
    console.log(JSON.stringify(userInfo));
  },
  update_dp_url: function (dp_url) {
    realm.write(() => {
      let user = realm.objects('UserInfo')[0];
      user.dp_url = dp_url;


    });
    let userInfo = realm.objects('UserInfo');
    console.log(JSON.stringify(userInfo));
  },
  deleteAll: function () {
    realm.write(() => {
      realm.delete(realm.objects('UserRoles'));
      realm.delete(realm.objects('UserLocations'));
      realm.delete(realm.objects('UserInfo'));
    })
  },

  // update: function(todo, callback) {
  //   if (!callback) return;
  //   repository.write(() => {
  //     callback();
  //     todo.updatedAt = new Date();
  //   });
  // }
};

export default UserInfoService;
