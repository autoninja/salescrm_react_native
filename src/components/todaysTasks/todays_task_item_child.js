import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, NativeModules, Platform } from 'react-native'


export default class TodayTaskItemChild extends Component {
  constructor(props) {
    super(props);
    this.state = { expand: false, showImagePlaceHolder: true };
  }
  openTaskList() {
    if (Platform.OS == "ios") {
      this.props.openTasksList(this.props.info.id, this.props.info.name, this.props.info.dp_url)
    }
    else {
      NativeModules.ReactNativeToAndroid.openTaskList(this.props.info.id, this.props.info.name, this.props.info.dp_url);
    }
  }

  render() {
    let imageSource;
    let placeHolder

    imageSource = { uri: this.props.info.dp_url ? this.props.info.dp_url : 'null' };
    placeHolder = <View style={{ position: 'absolute', display: (this.state.showImagePlaceHolder ? 'flex' : 'none'), width: 36, height: 36, borderRadius: 36 / 2, backgroundColor: '#FF8000', alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#fff' }}>{this.props.info.name.charAt(0).toUpperCase()}</Text></View>;;

    return (
      <TouchableOpacity onPress={() => {
        this.openTaskList();
      }}>
        <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#fff', borderWidth: 0.3, borderRadius: 6, borderColor: '#cfd8dc', alignItems: 'center' }}>

          <View style={{ flex: 1.5, alignItems: 'center' }} >

            <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
              {placeHolder}
              <TouchableOpacity
                onPress={() => {
                  this.openTaskList();
                }}
                style={{ position: 'absolute', }}
                onLongPress={() => this.props.onLongPress('user_info', { info: { name: this.props.info.name, dp_url: this.props.info.dp_url } })}>

                <Image
                  source={imageSource}
                  style={{ width: 36, height: 36, borderRadius: 36 / 2, }}
                />
              </TouchableOpacity>
            </View>





          </View>





          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#cfd8dc',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#2e5e86' }} >{this.props.abs.calls}</Text>
            </View>


          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#cfd8dc',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#2e5e86' }} >{this.props.abs.tdrives}</Text>
            </View>

          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#cfd8dc',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>

            <Text style={{ color: '#2e5e86' }} >{this.props.abs.visits}</Text>
          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#cfd8dc',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#2e5e86' }} >{this.props.abs.post_bookings}</Text>
            </View>


          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#cfd8dc',
            borderLeftWidth: 0.3
          }}>
            <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: '#2e5e86' }} >{this.props.abs.deliveries}</Text>
            </View>


          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#cfd8dc',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>

            <Text style={{ color: '#F5A623' }} >{this.props.abs.pending_total_count}</Text>
          </View>
          {
            /*
            <View style= {{flex:1,height:'100%',borderLeftColor: '#cfd8dc',
              borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

                <TouchableOpacity  hitSlop={{top: 10, bottom: 10, left: 10, right: 10}} onLongPress={ () => this.props.onLongPress('Uncalled Tasks',{info: this.props.info})}>
                  <Text style={{ color:'red',textDecorationLine: 'underline',
                    textDecorationColor: '#F20407'}} >{this.props.abs.uncalled_tasks}</Text>
                </TouchableOpacity>
            </View>
            */
          }






        </View>
      </TouchableOpacity>
    );
  }
}
