package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by prateek on 29/5/18.
 */

public class ProformaInvoiceAnswer {

    private String lead_id;
    private String on_road_price;
    private CustomerAndVehicleDetail customer_details;
    private List<InvoiceItemsAnswer> invoice_items = null;

    public List<InvoiceItemsAnswer> getInvoice_items() {
        return invoice_items;
    }

    public void setInvoice_items(List<InvoiceItemsAnswer> invoice_items) {
        this.invoice_items = invoice_items;
    }

    public CustomerAndVehicleDetail getCustomer_details() {
        return customer_details;
    }

    public void setCustomer_details(CustomerAndVehicleDetail customer_details) {
        this.customer_details = customer_details;
    }

    public String getOn_road_price() {
        return on_road_price;
    }

    public void setOn_road_price(String on_road_price) {
        this.on_road_price = on_road_price;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public class CustomerAndVehicleDetail{

        private String name;
        private String home_address;
        private String office_address;
        private String model_id;
        private String variant_id;
        private String color_id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getHome_address() {
            return home_address;
        }

        public void setHome_address(String home_address) {
            this.home_address = home_address;
        }

        public String getOffice_address() {
            return office_address;
        }

        public void setOffice_address(String office_address) {
            this.office_address = office_address;
        }

        public String getModel_id() {
            return model_id;
        }

        public void setModel_id(String model_id) {
            this.model_id = model_id;
        }

        public String getVariant_id() {
            return variant_id;
        }

        public void setVariant_id(String variant_id) {
            this.variant_id = variant_id;
        }

        public String getColor_id() {
            return color_id;
        }

        public void setColor_id(String color_id) {
            this.color_id = color_id;
        }
    }

    public class InvoiceItemsAnswer{

        private String id;
        private String value;
        private String percentage;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getPercentage() {
            return percentage;
        }

        public void setPercentage(String percentage) {
            this.percentage = percentage;
        }
    }

}
