import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    container: { flex: 1, backgroundColor:'#ffffff' },
    header: {
      height: 100,
      backgroundColor: '#007fff',
      flexDirection:'row',
      alignItems:'center',
      padding:16,
    },
    headerLeft: { flex:1, justifyContent: 'center',},
    headerRight: { flex:1, alignItems:'center', justifyContent: 'center',},
    titleContainer: { flexDirection:'row', alignSelf:'flex-start'},
    title: { fontSize: 20, color: '#ffffff', alignSelf:'baseline' },
    subTitle: { fontSize: 15, color: '#ffffff', alignSelf:'center'},
    row: { flexDirection: 'row', height : 50, borderBottomWidth: 1, borderBottomColor: '#ececec'},
    button: {width: '50%', paddingTop: 5, height: 50, borderRadius: 0},
    bottom: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    switchView: {
        alignItems:'center',
        justifyContent:'center',
        height:30,
        width:'100%',
        flexDirection:'row',
        borderRadius:20,
        borderColor:'#ffffff',
        borderWidth:2,
        backgroundColor:'#eeeeee'
    },
    switchTextActive: {
        flex:1,
        height:27,
        color:'#eee',
        fontSize:14,
        alignSelf:'center',
        backgroundColor:'#32c151',
        borderRadius:20,
        elevation:2,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 5,
        textAlign:'center',
        textAlignVertical:'center'
    },
    switchTextInActive: {
      flex:1,
      height:28,
      color:'#808080',
      fontSize:14,
      alignSelf:'center',
      borderRadius:20,
      textAlign:'center',
      textAlignVertical:'center'
    },
    listContainer: {
        flex: 1,
    }
});
