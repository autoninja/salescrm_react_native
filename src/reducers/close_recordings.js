import { CLOSE_RECORD_PLAYING }  from '../actions/close_recordings_action'

const initialState = {
    closeRecodringPlayer: false
  };
  
const closeRecordings = (state = initialState, action) => {
    switch(action.type) {
      case CLOSE_RECORD_PLAYING:
        return {
            closeRecodringPlayer: true
          };
      case 'OPEN_RECORD_PLAYING':
          return {
            closeRecodringPlayer: false
          };
      default:
        return state;
    }
  }
  
export default closeRecordings;