package com.salescrm.telephony.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.salescrm.telephony.R;
import com.salescrm.telephony.dataitem.FilterActivityTypeModel;
import com.salescrm.telephony.model.TransferEnquiriesModel;
import com.salescrm.telephony.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.zip.Inflater;

/**
 * Created by bharath on 21/8/17.
 */

public class TransferEnquiriesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int ITEM_HEADER =1;
    private final int ITEM_VIEW =2;
    private final boolean isFromPage;
    private final TransferEnqClickListener listener;
    private final ArrayList<TransferEnquiriesModel.Item> allUserItemLists;
    private boolean isFilter =false;
    private List<TransferEnquiriesModel.Item> userItemLists;
    private Context context;
    private Context contextThemeWrapper;
    private int blueColor;

    public TransferEnquiriesRecyclerAdapter(TransferEnqClickListener listener,List<TransferEnquiriesModel.Item> userItemLists, Context context, boolean isFrom,boolean isFilter) {
        this.listener = listener;
        this.userItemLists = userItemLists;
        this.allUserItemLists = new ArrayList<>();
        this.allUserItemLists.addAll(userItemLists);
        this.context = context;
        this.isFromPage =isFrom;
        this.blueColor = ContextCompat.getColor(context,R.color.blue_v2);
        this.isFilter = isFilter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ITEM_HEADER){
            return new ItemHeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.header_view, parent, false));
        }
        else if((viewType == ITEM_VIEW)) {
            if(isFromPage){
                contextThemeWrapper = new ContextThemeWrapper(parent.getContext(), R.style.AppThemeTransferEnqFrom);
            }
            else {
                contextThemeWrapper = new ContextThemeWrapper(parent.getContext(), R.style.AppThemeTransferEnqTo);
            }

            LayoutInflater localInflater = LayoutInflater.from(parent.getContext()).cloneInContext(contextThemeWrapper);
            View view = localInflater.inflate(R.layout.layout_transfer_enq_users, parent, false);
            return new ItemViewHolder(view);
        }
        return null;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        userItemLists.clear();
        if (charText.length() == 0) {
            userItemLists.addAll(allUserItemLists);
        } else {
            for (TransferEnquiriesModel.Item model : allUserItemLists) {
                if (model.getTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                    userItemLists.add(model);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int poa) {
        final int position = holder.getAdapterPosition();
        if(holder instanceof ItemHeaderViewHolder){
            ((ItemHeaderViewHolder) holder).headerTitle.setText(userItemLists.get(position).getTitle());
        }
        else if(holder instanceof  ItemViewHolder){
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.checkBoxTitle.setText(userItemLists.get(position).getTitle());


            Glide.with(context).
                    load(userItemLists.get(position).getImgUrl())
                    .asBitmap()
                    .centerCrop()
                    .placeholder(R.drawable.ic_person_white_48dp)
                    .error(R.drawable.ic_person_white_48dp)
                    .into(new BitmapImageViewTarget(itemViewHolder.imageView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            if(resource!=null && context!=null) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                itemViewHolder.imageView.setImageDrawable(circularBitmapDrawable);
                            }
                        }
                    });

            if(isFromPage){
                itemViewHolder.checkBoxTitle.setTextColor(Color.WHITE);
            }
            else {
                itemViewHolder.checkBoxTitle.setTextColor(blueColor);

            }

            if(isFilter){
                itemViewHolder.imageView.setVisibility(View.GONE);
                itemViewHolder.bgImage.setVisibility(View.GONE);
            }
            else {
                itemViewHolder.imageView.setVisibility(View.VISIBLE);
                itemViewHolder.bgImage.setVisibility(View.VISIBLE);

            }
            itemViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        listener.onClick(position,userItemLists.get(position));
                    }
                }
            });

            if(userItemLists.get(position).isChecked()) {
                itemViewHolder.checkBoxTitle.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        itemViewHolder.checkBoxTitle.setChecked(true);
                    }
                }, 0);
            }
            else {
                itemViewHolder.checkBoxTitle.setChecked(false);
            }


        }
    }


    @Override
    public int getItemCount() {
        return userItemLists==null?0:userItemLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(userItemLists.get(position).isHeader()){
            return ITEM_HEADER;
        }
        else {
            return ITEM_VIEW;
        }
    }

    private class ItemHeaderViewHolder extends RecyclerView.ViewHolder{
        private TextView headerTitle;
        ItemHeaderViewHolder(View itemView) {
            super(itemView);
            headerTitle = (TextView) itemView.findViewById(R.id.tv_recycler_view_header);
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder{
        private AppCompatCheckBox checkBoxTitle;
        private ImageView imageView;
        private View bgImage;
        ItemViewHolder(View itemView) {
            super(itemView);
            checkBoxTitle = (AppCompatCheckBox) itemView.findViewById(R.id.checkbox_transfer_enq_profile);
            imageView     = (ImageView) itemView.findViewById(R.id.img_transfer_enq_profile);
            bgImage       =  itemView.findViewById(R.id.img_transfer_enq_profile_bg);
        }
    }

    public interface TransferEnqClickListener {
        void onClick(int position,TransferEnquiriesModel.Item item);
    }
}
