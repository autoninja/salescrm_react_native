package com.salescrm.telephony.db;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 29/6/17.
 */
public class EnqSourceMainDB extends RealmObject {
    private RealmList<EnqSourceDB> leadSources;
    private RealmList<EnqSourceCategoryDB> lead_source_categories;

    public RealmList<EnqSourceDB> getLeadSources() {
        return leadSources;
    }

    public void setLeadSources(RealmList<EnqSourceDB> leadSources) {
        this.leadSources = leadSources;
    }

    public RealmList<EnqSourceCategoryDB> getLead_source_categories() {
        return lead_source_categories;
    }

    public void setLead_source_categories(RealmList<EnqSourceCategoryDB> lead_source_categories) {
        this.lead_source_categories = lead_source_categories;
    }
}
