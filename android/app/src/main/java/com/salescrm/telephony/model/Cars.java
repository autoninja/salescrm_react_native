package com.salescrm.telephony.model;

/**
 * Created by bannhi on 10/2/17.
 */

public class Cars
{
    private Model model;

    private String kms_run;

    private String purchase_date;

    private String reg_no;


    public Cars(Model model, String kms_run, String purchase_date, String reg_no){
        this.model = model;
        this.kms_run = kms_run;
        this.purchase_date = purchase_date;
        this.reg_no = reg_no;
    }
    public Model getModel ()
    {
        return model;
    }

    public void setModel (Model model)
    {
        this.model = model;
    }

    public String getKms_run ()
    {
        return kms_run;
    }

    public void setKms_run (String kms_run)
    {
        this.kms_run = kms_run;
    }

    public String getPurchase_date ()
    {
        return purchase_date;
    }

    public void setPurchase_date (String purchase_date)
    {
        this.purchase_date = purchase_date;
    }

    public String getReg_no ()
    {
        return reg_no;
    }

    public void setReg_no (String reg_no)
    {
        this.reg_no = reg_no;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [model = "+model+", kms_run = "+kms_run+", purchase_date = "+purchase_date+", reg_no = "+reg_no+"]";
    }
}
