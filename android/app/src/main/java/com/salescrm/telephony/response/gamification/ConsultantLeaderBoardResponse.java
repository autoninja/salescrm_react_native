package com.salescrm.telephony.response.gamification;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bannhi on 17/1/18.
 */

public class ConsultantLeaderBoardResponse implements Serializable{
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }


    public class Error implements Serializable
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }

    public class Result implements Serializable
    {


        private ArrayList<Categories> categories;

        public ArrayList<Categories> getCategories() {
            return categories;
        }

        public void setCategories(ArrayList<Categories> categories) {
            this.categories = categories;
        }
        @Override
        public String toString()
        {
            return "ClassPojo [categories = "+categories+"]";
        }
    }

    public class Categories implements Serializable
    {
        private int id;

        private ArrayList<Users> users;

        private String name;

        private int spotlight;


        public ArrayList<Users> getUsers() {
            return users;
        }

        public void setUsers(ArrayList<Users> users) {
            this.users = users;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getSpotlight() {
            return spotlight;
        }

        public void setSpotlight(int spotlight) {
            this.spotlight = spotlight;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [id = "+id+", users = "+users+", name = "+name+", spotlight = "+spotlight+"]";
        }
    }

    public class Users implements  Serializable
    {
        private ArrayList<UserBadges> badges;

        private String name;

        private String user_id;

        private String points;

        private double booster_constant;

        private Integer booster_multiplier;

        private int spotlight;

        private String photo_url;

        private String rank;

        public ArrayList<UserBadges> getBadges() {
            return badges;
        }

        public void setBadges(ArrayList<UserBadges> badges) {
            this.badges = badges;
        }

        public String getName ()
        {
            return name;
        }

        public String getPhoto_url() {
            return photo_url;
        }

        public void setPhoto_url(String photo_url) {
            this.photo_url = photo_url;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getUser_id ()
        {
            return user_id;
        }

        public void setUser_id (String user_id)
        {
            this.user_id = user_id;
        }


        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public double getBooster_constant() {
            return booster_constant;
        }

        public void setBooster_constant(double booster_constant) {
            this.booster_constant = booster_constant;
        }

        public Integer getBooster_multiplier() {
            return booster_multiplier;
        }

        public void setBooster_multiplier(Integer booster_multiplier) {
            this.booster_multiplier = booster_multiplier;
        }

        public int getSpotlight() {
            return spotlight;
        }

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public void setSpotlight(int spotlight) {
            this.spotlight = spotlight;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [badges = "+badges+", name = "+name+", user_id = "+user_id+", points = "+points+", booster_constant = "+booster_constant+", booster_multiplier = "+booster_multiplier+"]";
        }

        public class UserBadges implements Serializable{
            private Integer id;

            private String updated_at;

            private String description;

            private Integer badge_id;

            private String name;

            private String created_at;

            private Integer user_id;

            private String type;


            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public Integer getBadge_id() {
                return badge_id;
            }

            public void setBadge_id(Integer badge_id) {
                this.badge_id = badge_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public Integer getUser_id() {
                return user_id;
            }

            public void setUser_id(Integer user_id) {
                this.user_id = user_id;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [id = "+id+", updated_at = "+updated_at+", description = "+description+", badge_id = "+badge_id+", name = "+name+", created_at = "+created_at+", user_id = "+user_id+", type = "+type+"]";
            }
        }
    }

}
