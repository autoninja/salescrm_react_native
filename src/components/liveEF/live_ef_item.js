import React, { Component } from 'react'
import { View, Text, Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Alert} from 'react-native'


export default class LiveEFItem extends Component {
  constructor(props) {
    super(props);
    this.state ={showImagePlaceHolder:true};
  }
  _onLongPressButton() {
    console.log('Long press worked');
    Alert.alert('You long-pressed the button!')
  }
  render() {
    let imageSource;
    let placeHolder
    if(this.props.from_location) {
      imageSource = require( '../../images/ic_location.png');
      imageStyle = {position:'absolute',width:36, height:36};
      placeHolder = null;

    }
    else {
      imageSource = {uri : this.props.info.dp_url};
      imageStyle = {position:'absolute',width:36, height:36, borderRadius: 36/2,};
      placeHolder = <View style={{position:'absolute',display:(this.state.showImagePlaceHolder?'flex':'none'),width:36, height:36, borderRadius: 36/2,backgroundColor:'#FF8000', alignItems:'center', justifyContent:'center'}}><Text style={{color:'#fff'}}>{this.props.info.name.charAt(0).toUpperCase()}</Text></View>;;

      }

    if(this.props.showPercentage) {
      return (
        <View style={{marginTop:10}}>
        <View style={{marginTop:4, marginBottom:4, flexDirection:'row', alignItems:'center'}}>
            <Text style={{flex:1, fontSize:15, color:'#2D4F8B',  }}>{this.props.info.name}</Text>
            <TouchableOpacity 
             hitSlop={{top: 20, bottom: 20, left: 20, right: 20}} 
            onPress={()=>{this.props.leadSourceCarModelWiseETVBR()}}>
            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center',padding:3, }}>
                <Text style={{fontSize:12, color:'#007FFF',}}>Source / Model</Text>
                </View>
            </TouchableOpacity>
        </View>  
      <View style={{flexDirection: 'row', height:60, backgroundColor:'#eceff1',alignItems:'center',borderRadius:6}}>

      <View style={{flex:1.5, alignItems:'center', justifyContent:'center'}}>
              {placeHolder}
              <TouchableOpacity
              style={{position:'absolute',}}
              onPress = {() => this.props.onPress()}
              onLongPress={() =>{
              this.props.onLongPress('user_info', 
              {info:{name:this.props.info.name, dp_url:this.props.info.dp_url}})
               }}>
              <Image
                source= {imageSource}
                style={{width:36, height:36, borderRadius: 36/2,}}
              />
              </TouchableOpacity>
      </View>






      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#303030',  }} >{this.props.rel.e}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#303030',  }} >{this.props.rel.f_i}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#303030',  }} >{this.props.rel.f_o}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#00bfa5',  }} >-</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

          <Text style={{ color:'#00bfa5',  }} >-</Text>
      </View>


      </View>
      </View>
    );
    }
    else {
    return(
      <View style={{marginTop:10}}>
        <View style={{marginTop:4, marginBottom:4, flexDirection:'row', alignItems:'center'}}>
            <Text style={{flex:1, fontSize:15, color:'#2D4F8B',  }}>{this.props.info.name}</Text>
            <TouchableOpacity 
             hitSlop={{top: 20, bottom: 20, left: 20, right: 20}} 
            onPress={()=>{this.props.leadSourceCarModelWiseETVBR()}}>
            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center',padding:3, }}>
                <Text style={{fontSize:12, color:'#007FFF',}}>Source / Model</Text>
                </View>
            </TouchableOpacity>
        </View>  
      <View style={{flexDirection: 'row', height:60, backgroundColor:'#eceff1', alignItems:'center', borderRadius:6}}>

      <View style={{flex:1.5, alignItems:'center', justifyContent:'center'}}>
              {placeHolder}
              <TouchableOpacity
              style={{position:'absolute',}}
              onPress = {() => this.props.onPress()}
              onLongPress={() =>{
              this.props.onLongPress('user_info', 
              {info:{name:this.props.info.name, dp_url:this.props.info.dp_url}})
               }}>
              <Image
                source= {imageSource}
                style={{width:36, height:36, borderRadius: 36/2,}}
              />
              </TouchableOpacity>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onPress = {() => this.props.onPress()}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Retails',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>
        <Text style={{
          color:'#303030',  textDecorationLine:'underline'}} >{this.props.abs.r}</Text>
        </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030',  }} >{this.props.targets.r}</Text>
        </View>
      </View>




      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
         onPress = {() => this.props.onPress()}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Exchanged',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#303030',  textDecorationLine:'underline'}} >{this.props.abs.e}</Text>
        </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030',  }} >{this.props.targets.e}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
         onPress = {() => this.props.onPress()}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'In House Financed',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#303030',  textDecorationLine:'underline'}} >{this.props.abs.f_i}</Text>
</TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030',  }} >{this.props.targets.f_i}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
         onPress = {() => this.props.onPress()}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Out House Financed',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#303030',  textDecorationLine:'underline'}} >{this.props.abs.f_o}</Text>
          </TouchableOpacity>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
          onPress = {() => this.props.onPress()}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Pending Bookings',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#00bfa5',  textDecorationLine:'underline'}} >{this.props.abs.p_b}</Text>
            </TouchableOpacity>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
          onPress = {() => this.props.onPress()}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Live Enquiries',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#00bfa5',  textDecorationLine:'underline'}} >{this.props.abs.l_e}</Text>
              </TouchableOpacity>
      </View>


      </View>
      </View>
    );
  }
  }
}
