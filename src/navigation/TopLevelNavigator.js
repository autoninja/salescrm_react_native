import { createStackNavigator, createAppContainer } from 'react-navigation';
import Home from './DrawerNavigator';
import LoginStack from './LoginStackNavigator';
import RefreshData from '../components/refresh_data';
import SplashScreen from '../components/splash_screen';
import SwitchDealership from '../components/switch_dealership';
import CreateEnquiryRouter from '../containers/CreateEnquiry/Main/CreateEnquiry.navigation.ios'
import GameIntro from '../components/game_intro';
import ForceUpdate from '../components/force_update'
import C360MainHolder from '../containers/C360/C360MainHolder.ios';
import ExchangeCar from '../containers/CreateEnquiry/Main/exchange_car'
import ColdVisitRouter from '../containers/ColdVisit/Form/ColdVisit.navigation.ios';
import UpdateExchangeCarDetails from '../containers/CreateEnquiry/Main/update_exchange_car_details';
import FormRenderingRouterIOS from '../components/DynamicForms/form_rendering_index.ios'
import BookBike from '../containers/BookBike'
import BookVehicleInit from '../containers/BookVehicle/Init/BookVehicleInit'

const TopLevelNavigator = createAppContainer(createStackNavigator({
    SplashScreen: {
        screen: SplashScreen,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    Home: {
        screen: Home,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    Login: {
        screen: LoginStack,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    RefreshData: {
        screen: RefreshData,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    GameIntro: {
        screen: GameIntro,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    SwitchDealership: {
        screen: SwitchDealership,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    CreateEnquiryRouter: {
        screen: CreateEnquiryRouter,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    ColdVisitRouter: {
        screen: ColdVisitRouter,
        navigationOptions: ({ }) => ({
            header: null,
        }),
    },
    ForceUpdate: {
        screen: ForceUpdate,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    C360MainScreen: {
        screen: C360MainHolder,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    BookBike: {
        screen: BookBike,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    BookCar: {
        screen: BookVehicleInit,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    ExchangeCar: {
        screen: ExchangeCar,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
    UpdateExchangeCarDetails: {
        screen: UpdateExchangeCarDetails,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },
    FormRenderingRouter: {
        screen: FormRenderingRouterIOS,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    }

},
    {
        initialRouteName: 'SplashScreen',
    },

    {
        cardStyle: {
            backgroundColor: '#303030BE',
            opacity: 1,
        },
        mode: 'modal'
    },
));

export default TopLevelNavigator;
