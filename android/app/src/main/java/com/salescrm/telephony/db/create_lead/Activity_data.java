package com.salescrm.telephony.db.create_lead;

import io.realm.RealmObject;

/**
 * Created by bharath on 8/12/16.
 */

public class Activity_data extends RealmObject
{
    private Activity_owner activity_owner;

    private String visit_type;

    private String scheduledDateTime;

    private String activity;

    private String address;

    private String visit_time;
    private String location_id;

    private String carId;
    private String carName;
    private String assignName;
    private String activityName;
    private String remarks;

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getAssignName() {
        return assignName;
    }

    public void setAssignName(String assignName) {
        this.assignName = assignName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getVisit_time() {
        return visit_time;
    }

    public void setVisit_time(String visit_time) {
        this.visit_time = visit_time;
    }

    public Activity_owner getActivity_owner ()
    {
        return activity_owner;
    }

    public void setActivity_owner (Activity_owner activity_owner)
    {
        this.activity_owner = activity_owner;
    }

    public String getVisit_type ()
    {
        return visit_type;
    }

    public void setVisit_type (String visit_type)
    {
        this.visit_type = visit_type;
    }

    public String getScheduledDateTime ()
    {
        return scheduledDateTime;
    }

    public void setScheduledDateTime (String scheduledDateTime)
    {
        this.scheduledDateTime = scheduledDateTime;
    }

    public String getActivity ()
    {
        return activity;
    }

    public void setActivity (String activity)
    {
        this.activity = activity;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [activity_owner = "+activity_owner+", visit_type = "+visit_type+", scheduledDateTime = "+scheduledDateTime+", activity = "+activity+"]";
    }


}