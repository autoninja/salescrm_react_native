package com.salescrm.telephony.interfaces;

/**
 * Created by bharath on 6/10/17.
 */

public interface TeamDashboardSwipedListener {
    void onTeamDashboardSwiped(int position,int tabCount);
}
