import React, { Component } from 'react'
import { View, Text, } from 'react-native'

export default class LiveEFFooter extends Component {

  render() {
      return(
      <View style={{
      marginTop:40, alignItems:'flex-start', borderRadius:6, padding:5}}>

      <View style= {{flex:1,flexDirection: 'row',
        }}>
          <Text style={{ color:'#00bfa5', fontSize:14, textAlign:'center'}} >PB - </Text>
          <Text style={{ color:'#546e7a',fontSize:14, textAlign:'center'}} >Pending Bookings(Booked but no retail)</Text>
      </View>

      <View style= {{flex:1,flexDirection: 'row',marginTop:10}}>
          <Text style={{ color:'#00bfa5',fontSize:14,textAlign:'center'}} >LE - </Text>
          <Text style={{ color:'#546e7a',fontSize:14,textAlign:'center'}} >Live Enquiries</Text>
      </View>

      </View>
    )
  }
}
