package com.salescrm.telephony.response.gamification;

import java.util.ArrayList;

/**
 * Created by nndra on 30-Jan-18.
 */

public class GamificationTeamProfileResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private String title;

        private String team_average_points;

        private Points points;

        private String team_leader_name;

        private String team_leader_photo_url;

        public String getTitle ()
        {
            return title;
        }

        public void setTitle (String title)
        {
            this.title = title;
        }

        public String getTeam_average_points ()
        {
            return team_average_points;
        }

        public void setTeam_average_points (String team_average_points)
        {
            this.team_average_points = team_average_points;
        }

        public Points getPoints ()
        {
            return points;
        }

        public void setPoints (Points points)
        {
            this.points = points;
        }

        public String getTeam_leader_name ()
        {
            return team_leader_name;
        }

        public void setTeam_leader_name (String team_leader_name)
        {
            this.team_leader_name = team_leader_name;
        }

        public String getTeam_leader_photo_url ()
        {
            return team_leader_photo_url;
        }

        public void setTeam_leader_photo_url (String team_leader_photo_url)
        {
            this.team_leader_photo_url = team_leader_photo_url;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [title = "+title+", team_average_points = "+team_average_points+", points = "+points+", team_leader_name = "+team_leader_name+", team_leader_photo_url = "+team_leader_photo_url+"]";
        }
    }

    public class Points
    {
        private ArrayList<Users> users;

        private String[] others;

        public ArrayList<Users> getUsers ()
        {
            return users;
        }

        public void setUsers (ArrayList<Users> users)
        {
            this.users = users;
        }

        public String[] getOthers ()
        {
            return others;
        }

        public void setOthers (String[] others)
        {
            this.others = others;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [users = "+users+", others = "+others+"]";
        }
    }

    public class Users
    {
        private String dse_photo_url;

        private String name;

        private String user_id;

        private String points;

        private Integer above_average;

        private Integer spotlight;

        public String getDse_photo_url ()
        {
            return dse_photo_url;
        }

        public void setDse_photo_url (String dse_photo_url)
        {
            this.dse_photo_url = dse_photo_url;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getUser_id ()
        {
            return user_id;
        }

        public void setUser_id (String user_id)
        {
            this.user_id = user_id;
        }

        public String getPoints ()
        {
            return points;
        }

        public void setPoints (String points)
        {
            this.points = points;
        }

        public Integer getAbove_average() {
            return above_average;
        }

        public Integer getSpotlight() {
            return spotlight;
        }

        public void setSpotlight(Integer spotlight) {
            this.spotlight = spotlight;
        }

        public void setAbove_average(Integer above_average) {
            this.above_average = above_average;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [dse_photo_url = "+dse_photo_url+", name = "+name+", user_id = "+user_id+", points = "+points+"]";
        }
    }

    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }


}