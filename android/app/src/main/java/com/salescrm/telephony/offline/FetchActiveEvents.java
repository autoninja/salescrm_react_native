package com.salescrm.telephony.offline;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.events.EventDB;
import com.salescrm.telephony.db.events.EventsLeadSourceCategoryDB;
import com.salescrm.telephony.db.events.EventsLeadSourceDB;
import com.salescrm.telephony.db.events.EventsMainDB;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.ActiveEventsResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 15/2/17.
 * Call this to get the lead basic elements
 */
public class FetchActiveEvents implements Callback<ActiveEventsResponse> {
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;

    public FetchActiveEvents(OfflineSupportListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }

    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getActiveEvents(this);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ACTIVE_EVENTS);
        }
    }


    @Override
    public void success(final ActiveEventsResponse activeEventsResponse, Response response) {
        Util.updateHeaders(response.getHeaders());

        if (activeEventsResponse.getStatusCode() != null && !activeEventsResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + activeEventsResponse.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ACTIVE_EVENTS);
        } else {
            if (activeEventsResponse.getResult() != null) {
                System.out.println("Success:1" + activeEventsResponse.getMessage());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm1) {
                        insertData(realm1, activeEventsResponse.getResult());

                    }
                });
                listener.onFetchActiveEvents(activeEventsResponse);
            } else {
                // relLoader.setVisibility(View.GONE);

                System.out.println("Success:2" + activeEventsResponse.getMessage());
                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ACTIVE_EVENTS);
                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }


    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ACTIVE_EVENTS);

    }

    private void insertData(Realm realm, ActiveEventsResponse.Result result) {
        System.out.println("Insert Data Called");
        realm.delete(EventDB.class);
        realm.delete(EventsLeadSourceDB.class);
        realm.delete(EventsLeadSourceCategoryDB.class);
        realm.delete(EventsMainDB.class);

        for (ActiveEventsResponse.Locations location : result.getLocations()) {
            RealmList<EventsLeadSourceCategoryDB> eventsLeadSourceCategoryDBS = new RealmList<EventsLeadSourceCategoryDB>();
            for (ActiveEventsResponse.Lead_source_categories lead_source_category : location.getLead_source_categories()) {
                RealmList<EventsLeadSourceDB> eventsLeadSourceDBS = new RealmList<EventsLeadSourceDB>();
                for (ActiveEventsResponse.Lead_sources lead_source : lead_source_category.getLead_sources()) {
                    RealmList<EventDB> eventDBS = new RealmList<EventDB>();
                    for (ActiveEventsResponse.Events event : lead_source.getEvents()) {
                        eventDBS.add(new EventDB(event.getId(), event.getName(), event.getStart_date(), event.getEnd_date()));
                    }
                    eventsLeadSourceDBS.add(new EventsLeadSourceDB(lead_source.getId(), lead_source.getName(), eventDBS));
                }
                eventsLeadSourceCategoryDBS.add(new EventsLeadSourceCategoryDB(
                        lead_source_category.getId(),
                        lead_source_category.getName(),
                        eventsLeadSourceDBS
                ));
            }
            realm.copyToRealm(new EventsMainDB(
                    location.getId(),
                    location.getName(),
                    eventsLeadSourceCategoryDBS));
        }


    }

}
