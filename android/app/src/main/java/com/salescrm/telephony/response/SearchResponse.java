package com.salescrm.telephony.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bharath on 11/7/16.
 */
public class SearchResponse {
    private String statusCode;

    private String message;

    private List<Result> result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result implements Serializable {
        private String lastName;

        private String status;

        private String leadId;

        private String tag_name;

        private String lead_created_date;

        private String lead_enquiry_date;

        private String variant_name;

        private String crmname;

        private String username;

        private String stage_name;

        private String address;

        private String enquiry_number;

        private String gender;

        private String active;

        private String dsemobno;

        private String firstName;

        private String dsename;
        private boolean is_view_allowed;
        private String location_name;

        private List<Activities_data> activities_data;

        private String stage_id;

        private String tag_id;

        private String mobile;
        private String assign_id;

        private boolean my_location_lead;

        public boolean is_view_allowed() {
            return is_view_allowed;
        }

        public void setIs_view_allowed(boolean is_view_allowed) {
            this.is_view_allowed = is_view_allowed;
        }

        public boolean isMy_location_lead() {
            return my_location_lead;
        }

        public void setMy_location_lead(boolean my_location_lead) {
            this.my_location_lead = my_location_lead;
        }

        public String getAssign_id() {
            return assign_id;
        }

        public void setAssign_id(String assign_id) {
            this.assign_id = assign_id;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getLeadId() {
            return leadId;
        }

        public void setLeadId(String leadId) {
            this.leadId = leadId;
        }

        public String getTag_name() {
            return tag_name;
        }

        public void setTag_name(String tag_name) {
            this.tag_name = tag_name;
        }

        public String getLead_created_date() {
            return lead_created_date;
        }

        public void setLead_created_date(String lead_created_date) {
            this.lead_created_date = lead_created_date;
        }

        public String getLead_enquiry_date() {
            return lead_enquiry_date;
        }

        public void setLead_enquiry_date(String lead_enquiry_date) {
            this.lead_enquiry_date = lead_enquiry_date;
        }

        public String getVariant_name() {
            return variant_name;
        }

        public void setVariant_name(String variant_name) {
            this.variant_name = variant_name;
        }

        public String getCrmname() {
            return crmname;
        }

        public void setCrmname(String crmname) {
            this.crmname = crmname;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getStage_name() {
            return stage_name;
        }

        public void setStage_name(String stage_name) {
            this.stage_name = stage_name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEnquiry_number() {
            return enquiry_number;
        }

        public void setEnquiry_number(String enquiry_number) {
            this.enquiry_number = enquiry_number;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            if(gender==null){
                this.gender = "Mr";
            }
            else {
                this.gender = gender;
            }
        }

        public String getActive() {
            return active;
        }

        public void setActive(String active) {
            this.active = active;
        }

        public String getDsemobno() {
            return dsemobno;
        }

        public void setDsemobno(String dsemobno) {
            this.dsemobno = dsemobno;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getDsename() {
            return dsename;
        }

        public void setDsename(String dsename) {
            this.dsename = dsename;
        }

        public List<Activities_data> getActivities_data() {
            return activities_data;
        }

        public void setActivities_data(List<Activities_data> activities_data) {
            this.activities_data = activities_data;
        }

        public String getStage_id() {
            return stage_id;
        }

        public void setStage_id(String stage_id) {
            this.stage_id = stage_id;
        }

        public String getTag_id() {
            return tag_id;
        }

        public void setTag_id(String tag_id) {
            this.tag_id = tag_id;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getLocation_name() {
            return location_name;
        }

        public void setLocation_name(String location_name) {
            this.location_name = location_name;
        }

        @Override
        public String toString() {
            return "ClassPojo [lastName = " + lastName + ", status = " + status + ", leadId = " + leadId + ", tag_name = " + tag_name + ", lead_created_date = " + lead_created_date + ", lead_enquiry_date = " + lead_enquiry_date + ", variant_name = " + variant_name + ", crmname = " + crmname + ", username = " + username + ", stage_name = " + stage_name + ", address = " + address + ", enquiry_number = " + enquiry_number + ", gender = " + gender + ", active = " + active + ", dsemobno = " + dsemobno + ", firstName = " + firstName + ", dsenameId = " + dsename + ", activities_data = " + activities_data + ", stage_id = " + stage_id + ", tag_id = " + tag_id + ", mobile = " + mobile + "]";
        }

        public class Activities_data implements Serializable {
            private String activity_scheduled_date;

            private String scheduled_activity_name;

            public String getActivity_scheduled_date() {
                return activity_scheduled_date;
            }

            public void setActivity_scheduled_date(String activity_scheduled_date) {
                this.activity_scheduled_date = activity_scheduled_date;
            }

            public String getScheduled_activity_name() {
                return scheduled_activity_name;
            }

            public void setScheduled_activity_name(String scheduled_activity_name) {
                this.scheduled_activity_name = scheduled_activity_name;
            }

            @Override
            public String toString() {
                return "ClassPojo [activity_scheduled_date = " + activity_scheduled_date + ", scheduled_activity_name = " + scheduled_activity_name + "]";
            }
        }
    }
}

