/** @format */

import {
  AppRegistry,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import codePush from "react-native-code-push";
import App from "./App";
import { name as appName } from "./app.json";

import React from "react";

import { Provider } from "react-redux";
import { Provider as PaperProvider } from "react-native-paper";

import configureStore from "./src/store";

import * as Sentry from "@sentry/react-native";

if (!__DEV__) {
  Sentry.init({
    dsn: "https://41c8cdb33fa746f6beaae1f6b3266e97@sentry.io/1761934"
  });
}

const store = configureStore();

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE
};

class NinjaCRMSalesIOS extends React.Component {
  constructor(props) {
    super(props);
    this.state = { updating: false, is_manually_closed: false };
  }
  codePushStatusDidChange(status) {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        if (this.state.updating) {
          this.setState({ updating: false });
        }
        console.log("Checking for updates.");
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        this.setState({ updating: true });
        console.log("Downloading package.");
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        this.setState({ updating: true });
        console.log("Installing update.");
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        if (this.state.updating) {
          this.setState({ updating: false });
        }
        console.log("Up-to-date.");
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        if (this.state.updating) {
          this.setState({ updating: false });
        }
        console.log("Update installed.");
        break;
    }
  }
  codePushDownloadDidProgress(progress) {
    console.log(
      progress.receivedBytes + " of " + progress.totalBytes + " received."
    );
  }
  render() {
    if (this.state.updating && !this.state.is_manually_closed) {
      return (
        <View
          style={{
            backgroundColor: "#fff",
            flex: 1,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignSelf: "center",
              alignItems: "center"
            }}
          >
            <ActivityIndicator
              animating={this.state.updating}
              style={{ alignSelf: "center" }}
              color="#2e5e86"
              size="small"
              hidesWhenStopped={true}
            />
            <Text style={{ marginTop: 8, fontSize: 16, color: "#303030" }}>
              {" "}
              Please wait we are updating the app ..
            </Text>
            <TouchableOpacity
              style={{ width: "100%", alignItems: "center" }}
              onPress={() => {
                this.setState({ is_manually_closed: true });
              }}
            >
              <View
                style={{
                  padding: 5,
                  justifyContent: "center",
                  marginTop: 8,
                  backgroundColor: "#808080",
                  borderRadius: 3
                }}
              >
                <Text
                  adjustsFontSizeToFit={true}
                  numberOfLines={1}
                  style={{
                    textAlign: "center",
                    fontSize: 12,
                    textAlignVertical: "center",
                    color: "#fff"
                  }}
                >
                  Do in background
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      );
    }

    return (
      <Provider store={store}>
        <PaperProvider>
          <App />
        </PaperProvider>
      </Provider>
    );
  }
}

AppRegistry.registerComponent(appName, () =>
  codePush(codePushOptions)(NinjaCRMSalesIOS)
);
