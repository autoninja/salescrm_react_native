package com.nll.nativelibs.callrecording;

import android.os.Build;
import android.util.Log;

import java.util.Locale;

public class DeviceHelper {
    private static String TAG = "DeviceHelper";

    /**
     * You might want to return false for Note 8 Global version s it does not have any issues
     *
     * @return
     */
    public static boolean isAndroid71FixRequired() {
        return (Build.VERSION.RELEASE.equals("7.1.1") || Build.VERSION.RELEASE.equals("7.1.2"));
    }

    public static boolean useApi3() {
        return isHuaweiWithApi3() || isMotorolaWithApi3();
    }

    private static boolean isHuaweiAndroid8AndAbove() {
        return getDeviceManufacturer().equals("HUAWEI") && Build.VERSION.SDK_INT >= 26;
    }

    private static boolean isHuaweiWithApi3() {
        //always 10 plus on Android 8 and above
        if (isHuaweiAndroid8AndAbove()) {
            return true;
        } else {
            //check for others
            if (getDeviceManufacturer().equals("HUAWEI")) {
                //Check ro.build.hw_emui_api_level property and call it only if it's 10 or higher (emui 4.1+)
                int emuiVersion = PropManager.getInt("ro.build.hw_emui_api_level");
                Log.d(TAG, "emuiVersion: " + emuiVersion);
                return emuiVersion >= 10;
            } else {
                return false;
            }
        }
    }

    public static void sleepForAndroid71() {
        Log.d(TAG, "Android 7.1.1 requires more delay and extra sleep. Sometimes delay does not help either. It seems to be issue with OnePlus and Sony 7.1.1");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    private static boolean isMotorolaWithApi3() {
        //motorola move to api 3 on android 8
        return getDeviceManufacturer().equals("MOTOROLA") && Build.VERSION.SDK_INT >= 26 && !isNexus();

    }

    private static boolean isNexus() {
        return getDeviceModel().contains("NEXUS");
    }

    private static String getDeviceModel() {
        try {
            String model = Build.MODEL;
            return model.toUpperCase(Locale.ENGLISH);
        } catch (Exception e) {
            return "";
        }
    }

    private static String getDeviceManufacturer() {
        try {
            String manufacturer = Build.MANUFACTURER;
            return manufacturer.toUpperCase(Locale.ENGLISH);

        } catch (Exception e) {
            return "";
        }
    }
}
