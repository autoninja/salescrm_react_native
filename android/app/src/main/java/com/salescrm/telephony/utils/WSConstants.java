package com.salescrm.telephony.utils;

import android.util.SparseArray;
import android.util.SparseBooleanArray;

import com.salescrm.telephony.R;
import com.salescrm.telephony.model.Locations;
import com.salescrm.telephony.model.NewFilter.EtvbrSelectedFilters;
import com.salescrm.telephony.model.NewFilter.Filter;
import com.salescrm.telephony.model.NewFilter.NewFilterRequest;
import com.salescrm.telephony.model.gamification.BadgesModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ravindra P on 27-11-2015.
 */
public class WSConstants {

   // public static final String APP_VERSION = "1.3";

    public static final String BEARER = "Bearer ";
    public static final String RESPONSE_OK = "2002";
    public static final int    RESPONSE_STATUS_OK = 2002;
    public static final String TOKEN_EXPIRED = "401";
    public static final String LOGOUT_CODE = "4003";
    public static final String AUTONINJA_SMS = "ANINJA";
    public static final String AUDIO_RECORDER_FOLDER = ".Ninja_Crm";
    public static final String LEAD_TAG_HOT = "Hot";
    public static final String LEAD_TAG_HOT_ID = "6";
    public static final String LEAD_TAG_WARM = "Warm";
    public static final String LEAD_TAG_WARM_ID = "7";
    public static final String LEAD_TAG_COLD = "Cold";
    public static final String LEAD_TAG_COLD_ID = "8";
    public static final String BUYER_TAG_FIRST_TIME = "First Time";
    public static final String BUYER_TAG_FIRST_TIME_ID = "1";
    public static final String BUYER_TAG_EXCHANGE = "Exchange Car";
    public static final String BUYER_TAG_EXCHANGE_ID = "3";
    public static final String BUYER_TAG_ADDITIONAL = "Additional Car";
    public static final String BUYER_TAG_ADDITIONAL_ID = "2";
    public static final String LEAD_TAG_OVERDUE = "Overdue";
    public static final String LEAD_TAG_OVERDUE_ID = "5";
    public static final String RESULT_STATUS_OPEN = "1";
    public static final String RESULT_STATUS_CLOSED = "0";
    public static final String PROPERTY_CRM_TYPE = "crm_type";//0: dse 1:insurance
    public static final String PROPERTY_DSE_NAME = "dse_name";
    public static final String PROPERTY_DSE_ID = "dse_id";
    public static final int NOTIFICATION_TYPE_SMS = 1;
    public static final int NOTIFICATION_TYPE_EMAIL = 2;
    public static final String PROPERTY_PENDING_CURRENT_PAGE = "pending_current_page";
    public static final String PROPERTY_PENDING_TOTAL = "pending_total";
    public static final String PROPERTY_TODAY_TOTAL = "today_total";
    public static final String FUTURE = "future";
    public static final String PENDING = "pending";
    public static final String TOMORROW = "tomorrow";
    public static final String TODAY = "today";
    public static final String TODAY_WITH_PENDING = "today_with_pending";
    public static final String ORDER_DESC = "DESC";
    public static final String ORDER_ASC = "ASC";
    public static final String AUTHORIZATION = "Authorization";
    public static final String MOBILE_OS = "os";
    public static final String MOBILE_OS_VALUE = "Android";
    public static final String MOBILE_VERSION = "version";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_TYPE_VAL = "application/json";
    public static final String[] LEAD_GENDERS = new String[]{"Mr", "Miss", "M/S", "Dr"};
    public static final String[] LEAD_MODE_OF_PAYMENTS = new String[]{"<Select>","Full Cash", "Company Lease Plan", "In House Loan", "Out House Loan"};
    public static final List<String> genderList = Arrays.asList("<Select>","Mr", "Miss", "M/S", "Dr");
    public static final List<String> paymentModes = Arrays.asList("<Select>","Full Cash", "Company Lease Plan", "In House Loan", "Out House Loan");
    public static final List<String> customerType = Arrays.asList("Customer Type *","Corporate(Individual)","Individual", "Government",
            "Corporate(Company)", "Fleet(Individual)", "Fleet(Company)");
    public static final List<String> customerTypes = Arrays.asList("Corporate(Individual)","Individual", "Government",
            "Corporate(Company)", "Fleet(Individual)", "Fleet(Company)");
    public static final List<String> customerCareOfType = Arrays.asList("S/O", "D/O", "W/O");
    public static final List<String> buyerTypeList = Arrays.asList("<Select>","First Time", "Additional Car", "Exchange Car");
    public static final String PROPERTY_ADD_LEAD_NUMBER = "add_lead_number";
    public static final int ADDRESS_TYPE_OFFICE = 0;
    public static final int ADDRESS_TYPE_HOME = 1;
    public static final String USER_ROLE_BRANCH_HEAD = "21";
    public static final String USER_ROLE_OPERATIONS = "30";
    public static final String USER_ROLE_EVALUATOR = "28";
    public static final String MANAGER_ROLE_ID = "8";
    public static final String TEAM_LEAD_ROLE_ID = "6";
    public static final String DSE_ROLE_ID = "4";
    public static final String BILLING_EXECUTIVE = "11";
    public static final String USER_ROLE_VERIFIER = "32";
    public static final String REFRESH_OFFLINE_DATA ="refresh_offline_data" ;
    public static final String APP_LAST_OPENED_DATE ="app_last_opened_date" ;
    public static final String IS_ACTION_PLAN_SYNC = "actionPlanSynced";
    public static final String IS_SYNC_ENABLED = "app_sync_status";
    public static final String CAR_CATEGORY_BOTH ="BOTH" ;
    public static final String CAR_CATEGORY_OLD_CAR ="OLD_CAR" ;
    public static final String NINJA_BROADCAST_MESSAGE = "ninja_broadcast_message";
    public static final String BASIC_OFFLINE_LAST_UPDATED = "basic_offline_last_updated";
    public static final String NINJA_BROADCAST_MESSAGE_FORCE_SYNC = "ninja_broadcast_message_force_sync";
    public static final String APP_VERSION_NAME = "app_version";
    public static final String APP_VERSION_CODE = "app_version_code";
    public static final String APP_LEAD_LIST_COUNT = "lead_list_count";
    public static final String REFRESH_FROM_HELP = "refresh_from_help";
    public static final String VALIDATION_INPUT_NUMBER = "number";
    public static final int PNT_CHECKBOX_QUESTION_ID =65 ;
    public static final String IS_FILTER_ALREADY_APPLIED = "is_filter_already_applied";
    public static final String PROPERTY_MY_LEAD_VIEWED = "my_lead_viewed";
    public static final String LEAD_ID = "leadId";
    public static final String SCHEDULED_ACTIVITY_ID = "scheduledActivityId";
    public static final String SCHEDULED_DATE = "scheduledDate";
    public static final String ACTIVITY_NAME = "activityName";
    public static final String NOTIFICATION_CUSTOMER_NAME = "notification_customer_name";
    public static final String NOTIFICATION_ACTIVITY_ID = "notification_activity_id";
    public static final String NOTIFICATION_LOCATION_ID = "notification_location_id";
    public static final int DEFAULT_NOTIFICATION_BEFORE_TIME = 10;
    public static final String OPEN_C360_FROM_NOTIFICATION_TRAY = "c360_from_notification_tray";
    public static final String OPEN_TASKS_FROM_NOTIFICATION_TRAY = "open_tasks_from_notification_tray";
    public static final String NOTIFICATION_SERVER = "notification_server";
    public static final String NOTIFICATION_FIREBASE_EVENT = "notification_firebase_event";
    public static final String NOTIFICATION_CLICKED_NAME = "notification_clicked_name";
    public static final String NOTIFICATION_TRIGGERED_FROM = "notification_triggered_from";
    public static final String USER_ID = "user_id";
    public static final String USER_ROLE_ID = "user_role_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_PHOTO_URL = "user_photo_url";
   /* public static final String USER_ROLE_DSE = "4";
    public static final String USER_ROLE_TEAM_LEAD= "6";
    public static final String USER_ROLE_SALES_MANAGER = "8";*/
    public static final String C360_TAB_POSITION = "c360_tab_position";
    public static final String C360_PREV_FORM_FOR_BOOK_CAR = "c360_prev_form_for_book_car";
    public static final int TODAY_TAB = 0;
    public static final int ALL_TAB = 1;
    public static final int DONE_TAB = 2;
    public static final String IS_USER_DATA_FETCH_REQUIRED ="is_user_data_fetch_required" ;
    public static final int DEFAULT_SCHEDULED_ACTIVITY_ID_POST_BOOKING = -1111;
    public static final String USER_ROLE_EVALUATOR_MANAGER = "31";

    public static String PREF_NAME = "pref_names";
    public static String PROPERTY_LOGGED_STATUS = "login_status";
    public static String PROPERTY_DEALER_SELECTED_STATUS = "property_dealer_selected_status";
    public static String PROPERTY_DEALER_SELECTED_ID = "property_dealer_selected_id";
    public static String PROPERTY_IS_C360_DESTROYED = "is_c306_detroyed";
    public static String PROPERTY_FIRST_TIME = "property_first_time";
    public static String PROPERTY_ACCESS_TOKEN = "property_access_token";
    public static String PROPERTY_USER_NAME = "property_user_name";
    public static String PROPERTY_PASSWORD = "property_password";
    public static String PROPERTY_USER_MOBILE = "property_user_mobile";
    public static String PROPERTY_USER_PROFILE_PIC_URL = "property_user_profile_pic_url";
    public static String PROPERTY_TOKEN_EXPIRES_IN = "property_token_expires_in";
    public static String PROPERTY_DEALER_ID = "dealerId";
    public static String PROPERTY_IMEI_NUMBER = "imei_number";
    public static String PROPERTY_DEALER_NAME = "dealerName";
    public static String PROPERTY_LEAD_ID = "property_lead_id";
    public static String PROPERTY_SHEDULE_ACTIVITY_ID = "property_shedue_activity_id";
    public static String PROPERTY_SCHEDULED_ACTIVITY_ID = "property_scheduled_activity_id";
    public static String PROPERTY_BRAND_ID = "property_brand_id";
    public static String PROPERTY_LEAD_LEAST_UPDATED = "lead_last_updated";
    public static String PROPERTY_ROLE = "role";
    public static String PROPERTY_ROLE_ID = "role_id";
    public static String PROPERTY_EMAIL = "email";
    public static String PROPERTY_NAME= "name";
    public static String PROPERTY_USER_ID= "user_id";
    public static String PROPERTY_FABRICS_NEEDED = "is_fabrics_needed";
    public static String PROPERTY_TELEPHONY_NEEDED = "is_telephony_needed";
    public static String PROPERTY_IS_CALL_ONGOING = "is_call_on_going";
    public static String FCM_ID = "fcm_id";
    public static String IS_FCM_TABLE_CREATED = "is_fcm_table_created";
    public static String FCM_SYNC_VALUE = "fcm_sync_value";
    public static String PROPERTY_TARGET_MONTH = "target_month";
    public static String PROPERTY_LEADERBOARD_SELECTED_TAB = "leaderboard_selected_tab";
    public static String PROPERTY_TARGET_LOCATION = "target_location";
    public static String PROPERTY_NEW_UPDATE_NOTICE= "new_update_notice";
    public static String PROPERTY_SHOW_BALLOON_ANIMATION= "show_balloon_animation";
    public static String PROPERTY_RELOAD_C360= "reload_c360";
    public static String PROPERTY_SHOW_POST_BOOKING_TD_VISIT= "show_post_booking_td_visit";
    public static String PROPERTY_SELECTED_GAME_DATE= "selected_game_date";
    public static String PROPERTY_SYSTEM_DATE= "system_date";


    public static String PROPERTY_ACR_LICENSE_KEY= "property_acr_license_key";
    public static String PROPERTY_ACR_SERIAL= "property_acr_serial";

    public static String PROPERTY_DEVICE_ID = "device_id";

    public static String PROPERTY_IS_DEVICE_SUPPORT_NLL = "is_device_support_nll";
    public static String PROPERTY_IS_EVENT_DASHBOARD_UPDATE_REQUIRED = "is_event_dashboard_update_required";

    public static String PROPERTY_GOVERNMENT_API_KEYS = "government_api_keys";

    public static final String PROPERTY_S3_USER_NAME="property_s3_user_name";
    public static final String PROPERTY_S3_PASSWORD="property_s3_password";
    public static final String PROPERTY_S3_BUCKET_NAME="property_s3_bucket_name";
    public static final String PROPERTY_S3_SESSION_TOKEN ="property_s3_session_token";

    public static final String PROPERTY_HERE_MAP_APP_ID="property_here_map_app_id";
    public static final String PROPERTY_HERE_MAP_APP_CODE="property_here_map_app_code";

    public static final String PROPERTY_TWO_WHEELER_CLIENT="property_two_wheeler_client";

    public static final String PROPERTY_I_LOM_CLIENT="property_i_lom_client";

    public static final String PROPERTY_I_LOM_BANK="property_i_lom_bank_flow";

    public static final String PROPERTY_SCREEN_SHOT="property_screenshot";

    public static final String PROPERTY_HOT_LINE="property_hotline";

    public static final String PROPERTY_CALLING_LEAD_ID="property_calling_lead_id";

    public static final String PROPERTY_CALL_STAT_CREATED_AT="call_stat_created_at";

    public static final String BUYER_TYPE = "buyer_type";

    public static final String TOTAL_TASK_COUNT = "total_task_count";


    public static final int TYPE_ADD_CAR = 0;
    public static final int TYPE_EXCHANGE_CAR = 1;
    public static final String APP_CLOSED_LEAD_LIST_COUNT = "closed_lead_list";
    public static final String APP_ASSIGNED_LEAD_LIST_COUNT = "assigned_lead_list";
    public static final String APP_USER_ID = "app_user_id";
    public static final String APP_CLICKED_DSE_ID = "app_current_clicked_id";
    public static final String APP_TEAM_ID_ID = "app_team_id";

    public static final String CALL_STATE="call_state";
    public static final String IN_OUT="in_out";
    public static final String CALLED_MOBILE_NUMBER = "called_mobile_number";
    public static final String DB_ID = "db_id";
    public static final String RECORDING_TYPE = "recording_type";
    public static final String CALL_ID = "call_id";
    public static final String CALL_HAPPENED = "call_happened";
    public static final String INCOMING_CALLS = "incoming_calls";
    public static final String OUTGOING_CALLS = "outgoing_calls";
    public static final String FILE_NAME = "file_name";
    public static final String FILE_PATH = "file_path";
    public static final String ENABLE_MULTI_THREAD="enableMultithread";
    public static final String LIMIT_SYNC="limitSync";
    public static final String BACKGROUND_THREAD="backgroundThread";
  //  public static String BUCKET_NAME="an-ninjacrm-telephony";
    public static final String CALL_START="callStart";
    public static final String CALL_END="callEnd";
    public static final String CALL_DURATION="callDuration";
    public static final String MAX_ID="maxID";
    public static String TELEPHONY_LEAD_ID = "telephony_lead_id";
    public static boolean TELEPHONY_ENABLE = true;
    public static ArrayList<Locations> Locations = new ArrayList<Locations>();
    public static final String dseDataFetchedIntent = "DSEDataFetched";
    public static final String teamDataFetchedIntent = "TeamDataFetched";
    public static final String wallOfFameDataFetchedIntent = "WallOfFameDataFetched";
    public static final String LOCATION_GAME_ID = "LOCATION_ID";
    public static final String LOCATION_GAME_NAME= "LOCATION_GAME_NAME";
    public static final String PROFORMA_PDF_LINK = "proforma_pdf_link";
    public static final String PROFORMA_ENABLED = "proforma_enabled";
    public static final String PROFORMA_SENT = "proforma_sent";
    public static final String IS_GAME_INTRO_SEEN = "is_game_intro_seen";
    /**testing
     *
     */
    public static String DP_URI = "dp_uri";
    public static String DP_USER_NAME = "dp_user_name";
    public static final String BOOSTER_MODE = "booster_mode";
    public static final String RANK_VISIBILITY = "rank_visibility";

    public static String DP_TAKEN_FROM_DEVICE = "dp_taken_from_device";
    public static String ENCODED_URI = "encode_uri_dp";
    public static final String START_NINJA_NOTIFY= "START_NINJA_NOTIFY";
    public static final String MY_SALES_DIRECTORY = "SalesProfileDoc";
    public static final String MY_LOCATION_KEY = "location_id";
    public static final String START_DATE_ETVBR = "start_date_etvbr";
    public static final String END_DATE_ETVBR = "end_date_etvbr";
    public static final String SHOW_DATE_ETVBR = "show_date_etvbr";
    public static final String SHOW_GAMIFICATION = "false";

    //telephony server constants
    public static final String SERVER_NAME = "https://telephony.ninjacrm.com";
    public static final String SERVER_NAME_DB = "https://ninjacrm.com";

    //Filter related stuff
    public static int FRAGMENT_ID = 2;

    //constants for details page callbacks
    public static final int PERSONAL_DETAILS = 1;
    public static final int RESIDENCE_DETAILS = 2;
    public static final int COMPANY_DETAILS = 3;
    public static String LAST_VISITED_TAB = "last_visited_tab";
    public static int LAST_PENDING_TAB = 0;
    public static int c360TabId = 0;
    public static int GAMIFICATION_TEAM_LEAD_SPIN_WHEEL_ON= 0;
    public static final String GAMIFICATION_TEAM_LEAD_SPIN_WHEEL_DATA = "gamificatio_team_lead_spin_wheel_data";

    /**
     * Gamification stuff
      */

    public static String SPIN_ID_DSE_ONE = "1";
    public static String SPIN_ID_DSE_TWO = "2";
    public static String SPIN_ID_TEAM_LEAD = "3";
    public static String SPIN_AGAIN = "16";
    public static String SPIN_AGAIN_PLUS_POINT_ONE ="2";
    public static String SPIN_AGAIN_PLUS_POINT_TWO ="9";

    //Live
  //  public static String DUMMY_SERVER_BASIC_COMMON_URL = "https://salescmrapi.ninjacrm.com/common";
    public static final String PRODUCTION_SERVER_BASIC_URL ="https://salescmrapi.ninjacrm.com";

    //Staging
    //public static String SERVER_BASIC_COMMON_URL_STAGING = "http://salesstagingapi.ninjacrm.com/common";
    public static String STAGING_SERVER_BASIC_URL = "http://salesstagingapi.ninjacrm.com";

    public static String PINCODE_ADDRESS_MAP_API = "https://api.data.gov.in";

    public static String HERE_MAP_API_END_POINT = "https://reverse.geocoder.api.here.com/6.2";

    //public static String SERVER_BASIC_URL_TESTING_DEV = "http://172.17.0.1/salescrm_api/public";

    /*//dev
    public static String SERVER_BASIC_URL_TESTING_DEV = "http://130.211.241.85:9000/demo_kalyani/mobile";

    public static String SERVER_BASIC_URL_TESTING_DEV_LOCAL = "http://172.20.1.142/salescrm_api/public";*/


    public static String GCM_ID = "gcm_id";
    public static int MAX_OTP_LENGTH = 6;
    public static int LEAD_STAGE_MIN = 0;
    public static int LEAD_STAGE_MAX = 8;

    public static int CAR_STAGE_MIN = 0;
    public static int CAR_STAGE_MAX = 2;
    public static int API_MAX_CALL = 5;
    public static final String FORM_ACTION_VISIT_FEEDBACK_TRIGGER = "Visit_Feed_Back_Activity_Trigger";

    public static int LEAD_COUNTS = 0;
    public static boolean RECYCLER_STATUS = false;
    public static ArrayList<String> ENQUIRY_LIST_LOCATION_ID = new ArrayList<>();


    public static final SparseArray<Integer> NOTIFICATION_DRAWABLE = new SparseArray<Integer>(){
        {
            append(TaskActivityName.FOLLOWUP_CALL_ID, R.drawable.ic_pnt_call);
            append(TaskActivityName.HOME_VISIT_ID, R.drawable.ic_pnt_visit);
            append(TaskActivityName.SHOWROOM_VISIT_ID, R.drawable.ic_pnt_visit);
            append(TaskActivityName.EVALUATION_REQUEST_ID, R.drawable.ic_pnt_evaluation);
            append(TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID, R.drawable.ic_pnt_test_drive);
            append(TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID, R.drawable.ic_pnt_test_drive);
            append(TaskActivityName.RECEIVE_PAYMENT_ID, R.drawable.ic_pnt_book_car);
            append(TaskActivityName.DELIVERY_REQUEST_ID, R.drawable.ic_pnt_book_car);
            append(TaskActivityName.INVOICE_REQUEST_ID, R.drawable.ic_pnt_book_car);
            append(TaskActivityName.LOST_DROP_ID, R.drawable.ic_pnt_lost_drop);
            append(-1, R.drawable.ic_notifications_white_24dp);

        }
    };

    public class SRCMRealDbFields {
        public static final String LEAD_ID = "leadId";
        public static final String NAME = "name";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String MOBILE = "customerNumber";
        public static final String ENQUIRY_NUMBER="leadEnquiryNumber";
        public static final String CUSTOMER_NAME = "customerName";
        public static final String CUSTOMER_EMAIL = "customerEmail";
    }

    public class OfflineAPIRequest {
        public static final int INIT = 777;
        public static final int USER_DATA = 0;
        public static final int ACTION_PLAN = 1;
        public static final int FORM_OBJECT = 2;
        public static final int MANUAL_ACTIVITY = 3;
        public static final int C360 = 4;
        public static final int SCOREBOARD = 5;
        public static final int FETCH_LEAD_ELEMENTS = 6;
        public static final int FETCH_FILTER_DATA =7 ;
        public static final int TEAM_USERS = 8;
        public static final int ACTIVITY_USERS = 9;
        public static final int CAR_MODEL_VARIANTS = 10;
        public static final int LEAD_STAGES = 11;
        public static final int ENQUIRY_DATA = 12;
        public static final int APP_CONFIG = 13;
        public static final int LEAD_MOBILE = 14;
        public static final int GAME_CONFIG = 15;
        public static final int ETVBR_FILTER = 16;
        public static final int DEALERSHIPS = 17;
        public static final int BOOKING_ALLOCATION = 18;
        public static final int ACTIVE_EVENTS = 19;
        public static final int EVENT_CATEGORIES = 20;
        public static final int EXTERNAL_API = 21;
        public static final int EVALUATION_MANAGER = 22;
        public static final int AWS_CREDENTIALS = 23;

    }
    public class DSEPointsCategory {
        public static final int RETAIL = 1;
        public static final int TESTDRIVE =3 ;
        public static final int VISIT = 2;
        public static final int ZERO_PENDING = 4;
        public static final int  BONUS = 5;
        public static final int EXCHANGE = 6;
        public static final int FINANCE = 7;
    }

    public class CarBookingStages {
        public static final int BOOK_CAR = 0;
        public static final int CAR_BOOKED_HALF_PAYMENT = 1;
        public static final int CAR_CANCELLED = 2;
        public static final int INVOICE_PENDING = 3;
        public static final int INVOICED_DELIVERY_PENDING = 4;
        public static final int DELIVERED = 5;


        public static final int CAR_BOOKED_FULL_PAYMENT_NO = 6;
        public static final int CUSTOM_INVOICED_DELIVERY_PENDING_NO = 7;

        public static final int CUSTOM_CAR_BOOKED_EDIT = -11;



    }
    public class CarEvaluationStage {
        public static final int SCHEDULE_EVALUATION = 0;
        public static final int EVALUATION_CONDUCTED = 23;
        public static final int EVALUATION_SCHEDULED = 22;
    }
    public class ROLES {
        public static final String DSE = "4";
        public static final String TEAM_LEAD = "6";
        public static final String MANAGER= "8";
        public static final String BILLING_EXECUTIVE = "11";
        public static final String BRANCH_HEAD = "21";
        public static final String OPERATIONS = "30";


    }

    public static class BADGES {
        public static final HashMap<String, BadgesModel> badgesMap;

        static {
            badgesMap = new HashMap<String, BadgesModel>();
            badgesMap.put("1", new BadgesModel("1", R.drawable.badge_bulb, R.drawable.badge_bulb_mulitple));
            badgesMap.put("2", new BadgesModel("2", R.drawable.badge_car, R.drawable.badge_car_multiple));
            badgesMap.put("3", new BadgesModel("3", R.drawable.badge_target, R.drawable.badge_target_multiple));
            badgesMap.put("4", new BadgesModel("4", R.drawable.badge_fire, R.drawable.badge_fire_multiple));


            badgesMap.put("5", new BadgesModel("5", R.drawable.badge_explorer, R.drawable.explorers_multi_medal_badge));

            badgesMap.put("6", new BadgesModel("6", R.drawable.badge_achiever, R.drawable.achievers_multi_medal_badge));
            badgesMap.put("7", new BadgesModel("7", R.drawable.badge_dynamo, R.drawable.dynamos_multi_medal_badge));
            badgesMap.put("8", new BadgesModel("8", R.drawable.badge_guru, R.drawable.multi_guru_medal_badge));
            badgesMap.put("9", new BadgesModel("9", R.drawable.badge_trophy, R.drawable.badge_trophy_multiple));

        }


    }

    public class FormAction {
        public static final int ADD_ACTIVITY = 0;
        public static final int VIEW = 1;
        public static final int CREATE = 2;
        public static final int EDIT = 3;
        public static final int DELETE = 4;
        public static final int PRE_DONE = 5;
        public static final int POST_DONE = 55;
        public static final int RESCHEDULE = 6;
        public static final int FLAG = 7;
        public static final int AUTO = 8;
    }

    public class TaskActivityName {
        public static final String CONDUCT_TEST_DRIVE_SHOWROOM ="Conduct Test Drive Showroom";
        public static final String CONDUCT_TEST_DRIVE_HOME ="Conduct Test Drive Home";
        public static final String EVALUATION_REQUEST ="Evaluation Request";
        public static final String ALLOT_DSE = "Allot Dse";
        public static final String SCHEDULE_NEXT_ACTIVITY = "Schedule Next Activity";
        public static final int AUTO = 8;
        public static final int ALLOT_DSE_ID =22 ;
        public static final int SCHEDULE_NEXT_ACTIVITY_ID =23 ;
        public static final int FOLLOWUP_CALL_ID = 1;
        public static final int HOME_VISIT_ID = 2;
        public static final int SHOWROOM_VISIT_ID = 3;
        public static final int BOOK_TEST_DRIVE_CAR_ID = 4;
        public static final int CONDUCT_TEST_DRIVE_SHOWROOM_ID = 5;
        public static final int CONDUCT_TEST_DRIVE_HOME_ID = 6;
        public static final int PROPOSAL_REQUEST_ID = 8;
        public static final int EVALUATION_REQUEST_ID = 10;
        public static final int LOST_DROP_ID =24 ;
        public static final int NO_CAR_BOOKED_ID = 34;
        public static final int RECEIVE_PAYMENT_ID = 35;
        public static final int INVOICE_REQUEST_ID = 36;
        public static final int DELIVERY_REQUEST_ID = 37;
        public static final int FIRST_CALL_ID = 33;
        public static final int SUBMIT_FINAL_QUOTE = 11;

        public static final int ARRANGE_CALL_FROM_DSE = 60;

        public static final int PROSPECT_CALL = 6;

        public static final int BACK_TO_SALE = 60;
    }

    public class CreateLead {
        public static final String ALLOT_DSE = "Allot Dse";
        public static final String SCHEDULE_NEXT_ACTIVITY = "Schedule Next Activity";
        public static final int CREATE = 2;
    }

    public class SmsTypeName {
        public static final String TEST_DRIVE_SCHEDULED_ID = "3";
        public static final String VISIT_SCHEDULED_ID = "26";
        public static final String EVALUATION_SCHEDULED_ID = "27";
    }

    public class FormAnswerId {
        public static final int FOLLOW_UP_CALL = 1;
        public static final int DISABLED = -100;
        public static final int TEST_DRIVE_INIT = 65; //
        public static final int HOME_INIT = 23;

        public static final int LOST_DROP = 24;

        //For second view
        public static final int TEST_DRIVE_HOME = 6;
        public static final int TEST_DRIVE_SHOW_ROOM = 5;
        public static final int HOME_VISIT = 2;
        public static final int SHOWROOM_VISIT = 3;
        public static final int BOOK_CAR_ID = 10;



        //For RoyalEnfield
        public static final int FOLLOW_UP_CALL_RE = 1;
        public static final int TEST_RIDE_INIT_RE = -11;
        public static final int DROP_OUT_RE = 24;
        public static final int BOOK_RE = -10;

        //Two Wheeler
        public static final int FIRST_CALL = 1;
        public static final int ARRANGE_CALL = 60;
        public static final int BOOKED = -12;

        //Second View
        public static final int TEST_RIDE_HOME_RE = 6;
        public static final int TEST_RIDE_SHOWROOM_RE = 5;
        public static final int TEST_RIDE_EXP_RE = 57;
        public static final int TEST_RIDE_DEMO_RE = 58;
        //RoyalEnfield fields ends here

        public static final int RESCHEDULED_ID_1 = 139 ;
        public static final int RESCHEDULED_ID_2 = 149 ;
        public static final int RESCHEDULED_ID_3 = 188 ;
        public static final int RESCHEDULED_ID_4 = 161 ;

        //ILom Activities
        public static final int CALL_BACK = 1;
        public static final int PROSPECT = 6; //
        public static final int IL_RENEWED = -1;
        public static final int LOST_NOT_ELIGIBLE = 24;
        public static final int SALES_CONFIRMED = -2;
        public static final int VERIFICATION = -101;
        public static final int BACK_TO_SALE = 60;
    }

    public class FormQuestionId {
        public static final int SELECT_ACTIVITY =351;
        public static final int SELECT_ACTIVITY_POST_BOOKING =743;
        public static final int MANUFACTURING_YEAR =99;
        public static final int RESCHEDULED =71;
        public static final int LOST_DROP_NO =95;
        public static final int VISIT_YES =72;
        public static final int TESTDRIVE_YES =74;
        public static final int OTHER_ACTIVITY = 148;

        public static final int PNT_ACTIVITY = 2;
    }
    public class TYPE_DATE_PICKER {
        public static final int BOOKING_FOLLOW_UP_DATE =0;
        public static final int ADD_EXCHANGE_CAR =1;
        public static final int EVALUATION_EXCHANGE_CAR =2;
        public static final int DOB =3;
        public static final int EXCHANGE_PURCHASE_DATE =4;
        public static final int EXPECTED_DELIVERY_DATE =5;
        public static final int INVOICE_DATE =6;
        public static final int DELIVERY_RESCHEDULE_DATE =7;
        public static final int FILTER_DATE =8;
        public static final int BUYER_CLOSING_DATE =9;
    }

    public class AppConfig {

        public static final String UNIQUE_ACROSS_DEALERSHIP ="unique_across_dealer";
        public static final String SHOW_LEAD_SOURCE_CATEGORIES ="show_lead_source_categories";
        public static final String TARGET_RATIO_ETVBR ="etvbr";
        public static final String SHOW_LEAD_COMPETITOR ="showLeadCompetitor";
        public static final String BIKE_ACTIVITY_CONF = "bike_activity_conf";
        public static final String ADD_LEAD_ENABLED ="addLeadEnabled";
        public static final String TWO_WHEELER_CONF ="two_wheeler_conf";
        public static final String FIRST_CALL_ADVANCE_DAYS="first_call_advance_days";
        public static final String COLD_VISIT_ENABLED="cold_visits_enabled";
        public static final String I_LOM_FLOW="il_flow";
        public static final String I_LOM_BANK_FLOW="il_bank_flow";
        public static final String SCREESHOT_ENABLED="screenshot_enabled";
        public static final String HOTLINE_NUMBER="hotlineNo";
    }


    public static class FirebaseEvent {
        public static final String DEFAULT = "default";
        public static final String TASK_ASSIGNED = "task_assigned";
        public static final String UPCOMING_EVALUATION = "upcoming_evaluation";
        public static final String EVALUATION_COMPLETE = "evaluation_complete";
        public static final String NEW_ENQUIRY = "new_enquiry";
        public static final String PAYMENT_RECEIVED = "payment_received";
        public static final String INVOICE_DONE = "invoice_done";
        public static final String INVOICE_REJECTED = "invoice_rejected";
        public static final String LOST_DROPPED = "lost_dropped";
        public static final String ACTIVITY_DONE = "activity_done";
        public static final String EOD_SUMMARY = "eod_summary";
        public static final String PENDING_ALERT= "pending_alert";
        public static final String SHARE_PROFORMA_INVOICE= "share_proforma_invoice";
    }

    public static class InAppNotification{
        public static final String START_NINJA_NOTIFY_CUSTOM= "START_NINJA_NOTIFY_CUSTOM";
        public static final String TITLE= "title";
        public static final String CONTENT= "content";
        public static final String TYPE = "type";

    }
    public static class InAppNotificationType{
        public static final int LESS_TASK_COMPLETION = 0;
    }

    public class NavigationDrawerId {
        public static final int PROFILE = 0;
        public static final int LEADER_BOARD = 1;
        public static final int TEAM_DASHBOARD = 2;
        public static final int TARGETS = 3;
        public static final int MY_TASKS = 4;
        public static final int MY_ENQUIRIES = 5;
        public static final int MY_PERFORMANCE =6;
        public static final int TEAM_SHUFFLE = 7;
        public static final int LOGOUT = 8;
        public static final int TRANSFER_ENQUIRY = 9;
        public static final int SETTINGS = 10;
        public static final int COLD_VISITS = 11;



    }

    public class StandardPickerIds {
        public static final int YES = 1;
        public static final int NO = 2;
    }

    public class USER_ROLES {
        public static final int DSE = 4;
        public static final int TEAM_LEAD = 6;
        public static final int MANAGER= 8;
        public static final int BILLING_EXECUTIVE = 11;
        public static final int BRANCH_HEAD = 21;
        public static final int OPERATIONS = 30;


    }

    public class REACT_NATIVE_FUNCTIONS {
        public static final int OPEN_DRAWER = 1;
        public static final int OPEN_TASK_LIST = 2;

    }


    public static NewFilterRequest filterSelection = new NewFilterRequest();
    public static List<Filter> filters = new ArrayList<>();
    public static EtvbrSelectedFilters selectedFilters = new EtvbrSelectedFilters();
    public static boolean API_NOT_CALLED = true;
    public static boolean ETVBR_CALLED_ONCE = false;
    public static int ETVBR_TAB_OPENED  = 0;
}
