package com.salescrm.telephony.db.C360;

import com.salescrm.telephony.db.CustomerEmailId;
import com.salescrm.telephony.db.CustomerPhoneNumbers;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.response.AddEmailAddressResponse;
import com.salescrm.telephony.response.AddMobileNumberResponse;
import com.salescrm.telephony.response.EditEmailResponse;
import com.salescrm.telephony.response.EditMobileNumberResponse;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by bannhi on 27/12/16.
 */

public class DetailsPageDbHandler {

    public static final DetailsPageDbHandler INSTANCE = new DetailsPageDbHandler();
    SalesCRMRealmTable salesCRMRealmTable;
    C360RealmObject c360RealmObject;

    private DetailsPageDbHandler() {

    }

    public static DetailsPageDbHandler getInstance() {
        return DetailsPageDbHandler.INSTANCE;
    }

    public void addMobToRealmOnline(int leadId, Realm realm, AddMobileNumberResponse addmobresponse) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId)
                .findFirst();
        CustomerPhoneNumbers customerPhoneNumbers = new CustomerPhoneNumbers();
        if (addmobresponse.getResult() != null) {
            for (int i = 0; i < addmobresponse.getResult().getMobileData().length; i++) {
                AddMobileNumberResponse.MobileData mobileData = addmobresponse.getResult().getMobileData()[i];
                if (mobileData.getNumber() != null) {
                    customerPhoneNumbers.setLeadId(leadId);
                    customerPhoneNumbers.setPhoneNumberStatus("SECONDARY");
                    customerPhoneNumbers.setCustomerID(salesCRMRealmTable.getCustomerId());
                    if (!mobileData.getMobile_number_id().equalsIgnoreCase("")) {
                        customerPhoneNumbers.setPhoneNumberID(mobileData.getMobile_number_id());
                    }
                    if (!mobileData.getNumber().equalsIgnoreCase("")) {
                        customerPhoneNumbers.setPhoneNumber(Long.parseLong(mobileData.getNumber()));
                    }
                    if (!mobileData.getLead_phone_mapping_id().equalsIgnoreCase("")) {
                        customerPhoneNumbers.setLead_phone_mapping_id(mobileData.getLead_phone_mapping_id());
                    }
                }
            }
        }
        // realm.copyToRealmOrUpdate(customerPhoneNumbers);
        salesCRMRealmTable.customerPhoneNumbers.add(customerPhoneNumbers);
        if (addmobresponse.getResult().getLead_last_updated() != null && !addmobresponse.getResult().getLead_last_updated().equalsIgnoreCase("")) {
            salesCRMRealmTable.setLeadLastUpdated(addmobresponse.getResult().getLead_last_updated());

        }


    }

    /**
     * adding mobile details in offline, to be synced when internet comes
     * createdoffline true indicates, any EDIT to the number will actually add the number, but then a DELETE operation will have no effect overall
     * createdoffline false indicates, any EDIT to the number will actually edit the number, and DELETE operation will set the delete flag to be synced later on
     *
     * @param leadId
     * @param realm
     * @param mobileNumber
     */
    public void addMobToRealmOffline(int leadId, Realm realm, String mobileNumber) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId)
                .findFirst();
        int offlineCustomerId = salesCRMRealmTable.getCustomerId();
        String offlinePhoneNumberId = "0";
        String offlineLeadPhoneMappingId = "0";
        if (!mobileNumber.equalsIgnoreCase("") && leadId != 0) {
            offlinePhoneNumberId = "" + mobileNumber + leadId;
            offlineLeadPhoneMappingId = "" + mobileNumber + leadId;
            salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                    .equalTo("leadId", leadId)
                    .findFirst();
            CustomerPhoneNumbers customerPhoneNumbers = new CustomerPhoneNumbers();
            customerPhoneNumbers.setLeadId(leadId);
            customerPhoneNumbers.setPhoneNumberStatus("SECONDARY");
            customerPhoneNumbers.setCustomerID(salesCRMRealmTable.getCustomerId());
            customerPhoneNumbers.setPhoneNumberID(offlinePhoneNumberId);
            customerPhoneNumbers.setPhoneNumber(Long.parseLong(mobileNumber));
            customerPhoneNumbers.setLead_phone_mapping_id(offlineLeadPhoneMappingId);
            salesCRMRealmTable.customerPhoneNumbers.add(customerPhoneNumbers);

            c360RealmObject = realm.where(C360RealmObject.class)
                    .equalTo("leadId", leadId)
                    .findFirst();

            if (c360RealmObject == null) {
                c360RealmObject = realm.createObject(C360RealmObject.class, leadId);
                // c360RealmObject.setLead_id(leadId);
                c360RealmObject.setMobileSync(false);
            }
            MobileNumbers offlineMobileObject = realm.createObject(MobileNumbers.class);
            offlineMobileObject.setLeadId(leadId);
            offlineMobileObject.setPhoneNumberStatus("SECONDARY");
            offlineMobileObject.setCustomerID(offlineCustomerId);
            offlineMobileObject.setPhoneNumber(Long.parseLong(mobileNumber));
            offlineMobileObject.setPhoneNumberID(offlinePhoneNumberId);
            offlineMobileObject.setLead_phone_mapping_id(offlineLeadPhoneMappingId);
            offlineMobileObject.setCreatedOffline(true);
            offlineMobileObject.setEditedOffline(false);
            offlineMobileObject.setDeletedOffline(false);
            // offlineMobileObject.setSynced(false);
            c360RealmObject.getMobileNumbers().add(offlineMobileObject);
        }
    }

    /**
     * method to upate mobile number after editing it
     *
     * @param realm
     * @param editmobile
     */
    public void updateEdittedNumber(Realm realm, EditMobileNumberResponse.MobileData editmobile,
                                    long phoneMappingId, int position, int leadId, String leadLastUpdated) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId)
                .findFirst();
        if (editmobile != null) {
            if (editmobile.getNumber() != null) {
                CustomerPhoneNumbers customerPhoneNumbers = realm.where(CustomerPhoneNumbers.class)
                        .equalTo("lead_phone_mapping_id", ""+phoneMappingId)
                        .findFirst();
                if (customerPhoneNumbers != null && !editmobile.getNumber().equalsIgnoreCase("")) {
                    customerPhoneNumbers.setPhoneNumber(Long.parseLong(editmobile.getNumber()));
                    salesCRMRealmTable.customerPhoneNumbers.set(position, customerPhoneNumbers);
                }
            }
        }
        if (leadLastUpdated != null && !leadLastUpdated.equalsIgnoreCase("")) {
            salesCRMRealmTable.setLeadLastUpdated(leadLastUpdated);

        }

        System.out.println("Realupdated");

    }

    /**
     * editting a mobile number offline , to be synced when online
     * 1. createdoffline true indicates, any EDIT to the number will actually add the number,
     * but then a DELETE true flag will have no effect overall and will remove the entry from both the table
     * <p>
     * 2. createdoffline false indicates, any EDIT to the number will actually edit the number,
     * and DELETE true flag will set the delete flag to be synced later on
     * <p>
     * 3. sync flag should be set only for add and edit operation
     *
     * @param realm
     * @param phoneMappingId
     * @param mobileNumber
     * @param position
     * @param leadId
     */
    public void updateEdittedNumberOffline(Realm realm, String phoneMappingId, String mobileNumber, int position, long leadId, boolean deletedFlag, boolean edittedFlag) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId)
                .findFirst();
        CustomerPhoneNumbers customerPhoneNumbers = realm.where(CustomerPhoneNumbers.class)
                .equalTo("lead_phone_mapping_id", phoneMappingId)
                .findFirst();
        if (customerPhoneNumbers != null && !mobileNumber.equalsIgnoreCase("")) {
            customerPhoneNumbers.setPhoneNumber(Long.parseLong(mobileNumber));
            salesCRMRealmTable.customerPhoneNumbers.set(position, customerPhoneNumbers);
        }

        c360RealmObject = realm.where(C360RealmObject.class)
                .equalTo("leadId", leadId)
                .findFirst();

        if (c360RealmObject == null) {
            c360RealmObject = realm.createObject(C360RealmObject.class, leadId);
        }
        MobileNumbers offlineMobileEditObject = realm.where(MobileNumbers.class)
                .equalTo("lead_phone_mapping_id", phoneMappingId)
                .findFirst();
        /**
         * when the object is not null dat means it is already created(offline/online) need not be updated
         */
        if (offlineMobileEditObject != null && !mobileNumber.equalsIgnoreCase("'")) {
            offlineMobileEditObject.setPhoneNumber(Long.parseLong(mobileNumber));
            offlineMobileEditObject.setDeletedOffline(deletedFlag);
            offlineMobileEditObject.setEditedOffline(edittedFlag);
            // offlineMobileEditObject.setSynced(false);
        }/*
        when the object is null dat means it is definitly created online and getting its first entry in the C360 table hence set it to false
        */ else if (offlineMobileEditObject == null && customerPhoneNumbers != null) {
            offlineMobileEditObject = realm.createObject(MobileNumbers.class);
            offlineMobileEditObject.setPhoneNumberID(customerPhoneNumbers.getPhoneNumberID());
            offlineMobileEditObject.setLeadId(customerPhoneNumbers.getLeadId());
            offlineMobileEditObject.setPhoneNumberStatus(customerPhoneNumbers.getPhoneNumberStatus());
            offlineMobileEditObject.setPhoneNumber(customerPhoneNumbers.getPhoneNumber());
            offlineMobileEditObject.setLead_phone_mapping_id(customerPhoneNumbers.getLead_phone_mapping_id());
            offlineMobileEditObject.setCustomerID(customerPhoneNumbers.getCustomerID());
            offlineMobileEditObject.setEditedOffline(edittedFlag);
            offlineMobileEditObject.setDeletedOffline(deletedFlag);
            offlineMobileEditObject.setCreatedOffline(false);
            c360RealmObject.getMobileNumbers().add(offlineMobileEditObject);

        }

        c360RealmObject.setMobileSync(false);

    }

    /**
     * method to upate mobile number after making it primary
     *
     * @param realm
     * @param editmobile
     */
    public void updatePrimaryMobileNumber(Realm realm, String mob_id, int leadID, int position, String leadLastUpdated) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadID)
                .findFirst();
        int oldPos = 0;
        if (mob_id != null && !mob_id.equalsIgnoreCase("") && !mob_id.equalsIgnoreCase("0")) {
            CustomerPhoneNumbers customerPhoneNumbers = realm.where(CustomerPhoneNumbers.class)
                    .equalTo("phoneNumberStatus", "PRIMARY")
                    .equalTo("leadId", leadID)
                    .findFirst();
            System.out.println("MOBILE NUMBER LENGHTH: " + salesCRMRealmTable.customerPhoneNumbers.size());
            if (customerPhoneNumbers != null) {
                for (int i = 0; i < salesCRMRealmTable.customerPhoneNumbers.size(); i++) {
                    if (salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumber() == customerPhoneNumbers.getPhoneNumber()) {
                        oldPos = i;
                    }
                }
                customerPhoneNumbers.setPhoneNumberStatus("SECONDARY");
                salesCRMRealmTable.customerPhoneNumbers.set(oldPos, customerPhoneNumbers);
            }

            CustomerPhoneNumbers newCustomerPhone = realm.where(CustomerPhoneNumbers.class)
                    .equalTo("phoneNumberID", mob_id)
                    .equalTo("leadId", leadID)
                    .findFirst();

            if (newCustomerPhone != null) {
                newCustomerPhone.setPhoneNumberStatus("PRIMARY");
                salesCRMRealmTable.customerPhoneNumbers.set(position, newCustomerPhone);
                salesCRMRealmTable.setCustomerNumber(""+newCustomerPhone.getPhoneNumber());
            }


        }

        if (leadLastUpdated != null && !leadLastUpdated.equalsIgnoreCase("")) {
            salesCRMRealmTable.setLeadLastUpdated(leadLastUpdated);

        }
        System.out.println("Realupdated");

    }

    ////////////////////////////////////////////////////Email DB operations////////////////////////////////////////////////////////////////////////

    /**
     * adding a new email id
     *
     * @param realm
     * @param addemailresponse
     */
    public void addEmailToRealmOnline(Realm realm, AddEmailAddressResponse addemailresponse, int leadID) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadID)
                .findFirst();
        CustomerEmailId customerEmailId = new CustomerEmailId();
        if (addemailresponse.getResult() != null) {
            for (int i = 0; i < addemailresponse.getResult().getEmailData().length; i++) {
                AddEmailAddressResponse.EmailData emailData = addemailresponse.getResult().getEmailData()[i];
                if (emailData.getAddress() != null) {

                    if (!emailData.getEmail_address_id().equalsIgnoreCase("")) {
                        // customerEmailId.setEmailID(addemailresponse.getResult()[i].getEmail_address_id());
                        customerEmailId.setEmailID(emailData.getEmail_address_id());
                    }

                    customerEmailId.setEmail(emailData.getAddress());
                    customerEmailId.setLeadId(leadID);
                    customerEmailId.setEmailStatus("SECONDARY");
                    customerEmailId.setCustomerID(salesCRMRealmTable.getCustomerId());
                    if (!emailData.getLead_email_mapping_id().equalsIgnoreCase("")) {
                        //customerEmailId.setLead_email_mapping_id(addemailresponse.getResult()[i].getLead_email_mapping_id());
                        customerEmailId.setLead_email_mapping_id(emailData.getLead_email_mapping_id());
                    }

                }
            }
        }

        // realm.copyToRealmOrUpdate(customerEmailId);
        System.out.println("Realmemailaddded");
        salesCRMRealmTable.customerEmailId.add(customerEmailId);
        if (addemailresponse.getResult().getLead_last_updated() != null && !addemailresponse.getResult().getLead_last_updated().equalsIgnoreCase("")) {
            salesCRMRealmTable.setLeadLastUpdated(addemailresponse.getResult().getLead_last_updated());

        }

    }

    /**
     * adding email id offline to be synced later
     *
     * @param realm
     * @param email
     * @param leadID
     */
    public void addEmailToRealmOffline(Realm realm, String email, int leadID) {
        String offlineEmailAddressId = "" + leadID + System.currentTimeMillis();
        String offlineLeadEmailMappingId = "" + leadID + System.currentTimeMillis();
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadID)
                .findFirst();
        CustomerEmailId customerEmailId = new CustomerEmailId();
        if (!email.equalsIgnoreCase("")) {
            customerEmailId.setEmailID(offlineEmailAddressId);
            customerEmailId.setEmail(email);
            customerEmailId.setLeadId(leadID);
            customerEmailId.setEmailStatus("SECONDARY");
            customerEmailId.setCustomerID(salesCRMRealmTable.getCustomerId());
            customerEmailId.setLead_email_mapping_id(offlineLeadEmailMappingId);
        }
        salesCRMRealmTable.customerEmailId.add(customerEmailId);

        c360RealmObject = realm.where(C360RealmObject.class)
                .equalTo("leadId", leadID)
                .findFirst();

        if (c360RealmObject == null) {
            c360RealmObject = realm.createObject(C360RealmObject.class, leadID);
            c360RealmObject.setEmailSync(false);
        }
        EmailIds offlineEmailIdObject = realm.createObject(EmailIds.class);
        offlineEmailIdObject.setLeadId(leadID);
        offlineEmailIdObject.setEmailIdStatus("SECONDARY");
        offlineEmailIdObject.setEmailAddressId(offlineEmailAddressId);
        offlineEmailIdObject.setEmail(email);
        offlineEmailIdObject.setCustomerId(salesCRMRealmTable.getCustomerId());
        offlineEmailIdObject.setEmailLeadMappingId(offlineLeadEmailMappingId);
        offlineEmailIdObject.setCreatedOffline(true);
        offlineEmailIdObject.setEditedOffline(false);
        offlineEmailIdObject.setDeletedOffline(false);
        c360RealmObject.getEmailIds().add(offlineEmailIdObject);

    }

    /**
     * updating realm after editing the email id
     *
     * @param realm
     * @param editemail
     */
    public void updateEmailInRealm(Realm realm, EditEmailResponse.EmailData editemail, long emailMappingId, int position, int leadId, String leadLastUpdated) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId)
                .findFirst();
        if (editemail != null) {
            if (editemail.getAddress() != null) {
                CustomerEmailId customerEmailId = realm.where(CustomerEmailId.class)
                        .equalTo("lead_email_mapping_id", "" + emailMappingId)
                        .findFirst();
                if (customerEmailId != null) {
                    customerEmailId.setEmail(editemail.getAddress());
                    salesCRMRealmTable.customerEmailId.set(position, customerEmailId);
                }
            }
        }
        if (leadLastUpdated != null && !leadLastUpdated.equalsIgnoreCase("")) {
            salesCRMRealmTable.setLeadLastUpdated(leadLastUpdated);

        }
        System.out.println("Realmemailaddded");


    }

    public void updateEmailInRealmOffline(Realm realm, long emailMappingId, String email, int position, int leadId, boolean deletedFlag, boolean editedFlag) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId)
                .findFirst();
        CustomerEmailId customerEmailId = realm.where(CustomerEmailId.class)
                .equalTo("lead_email_mapping_id", "" + emailMappingId)
                .findFirst();
        if (customerEmailId != null) {
            customerEmailId.setEmail(email);
            salesCRMRealmTable.customerEmailId.set(position, customerEmailId);
        }
        c360RealmObject = realm.where(C360RealmObject.class)
                .equalTo("leadId", leadId)
                .findFirst();

        if (c360RealmObject == null) {
            c360RealmObject = realm.createObject(C360RealmObject.class, leadId);
        }
        EmailIds offlineEmailEditObject = realm.where(EmailIds.class)
                .equalTo("emailLeadMappingId", "" + emailMappingId)
                .findFirst();
        if (offlineEmailEditObject != null && !email.equalsIgnoreCase("'")) {
            offlineEmailEditObject.setEmail(email);
            offlineEmailEditObject.setEditedOffline(editedFlag);
            offlineEmailEditObject.setDeletedOffline(deletedFlag);


        } else if (offlineEmailEditObject == null && customerEmailId != null) {
            offlineEmailEditObject = realm.createObject(EmailIds.class);
            offlineEmailEditObject.setEmailAddressId(customerEmailId.getEmailID());
            offlineEmailEditObject.setLeadId(customerEmailId.getLeadId());
            offlineEmailEditObject.setEmailIdStatus(customerEmailId.getEmailStatus());
            offlineEmailEditObject.setEmail(customerEmailId.getEmail());
            offlineEmailEditObject.setEmailLeadMappingId(customerEmailId.getLead_email_mapping_id());
            offlineEmailEditObject.setCustomerId(customerEmailId.getCustomerID());
            offlineEmailEditObject.setDeletedOffline(deletedFlag);
            offlineEmailEditObject.setCreatedOffline(false);
            offlineEmailEditObject.setDeletedOffline(deletedFlag);
            offlineEmailEditObject.setEditedOffline(editedFlag);
            c360RealmObject.getEmailIds().add(offlineEmailEditObject);

        }
        c360RealmObject.setEmailSync(false);
        System.out.println("Realmemailaddded");


    }


    /**
     * method to upate email id after making it primary
     *
     * @param realm
     * @param editmobile
     */
    public void updatePrimaryEmailId(Realm realm, String mailId, int leadID, int position, String leadLastUpdated) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadID)
                .findFirst();
        int oldPos = 0;
        if (!mailId.equalsIgnoreCase("0")) {
            CustomerEmailId customerEmailId = realm.where(CustomerEmailId.class)
                    .equalTo("emailStatus", "PRIMARY")
                    .equalTo("leadId", leadID)
                    .findFirst();
            if (customerEmailId != null) {
                for (int i = 0; i < salesCRMRealmTable.customerEmailId.size(); i++) {
                    if (salesCRMRealmTable.customerEmailId.get(i).getEmailID().equalsIgnoreCase(mailId)) {
                        oldPos = i;
                    }
                }
            }

            if (customerEmailId != null) {
                customerEmailId.setEmailStatus("SECONDARY");
                salesCRMRealmTable.customerEmailId.set(oldPos, customerEmailId);
            }
            CustomerEmailId newCustomerEmail = realm.where(CustomerEmailId.class)
                    .equalTo("emailID", mailId)
                    .equalTo("leadId", leadID)
                    .findFirst();

            if (newCustomerEmail != null) {
                newCustomerEmail.setEmailStatus("PRIMARY");
                salesCRMRealmTable.customerEmailId.set(position, newCustomerEmail);
            }

        }
        if (leadLastUpdated != null && !leadLastUpdated.equalsIgnoreCase("")) {
            salesCRMRealmTable.setLeadLastUpdated(leadLastUpdated);
        }
    }

    /**
     * updating location data
     *
     * @param realm
     * @param leadId
     * @param location
     * @param locationId
     * @param dse
     * @param dseId
     * @param crmSrc
     * @param crmSrcId
     */
/*
<<<<<<< HEAD
    public void updateLocationData(Realm realm, int leadId, String location, String locationId,
                                   String dse, String dseId, String crmSrc, String crmSrcId, boolean createdOffline, boolean synced) {
=======
*/
    public void updateLocationData(Realm realm, int leadId, String location, String locationId,
                                   String dse, String dseId, String crmSrc, String crmSrcId, boolean createdOffline, boolean synced, String leadLastUpdated) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId)
                .findFirst();

        if (salesCRMRealmTable != null) {

            salesCRMRealmTable.setLeadSourceName(crmSrc);
            salesCRMRealmTable.setLeadSourceId(crmSrcId);
            if (leadLastUpdated != null && !leadLastUpdated.equalsIgnoreCase("")) {
                salesCRMRealmTable.setLeadLastUpdated(leadLastUpdated);
            }
        }
        if (createdOffline) {
            c360RealmObject = realm.where(C360RealmObject.class)
                    .equalTo("leadId", leadId)
                    .findFirst();

            if (c360RealmObject == null) {
                c360RealmObject = realm.createObject(C360RealmObject.class, leadId);
                //c360RealmObject.setLead_id(leadId);
            }
            c360RealmObject.setLocationId(locationId);
            c360RealmObject.setLocationName(location);
            c360RealmObject.setCrmSource(crmSrc);
            c360RealmObject.setCrmId(crmSrcId);
            c360RealmObject.setDseId(dseId);
            c360RealmObject.setDseName(dse);
            c360RealmObject.setCrmSourceSynced(synced);
            c360RealmObject.setLeadLastUpdated(salesCRMRealmTable.getLeadLastUpdated());
        }
    }

    /**
     * updating buyer type after updating
     *
     * @param realm
     * @param leadId
     * @param typeOfPayment
     * @param typeOfBuyer
     * @param buyerTypeId
     * @param closingDate
     * @param typeOfCustomerId
     * @param enqSrcId
     * @param dseId
     * @param dseName
     * @param locId
     * @param locationName
     * @param createdOffline
     * @param setSync
     * @param enqSrcId
     * @param buyerTypeId
     */
    public void updateBuyerType(Realm realm, int leadId, String typeOfPayment, String typeOfBuyer, String buyerTypeId,
                                String closingDate, String typeOfCustomerId, String enqSrcId, String dseId,
                                String dseName, String locId, String locationName, boolean createdOffline,
                                boolean setSync, String leadLastUpdated) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId)
                .findFirst();
        if (salesCRMRealmTable != null) {
            salesCRMRealmTable.setBuyerType(typeOfBuyer);
            salesCRMRealmTable.setBuyerTypeId(buyerTypeId);
            salesCRMRealmTable.setModeOfPayment(typeOfPayment);
            salesCRMRealmTable.setCloseDate(closingDate);
            if (!typeOfCustomerId.equalsIgnoreCase("")) {
                salesCRMRealmTable.setTypeOfCustomer(WSConstants.customerType.get(Integer.parseInt(typeOfCustomerId)));
            }
            if (leadLastUpdated != null && !leadLastUpdated.equalsIgnoreCase("")) {
                salesCRMRealmTable.setLeadLastUpdated(leadLastUpdated);
            }
        }
        if (createdOffline) {
            c360RealmObject = realm.where(C360RealmObject.class)
                    .equalTo("leadId", leadId)
                    .findFirst();
            if (c360RealmObject == null) {
                c360RealmObject = realm.createObject(C360RealmObject.class, leadId);
                c360RealmObject.setLocationId(locId);
                c360RealmObject.setLocationName(locationName);
                c360RealmObject.setDseId(dseId);
                c360RealmObject.setDseName(dseName);
                c360RealmObject.setEnqSrcId(enqSrcId);
            }
            c360RealmObject.setBuyerType(typeOfBuyer);
            c360RealmObject.setBuyerTypeId(buyerTypeId);
            c360RealmObject.setModeOfPayment(typeOfPayment);
            c360RealmObject.setExpectedClosingDate(closingDate);
            c360RealmObject.setLeadLastUpdated(salesCRMRealmTable.getLeadLastUpdated());
            c360RealmObject.setBuyerTypeSynced(setSync);
            c360RealmObject.setTypeOfCustomerId(typeOfCustomerId);
        }

    }

    /**
     * updating customer details
     *
     * @param realm
     * @param leadId
     * @param gender
     * @param firstName
     * @param age
     */
    public void updateCustomerdetails(Realm realm, int leadId, String gender, String title, String firstName,
                                      String lastName, String age, boolean createdOffline, boolean sync, String leadLastUpdated) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId)
                .findFirst();
        if (salesCRMRealmTable != null) {
            salesCRMRealmTable.setGender(gender);
            salesCRMRealmTable.setTitle(title);
            salesCRMRealmTable.setFirstName(firstName);
            salesCRMRealmTable.setCustomerAge(age);
            salesCRMRealmTable.setLastName(lastName);
            if (leadLastUpdated != null && !leadLastUpdated.equalsIgnoreCase("")) {
                salesCRMRealmTable.setLeadLastUpdated(leadLastUpdated);
            }
        }
        if (createdOffline) {
            updateC360Object(leadId, salesCRMRealmTable.getCustomerOccupation(), salesCRMRealmTable.getOfficeAddress(),
                    salesCRMRealmTable.getCompanyName(), salesCRMRealmTable.getOfficePinCode(), sync,
                    salesCRMRealmTable.getLeadLastUpdated(), salesCRMRealmTable.getResidenceAddress(),
                    salesCRMRealmTable.getResidencePinCode(), gender, title, firstName, age, lastName, realm);

        }


    }

    /**
     * update residency address
     *
     * @param realm
     * @param leadId
     * @param residence
     * @param pincode
     */
    public void updateResidenceType(Realm realm, int leadId, String residence, String pincode,
                                    String locality, String city, String state,
                                    boolean createdOffline, boolean sync, String leadLastUpdated) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId)
                .findFirst();
        if (salesCRMRealmTable != null) {
            salesCRMRealmTable.setResidenceAddress(residence);
            salesCRMRealmTable.setResidencePinCode(pincode);
            salesCRMRealmTable.setResidenceLocality(locality);
            salesCRMRealmTable.setResidenceCity(city);
            salesCRMRealmTable.setResidenceState(state);
            if (leadLastUpdated != null && !leadLastUpdated.equalsIgnoreCase("")) {
                salesCRMRealmTable.setLeadLastUpdated(leadLastUpdated);
            }
        }
        if (createdOffline) {
            updateC360Object(leadId, salesCRMRealmTable.getCustomerOccupation(), salesCRMRealmTable.getOfficeAddress(),
                    salesCRMRealmTable.getCompanyName(), salesCRMRealmTable.getOfficePinCode(), sync,
                    salesCRMRealmTable.getLeadLastUpdated(), residence,
                    pincode, salesCRMRealmTable.getGender(), salesCRMRealmTable.getTitle(), salesCRMRealmTable.getFirstName(),
                    salesCRMRealmTable.getCustomerAge(), salesCRMRealmTable.getLastName(), realm);
        }


    }

    /**
     * update office details
     *
     * @param realm
     * @param leadId
     * @param residence
     * @param pincode
     */
    public void updateOfficeDetails(Realm realm, int leadId, String occupation, String officeAddress,
                                    String pinCode,
                                    String officeLocality,
                                    String officeCity,
                                    String officeState,
                                    String companyName, boolean createdOffline, boolean sync, String leadLastUpdated) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", leadId)
                .findFirst();

        if (salesCRMRealmTable != null) {
            salesCRMRealmTable.setCustomerOccupation(occupation);
            salesCRMRealmTable.setOfficeAddress(officeAddress);
            salesCRMRealmTable.setCustomerDesignation(occupation);
            salesCRMRealmTable.setOfficePinCode(pinCode);
            salesCRMRealmTable.setOfficeLocality(officeLocality);
            salesCRMRealmTable.setOfficeCity(officeCity);
            salesCRMRealmTable.setOfficeState(officeState);
            System.out.println("updateOfficeDetails Company Before" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
            salesCRMRealmTable.setCompanyName(companyName);
            System.out.println("updateOfficeDetails Company After" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
            if (leadLastUpdated != null && !leadLastUpdated.equalsIgnoreCase("")) {
                salesCRMRealmTable.setLeadLastUpdated(leadLastUpdated);
            }
        }
        if (createdOffline) {
            updateC360Object(leadId, occupation, officeAddress, companyName, pinCode, sync,
                    salesCRMRealmTable.getLeadLastUpdated(), salesCRMRealmTable.getResidenceAddress(),
                    salesCRMRealmTable.getResidencePinCode(), salesCRMRealmTable.getGender(), salesCRMRealmTable.getTitle(),
                    salesCRMRealmTable.getFirstName(), salesCRMRealmTable.getCustomerAge(), salesCRMRealmTable.getLastName(), realm);
        }


    }

    /**
     * unsynced customer details(residence, office, personal details)
     *
     * @param realm
     * @return
     */
    public RealmResults<C360RealmObject> getUnsyncedCustomerDetails(Realm realm) {
        return realm.where(C360RealmObject.class)
                .equalTo("customerDetailsSynced", false)
                .findAll();

    }

    /**
     * unsynced buyer type details
     *
     * @param realm
     * @return
     */
    public RealmResults<C360RealmObject> getUnsyncedBuyerTypeDetails(Realm realm) {
        return realm.where(C360RealmObject.class)
                .equalTo("buyerTypeSynced", false)
                .findAll();


    }

    /**
     * unsynced crm source details
     *
     * @param realm
     * @return
     */
    public RealmResults<C360RealmObject> getUnsyncedCrmSrcDetails(Realm realm) {
        return realm.where(C360RealmObject.class)
                .equalTo("crmSourceSynced", false)
                .findAll();


    }

    /**
     * get all the leads with unsynced mobile numbers
     *
     * @param realm
     * @return
     */
    public RealmResults<C360RealmObject> getAllLeadsWithUnsyncedMobileNumbers(Realm realm) {
        return realm.where(C360RealmObject.class)
                .equalTo("mobileSync", false)
                .findAll();


    }

    /**
     * to check if there exists any lead with unsynced email details
     *
     * @param realm
     * @return
     */
    public RealmResults<C360RealmObject> getAllLeadsWithUnsyncedEmailIds(Realm realm) {
        return realm.where(C360RealmObject.class)
                .equalTo("emailSync", false)
                .findAll();


    }

    /**
     * to check if the lead has any unsynced mobile numbers if any of the flags is true,
     * it indicates all are not yet synced hence return true and
     * iff all are false it means all are synced hence return false;
     *
     * @param realm
     * @return
     */
    public boolean isAnyUnsyncedMobileNumber(Realm realm, long leadId) {
        RealmResults<MobileNumbers> unsyncedMobileNumbers = realm.where(MobileNumbers.class)
                .equalTo("leadId", leadId)
                .beginGroup()
                .equalTo("deletedOffline", true)
                .or()
                .equalTo("createdOffline", true)
                .or()
                .equalTo("editedOffline", true)
                .endGroup()
                .findAll();
        if (unsyncedMobileNumbers != null && unsyncedMobileNumbers.size() > 0) {
            //something is not yet synced hence overall not synced hence return false
            return false;
        } else {
            //else if its null or size = 0; all are synced hence overall synced hence return true;
            return true;
        }


    }


    /**
     * update the C360 object for its customer details baed on the values changed from ui
     *
     * @param leadId
     * @param occupation
     * @param officeAddress
     * @param companyName
     * @param officePin
     * @param sync
     * @param leadLastUpdated
     * @param residence
     * @param residencePin
     * @param gender
     * @param firstName
     * @param age
     * @param lastName
     * @param realm
     */
    private void updateC360Object(int leadId, String occupation, String officeAddress, String companyName,
                                  String officePin, boolean sync, String leadLastUpdated, String residence,
                                  String residencePin, String gender, String title, String firstName, String age, String lastName, Realm realm) {

        c360RealmObject = realm.where(C360RealmObject.class)
                .equalTo("leadId", leadId)
                .findFirst();

        if (c360RealmObject == null) {
            c360RealmObject = realm.createObject(C360RealmObject.class, leadId);
        }
        c360RealmObject.setOccupation(occupation);
        c360RealmObject.setOfficeAddress(officeAddress);
        c360RealmObject.setDesignation(occupation + ", " + companyName);
        c360RealmObject.setOfficePin(officePin);
        c360RealmObject.setCompanyName(companyName);
        c360RealmObject.setCustomerDetailsSynced(sync);
        c360RealmObject.setLeadLastUpdated(salesCRMRealmTable.getLeadLastUpdated());

        c360RealmObject.setResidenceAddress(residence);
        c360RealmObject.setResidencePin(residencePin);
        c360RealmObject.setCustomerDetailsSynced(sync);
        c360RealmObject.setLeadLastUpdated(leadLastUpdated);


        c360RealmObject.setGender(gender);
        c360RealmObject.setTitle(title);
        c360RealmObject.setFirstName(firstName);
        c360RealmObject.setAge(age);
        c360RealmObject.setLastName(lastName);
        c360RealmObject.setCustomerDetailsSynced(sync);
        c360RealmObject.setLeadLastUpdated(salesCRMRealmTable.getLeadLastUpdated());
    }

    /**
     * delete mobile number from customer 360 DB if it synced and deleted on server
     *
     * @param realm
     * @param mobileNumber
     */
    public void deleteMobileNumberFrom360(Realm realm, final MobileNumbers mobileNumber) {
        MobileNumbers result = realm.where(MobileNumbers.class).
                equalTo("lead_phone_mapping_id", mobileNumber.getLead_phone_mapping_id()).findFirst();
        if (result.isValid()) {
            result.deleteFromRealm();
        }

    }

    /**
     * delete email id from customer 360 DB if it synced and deleted on server
     *
     * @param realm
     * @param mobileNumber
     */
    public void deleteEmailIdFrom360(Realm realm, final EmailIds emailIds) {
        EmailIds result = realm.where(EmailIds.class).
                equalTo("emailLeadMappingId", emailIds.getEmailLeadMappingId()).findFirst();
        if (result != null && result.isValid()) {
            result.deleteFromRealm();
        }
    }


    /**
     * to check if the lead has any unsynced email ids if any of the flags is true,
     * it indicates all are not yet synced hence return true and
     * iff all are false it means all are synced hence return false;
     *
     * @param realm
     * @return
     */
    public boolean isAnyUnsyncedEmailIds(Realm realm, long leadId) {
        RealmResults<EmailIds> unsyncedEmailIds = realm.where(EmailIds.class)
                .equalTo("leadId", leadId)
                .beginGroup()
                .equalTo("deletedOffline", true)
                .or()
                .equalTo("createdOffline", true)
                .or()
                .equalTo("editedOffline", true)
                .endGroup()
                .findAll();
        if (unsyncedEmailIds != null && unsyncedEmailIds.size() > 0) {
            //something is not yet synced hence overall not synced hence return false
            return false;
        } else {
            //else if its null or size = 0; all are synced hence overall synced hence return true;
            return true;
        }

    }

}
