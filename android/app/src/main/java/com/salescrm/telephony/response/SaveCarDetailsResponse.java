package com.salescrm.telephony.response;

/**
 * Created by akshata on 15/9/16.
 */
public class SaveCarDetailsResponse {

        private String statusCode;

        private String message;

        private String result;

        private Error error;

        public String getStatusCode ()
        {
            return statusCode;
        }

        public void setStatusCode (String statusCode)
        {
            this.statusCode = statusCode;
        }

        public String getMessage ()
        {
            return message;
        }

        public void setMessage (String message)
        {
            this.message = message;
        }

        public String getResult ()
        {
            return result;
        }

        public void setResult (String result)
        {
            this.result = result;
        }

        public Error getError ()
        {
            return error;
        }

        public void setError (Error error)
        {
            this.error = error;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
        }


    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }
}
