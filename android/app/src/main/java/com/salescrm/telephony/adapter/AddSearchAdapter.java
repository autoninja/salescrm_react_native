package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.dataitem.SectionModel;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.interfaces.AdapterCommunication;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.SearchResponse;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by bharath on 9/8/16.
 */
public class AddSearchAdapter extends RecyclerView.Adapter<AddSearchAdapter.ViewHolder> {

    private static AdapterCommunication mlistener;
    private String userId;
    Activity mActivity;
    // private TodayActionPlanFragment mFragment;
    boolean menuOpen = false, expand = false;
    private Context mContext;
    private Preferences pref = null;
    // int size;
    private ViewGroup classParent;
    private List<SearchResponse.Result> searchResponse;
    private List<SectionModel> sections;

    public AddSearchAdapter(Context context, List<SearchResponse.Result> searchResponse, String user_id, AdapterCommunication listener) {
        this.searchResponse = searchResponse;
        this.sections = sections;
        mlistener = listener;
        this.mContext = context;
        this.userId = user_id;
    }


    @Override
    public AddSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = null;
        classParent = parent;
        pref = Preferences.getInstance();
        pref.load(parent.getContext());
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_card_layout, parent, false);
        return new ViewHolder(itemLayoutView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(AddSearchAdapter.ViewHolder viewHolder, int position) {
        final SearchResponse.Result result = searchResponse.get(position);
        if (TextUtils.isEmpty(result.getFirstName()) || TextUtils.isEmpty(result.getFirstName())) {
            viewHolder.tvSearchName.setText("Customer Name");
        } else {
            viewHolder.tvSearchName.setText(String.format("%s %s", result.getFirstName(), result.getLastName()));

        }
        if (result.getDsename() == null) {
            viewHolder.tvSearchAssigned.setText("Not Assigned");
        } else {
            viewHolder.tvSearchAssigned.setText(String.format("Assigned: %s", result.getDsename()));
        }

        viewHolder.tvSearchAssigned.append(" \nLead Status : "+result.getStatus()+"");
        if(result.getActive().equalsIgnoreCase("0")){
            viewHolder.tvSearchAssigned.append(" (closed)");
        }
        else {
            viewHolder.tvSearchAssigned.append(" (open)");

        }
        viewHolder.tvSearchAssigned.append(" \nLead Stage : "+result.getStage_name()+"");

        viewHolder.tvSearchAssigned.append(" \nLocation : "+result.getLocation_name());

        setTagColor(viewHolder, result.getTag_name());
        SimpleDateFormat dateFormatter = new SimpleDateFormat("E dd/MM", Locale.getDefault());
        if (result.getActivities_data() != null && result.getActivities_data().size() > 0) {
            viewHolder.tvSearchFollowDate.setVisibility(View.VISIBLE);
            viewHolder.viewStatusOval.setVisibility(View.VISIBLE);
            String string = result.getActivities_data().get(0).getActivity_scheduled_date();
            DateFormat format = new SimpleDateFormat("y-M-d H:m:s", Locale.ENGLISH);
            Date date = null;
            try {
                date = format.parse(string);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            viewHolder.tvSearchFollowDate.setText(String.format("%s  %s", result.getActivities_data().get(0).getScheduled_activity_name(), dateFormatter.format(date)));
        } else {
            viewHolder.tvSearchFollowDate.setVisibility(View.GONE);
            viewHolder.viewStatusOval.setVisibility(View.GONE);
        }

        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(result.is_view_allowed()){
                    pref.setLeadID("" + result.getLeadId());
                    Intent intent = new Intent(mContext, C360Activity.class);
                    intent.putExtra("online_search_data", result);
                    mContext.startActivity(intent);
                }
                else {
                    Toast.makeText(mContext,"Access Denied",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void setTagColor(ViewHolder viewHolder, String tagName) {
        if (tagName == null) {
            return;
        }
        if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_HOT)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.hot));
        } else if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_WARM)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.warm));
        } else if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_COLD)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.cold));
        } else if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_OVERDUE)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.overdue));

        }
    }

    @Override
    public int getItemCount() {
        return searchResponse == null ? 0 : searchResponse.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder {
        public View view;
        TextView tvSearchName;
        ImageView viewStatusOval;
        TextView tvSearchFollowDate;
        TextView tvSearchAssigned;
        SalesCRMRealmTable searchResponse;
        CardView cardTag;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            view = itemLayoutView;
            tvSearchName = (TextView) view.findViewById(R.id.tv_search_name);
            viewStatusOval = (ImageView) view.findViewById(R.id.iV_search);
            tvSearchFollowDate = (TextView) view.findViewById(R.id.tv_search_follow_date);
            tvSearchAssigned = (TextView) view.findViewById(R.id.tv_search_assigned);
            cardTag = (CardView) view.findViewById(R.id.cardview_search);


        }


    }
}
