package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by bharath on 22/9/17.
 */

public class AppUserResponse {
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private Brands brands;

        private List<Roles_user> roles_user;

        private List<Locations> locations;

        private Data data;

        private String team_id;

        public Brands getBrands() {
            return brands;
        }

        public void setBrands(Brands brands) {
            this.brands = brands;
        }

        public List<Roles_user> getRoles_user() {
            return roles_user;
        }

        public void setRoles_user(List<Roles_user> roles_user) {
            this.roles_user = roles_user;
        }

        public List<Locations> getLocations() {
            return locations;
        }

        public void setLocations(List<Locations> locations) {
            this.locations = locations;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public String getTeam_id() {
            return team_id;
        }

        public void setTeam_id(String team_id) {
            this.team_id = team_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [brands = " + brands + ", roles_user = " + roles_user + ", locations = " + locations + ", data = " + data + ", team_id = " + team_id + "]";
        }

        public class Data {
            private String dealer;

            private String id;

            private String user_name;

            private String email;

            private String dp_filename;

            private String name;

            private String dp_url;

            private String mobile;

            public String getDealer() {
                return dealer;
            }

            public void setDealer(String dealer) {
                this.dealer = dealer;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getDp_filename() {
                return dp_filename;
            }

            public void setDp_filename(String dp_filename) {
                this.dp_filename = dp_filename;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDp_url() {
                return dp_url;
            }

            public void setDp_url(String dp_url) {
                this.dp_url = dp_url;
            }

            public String getMobile() {
                return mobile;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            @Override
            public String toString() {
                return "ClassPojo [dealer = " + dealer + ", id = " + id + ", user_name = " + user_name + ", email = " + email + ", dp_filename = " + dp_filename + ", name = " + name + ", dp_url = " + dp_url + ", mobile = " + mobile + "]";
            }
        }

        public class Locations {
            private String name;

            private String location_id;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            @Override
            public String toString() {
                return "ClassPojo [name = " + name + ", location_id = " + location_id + "]";
            }
        }

        public class Roles_user {
            private String id;

            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", name = " + name + "]";
            }
        }


        public class Brands {
            private String brand_id;

            private String brand_name;

            public String getBrand_id() {
                return brand_id;
            }

            public void setBrand_id(String brand_id) {
                this.brand_id = brand_id;
            }

            public String getBrand_name() {
                return brand_name;
            }

            public void setBrand_name(String brand_name) {
                this.brand_name = brand_name;
            }

            @Override
            public String toString() {
                return "ClassPojo [brand_id = " + brand_id + ", brand_name = " + brand_name + "]";
            }
        }


    }


}
