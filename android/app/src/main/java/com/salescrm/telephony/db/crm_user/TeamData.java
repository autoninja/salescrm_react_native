package com.salescrm.telephony.db.crm_user;

import io.realm.RealmObject;

/**
 * Created by bharath on 9/12/16.
 */

public class TeamData extends RealmObject {
    private String manager_id;
    private String team_leader_id;
    private String user_id;

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getTeam_leader_id() {
        return team_leader_id;
    }

    public void setTeam_leader_id(String team_leader_id) {
        this.team_leader_id = team_leader_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
