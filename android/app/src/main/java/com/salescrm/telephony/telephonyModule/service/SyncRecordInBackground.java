package com.salescrm.telephony.telephonyModule.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.offline.FetchAwsCredentials;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AwsCredentialsResponse;
import com.salescrm.telephony.telephonyModule.SyncDbResponse;
import com.salescrm.telephony.telephonyModule.db.TelephonyDB;
import com.salescrm.telephony.telephonyModule.db.TelephonyDbHandler;
import com.salescrm.telephony.telephonyModule.interfaces.DbRecordSyncCountListener;
import com.salescrm.telephony.telephonyModule.model.RecordingsModel;
import com.salescrm.telephony.telephonyModule.model.TelephonyModel;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.NinjaEncryption;
import com.salescrm.telephony.utils.NotificationsUI;
import com.salescrm.telephony.utils.WSConstants;

import java.io.File;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

public class SyncRecordInBackground extends IntentService {

    private static boolean dbfetch = false;
    ArrayList<RecordingsModel> wordList = null;
    private Preferences pref = null;
    private String imeiNumber;
    private ArrayList<RecordingsModel> details;
    private RealmResults<TelephonyDB> telephonyDbList;
    private Realm realm;
    // The TransferUtility is the primary class for managing transfer to S3
    private TransferUtility transferUtility;
    private AmazonS3Client s3Client;
    private int callCount = 0;
    public SyncRecordInBackground() {
        super(SyncRecordInBackground.class.getName());
    }

    /**
     * @param f
     * @return
     */
    public static long getFolderSize(File f) {
        long size = 0;
        if (f.isDirectory()) {
            for (File file : f.listFiles()) {
                size += getFolderSize(file);
            }
        } else {
            size = f.length();
        }
        return size;
    }

    /**
     * to check if the recorded file is valid or not
     *
     * @param s3
     * @param path
     * @return
     * @throws AmazonClientException
     * @throws AmazonServiceException
     * @throws UnknownHostException
     */
    public static boolean isValidFile(AmazonS3 s3, String path, String bucketName) throws AmazonClientException, AmazonServiceException, UnknownHostException {
        boolean isValidFile = true;
        try {
            ObjectMetadata objectMetadata = s3.getObjectMetadata(bucketName, path);
        } catch (AmazonS3Exception s3e) {
            if(s3e.getStatusCode() == 400) {

            }
            else if (s3e.getStatusCode() == 404) {
                isValidFile = false;
            } else {
                throw s3e;    // rethrow all S3 exceptions other than 404

            }
        }

        return isValidFile;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startMyOwnForeground(){
        String channelName = "Background Service";
        NotificationChannel chan = new NotificationChannel(NotificationsUI.CHANNEL_ID_BACKGROUND, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NotificationsUI.CHANNEL_ID_BACKGROUND);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("NinjaCRM is running")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(NotificationsUI.BACKGROUND_ID, notification);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        pref = Preferences.getInstance();
        pref.load(this);
        pref.setBackgroundThread(false);
        realm = Realm.getDefaultInstance();
        System.out.println("Running Intent");
        try {
            if (Build.VERSION.SDK_INT >= 28) {
                startMyOwnForeground();
            }
        } catch (Exception e) {

        }
        //checkForPermission();
        syncCDR();
        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.P) {
            checkAwsCreds(this);
        }
        //TelephonyDbHandler.getInstance().deleteFileEmptyRecord();
        TelephonyDbHandler.getInstance().delete7DaysOldRecords();
        TelephonyDbHandler.getInstance().deleteNullRecords();
    }

    private void checkAwsCreds(Context context) {
        BasicSessionCredentials sessionCredentials = new BasicSessionCredentials(
                new NinjaEncryption().decrypt(pref.getS3UserName()),
                new NinjaEncryption().decrypt(pref.getS3Password()),
                new NinjaEncryption().decrypt(pref.getS3SessionToken()));
        s3Client = new AmazonS3Client(sessionCredentials, Region.getRegion(Regions.AP_SOUTHEAST_1));
        try {
            System.out.println("S3 Client::"+Thread.currentThread().equals(Looper.getMainLooper().getThread()));
            List<Bucket> buckets = s3Client.listBuckets();
            System.out.println("S3 Client::"+buckets.toString());
            syncAws();
        }
        catch (AmazonS3Exception s3) {
            System.out.println("S3 Client:: Exception");
            s3.printStackTrace();
            if(s3.getStatusCode() == 400 && callCount < 3) {
                System.out.println("S3 Client:: Calling AWS FETCH");
                new FetchAwsCredentials(new FetchAwsCredentials.FetchAwsCredentialsCallBack() {
                    @Override
                    public void onFetchAwsCredentialsSuccess() {
                        checkAwsCreds(context);
                    }
                    @Override
                    public void onFetchAwsCredentialsError() {

                    }
                }, context).call();
                callCount++;
            }
        }
    }

    /**
     * syncing rcords with sales cRM server
     */
    private void syncCDR() {
        System.out.println("SyncCDR");
        pref = Preferences.getInstance();
        pref.load(this);
        try {
            details = composeDetailedJSONfromRealm(false);
        } catch (OutOfMemoryError me) {
            me.printStackTrace();
            details = composeDetailedJSONfromRealm(true);
        }

        System.out.println("Sync CDR PROCESS");

        //making API call for syncCDR using retrofit
        System.out.println("CDR:"+!pref.getLimitSyncStatus() );
        if (!pref.getLimitSyncStatus() && details != null && details.size()>0) {
            System.out.println("SyncCDRCalled");
            pref.setLimitSyncStatus(true);
            TelephonyDbHandler.getInstance().dbRecordCDRUnSyncedCount(new DbRecordSyncCountListener() {
                @Override
                public void dbRecordSyncCountQueryResult(int count) {
                    System.out.println("SyncCDRCalled:" + count);
                    if (count > 0) {
                        TelephonyModel data = new TelephonyModel();
                        data.setData(details);
                        pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_CDR_START, getCDRDetails(details));
                        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).SyncCDR(data, new Callback<SyncDbResponse>() {
                            @Override
                            public void success(final SyncDbResponse syncDbResponse, Response response) {
                                List<Header> headerList = response.getHeaders();
                                for (Header header : headerList) {
                                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                                        ApiUtil.UpdateAccessToken(header.getValue());
                                    }
                                }

                                pref = Preferences.getInstance();
                                pref.load(SyncRecordInBackground.this);
                                pref.setLimitSyncStatus(false);
                                System.out.println("TELEPHONY syncDbResponse 00 - " + syncDbResponse.toString());
                                System.out.println("TELEPHONY syncDbResponse 0 - " + syncDbResponse.getStatusCode());
                                if (syncDbResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                    pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_CDR_SUCCESS, getCDRDetails(syncDbResponse));
                                    // System.out.println("TELEPHONY syncDbResponse 0 - "+syncDbResponse.getStatusCode());
                                    realm = Realm.getDefaultInstance();
                                    realm.executeTransactionAsync(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            for (int i = 0; i < syncDbResponse.getResult().size(); i++) {
                                                System.out.println("TELEPHONY syncDbResponse 1 - " + syncDbResponse.getResult().get(i).getStatus());
                                                if (syncDbResponse.getResult().get(i).getStatus().equalsIgnoreCase("true")) {
                                                    System.out.println("TELEPHONY syncDbResponse 2- " + true);
                                                    TelephonyDbHandler.getInstance().updateAfterSync(realm, Integer.parseInt
                                                            (syncDbResponse.getResult().get(i).getId()), 1);
                                                } else {
                                                    System.out.println("TELEPHONY syncDbResponse 3- " + false);
                                                }
                                            }
                                            //realm.close();

                                        }
                                    });
                                    //realm.close();
                                }
                                else {
                                    pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_CDR_FAILED, CleverTapConstants.EVENT_TELEPHONY_FAILURE_REASON_STATUS_CODE+":"+syncDbResponse.getStatusCode(), getCDRDetails(details));
                                }

                            }

                            @Override
                            public void failure(RetrofitError error) {
                                pref.setLimitSyncStatus(false);
                                System.out.println("TELEPHONY syncDbResponse000 - " + error.toString());
                                pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_CDR_FAILED, CleverTapConstants.EVENT_TELEPHONY_FAILURE_REASON_NETWORK, getCDRDetails(details));
                            }
                        });

                    } else {
                        pref.setLimitSyncStatus(false);
                    }
                }
            });
        }
        if (realm != null) {
            try {
                realm.close();
            } catch (Exception ignored) {
                // Crashlytics.logException(ignored);
            }

        }
    }

    private String getCDRDetails(TelephonyDB telephonyDB) {
        StringBuilder cdrDetails = new StringBuilder();
        if (telephonyDB!=null) {
                cdrDetails.append("CDR_NUMBER:").append(telephonyDB.getCalledNumber());
                cdrDetails.append("-LEAD_ID:").append(telephonyDB.getLeadId());
                cdrDetails.append("-DURATION:").append(telephonyDB.getDuration());
        }
        return  cdrDetails.toString();
    }

    private String getCDRDetails(RealmResults<TelephonyDB> telephonyDbList) {
        StringBuilder cdrDetails = new StringBuilder();
        if (telephonyDbList!=null) {
            for (TelephonyDB telephonyDB : telephonyDbList) {
                cdrDetails.append("CDR_NUMBER:").append(telephonyDB.getCalledNumber());
                cdrDetails.append("-LEAD_ID:").append(telephonyDB.getLeadId());
                cdrDetails.append("-DURATION:").append(telephonyDB.getDuration());
                cdrDetails.append(",");
            }
        }
        return  cdrDetails.toString();
    }

    private String getCDRDetails(ArrayList<RecordingsModel> details) {
        StringBuilder cdrDetails = new StringBuilder();
        if (details!=null) {
            for (RecordingsModel recordingsModel : details) {
                cdrDetails.append("CDR_NUMBER:").append(recordingsModel.getCalled_number());
                cdrDetails.append("-LEAD_ID:").append(recordingsModel.getLead_id());
                cdrDetails.append("-DURATION:").append(recordingsModel.getCall_duration());
                cdrDetails.append(",");
            }
        }
        return  cdrDetails.toString();
    }

    private String getCDRDetails(SyncDbResponse syncDbResponse) {
        StringBuilder cdrDetails = new StringBuilder();
        if(syncDbResponse!=null && syncDbResponse.getResult()!=null) {
            for (SyncDbResponse.Result result : syncDbResponse.getResult()) {
                cdrDetails.append("CDR_NUMBER:").append(result.getCalled_number());
                cdrDetails.append("-LEAD_ID:").append(result.getLead_id());
                cdrDetails.append("-DURATION:").append(result.getDuration());
                cdrDetails.append("-STATUS:").append(result.getStatus());
                cdrDetails.append(",");
            }
        }
        return cdrDetails.toString();
    }

    /**
     * syncing records with azure
     */
    private void syncAws() {
        System.out.println("SyncAzure");
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    telephonyDbList = realm.where(TelephonyDB.class).equalTo("fileSyncStatus", 0)
                            .findAll();
                    pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_FILE_START, getCDRDetails(telephonyDbList));

                    if (telephonyDbList != null) {
                        for (int i = 0; i < telephonyDbList.size(); i++) {
                            TelephonyDB temp = telephonyDbList.get(i);
                            String filePath = temp.getFilePath();
                            String fileName = temp.getFileName();
                            if (isExistsFile(fileName)) {
                                try {
                                    if (!isValidFile(s3Client, pref.getDealerName() + "/" + fileName, new NinjaEncryption().decrypt(pref.getS3BucketName()))) {
                                        PutObjectRequest por = new PutObjectRequest(new NinjaEncryption().decrypt(pref.getS3BucketName()), pref.getDealerName() + "/" + fileName, new java.io.File(filePath)).withCannedAcl(CannedAccessControlList.PublicRead);
                                        s3Client.putObject(por);
                                        System.out.println("TELEPHONY - " + pref.getDealerName() + "-" + s3Client);
                                    } else {
                                        /**
                                         * update sync status
                                         */
                                        pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_FILE_SUCCESS, getCDRDetails(temp));
                                        TelephonyDbHandler.getInstance().updateFileSyncStatus(realm, temp.getUuId(), 1);
                                        // Log.e("BackFile", " uploaded: " + dBHelper.updateDetailedStorageStatus(id, 1, 1, pref.getDealer()));

                                    }

                                } catch (AmazonS3Exception amazonS3Exception) {
                                    pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_FILE_FAILED, "AmazonS3Exception", getCDRDetails(temp));
                                    amazonS3Exception.printStackTrace();
                                } catch (AmazonClientException ace) {
                                    pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_FILE_FAILED, "AmazonClientException", getCDRDetails(temp));
                                    ace.printStackTrace();
                                } catch (UnknownHostException e) {
                                    pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_FILE_FAILED, "UnknownHostException", getCDRDetails(temp));
                                    e.printStackTrace();
                                }

                                try {
                                    if (isValidFile(s3Client, pref.getDealerName() + "/" + fileName, new NinjaEncryption().decrypt(pref.getS3BucketName()))) {
                                        TelephonyDbHandler.getInstance().updateFileSyncStatus(realm, temp.getUuId(), 1);
                                        System.out.println("TELEPHONY - " + 1);
                                        pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_FILE_SUCCESS, getCDRDetails(temp));
                                        // Log.e("BackFile", " uploaded: " + dBHelper.updateDetailedStorageStatus(id, 1, 1, pref.getDealer()));
                                    } else {
                                        System.out.println("TELEPHONY - " + 0);
                                        TelephonyDbHandler.getInstance().updateFileSyncStatus(realm, temp.getUuId(), 0);
                                        pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_FILE_FAILED, "Invalid File", getCDRDetails(temp));

                                        // Log.e("BackFile", "Not Uploaded" + dBHelper.updateDetailedStorageStatus(id, 0, 0, pref.getDealer()));
                                    }
                                } catch (UnknownHostException e) {
                                    e.printStackTrace();
                                    pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_FILE_FAILED, "UnknownHostException", getCDRDetails(temp));
                                } catch (AmazonS3Exception amazonS3Exception) {
                                    amazonS3Exception.printStackTrace();
                                    pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_FILE_FAILED, "AmazonS3Exception", getCDRDetails(temp));

                                } catch (AmazonClientException ace) {
                                    ace.printStackTrace();
                                    pushCleverTapTelephony(CleverTapConstants.EVENT_TELEPHONY_TYPE_SYNC_FILE_FAILED, "AmazonClientException", getCDRDetails(temp));
                                }


                            } else {
                                //dBHelper.deleteNotExistsRecords(id);
                                TelephonyDbHandler.getInstance().deleteRecordFromDB(realm, temp.getUuId());

                            }

                        }
                    }
                    //realm.close();
                }
            });
            if (realm != null) {
                try {
                    realm.close();
                } catch (Exception ignored) {
                    // Crashlytics.logException(ignored);
                }

            }
        } catch (Exception ignored) {

        }

    }


    /**
     * check if there is permission
     */
    private void checkForPermission() {
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<TelephonyDB> telephonyDBRealmResults = TelephonyDbHandler.getInstance().queryLastEvents(realm);

                    int i = 0;
                    if (telephonyDBRealmResults != null && telephonyDBRealmResults.size() > 0) {
                        long total = 0;
                        while (i < 3 && i < telephonyDBRealmResults.size()) {
                            TelephonyDB temp = telephonyDBRealmResults.get(i);
                            String fileName = temp.getFileName();
                            if (isExistsFile(fileName)) {
                                String filePath = Environment.getExternalStorageDirectory().getPath();
                                File file = new File(filePath, WSConstants.AUDIO_RECORDER_FOLDER + "/" + fileName);
                                long size = file.length();
                                total += size;
                                Log.e("FilePath", getFolderSize(file) + "  :  File Size : " + total);

                            } else {
                                Log.e("FilePath", "File not existrs...");
                            }
                            i++;

                        }
                        System.out.println("TOTAL IS " + total);
                        if (total == 0) {
                            /*Intent activitIintent = new Intent(getApplicationContext(), PermissionDialog.class);
                            activitIintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            activitIintent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            getApplicationContext().startActivity(activitIintent);*/
                        }
                    }
                    // realm.close();

                }
            });

            if (realm != null) {
                try {
                    realm.close();
                } catch (Exception ignored) {
                    // Crashlytics.logException(ignored);
                }
            }
        } catch (Exception ignored) {

        }
    }

    /**
     * to check the recorded file size
     *
     * @param filePath
     */
    void fileSizeCheck(String filePath) {
        try {
            String fpath = Environment.getExternalStorageDirectory().getPath();
            File file = new File(fpath, WSConstants.AUDIO_RECORDER_FOLDER + "/" + filePath);
            long length = file.length();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * to check if the file exists in location
     *
     * @param //filePath
     * @return
     */
    boolean isExistsFile(String fileName) {
        try {
            String filepath = Environment.getExternalStorageDirectory().getPath();
            File file = new File(filepath, WSConstants.AUDIO_RECORDER_FOLDER + "/" + fileName);
            if (file.exists()) {
                return true;
            } else {
                return false;
            }
        }
        catch (Exception e) {
            return false;
        }
    }

    public void onDestroy() {
        System.out.println("SyncIntentServiceDestroy");
        super.onDestroy();
    }

    /**
     * creating the request data string without any limit
     *
     * @return
     */
    private ArrayList<RecordingsModel> composeDetailedJSONfromRealm(final Boolean limitFlag) {
        try {
            pref = Preferences.getInstance();
            pref.load(this);
            TelephonyModel data = new TelephonyModel();
            realm = Realm.getDefaultInstance();
            telephonyDbList = realm.where(TelephonyDB.class).equalTo("recordSyncStatus", 0)
                    .findAll();

            if (telephonyDbList != null) {
                wordList = new ArrayList<RecordingsModel>();
                wordList.clear();
                int limit = telephonyDbList.size();
                if (limitFlag && telephonyDbList.size() <= 100) {
                    limit = 100;
                }
                for (int i = 0; i < limit; i++) {
                    TelephonyDB temp = telephonyDbList.get(i);

                    RecordingsModel recordingsModelTemp = new RecordingsModel();
                    recordingsModelTemp.setId("" + temp.getId());
                    recordingsModelTemp.setLead_id(temp.getLeadId());
                    recordingsModelTemp.setCalled_number(temp.getCalledNumber());
                    recordingsModelTemp.setCall_start_time(temp.getStartTime());
                    recordingsModelTemp.setCall_end_time(temp.getEndTime());
                    recordingsModelTemp.setCall_duration("" + temp.getDuration());
                    recordingsModelTemp.setCall_hangup_cause(temp.getCallHangUpCause());
                    recordingsModelTemp.setFile_name(temp.getFileName());
                    recordingsModelTemp.setFile_created_on(temp.getFileCreatedDate());
                    recordingsModelTemp.setUuid(temp.getUuId());
                    recordingsModelTemp.setCaller_number(pref.getUserMobile());
                    recordingsModelTemp.setImei_number(pref.getImeiNumber());
                    recordingsModelTemp.setLast_modified(temp.getLastModifiedTime());
                    recordingsModelTemp.setCall_type(temp.getCall_type());
                    recordingsModelTemp.setCall_id(temp.getCallId());
                    wordList.add(recordingsModelTemp);
                }
            }
            try {
                realm.close();
            } catch (Exception ignored) {
                //Crashlytics.logException(ignored);
            }

        } catch (Exception ignored) {

        }
        return wordList;
    }

    private void pushCleverTapTelephony(String eventType, String cdrDetails) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_TYPE, eventType);
        hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_CDR_DETAILS, cdrDetails);
         CleverTapPush.pushEvent(CleverTapConstants.EVENT_TELEPHONY, hashMap);

    }
    private void pushCleverTapTelephony(String eventType, String reason, String cdrDetails ) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_TYPE, eventType);
        hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_FAILURE_REASON, reason);

        hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_CDR_DETAILS, cdrDetails);

        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TELEPHONY, hashMap);

    }

}
