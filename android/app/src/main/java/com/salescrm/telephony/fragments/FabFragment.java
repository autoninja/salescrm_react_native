package com.salescrm.telephony.fragments;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddExchangeCarActivity;
import com.salescrm.telephony.activity.AddLeadActivity;
import com.salescrm.telephony.activity.AddToContactsActivity;
import com.salescrm.telephony.activity.ChangeLeadStatusActivity;
import com.salescrm.telephony.activity.EmailSmsActivity;
import com.salescrm.telephony.activity.FormRenderingActivity;
import com.salescrm.telephony.activity.PlanNextTaskPicker;
import com.salescrm.telephony.activity.ProformaInvoiceActivity;
import com.salescrm.telephony.activity.WhatsappActivity;
import com.salescrm.telephony.activity.createEnquiry.RNCreateLeadActivity;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.PlannedActivities;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.car.ExchangeCarDetails;
import com.salescrm.telephony.dbOperation.CallStatDbOperation;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.dbOperation.LeadMobileMappingDbOperations;
import com.salescrm.telephony.interfaces.FabFragmentcommunication;
import com.salescrm.telephony.interfaces.PlanNextTaskResultListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AddNoteResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bharath on 6/6/16.
 */
public class FabFragment extends Fragment implements FabFragmentcommunication,View.OnClickListener {
    FloatingActionButton fab_frame_call,fab_frame_change_lead,fab_frame_activity,fab_frame_car,fab_frame_email,
            fab_frame_whatsapp, fab_frame_proforma, fab_frame_exchange_car;
    private TextView tvAddNote, tvFrameCar,tvFrameEmail, tvFrameProforma, tvFrameExchangeCar;
    private FloatingActionButton fabAddNote;
    private Preferences pref;
    private String brand_id;
    private Realm realm;
    private SalesCRMRealmTable salesCRMRealmTable;
    private ButtonClickListenerC360 listenerC360;
    private boolean isClientILomOrBank;


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Override
    public void onAttach(Context context) {
        try {
            listenerC360 = (ButtonClickListenerC360) context;
        }
        catch (ClassCastException cl){
            cl.printStackTrace();
        }
        super.onAttach(context);
    }

    private String getScheduledActivityId() {
        if(realm.where(PlannedActivities.class)
                .equalTo("leadID", Integer.parseInt(pref.getLeadID())).equalTo("isDone", false)
                .findAll().isValid()) {
            RealmResults<PlannedActivities> plannedActivitiesList = realm.where(PlannedActivities.class)
                    .equalTo("leadID", Integer.parseInt(pref.getLeadID())).equalTo("isDone", false).findAll();
            if(plannedActivitiesList.size() == 1) {
                return plannedActivitiesList.get(0).getPlannedActivityScheduleId()+"";
            }
        }
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        pref = Preferences.getInstance();
        realm=Realm.getDefaultInstance();
        pref.load(getActivity());
        isClientILomOrBank = pref.isClientILom()||pref.isClientILBank();
        View view=inflater.inflate(
                pref.isClientILom()?R.layout.fab_fragment_i_lom:
                        pref.isClientILBank()?R.layout.fab_fragment_i_lom_bank :
                                R.layout.fab_fragment
                ,container,false);


        RealmResults<SalesCRMRealmTable> data = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .equalTo("isDone", false)
                .equalTo("createdOffline",false).findAll();

        for(int i=0;i<data.size();i++){
            if(data.get(i).getIsLeadActive()==0){
                disableViews(view,inflater,container);
               break;
            }
        }
  //      fab_frame_change_lead=(FloatingActionButton) view.findViewById(R.id.fab_frame_change_lead);
  //      fab_frame_activity=(FloatingActionButton) view.findViewById(R.id.fab_frame_activity);
        fab_frame_email=(FloatingActionButton) view.findViewById(R.id.fab_frame_email);
        tvFrameEmail =(TextView) view.findViewById(R.id.tv_frame_email);
        fabAddNote=(FloatingActionButton)view.findViewById(R.id.fab_frame_add_note);
        tvAddNote=(TextView)view.findViewById(R.id.tv_frame_add_note);
        tvFrameCar=(TextView)view.findViewById(R.id.tv_frame_car);
        tvFrameProforma = (TextView) view.findViewById(R.id.tv_frame_proforma);
        tvFrameExchangeCar = (TextView) view.findViewById(R.id.tv_frame_exchange_car);
        fab_frame_call= (FloatingActionButton) view.findViewById(R.id.fab_frame_call);
        fab_frame_car=(FloatingActionButton) view.findViewById(R.id.fab_frame_car);
        fab_frame_whatsapp = (FloatingActionButton) view.findViewById(R.id.fab_frame_whatsapp);
        fab_frame_proforma = (FloatingActionButton) view.findViewById(R.id.fab_frame_proforma);
        fab_frame_exchange_car = (FloatingActionButton) view.findViewById(R.id.fab_frame_exchange_car);
        fabAddNote.setOnClickListener(this);
        tvAddNote.setOnClickListener(this);
//        fab_frame_activity.setEnabled(false);
//        fab_frame_change_lead.setEnabled(false);
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findFirst();

        RealmResults<ExchangeCarDetails> exchangeCarDetailsRealmResults = realm.where(ExchangeCarDetails.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findAll();

        if(exchangeCarDetailsRealmResults.size() > 0){
            fab_frame_exchange_car.setVisibility(View.GONE);
            tvFrameExchangeCar.setVisibility(View.GONE);
        }else {
            fab_frame_exchange_car.setVisibility(View.VISIBLE);
            tvFrameExchangeCar.setVisibility(View.VISIBLE);
        }
       /* tvAddNote.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                add_note();

            }
        });*/


       if(DbUtils.isBike()) {
           //tvFrameCar.setText("Add Bike");
           //fab_frame_car.setImageResource(R.drawable.ic_bike_white);
           fab_frame_email.setVisibility(View.GONE);
           tvFrameEmail.setVisibility(View.GONE);
           tvFrameCar.setVisibility(View.GONE);
           fab_frame_car.setVisibility(View.GONE);
           fabAddNote.setVisibility(View.GONE);
           tvAddNote.setVisibility(View.GONE);
       }else{
           fab_frame_email.setVisibility(View.VISIBLE);
           tvFrameEmail.setVisibility(View.VISIBLE);
           tvFrameCar.setVisibility(View.VISIBLE);
           fab_frame_car.setVisibility(View.VISIBLE);
           fabAddNote.setVisibility(View.VISIBLE);
           tvAddNote.setVisibility(View.VISIBLE);
       }
   /*     fab_frame_change_lead.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //SalesCRMApplication.sendEventToCleverTap("Mob Fab Change Lead");
                Util.setUserProfileCleverTap("Mob Fab Change Lead",null);
                change_lead_status();

            }
        });*/
     /*   fab_frame_activity.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
               // SalesCRMApplication.sendEventToCleverTap("Mob Fab Add Activity");
                Util.setUserProfileCleverTap("Mob Fab Add Activity",null);
                add_activity();

            }
        });*/
        fab_frame_car.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
               // SalesCRMApplication.sendEventToCleverTap("Mob Fab Add Car");
                add_car();

            }
        });
        fab_frame_email.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
               // SalesCRMApplication.sendEventToCleverTap("Mob Fab Send sms/email");
                email_sms();

            }
        });
        fab_frame_call.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
               // SalesCRMApplication.sendEventToCleverTap("Mob Fab Call");
                call();
            }
        });

        fab_frame_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendWhatsappMessage();
            }
        });

        if(salesCRMRealmTable!= null && salesCRMRealmTable.isTemplateExist()){
            fab_frame_proforma.setVisibility(View.VISIBLE);
            tvFrameProforma.setVisibility(View.VISIBLE);
        }else {
            fab_frame_proforma.setVisibility(View.GONE);
            tvFrameProforma.setVisibility(View.GONE);
        }
        fab_frame_proforma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pref.getUsersTopRole()) {
                    openProformaInvoice();
                }else{
                    Util.showToast(getActivity(), "Access Denied!!", Toast.LENGTH_LONG);
                }
                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put(CleverTapConstants.EVENT_PROFORMA_INVOICE_KEY_TYPE,
                        CleverTapConstants.EVENT_PROFORMA_INVOICE_TYPE_CLICK);
                CleverTapPush.pushEvent(CleverTapConstants.EVENT_PROFORMA_INVOICE, hashMap);
            }
        });

        fab_frame_exchange_car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RNCreateLeadActivity.class);
                intent.putExtra("isExchangeCar", true);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return view;
    }

    private void openProformaInvoice() {
        pref.setLeadID("" + salesCRMRealmTable.getLeadId());
        if(new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
            Intent proformaActivity = new Intent(getActivity(), ProformaInvoiceActivity.class);
            proformaActivity.putExtra("phone", getPhoneNumber());
            getActivity().startActivity(proformaActivity);
        }
    }

    private void sendWhatsappMessage() {

        //String phone = salesCRMRealmTable.getCustomerNumber().;
        //String customerName = salesCRMRealmTable.getFirstName()+" " + salesCRMRealmTable.getLastName();
        String customerName = salesCRMRealmTable.getCustomerName();
        if(Util.checkContactAvailable(getPhoneNumber(), getContext())){
            Intent intent = new Intent(getContext(), WhatsappActivity.class);
            intent.putExtra("phono", getPhoneNumber());
            intent.putExtra("from_my_task",false);
            getContext().startActivity(intent);
        }else {
            Intent intent = new Intent(getContext(), AddToContactsActivity.class);
            intent.putExtra("phono", getPhoneNumber());
            intent.putExtra("customer_name", customerName);
            intent.putExtra("from_proforma", false);
            intent.putExtra("from_my_task",false);
            getContext().startActivity(intent);
        }
    }

    private String getPhoneNumber() {
        String phone = "";
        for (int i = 0; i<salesCRMRealmTable.customerPhoneNumbers.size(); i++){
            if (salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumberStatus().equalsIgnoreCase("PRIMARY")) {
                phone = "" + salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumber();
            }
        }
        return phone;
    }

    private void disableViews(View view,LayoutInflater inflater, ViewGroup container) {
        view=inflater.inflate(R.layout.fab_lead_closed,container,false);
        //fab_frame_change_lead=(FloatingActionButton) view.findViewById(R.id.fab_frame_change_lead);
        //fab_frame_activity=(FloatingActionButton) view.findViewById(R.id.fab_frame_activity);
        fab_frame_email=(FloatingActionButton) view.findViewById(R.id.fab_frame_email);
        //fab_frame_activity.setEnabled(false);
        //fab_frame_change_lead.setEnabled(false);
        fab_frame_email.setEnabled(false);
    }


    @Override
    public void add_note() {
       // SalesCRMApplication.sendEventToCleverTap("Mob Fab Add Note");
        addNote();
    }

    @Override
    public void change_lead_status() {
        Intent in= new Intent(getActivity(), ChangeLeadStatusActivity.class);
        startActivity(in);
        getActivity().finish();
    }

    @Override
    public void add_activity() {
        new PlanNextTaskPicker(getContext(), new PlanNextTaskResultListener() {
            @Override
            public void onPlanNextTaskResult(int answerId) {
                if(answerId == WSConstants.FormAnswerId.BOOK_CAR_ID){
                    if(listenerC360!=null){
                        listenerC360.onBookCarRequested(true);
                    }
                }
                else {
                    callFormRendering(answerId);
                }
            }
        }).show();
    }
    private void callFormRendering(int answerId){
        Intent in= new Intent(getActivity(), FormRenderingActivity.class);
        in.putExtra("form_title","Plan Next Task");
        in.putExtra("form_action","Submit");
        in.putExtra("action_id", WSConstants.FormAction.ADD_ACTIVITY);
        int type = 1;
        SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("scheduledActivityId",pref.getScheduledActivityId()).findFirst();
        if(salesCRMRealmTable!=null&&(salesCRMRealmTable.getActivityId()
                ==WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY_ID)){
            type = 2;
        }
        in.putExtra("activity_type",WSConstants.FormAction.ADD_ACTIVITY);
        in.putExtra("answer_id",answerId);
        in.putExtra("scheduled_type", type);
        in.putExtra("from_c360",true);
        in.putExtra("dseId",pref.getCurrentDseId());
        startActivity(in);
        getActivity().finish();
    }

    @Override
    public void add_car() {
        Intent in= new Intent(getActivity(), AddExchangeCarActivity.class);
        startActivity(in);
        getActivity().finish();
    }

    @Override
    public void email_sms() {
       System.out.println("EMailSms"+"Hello SMSEMAIL");
        Intent in= new Intent(getActivity(), EmailSmsActivity.class);
        in.putExtra("from_proforma", false);
        in.putExtra("from_my_task",false);
        startActivity(in);
        getActivity().finish();
    }

    @Override
    public void call() {

        System.out.println("TELEPHONY LEAD ID IN CALL METHOD"+pref.getmTelephonyLeadID());
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findFirst();
        if (salesCRMRealmTable != null) {

            AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
            //builderSingle.setIcon(R.drawable.);
            builderSingle.setTitle("Select One Number to call:-");

            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_singlechoice);
            final List<String> strLeadId = new ArrayList<String>();
            final List<String> actualNumber = new ArrayList<String>();
            for(int i=0;i<salesCRMRealmTable.customerPhoneNumbers.size();i++){

                    String number = salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumber()+"";
                    if(isClientILomOrBank && salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumberStatus().equalsIgnoreCase("PRIMARY")) {
                        try {
                            pref.setmTelephonyLeadID(pref.getLeadID());
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            String callingNumber = pref.getHotlineNumber()+number;
                            if(Util.isNotNull(pref.getHotlineNumber())) {
                                callingNumber = callingNumber+Uri.encode("#");
                            }
                            callIntent.setData(Uri.parse("tel:"+callingNumber));
                            if(isAdded() && listenerC360 != null) {
                                pref.setCallingLeadId(salesCRMRealmTable.getLeadId()+"");
                                createCallStat(callingNumber);
                                startActivity(callIntent);
                                CleverTapPush.pushCommunicationEvent(CleverTapConstants.EVENT_COMMUNICATION_CALL, false);
                            }
                        } catch (ActivityNotFoundException activityException) {
                            pref.setmTelephonyLeadID("0");
                            Log.e("Calling a Phone Number", "Call failed", activityException);
                        }
                        return;
                    }
                    if(isClientILomOrBank && Util.isNotNull(number) && number.length()>2) {
                        number = number.charAt(0)+"" + number.charAt(1) + "XXXXXXXX";
                    }
                    arrayAdapter.add(""+number);
                    actualNumber.add(""+salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumber());
                    strLeadId.add(""+salesCRMRealmTable.customerPhoneNumbers.get(i).getLeadId());
            }


            builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    pref.setmTelephonyLeadID("0");
                    dialog.dismiss();
                }
            });

            builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    String strName = actualNumber.get(which);
                    String leadId = strLeadId.get(which);
                    LeadMobileMappingDbOperations.copyOrUpdateMobileNumber(realm, strName, Util.getLongDefaultZero(leadId));

                    try {
                        pref.setmTelephonyLeadID(pref.getLeadID());
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:"+strName));
                        if(isAdded() && listenerC360 != null) {
                            createCallStat(strName);
                            startActivity(callIntent);
                            CleverTapPush.pushCommunicationEvent(CleverTapConstants.EVENT_COMMUNICATION_CALL, false);
                        }
                    } catch (ActivityNotFoundException activityException) {
                        pref.setmTelephonyLeadID("0");
                        Log.e("Calling a Phone Number", "Call failed", activityException);
                    }

                }
            });
            builderSingle.show();
        }
    }

    @Override
    public void startSearchActivity() {
        startActivity(new Intent(getContext(), AddLeadActivity.class));
    }

    void createCallStat(String mobileNumber) {
        long createdAt = System.currentTimeMillis();
        pref.setCallStatCreatedAt(createdAt);
        CallStatDbOperation.createOnStartCall(
                createdAt,
                mobileNumber,
                salesCRMRealmTable.getLeadId()+"",
                getScheduledActivityId());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
           case R.id.tv_frame_add_note:
            case R.id.fab_frame_add_note:
                add_note();
                break;
        }
    }

    public void addNote(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.send_note_dialog);
        dialog.setTitle("Add Note");
        TextView btn_yes = (TextView) dialog.findViewById(R.id.bt_custom_dialog_yes);
        final EditText note = (EditText) dialog.findViewById(R.id.et_custom_dialog);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!note.getText().toString().isEmpty()){
                    submitNoteApi(note.getText().toString(),dialog);
                }
                else {
                    Toast.makeText(getContext(),"Please enter the note",Toast.LENGTH_SHORT).show();
                }

            }
        });

        TextView btn_no = (TextView) dialog.findViewById(R.id.bt_custom_dialog_no);
        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void submitNoteApi(String s, final Dialog dialog){
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).AddNote(pref.getLeadID(), s, new Callback<AddNoteResponse>() {
            @Override
            public void success(AddNoteResponse addNoteResponse, Response response) {
                Util.showToast(getContext(),"Note added successfully.",Toast.LENGTH_LONG);

                if(addNoteResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }

                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    Log.e("noteadd", "noteadd -  " + header.getName() + " - " + header.getValue());
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }
                AddNoteResponse addnote = (AddNoteResponse) addNoteResponse;
                if(!getActivity().isFinishing()) {
                    dialog.dismiss();
                }
                }


            @Override
            public void failure(RetrofitError error) {
                Log.e("Noteadd", "noteadd - " + error.getMessage());

            }
        });

    }

    public interface ButtonClickListenerC360{
        void onBookCarRequested(boolean flag);
    }

}
