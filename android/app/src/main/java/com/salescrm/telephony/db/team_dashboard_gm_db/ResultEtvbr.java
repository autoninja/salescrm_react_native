package com.salescrm.telephony.db.team_dashboard_gm_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 13/7/17.
 */

public class ResultEtvbr extends RealmObject {

    @SerializedName("sales_manager")
    @Expose
    public RealmList<ResultSM> salesManager = new RealmList<>();

    @PrimaryKey
    @SerializedName("userId")
    @Expose
    private int userId;

    @SerializedName("abs_total")
    @Expose
    private AbsTotalGm absTotal;
    @SerializedName("rel_total")
    @Expose
    private RelTotalGm relTotal;

    public AbsTotalGm getAbsTotal() {
        return absTotal;
    }

    public void setAbsTotal(AbsTotalGm absTotal) {
        this.absTotal = absTotal;
    }

    public RelTotalGm getRelTotal() {
        return relTotal;
    }

    public void setRelTotal(RelTotalGm relTotal) {
        this.relTotal = relTotal;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public RealmList<ResultSM> getSalesManager() {
        return salesManager;
    }

    public void setSalesManager(RealmList<ResultSM> salesManager) {
        this.salesManager = salesManager;
    }
}
