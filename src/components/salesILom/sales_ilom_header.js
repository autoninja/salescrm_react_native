import React, { Component } from 'react'
import { View, Text, } from 'react-native'

export default class SalesILomHeader extends Component {

  render() {
    if (this.props.showPercentage) {
      return (
        <View style={{
          flexDirection: 'row',
          height: 36,
          borderRadius: 2,
          borderColor: '#CFD9E6',
          borderWidth: 1,
          backgroundColor: '#fff', marginTop: 10, alignItems: 'center',
        }}>

          <View style={{ flex: 1.5 }} />


          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <Text style={{ color: '#2D4F8B', textAlign: 'center' }} >Call%</Text>
          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <Text style={{ color: '#2D4F8B', textAlign: 'center' }} >Pros%</Text>
          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <Text style={{ color: '#2D4F8B', textAlign: 'center' }} >Sale%</Text>
          </View>
          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <Text style={{ color: '#2D4F8B', textAlign: 'center' }} >IL Done%</Text>
          </View>
          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <Text style={{ color: '#2D4F8B', textAlign: 'center' }} >L%</Text>
          </View>

        </View>
      )
    }
    else {
      return (
        <View style={{
          flexDirection: 'row',
          height: 36,
          marginTop: 10, alignItems: 'center',
          borderRadius: 2,
          borderColor: '#CFD9E6',
          borderWidth: 1,
          backgroundColor: '#fff'
        }}>

          <View style={{ flex: 1.5 }} />
          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <Text style={{ color: '#2D4F8B', textAlign: 'center' }} >Asgn</Text>
          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <Text style={{ color: '#2D4F8B', textAlign: 'center' }} >Call</Text>
          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <Text style={{ color: '#2D4F8B', textAlign: 'center' }} >Pros</Text>
          </View>

          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <Text style={{ color: '#2D4F8B', textAlign: 'center' }} >Sale</Text>
          </View>
          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <Text style={{ color: '#2D4F8B', textAlign: 'center' }} >IL Done</Text>
          </View>
          <View style={{
            flex: 1, height: '100%', borderLeftColor: '#fff',
            borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
          }}>
            <Text style={{ color: '#2D4F8B', textAlign: 'center' }} >L</Text>
          </View>

        </View>
      )
    }
  }
}
