package com.salescrm.telephony.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddExchangeCarActivity;
import com.salescrm.telephony.db.AppConfigDB;
import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandModelsDB;
import com.salescrm.telephony.db.car.CarBrandsDB;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by akshata on 27/5/16.
 */

public class AddcarFragment extends Fragment implements View.OnClickListener, View.OnFocusChangeListener,AddExchangeCarActivity.AddCar{

    static RealmResults<CarBrandsDB> mUserCarBrand;
    static AppConfigDB appConfdDb;
    Preferences pref;

    private static String selectedBrandId ="";
    private static String selectedBrandName ="";
    private static String selectedVariantId="";
    private static String selectedModelId="";
    private static String selectedColorId="";
    private static String selectedFuelTypeId="";
    private static String selectedModelName ="";
    private static String selectedVariantName ="";
    private static String selectedColorName ="";
    private static String selectedFuelName ="";

    private static String sSyncType="0";

    private ProgressDialog progressDialog;
    private Realm realm;

    private AutoCompleteTextView  autoTvInterCarModel, autoTvInterCarVariant,autoTvInterCarColor,autoTvInterCarFuelType;
    private RealmResults<CarBrandModelVariantsDB> carVariantList=null;
    private RealmResults<CarBrandModelsDB> carModelList=null;
    private RealmResults<CarBrandModelVariantsDB> colorList=null;
    private RealmResults<CarBrandModelsDB> carModelListOld;


    public static AddcarFragment newInstance(RealmResults<CarBrandsDB> userCarBrand) {
        AddcarFragment addcar = new AddcarFragment();
        mUserCarBrand = userCarBrand;
        return addcar;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        AddExchangeCarActivity a;
        if (context instanceof AddExchangeCarActivity) {
            a = (AddExchangeCarActivity) context;
            try {
               // _addCar = (AddCar) a;
            } catch (ClassCastException e) {
                throw new ClassCastException(a.toString() + " must implement AddCar");
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view1 = inflater.inflate(R.layout.add_car, container, false);
        final FragmentActivity c = getActivity();
        pref = Preferences.getInstance();
        pref.load(getActivity());

        autoTvInterCarModel = (AutoCompleteTextView) view1.findViewById(R.id.add_car_auto_tv_add_lead_car_model);
        autoTvInterCarVariant = (AutoCompleteTextView) view1.findViewById(R.id.add_car_auto_tv_lead_car_var);
        autoTvInterCarColor = (AutoCompleteTextView) view1.findViewById(R.id.add_car_auto_tv_lead_car_color);
        autoTvInterCarFuelType = (AutoCompleteTextView) view1.findViewById(R.id.add_car_auto_tv_lead_car_fuel_type);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        realm = Realm.getDefaultInstance();

        selectedVariantId = "";
        selectedModelId = "";
        selectedColorId = "";
        selectedFuelTypeId = "";
        selectedModelName = "";
        selectedVariantName = "";
        selectedColorName = "";
        selectedFuelName = "";

        initAdapter();

        appConfdDb = realm.where(AppConfigDB.class)
                .equalTo("key","dealerBrand")
                .findFirst();

        if(appConfdDb!=null){
            selectedBrandId = appConfdDb.getId();
            selectedBrandName = appConfdDb.getValue();
        }

        autoTvInterCarModel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Util.HideKeyboard(getActivity());
                //progressDialog.show();
                autoTvInterCarVariant.setText("");
                autoTvInterCarColor.setText("");
                autoTvInterCarFuelType.setText("");
                carVariantList = null;
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.dialog_textview, R.id.item, getCarBrandModelVariants());
                autoTvInterCarVariant.setAdapter(adapter);
                autoTvInterCarVariant.requestFocus();
                selectedModelId = carModelList.where().
                        equalTo("model_name",autoTvInterCarModel.getText().toString())
                        .findFirst()
                        .getModel_id();
                selectedModelName = autoTvInterCarModel.getText().toString();

            }
        });

        autoTvInterCarVariant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(getActivity());
                autoTvInterCarColor.setText("");
                autoTvInterCarFuelType.setText("");
//
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, setColorAndFuelType());
                autoTvInterCarColor.setAdapter(adapter);
                autoTvInterCarColor.requestFocus();
            }
        });

        autoTvInterCarColor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(getActivity());

        selectedColorId = colorList.where().equalTo("color", autoTvInterCarColor.getText().toString()).findFirst().getColor_id();
        selectedColorName =  autoTvInterCarColor.getText().toString();
            }
        });


        return view1;
    }


    private String[] getCarBrandModelVariants() {
        carVariantList = carModelList.where().equalTo("model_name",autoTvInterCarModel.getText().toString())
                .findFirst().getCar_variants().where()
                .distinct("variant_id");

        selectedModelId = carModelList.where().
                equalTo("model_name",autoTvInterCarModel.getText().toString())
                .findFirst()
                .getModel_id();
        selectedModelName = autoTvInterCarModel.getText().toString();

        List<String> data = new ArrayList<>();


        for (int i = 0; i < carVariantList.size(); i++) {
            if(carVariantList.get(i).getVariant()!=null){
                data.add(carVariantList.get(i).getVariant());
            }

        }
        String[] arr = new String[data.size()];
        for(int i=0;i<data.size();i++){
            arr[i] = data.get(i);
        }

        return arr;
    }

    private void initAdapter() {
        ArrayAdapter<String> brandsAdapterModel = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                getCarBrandModel());
        autoTvInterCarModel.setAdapter(brandsAdapterModel);

        ArrayAdapter<String> modelAdapterOld = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, getCarBrandModelForOld());

        autoTvInterCarModel.setThreshold(0);
        autoTvInterCarVariant.setThreshold(0);
        autoTvInterCarFuelType.setThreshold(0);
        autoTvInterCarColor.setThreshold(0);

        autoTvInterCarModel.setOnFocusChangeListener(this);
        autoTvInterCarVariant.setOnFocusChangeListener(this);
        autoTvInterCarColor.setOnFocusChangeListener(this);
        autoTvInterCarFuelType.setOnFocusChangeListener(this);

    }



    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus&& v instanceof AutoCompleteTextView && (((AutoCompleteTextView) v).getAdapter()!=null)) {
            ((AutoCompleteTextView) v).showDropDown();
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof AutoCompleteTextView) {
            ((AutoCompleteTextView) v).showDropDown();
        }
    }

    private String[] getCarBrandModel() {
        carModelList = realm.where(CarBrandModelsDB.class)
                .equalTo("category", WSConstants.CAR_CATEGORY_BOTH)
                .findAll();

        List<String> data = new ArrayList<>();
        for (int i = 0; i < carModelList.size(); i++) {
            if(carModelList.get(i).getModel_name()!=null){
                data.add(carModelList.get(i).getModel_name());
            }
        }
        String[] arr = new String[data.size()];
        for(int i=0;i<data.size();i++){
            arr[i] = data.get(i);
        }
        return arr;
    }


    private String[] getCarBrandModelForOld() {
        carModelListOld = realm.where(CarBrandModelsDB.class).equalTo("category",WSConstants.CAR_CATEGORY_BOTH).or()
                .equalTo("category",WSConstants.CAR_CATEGORY_OLD_CAR).findAll();
        List<String> data = new ArrayList<>();
        for (int i = 0; i < carModelListOld.size(); i++) {
            if(carModelListOld.get(i).getModel_name()!=null){
                data.add(carModelListOld.get(i).getModel_name());
            }

        }
        String[] arr = new String[data.size()];
        for(int i=0;i<data.size();i++){
            arr[i] = data.get(i);
        }
        return arr;
    }

    private String[] setColorAndFuelType() {
        colorList = carModelList.where().equalTo("model_name",autoTvInterCarModel.getText().toString())
                .findFirst().getCar_variants().where()
                .equalTo("variant",autoTvInterCarVariant.getText().toString()).findAll();
        CarBrandModelVariantsDB currentVariant = carVariantList.where().equalTo("variant",autoTvInterCarVariant.getText().toString()).findFirst();

        selectedVariantId = carVariantList.where().equalTo("variant",autoTvInterCarVariant.getText().toString())
                .findFirst().getVariant_id();
        selectedVariantName = autoTvInterCarVariant.getText().toString();

        List<String> data = new ArrayList<>();
        for (int i = 0; i < colorList.size(); i++) {
            if(colorList.get(i).getColor()!=null){
                data.add(colorList.get(i).getColor());
            }

        }


        String[] arr = new String[data.size()];
        for(int i=0;i<data.size();i++){
            arr[i] = data.get(i);
        }

        autoTvInterCarFuelType.setText(currentVariant.getFuel_type());
        autoTvInterCarFuelType.setTag(currentVariant.getFuel_type_id());
        selectedFuelTypeId = currentVariant.getFuel_type_id();
        selectedFuelName = currentVariant.getFuel_type();
        autoTvInterCarFuelType.setTag(R.id.val,currentVariant.getFuel_type());
        return arr;
    }


    @Override
    public void sendAddCarData() {

        AddExchangeCarActivity.addcarmodelid = selectedModelId;
        AddExchangeCarActivity.addcarcolorid = selectedColorId;
        AddExchangeCarActivity.addcarvariantid = selectedVariantId;
        AddExchangeCarActivity.addfueltypeid =selectedFuelTypeId;
        AddExchangeCarActivity.addCarModelName =selectedModelName;
        AddExchangeCarActivity.addCarVariantName =selectedVariantName;
        AddExchangeCarActivity.addCarFuelTypeName =selectedFuelName;
        AddExchangeCarActivity.addCarColorName =selectedColorName;

        //AddExchangeCarActivity.syncTypeId = sSyncType;

       Log.e("MODEL","SELECTED MODEL ID=" + selectedModelId + " SELECTED COLOR=" + selectedColorId + " SELECTED VARIANT ID=" + selectedVariantId);

    }



}
