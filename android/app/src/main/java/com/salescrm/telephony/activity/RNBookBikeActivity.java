package com.salescrm.telephony.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.createEnquiry.RNCreateLeadActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandModelsDB;
import com.salescrm.telephony.db.car.CarBrandsDB;
import com.salescrm.telephony.dbOperation.TasksDbOperation;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.interfaces.ReactNativeToAndroidConnect;
import com.salescrm.telephony.locationHelper.LocationHelper;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.service_handlers.CarsFragmentServiceHandler;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;


public class RNBookBikeActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {

    private ReactInstanceManager mReactInstanceManager;
    private ReactRootView mReactRootView;
    private Preferences pref;
    private ProgressDialog progressDialog;
    private Realm realm;
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals("ON_BOOK_BIKE_RNBookBikeActivity")) {
                String ids = intent.getStringExtra("scheduledActivityIds");
                if (Util.isNotNull(ids)) {
                    String[] idArray = ids.split(",");
                    for (String id : idArray) {
                        Util.postBookingDbHandler(realm,
                                null,
                                Util.getInt(id),
                                5,
                                -1);
                    }
                }
                String leadLastUpdated =  intent.getStringExtra("leadLastUpdated");
                if(Util.isNotNull(leadLastUpdated)) {
                    TasksDbOperation.getInstance().updateLeadLastUpdated(realm, intent.getStringExtra("leadId"), leadLastUpdated);
                }

                pref.setReloadC360(true);
                finish();
                System.out.println("OnClose");
            }

        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        Preferences.load(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Redirecting to C360 !!!.. Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();
        Bundle initialProps = new Bundle();
        initialProps.putString("token", Preferences.getAccessToken());
        initialProps.putString("module", "BOOK_BIKE");
        initialProps.putInt("appUserId", pref.getAppUserId());
        initialProps.putString("interestedVehicle", new Gson().toJson(getInterestedVehicleModel()));
        initialProps.putString("formSubmissionInputData", getIntent().getStringExtra("formSubmissionInputData"));
        initialProps.putString("leadId", getIntent().getStringExtra("leadId"));
        initialProps.putString("leadLastUpdated", getIntent().getStringExtra("leadLastUpdated"));

        mReactRootView = new ReactRootView(this);
        mReactRootView.startReactApplication(
                mReactInstanceManager,
                "NinjaCRMSales",
                initialProps
        );
        setContentView(mReactRootView);

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }


    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
        try {
            registerReceiver(broadcastReceiver, new IntentFilter("ON_BOOK_BIKE_RNBookBikeActivity"));

        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private VehicleModel getInterestedVehicleModel() {
        UserDetails userDetails = realm.where(UserDetails.class).findFirst();
        if (userDetails != null && userDetails.getUserCarBrands().size() > 0) {
            CarBrandsDB brand = realm.where(CarBrandsDB.class).equalTo("brand_id", userDetails.getUserCarBrands().get(0).getBrand_id()).findFirst();
            if (brand != null) {
                List<VehicleModelsModel> models = new ArrayList<>();
                for (CarBrandModelsDB model : brand.getBrand_models()) {

                    if (model.getCategory().equalsIgnoreCase(WSConstants.CAR_CATEGORY_BOTH)) {

                        List<VehicleModelsVariantModel> variants = new ArrayList<>();
                        for (CarBrandModelVariantsDB variant : model.getCar_variants()) {
                            variants.add(new VehicleModelsVariantModel(
                                    Util.getInt(variant.getVariant_id()),
                                    variant.getVariant(),
                                    Util.getInt(variant.getFuel_type_id()),
                                    variant.getFuel_type(),
                                    Util.getInt(variant.getColor_id()),
                                    variant.getColor()));
                        }

                        models.add(new VehicleModelsModel(Util.getInt(model.getModel_id()),
                                model.getModel_name(), model.getCategory(), variants));
                    }
                }
                return new VehicleModel(Util.getInt(brand.getBrand_id()), brand.getBrand_name(), models);
            }

        }

        return null;
    }

    class VehicleModel {
        int brand_id;
        String brand_name;
        List<VehicleModelsModel> brand_models;

        VehicleModel(int brand_id, String brand_name, List<VehicleModelsModel> brand_models) {
            this.brand_id = brand_id;
            this.brand_name = brand_name;
            this.brand_models = brand_models;
        }
    }

    class VehicleModelsModel {
        int model_id;
        String model_name;
        String category;
        List<VehicleModelsVariantModel> car_variants;

        VehicleModelsModel(int model_id, String model_name, String category,
                           List<VehicleModelsVariantModel> car_variants) {
            this.model_id = model_id;
            this.model_name = model_name;
            this.category = category;
            this.car_variants = car_variants;
        }
    }

    class VehicleModelsVariantModel {
        int variant_id;
        String variant;
        int fuel_type_id;
        String fuel_type;
        int color_id;
        String color;

        VehicleModelsVariantModel(int variant_id, String variant,
                                  int fuel_type_id, String fuel_type, int color_id, String color) {
            this.variant_id = variant_id;
            this.variant = variant;
            this.fuel_type_id = fuel_type_id;
            this.fuel_type = fuel_type;
            this.color_id = color_id;
            this.color = color;
        }
    }


}
