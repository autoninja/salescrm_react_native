package com.salescrm.telephony.db.etvbr_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by prateek on 8/11/17.
 */

public class SalesManager extends RealmObject {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("info")
    @Expose
    private Info info;
    @SerializedName("team_leaders")
    @Expose
    private RealmList<TeamLeader> team_leaders = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public RealmList<TeamLeader> getTeamLeaders() {
        return team_leaders;
    }

    public void setTeamLeaders(RealmList<TeamLeader> teamLeaders) {
        this.team_leaders = teamLeaders;
    }
}
