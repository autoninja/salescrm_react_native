package com.salescrm.telephony.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.salescrm.telephony.BuildConfig;
import com.salescrm.telephony.telephonyModule.service.ServiceBackgroundRun;
import com.salescrm.telephony.utils.Util;

public class BootCompleteReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED") ||
                intent.getAction().equals("android.intent.action.QUICKBOOT_POWERON")) {
            //Util.showToast(context,"NinjaCRM Sales", Toast.LENGTH_LONG);
            //Handled in CallReceiver class

        } else if (intent.getAction().equals("android.intent.action.PACKAGE_REPLACED") &&
                intent.getData().getSchemeSpecificPart().equals(BuildConfig.APPLICATION_ID)) {

           // Util.showToast(context,"", Toast.LENGTH_LONG);
            //Telephony Removed
//            try{
//                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    Intent service = new Intent(context, ServiceBackgroundRun.class);
//                    context.startForegroundService(service);
//                }
//                else {
//                    Intent service = new Intent(context, ServiceBackgroundRun.class);
//                    context.startService(service);
//                }
//            }
//            catch (Exception e) {
//                Crashlytics.logException(e);
//            }

        }
    }
}
