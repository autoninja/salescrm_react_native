package com.salescrm.telephony.fragments.offline;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.offline.OfflineC360LeadProcessActivity;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.create_lead.Lead_data;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;

import io.realm.Realm;

/**
 * Created by bharath on 30/12/16.
 */
public class FabC360FragmentOffline extends Fragment implements View.OnClickListener {
    FloatingActionButton fab_frame_call,fab_frame_change_lead,fab_frame_activity,fab_frame_car,fab_frame_email;
    private TextView tvAddNote;
    private FloatingActionButton fabAddNote;
    private Preferences pref;
    private String brand_id;
    private Realm realm;
    private int scheduledActivityId=0;
    private TextView tvAddActivity;
    private SalesCRMRealmTable data;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fab_fragment_offline,container,false);
        pref = Preferences.getInstance();
        realm=Realm.getDefaultInstance();
        pref.load(getActivity());

        tvAddActivity = (TextView)view.findViewById(R.id.tv_frame_activity);
        fabAddNote=(FloatingActionButton)view.findViewById(R.id.fab_frame_add_note);
        tvAddNote=(TextView)view.findViewById(R.id.tv_frame_add_note);
        fab_frame_call= (FloatingActionButton) view.findViewById(R.id.fab_frame_call);
        fab_frame_change_lead=(FloatingActionButton) view.findViewById(R.id.fab_frame_change_lead);
        fab_frame_activity=(FloatingActionButton) view.findViewById(R.id.fab_frame_activity);
        fab_frame_car=(FloatingActionButton) view.findViewById(R.id.fab_frame_car);
        fab_frame_email=(FloatingActionButton) view.findViewById(R.id.fab_frame_email);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            scheduledActivityId = bundle.getInt("scheduledActivityId",0);
            data = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
            updateView();
        }
        fab_frame_activity.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(data!=null&&data.getCreateLeadInputDataDB()!=null){
                    if(data.getCreateLeadInputDataDB().getActivity_data()==null
                            ||data.getCreateLeadInputDataDB().getActivity_data().getActivity()==null){
                        Intent intent = new Intent(getContext(), OfflineC360LeadProcessActivity.class);
                        intent.putExtra("title","Add Activity");
                        intent.putExtra("action","Save");
                        intent.putExtra("from",1);
                        intent.putExtra("scheduledActivityId",scheduledActivityId);
                        startActivity(intent);
                        getActivity().onBackPressed();
                    }
                }
                else {
                    Toast.makeText(getContext(),"Activity Already Created",Toast.LENGTH_SHORT).show();
                }

            }
        });
        fab_frame_car.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), OfflineC360LeadProcessActivity.class);
                intent.putExtra("title","Add Car");
                intent.putExtra("action","Save");
                intent.putExtra("from",0);
                intent.putExtra("scheduledActivityId",scheduledActivityId);
                startActivity(intent);
                getActivity().onBackPressed();

            }
        });
        fab_frame_call.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                SalesCRMRealmTable realmTable = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
                if(realmTable!=null&&realmTable.getCreateLeadInputDataDB()!=null){
                    Lead_data data = realmTable.getCreateLeadInputDataDB().getLead_data();
                    if(data!=null){
                        for(int i=0;i<data.getMobile_numbers().size();i++){
                            if(data.getMobile_numbers().get(i).getIs_primary()){
                                Util.callNumber(data.getMobile_numbers().get(i).getNumber(),getContext(), null, null);
                                getActivity().onBackPressed();
                                break;
                            }
                        }
                    }
                }

            }
        });
        return view;
    }

    private void updateView() {
        if(data.getCreateLeadInputDataDB().getActivity_data()==null
                ||data.getCreateLeadInputDataDB().getActivity_data().getActivity()==null){
         changeBackground(tvAddActivity,fab_frame_activity,true);
        }
        else {
            changeBackground(tvAddActivity,fab_frame_activity,false);
        }
    }

    private void changeBackground(TextView tvAddActivity,FloatingActionButton fab, boolean b) {
        Drawable drawable;
        if(b){
            fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#0076ff")));
            drawable = ContextCompat.getDrawable(getContext(), R.drawable.card_fab_text);
        }
        else {
            fab.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#979797")));
            drawable = ContextCompat.getDrawable(getContext(), R.drawable.card_fab_text_inactive);

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            tvAddActivity.setBackground(drawable);
        } else {
            tvAddActivity.setBackgroundDrawable(drawable);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
           case R.id.tv_frame_add_note:

            case R.id.fab_frame_add_note:
                break;
        }
    }

    private void addActivity(){

    }



}
