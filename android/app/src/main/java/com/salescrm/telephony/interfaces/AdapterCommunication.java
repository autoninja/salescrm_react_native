package com.salescrm.telephony.interfaces;

/**
 * Created by Ravindra P on 28-03-2016.
 */
public interface AdapterCommunication {

    public void AllTaskFragmentCallBack(String LeadId);
    public void FormActivityFragmentCallBack();
    public void FormActivityRescheduleCallBack();
    public void FormActivityEmailSmaCallBack();



}
