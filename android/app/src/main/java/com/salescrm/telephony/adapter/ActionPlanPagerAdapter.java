package com.salescrm.telephony.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.salescrm.telephony.fragments.ActionPlanFragment;
import com.salescrm.telephony.fragments.AllActionPlanFragment;
import com.salescrm.telephony.fragments.DoneActionPlanFragment;
import com.salescrm.telephony.fragments.TodayActionPlanFragment;

/**
 * Created by Ravindra P on 08-12-2015.
 */
public class ActionPlanPagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 3;
    private ActionPlanFragment context;
    private Integer dseId;
    private Integer appUserId;

    public ActionPlanPagerAdapter(FragmentManager fragmentManager, ActionPlanFragment context, Integer dseId, Integer appUserId) {
        super(fragmentManager);
        this.context = context;
        this.appUserId = appUserId;
        this.dseId = dseId;
    }


    // Returns total number of pages
    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                // SalesCRMApplication.sendEventToCleverTap("Mob Tab Today");
                return TodayActionPlanFragment.newInstance(context);

           /* case 1:
                return TomorrowActionPlanFragment.newInstance();

            case 2:
                return TodayActionPlanFragment.newInstance();*/

            case 1:
                return AllActionPlanFragment.newInstance(context);

            case 2:
                return DoneActionPlanFragment.newInstance(context);

            default:
                //Never gonna happen
                return null;
        }
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Today";
            case 1:
                return "Tomorrow";
            case 2:
                return "Pending";
            case 3:
                return "All";
            default:
                return null;
        }
    }

    /*private void callAnalytics(String key, String value) {
        Bundle params = new Bundle();
        params.putString(key, value);
        SalesCRMApplication.getFirebaseAnalytics().logEvent("TAB", params);
    }*/

}