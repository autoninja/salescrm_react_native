package com.salescrm.telephony.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.NotificationRecyclerAdapter;
import com.salescrm.telephony.db.FcmNotificationObject;

import java.util.Collections;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by bharath on 5/12/16.
 */
public class AppNotificationActivity extends AppCompatActivity implements NotificationRecyclerAdapter.UnreadCount{
    private Toolbar toolbar;
    private int unreadCount;
    private RecyclerView nRecyclerView;
    private NotificationRecyclerAdapter notificationRecyclerAdapter;
    private RealmList<FcmNotificationObject> notificationObjects;
    private Realm realm;
    private TextView tvnotificationNotAvailable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        toolbar = (Toolbar) findViewById(R.id.app_toolbar);
        setSupportActionBar(toolbar);
        realm = Realm.getDefaultInstance();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        tvnotificationNotAvailable = (TextView) findViewById(R.id.notification_not_available);
        nRecyclerView = (RecyclerView) findViewById(R.id.notificationRecycler);
        nRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppNotificationActivity.this);
        nRecyclerView.setLayoutManager(mLayoutManager);
        notificationObjects = new RealmList<>();
        if((realm.where(FcmNotificationObject.class).findAll()!= null)){
            notificationObjects.addAll(realm.where(FcmNotificationObject.class).findAll());
            unreadCount = (realm.where(FcmNotificationObject.class).equalTo("notification_read", false).findAll()).size();
        }else {

        }
        if(notificationObjects.size() == 0){
            tvnotificationNotAvailable.setVisibility(View.VISIBLE);
            nRecyclerView.setVisibility(View.GONE);
        }else{
            nRecyclerView.setVisibility(View.VISIBLE);
            tvnotificationNotAvailable.setVisibility(View.GONE);
        }
        toolbar.setTitle(getString(R.string.notification) + "(" + unreadCount + ")");
        Collections.reverse(notificationObjects);
        notificationRecyclerAdapter = new NotificationRecyclerAdapter(notificationObjects, AppNotificationActivity.this);
        nRecyclerView.setAdapter(notificationRecyclerAdapter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void sendUnreadCount(int count) {
        toolbar.setTitle(getString(R.string.notification)+"("+count+")");
    }
}
