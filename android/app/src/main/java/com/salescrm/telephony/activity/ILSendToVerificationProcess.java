package com.salescrm.telephony.activity;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.dbOperation.TasksDbOperation;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.model.SendToVerificationILModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.BookingConfirmResponse;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
public class ILSendToVerificationProcess implements View.OnClickListener {
    ProgressDialog progressDialog;
    Realm realm;
    Preferences pref;
    String leadId, scheduledActivityId, leadLastUpdated;
    Context context;
    Activity activity;
    int from;

    private int answerId;
    private String selectedTenure;
    public static final int FROM_C_360 = 1;
    static final int FROM_FORM_RENDERING = 2;
    public static final int FROM_FORM_TASKS_LIST = 3;
    private FormSubmissionInputData prevForm = null;
    private String selectedLanguage = null;
    private String proposalId = null;
    private Dialog dialog;
    private View prevSelected = null;

    public void show(Activity activity, Context context, String leadId, String scheduledActivityId, int answerId,
                     FormSubmissionInputData formSubmissionInputData, int from) {
        this.activity = activity;
        this.context = context;
        progressDialog = new ProgressDialog(context);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        Preferences.load(context);

        this.leadId = leadId;
        this.scheduledActivityId = scheduledActivityId;
        this.leadLastUpdated = getLeadLastUpdated();
        this.from = from;
        this.prevForm = formSubmissionInputData;
        this.answerId = answerId;
        startVerificationProcess();
    }

    private String getLeadLastUpdated() {
        SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Util.getInt(leadId))
                .findAllSorted("leadLastUpdated", Sort.DESCENDING).first();
        if (salesCRMRealmTable != null) {
            return salesCRMRealmTable.getLeadLastUpdated();
        }
        return null;
    }

    private void startVerificationProcess() {
        dialog = new Dialog(context,R.style.AppTheme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
            //  dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }
        dialog.setContentView(R.layout.layout_language_verfication_picker);

        dialog.setContentView(R.layout.layout_language_verfication_picker);
        EditText etProposalId = dialog.findViewById(R.id.et_verification_language_proposal_id);
        dialog.findViewById(R.id.tv_submit_language_verification).setOnClickListener(this);
        dialog.findViewById(R.id.tv_cancel_language_verification).setOnClickListener(this);
        dialog.findViewById(R.id.tv_verification_language_english).setOnClickListener(this);
        dialog.findViewById(R.id.tv_verification_language_hindi).setOnClickListener(this);
        dialog.findViewById(R.id.tv_verification_language_tamil).setOnClickListener(this);
        dialog.findViewById(R.id.tv_verification_language_telugu).setOnClickListener(this);
        dialog.findViewById(R.id.tv_verification_language_malayalam).setOnClickListener(this);
        dialog.findViewById(R.id.tv_verification_language_kannada).setOnClickListener(this);
        etProposalId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                proposalId = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    void cancel() {
        dialog.dismiss();
    }
    private void submitForm() {
        if(Util.isNull(selectedLanguage)) {
            Util.showToast(context, "Please select the language", Toast.LENGTH_LONG);
        }
        else if(Util.isNull(proposalId)) {
            Util.showToast(context, "Please enter proposal id", Toast.LENGTH_LONG);
        }
        else {
            progressDialog.setMessage("Please wait..");
            progressDialog.show();
            SendToVerificationILModel sendToVerificationILModel = new SendToVerificationILModel();
            sendToVerificationILModel.setLead_id(leadId);
            sendToVerificationILModel.setLead_last_updated(leadLastUpdated);
            sendToVerificationILModel.setProposal_id(proposalId);
            sendToVerificationILModel.setLang_code(selectedLanguage);
            if (prevForm == null) {
                FormSubmissionInputData formSubmissionInputData = new FormSubmissionInputData();
                formSubmissionInputData.setLead_id(leadId);
                formSubmissionInputData.setLead_last_updated(leadLastUpdated);
                formSubmissionInputData.setScheduled_activity_id(scheduledActivityId);
                sendToVerificationILModel.setForm_object(formSubmissionInputData);
            } else {
                //Came from FormRendering
                sendToVerificationILModel.setForm_object(prevForm);
            }

            ApiUtil.GetRestApiWithHeader(Preferences.getAccessToken()).sendToVerificationIL(sendToVerificationILModel, new Callback<BookingConfirmResponse>() {
                @Override
                public void success(BookingConfirmResponse o, Response response) {
                    Util.updateHeaders(response.getHeaders());
                    if (!o.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        handleFailure();
                    } else {
                        if (o.getResult() != null) {
                            TasksDbOperation.getInstance().updateLeadLastUpdated(realm, leadId, o.getResult().getLead_last_updated());
                            handleSuccess();
                            Util.showToast(context, "Form has been submitted", Toast.LENGTH_LONG);
                        } else {
                            handleFailure();

                        }
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    handleFailure();
                }
            });
        }

    }

    private void handleFailure() {
        Util.showToast(context, "Form will be refreshed, Try again", Toast.LENGTH_SHORT);
        progressDialog.dismiss();
        navigate();
    }

    private void handleSuccess() {
        realm.beginTransaction();
        try {
            SalesCRMRealmTable data = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", Util.getInt(scheduledActivityId)).findFirst();
            if (data != null) {
                data.setDone(true);
            }
        } catch (Exception e) {
        }

        realm.commitTransaction();
        progressDialog.dismiss();
        navigate();
    }

    private void navigate() {
        switch (from) {
            case FROM_C_360:
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = activity.getIntent();
                        activity.overridePendingTransition(0, 0);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        activity.finish();
                        activity.overridePendingTransition(0, 0);
                        Intent in = new Intent(context, C360Activity.class);
                        context.startActivity(in);
                    }
                });
                break;
            case FROM_FORM_TASKS_LIST:
                Intent in = new Intent(context, C360Activity.class);
                context.startActivity(in);
                break;
            case FROM_FORM_RENDERING:
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = activity.getIntent();
                        activity.overridePendingTransition(0, 0);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        activity.finish();
                    }
                });
                break;
        }

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_submit_language_verification:
                submitForm();
                break;
            case R.id.tv_cancel_language_verification:
                cancel();
                break;
            default:
                if(prevSelected!=null) {
                    prevSelected.setSelected(false);
                }
                v.setSelected(true);
                selectedLanguage = v.getTag().toString();
                prevSelected = v;
        }
    }

}

