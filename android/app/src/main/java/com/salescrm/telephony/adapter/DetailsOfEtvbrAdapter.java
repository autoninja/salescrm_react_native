package com.salescrm.telephony.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.EtvbrDetailsResponse;
import com.salescrm.telephony.utils.Util;

import java.util.List;

/**
 * Created by nndra on 23-Nov-17.
 */

public class DetailsOfEtvbrAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<EtvbrDetailsResponse.LeadDetails> mEtvbrDetails;
    private Context mContext;
    private Preferences pref = null;


    public DetailsOfEtvbrAdapter(List<EtvbrDetailsResponse.LeadDetails> etvbrDetails,Context context) {
        mEtvbrDetails = etvbrDetails;
        mContext = context;
        this.pref = Preferences.getInstance();
        this.pref.load(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.details_etvbr_adapter_view, parent, false);
        return new DetailsOfEtvbrAdapter.DetailsOfEtvbrHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
              final int currentPosition = position;
        if (holder instanceof DetailsOfEtvbrHolder) {

            ((DetailsOfEtvbrHolder)holder).tvEnquiryListName.setText(mEtvbrDetails.get(position).getCustomer_name());
            if(mEtvbrDetails.get(position).getTitle() != null) {
                ((DetailsOfEtvbrHolder) holder).tvEnquiryListNameMr.setText(mEtvbrDetails.get(position).getTitle());
            }else{
                ((DetailsOfEtvbrHolder) holder).tvEnquiryListNameMr.setText("");
            }
            ((DetailsOfEtvbrHolder)holder).tvEnquiryListCar.setText(mEtvbrDetails.get(position).getCar_model_name());
            ((DetailsOfEtvbrHolder)holder).tvActivityName.setText(mEtvbrDetails.get(position).getActivity_name());

            if(mEtvbrDetails.get(position).getLead_tag() != null){
                String leadColor = mEtvbrDetails.get(position).getLead_tag();
                if(leadColor.contains("Hot")){
                    ((DetailsOfEtvbrHolder)holder).cardView.setCardBackgroundColor(Color.parseColor("#f7412d"));
                }else if(leadColor.contains("Cold")){
                    ((DetailsOfEtvbrHolder)holder).cardView.setCardBackgroundColor(Color.parseColor("#4DD0E1"));
                }else if(leadColor.contains("Warm")){
                    ((DetailsOfEtvbrHolder)holder).cardView.setCardBackgroundColor(Color.parseColor("#ff9900"));
                }
            }

            ((DetailsOfEtvbrHolder)holder).tvEnquiryListName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pref.setLeadID(mEtvbrDetails.get(currentPosition).getLead_id());
                    pref.setScheduledActivityId(Util.getInt(mEtvbrDetails.get(currentPosition).getSchedule_activity_id()));
                    if(pref.getLeadID().equalsIgnoreCase("")){

                    }else {
                        Intent intent = new Intent(mContext, C360Activity.class);
                        mContext.startActivity(intent);
                    }
                }
            });


        }

    }

    @Override
    public int getItemCount() {
        return mEtvbrDetails.size();
    }

    public class DetailsOfEtvbrHolder extends RecyclerView.ViewHolder {

        TextView tvEnquiryListName,tvEnquiryListNameMr,tvEnquiryListCar,tvActivityName;
        CardView cardView;
        ImageButton imgEnquiryListGo360;

        public DetailsOfEtvbrHolder(View itemView) {
            super(itemView);
            tvEnquiryListName = (TextView)itemView.findViewById(R.id.tv_enquiry_list_name);
            tvEnquiryListNameMr = (TextView)itemView.findViewById(R.id.tv_enquiry_list_name_mr);
            tvEnquiryListCar = (TextView)itemView.findViewById(R.id.tv_enquiry_list_car);
            tvActivityName = (TextView)itemView.findViewById(R.id.tv_activity_name);
            cardView = (CardView) itemView.findViewById(R.id.leadlist_cardview);
            imgEnquiryListGo360 = (ImageButton) itemView.findViewById(R.id.img_enquiry_list_go_360);
        }
    }

}
