package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by prateek on 9/12/16.
 */

public class SendSMSObjects extends RealmObject{

    String phone;
    String smsResult;
    int smsSync;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSmsResult() {
        return smsResult;
    }

    public void setSmsResult(String smsResult) {
        this.smsResult = smsResult;
    }

    public int getSmsSync() {
        return smsSync;
    }

    public void setSmsSync(int smsSync) {
        this.smsSync = smsSync;
    }
}
