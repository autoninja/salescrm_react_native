import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image } from 'react-native';
import CustomDateTimePicker from '../../utils/DateTimePicker'
import Utils from '../../utils/Utils'
import Moment from 'moment';

export default class DateTimeView extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            isFetching: false,
            dateTimePickerVisibility: false,
            dateTimePickerPayload: '',
            selectedDate: '',
            defaultDate: ''
        }
    }

    componentWillUnmount() {
        if (this.props.radioObject && this.props.radioObject.fQId) {
            this.props.pushToarray({ name: this.props.radioObject.fQId }, -1)
        }

        if (this.props.radioObject.ifVisibleMandatory) {
            this.props.mandatoryField({ id: this.props.radioObject.fQId }, -1)
        }
    }

    onDateSelect(date, payload) {
        this.setState({ defaultDate: '', selectedDate: date, dateTimePickerVisibility: !this.state.dateTimePickerVisibility })
        this.props.pushToarray({ name: payload, value: { answerValue: Moment(date).format("YYYY-MM-DD HH:mm:ss") + '', displayText: "", fAnsId: "" }, questionId: this.props.radioObject.questionId })
        if (this.props.radioObject.ifVisibleMandatory) {
            this.props.mandatoryField({ id: this.props.radioObject.fQId, isSelected: true })
        }
    }

    generateView(innerObjectsValues) {
        let { selectedDate } = this.state
        let { defaultDate } = this.state
        let isDisabled = (innerObjectsValues.editable == 0) ? true : false;
        if (!selectedDate) {
            if (innerObjectsValues.defaultFAId) {
                selectedDate = innerObjectsValues.defaultFAId.answerValue;
                defaultDate = Moment(innerObjectsValues.defaultFAId.answerValue).format("DD MMM YYYY, hh:mm a")
            }

            if (selectedDate) {
                this.props.pushToarray({ name: innerObjectsValues.fQId, value: { answerValue: selectedDate + '', displayText: "", fAnsId: "" }, questionId: innerObjectsValues.questionId })

                if (innerObjectsValues.ifVisibleMandatory) {
                    this.props.mandatoryField({ id: innerObjectsValues.fQId, isSelected: true })
                }
            }
        }
        return (
            <View>
                {(innerObjectsValues.title) ?
                    <Text style={{ fontSize: 18, marginTop: 10, fontWeight: 'bold' }}>{(innerObjectsValues.title) ? innerObjectsValues.title + "*" : ''}</Text> : null}

                <TouchableOpacity disabled={isDisabled} onPress={() => this.setState({ dateTimePickerPayload: innerObjectsValues.fQId, dateTimePickerVisibility: !this.state.dateTimePickerVisibility })}>
                    <View style={{ margin: 5, borderBottomWidth: 1, borderBottomColor: "#303030" }}>
                        <Text style={{ fontSize: 18, marginTop: 10 }}>{((defaultDate) ? defaultDate + '' : (selectedDate) ? Utils.getReadableDateTime(selectedDate) : "Pick a date")}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )


    }

    render() {
        //console.log("innerObjects", "reached")
        //console.log("innerObjects", this.props.innerObjects.fQId)
        //console.error("innerObjectRadio")
        //console.log("innerObjectRadio", JSON.stringify(this.props.innerObjects))
        return (
            <View style={{ flex: 1 }}>
                <CustomDateTimePicker
                    minimumDate={new Date()}
                    //maximumDate='undefined'
                    dateTimePickerVisibility={this.state.dateTimePickerVisibility}
                    datePickerMode="datetime"
                    payload={this.state.dateTimePickerPayload}
                    handleDateTimePicked={(date, payload) => {

                        this.onDateSelect(date, payload);
                    }}
                    hideDateTimePicked={() => {
                        this.setState({
                            dateTimePickerVisibility: false,
                            datePickerMode: 'datetime',
                            dateTimePickerMinimumDate: undefined,
                            dateTimePickerMaximumDate: undefined
                        })
                    }}
                />
                {this.generateView(this.props.radioObject)}
            </View>
        )
    }
}