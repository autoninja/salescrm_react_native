package com.salescrm.telephony.model;

public class RescheduleData {
    private String lead_id;
    private String date;
    private String remarks;

    public RescheduleData(String lead_id, String date, String remarks) {
        this.lead_id = lead_id;
        this.date = date;
        this.remarks = remarks;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }
}
