## IOS
> OPEN `ios/NinjaCRMSales.xcworkspace` file in Xcode

## Android
> OPEN `android` folder in Android Studio

<hr/>

### CodePush | [Repo](https://github.com/microsoft/react-native-code-push "CodePush Repo") | [Getting Started](https://docs.microsoft.com/en-us/appcenter/distribution/codepush/index "Getting Started")


#### 1. Install the App Center CLI and adding App center

```bash
npm install -g appcenter-cli
```

After successfuly installing the App Center CLI, execute the `appcenter login` command to configure the CLI for your App Center account details.

#### 2. Release to codepush
`ANDROID`
```
$ appcenter codepush release-react -a Autoninja/NinjaCRM-Sales-Android -m --description "Android Release"
```

`IOS`
```
$ appcenter codepush release-react -a Autoninja/NinjaCRM-Sales-IOS -m --description "iOS Release"
```
