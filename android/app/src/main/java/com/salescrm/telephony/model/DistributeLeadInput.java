package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by bharath on 28/8/17.
 */

public class DistributeLeadInput {
    private List<DSEIds> DSEIds;

    private List<Integer> leadIds;

    public List<DSEIds> getDSEIds ()
    {
        return DSEIds;
    }

    public void setDSEIds (List<DSEIds> DSEIds)
    {
        this.DSEIds = DSEIds;
    }

    public List<Integer> getLeadIds ()
    {
        return leadIds;
    }

    public void setLeadIds (List<Integer> leadIds)
    {
        this.leadIds = leadIds;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [DSEIds = "+DSEIds+", leadIds = "+leadIds+"]";
    }
    public class DSEIds
    {
        private Integer leads_to_distribute;

        private Integer user_id;

        public Integer getLeads_to_distribute ()
        {
            return leads_to_distribute;
        }

        public void setLeads_to_distribute (Integer leads_to_distribute)
        {
            this.leads_to_distribute = leads_to_distribute;
        }

        public Integer getUser_id ()
        {
            return user_id;
        }

        public void setUser_id (Integer user_id)
        {
            this.user_id = user_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [leads_to_distribute = "+leads_to_distribute+", user_id = "+user_id+"]";
        }
    }

}
