package com.salescrm.telephony.response.gamification;

import java.util.ArrayList;

/**
 * Created by bannhi on 30/1/18.
 */

public class WallOfFameResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    private WallOfFameResponse wallOfFameResponse;

    public WallOfFameResponse(WallOfFameResponse wallOfFameResponse){
        this.wallOfFameResponse = wallOfFameResponse;
    }
    public WallOfFameResponse(){

    }

    public WallOfFameResponse getWallOfFameResponse() {
        return wallOfFameResponse;
    }

    public void setWallOfFameResponse(WallOfFameResponse wallOfFameResponse) {
        this.wallOfFameResponse = wallOfFameResponse;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private Best_team best_team;

        private Best_consultant best_consultant;

        public Best_team getBest_team ()
        {
            return best_team;
        }

        public void setBest_team (Best_team best_team)
        {
            this.best_team = best_team;
        }

        public Best_consultant getBest_consultant ()
        {
            return best_consultant;
        }

        public void setBest_consultant (Best_consultant best_consultant)
        {
            this.best_consultant = best_consultant;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [best_team = "+best_team+", best_consultant = "+best_consultant+"]";
        }
    }
    public class Best_consultant
    {
        private String title;

        private String month;

        private ArrayList<Best_dses> best_dses;

        public String getTitle ()
        {
            return title;
        }

        public void setTitle (String title)
        {
            this.title = title;
        }

        public String getMonth ()
        {
            return month;
        }

        public void setMonth (String month)
        {
            this.month = month;
        }

        public ArrayList<Best_dses> getBest_dses() {
            return best_dses;
        }

        public void setBest_dses(ArrayList<Best_dses> best_dses) {
            this.best_dses = best_dses;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [title = "+title+", month = "+month+", best_dses = "+best_dses+"]";
        }
    }

    public class Best_dses
    {
        private String category;

        private int category_id;

        private String photo_url;

        private String name;

        public String getCategory ()
        {
            return category;
        }

        public void setCategory (String category)
        {
            this.category = category;
        }

        public String getPhoto_url ()
        {
            return photo_url;
        }

        public void setPhoto_url (String photo_url)
        {
            this.photo_url = photo_url;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [category = "+category+", photo_url = "+photo_url+", name = "+name+"]";
        }
    }

    public class Best_team
    {
        private String title;

        private String team_leader_id;

        private String team_name;

        private String month;

        private ArrayList<Team_users> team_users;

        private String team_leader_name;

        private String team_leader_photo_url;

        private String team_id;

        public String getTitle ()
        {
            return title;
        }

        public void setTitle (String title)
        {
            this.title = title;
        }

        public String getTeam_leader_id ()
        {
            return team_leader_id;
        }

        public void setTeam_leader_id (String team_leader_id)
        {
            this.team_leader_id = team_leader_id;
        }

        public String getTeam_name ()
        {
            return team_name;
        }

        public void setTeam_name (String team_name)
        {
            this.team_name = team_name;
        }

        public String getMonth ()
        {
            return month;
        }

        public void setMonth (String month)
        {
            this.month = month;
        }

        public ArrayList<Team_users> getTeam_users() {
            return team_users;
        }

        public void setTeam_users(ArrayList<Team_users> team_users) {
            this.team_users = team_users;
        }

        public String getTeam_leader_name ()
        {
            return team_leader_name;
        }

        public void setTeam_leader_name (String team_leader_name)
        {
            this.team_leader_name = team_leader_name;
        }

        public String getTeam_leader_photo_url ()
        {
            return team_leader_photo_url;
        }

        public void setTeam_leader_photo_url (String team_leader_photo_url)
        {
            this.team_leader_photo_url = team_leader_photo_url;
        }

        public String getTeam_id ()
        {
            return team_id;
        }

        public void setTeam_id (String team_id)
        {
            this.team_id = team_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [title = "+title+", team_leader_id = "+team_leader_id+", team_name = "+team_name+", month = "+month+", team_users = "+team_users+", team_leader_name = "+team_leader_name+", team_leader_photo_url = "+team_leader_photo_url+", team_id = "+team_id+"]";
        }
    }

    public class Team_users
    {
        private String user_photo_url;

        private String user_id;

        private String user_name;

        public String getUser_photo_url ()
        {
            return user_photo_url;
        }

        public void setUser_photo_url (String user_photo_url)
        {
            this.user_photo_url = user_photo_url;
        }

        public String getUser_id ()
        {
            return user_id;
        }

        public void setUser_id (String user_id)
        {
            this.user_id = user_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [user_photo_url = "+user_photo_url+", user_id = "+user_id+", user_name = "+user_name+"]";
        }
    }

    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }
}
