import React, { Component } from "react";
import {
  View,
  Text,
  ActivityIndicator,
  ScrollView,
  NativeModules,
  DeviceEventEmitter,
  Platform,
  TouchableOpacity
} from "react-native";
import Geolocation from '@react-native-community/geolocation';
import Button from "../../components/uielements/Button";
import MapView from "react-native-maps";
import { Marker } from "react-native-maps";
import Toast, {DURATION} from 'react-native-easy-toast';
import RestClient from "../../network/rest_client";
import styles from "./OtpPicker.styles";
import OTPInput from "../../components/uielements/OTPInput";
import Loader from "../../components/uielements/Loader";
import {TaskConstants} from '../../utils/Constants'


export default class OtpPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      remarks: null,
      mapRegion: null,
      lastLat: null,
      lastLong: null,
      currentLocationHolder: "Getting your location, please wait",
      addressFetchingFromLatLong: true,
      isReverseAddressValid: false,
      otp: "",
      loading: false,
      activityId:this.props.info.activityId,
      leadId:this.props.info.leadId,
      mobileNumber:null,
      otpSent:false,
    };
  }
  
  componentDidMount() {
    this.requestOtp();
    if (Platform.OS == "android") {
        NativeModules.ReactNativeToAndroid.componentDidMount("FORM_RENDERING");
    }
      if (Platform.OS == "ios") {
        Geolocation.getCurrentPosition(
          position => {
            let region = {
              latitude: parseFloat(position.coords.latitude),
              longitude: parseFloat(position.coords.longitude),
              latitudeDelta: 0.00922 * 1.5,
              longitudeDelta: 0.00421 * 1.5
            }
            this.onRegionChange(region, region.latitude, region.longitude);
            this.requestAddressFromLatLong(region.latitude + ',' + region.longitude)
          },
          error => this.showAlert('Error Getting the location, please enable the permission'),
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
      }
      else {
        this.subscription = DeviceEventEmitter.addListener(
          "onGettingLocationFormRendering",
          function (e: Event) {
            if (e) {
              let region = {
                latitude: parseFloat(e.LATITUDE),
                longitude: parseFloat(e.LONGITUDE),
                latitudeDelta: 0.00922 * 1.5,
                longitudeDelta: 0.00421 * 1.5
              };
              this.onRegionChange(region, region.latitude, region.longitude);
              this.requestAddressFromLatLong(
                region.latitude + "," + region.longitude
              );
            }
          }.bind(this)  
        );
      }
      this._mounted = true
  }
  componentWillUnmount() {
    if (Platform.OS == "android") {
        NativeModules.ReactNativeToAndroid.componentWillUnmount("FORM_RENDERING");
      }
      this._mounted = false
  }
  onRegionChange(region, lastLat, lastLong) {
    this.setState({
      mapRegion: region,
      // If there are no new values set the current ones
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong
    });
  }
  
  requestAddressFromLatLong(latlong) {
    this.setState({
      currentLocationHolder: "Getting your location, please wait",
      addressFetchingFromLatLong: true,
      isReverseAddressValid: false
    });
    let params = {
      appId: this.props.info.HERE_MAP_APP_ID,
      appCode: this.props.info.HERE_MAP_APP_CODE,
      CSVLatLong: latlong
    };
    new RestClient()
      .getReverseGeoCode(params)
      .then(data => {
        if(!this._mounted) {
            return false;
        }
        if (
          data &&
          data.Response &&
          data.Response.View &&
          data.Response.View.length > 0 &&
          data.Response.View[0] &&
          data.Response.View[0].Result &&
          data.Response.View[0].Result.length > 0 &&
          data.Response.View[0].Result[0] &&
          data.Response.View[0].Result[0].Location &&
          data.Response.View[0].Result[0].Location.Address &&
          data.Response.View[0].Result[0].Location.Address.Label
        ) {
          let currentLocationHolder =
            data.Response.View[0].Result[0].Location.Address;
          this.setState({
            currentLocationHolder:
              currentLocationHolder.Label +
              ", " +
              currentLocationHolder.PostalCode,
            addressFetchingFromLatLong: false,
            isReverseAddressValid: true
          });
        } else {
          this.setState({
            currentLocationHolder: "Failed to get the address, Tap to retry",
            addressFetchingFromLatLong: false
          });
        }
      })
      .catch(error => {
        this.setState({
          currentLocationHolder: "Failed to get the address, Tap to retry",
          addressFetchingFromLatLong: false
        });
      });
  }
  callPnt() {
    this.props.navigation.push("PlanNextTask", {should_render_post_form:true, 
      post_form_object:this.props.navigation.getParam('post_form_object'), 
      previousFormAnswers:this.props.navigation.getParam('previousFormAnswers')})
  }
  
  validateOtp() {
    let {leadId, otp } = this.state;
    this.setState({ loading: true });
    new RestClient()
      .validatePntOtp({leadId, otp})
      .then(data => {
        if(!this._mounted) {
            return false;
        }
        if (data) {
          if (data.statusCode == "2002" && data.result) {
            this.setState({ loading: false, validated:true});
           //callback
            this.callPnt();
          } else if (data.message) {
            this.setState({ loading: false });
            this.showAlert(data.message);
          } else {
            this.setState({ loading: false });
            this.showAlert("Failed to verify OTP! Try Again");
          }
        } else {
          this.setState({ loading: false });
          this.showAlert("Failed to verify OTP! Try Again");
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        this.showAlert("Failed to verify OTP! Try Again");
      });
  }
 
  requestOtp() {
    let { activityId, leadId } = this.state;
    this.setState({ loading: true });
    new RestClient()
      .getPntOtp(activityId, {lead_id:leadId})
      .then(data => {
        if(!this._mounted) {
            return false;
        }
        if (data) {
          if (data.statusCode == "2002" && data.result && data.result.primary_number) {
            this.setState({ loading: false, otpSent:true, mobileNumber:data.result.primary_number});
            this.showAlert("OTP has been sent to " + data.result.primary_number);
          } else if (data.message) {
            this.setState({ loading: false });
            this.showAlert(data.message);
          } else {
            this.setState({ loading: false });
            this.showAlert("Failed to send OTP! Try Again");
          }
        } else {
          this.setState({ loading: false });
          this.showAlert("Failed to send OTP! Try Again");
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        this.showAlert("Failed to send OTP! Try Again");
      });
  }
  showAlert(message) {
    try{
        console.log("Alert message:"+message)
        this.refs.toast.show(message);
    }
    catch(e) {

    }
  }

  close() {
    
  }
 
  getActivityTitle(activityId) {
    switch (activityId) {
        case TaskConstants.HOME_VISIT.id:
            return "Home Visit done at,";
        case TaskConstants.SHOWROOM_VISIT.id:
            return "Showroom Visit done at,";
        case TaskConstants.TEST_DRIVE_HOME.id:
            return "Test Drive Home taken at,";
        case TaskConstants.TEST_DRIVE_SHOWROOM.id:
            return "Test Drive Showroom taken at,";
    }
    return "Task taken at,";
  }

  mainAction() {
    if(this.state.otpSent) {
        if(this.state.validated) {
          this.callPnt();
        }
        else if(this.state.otp && this.state.otp.length==4) {
            this.validateOtp();
        }
        else {
            this.showAlert("Please enter the otp")
        }
    }
    else {
        this.requestOtp();
    }
  }

  render() {
    let { currentLocationHolder, mobileNumber, activityId, otpSent, isReverseAddressValid } = this.state;
    return (
      <View style={[styles.main, { padding: 0, paddingBottom: 20 }]}>
        <ScrollView>
          <View style={styles.main}>
            <OTPInput
                title = {mobileNumber?"OTP has been sent to "+mobileNumber:"Sending otp.."}
                onOTPEntered={otp => {
                    this.setState({ otp });
                }}
            />



            <View style={styles.mapViewHolder}>
              <Text style={[styles.title,{textAlign:'left', padding:10}]}>{this.getActivityTitle(activityId)}</Text>
              <View style={{ flexDirection: "row" }}>
                {this.state.addressFetchingFromLatLong && (
                  <ActivityIndicator
                    style={{ marginLeft: 10 }}
                    size="small"
                    color="#007fff"
                  />
                )}
                <TouchableOpacity
                disabled = {isReverseAddressValid}
                onPress = {()=> {
                    this.requestAddressFromLatLong();
                }}
                >
                    <Text style={styles.mapViewLocation}>
                    {currentLocationHolder}
                    </Text>
                </TouchableOpacity>
              </View>
              <MapView
                liteMode={true}
                style={styles.mapView}
                region={this.state.mapRegion}
              >
                {this.state.lastLat && (
                  <Marker
                    coordinate={{
                      latitude: this.state.lastLat,
                      longitude: this.state.lastLong
                    }}
                  />
                )}
              </MapView>
            </View>
          </View>
        </ScrollView>
      
        <Button onPress={() => this.mainAction()} title={otpSent?"Plan Next Task":"Request Otp"} />
        {this.state.loading && <Loader lightMode={true} isLoading={this.state.loading} />}
        <Toast
            ref="toast"
            position='top'
            style={{backgroundColor:'red'}}
            fadeInDuration={750}
            fadeOutDuration={750}
            opacity={0.8}
            textStyle={{color:'white'}}
        />
      </View>
    );
  }
}

String.prototype.isEmpty = function () {
  return this.length === 0 || !this.trim();
};
