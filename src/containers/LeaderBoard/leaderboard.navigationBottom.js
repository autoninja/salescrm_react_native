import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import LeaderBoardTeams from '../../components/leader_board_teams';
import LeaderBoardConsultants from '../../components/leader_board_consultants';
import LeaderBoardWallOfFame from '../../components/leader_board_wall_of_fame';

const LeaderBoardMain =  createBottomTabNavigator({
    Team : {screen : LeaderBoardTeams,
        /* navigationOptions: ({ navigation }) => ({

         })*/

    },
    Consultants : {screen : LeaderBoardConsultants},
    Wall : {screen : LeaderBoardWallOfFame},
},{
    tabBarOptions:{
        showIcon:false,
        style: {
            alignItems: 'center'
        },
        labelStyle :{
            fontWeight: 'bold',
            fontSize : 18
        }
    }
});

export default LeaderBoardMain;