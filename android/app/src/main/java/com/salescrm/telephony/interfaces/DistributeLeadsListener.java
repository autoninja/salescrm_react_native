package com.salescrm.telephony.interfaces;

import android.text.Editable;

import com.salescrm.telephony.model.TransferEnquiriesModel;

/**
 * Created by bharath on 28/8/17.
 */

public interface DistributeLeadsListener{
    void onChangeDistributeLeads(int position, TransferEnquiriesModel.Item item);
}
