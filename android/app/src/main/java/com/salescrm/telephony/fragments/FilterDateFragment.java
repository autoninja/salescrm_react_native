package com.salescrm.telephony.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Ravindra P on 03-06-2016.
 */
public class FilterDateFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final Integer FILTER_DATE_POS = 7;
    FilterSelectionHolder selectionHolder;
    private View rootView;
    private TextView tvFilterCloseDateFrom, tvFilterCloseDateTo, tvFilterEnqDateFrom, tvFilterEnqDateTo;
    private SparseArray<String> selectedValues;
    private TextView[] tvDates;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.filter_date_fragment, container, false);
        selectionHolder = FilterSelectionHolder.getInstance();
        tvFilterCloseDateFrom = (TextView) rootView.findViewById(R.id.tv_filter_close_date_from);
        tvFilterCloseDateTo = (TextView) rootView.findViewById(R.id.tv_filter_close_date_to);
        tvFilterEnqDateFrom = (TextView) rootView.findViewById(R.id.tv_filter_enq_date_from);
        tvFilterEnqDateTo = (TextView) rootView.findViewById(R.id.tv_filter_enq_date_to);

        tvDates = new TextView[]{tvFilterCloseDateFrom, tvFilterCloseDateTo, tvFilterEnqDateFrom, tvFilterEnqDateTo};

        ///Onclick listener
        tvFilterCloseDateFrom.setOnClickListener(this);
        tvFilterCloseDateTo.setOnClickListener(this);
        tvFilterEnqDateFrom.setOnClickListener(this);
        tvFilterEnqDateTo.setOnClickListener(this);

        //Saved data
        selectedValues = selectionHolder.getMapData(FILTER_DATE_POS);
        if (selectedValues == null) {
            selectedValues = new SparseArray<String>();
        } else {
            setDateValues();
        }

        return rootView;

    }

    private void setDateValues() {
        for (int i = 0; i < selectedValues.size(); i++) {
            System.out.println("Val:" + selectedValues.get(selectedValues.keyAt(i)));
            Calendar calendar = Util.getCalender(selectedValues.get(selectedValues.keyAt(i)));
            tvDates[selectedValues.keyAt(i)].setText(String.format(Locale.getDefault(), "%d %s %d", calendar.get(Calendar.DAY_OF_MONTH), calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), calendar.get(Calendar.YEAR)));

        }

    }

    @Override
    public void onClick(View v) {
        Util.showDatePicker(FilterDateFragment.this, getActivity(), Integer.parseInt(v.getTag().toString()), WSConstants.TYPE_DATE_PICKER.FILTER_DATE);

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);

        if (Integer.parseInt(view.getTag()) == 0 || Integer.parseInt(view.getTag()) == 2){
            calendar.set(Calendar.HOUR_OF_DAY,0);
            calendar.set(Calendar.MINUTE,0);
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MILLISECOND,0);
        }else{
            calendar.set(Calendar.HOUR_OF_DAY,23);
            calendar.set(Calendar.MINUTE,59);
            calendar.set(Calendar.SECOND,59);
            calendar.set(Calendar.MILLISECOND,0);
        }
        // textView.setTag(R.id.autoninja_date, calendar.getTime().toString());
        selectedValues.put(Integer.parseInt(view.getTag()),String.valueOf(calendar.getTimeInMillis()));
        selectionHolder.setMapData(FILTER_DATE_POS, selectedValues);
        setDateValues();

    }
}
