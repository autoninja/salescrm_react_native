import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback
} from 'react-native'
import EditLeadDetail from './edit_lead_detail'
import { ScrollView } from 'react-native-gesture-handler';
//import Modal from "react-native-modal";
import RestClient from '../../network/rest_client'
import Loader from "../../components/uielements/Loader";
import Toast, { DURATION } from 'react-native-easy-toast'
//import UserInfoService from '../../storage/user_info_service'


const styles = StyleSheet.create({
    top_view: {
        flex: 1,
        alignItems: 'center',
    },
    lead_detail_view: {
        margin: 5,
        alignSelf: 'stretch',
    },
    customer_detail_view: {
        margin: 5,
        alignSelf: 'stretch',
    },
    lead_detail: {
        padding: 10,
        backgroundColor: '#fff'
    },
    heading: {
        fontSize: 16,
        color: '#000000',
        fontWeight: 'bold',
        margin: 5
    },
    title: {
        fontSize: 16,
        color: '#000000',
        margin: 5
    },
    grey_line: {
        height: 0.5,
        backgroundColor: "#9a9a9a",
        marginBottom: 10,
        marginTop: 10
    },
    grey_line_inner: {
        height: 0.5,
        backgroundColor: "#9a9a9a",
        marginBottom: 5,
        marginTop: 5
    },
    save: {
        backgroundColor: "#007fff"
    },
    cancel: {
        backgroundColor: "#808080"
    },
    mobile_nos_view: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        padding: 5
    },
    email_view: {
        flexDirection: 'column',
        alignItems: 'flex-start'
    }
})

const customerTypeArray = [{ "id": 1, "text": "Corporate(Individual)" }, { "id": 2, "text": "Individual" },
{ "id": 3, "text": "Government" }, { "id": 4, "text": "Corporate(Company)" },
{ "id": 5, "text": "Fleet(Individual)" }, { "id": 6, "text": "Fleet(Company)" }]
export default class C360Details extends Component {
    constructor(props) {
        super(props);
        //console.log("leadDetails<>><><>><> ", this.props.leadDetails);
        this.state = {
            leadDetailMain: this.props.leadDetails,
            openEditLeadDetail: false,
            edit_id: "",
            loading: false,
            editedContactInfo: { "id": '', "value": '' },
        };
    }

    _editLeadDetail = () => {
        console.log("openEditLeadDetail")
        var { openEditLeadDetail } = this.state;
        openEditLeadDetail = !openEditLeadDetail;
        this.setState({ openEditLeadDetail, edit_id: "edit_lead" });
    }

    _editCustomerDetail = () => {
        var { openEditLeadDetail } = this.state;
        openEditLeadDetail = !openEditLeadDetail;
        this.setState({ openEditLeadDetail, edit_id: "edit_customer" });
    }

    _addMobileNumbers = () => {
        var { openEditLeadDetail } = this.state;
        openEditLeadDetail = !openEditLeadDetail;
        this.setState({ openEditLeadDetail, edit_id: "add_number" });
    }

    _addEmailIds = () => {
        var { openEditLeadDetail } = this.state;
        openEditLeadDetail = !openEditLeadDetail;
        this.setState({ openEditLeadDetail, edit_id: "add_email" });
    }

    _residenceAddress = () => {
        var { openEditLeadDetail } = this.state;
        openEditLeadDetail = !openEditLeadDetail;
        this.setState({ openEditLeadDetail, edit_id: "edit_residence_address" });
    }

    _officeAddress = () => {
        var { openEditLeadDetail } = this.state;
        openEditLeadDetail = !openEditLeadDetail;
        this.setState({ openEditLeadDetail, edit_id: "edit_office_address" });
    }

    _changePrimaryEmail = (editedData, index) => {
        let { leadDetailMain } = this.state;
        this.setState({ loading: true });
        let formData = {
            "lead_id": leadDetailMain.lead_id,
            "lead_last_updated": leadDetailMain.lead_last_updated,
            "mail_id": editedData
        }

        new RestClient().changePrimaryEmail(formData)
            .then(data => {
                if (data) {
                    //console.log("saveLeadEditedData", JSON.stringify(data))
                    if (data.statusCode == "2002" && data.result) {
                        //console.log("saveLeadEditedData", JSON.stringify(data))
                        leadDetailMain.lead_last_updated = data.result.lead_last_updated;
                        var temp_email = leadDetailMain.email_ids[index];
                        //console.log("temp_email", temp_email)
                        leadDetailMain.email_ids[0].status = "SECONDARY"
                        temp_email.status = "PRIMARY"
                        leadDetailMain.email_ids.splice(index, 1)
                        leadDetailMain.email_ids.splice(0, 0, temp_email);
                        // for (var i = 0; i < leadDetailMain.email_ids.length; i++) {
                        //     if (editedData == leadDetailMain.email_ids[i].id) {
                        //         leadDetailMain.email_ids[i].status = "PRIMARY"
                        //     } else {
                        //         leadDetailMain.email_ids[i].status = "SECONDARY"
                        //     }
                        // }

                        this.setState({ loading: false, leadDetailMain });

                        //this.showAlert("Booking successfull");
                    } else if (data.message) {
                        //console.log("message", message)
                        this.setState({ loading: false });
                        //this.showAlert(data.message);
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                } else {
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                }
            })
            .catch(error => {
                console.error(error);
                this.setState({ loading: false });
                //this.showAlert("Failed to book bike! Try Again");
            });
    }

    _changePrimaryMobile = (editedData, index) => {
        let { leadDetailMain } = this.state;
        this.setState({ loading: true });
        let formData = {
            "lead_id": leadDetailMain.lead_id,
            "lead_last_updated": leadDetailMain.lead_last_updated,
            "mob_id": editedData
        }
        new RestClient().changePrimaryMobile(formData)
            .then(data => {
                if (data) {
                    //console.log("saveLeadEditedData", JSON.stringify(data))
                    if (data.statusCode == "2002" && data.result) {
                        //console.log("saveLeadEditedData", JSON.stringify(data))
                        leadDetailMain.lead_last_updated = data.result.lead_last_updated;
                        let temp_number = leadDetailMain.mobile_nos[index];
                        temp_number.status = "PRIMARY"
                        leadDetailMain.mobile_nos[0].status = "SECONDARY"
                        leadDetailMain.mobile_nos.splice(index, 1)
                        leadDetailMain.mobile_nos.splice(0, 0, temp_number);
                        // for (var i = 0; i < leadDetailMain.mobile_nos.length; i++) {
                        //     if (editedData == leadDetailMain.mobile_nos[i].id) {
                        //         leadDetailMain.mobile_nos[i].status = "PRIMARY"
                        //     } else {
                        //         leadDetailMain.mobile_nos[i].status = "SECONDARY"
                        //     }
                        // }

                        this.setState({ loading: false, leadDetailMain });

                        //this.showAlert("Booking successfull");
                    } else if (data.message) {
                        //console.log("message", message)
                        this.setState({ loading: false });
                        //this.showAlert(data.message);
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                } else {
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                }
            })
            .catch(error => {
                console.error(error);
                this.setState({ loading: false });
                //this.showAlert("Failed to book bike! Try Again");
            });
    }

    _editMobileNumber = (editedInfo) => {
        var { openEditLeadDetail } = this.state;
        var { editedContactInfo } = this.state;
        editedContactInfo.id = editedInfo.id;
        editedContactInfo.value = editedInfo.value;
        openEditLeadDetail = !openEditLeadDetail;

        this.setState({ openEditLeadDetail, edit_id: "edit_number", editedContactInfo });
    }

    _editEmailId = (editedInfo) => {
        var { openEditLeadDetail } = this.state;
        var { editedContactInfo } = this.state;
        editedContactInfo.id = editedInfo.id;
        editedContactInfo.value = editedInfo.value;
        openEditLeadDetail = !openEditLeadDetail;

        this.setState({ openEditLeadDetail, edit_id: "edit_email", editedContactInfo });
    }

    _deleteMobileNumber = (mapping_id) => {
        let { leadDetailMain } = this.state;
        this.setState({ loading: true });
        let formData = {
            "lead_id": leadDetailMain.lead_id,
            "lead_last_updated": leadDetailMain.lead_last_updated,
            phone: {
                "lead_phone_mapping_id": mapping_id
            }
        }

        new RestClient().deleteMobileNumber(formData)
            .then(data => {
                if (data) {
                    //console.log("saveLeadEditedData", JSON.stringify(data))
                    if (data.statusCode == "2002" && data.result) {
                        //console.log("saveLeadEditedData", JSON.stringify(data))
                        leadDetailMain.lead_last_updated = data.result.lead_last_updated;

                        for (var i = 0; i < leadDetailMain.mobile_nos.length; i++) {
                            if (leadDetailMain.mobile_nos[i].lead_phone_mapping_id == mapping_id) {
                                leadDetailMain.mobile_nos.splice(i, 1);
                            }
                        }

                        this.setState({ loading: false, leadDetailMain });

                        //this.showAlert("Booking successfull");
                    } else if (data.message) {
                        //console.log("message", message)
                        this.setState({ loading: false });
                        //this.showAlert(data.message);
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                } else {
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                }
            })
            .catch(error => {
                console.error(error);
                this.setState({ loading: false });
                //this.showAlert("Failed to book bike! Try Again");
            });
    }

    _deleteEmailId = (mapping_id) => {
        let { leadDetailMain } = this.state;
        this.setState({ loading: true });
        let formData = {
            "lead_id": leadDetailMain.lead_id,
            "lead_last_updated": leadDetailMain.lead_last_updated,
            email: {
                "lead_email_mapping_id": mapping_id
            }
        }

        new RestClient().deleteEmailAddress(formData)
            .then(data => {
                if (data) {
                    //console.log("saveLeadEditedData", JSON.stringify(data))
                    if (data.statusCode == "2002" && data.result) {
                        //console.log("saveLeadEditedData", JSON.stringify(data))
                        leadDetailMain.lead_last_updated = data.result.lead_last_updated;

                        for (var i = 0; i < leadDetailMain.email_ids.length; i++) {
                            if (leadDetailMain.email_ids[i].lead_email_mapping_id == mapping_id) {
                                leadDetailMain.email_ids.splice(i, 1);
                            }
                        }

                        this.setState({ loading: false, leadDetailMain });

                        //this.showAlert("Booking successfull");
                    } else if (data.message) {
                        //console.log("message", message)
                        this.setState({ loading: false });
                        //this.showAlert(data.message);
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                } else {
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                }
            })
            .catch(error => {
                console.error(error);
                this.setState({ loading: false });
                //this.showAlert("Failed to book bike! Try Again");
            });
    }

    getCustomerTypeId(customerType) {
        for (var i = 0; i < customerTypeArray.length; i++) {
            if (customerType == customerTypeArray[i].text) {
                return customerTypeArray[i].id;
            }
        }

    }

    onSave = (editId, editedData) => {
        //console.log("editIdeditId", editId, JSON.stringify(editedData))
        let { leadDetailMain } = this.state;
        this.setState({ loading: true });

        if (editId == "edit_lead") {

            let formData = {
                "lead_id": leadDetailMain.lead_id,
                "lead_last_updated": leadDetailMain.lead_last_updated,
                data: {
                    "location_id": leadDetailMain.lead_location.id,
                    "dse_id": leadDetailMain.dse_details.dse_id,
                    "lead_source_id": leadDetailMain.lead_source_id,
                    "buyer_type_id": leadDetailMain.buyer_type_id,
                    "expected_closing_date": editedData.close_date,
                    "mode_of_payment": editedData.mode_of_payment,
                    "customer_type": this.getCustomerTypeId(editedData.type_of_customer)
                }
            }

            new RestClient().saveLeadEditedData(formData)
                .then(data => {
                    if (data) {
                        if (data.statusCode == "2002" && data.result) {
                            //console.log("saveLeadEditedData", JSON.stringify(data))
                            leadDetailMain.lead_last_updated = data.result.lead_last_updated;
                            leadDetailMain.close_date = editedData.close_date;
                            leadDetailMain.mode_of_payment = editedData.mode_of_payment;
                            leadDetailMain.type_of_customer = editedData.type_of_customer;

                            this.setState({ loading: false, leadDetailMain });

                            //this.showAlert("Booking successfull");
                        } else if (data.message) {
                            //console.log("message", message)
                            this.setState({ loading: false });
                            //this.showAlert(data.message);
                        } else {
                            this.setState({ loading: false });
                            //this.showAlert("Failed to book bike! Try Again");
                        }
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                })
                .catch(error => {
                    console.error(error);
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                });
        }

        if (editId == "edit_customer" || editId == "edit_residence_address" || editId == "edit_office_address") {

            let formData = {
                "lead_id": leadDetailMain.lead_id,
                "lead_last_updated": leadDetailMain.lead_last_updated,
                edited_client_info: {
                    "first_name": editedData.first_name,
                    "last_name": editedData.last_name,
                    "address": editedData.full_address,
                    "pin_code": editedData.pin_code,
                    "locality": editedData.locality,
                    "city": editedData.city,
                    "state": editedData.residence_state,
                    "office_address": editedData.office_address,
                    "office_pin_code": editedData.office_pincode,
                    "office_locality": editedData.office_locality,
                    "office_city": editedData.office_city,
                    "company_name": editedData.company_name,
                    "occupation": editedData.occupation,
                    "gender": leadDetailMain.gender,
                    "title": editedData.title,
                    "customer_age": editedData.age
                }
            }

            new RestClient().saveCustomerEditedData(formData)
                .then(data => {
                    if (data) {
                        if (data.statusCode == "2002" && data.result) {
                            //console.log("saveLeadEditedData", JSON.stringify(data))
                            leadDetailMain.lead_last_updated = data.result.lead_last_updated;
                            leadDetailMain.first_name = editedData.first_name;
                            leadDetailMain.last_name = editedData.last_name;
                            leadDetailMain.age = editedData.age;
                            leadDetailMain.pin_code = editedData.pin_code;
                            leadDetailMain.full_address = editedData.full_address;
                            leadDetailMain.locality = editedData.locality;
                            leadDetailMain.city = editedData.city;
                            leadDetailMain.state = editedData.residence_state;
                            leadDetailMain.office_pin_code = editedData.office_pincode;
                            leadDetailMain.office_address = editedData.office_address;
                            leadDetailMain.office_locality = editedData.office_locality;
                            leadDetailMain.office_city = editedData.office_city;
                            leadDetailMain.state = editedData.office_state;
                            leadDetailMain.company_name = editedData.company_name;
                            leadDetailMain.occupation = editedData.occupation;

                            this.setState({ loading: false, leadDetailMain });

                            //this.showAlert("Booking successfull");
                        } else if (data.message) {
                            //console.log("message", message)
                            this.setState({ loading: false });
                            //this.showAlert(data.message);
                        } else {
                            this.setState({ loading: false });
                            //this.showAlert("Failed to book bike! Try Again");
                        }
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                })
                .catch(error => {
                    console.error(error);
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                });
        }

        if (editId == "add_number") {
            this.setState({ loading: true });
            let formData = {
                "lead_id": leadDetailMain.lead_id,
                "lead_last_updated": leadDetailMain.lead_last_updated,
                "newMob": editedData
            }

            new RestClient().addMobileNumber(formData)
                .then(data => {
                    if (data) {
                        //console.log("saveLeadEditedData", JSON.stringify(data))
                        if (data.statusCode == "2002" && data.result) {
                            //console.log("saveLeadEditedData", JSON.stringify(data))
                            leadDetailMain.lead_last_updated = data.result.lead_last_updated;
                            leadDetailMain.mobile_nos = data.result.mobileData;
                            leadDetailMain.mobile_nos.sort(compare)

                            this.setState({ loading: false, leadDetailMain });

                            this.showAlert("Number Added");
                        } else if (data.message) {
                            //console.log("message", message)
                            this.setState({ loading: false });
                            //this.showAlert(data.message);
                        } else {
                            this.setState({ loading: false });
                            //this.showAlert("Failed to book bike! Try Again");
                        }
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                })
                .catch(error => {
                    console.error(error);
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                });
        }

        if (editId == "add_email") {
            this.setState({ loading: true });
            let formData = {
                "lead_id": leadDetailMain.lead_id,
                "lead_last_updated": leadDetailMain.lead_last_updated,
                "new_mail": editedData
            }

            new RestClient().addEmailId(formData)
                .then(data => {
                    if (data) {
                        if (data.statusCode == "2002" && data.result) {
                            //console.log("saveLeadEditedData", JSON.stringify(data))
                            leadDetailMain.lead_last_updated = data.result.lead_last_updated;
                            leadDetailMain.email_ids = data.result.emailData;

                            this.setState({ loading: false, leadDetailMain });

                            //this.showAlert("Booking successfull");
                        } else if (data.message) {
                            //console.log("message", message)
                            this.setState({ loading: false });
                            //this.showAlert(data.message);
                        } else {
                            this.setState({ loading: false });
                            //this.showAlert("Failed to book bike! Try Again");
                        }
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                })
                .catch(error => {
                    console.error(error);
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                });
        }

        // Comparing status string to brig primary array in fisrt position
        function compare(a, b) {
            if (a.status < b.status) {
                return -1;
            }
            if (a.status > b.status) {
                return 1;
            }
            return 0;
        }

        if (editId == "edit_email") {
            this.setState({ loading: true });
            let formData = {
                "lead_id": leadDetailMain.lead_id,
                "lead_last_updated": leadDetailMain.lead_last_updated,
                "mail": editedData.value,
                "object": editedData.id
            }

            new RestClient().editEmailAddress(formData)
                .then(data => {
                    if (data) {
                        if (data.statusCode == "2002" && data.result) {
                            //console.log("saveLeadEditedData", JSON.stringify(data))
                            leadDetailMain.lead_last_updated = data.result.lead_last_updated;
                            leadDetailMain.email_ids = data.result.emailData;
                            leadDetailMain.email_ids.sort(
                                compare
                            )

                            this.setState({ loading: false, leadDetailMain });

                            //this.showAlert("Booking successfull");
                        } else if (data.message) {
                            //console.log("message", message)
                            this.setState({ loading: false });
                            //this.showAlert(data.message);
                        } else {
                            this.setState({ loading: false });
                            //this.showAlert("Failed to book bike! Try Again");
                        }
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                })
                .catch(error => {
                    console.error(error);
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                });
        }

        if (editId == "edit_number") {
            this.setState({ loading: true });
            let formData = {
                "lead_id": leadDetailMain.lead_id,
                "lead_last_updated": leadDetailMain.lead_last_updated,
                "phone": editedData.value,
                "object": editedData.id
            }

            new RestClient().editMobileNumber(formData)
                .then(data => {
                    if (data) {
                        if (data.statusCode == "2002" && data.result) {
                            //console.log("saveLeadEditedData", JSON.stringify(data))
                            leadDetailMain.lead_last_updated = data.result.lead_last_updated;
                            leadDetailMain.mobile_nos = data.result.mobileData;

                            this.setState({ loading: false, leadDetailMain });

                            //this.showAlert("Booking successfull");
                        } else if (data.message) {
                            //console.log("message", message)
                            this.setState({ loading: false });
                            //this.showAlert(data.message);
                        } else {
                            this.setState({ loading: false });
                            //this.showAlert("Failed to book bike! Try Again");
                        }
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                })
                .catch(error => {
                    console.error(error);
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                });
        }

        this.setState({ openEditLeadDetail: false });
    }

    onCancel = () => {
        this.setState({ openEditLeadDetail: false });
    }

    showAlert(alert) {
        this.refs.alert.show(alert);
    }

    render() {
        let details = this.state.leadDetailMain;

        return (
            <View style={styles.top_view}>

                <Toast
                    ref="alert"
                    style={{ backgroundColor: 'red' }}
                    position='top'
                    fadeInDuration={1000}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: 'white' }}
                />

                <EditLeadDetail
                    edit_id={this.state.edit_id}
                    leadDetails={this.state.leadDetailMain}
                    onSave={this.onSave}
                    onCancel={this.onCancel}
                    visible={this.state.openEditLeadDetail}
                    editedContactInfo={this.state.editedContactInfo}
                />

                <ScrollView style={{ width: "100%", padding: 5 }}>
                    <View style={styles.lead_detail_view}>
                        <Text style={{ fontSize: 16, textAlign: 'center', color: '#ffffff', marginBottom: 5, marginTop: 5 }}>Lead Details</Text>
                        <View style={styles.lead_detail}>
                            <Text style={styles.heading}>{"Lead ID - " + details.lead_id}</Text>

                            <View style={styles.grey_line} />

                            <Text style={styles.heading}>Showroom Location</Text>
                            <Text style={styles.title}>{(details.lead_location.name) ? details.lead_location.name : "---"}</Text>

                            <Text style={styles.heading}>Sales Consultant</Text>
                            <Text style={styles.title}>{(details.dse_details.dse_name) ? details.dse_details.dse_name : "---"}</Text>

                            <Text style={styles.heading}>Lead Source</Text>
                            <Text style={styles.title}>{(details.lead_source) ? details.lead_source : "---"}</Text>

                            <View style={styles.grey_line} />

                            <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={() => this._editLeadDetail()}>
                                <Image
                                    style={{ width: 24, height: 24, alignSelf: 'flex-end', marginRight: 10 }}
                                    source={require('../../images/ic_edit_blue_24dp.png')}
                                />
                            </TouchableOpacity>

                            <Text style={styles.heading}>Customer type</Text>
                            <Text style={styles.title}>{(details.type_of_customer) ? details.type_of_customer : "---"}</Text>

                            <Text style={styles.heading}>Mode of Payment</Text>
                            <Text style={styles.title}>{(details.mode_of_payment) ? details.mode_of_payment : "---"}</Text>

                            <Text style={styles.heading}>Expected buying date</Text>
                            <Text style={styles.title}>{(details.close_date) ? details.close_date : "---"}</Text>
                        </View>
                    </View>
                    <View style={styles.customer_detail_view}>
                        <Text style={{ fontSize: 16, textAlign: 'center', color: '#ffffff', marginBottom: 5, marginTop: 5 }}>Customer Details</Text>
                        <View style={styles.lead_detail}>

                            <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={() => this._editCustomerDetail()}>
                                <Image
                                    style={{ width: 24, height: 24, alignSelf: 'flex-end', marginRight: 10 }}
                                    source={require('../../images/ic_edit_blue_24dp.png')}
                                />
                            </TouchableOpacity>
                            <Text style={styles.heading}>Name</Text>
                            <Text style={styles.title}>{(details.first_name) ? details.first_name : "---"}</Text>

                            <Text style={styles.heading}>Last name</Text>
                            <Text style={styles.title}>{(details.last_name) ? details.last_name : "---"}</Text>

                            <Text style={styles.heading}>Title</Text>
                            <Text style={styles.title}>{(details.title) ? details.title : "---"}</Text>

                            <Text style={styles.heading}>Age</Text>
                            <Text style={styles.title}>{(details.age) ? details.age : "---"}</Text>

                            <View style={styles.grey_line} />

                            <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={() => this._addMobileNumbers()}>
                                <Image
                                    style={{ width: 24, height: 24, alignSelf: 'flex-end', marginRight: 10 }}
                                    source={require('../../images/add_mobile_email.png')}
                                />
                            </TouchableOpacity>

                            <Text style={styles.heading}>Mobile numbers</Text>
                            {details.mobile_nos.map((item, index) => {
                                return (<View key={index}>
                                    <View style={styles.mobile_nos_view}>
                                        <Text style={styles.title, { flex: 6 }}>{item.number}</Text>
                                        {(item && item.status == "PRIMARY") ? null :
                                            <TouchableOpacity style={{ alignSelf: 'flex-end', flex: 1 }} onPress={() => this._deleteMobileNumber(item.lead_phone_mapping_id)}>
                                                <Image
                                                    style={{ width: 20, height: 20, alignSelf: 'flex-end' }}
                                                    source={require('../../images/delete.png')}
                                                />
                                            </TouchableOpacity>}

                                        {(item && item.status == "PRIMARY") ?
                                            <TouchableOpacity style={{ alignSelf: 'flex-end', flex: 1 }} onPress={null}>
                                                <Image
                                                    style={{ width: 20, height: 20, alignSelf: 'flex-end' }}
                                                    source={require('../../images/ic_primary.png')}
                                                />
                                            </TouchableOpacity>
                                            :
                                            (Platform.OS === "ios" && Math.max(this.props.userRoles) > 4) ? <TouchableOpacity style={{ alignSelf: 'flex-end', flex: 1 }} onPress={() => this._changePrimaryMobile(item.id, index)}>
                                                <Image
                                                    style={{ width: 20, height: 20, alignSelf: 'flex-end' }}
                                                    source={require('../../images/ic_secondary.png')}
                                                />
                                            </TouchableOpacity> : null
                                        }

                                        {(item && item.status == "PRIMARY") ? null :
                                            <TouchableOpacity style={{ alignSelf: 'flex-end', flex: 1 }} onPress={() => this._editMobileNumber({ "id": item.lead_phone_mapping_id, "value": item.number })}>
                                                <Image
                                                    style={{ width: 20, height: 20, alignSelf: 'flex-end' }}
                                                    source={require('../../images/ic_edit_blue_24dp.png')}
                                                />
                                            </TouchableOpacity>}
                                    </View>
                                    {(details.mobile_nos.length - 1 == index) ? null
                                        :
                                        <View style={styles.grey_line_inner} />
                                    }
                                </View>)
                            })}

                            <View style={styles.grey_line} />

                            <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={() => this._addEmailIds()}>
                                <Image
                                    style={{ width: 24, height: 24, alignSelf: 'flex-end', marginRight: 10 }}
                                    source={require('../../images/add_mobile_email.png')}
                                />
                            </TouchableOpacity>

                            <Text style={styles.heading}>Email address</Text>
                            {details.email_ids.map((item, index) => {
                                return (<View key={index}>
                                    <View style={styles.mobile_nos_view}>
                                        <Text style={styles.title, { flex: 6 }}>{item.address}</Text>

                                        {(item && item.status == "PRIMARY") ? null
                                            : <TouchableOpacity style={{ alignSelf: 'flex-end', flex: 1 }} onPress={() => this._deleteEmailId(item.lead_email_mapping_id)}>
                                                <Image
                                                    style={{ width: 20, height: 20, alignSelf: 'flex-end' }}
                                                    source={require('../../images/delete.png')}
                                                />
                                            </TouchableOpacity>
                                        }

                                        {(item && item.status == "PRIMARY") ?
                                            <TouchableOpacity style={{ alignSelf: 'flex-end', flex: 1 }} onPress={null}>
                                                <Image
                                                    style={{ width: 20, height: 20, alignSelf: 'flex-end' }}
                                                    source={require('../../images/ic_primary.png')}
                                                />
                                            </TouchableOpacity>
                                            :
                                            //this.props.userRoles used instead of UserInfoService.getRoles()
                                            (Platform.OS === "ios" && Math.max(this.props.userRoles) > 4) ? <TouchableOpacity style={{ alignSelf: 'flex-end', flex: 1 }} onPress={() => this._changePrimaryEmail(item.id, index)}>
                                                <Image
                                                    style={{ width: 20, height: 20, alignSelf: 'flex-end' }}
                                                    source={require('../../images/ic_secondary.png')}
                                                />
                                            </TouchableOpacity> : null
                                        }
                                        {(item && item.status == "PRIMARY") ? null :
                                            <TouchableOpacity style={{ alignSelf: 'flex-end', flex: 1 }} onPress={() => this._editEmailId({ "id": item.lead_email_mapping_id, "value": item.address })}>
                                                <Image
                                                    style={{ width: 20, height: 20, alignSelf: 'flex-end' }}
                                                    source={require('../../images/ic_edit_blue_24dp.png')}
                                                />
                                            </TouchableOpacity>}
                                    </View>
                                    {(details.email_ids.length - 1 == index) ? null
                                        :
                                        <View style={styles.grey_line_inner} />
                                    }
                                </View>)
                            })}

                            <View style={styles.grey_line} />

                            <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={() => this._residenceAddress()}>
                                <Image
                                    style={{ width: 24, height: 24, alignSelf: 'flex-end', marginRight: 10 }}
                                    source={require('../../images/ic_edit_blue_24dp.png')}
                                />
                            </TouchableOpacity>
                            <Text style={styles.heading}>Residence address</Text>
                            <Text style={styles.title}>{(details.full_address) ? details.full_address : "---"}</Text>

                            <Text style={styles.heading}>Residence pincode</Text>
                            <Text style={styles.title}>{(details.pin_code) ? details.pin_code : "---"}</Text>

                            <View style={styles.grey_line} />

                            <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={() => this._officeAddress()}>
                                <Image
                                    style={{ width: 24, height: 24, alignSelf: 'flex-end', marginRight: 10 }}
                                    source={require('../../images/ic_edit_blue_24dp.png')}
                                />
                            </TouchableOpacity>
                            <Text style={styles.heading}>Company name</Text>
                            <Text style={styles.title}>{(details.company_name ? details.company_name : "---")}</Text>

                            <Text style={styles.heading}>Designation</Text>
                            <Text style={styles.title}>{(details.occupation) ? details.occupation : "---"}</Text>

                            <Text style={styles.heading}>Office address</Text>
                            <Text style={styles.title}>{(details.office_address) ? details.office_address : "---"}</Text>

                            <Text style={styles.heading}>Office pincode</Text>
                            <Text style={styles.title}>{(details.office_pin_code) ? details.office_pin_code : "---"}</Text>

                            <View style={styles.grey_line} />

                        </View>
                    </View>
                </ScrollView>
                {this.state.loading && <Loader isLoading={this.state.loading} />}
            </View>
        );
    }
}