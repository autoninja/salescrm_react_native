import React, { Component } from 'react'
import { View } from 'react-native'

export default class LayoutHr extends Component {
  render() {
    return <View style={[this.props.style,{backgroundColor:'#cccccc', height:1, width:'100%'}]}/>
  }
}
