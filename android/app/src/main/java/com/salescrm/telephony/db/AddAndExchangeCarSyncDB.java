package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 19/12/16.
 */

public class AddAndExchangeCarSyncDB extends RealmObject {
    @Index
    private String leadId;

    private String brandId;
    private String modelId;
    private String variantId;
    private String fuelTypeId;
    private String colorId;
    private int addType;
    private boolean is_synced;
    private String kms_run;
    private String reg_no;
    private String year;
    private String modelName;
    private String variantName;
    private String fuelTypeName;
    private String colorName;
    private boolean result;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getKms_run() {
        return kms_run;
    }

    public void setKms_run(String kms_run) {
        this.kms_run = kms_run;
    }

    public String getReg_no() {
        return reg_no;
    }

    public void setReg_no(String reg_no) {
        this.reg_no = reg_no;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public boolean is_synced() {
        return is_synced;
    }

    public void setIs_synced(boolean is_synced) {
        this.is_synced = is_synced;
    }

    public int getAddType() {
        return addType;
    }

    public void setAddType(int addExchangeSync) {
        this.addType = addExchangeSync;
    }

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(String fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public void setVariantName(String variantName) {
        this.variantName = variantName;
    }

    public void setFuelTypeName(String fuelTypeName) {
        this.fuelTypeName = fuelTypeName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getModelName() {
        return modelName;
    }

    public String getVariantName() {
        return variantName;
    }

    public String getFuelTypeName() {
        return fuelTypeName;
    }

    public String getColorName() {
        return colorName;
    }
}
