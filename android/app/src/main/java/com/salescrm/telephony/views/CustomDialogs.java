package com.salescrm.telephony.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.car.AllInterestedCars;
import com.salescrm.telephony.db.car.CarsDBHandler;
import com.salescrm.telephony.db.car.ExchangeCarBrandModelsDB;
import com.salescrm.telephony.db.car.ExchangeCarDetails;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.interfaces.NewCarsProgressLoader;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.service_handlers.CarsFragmentServiceHandler;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;


/**
 * Created by bannhi on 15/6/17.
 */

public class CustomDialogs implements
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener,
        com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener {

    static Activity activity;
    EditText tv_evaluation_address;
    TextView title_evaluation_address;
    String[] showRoomList;
    public static List<String> showRoomIdList = new ArrayList<String>();
    String addressType = "0";
    String selectedShowRoom = "";
    String selectedShowRoomId = "";
    AutoCompleteTextView tv_evaluation_showroom;
    static NewCarsProgressLoader mNewCarsProgressLoader;
    int clickedCount = 0;
    AlertDialog alertDialog;
    ArrayList<EditText> allBookEds = new ArrayList<EditText>();
    static Preferences pref;
    static AllInterestedCars allInterestedCar;
    static ExchangeCarDetails exchangeCarDetails;
    TextView followUpDate,followUpTime;
    TextView tv_dob;
    TextView expectedDeliveryDateTv;
    TextView expectedDeliveryDateTitle,tvRemarkTitle;
    EditText etRemarkTitle;
    TextView txt_invoice_date_value;
    TextView txt_resceduleDate;
    TextView tv_evaluation_date;
    TextView tv_evaluation_time;
    TextView tv_purchase_date_exchange;
    TextView tv_final_evaluation_schedule,tv_final_followup =null;
    String selectedFollowUpDate = "", selectedFollowUpTime=""  , selectedExpectedDeliveryDate = "", selectedInvoiceDate = "",
            selectedRescheduleDate="", selectedEvaluationDate = "", selectedEvaluationTime = "", selectedDOB="";
    public static List<String> variantIdsList = new ArrayList<String>();
    public static List<String> colorIdsList = new ArrayList<String>();
    public static List<String> fuelIdsList = new ArrayList<String>();
    public static List<String> evaluatorIdList = new ArrayList<String>();
    String selectedVariantId = "";
    String selectedColorId = "";
    String selectedFuelId = "";
    String selectedVariant = "";
    String selectedColor = "";
    String selectedFuel = "";
    String selectedExpectedPrice = "";
    String selectedMarketPrice = "";
    String selectedQuotedPrice = "";
    String selectedTotalRuns = "";
    String selectedRegNum = "";
    String selectedModel = "";
    String selectedModelId = "";
    String selectedYear = "";
    String reason = "";
    String selectedEvaluator = "";
    String selectedEvaluatorId = "";
    boolean selected = false;
    String[] carVariantNamesList;
    String[] colorsNameList;
    String[] fuelNamesList;
    String[] evaluatorNameList;
    static Realm realm;
    String bookingName = "", pan = "", aadhar = "",bokingRemarks="", gst = "";
    static CustomDialogs newInstance = new CustomDialogs();
    Spinner spinnerColor;
    boolean gstSwitch;

    public static CustomDialogs getInstance(Activity activity, AllInterestedCars allIntrdtedCar, NewCarsProgressLoader newCarsProgressLoader) {
        CustomDialogs.activity = activity;
        mNewCarsProgressLoader = newCarsProgressLoader;
        allInterestedCar = allIntrdtedCar;
        pref = Preferences.getInstance();
        pref.load((Context) activity);
        realm = Realm.getDefaultInstance();
        if (newInstance == null)
            newInstance = new CustomDialogs();
        return newInstance;
    }

    public static CustomDialogs getInstance(Activity activity, ExchangeCarDetails exCarDetails, NewCarsProgressLoader newCarsProgressLoader) {
        CustomDialogs.activity = activity;
        exchangeCarDetails = exCarDetails;
        mNewCarsProgressLoader = newCarsProgressLoader;
        pref = Preferences.getInstance();
        pref.load((Context) activity);
        realm = Realm.getDefaultInstance();
        if (newInstance == null)
            newInstance = new CustomDialogs();
        return newInstance;
    }

    private CustomDialogs() {

    }


    private void selectShowroom(){
        showRoomList = CarsDBHandler.getInstance().getShowroomsForEvaluation(realm);
        ArrayAdapter<String> showRoomsAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, showRoomList);
        tv_evaluation_showroom.setAdapter(showRoomsAdapter);
        tv_evaluation_showroom.setThreshold(0);
        if(showRoomList!=null && showRoomList.length==1){
            tv_evaluation_showroom.setText(showRoomList[0]);
            selectedShowRoom = showRoomList[0];
            selectedShowRoomId = showRoomIdList.get(0);
        }
        tv_evaluation_showroom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                showRoomList = CarsDBHandler.getInstance().getShowroomsForEvaluation(realm);
                if (showRoomList != null && showRoomList.length > 0) {
                    if (!TextUtils.isEmpty(tv_evaluation_showroom.getText())) {
                        tv_evaluation_showroom.showDropDown();
                    }
                    if (!TextUtils.isEmpty(tv_evaluation_showroom.getText())) {
                        for (int i = 0; i < showRoomList.length; i++) {
                            if (tv_evaluation_showroom.getText().toString().trim().equalsIgnoreCase(showRoomList[i])) {
                                selectedShowRoom = showRoomList[i];
                                selectedShowRoomId = showRoomIdList.get(i);
                            }
                        }
                    }
                } else {
                    Toast.makeText((Context) activity, "Please wait till we retrieve the evaluator names", Toast.LENGTH_LONG);
                }

                return false;
            }
        });
        tv_evaluation_showroom.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                showRoomList = CarsDBHandler.getInstance().getShowroomsForEvaluation(realm);
                if (showRoomList.length > 0) {
                    if (!TextUtils.isEmpty(tv_evaluation_showroom.getText())) {
                        for (int i = 0; i < showRoomList.length; i++) {
                            if (tv_evaluation_showroom.getText().toString().trim().equalsIgnoreCase(showRoomList[i])) {
                                selectedShowRoom = showRoomList[i];
                                selectedShowRoomId = showRoomIdList.get(i);
                            }
                        }
                    }
                    if (hasFocus && TextUtils.isEmpty(tv_evaluation_showroom.getText()) && tv_evaluation_showroom.getAdapter() != null) {
                        tv_evaluation_showroom.showDropDown();

                    }
                } else {
                    Toast.makeText((Context) activity, "Please wait till we retrieve the showrooms", Toast.LENGTH_LONG);
                }
            }
        });
        tv_evaluation_showroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRoomList = CarsDBHandler.getInstance().getEvaluatorNameList(realm);
                if (showRoomList.length > 0) {
                    if (!TextUtils.isEmpty(tv_evaluation_showroom.getText())) {
                        for (int i = 0; i < showRoomList.length; i++) {
                            if (tv_evaluation_showroom.getText().toString().trim().equalsIgnoreCase(showRoomList[i])) {
                                selectedShowRoom = showRoomList[i];
                                selectedShowRoomId = showRoomIdList.get(i);
                            }
                        }
                    }
                    if (TextUtils.isEmpty(tv_evaluation_showroom.getText())) {
                        tv_evaluation_showroom.showDropDown();

                    }
                } else {
                    Toast.makeText((Context) activity, "Please wait till we retrieve the showrooms", Toast.LENGTH_LONG);
                }
            }
        });

    }
    private void fillAddress(final int type) {
        final SalesCRMRealmTable salesCRMRealmTable = Realm.getDefaultInstance().where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findFirst();
         String homeAddress = salesCRMRealmTable.getResidenceAddress();
         String homePin = salesCRMRealmTable.getResidencePinCode();

        String officeAddress = salesCRMRealmTable.getOfficeAddress();
        String officePin = salesCRMRealmTable.getOfficePinCode();

        if(homeAddress==null || homeAddress.equalsIgnoreCase("null")){
            homeAddress = "";
        }
        if(homePin == null || homePin.equalsIgnoreCase("null")){
            homePin = "";
        }

        if(officeAddress==null || officeAddress.equalsIgnoreCase("null")){
            officeAddress = "";
        }
        if(officePin == null || officePin.equalsIgnoreCase("null")){
            officePin = "";
        }

        switch (type) {
            case 1:
                tv_evaluation_showroom.setVisibility(View.GONE);
                tv_evaluation_address.setVisibility(View.VISIBLE);
                title_evaluation_address.setText("Address");
                tv_evaluation_address.setText(homeAddress+homePin);
                tv_evaluation_showroom.setText("");
                break;
            case 2:
                tv_evaluation_showroom.setVisibility(View.GONE);
                tv_evaluation_address.setVisibility(View.VISIBLE);
                title_evaluation_address.setText("Address");
                tv_evaluation_address.setText(officeAddress+officePin);
                tv_evaluation_showroom.setText("");
                break;
            case 3:
                tv_evaluation_showroom.setVisibility(View.VISIBLE);
                tv_evaluation_address.setVisibility(View.GONE);
                title_evaluation_address.setText("Showroom");
                tv_evaluation_address.setText("");
                selectShowroom();
                break;
            case 4:
                tv_evaluation_showroom.setVisibility(View.GONE);
                tv_evaluation_address.setVisibility(View.VISIBLE);
                title_evaluation_address.setText("Address");
                tv_evaluation_showroom.setText("");
                break;
            default:
                tv_evaluation_showroom.setVisibility(View.GONE);
                tv_evaluation_address.setVisibility(View.VISIBLE);
                title_evaluation_address.setText("Address");
                tv_evaluation_showroom.setText("");
                break;
        }
    }
    public void showEvaluationDialog(final String leadLastUpdated) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.evaluation_form, null);
        dialogBuilder.setView(dialogView);

        final AutoCompleteTextView tv_evaluator_name = (AutoCompleteTextView) dialogView.findViewById(R.id.edt_evaluator_name);

        tv_evaluation_address = (EditText) dialogView.findViewById(R.id.edt_evaluation_address);
        title_evaluation_address = (TextView) dialogView.findViewById(R.id.txt_evaluation_address);
        tv_evaluation_showroom = (AutoCompleteTextView) dialogView.findViewById(R.id.edt_showroom_address);
        final EditText tv_evaluation_remarks = (EditText) dialogView.findViewById(R.id.edt_evaluation_remarks);
        tv_evaluation_date = (TextView) dialogView.findViewById(R.id.tv_evaluation_date);
        tv_evaluation_time = (TextView) dialogView.findViewById(R.id.tv_evaluation_time);

        final RadioGroup radioGroup = (RadioGroup) dialogView.findViewById(R.id.radio_group);
        final RadioButton radioHome = (RadioButton) dialogView.findViewById(R.id.home_radio);
        final RadioButton radioOffice = (RadioButton) dialogView.findViewById(R.id.office_radio);
        final RadioButton radioOther = (RadioButton) dialogView.findViewById(R.id.other_radio);
        final RadioButton radioShowroom = (RadioButton) dialogView.findViewById(R.id.showroom_radio);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.home_radio:
                        radioOffice.setChecked(false);
                        radioOther.setChecked(false);
                        radioShowroom.setChecked(false);
                        addressType = "1";
                        fillAddress(1);

                        break;
                    case R.id.office_radio:
                        radioHome.setChecked(false);
                        radioOther.setChecked(false);
                        radioShowroom.setChecked(false);
                        addressType = "2";
                        fillAddress(2);
                        break;
                    case R.id.other_radio:
                        radioOffice.setChecked(false);
                        radioHome.setChecked(false);
                        radioShowroom.setChecked(false);
                        addressType = "4";
                        fillAddress(4);
                        break;
                    case R.id.showroom_radio:
                        radioOffice.setChecked(false);
                        radioOther.setChecked(false);
                        radioHome.setChecked(false);
                        addressType = "3";
                        fillAddress(3);
                        break;

                }
            }
        });


        /**
         * for variants
         */
        evaluatorNameList = CarsDBHandler.getInstance().getEvaluatorNameList(realm);
        // if(evaluatorNameList!=null && evaluatorNameList.length>0){
        ArrayAdapter<String> evaluatorsAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, evaluatorNameList);
        tv_evaluator_name.setAdapter(evaluatorsAdapter);
        tv_evaluator_name.setThreshold(0);
        //}
        tv_evaluator_name.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                evaluatorNameList = CarsDBHandler.getInstance().getEvaluatorNameList(realm);
                if (evaluatorNameList != null && evaluatorNameList.length > 0) {
                    if (!TextUtils.isEmpty(tv_evaluator_name.getText())) {
                        tv_evaluator_name.showDropDown();
                    }
                    if (!TextUtils.isEmpty(tv_evaluator_name.getText())) {
                        for (int i = 0; i < evaluatorNameList.length; i++) {
                            if (tv_evaluator_name.getText().toString().trim().equalsIgnoreCase(evaluatorNameList[i])) {
                                selectedEvaluator = evaluatorNameList[i];
                                selectedEvaluatorId = evaluatorIdList.get(i);
                            }
                        }
                    }
                } else {
                    Toast.makeText((Context) activity, "Please wait till we retrieve the evaluator names", Toast.LENGTH_LONG);
                }

                return false;
            }
        });
        tv_evaluator_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                evaluatorNameList = CarsDBHandler.getInstance().getEvaluatorNameList(realm);
                if (evaluatorNameList.length > 0) {
                    if (!TextUtils.isEmpty(tv_evaluator_name.getText())) {
                        for (int i = 0; i < evaluatorNameList.length; i++) {
                            if (tv_evaluator_name.getText().toString().trim().equalsIgnoreCase(evaluatorNameList[i])) {
                                selectedEvaluator = evaluatorNameList[i];
                                selectedEvaluatorId = evaluatorIdList.get(i);
                            }
                        }
                    }
                    if (hasFocus && TextUtils.isEmpty(tv_evaluator_name.getText()) && tv_evaluator_name.getAdapter() != null) {
                        tv_evaluator_name.showDropDown();

                    }
                } else {
                    Toast.makeText((Context) activity, "Please wait till we retrieve the evaluator names", Toast.LENGTH_LONG);
                }
            }
        });
        tv_evaluator_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                evaluatorNameList = CarsDBHandler.getInstance().getEvaluatorNameList(realm);
                if (evaluatorNameList.length > 0) {
                    if (!TextUtils.isEmpty(tv_evaluator_name.getText())) {
                        for (int i = 0; i < evaluatorNameList.length; i++) {
                            if (tv_evaluator_name.getText().toString().trim().equalsIgnoreCase(evaluatorNameList[i])) {
                                selectedEvaluator = evaluatorNameList[i];
                                selectedEvaluatorId = evaluatorIdList.get(i);
                            }
                        }
                    }
                    if (TextUtils.isEmpty(tv_evaluator_name.getText())) {
                        tv_evaluator_name.showDropDown();

                    }
                } else {
                    Toast.makeText((Context) activity, "Please wait till we retrieve the evaluator names", Toast.LENGTH_LONG);
                }
            }
        });


        tv_evaluation_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calenderMaxVal = Calendar.getInstance();
                calenderMaxVal.add(Calendar.DAY_OF_MONTH,10);
                Util.showDatePickerPostBooking(CustomDialogs.this, activity, R.id.tv_evaluation_date,WSConstants.TYPE_DATE_PICKER.EVALUATION_EXCHANGE_CAR,Calendar.getInstance(), calenderMaxVal);
              //  Util.showDatePicker(CustomDialogs.this, activity, R.id.tv_evaluation_date, WSConstants.TYPE_DATE_PICKER.EVALUATION_EXCHANGE_CAR);
            }
        });
        tv_evaluation_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.showTimePicker(CustomDialogs.this, activity, R.id.tv_evaluation_time);
            }
        });
        TextView tvSave = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);


        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int addTypeId = Integer.parseInt(addressType);
                if(selectedEvaluator.isEmpty() || selectedEvaluationTime.isEmpty()){
                    Toast.makeText(activity,"Enter correct values in all fields",Toast.LENGTH_SHORT).show();
                } if(addTypeId==0 || (addTypeId!=0 && addTypeId!=3 && tv_evaluation_address.getText().toString().isEmpty())){
                    Toast.makeText(activity,"Enter correct values in all fields",Toast.LENGTH_SHORT).show();
                }else if(addTypeId==3 && selectedShowRoom!=null && selectedShowRoom.isEmpty()){
                    Toast.makeText(activity,"Enter correct values in all fields",Toast.LENGTH_SHORT).show();
                }else{
                    alertDialog.dismiss();
                    WSConstants.c360TabId = 1;
                    mNewCarsProgressLoader.progressButtonClicked();
                    CarsFragmentServiceHandler.getInstance(activity).evaluationFormSubmit(
                            CarsFragmentServiceHandler.getInstance(activity).createEvaluationModel("" + exchangeCarDetails.getLeadId(), leadLastUpdated,
                                    exchangeCarDetails.getExchangecarname(), selectedEvaluator,
                                    tv_evaluation_address.getText().toString().toUpperCase().trim(), tv_evaluation_remarks.getText().toString().trim(),
                                    exchangeCarDetails.getLead_exchange_car_id(), selectedEvaluatorId,
                                    selectedEvaluationTime.trim(),addressType,selectedShowRoom,selectedShowRoomId),
                            pref.getAccessToken());
                }

            }
        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

    }


    public void showAlrdyInvoicedDialog(final String locationId, final String leadSourceId,
                                        final FormSubmissionInputData formSubmissionInputData,
                                        final String leadLastUpdated){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.alrdy_booked_dialog, null);
        dialogBuilder.setView(dialogView);


        TextView tvSave = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                showBookCarDetailsDialog(locationId, leadSourceId, formSubmissionInputData,leadLastUpdated);

                }

        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();


    }
    /**
     * dialog to book a car
     *
     * @param locationId
     * @param leadSourceId
     * @param formSubmissionInputData
     */
    public void showBookCarDetailsDialog(final String locationId, final String leadSourceId,
                                         final FormSubmissionInputData formSubmissionInputData,
                                         final String leadLastUpdated) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        System.out.println("CustomDialogs is called "+activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.book_car_details, null);
        dialogBuilder.setView(dialogView);
        TextView carName = (TextView) dialogView.findViewById(R.id.car_details_name);
        carName.setText(allInterestedCar.getIntrestedCarName());
        final EditText edt_bookingName = (EditText) dialogView.findViewById(R.id.tv_booking_name);
        final EditText edtPan = (EditText) dialogView.findViewById(R.id.tv_pan_no);
        final EditText edtAdhaar = (EditText) dialogView.findViewById(R.id.tv_aadhaar);
        final EditText edtRemarks = (EditText) dialogView.findViewById(R.id.et_booking_remarks);
        final TextView aadharTitle = (TextView) dialogView.findViewById(R.id.txt_title_aadhar);
        final EditText edtGst = (EditText) dialogView.findViewById(R.id.tv_gst);
        final TextView gstTitle = (TextView) dialogView.findViewById(R.id.txt_title_gst);
        tv_dob = (TextView) dialogView.findViewById(R.id.tv_dob);
        gstSwitch = false;
        SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findFirst();
        if(salesCRMRealmTable.getTypeOfCustomer().equalsIgnoreCase(WSConstants.customerType.get(4)) ||
                salesCRMRealmTable.getTypeOfCustomer().equalsIgnoreCase(WSConstants.customerType.get(6))){
            gstTitle.setVisibility(View.VISIBLE);
            edtGst.setVisibility(View.VISIBLE);
            aadharTitle.setVisibility(View.GONE);
            edtAdhaar.setVisibility(View.GONE);
            gstSwitch = true;
        }else{
            gstTitle.setVisibility(View.GONE);
            edtGst.setVisibility(View.GONE);
            aadharTitle.setVisibility(View.VISIBLE);
            edtAdhaar.setVisibility(View.VISIBLE);
            gstSwitch = false;
        }

        tv_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calenderMaxVal = Calendar.getInstance();
                calenderMaxVal.add(Calendar.DAY_OF_MONTH,10);
                Util.showDatePickerBirthDay(CustomDialogs.this, activity, R.id.tv_dob,Calendar.getInstance());
               // Util.showDatePicker(CustomDialogs.this, activity, R.id.tv_dob, WSConstants.TYPE_DATE_PICKER.DOB);
            }
        });
        TextView tvNext = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strRemarkTitle = edtRemarks.getText().toString();

                if(!selectedDOB.isEmpty()  && !selectedDOB.matches("Select a Date")
                        && !edt_bookingName.equals("") && !edt_bookingName.getText().toString().isEmpty()
                        && !edtPan.equals("") && !edtPan.getText().toString().isEmpty() && edtPan.length() == 10
                        && ! TextUtils.isEmpty(strRemarkTitle) && !strRemarkTitle.equalsIgnoreCase("")) {

                    if(gstSwitch){
                        if(edtGst.equals("") && edtGst.getText().toString().isEmpty()) {
                            Toast.makeText(activity, "Enter correct values in all fields", Toast.LENGTH_SHORT).show();
                        }else{
                            bookingName = edt_bookingName.getText().toString().toUpperCase().trim();
                            pan = edtPan.getText().toString().toUpperCase().trim();
                            aadhar = edtAdhaar.getText().toString().toUpperCase().trim();
                            bokingRemarks =   edtRemarks.getText().toString().toUpperCase().trim();
                            gst = edtGst.getText().toString().toUpperCase().trim();
                            alertDialog.dismiss();
                            showBookCarDialog(locationId, leadSourceId, formSubmissionInputData,leadLastUpdated);
                        }
                    }else {
                        if (!edtAdhaar.equals("") && !edtAdhaar.getText().toString().isEmpty() && edtAdhaar.length() == 12) {
                            bookingName = edt_bookingName.getText().toString().toUpperCase().trim();
                            pan = edtPan.getText().toString().toUpperCase().trim();
                            aadhar = edtAdhaar.getText().toString().toUpperCase().trim();
                            bokingRemarks = edtRemarks.getText().toString().toUpperCase().trim();
                            gst = edtGst.getText().toString().toUpperCase().trim();
                            alertDialog.dismiss();
                            showBookCarDialog(locationId, leadSourceId, formSubmissionInputData, leadLastUpdated);
                        }
                        else {
                            Toast.makeText(activity, "Enter correct values in all fields", Toast.LENGTH_SHORT).show();

                        }
                    }
                }else{
                    Toast.makeText(activity,"Enter correct values in all fields",Toast.LENGTH_SHORT).show();
                }
            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    /**
     * dialog to book a car
     *
     * @param locationId
     * @param leadSourceId
     * @param formSubmissionInputData
     */
    public void showBookCarDialog(final String locationId, final String leadSourceId,
                                  final FormSubmissionInputData formSubmissionInputData,
                                  final String leadLastUpdated) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        System.out.println("CustomDialogs is called "+activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.book_car, null);
        dialogBuilder.setView(dialogView);
        ImageButton addCar = (ImageButton) dialogView.findViewById(R.id.plus);
        ImageButton reduceCar = (ImageButton) dialogView.findViewById(R.id.minus);
        TextView carName = (TextView) dialogView.findViewById(R.id.car_details_name);
        carName.setText(allInterestedCar.getIntrestedCarName());
        final LinearLayout bookings_lyt = (LinearLayout) dialogView.findViewById(R.id.bookings_lyt);
        final TextView bookingCount = (TextView) dialogView.findViewById(R.id.booking_quantity_number);
        final EditText amtRecvdEt = (EditText) dialogView.findViewById(R.id.car_amount_received_name_value);
        followUpDate = (TextView) dialogView.findViewById(R.id.follow_up_date_tv);
        followUpTime = (TextView) dialogView.findViewById(R.id.follow_up_time_tv);
        followUpDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calenderMaxVal = Calendar.getInstance();
                calenderMaxVal.add(Calendar.DAY_OF_MONTH,10);
                Util.showDatePickerPostBooking(CustomDialogs.this, activity, R.id.follow_up_date_tv,WSConstants.TYPE_DATE_PICKER.BOOKING_FOLLOW_UP_DATE,Calendar.getInstance(), calenderMaxVal);
               //Util.showDatePicker(CustomDialogs.this, activity, R.id.follow_up_date_tv, WSConstants.TYPE_DATE_PICKER.BOOKING_FOLLOW_UP_DATE);
            }
        });
        followUpTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.showTimePicker(CustomDialogs.this, activity, R.id.follow_up_time_tv);
            }
        });
        amtRecvdEt.setText(allInterestedCar.getBooking_amount());
        TextView tvSave = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);
        allBookEds.clear();
        clickedCount = 0;

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedCount = 0;
                alertDialog.dismiss();
                allBookEds.clear();
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!selectedFollowUpDate.isEmpty()  && !selectedFollowUpDate.equals("Select Date")
                        && !amtRecvdEt.equals("") && !amtRecvdEt.getText().toString().isEmpty()
                        && !selectedFollowUpTime.isEmpty()  && !selectedFollowUpTime.equals("Select Time")
                        && allBookEds.size() != 0 && !allBookEds.get(0).getText().equals("")) {
                    alertDialog.dismiss();
                    clickedCount = 0;
                    WSConstants.c360TabId = 1;

                    String DateTime =  ""+selectedFollowUpTime.trim();
                    if(DateTime.isEmpty())
                        DateTime = ""+selectedFollowUpDate;

                    mNewCarsProgressLoader.progressButtonClicked();
                    CarsFragmentServiceHandler.getInstance(activity).callBookCarApi(
                            CarsFragmentServiceHandler.getInstance(activity).createBookCarObject(bokingRemarks,bookingName,
                                    selectedDOB, pan, aadhar, gst, allBookEds, amtRecvdEt.getText().toString().trim(),
                                    allInterestedCar, locationId, DateTime, leadSourceId, formSubmissionInputData,leadLastUpdated),
                            allInterestedCar.getCarId(),""+allInterestedCar.getLeadId(), pref.getAccessToken(), new CarsFragmentServiceHandler.CarBookingCallBack() {
                                @Override
                                public void onCallBookingCallBack(boolean success, String response) {
                                }
                            }, pref);

                }else{
                    Toast.makeText(activity,"Enter correct values in all fields",Toast.LENGTH_SHORT).show();
                }
            }
        });
        addCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v != null) {
                    InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputManager.isActive() || inputManager.isAcceptingText()) {
                        inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                }
                clickedCount++;
                bookingCount.setText("" + clickedCount);

               RelativeLayout rl1 = new RelativeLayout(activity);
                rl1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

             /*   LinearLayout rl1 = new LinearLayout(activity);
                rl1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                rl1.setOrientation(LinearLayout.HORIZONTAL);*/

                TextView label = new TextView(activity);
                RelativeLayout.LayoutParams labelParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                labelParams.setMargins(0, 2, 2, 0);
                label.setText("ID " + clickedCount);

                EditText editBooking = new EditText(activity);
                editBooking.setId(clickedCount);
                editBooking.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
                //editBooking.setHint("0");
                RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params1.addRule(RelativeLayout.END_OF, label.getId());
                //params1.addRule(RelativeLayout.ALIGN_BOTTOM, label.getId());
                params1.setMargins(50, 0, 0, 0);

                rl1.addView(label, labelParams);
                rl1.addView(editBooking, params1);
                allBookEds.add(editBooking);

                bookings_lyt.addView(rl1);

            }
        });

        reduceCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v != null) {
                    InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (inputManager.isActive() || inputManager.isAcceptingText()) {
                        inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                }
                if (clickedCount > 0) {
                    --clickedCount;
                    bookingCount.setText("" + clickedCount);
                    bookings_lyt.removeViewAt(clickedCount);
                    allBookEds.remove(clickedCount);
                }


            }
        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }
    /**
     *
     */
    public void showExchangeCarEditdialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_exchange_car_edit, null);
        dialogBuilder.setView(dialogView);

        Button cancel = (Button) dialogView.findViewById(R.id.cancel_car);
        Button save = (Button) dialogView.findViewById(R.id.save_car);
        final EditText edtExpectedPrice = (EditText) dialogView.findViewById(R.id.edt_expected_price);
        final EditText edtMarketPrice = (EditText) dialogView.findViewById(R.id.edt_market_price);
        final EditText edtQuotedPrice = (EditText) dialogView.findViewById(R.id.edt_quoted_price);
        final EditText edtTotalRun = (EditText) dialogView.findViewById(R.id.edt_total_run);
        final EditText edtRegNum = (EditText) dialogView.findViewById(R.id.edt_reg_number);
        tv_purchase_date_exchange = (TextView) dialogView.findViewById(R.id.date_calendar);

        edtExpectedPrice.setText(exchangeCarDetails.getExpected_price());
        edtMarketPrice.setText(exchangeCarDetails.getMarket_price());
        edtQuotedPrice.setText(exchangeCarDetails.getPrice_quoted());
        edtRegNum.setText(exchangeCarDetails.getReg_no());
        edtTotalRun.setText(exchangeCarDetails.getKms_run());
        tv_purchase_date_exchange.setText(exchangeCarDetails.getPurchase_date());

        edtExpectedPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                selectedExpectedPrice = edtExpectedPrice.getText().toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectedExpectedPrice = edtExpectedPrice.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                selectedExpectedPrice = edtExpectedPrice.getText().toString();

            }
        });
        selectedExpectedPrice = edtExpectedPrice.getText().toString();
        edtMarketPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                selectedMarketPrice = edtMarketPrice.getText().toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectedMarketPrice = edtMarketPrice.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        selectedMarketPrice = edtMarketPrice.getText().toString();
        edtQuotedPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                selectedQuotedPrice = edtQuotedPrice.getText().toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectedQuotedPrice = edtQuotedPrice.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        selectedQuotedPrice = edtQuotedPrice.getText().toString();
        edtTotalRun.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                selectedTotalRuns = edtTotalRun.getText().toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectedTotalRuns = edtTotalRun.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        selectedTotalRuns = edtTotalRun.getText().toString();
        edtRegNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                selectedRegNum = edtRegNum.getText().toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                selectedRegNum = edtRegNum.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                selectedRegNum = edtRegNum.getText().toString();
            }
        });
        selectedRegNum = edtRegNum.getText().toString();
        tv_purchase_date_exchange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  showDatePicker(CarsFragment.this, getActivity(), R.id.tv_purchase_date_exchange);
                Calendar calenderMaxVal = Calendar.getInstance();
                calenderMaxVal.add(Calendar.DAY_OF_MONTH,10);
                Util.showDatePicker(CustomDialogs.this, activity, R.id.date_calendar,WSConstants.TYPE_DATE_PICKER.EXCHANGE_PURCHASE_DATE);


            }
        });
        TextView txtSelectModel = (TextView) dialogView.findViewById(R.id.addcarselectmodel);
        selectedModelId = exchangeCarDetails.getModel_id();
        ExchangeCarBrandModelsDB mCarBrandModelsDB = realm.where(ExchangeCarBrandModelsDB.class)
                .equalTo("model_id", selectedModelId)
                .findFirst();
        selectedModel = mCarBrandModelsDB.getModel_name();
        txtSelectModel.setText(selectedModel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //callsavedcardetails();
                mNewCarsProgressLoader.progressButtonClicked();
                CarsFragmentServiceHandler.getInstance(activity).editExchangeCarApiCall(pref.getAccessToken(), CarsFragmentServiceHandler.getInstance(activity)
                        .createExchangeEditObject(exchangeCarDetails, selectedYear, selectedModelId, selectedExpectedPrice,
                                selectedMarketPrice, selectedQuotedPrice, selectedRegNum, selectedTotalRuns));
                alertDialog.dismiss();

            }
        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    /**
     * dialog to edit a new car
     */
    public void showEditCarDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.edit_car, null);
        dialogBuilder.setView(dialogView);

        final TextView car_details_name = (TextView) dialogView.findViewById(R.id.car_details_name);
        final AutoCompleteTextView car_variant_name_value = (AutoCompleteTextView) dialogView.findViewById(R.id.car_variant_name_value);
        final AutoCompleteTextView car_fuel_type_name_value = (AutoCompleteTextView) dialogView.findViewById(R.id.car_fuel_type_name_value);
       // final AutoCompleteTextView car_color_name_value = (AutoCompleteTextView) dialogView.findViewById(R.id.car_color_name_value);
         spinnerColor = (Spinner) dialogView.findViewById(R.id.addcarselectcolor);
        final EditText car_booking_number_name_value = (EditText) dialogView.findViewById(R.id.car_booking_id_name_value);
        final EditText car_amount_received_name_value = (EditText) dialogView.findViewById(R.id.car_amount_received_name_value);

        TextView bookingNoTxtView = (TextView) dialogView.findViewById(R.id.car_booking_id_name);
        TextView amtRecvdTxtView = (TextView) dialogView.findViewById(R.id.car_amount_received_name);

        if(allInterestedCar.getCar_stage_id().equalsIgnoreCase(""+WSConstants.CarBookingStages.BOOK_CAR)){
            car_booking_number_name_value.setVisibility(View.GONE);
            car_amount_received_name_value.setVisibility(View.GONE);
            bookingNoTxtView.setVisibility(View.GONE);
            amtRecvdTxtView.setVisibility(View.GONE);
        }
        TextView tvSave = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);

        car_details_name.setText(allInterestedCar.getIntrestedCarName());
        car_variant_name_value.setText(allInterestedCar.getVariant());
        car_fuel_type_name_value.setText(allInterestedCar.getFuelType());
       // car_color_name_value.setText(allInterestedCar.getColor());
        car_booking_number_name_value.setText(allInterestedCar.getBooking_number());
        car_amount_received_name_value.setText(allInterestedCar.getBooking_amount());
        /**
         * for variants
         */

        car_variant_name_value.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!TextUtils.isEmpty(car_variant_name_value.getText())) {
                    car_variant_name_value.showDropDown();
                }
                if (!TextUtils.isEmpty(car_variant_name_value.getText())) {
                    for (int i = 0; i < carVariantNamesList.length; i++) {
                        if (car_variant_name_value.getText().toString().trim().equalsIgnoreCase(carVariantNamesList[i])) {
                            selectedVariantId = variantIdsList.get(i);
                            selectedVariant = carVariantNamesList[i];
                            //car_variant_name_value.setText(selectedVariant);
                        }
                    }
                    handleColorList(allInterestedCar.getIntrestedCarColorId());
                    handleFuelList(car_fuel_type_name_value);
                }
                return false;
            }
        });
        carVariantNamesList = CarsDBHandler.getInstance().getCarVariants(realm, allInterestedCar.getIntrestedCarModelId());
        ArrayAdapter<String> variantAdapter = new ArrayAdapter<String>(activity, R.layout.dialog_textview, R.id.item, carVariantNamesList);
        car_variant_name_value.setAdapter(variantAdapter);
        car_variant_name_value.setThreshold(0);
        car_variant_name_value.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!TextUtils.isEmpty(car_variant_name_value.getText())) {
                    for (int i = 0; i < carVariantNamesList.length; i++) {
                        if (car_variant_name_value.getText().toString().trim().equalsIgnoreCase(carVariantNamesList[i])) {
                            selectedVariantId = variantIdsList.get(i);
                            selectedVariant = carVariantNamesList[i];
                            //car_variant_name_value.setText(selectedVariant);
                        }
                    }
                    handleColorList(allInterestedCar.getIntrestedCarColorId());
                    handleFuelList(car_fuel_type_name_value);
                }
                if (hasFocus && TextUtils.isEmpty(car_variant_name_value.getText()) && car_variant_name_value.getAdapter() != null) {
                    car_variant_name_value.showDropDown();

                }
            }
        });
        car_variant_name_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  System.out.println("On CLick LISTENER CALLED");
                if (!TextUtils.isEmpty(car_variant_name_value.getText())) {
                    for (int i = 0; i < carVariantNamesList.length; i++) {
                        if (car_variant_name_value.getText().toString().trim().equalsIgnoreCase(carVariantNamesList[i])) {
                            selectedVariantId = variantIdsList.get(i);
                            selectedVariant = carVariantNamesList[i];
                            //car_variant_name_value.setText(selectedVariant);
                        }
                    }
                    handleColorList(allInterestedCar.getIntrestedCarColorId());
                    handleFuelList(car_fuel_type_name_value);
                }
                if (TextUtils.isEmpty(car_variant_name_value.getText())) {
                    car_variant_name_value.showDropDown();

                }*/
            }
        });
        car_variant_name_value.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("ON ITEM CLICK LISTENER CALLED");
                System.out.println("On CLick LISTENER CALLED");
                if (!TextUtils.isEmpty(car_variant_name_value.getText())) {
                    for (int i = 0; i < carVariantNamesList.length; i++) {
                        if (car_variant_name_value.getText().toString().trim().equalsIgnoreCase(carVariantNamesList[i])) {
                            selectedVariantId = variantIdsList.get(i);
                            selectedVariant = carVariantNamesList[i];
                            //car_variant_name_value.setText(selectedVariant);
                        }
                    }
                    handleColorList(allInterestedCar.getIntrestedCarColorId());
                    handleFuelList(car_fuel_type_name_value);
                }
                if (TextUtils.isEmpty(car_variant_name_value.getText())) {
                    car_variant_name_value.showDropDown();

                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.dismiss();
                if (spinnerColor.getSelectedItem()!=null && !TextUtils.isEmpty(spinnerColor.getSelectedItem().toString().trim())) {
                    for (int i = 0; i < colorsNameList.length; i++) {
                        if (spinnerColor.getSelectedItem().toString().trim().equalsIgnoreCase(colorsNameList[i])) {
                            selectedColorId = colorIdsList.get(i);
                            selectedColor = colorsNameList[i];
                        }
                    }

                }
                if (!TextUtils.isEmpty(car_fuel_type_name_value.getText().toString().trim())) {
                    for (int i = 0; i < fuelNamesList.length; i++) {
                        if (car_fuel_type_name_value.getText().toString().trim().equalsIgnoreCase(fuelNamesList[i])) {
                            selectedFuelId = fuelIdsList.get(i);
                            selectedFuel = fuelNamesList[i];
                        }
                    }

                }
                mNewCarsProgressLoader.progressButtonClicked();
                if(selectedFuelId.isEmpty())
                    selectedFuelId="1";
                if(selectedFuel.isEmpty())
                    selectedFuel = "PETROL";
                CarsFragmentServiceHandler.getInstance(activity).callEditCarApi(
                        "" + allInterestedCar.getLeadId(), "" + allInterestedCar.getIntrestedLeadCarId(),
                        allInterestedCar.getIntrestedCarModelId(), selectedVariantId, selectedVariant,
                        selectedColorId, selectedColor, selectedFuelId, selectedFuel, car_booking_number_name_value.getText().toString().trim(),
                        car_amount_received_name_value.getText().toString().trim(), pref.getAccessToken(), allInterestedCar.getCarId());
            }
        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    /**
     * dialog to show while confirming full payment is recieved
     *
     * @param allInterestedCar
     * @param locId
     * @param leadSrcId
     */
    public void showFullPaymentRecievedConfirmationDialog(final AllInterestedCars allInterestedCar,
                                                          final int locId, final int leadSrcId, final String leadLastUpdated) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.payment_recieved_form, null);
        dialogBuilder.setView(dialogView);

        TextView tv_question = (TextView) dialogView.findViewById(R.id.tv_custom_dialog_title);
        tv_question.setText("Full Payment Recieved ?");

        TextView tvSave = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);
        expectedDeliveryDateTv = (TextView) dialogView.findViewById(R.id.follow_up_date_tv);
        expectedDeliveryDateTitle = (TextView) dialogView.findViewById(R.id.follow_up_date_title);
        tvRemarkTitle = (TextView) dialogView.findViewById(R.id.tv_remark_title);
        etRemarkTitle = (EditText) dialogView.findViewById(R.id.et_remark_title);
        final RadioButton radioYes = (RadioButton) dialogView.findViewById(R.id.radio_yes);
        final RadioButton radioNo = (RadioButton) dialogView.findViewById(R.id.radio_no);
        final RadioGroup radioGroup = (RadioGroup) dialogView.findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                expectedDeliveryDateTv.setVisibility(View.VISIBLE);
                expectedDeliveryDateTitle.setVisibility(View.VISIBLE);
                etRemarkTitle.setVisibility(View.VISIBLE);
                tvRemarkTitle.setVisibility(View.VISIBLE);
                switch (checkedId) {
                    case R.id.radio_yes:
                        radioNo.setChecked(false);
                        expectedDeliveryDateTitle.setText("Expected Delivery date *");
                        break;
                    case R.id.radio_no:
                        radioYes.setChecked(false);
                        expectedDeliveryDateTitle.setText("Rescedule date *");
                        break;
                }
            }
        });
        expectedDeliveryDateTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calenderMaxVal = Calendar.getInstance();
                calenderMaxVal.add(Calendar.DAY_OF_MONTH,10);
                Util.showDatePickerPostBooking(CustomDialogs.this, activity, R.id.follow_up_date_tv,WSConstants.TYPE_DATE_PICKER.EXPECTED_DELIVERY_DATE,Calendar.getInstance(), calenderMaxVal);
               // Util.showDatePicker(CustomDialogs.this, activity, R.id.follow_up_date_tv,WSConstants.TYPE_DATE_PICKER.EXPECTED_DELIVERY_DATE);
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strRemarkTitle = etRemarkTitle.getText().toString();
                if ( selectedExpectedDeliveryDate.matches("") ||  strRemarkTitle.trim().length() <= 0 || TextUtils.isEmpty(strRemarkTitle)){

                    Toast.makeText(activity, "Please complete all fields.", Toast.LENGTH_LONG).show();

                } else {
                    //your business logic
                    int bookingId = 0, leadCarId = 0, stageId = 0, enquiryId = 0;
                    long  leadId = 0;
                    if (!allInterestedCar.getBooking_id().isEmpty()) {
                        bookingId = Integer.parseInt(allInterestedCar.getBooking_id());
                    }
                    if (!allInterestedCar.getBooking_id().isEmpty()) {
                        bookingId = Integer.parseInt(allInterestedCar.getBooking_id());
                    }
                    if (!allInterestedCar.getCar_stage_id().isEmpty()) {
                        stageId = Integer.parseInt(allInterestedCar.getCar_stage_id());
                    }
                    leadCarId = allInterestedCar.getIntrestedLeadCarId();
                    leadId = allInterestedCar.getLeadId();
                    if (!allInterestedCar.getEnquiry_id().isEmpty()) {
                        enquiryId = Integer.parseInt(allInterestedCar.getEnquiry_id());
                    }
                    if (radioYes.isChecked()) {
                        mNewCarsProgressLoader.progressButtonClicked();
//                        CarsFragmentServiceHandler.getInstance(activity).fullPaymentReceived(strRemarkTitle,bookingId, allInterestedCar.getBooking_number(),
//                                leadCarId, leadSrcId, locId, leadId, stageId, selectedExpectedDeliveryDate.trim(),
//                                pref.getAccessToken(), allInterestedCar.getCarId(), enquiryId,leadLastUpdated, bookingMainModel.getApiInputDetails());

                    } else if (radioNo.isChecked()) {
                        //call the rescedule api.
                        mNewCarsProgressLoader.progressButtonClicked();
                        CarsFragmentServiceHandler.getInstance(activity).rescheduleTask(pref.getAccessToken(), bookingId, leadCarId, leadId,
                                stageId, allInterestedCar.getCarId(), enquiryId, 0, selectedExpectedDeliveryDate.trim(),"");
                    }
                    alertDialog.dismiss();
                }

            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();


    }

    /**
     * dialog to confirm invoice
     *
     * @param locationId
     */
    public void showUpdateInvoiceDialog(final String locationId, final String leadLastUpdated) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_invoice, null);
        dialogBuilder.setView(dialogView);

        final EditText editxt_invoice_no_value = (EditText) dialogView.findViewById(R.id.editxt_invoice_no_value);
        txt_invoice_date_value = (TextView) dialogView.findViewById(R.id.editxt_invoice_date_value);
        final EditText editxt_invoice_name_value = (EditText) dialogView.findViewById(R.id.editxt_invoice_name_value);
        final EditText editxt_invoice_address_value = (EditText) dialogView.findViewById(R.id.editxt_invoice_address_value);
        final EditText editxt_invoice_amount_value = (EditText) dialogView.findViewById(R.id.editxt_invoice_amount_value);
        final EditText editxt_invoice_mobile_number_value = (EditText) dialogView.findViewById(R.id.editxt_invoice_mobile_number_value);
        final EditText editxt_invoice_vin_number_value = (EditText) dialogView.findViewById(R.id.editxt_invoice_vin_number_value);
        TextView tvSave = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);


        txt_invoice_date_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calenderMaxVal = Calendar.getInstance();
                calenderMaxVal.add(Calendar.DAY_OF_MONTH,10);
                Util.showDatePickerPostBooking(CustomDialogs.this, activity, R.id.editxt_invoice_date_value,WSConstants.TYPE_DATE_PICKER.INVOICE_DATE,Calendar.getInstance(), calenderMaxVal);
               // Util.showDatePicker(CustomDialogs.this, activity, R.id.editxt_invoice_date_value, WSConstants.TYPE_DATE_PICKER.INVOICE_DATE);
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int bookingId = 0, leadCarId = 0, enquiryId = 0, locId = 0, leadCarStageId = 0;
                long  leadId = 0;
                String bookingNo = "", expectedDate = "";

                leadCarId = allInterestedCar.getIntrestedLeadCarId();
                leadId = allInterestedCar.getLeadId();
                bookingNo = allInterestedCar.getBooking_number();
                expectedDate = allInterestedCar.getExpected_delivery_date();
                if (allInterestedCar.getBooking_id() != null && !allInterestedCar.getBooking_id().isEmpty()) {
                    bookingId = Integer.parseInt(allInterestedCar.getBooking_id());
                }
                if (allInterestedCar.getCar_stage_id() != null && !allInterestedCar.getCar_stage_id().isEmpty()) {
                    leadCarStageId = Integer.parseInt(allInterestedCar.getCar_stage_id());
                }
                if (allInterestedCar.getEnquiry_id() != null && !allInterestedCar.getEnquiry_id().isEmpty()) {
                    enquiryId = Integer.parseInt(allInterestedCar.getEnquiry_id());
                }
                if (locationId != null && !locationId.isEmpty()) {
                    locId = Integer.parseInt(locationId);
                }
                if(selectedInvoiceDate.isEmpty() || editxt_invoice_no_value.getText().toString().trim().isEmpty() ||
                        editxt_invoice_address_value.getText().toString().trim().isEmpty() ||
                        editxt_invoice_name_value.getText().toString().trim().isEmpty() ||
                        editxt_invoice_amount_value.getText().toString().trim().isEmpty() ||
                        editxt_invoice_mobile_number_value.getText().toString().trim().isEmpty() ||
                        editxt_invoice_vin_number_value.getText().toString().trim().isEmpty()){
                    Toast.makeText(activity, "Please fill all details", Toast.LENGTH_LONG).show();

                }else{
                    alertDialog.dismiss();
                    mNewCarsProgressLoader.progressButtonClicked();
                    CarsFragmentServiceHandler.getInstance(activity).updateInvoice(
                            CarsFragmentServiceHandler.getInstance(activity).
                                    createInvoiceModelObject(editxt_invoice_no_value.getText().toString().trim(), selectedInvoiceDate.trim(),
                                            editxt_invoice_name_value.getText().toString().trim(), editxt_invoice_address_value.getText().toString().trim(),
                                            editxt_invoice_amount_value.getText().toString().trim(), editxt_invoice_mobile_number_value.getText().toString().trim(),
                                            editxt_invoice_vin_number_value.getText().toString().trim(),
                                            bookingId, bookingNo, leadCarId, enquiryId, locId, leadId, leadCarStageId, expectedDate,leadLastUpdated),
                            allInterestedCar.getCarId(), pref.getAccessToken(),allInterestedCar.getLeadId(), new CarsFragmentServiceHandler.CarBookingCallBack() {
                                @Override
                                public void onCallBookingCallBack(boolean success, String response) {
                                   // apiSubmitAcknowledgement(success, response);
                                }
                            });
                }



            }
        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }


    /**
     * dialog to confirm delivery
     *
     * @param locationId
     */
    public void showUpdateDeliveryDialog(final String locationId, final String leadLastUpdated) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.update_delivery, null);
        dialogBuilder.setView(dialogView);

        final TextView txt_invoice_name_value = (TextView) dialogView.findViewById(R.id.editxt_invoice_name_value);
        final TextView txt_invoice_mobile_number_value = (TextView) dialogView.findViewById(R.id.editxt_invoice_mobile_number_value);
        final TextView txt_invoice_vin_number_value = (TextView) dialogView.findViewById(R.id.editxt_invoice_vin_number_value);
        txt_resceduleDate = (TextView) dialogView.findViewById(R.id.follow_up_date_tv);
        final EditText edtxt_delivery_challan = (EditText) dialogView.findViewById(R.id.edtxt_delivery_challan);
        final RadioGroup radioGroup = (RadioGroup) dialogView.findViewById(R.id.radio_group);
        final RadioButton radioYes = (RadioButton) dialogView.findViewById(R.id.radio_yes);
        final RadioButton radioNo = (RadioButton) dialogView.findViewById(R.id.radio_no);

        txt_invoice_name_value.setText(allInterestedCar.getInvoiceDetails().getInvoice_name());
        txt_invoice_mobile_number_value.setText(allInterestedCar.getInvoiceDetails().getInvoice_mob_no());
        txt_invoice_vin_number_value.setText(allInterestedCar.getInvoiceDetails().getVin_no());
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_yes:
                        radioNo.setChecked(false);
                        edtxt_delivery_challan.setVisibility(View.VISIBLE);
                        txt_resceduleDate.setVisibility(View.GONE);
                        edtxt_delivery_challan.setHint("Enter Delivery Challan");
                        break;
                    case R.id.radio_no:
                        radioYes.setChecked(false);
                        edtxt_delivery_challan.setVisibility(View.GONE);
                        txt_resceduleDate.setVisibility(View.VISIBLE);
                        txt_resceduleDate.setText("Select Reschedule date");
                        break;
                }
            }
        });

        txt_resceduleDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calenderMaxVal = Calendar.getInstance();
                calenderMaxVal.add(Calendar.DAY_OF_MONTH,10);
                Util.showDatePickerPostBooking(CustomDialogs.this, activity, R.id.follow_up_date_tv,WSConstants.TYPE_DATE_PICKER.DELIVERY_RESCHEDULE_DATE,Calendar.getInstance(), calenderMaxVal);
               // Util.showDatePicker(CustomDialogs.this, activity, R.id.follow_up_date_tv,WSConstants.TYPE_DATE_PICKER.DELIVERY_RESCHEDULE_DATE);
            }
        });

        TextView tvSave = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int bookingId = 0, leadCarId = 0, enquiryId = 0, locId = 0, leadCarStageId = 0, invoiceId = 0;
                long  leadId = 0;
                String bookingNo = "";

                leadCarId = allInterestedCar.getIntrestedLeadCarId();
                leadId = allInterestedCar.getLeadId();
                bookingNo = allInterestedCar.getBooking_number();
                if (allInterestedCar.getBooking_id() != null && !allInterestedCar.getBooking_id().isEmpty()) {
                    bookingId = Integer.parseInt(allInterestedCar.getBooking_id());
                }
                if (allInterestedCar.getCar_stage_id() != null && !allInterestedCar.getCar_stage_id().isEmpty()) {
                    leadCarStageId = Integer.parseInt(allInterestedCar.getCar_stage_id());
                }
                if (allInterestedCar.getEnquiry_id() != null && !allInterestedCar.getEnquiry_id().isEmpty()) {
                    enquiryId = Integer.parseInt(allInterestedCar.getEnquiry_id());
                }
                if (locationId != null && !locationId.isEmpty()) {
                    locId = Integer.parseInt(locationId);
                }
                if (radioYes.isChecked()) {
                    if (edtxt_delivery_challan.getText().toString().trim().isEmpty() ||
                            edtxt_delivery_challan.getText().toString().trim().equalsIgnoreCase("Enter Delivery Challan")) {

                        Toast.makeText(activity, "Please enter delivery challan number", Toast.LENGTH_LONG).show();
                    } else {
                        alertDialog.dismiss();
                        mNewCarsProgressLoader.progressButtonClicked();
                        CarsFragmentServiceHandler.getInstance(activity).updateDelivery(
                                CarsFragmentServiceHandler.getInstance(activity).
                                        createUpdateDeliveryModelObject(edtxt_delivery_challan.getText().toString().trim(),
                                                allInterestedCar.getInvoiceDetails().getInvoice_number().trim(),
                                                txt_invoice_name_value.getText().toString().trim(),
                                                txt_invoice_mobile_number_value.getText().toString().trim(),
                                                txt_invoice_vin_number_value.getText().toString().trim(),
                                                bookingId, bookingNo, leadCarId, enquiryId, locId, leadId, leadCarStageId,
                                                allInterestedCar.getInvoiceDetails().getInvoiceId(), leadLastUpdated),
                                allInterestedCar.getCarId(), pref.getAccessToken(), new CarsFragmentServiceHandler.CarBookingCallBack() {
                                    @Override
                                    public void onCallBookingCallBack(boolean success, String response) {

                                    }
                                });
                    }
                } else if (radioNo.isChecked()) {
                    if (txt_resceduleDate.getText().toString().trim().isEmpty() || txt_resceduleDate.getText().toString().trim().equalsIgnoreCase("Select Reschedule date")) {
                        Toast.makeText(activity, "Please select a date to reschedule delivery", Toast.LENGTH_LONG).show();
                    } else {
                        alertDialog.dismiss();
                        if (allInterestedCar.getInvoiceDetails() != null) {
                            invoiceId = allInterestedCar.getInvoiceDetails().getInvoiceId();
                        }
                       // rescheduleDate = txt_resceduleDate.getText().toString().trim();
                        CarsFragmentServiceHandler.getInstance(activity).rescheduleTask(pref.getAccessToken(), bookingId, leadCarId, leadId,
                                leadCarStageId, allInterestedCar.getCarId(), enquiryId, invoiceId, selectedRescheduleDate,"");
                    }

                }
            }


        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    /**
     * cancel invoice dialog
     *
     * @param bookingId
     * @param leadCarId
     * @param locId
     * @param leadId
     * @param stageId
     * @param presentCarId
     * @param enquiryId
     * @param invoiceId
     * @param accessToken
     */
    public void showCancelInvoiceDialog(final int bookingId, final int leadCarId, final int locId,
                                        final long leadId, final int stageId, final String presentCarId,
                                        final int enquiryId, final int invoiceId, final String accessToken, final String leadLastUpdated) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog_yes_no_layout, null);
        dialogBuilder.setView(dialogView);

        TextView tv_question = (TextView) dialogView.findViewById(R.id.tv_custom_dialog_title);
        tv_question.setText("Are you sure to cancel the Invoice?");
        TextView tvSave = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNewCarsProgressLoader.progressButtonClicked();
                CarsFragmentServiceHandler.getInstance(activity).cancelInvoice(accessToken,
                        bookingId, leadCarId, locId, leadId, stageId, presentCarId, enquiryId, invoiceId, leadLastUpdated);
                alertDialog.dismiss();


            }


        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();


    }

    /**
     *
     * @param locId
     */
    public void showUpdateInvoiceConfirmationDialog(final String locId, final String leadLastUpdated, final CallBack callBack) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.payment_new_dialog, null);
        dialogBuilder.setView(dialogView);
        TextView bookingID = (TextView) dialogView.findViewById(R.id.booking_id);
        TextView carName = (TextView) dialogView.findViewById(R.id.car_name);
        TextView customerName = (TextView) dialogView.findViewById(R.id.customer_name);

        bookingID.setText(allInterestedCar.getBooking_number());
        customerName.setText(allInterestedCar.getCustomerName());
        carName.setText(allInterestedCar.getModel());
        TextView tvSave = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                int bookingId = 0, leadCarId = 0, stageId = 0, enquiryId = 0;
                long  leadId = 0;
                if (!allInterestedCar.getBooking_id().isEmpty()) {
                    bookingId = Integer.parseInt(allInterestedCar.getBooking_id());
                }
                if (!allInterestedCar.getBooking_id().isEmpty()) {
                    bookingId = Integer.parseInt(allInterestedCar.getBooking_id());
                }
                if (!allInterestedCar.getCar_stage_id().isEmpty()) {
                    stageId = Integer.parseInt(allInterestedCar.getCar_stage_id());
                }
                leadCarId = allInterestedCar.getIntrestedLeadCarId();
                leadId = allInterestedCar.getLeadId();
                if (!allInterestedCar.getEnquiry_id().isEmpty()) {
                    enquiryId = Integer.parseInt(allInterestedCar.getEnquiry_id());
                }
                mNewCarsProgressLoader.progressButtonClicked();
                alertDialog.dismiss();
                CarsFragmentServiceHandler.getInstance(activity).callCancelPaymentApi(pref.getAccessToken(),
                        leadId, leadCarId, bookingId, stageId, enquiryId, leadLastUpdated);
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                //showUpdateInvoiceDialog(locId, leadLastUpdated);
                callBack.onCallBack();

            }


        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();


    }

    /**
     * shown when user cancels a booking
     *
     * @param bookingId
     * @param leadCarId
     * @param locId
     * @param leadId
     * @param stageId
     * @param presentCarId
     * @param enquiryId
     * @param accessToken
     */
    public void showCancelReasonDialog(final int bookingId, final int leadCarId, final int locId,
                                       final long leadId, final int stageId, final String presentCarId,
                                       final int enquiryId, final String accessToken, final String invoiceId, final String leadLastUpdated) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.cancel_reason_dialog, null);
        dialogBuilder.setView(dialogView);


        final RadioGroup radioGroup = (RadioGroup) dialogView.findViewById(R.id.radio_group);
        final RadioButton radioReasonOne = (RadioButton) dialogView.findViewById(R.id.reason1);
        final RadioButton radioReasonTwo = (RadioButton) dialogView.findViewById(R.id.reason2);
        final RadioButton radioReasonThree = (RadioButton) dialogView.findViewById(R.id.reason3);

        radioReasonOne.setText("Model Change");
        radioReasonTwo.setText("Plan Postponed");
        radioReasonThree.setText("Issue In Price");
        radioGroup.clearCheck();
        selected = false;
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.reason1:
                        radioReasonTwo.setChecked(false);
                        radioReasonThree.setChecked(false);
                        reason = radioReasonOne.getText().toString().trim();
                        selected = true;
                        break;
                    case R.id.reason2:
                        radioReasonOne.setChecked(false);
                        radioReasonThree.setChecked(false);
                        reason = radioReasonTwo.getText().toString().trim();
                        selected = true;
                        break;
                    case R.id.reason3:
                        radioReasonTwo.setChecked(false);
                        radioReasonOne.setChecked(false);
                        reason = radioReasonThree.getText().toString().trim();
                        selected = true;
                        break;
                }
            }
        });

        TextView tvSave = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                selected = false;
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selected) {
                    Toast.makeText(activity, "Please select one reason", Toast.LENGTH_LONG).show();
                } else {
                    mNewCarsProgressLoader.progressButtonClicked();
                    CarsFragmentServiceHandler.getInstance(activity).cancelBooking(accessToken,
                            bookingId, leadCarId, locId, leadId, stageId, presentCarId, enquiryId, reason,invoiceId,leadLastUpdated);
                    alertDialog.dismiss();
                }

            }


        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

    }

    public void showEvaluatorsCancel(){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.evaluators_cancel_dialog, null);
        dialogBuilder.setView(dialogView);

        final RadioGroup radioGroup = (RadioGroup) dialogView.findViewById(R.id.radio_group);
        final RadioButton radioReasonOne = (RadioButton) dialogView.findViewById(R.id.reason1);
        final RadioButton radioReasonTwo = (RadioButton) dialogView.findViewById(R.id.reason2);
        final RadioButton radioReasonThree = (RadioButton) dialogView.findViewById(R.id.reason3);

        radioReasonOne.setText("Option 1");
        radioReasonTwo.setText("Option 2");
        radioReasonThree.setText("Option 3");
        radioGroup.clearCheck();
        selected = false;
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.reason1:
                        radioReasonTwo.setChecked(false);
                        radioReasonThree.setChecked(false);
                        reason = radioReasonOne.getText().toString().trim();
                        selected = true;
                        break;
                    case R.id.reason2:
                        radioReasonOne.setChecked(false);
                        radioReasonThree.setChecked(false);
                        reason = radioReasonTwo.getText().toString().trim();
                        selected = true;
                        break;
                    case R.id.reason3:
                        radioReasonTwo.setChecked(false);
                        radioReasonOne.setChecked(false);
                        reason = radioReasonThree.getText().toString().trim();
                        selected = true;
                        break;
                }
            }
        });

        TextView tvSave = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvCancel = (TextView) dialogView.findViewById(R.id.bt_custom_dialog_no);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                selected = false;
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selected) {
                    Toast.makeText(activity, "Please select one reason", Toast.LENGTH_LONG).show();
                } else {
                    mNewCarsProgressLoader.progressButtonClicked();
                    alertDialog.dismiss();
                }

            }


        });

        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }


    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        if (followUpDate != null && (ViewCompat.isAttachedToWindow(followUpDate)  || followUpDate.isActivated())) {
            followUpDate.setTag(R.id.autoninja_date, calendar);
            tv_final_followup = followUpDate;
            followUpDate.setText(String.format(Locale.getDefault(), "%d-%d-%d", (dayOfMonth), (monthOfYear + 1), year));
            selectedFollowUpDate = String.format(Locale.getDefault(), "%d-%d-%d", dayOfMonth, monthOfYear+1, year);
            Util.showTimePicker(CustomDialogs.this, activity,followUpDate.getId());
        } else if (expectedDeliveryDateTv != null && (ViewCompat.isAttachedToWindow(expectedDeliveryDateTv) || expectedDeliveryDateTv.isActivated())) {
            expectedDeliveryDateTv.setTag(R.id.autoninja_date, calendar);
            Util.showTimePicker(CustomDialogs.this, activity,expectedDeliveryDateTv.getId());
        } else if (txt_resceduleDate != null && (ViewCompat.isAttachedToWindow(txt_resceduleDate)  ||  txt_resceduleDate.isActivated())) {
            txt_resceduleDate.setTag(R.id.autoninja_date, calendar);
            Util.showTimePicker(CustomDialogs.this, activity,txt_resceduleDate.getId());
        } else if (tv_evaluation_date != null && (ViewCompat.isAttachedToWindow(tv_evaluation_date) || tv_evaluation_date.isActivated())) {
            tv_evaluation_date.setTag(R.id.autoninja_date, calendar);
            tv_evaluation_date.setText(String.format(Locale.getDefault(), "%d-%d-%d", (dayOfMonth), (monthOfYear + 1), year));
            tv_final_evaluation_schedule = tv_evaluation_date;
            selectedEvaluationDate = Util.getSQLDateTime(calendar.getTime()).split("\\.")[0];
        } else if (txt_invoice_date_value != null && (ViewCompat.isAttachedToWindow(txt_invoice_date_value)  || txt_invoice_date_value.isActivated())) {
            txt_invoice_date_value.setText(String.format(Locale.getDefault(), "%d-%d-%d", (dayOfMonth), (monthOfYear + 1), year));
            selectedInvoiceDate = String.format(Locale.getDefault(), "%d-%d-%d", dayOfMonth, monthOfYear+1, year);
        } else if (tv_purchase_date_exchange != null && (ViewCompat.isAttachedToWindow(tv_purchase_date_exchange) || tv_purchase_date_exchange.isActivated())) {
            tv_purchase_date_exchange.setText(String.format(Locale.getDefault(), "%d-%d-%d", (dayOfMonth), (monthOfYear + 1), year));
            selectedYear = String.format(Locale.getDefault(), "%d-%d-%d", dayOfMonth, monthOfYear+1, year);
        }else if(tv_dob !=null && (ViewCompat.isAttachedToWindow(tv_dob)  || tv_dob.isActivated())){
            tv_dob.setText(String.format(Locale.getDefault(), "%d-%d-%d", (dayOfMonth), (monthOfYear + 1), year));
            selectedDOB = String.format(Locale.getDefault(), "%d-%d-%d", dayOfMonth, monthOfYear+1, year);
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {

        if (tv_evaluation_time != null) {
            Calendar calDate = (Calendar) tv_final_evaluation_schedule.getTag(R.id.autoninja_date);
            if(calDate != null) {
                calDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calDate.set(Calendar.MINUTE, minute);
                calDate.set(Calendar.SECOND, second);
                tv_evaluation_time.setTag(R.id.autoninja_date, calDate);
                tv_evaluation_time.setText(String.format(Locale.getDefault(), "%s", Util.getAmPmTime(calDate)));
                selectedEvaluationTime = Util.getSQLDateTime(calDate.getTime()).split("\\.")[0];
            }
        }
        if(followUpTime != null && tv_final_followup != null){
            System.out.println("CustomDialogs is called "+5);
            Calendar calDate = (Calendar) tv_final_followup.getTag(R.id.autoninja_date);
            if(calDate != null) {
                calDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calDate.set(Calendar.MINUTE, minute);
                calDate.set(Calendar.SECOND, second);
                followUpTime.setTag(R.id.autoninja_date, calDate);
                followUpTime.setText(String.format(Locale.getDefault(), "%s", Util.getAmPmTime(calDate)));
                selectedFollowUpTime = Util.getSQLDateTime(calDate.getTime()).split("\\.")[0];
            }
        }
        if(expectedDeliveryDateTv!=null){
            Calendar calDate = (Calendar) expectedDeliveryDateTv.getTag(R.id.autoninja_date);
            if(calDate != null) {
                calDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calDate.set(Calendar.MINUTE, minute);
                calDate.set(Calendar.SECOND, second);
                expectedDeliveryDateTv.setTag(R.id.autoninja_date, calDate);
                expectedDeliveryDateTv.setText(String.format(Locale.getDefault(), "%d %s %d", calDate.get(Calendar.DAY_OF_MONTH),
                        calDate.getDisplayName(Calendar.MONTH, Calendar.LONG,
                                Locale.getDefault()).toUpperCase().substring(0, 3), calDate.get(Calendar.YEAR)));
                expectedDeliveryDateTv.append(String.format(Locale.getDefault(), ", %s", Util.getAmPmTime(calDate)));
                selectedExpectedDeliveryDate = Util.getSQLDateTime(calDate.getTime()).split("\\.")[0];
            }
        }
        if(txt_resceduleDate!=null){
            Calendar calDate = (Calendar) txt_resceduleDate.getTag(R.id.autoninja_date);
            if(calDate != null) {
                calDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calDate.set(Calendar.MINUTE, minute);
                calDate.set(Calendar.SECOND, second);
                txt_resceduleDate.setTag(R.id.autoninja_date, calDate);
                txt_resceduleDate.setText(String.format(Locale.getDefault(), "%d %s %d", calDate.get(Calendar.DAY_OF_MONTH),
                        calDate.getDisplayName(Calendar.MONTH, Calendar.LONG,
                                Locale.getDefault()).toUpperCase().substring(0, 3), calDate.get(Calendar.YEAR)));
                txt_resceduleDate.append(String.format(Locale.getDefault(), ", %s", Util.getAmPmTime(calDate)));
                selectedRescheduleDate = Util.getSQLDateTime(calDate.getTime()).split("\\.")[0];
            }
        }


    }


    /**
     * handling selection of a color from the dropdown
     *
     * @param colorID
     */
    private void handleColorList(String colorID) {
        colorsNameList = CarsDBHandler.getInstance().getColors(realm, allInterestedCar.getIntrestedCarModelId(), selectedVariantId);
        if(colorsNameList!=null || colorsNameList.length!=0){
            spinnerColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }

                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (colorIdsList.size() > 0) {
                        selectedColorId = colorIdsList.get(i);
                        selectedColor = colorsNameList[i];
                    }
                }
            });

            ArrayAdapter<String> carColorAdapter = new ArrayAdapter<String>(activity, R.layout.spinner_item, colorsNameList);
            carColorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerColor.setAdapter(carColorAdapter);
            int selectedPos = colorIdsList.indexOf(colorID);
            if(selectedPos == -1){
                spinnerColor.setSelection(0);
            }else{
                spinnerColor.setSelection(selectedPos);
            }
            carColorAdapter.notifyDataSetChanged();
            spinnerColor.setSelection(0);

        }


    }

    /**
     * @param car_fuel_type_name_value
     */
    private void handleFuelList(final AutoCompleteTextView car_fuel_type_name_value) {
        car_fuel_type_name_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(car_fuel_type_name_value.getText())) {
                    car_fuel_type_name_value.showDropDown();

                }
            }
        });
        car_fuel_type_name_value.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!TextUtils.isEmpty(car_fuel_type_name_value.getText())) {
                    car_fuel_type_name_value.showDropDown();
                }
                return false;
            }
        });
        fuelNamesList = CarsDBHandler.getInstance().getFuelList(realm, allInterestedCar.getIntrestedCarModelId(), selectedVariantId);
        ArrayAdapter<String> fuelAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, fuelNamesList);
        car_fuel_type_name_value.setAdapter(fuelAdapter);
        car_fuel_type_name_value.setThreshold(0);

        car_fuel_type_name_value.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && TextUtils.isEmpty(car_fuel_type_name_value.getText()) && car_fuel_type_name_value.getAdapter() != null) {
                    car_fuel_type_name_value.showDropDown();

                }
            }
        });
        fuelAdapter.notifyDataSetChanged();
        car_fuel_type_name_value.setSelection(0);
        if(fuelNamesList!=null && fuelNamesList.length>0) {
            car_fuel_type_name_value.setText(fuelNamesList[0]);
        }
    }

}
