package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 25-08-2016.
 */
public class IntrestedCarActivityGroupStages extends RealmObject {

    public static final String INTRESTEDCARACTIVITYGROUPSTAGES = "IntrestedCarActivityGroupStages";

    private String intrestedCarActivityStageID;
    private long leadId;
    private String intrestedCarStageName;

    private String intrestedCarStageWidth;
    private String intrestedCarStageBarClass;

    public String getIntrestedCarActivityStageID() {
        return intrestedCarActivityStageID;
    }

    public void setIntrestedCarActivityStageID(String intrestedCarActivityStageID) {
        this.intrestedCarActivityStageID = intrestedCarActivityStageID;
    }

    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public String getIntrestedCarStageName() {
        return intrestedCarStageName;
    }

    public void setIntrestedCarStageName(String intrestedCarStageName) {
        this.intrestedCarStageName = intrestedCarStageName;
    }

    public String getIntrestedCarStageWidth() {
        return intrestedCarStageWidth;
    }

    public void setIntrestedCarStageWidth(String intrestedCarStageWidth) {
        this.intrestedCarStageWidth = intrestedCarStageWidth;
    }

    public String getIntrestedCarStageBarClass() {
        return intrestedCarStageBarClass;
    }

    public void setIntrestedCarStageBarClass(String intrestedCarStageBarClass) {
        this.intrestedCarStageBarClass = intrestedCarStageBarClass;
    }

}
