import { realm } from './realm_schemas';
import { ActvityModel } from '../models/lead_elements_model'



const ActivityDBService = {
  findAll: function() {
    return realm.objects('ActvitiyDB');
  },
  save: function(activities) {
    realm.write(() => {
      activities.map((activity)=> {
        realm.create('ActvitiyDB', activity, true);
      })
    })
    let actvitiesDb = realm.objects('ActvitiyDB');
    console.log('actvities.length', actvitiesDb.length);
  },
  getActivities: () => {
    let actvities = [];
    let  activitiesDB =  realm.objects('ActvitiyDB');
    if(activitiesDB) {
      activitiesDB.map((activityDB)=> {
        actvities.push(new ActvityModel(activityDB.id, activityDB.name));
      })
    }
    return actvities;

  },
  deleteAll: function() {
    realm.write(() => {
      realm.delete(realm.objects('ActvitiyDB'));
    })
  },
}

export default ActivityDBService;
