import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Alert } from 'react-native';
import { FAB } from 'react-native-paper';
export default class TotalETVBRL extends Component{
    constructor(props) {
        super(props);
    }
    onLongPress() {
        if(this.props.showPercentage) {
            Alert.alert('Long Disabled');
        }
        else {
            Alert.alert('Long Enabled');
        }
    }
    render() {
        let title = "Total";
        if(this.props.title) {
            title = this.props.title;
        }
        let data = this.props.data;
        let e,t,v,b,r,l;
        if(data) {
            if(this.props.showPercentage) {
                e = data.rel.e;
                t = data.rel.t;
                v = data.rel.v;
                b = data.rel.b;
                r = data.rel.r;
                l = data.rel.l;
            }
            else {
                e = data.abs.e;
                t = data.abs.t;
                v = data.abs.v;
                b = data.abs.b;
                r = data.abs.r;
                l = data.abs.l;
            }
    
        }
       
        return(
            <View style={{margin:2}}>
                <View style={styles.containerWhite}>
                    <View style={styles.itemEmpty}>
                        <Text style={styles.itemHeader}>{title}</Text>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{e}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{t}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{v}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{b}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{r}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={[styles.itemText, {color:'red'}]}>{l}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>

                 
                </View>
            </View>
        )
    }
}

let styles = StyleSheet.create({
    containerWhite: {
        backgroundColor:'#fff',
        paddingTop:8,
        paddingBottom:8,
        paddingLeft:2,
        paddingRight:2,
        flexDirection:'row',
    },
    itemEmpty: {
        flex:1.5,
        alignSelf:'center',
    },
    item: {
        flex:1,
        alignSelf:'center',
    },
    itemText: {
        fontSize:14,
        color:'#1B075A',
        alignSelf:'center',
        textAlign:'center'
    },
    itemHeader: {
        fontSize:15,
        fontWeight:'bold',
        color:'#2D4F8B',
        alignSelf:'flex-start',
        paddingLeft:2,
       
    }
})