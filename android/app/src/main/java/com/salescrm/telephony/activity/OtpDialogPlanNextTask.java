package com.salescrm.telephony.activity;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.salescrm.telephony.BuildConfig;
import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.interfaces.TaskOtpResultListener;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.model.FormSubmissionInputDataServer;
import com.salescrm.telephony.model.GetOtpForPNT;
import com.salescrm.telephony.model.OtpValidationPnt;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.HereReverseGeoResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;


public class OtpDialogPlanNextTask {

    private Activity context;
    private TaskOtpResultListener listener;
    private Dialog dialog;
    private Button bt_form_action;
    private EditText mOtpOneField, mOtpTwoField, mOtpThreeField, mOtpFourField;
    private RelativeLayout rLoadingFrame;
    private FormSubmissionInputData formSubmissionInputData;
    private int activityId;
    private Preferences pref;
    private ImageView imgBackButtonPnt;
    private TextView tvCustomersNumber;
    private String customersPhoneNumber = "";
    private TextView tvUserLocation;
    private ProgressBar progressBar;
    private boolean fromPostBooking;

    private MapView mapView;
    private GoogleMap googleMap = null;
    private String readableActivityName;
    private Location currentLocation = null;
    private String currentAddress = "";
    private View.OnClickListener userLocationClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progressBar.setVisibility(View.VISIBLE);
            requestAddressFromLatLong(currentLocation);
        }
    };


    public OtpDialogPlanNextTask(Activity context, boolean fromPostBooking, String primaryNumber, int activityId, FormSubmissionInputData formSubmissionInputData, TaskOtpResultListener listener) {
        this.context = context;
        this.listener = listener;
        this.formSubmissionInputData = formSubmissionInputData;
        this.activityId = activityId;
        this.customersPhoneNumber = primaryNumber;
        this.fromPostBooking = fromPostBooking;
        this.readableActivityName = getActivityNameReadable(activityId);
    }

    private String getActivityNameReadable(int activityId) {
        switch (activityId) {
            case WSConstants.TaskActivityName.HOME_VISIT_ID:
                return "Home Visit done at";
            case WSConstants.TaskActivityName.SHOWROOM_VISIT_ID:
                return "Showroom Visit done at";
            case WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID:
                return "Test Drive Home taken at";
            case WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID:
                return "Test Drive Showroom taken at";
        }
        return "Task taken at";
    }

    public void show() {
        dialog = new Dialog(context, R.style.AppTheme_Black);

        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
        }

        pref = Preferences.getInstance();
        dialog.setContentView(R.layout.otp_check_for_plan_next_task);
        mapView = dialog.findViewById(R.id.user_location_map_view);
        progressBar = dialog.findViewById(R.id.progress_bar_location_update);
        bt_form_action = dialog.findViewById(R.id.bt_form_action);
        mOtpOneField = dialog.findViewById(R.id.otp_one_edit_text);
        mOtpTwoField = dialog.findViewById(R.id.otp_two_edit_text);
        mOtpThreeField = dialog.findViewById(R.id.otp_three_edit_text);
        mOtpFourField = dialog.findViewById(R.id.otp_four_edit_text);
        imgBackButtonPnt = dialog.findViewById(R.id.back_button_pnt);
        tvCustomersNumber = dialog.findViewById(R.id.tv_phone_number);
        tvCustomersNumber.setText("OTP has been sent to " + customersPhoneNumber);
        ((TextView) dialog.findViewById(R.id.tv_activity_name_readable)).setText(readableActivityName + ",");

        tvUserLocation = dialog.findViewById(R.id.tv_user_location);
//        tvMessage2 = (TextView) dialog.findViewById(R.id.tv_message_2);
//        String text = "<font color=#2A22E3>Ask customer</font> <font color=#000000>to call on</font> " +
//                "<font color=#2A22E3>080-45452520</font> <font color=#000000><br> from " + customersPhoneNumber + " to get the OTP</font>";
//        tvMessage2.setText(Html.fromHtml(text));

        bt_form_action.setEnabled(false);
        bt_form_action.setAlpha(0.5f);
        bt_form_action.setVisibility(View.VISIBLE);
        if (fromPostBooking) {
            bt_form_action.setText("Submit");
        } else {
            bt_form_action.setText("Plan Next Task");
        }

        //Initialize map
        initMap();

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        if (new ConnectionDetectorService(context).isConnectingToInternet()) {
            requestToServerForOtp();
        } else {
            Toast.makeText(context, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
        }

        bt_form_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkOtpFormatAndValidate();
            }
        });
        imgBackButtonPnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null)
                    dialog.dismiss();
            }
        });


        mOtpOneField.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {

                }
                return false;
            }
        });

        mOtpTwoField.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (mOtpTwoField.getText().toString().equalsIgnoreCase("")) {
                        mOtpOneField.requestFocus();
                    }
                }

                return false;
            }
        });

        mOtpThreeField.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (mOtpThreeField.getText().toString().equalsIgnoreCase("")) {
                        mOtpTwoField.requestFocus();
                    }
                }
                return false;
            }
        });

        mOtpFourField.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (mOtpFourField.getText().toString().equalsIgnoreCase("")) {
                        mOtpThreeField.requestFocus();
                    }
                }
                return false;
            }
        });
        mOtpOneField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mOtpOneField.getText().toString().equalsIgnoreCase("")) {
                    mOtpTwoField.requestFocus();
                }
            }
        });

        mOtpTwoField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mOtpTwoField.getText().toString().equalsIgnoreCase("")) {
                    mOtpThreeField.requestFocus();
                }
            }
        });

        mOtpThreeField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mOtpThreeField.getText().toString().equalsIgnoreCase("")) {
                    mOtpFourField.requestFocus();
                }
            }
        });
    }

    private void initMap() {
/*        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            System.out.println("Location Updated Requested");
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
            fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    userLocation = location;
                    if (location != null) {
                        Log.d("LOCATION", "Location Updated Received");
                        Log.d("LOCATION", "Latitude:" + userLocation.getLatitude());
                        Log.d("LOCATION", "Longitude:" + userLocation.getLongitude());

                    }
                }
            }
            ).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d("LOCATION", "Location Failed");
                }
            });
        }*/


        MapsInitializer.initialize(context);
        GoogleMapOptions options = new GoogleMapOptions().liteMode(true);
        mapView.onCreate(dialog.onSaveInstanceState());
        mapView.onResume();
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap map) {
                googleMap = map;
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setAllGesturesEnabled(false);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(20.5937, 78.9629), 3));
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        return true;
                    }
                });
            }
        });
    }

    private void requestToServerForOtp() {
        ApiUtil.GetRestApiWithHeader(Preferences.getAccessToken()).getOtpForPNT("" + activityId, formSubmissionInputData.getLead_id(), new Callback<GetOtpForPNT>() {
            @Override
            public void success(GetOtpForPNT getOtpForPNT, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }
                try {
                    if (getOtpForPNT != null && getOtpForPNT.getResult() != null && getOtpForPNT.getResult().getPrimaryNumber() != null) {
                        if (dialog.isShowing()) {
                            tvCustomersNumber.setText(String.format("OTP has been sent to %s", getOtpForPNT.getResult().getPrimaryNumber()));
                        }

                    }
                } catch (Exception ignored) {

                }

            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse() != null) {
                    Util.showToast(context, "Server not responding - " + error.getResponse().getStatus(), Toast.LENGTH_SHORT);
                }
            }
        });
    }

    private void checkOtpFormatAndValidate() {
        if (mOtpOneField.getText().toString().equalsIgnoreCase("") && mOtpTwoField.getText().toString().equalsIgnoreCase("") ||
                mOtpThreeField.getText().toString().equalsIgnoreCase("") || mOtpFourField.getText().toString().equalsIgnoreCase("")) {
            Util.showToast(context, "Please enter complete OTP", Toast.LENGTH_SHORT);
        } else {
            String otp = mOtpOneField.getText().toString() + mOtpTwoField.getText().toString()
                    + mOtpThreeField.getText().toString() + mOtpFourField.getText().toString();
            if (new ConnectionDetectorService(context).isConnectingToInternet()) {
                validatePntOtp(otp);
            } else {
                Toast.makeText(context, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void validatePntOtp(final String otp) {
        int iOtp = Integer.parseInt(otp);
        ApiUtil.GetRestApiWithHeader(Preferences.getAccessToken()).validatePNTotp(formSubmissionInputData.getLead_id(), iOtp, new Callback<OtpValidationPnt>() {
            @Override
            public void success(OtpValidationPnt otpValidationPnt, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if (("" + otpValidationPnt.getStatusCode()).equals(WSConstants.RESPONSE_OK)) {
                    FormSubmissionInputDataServer.LocationData locationData = new FormSubmissionInputDataServer(). new LocationData();
                    if(currentLocation!=null) {
                        locationData.setLatitude(currentLocation.getLatitude()+"");
                        locationData.setLongitude(currentLocation.getLongitude()+"");
                    }
                    locationData.setLocality(currentAddress);

                    listener.onValidatingTaskOtp(true, otpValidationPnt.getResult().getSuccess(), locationData);
                    dialog.dismiss();
                } else {
                    Util.showToast(context, otpValidationPnt.getError().getDetails() + "", Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse() != null) {
                    Util.showToast(context, error.getResponse().getReason() + " - " + error.getResponse().getReason() + "", Toast.LENGTH_SHORT);
                }
            }
        });
    }

    void updateLocation(Location location) {
        if (googleMap != null && location != null) {
            if (currentLocation == null
                    || currentLocation.getLatitude() != location.getLatitude()
                    || currentLocation.getLongitude() != location.getLongitude()
            ) {
                currentLocation = location;
                setMapMarker(location);
                requestAddressFromLatLong(currentLocation);
            }

        }
    }

    private void setMapMarker(Location location) {
        new Handler(context.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                LatLng position = new LatLng(location.getLatitude(), location.getLongitude()); ////your lat lng
                googleMap.clear();
                googleMap.addMarker(new MarkerOptions().position(position));
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(position));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 14));
                bt_form_action.setEnabled(true);
                bt_form_action.setAlpha(1f);
            }
        });

    }

    private void onAddressApiResponse( HereReverseGeoResponse hereReverseGeoResponse, boolean isSuccess) {
        new Handler(context.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                if (isSuccess && hereReverseGeoResponse != null && Util.isNotNull(hereReverseGeoResponse.getAddress())) {
                    tvUserLocation.setText(hereReverseGeoResponse.getAddress());
                    currentAddress = hereReverseGeoResponse.getAddress();
                    tvUserLocation.setOnClickListener(null);
                } else {
                    tvUserLocation.setText("Failed to get the address. Tap to retry");
                    tvUserLocation.setOnClickListener(userLocationClickListener);

                }
            }
        });
    }

    private void requestAddressFromLatLong(Location location) {
        ApiUtil.getHereMap().getReverseGeoCode(
                pref.getHereMapAppId(),
                pref.getHereMapAppCode(),
                9,
                1,
                BuildConfig.HERE_MAP_MODE_RETRIEVE_ADDRESSES,
                location.getLatitude() + "," + location.getLongitude(),
                new Callback<HereReverseGeoResponse>() {
                    @Override
                    public void success(HereReverseGeoResponse hereReverseGeoResponse, Response response) {
                        onAddressApiResponse(hereReverseGeoResponse, true);
                        if(hereReverseGeoResponse == null || !Util.isNotNull(hereReverseGeoResponse.getAddress())) {
                            pushCleverTapTelephony(location.getLatitude() + "," + location.getLongitude(), "NULL");
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        onAddressApiResponse(null, false);
                        pushCleverTapTelephony(location.getLatitude() + "," + location.getLongitude(),
                                error!=null&&error.getKind()!=null?"RETROFIT ERROR:"+error.getKind().toString():"ERROR");

                    }
                }
        );
    }
    private void pushCleverTapTelephony(String input, String response) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_API_ERROR_KEY_API, CleverTapConstants.EVENT_API_ERROR_API_HERE_MAP);
        hashMap.put(CleverTapConstants.EVENT_API_ERROR_KEY_INPUT, input);
        hashMap.put(CleverTapConstants.EVENT_API_ERROR_KEY_RESPONSE, response);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_API_ERROR, hashMap);

    }
}
