package com.salescrm.telephony.db.create_lead;

import io.realm.RealmObject;

/**
 * Created by bharath on 8/12/16.
 */

public class Ad_car_details extends RealmObject{
    private int brand_id;
    private String milage;

    private String kms_run;

    private String model_id;

    private boolean is_primary;

    private String purchase_date;

    private boolean is_activity_target;

    private String brand_name;
    private String model_name;

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public boolean is_primary() {
        return is_primary;
    }

    public boolean is_activity_target() {
        return is_activity_target;
    }

    public void setIs_activity_target(boolean is_activity_target) {
        this.is_activity_target = is_activity_target;
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public String getMilage() {
        return milage;
    }

    public void setMilage(String milage) {
        this.milage = milage;
    }

    public String getKms_run() {
        return kms_run;
    }

    public void setKms_run(String kms_run) {
        this.kms_run = kms_run;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public boolean getIs_primary() {
        return is_primary;
    }

    public void setIs_primary(boolean is_primary) {
        this.is_primary = is_primary;
    }

    public String getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(String purchase_date) {
        this.purchase_date = purchase_date;
    }

    @Override
    public String toString() {
        return "ClassPojo [milage = " + milage + ", kms_run = " + kms_run + ", model_id = " + model_id + ", is_primary = " + is_primary + ", purchase_date = " + purchase_date + "]";
    }
}