package com.salescrm.telephony.interfaces;

/**
 * Created by nndra on 13-Sep-17.
 */

public interface AudioCallbacks {

    void onUpdate(int percentage);

    void onPause();

    void onStop();

}
