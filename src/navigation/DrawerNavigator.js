import React, { Component } from 'react'
import { Dimensions } from 'react-native';
import { createDrawerNavigator } from 'react-navigation';
import Dashboard from '../containers/Dashboard'
import LeaderBoardStackNavigator from '../containers/LeaderBoard/leaderboard.navigationTop';
import SettingsRouter from '../containers/Settings/Main/settings.navigation';
import SlideMenu from '../components/slide_menu';
import LoginRedirector from '../components/login_redirector';
import UserInfoService from '../storage/user_info_service'
import EnquirySourceDBService from '../storage/enquiry_source_db_service'
import EventCategoriesDBService from '../storage/event_categories_db_service'
import { getColdVisitDashboardNavigator } from "../containers/ColdVisit/Dashboard/ColdVisitDashboard.navigation";
import AppConfDBService from '../storage/app_conf_db_service';
import { getTaskListNavigator } from '../containers/TasksList/Navigation/tasksList.navigation';
import VehicleDBService from '../storage/vehicle_db_service';
import TeamShuffleRouter from '../containers/TeamShuffle/team_shuffle_navigation'

const Home = createDrawerNavigator({
    LeaderBoard: {
        screen: LeaderBoardStackNavigator
    },
    TeamDashboard: {
        screen: props => <Dashboard {...props}
            isUserBH={UserInfoService.isUserBranchHead()}
            isUserOps={UserInfoService.isUserOperations()}
            isUserSM={UserInfoService.isUserSalesManager()}
            isUserEM={UserInfoService.isUserEvaluatorManager()}
            isUserEV={UserInfoService.isUserEvaluator()}
            isUserTL={UserInfoService.isUserTeamLead()}
            isUserSC={UserInfoService.isUserSalesConsultant()}
            allLocations={UserInfoService.getLocationsList()}
            enquirySourceCategories={EnquirySourceDBService.getEnquirySourceCategories()}
            eventCategories={EventCategoriesDBService.getEventCategories()}
            uniqueAcrossDealerShip={AppConfDBService.getVal('unique_across_dealer')}
            addLeadEnabled={AppConfDBService.getVal('addLeadEnabled')}
            coldVisitEnabled={AppConfDBService.getVal('cold_visits_enabled')}
            showLeadCompetitor={AppConfDBService.getVal('showLeadCompetitor')}
            vehcileModel={VehicleDBService.getInterestedVehicle()}
            enquirySource={EnquirySourceDBService.getEnquirySourceMain()}
            isClientILom={AppConfDBService.getVal('il_flow')}
            isClientILBank={AppConfDBService.getVal('il_bank_flow')}
            isClientTwoWheeler={AppConfDBService.getVal('two_wheeler_conf')}
            hotlineNo={AppConfDBService.getVal('hotlineNo')}
        />,
    },
    MyTasks: {
        screen: getTaskListNavigator(null, false, {
            uniqueAcrossDealerShip: AppConfDBService.getVal('unique_across_dealer'),
            addLeadEnabled: AppConfDBService.getVal('addLeadEnabled'),
            coldVisitEnabled: AppConfDBService.getVal('cold_visits_enabled'),
            showLeadCompetitor: AppConfDBService.getVal('showLeadCompetitor'),
            vehcileModel: VehicleDBService.getInterestedVehicle(),
            enquirySource: EnquirySourceDBService.getEnquirySourceMain(),
            isClientILom: AppConfDBService.getVal('il_flow'),
            isClientILBank: AppConfDBService.getVal('il_bank_flow'),
            isClientTwoWheeler: AppConfDBService.getVal('two_wheeler_conf'),
            hotlineNo: AppConfDBService.getVal('hotlineNo'),
        })
    },
    ColdVisitDashboard: {
        screen: getColdVisitDashboardNavigator(
            UserInfoService.isUserBranchHead() || UserInfoService.isUserOperations(),
            UserInfoService.isUserSalesManager(),
            UserInfoService.isUserTeamLead(),
            UserInfoService.isUserSalesConsultant(),
            {
                uniqueAcrossDealerShip: AppConfDBService.getVal('unique_across_dealer'),
                addLeadEnabled: AppConfDBService.getVal('addLeadEnabled'),
                coldVisitEnabled: AppConfDBService.getVal('cold_visits_enabled'),
                showLeadCompetitor: AppConfDBService.getVal('showLeadCompetitor')
            }

        )
    },

    TeamShuffle: {
        screen: TeamShuffleRouter,
    },

    Settings: {
        screen: SettingsRouter
    },
    Logout: {
        screen: LoginRedirector
    },
},
    {
        initialRouteName: 'TeamDashboard',
        contentComponent: SlideMenu,
        backBehavior: 'none',
        drawerWidth: (Dimensions.get('window').width) * .85,
        drawerBackgroundColor: "#2e3486",

    },
);

export default Home;
