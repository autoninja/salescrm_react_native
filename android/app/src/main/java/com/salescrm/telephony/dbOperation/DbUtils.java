package com.salescrm.telephony.dbOperation;

import android.os.Environment;

import com.google.gson.Gson;
import com.salescrm.telephony.db.AppConfigDB;
import com.salescrm.telephony.db.GameLocationDB;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.model.GenericItem;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.internal.IOException;

/**
 * Created by bharath on 5/10/17.
 */

public class DbUtils {

    /**
     * @return
     */
    public static String getRolesCombination() {
        Realm realm = Realm.getDefaultInstance();
        StringBuilder roles = new StringBuilder();
        roles.append("[");
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                roles.append(userRoles.get(i).getName());
                if (i < userRoles.size() - 1) {
                    roles.append(",");
                }
            }
        }
        roles.append("]");
        return roles.toString();
    }

    public static List<String> getRoles() {
        List<String> roles = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                roles.add(userRoles.get(i).getId());
            }
        }
        return roles;
    }

    public static List<String> getRolesName() {
        List<String> roles = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i) != null) {
                    roles.add(userRoles.get(i).getName());
                }
            }
        }
        return roles;
    }

    public static boolean isBike() {
        Realm realm = Realm.getDefaultInstance();
        AppConfigDB appConfigDB = realm.where(AppConfigDB.class).equalTo("key", WSConstants.AppConfig.BIKE_ACTIVITY_CONF).findFirst();
        if (appConfigDB != null && appConfigDB.getValue() != null) {
            if (appConfigDB.getValue().equalsIgnoreCase("1")) {
                return true;
            } else if (appConfigDB.getValue().equalsIgnoreCase("0")) {
                return false;
            }
        }
        return false;
    }

    public static boolean isAddLeadEnabled() {
        Realm realm = Realm.getDefaultInstance();
        AppConfigDB appConfigDB = realm.where(AppConfigDB.class).equalTo("key", WSConstants.AppConfig.ADD_LEAD_ENABLED).findFirst();
        if (appConfigDB != null && appConfigDB.getValue() != null) {
            if (appConfigDB.getValue().equalsIgnoreCase("true")) {
                return true;
            } else if (appConfigDB.getValue().equalsIgnoreCase("false")) {
                return false;
            }
        }
        return true;
    }

    public static boolean isUserOperations() {
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_OPERATIONS)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static List<String> getLocationsName() {
        List<String> locations = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<UserLocationsDB> data =
                realm.where(UserLocationsDB.class).distinct("location_id");
        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i) != null) {
                    locations.add(data.get(i).getName());
                }
            }
        }
        return locations;
    }

    public static List<Integer> getLocationsId() {
        List<Integer> locations = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        RealmResults<UserLocationsDB> data =
                realm.where(UserLocationsDB.class).distinct("location_id");
        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i) != null) {
                    locations.add(Util.getInt(data.get(i).getLocation_id()));
                }
            }
        }
        return locations;
    }

    public static boolean isGamificationEnabled() {
        Realm realm = Realm.getDefaultInstance();
        boolean gamificationEnabled = false;
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                Integer id = Util.getInt(userRoles.get(i).getId());
                if (id == WSConstants.USER_ROLES.DSE ||
                        id == WSConstants.USER_ROLES.TEAM_LEAD ||
                        id == WSConstants.USER_ROLES.MANAGER ||
                        id == WSConstants.USER_ROLES.BRANCH_HEAD ||
                        id == WSConstants.USER_ROLES.OPERATIONS) {
                    RealmResults<GameLocationDB> gameLocationDBS = realm.where(GameLocationDB.class).equalTo("gamification", true).findAll();
                    gamificationEnabled = gameLocationDBS.size() > 0;
                    break;
                }
            }
        }
        return gamificationEnabled;
    }


    public static RealmResults<GameLocationDB> getActiveGameLocations() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<GameLocationDB> gameLocationDBS = realm.where(GameLocationDB.class).equalTo("gamification", true).findAll();
        return gameLocationDBS;

    }

    public static List<String> getActiveGameLocationsNames() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<GameLocationDB> gameLocationDBS = realm.where(GameLocationDB.class).equalTo("gamification", true).findAll();

        List<String> gameLocationNames = new ArrayList<>();
        for (GameLocationDB gameLocationDB : gameLocationDBS) {
            gameLocationNames.add(gameLocationDB.getName());
        }
        return gameLocationNames;

    }

    public static boolean isClientTwoWheeler() {
        Realm realm = Realm.getDefaultInstance();
        AppConfigDB appConfigDB = realm.where(AppConfigDB.class).equalTo("key", WSConstants.AppConfig.TWO_WHEELER_CONF).findFirst();
        if (appConfigDB != null && appConfigDB.getValue() != null) {
            if (appConfigDB.getValue().equalsIgnoreCase("1")) {
                return true;
            } else if (appConfigDB.getValue().equalsIgnoreCase("0")) {
                return false;
            }
        }
        return false;
    }

    public static boolean isAppConfEnabled(String appConf) {
        Realm realm = Realm.getDefaultInstance();
        AppConfigDB appConfigDB = realm.where(AppConfigDB.class).equalTo("key", appConf).findFirst();
        if (appConfigDB != null && appConfigDB.getValue() != null) {
            if (appConfigDB.getValue().equalsIgnoreCase("1")) {
                return true;
            } else if (appConfigDB.getValue().equalsIgnoreCase("0")) {
                return false;
            }
        }
        return false;
    }

    public static boolean isShowLeadSourceCategory(){
        Realm realm = Realm.getDefaultInstance();
        AppConfigDB appConfigDB =realm.where(AppConfigDB.class).equalTo("key",WSConstants.AppConfig.SHOW_LEAD_SOURCE_CATEGORIES).findFirst();
        if(appConfigDB!=null){
            if(appConfigDB.getValue().equalsIgnoreCase("0")){
                return false;
            }
            else if(appConfigDB.getValue().equalsIgnoreCase("1")){
                return true;
            }
        }
        return false;
    }

    public static int getFirstCallAdvanceDate() {
        Realm realm = Realm.getDefaultInstance();
        AppConfigDB appConfigDB = realm.where(AppConfigDB.class).equalTo("key", WSConstants.AppConfig.FIRST_CALL_ADVANCE_DAYS).findFirst();
        if (appConfigDB != null) {
            return Util.getInt(appConfigDB.getValue());
        }
        return 0;
    }
    public static boolean isUserBranchHeadOperationsOrSM() {
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_BRANCH_HEAD)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_OPERATIONS)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLES.MANAGER+"")) {
                    return true;
                }
            }
        }
        return false;
    }
    public static boolean isUserBranchHead() {
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_BRANCH_HEAD)) {
                    return true;
                }
            }
        }
        return false;
    }
    public static boolean isUserSalesManager() {
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLES.MANAGER+"")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isUserEvaluatorManager() {
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR_MANAGER+"")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isUserTeamLead() {
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.TEAM_LEAD_ROLE_ID+"")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isUserSalesConsultant() {
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.DSE_ROLE_ID+"")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isUserVarifier() {
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_VERIFIER+"")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isUserEvaluator() {
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR+"")) {
                    return true;
                }
            }
        }
        return false;
    }
    public static boolean isUserVerifier() {
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_VERIFIER+"")) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean exportRealm() {
        final Realm realm = Realm.getDefaultInstance();
        try {
            final File file = new File(Environment.getExternalStorageDirectory().getPath().concat("/sample.realm"));
            if (file.exists()) {
                //noinspection ResultOfMethodCallIgnored
                file.delete();
            }

            realm.writeCopyTo(file);
            return true;
        } catch (IOException e) {
            realm.close();
            e.printStackTrace();
            return false;
        }

    }
    public static String getRolesJSON() {
        List<GenericItem> roles = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                roles.add(new GenericItem(userRoles.get(i).getId(),userRoles.get(i).getName()));
            }
        }
        return new Gson().toJson(roles);
    }
}
