package com.salescrm.telephony.preferences;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import com.salescrm.telephony.utils.WSConstants;

/**
 * Created by Ravindra P on 27-11-2015.
 */
public class Preferences {
    private static Context mContext;
    private static SharedPreferences mPref;
    private static SharedPreferences.Editor mEditor;
    private static Preferences mInstance;
    private static boolean mIsLogedIn = false;
    private static boolean dealershipSelected = false;
    private static int dealershipSelectedId = 0;
    private static String mAccessToken = "";
    private static String mLeadLastUpdated = "";
    private static String mUserMobile = "";
    private static boolean mIsFirstTime = true;
    private static String mTokenExpiresIn;
    private static String mLeadID;
    private static int mScheduledActivityId;
    // private String mGcmID;
    private static String mUserName;
    private static String mPassword;
    private static String mDseName;
    private static int mCrmType;
    private static String mBrand_id;
    private static String mAddSearchNumber;
    private static String mFcmId = "";
    private static boolean mIsFcmTableCreated;
    private static int mFcmSync;
    private static long mAppLastOpenedDate;
    private static boolean mRefreshOfflineData;
    private static String mDealerName;
    private static String mDealerId;
    private static boolean mActionPlanSynced;
    private static boolean mSyncEnabled;
    private static String mImeiNumber;
    private static int mGameLocationId;
    private static String mGameLocationName;
    private static boolean mIsC360Destroyed = false;

    private static String mRole;
    private static String mRoleId;
    private static String mEmail;
    private static String mName;
    private static String mUserId;
    private static long mBasicOfflineLastUpdated;
    private static int mVersionCode;
    private static String mVersionName;
    private static int leadListCount;
    private static int mCloseLeadListCount;
    private static int assignedListCount;

    //for telephonyModule
    private static int mCallState = 0;
    private static String mInOut = "";
    private static String mCallingNumber="";
    private static String mDbUuid;
    private static String mRecordingType;
    private static Boolean mIsCallHappened = false;
    private static int mIncomingCalls, mOutgoingCalls;
    private static String fileName, mFilePath;
    private static String mCallId;
    private static int mEnableMultithread = 0;
    private static boolean mLimitSync = false;
    private static boolean backgroundThread = false;
    private static boolean myLeadViewed = false;
    private static String mStart = "";
    private static String mEnd = "";
    private static long mDuration = 0;
    private static int mMaxID = 0;
    // private static int mSimSlot = 0;
    private static String mTelephonyLeadID = "0";
    private static int lastVisitedTab = 0;
    private static boolean isFabricsNeeded = false;
    private static boolean isTelephonyNeeded = true;
    private static boolean isCallOnGoing = true;
    private static String appUserId;
    private static String currentDseId;
    private static String teamId;
    private static String mSheduleActivityID;
    private static int mTargetMonth=0;
    private static int mTargetLocationID=0;
    private static String newUpdateNotice;
    private static String dpUri = "";
    private static String dpUserName = "";
    private static boolean dpTakenFromDevice = false;
    private static String encodedUri = "";
    private static int locationId = 0;
    private static String startdateEtvbr = "";
    private static String endDateEtvbr = "";
    private static String showDateEtvbr = "";
    private static int mLeaderBoardTab = 0;
    private static boolean mshowGamification = false;
    private static String mGamificationTeamLeadSpinWheelData = "";
    private static boolean showBalloonAnimation =false;
    private static boolean reloadC360 =false;
    private static String proformaPdfLink = "";
    private static boolean usersTopRole = false;
    private static boolean proformaSent = false;
    private static boolean gameIntroSeen = false;
    private static boolean boosterMode = false;
    private static boolean rankVisible = false;
    private static String userProfilePicUrl = "";
    private static boolean showPostBooking =false;
    private static String selectedGameDate = "";
    private static String systemDate = "";

    private static String acrLicenseKey;
    private static String acrSerial;

    private static String deviceId;

    private static boolean isDeviceSupportedForNLL;

    private static String s3UserName;
    private static String s3Password;
    private static String s3BucketName;
    private static String s3SessionToken;
    private static boolean isEventDashboardUpdate;
    private static String govtApiKeys;
    private static String buyerType;
    private static String hereMapAppId;
    private static String hereMapAppCode;
    private static boolean twoWheelerClient = false;
    private static boolean clientILom = false;
    private static boolean clientILBank = false;
    private static boolean screenshotEnabled = false;
    private static String hotlineNumber = "";
    private static String totalTaskCount = "";
    private static String callingLeadId="";
    private static long callStatCreatedAt;



    public Preferences() {
    }

    public static synchronized Preferences getInstance() {
        if (mInstance == null)
            mInstance = new Preferences();
        return mInstance;
    }

    public static void load(Context context) {
        mContext = context;
        mPref = mContext.getSharedPreferences(WSConstants.PREF_NAME, Activity.MODE_PRIVATE);
        mEditor = mPref.edit();


        mSheduleActivityID = mPref.getString(WSConstants.PROPERTY_SHEDULE_ACTIVITY_ID, "");
        isTelephonyNeeded = mPref.getBoolean(WSConstants.PROPERTY_TELEPHONY_NEEDED, false);
        isCallOnGoing = mPref.getBoolean(WSConstants.PROPERTY_IS_CALL_ONGOING, false);
        isFabricsNeeded = mPref.getBoolean(WSConstants.PROPERTY_FABRICS_NEEDED, true);
        mIsC360Destroyed = mPref.getBoolean(WSConstants.PROPERTY_IS_C360_DESTROYED, false);
        mImeiNumber = mPref.getString(WSConstants.PROPERTY_IMEI_NUMBER, "");
        mGameLocationId = mPref.getInt(WSConstants.LOCATION_GAME_ID,-1);
        mGameLocationName = mPref.getString(WSConstants.LOCATION_GAME_NAME,"");
        mDealerId = mPref.getString(WSConstants.PROPERTY_DEALER_ID, "");
        mDealerName = mPref.getString(WSConstants.PROPERTY_DEALER_NAME, "test");
        mLeadID = mPref.getString(WSConstants.PROPERTY_LEAD_ID, "");
        mScheduledActivityId = mPref.getInt(WSConstants.PROPERTY_SCHEDULED_ACTIVITY_ID, -1);
        mBrand_id = mPref.getString(WSConstants.PROPERTY_BRAND_ID, "");
        mIsLogedIn = mPref.getBoolean(WSConstants.PROPERTY_LOGGED_STATUS, false);
        dealershipSelected = mPref.getBoolean(WSConstants.PROPERTY_DEALER_SELECTED_STATUS, false);
        dealershipSelectedId = mPref.getInt(WSConstants.PROPERTY_DEALER_SELECTED_ID, 0);
        mIsFirstTime = mPref.getBoolean(WSConstants.PROPERTY_FIRST_TIME, true);
        mAccessToken = mPref.getString(WSConstants.PROPERTY_ACCESS_TOKEN, "");
        mUserName = mPref.getString(WSConstants.PROPERTY_USER_NAME, "");
        mPassword = mPref.getString(WSConstants.PROPERTY_PASSWORD, "");
        mUserMobile = mPref.getString(WSConstants.PROPERTY_USER_MOBILE, "");
        //mGcmID = pref.getString(WSConstants.GCM_ID, "");
        mLeadLastUpdated = mPref.getString(WSConstants.PROPERTY_LEAD_LEAST_UPDATED, "");
        mCrmType = mPref.getInt(WSConstants.PROPERTY_CRM_TYPE, 0);
        mDseName = mPref.getString(WSConstants.PROPERTY_DSE_NAME, "");
        mAddSearchNumber = mPref.getString(WSConstants.PROPERTY_ADD_LEAD_NUMBER, "");

        mFcmId = mPref.getString(WSConstants.FCM_ID, "");
        mIsFcmTableCreated = mPref.getBoolean(WSConstants.IS_FCM_TABLE_CREATED, false);
        mRefreshOfflineData = mPref.getBoolean(WSConstants.REFRESH_OFFLINE_DATA, true);
        mFcmSync = mPref.getInt(WSConstants.FCM_SYNC_VALUE, 0);

        mAppLastOpenedDate = mPref.getLong(WSConstants.APP_LAST_OPENED_DATE, 0);
        mBasicOfflineLastUpdated = mPref.getLong(WSConstants.BASIC_OFFLINE_LAST_UPDATED, 0);
        mActionPlanSynced = mPref.getBoolean(WSConstants.IS_ACTION_PLAN_SYNC, true);
        mSyncEnabled = mPref.getBoolean(WSConstants.IS_SYNC_ENABLED, true);
        mVersionCode = mPref.getInt(WSConstants.APP_VERSION_CODE, 0);
        mVersionName = mPref.getString(WSConstants.APP_VERSION_NAME, "");
        leadListCount = mPref.getInt(WSConstants.APP_LEAD_LIST_COUNT, 0);
        mCloseLeadListCount = mPref.getInt(WSConstants.APP_CLOSED_LEAD_LIST_COUNT, 0);
        assignedListCount = mPref.getInt(WSConstants.APP_ASSIGNED_LEAD_LIST_COUNT, 0);


        //for telephonyModule
        mCallState = mPref.getInt(WSConstants.CALL_STATE, 0);
        mInOut = mPref.getString(WSConstants.IN_OUT, "");
        mCallingNumber = mPref.getString(WSConstants.CALLED_MOBILE_NUMBER, "");
        mDbUuid = mPref.getString(WSConstants.DB_ID, "");
        mRecordingType = mPref.getString(WSConstants.RECORDING_TYPE, "3gp");
        mCallId = mPref.getString(WSConstants.CALL_ID, "");
        mIsCallHappened = mPref.getBoolean(WSConstants.CALL_HAPPENED, false);
        mIncomingCalls = mPref.getInt(WSConstants.INCOMING_CALLS, 0);
        mOutgoingCalls = mPref.getInt(WSConstants.OUTGOING_CALLS, 0);
        mFilePath = mPref.getString(WSConstants.FILE_PATH, "");
        fileName = mPref.getString(WSConstants.FILE_NAME, "");
        mEnableMultithread = mPref.getInt(WSConstants.ENABLE_MULTI_THREAD, 0);
        mLimitSync = mPref.getBoolean(WSConstants.LIMIT_SYNC, false);
        mStart = mPref.getString(WSConstants.CALL_START, "");
        mEnd = mPref.getString(WSConstants.CALL_END, "");
        mDuration = mPref.getLong(WSConstants.CALL_DURATION, 0);
        mMaxID = mPref.getInt(WSConstants.MAX_ID, 0);
        //mSimSlot = mPref.getInt(WSConstants.SIM_SLOT,0);
        mTelephonyLeadID = mPref.getString(WSConstants.TELEPHONY_LEAD_ID, "0");
        mUserId = mPref.getString(WSConstants.PROPERTY_USER_ID, "0");
        lastVisitedTab = mPref.getInt(WSConstants.LAST_VISITED_TAB, 0);
        myLeadViewed = mPref.getBoolean(WSConstants.PROPERTY_MY_LEAD_VIEWED, false);
        appUserId = mPref.getString(WSConstants.APP_USER_ID, null);
        currentDseId = mPref.getString(WSConstants.APP_CLICKED_DSE_ID, null);
        teamId = mPref.getString(WSConstants.APP_TEAM_ID_ID, null);
        mRole = mPref.getString(WSConstants.PROPERTY_ROLE, "");
        mRoleId = mPref.getString(WSConstants.PROPERTY_ROLE_ID, "");
        mTargetMonth = mPref.getInt(WSConstants.PROPERTY_TARGET_MONTH,0);
        mLeaderBoardTab = mPref.getInt(WSConstants.PROPERTY_LEADERBOARD_SELECTED_TAB,0);
        mTargetLocationID = mPref.getInt(WSConstants.PROPERTY_TARGET_LOCATION, 0);
        newUpdateNotice = mPref.getString(WSConstants.PROPERTY_NEW_UPDATE_NOTICE,"Bug Fixes and Performance Improvement ");
        dpUri = mPref.getString(WSConstants.DP_URI, "");
        dpUserName = mPref.getString(WSConstants.DP_USER_NAME, "");
        dpTakenFromDevice = mPref.getBoolean(WSConstants.DP_TAKEN_FROM_DEVICE, false);
        encodedUri = mPref.getString(WSConstants.ENCODED_URI, "");
        locationId = mPref.getInt(WSConstants.MY_LOCATION_KEY, 0);
        startdateEtvbr = mPref.getString(WSConstants.START_DATE_ETVBR, "");
        endDateEtvbr = mPref.getString(WSConstants.END_DATE_ETVBR, "");
        showDateEtvbr = mPref.getString(WSConstants.SHOW_DATE_ETVBR, "");
        mshowGamification = mPref.getBoolean(WSConstants.SHOW_GAMIFICATION, false);
        mGamificationTeamLeadSpinWheelData = mPref.getString(WSConstants.GAMIFICATION_TEAM_LEAD_SPIN_WHEEL_DATA, "");
        proformaPdfLink = mPref.getString(WSConstants.PROFORMA_PDF_LINK, "");
        usersTopRole = mPref.getBoolean(WSConstants.PROFORMA_ENABLED, false);
        proformaSent = mPref.getBoolean(WSConstants.PROFORMA_SENT, false);
        gameIntroSeen = mPref.getBoolean(WSConstants.IS_GAME_INTRO_SEEN, false);
        boosterMode = mPref.getBoolean(WSConstants.BOOSTER_MODE, false);
        rankVisible = mPref.getBoolean(WSConstants.RANK_VISIBILITY, false);
        userProfilePicUrl = mPref.getString(WSConstants.PROPERTY_USER_PROFILE_PIC_URL,"");
        showPostBooking = mPref.getBoolean(WSConstants.PROPERTY_SHOW_POST_BOOKING_TD_VISIT,false);
        systemDate = mPref.getString(WSConstants.PROPERTY_SYSTEM_DATE,System.currentTimeMillis()+"");
        selectedGameDate = mPref.getString(WSConstants.PROPERTY_SELECTED_GAME_DATE,systemDate);

        acrLicenseKey = mPref.getString(WSConstants.PROPERTY_ACR_LICENSE_KEY,"8D5A8F1B55ADA698CC459ED45DEB9BF9" );
        acrSerial = mPref.getString(WSConstants.PROPERTY_ACR_SERIAL, "1538870400");

        deviceId = mPref.getString(WSConstants.PROPERTY_DEVICE_ID,"");

        isDeviceSupportedForNLL = mPref.getBoolean(WSConstants.PROPERTY_IS_DEVICE_SUPPORT_NLL, false);

        s3UserName = mPref.getString(WSConstants.PROPERTY_S3_USER_NAME, null);
        s3Password = mPref.getString(WSConstants.PROPERTY_S3_PASSWORD, null);
        s3BucketName = mPref.getString(WSConstants.PROPERTY_S3_BUCKET_NAME, null);
        s3SessionToken = mPref.getString(WSConstants.PROPERTY_S3_SESSION_TOKEN, null);
        isEventDashboardUpdate = mPref.getBoolean(WSConstants.PROPERTY_IS_EVENT_DASHBOARD_UPDATE_REQUIRED, false);
        govtApiKeys = mPref.getString(WSConstants.PROPERTY_GOVERNMENT_API_KEYS, null);
        buyerType = mPref.getString(WSConstants.BUYER_TYPE, null);

        hereMapAppId = mPref.getString(WSConstants.PROPERTY_HERE_MAP_APP_ID, null);
        hereMapAppCode = mPref.getString(WSConstants.PROPERTY_HERE_MAP_APP_CODE, null);

        twoWheelerClient = mPref.getBoolean(WSConstants.PROPERTY_TWO_WHEELER_CLIENT, false);

        clientILom = mPref.getBoolean(WSConstants.PROPERTY_I_LOM_CLIENT, false);

        clientILBank = mPref.getBoolean(WSConstants.PROPERTY_I_LOM_BANK, false);

        screenshotEnabled = mPref.getBoolean(WSConstants.PROPERTY_SCREEN_SHOT, false);

        hotlineNumber = mPref.getString(WSConstants.PROPERTY_HOT_LINE, "");
        totalTaskCount = mPref.getString(WSConstants.TOTAL_TASK_COUNT, "0");

        callingLeadId = mPref.getString(WSConstants.PROPERTY_CALLING_LEAD_ID, "");

        callStatCreatedAt = mPref.getLong(WSConstants.PROPERTY_CALL_STAT_CREATED_AT, 0);

    }

    public static String getmGamificationTeamLeadSpinWheelData() {
        return mGamificationTeamLeadSpinWheelData;
    }

    public static void setmGamificationTeamLeadSpinWheelData(String mGamificationTeamLeadSpinWheelData) {
        mEditor.putString(WSConstants.GAMIFICATION_TEAM_LEAD_SPIN_WHEEL_DATA, mGamificationTeamLeadSpinWheelData);
        mEditor.commit();
        Preferences.mGamificationTeamLeadSpinWheelData = mGamificationTeamLeadSpinWheelData;

        showBalloonAnimation = mPref.getBoolean(WSConstants.PROPERTY_SHOW_BALLOON_ANIMATION, false);
        reloadC360 = mPref.getBoolean(WSConstants.PROPERTY_RELOAD_C360, false);
    }

    public static boolean isBoosterMode() {
        return boosterMode;
    }

    public static void setBoosterMode(boolean boosterMode) {
        mEditor.putBoolean(WSConstants.BOOSTER_MODE, boosterMode);
        mEditor.commit();
        Preferences.boosterMode = boosterMode;
    }

    public static boolean isRankVisible() {
        return rankVisible;
    }

    public static void setRankVisible(boolean rankVisible) {
        mEditor.putBoolean(WSConstants.RANK_VISIBILITY, rankVisible);
        mEditor.commit();
        Preferences.rankVisible = rankVisible;
    }

    public static boolean isCallOnGoing() {
        return isCallOnGoing;
    }

    public static void setIsCallOnGoing(boolean isCallOnGoing) {
        mEditor.putBoolean(WSConstants.PROPERTY_IS_CALL_ONGOING, isCallOnGoing);
        mEditor.commit();
        Preferences.isCallOnGoing = isCallOnGoing;
    }

    public static boolean isProformaSent() {
        return proformaSent;
    }

    public static void setProformaSent(boolean proformaSent) {
        mEditor.putBoolean(WSConstants.PROFORMA_SENT, proformaSent);
        mEditor.commit();
        Preferences.proformaSent = proformaSent;
    }

    public static String getEncodedUri() {
        return encodedUri;
    }

    public static void setEncodedUri(String encodedUri) {
        mEditor.putString(WSConstants.ENCODED_URI, encodedUri);
        mEditor.commit();
        Preferences.encodedUri = encodedUri;
    }

    public static boolean isDpTakenFromDevice() {
        return dpTakenFromDevice;
    }

    public static void setDpTakenFromDevice(boolean dpTakenFromDevice) {
        mEditor.putBoolean(WSConstants.DP_TAKEN_FROM_DEVICE, dpTakenFromDevice);
        mEditor.commit();
        Preferences.dpTakenFromDevice = dpTakenFromDevice;
    }

    public static boolean isMshowGamification() {
        return mshowGamification;
    }

    public static void setMshowGamification(boolean showGamification) {
        mEditor.putBoolean(WSConstants.SHOW_GAMIFICATION, showGamification);
        mEditor.commit();
        Preferences.mshowGamification = showGamification;
    }

    public static String getDpUri() {
        return dpUri;
    }

    public static void setDpUri(String dpUri) {
        mEditor.putString(WSConstants.DP_URI, dpUri);
        mEditor.commit();
        Preferences.dpUri = dpUri;
    }

    public static String getDpUserName() {
        return dpUserName;
    }

    public static void setDpUserName(String dpUserName) {
        mEditor.putString(WSConstants.DP_USER_NAME, "");
        mEditor.commit();
        Preferences.dpUserName = dpUserName;
    }

    public static String getmSheduleActivityID() {
        return mSheduleActivityID;
    }

    public static void setmSheduleActivityID(String mSheduleActivityID) {
        mEditor.putString(WSConstants.PROPERTY_SHEDULE_ACTIVITY_ID, mSheduleActivityID);
        mEditor.commit();
        Preferences.mSheduleActivityID = mSheduleActivityID;
    }

    public static boolean isTelephonyNeeded() {
        return isTelephonyNeeded;
    }

    public static void setIsTelephonyNeeded(boolean isTelephonyNeeded) {
        mEditor.putBoolean(WSConstants.PROPERTY_TELEPHONY_NEEDED, isTelephonyNeeded);
        mEditor.commit();
        Preferences.isTelephonyNeeded = isTelephonyNeeded;
    }

    public static boolean isFabricsNeeded() {
        return isFabricsNeeded;
    }

    public static void setFabricsNeeded(boolean login) {
        mEditor.putBoolean(WSConstants.PROPERTY_FABRICS_NEEDED, login);
        mEditor.commit();
        isFabricsNeeded = login;
    }

    public static boolean isMyLeadViewed() {
        return myLeadViewed;
    }

    public static void setMyLeadViewed(boolean data) {
        mEditor.putBoolean(WSConstants.PROPERTY_MY_LEAD_VIEWED, data);
        mEditor.commit();
        myLeadViewed = data;
    }

    public static boolean isC360Destroyed() {
        return mIsC360Destroyed;
    }

    public static void setC360Destroyed(boolean c360Destroyed) {
        mEditor.putBoolean(WSConstants.PROPERTY_IS_C360_DESTROYED, c360Destroyed);
        mEditor.commit();
        mIsC360Destroyed = c360Destroyed;
    }

    public static String getUserId() {
        return mUserId;
    }

    public static void setUserId(String userId) {
        mEditor.putString(WSConstants.PROPERTY_USER_ID, userId);
        mEditor.commit();
        mUserId = userId;
    }

    public static String getEmail() {
        return mEmail;
    }

    public static void setEmail(String email) {
        mEditor.putString(WSConstants.PROPERTY_EMAIL, email);
        mEditor.commit();
        mEmail = email;
    }

    public static String getName() {
        return mName;
    }

    public static void setName(String name) {
        mEditor.putString(WSConstants.PROPERTY_NAME, name);
        mEditor.commit();
        mName = name;
    }

    public static String getRole() {
        return mRole;
    }

    public static void setRole(String role) {
        mEditor.putString(WSConstants.PROPERTY_ROLE, role);
        mEditor.commit();
        mRole = role;
    }
    public static String getRoleId() {
        return mRoleId;
    }

    public static void setRoleId(String roleId) {
        mEditor.putString(WSConstants.PROPERTY_ROLE_ID, roleId);
        mEditor.commit();
        mRoleId = roleId;
    }

    public static int getTargetMonth() {
        return mTargetMonth;
    }

    public static void setTargetMonth(int targetMonth) {
        mEditor.putInt(WSConstants.PROPERTY_TARGET_MONTH, targetMonth);
        mEditor.commit();
        mTargetMonth = targetMonth;
    }

    public static int getLeaderBoardTab() {
        return mLeaderBoardTab;
    }

    public static void setLeaderBoardTab(int leaderBoardTab) {
        mEditor.putInt(WSConstants.PROPERTY_LEADERBOARD_SELECTED_TAB,leaderBoardTab);
        mEditor.commit();
        mLeaderBoardTab = leaderBoardTab;
    }

    public static int getTargetLocation() {
        return mTargetLocationID;
    }

    public static void setTargetLocation(int targetLocationID) {
        mEditor.putInt(WSConstants.PROPERTY_TARGET_LOCATION, targetLocationID);
        mEditor.commit();
        mTargetLocationID = targetLocationID;
    }

    public static String getImeiNumber() {
        return mImeiNumber;
    }

    public static void setImeiNumber(String imeiNumber) {
        mEditor.putString(WSConstants.PROPERTY_IMEI_NUMBER, imeiNumber);
        mEditor.commit();
        mImeiNumber = imeiNumber;
    }

    public static int getmGameLocationId() {
        return mGameLocationId;
    }

    public static void setmGameLocationId(int locationId) {
        mEditor.putInt(WSConstants.LOCATION_GAME_ID, locationId);
        mEditor.commit();
        mGameLocationId = locationId;
    }

    public static String getGameLocationName() {
        return mGameLocationName;
    }

    public static void setGameLocationName(String data) {
        mEditor.putString(WSConstants.LOCATION_GAME_NAME, data);
        mEditor.commit();
        mGameLocationName = data;
    }

    public static boolean isSyncEnabled() {
        return mSyncEnabled;
    }

    public static void setSyncEnabled(boolean syncEnabled) {
        mEditor.putBoolean(WSConstants.IS_SYNC_ENABLED, syncEnabled);
        mEditor.commit();
        mSyncEnabled = syncEnabled;
    }

    public static String getDealerId() {
        return mDealerId;
    }

    public static void setDealerId(String dealerId) {
        mEditor.putString(WSConstants.PROPERTY_DEALER_ID, dealerId);
        mEditor.commit();
        mDealerId = dealerId;
    }

    public static String getDealerName() {
        return mDealerName;
    }

    public static void setDealerName(String dealerName) {
        mEditor.putString(WSConstants.PROPERTY_DEALER_NAME, dealerName);
        mEditor.commit();
        mDealerName = dealerName;
    }

    public static boolean isRefreshOfflineData() {
        return mRefreshOfflineData;
    }

    public static void setRefreshOfflineData(boolean refreshOfflineData) {
        mEditor.putBoolean(WSConstants.REFRESH_OFFLINE_DATA, refreshOfflineData);
        mEditor.commit();
        mRefreshOfflineData = refreshOfflineData;
    }

    public static String getDseName() {
        return mDseName;
    }

    public static void setDseName(String dseName) {
        mEditor.putString(WSConstants.PROPERTY_DSE_NAME, dseName);
        mEditor.commit();
        mDseName = dseName;
    }

    public static long getAppLastOpenedDate() {
        return mAppLastOpenedDate;
    }

    public static void setAppLastOpenedDate(long appLastOpenedDate) {
        mEditor.putLong(WSConstants.APP_LAST_OPENED_DATE, appLastOpenedDate);
        mEditor.commit();
        mAppLastOpenedDate = appLastOpenedDate;
    }

    public static long getBasicOfflineLastUpdated() {
        return mBasicOfflineLastUpdated;
    }

    public static void setBasicOfflineLastUpdated(long basicOfflineLastUpdated) {
        mEditor.putLong(WSConstants.BASIC_OFFLINE_LAST_UPDATED, basicOfflineLastUpdated);
        mEditor.commit();
        mBasicOfflineLastUpdated = basicOfflineLastUpdated;
    }

    public static String getLeadID() {
        return mLeadID;
    }

    public static void setLeadID(String leadID) {
        mEditor.putString(WSConstants.PROPERTY_LEAD_ID, leadID);
        mEditor.commit();
        mLeadID = leadID;
    }

    public static int getScheduledActivityId() {
        return mScheduledActivityId;
    }

    public static void setScheduledActivityId(int scheduledActivityId) {
        mEditor.putInt(WSConstants.PROPERTY_SCHEDULED_ACTIVITY_ID, scheduledActivityId);
        mEditor.commit();
        mScheduledActivityId = scheduledActivityId;
    }

    public static int getCrmType() {
        return mCrmType;
    }

    public static void setCrmType(int crmType) {
        mEditor.putInt(WSConstants.PROPERTY_CRM_TYPE, crmType);
        mEditor.commit();
        mCrmType = crmType;
    }


    public static String getTokenExpiresIn() {
        return mTokenExpiresIn;
    }

    public static void setTokenExpiresIn(String tokenExpiresIn) {
        mEditor.putString(WSConstants.PROPERTY_TOKEN_EXPIRES_IN, tokenExpiresIn);
        mEditor.commit();
        mTokenExpiresIn = tokenExpiresIn;
    }

    public static boolean isLogedIn() {
        return mIsLogedIn;
    }

    public static void setIsLogedIn(boolean isLogedIn) {
        mEditor.putBoolean(WSConstants.PROPERTY_LOGGED_STATUS, isLogedIn);
        mEditor.commit();
        mIsLogedIn = isLogedIn;
    }

    public static boolean isDealershipSelected() {
        return dealershipSelected;
    }

    public static void setDealershipSelected(boolean data) {
        mEditor.putBoolean(WSConstants.PROPERTY_DEALER_SELECTED_STATUS, data);
        mEditor.commit();
        dealershipSelected = data;
    }

    public static int getDealershipSelectedId() {
        return dealershipSelectedId;
    }

    public static void setDealershipSelectedId(int data) {
        mEditor.putInt(WSConstants.PROPERTY_DEALER_SELECTED_ID, data);
        mEditor.commit();
        dealershipSelectedId = data;
    }

    public static String getUserMobile() {
        return mUserMobile;
    }

    public static void setUserMobile(String userMobile) {
        mEditor.putString(WSConstants.PROPERTY_USER_MOBILE, userMobile);
        mEditor.commit();
        mUserMobile = userMobile;
    }

    public static String getUserName() {
        return mUserName;
    }

    public static void setUserName(String userName) {
        mEditor.putString(WSConstants.PROPERTY_USER_NAME, userName);
        mEditor.commit();
        mUserName = userName;
    }

    public static String getPassword() {
        return mPassword;
    }

    public static void setPassword(String password) {
        mEditor.putString(WSConstants.PROPERTY_PASSWORD, password);
        mEditor.commit();
        mPassword = password;
    }

    public static String getAccessToken() {
        return mAccessToken;
    }

    public static void setAccessToken(String accessToken) {
        mEditor.putString(WSConstants.PROPERTY_ACCESS_TOKEN, accessToken);
        mEditor.commit();
        mAccessToken = accessToken;
    }

    public static boolean isFirstTime() {
        return mIsFirstTime;
    }

    public static void setFirstTime(boolean isFirstTime) {
        mEditor.putBoolean(WSConstants.PROPERTY_FIRST_TIME, isFirstTime);
        mEditor.commit();
        mIsFirstTime = isFirstTime;
    }

    public static void setGCM(String gcmID) {
        mEditor.putString(WSConstants.GCM_ID, gcmID);
        mEditor.commit();
        //mGcmID = gcmID;
    }

    public static String getLead_last_updated() {
        return mLeadLastUpdated;
    }

    public static int getLocationId() {
        return locationId;
    }

    public static void setLocationId(int locationId) {
        mEditor.putInt(WSConstants.MY_LOCATION_KEY, locationId);
        mEditor.commit();
        locationId = locationId;
    }

    public static String getEndDateEtvbr() {
        return endDateEtvbr;
    }

    public static void setEndDateEtvbr(String endDateEtvbr) {
        mEditor.putString(WSConstants.END_DATE_ETVBR, endDateEtvbr);
        mEditor.commit();
    }

    public static String getStartdateEtvbr() {
        return startdateEtvbr;
    }

    public static void setStartdateEtvbr(String startdateEtvbr) {
        mEditor.putString(WSConstants.START_DATE_ETVBR, startdateEtvbr);
        mEditor.commit();
    }

    public static String getShowDateEtvbr() {
        return showDateEtvbr;
    }

    public static void setShowDateEtvbr(String showDateEtvbr) {
        mEditor.putString(WSConstants.SHOW_DATE_ETVBR, showDateEtvbr);
        mEditor.commit();
    }

    public static String getProformaPdfLink() {
        return proformaPdfLink;
    }

    public static void setProformaPdfLink(String proformaPdfLink) {
        mEditor.putString(WSConstants.PROFORMA_PDF_LINK, proformaPdfLink);
        mEditor.commit();
        Preferences.proformaPdfLink = proformaPdfLink;
    }

    public static boolean getUsersTopRole() {
        return usersTopRole;
    }

    public static void setUsersTopRole(boolean usersTopRole) {
        mEditor.putBoolean(WSConstants.PROFORMA_ENABLED, usersTopRole);
        mEditor.commit();
        Preferences.usersTopRole = usersTopRole;
    }
    public void setLead_last_updated(String lead_last_updated) {
        mEditor.putString(WSConstants.PROPERTY_LEAD_LEAST_UPDATED, lead_last_updated);
        mEditor.commit();
        mLeadLastUpdated = lead_last_updated;
    }

    public static int getLastVisitedTab() {
        return lastVisitedTab;
    }

    public static void setLastVisitedTab(int lastVisitedTab) {
        //Preferences.lastVisitedTab = lastVisitedTab;
        mEditor.putInt(WSConstants.LAST_VISITED_TAB, lastVisitedTab);
        mEditor.commit();
    }

    public static String getBrand_id() {
        return mBrand_id;
    }

    public static void setBrand_id(String brand_id) {
        mEditor.putString(WSConstants.PROPERTY_BRAND_ID, brand_id);
        mEditor.commit();
        mBrand_id = brand_id;
    }

    public static String getAddSearchNumber() {
        return mAddSearchNumber;
    }

    public static void setAddSearchNumber(String addSearchNumber) {
        mEditor.putString(WSConstants.PROPERTY_ADD_LEAD_NUMBER, addSearchNumber);
        mEditor.commit();
        mAddSearchNumber = addSearchNumber;
    }

    public static String getFcmId() {
        return mFcmId;
    }

    public static void setFcmId(String fcmId) {
        mEditor.putString(WSConstants.FCM_ID, fcmId);
        mEditor.commit();
        mFcmId = fcmId;
    }

    public static boolean isFcmTableCreated() {
        return mIsFcmTableCreated;
    }

    public static void setFcmTableCreated(boolean fcmTableCreated) {
        mEditor.putBoolean(WSConstants.IS_FCM_TABLE_CREATED, fcmTableCreated);
        mEditor.commit();
        mIsFcmTableCreated = fcmTableCreated;
    }


    public static int getFcmSync() {
        return mFcmSync;
    }

    public static String getTotalTaskCount() {
        return totalTaskCount;
    }

    public static void setTotalTaskCount(String taskCount) {
        mEditor.putString(WSConstants.TOTAL_TASK_COUNT, taskCount);
        mEditor.commit();
        totalTaskCount = taskCount;
    }

    public static void setFcmSync(int fcmSync) {
        mEditor.putInt(WSConstants.FCM_SYNC_VALUE, fcmSync);
        mEditor.commit();
        mFcmSync = fcmSync;
    }

    public static void deleteAll(Context context) {
        mEditor.clear();
        mEditor.commit();
        load(context);

    }

    public static boolean isActionPlanSynced() {
        return mActionPlanSynced;
    }

    public static void setActionPlanSynced(boolean actionPlanSynced) {
        mEditor.putBoolean(WSConstants.IS_ACTION_PLAN_SYNC, actionPlanSynced);
        mEditor.commit();
        mActionPlanSynced = actionPlanSynced;
    }

    public static int getVersionCode() {
        return mVersionCode;
    }

    public static void setVersionCode(int versionCode) {
        mEditor.putInt(WSConstants.APP_VERSION_CODE, versionCode);
        mEditor.commit();
        mVersionCode = versionCode;
    }

    public static String getVersionName() {
        return mVersionName;
    }

    public static void setVersionName(String versionName) {
        mEditor.putString(WSConstants.APP_VERSION_NAME, versionName);
        mEditor.commit();
        mVersionName = versionName;
    }

    public static int getmCallState() {
        return mCallState;
    }

    public static void setmCallState(int callState) {
        mEditor.putInt(WSConstants.CALL_STATE, callState);
        mEditor.commit();
        mCallState = callState;
    }


  /*  public static String getmInOut() {
        return mInOut;
    }

    public static void setmInOut(String inOu) {
        mEditor.putString(WSConstants.IN_OUT, inOu);
        mEditor.commit();
        mInOut = inOu;
    }*/

    public static String getCalledNumber() {
        return mCallingNumber;
    }

    public static void setCalledNumber(String data) {
        mEditor.putString(WSConstants.CALLED_MOBILE_NUMBER, data);
        mEditor.commit();
        mCallingNumber = data;
    }

  /*  public String getDbUuid() {
        return mDbUuid;
    }

    public void setDbUuid(String dbID) {
        editor.putString(WSConstants.DB_ID, dbID);
        editor.commit();
        this.mDbUuid = dbID;
    }*/

    /*public String getRecordingTpe() {
        return mRecordingType;
    }
    public void setRecordingType(String mRecordingType) {
        editor.putString(WSConstants.RECORDING_TYPE, mRecordingType);
        editor.commit();
        this.mRecordingType = mRecordingType;
    }*/
    /*public static String getmCallId() {
        return mCallId;
    }

    public static void setmCallId(String mCallId) {
        mEditor.putString(WSConstants.CALL_ID, mCallId);
        mEditor.commit();
        mCallId = mCallId;
    }*/

    public static boolean isCalledHappened() {
        return mIsCallHappened;
    }

    public static void setCalledHappened(boolean callHappened) {
        mEditor.putBoolean(WSConstants.CALL_HAPPENED, callHappened);
        mEditor.commit();
        mIsCallHappened = callHappened;
    }

    public static int getmOutgoingCalls() {
        return mOutgoingCalls;
    }

    public static void setmOutgoingCalls(int data) {
        mEditor.putInt(WSConstants.OUTGOING_CALLS, data);
        mEditor.commit();
        mOutgoingCalls = data;
    }

    public static int getmIncomingCalls() {
        return mIncomingCalls;
    }

    public static void setmIncomingCalls(int data) {
        mEditor.putInt(WSConstants.INCOMING_CALLS, data);
        mEditor.commit();
        mIncomingCalls = data;
    }

    public static String getmFilePath() {
        return mFilePath;
    }

    public static void setmFilePath(String data) {
        mEditor.putString(WSConstants.FILE_PATH, data);
        mEditor.commit();
        mFilePath = data;
    }

    public static String getFileName() {
        return fileName;
    }

    public static void setFileName(String data) {
        mEditor.putString(WSConstants.FILE_NAME, data);
        mEditor.commit();
        fileName = data;
    }

    public static int getmEnableMultithread() {
        return mEnableMultithread;
    }

    public static void setmEnableMultithread(int data) {
        mEditor.putInt(WSConstants.ENABLE_MULTI_THREAD, data);
        mEditor.commit();
//		this.dialedCalls = dialedCalls;
        mEnableMultithread = data;
    }

    public static boolean isBackgroundThread() {
        return backgroundThread;
    }

    public static void setBackgroundThread(boolean data) {
        mEditor.putBoolean(WSConstants.BACKGROUND_THREAD, data);
        mEditor.commit();
        backgroundThread = data;
    }

    public static boolean getLimitSyncStatus() {
        return mLimitSync;
    }

    public static void setLimitSyncStatus(boolean sync) {
        mEditor.putBoolean(WSConstants.LIMIT_SYNC, sync);
        mEditor.commit();
        mLimitSync = sync;
    }

    public static String getmStart() {
        return mStart;
    }

    public static void setmStart(String start) {
        mEditor.putString(WSConstants.CALL_START, start);
        mEditor.commit();
        mStart = start;
    }

    public static String getmEnd() {
        return mEnd;
    }

    public static void setmEnd(String end) {
        mEditor.putString(WSConstants.CALL_END, end);
        mEditor.commit();
        mEnd = end;
    }

    public static long getmDuration() {
        return mDuration;
    }

    public static void setmDuration(long duration) {
        mEditor.putLong(WSConstants.CALL_DURATION, duration);
        mEditor.commit();
        mDuration = duration;
    }

    public static int getmaxId() {
        return mMaxID;
    }

    public static void setMaxId(int maxId) {
        mEditor.putInt(WSConstants.MAX_ID, maxId);
        mEditor.commit();
        mMaxID = maxId;
    }

   /* public static int getSimSlotId() {
        return mSimSlot;
    }

    public static void setSlimSlotId(int simSlotId) {
        mEditor.putInt(WSConstants.SIM_SLOT, simSlotId);
        mEditor.commit();
        mSimSlot = simSlotId;
    }*/

    public static String getmTelephonyLeadID() {
        return mTelephonyLeadID;
    }

    public static void setmTelephonyLeadID(String telephonyLeadID) {
        mEditor.putString(WSConstants.TELEPHONY_LEAD_ID, telephonyLeadID);
        mEditor.commit();
        mTelephonyLeadID = telephonyLeadID;
    }

    public static int getCloseLeadListCount() {
        return mCloseLeadListCount;
    }

    public static void setCloseLeadListCount(int closeLeadListCount) {
        mEditor.putInt(WSConstants.APP_CLOSED_LEAD_LIST_COUNT, leadListCount);
        mEditor.commit();
        mCloseLeadListCount = closeLeadListCount;
    }

    public static int getAssignedListCount() {
        return assignedListCount;
    }

    public static void setAssignedListCount(int mAssignedListCount) {
        mEditor.putInt(WSConstants.APP_ASSIGNED_LEAD_LIST_COUNT, leadListCount);
        mEditor.commit();
        assignedListCount = mAssignedListCount;
    }

    public int getLeadListCount() {
        return leadListCount;
    }

    public void setLeadListCount(int leadListCount) {
        mEditor.putInt(WSConstants.APP_LEAD_LIST_COUNT, leadListCount);
        mEditor.commit();
        this.leadListCount = leadListCount;
    }

    public Integer getAppUserId() {
        return appUserId == null ? null : Integer.valueOf(appUserId);
    }

    public void setAppUserId(String dataAppUserId) {
        mEditor.putString(WSConstants.APP_USER_ID, dataAppUserId);
        mEditor.commit();
        appUserId = dataAppUserId;
    }

    public Integer getCurrentDseId() {
        if (currentDseId == null) {
            return null;
        } else {
            return Integer.valueOf(currentDseId);
        }
    }

    public void setCurrentDseId(String data) {
        mEditor.putString(WSConstants.APP_CLICKED_DSE_ID, data);
        mEditor.commit();
        currentDseId = data;
    }

    public void setTeamId(String data) {
        mEditor.putString(WSConstants.APP_TEAM_ID_ID, data);
        mEditor.commit();
        teamId = data;

    }

    public String getTeamId() {
        return teamId;
    }

    public void setNewUpdateNotice(String data) {
        mEditor.putString(WSConstants.PROPERTY_NEW_UPDATE_NOTICE, data);
        mEditor.commit();
        newUpdateNotice = data;

    }

    public String getNewUpdateNotice() {
        return newUpdateNotice;
    }

    public void setShowBalloonAnimation(boolean data) {
        mEditor.putBoolean(WSConstants.PROPERTY_SHOW_BALLOON_ANIMATION, data);
        mEditor.commit();
        showBalloonAnimation = data;
    }
    public boolean isShowBalloonAnimation() {
        return showBalloonAnimation;
    }

    public void setReloadC360(boolean data) {
        mEditor.putBoolean(WSConstants.PROPERTY_RELOAD_C360, data);
        mEditor.commit();
        reloadC360 = data;
    }

    public static boolean isReloadC360() {
        return reloadC360;
    }

    public static boolean isGameIntroSeen() {
        return gameIntroSeen;
    }

    public static void setGameIntroSeen(boolean data) {
        mEditor.putBoolean(WSConstants.IS_GAME_INTRO_SEEN, data);
        mEditor.commit();
        gameIntroSeen = data;
    }

    public static String getUserProfilePicUrl() {
        return userProfilePicUrl;
    }

    public static void setUserProfilePicUrl(String data) {
        mEditor.putString(WSConstants.PROPERTY_USER_PROFILE_PIC_URL, data);
        mEditor.commit();
        userProfilePicUrl = data;
    }

    public static void setShowPostBookingTdVisit(boolean data) {
        mEditor.putBoolean(WSConstants.PROPERTY_SHOW_POST_BOOKING_TD_VISIT, data);
        mEditor.commit();
        showPostBooking = data;
    }
    public static boolean isShowPostBookingTdVisit() {
        return showPostBooking;
    }
    public static String getSelectedGameDate() {
        return selectedGameDate;
    }

    public static void setSelectedGameDate(String data) {
        mEditor.putString(WSConstants.PROPERTY_SELECTED_GAME_DATE, data);
        mEditor.commit();
        selectedGameDate = data;
    }

    public static String getSystemDate() {
        return systemDate;
    }

    public static void setSystemDate(String data) {
        mEditor.putString(WSConstants.PROPERTY_SYSTEM_DATE, data);
        mEditor.commit();
        systemDate = data;
    }

    public static String getAcrLicenseKey() {
        return acrLicenseKey;
    }

    public static void setAcrLicenseKey(String data) {
        mEditor.putString(WSConstants.PROPERTY_ACR_LICENSE_KEY, data);
        mEditor.commit();
        acrLicenseKey = data;
    }

    public static String getAcrSerial() {
        return acrSerial;
    }

    public static void setAcrSerial(String data) {
        mEditor.putString(WSConstants.PROPERTY_ACR_SERIAL, data);
        mEditor.commit();
        acrSerial = data;
    }

    public static void setDeviceId(String data) {
        mEditor.putString(WSConstants.PROPERTY_DEVICE_ID, data);
        mEditor.commit();
        deviceId = data;
    }
    public static String getDeviceId() {
        return deviceId;
    }

    public static String getBuyerType() {
        return buyerType;
    }

    public static void setBuyerType(String buyerType) {
        mEditor.putString(WSConstants.BUYER_TYPE, buyerType);
        mEditor.commit();
        deviceId = buyerType;
    }

    public boolean isIsDeviceSupportedForNLL() {
        return isDeviceSupportedForNLL  && Build.VERSION.SDK_INT >= 23 ;
    }

    public void setDeviceSupportedForNLL(boolean data) {
        mEditor.putBoolean(WSConstants.PROPERTY_IS_DEVICE_SUPPORT_NLL, data);
        mEditor.commit();
        isDeviceSupportedForNLL = data;
    }

    public static String getS3UserName() {
        return s3UserName;
    }

    public static void setS3UserName(String userName) {
        mEditor.putString(WSConstants.PROPERTY_S3_USER_NAME, userName);
        mEditor.commit();
        s3UserName = userName;
    }

    public static String getS3Password() {
        return s3Password;
    }

    public static void setS3Password(String password) {
        mEditor.putString(WSConstants.PROPERTY_S3_PASSWORD, password);
        mEditor.commit();
        s3Password = password;
    }

    public static String getS3BucketName() {
        return s3BucketName;
    }

    public static void setS3BucketName(String bucketName) {
        mEditor.putString(WSConstants.PROPERTY_S3_BUCKET_NAME, bucketName);
        mEditor.commit();
        s3BucketName = bucketName;
    }
    public static String getS3SessionToken() {
        return s3SessionToken;
    }

    public static void setS3SessionToken(String data) {
        mEditor.putString(WSConstants.PROPERTY_S3_SESSION_TOKEN, data);
        mEditor.commit();
        s3SessionToken = data;
    }

    public boolean isEventDashboardUpdateRequired() {
        return isEventDashboardUpdate;
    }

    public void setEventDashboardUpdateRequired(boolean data) {
        mEditor.putBoolean(WSConstants.PROPERTY_IS_EVENT_DASHBOARD_UPDATE_REQUIRED, data);
        mEditor.commit();
        isEventDashboardUpdate = data;
    }

    public String[] getGovtApiKeys() {
        return govtApiKeys.split(",");
    }

    public void setGovtApiKeys(String key) {
        mEditor.putString(WSConstants.PROPERTY_GOVERNMENT_API_KEYS, key);
        mEditor.commit();
        govtApiKeys = key;
    }

    public static String getHereMapAppId() {
        return hereMapAppId;
    }

    public static void setHereMapAppId(String id) {
        mEditor.putString(WSConstants.PROPERTY_HERE_MAP_APP_ID, id);
        mEditor.commit();
        hereMapAppId = id;
    }

    public static String getHereMapAppCode() {
        return hereMapAppCode;
    }

    public static void setHereMapAppCode(String appCode) {
        mEditor.putString(WSConstants.PROPERTY_HERE_MAP_APP_CODE, appCode);
        mEditor.commit();
        hereMapAppCode = appCode;
    }

    public static boolean isTwoWheelerClient() {
        return twoWheelerClient;
    }

    public static void setTwoWheelerClient(boolean data) {
        mEditor.putBoolean(WSConstants.PROPERTY_TWO_WHEELER_CLIENT, data);
        mEditor.commit();
        twoWheelerClient = data;
    }

    public static boolean isClientILom() {
        return clientILom;
    }

    public static void setClientILom(boolean data) {
        mEditor.putBoolean(WSConstants.PROPERTY_I_LOM_CLIENT, data);
        mEditor.commit();
        clientILom = data;
    }

    public static boolean isClientILBank() {
        return clientILBank;
    }

    public static void setClientILBank(boolean data) {
        mEditor.putBoolean(WSConstants.PROPERTY_I_LOM_BANK, data);
        mEditor.commit();
        clientILBank = data;
    }
    public static boolean isScreenshotEnabled() {
        return screenshotEnabled;
    }

    public static void setScreenshotEnabled(boolean data) {
        mEditor.putBoolean(WSConstants.PROPERTY_SCREEN_SHOT, data);
        mEditor.commit();
        screenshotEnabled = data;
    }
    public static String getHotlineNumber() {
        return hotlineNumber;
    }

    public static void setHotlineNumber(String data) {
        mEditor.putString(WSConstants.PROPERTY_HOT_LINE, data);
        mEditor.commit();
        hotlineNumber = data;
    }

    public static String getCallingLeadId() {
        return callingLeadId;
    }

    public static void setCallingLeadId(String data) {
        mEditor.putString(WSConstants.PROPERTY_CALLING_LEAD_ID, data);
        mEditor.commit();
        callingLeadId = data;
    }
    public static long getCallStatCreatedAt() {
        return callStatCreatedAt;
    }

    public static void setCallStatCreatedAt(long data) {
        mEditor.putLong(WSConstants.PROPERTY_CALL_STAT_CREATED_AT, data);
        mEditor.commit();
        callStatCreatedAt = data;
    }
}

