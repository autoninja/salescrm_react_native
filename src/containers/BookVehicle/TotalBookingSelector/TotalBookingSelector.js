import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, FlatList, Platform } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import { NavigationActions } from 'react-navigation'
import styles from './TotalBookingSelector.styles';
import Toast, { DURATION } from 'react-native-easy-toast';
import Button from "../../../components/uielements/Button";
import { BOOKING_ROUTES } from '../BookingUtils/BookingUtils'
import NavigationService from '../../../navigation/NavigationService';
export default class TotalBookingSelector extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bookingCount: 1
        }
    }
    changeCount(val) {
        let { bookingCount } = this.state;
        if (bookingCount == 1 && val < 0) {
            return;
        }
        bookingCount += val
        this.setState({ bookingCount })

    }
    onPressNext(totalBookings) {
        NavigationService.resetScreen(BOOKING_ROUTES.BookVehicleMain, { totalBookings }, this.props.navigation)

    }
    showAlert(message) {
        try {
            this.refs.toast.show(message);
        }
        catch (e) {

        }
    }

    render() {
        let backImage = require("../../../images/ic_close_black.png");
        let { bookingCount } = this.state;
        return (
            <View style={styles.main}>
                <LinearGradient
                    colors={["#2e5e86", "#2e3486"]}
                    style={styles.linearGradient}>
                    <View style={{ flexWrap: 'wrap' }}>

                        <View style={styles.header}>
                            <Text style={styles.title}>{this.props.info.modelName}</Text>
                            <TouchableOpacity
                                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                style={styles.closeButton}
                                onPress={() => {
                                    this.props.callbacks.goBack();

                                }}>
                                <Image
                                    source={backImage}
                                    style={{ width: 16, height: 16 }}
                                />
                            </TouchableOpacity>
                        </View>
                        <Text style={{
                            color: '#b5b8bd',
                            fontSize: 15,
                            textAlign: 'center',
                            marginTop: 10
                        }}>Select no. of vehicles to book</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 24 }}>
                            <TouchableOpacity
                                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                style={styles.vehicleCountButton}
                                onPress={() => {
                                    this.changeCount(-1);

                                }}>
                                <Image
                                    source={require('../../../images/ic_count_minus.png')}
                                    style={{ width: 16, height: 16 }}
                                />
                            </TouchableOpacity>
                            <Text style={{ paddingStart: 24, paddingEnd: 24, fontSize: 16, color: '#eee' }}>{("0" + bookingCount).slice(-2)}</Text>
                            <TouchableOpacity
                                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                style={styles.vehicleCountButton}
                                onPress={() => {
                                    this.changeCount(1);

                                }}>
                                <Image
                                    source={require('../../../images/ic_count_plus.png')}
                                    style={{ width: 16, height: 16 }}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Button
                                title={"Next"}
                                onPress={() => {
                                    this.onPressNext(bookingCount);
                                }}
                            />
                        </View>

                    </View>
                </LinearGradient>
                <Toast
                    ref="toast"
                    style={{ backgroundColor: 'red' }}
                    fadeInDuration={750}
                    fadeOutDuration={750}
                    opacity={0.8}
                    textStyle={{ color: 'white' }}
                />
            </View>
        )
    }
}