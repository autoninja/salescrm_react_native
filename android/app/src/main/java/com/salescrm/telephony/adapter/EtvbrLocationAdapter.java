package com.salescrm.telephony.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.DetailsOfEtvbr;
import com.salescrm.telephony.activity.EtvbrLocationActivity;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.db.etvbr_location.ResultEtvbrLocation;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.EtvbrLocationFragment;
import com.salescrm.telephony.preferences.Preferences;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by prateek on 7/11/17.
 */

public class EtvbrLocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private Realm realm;
    private ResultEtvbrLocation resultEtvbrLocation;
    private RealmResults<Location> locationsResults;
    private Preferences pref;

    public EtvbrLocationAdapter(FragmentActivity activity, ResultEtvbrLocation result, Realm realm) {
        this.context = activity;
        this.realm = realm;
        resultEtvbrLocation = result;
        pref = Preferences.getInstance();
        pref.load(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.etvbr_location_card_adpater, parent, false);
        return new EtvbrLocationAdapter.EtvbrLocationCardHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (EtvbrLocationFragment.PERCENT) {
            viewCardsGMRel(holder, position);
        } else {
            viewCardsGMAbs(holder, position);
        }
    }

    private void viewCardsGMAbs(RecyclerView.ViewHolder holder, final int position) {
        final EtvbrLocationCardHolder cardHolder = (EtvbrLocationCardHolder) holder;
        //final int position = pos;
        cardHolder.cardView.setVisibility(View.VISIBLE);
        cardHolder.llinearLayout.setVisibility(View.GONE);
        //absolute = results.get(position).getAbs();
        cardHolder.tvTargetRetailCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetEnquiyCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetTDCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetVisitCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetBookingCard.setVisibility(View.VISIBLE);
        cardHolder.viewTargetCard.setVisibility(View.VISIBLE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 0);

        Drawable drawableImg = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_img_location, null);
        cardHolder.imgUserCard.setBackground(drawableImg);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        if (position < resultEtvbrLocation.getLocations().size()) {
            locationsResults = realm.where(Location.class).findAll();

            if (locationsResults != null) {

                Paint paint = new Paint();
                paint.setColor(Color.BLUE);
                paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);

                cardHolder.tvEnquiryCard.setPaintFlags(paint.getFlags());
                cardHolder.tvEnquiryCard.setText("" + locationsResults.get(position).getLocationAbs().getEnquiries());

                cardHolder.tvTestDriveCard.setPaintFlags(paint.getFlags());
                cardHolder.tvTestDriveCard.setText("" + locationsResults.get(position).getLocationAbs().getTdrives());

                cardHolder.tvVisitCard.setPaintFlags(paint.getFlags());
                cardHolder.tvVisitCard.setText("" + locationsResults.get(position).getLocationAbs().getVisits());

                cardHolder.tvBookingCard.setPaintFlags(paint.getFlags());
                cardHolder.tvBookingCard.setText("" + locationsResults.get(position).getLocationAbs().getBookings());

                cardHolder.tvRetailCard.setPaintFlags(paint.getFlags());
                cardHolder.tvRetailCard.setText("" + locationsResults.get(position).getLocationAbs().getRetails());

                cardHolder.tvLocationName.setPaintFlags(paint.getFlags());
                cardHolder.tvLocationName.setText("" + locationsResults.get(position).getLocationName());

                cardHolder.tvLostCard.setPaintFlags(paint.getFlags());
                cardHolder.tvLostCard.setText(""+locationsResults.get(position).getLocationAbs().getLost_drop());

                cardHolder.tvTargetEnquiyCard.setText("" + locationsResults.get(position).getLocationTargets().getEnquiries());
                cardHolder.tvTargetTDCard.setText("" + locationsResults.get(position).getLocationTargets().getTdrives());
                cardHolder.tvTargetVisitCard.setText("" + locationsResults.get(position).getLocationTargets().getVisits());
                cardHolder.tvTargetBookingCard.setText("" + locationsResults.get(position).getLocationTargets().getBookings());
                cardHolder.tvTargetRetailCard.setText("" + locationsResults.get(position).getLocationTargets().getRetails());

                cardHolder.tvEnquiryCard.setVisibility(View.VISIBLE);
                cardHolder.viewEnquiryCard.setVisibility(View.VISIBLE);
                cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
                cardHolder.llEnquriyCard.setVisibility(View.VISIBLE);
                cardHolder.tvLocationName.setVisibility(View.VISIBLE);


                cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.tvNameCard.setVisibility(View.GONE);
                //Picasso.with(context).load("https://s3-ap-southeast-1.amazonaws.com/salescrm-images/staging/uploads/profile_images//SATYANARYANA CH-48-1510145708.png").placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).into(cardHolder.imgUserCard);

                cardHolder.llInner.setBackgroundColor(Color.WHITE);
                cardHolder.tvEnquiryCard.setTextColor(Color.BLACK);
                cardHolder.tvTestDriveCard.setTextColor(Color.BLACK);
                cardHolder.tvVisitCard.setTextColor(Color.BLACK);
                cardHolder.tvBookingCard.setTextColor(Color.BLACK);
                cardHolder.tvRetailCard.setTextColor(Color.BLACK);
                cardHolder.tvUserTotalCard.setTextColor(Color.BLACK);

                cardHolder.tvEnquiryCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvTestDriveCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvVisitCard.setTypeface(null, Typeface.NORMAL);

                cardHolder.tvBookingCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvRetailCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvUserTotalCard.setTypeface(null, Typeface.NORMAL);

                cardHolder.viewTdCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
                cardHolder.viewBookingCard.setVisibility(View.VISIBLE);
                cardHolder.viewLostCard.setVisibility(View.VISIBLE);

                cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetEnquiyCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetTDCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetVisitCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetBookingCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#C1C1C1"));

                cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "etvbr");
                        context.startActivity(intent);
                    }
                });

                cardHolder.tvEnquiryCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "etvbr");
                        context.startActivity(intent);
                    }
                });
                cardHolder.tvTestDriveCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "etvbr");
                        context.startActivity(intent);
                    }
                });
                cardHolder.tvVisitCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "etvbr");
                        context.startActivity(intent);
                    }
                });
                cardHolder.tvBookingCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "etvbr");
                        context.startActivity(intent);
                    }
                });
                cardHolder.tvRetailCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "etvbr");
                        context.startActivity(intent);
                    }
                });
                cardHolder.tvLostCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "etvbr");
                        context.startActivity(intent);
                    }
                });

                final String userId = ""+realm.where(UserDetails.class).findFirst().getUserId();


                cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(0,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getEnquiries(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });

                cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(1,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getTdrives(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });

                cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(2,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getVisits(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });

                cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(3,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getBookings(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });

                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(4,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getRetails(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });

                cardHolder.tvLostCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(5,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getLost_drop(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });


            }
            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
            }
        } else {
            if (resultEtvbrLocation != null) {
                cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));

                Paint paint = new Paint();
                paint.setColor(Color.BLUE);
                paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);

                cardHolder.tvEnquiryCard.setPaintFlags(paint.getFlags());
                cardHolder.tvEnquiryCard.setText("" + resultEtvbrLocation.getTotalAbs().getEnquiries());

                cardHolder.tvTestDriveCard.setPaintFlags(paint.getFlags());
                cardHolder.tvTestDriveCard.setText("" + resultEtvbrLocation.getTotalAbs().getTdrives());

                cardHolder.tvVisitCard.setPaintFlags(paint.getFlags());
                cardHolder.tvVisitCard.setText("" + resultEtvbrLocation.getTotalAbs().getVisits());

                cardHolder.tvBookingCard.setPaintFlags(paint.getFlags());
                cardHolder.tvBookingCard.setText("" + resultEtvbrLocation.getTotalAbs().getBookings());

                cardHolder.tvRetailCard.setPaintFlags(paint.getFlags());
                cardHolder.tvRetailCard.setText("" + resultEtvbrLocation.getTotalAbs().getRetails());

                cardHolder.tvLostCard.setPaintFlags(paint.getFlags());
                cardHolder.tvLostCard.setText(""+resultEtvbrLocation.getTotalAbs().getLost_drop());

                cardHolder.tvTargetEnquiyCard.setText("" + resultEtvbrLocation.getTotalTargets().getEnquiries());
                cardHolder.tvTargetTDCard.setText("" + resultEtvbrLocation.getTotalTargets().getTdrives());
                cardHolder.tvTargetVisitCard.setText("" + resultEtvbrLocation.getTotalTargets().getVisits());
                cardHolder.tvTargetBookingCard.setText("" + resultEtvbrLocation.getTotalTargets().getBookings());
                cardHolder.tvTargetRetailCard.setText("" + resultEtvbrLocation.getTotalTargets().getRetails());

                cardHolder.tvEnquiryCard.setVisibility(View.VISIBLE);
                cardHolder.viewEnquiryCard.setVisibility(View.INVISIBLE);
                cardHolder.tvTargetTitleCard.setVisibility(View.VISIBLE);
                cardHolder.llEnquriyCard.setVisibility(View.VISIBLE);
                cardHolder.tvLocationName.setVisibility(View.GONE);

                cardHolder.tvNameCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setText("Achieved");

                cardHolder.tvTargetTitleCard.setText("Target");

                cardHolder.tvEnquiryCard.setTextColor(Color.WHITE);
                cardHolder.tvTestDriveCard.setTextColor(Color.WHITE);
                cardHolder.tvVisitCard.setTextColor(Color.WHITE);
                cardHolder.tvBookingCard.setTextColor(Color.WHITE);
                cardHolder.tvRetailCard.setTextColor(Color.WHITE);
                cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);
                //cardHolder.tvTargetTitleCard.setTextColor(Color.WHITE);

                cardHolder.tvEnquiryCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvTestDriveCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvVisitCard.setTypeface(null, Typeface.BOLD);
                ;
                cardHolder.tvBookingCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvRetailCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvUserTotalCard.setTypeface(null, Typeface.BOLD);
                //cardHolder.tvTargetTitleCard.setTypeface(null, Typeface.BOLD);

                cardHolder.viewTdCard.setVisibility(View.INVISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
                cardHolder.viewBookingCard.setVisibility(View.INVISIBLE);
                cardHolder.viewLostCard.setVisibility(View.INVISIBLE);

                cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#8C94AB"));
                cardHolder.tvTargetEnquiyCard.setBackgroundColor(Color.parseColor("#8C94AB"));
                cardHolder.tvTargetTDCard.setBackgroundColor(Color.parseColor("#8C94AB"));
                cardHolder.tvTargetVisitCard.setBackgroundColor(Color.parseColor("#8C94AB"));
                cardHolder.tvTargetBookingCard.setBackgroundColor(Color.parseColor("#8C94AB"));
                cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#8C94AB"));


                final String userId = ""+realm.where(UserDetails.class).findFirst().getUserId();


                cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(0,"All Location's Data",
                                ""+userId,
                                ""+resultEtvbrLocation.getTotalAbs().getEnquiries(),
                                ""+resultEtvbrLocation.getRoleId(),"");
                        return true;
                    }
                });

                cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(1,"All Location's Data",
                                ""+userId,
                                ""+resultEtvbrLocation.getTotalAbs().getTdrives(),
                                ""+resultEtvbrLocation.getRoleId(),"");
                        return true;
                    }
                });

                cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(2,"All Location's Data",
                                ""+userId,
                                ""+resultEtvbrLocation.getTotalAbs().getVisits(),
                                ""+resultEtvbrLocation.getRoleId(),"");
                        return true;
                    }
                });

                cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(3,"All Location's Data",
                                ""+userId,
                                ""+resultEtvbrLocation.getTotalAbs().getBookings(),
                                ""+resultEtvbrLocation.getRoleId(),"");
                        return true;
                    }
                });

                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(4,"All Location's Data",
                                ""+userId,
                                ""+resultEtvbrLocation.getTotalAbs().getRetails(),
                                ""+resultEtvbrLocation.getRoleId(),"");
                        return true;
                    }
                });

                cardHolder.tvLostCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(5,"All Location's Data",
                                ""+userId,
                                ""+resultEtvbrLocation.getTotalAbs().getLost_drop(),
                                ""+resultEtvbrLocation.getRoleId(),"");
                        return true;
                    }
                });

            }
            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void viewCardsGMRel(RecyclerView.ViewHolder holder, final int position) {
        final EtvbrLocationCardHolder cardHolder = (EtvbrLocationCardHolder) holder;
        //final int position = pos;
        cardHolder.tvTargetRetailCard.setVisibility(View.GONE);
        cardHolder.tvTargetEnquiyCard.setVisibility(View.GONE);
        cardHolder.tvTargetTDCard.setVisibility(View.GONE);
        cardHolder.tvTargetVisitCard.setVisibility(View.GONE);
        cardHolder.tvTargetBookingCard.setVisibility(View.GONE);
        cardHolder.viewTargetCard.setVisibility(View.GONE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 6);

        Drawable drawableImg = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_img_location, null);
        cardHolder.imgUserCard.setBackground(drawableImg);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        cardHolder.llinearLayout.setVisibility(View.GONE);
        //rels = results.get(position).getRel();
        if (position < resultEtvbrLocation.getLocations().size()) {
            locationsResults = realm.where(Location.class).findAll();

            if (locationsResults != null) {
                cardHolder.tvEnquiryCard.setPaintFlags(0);
                cardHolder.tvEnquiryCard.setText("" + locationsResults.get(position).getLocationRel().getEnquiries());

                cardHolder.tvTestDriveCard.setPaintFlags(0);
                cardHolder.tvTestDriveCard.setText("" + locationsResults.get(position).getLocationRel().getTdrives());

                cardHolder.tvVisitCard.setPaintFlags(0);
                cardHolder.tvVisitCard.setText("" + locationsResults.get(position).getLocationRel().getVisits());

                cardHolder.tvBookingCard.setPaintFlags(0);
                cardHolder.tvBookingCard.setText("" + locationsResults.get(position).getLocationRel().getBookings());

                cardHolder.tvRetailCard.setPaintFlags(0);
                cardHolder.tvRetailCard.setText("" + locationsResults.get(position).getLocationRel().getRetails());

                cardHolder.tvLocationName.setPaintFlags(0);
                cardHolder.tvLocationName.setText("" + locationsResults.get(position).getLocationName());

                cardHolder.tvLostCard.setPaintFlags(0);
                cardHolder.tvLostCard.setText(""+ locationsResults.get(position).getLocationRel().getLost_drop());

                cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvLostCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvEnquiryCard.setVisibility(View.GONE);
                cardHolder.viewEnquiryCard.setVisibility(View.GONE);
                cardHolder.tvEnquiryCard.setVisibility(View.GONE);
                cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
                cardHolder.llEnquriyCard.setVisibility(View.GONE);
                cardHolder.tvLocationName.setVisibility(View.VISIBLE);


                cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.tvNameCard.setVisibility(View.GONE);
                //Picasso.with(context).load("https://s3-ap-southeast-1.amazonaws.com/salescrm-images/staging/uploads/profile_images//SATYANARYANA CH-48-1510145708.png").placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).into(cardHolder.imgUserCard);


                cardHolder.llInner.setBackgroundColor(Color.WHITE);
                cardHolder.tvEnquiryCard.setTextColor(Color.BLACK);
                cardHolder.tvTestDriveCard.setTextColor(Color.BLACK);
                cardHolder.tvVisitCard.setTextColor(Color.BLACK);
                cardHolder.tvBookingCard.setTextColor(Color.BLACK);
                cardHolder.tvRetailCard.setTextColor(Color.BLACK);
                cardHolder.tvUserTotalCard.setTextColor(Color.BLACK);

                cardHolder.viewTdCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
                cardHolder.viewBookingCard.setVisibility(View.VISIBLE);
                cardHolder.viewLostCard.setVisibility(View.VISIBLE);

                cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "etvbr");
                        context.startActivity(intent);
                    }
                });
            }
            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
            }
        } else {
            if (resultEtvbrLocation != null) {
                cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));

                cardHolder.tvEnquiryCard.setPaintFlags(0);
                cardHolder.tvEnquiryCard.setText("" + resultEtvbrLocation.getTotalRel().getEnquiries());

                cardHolder.tvTestDriveCard.setPaintFlags(0);
                cardHolder.tvTestDriveCard.setText("" + resultEtvbrLocation.getTotalRel().getTdrives());

                cardHolder.tvVisitCard.setPaintFlags(0);
                cardHolder.tvVisitCard.setText("" + resultEtvbrLocation.getTotalRel().getVisits());

                cardHolder.tvBookingCard.setPaintFlags(0);
                cardHolder.tvBookingCard.setText("" + resultEtvbrLocation.getTotalRel().getBookings());

                cardHolder.tvRetailCard.setPaintFlags(0);
                cardHolder.tvRetailCard.setText("" + resultEtvbrLocation.getTotalRel().getRetails());

                cardHolder.tvLostCard.setPaintFlags(0);
                cardHolder.tvLostCard.setText(""+ resultEtvbrLocation.getTotalRel().getLost_drop());

                cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvEnquiryCard.setVisibility(View.GONE);
                cardHolder.viewEnquiryCard.setVisibility(View.GONE);
                cardHolder.tvEnquiryCard.setVisibility(View.GONE);
                cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
                cardHolder.llEnquriyCard.setVisibility(View.GONE);
                cardHolder.tvLocationName.setVisibility(View.GONE);

                cardHolder.tvNameCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setText("Total%");

                cardHolder.tvEnquiryCard.setTextColor(Color.WHITE);
                cardHolder.tvTestDriveCard.setTextColor(Color.WHITE);
                cardHolder.tvVisitCard.setTextColor(Color.WHITE);
                cardHolder.tvBookingCard.setTextColor(Color.WHITE);
                cardHolder.tvRetailCard.setTextColor(Color.WHITE);
                cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);

                cardHolder.viewTdCard.setVisibility(View.INVISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
                cardHolder.viewBookingCard.setVisibility(View.INVISIBLE);
                cardHolder.viewLostCard.setVisibility(View.INVISIBLE);
            }
            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        //resultSMs = realm.where(ResultSM.class).findAll();
        if(resultEtvbrLocation.getLocations()!= null && resultEtvbrLocation.isValid() ) {
            return (resultEtvbrLocation.getLocations().size() > 0) ? resultEtvbrLocation.getLocations().size() + 1 : 0;
        }else{
            return 0;
        }
    }

    public class EtvbrLocationCardHolder extends RecyclerView.ViewHolder {

        ImageView imgUser, imgUserCard;
        TextView tvNameCard, tvEnquiryCard, tvTestDriveCard, tvVisitCard, tvBookingCard, tvRetailCard, tvLostCard;
        TextView tvName, tvEnquiry, tvTestDrive, tvVisit, tvBooking, tvRetail;
        View viewEnquiryCard, viewTdCard, viewVisitCard, viewBookingCard, viewReatilCard, viewLostCard;
        View viewEnquiry, viewTd, viewVisit, viewBooking, viewReatil;
        LinearLayout llaLinearLayout, llinearLayout;
        LinearLayout llInner;
        TextView tvUserTotalCard;
        TextView tvUserTotal;
        CardView cardView;
        LinearLayout llEnquriyCard, llTdCard, llVisitCard, llBookingCard, llRetailsCard, llLostCard;
        TextView tvTargetTitleCard;
        TextView tvTargetEnquiyCard, tvTargetTDCard, tvTargetVisitCard, tvTargetBookingCard, tvTargetRetailCard;
        View viewTargetCard;
        RelativeLayout rlFrameLayoutCard;
        TextView tvLocationName;


        public EtvbrLocationCardHolder(View itemView) {
            super(itemView);
            imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            tvName = (TextView) itemView.findViewById(R.id.text_user);
            tvEnquiry = (TextView) itemView.findViewById(R.id.txt_enquiry);
            tvTestDrive = (TextView) itemView.findViewById(R.id.txt_td);
            tvVisit = (TextView) itemView.findViewById(R.id.txt_visit);
            tvBooking = (TextView) itemView.findViewById(R.id.txt_booking);
            tvRetail = (TextView) itemView.findViewById(R.id.txt_retail);
            //llaLinearLayout = (LinearLayout) itemView.findViewById(R.id.ll_today_summary_main);
            tvUserTotal = (TextView) itemView.findViewById(R.id.text_user_total);
            viewEnquiry = (View) itemView.findViewById(R.id.enquiry_view);
            viewTd = (View) itemView.findViewById(R.id.td_view);
            viewVisit = (View) itemView.findViewById(R.id.visit_view);
            viewBooking = (View) itemView.findViewById(R.id.booking_view);
            viewReatil = (View) itemView.findViewById(R.id.retail_view);
            llinearLayout = (LinearLayout) itemView.findViewById(R.id.llinearLayout);
            llInner = (LinearLayout) itemView.findViewById(R.id.card_ll);

            imgUserCard = (ImageView) itemView.findViewById(R.id.img_user_card);
            tvNameCard = (TextView) itemView.findViewById(R.id.text_user_card);
            tvEnquiryCard = (TextView) itemView.findViewById(R.id.txt_enquiry_card);
            tvTestDriveCard = (TextView) itemView.findViewById(R.id.txt_td_card);
            tvVisitCard = (TextView) itemView.findViewById(R.id.txt_visit_card);
            tvBookingCard = (TextView) itemView.findViewById(R.id.txt_booking_card);
            tvRetailCard = (TextView) itemView.findViewById(R.id.txt_retail_card);
            tvUserTotalCard = (TextView) itemView.findViewById(R.id.text_user_total_card);
            viewEnquiryCard = (View) itemView.findViewById(R.id.enquiry_view_card);
            viewTdCard = (View) itemView.findViewById(R.id.td_view_card);
            viewVisitCard = (View) itemView.findViewById(R.id.visit_view_card);
            viewBookingCard = (View) itemView.findViewById(R.id.booking_view_card);
            viewReatilCard = (View) itemView.findViewById(R.id.retail_view_card);
            cardView = (CardView) itemView.findViewById(R.id.cardview_etvbr);
            llEnquriyCard = (LinearLayout) itemView.findViewById(R.id.enquiry_ll_card);
            tvTargetTitleCard = (TextView) itemView.findViewById(R.id.tv_target_title_card);
            tvTargetEnquiyCard = (TextView) itemView.findViewById(R.id.txt_target_enquiry_card);
            tvTargetTDCard = (TextView) itemView.findViewById(R.id.txt_target_td_card);
            tvTargetVisitCard = (TextView) itemView.findViewById(R.id.txt_target_visit_card);
            tvTargetBookingCard = (TextView) itemView.findViewById(R.id.txt_target_booking_card);
            tvTargetRetailCard = (TextView) itemView.findViewById(R.id.txt_target_retail_card);
            viewTargetCard = (View) itemView.findViewById(R.id.target_view_card);
            rlFrameLayoutCard = (RelativeLayout) itemView.findViewById(R.id.rl_frame_layout_card);
            tvLocationName = (TextView) itemView.findViewById(R.id.location_name);
            llTdCard = (LinearLayout) itemView.findViewById(R.id.td_ll_card);
            llVisitCard = (LinearLayout) itemView.findViewById(R.id.visit_ll_card);
            llBookingCard = (LinearLayout) itemView.findViewById(R.id.booking_ll_card);
            llRetailsCard = (LinearLayout) itemView.findViewById(R.id.retail_ll_card);
            llLostCard = (LinearLayout) itemView.findViewById(R.id.lost_ll_card);
            tvLostCard = (TextView) itemView.findViewById(R.id.txt_lost_card);
            viewLostCard = (View) itemView.findViewById(R.id.lost_view_card);
        }
    }

    private void goToDetailsOfEtvbr(int position, String title_name, String user_id, String etvbrtotal, String user_role_id, String user_location_id) {

        Intent etvbrDetails =  null;

        switch (position){
            case 0:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Enquiries");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 1:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Test Drives");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 2:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Visits");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 3:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Bookings");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 4:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Retails");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 5:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Lost Drop");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
        }
        if(etvbrDetails != null) {
            if(user_location_id.equalsIgnoreCase("")){
                pref.setLocationId(-1);
            }else {
                pref.setLocationId(Integer.parseInt(user_location_id));
            }
            context.startActivity(etvbrDetails);
        }
    }

}
