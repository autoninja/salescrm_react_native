package com.salescrm.telephony.telephonyModule.recorder;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.nll.nativelibs.callrecording.DeviceHelper;
import com.nll.nativelibs.callrecording.Native;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class AudioRecordThread implements Runnable {

    private static final String TAG = AudioRecordThread.class.getSimpleName();

    private static final int SAMPLE_RATE = 44100;
    private static final int SAMPLE_RATE_INDEX = 4;
    private static final int CHANNELS = 1;
    private static final int BIT_RATE = 32000;

    private final int bufferSize;
    private final MediaCodec mediaCodec;
    private final AudioRecord audioRecord;
    private final OutputStream outputStream;
    private final Context context;

    private OnRecorderFailedListener onRecorderFailedListener;

/*    private static long SERIAL = 1534885200;
    private static String KEY = "BF5A4F8D52A7213E44F83D9B5CD74FE0";*/

    //private static String TAG = "AudioRecordWrapper";
    private boolean useApi3;


    AudioRecordThread(Context context, OutputStream outputStream, Preferences pref, OnRecorderFailedListener onRecorderFailedListener) throws IOException {

        this.context = context;
        this.bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        this.audioRecord = createAudioRecord(this.bufferSize);
        this.mediaCodec = createMediaCodec(this.bufferSize);
        this.outputStream = outputStream;
        this.onRecorderFailedListener = onRecorderFailedListener;



        useApi3 = DeviceHelper.useApi3();
        Log.i(TAG, "Remaining license time in seconds  " + Native.getExpiry(Util.getLong(pref.getAcrSerial()), pref.getAcrLicenseKey()));
        Log.i(TAG, "Package and cert check result  " + Native.checkPackageAndCert(context));

        if (DeviceHelper.isAndroid71FixRequired()) {
            DeviceHelper.sleepForAndroid71();
            Native.fixAndroid71(Native.FIX_ANDROID_7_1_ON);
        }
        int result = useApi3 ? Native.start3(context, audioRecord, Util.getLong(pref.getAcrSerial()), pref.getAcrLicenseKey()) : Native.start7(context, audioRecord, Util.getLong(pref.getAcrSerial()), pref.getAcrLicenseKey());
        Log.d(TAG, "Start result " + result);


        this.mediaCodec.start();

        try {
            audioRecord.startRecording();
        } catch (Exception e) {
            Log.w(TAG, e);
            mediaCodec.release();
            throw new IOException(e);
        }
    }

    @Override
    public void run() {
        if (onRecorderFailedListener != null) {
            Log.d(TAG, "onRecorderStarted");
            onRecorderFailedListener.onRecorderStarted();
        }
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                int res = useApi3 ? Native.stop3() : Native.stop7();
                Log.d(TAG, "Stop result " + res);
            }
        }, 500);
        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
        ByteBuffer[] codecInputBuffers = mediaCodec.getInputBuffers();
        ByteBuffer[] codecOutputBuffers = mediaCodec.getOutputBuffers();

        try {
            while (!Thread.interrupted()) {

                boolean success = handleCodecInput(audioRecord, mediaCodec, codecInputBuffers, Thread.currentThread().isAlive());
                if (success)
                    handleCodecOutput(mediaCodec, codecOutputBuffers, bufferInfo, outputStream);
            }
        } catch (IOException e) {
            Log.w(TAG, e);
        } finally {
           try{
               mediaCodec.stop();
           }
           catch (Exception me) {
               Crashlytics.log(Log.ERROR, "AUDIO_RECORD_THREAD","Failed to stop mediaCodec_"+Build.MODEL+'_'+Build.VERSION.SDK_INT);
               Crashlytics.logException(me);
           }

            try {
                audioRecord.stop();
            }
            catch (Exception e) {
                Crashlytics.log(Log.ERROR, "AUDIO_RECORD_THREAD","Failed to stop audioRecord_"+Build.MODEL+'_'+Build.VERSION.SDK_INT);
                Crashlytics.logException(e);
            }
            try{
                mediaCodec.release();
            }
            catch (Exception e) {
                Crashlytics.log(Log.ERROR, "AUDIO_RECORD_THREAD","Failed to release mediaCodec_"+Build.MODEL+'_'+Build.VERSION.SDK_INT);
                Crashlytics.logException(e);
            }
            try{
                audioRecord.release();
            }
            catch (Exception e) {
                Crashlytics.log(Log.ERROR, "AUDIO_RECORD_THREAD","Failed to release audioRecord_"+Build.MODEL+'_'+Build.VERSION.SDK_INT);
                Crashlytics.logException(e);
            }


            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private boolean handleCodecInput(AudioRecord audioRecord,
                                     MediaCodec mediaCodec, ByteBuffer[] codecInputBuffers,
                                     boolean running) throws IOException {
        byte[] audioRecordData = new byte[bufferSize];
        int length = audioRecord.read(audioRecordData, 0, audioRecordData.length);
        if (length == AudioRecord.ERROR_BAD_VALUE ||
                length == AudioRecord.ERROR_INVALID_OPERATION ||
                length > bufferSize) {

            if (length > bufferSize) {
                if (onRecorderFailedListener != null) {
                    Log.d(TAG, "length != BufferSize calling onRecordFailed");
                    onRecorderFailedListener.onRecorderFailed();
                }
                return false;
            }
        }


        int codecInputBufferIndex = mediaCodec.dequeueInputBuffer(10 * 1000);

        if (codecInputBufferIndex >= 0) {
            ByteBuffer codecBuffer = codecInputBuffers[codecInputBufferIndex];
            codecBuffer.clear();
            codecBuffer.put(audioRecordData);
            try{
                mediaCodec.queueInputBuffer(codecInputBufferIndex, 0, length, 0, running ? 0 : MediaCodec.BUFFER_FLAG_END_OF_STREAM);

            }
            catch (Exception e) {
                Crashlytics.log(Log.ERROR, "AUDIO_RECORD_THREAD","Failed to queueInputBuffer_"+Build.MODEL+'_'+Build.VERSION.SDK_INT);
                Crashlytics.logException(e);
            }
        }

        return true;
    }

    private void handleCodecOutput(MediaCodec mediaCodec,
                                   ByteBuffer[] codecOutputBuffers,
                                   MediaCodec.BufferInfo bufferInfo,
                                   OutputStream outputStream)
            throws IOException {
        int codecOutputBufferIndex = mediaCodec.dequeueOutputBuffer(bufferInfo, 0);

        while (codecOutputBufferIndex != MediaCodec.INFO_TRY_AGAIN_LATER) {
            if (codecOutputBufferIndex >= 0) {
                ByteBuffer encoderOutputBuffer = codecOutputBuffers[codecOutputBufferIndex];

                encoderOutputBuffer.position(bufferInfo.offset);
                encoderOutputBuffer.limit(bufferInfo.offset + bufferInfo.size);

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != MediaCodec.BUFFER_FLAG_CODEC_CONFIG) {
                    byte[] header = createAdtsHeader(bufferInfo.size - bufferInfo.offset);


                    outputStream.write(header);

                    byte[] data = new byte[encoderOutputBuffer.remaining()];
                    encoderOutputBuffer.get(data);
                    outputStream.write(data);
                }

                encoderOutputBuffer.clear();

                mediaCodec.releaseOutputBuffer(codecOutputBufferIndex, false);
            } else if (codecOutputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                codecOutputBuffers = mediaCodec.getOutputBuffers();
            }

            codecOutputBufferIndex = mediaCodec.dequeueOutputBuffer(bufferInfo, 0);
        }
    }


    private byte[] createAdtsHeader(int length) {
        int frameLength = length + 7;
        byte[] adtsHeader = new byte[7];

        adtsHeader[0] = (byte) 0xFF; // Sync Word
        adtsHeader[1] = (byte) 0xF1; // MPEG-4, Layer (0), No CRC
        adtsHeader[2] = (byte) ((MediaCodecInfo.CodecProfileLevel.AACObjectLC - 1) << 6);
        adtsHeader[2] |= (((byte) SAMPLE_RATE_INDEX) << 2);
        adtsHeader[2] |= (((byte) CHANNELS) >> 2);
        adtsHeader[3] = (byte) (((CHANNELS & 3) << 6) | ((frameLength >> 11) & 0x03));
        adtsHeader[4] = (byte) ((frameLength >> 3) & 0xFF);
        adtsHeader[5] = (byte) (((frameLength & 0x07) << 5) | 0x1f);
        adtsHeader[6] = (byte) 0xFC;

        return adtsHeader;
    }

    private AudioRecord createAudioRecord(int bufferSize) {
        AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, bufferSize * 10);

        if (audioRecord.getState() != AudioRecord.STATE_INITIALIZED) {
            Log.d(TAG, "Unable to initialize AudioRecord");
            throw new RuntimeException("Unable to initialize AudioRecord");
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (android.media.audiofx.NoiseSuppressor.isAvailable()) {
                android.media.audiofx.NoiseSuppressor noiseSuppressor = android.media.audiofx.NoiseSuppressor
                        .create(audioRecord.getAudioSessionId());
                if (noiseSuppressor != null) {
                    noiseSuppressor.setEnabled(true);
                }
            }
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (android.media.audiofx.AutomaticGainControl.isAvailable()) {
                android.media.audiofx.AutomaticGainControl automaticGainControl = android.media.audiofx.AutomaticGainControl
                        .create(audioRecord.getAudioSessionId());
                if (automaticGainControl != null) {
                    automaticGainControl.setEnabled(true);
                }
            }
        }


        return audioRecord;
    }

    private MediaCodec createMediaCodec(int bufferSize) throws IOException {
        MediaCodec mediaCodec = MediaCodec.createEncoderByType("audio/mp4a-latm");
        MediaFormat mediaFormat = new MediaFormat();

        mediaFormat.setString(MediaFormat.KEY_MIME, "audio/mp4a-latm");
        mediaFormat.setInteger(MediaFormat.KEY_SAMPLE_RATE, SAMPLE_RATE);
        mediaFormat.setInteger(MediaFormat.KEY_CHANNEL_COUNT, CHANNELS);
        mediaFormat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, bufferSize);
        mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, BIT_RATE);
        mediaFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);

        try {
            mediaCodec.configure(mediaFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        } catch (Exception e) {
            Log.w(TAG, e);
            mediaCodec.release();
            throw new IOException(e);
        }

        return mediaCodec;
    }

    interface OnRecorderFailedListener {
        void onRecorderFailed();

        void onRecorderStarted();
    }

    public AudioRecord getAudioRecord() {
        return audioRecord;
    }
}