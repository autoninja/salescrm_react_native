package com.salescrm.telephony.db.teams;

import com.salescrm.telephony.db.crm_user.TeamData;
import com.salescrm.telephony.db.crm_user.TeamUserDB;
import com.salescrm.telephony.db.crm_user.UsersDB;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 9/12/16.
 */

public class TeamLocationDB extends RealmObject {

    @PrimaryKey
    private int location_id;

    private RealmList<TeamMainDB> teams;
    private RealmList<TeamUserDetailsDB> users;

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public RealmList<TeamMainDB> getTeams() {
        return teams;
    }

    public void setTeams(RealmList<TeamMainDB> teams) {
        this.teams = teams;
    }

    public RealmList<TeamUserDetailsDB> getUsers() {
        return users;
    }

    public void setUsers(RealmList<TeamUserDetailsDB> users) {
        this.users = users;
    }
}
