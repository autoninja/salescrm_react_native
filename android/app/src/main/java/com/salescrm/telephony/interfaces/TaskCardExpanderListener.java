package com.salescrm.telephony.interfaces;

/**
 * Created by bharath on 29/6/17.
 */

public interface TaskCardExpanderListener {
    void onExpandCard(int position,int scheduledActivityId,boolean isClosed);
}
