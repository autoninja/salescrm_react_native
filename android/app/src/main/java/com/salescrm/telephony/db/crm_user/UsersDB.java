package com.salescrm.telephony.db.crm_user;

import io.realm.RealmObject;

/**
 * Created by bharath on 9/12/16.
 */

public class UsersDB extends RealmObject {
    private String id;
    private String name;
    private String mobile_number;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }
}
