import React from 'react';
import { createStackNavigator } from 'react-navigation';
import TeamShuffleMain from '../../containers/TeamShuffle/team_shuffle_main'
import HamburgerIcon from "../../components/uielements/HamburgerIcon";
import UserInfoService from '../../storage/user_info_service'

const TeamShuffleRouter = createStackNavigator(
    {
        TeamShuffle: {
            screen: props => (
                <TeamShuffleMain
                    {...props}
                    isUserBH={UserInfoService.isUserBranchHead()}
                    isUserSM={UserInfoService.isUserSalesManager()}
                    allLocations={UserInfoService.getLocationsList()}
                />),
            navigationOptions: ({ navigation }) => ({
                title: 'Team Shuffle',
                headerBackTitle: null,
                headerLeft: <HamburgerIcon navigationProps={navigation} />,
                headerStyle: {
                    backgroundColor: '#2e5e86'
                },
                headerTransperent: true,
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                    width: '100%',
                    textAlign: 'left',
                },
            })
        }
    },
    {
        initialRouteName: "TeamShuffle",
        initialRouteKey: "TeamShuffle"
    }
);

export default TeamShuffleRouter;