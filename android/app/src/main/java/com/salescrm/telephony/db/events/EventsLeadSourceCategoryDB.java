package com.salescrm.telephony.db.events;

import io.realm.RealmList;
import io.realm.RealmObject;

public class EventsLeadSourceCategoryDB extends RealmObject {
    private int id;
    private String name;
    private RealmList<EventsLeadSourceDB> eventsLeadSourceDBS;

    public EventsLeadSourceCategoryDB() {
    }

    public EventsLeadSourceCategoryDB(int id, String name, RealmList<EventsLeadSourceDB> eventsLeadSourceDBS) {
        this.id = id;
        this.name = name;
        this.eventsLeadSourceDBS = eventsLeadSourceDBS;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<EventsLeadSourceDB> getEventsLeadSourceDBS() {
        return eventsLeadSourceDBS;
    }

    public void setEventsLeadSourceDBS(RealmList<EventsLeadSourceDB> eventsLeadSourceDBS) {
        this.eventsLeadSourceDBS = eventsLeadSourceDBS;
    }
}
