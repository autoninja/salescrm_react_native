import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Button, Flex, WhiteSpace, WingBlank } from '@ant-design/react-native';

class Header extends Component {

    constructor(props) {
        super(props);
    }

    render()
    {
        return (
            <View
                style={{
                    backgroundColor: '#3A5187',
                    height: 50,
                }}
                {...this.props}
            >
                {this.props.children}
            </View>
        )
    }
}

export default Header;