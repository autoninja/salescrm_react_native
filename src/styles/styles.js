import { StyleSheet, Platform } from 'react-native';
var Dimensions = require('Dimensions');

var { width, height } = Dimensions.get('window');
export default StyleSheet.create({

 MainContainer :{

  flex:1,
  paddingTop: (Platform.OS) === 'ios' ? 20 : 10,
  paddingBottom: 10,
  paddingLeft : 10,
  paddingRight:10,
  backgroundColor: '#2e5e86',

},
MainContainerBlack :{

  flex:1,
  paddingTop: (Platform.OS) === 'ios' ? 20 : 10,
  paddingBottom: 20,
  paddingLeft : 10,
  paddingRight:10,
  backgroundColor: '#303030',

},
MainContainerWhite :{

  flex:1,
  paddingTop: (Platform.OS) === 'ios' ? 20 : 10,
  paddingBottom: 20,
  paddingLeft : 10,
  paddingRight:10,
  backgroundColor: '#FFF',

},
MainContainerYellow :{

  flex:1,
  backgroundColor: '#BEC320',
  borderRadius:6,

},
contentContainer: {
    paddingVertical: 20,
    width: '100%',
    height: '100%',
    backgroundColor: '#2e3486',
    alignItems: 'center',

  },
icon: {
    width: 24,
    height: 24,
  },
sideMenuContainer: {

 width: '100%',
 height: '100%',
 backgroundColor: '#2e3486',
 alignItems: 'center',
 paddingTop: 20
},

sideMenuProfileIcon:
{
 resizeMode: 'center',
 width: 80,
 height: 80,
 borderRadius: 80/2,
 marginTop:20,
},

sideMenuIcon:
{

 width: 20,
 height: 20,
 marginRight: 10,
 marginLeft: 20,


},
icon:
{

 width: 24,
 height: 24,

},

menuText:{

 fontSize: 20,
 color: '#fff',
 paddingTop:5,
 paddingBottom:5,

},
appTitle:{

 fontSize: 25,
 color: '#fff',
 paddingTop:10,

},

subTitle:{

 fontSize: 15,
 color: '#e8e5e5',
 paddingTop:5,

},

Alert_Main_View:{

  alignItems: 'stretch',
  justifyContent: 'center',
  backgroundColor:'rgba(0,0,0,0.6)',
  width: '100%',
  height : '100%',
  padding:20,

},

Alert_Filter:{

  alignItems: 'stretch',
  justifyContent: 'center',
  backgroundColor:'rgba(0,0,0,0.6)',
  width: '100%',
  height : '100%',
  paddingRight:20,
  flex:1,

},


TextStyle:{
    color:'#fff',
    textAlign:'center',
    fontSize: 22,
    marginTop: -5
},

overlay: {
   flex: 1,
   position: 'absolute',
   left: 0,
   top: 0,
   opacity: 0.5,
   backgroundColor: 'black',
   width: width,
   height:height,
 },

 switch : {
     transform: [{ scaleX: .8 }, { scaleY: .8 }],
 },
 circle: {
    width: 10,
    height: 10,
    borderRadius: 10/2
}

});
