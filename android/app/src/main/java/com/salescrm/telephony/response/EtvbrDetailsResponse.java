package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by nndra on 24-Nov-17.
 */

public class EtvbrDetailsResponse {
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private List<LeadDetails> leadDetails;

        private String count;

        private String offset;

        public List<LeadDetails> getLeadDetails ()
        {
            return leadDetails;
        }

        public void setLeadDetails (List<LeadDetails> leadDetails)
        {
            this.leadDetails = leadDetails;
        }

        public String getCount ()
        {
            return count;
        }

        public void setCount (String count)
        {
            this.count = count;
        }

        public String getOffset ()
        {
            return offset;
        }

        public void setOffset (String offset)
        {
            this.offset = offset;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [leadDetails = "+leadDetails+", count = "+count+", offset = "+offset+"]";
        }
    }

    public class LeadDetails
    {
        private String customer_gender;

        private String lead_tag;

        private String scheduled_activity_date;

        private String schedule_activity_id;

        private String car_model_name;

        private String customer_name;

        private String activity_name;

        private String lead_id;

        private String title;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCustomer_gender ()
        {
            return customer_gender;
        }

        public void setCustomer_gender (String customer_gender)
        {
            this.customer_gender = customer_gender;
        }

        public String getLead_tag ()
        {
            return lead_tag;
        }

        public void setLead_tag (String lead_tag)
        {
            this.lead_tag = lead_tag;
        }

        public String getScheduled_activity_date ()
        {
            return scheduled_activity_date;
        }

        public void setScheduled_activity_date (String scheduled_activity_date)
        {
            this.scheduled_activity_date = scheduled_activity_date;
        }

        public String getSchedule_activity_id ()
        {
            return schedule_activity_id;
        }

        public void setSchedule_activity_id (String schedule_activity_id)
        {
            this.schedule_activity_id = schedule_activity_id;
        }

        public String getCar_model_name ()
        {
            return car_model_name;
        }

        public void setCar_model_name (String car_model_name)
        {
            this.car_model_name = car_model_name;
        }

        public String getCustomer_name ()
        {
            return customer_name;
        }

        public void setCustomer_name (String customer_name)
        {
            this.customer_name = customer_name;
        }

        public String getActivity_name ()
        {
            return activity_name;
        }

        public void setActivity_name (String activity_name)
        {
            this.activity_name = activity_name;
        }

        public String getLead_id ()
        {
            return lead_id;
        }

        public void setLead_id (String lead_id)
        {
            this.lead_id = lead_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [customer_gender = "+customer_gender+", lead_tag = "+lead_tag+", scheduled_activity_date = "+scheduled_activity_date+", schedule_activity_id = "+schedule_activity_id+", car_model_name = "+car_model_name+", customer_name = "+customer_name+", activity_name = "+activity_name+", lead_id = "+lead_id+"]";
        }
    }

    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }

}