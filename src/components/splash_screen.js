import React, { Component } from 'react';

import { View, Text } from 'react-native';
import axios from 'axios';
import RestClient from '../network/rest_client'
import UserInfoService from '../storage/user_info_service'
import DealershipService from '../storage/dealership_service'
import DealershipModel from '../models/dealership_model'
import Utils from '../utils/Utils'

import { UserInfoModel, UserBrandsModel, UserRolesModel, UserLocationsModel } from '../models/user_info_model'

import styles from '../styles/styles'

import { StackActions, NavigationActions } from 'react-navigation';

import CleverTapPush from '../clevertap/CleverTapPush'

import * as async_storage from '../storage/async_storage';

import GamificationConfigurationService from '../storage/gamification_configuration_service'
import NavigationService from '../navigation/NavigationService'


export default class SplashScreen extends Component {

  constructor(props) {
    super(props);
    this.state = { loading: true, dataRefreshed: false };
  }
  navigate(routeName, params) {
    var resetAction = StackActions.reset({
      index: 0, // <-- currect active route from actions array
      key: null,
      actions: [
        NavigationActions.navigate({ routeName, params }),
      ],
    });;
    this.props.navigation.dispatch(resetAction);
  }

  componentDidMount() {
    var { navigation } = this.props;
    async_storage.getToken().then((token) => {
      if (token) {
        global.token = token;
      }
      async_storage.getSelectedGameLocationId().then((selectedGameLocationId) => {
        if (selectedGameLocationId) {
          global.selectedGameLocationId = selectedGameLocationId;
        }
        else if (GamificationConfigurationService.getLocations().length > 0) {
          global.selectedGameLocationId = GamificationConfigurationService.getLocations()[0].id;
          async_storage.setSelectedGameLocationId(global.selectedGameLocationId.toString()).then(() => {
          });
        }
        async_storage.isLoggedIn().then((loginStatus) => {
          async_storage.getLastOpenedDate().then((date) => {
            if (loginStatus == "1") {
              if (!date || Utils.dateDiffInDays(new Date(parseInt(date)), new Date()) != 0) {

                this.navigate("RefreshData");
              }
              else {
                async_storage.getAppUserId().then((id) => {
                  if (id) {
                    global.appUserId = id;
                  }

                  async_storage.getHereMapAppId().then((appId) => {
                    if (appId) {
                      global.HERE_MAP_APP_ID = appId;
                    }
                  })
                  async_storage.getHereMapAppCode().then((appCode) => {
                    if (appCode) {
                      global.HERE_MAP_APP_CODE = appCode;
                    }
                  })
                  async_storage.getGovtApiKeys().then((keys) => {
                    if (keys) {
                      global.GOVT_API_KEYS = keys;
                    }
                  })

                  if (global.OPEN_C360_WITH_DEEP_LINK) {
                    this.navigate("C360MainScreen", { leadId: global.OPEN_C360_WITH_DEEP_LINK_LEAD_ID });
                  } else {
                    this.navigate("Home");
                  }

                })
              }
            }
            else {
              this.navigate("Login");
            }
            // var dataRefreshed = false
            // if(date) {
            //   dataRefreshed = true;
            // }
            // if(result === '1') {
            //     this.setState({ loading:false,isLoggedIn: true, dataRefreshed:dataRefreshed });
            // }
            // else {
            //     this.setState({ loading:false, dataRefreshed:dataRefreshed});
            // }
            // console.log('lastopeneddate'+date);
          });


        });
      });

    });


  }
  render() {
    return (
      <View style={{
        backgroundColor: '#fff', flex: 1, justifyContent: 'center',
        alignItems: 'center',
      }}>

      </View>)


  }
}
