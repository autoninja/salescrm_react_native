import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Alert } from 'react-native';
import { FAB } from 'react-native-paper';
export default class CarModelItemExpandableETVBRL extends Component{
    constructor(props) {
        super(props);
        this.state = {expand:false, arrray:[1,2,3]};
    }
    expand() {
        this.setState({expand : !this.state.expand});
    }
    onLongPress() {
        // if(this.props.showPercentage) {
        //     Alert.alert('Long Disabled');
        // }
        // else {
        //     Alert.alert('Long Enabled');
        // }
    }
    render() {
        let data = this.props.data;
        let e,t,v,b,r,l;
        if(this.props.showPercentage) {
            e = data.rel.e;
            t = data.rel.t;
            v = data.rel.v;
            b = data.rel.b;
            r = data.rel.r;
            l = data.rel.l;
        }
        else {
            e = data.abs.e;
            t = data.abs.t;
            v = data.abs.v;
            b = data.abs.b;
            r = data.abs.r;
            l = data.abs.l;
        }
        return(
            <View style={{paddingTop:6, margin:2}}>
                <TouchableWithoutFeedback onPress={()=> {
                    this.expand();
                }}>
                <View style={styles.container}>
                    <View style={styles.itemEmpty}>
                        <Text 
                        style={styles.itemTextCar}
                        numberOfLines={2} 
                        ellipsizeMode='tail'
                        >{data.info.carName}</Text>
                    </View>

                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{e}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{t}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{v}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{b}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{r}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={[styles.itemText, {color:'red'}]}>{l}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                </View>        
                </TouchableWithoutFeedback>



                {this.state.expand && data.sources.map((item, index)=> {
                    let eInner,tInner,vInner,bInner,rInner,lInner;
                    if( item.rel && item.abs) {
                        if(this.props.showPercentage) {
                            eInner = item.rel.enquiries;
                            tInner = item.rel.tdrives;
                            vInner = item.rel.visits;
                            bInner = item.rel.bookings;
                            rInner = item.rel.retails;
                            lInner = item.rel.lost_drop;
                        }
                        else {
                            eInner = item.abs.enquiries;
                            tInner = item.abs.tdrives;
                            vInner = item.abs.visits;
                            bInner = item.abs.bookings;
                            rInner = item.abs.retails;
                            lInner = item.abs.lost_drop;
                        }
                    }
                    return(
                    <View style={styles.containerWhite} key={index+''}>
                    <View style={styles.itemEmpty}>
                        <Text 
                        style={styles.itemTextInner}
                        numberOfLines={2} 
                        ellipsizeMode='tail'
                        >{item.name}</Text>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{eInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{tInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{vInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{bInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{rInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={[styles.itemText, {color:'red'}]}>{lInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>

                 
                </View>
                    );
                })}
                


            </View>
        )
    }
}

let styles = StyleSheet.create({
    container: {
        height:48,
        backgroundColor:'#eceff1',
        borderRadius:4,
        paddingTop:8,
        paddingBottom:8,
        paddingLeft:2,
        paddingRight:2,
        flexDirection:'row',
        elevation:2,
    },
    containerWhite: {
        height:48,
        backgroundColor:'#fff',
        paddingTop:8,
        paddingBottom:8,
        paddingLeft:2,
        paddingRight:2,
        flexDirection:'row',
        borderBottomColor:'#E7EAEF',
        borderBottomWidth:1,
    },
    itemEmpty: {
        flex:1.5,
        alignSelf:'center',
    },
    item: {
        flex:1,
        alignSelf:'center',
    },
    itemText: {
        fontSize:14,
        color:'#4B4B4B',
        alignSelf:'center',
        textAlign:'center'
    },
    itemTextCar: {
        fontSize:14,
        paddingLeft:2,
        color:'#2D4F8B',
        alignSelf:'flex-start',
        textAlign:'left'
    },
    itemTextInner: {
        fontSize:12,
        color:'#2D4F8B',
        alignSelf:'flex-start',
        textAlign:'left'
    },
})