package com.salescrm.telephony.response;

/**
 * Created by bannhi on 19/6/17.
 */

public class CancelBookingResponse {

    private String message;

    private String statusCode;

    private Result result;

    private Error error;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", statusCode = " + statusCode + ", result = " + result + ", error = " + error + "]";
    }

    public class Error {
        private String details;

        private String type;

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "ClassPojo [details = " + details + ", type = " + type + "]";
        }
    }

    public class Result {
        private Integer[] scheduled_activity_ids;
        private Integer[] activity_ids;
        private Integer action_id;
        private String next_stage_id;
        private String lead_last_updated;


        public Integer[] getScheduled_activity_ids() {
            return scheduled_activity_ids;
        }

        public void setScheduled_activity_ids(Integer[] scheduled_activity_ids) {
            this.scheduled_activity_ids = scheduled_activity_ids;
        }

        public Integer[] getActivity_ids() {
            return activity_ids;
        }

        public void setActivity_ids(Integer[] activity_ids) {
            this.activity_ids = activity_ids;
        }

        public Integer getAction_id() {
            return action_id;
        }

        public void setAction_id(Integer action_id) {
            this.action_id = action_id;
        }

        public String getNext_stage_id() {
            return next_stage_id;
        }

        public void setNext_stage_id(String next_stage_id) {
            this.next_stage_id = next_stage_id;
        }

        public String getLead_last_updated() {
            return lead_last_updated;
        }

        public void setLead_last_updated(String lead_last_updated) {
            this.lead_last_updated = lead_last_updated;
        }

        @Override
        public String toString() {
            return "ClassPojo [next_stage_id = " + next_stage_id + "]";
        }
    }
}
