import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'


import TodayTaskItemChild from './todays_task_item_child_ilom'

export default class TodaysTaskItemExpandableILom extends Component {
  constructor(props) {
    super(props);
    this.state = { expand: false, showImagePlaceHolder: true };
  }
  _etvbrLongPress(from, info) {
    this.props.onLongPress(from, info)
    console.log(info)
  }
  render() {

    let team_leader = this.props.team_leader;
    let imageSource;
    let placeHolder

    imageSource = { uri: this.props.info.dp_url };
    placeHolder = <View style={{ position: 'absolute', display: (this.state.showImagePlaceHolder ? 'flex' : 'none'), width: 36, height: 36, borderRadius: 36 / 2, backgroundColor: '#FF8000', alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#fff' }}>{this.props.info.name.charAt(0).toUpperCase()}</Text></View>;;


    let sc_views = []
    for (var i = 0; i < team_leader.sales_consultants.length; i++) {
      var sc = team_leader.sales_consultants[i];
      sc_views.push(

        <TodayTaskItemChild
          openTasksList={this.props.openTasksList.bind(this)}
          onLongPress={this.props.onLongPress.bind(this)}
          key={sc.info.id}

          abs={
            {
              calls: sc.info.abs.calls,
              tdrives: sc.info.abs.tdrives,
              visits: sc.info.abs.visits,
              post_bookings: sc.info.abs.post_bookings,
              deliveries: sc.info.abs.deliveries,
              pending_total_count: sc.info.abs.pending_total_count,
              uncalled_tasks: sc.info.abs.uncalled_tasks,
              first_call: sc.info.abs.visits,
              call_back: sc.info.abs.calls,
              prospect: sc.info.abs.tdrives,

            }}

          info={
            {
              id: sc.info.id,
              dp_url: sc.info.dp_url,
              name: sc.info.name,
              user_id: sc.info.id,
              location_id: this.props.info.location_id,
              user_role_id: 4,
            }
          }


        ></TodayTaskItemChild>)

    }

    return (
      <View style={{ marginBottom: 8 }}>
        <TouchableOpacity onPress={() => this.setState({ expand: !this.state.expand })}>
          <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#eceff1', borderColor: '#bdbdbd', borderWidth: 0.3, borderRadius: 6, alignItems: 'center' }}>

            <View style={{ flex: 1, alignItems: 'center' }} >

              <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                {placeHolder}
                <TouchableOpacity
                  onPress={() => this.setState({ expand: !this.state.expand })}
                  style={{ position: 'absolute', }}
                  onLongPress={() => this.props.onLongPress('user_info', { info: { name: this.props.info.name, dp_url: this.props.info.dp_url } })}>
                  <Image
                    source={imageSource}
                    style={{ width: 36, height: 36, borderRadius: 36 / 2, }}
                  />
                </TouchableOpacity>
              </View>





            </View>





            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#303030' }} >{this.props.abs.first_call}</Text>
              </View>


            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#303030' }} >{this.props.abs.call_back}</Text>
              </View>

            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
            }}>

              <Text style={{ color: '#303030' }} >{this.props.abs.prospect}</Text>
            </View>




            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#cfd8dc',
              borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
            }}>

              <Text style={{ color: '#F5A623' }} >{this.props.abs.pending_total_count}</Text>
            </View>
            {
              /*
              <View style= {{flex:1,height:'100%',borderLeftColor: '#cfd8dc',
                borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
                  <TouchableOpacity  hitSlop={{top: 10, bottom: 10, left: 10, right: 10}} onLongPress={ () => this.props.onLongPress('Uncalled Tasks',{info: this.props.info})}>
                    <Text style={{ color:'red',textDecorationLine: 'underline',
                      textDecorationColor: '#F20407'}} >{this.props.abs.uncalled_tasks}</Text>
                  </TouchableOpacity>
              </View>
              */
            }





          </View>
        </TouchableOpacity>

        {this.state.expand && (
          <View>
            {sc_views}
          </View>
        )}
      </View>
    );
  }
}
