package com.salescrm.telephony.dataitem;

/**
 * Created by akshata on 3/8/16.
 */
public class Detail_Mobile_model {

    String number;
    int value;

    public Detail_Mobile_model(String number,int status) {
        this.number = number;
        this.value=status;
    }

    public Detail_Mobile_model(int value)
    {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
