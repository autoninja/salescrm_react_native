package com.salescrm.telephony.fragments.addlead;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddLeadActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.AppConfigDB;
import com.salescrm.telephony.db.EnqSourceCategoryDB;
import com.salescrm.telephony.db.EnqSourceDB;
import com.salescrm.telephony.db.EnqSubInnerSourceDB;
import com.salescrm.telephony.interfaces.AddLeadCommunication;
import com.salescrm.telephony.model.AddLeadLeadInputData;
import com.salescrm.telephony.model.ViewPagerSwiped;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import java.util.Calendar;
import java.util.Locale;

import io.realm.Realm;

import static com.salescrm.telephony.utils.Util.showDatePicker;

/**
 * Created by bharath on 30/5/16.
 */
public class AddLeadDetailsFragment extends Fragment implements View.OnClickListener, com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener {
    public static final String EXPECTED_PUR_DATE = "expPurDate";
    private static final String LEAD_DATE = "leadDate";
    AutoCompleteTextView spinnerEnqSrc;
    Spinner spinnerModPay;
    TextView tvLeadSelectDateHeader, tvLeadSelectDateVal, tvLeadExpPurDateHeader, tvLeadExpPurDateVal,vinNoTitle;
    private Realm realm;
    private AddLeadCommunication addLeadCommunication;
    private String TAG = "AddLeadDetailsFragment";
    private TextView tvSpinnerSrcHeader;
    private View view;
    private LinearLayout llLeadSourceCategory;
    private EditText vinNoEdit;
    private AutoCompleteTextView autoTvSourceCategory;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_lead_details, container, false);
        realm = Realm.getDefaultInstance();
        spinnerEnqSrc = (AutoCompleteTextView) view.findViewById(R.id.spinner_add_lead_enq_src);
        tvSpinnerSrcHeader = (TextView) view.findViewById(R.id.add_lead_enq_src_header);
        spinnerModPay = (Spinner) view.findViewById(R.id.spinner_add_lead_mod_pay);
        tvLeadSelectDateHeader = (TextView) view.findViewById(R.id.tv_lead_date);
        tvLeadSelectDateVal = (TextView) view.findViewById(R.id.tv_add_lead_date_val);
        tvLeadExpPurDateHeader = (TextView) view.findViewById(R.id.tv_lead_exp_pur_date_header);
        tvLeadExpPurDateVal = (TextView) view.findViewById(R.id.tv_lead_exp_pur_date_val);
        vinNoTitle =  (TextView) view.findViewById(R.id.vin_no_title);
        vinNoEdit =  (EditText) view.findViewById(R.id.vin_edt);
        llLeadSourceCategory = (LinearLayout) view.findViewById(R.id.ll_add_lead_details_source_category);
        autoTvSourceCategory = (AutoCompleteTextView) view.findViewById(R.id.spinner_add_lead_enq_src_category);


        tvLeadExpPurDateVal.setOnClickListener(this);
        tvLeadExpPurDateHeader.setOnClickListener(this);
        tvLeadSelectDateVal.setOnClickListener(this);
        tvLeadSelectDateHeader.setOnClickListener(this);


        if(isShowLeadSourceCategory()){
            llLeadSourceCategory.setVisibility(View.VISIBLE);
            ArrayAdapter<String> adapterSub = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, getEnquirySubSource());
            autoTvSourceCategory.setAdapter(adapterSub);
            autoTvSourceCategory.setThreshold(0);
            autoTvSourceCategory.requestFocus();

            autoTvSourceCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    spinnerEnqSrc.setText("");
                    showAlert(ContextCompat.getColor(getContext(), R.color.search_heading_color));
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                            getEnquirySource(getEnquirySourceCategoryId()));
                    spinnerEnqSrc.setAdapter(adapter);
                    spinnerEnqSrc.setThreshold(0);
                    spinnerEnqSrc.requestFocus();
                }
            });
        }
        else {
            llLeadSourceCategory.setVisibility(View.GONE);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, getEnquirySource());
            spinnerEnqSrc.setAdapter(adapter);
            spinnerEnqSrc.setThreshold(0);
        }

        autoTvSourceCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                spinnerEnqSrc.setAdapter(null);
                spinnerEnqSrc.setText("");
            }
        });


        //Init Date
        Calendar currentCal = Calendar.getInstance();
        tvLeadSelectDateVal.setText(String.format(Locale.getDefault(), "%d %s %d", currentCal.get(Calendar.DAY_OF_MONTH) , currentCal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), currentCal.get(Calendar.YEAR)));
        tvLeadSelectDateVal.setTag(R.id.autoninja_date,currentCal);

        //


        autoTvSourceCategory.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(TextUtils.isEmpty(autoTvSourceCategory.getText())){

                    autoTvSourceCategory.showDropDown();
                }
                return false;
            }
        });

        autoTvSourceCategory.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && TextUtils.isEmpty(autoTvSourceCategory.getText()) && autoTvSourceCategory.getAdapter() != null){
                    autoTvSourceCategory.showDropDown();
                    Util.HideKeyboard(getContext());

                }
            }
        });
        spinnerEnqSrc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(!tvSpinnerSrcHeader.isEnabled()) {
                    tvSpinnerSrcHeader.setEnabled(true);
                    showAlert(ContextCompat.getColor(getContext(), R.color.search_heading_color));

                }
                if(!TextUtils.isEmpty(spinnerEnqSrc.getText())){
                    spinnerEnqSrc.showDropDown();
                }
              /*  if(getEnquirySourceId()==37){
                    vinNoTitle.setVisibility(View.VISIBLE);
                    vinNoEdit.setVisibility(View.VISIBLE);
                }else{
                    vinNoTitle.setVisibility(View.GONE);
                    vinNoEdit.setVisibility(View.GONE);
                }*/
                return false;
            }
        });
        spinnerEnqSrc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(getEnquirySourceId()==37){
                    vinNoTitle.setVisibility(View.VISIBLE);
                    vinNoEdit.setVisibility(View.VISIBLE);
                }else{
                    vinNoTitle.setVisibility(View.GONE);
                    vinNoEdit.setVisibility(View.GONE);
                }
            }
        });
        spinnerEnqSrc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(getEnquirySourceId()==37){
                    vinNoTitle.setVisibility(View.VISIBLE);
                    vinNoEdit.setVisibility(View.VISIBLE);
                }else{
                    vinNoTitle.setVisibility(View.GONE);
                    vinNoEdit.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerEnqSrc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && TextUtils.isEmpty(spinnerEnqSrc.getText()) && spinnerEnqSrc.getAdapter() != null){
                    spinnerEnqSrc.showDropDown();

                }
               /* if(getEnquirySourceId()==37){
                    vinNoTitle.setVisibility(View.VISIBLE);
                    vinNoEdit.setVisibility(View.VISIBLE);
                }else{
                    vinNoTitle.setVisibility(View.GONE);
                    vinNoEdit.setVisibility(View.GONE);
                }*/
            }
        });
        spinnerEnqSrc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(spinnerEnqSrc.getText())){
                    spinnerEnqSrc.showDropDown();

                }
              /*  if(getEnquirySourceId()==37){
                    vinNoTitle.setVisibility(View.VISIBLE);
                    vinNoEdit.setVisibility(View.VISIBLE);
                }else{
                    vinNoTitle.setVisibility(View.GONE);
                    vinNoEdit.setVisibility(View.GONE);
                }*/
            }
        });



        setSpinnerAdapter(spinnerModPay, WSConstants.LEAD_MODE_OF_PAYMENTS);


        return view;


    }

    private String[] getEnquirySource(int enquirySourceCategoryId) {

        EnqSourceCategoryDB enqSourceCategoryDB =
                realm.where(EnqSourceCategoryDB.class).equalTo("id", enquirySourceCategoryId).findFirst();

        String[] arr = new String[0];
        if (enqSourceCategoryDB != null) {
            arr = new String[enqSourceCategoryDB.getSubLeadSources().size()];
            for (int i = 0; i < enqSourceCategoryDB.getSubLeadSources().size(); i++) {
                arr[i] = enqSourceCategoryDB.getSubLeadSources().get(i).getName();
            }
        }
        return arr;

    }

    private String[] getEnquirySource() {
        String[] arr = new String[realm.where(EnqSourceDB.class).findAll().size()];
        for (int i = 0; i < realm.where(EnqSourceDB.class).findAll().size(); i++) {
            arr[i] = realm.where(EnqSourceDB.class).findAll().get(i).getName();
        }
        return arr;
    }

    public void setSpinnerAdapter(Spinner spinner, String data[]) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, data);
        spinner.setAdapter(adapter);
    }

    private String[] getEnquirySubSource() {
        String[] arr = new String[realm.where(EnqSourceCategoryDB.class).findAll().size()];
        for (int i = 0; i < realm.where(EnqSourceCategoryDB.class).findAll().size(); i++) {
            arr[i] = realm.where(EnqSourceCategoryDB.class).findAll().get(i).getName();
        }
        return arr;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_lead_date:
            case R.id.tv_add_lead_date_val:
                showDatePicker(this, getActivity(), R.id.tv_add_lead_date_val, Calendar.getInstance(), null);
                break;
            case R.id.tv_lead_exp_pur_date_header:
            case R.id.tv_lead_exp_pur_date_val:
                showDatePicker(this, getActivity(), R.id.tv_lead_exp_pur_date_val, Calendar.getInstance(), null);
                break;

        }

    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        switch (Integer.parseInt(view.getTag())) {
            case R.id.tv_lead_date:
            case R.id.tv_add_lead_date_val:
                tvLeadSelectDateVal.setText(String.format(Locale.getDefault(), "%d %s %d", dayOfMonth , calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), year));
                tvLeadSelectDateVal.setTag(R.id.autoninja_date, calendar);
                break;
            case R.id.tv_lead_exp_pur_date_val:
                tvLeadExpPurDateVal.setText(String.format(Locale.getDefault(), "%d %s %d", dayOfMonth , calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), year));
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                tvLeadExpPurDateVal.setTag(R.id.autoninja_date, calendar);
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        try {
            addLeadCommunication = (AddLeadCommunication) context;
        } catch (Exception e) {
            System.out.println("ClassCastException");
        }
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);

    }

    @Subscribe
    public void onViewPagerMoved(ViewPagerSwiped viewPagerSwiped) {
        if (viewPagerSwiped.getOldPosition() == 1 && viewPagerSwiped.getNewPosition() == 2) {
            System.out.println(TAG + "ViewPagerTryToMoving");

            if(getEnquirySourceId() == -1){
                tvSpinnerSrcHeader.setEnabled(false);
                showAlert(Color.RED);
            }else if(getEnquirySourceCategoryId() == -1 ){
                showAlert(Color.RED);
            }else if(vinNoEdit.getVisibility()==View.VISIBLE && vinNoEdit.getText().toString().isEmpty()){
                showAlertForNoVIN(Color.RED);
            }else if(vinNoEdit.getVisibility()==View.VISIBLE && vinNoEdit.getText().toString().length()<6){
                showAlertForNoVINSize(Color.RED);
            }else{
                AddLeadActivity.leadInputData = getLeadInputData();
                addLeadCommunication.onLeadDetailsEdited(getLeadInputData(), viewPagerSwiped.getOldPosition());
            }




          /*  if (getEnquirySourceId() != -1) {
                AddLeadActivity.leadInputData = getLeadInputData();
                addLeadCommunication.onLeadDetailsEdited(getLeadInputData(), viewPagerSwiped.getOldPosition());
            } else {
                tvSpinnerSrcHeader.setEnabled(false);
                showAlert(Color.RED);
            }
*/


        }
    }

    private void showAlert(int color) {
        if(!tvSpinnerSrcHeader.isEnabled()){
            showAlert("Enquiry source should't be empty!");
        }
        tvSpinnerSrcHeader.setTextColor(color);
    }
    private void showAlertForNoVIN(int color) {
        if(vinNoEdit.isEnabled()){
            showAlert("Please enter VIN/REG No.");
        }
    }
    private void showAlertForNoVINSize(int color) {
        if(vinNoEdit.isEnabled()){
            showAlert("Please enter valid VIN/REG No.");
        }
        vinNoEdit.setTextColor(color);
    }

    private AddLeadLeadInputData getLeadInputData() {

        String leadDate = "";
        String purchaseDate = "";
        if (tvLeadSelectDateVal.getTag(R.id.autoninja_date) != null) {
            leadDate = Util.getSQLDateTime(((Calendar)tvLeadSelectDateVal.getTag(R.id.autoninja_date)).getTime());
            System.out.println("leadDate:"+leadDate);

        }
        if (tvLeadExpPurDateVal.getTag(R.id.autoninja_date) != null) {
            purchaseDate = Util.getSQLDateTime(((Calendar)tvLeadExpPurDateVal.getTag(R.id.autoninja_date)).getTime());
        }
        return new AddLeadLeadInputData(String.valueOf(getEnquirySourceId()),
                leadDate, purchaseDate,
                (spinnerModPay.getSelectedItemPosition()==0?"":spinnerModPay.getSelectedItem().toString()),
                getEnquirySourceCategoryIdString(),vinNoEdit.getText().toString());
    }

    private int getEnquirySourceId() {
        if(isShowLeadSourceCategory()){
            if (realm.where(EnqSubInnerSourceDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, (String) spinnerEnqSrc.getText().toString()).findAll().size() > 0) {
                return realm.where(EnqSubInnerSourceDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, spinnerEnqSrc.getText().toString()).findFirst().getId();
            } else {
                return -1;
            }
        }
        else {
            if (realm.where(EnqSourceDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, (String) spinnerEnqSrc.getText().toString()).findAll().size() > 0) {
                return realm.where(EnqSourceDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, spinnerEnqSrc.getText().toString()).findFirst().getId();
            } else {
                return -1;
            }
        }

    }

    private int getEnquirySourceCategoryId() {
        if (realm.where(EnqSourceCategoryDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, (String) autoTvSourceCategory.getText().toString()).findAll().size() > 0) {
            return realm.where(EnqSourceCategoryDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, autoTvSourceCategory.getText().toString()).findFirst().getId();
        } else {
            return -1;
        }

    }
    private String getEnquirySourceCategoryIdString() {
        if(isShowLeadSourceCategory()) {
            if (realm.where(EnqSourceCategoryDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, (String) autoTvSourceCategory.getText().toString()).findAll().size() > 0) {
                return realm.where(EnqSourceCategoryDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, autoTvSourceCategory.getText().toString()).findFirst().getId() + "";
            } else {
                return null;
            }
        }
        else {
            return null;
        }

    }

    public void showAlert(String alert) {
        Snackbar snackbar = Snackbar.make(view, alert, Snackbar.LENGTH_LONG)
                .setAction("Close", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }
    private boolean isShowLeadSourceCategory(){
        AppConfigDB appConfigDB =realm.where(AppConfigDB.class).equalTo("key",WSConstants.AppConfig.SHOW_LEAD_SOURCE_CATEGORIES).findFirst();
        if(appConfigDB!=null){
            if(appConfigDB.getValue().equalsIgnoreCase("0")){
                return false;
            }
            else if(appConfigDB.getValue().equalsIgnoreCase("1")){
                return true;
            }
        }
        return false;
    }
}
