package com.salescrm.telephony.model;

public class ExchangeCarCancelModel {
    private String lead_id;
    private String remarks;

    public ExchangeCarCancelModel(String lead_id, String remarks) {
        this.lead_id = lead_id;
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }
}
