package com.salescrm.telephony.model.booking;

import android.util.SparseArray;

/**
 * Created by bharath on 23/2/18.
 */

public class BookingSectionModel {
    private SparseArray<BookingSectionInnerModel> bookingSectionInnerSparseArray;

    public BookingSectionModel() {
        this.bookingSectionInnerSparseArray = new SparseArray<>();
    }

    public void putBookingSectionValue(int key, BookingSectionInnerModel bookingSectionInnerModel) {
        bookingSectionInnerSparseArray.put(key, bookingSectionInnerModel);
    }
    public BookingSectionInnerModel getBookingSectionInnerValue(int key) {
        BookingSectionInnerModel bookingSectionInnerModel =
                bookingSectionInnerSparseArray.get(key, null);
        if(bookingSectionInnerModel ==null) {
            bookingSectionInnerModel = new BookingSectionInnerModel();
        }
        return bookingSectionInnerModel;
    }

    public SparseArray<BookingSectionInnerModel> getBookingSectionInnerSparseArray() {
        return bookingSectionInnerSparseArray;
    }

    public void setBookingSectionInnerSparseArray(SparseArray<BookingSectionInnerModel> bookingSectionInnerSparseArray) {
        this.bookingSectionInnerSparseArray = bookingSectionInnerSparseArray;
    }


    public class BookingSectionInnerModel {
        private int sectionId;
        private int sectionImageId;
        private boolean visibility;

        public int getSectionId() {
            return sectionId;
        }

        public void setSectionId(int sectionId) {
            this.sectionId = sectionId;
        }

        public boolean isVisibility() {
            return visibility;
        }

        public void setVisibility(boolean visibility) {
            this.visibility = visibility;
        }

        public int getSectionImageId() {
            return sectionImageId;
        }

        public void setSectionImageId(int sectionImageId) {
            this.sectionImageId = sectionImageId;
        }
    }
}
