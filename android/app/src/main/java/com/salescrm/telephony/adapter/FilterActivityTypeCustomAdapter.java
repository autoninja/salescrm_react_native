package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.dataitem.FilterActivityTypeModel;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ravindra P on 02-06-2016.
 */
public class FilterActivityTypeCustomAdapter extends ArrayAdapter<FilterActivityTypeModel> {

    Context context;
    int colorActive;
    int colorInactive;
    int colorHot, colorWarm, colorCold, colorOverdue;
    private List<FilterActivityTypeModel> allModelList = null;
    private List<FilterActivityTypeModel> modelList = null;

    public FilterActivityTypeCustomAdapter(Context context, List<FilterActivityTypeModel> resource) {
        super(context, R.layout.filter_activity_type_fragment_listview_row, resource);
        this.context = context;
        this.modelList = resource;
        this.allModelList = new ArrayList<>();
        this.allModelList.addAll(modelList);
        this.colorActive = ContextCompat.getColor(context, R.color.blue);
        this.colorInactive = ContextCompat.getColor(context, R.color.textColorCardDarker);
        this.colorHot = ContextCompat.getColor(context, R.color.hot);
        this.colorWarm = ContextCompat.getColor(context, R.color.warm);
        this.colorCold = ContextCompat.getColor(context, R.color.cold);
        this.colorOverdue = ContextCompat.getColor(context, R.color.overdue);
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        modelList.clear();
        if (charText.length() == 0) {
            modelList.addAll(allModelList);
        } else {
            for (FilterActivityTypeModel model : allModelList) {
                if (model.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    modelList.add(model);
                }
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            convertView = ((Activity) context).getLayoutInflater().inflate(R.layout.filter_activity_type_fragment_listview_row, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.textView1);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textView.setText(modelList.get(position).getName());
        if (modelList.get(position).getValue() == 1) {
            holder.checkBox.setChecked(true);

           /* if (!holder.checkBox.isChecked()) {


                holder.checkBox.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        holder.checkBox.setChecked(true);

                    }
                }, 10);
            }*/
            holder.textView.setTextColor(colorActive);
        } else {
            holder.checkBox.setChecked(false);
            holder.textView.setTextColor(colorInactive);
        }
        if (modelList.get(position).getName().trim().equalsIgnoreCase(WSConstants.LEAD_TAG_HOT)) {
            holder.textView.setTextColor(colorHot);
        } else if (modelList.get(position).getName().trim().equalsIgnoreCase(WSConstants.LEAD_TAG_WARM)) {
            holder.textView.setTextColor(colorWarm);
        } else if (modelList.get(position).getName().trim().equalsIgnoreCase(WSConstants.LEAD_TAG_COLD)) {
            holder.textView.setTextColor(colorCold);
        } else if (modelList.get(position).getName().trim().equalsIgnoreCase(WSConstants.PENDING)) {
            holder.textView.setTextColor(colorInactive);
        }
        return convertView;
    }

    static class ViewHolder {
        TextView textView;
        CheckBox checkBox;

    }

}