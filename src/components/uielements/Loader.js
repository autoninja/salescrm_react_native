import React, { Component } from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";
export default class Loader extends Component {
  render() {
    return (
      <View style={[styles.loaderMain,{backgroundColor:this.props.lightMode?"rgba(255, 255, 255, 0.6)":"rgba(0, 0, 0, 0.6)"}]}>
        <View style={styles.loaderInner}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  loaderMain: {
    backgroundColor: "rgba(0, 0, 0, 0.6)",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    alignSelf: "center",
    justifyContent: "center"
  },
  loaderInner: {
    height: 100,
    width: 100,
    opacity: 1,
    backgroundColor: "#007fff",
    borderRadius: 10,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  indicator: {}
});
