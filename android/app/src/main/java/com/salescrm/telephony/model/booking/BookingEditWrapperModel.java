package com.salescrm.telephony.model.booking;

public class BookingEditWrapperModel {
    private boolean isCarDetailsEditable;
    private boolean isBookingAmountEditable;
    private boolean isDeliveryDetailsEditable;
    private boolean isCustomerDetailsEditable;
    private boolean isPriceBreakupEditable;
    private boolean isDiscountEditable;
    private boolean isSubmitButtonVisible;
    private boolean isInvoiceEditable;
    private boolean isExchFinEditable;

    public BookingEditWrapperModel(boolean isCarDetailsEditable,
                                   boolean isBookingAmountEditable,
                                   boolean isInvoiceEditable,
                                   boolean isDeliveryDetailsEditable,
                                   boolean isCustomerDetailsEditable,
                                   boolean isPriceBreakupEditable,
                                   boolean isDiscountEditable,
                                   boolean isExchFinEditable,
                                   boolean isSubmitButtonVisible) {
        this.isCarDetailsEditable = isCarDetailsEditable;
        this.isBookingAmountEditable = isBookingAmountEditable;
        this.isInvoiceEditable = isInvoiceEditable;
        this.isDeliveryDetailsEditable = isDeliveryDetailsEditable;
        this.isCustomerDetailsEditable = isCustomerDetailsEditable;
        this.isPriceBreakupEditable = isPriceBreakupEditable;
        this.isDiscountEditable = isDiscountEditable;
        this.isExchFinEditable = isExchFinEditable;

        this.isSubmitButtonVisible = isSubmitButtonVisible;
    }

    public BookingEditWrapperModel() {

    }

    public boolean isCarDetailsEditable() {
        return isCarDetailsEditable;
    }

    public void setCarDetailsEditable(boolean carDetailsEditable) {
        isCarDetailsEditable = carDetailsEditable;
    }

    public boolean isDeliveryDetailsEditable() {
        return isDeliveryDetailsEditable;
    }

    public void setDeliveryDetailsEditable(boolean deliveryDetailsEditable) {
        isDeliveryDetailsEditable = deliveryDetailsEditable;
    }

    public boolean isCustomerDetailsEditable() {
        return isCustomerDetailsEditable;
    }

    public void setCustomerDetailsEditable(boolean customerDetailsEditable) {
        isCustomerDetailsEditable = customerDetailsEditable;
    }

    public boolean isPriceBreakupEditable() {
        return isPriceBreakupEditable;
    }

    public void setPriceBreakupEditable(boolean priceBreakupEditable) {
        isPriceBreakupEditable = priceBreakupEditable;
    }

    public boolean isBookingAmountEditable() {
        return isBookingAmountEditable;
    }

    public void setBookingAmountEditable(boolean bookingAmountEditable) {
        isBookingAmountEditable = bookingAmountEditable;
    }

    public boolean isDiscountEditable() {
        return isDiscountEditable;
    }

    public void setDiscountEditable(boolean discountEditable) {
        isDiscountEditable = discountEditable;
    }

    public boolean isExchFinEditable() {
        return isExchFinEditable;
    }

    public void setExchFinEditable(boolean exchFinEditable) {
        isExchFinEditable = exchFinEditable;
    }

    public boolean isSubmitButtonVisible() {
        return isSubmitButtonVisible;
    }

    public boolean isInvoiceEditable() {
        return isInvoiceEditable;
    }

    public void setInvoiceEditable(boolean invoiceEditable) {
        isInvoiceEditable = invoiceEditable;
    }

    public void setSubmitButtonVisible(boolean submitButtonVisible) {
        isSubmitButtonVisible = submitButtonVisible;
    }
}
