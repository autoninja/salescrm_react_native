import React, { Component } from "react";
import { View, TextInput, Image, TouchableOpacity, Text } from "react-native";

export default class CustomTextInput extends Component {
  constructor(props) {
    super(props);
    this.state = { isFocused: false };
  }
  render() {
    let from = this.props.from;
    let text = this.props.text;
    let placeholder = this.props.hint;
    let header = this.props.header;
    let showRemove = this.props.showRemove;
    let showError = this.props.showError;
    let payload = this.props.payload;
    let lightmode = this.props.lightmode;
    let textColor;
    if (this.props.editable === false) {
      if (lightmode) {
        textColor = "#cacccf"
      }
      else {
        textColor = "#808080"
      }
    }
    else {
      if (lightmode) {
        textColor = "#EEEEEE";
      }
      else {
        textColor = "#303030";
      }
    }
    return (
      <View style={[{ marginTop: 12 }]}>
        {this.props.showPlaceHolder && (
          <Text
            style={{
              color: lightmode ? '#fff' : "#303030",
              fontSize: 16,
              paddingLeft: 10,
              paddingRight: 10,
              fontWeight: "bold"
            }}
          >
            {header}
          </Text>
        )}
        <View
          style={{
            flexDirection: "row",
            alignItems: "center"
          }}
        >
          <TextInput
            style={{
              paddingBottom: 4,
              paddingTop: 4,
              marginLeft: 8,
              paddingRight: showRemove || showError ? 35 : 0,
              marginEnd: 8,
              flex: 1,
              borderColor: this.state.isFocused ? "#007FFF" : "#CFCFCF",
              fontSize: 16,
              borderBottomWidth: this.state.isFocused ? 2 : 1,
              color: textColor
            }}
            onChangeText={text =>
              this.props.onInputTextChanged(from, text, payload)
            }
            onFocus={() => {
              this.setState({ isFocused: true });
            }}
            onBlur={() => {
              this.setState({ isFocused: false });
            }}
            value={text}
            placeholder={placeholder}
            placeholderTextColor="#CFCFCF"
            {...this.props}
          />
          {showRemove && (
            <TouchableOpacity
              style={{ position: "absolute", right: 0 }}
              onPress={() => {
                this.props.onRemoveEditText(from, payload);
              }}
            >
              <Image
                source={require("../../images/ic_delete_black.png")}
                style={{
                  width: 20,
                  height: 20,
                  alignSelf: "flex-end",
                  marginRight: 10
                }}
              />
            </TouchableOpacity>
          )}
          {showError && (
            <TouchableOpacity
              style={{ position: "absolute", right: 0 }}
              onPress={() => {
                this.props.onClickErrorButton(from, payload);
              }}
            >
              <Image
                source={require("../../images/ic_error_mark.png")}
                style={{
                  width: 20,
                  height: 20,
                  alignSelf: "flex-end",
                  marginRight: 10
                }}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}
