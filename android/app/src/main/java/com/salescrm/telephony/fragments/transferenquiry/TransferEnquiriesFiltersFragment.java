package com.salescrm.telephony.fragments.transferenquiry;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexboxLayout;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.TransferEnquiriesRecyclerAdapter;
import com.salescrm.telephony.interfaces.InsertLeadSearchSuggestionListener;
import com.salescrm.telephony.model.FilteredLeadIdsModel;
import com.salescrm.telephony.model.LeadSearchSuggestionModel;
import com.salescrm.telephony.model.TransferEnquiriesModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Stack;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 19/8/17.
 */

public class TransferEnquiriesFiltersFragment extends Fragment implements View.OnClickListener, InsertLeadSearchSuggestionListener {
    private final int fromCarModel = 0;
    private final int fromEnquirySource = 1;
    private final int fromCustomerType = 2;
    private final int fromStage = 3;
    private View inflatedView;
    private LinearLayout llRadioFilters;
    private LinearLayout llRadioLeads;
    private int leadTransferType = 0;
    private Realm realm;
    private TransferEnquiriesModel transferEnquiriesModel;
    private TransferEnquiriesRecyclerAdapter adapter = null;
    private ProgressDialog progressDialog;
    private TextView tvModelCount, tvEnqSourceCount, tvCustomerTypeCount, tvEnquiryStageCount;
    private Preferences pref;
    private ApplyFilterListener listener;

    private EditText etSearchLead;
    private RecyclerView recyclerViewSearchLead;
    private LeadSearchResultAdapter leadSearchResultAdapter;
    private List<LeadSearchSuggestionModel.Result> leadSearchSuggestionModels;
    private FlexboxLayout flexboxLayout;
    private ProgressBar progressSearchLead;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        inflatedView = inflater.inflate(R.layout.fragment_transfer_enquiries_filters, container, false);
        llRadioFilters = (LinearLayout) inflatedView.findViewById(R.id.ll_transfer_enq_filters);
        llRadioLeads = (LinearLayout) inflatedView.findViewById(R.id.ll_transfer_enq_filters_leads);
        leadTransferType = getActivity().getIntent().getExtras() != null ? getActivity().getIntent().getExtras().getInt("lead_transfer_type", 0) : 0;
        pref = Preferences.getInstance();
        pref.load(getContext());
        leadSearchSuggestionModels = new ArrayList<>();
        leadSearchResultAdapter = new LeadSearchResultAdapter(leadSearchSuggestionModels, this);
        tvModelCount = (TextView) inflatedView.findViewById(R.id.tv_trans_enq_filter_model_sub);
        tvEnqSourceCount = (TextView) inflatedView.findViewById(R.id.tv_trans_enq_filter_source_sub);
        tvCustomerTypeCount = (TextView) inflatedView.findViewById(R.id.tv_trans_enq_filter_type_sub);
        tvEnquiryStageCount = (TextView) inflatedView.findViewById(R.id.tv_trans_enq_filter_stage_sub);


        etSearchLead = (EditText) inflatedView.findViewById(R.id.et_trans_enq_search_lead);
        progressSearchLead = (ProgressBar) inflatedView.findViewById(R.id.loader_trans_enq_search_lead);
        recyclerViewSearchLead = (RecyclerView) inflatedView.findViewById(R.id.recycler_view_trans_enq_search_lead);
        flexboxLayout = (FlexboxLayout) inflatedView.findViewById(R.id.flex_trans_enq_filter_leads);
        flexboxLayout.setFlexWrap(FlexboxLayout.FLEX_WRAP_WRAP);
        recyclerViewSearchLead.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewSearchLead.setHasFixedSize(true);
        recyclerViewSearchLead.setItemAnimator(new DefaultItemAnimator());
        recyclerViewSearchLead.setAdapter(leadSearchResultAdapter);

        inflatedView.findViewById(R.id.frame_trans_enq_filter_model).setOnClickListener(this);
        inflatedView.findViewById(R.id.frame_trans_enq_filter_source).setOnClickListener(this);
        inflatedView.findViewById(R.id.frame_trans_enq_filter_type).setOnClickListener(this);
        inflatedView.findViewById(R.id.frame_trans_enq_filter_stage).setOnClickListener(this);

        realm = Realm.getDefaultInstance();
        transferEnquiriesModel = TransferEnquiriesModel.getInstance();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);


        etSearchLead.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                leadSearchSuggestionModels.clear();
                leadSearchResultAdapter.notifyDataSetChanged();
                if (Util.isNotNull(s.toString())) {
                    getSuggestion(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return inflatedView;
    }

    private void getSuggestion(final String s) {
        progressSearchLead.setVisibility(View.VISIBLE);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getLeadSearchSuggestion(s, transferEnquiriesModel.getLocationId(), new Callback<LeadSearchSuggestionModel>() {
            @Override
            public void success(LeadSearchSuggestionModel leadSearchSuggestionModel, Response response) {
                progressSearchLead.setVisibility(View.GONE);
                Util.updateHeaders(response.getHeaders());

                if(leadSearchSuggestionModel.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }

                if (leadSearchSuggestionModel.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)
                        && leadSearchSuggestionModel.getResult() != null) {
                    if(s.equalsIgnoreCase(etSearchLead.getText().toString())) {
                        showDataOnRecyclerView(leadSearchSuggestionModel.getResult());
                    }

                }else{
                    showDataOnRecyclerView(null);

                }
            }

            @Override
            public void failure(RetrofitError error) {
                progressSearchLead.setVisibility(View.GONE);
                if(s.equalsIgnoreCase(etSearchLead.getText().toString())) {
                    showDataOnRecyclerView(null);
                }

            }
        });
    }

    private void showDataOnRecyclerView(List<LeadSearchSuggestionModel.Result> result) {
        leadSearchSuggestionModels.clear();
        if (null != result && !etSearchLead.getText().toString().equalsIgnoreCase("")) {
            leadSearchSuggestionModels.addAll(result);
        }
        leadSearchResultAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        checkFilterType();
        showSelectedText();
        showFilterLeadSelected();
    }

    private void showFilterLeadSelected() {
        if (leadTransferType == 1 && transferEnquiriesModel.getSelectedLeadList() != null && transferEnquiriesModel.getOrderedSelectedLeadList().size() > 0) {
            flexboxLayout.removeAllViews();
            for (String s : transferEnquiriesModel.getOrderedSelectedLeadList()) {
                addSelectedLead(s, false);
            }
        }
    }

    private void checkFilterType() {
        if (leadTransferType == 0) {
            llRadioFilters.setVisibility(View.VISIBLE);
            llRadioLeads.setVisibility(View.GONE);
        } else {
            llRadioFilters.setVisibility(View.GONE);
            llRadioLeads.setVisibility(View.VISIBLE);
        }
    }

    public void showAlertFilters(int from, final List<TransferEnquiriesModel.Item> items) {

        if (items == null) {
            return;
        }

        //Previous data for close filter
       /* final TransferEnquiriesModel transferEnquiriesModelPrevious = new TransferEnquiriesModel();
        transferEnquiriesModelPrevious.setUserList(items);*/
        final List<TransferEnquiriesModel.Item> dataPrevious = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            TransferEnquiriesModel.Item item = transferEnquiriesModel.new Item();
            item.setChecked(items.get(i).isChecked());
            item.setTitle(items.get(i).getTitle());
            item.setId(items.get(i).getId());
            item.setImgUrl(items.get(i).getImgUrl());
            item.setHeader(items.get(i).isHeader());
            dataPrevious.add(item);
        }


        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialog.setContentView(R.layout.layout_transfer_enq_filter_dialog);
        final TextView tvClear = (TextView) dialog.findViewById(R.id.bt_filter_clear);
        TextView tvApply = (TextView) dialog.findViewById(R.id.bt_filter_apply);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view_trans_enq);
        adapter = new TransferEnquiriesRecyclerAdapter(new TransferEnquiriesRecyclerAdapter.TransferEnqClickListener() {
            @Override
            public void onClick(int position, TransferEnquiriesModel.Item item) {

                if (items.get(position).isChecked()) {
                    item.setChecked(false);
                } else {
                    item.setChecked(true);
                }
                items.set(position, item);
                //transferEnquiriesModel.setUserListFrom(position,item);
                if (adapter != null) {
                    adapter.notifyItemChanged(position);
                }
            }
        }, items, getContext(), false, true);

        tvApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyFilter("Applying filter..");
                dialog.dismiss();
            }
        });

        tvClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearFilter(items);
                applyFilter("Clearing filter..");
                dialog.dismiss();
            }

            private void clearFilter(List<TransferEnquiriesModel.Item> items) {
                for (int i = 0; i < items.size(); i++) {
                    items.get(i).setChecked(false);
                }
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                System.out.println("dataPrevious size::" + transferEnquiriesModel.getCheckedCount(dataPrevious));
                System.out.println("items size::" + transferEnquiriesModel.getCheckedCount(items));
                items.clear();
                items.addAll(dataPrevious);
            }
        });


        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }


    private void applyFilter(String filter) {
        showSelectedText();
        progressDialog.setMessage(filter);
        progressDialog.show();
        ArrayList<String> userData = new ArrayList<>();
        for (int i = 0; i < transferEnquiriesModel.getUserList().size(); i++) {
            if (transferEnquiriesModel.getUserList().get(i).isChecked()) {
                userData.add(transferEnquiriesModel.getUserList().get(i).getId());
            }
        }

        ArrayList<String> models = new ArrayList<>();
        for (int i = 0; i < transferEnquiriesModel.getCarModelList().size(); i++) {
            if (transferEnquiriesModel.getCarModelList().get(i).isChecked()) {
                models.add(transferEnquiriesModel.getCarModelList().get(i).getId());
            }
        }

        ArrayList<String> enqSources = new ArrayList<>();
        for (int i = 0; i < transferEnquiriesModel.getEnqSourceList().size(); i++) {
            if (transferEnquiriesModel.getEnqSourceList().get(i).isChecked()) {
                enqSources.add(transferEnquiriesModel.getEnqSourceList().get(i).getId());
            }
        }

        //Selected customer type of filter
        ArrayList<String> customerTypes = new ArrayList<>();
        for (int i = 0; i < transferEnquiriesModel.getCustomerTypeList().size(); i++) {
            if (transferEnquiriesModel.getCustomerTypeList().get(i).isChecked()) {
                customerTypes.add(transferEnquiriesModel.getCustomerTypeList().get(i).getId());
            }
        }

        //Selected stage ids' of filter
        ArrayList<String> stageIds = new ArrayList<>();
        for (int i = 0; i < transferEnquiriesModel.getEnquiryStageList().size(); i++) {
            if (transferEnquiriesModel.getEnquiryStageList().get(i).isChecked()) {
                stageIds.add(transferEnquiriesModel.getEnquiryStageList().get(i).getId());
            }
        }


        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getFilteredLeadIds(userData,
                models, enqSources, customerTypes, stageIds, new Callback<FilteredLeadIdsModel>() {
                    @Override
                    public void success(FilteredLeadIdsModel filteredLeadIdsModel, Response response) {
                        progressDialog.dismiss();
                        Util.updateHeaders(response.getHeaders());

                        if(filteredLeadIdsModel.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                            ApiUtil.InvalidUserLogout(getActivity(),0);
                        }

                        if (filteredLeadIdsModel.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)
                                && filteredLeadIdsModel.getResult() != null) {

                            updateLeadSelected(filteredLeadIdsModel.getResult());
                        }else{
                            dismissProgressDialog();
                            //showErrorDialog("Error while fetching leads",1);
                            Util.showToast(getContext(), "Error while fetching lead list", Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        dismissProgressDialog();
                        Util.showToast(getContext(), "Error while fetching lead list", Toast.LENGTH_SHORT);

                    }
                });

    }

    private void updateLeadSelected(FilteredLeadIdsModel.Result[] result) {
        dismissProgressDialog();
        if (listener != null) {
            listener.onApplyFilter(result);
            transferEnquiriesModel.enquiriesChangedByFilters();
        }
    }

    private void showSelectedText() {
        if (transferEnquiriesModel.getCheckedCount(transferEnquiriesModel.getCarModelList()) == 0) {
            tvModelCount.setText("");
        } else {
            tvModelCount.setText(String.format(Locale.getDefault(), "(%d Selected)",
                    transferEnquiriesModel.getCheckedCount(transferEnquiriesModel.getCarModelList())));
        }
        if (transferEnquiriesModel.getCheckedCount(transferEnquiriesModel.getEnqSourceList()) == 0) {
            tvEnqSourceCount.setText("");
        } else {
            tvEnqSourceCount.setText(String.format(Locale.getDefault(), "(%d Selected)",
                    transferEnquiriesModel.getCheckedCount(transferEnquiriesModel.getEnqSourceList())));
        }
        if (transferEnquiriesModel.getCheckedCount(transferEnquiriesModel.getCustomerTypeList()) == 0) {
            tvCustomerTypeCount.setText("");
        } else {
            tvCustomerTypeCount.setText(String.format(Locale.getDefault(), "%d Selected",
                    transferEnquiriesModel.getCheckedCount(transferEnquiriesModel.getCustomerTypeList())));
        }

        if (transferEnquiriesModel.getCheckedCount(transferEnquiriesModel.getEnquiryStageList()) == 0) {
            tvEnquiryStageCount.setText("");
        } else {
            tvEnquiryStageCount.setText(String.format(Locale.getDefault(), "(%d Selected)",
                    transferEnquiriesModel.getCheckedCount(transferEnquiriesModel.getEnquiryStageList())));
        }

    }

    private void dismissProgressDialog() {
        if (getActivity() != null && !getActivity().isFinishing() && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.frame_trans_enq_filter_model:
                showAlertFilters(fromCarModel, transferEnquiriesModel.getCarModelList());
                break;
            case R.id.frame_trans_enq_filter_source:
                showAlertFilters(fromEnquirySource, transferEnquiriesModel.getEnqSourceList());
                break;
            case R.id.frame_trans_enq_filter_type:
                showAlertFilters(fromCustomerType, transferEnquiriesModel.getCustomerTypeList());
                break;
            case R.id.frame_trans_enq_filter_stage:
                showAlertFilters(fromStage, transferEnquiriesModel.getEnquiryStageList());
                break;
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (ApplyFilterListener) context;

        } catch (ClassCastException e) {

        }
    }

    @Override
    public void onInsertLeadSuggestion(String lead, int position) {
        System.out.println("Lead Inserted");

        FilteredLeadIdsModel.Result[] result = new FilteredLeadIdsModel.Result[1];
        if (transferEnquiriesModel.getSelectedLeadList() != null) {
            result = new FilteredLeadIdsModel.Result[transferEnquiriesModel.getSelectedLeadList().size() + 1];
            int i = 0;
            for (String s : transferEnquiriesModel.getSelectedLeadList()) {
                FilteredLeadIdsModel.Result resultItem = new FilteredLeadIdsModel().new Result();
                resultItem.setLead_id(s);
                result[i] = resultItem;
                i++;
            }
        }
        FilteredLeadIdsModel.Result resultItem = new FilteredLeadIdsModel().new Result();
        resultItem.setLead_id(lead);
        result[result.length - 1] = resultItem;

        if (listener != null) {
            listener.onApplyFilter(result);
        }
        transferEnquiriesModel.enquiriesChangedByFilters();

        if(position<leadSearchSuggestionModels.size()) {
            leadSearchSuggestionModels.remove(position);
            leadSearchResultAdapter.notifyItemRemoved(position);
            leadSearchResultAdapter.notifyItemRangeChanged(position, leadSearchSuggestionModels.size());
        }

        for (int i = 0; i < flexboxLayout.getChildCount(); i++) {
            if (flexboxLayout.getChildAt(i).getTag().toString().equalsIgnoreCase(lead)) {
                Util.showToast(getContext(), "Lead already added", Toast.LENGTH_SHORT);
                return;
            }
        }

        addSelectedLead(lead, true);

    }

    private void addSelectedLead(String lead, boolean insertToLocal) {
        if (lead == null) {
            Util.showToast(getContext(), "Invalid lead", Toast.LENGTH_SHORT);
            return;
        }
        LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = vi.inflate(R.layout.layout_bubble_text_view, null);
        TextView textView = (TextView) view.findViewById(R.id.tv_bubble_title);
        LinearLayout btBubble = (LinearLayout) view.findViewById(R.id.bt_bubble_remove);
        btBubble.setTag(lead);
        view.setTag(lead);
        btBubble.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flexboxLayout.removeView(flexboxLayout.findViewWithTag(v.getTag().toString()));
                transferEnquiriesModel.removeSelectedLead(v.getTag().toString());
                transferEnquiriesModel.getOrderedSelectedLeadList().remove(v.getTag().toString());
                if (listener != null) {
                    listener.onApplyFilter(null);
                    transferEnquiriesModel.enquiriesChangedByFilters();
                }
            }
        });
        textView.setText(lead);
        if (insertToLocal) {
            transferEnquiriesModel.getOrderedSelectedLeadList().add(lead);
        }
        flexboxLayout.addView(view);

    }

    public interface ApplyFilterListener {
        void onApplyFilter(FilteredLeadIdsModel.Result[] result);
    }

    class LeadSearchResultAdapter extends RecyclerView.Adapter<LeadSearchResultAdapter.LeadSearchViewHolder> {
        private List<LeadSearchSuggestionModel.Result> leadSearchSuggestionModels;
        private InsertLeadSearchSuggestionListener listener;

        LeadSearchResultAdapter(List<LeadSearchSuggestionModel.Result> leadSearchSuggestionModels,
                                InsertLeadSearchSuggestionListener listener) {
            this.leadSearchSuggestionModels = leadSearchSuggestionModels;
            this.listener = listener;
        }

        @Override
        public LeadSearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new LeadSearchViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_filter_lead_search_result, parent, false));
        }

        @Override
        public void onBindViewHolder(LeadSearchViewHolder holder, int pos) {
            final int position = holder.getAdapterPosition();
            final LeadSearchSuggestionModel.Result leadData = leadSearchSuggestionModels.get(position);
            if (leadData != null) {

                holder.tvLeadId.setText(leadData.getId());
                SpannableString spannableString=  new SpannableString(leadData.getFirst_name()+leadData.getLast_name() +" (" + leadData.getAssigned_dse() + ")");
                spannableString.setSpan(new RelativeSizeSpan(1.3f), 0,
                        (leadData.getFirst_name()+leadData.getLast_name()).length(), 0); // set size

                holder.tvLeadName.setText(spannableString);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onInsertLeadSuggestion(leadData.getId(), position);
                        }
                    }
                });
                if (position == leadSearchSuggestionModels.size() - 1) {
                    holder.hrView.setVisibility(View.GONE);
                } else {
                    holder.hrView.setVisibility(View.VISIBLE);

                }
            }
        }

        @Override
        public int getItemCount() {
            return leadSearchSuggestionModels == null ? 0 : leadSearchSuggestionModels.size();
        }

        class LeadSearchViewHolder extends RecyclerView.ViewHolder {
            private ImageButton btInsertLead;
            private TextView tvLeadId, tvLeadName, tvLeadHolderName;
            private View hrView;

            LeadSearchViewHolder(View itemView) {
                super(itemView);
                btInsertLead = (ImageButton) itemView.findViewById(R.id.bt_filter_lead_search_result_add);
                tvLeadId = (TextView) itemView.findViewById(R.id.tv_filter_lead_search_result_lead_id);
                tvLeadName = (TextView) itemView.findViewById(R.id.tv_filter_lead_search_result_lead_name);
                tvLeadHolderName = (TextView) itemView.findViewById(R.id.tv_filter_lead_search_result_lead_sub);
                hrView = itemView.findViewById(R.id.view_filter_lead_search_result);
            }
        }
    }
}
