package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by prateek on 7/3/17.
 */

public class LeadListStageProgress extends RealmObject {

    private int width;

    private String name;

    private String bar_class;

    private int stage_id;

    public int getWidth ()
    {
        return width;
    }

    public void setWidth (int width)
    {
        this.width = width;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getBar_class ()
    {
        return bar_class;
    }

    public void setBar_class (String bar_class)
    {
        this.bar_class = bar_class;
    }

    public int getStage_id ()
    {
        return stage_id;
    }

    public void setStage_id (int stage_id)
    {
        this.stage_id = stage_id;
    }
}
