package com.salescrm.telephony.fragments.gamification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.gamification.GamificatonSMDetailsAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.gamification.ManagerLeaderBoardResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.services.service_handlers.GamificationServiceHandler;
import com.salescrm.telephony.utils.Util;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by nndra on 04-Jan-18.
 */

public class LeaderBoardSMFragment extends Fragment {

    TextView section_zero;
    private GamificatonSMDetailsAdapter gamificatonSMDetailsAdapter;
    private RecyclerView recyclerTeamScore;
    private Preferences pref;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RelativeLayout relLoading, relGameJustStarted;
    private String monthYear;
    private TextView tvTargetSet, tvActivitiesSet, tvBoosterSet;

    @Override
    public void onStop() {
        super.onStop();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        SalesCRMApplication.getBus().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rView = inflater.inflate(R.layout.gamification_sm_score, container, false);
        recyclerTeamScore = (RecyclerView) rView.findViewById(R.id.recycler_team_score);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rView.findViewById(R.id.swipe_container);
        relLoading = (RelativeLayout) rView.findViewById(R.id.gamification_rel_loading_frame);
        section_zero = (TextView) rView.findViewById(R.id.section_zero);
        relGameJustStarted = (RelativeLayout) rView.findViewById(R.id.rel_game_not_started);
        tvTargetSet = (TextView) rView.findViewById(R.id.tv_game_not_started_target_set);
        tvActivitiesSet = (TextView) rView.findViewById(R.id.tv_game_not_started_activities_details);
        tvBoosterSet = (TextView) rView.findViewById(R.id.tv_game_not_started_booster_details);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        monthYear = Util.getMonthYear(pref.getSelectedGameDate());

        callApi();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                callApi();
            }
        });
        return rView;
    }
    private void callApi() {
        relGameJustStarted.setVisibility(View.GONE);
        if (new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
            relLoading.setVisibility(View.VISIBLE);
            System.out.println("Selected Date:" + monthYear);
            GamificationServiceHandler.getInstance(getActivity()).fetchManagerBoardData(pref.getAccessToken(), monthYear, new CallBack() {
                @Override
                public void onCallBack() {

                }

                @Override
                public void onCallBack(Object data) {
                    updateUiOnResponse((ManagerLeaderBoardResponse) data);
                }

                @Override
                public void onError(String error) {
                    relLoading.setVisibility(View.GONE);
                    section_zero.setVisibility(View.VISIBLE);
                    section_zero.setText(error);
                }
            });
        } else {
            section_zero.setVisibility(View.VISIBLE);
            section_zero.setText("Please Enable Internet Connection");
            Toast.makeText(getActivity(), "Please Enable Internet Connection", Toast.LENGTH_LONG).show();
        }
    }



    private void updateUiOnResponse(final ManagerLeaderBoardResponse managerLeaderBoardResponse) {
        recyclerTeamScore.setHasFixedSize(true);
        // finalTeamLeaderBoardResponse = null;
        relLoading.setVisibility(View.GONE);
        if (managerLeaderBoardResponse == null || managerLeaderBoardResponse.getResult() == null
                || managerLeaderBoardResponse.getResult().getManagers() == null || managerLeaderBoardResponse.getResult().getManagers().size() == 0) {
            section_zero.setVisibility(View.VISIBLE);
            section_zero.setText("No Data");
            if (isSelectedMonth()) {
                section_zero.setVisibility(View.GONE);
                relGameJustStarted.setVisibility(View.VISIBLE);
                String month = Util.getCalender(pref.getSystemDate()).getDisplayName(Calendar.MONTH,
                        Calendar.LONG, Locale.getDefault());
                tvTargetSet.setText("Game for "+month+" started !");
                tvActivitiesSet.setText("All activities marked from "+month+" 1st will be taken care when we will allot points for month");
                tvBoosterSet.setText("Booster period is ON till 10th "+month+" so all activities will give you double points!");


            }
        } else {
            section_zero.setVisibility(View.GONE);
            final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
            recyclerTeamScore.setLayoutManager(mLayoutManager);
            gamificatonSMDetailsAdapter = new GamificatonSMDetailsAdapter(getActivity(), managerLeaderBoardResponse);
            recyclerTeamScore.setAdapter(gamificatonSMDetailsAdapter);


            recyclerTeamScore.post(new Runnable() {
                public void run() {
                    int spotLight = getSpotLight(managerLeaderBoardResponse);
                    if (spotLight > mLayoutManager.findLastCompletelyVisibleItemPosition()) {
                        recyclerTeamScore.scrollToPosition(spotLight);
                    }
                }
            });


        }

    }

    private boolean isSelectedMonth() {
        Calendar selectedCalender = Util.getCalender(pref.getSelectedGameDate());
        Calendar systemCalender = Util.getCalender(pref.getSystemDate());
        return selectedCalender.get(Calendar.MONTH) == systemCalender.get(Calendar.MONTH) && selectedCalender.get(Calendar.YEAR) == selectedCalender.get(Calendar.YEAR);
    }

    private int getSpotLight(ManagerLeaderBoardResponse managerLeaderBoardResponse) {
        if (managerLeaderBoardResponse == null ||
                managerLeaderBoardResponse.getResult() == null ||
                managerLeaderBoardResponse.getResult().getManagers() == null) {
            return 0;
        }
        for (int i = 0; i < managerLeaderBoardResponse.getResult().getManagers().size(); i++) {
            if (managerLeaderBoardResponse.getResult().getManagers().get(i).getSpotlight().equalsIgnoreCase("1")) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

}
