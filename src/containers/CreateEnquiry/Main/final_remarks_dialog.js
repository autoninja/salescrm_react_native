import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback, KeyboardAvoidingView, ScrollView, TextInput
} from 'react-native'
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';
import styles from './CreateEnquiry.styles'
import UpdateExchangeCarModel from './UpdateExchangeCarModel'
import RestClient from '../../../network/rest_client'
import Toast, { DURATION } from 'react-native-easy-toast'
import LinearGradient from 'react-native-linear-gradient'
import NavigationServise from '../../../navigation/NavigationService';

const localStyles = StyleSheet.create({
    footerInvalid: {
        width: 200,
        backgroundColor: '#007fff',
        marginTop: 10,
        marginBottom: 8,
        marginLeft: 20,
        marginRight: 20,
        height: 30,
    },
    footerValid: {
        backgroundColor: '#007fff',
        width: 200,
        marginTop: 10,
        marginBottom: 8,
        marginLeft: 20,
        marginRight: 20,
        height: 30
    },
    validText: { alignSelf: 'center', color: '#fff', fontSize: 16, alignItems: 'center', marginTop: 2 },
    invalidText: { alignSelf: 'center', color: '#85929e', fontSize: 16, alignItems: 'center', marginTop: 2 }
});

export default class FinalRemarksDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            end_remarks: '',
            end_date: 'Select date*',
            show_date: 'Select date*',
            date_time_picker_visible: false,
            exchange_car_fields: this.getExchangeCarFields(),
            activity_id: this.getActivityId(),
            loading: false,
            leadId: this.getLeadId(),
        }
    }

    getExchangeCarFields() {
        return this.props.exchange_car_fields;
    }

    getActivityId() {
        //console.log("activity_id", this.props.activity_id)
        return this.props.activity_id;
    }

    getLeadId() {
        return this.props.leadId;
    }


    _date_time_picker_visible = () => {
        this.setState({ date_time_picker_visible: true });
    }

    _handleDatePicked = (date) => {
        this.setState({ end_date: Moment(date).format(), show_date: Moment(date).format('MMM DD YYYY, hh:mm:ss a'), date_time_picker_visible: false });
        //this._hideDateTimePicker();

    }

    _hideDateTimePicker = () => {
        this.setState({ date_time_picker_visible: false });
    }

    updateEnquiry = () => {
        let inputData;
        if (this.state.activity_id == 10) {
            inputData = UpdateExchangeCarModel.getInputDataServer(this.state.exchange_car_fields, this.state.activity_id, this.state.end_date, this.state.end_remarks, this.state.leadId);
        } else {
            inputData = UpdateExchangeCarModel.getInputDataServer(this.state.exchange_car_fields, this.state.activity_id, null, this.state.end_remarks, this.state.leadId);
        }
        //console.error("inputtttdata", inputData);
        try {
            this.setState({ loading: true });
            new RestClient().updateExchangeCar(inputData).then((data) => {
                //console.log("inputtttdataSuccess", data);
                this.setState({ loading: false })
                if (data) {
                    if (data.statusCode == '2002' && data.result) {
                        if (Platform.OS === 'ios') {
                            // setTimeout(() => {
                            //     this.setState({ loading: false })
                            //     NavigationServise.navigate("C360MainScreen",
                            //         {
                            //             leadId: data.result.lead_id, from: "FinalRemarksDialog"
                            //         });
                            // }, 500)
                            this.props.gobackToLastScreen()
                        } else {
                            NativeModules.ReactNativeToAndroid.onUpdateExchangeCar(data.result.lead_id, data.result.scheduled_activity_id + '');
                        }

                    }
                    else if (data.message) {
                        this.showAlert(data.message);
                    }
                    else {
                        this.showAlert('Error in updation');
                    }

                }
                else {
                    this.showAlert('Error in updation');
                }
                this.setState({ loading: false });

            }).catch(error => {
                console.error(error)
                if (!error.response) {
                    this.showAlert('No Interent Connection');
                }
                else {
                    this.showAlert('Error in updation');
                }
                this.setState({ loading: false });
            });
        }
        catch (e) {
            this.showAlert(e + '');
        }
    }

    showAlert(alert) {
        this.refs.alert.show(alert);
    }

    render() {
        let remarks = ""
        let { end_remarks } = this.state;
        let { end_date } = this.state;
        let { show_date } = this.state;
        let isAllDone;
        if (this.state.activity_id == 10) {
            isAllDone = end_date != 'Select date*' && end_date && end_remarks;
        } else {
            isAllDone = end_remarks;
        }

        if (end_remarks) {
            remarks = end_remarks;
        }
        if (this.state.date_time_picker_visible) {
            let maxDate = new Date();
            maxDate.setDate(maxDate.getDate() + 10);
            return (
                <View style={{ flex: 1 }}>
                    <DateTimePicker
                        isVisible={this.state.date_time_picker_visible}
                        is24Hour={false}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                        mode={'datetime'}
                        minimumDate={new Date()}
                        maximumDate={maxDate}
                    />
                </View>
            );
        }

        return (
            <View style={{ flex: 1, justifyContent: 'center', backgroundColor: '#000' }}>
                <Toast
                    ref="alert"
                    style={{ backgroundColor: 'red' }}
                    position='top'
                    fadeInDuration={1000}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: 'white' }}
                />
                <LinearGradient colors={['#2e5e86', '#2e3486']} style={{ borderRadius: 5, padding: 10 }}>
                    <View style={{ justifyContent: "center", alignItems: "center", paddingBottom: 10, padding: 10, }}>
                        {/* <Text style={{ padding: 10, fontSize: 16, marginTop: 8, color: '#fff' }}>Schedule Date</Text> */}

                        {(this.state.activity_id == 10) ? <TouchableOpacity style={{ width: '100%', marginTop: 10 }} onPress={this._date_time_picker_visible} >
                            <Text style={{ color: '#fff', fontWeight: 'normal', fontSize: 16, borderBottomWidth: 1, borderColor: '#CFCFCF', padding: 3 }}>{show_date}</Text>
                            <View style={(Platform.OS === 'ios') ? { color: '#CFCFCF', height: 1 } : { color: '#8a8a8a', height: 1 }}></View>
                        </TouchableOpacity> : null}
                        <TextInput
                            style={{ padding: 3, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, marginBottom: 10, color: '#fff', width: '100%' }}
                            placeholder='Remarks*'
                            placeholderTextColor='#abb2b9'
                            value={remarks}
                            onChangeText={(text) => {
                                end_remarks = text;
                                this.setState({ end_remarks });
                            }}
                        />
                        <TouchableOpacity disabled={!isAllDone} style={[(isAllDone) ? localStyles.footerValid : localStyles.footerInvalid, { paddingLeft: 10, paddingRight: 10, paddingBottom: 5, paddingTop: 2, }]} onPress={this.updateEnquiry}>
                            <View style={{ justifyContent: 'center' }}>
                                <Text style={isAllDone ? localStyles.validText : localStyles.invalidText}>Submit</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </LinearGradient>
                {this.state.loading && <View style={styles.loading}>
                    <ActivityIndicator size="large" color="#007fff" />
                </View>}
            </View>
        );

    }
}