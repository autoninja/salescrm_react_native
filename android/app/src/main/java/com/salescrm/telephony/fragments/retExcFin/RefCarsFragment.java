package com.salescrm.telephony.fragments.retExcFin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.CalendarDialogActivity;
import com.salescrm.telephony.adapter.EtvbrCarsAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.etvbr_location.AbsoluteCarModel;
import com.salescrm.telephony.db.etvbr_location.CarModelList;
import com.salescrm.telephony.db.etvbr_location.InfoCarModel;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.db.etvbr_location.LocationAbsCarModel;
import com.salescrm.telephony.db.etvbr_location.LocationCarModelDb;
import com.salescrm.telephony.db.etvbr_location.LocationCarModelResult;
import com.salescrm.telephony.db.etvbr_location.LocationRelCarModel;
import com.salescrm.telephony.db.etvbr_location.RelationalCarModel;
import com.salescrm.telephony.interfaces.SelectDateRange;
import com.salescrm.telephony.model.EtvbrCarModelLocationWise;
import com.salescrm.telephony.model.NewFilter.EtvbrFilterApply;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by prateek on 14/9/17.
 */

public class RefCarsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView tvEnquiry, tvTestDrive, tvV, tvBooking, tvR, tvL;
    private Switch swtchSwitch;
    private Preferences pref;
    private RelativeLayout rel_loading_frame;
    private Realm realm;
    private TextView tvDateIcon;
    private TextView tvDate;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String startDateString = "";
    private String endDateString = "";
    private EtvbrCarsAdapter etvbrCarsAdapter;
    public static boolean CARS_PERCENT = false;
    public LocationCarModelDb locationCarModelDb;
    public Location locationResult;
    private String selectedFilter = "";

    public static Fragment newInstance(int i, String etvbr) {
        Bundle args = new Bundle();
        args.putInt("someInt", i);
        args.putString("someTitle", etvbr);
        RefCarsFragment etvbrCarsFragment = new RefCarsFragment();
        etvbrCarsFragment.setArguments(args);
        return  etvbrCarsFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.etvbr_cars_fragment, container, false);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        mRecyclerView = (RecyclerView) rView.findViewById(R.id.recyclerViewEtvbr);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        tvEnquiry = (TextView) rView.findViewById(R.id.tv_enq);
        tvTestDrive = (TextView) rView.findViewById(R.id.tv_td);
        tvV = (TextView) rView.findViewById(R.id.tv_v);
        tvBooking = (TextView) rView.findViewById(R.id.tv_b);
        tvR = (TextView) rView.findViewById(R.id.tv_r);
        tvL = (TextView) rView.findViewById(R.id.tv_l);
        tvDate = (TextView) rView.findViewById(R.id.date_date);
        swtchSwitch = (Switch) rView.findViewById(R.id.swtch_etvbr);
        rel_loading_frame = (RelativeLayout) rView.findViewById(R.id.rel_loading_frame);
        tvDateIcon = (TextView) rView.findViewById(R.id.date_icon);
        tvDateIcon.setVisibility(View.INVISIBLE);
        swipeRefreshLayout = (SwipeRefreshLayout) rView.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
                    rel_loading_frame.setVisibility(View.VISIBLE);
                    //getEtvbrCarInformationFromServer(startDateString, endDateString);
                    getEtvbrCarInformationFromServer(pref.getStartdateEtvbr(), pref.getEndDateEtvbr());
                    etvbrCarsAdapter.notifyDataSetChanged();
                }else{
                    Util.showToast(getContext(), "No internet connection", Toast.LENGTH_SHORT);
                    setSwipeRefreshView(false);
                }
            }
        });
        setUpRef();
        tvDateIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CalendarDialogActivity.class);
                startActivity(intent);
            }
        });

        if(new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
            //callServer();
            /*Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            String startDate = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
            String endDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
            startDateString = startDate;
            endDateString = endDate;
            getEtvbrCarInformationFromServer(startDateString, endDateString);
            String startDateTv = new SimpleDateFormat("dd/MM").format(calendar.getTime());
            String endDateTv = new SimpleDateFormat("dd/MM").format(date);
            tvDate.setText(startDateTv + "-" + endDateTv);*/
            tvDate.setText(pref.getShowDateEtvbr());
            getEtvbrCarInformationFromServer(pref.getStartdateEtvbr(), pref.getEndDateEtvbr());
            setUpAdapter();
        }else{
            setUpAdapter();
        }

        CARS_PERCENT = false;

        swtchSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                tvEnquiry.setVisibility(View.VISIBLE);
                tvTestDrive.setVisibility(View.VISIBLE);
                tvV.setVisibility(View.VISIBLE);
                tvBooking.setVisibility(View.VISIBLE);
                tvR.setVisibility(View.VISIBLE);
                tvL.setVisibility(View.VISIBLE);
                if (isChecked) {
                    CARS_PERCENT = true;
                    //tvEnquiry.setVisibility(View.GONE);
                    tvEnquiry.setVisibility(View.GONE);
                    tvEnquiry.setText("Retail%");
                    tvTestDrive.setText("Exch%");
                    tvV.setText("Fin/I%");
                    tvBooking.setText("Fin/O%");
                    tvR.setText("PB%");
                    tvL.setText("LE%");
                    logPercentageEvent();
                } else {
                    CARS_PERCENT = false;
                    tvEnquiry.setVisibility(View.GONE);
                    tvEnquiry.setVisibility(View.VISIBLE);
                    tvEnquiry.setText("Retail");
                    tvTestDrive.setText("Exch");
                    tvV.setText("Fin/I");
                    tvBooking.setText("Fin/O");
                    tvR.setText("PB");
                    tvL.setText("LE");
                }
                if(etvbrCarsAdapter != null) {
                    etvbrCarsAdapter.notifyDataSetChanged();
                }
            }
        });
        return rView;
    }

    private void setUpRef() {
        tvBooking.setVisibility(View.VISIBLE);
        tvR.setVisibility(View.VISIBLE);
        tvL.setVisibility(View.VISIBLE);
        tvEnquiry.setText("Retail");
        tvTestDrive.setText("Exch");
        tvV.setText("Fin/I");
        tvBooking.setText("Fin/O");
        tvR.setText("PB");
        tvL.setText("LE");
    }

    private void getEtvbrCarInformationFromServer(String startDateString, String endDateString) {
        rel_loading_frame.setVisibility(View.VISIBLE);
        selectedFilter = new Gson().toJson(WSConstants.selectedFilters.getFilters()).toString();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getEtvbrCarsInfoLocationWise(startDateString, endDateString, selectedFilter,new Callback<EtvbrCarModelLocationWise>() {
            @Override
            public void success(final EtvbrCarModelLocationWise etvbrCarsPojo, Response response) {
                setSwipeRefreshView(false);
                rel_loading_frame.setVisibility(View.GONE);
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(etvbrCarsPojo.getStatusCode().equals(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }

                if(getActivity() != null) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            emptyDb();
                            realm.createObjectFromJson(LocationCarModelResult.class, new Gson().toJson(etvbrCarsPojo.getResult()));
                            setUpAdapter();
                        }
                    });
                }

            }

            @Override
            public void failure(RetrofitError error) {
                setSwipeRefreshView(false);
                rel_loading_frame.setVisibility(View.GONE);
                Util.showToast(getContext(), "Server not responding - "+ error.getMessage(), Toast.LENGTH_SHORT);
            }
        });
    }

    private void emptyDb() {
        realm.delete(LocationCarModelDb.class);
        realm.delete(LocationAbsCarModel.class);
        realm.delete(LocationRelCarModel.class);
        realm.delete(InfoCarModel.class);
        realm.delete(CarModelList.class);
        realm.delete(AbsoluteCarModel.class);
        realm.delete(RelationalCarModel.class);
    }


    private void setUpAdapter() {
        if(realm.where(LocationCarModelDb.class).findAll()!= null && !realm.where(LocationCarModelDb.class).findAll().isEmpty()) {
            try {
                locationCarModelDb = realm.where(LocationCarModelDb.class).equalTo("location_id", pref.getLocationId()).findFirst();
            }catch (Exception e){
                e.printStackTrace();
            }
            etvbrCarsAdapter = new EtvbrCarsAdapter(getActivity(), locationCarModelDb, realm, 1);
            mRecyclerView.setAdapter(etvbrCarsAdapter);
        }
    }

    @Subscribe
    public void dateRangeTeamDashboard(SelectDateRange selectDateRange){
        if(WSConstants.ETVBR_TAB_OPENED == 1) {
            //System.out.println("Yes I'm Range Listener: "+ selectDateRange.getDateArrayList().get(0)+"  and  "+selectDateRange.getDateArrayList().get(selectDateRange.getDateArrayList().size()-1));
            String startFirst = new SimpleDateFormat("dd/MM").format(selectDateRange.getDateArrayList().get(0));
            String endLast = new SimpleDateFormat("dd/MM").format(selectDateRange.getDateArrayList().get(selectDateRange.getDateArrayList().size() - 1));

            String startDate = new SimpleDateFormat("yyyy-MM-dd").format(selectDateRange.getDateArrayList().get(0));
            String endDate = new SimpleDateFormat("yyyy-MM-dd").format(selectDateRange.getDateArrayList().get(selectDateRange.getDateArrayList().size() - 1));
            startDateString = startDate;
            endDateString = endDate;
            tvDate.setText(startFirst + "-" + endLast);
            getEtvbrCarsInfoFromServerOnDateSelection(startDate, endDate);
        }
    }

    private void getEtvbrCarsInfoFromServerOnDateSelection(String startDate, String endDate) {
        rel_loading_frame.setVisibility(View.VISIBLE);
        selectedFilter = new Gson().toJson(WSConstants.selectedFilters.getFilters()).toString();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getEtvbrCarsInfoLocationWise(startDate, endDate, selectedFilter, new Callback<EtvbrCarModelLocationWise>() {
            @Override
            public void success(final EtvbrCarModelLocationWise etvbrCarsPojo, Response response) {
                setSwipeRefreshView(false);
                rel_loading_frame.setVisibility(View.GONE);
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }


                if(etvbrCarsPojo.getStatusCode().equals(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }
                if(getActivity() != null) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            emptyDb();
                            realm.createOrUpdateObjectFromJson(LocationCarModelDb.class, new Gson().toJson(etvbrCarsPojo.getResult().getLocations()));
                            setUpAdapter();
                        }
                    });
                }

            }

            @Override
            public void failure(RetrofitError error) {
                setSwipeRefreshView(false);
                rel_loading_frame.setVisibility(View.GONE);
                Util.showToast(getContext(), "Server not responding - "+ error.getMessage(), Toast.LENGTH_SHORT);
            }
        });
    }
    @Subscribe
    public void applyFilter(EtvbrFilterApply etvbrFilterApply){
        if(etvbrFilterApply.isApplyFilter()) {
            if (WSConstants.ETVBR_TAB_OPENED == 1) {
                if (new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
                    rel_loading_frame.setVisibility(View.VISIBLE);
                    //getEtvbrCarInformationFromServer(startDateString, endDateString);
                    getEtvbrCarInformationFromServer(pref.getStartdateEtvbr(), pref.getEndDateEtvbr());
                    etvbrCarsAdapter.notifyDataSetChanged();
                } else {
                    Util.showToast(getContext(), "No internet connection", Toast.LENGTH_SHORT);
                    setSwipeRefreshView(false);
                }
            }
        }
    }
    private void setSwipeRefreshView(final boolean val) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(val);
            }
        });
    }
    private void logPercentageEvent(){
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_PERCENTAGE);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
    }
}