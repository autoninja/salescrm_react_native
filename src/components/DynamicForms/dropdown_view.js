import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image } from 'react-native';
import PickerList from '../../utils/PickerList'
import ItemPicker from '../../components/createLead/item_picker'


export default class DropdownView extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			isFetching: false,
			showItemPicker: false,
			dataItemPicker: '',
			selectedItem: '',
		}
	}

	componentWillUnmount() {
		if (this.props.radioObject && this.props.radioObject.fQId) {
			this.props.pushToarray({ name: this.props.radioObject.fQId }, -1)
		}

		if (this.props.radioObject.ifVisibleMandatory) {
			this.props.mandatoryField({ id: this.props.radioObject.fQId }, -1)
		}
	}

	showPicker(data) {
		this.setState({ showItemPicker: true, dataItemPicker: data })
	}

	onSelect = (id, item, payload) => {
		this.setState({ selectedItem: item, showItemPicker: false })
		this.props.pushToarray({
			name: id,
			value: { fAnsId: item.id, displayText: item.text, answerValue: item.payload }, questionId: this.props.radioObject.questionId
		})

		if (this.props.radioObject.ifVisibleMandatory) {
			this.props.mandatoryField({ id: this.props.radioObject.fQId, isSelected: true })
		}
	}

	onCancel = () => {
		this.setState({
			showItemPicker: false
		});
	}

	getPickerList = (itemsArray) => {
		let list = [];
		if (itemsArray) {
			itemsArray.map((item) => {
				list.push({ id: item.fAnsId, text: item.displayText, payload: item.answerValue });
			})
		}
		return list;
	}
	setPickerValue = (itemsArray) => {
		let item = {}
		if (itemsArray) {
			for (var i = 0; i < itemsArray.length; i++) {
				console.log("Selected FAnswer ID:"+ this.props.fAnswerId)
				if (itemsArray[i].answerValue == this.props.fAnswerId) {
					item = { id: itemsArray[i].fAnsId, text: itemsArray[i].displayText, payload: itemsArray[i].answerValue }
					return item
				}
			}
		}
	}
	setDefault = (items, condition) => {
		let item = {}
		switch (condition) {
			case 'defaultValue':
				item = { id: items.fAnsId, text: items.displayText, payload: items.answerValue }
				return item
			case 'onlyValue':
				item = { id: items[0].fAnsId, text: items[0].displayText, payload: items[0].answerValue }
				return item
		}
	}

	generateView(innerObjectsValues) {
		//console.log("innerObjectRadio questionChildren DropdownView", JSON.stringify(innerObjectsValues))
		//this.props.callMainGenerateViews(item.values[i], item.values[i].formInputType)
		//console.log("innerObjectsValues.answerChildrennnnn", JSON.stringify(innerObjectsValues.answerChildren))
		let isDisabled = (innerObjectsValues.editable == 0) ? true : false;
		let { selectedItem } = this.state;
		if (!selectedItem) {
			if (innerObjectsValues.defaultFAId) {
				selectedItem = this.setDefault(innerObjectsValues.defaultFAId, 'defaultValue')
			} else if (innerObjectsValues.editable == 0) {
				selectedItem = this.setPickerValue(innerObjectsValues.answerChildren)
			} if (innerObjectsValues.answerChildren && innerObjectsValues.answerChildren.length == 1) {
				selectedItem = this.setDefault(innerObjectsValues.answerChildren, 'onlyValue')
			}

			if (selectedItem) {
				this.props.pushToarray({
					name: innerObjectsValues.fQId,
					value: { fAnsId: selectedItem.id, displayText: selectedItem.text, answerValue: selectedItem.payload }, questionId: innerObjectsValues.questionId
				})

				if (innerObjectsValues.ifVisibleMandatory) {
					this.props.mandatoryField({ id: innerObjectsValues.fQId, isSelected: true })
				}
			}
		}
		return (
			<View>
				{(innerObjectsValues.title) ?
					<Text style={{ fontSize: 18, marginTop: 10, fontWeight: 'bold' }}>{(innerObjectsValues.ifVisibleMandatory) ? innerObjectsValues.title + "*" : ''}</Text> : null}

				<ItemPicker clickdisabled={isDisabled} title={(selectedItem) ? selectedItem.text : 'Select One'} onClick={this.showPicker.bind(this, { title: innerObjectsValues.title, fromId: innerObjectsValues.fQId, listItems: this.getPickerList(innerObjectsValues.answerChildren) })} />


				{(innerObjectsValues.questionChildren) ?
					innerObjectsValues.questionChildren.map((item, key) => {

						if (selectedItem.id == item.key) {
							//console.log("item.displayTextttt", item.key, item.values.length)
							return item.values.map(itemvalue => {
								return this.props.callMainGenerateViews(itemvalue, itemvalue.formInputType)
							})
						}
					}
					) : null}
			</View>
		)


	}

	render() {
		//console.log("innerObjectRadio", JSON.stringify(this.props.innerObjects))
		let { showItemPicker, dataItemPicker } = this.state;
		return (
			<View style={{ flex: 1 }}>
				<PickerList
					showFilter={false}
					visible={showItemPicker}
					onSelect={this.onSelect}
					onCancel={this.onCancel}
					data={dataItemPicker}
				/>
				{this.generateView(this.props.radioObject)}
			</View>
		)
	}
}