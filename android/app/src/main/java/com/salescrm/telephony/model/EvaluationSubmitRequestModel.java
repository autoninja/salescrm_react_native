package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by bannhi on 24/6/17.
 */

public class EvaluationSubmitRequestModel {

    private String lead_last_updated;
    private String lead_id;
    private int scheduled_type;
    private List<Form_response> form_response;


    public int getScheduled_type() {
        return scheduled_type;
    }

    public void setScheduled_type(int scheduled_type) {
        this.scheduled_type = scheduled_type;
    }

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public List<EvaluationSubmitRequestModel.Form_response> getForm_response() {
        return form_response;
    }

    public void setForm_response(List<EvaluationSubmitRequestModel.Form_response> form_response) {
        this.form_response = form_response;
    }

    @Override
    public String toString() {
        return "{\"form_response\":\""+form_response+"\",\"lead_last_updated\" : \"" + lead_last_updated + "\",\"lead_id\" : \"" + lead_id + "\"}";
    }

    public class Form_response
    {
        private String name;

        private Value value;

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public Value getValue ()
        {
            return value;
        }

        public void setValue (Value value)
        {
            this.value = value;
        }

        @Override
        public String toString()
        {
            return "{\\\"name\\\" : \\\""+name+"\\\", \\\"value\\\" : \\\""+value+"\\\"}";
        }


        public class Value
        {
            private String fAnsId;

            private String displayText;

            private String answerValue;

            public String getFAnsId ()
            {
                return fAnsId;
            }

            public void setFAnsId (String fAnsId)
            {
                this.fAnsId = fAnsId;
            }

            public String getDisplayText ()
            {
                return displayText;
            }

            public void setDisplayText (String displayText)
            {
                this.displayText = displayText;
            }

            public String getAnswerValue ()
            {
                return answerValue;
            }

            public void setAnswerValue (String answerValue)
            {
                this.answerValue = answerValue;
            }

            @Override
            public String toString()
            {
                return "{\\\\\\\"fAnsId\\\\\\\" : \\\\\\\""+fAnsId+"\\\\\\\", \\\\\\\"displayText\\\\\\\" : \\\\\\\""+displayText+"\\\\\\\", \\\\\\\"answerValue\\\\\\\" : \\\\\\\""+answerValue+"\\\\\\\"}";
            }
        }
    }
}
