import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import styles from '../styles/styles'
import { NavigationActions, StackActions } from 'react-navigation';

export default class MenuItem extends Component {

  clickHandler = (keyName) => {
    console.log(keyName);
    if (keyName == 'TeamShuffle' || keyName == 'MyTasks') {
      this.props
        .navigationProps
        .dispatch(StackActions.reset(
          {
            index: 0,
            actions: [
              NavigationActions.navigate({ routeName: 'Home' })
            ]
          }));
      this.props.navigationProps.navigate(keyName);
      this.props.navigationProps.toggleDrawer();
    } else {
      this.props.navigationProps.navigate(keyName);
      this.props.navigationProps.toggleDrawer();
    }
  }

  render() {
    let bgColor = 'transparent'
    if (this.props.keyName === 'LeaderBoard') {
      bgColor = 'rgba(190, 195, 32, 0.37)';
    }
    return (
      <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: bgColor, paddingTop: 10, paddingBottom: 10, borderTopWidth: 0.3, borderTopColor: '#607D8B' }} onPress={this.clickHandler.bind(this, this.props.keyName)}>
        <Image source={this.props.imageSource}
          style={styles.sideMenuIcon}
          resizeMode='contain'
        />
        <Text style={styles.menuText}> {this.props.title} </Text>
      </TouchableOpacity>
    );
  }
}
