package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by bharath on 7/9/16.
 */
public class CarModelDetailsResponse {
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private List<CarColor> carColor;

        private List<Variant> variant;

        public List<CarColor> getCarColor() {
            return carColor;
        }

        public void setCarColor(List<CarColor> carColor) {
            this.carColor = carColor;
        }

        public List<Variant> getVariant() {
            return variant;
        }

        public void setVariant(List<Variant> variant) {
            this.variant = variant;
        }

        @Override
        public String toString() {
            return "ClassPojo [carColor = " + carColor + ", variant = " + variant + "]";
        }

        public class CarColor {
            private String id;

            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", name = " + name + "]";
            }
        }

        public class Variant {
            private String id;

            private String fuel_type_id;

            private String color_id;

            private String name;

            private String variant;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getFuel_type_id() {
                return fuel_type_id;
            }

            public void setFuel_type_id(String fuel_type_id) {
                this.fuel_type_id = fuel_type_id;
            }

            public String getColor_id() {
                return color_id;
            }

            public void setColor_id(String color_id) {
                this.color_id = color_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getVariant() {
                return variant;
            }

            public void setVariant(String variant) {
                this.variant = variant;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", fuel_type_id = " + fuel_type_id + ", color_id = " + color_id + ", name = " + name + ", variant = " + variant + "]";
            }
        }

    }
}