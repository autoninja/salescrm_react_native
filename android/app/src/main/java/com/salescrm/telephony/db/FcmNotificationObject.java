package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by prateek on 29/11/16.
 */

public class FcmNotificationObject extends RealmObject {

    private int notification_type;
    private int notification_cat;
    private String notifiacation_message;
    private boolean notification_read;
    private String leadId;

    public int getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(int notification_type) {
        this.notification_type = notification_type;
    }

    public int getNotification_cat() {
        return notification_cat;
    }

    public void setNotification_cat(int notification_cat) {
        this.notification_cat = notification_cat;
    }

    public String getNotifiacation_message() {
        return notifiacation_message;
    }

    public void setNotifiacation_message(String notifiacation_message) {
        this.notifiacation_message = notifiacation_message;
    }

    public boolean isNotification_read() {
        return notification_read;
    }

    public void setNotification_read(boolean notification_read) {
        this.notification_read = notification_read;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

}
