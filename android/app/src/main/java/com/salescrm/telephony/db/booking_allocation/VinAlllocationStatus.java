package com.salescrm.telephony.db.booking_allocation;

import io.realm.RealmList;
import io.realm.RealmObject;

public class VinAlllocationStatus extends RealmObject {
    private String id;
    private String name;
    public RealmList<String> role_ids = new RealmList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<String> getVinAllocationRoleIds() {
        return role_ids;
    }

    public void setVinAllocationRoleIds(RealmList<String> vinAllocationRoleIds) {
        this.role_ids = vinAllocationRoleIds;
    }
}
