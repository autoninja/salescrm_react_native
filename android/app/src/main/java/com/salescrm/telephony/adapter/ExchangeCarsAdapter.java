package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddSearchActivity;
import com.salescrm.telephony.activity.HomeActivity;
import com.salescrm.telephony.activity.createEnquiry.RNCreateLeadActivity;
import com.salescrm.telephony.activity.createEnquiry.RNUpdateExchangeCarDetailActivity;
import com.salescrm.telephony.db.PlannedActivities;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.db.car.ExchangeCarDetails;
import com.salescrm.telephony.fragments.NewCarsFragment;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.interfaces.NewCarsProgressLoader;
import com.salescrm.telephony.model.ExchangeCarCancelModel;
import com.salescrm.telephony.model.RescheduleData;
import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.service_handlers.CarsFragmentServiceHandler;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.CustomDialogs;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bannhi on 22/6/17.
 */

public class ExchangeCarsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    RealmList<ExchangeCarDetails> exchangeCarDetailsRealmList = new RealmList<>();
    ExchangeCarDetails exchangeCarDetails;
    Activity activity;
    String leadLastUpdated = "";
    Preferences pref;
    private NewCarsProgressLoader mNewCarsProgressLoader;
    private AlertDialog alertDialog;
    private String exchangeRescheduleDate = "";
    private String exchangeRescheduleRemarks = "";
    private String activityId = "";
    private Realm realm;
    private String selectedRadio = "";
    private String selectedCancelReason = "";
    private String selectedCancelRemarks = "";
    private Calendar calendarMax =null;

    public ExchangeCarsAdapter(RealmList<ExchangeCarDetails> exchangeCarDetailsRealmList, String leadLastUpdated, Activity activity) {

        this.activity = activity;
        this.exchangeCarDetailsRealmList = exchangeCarDetailsRealmList;
        this.leadLastUpdated = leadLastUpdated;
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(activity);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.exchange_car_card, null);
        System.out.println("Exchange CARS ADAPTER CALLED");
        mNewCarsProgressLoader = new NewCarsFragment();
        return new ExchangeCarItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RecyclerView.ViewHolder iViewHolder = holder;
        calendarMax = Calendar.getInstance();
        calendarMax.add(Calendar.DAY_OF_MONTH, 10);
        if (holder instanceof ExchangeCarItemHolder) {
            final ExchangeCarDetails temp = exchangeCarDetailsRealmList.get(position);
            //System.out.println("getRoleId"+ pref.getRoleId());

            //System.out.println("aaila "+temp.getOwner_type() +", " +temp.getMake_year());
            activityId = temp.getActivity_id();
            String purchaseDate = "", regNo = "",kms="", ownerType = "";
            if(temp.getPurchase_date()!=null && !temp.getPurchase_date().isEmpty()){
                purchaseDate = temp.getMake_year();
            }
            if(temp.getReg_no()!=null && !temp.getReg_no().isEmpty()){
                regNo = temp.getReg_no();
            }
            if(temp.getKms_run()!=null && !temp.getKms_run().isEmpty()){
                kms = temp.getKms_run();
            }

            if(purchaseDate!=null && !purchaseDate.isEmpty()){
                ((ExchangeCarItemHolder) iViewHolder).carNameTv.setText
                        (temp.getExchangecarname() + "(" + purchaseDate + ")");
            }else{
                ((ExchangeCarItemHolder) iViewHolder).carNameTv.setText
                        (temp.getExchangecarname());
            }

            if(temp.getOwner_type()!= null && !temp.getOwner_type().isEmpty()){
                ((ExchangeCarItemHolder) iViewHolder).car_details.setText(kms+" Km, "
                                +"Owner Type : "+temp.getOwner_type());
            }

            /*if(regNo!=null && !regNo.isEmpty() && kms!=null && !kms.isEmpty()){
                ((ExchangeCarItemHolder) iViewHolder).car_details.setText(regNo + ", " + kms + " Km");
            }else if((regNo==null || regNo.isEmpty()) && (kms!=null && !kms.isEmpty() && !kms.equalsIgnoreCase("0"))){
                ((ExchangeCarItemHolder) iViewHolder).car_details.setText(kms + " Km");
            }else{
                ((ExchangeCarItemHolder) iViewHolder).car_details.setText(regNo);
            }*/


            ((ExchangeCarItemHolder) iViewHolder).carEditIb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleCarEditClick(temp);
                }
            });

            if(!isEvaluatorOrEvaluatorManager()|| activityId == null || (getUserRole().equalsIgnoreCase("28")  && activityId.equalsIgnoreCase("11"))){
                ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setEnabled(false);
                //((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setHeight(30);
                //((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setPaddingRelative(15, 0, 15, 0);
                //((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setBackgroundColor(Color.parseColor("#8c8c8c"));
                ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv
                        .setBackground(ContextCompat.getDrawable(activity, R.drawable.button_grey));

            }else{
                ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setEnabled(true);
                ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv
                        .setBackground(ContextCompat.getDrawable(activity, R.drawable.button_shadow));
            }

            ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleStageChangeBtnClick(temp);
                }
            });

            /*if(isEvaluatorOrEvaluatorManager()){
                ((ExchangeCarItemHolder) iViewHolder).btnExchangeDetail.setEnabled(true);
                ((ExchangeCarItemHolder) iViewHolder).btnExchangeDetail.setVisibility(View.VISIBLE);
            }else{
                ((ExchangeCarItemHolder) iViewHolder).btnExchangeDetail.setEnabled(false);
                ((ExchangeCarItemHolder) iViewHolder).btnExchangeDetail.setVisibility(View.GONE);
            }*/

            ((ExchangeCarItemHolder) iViewHolder).btnExchangeDetail.setEnabled(true);
            ((ExchangeCarItemHolder) iViewHolder).btnExchangeDetail.setVisibility(View.VISIBLE);

            ((ExchangeCarItemHolder) iViewHolder).btnExchangeDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, RNUpdateExchangeCarDetailActivity.class);
                    intent.putExtra("update_without_changing_activity", true);
                    activity.startActivity(intent);
                }
            });
            populateData(iViewHolder, temp);


        }
    }

    @Override
    public int getItemCount() {
        return exchangeCarDetailsRealmList.size();
    }

    private boolean isEvaluatorOrEvaluatorManager() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR)
                || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR_MANAGER)) {
                    return true;
                }
            }
        }
        return false;
    }

    private String getUserRole() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        //ArrayList<Integer> arrayList = new ArrayList<>();
        int role = 0;
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR_MANAGER)) {
                    if(Integer.parseInt(userRoles.get(i).getId())> role){
                        role = Integer.parseInt(userRoles.get(i).getId());
                    }
                }
            }
        }
        return role+"";
    }


    private void handleStageChangeBtnClick(ExchangeCarDetails exchangeCarDetails){
            /*switch (stageId) {
                case WSConstants.CarEvaluationStage.SCHEDULE_EVALUATION:*/
                    /*CustomDialogs.getInstance((Activity) activity, exchangeCarDetails, mNewCarsProgressLoader).
                            showEvaluationDialog(leadLastUpdated);*/
                    System.out.println("Rama: "+activityId);
                    final Dialog dialog = new Dialog(activity, R.style.AppTheme_Black);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    if( dialog.getWindow()!=null){
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
                        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                                WindowManager.LayoutParams.MATCH_PARENT);
                    }
                    dialog.setContentView(R.layout.evaluators_actions_dialog);
                    Button done = (Button) dialog.findViewById(R.id.evaluation_done);
                    done.setText(activityId.equalsIgnoreCase("10")? "Evaluation Done":"Price Quoted");
                    Button reSchedule = (Button) dialog.findViewById(R.id.evaluation_reschedule);
                    Button cancel = (Button) dialog.findViewById(R.id.evaluation_cancel);
                    ImageButton clsoeDialog = (ImageButton) dialog.findViewById(R.id.close_dialog);
                    cancel.setText(activityId.equalsIgnoreCase("10")? "Cancel Evaluation":"Customer Denied");

                    clsoeDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    done.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //dialog.dismiss();
                            //activity.finish();
                            Intent intent = new Intent(activity, RNUpdateExchangeCarDetailActivity.class);
                            intent.putExtra("update_without_changing_activity", false);
                            activity.startActivity(intent);

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if(dialog != null){
                                        dialog.dismiss();
                                    }
                                }
                            }, 100);
                        }
                    });

                    reSchedule.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            TextView tvDateHeader;
                            TextView tvDateValue;
                            TextView tvRemarks;
                            TextView etRemarks;
                            Button btAction;
                            final Dialog dialogReschedule = new Dialog(activity, R.style.AppTheme_Black);
                            dialogReschedule.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            if( dialogReschedule.getWindow()!=null){
                                dialogReschedule.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
                                dialogReschedule.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                                        WindowManager.LayoutParams.MATCH_PARENT);
                            }
                            dialogReschedule.setContentView(R.layout.booking_car_submit_form);
                            tvDateHeader = (TextView) dialogReschedule.findViewById(R.id.tv_booking_submit_date_header);
                            tvDateHeader.setVisibility(View.GONE);
                            tvDateValue = (TextView) dialogReschedule.findViewById(R.id.tv_booking_submit_date_value);
                            tvRemarks = (TextView) dialogReschedule.findViewById(R.id.tv_booking_submit_remarks_header);
                            etRemarks = (EditText) dialogReschedule.findViewById(R.id.et_booking_submit_remarks_value);
                            btAction = (Button) dialogReschedule.findViewById(R.id.bt_booking_submit_action);



                            btAction.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    exchangeRescheduleRemarks = etRemarks.getText().toString().trim();
                                    exchangeRescheduleDate = tvDateValue.getText().toString();
                                    RescheduleData rescheduleData = new RescheduleData(pref.getLeadID(), exchangeRescheduleDate, exchangeRescheduleRemarks);
                                    System.out.println("rescheduleData"+ new Gson().toJson(rescheduleData).toString());
                                    System.out.println("rescheduleData dd "+exchangeRescheduleRemarks+", "+exchangeRescheduleDate);
                                    if(!exchangeRescheduleRemarks.equalsIgnoreCase("") && !exchangeRescheduleDate.equalsIgnoreCase("")){
                                    ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).rescheduleExchange(rescheduleData, new Callback<Response>() {
                                        @Override
                                        public void success(Response response, Response response2) {
                                            dialogReschedule.dismiss();
                                            //dialog.dismiss();
                                            //activity.finish();
                                            fetchC360(pref.getLeadID(), dialog);
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {

                                        }
                                    });
                                    }else{

                                    }
                                }
                            });

                            tvDateValue.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(final View v) {
                                    Util.showDatePicker(new DatePickerDialog.OnDateSetListener() {

                                        @Override
                                        public void onDateSet(DatePickerDialog view, final int year, final int monthOfYear, final int dayOfMonth) {
                                            final Calendar calendar = Calendar.getInstance();
                                            calendar.set(year, monthOfYear, dayOfMonth);
                                            Util.showTimePicker(new TimePickerDialog.OnTimeSetListener() {
                                                @Override
                                                public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
                                                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                                    calendar.set(Calendar.MINUTE, minute);
                                                    calendar.set(Calendar.SECOND, second);
                                                    exchangeRescheduleDate = calendar.get(Calendar.YEAR)
                                                            + "-"
                                                            + ((calendar.get(Calendar.MONTH)+1) < 10? ("0"+(calendar.get(Calendar.MONTH)+1)) : calendar.get(Calendar.MONTH)+1 )
                                                            + "-"
                                                            + ((calendar.get(Calendar.DAY_OF_MONTH) < 10 )?"0"+calendar.get(Calendar.DAY_OF_MONTH):calendar.get(Calendar.DAY_OF_MONTH))
                                                            + "  "
                                                            + ((calendar.get(Calendar.HOUR_OF_DAY) <10 )? "0"+calendar.get(Calendar.HOUR_OF_DAY) : calendar.get(Calendar.HOUR_OF_DAY))
                                                            +":"
                                                            +((calendar.get(Calendar.MINUTE) < 10)? "0"+calendar.get(Calendar.MINUTE) : calendar.get(Calendar.MINUTE))
                                                            +":"
                                                            +((calendar.get(Calendar.SECOND) < 10)? "0"+calendar.get(Calendar.SECOND):calendar.get(Calendar.SECOND));

                                                    tvDateValue.setText(exchangeRescheduleDate);
                                                }
                                            }, (Activity) activity, v.getId() );


                                        }
                                    }, (Activity) activity, v.getId(), null, Calendar.getInstance(),calendarMax);
                                }
                            });


                            dialogReschedule.setCanceledOnTouchOutside(false);
                            dialogReschedule.setCanceledOnTouchOutside(false);
                            dialogReschedule.show();



                        }
                    });

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectedRadio = "";
                            selectedCancelReason = "";
                            selectedCancelRemarks = "";
                            final Dialog dialogCancel = new Dialog(activity, R.style.AppTheme_Black);

                            dialogCancel.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            if( dialogCancel.getWindow()!=null){
                                dialogCancel.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
                                dialogCancel.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                                        WindowManager.LayoutParams.MATCH_PARENT);
                            }
                            dialogCancel.setContentView(R.layout.exchange_car_cancel);
                            Button btAction = (Button) dialogCancel.findViewById(R.id.btn_exchange_cancel);
                            EditText edtOthers = (EditText) dialogCancel.findViewById(R.id.et_exchange_cancel);


                            btAction.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String remarks = edtOthers.getText().toString().trim();
                                    ExchangeCarCancelModel exchangeCarCancelModel = new ExchangeCarCancelModel(pref.getLeadID(), remarks);
                                    System.out.println("exchangeCarCancelModel " + new Gson().toJson(exchangeCarCancelModel).toString());
                                    if(!remarks.equalsIgnoreCase("")){
                                        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).cancelExchange(exchangeCarCancelModel, new Callback<Response>() {
                                            @Override
                                            public void success(Response response, Response response2) {
                                                    dialogCancel.dismiss();
                                                    fetchC360(pref.getLeadID(), dialog);

                                            }

                                            @Override
                                            public void failure(RetrofitError error) {

                                            }
                                        });
                                    }else{

                                    }
                                }
                            });

                            dialogCancel.setCanceledOnTouchOutside(false);
                            dialogCancel.setCanceledOnTouchOutside(false);
                            dialogCancel.show();
                        }
                    });

                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                    //break;
    }

    /**
     * handling click on edit button
     */
    private void handleCarEditClick(ExchangeCarDetails exchangeCarDetails) {
        CustomDialogs.getInstance((Activity) activity, exchangeCarDetails,mNewCarsProgressLoader).showExchangeCarEditdialog();

    }

    private void populateData(RecyclerView.ViewHolder iViewHolder, ExchangeCarDetails exchangeCarDetails) {
        /*CarsFragmentServiceHandler.getInstance(activity).getEvaluationList(exchangeCarDetails.getLeadId(),pref.getAccessToken());
        int stageId = -1;
        if (exchangeCarDetails.getCar_stage_id() != null && !exchangeCarDetails.getCar_stage_id().isEmpty()) {
            try {
                stageId = Integer.parseInt(exchangeCarDetails.getCar_stage_id());
                //stageId = 23;
                switch (stageId) {
                    case WSConstants.CarEvaluationStage.SCHEDULE_EVALUATION:
                        ((ExchangeCarItemHolder) iViewHolder).priceLyt.setVisibility(View.GONE);
                        //((ExchangeCarItemHolder) iViewHolder).priceValue.setVisibility(View.GONE);
                        ((ExchangeCarItemHolder) iViewHolder).evulationDetailsLyt.setVisibility(View.GONE);
                        ((ExchangeCarItemHolder) iViewHolder).stageTv.setVisibility(View.GONE);
                        ((ExchangeCarItemHolder) iViewHolder).stageIcon.setVisibility(View.GONE);
                        ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.VISIBLE);
                        ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setText("Schedule Evaluation");
                        break;
                    case WSConstants.CarEvaluationStage.EVALUATION_CONDUCTED:
                        ((ExchangeCarItemHolder) iViewHolder).priceLyt.setVisibility(View.VISIBLE);
                       // ((ExchangeCarItemHolder) iViewHolder).priceValue.setVisibility(View.VISIBLE);
                        ((ExchangeCarItemHolder) iViewHolder).evulationDetailsLyt.setVisibility(View.VISIBLE);
                        ((ExchangeCarItemHolder) iViewHolder).evaluatorName.setVisibility(View.VISIBLE);
                        ((ExchangeCarItemHolder) iViewHolder).evaluation_date_time.setVisibility(View.GONE);
                        ((ExchangeCarItemHolder) iViewHolder).stageTv.setVisibility(View.VISIBLE);
                        ((ExchangeCarItemHolder) iViewHolder).stageIcon.setVisibility(View.VISIBLE);
                        ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);

                        ((ExchangeCarItemHolder) iViewHolder).stageTv.setText("Evaluation Conducted");
                          ((ExchangeCarItemHolder) iViewHolder).stageIcon.setImageResource(R.drawable.tick);

                        ((ExchangeCarItemHolder) iViewHolder).evaluatorName.setText(exchangeCarDetails.getEvaluator_name());

                        ((ExchangeCarItemHolder) iViewHolder).tvPriceExpectedValue.setText(exchangeCarDetails.getExpected_price());
                        ((ExchangeCarItemHolder) iViewHolder).tvPriceMarketValue.setText(exchangeCarDetails.getMarket_price());
                        ((ExchangeCarItemHolder) iViewHolder).tvPriceQuoteValue.setText(exchangeCarDetails.getPrice_quoted());

                        break;
                    case WSConstants.CarEvaluationStage.EVALUATION_SCHEDULED:
                        ((ExchangeCarItemHolder) iViewHolder).priceLyt.setVisibility(View.GONE);
                       // ((ExchangeCarItemHolder) iViewHolder).priceValue.setVisibility(View.GONE);
                        ((ExchangeCarItemHolder) iViewHolder).evulationDetailsLyt.setVisibility(View.VISIBLE);
                        ((ExchangeCarItemHolder) iViewHolder).evaluatorName.setVisibility(View.VISIBLE);
                        ((ExchangeCarItemHolder) iViewHolder).evaluation_date_time.setVisibility(View.VISIBLE);
                        ((ExchangeCarItemHolder) iViewHolder).stageTv.setVisibility(View.VISIBLE);
                        ((ExchangeCarItemHolder) iViewHolder).stageIcon.setVisibility(View.VISIBLE);
                        ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.GONE);

                        ((ExchangeCarItemHolder) iViewHolder).stageTv.setText("Evaluation Scheduled");
                        ((ExchangeCarItemHolder) iViewHolder).stageIcon.setImageResource(R.drawable.scheduled_icon);
                        //  ((ExchangeCarItemHolder) iViewHolder).stageIcon.setImageDrawable(something else);

                        ((ExchangeCarItemHolder) iViewHolder).evaluatorName.setText(exchangeCarDetails.getEvaluator_name());
                        ((ExchangeCarItemHolder) iViewHolder).evaluation_date_time.setText(exchangeCarDetails.getEvaluation_date_time());


                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }*/

        ((ExchangeCarItemHolder) iViewHolder).priceLyt.setVisibility(View.GONE);
        //((ExchangeCarItemHolder) iViewHolder).priceValue.setVisibility(View.GONE);
        ((ExchangeCarItemHolder) iViewHolder).evulationDetailsLyt.setVisibility(View.GONE);
        ((ExchangeCarItemHolder) iViewHolder).stageTv.setVisibility(View.GONE);
        ((ExchangeCarItemHolder) iViewHolder).stageIcon.setVisibility(View.GONE);
        ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setVisibility(View.VISIBLE);
        if(activityId != null) {
            if (activityId.equalsIgnoreCase("10")) {
                ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setText("Old Car Evaluation");
            } else if (activityId.equalsIgnoreCase("11")) {
                ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setText("Manager's Evaluation");
            }
        }else{
            ((ExchangeCarItemHolder) iViewHolder).stageChangeBtnTv.setText("Done");
        }

    }

    private void fetchC360(String leadId, Dialog dialogMain) {
        //progressDialog.setMessage("Redirecting to Home !!!.. Please wait");
        //progressDialog.show();
        //this.generatedLeadId = leadId;

        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(leadId);
        new FetchC360InfoOnCreateLead(new FetchC360OnCreateLeadListener() {
            @Override
            public void onFetchC360OnCreateLeadSuccess() {
                //progressDialog.dismiss();
                //Preferences.setLeadID("" + generatedLeadId);
                SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", Util.getInt(leadId)).findFirst();
                if (salesCRMRealmTable != null) {
                    Preferences.setScheduledActivityId(salesCRMRealmTable.getScheduledActivityId());

                }

                activity.startActivity(new Intent(activity, HomeActivity.class));
                dialogMain.dismiss();
                activity.finish();
            }

            @Override
            public void onFetchC360OnCreateLeadError() {
                //progressDialog.dismiss();
                Util.showToast(activity, "Failed to fetch Home information", Toast.LENGTH_SHORT);
                activity.finish();

            }
        }, activity, leadData, pref.getAppUserId()).call(true);

    }

    private class ExchangeCarItemHolder extends RecyclerView.ViewHolder {

        //public   View parentLayout;
        TextView carNameTv, tvPriceQuoteValue, tvPriceExpectedValue, tvPriceMarketValue, stageTv,
                stageChangeBtnTv, evaluatorName, evaluation_date_time, car_details;
        ImageButton carEditIb;
        ImageView stageIcon;
        RelativeLayout priceLyt, evulationDetailsLyt;
        TextView btnExchangeDetail;

        public ExchangeCarItemHolder(View itemView) {
            super(itemView);
            // parentLayout = itemView;
            carNameTv = (TextView) itemView.findViewById(R.id.car_name);
            priceLyt = (RelativeLayout) itemView.findViewById(R.id.price_rll);
           // priceValue = (RelativeLayout) itemView.findViewById(R.id.price_value_block);

            evulationDetailsLyt = (RelativeLayout) itemView.findViewById(R.id.evaluation_details);
            evaluatorName = (TextView) itemView.findViewById(R.id.evaluator_name);
            evaluation_date_time = (TextView) itemView.findViewById(R.id.evaluation_date_time);
            car_details = (TextView) itemView.findViewById(R.id.car_details);
            tvPriceQuoteValue = (TextView) itemView.findViewById(R.id.price_quote_value);
            tvPriceMarketValue = (TextView) itemView.findViewById(R.id.market_price_value);
            tvPriceExpectedValue = (TextView) itemView.findViewById(R.id.price_expected_value);

            stageTv = (TextView) itemView.findViewById(R.id.stage_text);
            stageIcon = (ImageView) itemView.findViewById(R.id.stage_icon);
            stageChangeBtnTv = (TextView) itemView.findViewById(R.id.btn_nxt_stage);
            carEditIb = (ImageButton) itemView.findViewById(R.id.car_edit);
            btnExchangeDetail = (TextView) itemView.findViewById(R.id.tv_exchange_details);


        }

    }
}
