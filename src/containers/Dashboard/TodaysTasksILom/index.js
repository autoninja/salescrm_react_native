import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  FlatList,
  Alert,
  TouchableOpacity,
  Image,
  Platform
} from "react-native";
import { eventTeamDashboard } from "../../../clevertap/CleverTapConstants";
import CleverTapPush from "../../../clevertap/CleverTapPush";

import Utils from "../../../utils/Utils";
import RestClient from "../../../network/rest_client";
import {
  NavigationEvents,
  createMaterialTopTabNavigator,
  createAppContainer
} from "react-navigation";
import TodaysTasksChild from "./todaysTasks";

export default class TodaysTasksMainILom extends React.Component {

  constructor(props) {
    super(props);
    let expandable = false;

    if (this.props.isUserBH || this.props.isUserOps || this.props.isUserSM) {
      expandable = true;
    }
    this.state = {
      isLoading: true,
      expandable,
      dataArray: []
    };
  }

  componentDidMount() {
    //Alert.alert(JSON.stringify(this.props.notification));
    this.makeRemoteRequest();
  }

  updateOnFocusChange() {
    CleverTapPush.pushEvent(eventTeamDashboard.event, {
      "Dashboard Type": eventTeamDashboard.keyType.valuesType.todaysTasks
    });
  }
  openTasksList(id, name, dp_url) {
    this.props.navigation.navigate("TasksList", { id, name, dp_url })
  }

  makeRemoteRequest = () => {
    this.setState({ isLoading: true });
    new RestClient()
      .getTodaysDashboard({
        date: Utils.getTodaysDate()
      })
      .then(responseJson => {
        let dataArray = [];
        if (responseJson.result && responseJson.result.length > 0) {
          dataArray = responseJson.result;
        }
        this.setState(
          {
            isLoading: false,
            dataArray: dataArray
          },
          function () {
            // In this block you can do something with new state.
            //console.log(responseJ
            // const size  = responseJson.result.length;
            // console.log(responseJson.result.length);
          }
        );
      })
      .catch(error => {
        this.setState({
          isLoading: false
        });
      });
  };

  navigate(navigation, from, payload) {
    if (from == "user_info") {
      navigation.navigate("UserInfo", {
        name: payload.info.name,
        dp_url: payload.info.dp_url
      });
      console.log("_showUserInfo");
    } else if (from == "Uncalled Tasks") {
      let data = {
        info: {
          clicked_val: "Uncalled Tasks",
          user_id: payload.info.id,
          title: payload.info.name,
          user_location_id: payload.info.location_id,
          user_role_id: payload.info.user_role_id
        }
      };
      navigation.navigate("ETVBRDetails", { data });
      console.log("_UncalledLongPress");
    }
  }
  getNotificationPendingLocationId() {
    if (
      this.props.notification &&
      this.props.notification.name == "pending_alert" &&
      this.props.notification.payload
    ) {
      return this.props.notification.payload.pendingLocationId;
    }
    return 0;
  }
  getTabs() {
    var tabs = {};
    let initialRouteName = this.state.dataArray[0].location_name;
    for (var i = 0; i < this.state.dataArray.length; i++) {
      let data = this.state.dataArray[i];
      if (this.getNotificationPendingLocationId() == data.location_id) {
        initialRouteName = data.location_name;
      }
      tabs[data.location_name] = {
        screen: props => (
          <TodaysTasksChild
            openTasksList={this.openTasksList.bind(this)}
            refresh={this.makeRemoteRequest.bind(this)}
            data={data}
            onLongPress={this.navigate.bind(this, this.props.navigation)}
            expandable={this.state.expandable}
          />
        )
      };
    }
    return createAppContainer(
      createMaterialTopTabNavigator(tabs, {
        backBehavior: "none",
        swipeEnabled: true,
        initialRouteName: initialRouteName,
        tabBarOptions: {
          style: { backgroundColor: "#fff", elevation: 0 },
          tabStyle: {
            height: 48
          },
          labelStyle: {
            fontSize: 14
          },
          inactiveTintColor: "#808080",
          activeTintColor: "#2e5e86",
          upperCaseLabel: false,
          indicatorStyle: { height: 2, backgroundColor: "#2e5e86" },
          scrollEnabled: true
        }
      })
    );
  }
  render() {
    let navigation = this.props.navigation;

    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: "#fff",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <ActivityIndicator />
          <NavigationEvents
            onWillFocus={payload => {
              this.updateOnFocusChange();
            }}
          />
        </View>
      );
    } else if (this.state.dataArray.length == 1) {
      return (
        <View style={{ flex: 1, backgroundColor: "#fff", padding: 6 }}>
          <NavigationEvents
            onWillFocus={payload => {
              this.updateOnFocusChange();
            }}
          />
          <TodaysTasksChild
            openTasksList={this.openTasksList.bind(this)}
            refresh={this.makeRemoteRequest.bind(this)}
            data={this.state.dataArray[0]}
            onLongPress={this.navigate.bind(this, this.props.navigation)}
            expandable={this.state.expandable}
          />
        </View>
      );
    } else if (this.state.dataArray.length > 1) {
      let TABS = this.getTabs();
      return (
        <View style={{ flex: 1, backgroundColor: "#fff", padding: 6 }}>
          <NavigationEvents
            onWillFocus={payload => {
              this.updateOnFocusChange();
            }}
          />
          <TABS />
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            alignItems: "center",
            backgroundColor: "#fff",
            justifyContent: "center"
          }}
        >
          <Text>No data from server</Text>
          <TouchableOpacity
            onPress={() => {
              this.makeRemoteRequest();
            }}
          >
            <Text
              style={{
                marginTop: 10,
                padding: 10,
                borderColor: "#c4c4c4",
                borderRadius: 4,
                borderWidth: 1
              }}
            >
              {" "}
              Reload{" "}
            </Text>
          </TouchableOpacity>
          <NavigationEvents
            onWillFocus={payload => {
              this.updateOnFocusChange();
            }}
          />
        </View>
      );
    }
  }
}
