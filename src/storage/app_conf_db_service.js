import { realm } from './realm_schemas';
import { AppConfModel } from '../models/app_conf_model'

const AppConfDBService = {
  findAll: function () {
    return realm.objects('AppConfDB');
  },
  save: function (appConfigs) {
    realm.write(() => {
      appConfigs.map((appConf) => {
        realm.create('AppConfDB', appConf, true);
      })
    })
    let appConfDB = realm.objects('AppConfDB');
    console.log('appConfDB.length', appConfDB.length);
  },
  isEnabled: (key) => {
    let appConf = realm.objects('AppConfDB').filtered('key == "' + key + '"')
    return appConf && appConf.length > 0 && appConf[0].value == '1';
  },
  getVal: (key) => {
    let appConf = realm.objects('AppConfDB').filtered('key == "' + key + '"')
    if (appConf && appConf.length > 0) {
      if (key == "first_call_advance_days") {
        return appConf[0].value;
      } else {
        return (appConf[0].value == 0) ? 0 : 1;
      }
    }
    else {
      return 0;
    }

  },
  deleteAll: function () {
    realm.write(() => {
      realm.delete(realm.objects('AppConfDB'));
    })
  },
}

export default AppConfDBService;
