
import React from 'react';
import { View, StyleSheet, Platform} from 'react-native';

import {TaskConstants, FormQuestionIdConstants} from '../../utils/Constants';
import Loader from "../uielements/Loader";
import SuccessView from "../uielements/SuccessView";
import {FormAnswerIds} from '../../containers/PlanNextTask/PNTModel';
import GenericFormDialog from './GenericFormDialog'
import VerificationLanguagePicker from './VerificationLanguagePicker'
import RestClient from '../../network/rest_client'
import Toast, {DURATION} from 'react-native-easy-toast';
import NavigationService from '../../navigation/NavigationService'
import { NavigationActions } from 'react-navigation';
const styles = StyleSheet.create({
  main: {
    backgroundColor:'#000000',
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop:80,
    paddingBottom:80,
    flex:1,
    justifyContent:'center'
  },
  
});

export default class PNTSubmission extends React.Component {

	constructor(props)
		{
		    super(props);
		    this.state = {
		    	isLoading: false,
				pntModel:this.props.navigation.getParam('pntModel',null),
				previousFormAnswers:this.props.navigation.getParam("previousFormAnswers",null),
				info:this.props.info,
				action:this.getAction()
			}
		}

	componentDidMount(){
		if(this.state.action.name == "book_vehicle") {
			//Redirect to c360
			let formSubmissionInputData = {
				lead_id:this.state.info.leadId,
				lead_last_updated:this.state.info.leadLastUpdated,
				scheduled_activity_id:this.state.info.scheduledActivityId
			}
			if(this.state.previousFormAnswers != null) {
				formSubmissionInputData.action_id = this.state.info.actionId;
				formSubmissionInputData.form_response = this.getServerFormResponse(this.state.previousFormAnswers)
			}
			NavigationService.resetScreen("C360MainScreen",
			 { 
				 leadId:this.state.info.leadId,
				 formSubmissionInputData,
				 OPEN_TAB:'Vehicles'
			});
		}
	}
	getAction() {
		let pntModel = this.props.navigation.getParam('pntModel',null)
		let {info} = this.props;
		let action={name:"Default"};
		if(pntModel) {
        switch(pntModel.fAnswerId) {
            case FormAnswerIds.BOOK_CAR_ID:
				action = {name:"book_vehicle", title:"Booking Car From C360"}
				break;
			case FormAnswerIds.BOOKED:
				action = {name:"book_vehicle", title:"Booking Two wheelers From C360"}
				break;
            case FormAnswerIds.BOOK_RE:
				action = {name:"book_re", title:"This Booking has been marked in DMS."}
				break;
            case FormAnswerIds.IL_RENEWED:
				action = {name:"il_renewed", title:"Already Renewed by ICICI Lombard."}
				break;
            case FormAnswerIds.SALES_CONFIRMED:
				if(info.isClientILomBank) {
					action = {name:"sales_confirmed_il_bank", title:"Sales Confirmed"}
				}
				else if(info.isClientILom) {
					action = {name:"sales_confirmed_il", title:"Sales Confirmed"}

				}
				break;
            case FormAnswerIds.VERIFICATION:
                action = {name:"send_to_verification", title:"Send to Verification"}
                break;
           
		}
		}
		return action;
        
	}
	getServerFormResponse(previousFormAnswers) {
		previousFormAnswers.map((item)=> {
			delete item.questionId;
			return item;
		});
		return previousFormAnswers;
	}
	postSalesConfirmed(bookingObj) {
		new RestClient().ilBookSaleConfirmed(bookingObj).then((data) => {
			if(data && data.statusCode == "2002") {
				this.onSuccessFormSubmission("Sales Confirmed");
				
			}
			else {
				this.showAlert("Failed to confirm sales");
				this.setState({ isLoading: false})    
			}
				 
			}).catch(error => {
				console.error(error);
				this.showAlert("Failed to confirm sales");
				this.setState({ isLoading: false});
		});
	}
	submit(action, payload) {
		let { info, previousFormAnswers } = this.state;
		let bookingObj = {
			lead_id:info.leadId,
			lead_last_updated:info.leadLastUpdated,
			form_object:{
				lead_id:info.leadId,
				lead_last_updated:info.leadLastUpdated,
				scheduled_activity_id:info.scheduledActivityId	
			}
		}
		if(previousFormAnswers != null) {
			bookingObj.form_object.action_id = info.actionId;
			bookingObj.form_object.form_response = this.getServerFormResponse(previousFormAnswers)
		}
		this.setState({isLoading:true})
		switch(action.name) {
			case 'book_re' :
				//RE Booking Process should start here
				new RestClient().bookBike(bookingObj).then((data) => {
					if(data && data.statusCode == "2002") {
						this.onSuccessFormSubmission("Successfully Booked");
						
					}
					else {
						this.showAlert("Failed to confirm renewel");
						this.setState({ isLoading: false})    
					}
					     
					}).catch(error => {
						console.error(error);
						this.showAlert("Failed to confirm renewel");
						this.setState({ isLoading: false});
				});
				break;
			case 'il_renewed' :
				//IL Renewed Process
				new RestClient().ilBookRenewel(bookingObj).then((data) => {
					if(data && data.statusCode == "2002") {
						this.onSuccessFormSubmission("Successfully Renewed");
						
					}
					else {
						this.showAlert("Failed to confirm renewel");
						this.setState({ isLoading: false})    
					}
					     
					}).catch(error => {
						console.error(error);
						this.showAlert("Failed to confirm renewel");
						this.setState({ isLoading: false});
				});
				break;
			case 'sales_confirmed_il':
				//sales confirmed api call
				bookingObj.tenure = payload;
				this.postSalesConfirmed(bookingObj);
				break;
			case 'sales_confirmed_il_bank':
				//sales confirmed api call for ilbank
				this.postSalesConfirmed(bookingObj);
				break;
			case 'send_to_verification':
				//send to verification api call
				bookingObj.proposal_id = payload.proposalId;
				bookingObj.lang_code   = payload.selectedLanguage;
				new RestClient().ilSendToVerification(bookingObj).then((data) => {
					if(data && data.statusCode == "2002") {
						this.onSuccessFormSubmission("Sent to verification");
						
					}
					else {
						this.showAlert("Failed to confirm verification");
						this.setState({ isLoading: false})    
					}
					     
					}).catch(error => {
						console.error(error);
						this.showAlert("Failed to confirm verification");
						this.setState({ isLoading: false});
				});
				break;
		}
	}
	showAlert(message) {
        try{
            this.refs.toast.show(message);
        }
        catch(e) {

        }
	}
	onSuccessFormSubmission(message) {
		this.setState({	isFormSubmitSuccess: true, successMessage:message, isLoading:false});
		setTimeout(
		  function () {
			//redirect
			this.setState({	isFormSubmitSuccess: false});
			if(Platform.OS == "ios") {
				NavigationService.resetScreen({
					routeName: "Home",
					action: NavigationActions.navigate({ routeName: "MyTasks" })
				});
			}
			else if(Platform.OS == "android"){
				//redirect to specific activity;
			}
		  }.bind(this),
		  1000
		);
	}
    render() {
		let {action} = this.state;
        return(
			<View style={styles.main}>
				{(action.name == "book_re" 
				|| action.name =="il_renewed" 
				|| action.name=="sales_confirmed_il"
				|| action.name == "sales_confirmed_il_bank" )&& (
					<GenericFormDialog
					title={action.title}
					showTenure = {action.name=="sales_confirmed_il"}
					onCancel={()=> {
						this.props.navigation.goBack(null)
					}}
					onConfirm={(payload)=> {
						this.submit(action, payload);
					}}
				/>
				)}
				{action.name =="send_to_verification" &&(
					<VerificationLanguagePicker
					onCancel={()=> {
						this.props.navigation.goBack(null)
					}}
					onConfirm={(payload)=> {
						this.submit(action, payload);
					}}/>
				)}
				
				{this.state.isLoading && <Loader lightMode={false} isLoading={this.state.isLoading} />}
				 <Toast
                    ref="toast"
                     style={{backgroundColor:'red'}}
                     fadeInDuration={750}
                     fadeOutDuration={750}
                     opacity={0.8}
                     textStyle={{color:'white'}}
                 />
				   {this.state.isFormSubmitSuccess && (
					<SuccessView text={this.state.successMessage} />
					)}
            </View>
        )
    }
}