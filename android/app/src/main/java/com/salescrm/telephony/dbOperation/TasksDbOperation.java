package com.salescrm.telephony.dbOperation;

import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.utils.Util;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by bharath on 20/9/17.
 */

public class TasksDbOperation {
    public static final TasksDbOperation instance = new TasksDbOperation();

    public static TasksDbOperation getInstance() {
        return instance;
    }

    /**
     *
     * @param realm
     * @param activeTaskIds
     * @param isCreatedTemporary
     * @param dseId
     */
    public void refreshTasks(Realm realm, Integer[] activeTaskIds, boolean isCreatedTemporary, Integer dseId){
        String TAG ="TaskDbOperation:";
        if(realm == null || realm.isInTransaction() || activeTaskIds==null){
            return;
        }
        realm.beginTransaction();
        System.out.println(TAG+activeTaskIds);
        RealmQuery<SalesCRMRealmTable> realmQuery = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemporary)
                .equalTo("isDone", false)
                .equalTo("dseId", dseId)
                .equalTo("createdOffline", false);

        if(activeTaskIds.length !=0) {
            realmQuery = realmQuery.not().in("scheduledActivityId", activeTaskIds);
        }
        realmQuery.findAll().deleteAllFromRealm();

        realm.commitTransaction();

    }

    public void updateLeadLastUpdated(Realm realm, String leadId, String leadLastUpdated){
        if(realm == null || realm.isInTransaction() || leadLastUpdated==null || leadId == null){
            return;
        }
        realm.beginTransaction();
        RealmResults<SalesCRMRealmTable> salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).equalTo("leadId", Long.parseLong(leadId)).findAll();
        if(salesCRMRealmTable != null) {
            for(SalesCRMRealmTable table: salesCRMRealmTable){
                System.out.println("Update Correction: "+salesCRMRealmTable+", "+leadLastUpdated);
                table.setLeadLastUpdated(leadLastUpdated);
            }
        }
        realm.commitTransaction();
    }
}
