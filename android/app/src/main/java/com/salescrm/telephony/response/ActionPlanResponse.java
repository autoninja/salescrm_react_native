package com.salescrm.telephony.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ravindra P on 27-06-2016.
 */
public class ActionPlanResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private String total;

        private String page;

        private String page_limit;

        private String total_pages;

        private List<Data> data;

        @SerializedName("scheduled_activity_ids")
        private Integer[] activeTaskIds;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getPage() {
            return page;
        }

        public void setPage(String page) {
            this.page = page;
        }

        public String getPage_limit() {
            return page_limit;
        }

        public void setPage_limit(String page_limit) {
            this.page_limit = page_limit;
        }

        public String getTotal_pages() {
            return total_pages;
        }

        public void setTotal_pages(String total_pages) {
            this.total_pages = total_pages;
        }

        public List<Data> getData() {
            return data;
        }

        public Integer[] getActiveTaskIds() {
            return activeTaskIds;
        }

        public void setActiveTaskIds(Integer[] activeTaskIds) {
            this.activeTaskIds = activeTaskIds;
        }

        public void setData(List<Data> data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return "ClassPojo [total = " + total + ", page = " + page + ", page_limit = " + page_limit + ", total_pages = " + total_pages + ", data = " + data + "]";
        }


        public class Data {

            private boolean template_exist;

            private Lead lead;

            private Customer customer;

            private Lead_car lead_car;

            private Activity activity;

            public boolean getTemplate_exist() {
                return template_exist;
            }

            public void setTemplate_exist(boolean template_exist) {
                this.template_exist = template_exist;
            }

            public Lead getLead() {
                return lead;
            }

            public void setLead(Lead lead) {
                this.lead = lead;
            }

            public Customer getCustomer() {
                return customer;
            }

            public void setCustomer(Customer customer) {
                this.customer = customer;
            }

            public Lead_car getLead_car() {
                return lead_car;
            }

            public void setLead_car(Lead_car lead_car) {
                this.lead_car = lead_car;
            }


            public Activity getActivity() {
                return activity;
            }

            public void setActivity(Activity activity) {
                this.activity = activity;
            }

            @Override
            public String toString() {
                return "ClassPojo [lead = " + lead + ", customer = " + customer + ", lead_car = " + lead_car + ", activity_type = " + ", activity = " + activity + "]";
            }

            public class Lead {
                private Lead_tags lead_tags;

                private String lead_last_updated;

                private DseAlloted dseAlloted;

                private String lead_source_name;

                private String ref_vin_reg_no;

                private String lead_source_id;

                private Lead_stage lead_stage;

                private String lead_age;

                private String lead_id;
                private String expected_closing_date;
                private Integer taskStatus;
                private String location_name;

                public Lead_tags getLead_tags() {
                    return lead_tags;
                }

                public void setLead_tags(Lead_tags lead_tags) {
                    this.lead_tags = lead_tags;
                }

                public String getLead_last_updated() {
                    return lead_last_updated;
                }

                public void setLead_last_updated(String lead_last_updated) {
                    this.lead_last_updated = lead_last_updated;
                }

                public DseAlloted getDseAlloted() {
                    return dseAlloted;
                }

                public void setDseAlloted(DseAlloted dseAlloted) {
                    this.dseAlloted = dseAlloted;
                }

                public String getRef_vin_reg_no() {
                    return ref_vin_reg_no;
                }

                public void setRef_vin_reg_no(String ref_vin_reg_no) {
                    this.ref_vin_reg_no = ref_vin_reg_no;
                }

                public String getLead_source_name() {
                    return lead_source_name;
                }

                public void setLead_source_name(String lead_source_name) {
                    this.lead_source_name = lead_source_name;
                }

                public String getLead_source_id() {
                    return lead_source_id;
                }

                public void setLead_source_id(String lead_source_id) {
                    this.lead_source_id = lead_source_id;
                }

                public Lead_stage getLead_stage() {
                    return lead_stage;
                }

                public void setLead_stage(Lead_stage lead_stage) {
                    this.lead_stage = lead_stage;
                }

                public String getLead_age() {
                    return lead_age;
                }

                public void setLead_age(String lead_age) {
                    this.lead_age = lead_age;
                }

                public String getLead_id() {
                    return lead_id;
                }

                public void setLead_id(String lead_id) {
                    this.lead_id = lead_id;
                }

                public String getExpected_closing_date() {
                    return expected_closing_date;
                }

                public void setExpected_closing_date(String expected_closing_date) {
                    this.expected_closing_date = expected_closing_date;
                }

                public String getLocation_name() {
                    return location_name;
                }

                public void setLocation_name(String location_name) {
                    this.location_name = location_name;
                }

                @Override
                public String toString() {
                    return "ClassPojo [lead_tags = " + lead_tags + ", lead_last_updated = " + lead_last_updated + ", dseAlloted = " + dseAlloted + ", lead_source_name = " + lead_source_name + ", lead_stage = " + lead_stage + ", lead_age = " + lead_age + ", lead_id = " + lead_id + ", expected_closing_date = " + expected_closing_date + "]";
                }

                public Integer getTaskStatus() {
                    return taskStatus;
                }

                public void setTaskStatus(Integer taskStatus) {
                    this.taskStatus = taskStatus;
                }

                public class Lead_tags {
                    private String tag_names;

                    private String tag_colors;

                    public String getTag_names() {
                        return tag_names;
                    }

                    public void setTag_names(String tag_names) {
                        this.tag_names = tag_names;
                    }

                    public String getTag_colors() {
                        return tag_colors;
                    }

                    public void setTag_colors(String tag_colors) {
                        this.tag_colors = tag_colors;
                    }

                    @Override
                    public String toString() {
                        return "ClassPojo [tag_names = " + tag_names + ", tag_colors = " + tag_colors + "]";
                    }
                }

                public class DseAlloted {
                    private String dseName;

                    private String dseMobNo;

                    public String getDseName() {
                        return dseName;
                    }

                    public void setDseName(String dseName) {
                        this.dseName = dseName;
                    }

                    public String getDseMobNo() {
                        return dseMobNo;
                    }

                    public void setDseMobNo(String dseMobNo) {
                        this.dseMobNo = dseMobNo;
                    }

                    @Override
                    public String toString() {
                        return "ClassPojo [dseName = " + dseName + ", dseMobNo = " + dseMobNo + "]";
                    }
                }

                public class Lead_stage {
                    private String id;

                    private String stage;

                    public String getId() {
                        return id;
                    }

                    public void setId(String id) {
                        this.id = id;
                    }

                    public String getStage() {
                        return stage;
                    }

                    public void setStage(String stage) {
                        this.stage = stage;
                    }

                    @Override
                    public String toString() {
                        return "ClassPojo [id = " + id + ", stage = " + stage + "]";
                    }
                }

            }

            public class Customer {
                private String mobile_number;

                private String email;

                private String gender;

                private String title;

                private String name;

                private String first_name;

                private String last_name;

                private String customer_address;

                private String residence_pin_code;

                private String office_address;

                private String office_pin_code;

                private String age;

                private String customer_id;

                private String company_name;

                private List<Phone_nos> mobile_nos;

                private String secondary_mobile_number;

                public String getSecondary_mobile_number() {
                    return secondary_mobile_number;
                }

                public void setSecondary_mobile_number(String secondary_mobile_number) {
                    this.secondary_mobile_number = secondary_mobile_number;
                }

                public String getCompany_name() {
                    return company_name;
                }

                public void setCompany_name(String company_name) {
                    this.company_name = company_name;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getFirst_name() {
                    return first_name;
                }

                public void setFirst_name(String first_name) {
                    this.first_name = first_name;
                }

                public String getLast_name() {
                    return last_name;
                }

                public void setLast_name(String last_name) {
                    this.last_name = last_name;
                }

                public String getCustomer_address() {
                    return customer_address;
                }

                public void setCustomer_address(String customer_address) {
                    this.customer_address = customer_address;
                }

                public String getResidence_pin_code() {
                    return residence_pin_code;
                }

                public void setResidence_pin_code(String residence_pin_code) {
                    this.residence_pin_code = residence_pin_code;
                }

                public String getOffice_address() {
                    return office_address;
                }

                public void setOffice_address(String office_address) {
                    this.office_address = office_address;
                }

                public String getOffice_pin_code() {
                    return office_pin_code;
                }

                public void setOffice_pin_code(String office_pin_code) {
                    this.office_pin_code = office_pin_code;
                }

                public String getAge() {
                    return age;
                }

                public void setAge(String age) {
                    this.age = age;
                }

                public String getMobile_number() {
                    return mobile_number;
                }

                public void setMobile_number(String mobile_number) {
                    this.mobile_number = mobile_number;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getCustomer_id() {
                    return customer_id;
                }

                public void setCustomer_id(String customer_id) {
                    this.customer_id = customer_id;
                }

                public List<Phone_nos> getMobile_nos()
                {
                    return mobile_nos;
                }

                public void setMobile_nos(List<Phone_nos> mobile_nos)
                {
                    this.mobile_nos = mobile_nos;
                }

                @Override
                public String toString() {
                    return "ClassPojo [mobile_number = " + mobile_number + ", email = " + email + ", name = " + name + ", customer_id = " + customer_id + "]";
                }

                public class Phone_nos
                {
                    private String id;

                    private String status;

                    private String number;

                    private String lead_phone_mapping_id;

                    public String getLead_phone_mapping_id() {
                        return lead_phone_mapping_id;
                    }

                    public void setLead_phone_mapping_id(String lead_phone_mapping_id) {
                        this.lead_phone_mapping_id = lead_phone_mapping_id;
                    }

                    public String getId ()
                    {
                        return id;
                    }

                    public void setId (String id)
                    {
                        this.id = id;
                    }

                    public String getStatus ()
                    {
                        return status;
                    }

                    public void setStatus (String status)
                    {
                        this.status = status;
                    }

                    public String getNumber ()
                    {
                        return number;
                    }

                    public void setNumber (String number)
                    {
                        this.number = number;
                    }

                    @Override
                    public String toString()
                    {
                        return "ClassPojo [id = "+id+", status = "+status+", number = "+number+"]";
                    }
                }
            }

            public class Lead_car {

                private String variant_name;

                private String model_name;

                private String model_id;


                public String getVariant_name() {
                    return variant_name;
                }

                public void setVariant_name(String variant_name) {
                    this.variant_name = variant_name;
                }

                public String getModel_name() {
                    return model_name;
                }

                public void setModel_name(String model_name) {
                    this.model_name = model_name;
                }

                public String getModel_id() {
                    return model_id;
                }

                public void setModel_id(String model_id) {
                    this.model_id = model_id;
                }

                @Override
                public String toString() {
                    return "ClassPojo [variant_name = " + variant_name + ", model_name = " + model_name + "]";
                }
            }

            public class Activity {
                private String icon_type;

                private String activity_id;

                private String activity_type_id;

                private String activity_scheduled_date;

                private String activity_name;

                private String scheduled_activity_id;

                private String type;

                private String activity_group_id;

                private String activity_description;

                private String activity_creation_date;

                public String getActivity_creation_date() {
                    return activity_creation_date;
                }

                public void setActivity_creation_date(String activity_creation_date) {
                    this.activity_creation_date = activity_creation_date;
                }

                public String getIcon_type() {
                    return icon_type;
                }

                public void setIcon_type(String icon_type) {
                    this.icon_type = icon_type;
                }

                public String getActivity_id() {
                    return activity_id;
                }

                public void setActivity_id(String activity_id) {
                    this.activity_id = activity_id;
                }

                public String getActivity_type_id() {
                    return activity_type_id;
                }

                public void setActivity_type_id(String activity_type_id) {
                    this.activity_type_id = activity_type_id;
                }

                public String getActivity_scheduled_date() {
                    return activity_scheduled_date;
                }

                public void setActivity_scheduled_date(String activity_scheduled_date) {
                    this.activity_scheduled_date = activity_scheduled_date;
                }

                public String getActivity_name() {
                    return activity_name;
                }

                public void setActivity_name(String activity_name) {
                    this.activity_name = activity_name;
                }

                public String getScheduled_activity_id() {
                    return scheduled_activity_id;
                }

                public void setScheduled_activity_id(String scheduled_activity_id) {
                    this.scheduled_activity_id = scheduled_activity_id;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getActivity_group_id() {
                    return activity_group_id;
                }

                public void setActivity_group_id(String activity_group_id) {
                    this.activity_group_id = activity_group_id;
                }

                public String getActivity_description() {
                    return activity_description;
                }

                public void setActivity_description(String activity_description) {
                    this.activity_description = activity_description;
                }

                @Override
                public String toString() {
                    return "ClassPojo [icon_type = " + icon_type + ", activity_id = " + activity_id + ", activity_type_id = " + activity_type_id + ", activity_scheduled_date = " + activity_scheduled_date + ", activity_name = " + activity_name + ", scheduled_activity_id = " + scheduled_activity_id + ", type = " + type + ", activity_group_id = " + activity_group_id + ", activity_description = " + activity_description + "]";
                }
            }
        }
    }

}