package com.salescrm.telephony.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddLeadActivity;
import com.salescrm.telephony.activity.AddSearchActivity;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.activity.offline.C360ActivityOffline;
import com.salescrm.telephony.adapter.AddSearchAdapter;
import com.salescrm.telephony.adapter.AddSearchRealmAdapter;
import com.salescrm.telephony.db.AppConfigDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.model.SearchInputData;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.SearchResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.WSConstants;

import java.util.List;
import java.util.Locale;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bharath on 30/5/16.
 */
public class SearchResultsFragment extends Fragment implements Callback {

    ImageButton imageViewBackToSearch;
    OnEmptySearchResultListener onEmptySearchResultListener;
    SearchResultsNotFoundFragment.OnStartAddLeadActivity onStartAddLeadActivity;
    private Preferences pref = null;
    private RelativeLayout rlSearchResultsLoadingFrame;
    private TextView tvResultsCount, tvToolbarTitle;
    private RecyclerView recyclerViewSearchResult;
    private AddSearchAdapter addSearchAdapter;
    private Realm realm;
    private String mobileNumber, enquiryNumber, customerName, emailId;
    private long leadId;
    private TextView btAddLead, btAddColdVisit;
    private LinearLayout ll_buttons;
    private FrameLayout frameAddColdVisit;
    private boolean uniqueAcrossDealerShip = false;
    private boolean addLeadEnabled = true;


    public static SearchResultsFragment newInstance(SearchInputData searchInputData) {
        SearchResultsFragment fragment = new SearchResultsFragment();
        Bundle args = new Bundle();
        args.putString(WSConstants.SRCMRealDbFields.MOBILE, searchInputData.getMobile());
        args.putString(WSConstants.SRCMRealDbFields.ENQUIRY_NUMBER, searchInputData.getEnqNumber());
        args.putString(WSConstants.SRCMRealDbFields.CUSTOMER_NAME, searchInputData.getCustomerName());
        args.putLong(WSConstants.SRCMRealDbFields.LEAD_ID, searchInputData.getLeadID());
        args.putString(WSConstants.SRCMRealDbFields.CUSTOMER_EMAIL, searchInputData.getEmailId());
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            onEmptySearchResultListener = (OnEmptySearchResultListener) activity;
            onStartAddLeadActivity = (SearchResultsNotFoundFragment.OnStartAddLeadActivity) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        View rootView = inflater.inflate(R.layout.fragment_search_result, container, false);
        imageViewBackToSearch = (ImageButton) rootView.findViewById(R.id.imageViewBackToSearch);
        tvResultsCount = (TextView) rootView.findViewById(R.id.tvSearchResultsCount);
        rlSearchResultsLoadingFrame = (RelativeLayout) rootView.findViewById(R.id.rel_loading_frame);
        tvToolbarTitle = (TextView) rootView.findViewById(R.id.tv_search_results_title);
        btAddLead =  rootView.findViewById(R.id.bt_add_lead);
        btAddColdVisit =  rootView.findViewById(R.id.bt_add_cold_visit);
        ll_buttons = (LinearLayout) rootView.findViewById(R.id.ll_buttons);
        recyclerViewSearchResult = (RecyclerView) rootView.findViewById(R.id.recyclerViewSearchResult);

        uniqueAcrossDealerShip = getUniqueAcrossDealershipValKey();
        addLeadEnabled = DbUtils.isAddLeadEnabled();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewSearchResult.setLayoutManager(mLayoutManager);
        imageViewBackToSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        btAddLead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onStartAddLeadActivity != null) {
                    onStartAddLeadActivity.startAddLeadActivity(true);
                }
            }
        });
        if(DbUtils.isAppConfEnabled(WSConstants.AppConfig.COLD_VISIT_ENABLED)){
            rootView.findViewById(R.id.frame_add_cold_visit).setVisibility(View.VISIBLE);

            btAddColdVisit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onStartAddLeadActivity!=null){
                        onStartAddLeadActivity.startAddColdVisitActivity(true);

                    }
                }
            });

        }
        else {
            rootView.findViewById(R.id.frame_add_cold_visit).setVisibility(View.GONE);
        }
        populateField();

        rlSearchResultsLoadingFrame.setVisibility(View.VISIBLE);
        changeStatusBarColor("#224663");
        getSearchResults();
        return rootView;
    }

    private boolean getUniqueAcrossDealershipValKey() {
        AppConfigDB appConfigDB = realm.where(AppConfigDB.class).equalTo("key", WSConstants.AppConfig.UNIQUE_ACROSS_DEALERSHIP).findFirst();
        if (appConfigDB != null) {
            if (appConfigDB.getValue().equalsIgnoreCase("0")) {
                return false;
            } else if (appConfigDB.getValue().equalsIgnoreCase("1")) {
                return true;
            }
        }
        return false;
    }

    private boolean isShowLeadCompetitor() {
        AppConfigDB appConfigDB = realm.where(AppConfigDB.class).equalTo("key", WSConstants.AppConfig.SHOW_LEAD_COMPETITOR).findFirst();
        if (appConfigDB != null) {
            if (appConfigDB.getValue().equalsIgnoreCase("0")) {
                return false;
            } else if (appConfigDB.getValue().equalsIgnoreCase("1")) {
                return true;
            }
        }
        return false;
    }

    private void populateField() {
        mobileNumber = getArguments().getString(WSConstants.SRCMRealDbFields.MOBILE, "0");
        enquiryNumber = getArguments().getString(WSConstants.SRCMRealDbFields.ENQUIRY_NUMBER, "0");
        customerName = getArguments().getString(WSConstants.SRCMRealDbFields.CUSTOMER_NAME, "0");
        leadId = getArguments().getLong(WSConstants.SRCMRealDbFields.LEAD_ID, -1);
        emailId = getArguments().getString(WSConstants.SRCMRealDbFields.CUSTOMER_EMAIL, "0");

    }

    public void changeStatusBarColor(String color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }

    public void getSearchResults() {
        ConnectionDetectorService cd = new ConnectionDetectorService(getActivity());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).
                    GetSearchLead(mobileNumber, enquiryNumber, customerName, String.valueOf(leadId), emailId, "true", this);
        } else {
            OrderedRealmCollection<SalesCRMRealmTable> realmResult = realm.where(SalesCRMRealmTable.class)
                    .equalTo(WSConstants.SRCMRealDbFields.MOBILE, mobileNumber)
                    .or()
                    .equalTo(WSConstants.SRCMRealDbFields.ENQUIRY_NUMBER, enquiryNumber)
                    .or()
                    .contains(WSConstants.SRCMRealDbFields.FIRST_NAME, customerName)
                    .or()
                    .contains(WSConstants.SRCMRealDbFields.LAST_NAME, customerName)
                    .or()
                    .equalTo(WSConstants.SRCMRealDbFields.LEAD_ID, leadId)
                    .or()
                    .equalTo(WSConstants.SRCMRealDbFields.CUSTOMER_EMAIL, emailId)
                    .findAll();
            rlSearchResultsLoadingFrame.setVisibility(View.GONE);
            tvToolbarTitle.setText(getString(R.string.results));
            //Change back to Primary Color
            changeStatusBarColor("#2e5e86");
            if (realmResult.size() > 0) {
                if (realmResult.size() == 1) {
                    if (realmResult.get(0).isCreatedOffline()) {
                        Intent intent = new Intent(getContext(), C360ActivityOffline.class);
                        intent.putExtra("scheduled_activity_id", realmResult.get(0).getScheduledActivityId());
                        getActivity().startActivity(intent);
                    } else {
                        pref.setLeadID("" + realmResult.get(0).getLeadId());
                        Intent filterActivity = new Intent(getContext(), C360Activity.class);
                        filterActivity.putExtra("customerID", "" + realmResult.get(0).getCustomerId());
                        getActivity().startActivity(filterActivity);
                        addLeadVisibility(realmResult);

                    }

                }
                recyclerViewSearchResult.setAdapter(new AddSearchRealmAdapter((AddSearchActivity) getActivity(), realmResult));
                tvResultsCount.setText(String.format(Locale.getDefault(), "%d results found", realmResult.size()));
                tvResultsCount.setVisibility(View.VISIBLE);
            } else {
                onEmptySearchResultListener.isEmptySearchResult(true);
            }
        }
    }

    private void addLeadVisibility(OrderedRealmCollection<SalesCRMRealmTable> realmResult) {
        if(!addLeadEnabled) {
            ll_buttons.setVisibility(View.GONE);
            return;
        }
        if (!pref.getAddSearchNumber().equalsIgnoreCase("")) {
            for (int i = 0; i < realmResult.size(); i++) {
                SalesCRMRealmTable current = realmResult.get(i);
                if (current.getIsLeadActive() == 0) {
                    if(!mobileNumber.equalsIgnoreCase("") && !mobileNumber.equalsIgnoreCase("0") && !mobileNumber.equalsIgnoreCase("-1"))
                        ll_buttons.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    }

    private void addLeadVisibility(List<SearchResponse.Result> result) {
        if(!addLeadEnabled) {
            ll_buttons.setVisibility(View.GONE);
            return;
        }
        if (!pref.getAddSearchNumber().equalsIgnoreCase("")) {
            for (int i = 0; i < result.size(); i++) {
                SearchResponse.Result current = result.get(i);
                if (current.getActive().equalsIgnoreCase("0")) {
                    if(!mobileNumber.equalsIgnoreCase("") && !mobileNumber.equalsIgnoreCase("0") && !mobileNumber.equalsIgnoreCase("-1"))
                        ll_buttons.setVisibility(View.VISIBLE);

                } else {
                    if (current.getStatus().equalsIgnoreCase("PRIMARY")) {
                        if (!uniqueAcrossDealerShip && !current.isMy_location_lead()) {
                            if(!mobileNumber.equalsIgnoreCase("") && !mobileNumber.equalsIgnoreCase("0") && !mobileNumber.equalsIgnoreCase("-1"))
                                ll_buttons.setVisibility(View.VISIBLE);
                        } else {
                            ll_buttons.setVisibility(View.GONE);
                            break;
                        }
                    } else {
                        if(!mobileNumber.equalsIgnoreCase("") && !mobileNumber.equalsIgnoreCase("0") && !mobileNumber.equalsIgnoreCase("-1"))
                            ll_buttons.setVisibility(View.VISIBLE);
                    }

                }
            }
        }
    }

    @Override
    public void success(Object o, Response response) {
        if (getActivity() == null) {
            // new OnFailureGetToken(getContext(),pref.getUserName(),pref.getPassword(),"").callGetToken();
            return;

        }
        List<Header> headerList = response.getHeaders();
        for (Header header : headerList) {
            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                ApiUtil.UpdateAccessToken(header.getValue());
            }
        }
        SearchResponse searchResponse = (SearchResponse) o;
        rlSearchResultsLoadingFrame.setVisibility(View.GONE);
        tvToolbarTitle.setText(getString(R.string.results));

        if(searchResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
            ApiUtil.InvalidUserLogout(getActivity(),0);
        }

            //Change back to Primary Color
        changeStatusBarColor("#2e5e86");
        if (searchResponse.getStatusCode().equals(WSConstants.RESPONSE_OK) && searchResponse.getResult() != null && searchResponse.getResult().size() > 0) {
            tvResultsCount.setText(String.format(Locale.getDefault(), "%d results found", searchResponse.getResult().size()));
            tvResultsCount.setVisibility(View.VISIBLE);
            String user_id = "";
            UserDetails userData = realm.where(UserDetails.class).findFirst();
            if (userData != null) {
                user_id = userData.getUserId() + "";
            }
            System.out.println("Real User id:" + realm.where(UserDetails.class).findFirst().getUserId());
            addSearchAdapter = new AddSearchAdapter(getContext(), searchResponse.getResult(), user_id, null);
            addLeadVisibility(searchResponse.getResult());
            if (searchResponse.getResult().size() == 1) {
                if (searchResponse.getResult().get(0).is_view_allowed()
                        && searchResponse.getResult().get(0).getStatus().equalsIgnoreCase("PRIMARY")) {
                    pref.setLeadID("" + searchResponse.getResult().get(0).getLeadId());
                    Intent intent = new Intent(getContext(), C360Activity.class);
                    intent.putExtra("online_search_data", searchResponse.getResult().get(0));
                    getActivity().startActivity(intent);
                    if (!uniqueAcrossDealerShip && !searchResponse.getResult().get(0).isMy_location_lead()) {
                        if(addLeadEnabled) {
                            if(!mobileNumber.equalsIgnoreCase("") && !mobileNumber.equalsIgnoreCase("0") && !mobileNumber.equalsIgnoreCase("-1"))
                                ll_buttons.setVisibility(View.VISIBLE);
                        }
                        if(!isShowLeadCompetitor()) {
                            onEmptySearchResultListener.isEmptySearchResult(true);
                        }
                    }
                } else if (searchResponse.getResult().get(0).getStatus().equalsIgnoreCase("SECONDARY")) {
                    if(addLeadEnabled) {
                        if(!mobileNumber.equalsIgnoreCase("") && !mobileNumber.equalsIgnoreCase("0") && !mobileNumber.equalsIgnoreCase("-1"))
                            ll_buttons.setVisibility(View.VISIBLE);
                    }
                } else if (!uniqueAcrossDealerShip && !searchResponse.getResult().get(0).isMy_location_lead()) {
                    if(addLeadEnabled) {
                        if(!mobileNumber.equalsIgnoreCase("") && !mobileNumber.equalsIgnoreCase("0") && !mobileNumber.equalsIgnoreCase("-1"))
                            ll_buttons.setVisibility(View.VISIBLE);
                    }
                    if(!isShowLeadCompetitor()) {
                        onEmptySearchResultListener.isEmptySearchResult(true);
                    }
                }
            }

            populateDateOnView();

        }else {
            //Show add util
            onEmptySearchResultListener.isEmptySearchResult(true);
        }


        System.out.println(getClass() + " success:" + o.toString());
    }

    @Override
    public void failure(RetrofitError error) {
        if (getActivity() == null) {
            //new OnFailureGetToken(getContext(),pref.getUserName(),pref.getPassword(),"").callGetToken();
            return;
        }
        System.out.println(getClass() + " error:" + error);
        onEmptySearchResultListener.isEmptySearchResult(false);

    }


    private void populateDateOnView() {
        recyclerViewSearchResult.setAdapter(addSearchAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    public interface OnEmptySearchResultListener {
        void isEmptySearchResult(boolean val);
    }

}
