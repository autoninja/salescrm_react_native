package com.salescrm.telephony.model.booking;

import com.salescrm.telephony.views.booking.CarBookingForm;

public class CarBookingFormHolderModel {
    private CarBookingForm carBookingForm;
    private BookingMainModel bookingMainModel;
    private boolean isSameAsBookingOne;

    public CarBookingForm getCarBookingForm() {
        return carBookingForm;
    }

    public void setCarBookingForm(CarBookingForm carBookingForm) {
        this.carBookingForm = carBookingForm;
    }

    public BookingMainModel getBookingMainModel() {
        return bookingMainModel;
    }

    public void setBookingMainModel(BookingMainModel bookingMainModel) {
        this.bookingMainModel = bookingMainModel;
    }

    public boolean isSameAsBookingOne() {
        return isSameAsBookingOne;
    }

    public void setSameAsBookingOne(boolean sameAsBookingOne) {
        isSameAsBookingOne = sameAsBookingOne;
    }
}
