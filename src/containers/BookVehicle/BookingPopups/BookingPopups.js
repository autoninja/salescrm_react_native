import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import NavigationService from '../../../navigation/NavigationService';
import { BOOKING_ROUTES } from '../BookingUtils/BookingUtils'
let styles = StyleSheet.create({
    linearGradient: {
        padding: 16,
        paddingBottom: 24,
        borderRadius: 10,
    },
    main: {
        backgroundColor: '#000000',
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 80,
        paddingBottom: 80,
        flex: 1,
        justifyContent: 'center'

    },
    header: {
        flexDirection: "row",
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    title: {
        flex: 1,
        padding: 6,
        color: "#eeeeee",
        fontSize: 18,
        textAlign: 'center',
    },
    closeButton: {
        position: "absolute",
        right: 8,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
        height: 24,
        width: 24,
        borderRadius: 14
    },
    vehicleCountButton: {
        backgroundColor: '#007fff',
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
        height: 20,
        width: 20,
        borderRadius: 20
    },


    mainActionText: {
        textAlign: 'center',
        color: '#fff', fontSize: 16,
        marginTop: 20,
        flexWrap: 'wrap', textTransform: 'uppercase'
    },

});

export default class BookingPopups extends Component {
    constructor(props) {
        super(props);
        this._userClickExit = false;
    }

    getRichData() {
        if (this.props.popupData) {
            return this.props.popupData;
        }
        return {
            title: "Title",
        }
    }
    onPressYesButton() {
        let data = this.getRichData();
        if (data.yesAction == "booking") {
            NavigationService.resetScreen(BOOKING_ROUTES.BookVehicleMain, {}, this.props.navigation);
        }
        else if (data.yesAction == "total_selection") {
            NavigationService.resetScreen(BOOKING_ROUTES.TotalBookingSelector, {}, this.props.navigation);
        }
        else {
            //This should not be a case
            this.props.callbacks.goBack();
        }
    }
    onPressNoButton() {
        let data = this.getRichData();
        if (data.noAction == "reschedule") {
            NavigationService.resetScreen(BOOKING_ROUTES.Submission, { reschedule: true }, this.props.navigation)
        }
        else {
            //Exit using parent navigation
            this.props.callbacks.goBack();
        }
    }

    render() {
        let { title } = this.getRichData();
        return (
            <View style={styles.main}>
                <LinearGradient
                    colors={["#2e5e86", "#2e3486"]}
                    style={styles.linearGradient}>
                    <View style={{ flexWrap: 'wrap' }}>

                        <View style={styles.header}>
                            <Text style={styles.title}>{title}</Text>
                        </View>

                        <View style={{
                            backgroundColor: '#4C6794',
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 16,
                            borderRadius: 6,
                        }}>
                            <View style={{ flex: 1, paddingTop: 36, paddingBottom: 36 }}>
                                <TouchableOpacity
                                    onPress={() => this.onPressYesButton()}
                                    style={{
                                        justifyContent: 'center', alignItems: 'center'
                                    }}
                                >
                                    <Image
                                        source={require('../../../images/ic_check_mark_white_no_background.png')}
                                        style={{ width: 20, height: 20 }}
                                        resizeMode={"contain"}
                                    />
                                    <Text style={styles.mainActionText}>Yes</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: '100%', width: 0.6, backgroundColor: 'rgba(255,255,255,0.2)' }}></View>
                            <View style={{ flex: 1, paddingTop: 36, paddingBottom: 36 }}>
                                <TouchableOpacity
                                    onPress={() => this.onPressNoButton()}
                                    style={{
                                        justifyContent: 'center', alignItems: 'center'
                                    }}
                                >
                                    <Image
                                        source={require('../../../images/ic_close_white.png')}
                                        style={{ width: 20, height: 20 }}
                                        resizeMode={"contain"}
                                    />
                                    <Text style={styles.mainActionText}>No</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>
                </LinearGradient>

            </View>
        )
    }
}