package com.salescrm.telephony.db.team_dashboard_gm_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 13/7/17.
 */

public class ResultSM extends RealmObject{

    @SerializedName("sm_id")
    @Expose
    private Integer smId;
    @SerializedName("sm_username")
    @Expose
    private String smUsername;
    @SerializedName("sm_dp_url")
    @Expose
    private String smDpUrl;
    @SerializedName("teams")
    @Expose
    public RealmList<TeamSM> teams = new RealmList<>();

    @SerializedName("abs_total")
    @Expose
    private AbsTotalSm absTotal;
    @SerializedName("rel_total")
    @Expose
    private RelTotalSm relTotal;

    public AbsTotalSm getAbsTotal() {
        return absTotal;
    }

    public void setAbsTotal(AbsTotalSm absTotal) {
        this.absTotal = absTotal;
    }

    public RelTotalSm getRelTotal() {
        return relTotal;
    }

    public void setRelTotal(RelTotalSm relTotal) {
        this.relTotal = relTotal;
    }

    public Integer getSmId() {
        return smId;
    }

    public void setSmId(Integer smId) {
        this.smId = smId;
    }

    public String getSmUsername() {
        return smUsername;
    }

    public void setSmUsername(String smUsername) {
        this.smUsername = smUsername;
    }

    public String getSmDpUrl() {
        return smDpUrl;
    }

    public void setSmDpUrl(String smDpUrl) {
        this.smDpUrl = smDpUrl;
    }

    public RealmList<TeamSM> getTeams() {
        return teams;
    }

    public void setTeams(RealmList<TeamSM> teams) {
        this.teams = teams;
    }

}
