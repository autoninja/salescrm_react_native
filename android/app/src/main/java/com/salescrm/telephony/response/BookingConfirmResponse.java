package com.salescrm.telephony.response;

/**
 * Created by bharath on 18/1/18.
 */

public class BookingConfirmResponse {
    private String message;

    private String statusCode;

    private Result result;

    private Error error;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public Result getResult() {
        return result;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public class Result {
        private Integer action_id;
        private String lead_last_updated;

        public Integer getAction_id() {
            return action_id;
        }

        public void setAction_id(Integer action_id) {
            this.action_id = action_id;
        }

        public String getLead_last_updated() {
            return lead_last_updated;
        }

        public void setLead_last_updated(String lead_last_updated) {
            this.lead_last_updated = lead_last_updated;
        }
    }
}