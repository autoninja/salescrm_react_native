import React, { Component } from 'react'
import { View, Text, Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Alert } from 'react-native'

import CallStat from './call_stat'

export default class SalesILomItem extends Component {
  constructor(props) {
    super(props);
    this.state = { showImagePlaceHolder: true };
  }
  _onLongPressButton() {
    console.log('Long press worked');
    Alert.alert('You long-pressed the button!')
  }
  render() {
    let imageSource;
    let placeHolder
    if (this.props.from_location) {
      imageSource = require('../../images/ic_location.png');
      imageStyle = { position: 'absolute', width: 36, height: 36, borderRadius: 36 / 2, };
      placeHolder = null;

    }
    else {
      imageSource = { uri: this.props.info.dp_url };
      imageStyle = { position: 'absolute', width: 36, height: 36, borderRadius: 36 / 2, };
      placeHolder = <View style={{ position: 'absolute', display: (this.state.showImagePlaceHolder ? 'flex' : 'none'), width: 36, height: 36, borderRadius: 36 / 2, backgroundColor: '#FF8000', alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: '#fff' }}>{this.props.info.name.charAt(0).toUpperCase()}</Text></View>;;

    }

    if (this.props.showPercentage) {
      return (
        <View style={{ marginTop: 10 }}>
          <View style={{ marginTop: 4, marginBottom: 4, flexDirection: 'row', alignItems: 'center' }}>
            <Text style={{ flex: 1, fontSize: 15, color: '#2D4F8B', }}>{this.props.info.name}</Text>
            <CallStat call_stats = {this.props.call_stats}/>
          </View>
          <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#eceff1', alignItems: 'center', borderRadius: 6 }}>



            <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
              {placeHolder}
              <TouchableOpacity
                style={{ position: 'absolute', }}
                onPress={() => this.props.onPress()}
                onLongPress={() => {
                  this.props.onLongPress('user_info',
                    { info: { name: this.props.info.name, dp_url: this.props.info.dp_url } })
                }}>
                <Image
                  source={imageSource}
                  style={{ width: 36, height: 36, borderRadius: 36 / 2, }}
                />
              </TouchableOpacity>
            </View>







            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#303030', }} >{this.props.rel.callback}</Text>
              </View>
            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#303030', }} >{this.props.rel.prospect}</Text>
              </View>


            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#303030', }} >{this.props.rel.sale_confirmed}</Text>
              </View>


            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#303030', }} >{this.props.rel.il_done}</Text>
              </View>

            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
            }}>

              <Text style={{ color: 'red', }} >{this.props.rel.l}</Text>
            </View>


          </View>
        </View>
      );
    }
    else {
      return (
        <View style={{ marginTop: 10 }}>
          <View style={{ marginTop: 4, marginBottom: 4, flexDirection: 'row', alignItems: 'center' }}>
            <Text style={{ flex: 1, fontSize: 15, color: '#2D4F8B', }}>{this.props.info.name}</Text>
            <CallStat call_stats = {this.props.call_stats}/>
          </View>
          <View style={{ flexDirection: 'row', height: 60, backgroundColor: '#eceff1', alignItems: 'center', borderRadius: 6 }}>

            <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
              {placeHolder}
              <TouchableOpacity
                style={{ position: 'absolute', }}
                onPress={() => this.props.onPress()}
                onLongPress={() => {
                  this.props.onLongPress('user_info',
                    { info: { name: this.props.info.name, dp_url: this.props.info.dp_url } })
                }}>
                <Image
                  source={imageSource}
                  style={{ width: 36, height: 36, borderRadius: 36 / 2, }}
                />
              </TouchableOpacity>
            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  onPress={() => this.props.onPress()}
                  onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Assigned', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>
                  <Text style={{
                    color: '#303030', textDecorationLine: 'underline'
                  }} >{this.props.abs.assigned}</Text>
                </TouchableOpacity>
              </View>

            </View>




            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  onPress={() => this.props.onPress()}
                  onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Call back', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                  <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.callback}</Text>
                </TouchableOpacity>
              </View>

            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  onPress={() => this.props.onPress()}
                  onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Prospect', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                  <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.prospect}</Text>
                </TouchableOpacity>
              </View>

            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  onPress={() => this.props.onPress()}
                  onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Sale Confirmed', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                  <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.sale_confirmed}</Text>
                </TouchableOpacity>
              </View>


            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3
            }}>
              <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  onPress={() => this.props.onPress()}
                  onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'IL Done', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                  <Text style={{ color: '#303030', textDecorationLine: 'underline' }} >{this.props.abs.il_done}</Text>
                </TouchableOpacity>
              </View>

            </View>

            <View style={{
              flex: 1, height: '100%', borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3, justifyContent: "center", alignItems: "center"
            }}>
              <TouchableOpacity
                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                onPress={() => this.props.onPress()}
                onLongPress={() => this.props.onLongPress('etvbr_details', { info: { clicked_val: 'Lost / Not Eligible', user_id: this.props.info.user_id, title: this.props.info.name, user_location_id: this.props.info.location_id, user_role_id: this.props.info.user_role_id } })}>

                <Text style={{ color: 'red', textDecorationLine: 'underline' }} >{this.props.abs.l}</Text>
              </TouchableOpacity>
            </View>


          </View>
        </View>
      );
    }
  }
}
