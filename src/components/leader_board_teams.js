import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import { NavigationEvents } from "react-navigation";
import Utils from '../utils/Utils'
import RestClient from '../network/rest_client'
import GamificationConfigurationService from '../storage/gamification_configuration_service'

const img = "https://tineye.com/images/widgets/mona.jpg";

global.date;
import {eventLeaderBoard} from '../clevertap/CleverTapConstants';
import CleverTapPush from '../clevertap/CleverTapPush'

export default class LeaderBoardTeams extends React.Component {

	constructor(props)
		{
		    super(props);
		    this.state = {
		    	isLoading: true,
		    	teams: [],
					isPreviousEnabled : true,
					isNextEnabled : true,
					selectedGameDate:"",
			}

		}

		logClevertap(eventName) {
			CleverTapPush.pushEvent(eventLeaderBoard.event, { 'LeaderBoard Type' : eventName });

		}

		componentDidMount(){
					this.updateDatePicker();
   		   this.makeRemoteRequest();
				 const { navigation } = this.props;
				 this.logClevertap(eventLeaderBoard.keyType.valuesType.TEAM_LEADERBOARD);
  	}

		updateDatePicker() {
			let isNextEnabled = true;
			let isPreviousEnabled = true;
			if(global.selectedGameDate.getMonth() == global.systemDate.getMonth()
			 && global.selectedGameDate.getFullYear() == global.systemDate.getFullYear() ) {
				isNextEnabled = false;
			}
			let gameStartDate = new Date();
			if(Utils.getDate(GamificationConfigurationService.getSelectedGameLocation(selectedGameLocationId))) {
				gameStartDate = Utils.getDate(GamificationConfigurationService.getSelectedGameLocation(selectedGameLocationId).start_date);
			}
			if(global.selectedGameDate.getMonth() == gameStartDate.getMonth()
			 && global.selectedGameDate.getFullYear() == gameStartDate.getFullYear() ) {
				isPreviousEnabled = false;
			}
			let readableDate = Utils.getReadableDateForGame(new Date());
			if(Utils.getReadableDateForGame(global.selectedGameDate)) {
				readableDate = Utils.getReadableDateForGame(global.selectedGameDate);
			}
			this.setState({isNextEnabled:isNextEnabled, isPreviousEnabled : isPreviousEnabled, selectedGameDate :readableDate});

		}

  	makeRemoteRequest = () => {
  			this.setState({ isLoading: true});
  			new RestClient().getLeaderBoardTeams({date:getSelectedGameDate(),locationId:global.selectedGameLocationId}).then( (data) => {
		      if(data.statusCode == 4001 || data.statusCode == 5001) {
		                  Alert.alert(
		                'Failed',
		                'Please try again',
		                [
		                  {text: 'Logout', onPress: () => this.logout()},
		                  {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
		                ],
		                { cancelable: false }
		                )
		                  return;
		                }
		               	let teams = [];
		                if(data.result && data.result.team_leads && data.result.team_leads.length>0) {
		                	teams = data.result.team_leads;
		              	}
		               this.setState({
						    		isLoading: false,
						    		teams:teams
									});
										//console.error(this.state.teams)

		          }).catch((error) => {
		            console.error("Error Team Details"+ error)
		            if (error.response && error.response.status == 401) {
		                Alert.alert(
		              'Failed',
		              'Please try again',
		              [
		                {text: 'Logout', onPress: () => this.logout()},
		                {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
		              ],
		              { cancelable: false }
		            )
		              }
		          });
			}

		moveToPreviousMonth(){
			if(this.state.isPreviousEnabled) {
					var prevDate = global.selectedGameDate;
					prevDate.setMonth(global.selectedGameDate.getMonth()-1)
					global.selectedGameDate = prevDate;
					this.logClevertap(eventLeaderBoard.keyType.valuesType.PREVIOUS_MONTH);
					this.updateDatePicker();
					this.makeRemoteRequest();
			}

		}

		moveToNextMonth(){
			if(this.state.isNextEnabled) {
				var nextDate = global.selectedGameDate;
				nextDate.setMonth(global.selectedGameDate.getMonth()+1)
				global.selectedGameDate = nextDate;
				this.updateDatePicker();
				this.makeRemoteRequest();
			}

		}

		_showUserInfo(navigation, name, dp_url) {
			 navigation.navigate('UserInfo', {
			   name,
			   dp_url,

		 });}

		render () {

			let navigation = this.props.navigation;
			let placeNoData = null;
			if(this.state.teams.length == 0) {
				placeNoData = (<View style={{flex:1, height:'100%', alignItems:'center', justifyContent:'center'}}><Text style={{color:'#fff', alignSelf:'center', fontSize:16}}>No Data</Text></View>) ;
			}

			if (this.state.isLoading) {
		      return (
						<View style = {{display:(this.state.isLoading?'flex':'none'), flex:1,justifyContent: 'center',
 		 				alignItems: 'center',backgroundColor:'#2e5e86'}}>

 		 			<ActivityIndicator
 		 						animating={this.state.isLoading}
 		 						style={{flex: 1,   alignSelf:'center',}}
 		 						color="#fff"
 		 						size="large"
 		 						hidesWhenStopped={true}

 		 			/>

 		 			</View>
		      );
   			 }

		return (
				<View style = {{flex: 1, backgroundColor: '#2e5e86'}}>
				<NavigationEvents
          onWillFocus={payload => {
						this.logClevertap(eventLeaderBoard.keyType.valuesType.TEAM_LEADERBOARD);
						this.updateDatePicker();
            this.makeRemoteRequest();
          }}
        />
					<View style={{width: '100%',flexDirection: 'row', padding: 5, alignItems: 'center', justifyContent: 'center', margin: 10}}>
						<TouchableOpacity onPress={()=>this.moveToPreviousMonth()}>
							<Image
								        style={{ width: 12, height: 15,}}
			                          	source={require('../images/ic_left_arrow.png')}
								        />
						</TouchableOpacity>

								<Text style= {{color: '#fff', fontSize: 18, margin: 5}}> {this.state.selectedGameDate} </Text>

						<TouchableOpacity onPress={()=>this.moveToNextMonth()}>
								<Image
							        style={{ width: 12, height: 15,}}
		                          	source={require('../images/ic_right_arrow.png')}
							        />
						</TouchableOpacity>
					</View>

					<View style={{width: '100%', height: 30, alignItems : 'center', backgroundColor:'#566781',justifyContent: 'center', flexDirection: 'row'}}>
					    	<Text style={{flex: 1,  color: '#fff', textAlign: 'center'}}>Team Lead</Text>
					    	<Text style={{flex: 1, 	color: '#fff', textAlign: 'right', marginRight: 15}}>Avg Points</Text>
				    </View>
							{placeNoData}
				    <FlatList
				    	//ItemSeparatorComponent = {this.FlatListItemSeparator}
				    	data={this.state.teams}
						onRefresh={() => this.makeRemoteRequest()}
				      	refreshing={this.state.isLoading}
				    	renderItem= {({item, index}) => {
				    		let placeHolder = <View style={{position:'absolute',width:36, height:36, borderRadius: 36/2, alignItems:'center', backgroundColor: '#FF8000', justifyContent:'center'}}><Text style={{color:'#fff',alignItems: 'center', justifyContent:'center', textAlign: 'center'}}>{item.team_leader_name.charAt(0).toUpperCase()}</Text></View>;
				    		return(
					    		<TouchableOpacity onPress = { () => {
											this.props.navigation.navigate('LeaderBoardTeamsDetails',{item});
											if(item.spotlight == '1') {
												this.logClevertap(eventLeaderBoard.keyType.valuesType.TEAM_BREAK_UP_OWN);
											}
											else {
												this.logClevertap(eventLeaderBoard.keyType.valuesType.TEAM_BREAK_UP_OTHERS);
											}


									}}>
						    		<View style={{flex : 10, height: 50,flexDirection: 'row', alignItems : 'center', justifyContent: 'center', backgroundColor: item.spotlight == '1'?'#384C69':'#0F2B52'}}>
							    		 <View style={{flex : 1, alignItems : 'center', justifyContent: 'center'}}>
							    		 	<Text style={{color: '#fff'}}> {index+1}</Text>
							    		 </View>
							    		 <TouchableOpacity /*onLongPress={this._showUserInfo.bind(this, navigation, item.team_leader_name, item.team_leader_photo_url)}*/>
							    		 {placeHolder}
							    		 <Image
					                          style={{width: 36, height: 36, borderRadius: 36/2, alignItems: 'center'}}
					                          source={{uri: item.team_leader_photo_url}}
					                      />
					                      </TouchableOpacity>
					                      <View style={{flex : 4.5, alignItems : 'flex-start', justifyContent: 'center', paddingLeft: 15}}>
							    		 	<Text style={{color: '#fff', fontSize: 14}}> {item.team_leader_name} </Text>
							    		 </View>

							    		 <View style={{flex:0.5, alignItems: 'center'}} >
							    		 {(item.best_team_ever == '0')? null:
							    		 	<Image
										        style={{ width: 15, height: 15,}}
					                          	source={require('../images/big_trophy.png')}
									        />
									    }
							    		 </View>

							    		 <View style={{flex : 2, alignItems : 'center', justifyContent: 'center'}}>
							    		 	<Text style={{color: '#fff', fontSize: 12}}> {item.team_average_points} </Text>
							    		 </View>

							    		 <Image
									        style={{ width: 10, height: 13, marginRight: 15}}
				                          	source={require('../images/ic_right_arrow.png')}
									        />
						    		</View>
						    		<View
								        style={{
								          height: 0.5,
								          width: "100%",
								          backgroundColor: "#d3d3d3",
								        }}
								      />
					    		</TouchableOpacity>
				    		)
				    		}}
				    	keyExtractor= {(item, index) => index.toString() }
			    	/>
		    	</View>
			);
	}
}

    FlatListItemSeparator = () => {
	    return (
	      <View
	        style={{
	          height: 1,
	          width: "100%",
	          backgroundColor: "#fff",
	        }}
	      />
	    );
  }
