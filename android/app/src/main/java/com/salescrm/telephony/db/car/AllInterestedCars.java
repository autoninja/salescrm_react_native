package com.salescrm.telephony.db.car;

import com.salescrm.telephony.model.UpdateInvoiceModel;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bannhi on 20/6/17.
 */

public class AllInterestedCars extends RealmObject {

    @PrimaryKey
    private String carId;

    private String fuelId;

    private String fuelType;

    private int intrestedLeadCarId;

    private long leadId;

    private String intrestedCarName;

    private String intrestedCarVariantCode;

    private String intrestedCarVariantId;

    private String intrestedCarColorId;

    private String intrestedCarModelId;

    private int intrestedCarIsPrimary;

    private String enquiry_number;

    private String enquiry_id;

    private String booking_number;

    private String location_code;

    private String car_stage_id;

    private int testDriveConducted;

    private String model;

    private String customerName;

    private int isPrimary;

    private String vin_allocation_status_id;

    private String vin_no;

    private String vin_allocation_status;

    /* private String invoice_number;

    private String invoice_id;
*/


    private String booking_id;

    private String expected_delivery_date;

    private String booking_amount;

    private String color;

    private String variant;

    private String deliveryDate;
    private String deliveryChallan;


    private InvoiceDetailsDB invoiceDetails;
    private BookingCustomerDB bookingCustomerDB;
    private BookingPriceBreakupDB bookingPriceBreakupDB;
    private BookingDiscountDB bookingDiscountDB;
    private BookingExchFinDB exchFinDB;
    private BookingAllocationDB bookingAllocationDB;



    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public InvoiceDetailsDB getInvoiceDetails() {
        return invoiceDetails;
    }

    public void setInvoiceDetails(InvoiceDetailsDB invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }

    public int getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(int isPrimary) {
        this.isPrimary = isPrimary;
    }
  /*  public String getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(String invoice_id) {
        this.invoice_id = invoice_id;
    }*/

    public int getTestDriveConducted() {
        return testDriveConducted;
    }

    public void setTestDriveConducted(int testDriveConducted) {
        this.testDriveConducted = testDriveConducted;
    }

    public String getBooking_amount() {
        return booking_amount;
    }

    public void setBooking_amount(String booking_amount) {
        this.booking_amount = booking_amount;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getFuelId() {
        return fuelId;
    }

    public void setFuelId(String fuelId) {
        this.fuelId = fuelId;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public int getIntrestedLeadCarId() {
        return intrestedLeadCarId;
    }

    public void setIntrestedLeadCarId(int intrestedLeadCarId) {
        this.intrestedLeadCarId = intrestedLeadCarId;
    }

    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public String getIntrestedCarName() {
        return intrestedCarName;
    }

    public void setIntrestedCarName(String intrestedCarName) {
        this.intrestedCarName = intrestedCarName;
    }

    public String getIntrestedCarVariantCode() {
        return intrestedCarVariantCode;
    }

    public void setIntrestedCarVariantCode(String intrestedCarVariantCode) {
        this.intrestedCarVariantCode = intrestedCarVariantCode;
    }

    public String getIntrestedCarVariantId() {
        return intrestedCarVariantId;
    }

    public void setIntrestedCarVariantId(String intrestedCarVariantId) {
        this.intrestedCarVariantId = intrestedCarVariantId;
    }

    public String getIntrestedCarColorId() {
        return intrestedCarColorId;
    }

    public void setIntrestedCarColorId(String intrestedCarColorId) {
        this.intrestedCarColorId = intrestedCarColorId;
    }

    public String getIntrestedCarModelId() {
        return intrestedCarModelId;
    }

    public void setIntrestedCarModelId(String intrestedCarModelId) {
        this.intrestedCarModelId = intrestedCarModelId;
    }

    public int getIntrestedCarIsPrimary() {
        return intrestedCarIsPrimary;
    }

    public void setIntrestedCarIsPrimary(int intrestedCarIsPrimary) {
        this.intrestedCarIsPrimary = intrestedCarIsPrimary;
    }

    public String getEnquiry_number() {
        return enquiry_number;
    }

    public void setEnquiry_number(String enquiry_number) {
        this.enquiry_number = enquiry_number;
    }

    public String getEnquiry_id() {
        return enquiry_id;
    }

    public void setEnquiry_id(String enquiry_id) {
        this.enquiry_id = enquiry_id;
    }

    public String getBooking_number() {
        return booking_number;
    }

    public void setBooking_number(String booking_number) {
        this.booking_number = booking_number;
    }

    public String getLocation_code() {
        return location_code;
    }

    public void setLocation_code(String location_code) {
        this.location_code = location_code;
    }

    public String getCar_stage_id() {
        return car_stage_id;
    }

    public void setCar_stage_id(String car_stage_id) {
        this.car_stage_id = car_stage_id;
    }

   /* public String getInvoice_number() {
        return invoice_number;
    }

    public void setInvoice_number(String invoice_number) {
        this.invoice_number = invoice_number;
    }*/

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getExpected_delivery_date() {
        return expected_delivery_date;
    }

    public void setExpected_delivery_date(String expected_delivery_date) {
        this.expected_delivery_date = expected_delivery_date;
    }

    public BookingCustomerDB getBookingCustomerDB() {
        return bookingCustomerDB;
    }

    public void setBookingCustomerDB(BookingCustomerDB bookingCustomerDB) {
        this.bookingCustomerDB = bookingCustomerDB;
    }

    public BookingPriceBreakupDB getBookingPriceBreakupDB() {
        return bookingPriceBreakupDB;
    }

    public void setBookingPriceBreakupDB(BookingPriceBreakupDB bookingPriceBreakupDB) {
        this.bookingPriceBreakupDB = bookingPriceBreakupDB;
    }

    public BookingDiscountDB getBookingDiscountDB() {
        return bookingDiscountDB;
    }

    public void setBookingDiscountDB(BookingDiscountDB bookingDiscountDB) {
        this.bookingDiscountDB = bookingDiscountDB;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryChallan() {
        return deliveryChallan;
    }

    public void setDeliveryChallan(String deliveryChallan) {
        this.deliveryChallan = deliveryChallan;
    }

    public void setExchFinDB(BookingExchFinDB exchFinDB) {
        this.exchFinDB = exchFinDB;
    }

    public BookingExchFinDB getExchFinDB() {
        return exchFinDB;
    }

    public String getVin_allocation_status_id() {
        return vin_allocation_status_id;
    }

    public void setVin_allocation_status_id(String vin_allocation_status_id) {
        this.vin_allocation_status_id = vin_allocation_status_id;
    }

    public String getVin_no() {
        return vin_no;
    }

    public void setVin_no(String vin_no) {
        this.vin_no = vin_no;
    }

    public String getVin_allocation_status() {
        return vin_allocation_status;
    }

    public void setVin_allocation_status(String vin_allocation_status) {
        this.vin_allocation_status = vin_allocation_status;
    }

    public BookingAllocationDB getBookingAllocationDB() {
        return bookingAllocationDB;
    }

    public void setBookingAllocationDB(BookingAllocationDB bookingAllocationDB) {
        this.bookingAllocationDB = bookingAllocationDB;
    }
}
