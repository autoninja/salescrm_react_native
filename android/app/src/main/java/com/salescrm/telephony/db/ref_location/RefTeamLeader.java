package com.salescrm.telephony.db.ref_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by prateek on 8/11/17.
 */

public class RefTeamLeader extends RealmObject {

    @SerializedName("info")
    @Expose
    private RefInfo info;
    @SerializedName("sales_consultants")
    @Expose
    private RealmList<RefSalesConsultant> sales_consultants = null;

    public RefInfo getInfo() {
        return info;
    }

    public void setInfo(RefInfo info) {
        this.info = info;
    }

    public RealmList<RefSalesConsultant> getSalesConsultants() {
        return sales_consultants;
    }

    public void setSalesConsutants(RealmList<RefSalesConsultant> salesConsultants) {
        this.sales_consultants = salesConsultants;
    }
}
