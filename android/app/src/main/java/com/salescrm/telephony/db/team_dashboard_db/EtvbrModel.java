package com.salescrm.telephony.db.team_dashboard_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.salescrm.telephony.model.EtvbrPojo;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 11/6/17.
 */

public class EtvbrModel extends RealmObject {

    /*@SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("error")
    @Expose
    private RealmList<Error> error = null;*/

    @PrimaryKey
    @SerializedName("userId")
    @Expose
    private Integer userId;

    @SerializedName("result")
    @Expose
    public RealmList<Result> result = new RealmList<>();

    /*public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public RealmList<Error> getError() {
        return error;
    }

    public void setError(RealmList<Error> error) {
        this.error = error;
    }*/
    public RealmList<Result> getResult() {
        return result;
    }

    public void setResult(RealmList<Result> result) {
        this.result = result;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
