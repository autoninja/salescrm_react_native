package com.salescrm.telephony.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.interfaces.SelectDateRange;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.squareup.timessquare.CalendarPickerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by prateek on 12/6/17.
 */

public class CalendarDialogActivity extends AppCompatActivity {
    private CalendarPickerView calendar;
    private Button btnDatePicker;
    private Preferences pref;

    @Override
    protected void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_dialog_activity);
        pref = Preferences.getInstance();
        pref.load(this);
        btnDatePicker = (Button) findViewById(R.id.btn_date_picker);
        Calendar lastDay = Calendar.getInstance();
        lastDay.add(Calendar.MONTH, -3);
        lastDay.set(Calendar.DATE, 1);

        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DATE, 1);

        calendar = (CalendarPickerView) findViewById(R.id.calendar_view);
        final Date today = new Date();
        calendar.init(lastDay.getTime(), nextDay.getTime())
                .withSelectedDate(today)
                .inMode(CalendarPickerView.SelectionMode.RANGE);
        // uncomment this if you want to highlight holidays
        //calendar.highlightDates(getHolidays());

        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(new ConnectionDetectorService(CalendarDialogActivity.this).isConnectingToInternet()) {
                    ArrayList<Date> selectedDates = (ArrayList<Date>) calendar.getSelectedDates();
                    Calendar calendar1 = Calendar.getInstance();
                    calendar1.setTime(selectedDates.get(0));

                    Calendar calendar2 = Calendar.getInstance();
                    calendar2.setTime(selectedDates.get(selectedDates.size() - 1));

                    boolean sameMonth = calendar1.get(Calendar.MONTH) == calendar2.get(Calendar.MONTH);

                    if(sameMonth) {
                        System.out.println("SelectedDates are: " + selectedDates.get(0) + " and " + selectedDates.get(selectedDates.size() - 1) + "   aur  " + today);
                        //SalesCRMApplication.getBus().post(new SelectDateRange(selectedDates));
                        SalesCRMApplication.getBus().post(new SelectDateRange(selectedDates));
                        HashMap<String, Object> hashMap = new HashMap<String, Object>();
                        if(calendar1.get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH)) {
                            hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_DATE);
                        }
                        else {
                            hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_PREVIOUS_DATE);
                        }
                        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
                        CalendarDialogActivity.this.finish();
                    }else{
                        Toast.makeText(CalendarDialogActivity.this, "Please select dates of same month", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(CalendarDialogActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private ArrayList<Date> getHolidays(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
        String dateInString = "10-06-2017";
        Date date = null;
        try {
            date = sdf.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ArrayList<Date> holidays = new ArrayList<>();
        holidays.add(date);
        return holidays;
    }
}
