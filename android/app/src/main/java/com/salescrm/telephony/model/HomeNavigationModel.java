package com.salescrm.telephony.model;

import android.util.SparseArray;

/**
 * Created by bharath on 15/2/18.
 */

public class HomeNavigationModel {
    private int id;
    private String title;
    private int icon;
    private boolean isShowSearch;
    private boolean isShowFilter;
    private boolean isShowLocation;
    private boolean isShowBottomBooster;
    private boolean isShowBoosterDaysLeft;
    private SparseArray<Object> payLoad;

    public HomeNavigationModel(int id,
                               String navMenuTitle,
                               int resourceId) {
        this.id = id;
        this.title = navMenuTitle;
        this.icon = resourceId;
    }

    public HomeNavigationModel(int id,
                               String title,
                               int icon,
                               boolean isShowSearch,
                               boolean isShowFilter,
                               boolean isShowLocation) {
        this.id = id;
        this.title = title;
        this.icon = icon;
        this.isShowSearch = isShowSearch;
        this.isShowFilter = isShowFilter;
        this.isShowLocation = isShowLocation;
    }

    public HomeNavigationModel(int id,
                               String title,
                               int icon,
                               boolean isShowSearch,
                               boolean isShowFilter,
                               boolean isShowLocation,
                               boolean isShowBottomBooster,
                               boolean isShowBoosterDaysLeft) {
        this.id = id;
        this.title = title;
        this.icon = icon;
        this.isShowSearch = isShowSearch;
        this.isShowFilter = isShowFilter;
        this.isShowLocation = isShowLocation;
        this.isShowBottomBooster = isShowBottomBooster;
        this.isShowBoosterDaysLeft = isShowBoosterDaysLeft;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public boolean isShowSearch() {
        return isShowSearch;
    }

    public void setShowSearch(boolean showSearch) {
        isShowSearch = showSearch;
    }

    public boolean isShowFilter() {
        return isShowFilter;
    }

    public void setShowFilter(boolean showFilter) {
        isShowFilter = showFilter;
    }

    public boolean isShowLocation() {
        return isShowLocation;
    }

    public void setShowLocation(boolean showLocation) {
        isShowLocation = showLocation;
    }

    public boolean isShowBottomBooster() {
        return isShowBottomBooster;
    }

    public void setShowBottomBooster(boolean showBottomBooster) {
        isShowBottomBooster = showBottomBooster;
    }
    public boolean isShowBoosterDaysLeft() {
        return isShowBoosterDaysLeft;
    }

    public void setShowBoosterDaysLeft(boolean showBoosterDaysLeft) {
        isShowBoosterDaysLeft = showBoosterDaysLeft;
    }

    public SparseArray<Object> getPayLoad() {
        return payLoad;
    }

    public void setPayLoad(SparseArray<Object> payLoad) {
        this.payLoad = payLoad;
    }
}
