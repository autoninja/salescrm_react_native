package com.salescrm.telephony.services;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.salescrm.telephony.db.FCMdata;
import com.salescrm.telephony.interfaces.RestApi;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.FCMTokenRefreshResponse;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.WSConstants;
import java.util.List;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by prateek on 4/11/16.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    public static String FCMToken;
    private Preferences pref;
    private FCMdata fcMdata;

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        // Get updated InstanceID token.
        pref = Preferences.getInstance();
        pref.load(getApplicationContext());
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        pref.setFcmId(refreshedToken);
        //System.out.println("SALES_CRM_FCM_ID::"+refreshedToken);
        if(pref.isLogedIn()) {
            pref.setFcmSync(1);
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).refreshFcmToken(pref.getFcmId(), new Callback<FCMTokenRefreshResponse>() {
                @Override
                public void success(FCMTokenRefreshResponse fcmTokenRefreshResponse, Response response) {

                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }
                    if (fcmTokenRefreshResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        pref.setFcmSync(0);
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    }
}
