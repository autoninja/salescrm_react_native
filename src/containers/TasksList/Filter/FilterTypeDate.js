import React, { Component } from "react";
import { View, Text } from 'react-native';
import { updateTasksListFilter } from '../../../actions/tasks_list_actions';
import ItemPicker from '../../../components/createLead/item_picker'
import Utils from '../../../utils/Utils'
export default class FilterTypeDate extends Component {
    constructor(props) {
        super(props);
    }
    getDate(date, placeHolder) {
        if (date) {
            return (Utils.getReadableDate(date));
        }
        else { return placeHolder }

    }
    render() {
        let { data } = this.props;
        return (
            <View style={{ padding: 10 }}>
                <Text style={{ fontSize: 16 }}>Expected buying date</Text>
                <ItemPicker
                    title={this.getDate(data[0].title, "From")}
                    onClick={() => {
                        this.props.onPress(0);
                    }}
                />
                <ItemPicker
                    title={this.getDate(data[1].title, "To")}
                    onClick={() => {
                        this.props.onPress(1);
                    }}
                />
                {
                    // <Text style={{ fontSize: 16, marginTop: 20 }}>Enquiry date</Text>
                    // <ItemPicker
                    //     title={this.getDate(data[2].title, "From")}
                    //     onClick={() => {
                    //         this.props.onPress(2);
                    //     }}
                    // />
                    // <ItemPicker
                    //     title={this.getDate(data[3].title, "To")}
                    //     onClick={() => {
                    //         this.props.onPress(3);
                    //     }}
                    // />
                }
            </View>
        )
    }
}