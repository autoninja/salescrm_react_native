package com.salescrm.telephony.views.gameintro;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.HomeActivity;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.presenter.gameintro.GameIntroMainPresenter;

import java.util.List;
import java.util.Locale;
import java.util.Random;

public class GameIntro extends Fragment {
    View mainView;
    int id;
    private Preferences pref;
    private List<String> locations;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        id = getArguments().getInt("ID", 1);
        mainView = inflater.inflate(getRootView(id), container, false);
        pref = Preferences.getInstance();
        pref.load(getContext());
        locations = DbUtils.getActiveGameLocationsNames();
        populate();
        return mainView;
    }

    private void populate() {
        switch (id) {
            case GameIntroMainPresenter.GAME_INTRO_WELCOME:
                TextView tvDealer = mainView.findViewById(R.id.tv_game_intro_dealer);
                tvDealer.setText(String.format("NinjaX @ %s", getProperName(pref.getDealerName())));
                break;
            case GameIntroMainPresenter.GAME_INTRO_TEAM_LEADER_BOARD:
                TextView tvLocation = mainView.findViewById(R.id.tv_game_intro_location);
                if(locations.size()>0) {
                    tvLocation.setText(String.format(Locale.getDefault(), "Be the Best Team at \n%s", locations.get(new Random().nextInt(locations.size()))));

                }
                else {
                    tvLocation.setText(String.format(Locale.getDefault(), "Be the Best Team"));
                }
                break;

            case GameIntroMainPresenter.GAME_INTRO_GET_STARTED:
                mainView.findViewById(R.id.tv_game_intro_get_started).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startGame();
                    }
                });
                break;

        }
    }
    private String getProperName(String name){
        if(name == null || name.equalsIgnoreCase("")) {
            return "";
        }
        return String.format("%s%s", name.substring(0, 1).toUpperCase(),
                name.split(" ")[0].substring(1));
    }

    private void startGame() {
        pref.setGameIntroSeen(true);
        startActivity(new Intent(getActivity(), HomeActivity.class));
        getActivity().finish();
    }

    private int getRootView(int id) {
        switch (id) {
            case GameIntroMainPresenter.GAME_INTRO_WELCOME:
                return R.layout.game_intro_welcome;
            case GameIntroMainPresenter.GAME_INTRO_TEAM_LEADER_BOARD:
                return R.layout.game_intro_team_leader_board;
            case GameIntroMainPresenter.GAME_INTRO_CONSULTANT_LEADER_BOARD:
                return R.layout.game_intro_consultant_leader_board;
            case GameIntroMainPresenter.GAME_INTRO_POINT_SYSTEM:
                return R.layout.game_intro_point_system;
            case GameIntroMainPresenter.GAME_INTRO_GET_STARTED:
                return R.layout.game_intro_get_started;
            default:
                return R.layout.game_intro_get_started;
        }
    }

    public static GameIntro newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt("ID", id);
        GameIntro fragment = new GameIntro();
        fragment.setArguments(args);
        return fragment;
    }
}
