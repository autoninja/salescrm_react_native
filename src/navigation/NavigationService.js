// NavigationService.js

import { NavigationActions, StackActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}
function resetScreen(routeName, params, navigation) {
  let resetAction = StackActions.reset({
    index: 0, // <-- currect active route from actions array
    key: null,
    actions: [
      (routeName.action) ?
        NavigationActions.navigate(routeName)
        :
        NavigationActions.navigate({ routeName, params })
    ],
  });;
  if (navigation) {
    navigation.dispatch(resetAction);
  }
  else {
    _navigator.dispatch(resetAction);
  }
}

// add other navigation functions that you need and export them

export default {
  navigate,
  resetScreen,
  setTopLevelNavigator,
};