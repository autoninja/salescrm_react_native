package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by bharath on 8/9/16.
 */
public class SalesManagerDseResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private List<Dses> dses;

        private List<Sales_managers> sales_managers;

        public List<Dses> getDses() {
            return dses;
        }

        public void setDses(List<Dses> dses) {
            this.dses = dses;
        }

        public List<Sales_managers> getSales_managers() {
            return sales_managers;
        }

        public void setSales_managers(List<Sales_managers> sales_managers) {
            this.sales_managers = sales_managers;
        }

        @Override
        public String toString() {
            return "ClassPojo [dses = " + dses + ", sales_managers = " + sales_managers + "]";
        }

        public class Dses {
            private String id;

            private String name;

            private String team_id;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getTeam_id() {
                return team_id;
            }

            public void setTeam_id(String team_id) {
                this.team_id = team_id;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", name = " + name + ", team_id = " + team_id + "]";
            }
        }

        public class Sales_managers {
            private String id;

            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", name = " + name + "]";
            }
        }
    }
}