package com.salescrm.telephony.views.gameintro;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.salescrm.telephony.presenter.gameintro.GameIntroMainPresenter;

public class GameIntroViewPagerAdapter extends FragmentStatePagerAdapter {

    private static final int COUNT = 5;

    public GameIntroViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        GameIntro gameIntro = null;
        switch (position) {
            case 0:
                gameIntro = GameIntro.newInstance(GameIntroMainPresenter.GAME_INTRO_WELCOME);
                break;
            case 1:
                gameIntro = GameIntro.newInstance(GameIntroMainPresenter.GAME_INTRO_TEAM_LEADER_BOARD);
                break;
            case 2:
                gameIntro = GameIntro.newInstance(GameIntroMainPresenter.GAME_INTRO_CONSULTANT_LEADER_BOARD);
                break;
            case 3:
                gameIntro = GameIntro.newInstance(GameIntroMainPresenter.GAME_INTRO_POINT_SYSTEM);
                break;
            case 4:
                gameIntro = GameIntro.newInstance(GameIntroMainPresenter.GAME_INTRO_GET_STARTED);
                break;
        }
        return gameIntro;
    }

    @Override
    public int getCount() {
        return COUNT;
    }
}
