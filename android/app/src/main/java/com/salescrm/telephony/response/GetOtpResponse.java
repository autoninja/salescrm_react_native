package com.salescrm.telephony.response;

/**
 * Created by bharath on 7/7/16.
 */
public class GetOtpResponse {

    private String responseMessage;

    private String statusCode;

    private String[] errorDetails;

    private Data data;

    private String responseTime;

    public String getResponseMessage ()
    {
        return responseMessage;
    }

    public void setResponseMessage (String responseMessage)
    {
        this.responseMessage = responseMessage;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String[] getErrorDetails ()
    {
        return errorDetails;
    }

    public void setErrorDetails (String[] errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    public String getResponseTime ()
    {
        return responseTime;
    }

    public void setResponseTime (String responseTime)
    {
        this.responseTime = responseTime;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [responseMessage = "+responseMessage+", statusCode = "+statusCode+", errorDetails = "+errorDetails+", data = "+data+", responseTime = "+responseTime+"]";
    }
    public class Data
    {
        private String success;

        private Info info;

        public String getSuccess ()
        {
            return success;
        }

        public void setSuccess (String success)
        {
            this.success = success;
        }

        public Info getInfo ()
        {
            return info;
        }

        public void setInfo (Info info)
        {
            this.info = info;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [success = "+success+", info = "+info+"]";
        }
    }
    public class Info
    {
        private Message[] message;

        private String cre_count;

        public Message[] getMessage ()
        {
            return message;
        }

        public void setMessage (Message[] message)
        {
            this.message = message;
        }

        public String getCre_count ()
        {
            return cre_count;
        }

        public void setCre_count (String cre_count)
        {
            this.cre_count = cre_count;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [message = "+message+", cre_count = "+cre_count+"]";
        }
    }

    public class Message
    {
        private String dealer;

        private String id;

        private String user_name;

        private String module;

        private String email;

        private String name;

        private String store_recording;

        private String otpStore;

        private String otp;

        private String mobile;

        public String getDealer ()
        {
            return dealer;
        }

        public void setDealer (String dealer)
        {
            this.dealer = dealer;
        }

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getUser_name ()
        {
            return user_name;
        }

        public void setUser_name (String user_name)
        {
            this.user_name = user_name;
        }

        public String getModule ()
        {
            return module;
        }

        public void setModule (String module)
        {
            this.module = module;
        }

        public String getEmail ()
        {
            return email;
        }

        public void setEmail (String email)
        {
            this.email = email;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getStore_recording ()
        {
            return store_recording;
        }

        public void setStore_recording (String store_recording)
        {
            this.store_recording = store_recording;
        }

        public String getOtpStore ()
        {
            return otpStore;
        }

        public void setOtpStore (String otpStore)
        {
            this.otpStore = otpStore;
        }

        public String getOtp ()
        {
            return otp;
        }

        public void setOtp (String otp)
        {
            this.otp = otp;
        }

        public String getMobile ()
        {
            return mobile;
        }

        public void setMobile (String mobile)
        {
            this.mobile = mobile;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [dealer = "+dealer+", id = "+id+", user_name = "+user_name+", module = "+module+", email = "+email+", name = "+name+", store_recording = "+store_recording+", otpStore = "+otpStore+", otp = "+otp+", mobile = "+mobile+"]";
        }
    }

    public class ErrorDetails
    {
        private String message;

        private String type;

        public String getMessage ()
        {
            return message;
        }

        public void setMessage (String message)
        {
            this.message = message;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [message = "+message+", type = "+type+"]";
        }
    }



}
