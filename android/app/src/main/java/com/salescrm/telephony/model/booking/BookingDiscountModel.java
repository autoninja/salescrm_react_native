package com.salescrm.telephony.model.booking;

/**
 * Created by bharath on 22/2/18.
 */

public class BookingDiscountModel {
    private String corporate;
    private String exchangeBonus;
    private String loyaltyBonus;
    private String oemScheme;
    private String dealer;
    private String accessories;

    public String getCorporate() {
        return corporate;
    }

    public void setCorporate(String corporate) {
        this.corporate = corporate;
    }

    public String getExchangeBonus() {
        return exchangeBonus;
    }

    public void setExchangeBonus(String exchangeBonus) {
        this.exchangeBonus = exchangeBonus;
    }

    public String getLoyaltyBonus() {
        return loyaltyBonus;
    }

    public void setLoyaltyBonus(String loyaltyBonus) {
        this.loyaltyBonus = loyaltyBonus;
    }

    public String getOemScheme() {
        return oemScheme;
    }

    public void setOemScheme(String oemScheme) {
        this.oemScheme = oemScheme;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public String getAccessories() {
        return accessories;
    }

    public void setAccessories(String accessories) {
        this.accessories = accessories;
    }


    public void copy(BookingDiscountModel bookingDiscountModel) {
        this.corporate = bookingDiscountModel.getCorporate();
        this.exchangeBonus = bookingDiscountModel.getExchangeBonus();
        this.loyaltyBonus = bookingDiscountModel.getLoyaltyBonus();
        this.oemScheme = bookingDiscountModel.getOemScheme();
        this.dealer = bookingDiscountModel.getDealer();
        this.accessories = bookingDiscountModel.getAccessories();
    }
}
