import React, { Component } from 'react'
import { View, Text, Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Alert} from 'react-native'
import LiveEFFooter from './live_ef_footer'
import styles from '../styles/styles'

export default class LiveEFResultItemCarModel extends Component {
  constructor(props) {
    super(props);
  }
  _onLongPressButton() {
    console.log('Long press worked');
    Alert.alert('You long-pressed the button!')
  }
  render() {
    if(this.props.showPercentage) {
      return (
      <View>
      <View style={{flexDirection:'column'}}>
      <View style={{backgroundColor:'#5E7692', height:1}}/>
      <View style={{flexDirection: 'row', height:60, backgroundColor:'#0000003c', alignItems:'center'}}>

      <View style={{flex:1.5, alignItems:'center'}} >

      <Text style={{ color:'#fff', fontWeight:'bold'}} >Total%</Text>


      </View>


      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.rel.r}</Text>
        </View>
      </View>



      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.rel.e}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.rel.f_i}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.rel.f_o}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#7ed321', fontWeight:'bold'}} >-</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

          <Text style={{ color:'#7ed321', fontWeight:'bold'}} >-</Text>
      </View>


      </View>
      <View style={{backgroundColor:'#5E7692', height:1}}/>
      </View>
      <LiveEFFooter />
      </View>
    );
    }
    else {
    return(
      <View>
      <View style={{flexDirection:'column'}}>
      <View style={{backgroundColor:'#5E7692', height:1}}/>
      <View style={{flexDirection: 'row', height:60, backgroundColor:'#0000003c', alignItems:'center'}}>

      <View style={{flex:1.5, alignItems:'center'}} >

        <Text style={{ color:'#fff', fontWeight:'bold'}} >Total</Text>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.abs.r}</Text>
        </View>

      </View>




      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.abs.e}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.abs.f_i}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.abs.f_o}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#7ed321', fontWeight:'bold'}} >{this.props.abs.p_b}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

          <Text style={{ color:'#7ed321', fontWeight:'bold'}} >{this.props.abs.l_e}</Text>
      </View>


      </View>
      <View style={{backgroundColor:'#5E7692', height:1}}/>
      </View>
      <LiveEFFooter />
      </View>
    );
  }
  }
}
