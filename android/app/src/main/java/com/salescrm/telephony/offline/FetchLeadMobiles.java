package com.salescrm.telephony.offline;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.AllMobileNumbersSearch;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.LeadMobileResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.List;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 15/2/17.
 * Call this to get the lead basic elements
 */
public class FetchLeadMobiles implements Callback<LeadMobileResponse> {
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;

    public FetchLeadMobiles(OfflineSupportListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }

    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getLeadMobiles(this);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.LEAD_MOBILE);
        }
    }


    @Override
    public void success(final LeadMobileResponse leadMobileResponse, Response response) {
        Util.updateHeaders(response.getHeaders());

        if (leadMobileResponse.getStatusCode() != null && !leadMobileResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + leadMobileResponse.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.LEAD_MOBILE);
        } else {
            if (leadMobileResponse.getResult() != null) {
                System.out.println("Success:1" + leadMobileResponse.getMessage());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm1) {
                        insertData(realm1, leadMobileResponse.getResult());

                    }
                });
                listener.onLeadMobileFetched(leadMobileResponse);
            } else {
                // relLoader.setVisibility(View.GONE);

                System.out.println("Success:2" + leadMobileResponse.getMessage());
                listener.onLeadMobileFetched(null);
                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }


    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.LEAD_MOBILE);

    }

    private void insertData(Realm realm, List<LeadMobileResponse.Result> leadMobileResponse) {
        System.out.println("Insert Data Called");
        AllMobileNumbersSearch allMobileNumbersSearchDb;
        realm.delete(AllMobileNumbersSearch.class);

        for (int i = 0; i < leadMobileResponse.size(); i++) {
            LeadMobileResponse.Result leadMobileResponseData = leadMobileResponse.get(i);
            for (int j=0;j<leadMobileResponseData.getMobiles().size();j++) {

                allMobileNumbersSearchDb = new AllMobileNumbersSearch();
                allMobileNumbersSearchDb.setMobileNumber(leadMobileResponseData.getMobiles().get(j));
                try {
                    allMobileNumbersSearchDb.setLeadId(Long.parseLong(leadMobileResponseData.getLead_id()));
                }
                catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                realm.copyToRealmOrUpdate(allMobileNumbersSearchDb);

            }
        }

    }

}
