package com.salescrm.telephony.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.ActionPlanRealmAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.dataitem.SectionModel;
import com.salescrm.telephony.db.CustomerPhoneNumbers;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.dbOperation.LeadMobileMappingDbOperations;
import com.salescrm.telephony.dbOperation.TasksDbOperation;
import com.salescrm.telephony.interfaces.ActionPlanHeaderUpdateListener;
import com.salescrm.telephony.interfaces.AdapterCommunication;
import com.salescrm.telephony.interfaces.AutoFetchFormListener;
import com.salescrm.telephony.interfaces.BadgeUpdater;
import com.salescrm.telephony.model.ActionPlanApiListener;
import com.salescrm.telephony.model.FilterDataChangedListener;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.model.TabLayoutText;
import com.salescrm.telephony.model.ViewPagerSwiped;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.ActionPlanResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bharath on 23/06/16.
 */

public class AllActionPlanFragment extends Fragment implements AdapterCommunication,
        Callback<ActionPlanResponse>, AutoFetchFormListener {

    public static ActionPlanHeaderUpdateListener actionPlanUpdateListener;
    private Integer dseId;
    SwipeRefreshLayout swipeRefreshLayout;
    List<SectionModel> sections;
    private String Tag = "AllActionPlan";
    private RecyclerView mRecyclerView;
    private Preferences pref = null;
    private RelativeLayout relAlert;
    private Button btRefresh;
    private TextView tvAlert;
    private Realm realm;
    private SalesCRMRealmTable salesCRMRealmTable;
    private int visibleItemCount, totalItemCount, pastVisiblesItems;
    private boolean loading;
    private int totalPending = 0;
    private int totalPagePending = 1;
    private int totalFuture = 0;
    private int totalPageFuture = 1;
    private int currentPagePending = 0;
    private int currentPageFuture = 0;
    private int prevPage;
    private int pageLimit;
    //    private ActionPlanAdapter mAdapter;
    private ProgressBar progressBottomLayout,progressTopLayout;
    private int todayCount = 0;
    private int todayStartIndex = -1;
    private LinearLayoutManager mLayoutManager;
    private int prevPosPending, prevPosFuture;
    private boolean futureRequested, pendingRequested;
    private int apiCallCount = 0;
    private ActionPlanRealmAdapter adapter;
    private FilterSelectionHolder filterSelectionHolder;
    private boolean isCreatedTemp = false;
    private FrameLayout frameLoaderWrapper;

    public static AllActionPlanFragment newInstance(ActionPlanFragment context) {
        try {
            actionPlanUpdateListener = (ActionPlanHeaderUpdateListener) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        return new AllActionPlanFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_all_action_plan, container, false);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        filterSelectionHolder = FilterSelectionHolder.getInstance();
        sections = new ArrayList<SectionModel>();
        progressBottomLayout = (ProgressBar) rootView.findViewById(R.id.progress_bottom_all_action);
        progressTopLayout = (ProgressBar) rootView.findViewById(R.id.progress_bottom_all_action_top);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_action_all);
        frameLoaderWrapper = (FrameLayout) rootView.findViewById(R.id.frame_task_loader_wrapper);
        relAlert = (RelativeLayout) rootView.findViewById(R.id.frame_alert);
        btRefresh = (Button) rootView.findViewById(R.id.btAlertRefresh);
        tvAlert = (TextView) rootView.findViewById(R.id.tv_alert);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.progressRecycler);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        dseId = pref.getCurrentDseId();

        if(pref.getCurrentDseId()!=null) {
            if (pref.getCurrentDseId().intValue() != pref.getAppUserId().intValue()) {
                isCreatedTemp = true;
            }
        }


        setupRealmAdapter();
        realm.where(SalesCRMRealmTable.class).findAll().addChangeListener(new RealmChangeListener<RealmResults<SalesCRMRealmTable>>() {
            @Override
            public void onChange(RealmResults<SalesCRMRealmTable> element) {
                SalesCRMApplication.getBus().post(new TabLayoutText(3, "Future " + realm.where(SalesCRMRealmTable.class)
                        .equalTo("isCreatedTemporary", false)
                        .equalTo("isDone", false)
                        .equalTo("isLeadActive", 1)
                        .greaterThanOrEqualTo("activityScheduleDate", Util.getTime(1))
                        .equalTo("createdOffline", false).findAll().size(),

                        realm.where(SalesCRMRealmTable.class)
                                .equalTo("isCreatedTemporary", false)
                                .equalTo("isDone", false)
                                .equalTo("isLeadActive", 1)
                                .equalTo("createdOffline", false)
                                .greaterThanOrEqualTo("activityScheduleDate", Util.getTime(1))
                                .findAll().size(), 0));
                System.out.println(Tag + ":Data change called:" + realm.where(SalesCRMRealmTable.class).findAll().size());
                sortActivityDetails();
                adapter.notifyDataSetChanged();
            }
        });

      /*  mAdapter = new ActionPlanAdapter(getContext(), realm, customerDetailsActionPlanResponseData,
                activityDetailsActionPlanResponseData, leadDetailsActionPlanResponseData, sections, getActivity(), this);*/
        loading = true;

        //setSwipeRefreshView(true);


        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading && (visibleItemCount + pastVisiblesItems) >= totalItemCount && currentPageFuture < totalPageFuture &&
                            new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                        loading = false;
                        System.out.println(Tag + ":Last Item Wow !");
                        // getAllData(true, currentPageFuture + 1, WSConstants.FUTURE, WSConstants.ORDER_DESC);
                        /*futureRequested = true;
                        pendingRequested = false;*/
                        getAllData(false, currentPageFuture+1, WSConstants.FUTURE, WSConstants.ORDER_DESC);

                    }
                }
                /*if (dy < 0) {
                    if (mLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                        if (loading && currentPagePending < totalPagePending &&
                                new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                            System.out.println(Tag + ":First Item checked !");
                            loading = false;
                            //Do pagination.. i.e. fetch new data At Top
                            getAllData(false, currentPagePending+1, WSConstants.PENDING, WSConstants.ORDER_DESC);
                        }
                    }
                }*/

            }
        });
        btRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relAlert.setVisibility(View.GONE);
                //getAllData(true,"");
                if (new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                    if (filterSelectionHolder != null && !filterSelectionHolder.isEmpty()) {
                        Toast.makeText(getContext(), "Refresh not allowed during filter", Toast.LENGTH_SHORT).show();
                        setSwipeRefreshView(false);
                    } else {
                        getAllData(true, 1, WSConstants.FUTURE, WSConstants.ORDER_ASC);
                    }
                } else {
                    //Offline
                    Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                    setSwipeRefreshView(false);
                }            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //  getAllData(true);
                relAlert.setVisibility(View.GONE);
                if (new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                    if (filterSelectionHolder != null && !filterSelectionHolder.isEmpty()) {
                        Toast.makeText(getContext(), "Refresh not allowed during filter", Toast.LENGTH_SHORT).show();
                        setSwipeRefreshView(false);
                    } else {
                        getAllData(true, 1, WSConstants.FUTURE, WSConstants.ORDER_ASC);
                    }
                } else {
                    //Offline
                    Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                    setSwipeRefreshView(false);
                }
                //setSwipeRefreshView(true);
            }
        });
        System.out.println(Tag + "OnCreate");
        return rootView;
    }

    private void setupRealmAdapter() {
        //Setting up adapter.
        OrderedRealmCollection<SalesCRMRealmTable> realmResult = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("createdOffline", false)
                .equalTo("dseId", dseId)
                .greaterThanOrEqualTo("activityScheduleDate", Util.getTime(1))
                .findAllSorted("activityScheduleDate", Sort.ASCENDING);
        adapter = new ActionPlanRealmAdapter(getContext(),
                realmResult, true, sections);
        mRecyclerView.setAdapter(adapter);

        updateViews();
    }

    private void setupRealmAdapterWithFilter() {
        //Setting up adapter with filter.
        RealmQuery<SalesCRMRealmTable> realmQuery = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("createdOffline", false)
                .greaterThanOrEqualTo("activityScheduleDate", Util.getTime(1))
                .equalTo("dseId", dseId);


        for (int i = 0; i < filterSelectionHolder.getAllMapData().size(); i++) {
            int insideKey = filterSelectionHolder.getAllMapData().keyAt(i);
            String key = Util.getAdvanceFilterKey(insideKey);
            System.out.println("Key::" + key);
            if (!TextUtils.isEmpty(key)) {

                //Special case
                if (key.equalsIgnoreCase("activityId")) {
                    List<String> activities = new ArrayList<>();
                    for (int j = 0; j < filterSelectionHolder.getMapData(insideKey).size(); j++) {
                        Collections.addAll(activities, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j)).split(","));
                    }

                    for (int activity = 0; activity < activities.size(); activity++) {
                        if (activity == 0) {
                            realmQuery = realmQuery.beginGroup();
                        }
                        realmQuery = realmQuery.equalTo(key, Util.getInt(activities.get(activity)));
                        if (activity != activities.size() - 1) {
                            realmQuery = realmQuery.or();
                        } else {
                            realmQuery = realmQuery.endGroup();
                        }
                    }


                } else {

                    for (int j = 0; j < filterSelectionHolder.getMapData(insideKey).size(); j++) {

                        System.out.println("value at" + j + ":" + filterSelectionHolder.getMapData(insideKey).keyAt(j));

                        if (key.equalsIgnoreCase("date")) {
                            int dateKey = filterSelectionHolder.getMapData(insideKey).keyAt(j);
                            Date dateVal = Util.getCalender(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j))).getTime();
                            System.out.println("Key:" + dateKey);
                            System.out.println("Date Val:" + dateVal.toString());
                            if (dateKey == 0) {
                                realmQuery = realmQuery.greaterThanOrEqualTo("leadExpectedClosingDate", dateVal);
                            } else if (dateKey == 1) {
                                realmQuery = realmQuery.lessThanOrEqualTo("leadExpectedClosingDate", dateVal);

                            } else if (dateKey == 2) {
                                realmQuery = realmQuery.greaterThanOrEqualTo("activityScheduleDate", dateVal);

                            } else if (dateKey == 3) {
                                realmQuery = realmQuery.lessThanOrEqualTo("activityScheduleDate", dateVal);

                            }
                            continue;
                        }
                        if (key.equalsIgnoreCase("leadAge")) {
                            int valFrom = Integer.parseInt(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j)));
                            int valTo = Integer.parseInt(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j + 1)));
                            realmQuery = realmQuery.between(key, valFrom, valTo);
                            break;
                        }


                        if (j == 0) {
                            realmQuery = realmQuery.beginGroup();
                        }
                        if(key.equalsIgnoreCase("leadCarModelId")){
                            realmQuery = realmQuery.equalTo(key, filterSelectionHolder.getMapData(insideKey).keyAt(j)+"");
                        }
                        else {
                            realmQuery = realmQuery.contains(key, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j)));

                        }

                        if (j != filterSelectionHolder.getMapData(insideKey).size() - 1) {
                            realmQuery = realmQuery.or();
                        } else {
                            realmQuery = realmQuery.endGroup();
                        }

                    }
                }

            }

        }
        OrderedRealmCollection<SalesCRMRealmTable> realmResult = realmQuery.findAllSorted("activityScheduleDate", Sort.ASCENDING);
        mRecyclerView.setAdapter(new ActionPlanRealmAdapter(getContext(),
                realmResult, true, null));

        updateViews();
        if (realmResult.size() == 0) {
            relAlert.setVisibility(View.VISIBLE);
            tvAlert.setText("No task in this filter");
            btRefresh.setVisibility(View.INVISIBLE);
        }
        sortActivityDetails(realmResult);
        if (actionPlanUpdateListener != null) {
            actionPlanUpdateListener.onFilterAppliedUpdateText(1, "Future " + realmQuery.findAll().size());
            //  SalesCRMApplication.getBus().post(new TabLayoutText(2, "Pending " + realmQuery.findAll().size(), realmQuery.findAll().size(), 0));
        }
    }

    public void getAllData(boolean fromSwipe, int pageNumber, String when, String order) {
        //SalesCRMApplication.getBus().post(new TabLayoutText(3, "All " + total, total, 0));
        if (when.equals(WSConstants.FUTURE)) {
            futureRequested = true;
            pendingRequested = false;
        } else {
            futureRequested = false;
            pendingRequested = true;

        }
        if (fromSwipe) {
            setSwipeRefreshView(true);
            progressBottomLayout.setVisibility(View.GONE);
        } else if(when.equalsIgnoreCase(WSConstants.PENDING)) {
            setSwipeRefreshView(false);
            progressTopLayout.setVisibility(View.VISIBLE);
        }
        else {
            setSwipeRefreshView(false);
            progressBottomLayout.setVisibility(View.VISIBLE);
        }


        ApiUtil.GetRestApiWithHeader(pref.getAccessToken())
                .GetActionPlanData(pageNumber, 1000, true, true, 0, null, null, "normal", when, null, order, dseId,0, this);

    }

    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
        System.out.println(Tag + "OnResume");
        if (!filterSelectionHolder.isEmpty()) {
            setupRealmAdapterWithFilter();
        } else {
            setupRealmAdapter();
        }

    }

    @Subscribe
    public void onFilterDataChanged(FilterDataChangedListener filterDataChangedListener){
        if(filterDataChangedListener.isUpdateRequired()){
            if (!filterSelectionHolder.isEmpty()) {
                setupRealmAdapterWithFilter();
            } else {
                setupRealmAdapter();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    public void AllTaskFragmentCallBack(String LeadId) {
        replaceFragment(LeadId);
    }

    @Override
    public void FormActivityFragmentCallBack() {
    }

    @Override
    public void FormActivityRescheduleCallBack() {
    }

    @Override
    public void FormActivityEmailSmaCallBack() {
    }

    private void replaceFragment(String LeadId) {
        /*Intent i = new Intent(getActivity(), LeadDetailsActivity.class);
        i.putExtra("LeadId", LeadId);
        startActivity(i);*/
    }


    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void setSwipeRefreshView(final boolean val) {
        //swipeRefreshLayout.setEnabled(val);
        if(val){
            frameLoaderWrapper.setVisibility(View.VISIBLE);
        }
        else {
            frameLoaderWrapper.setVisibility(View.GONE);
        }
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(val);
            }
        });
    }

    @Override
    public void success(final ActionPlanResponse actionPlanResponse, Response response) {
        List<Header> headerList = response.getHeaders();
        for (Header header : headerList) {
            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                ApiUtil.UpdateAccessToken(header.getValue());
            }
        }

        if(actionPlanResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
            ApiUtil.InvalidUserLogout(getActivity(),0);
        }

        if (!actionPlanResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println(Tag + " Success:0" + actionPlanResponse.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            loading = true;
            progressBottomLayout.setVisibility(View.GONE);
            progressTopLayout.setVisibility(View.GONE);
            setSwipeRefreshView(false);
        } else {
            if (actionPlanResponse.getResult() != null) {
                System.out.println(Tag + " Success:1" + actionPlanResponse.getMessage());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        addDataToDB(realm, actionPlanResponse.getResult().getData());

                    }
                });
                TasksDbOperation.getInstance().refreshTasks(realm,
                        actionPlanResponse.getResult().getActiveTaskIds(),
                        isCreatedTemp,dseId);
                BadgeUpdater.updateBadge(getContext());
                if (futureRequested) {
                    totalFuture = Integer.parseInt(actionPlanResponse.getResult().getTotal());
                    totalPageFuture = Integer.parseInt(actionPlanResponse.getResult().getTotal_pages());
                    currentPageFuture = Integer.parseInt(actionPlanResponse.getResult().getPage());
                } else {
                    totalPending = Integer.parseInt(actionPlanResponse.getResult().getTotal());
                    totalPagePending = Integer.parseInt(actionPlanResponse.getResult().getTotal_pages());
                    currentPagePending = Integer.parseInt(actionPlanResponse.getResult().getPage());
                }
                if (actionPlanUpdateListener != null) {
                    actionPlanUpdateListener.onActionPlanHeaderUpdateRequired(1);
                } else {
                    System.out.println("Listener null inside All");
                }

                pageLimit = Integer.parseInt(actionPlanResponse.getResult().getPage_limit());
                if (getContext() != null && dseId.intValue() == pref.getAppUserId().intValue()) {
                   /* new AutoFetchFormData(this, getContext()).call(realm.where(SalesCRMRealmTable.class)
                                    .equalTo("isCreatedTemporary", false)
                                    .equalTo("isDone", false)
                                    .equalTo("createdOffline", false)
                                    .equalTo("isLeadActive", 1)
                                    .equalTo("dseId", dseId)
                                    .greaterThanOrEqualTo("activityScheduleDate", Util.getTime(2)).findAll()
                            , true);*/
                    //SalesCRMApplication.getBus().post(validateOtpResponse);
                    loading = true;
                    progressTopLayout.setVisibility(View.GONE);
                    progressBottomLayout.setVisibility(View.GONE);
                    setSwipeRefreshView(false);
                    updateViews();
                } else {
                    loading = false;
                    progressBottomLayout.setVisibility(View.GONE);
                    progressTopLayout.setVisibility(View.GONE);
                    setSwipeRefreshView(false);
                    System.out.println(Tag + " Success:3" + actionPlanResponse.getMessage());
                    updateViews();
                }
            } else {
                loading = true;
                progressBottomLayout.setVisibility(View.GONE);
                setSwipeRefreshView(false);
                System.out.println(Tag + " Success:2" + actionPlanResponse.getMessage());
                //showAlert(validateOtpResponse.getMessage());
            }

            if (pref.isFirstTime()) {
                SalesCRMApplication.getBus().post(new ActionPlanApiListener(3));
                // SalesCRMApplication.getBus().post(new TabLayoutText(3, "All " + total, total, 0));
                pref.setFirstTime(false);
            }
            LeadMobileMappingDbOperations.updateCallingMobileNumbers(realm);
        }
    }

    @Override
    public void onAutoFormsDataFetched(boolean b, RealmResults<SalesCRMRealmTable> data) {
        loading = true;
        progressTopLayout.setVisibility(View.GONE);
        progressBottomLayout.setVisibility(View.GONE);
        setSwipeRefreshView(false);
        updateViews();

    }

    @Override
    public void onAutoFormsDataError(RetrofitError error, int from) {
        loading = true;

        progressBottomLayout.setVisibility(View.GONE);
        setSwipeRefreshView(false);
        updateViews();

    }


    @Override
    public void failure(RetrofitError error) {
        setSwipeRefreshView(false);
        progressBottomLayout.setVisibility(View.GONE);
        loading = true;
        System.out.println(Tag + "Error");
        onErrorApiCallController(error);

    }

    private void onErrorApiCallController(RetrofitError error) {
        if (error.getKind() == RetrofitError.Kind.NETWORK) {
            apiCallCount++;
            if (apiCallCount <= WSConstants.API_MAX_CALL) {
                Util.showToast(getContext(), "Try again", Toast.LENGTH_SHORT);
            } else {
                apiCallCount = 0;
            }
        } else {
            Util.showToast(getContext(), "Something went wrong", Toast.LENGTH_SHORT);
            Util.systemPrint("Something happened in the server");
            apiCallCount = 0;
        }
    }


    private void updateViews() {
        setSwipeRefreshView(false);
        int total = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("createdOffline", false)
                .equalTo("dseId", dseId)
                .greaterThanOrEqualTo("activityScheduleDate", Util.getTime(1))
                .findAll().size();


       /* if(new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            SalesCRMApplication.getBus().post(new TabLayoutText(3, "All " + totalFuture, totalFuture, 0));
        }*/
        if (total == 0) {
            relAlert.setVisibility(View.VISIBLE);
            tvAlert.setText("No task :)");
        } else {
            relAlert.setVisibility(View.GONE);
        }
        sortActivityDetails();
        adapter.notifyDataSetChanged();
       /* if (todayStartIndex != -1) {
            mLayoutManager.scrollToPositionWithOffset(todayStartIndex, 60);
        }*/

        //     mLayoutManager.smoothScrollToPosition(mRecyclerView, null,todayStartIndex+1);
//
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    public void addDataToDB(Realm realm, List<ActionPlanResponse.Result.Data> actionPlanResponseData) {

        for (int i = 0; i < actionPlanResponseData.size(); i++) {
            salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId",
                    Integer.parseInt(actionPlanResponseData.get(i).getActivity().getScheduled_activity_id())).findFirst();
            if (salesCRMRealmTable == null) {
                salesCRMRealmTable = new SalesCRMRealmTable();
                salesCRMRealmTable.setScheduledActivityId(Integer.parseInt(actionPlanResponseData.get(i).getActivity().getScheduled_activity_id()));

            }
            salesCRMRealmTable.setTemplateExist(actionPlanResponseData.get(i).getTemplate_exist());
            salesCRMRealmTable.setIsLeadActive(1);
            salesCRMRealmTable.setCustomerId(Integer.parseInt(actionPlanResponseData.get(i).getCustomer().getCustomer_id()));
            salesCRMRealmTable.setCustomerNumber(actionPlanResponseData.get(i).getCustomer().getMobile_number());
            salesCRMRealmTable.setCustomerName(actionPlanResponseData.get(i).getCustomer().getName());
            salesCRMRealmTable.setCustomerEmail(actionPlanResponseData.get(i).getCustomer().getEmail());
            salesCRMRealmTable.setFirstName(actionPlanResponseData.get(i).getCustomer().getFirst_name());
            salesCRMRealmTable.setLastName(actionPlanResponseData.get(i).getCustomer().getLast_name());
            salesCRMRealmTable.setGender(actionPlanResponseData.get(i).getCustomer().getGender());
            salesCRMRealmTable.setTitle(actionPlanResponseData.get(i).getCustomer().getTitle());
            String customerAge = actionPlanResponseData.get(i).getCustomer().getAge();
            salesCRMRealmTable.setCustomerAge(customerAge.equals("") ? null : (customerAge));
            salesCRMRealmTable.setCustomerAddress(actionPlanResponseData.get(i).getCustomer().getCustomer_address());
            salesCRMRealmTable.setLeadLocationName(actionPlanResponseData.get(i).getLead().getLocation_name());
            salesCRMRealmTable.setResidenceAddress(actionPlanResponseData.get(i).getCustomer().getCustomer_address());
            salesCRMRealmTable.setResidencePinCode(actionPlanResponseData.get(i).getCustomer().getResidence_pin_code());
            salesCRMRealmTable.setOfficeAddress(actionPlanResponseData.get(i).getCustomer().getOffice_address());
            salesCRMRealmTable.setOfficePinCode(actionPlanResponseData.get(i).getCustomer().getOffice_pin_code());
          //  System.out.println("All Action Plan  Company Before" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
            salesCRMRealmTable.setCompanyName(actionPlanResponseData.get(i).getCustomer().getCompany_name());
          //  System.out.println("All Action Plan  Company After" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
            salesCRMRealmTable.setLeadId(Integer.parseInt(actionPlanResponseData.get(i).getLead().getLead_id()));
            salesCRMRealmTable.setLeadCarVariantName(actionPlanResponseData.get(i).getLead_car().getVariant_name());
            salesCRMRealmTable.setLeadCarModelName(actionPlanResponseData.get(i).getLead_car().getModel_name());
            salesCRMRealmTable.setLeadCarModelId(actionPlanResponseData.get(i).getLead_car().getModel_id());
            salesCRMRealmTable.setLeadTagsName(actionPlanResponseData.get(i).getLead().getLead_tags().getTag_names());
            salesCRMRealmTable.setLeadTagsColor(actionPlanResponseData.get(i).getLead().getLead_tags().getTag_colors());
            salesCRMRealmTable.setLeadDseName(actionPlanResponseData.get(i).getLead().getDseAlloted().getDseName());
            salesCRMRealmTable.setLeadDsemobileNumber(actionPlanResponseData.get(i).getLead().getDseAlloted().getDseMobNo());
            salesCRMRealmTable.setLeadStageId(actionPlanResponseData.get(i).getLead().getLead_stage().getId());
            salesCRMRealmTable.setLeadStage(actionPlanResponseData.get(i).getLead().getLead_stage().getStage());
            salesCRMRealmTable.setLeadAge(actionPlanResponseData.get(i).getLead().getLead_age());
            salesCRMRealmTable.setLeadSourceName(actionPlanResponseData.get(i).getLead().getLead_source_name());
            salesCRMRealmTable.setLeadSourceId(actionPlanResponseData.get(i).getLead().getLead_source_id());
            salesCRMRealmTable.setLeadLastUpdated(actionPlanResponseData.get(i).getLead().getLead_last_updated());
            salesCRMRealmTable.setLeadExpectedClosingDate(actionPlanResponseData.get(i).getLead().getExpected_closing_date());

            salesCRMRealmTable.setActivityId(Integer.parseInt(actionPlanResponseData.get(i).getActivity().getActivity_id()));
            salesCRMRealmTable.setActivityDescription(actionPlanResponseData.get(i).getActivity().getActivity_description());
            salesCRMRealmTable.setActivityCreationDate(Util.getDate(actionPlanResponseData.get(i).getActivity().getActivity_creation_date()));
            salesCRMRealmTable.setActivityScheduleDate(Util.getNormalDate(actionPlanResponseData.get(i).getActivity().getActivity_scheduled_date()));
            salesCRMRealmTable.setActivityTypeId(actionPlanResponseData.get(i).getActivity().getActivity_type_id());
            salesCRMRealmTable.setActivityType(actionPlanResponseData.get(i).getActivity().getType());
            salesCRMRealmTable.setActivityName(actionPlanResponseData.get(i).getActivity().getActivity_name());
            salesCRMRealmTable.setActivityGroupId(actionPlanResponseData.get(i).getActivity().getActivity_group_id());
            salesCRMRealmTable.setActivityIconType(actionPlanResponseData.get(i).getActivity().getIcon_type());
            salesCRMRealmTable.setDseId(dseId);
            salesCRMRealmTable.setCreatedTemporary(isCreatedTemp);
            salesCRMRealmTable.setDone(false);
            if(actionPlanResponseData.get(i).getCustomer().getSecondary_mobile_number() != null) {
                salesCRMRealmTable.setSecondaryMobileNumber(Long.parseLong(actionPlanResponseData.get(i).getCustomer().getSecondary_mobile_number()));
            }

            //Storing lead's all Mobile numbers
            RealmList<CustomerPhoneNumbers> customerPhoneNumbers = new RealmList<CustomerPhoneNumbers>();
            /* CustomerPhoneNumbers customerPhoneNumbers = new CustomerPhoneNumbers();*/
            if (actionPlanResponseData.get(i).getCustomer().getMobile_nos() != null) {
                for (int j = 0; j < actionPlanResponseData.get(i).getCustomer().getMobile_nos().size(); i++) {
                    CustomerPhoneNumbers customerPhoneNumbersObj = new CustomerPhoneNumbers();
                    customerPhoneNumbersObj.setLeadId(Integer.parseInt(actionPlanResponseData.get(i).getLead().getLead_id()));
                    customerPhoneNumbersObj.setCustomerID(Integer.parseInt(actionPlanResponseData.get(i).getCustomer().getCustomer_id()));
                    customerPhoneNumbersObj.setPhoneNumber(Long.parseLong(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getNumber()));
                    customerPhoneNumbersObj.setPhoneNumberID(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getId());
                    customerPhoneNumbersObj.setPhoneNumberStatus(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getStatus());
                    customerPhoneNumbersObj.setLead_phone_mapping_id(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getLead_phone_mapping_id());

                    customerPhoneNumbers.add(customerPhoneNumbersObj);
                }
            }

            realm.copyToRealmOrUpdate(salesCRMRealmTable);
//            System.out.println(Tag + ":AfterUpdating Db Realm:: C_Size:" + realm.where(CustomerDetails.class).findAll().size() + " L_size:" + realm.where(LeadDetails.class).findAll().size()
//                    + " A_size:" + realm.where(ActivityDetails.class).findAll().size());
        }

    }

    public void sortActivityDetails() {
        OrderedRealmCollection<SalesCRMRealmTable> list = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("createdOffline", false)
                .equalTo("dseId", dseId)
                .greaterThanOrEqualTo("activityScheduleDate", Util.getTime(1))
                .findAllSorted("activityScheduleDate", Sort.ASCENDING);
        sections.clear();
        sections.addAll(Util.createDateSections(list,WSConstants.ALL_TAB).getSections());
        //todayStartIndex = Util.createDateSections(list,WSConstants.ALL_TAB).getTodayIndex();

    }

    private void sortActivityDetails(OrderedRealmCollection<SalesCRMRealmTable> list) {
        sections.clear();
        sections.addAll(Util.createDateSections(list,WSConstants.ALL_TAB).getSections());
        //todayStartIndex = Util.createDateSections(list,WSConstants.ALL_TAB).getTodayIndex();
        adapter.notifyDataSetChanged();
    }


    @Subscribe
    public void ActionPlanViewPagerSwiped(ViewPagerSwiped viewPagerSwiped) {
        if (viewPagerSwiped.getNewPosition() == 1 && todayStartIndex != -1) {
            sortActivityDetails();
           // mLayoutManager.scrollToPositionWithOffset(todayStartIndex, 60);
        }
        //System.out.println(Tag + "ActionPlanSwiped:From" + viewPagerSwiped.getOldPosition() + " To:" + viewPagerSwiped.getNewPosition());
    }


}
