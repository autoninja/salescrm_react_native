package com.salescrm.telephony.sync;

import android.content.Context;
import android.util.Log;

import com.salescrm.telephony.db.C360.C360RealmObject;
import com.salescrm.telephony.db.C360.EmailIds;
import com.salescrm.telephony.db.C360.MobileNumbers;
import com.salescrm.telephony.db.CustomerEmailId;
import com.salescrm.telephony.db.CustomerPhoneNumbers;
import com.salescrm.telephony.db.C360.DetailsPageDbHandler;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AddEmailAddressResponse;
import com.salescrm.telephony.response.AddMobileNumberResponse;
import com.salescrm.telephony.response.CustomerEditResponse;
import com.salescrm.telephony.response.DeleteEmailResponse;
import com.salescrm.telephony.response.DeleteMobileResponse;
import com.salescrm.telephony.response.EditEmailResponse;
import com.salescrm.telephony.response.EditMobileNumberResponse;
import com.salescrm.telephony.response.LocationEditResponse;
import com.salescrm.telephony.response.SaveEditLeadResponse;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bannhi on 15/5/17.
 */

public class SyncC360Data {

    SalesCRMRealmTable salesCRMRealmTable;
    C360RealmObject c360RealmObject;
    Context context;
    Preferences pref;
    Realm realm;
    RealmResults<C360RealmObject> unsyncedLeadDetails;

    public SyncC360Data(Context context) {
        this.context = context;
        pref = Preferences.getInstance();
        pref.load(context);
        realm = Realm.getDefaultInstance();
    }

    /*public void syncCustomerDetails() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                unsyncedLeadDetails = DetailsPageDbHandler.getInstance().getUnsyncedCustomerDetails(realm);
            }
        });
        if (unsyncedLeadDetails != null && !unsyncedLeadDetails.isEmpty()) {
            for (final C360RealmObject temp : unsyncedLeadDetails) {
                salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", temp.getLeadId())
                        .findFirst();
                ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).CustomerEdit("" + temp.getLeadId(),
                        salesCRMRealmTable.getLeadLastUpdated(), temp.getFirstName(), temp.getLastName(),
                        temp.getResidenceAddress(), temp.getResidencePin(),
                        temp.getResidenceAddress(),
                        temp.getOfficeAddress(),
                        temp.getOfficePin(), temp.getCompanyName(), temp.getOccupation(),
                        temp.getGender(), temp.getTitle(),temp.getAge(), new Callback<CustomerEditResponse>() {
                            @Override
                            public void success(CustomerEditResponse customerEditResponse, Response response) {
                                List<Header> headerList = response.getHeaders();
                                for (Header header : headerList) {
                                    Log.e("customeredit", "edit -  " + header.getName() + " - " + header.getValue());
                                    if (header.getName().equalsIgnoreCase("Authorization")) {
                                        ApiUtil.UpdateAccessToken(header.getValue());
                                    }

                                }
                                //customerEditResponse.setTypeOfEdit(typeOfEdit);
                                if (customerEditResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                    realm.beginTransaction();
                                    temp.setCustomerDetailsSynced(true);
                                    realm.commitTransaction();
                                    updateLeadLastUpdated(customerEditResponse.getResult().getLead_last_updated(), salesCRMRealmTable);
                                }

                                // SalesCRMApplication.getBus().post(new CustomerEditResponse(customerEditResponse));
                            }


                            @Override
                            public void failure(RetrofitError error) {
                                //  Log.e("Customedit", "customedit - " + error.getMessage());
                            }
                        });

            }
        }

    }*/

    public void syncCrmSourceDetails() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                unsyncedLeadDetails = DetailsPageDbHandler.getInstance().getUnsyncedCrmSrcDetails(realm);

            }
        });
        if (unsyncedLeadDetails != null && !unsyncedLeadDetails.isEmpty()) {
            for (final C360RealmObject temp : unsyncedLeadDetails) {
                salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", temp.getLeadId())
                        .findFirst();
                ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).ChangeLocationDse(salesCRMRealmTable.getLeadLastUpdated(), "" + temp.getLeadId(), temp.getCrmId(),
                        temp.getDseId(), true, true, true, temp.getLocationId(), new Callback<LocationEditResponse>() {

                            @Override
                            public void success(LocationEditResponse locationEditResponse, Response response) {
                                List<Header> headerList = response.getHeaders();
                                for (Header header : headerList) {
                                    Log.e("locationedit", "locationedit-  " + header.getName() + " - " + header.getValue());
                                    if (header.getName().equalsIgnoreCase("Authorizatiolocationn")) {
                                        ApiUtil.UpdateAccessToken(header.getValue());
                                    }
                                }
                                // SalesCRMApplication.getBus().post(locationEditResponse);
                                if (locationEditResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                    realm.beginTransaction();
                                    temp.setCrmSourceSynced(true);
                                    realm.commitTransaction();
                                    updateLeadLastUpdated(locationEditResponse.getResult().getLead_last_updated(), salesCRMRealmTable);
                                }

                            }


                            @Override
                            public void failure(RetrofitError error) {
                                // SalesCRMApplication.getBus().post(null);
                            }

                        });
            }
        }
    }

    public void syncBuyerTypeDetails() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                unsyncedLeadDetails = DetailsPageDbHandler.getInstance().getUnsyncedBuyerTypeDetails(realm);

            }
        });
        if (unsyncedLeadDetails != null && !unsyncedLeadDetails.isEmpty()) {
            for (final C360RealmObject temp : unsyncedLeadDetails) {
                salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", temp.getLeadId())
                        .findFirst();
            if(salesCRMRealmTable != null){

                ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).SaveLeadEdit
                        ("" + temp.getLeadId(), salesCRMRealmTable.getLeadLastUpdated(), temp.getLocationId(),
                                temp.getDseId(), temp.getEnqSrcId(), temp.getBuyerTypeId(), temp.getExpectedClosingDate(),
                                temp.getModeOfPayment(), temp.getTypeOfCustomerId(), new Callback<SaveEditLeadResponse>() {
                                    @Override
                                    public void success(SaveEditLeadResponse saveEditLeadResponse, Response response) {
                                        List<Header> headerList = response.getHeaders();
                                        for (Header header : headerList) {
                                            Log.e("saveedit", "edit -  " + header.getName() + " - " + header.getValue());
                                            if (header.getName().equalsIgnoreCase("Authorization")) {
                                                ApiUtil.UpdateAccessToken(header.getValue());
                                            }
                                        }
                                        //SaveEditLeadResponse saveeditresponse = (SaveEditLeadResponse) saveEditLeadResponse;
                                        // SalesCRMApplication.getBus().post(saveEditLeadResponse);
                                        if (saveEditLeadResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                            realm.beginTransaction();
                                            temp.setBuyerTypeSynced(true);
                                            realm.commitTransaction();
                                            updateLeadLastUpdated(saveEditLeadResponse.getResult().getLead_last_updated(), salesCRMRealmTable);
                                        }


                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Log.e("SaveEditLead", "saveeditlead - " + error.getMessage());
                                        // SalesCRMApplication.getBus().post(null);
                                    }
                                });
             }
            }
        }
    }

    /**
     * sync unsynced mobile numbers
     */
    public void syncMobileNumbers() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                unsyncedLeadDetails = DetailsPageDbHandler.getInstance().getAllLeadsWithUnsyncedMobileNumbers(realm);
                if (unsyncedLeadDetails != null && !unsyncedLeadDetails.isEmpty() && unsyncedLeadDetails.size()>0) {
                    for (final C360RealmObject temp : unsyncedLeadDetails) {
                        RealmList<MobileNumbers> unsyncedMobileNumbers = temp.getMobileNumbers();
                        if (unsyncedMobileNumbers != null && !unsyncedMobileNumbers.isEmpty() && unsyncedMobileNumbers.size()>0) {
                            /*for (final MobileNumbers mobileNumber : unsyncedMobileNumbers) {
                                handleMobileSync(mobileNumber, temp);
                            }*/
                            handleMobileSync(unsyncedMobileNumbers.get(0), temp);
                        }

                    }
                }

            }
        });
    }

    /**
     * sync unsynced email ids
     */
    public void syncEmailIds() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                unsyncedLeadDetails = DetailsPageDbHandler.getInstance().getAllLeadsWithUnsyncedEmailIds(realm);
                if (unsyncedLeadDetails != null && !unsyncedLeadDetails.isEmpty() && unsyncedLeadDetails.size()>0) {
                    for (final C360RealmObject temp : unsyncedLeadDetails) {
                        RealmList<EmailIds> unsyncedEmailIds = temp.getEmailIds();
                        if (unsyncedEmailIds != null && !unsyncedEmailIds.isEmpty() && unsyncedEmailIds.size()>0) {
                           /* for (final EmailIds emailId : unsyncedEmailIds) {
                                handleEmailSync(emailId, temp);
                            }*/
                            handleEmailSync(unsyncedEmailIds.get(0), temp);
                        }


                    }
                }

            }
        });
    }


    /**
     * handle edit, delete and addition of offline operations on mobile numbers
     * * editting a mobile number offline , to be synced when online
     * 1. createdoffline true indicates, any EDIT to the number will actually add the number,
     * but then a DELETE true flag will have no effect overall and will remove the entry from both the table
     * <p>
     * 2. createdoffline false indicates, any EDIT to the number will actually edit the number,
     * and DELETE true flag will set the delete flag to be synced later on
     * <p>
     * 3. sync flag should be set only for add and edit operation
     *
     * @param mobileNumbers
     */
    private void handleMobileSync(MobileNumbers mobileNumber, C360RealmObject tempC360Obj) {
        if (mobileNumber.isDeletedOffline() && mobileNumber.isCreatedOffline()) {
            //delete from c360 db
            DetailsPageDbHandler.getInstance().deleteMobileNumberFrom360(realm, mobileNumber);
        } else if (mobileNumber.isDeletedOffline() && !mobileNumber.isCreatedOffline()) {
            // call delete api and remove the entry from c360 db
            deleteMobileServiceHandler(mobileNumber, tempC360Obj);
        } else if (mobileNumber.isCreatedOffline()) {
            //call add api and createdOffline flag = false
            addMobileNumberServiceHandler(mobileNumber, tempC360Obj);
        } else if (!mobileNumber.isDeletedOffline() && mobileNumber.isEditedOffline() && !mobileNumber.isCreatedOffline()) {
            //call edit api and edited flag = false;
            editMobileNumberServiceHandler(mobileNumber, tempC360Obj);
        }/* else if (!mobileNumber.isDeletedOffline() && !mobileNumber.isEditedOffline() && mobileNumber.isCreatedOffline()) {
            //call add api created offline flag false
            addMobileNumberServiceHandler(mobileNumber, tempC360Obj);
        }*/
    }

    /**
     * similar to mobile numbers edit and delete and add concept
     *
     * @param emailIds
     */
    private void handleEmailSync(EmailIds emailId, C360RealmObject tempC360Obj) {
        if (emailId.isDeletedOffline() && emailId.isCreatedOffline()) {
            //delete from c360 db
            DetailsPageDbHandler.getInstance().deleteEmailIdFrom360(realm, emailId);
        } else if (emailId.isDeletedOffline() && !emailId.isCreatedOffline()) {
            // call delete api and set delete flag as false
            deleteEmailIdServiceHandler(emailId, tempC360Obj);
        } else if (!emailId.isDeletedOffline() && emailId.isEditedOffline() && emailId.isCreatedOffline()) {
            //call add api and createdOffline flag = false
            addEmailIdServiceHandler(emailId, tempC360Obj);
        } else if (!emailId.isDeletedOffline() && emailId.isEditedOffline() && !emailId.isCreatedOffline()) {
            //call edit api and edited flag = false;
            editEmailIdServiceHandler(emailId, tempC360Obj);
        } /*else if (!emailId.isDeletedOffline() && !emailId.isEditedOffline() && emailId.isCreatedOffline()) {
            //call add api created offline flag false
        }*/
    }

    /**
     * handling delete sync for numbers
     *
     * @param mobileNumber
     */
    private void deleteMobileServiceHandler(final MobileNumbers mobileNumber, final C360RealmObject tempC360Obj) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", mobileNumber.getLeadId())
                .findFirst();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).DeleteNumber(salesCRMRealmTable.getLeadLastUpdated(),
                mobileNumber.getLeadId(), "" + mobileNumber.getLead_phone_mapping_id(), new Callback<DeleteMobileResponse>() {
                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("Deletenumber", "Delete - " + error.getMessage());
                    }

                    @Override
                    public void success(final DeleteMobileResponse deleteMobileResponse, Response response) {
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            Log.e("Deleteresponsel", "delete -  " + header.getName() + " - " + header.getValue());
                            if (header.getName().equalsIgnoreCase("Authorization")) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }
                        }
                        if (deleteMobileResponse != null && deleteMobileResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    CustomerPhoneNumbers result =
                                            realm.where(CustomerPhoneNumbers.class).equalTo("lead_phone_mapping_id", "" + deleteMobileResponse.getLeadMappingId()).findFirst();
                                    if (result != null && result.isValid()) {
                                        result.deleteFromRealm();
                                    }
                                    DetailsPageDbHandler.getInstance().deleteMobileNumberFrom360(realm, mobileNumber);

                                }
                            });
                            updateLeadLastUpdated(deleteMobileResponse.getResult().getLead_last_updated(), salesCRMRealmTable);
                            updateMobileSyncFlagForLead(mobileNumber, tempC360Obj);


                        }


                    }
                });

    }

    /**
     * offline added mobile number sync
     *
     * @param mobileNumber
     * @param tempC360Obj
     */
    private void addMobileNumberServiceHandler(final MobileNumbers mobileNumber, final C360RealmObject tempC360Obj) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", mobileNumber.getLeadId())
                .findFirst();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).AddMobileNumber(salesCRMRealmTable.getLeadLastUpdated(),
                "" + mobileNumber.getPhoneNumber(), mobileNumber.getLeadId(), new Callback<AddMobileNumberResponse>() {
                    @Override
                    public void success(final AddMobileNumberResponse addMobileNumberResponse, Response response) {
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            Log.e("Addmobile", "Action -  " + header.getName() + " - " + header.getValue());
                            if (header.getName().equalsIgnoreCase("Authorization")) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }
                        }

                        realm.beginTransaction();
                        mobileNumber.setCreatedOffline(false);
                        realm.commitTransaction();
                        updateMobileSyncFlagForLead(mobileNumber, tempC360Obj);
                        updateLeadLastUpdated(addMobileNumberResponse.getResult().getLead_last_updated(), salesCRMRealmTable);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        error.printStackTrace();
                        Log.e("Getaddmbilenumber", "addmobilenumber - " + error.getMessage());
                    }
                });

    }

    /**
     * edit sync of an offline editted mobile number
     *
     * @param mobileNumber
     * @param tempC360Obj
     */
    private void editMobileNumberServiceHandler(final MobileNumbers mobileNumber, final C360RealmObject tempC360Obj) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", mobileNumber.getLeadId())
                .findFirst();
        if(salesCRMRealmTable == null) {
            return;
        }
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).EditMobileNumber(salesCRMRealmTable.getLeadLastUpdated(),
                "" + salesCRMRealmTable.getLeadId(), "" + mobileNumber.getLead_phone_mapping_id(), "" + mobileNumber.getPhoneNumber(), new Callback<EditMobileNumberResponse>() {
                    @Override
                    public void success(final EditMobileNumberResponse editResponse, Response response) {
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            if (header.getName().equalsIgnoreCase("Authorization")) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }
                        }
                        final EditMobileNumberResponse editMobileNumberResponse = editResponse.getEditMobileNumberResponse();
                        if (editMobileNumberResponse!=null && editMobileNumberResponse.getResult() != null && editMobileNumberResponse.getResult().getMobileData() != null &&
                                editMobileNumberResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                            for (int i = 0; i < editMobileNumberResponse.getResult().getMobileData().length; i++) {
                                EditMobileNumberResponse.MobileData mobileData = editMobileNumberResponse.getResult().getMobileData()[i];
                                if (mobileData.getStatus().equalsIgnoreCase("PRIMARY")) {
                                    realm.beginTransaction();
                                    mobileNumber.setPhoneNumberStatus("PRIMARY");
                                    mobileNumber.setEditedOffline(false);
                                    realm.commitTransaction();
                                } else {
                                    realm.beginTransaction();
                                    mobileNumber.setPhoneNumberStatus("SECONDARY");
                                    mobileNumber.setEditedOffline(false);
                                    realm.commitTransaction();
                                }
                                if (mobileData.getLead_phone_mapping_id().equalsIgnoreCase("" + mobileNumber.getLead_phone_mapping_id())) {
                                    updateMobileSyncFlagForLead(mobileNumber, tempC360Obj);
                                }
                                updateLeadLastUpdated(editMobileNumberResponse.getResult().getLead_last_updated(), salesCRMRealmTable);


                            }
                        }


                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("Geteditmobile", "editmobilenumber - " + error.getMessage());

                    }
                });

    }

    /**
     * if all mobile numbers are synced , the lead object is overall synced for the mobile numbers
     *
     * @param mobileNumber
     * @param c360RealmObject
     */
    private void updateMobileSyncFlagForLead(final MobileNumbers mobileNumber, final C360RealmObject c360RealmObject) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (DetailsPageDbHandler.getInstance().isAnyUnsyncedMobileNumber(realm, mobileNumber.getLeadId())) {
                    c360RealmObject.setMobileSync(true);
                }
            }
        });

    }

    //////////////////////////////////syncing emaild ids//////////////////////////////////////

    /**
     * api to delete an offline deleted email
     *
     * @param emailIds
     * @param c360RealmObject
     */
    private void deleteEmailIdServiceHandler(final EmailIds emailIds, final C360RealmObject tempC360Obj) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", emailIds.getLeadId())
                .findFirst();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).DeleteEmail(salesCRMRealmTable.getLeadLastUpdated(),
                emailIds.getLeadId(), emailIds.getEmailLeadMappingId(), new Callback<DeleteEmailResponse>() {
                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("Deleteemail", "Delete - " + error.getMessage());
                    }

                    @Override
                    public void success(final DeleteEmailResponse deleteEmailResponse, Response response) {
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            Log.e("Deleteresponsel", "delete -  " + header.getName() + " - " + header.getValue());
                            if (header.getName().equalsIgnoreCase("Authorization")) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }

                        }
                        if (deleteEmailResponse != null && deleteEmailResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    CustomerEmailId result =
                                            realm.where(CustomerEmailId.class).equalTo("lead_email_mapping_id", deleteEmailResponse.getEmailMappingId()).findFirst();
                                    if (result != null && result.isValid()) {
                                        result.deleteFromRealm();
                                    }

                                    DetailsPageDbHandler.getInstance().deleteEmailIdFrom360(realm, emailIds);
                                }
                            });
                            updateEmailSyncFlagForLead(emailIds, tempC360Obj);
                            updateLeadLastUpdated(deleteEmailResponse.getResult().getLead_last_updated(), salesCRMRealmTable);
                        }
                    }
                });

    }

    /**
     * sync unsynced edited email id with the server via api call
     *
     * @param emailId
     * @param tempC360Obj
     */
    private void editEmailIdServiceHandler(final EmailIds emailId, final C360RealmObject tempC360Obj) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", emailId.getLeadId())
                .findFirst();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).EditEmail(salesCRMRealmTable.getLeadLastUpdated(),
                "" + emailId.getLeadId(), emailId.getEmailLeadMappingId(), emailId.getEmail(), new Callback<EditEmailResponse>() {
                    @Override
                    public void success(EditEmailResponse editResponse, Response response) {
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            if (header.getName().equalsIgnoreCase("Authorization")) {
                                ApiUtil.UpdateAccessToken(header.getValue());

                            }
                            final EditEmailResponse editEmailResponse = editResponse.getEditEmailResponse();
                            if (editEmailResponse != null && editEmailResponse.getResult().getEmailData() != null
                                    && editEmailResponse.getResult().getEmailData() != null
                                    && editEmailResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {

                                for (int i = 0; i < editEmailResponse.getResult().getEmailData().length; i++) {
                                    EditEmailResponse.EmailData emailData = editEmailResponse.getResult().getEmailData()[i];
                                    if (emailData.getStatus().equalsIgnoreCase("PRIMARY")) {
                                        realm.beginTransaction();
                                        emailId.setEmailIdStatus("PRIMARY");
                                        emailId.setEditedOffline(false);
                                        realm.commitTransaction();
                                    } else {
                                        realm.beginTransaction();
                                        emailId.setEmailIdStatus("SECONDARY");
                                        emailId.setEditedOffline(false);
                                        realm.commitTransaction();
                                    }
                                    if (emailData.getLead_email_mapping_id().equalsIgnoreCase("" + emailId.getEmailLeadMappingId())) {
                                        updateEmailSyncFlagForLead(emailId, tempC360Obj);
                                    }

                                }
                                updateLeadLastUpdated(editEmailResponse.getResult().getLead_last_updated(), salesCRMRealmTable);
                            }

                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {


                    }
                });
    }

    /**
     * adding email id offline sync
     *
     * @param emailId
     * @param tempC360Obj
     */
    private void addEmailIdServiceHandler(final EmailIds emailId, final C360RealmObject tempC360Obj) {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", emailId.getLeadId())
                .findFirst();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).AddEmailAddress(salesCRMRealmTable.getLeadLastUpdated(),
                "" + emailId.getLeadId(), emailId.getEmail(), new Callback<AddEmailAddressResponse>() {
                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("Getaddemail", "addemail - " + error.getMessage());
                    }

                    @Override
                    public void success(AddEmailAddressResponse addEmailAddressResponse, Response response) {
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            Log.e("Addemail", "Action -  " + header.getName() + " - " + header.getValue());
                            if (header.getName().equalsIgnoreCase("Authorization")) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }
                        }
                        realm.beginTransaction();
                        emailId.setCreatedOffline(false);
                        realm.commitTransaction();
                        updateEmailSyncFlagForLead(emailId, tempC360Obj);
                        updateLeadLastUpdated(addEmailAddressResponse.getResult().getLead_last_updated(), salesCRMRealmTable);
                    }


                });

    }

    /**
     * if all email ids are synced , the lead object is overall synced for the email ids
     *
     * @param emailIds
     * @param c360RealmObject
     */
    private void updateEmailSyncFlagForLead(final EmailIds emailIds, final C360RealmObject c360RealmObject) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (DetailsPageDbHandler.getInstance().isAnyUnsyncedEmailIds(realm, emailIds.getLeadId())) {
                    c360RealmObject.setEmailSync(true);
                }
            }
        });


    }

    /**
     * updating leadlast updated from response
     *
     * @param leadLastUpdated
     * @param salesCRMRealmTable
     */
    private void updateLeadLastUpdated(final String leadLastUpdated, final SalesCRMRealmTable salesCRMRealmTable) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                if (leadLastUpdated != null && !leadLastUpdated.equalsIgnoreCase("")) {
                    salesCRMRealmTable.setLeadLastUpdated(leadLastUpdated);

                }
            }
        });

    }


}
