package com.salescrm.telephony.utils;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.salescrm.telephony.BuildConfig;
import com.salescrm.telephony.activity.SplashActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.interfaces.RestApi;
import com.salescrm.telephony.preferences.Preferences;
import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.realm.Realm;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;

/**
 * Created by Ravindra P on 27-11-2015.
 */
public class
ApiUtil {

//    private static OkHttpClient getUnsafeOkHttpClient() {
//        try {
//            // Create a trust manager that does not validate certificate chains
//            final TrustManager[] trustAllCerts = new TrustManager[] {
//                    new X509TrustManager() {
//                        @Override
//                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
//                        }
//
//                        @Override
//                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
//                        }
//
//                        @Override
//                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                            return new java.security.cert.X509Certificate[]{};
//                        }
//                    }
//            };
//
//            // Install the all-trusting trust manager
//            final SSLContext sslContext = SSLContext.getInstance("SSL");
//            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
//            // Create an ssl socket factory with our all-trusting manager
//            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
//
//            OkHttpClient.Builder builder = new OkHttpClient.Builder();
//            builder.sslSocketFactory(sslSocketFactory);
//            builder.hostnameVerifier(new HostnameVerifier() {
//                @Override
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            });
//
//            OkHttpClient okHttpClient = builder.build();
//            return okHttpClient;
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }

    public static RestApi GetRestApiForCommon() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BuildConfig.SERVER_BASE_URL + "/common")
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.BASIC).setLog(new AndroidLog("API_UTIL"))
              //  .setClient((Client) getUnsafeOkHttpClient())
                .build();
        return restAdapter.create(RestApi.class);
    }

    public static RestApi GetRestApiWithHeader(final String token) {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(100, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(100, TimeUnit.SECONDS);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader(WSConstants.AUTHORIZATION, WSConstants.BEARER + token);
                    }
                })
                .setClient(new OkClient(okHttpClient) {
                    @Override
                    public Response execute(Request request) throws IOException {
                        Response response = super.execute(request);
                        System.out.println("Updating new token");
                        Util.updateHeaders(response.getHeaders());
                        return response;
                    }
                })
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.BASIC).setLog(new AndroidLog("API_UTIL"))
                .setEndpoint(BuildConfig.SERVER_BASE_URL + "/mobile")
                .build();
        return restAdapter.create(RestApi.class);
    }


    public static RestApi getPincodeAddressMappingApi() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(WSConstants.PINCODE_ADDRESS_MAP_API)
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.BASIC).setLog(new AndroidLog("API_UTIL"))
                .build();
        return restAdapter.create(RestApi.class);
    }
    public static RestApi getHereMap() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(WSConstants.HERE_MAP_API_END_POINT)
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.BASIC).setLog(new AndroidLog("API_UTIL"))
                .build();
        return restAdapter.create(RestApi.class);
    }


    public static void UpdateAccessToken(String token) {
        Preferences pref = Preferences.getInstance();
        Preferences.load(SalesCRMApplication.GetAppContext());
        String[] accessToken = token.split("\\s+");
        Preferences.setAccessToken(accessToken[1]);
        Log.e("APiUtil ", "Access Token -  " + accessToken[1]);
    }

    public static void InvalidUserLogout(Activity context, int status) {
        if (status == 0) {
            CleverTapPush.pushEvent(CleverTapConstants.FORCED_LOGOUT);
            Preferences pref = Preferences.getInstance();
            Preferences.load(context);
            Realm realm = Realm.getDefaultInstance();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.deleteAll();
                }

            });
            String fcmId = Preferences.getFcmId();
            Preferences.setIsLogedIn(false);
            Preferences.deleteAll(context);
            Preferences.setFcmId(fcmId);
            context.startActivity(new Intent(context, SplashActivity.class));
            context.finish();
        }

    }

}
