import React, { Component } from "react";
import { View, FlatList, ActivityIndicator, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { updateTasksList } from '../../../actions/tasks_list_actions';
import { Slider } from 'react-native-elements';
import RestClient from '../../../network/rest_client'
import LinearGradient from "react-native-linear-gradient";
import TaskListCard from './taskListCard'

import DateUtils from '../../../utils/DateUtils'
import DateHeader from './DateHeader';

import styles from './tasksList.styles';
import Button from "../../../components/uielements/Button";
import TasksLoader from './TasksLoader';


class TasksTab extends Component {
    state = {
        todaysTabTotal: 0,
        futureTabTotal: 0,
        doneTabTotal: 0
    }
    constructor(props) {
        super(props);
        this._taskTabRef = {};
    }
    refresh() {

    }
    /*
    <Button
                    onPress={() => {
                        this.props.update(this.props.todaysTabTotal, this.props.futureTabTotal, 12)
                    }}
                    title="Add Cold Visit"
                />
    */

    scrollToIndex(index) {
        this._taskTabRef.flatList.scrollToIndex({ index, animated: true });
    }
    pagination = () => {

    }
    render() {
        let { data, loading, tabIndex, when, order, doneLeadStatus, page, totalPages, paginating, scrollToIndex, total, doneCount } = this.props.tab;
        return (
            <View style={{ flex: 1, }}>
                {tabIndex == 0 && !loading && (
                    <View>
                        <Slider
                            animateTransitions={true}
                            value={doneCount}
                            disabled={true}
                            maximumValue={total + doneCount}
                            minimumValue={0}
                            minimumTrackTintColor={"#47BEAB"}
                            maximumTrackTintColor={"transperent"}
                            style={{ marginLeft: 10, marginRight: 10 }}
                            trackStyle={{
                                height: 8, borderRadius: 6, borderWidth: 0.5,
                                borderColor: '#007fff'
                            }}
                            thumbStyle={{ height: 0, width: 0, }}
                        />
                    </View>
                )}

                {loading && (
                    <ActivityIndicator
                        style={styles.loader}
                        color="#fff"
                    />
                )}

                {!loading && data.length > 0 && (
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        contentContainerStyle={{ paddingBottom: 60 }}
                        data={data}
                        ref={ref => { this._taskTabRef.flatList = ref; }}
                        initialNumToRender={7}
                        renderItem={({ item, index }) => {
                            let headerView;
                            if (index == 0) {
                                headerView = <DateHeader tabIndex={tabIndex} date={item.activity.activity_scheduled_date} />
                            }
                            else {
                                let currentDate = item.activity.activity_scheduled_date;
                                let prevDate = data[index - 1].activity.activity_scheduled_date;
                                if (!DateUtils.isSameDate(currentDate, prevDate)) {
                                    headerView = <DateHeader tabIndex={tabIndex} date={currentDate} />
                                }

                            }
                            return (
                                <View>
                                    {headerView}
                                    <TaskListCard
                                        info={this.props.info}
                                        onClickAction={this.props.onClickAction.bind(this)}
                                        scrollToIndex={this.scrollToIndex.bind(this, index)} tabIndex={tabIndex} task={item} />
                                </View>
                            )
                        }
                        }
                        keyExtractor={(item, index) => index + ''}
                        onEndReachedThreshold={0.5}
                        onEndReached={({ distanceFromEnd }) => {
                            console.log('on end reached ' + tabIndex + "::", distanceFromEnd);
                            if (distanceFromEnd >= 0) {
                                if (page < totalPages) {
                                    console.log("Need to call pagination : " + totalPages);
                                    this.props.pagination(tabIndex,
                                        when,
                                        order,
                                        doneLeadStatus,
                                        page + 1,
                                        true
                                    );
                                }

                            }
                        }}
                        onRefresh={() => this.props.refresh()}
                        refreshing={loading}
                        scrollToIndex={scrollToIndex}
                    />)
                }
                {paginating && (<ActivityIndicator
                    style={styles.loader}
                    color="#fff"
                />)}
                {!loading && data.length == 0 && (
                    <View
                        style={{
                            alignItems: "center",
                            justifyContent: "center",
                            alignSelf: 'center',
                            flex: 1
                        }}
                    >
                        <Text style={{ color: '#fff' }}>No tasks</Text>
                        <Button
                            title={"Refresh"}
                            width={100}
                            onPress={() => {
                                this.props.refresh();
                            }}
                        />
                    </View>

                )
                }

            </View>
        )
    }
}
const mapStateToProps = state => {
    return {
        todaysTabTotal: state.tasksListReducer.todaysTabTotal,
        futureTabTotal: state.tasksListReducer.todaysTabTotal,
        doneTabTotal: state.tasksListReducer.todaysTabTotal
    }
}

const mapDispatchToProps = dispatch => {
    return {
        update: (todaysTabTotal, futureTabTotal, doneTabTotal) => {
            dispatch(updateTasksList({ todaysTabTotal, futureTabTotal, doneTabTotal }))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TasksTab)