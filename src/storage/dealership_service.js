
import DealershipModel from '../models/dealership_model';
import {realm} from './realm_schemas';

let DealershipService = {
  findAll: function() {
    return realm.objects('dealership');
  },
  update_dp_url: function(dp_url) {
    realm.write(() => {
    var dealershipsRealm = realm.objects('dealership');
    for (var i = 0; i < dealershipsRealm.length; i++) {
      dealershipsRealm[i].user_dp_url = dp_url;
    }
  })
  },
  save: function(dealership) {
    realm.write(() => {
      dealership.updated_at = new Date();
      realm.create('dealership', dealership, true);
    })
  },
  deleteAll: function() {
    realm.write(() => {
      realm.delete(realm.objects('dealership'));
    })
  },

  // update: function(todo, callback) {
  //   if (!callback) return;
  //   repository.write(() => {
  //     callback();
  //     todo.updatedAt = new Date();
  //   });
  // }
};


module.exports = DealershipService;
