import { StyleSheet, Platform } from "react-native";
export default styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#fff",
    paddingTop: Platform.OS === "ios" ? 20 : 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  icon: {
    width: 24,
    height: 24
  }
});
