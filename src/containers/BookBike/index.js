import React, { Component } from "react";
import {
  View,
  Text,
  ActivityIndicator,
  ScrollView,
  NativeModules,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
  Platform
} from "react-native";
import CustomTextInput from "../../components/createLead/custom_text_input";
import ItemPicker from "../../components/createLead/item_picker";
import PickerList from "../../utils/PickerList";
import Button from "../../components/uielements/Button";

import RestClient from "../../network/rest_client";

import styles from "./BookBike.styles";
import Loader from "../../components/uielements/Loader";
import SuccessView from "../../components/uielements/SuccessView";
import NavigationService from '../../navigation/NavigationService'
const mobile_pattern = /^[1-9]\d{9}/;

export default class BookBike extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileNumber: null,
      customerName: null,
      loading: false,
      showItemPicker: false,
      dataItemPicker: [],
      interestedVehicle: this.getInterestedVehicle(),
      vehicleModel: null,
      vehicleVariant: null,
      vehicleColor: null,
      vehicleFuelType: null,
      leadId: this.getLeadId(),
      leadLastUpdated: this.getLeadLastUpdated(),
      formSubmissionInputData: this.getFormSubmissionInputData()
    };
  }
  getNavigationParam(key) {
    if (this.props.navigation && this.props.navigation.getParam(key)) {
      return this.props.navigation.getParam(key)
    }
    return null;
  }
  getInterestedVehicle() {
    let interestedVehciles = this.getNavigationParam('interestedVehicles');
    if (interestedVehciles) {
      return interestedVehciles;
    }
    return this.props.interestedVehicle
  }

  getLeadId() {
    let leadId = this.getNavigationParam('leadId');
    if (leadId) {
      return leadId;
    }
    return this.props.leadId
  }
  getLeadLastUpdated() {
    let leadLastUpdated = this.getNavigationParam('leadLastUpdated');
    if (leadLastUpdated) {
      return leadLastUpdated;
    }
    return this.props.leadLastUpdated
  }
  getFormSubmissionInputData() {
    let formSubmissionInputData = this.getNavigationParam('formSubmissionInputData');
    if (formSubmissionInputData) {
      return formSubmissionInputData;
    }
    return this.props.formSubmissionInputData
  }
  componentDidMount() {
    console.log(this.getFormSubmissionInputData())
  }
  componentWillUnmount() {

  }
  showAlert(alert) {
    if (Platform.OS == 'android') {
      NativeModules.ReactNativeToAndroid.showToast(alert);
    }
  }
  close(result) {
    if (Platform.OS == 'android') {
      if (result) {
        NativeModules.ReactNativeToAndroid.onBookBikeClose(this.state.leadId, result.killed_activity_ids.toString(), result.lead_last_updated);
      }
      else {
        NativeModules.ReactNativeToAndroid.onBookBikeClose(null, null, null);
      }
    }
    else if (Platform.OS == 'ios') {
      NavigationService.resetScreen("C360MainScreen",
        {
          leadId: this.state.leadId
        });
    }
  }
  submitBookBike() {
    this.setState({ loading: true });
    let {
      mobileNumber,
      customerName,
      vehicleModel,
      vehicleVariant,
      vehicleColor,
      vehicleFuelType,
      leadId,
      leadLastUpdated,
      formSubmissionInputData
    } = this.state;
    let input = {
      customer_mobile: mobileNumber,
      customer_name: customerName,
      model_id: vehicleModel.id,
      variant_id: vehicleVariant.id,
      color_id: vehicleColor.id,
      fuel_type_id: vehicleFuelType.id,
      lead_id: leadId,
      lead_last_updated: leadLastUpdated,
      form_object: formSubmissionInputData,
    };
    new RestClient()
      .postBookBike(input)
      .then(data => {
        if (data) {
          if (data.statusCode == "2002" && data.result) {
            console.log(JSON.stringify(data))
            this.setState({ loading: false, isFormSubmitSuccess: true });
            setTimeout(
              function () {
                this.close(data.result);
              }.bind(this),
              500
            );
            this.showAlert("Booking successfull");
          } else if (data.message) {
            this.setState({ loading: false });
            this.showAlert(data.message);
          } else {
            this.setState({ loading: false });
            this.showAlert("Failed to book bike! Try Again");
          }
        } else {
          this.setState({ loading: false });
          this.showAlert("Failed to book bike! Try Again");
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
        this.showAlert("Failed to book bike! Try Again");
      });
  }

  showPicker(data) {
    this.setState({ showItemPicker: true, dataItemPicker: data });
  }
  onSelect = (fromId, item, payload) => {
    if (fromId == "vehicle_model") {
      this.setState({
        vehicleModel: item,
        vehicleVariant: null,
        vehicleFuelType: null,
        vehicleColor: null,
        showItemPicker: false
      });
    }
    else if (fromId == "vehicle_variant") {
      this.setState({
        vehicleVariant: item,
        vehicleFuelType: { id: item.payload.fuel_type_id, text: item.payload.fuel_type },
        vehicleColor: null,
        showItemPicker: false
      });
    }
    else if (fromId == "vehicle_color") {
      this.setState({
        vehicleColor: item,
        showItemPicker: false
      });
    }
  };
  onCancel = () => {
    this.setState({
      showItemPicker: false
    });
  };
  isFormValid() {
    let {
      mobileNumber,
      customerName,
      vehicleModel,
      vehicleVariant,
      vehicleColor,
    } = this.state;
    return (mobile_pattern.test(mobileNumber)
      && customerName
      && !customerName.isEmpty()
      && vehicleModel
      && !vehicleModel.text.isEmpty()
      && vehicleVariant
      && !vehicleVariant.text.isEmpty()
      && vehicleColor
      && !vehicleColor.text.isEmpty())
  }
  getInterestedVehicleModelsItems() {
    let { interestedVehicle } = this.state;
    let list = [];
    if (interestedVehicle && interestedVehicle.brand_models) {
      interestedVehicle.brand_models.map((model) => {
        list.push({ id: model.model_id, text: model.model_name, payload: { brand_id: interestedVehicle.brand_id, brand_name: interestedVehicle.brand_name } });
      })
    }
    return list;
  }
  getInterestedVehicleVariantsItems(modelItem) {
    let { interestedVehicle } = this.state;
    let list = [];
    if (interestedVehicle && modelItem) {
      interestedVehicle.brand_models.map((model) => {
        if (model.model_id == modelItem.id) {
          model.car_variants.map((variant) => {
            if (list.map(item => item.id).indexOf(variant.variant_id) == -1) {
              list.push({ id: variant.variant_id, text: variant.variant, payload: variant });
            }
          })
        }
      })
    }
    return list;
  }



  getInterestedVehcileColorsItems(modelItem, varaintItem) {
    let { interestedVehicle } = this.state;
    let list = [];
    if (interestedVehicle && modelItem && varaintItem) {
      interestedVehicle.brand_models.map((model) => {
        if (model.model_id == modelItem.id) {
          model.car_variants.map((variant) => {
            if (variant.variant_id == varaintItem.id) {
              list.push({ id: variant.color_id, text: variant.color });
            }
          })
        }
      })
    }
    return list;
  }
  render() {
    let currentLocationHolder = this.state.currentLocationHolder;
    let modelText = "Model *";
    let variantText = "Variant *";
    let colorText = "Color/Description *";
    let {
      mobileNumber,
      customerName,
      showItemPicker,
      dataItemPicker,
      vehicleModel,
      vehicleVariant,
      vehicleColor,
    } = this.state;
    if (vehicleModel && vehicleModel.text) {
      modelText = vehicleModel.text;
    }
    if (vehicleVariant && vehicleVariant.text) {
      variantText = vehicleVariant.text;
    }
    if (vehicleColor && vehicleColor.text) {
      colorText = vehicleColor.text;
    }
    return (

      <TouchableWithoutFeedback onPress={() => {
        this.close();
      }}>
        <View style={styles.overlay}>
          <TouchableWithoutFeedback onPress={() => {

          }}>
            <View style={[styles.main]}>
              <View style={{
                flexDirection: 'row', alignItems: 'flex-end', borderRadius: 5, justifyContent: 'flex-end',
                paddingTop: 10, paddingLeft: 20,
                paddingRight: 20, alignItems: 'center'
              }}>
                <TouchableOpacity
                  hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                  onPress={() => {
                    this.close();
                  }}>
                  <Image
                    source={require('../../images/ic_close_black.png')}
                    style={{ width: 20, height: 20, alignSelf: 'flex-end' }}
                  />
                </TouchableOpacity>
              </View>

              <PickerList
                showFilter={true}
                visible={showItemPicker}
                onSelect={this.onSelect}
                onCancel={this.onCancel}
                data={dataItemPicker}
              />
              <ScrollView>
                <View style={styles.main}>
                  <CustomTextInput
                    selectTextOnFocus={false}
                    showPlaceHolder={true}
                    header="Name"
                    hint="Enter customer name"
                    text={customerName}
                    onInputTextChanged={(id, text, payload) => {
                      this.setState({ customerName: text });
                    }}
                  />

                  <CustomTextInput
                    keyboardType='numeric'
                    showPlaceHolder={true}
                    header="Mobile"
                    hint="Enter mobile number"
                    maxLength={10}
                    text={mobileNumber}
                    onInputTextChanged={(id, text, payload) => {
                      this.setState({ mobileNumber: text });
                    }}
                  />

                  <ItemPicker
                    title={modelText}
                    onClick={this.showPicker.bind(this, {
                      title: "Select vehicle model",
                      fromId: "vehicle_model",
                      listItems: this.getInterestedVehicleModelsItems()
                    })}
                  />

                  <ItemPicker
                    title={variantText}
                    disabled={vehicleModel ? false : true}
                    onClick={this.showPicker.bind(this, {
                      title: "Select Variant",
                      fromId: "vehicle_variant",
                      listItems: this.getInterestedVehicleVariantsItems(vehicleModel)
                    })}
                  />

                  <ItemPicker
                    title={colorText}
                    disabled={vehicleModel && vehicleVariant ? false : true}
                    onClick={this.showPicker.bind(this, {
                      title: "Select Color/ Description",
                      fromId: "vehicle_color",
                      listItems: this.getInterestedVehcileColorsItems(vehicleModel, vehicleVariant)
                    })}
                  />

                  <View style={styles.mapViewHolder}>
                    <View style={{ flexDirection: "row" }}>
                      {this.state.addressFetchingFromLatLong && (
                        <ActivityIndicator
                          style={{ marginLeft: 10 }}
                          size="small"
                          color="#007fff"
                        />
                      )}

                      <Text style={styles.mapViewLocation}>
                        {currentLocationHolder}
                      </Text>
                    </View>
                  </View>
                </View>
              </ScrollView>
              <View>
                <Button
                  disabled={!this.isFormValid()}
                  onPress={() => this.submitBookBike()}
                  title="Book Bike"
                />
              </View>
              {this.state.loading && <Loader isLoading={this.state.loading} />}
              {this.state.isFormSubmitSuccess && (
                <SuccessView text="Booking successfull" />
              )}
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

String.prototype.isEmpty = function () {
  return this.length === 0 || !this.trim();
};
