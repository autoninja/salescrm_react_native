package com.salescrm.telephony.db.events;

import io.realm.RealmList;
import io.realm.RealmObject;

public class EventsLeadSourceDB extends RealmObject {
    private int id;
    private String name;
    private RealmList<EventDB> events;

    public EventsLeadSourceDB() {
    }

    public EventsLeadSourceDB(int id, String name, RealmList<EventDB> events) {
        this.id = id;
        this.name = name;
        this.events = events;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<EventDB> getEvents() {
        return events;
    }

    public void setEvents(RealmList<EventDB> events) {
        this.events = events;
    }
}
