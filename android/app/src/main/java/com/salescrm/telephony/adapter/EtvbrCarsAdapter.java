package com.salescrm.telephony.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.etvbr_location.LocationCarModelDb;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.EtvbrCarsFragment;
import com.salescrm.telephony.fragments.retExcFin.RefCarsFragment;

import io.realm.Realm;

/**
 * Created by prateek on 15/9/17.
 */

public class EtvbrCarsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private Realm realm;
    LocationCarModelDb resultEtvbrCars;
    private int screen = 0;

    public EtvbrCarsAdapter(FragmentActivity activity, LocationCarModelDb locationCarModelDb, Realm realm, int i) {
        this.context = activity;
        this.realm = realm;
        //this.resultEtvbrAbsolutes = resultEtvbrAbsolutes;
        //this.resultEtvbrRelational = resultEtvbrRelational;
        this.resultEtvbrCars = locationCarModelDb;
        this.screen = i;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.etvbr_cars_adapter_view, parent, false);
        return new EtvbrCarsHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(screen == 0) {
            if (EtvbrCarsFragment.CARS_PERCENT) {
                viewForRel(holder, position);
            } else {
                viewForAbsolute(holder, position);
            }
        }else {
            if (RefCarsFragment.CARS_PERCENT) {
                viewForRel(holder, position);
            } else {
                viewForAbsolute(holder, position);
            }
        }

    }

    private void viewForRel(RecyclerView.ViewHolder holder, int position) {
        EtvbrCarsHolder carsHolder = (EtvbrCarsHolder) holder;
        if(position % 2 == 0){
            carsHolder.llCarsEtvbr.setBackgroundColor(Color.parseColor("#243F6D"));
        }else {
            carsHolder.llCarsEtvbr.setBackgroundColor(Color.TRANSPARENT);
        }
        if(screen == 1){
            carsHolder.tvCarsEnquiry.setVisibility(View.GONE);
            carsHolder.tvCarsTDrive.setVisibility(View.GONE);
            carsHolder.tvCarsVisit.setVisibility(View.GONE);
            carsHolder.tvCarsBooking.setVisibility(View.GONE);
            carsHolder.tvCarsLost.setVisibility(View.GONE);
            carsHolder.viewEnquiry.setVisibility(View.GONE);
            carsHolder.viewTdrive.setVisibility(View.GONE);
            carsHolder.viewBooking.setVisibility(View.GONE);
            carsHolder.viewVisit.setVisibility(View.GONE);
            carsHolder.viewLost.setVisibility(View.GONE);

            carsHolder.tvCarsFin.setVisibility(View.VISIBLE);
            carsHolder.tvCarsExch.setVisibility(View.VISIBLE);
            carsHolder.viewFin.setVisibility(View.VISIBLE);
            carsHolder.viewExch.setVisibility(View.VISIBLE);
            carsHolder.tvCarsRetail.setVisibility(View.GONE);
            carsHolder.viewRetail.setVisibility(View.GONE);
            carsHolder.viewFinOut.setVisibility(View.VISIBLE);
            carsHolder.viewPendingBookings.setVisibility(View.VISIBLE);
            carsHolder.viewLiveEnquiries.setVisibility(View.VISIBLE);
            carsHolder.tvCarsFinOut.setVisibility(View.VISIBLE);
            carsHolder.tvCarsPendingBookings.setVisibility(View.VISIBLE);
            carsHolder.tvCarsPendingBookings.setTextColor(Color.parseColor("#7ED321"));
            carsHolder.tvCarsLiveEnquiries.setVisibility(View.VISIBLE);
            carsHolder.tvCarsLiveEnquiries.setTextColor(Color.parseColor("#7ED321"));
        }else {
            carsHolder.tvCarsEnquiry.setVisibility(View.VISIBLE);
            carsHolder.tvCarsTDrive.setVisibility(View.VISIBLE);
            carsHolder.tvCarsVisit.setVisibility(View.VISIBLE);
            carsHolder.tvCarsBooking.setVisibility(View.VISIBLE);
            carsHolder.tvCarsLost.setVisibility(View.VISIBLE);
            carsHolder.viewEnquiry.setVisibility(View.VISIBLE);
            carsHolder.viewTdrive.setVisibility(View.VISIBLE);
            carsHolder.viewBooking.setVisibility(View.VISIBLE);
            carsHolder.viewVisit.setVisibility(View.VISIBLE);
            carsHolder.viewLost.setVisibility(View.VISIBLE);

            carsHolder.tvCarsFin.setVisibility(View.GONE);
            carsHolder.tvCarsFinOut.setVisibility(View.GONE);
            carsHolder.tvCarsPendingBookings.setVisibility(View.GONE);
            carsHolder.tvCarsLiveEnquiries.setVisibility(View.GONE);
            carsHolder.tvCarsExch.setVisibility(View.GONE);
            carsHolder.viewFin.setVisibility(View.GONE);
            carsHolder.viewExch.setVisibility(View.GONE);
            carsHolder.tvCarsRetail.setVisibility(View.VISIBLE);
            carsHolder.viewRetail.setVisibility(View.VISIBLE);
            carsHolder.viewFinOut.setVisibility(View.GONE);
            carsHolder.viewPendingBookings.setVisibility(View.GONE);
            carsHolder.viewLiveEnquiries.setVisibility(View.GONE);
        }

        if(resultEtvbrCars.getCarModels()!= null && position < resultEtvbrCars.getCarModels().size()){
            carsHolder.tvCarsEnquiry.setText(""+resultEtvbrCars.getCarModels().get(position).getRel().getEnquiries());
            carsHolder.tvCarsTDrive.setText(""+resultEtvbrCars.getCarModels().get(position).getRel().getTdrives());
            carsHolder.tvCarsVisit.setText(""+resultEtvbrCars.getCarModels().get(position).getRel().getVisits());
            carsHolder.tvCarsBooking.setText(""+resultEtvbrCars.getCarModels().get(position).getRel().getBookings());
            carsHolder.tvCarsRetail.setText(""+resultEtvbrCars.getCarModels().get(position).getRel().getRetails());
            carsHolder.tvCarsLost.setText(""+resultEtvbrCars.getCarModels().get(position).getRel().getLost_drop());
            carsHolder.tvCarsName.setText(""+resultEtvbrCars.getCarModels().get(position).getInfo().getName());
            carsHolder.tvCarsExch.setText(""+resultEtvbrCars.getCarModels().get(position).getRel().getExchanged());
            carsHolder.tvCarsFin.setText(""+resultEtvbrCars.getCarModels().get(position).getRel().getIn_house_financed());
            carsHolder.tvCarsFinOut.setText("" +resultEtvbrCars.getCarModels().get(position).getRel().getOut_house_financed());
            carsHolder.tvCarsPendingBookings.setText(getReadableData(resultEtvbrCars.getCarModels().get(position).getRel().getPending_bookings()));
            carsHolder.tvCarsLiveEnquiries.setText(getReadableData(resultEtvbrCars.getCarModels().get(position).getRel().getLive_enquiries()));
            carsHolder.tvCarsName.setVisibility(View.VISIBLE);
            carsHolder.tvCarsTotal.setVisibility(View.GONE);

            carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsTDrive.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsVisit.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsBooking.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsRetail.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsLost.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsName.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsExch.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsFin.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsFinOut.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsPendingBookings.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsLiveEnquiries.setTypeface(null, Typeface.NORMAL);

            if(screen == 1){
                carsHolder.viewExch.setVisibility(View.VISIBLE);
                carsHolder.viewFin.setVisibility(View.VISIBLE);
                carsHolder.viewEnquiry.setVisibility(View.GONE);
                carsHolder.viewTdrive.setVisibility(View.GONE);
                carsHolder.viewBooking.setVisibility(View.GONE);
                carsHolder.viewVisit.setVisibility(View.GONE);
                carsHolder.viewRetail.setVisibility(View.GONE);
                carsHolder.viewLost.setVisibility(View.GONE);
                carsHolder.viewFinOut.setVisibility(View.VISIBLE);
                carsHolder.viewPendingBookings.setVisibility(View.VISIBLE);
                carsHolder.viewLiveEnquiries.setVisibility(View.VISIBLE);
            }else {
                carsHolder.viewExch.setVisibility(View.GONE);
                carsHolder.viewFin.setVisibility(View.GONE);
                carsHolder.viewEnquiry.setVisibility(View.VISIBLE);
                carsHolder.viewTdrive.setVisibility(View.VISIBLE);
                carsHolder.viewBooking.setVisibility(View.VISIBLE);
                carsHolder.viewVisit.setVisibility(View.VISIBLE);
                carsHolder.viewRetail.setVisibility(View.VISIBLE);
                carsHolder.viewLost.setVisibility(View.VISIBLE);
                carsHolder.viewFinOut.setVisibility(View.GONE);
                carsHolder.viewPendingBookings.setVisibility(View.GONE);
                carsHolder.viewLiveEnquiries.setVisibility(View.GONE);
            }
        }else {

            if (resultEtvbrCars != null && resultEtvbrCars.getCarModels().size() > 0) {

                carsHolder.tvCarsEnquiry.setText("" + resultEtvbrCars.getLocationRel().getEnquiries());
                carsHolder.tvCarsTDrive.setText("" + resultEtvbrCars.getLocationRel().getTdrives());
                carsHolder.tvCarsVisit.setText("" + resultEtvbrCars.getLocationRel().getVisits());
                carsHolder.tvCarsBooking.setText("" + resultEtvbrCars.getLocationRel().getBookings());
                carsHolder.tvCarsRetail.setText("" + resultEtvbrCars.getLocationRel().getRetails());
                carsHolder.tvCarsLost.setText("" + resultEtvbrCars.getLocationRel().getLost_drop());
                carsHolder.tvCarsExch.setText("" + resultEtvbrCars.getLocationRel().getExchanged());
                carsHolder.tvCarsFin.setText("" + resultEtvbrCars.getLocationRel().getIn_house_financed());
                carsHolder.tvCarsFinOut.setText("" + resultEtvbrCars.getLocationRel().getOut_house_financed());
                carsHolder.tvCarsPendingBookings.setText(getReadableData(resultEtvbrCars.getLocationRel().getPending_bookings()));
                carsHolder.tvCarsLiveEnquiries.setText(getReadableData(resultEtvbrCars.getLocationRel().getLive_enquiries()));

                //carsHolder.tvCarsEnquiry.setText("0");
                //carsHolder.tvCarsTDrive.setText("0");
                //carsHolder.tvCarsVisit.setText("0");
                //carsHolder.tvCarsBooking.setText("0");
                //carsHolder.tvCarsRetail.setText("0");
                carsHolder.tvCarsName.setVisibility(View.GONE);
                carsHolder.tvCarsTotal.setVisibility(View.VISIBLE);
                carsHolder.tvCarsTotal.setText("Total%");
                carsHolder.tvCarsTotal.setTypeface(null, Typeface.BOLD);

                carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsTDrive.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsVisit.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsBooking.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsRetail.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsLost.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsExch.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsFin.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsFinOut.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsPendingBookings.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsLiveEnquiries.setTypeface(null, Typeface.BOLD);

                carsHolder.viewEnquiry.setVisibility(View.GONE);
                carsHolder.viewTdrive.setVisibility(View.GONE);
                carsHolder.viewBooking.setVisibility(View.GONE);
                carsHolder.viewVisit.setVisibility(View.GONE);
                carsHolder.viewRetail.setVisibility(View.GONE);
                carsHolder.viewLost.setVisibility(View.GONE);
                carsHolder.viewExch.setVisibility(View.GONE);
                carsHolder.viewFin.setVisibility(View.GONE);
                carsHolder.viewFinOut.setVisibility(View.GONE);
                carsHolder.viewPendingBookings.setVisibility(View.GONE);
                carsHolder.viewLiveEnquiries.setVisibility(View.GONE);
            } else {

                /*carsHolder.tvCarsEnquiry.setText("" + resultEtvbrCars.get(0).getRelTotal().getEnqTotal());
                carsHolder.tvCarsTDrive.setText("" + resultEtvbrCars.get(0).getRelTotal().getTdTotal());
                carsHolder.tvCarsVisit.setText("" + resultEtvbrCars.get(0).getRelTotal().getVisitsTotal());
                carsHolder.tvCarsBooking.setText("" + resultEtvbrCars.get(0).getRelTotal().getBookTotal());
                carsHolder.tvCarsRetail.setText("" + resultEtvbrCars.get(0).getRelTotal().getRetailTotal());*/

                carsHolder.tvCarsEnquiry.setText("0");
                carsHolder.tvCarsTDrive.setText("0");
                carsHolder.tvCarsVisit.setText("0");
                carsHolder.tvCarsBooking.setText("0");
                carsHolder.tvCarsRetail.setText("0");
                carsHolder.tvCarsLost.setText("0");

                carsHolder.tvCarsName.setVisibility(View.GONE);
                carsHolder.tvCarsTotal.setVisibility(View.VISIBLE);
                carsHolder.tvCarsTotal.setText("Total%");
                carsHolder.tvCarsTotal.setTypeface(null, Typeface.BOLD);

                carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsTDrive.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsVisit.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsBooking.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsRetail.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsLost.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsExch.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsFin.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsFinOut.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsPendingBookings.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsLiveEnquiries.setTypeface(null, Typeface.BOLD);


                carsHolder.viewEnquiry.setVisibility(View.GONE);
                carsHolder.viewTdrive.setVisibility(View.GONE);
                carsHolder.viewBooking.setVisibility(View.GONE);
                carsHolder.viewVisit.setVisibility(View.GONE);
                carsHolder.viewRetail.setVisibility(View.GONE);
                carsHolder.viewLost.setVisibility(View.GONE);
                carsHolder.viewExch.setVisibility(View.GONE);
                carsHolder.viewFin.setVisibility(View.GONE);
                carsHolder.viewFinOut.setVisibility(View.GONE);
                carsHolder.viewPendingBookings.setVisibility(View.GONE);
                carsHolder.viewLiveEnquiries.setVisibility(View.GONE);
            }
        }
        if(screen == 0) {
            if (DbUtils.isBike()) {
                carsHolder.tvCarsVisit.setVisibility(View.GONE);
                carsHolder.tvCarsRetail.setVisibility(View.GONE);
            } else {
                carsHolder.tvCarsVisit.setVisibility(View.VISIBLE);
                carsHolder.tvCarsRetail.setVisibility(View.VISIBLE);
            }
        }
    }

    private String getReadableData(Integer data) {
        if(data==null) {
            return "--";
        }
        return ""+data;
    }

    private void viewForAbsolute(RecyclerView.ViewHolder holder, int position) {
        EtvbrCarsHolder carsHolder = (EtvbrCarsHolder) holder;
        if(position % 2 == 0){
            carsHolder.llCarsEtvbr.setBackgroundColor(Color.parseColor("#243F6D"));
        }else {
            carsHolder.llCarsEtvbr.setBackgroundColor(Color.TRANSPARENT);
        }

        if(screen == 1){
            carsHolder.tvCarsEnquiry.setVisibility(View.GONE);
            carsHolder.tvCarsTDrive.setVisibility(View.GONE);
            carsHolder.tvCarsVisit.setVisibility(View.GONE);
            carsHolder.tvCarsBooking.setVisibility(View.GONE);
            carsHolder.tvCarsLost.setVisibility(View.GONE);
            carsHolder.viewEnquiry.setVisibility(View.GONE);
            carsHolder.viewTdrive.setVisibility(View.GONE);
            carsHolder.viewBooking.setVisibility(View.GONE);
            carsHolder.viewVisit.setVisibility(View.GONE);
            carsHolder.viewLost.setVisibility(View.GONE);

            carsHolder.tvCarsFin.setVisibility(View.VISIBLE);
            carsHolder.tvCarsFinOut.setVisibility(View.VISIBLE);
            carsHolder.tvCarsPendingBookings.setVisibility(View.VISIBLE);
            carsHolder.tvCarsPendingBookings.setTextColor(Color.parseColor("#7ED321"));
            carsHolder.tvCarsLiveEnquiries.setVisibility(View.VISIBLE);
            carsHolder.tvCarsLiveEnquiries.setTextColor(Color.parseColor("#7ED321"));
            carsHolder.tvCarsExch.setVisibility(View.VISIBLE);
            carsHolder.viewFin.setVisibility(View.VISIBLE);
            carsHolder.viewExch.setVisibility(View.VISIBLE);
            carsHolder.tvCarsRetail.setVisibility(View.VISIBLE);
            carsHolder.viewRetail.setVisibility(View.VISIBLE);
            carsHolder.viewFinOut.setVisibility(View.VISIBLE);
            carsHolder.viewPendingBookings.setVisibility(View.VISIBLE);
            carsHolder.viewLiveEnquiries.setVisibility(View.VISIBLE);
        }else {
            carsHolder.tvCarsEnquiry.setVisibility(View.VISIBLE);
            carsHolder.tvCarsTDrive.setVisibility(View.VISIBLE);
            carsHolder.tvCarsVisit.setVisibility(View.VISIBLE);
            carsHolder.tvCarsBooking.setVisibility(View.VISIBLE);
            carsHolder.tvCarsLost.setVisibility(View.VISIBLE);
            carsHolder.viewEnquiry.setVisibility(View.VISIBLE);
            carsHolder.viewTdrive.setVisibility(View.VISIBLE);
            carsHolder.viewBooking.setVisibility(View.VISIBLE);
            carsHolder.viewVisit.setVisibility(View.VISIBLE);
            carsHolder.viewLost.setVisibility(View.VISIBLE);

            carsHolder.tvCarsFin.setVisibility(View.GONE);
            carsHolder.tvCarsFinOut.setVisibility(View.GONE);
            carsHolder.tvCarsPendingBookings.setVisibility(View.GONE);
            carsHolder.tvCarsLiveEnquiries.setVisibility(View.GONE);
            carsHolder.tvCarsExch.setVisibility(View.GONE);
            carsHolder.viewFin.setVisibility(View.GONE);
            carsHolder.viewFinOut.setVisibility(View.GONE);
            carsHolder.viewPendingBookings.setVisibility(View.GONE);
            carsHolder.viewLiveEnquiries.setVisibility(View.GONE);
            carsHolder.viewExch.setVisibility(View.GONE);
            carsHolder.tvCarsRetail.setVisibility(View.VISIBLE);
            carsHolder.viewRetail.setVisibility(View.VISIBLE);
        }
        if(resultEtvbrCars.getCarModels()!= null && position < resultEtvbrCars.getCarModels().size()){

            carsHolder.tvCarsEnquiry.setText(""+resultEtvbrCars.getCarModels().get(position).getAbs().getEnquiries());
            carsHolder.tvCarsTDrive.setText(""+resultEtvbrCars.getCarModels().get(position).getAbs().getTdrives());
            carsHolder.tvCarsVisit.setText(""+resultEtvbrCars.getCarModels().get(position).getAbs().getVisits());
            carsHolder.tvCarsBooking.setText(""+resultEtvbrCars.getCarModels().get(position).getAbs().getBookings());
            carsHolder.tvCarsRetail.setText(""+resultEtvbrCars.getCarModels().get(position).getAbs().getRetails());
            carsHolder.tvCarsLost.setText(""+resultEtvbrCars.getCarModels().get(position).getAbs().getLost_drop());
            carsHolder.tvCarsName.setText(""+resultEtvbrCars.getCarModels().get(position).getInfo().getName());
            carsHolder.tvCarsExch.setText(""+resultEtvbrCars.getCarModels().get(position).getAbs().getExchanged());
            carsHolder.tvCarsFin.setText(""+resultEtvbrCars.getCarModels().get(position).getAbs().getIn_house_financed());
            carsHolder.tvCarsFinOut.setText("" +resultEtvbrCars.getCarModels().get(position).getAbs().getOut_house_financed());
            carsHolder.tvCarsPendingBookings.setText(getReadableData(resultEtvbrCars.getCarModels().get(position).getAbs().getPending_bookings()));
            carsHolder.tvCarsLiveEnquiries.setText(getReadableData(resultEtvbrCars.getCarModels().get(position).getAbs().getLive_enquiries()));
            carsHolder.tvCarsName.setVisibility(View.VISIBLE);
            carsHolder.tvCarsTotal.setVisibility(View.GONE);

            carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsTDrive.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsVisit.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsBooking.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsRetail.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsLost.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsName.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsExch.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsFin.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsFinOut.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsPendingBookings.setTypeface(null, Typeface.NORMAL);
            carsHolder.tvCarsLiveEnquiries.setTypeface(null, Typeface.NORMAL);

            if(screen == 1){
                carsHolder.viewExch.setVisibility(View.VISIBLE);
                carsHolder.viewFin.setVisibility(View.VISIBLE);
                carsHolder.viewFinOut.setVisibility(View.VISIBLE);
                carsHolder.viewPendingBookings.setVisibility(View.VISIBLE);
                carsHolder.viewLiveEnquiries.setVisibility(View.VISIBLE);
                carsHolder.viewEnquiry.setVisibility(View.GONE);
                carsHolder.viewTdrive.setVisibility(View.GONE);
                carsHolder.viewBooking.setVisibility(View.GONE);
                carsHolder.viewVisit.setVisibility(View.GONE);
                carsHolder.viewRetail.setVisibility(View.VISIBLE);
                carsHolder.viewLost.setVisibility(View.GONE);
            }else {
                carsHolder.viewExch.setVisibility(View.GONE);
                carsHolder.viewFin.setVisibility(View.GONE);
                carsHolder.viewFinOut.setVisibility(View.GONE);
                carsHolder.viewPendingBookings.setVisibility(View.GONE);
                carsHolder.viewLiveEnquiries.setVisibility(View.GONE);
                carsHolder.viewEnquiry.setVisibility(View.VISIBLE);
                carsHolder.viewTdrive.setVisibility(View.VISIBLE);
                carsHolder.viewBooking.setVisibility(View.VISIBLE);
                carsHolder.viewVisit.setVisibility(View.VISIBLE);
                carsHolder.viewRetail.setVisibility(View.VISIBLE);
                carsHolder.viewLost.setVisibility(View.VISIBLE);
            }
        }else{
            if(resultEtvbrCars.getCarModels() != null && resultEtvbrCars.getCarModels().size() >0) {

                carsHolder.tvCarsEnquiry.setText("" + resultEtvbrCars.getLocationAbs().getEnquiries());
                carsHolder.tvCarsTDrive.setText("" + resultEtvbrCars.getLocationAbs().getTdrives());
                carsHolder.tvCarsVisit.setText("" + resultEtvbrCars.getLocationAbs().getVisits());
                carsHolder.tvCarsBooking.setText("" + resultEtvbrCars.getLocationAbs().getBookings());
                carsHolder.tvCarsRetail.setText("" + resultEtvbrCars.getLocationAbs().getRetails());
                carsHolder.tvCarsLost.setText("" + resultEtvbrCars.getLocationAbs().getLost_drop());
                carsHolder.tvCarsExch.setText("" + resultEtvbrCars.getLocationAbs().getExchanged());
                carsHolder.tvCarsFin.setText("" + resultEtvbrCars.getLocationAbs().getIn_house_financed());
                carsHolder.tvCarsFinOut.setText("" + resultEtvbrCars.getLocationAbs().getOut_house_financed());
                carsHolder.tvCarsPendingBookings.setText(getReadableData(resultEtvbrCars.getLocationAbs().getPending_bookings()));
                carsHolder.tvCarsLiveEnquiries.setText(getReadableData(resultEtvbrCars.getLocationAbs().getLive_enquiries()));

                //carsHolder.tvCarsEnquiry.setText("0");
                //carsHolder.tvCarsTDrive.setText("0");
                //carsHolder.tvCarsVisit.setText("0");
                //carsHolder.tvCarsBooking.setText("0");
                //carsHolder.tvCarsRetail.setText("0");
                carsHolder.tvCarsName.setVisibility(View.GONE);
                carsHolder.tvCarsTotal.setVisibility(View.VISIBLE);
                carsHolder.tvCarsTotal.setText("Total");
                carsHolder.tvCarsTotal.setTypeface(null, Typeface.BOLD);

                carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsTDrive.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsVisit.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsBooking.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsRetail.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsLost.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsExch.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsFin.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsFinOut.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsPendingBookings.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsLiveEnquiries.setTypeface(null, Typeface.BOLD);

                carsHolder.viewEnquiry.setVisibility(View.GONE);
                carsHolder.viewTdrive.setVisibility(View.GONE);
                carsHolder.viewBooking.setVisibility(View.GONE);
                carsHolder.viewVisit.setVisibility(View.GONE);
                carsHolder.viewRetail.setVisibility(View.GONE);
                carsHolder.viewLost.setVisibility(View.GONE);
                carsHolder.viewExch.setVisibility(View.GONE);
                carsHolder.viewFin.setVisibility(View.GONE);
                carsHolder.viewFinOut.setVisibility(View.GONE);
                carsHolder.viewPendingBookings.setVisibility(View.GONE);
                carsHolder.viewLiveEnquiries.setVisibility(View.GONE);

            }else {

                carsHolder.tvCarsEnquiry.setText("0");
                carsHolder.tvCarsTDrive.setText("0");
                carsHolder.tvCarsVisit.setText("0");
                carsHolder.tvCarsBooking.setText("0");
                carsHolder.tvCarsRetail.setText("0");
                carsHolder.tvCarsLost.setText("0");

                carsHolder.tvCarsName.setVisibility(View.GONE);
                carsHolder.tvCarsTotal.setVisibility(View.VISIBLE);
                carsHolder.tvCarsTotal.setText("Total");
                carsHolder.tvCarsTotal.setTypeface(null, Typeface.BOLD);

                carsHolder.tvCarsEnquiry.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsTDrive.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsVisit.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsBooking.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsRetail.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsLost.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsExch.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsFin.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsFinOut.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsPendingBookings.setTypeface(null, Typeface.BOLD);
                carsHolder.tvCarsLiveEnquiries.setTypeface(null, Typeface.BOLD);

                carsHolder.viewEnquiry.setVisibility(View.GONE);
                carsHolder.viewTdrive.setVisibility(View.GONE);
                carsHolder.viewBooking.setVisibility(View.GONE);
                carsHolder.viewVisit.setVisibility(View.GONE);
                carsHolder.viewRetail.setVisibility(View.GONE);
                carsHolder.viewLost.setVisibility(View.GONE);
                carsHolder.viewExch.setVisibility(View.GONE);
                carsHolder.viewFin.setVisibility(View.GONE);
                carsHolder.viewFinOut.setVisibility(View.GONE);
                carsHolder.viewPendingBookings.setVisibility(View.GONE);
                carsHolder.viewLiveEnquiries.setVisibility(View.GONE);

            }
        }
        if(screen == 0) {
            if (DbUtils.isBike()) {
                carsHolder.tvCarsVisit.setVisibility(View.GONE);
                carsHolder.tvCarsRetail.setVisibility(View.GONE);
            } else {
                carsHolder.tvCarsVisit.setVisibility(View.VISIBLE);
                carsHolder.tvCarsRetail.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        if(resultEtvbrCars.isValid() && resultEtvbrCars.getCarModels().isValid()) {
            return resultEtvbrCars != null ? resultEtvbrCars.getCarModels().size() + 1 : 0;
        }else{
            return 0;
        }
    }


    public class EtvbrCarsHolder extends RecyclerView.ViewHolder{

        TextView tvCarsEnquiry, tvCarsTDrive, tvCarsVisit, tvCarsBooking, tvCarsRetail, tvCarsLost, tvCarsExch, tvCarsFin, tvCarsFinOut, tvCarsPendingBookings, tvCarsLiveEnquiries;
        TextView tvCarsName, tvCarsTotal;
        LinearLayout llCarsEtvbr;
        View viewEnquiry, viewTdrive, viewVisit, viewBooking, viewRetail, viewLost, viewExch, viewFin, viewFinOut, viewPendingBookings, viewLiveEnquiries;

        public EtvbrCarsHolder(View itemView) {
            super(itemView);

            tvCarsEnquiry = (TextView) itemView.findViewById(R.id.txt_car_enquiry);
            tvCarsTDrive = (TextView) itemView.findViewById(R.id.txt_car_tdrive);
            tvCarsVisit = (TextView) itemView.findViewById(R.id.txt_car_visit);
            tvCarsBooking = (TextView) itemView.findViewById(R.id.txt_car_booking);
            tvCarsRetail = (TextView) itemView.findViewById(R.id.txt_car_retail);
            tvCarsLost = (TextView) itemView.findViewById(R.id.txt_car_lost);
            tvCarsExch = (TextView) itemView.findViewById(R.id.txt_car_exch);
            tvCarsFin = (TextView) itemView.findViewById(R.id.txt_car_fin);
            tvCarsFinOut = (TextView) itemView.findViewById(R.id.txt_car_fin_out);
            tvCarsPendingBookings = itemView.findViewById(R.id.txt_car_pending_bookings);
            tvCarsLiveEnquiries = itemView.findViewById(R.id.txt_car_live_enquiries);

            tvCarsName = (TextView) itemView.findViewById(R.id.tv_car_name);
            tvCarsTotal = (TextView) itemView.findViewById(R.id.tv_car_total);

            llCarsEtvbr = (LinearLayout) itemView.findViewById(R.id.ll_cars_etvbr);
            viewEnquiry = (View) itemView.findViewById(R.id.view_enquiry);
            viewTdrive = (View) itemView.findViewById(R.id.view_tdrive);
            viewVisit = (View) itemView.findViewById(R.id.view_visit);
            viewBooking = (View) itemView.findViewById(R.id.view_booking);
            viewRetail = (View) itemView.findViewById(R.id.view_retail);
            viewLost = (View) itemView.findViewById(R.id.view_lost);
            viewExch = (View) itemView.findViewById(R.id.view_exch);
            viewFin = (View) itemView.findViewById(R.id.view_fin);
            viewFinOut = (View) itemView.findViewById(R.id.view_fin_out);
            viewPendingBookings = (View) itemView.findViewById(R.id.view_pending_bookings);
            viewLiveEnquiries = (View) itemView.findViewById(R.id.view_live_enquiries);
        }
    }

}
