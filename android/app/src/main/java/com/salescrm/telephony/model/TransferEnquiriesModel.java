package com.salescrm.telephony.model;

import com.salescrm.telephony.interfaces.TransferEnqModelChangeListener;
import com.salescrm.telephony.interfaces.TransferEnquiriesDistributeListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by bharath on 19/8/17.
 */

public class TransferEnquiriesModel {

    private static TransferEnquiriesModel transferEnquiriesModel = new TransferEnquiriesModel();
    private static TransferEnqModelChangeListener listener;
    private static TransferEnquiriesDistributeListener distributeListener;
    private List<Item> carModelList;
    private List<Item> enqSourceList;
    private List<Item> customerTypeList;
    private List<Item> enquiryStageList;
    private List<Item> userList;
    private Set<String> selectedLeadList;
    private List<String> orderedSelectedLeadList =new ArrayList<>();
    private List<Item> userListTo = new ArrayList<>();
    private List<Item> distributeList;
    private String locationId;

    public static TransferEnquiriesModel getInstance() {
        return transferEnquiriesModel;
    }

    public static TransferEnquiriesModel getInstance(TransferEnqModelChangeListener data) {
        listener = data;
        return transferEnquiriesModel;
    }

    public static TransferEnquiriesModel getInstance(TransferEnquiriesDistributeListener data) {
        distributeListener = data;
        return transferEnquiriesModel;
    }

    public List<Item> getUserList() {
        return userList;
    }

    public void setUserList(List<Item> userList) {
        this.userList = userList;
    }

    public List<Item> getUserListTo() {
        return userListTo;
    }

    public void setUserListTo(List<Item> userListTo) {
        this.userListTo = userListTo;
    }

    public void setUserListFrom(int position, Item item) {
        for (Item itemData : userList) {
            if (itemData.getId().equalsIgnoreCase(item.getId())) {
                itemData.setChecked(item.isChecked);
            }
        }
        // userList.set(position,item);
        if (listener != null) {
            listener.onDataChanged();
        }
    }

    public void setUserListTo(int position, Item item) {
        for (Item itemData : userListTo) {
            if (itemData.getId().equalsIgnoreCase(item.getId())) {
                itemData.setChecked(item.isChecked);
            }
        }
        if (listener != null) {
            listener.onDataChanged();
        }
    }

    public List<Item> getDistributeList() {
        return distributeList;
    }

    public void setDistributeList(List<Item> distributeList) {
        this.distributeList = distributeList;
    }

    public void setDistributeList(int position, Item item) {
        distributeList.set(position, item);
    }

    public Set<String> getSelectedLeadList() {
        return selectedLeadList;
    }

    public void setSelectedLead(Set<String> selectedLeadList) {
        this.selectedLeadList = selectedLeadList;
    }

    public List<String> getOrderedSelectedLeadList() {
        return orderedSelectedLeadList;
    }

    public void setOrderedSelectedLeadList(List<String> orderedSelectedLeadList) {
        this.orderedSelectedLeadList = orderedSelectedLeadList;
    }

    public void userItemChanged() {
        if (listener != null) {
            listener.onChangeSelectedItem();
        }

        List<Item> userListTo = new ArrayList<>();
        Item prevItem = null;
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).isHeader() || !userList.get(i).isChecked()) {
                Item item = new Item();
                item.setId(userList.get(i).getId());
                item.setTitle(userList.get(i).getTitle());
                item.setImgUrl(userList.get(i).getImgUrl());
                item.setChecked(false);
                item.setHeader(userList.get(i).isHeader());
                userListTo.add(item);
                if (prevItem != null && prevItem.isHeader() && userList.get(i).isHeader()) {
                    userListTo.remove(prevItem);
                }
                prevItem = item;

            }
        }
        if (userListTo.get(userListTo.size() - 1).isHeader()) {
            userListTo.remove(userListTo.size() - 1);
        }

        this.userListTo = userListTo;

    }

    public void userItemToChanged() {
        if (distributeListener != null) {
            distributeListener.onDataChanged();
        }
    }

    public void enquiriesChangedByFilters() {
        if (distributeListener != null) {
            distributeListener.onDataChanged();
        }
    }

    public List<Item> getCarModelList() {
        return carModelList;
    }

    public void setCarModelList(List<Item> carModelList) {
        this.carModelList = carModelList;
    }

    public List<Item> getEnqSourceList() {
        return enqSourceList;
    }

    public void setEnqSourceList(List<Item> enqSourceList) {
        this.enqSourceList = enqSourceList;
    }

    public List<Item> getCustomerTypeList() {
        return customerTypeList;
    }

    public void setCustomerTypeList(List<Item> customerTypeList) {
        this.customerTypeList = customerTypeList;
    }

    public List<Item> getEnquiryStageList() {
        return enquiryStageList;
    }

    public void setEnquiryStageList(List<Item> enquiryStageList) {
        this.enquiryStageList = enquiryStageList;
    }

    public int getCheckedCount(List<Item> items) {
        int count = 0;
        for (int i = 0; i < (items == null ? 0 : items.size()); i++) {

            if (items.get(i).isChecked()) {
                count += 1;
            }

        }
        return count;
    }

    public void removeSelectedLead(String string) {
        if (selectedLeadList != null) {
            for (String s : selectedLeadList) {
                if (s.equalsIgnoreCase(string)) {
                    selectedLeadList.remove(s);
                    return;
                }
            }
        }
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationId() {
        return locationId;
    }

    public class SelectedLead {
        private String leadId;

        public SelectedLead(String leadId) {
            this.leadId = leadId;
        }

        public String getLeadId() {
            return leadId;
        }

        public void setLeadId(String leadId) {
            this.leadId = leadId;
        }
    }

    public class Item {
        private String title;
        private String imgUrl;
        private boolean isHeader;
        private boolean isChecked;
        private String id;

        public Item() {
        }

        public Item(String id, String title, boolean isHeader) {
            this.id = id;
            this.title = title;
            this.isHeader = isHeader;
        }

        public Item(String id, String title, String imgUrl, boolean isHeader, boolean isChecked) {
            this.title = title;
            this.imgUrl = imgUrl;
            this.isHeader = isHeader;
            this.isChecked = isChecked;
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public boolean isHeader() {
            return isHeader;
        }

        public void setHeader(boolean header) {
            isHeader = header;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }
    }
}
