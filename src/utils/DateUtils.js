import Moment from 'moment';
let DateUtils = {
    isBefore: function (date1, targetDate) {
        return Moment(date1).isBefore(targetDate);
    },
    isSameDate: function (date1, date2) {
        return Moment(date1).isSame(date2, 'day')
            && Moment(date1).isSame(date2, 'month')
            && Moment(date1).isSame(date2, 'year');
    },
    diff: function (date1, date2) {
        return Moment(date1).startOf('day').diff(date2, 'days');
    }

};

module.exports = DateUtils;
