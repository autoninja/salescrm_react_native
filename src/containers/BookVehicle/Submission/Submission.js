import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, FlatList, Platform } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import { NavigationActions } from 'react-navigation'
import styles from './Submission.styles';
import Toast, { DURATION } from 'react-native-easy-toast';
import Button from "../../../components/uielements/Button";
import { BOOKING_STAGES } from '../BookingUtils/BookingUtils'
import NavigationService from '../../../navigation/NavigationService';
import CustomTextInput from "../../../components/createLead/custom_text_input";
import CustomDateTimePicker from '../../../utils/DateTimePicker'
import Utils from '../../../utils/Utils'
import Loader from "../../../components/uielements/Loader";
import SuccessView from "../../../components/uielements/SuccessView";
import RestClient from '../../../network/rest_client'

const DEFAULT_DATE_TIME_PICKER = {
    selectedDate: undefined,
    minimumDate: undefined,
    maximumDate: undefined,
    visibility: false,
    mode: 'datetime',
    payload: null
}
export default class Submission extends Component {
    constructor(props) {
        super(props);
        let { dateTime, remarks } = this.getNavigationParam();
        this.state = {
            loading: false,
            dateTimePicker: DEFAULT_DATE_TIME_PICKER,
            dateTime,
            remarks,
            info: this.props.info,
            initData: this.getInitData(),
            requireRefreshC360: false
        }
    }

    getNavigationParam() {
        let dateTime = null;
        let remarks = null;
        let followUpInfo = this.props.navigation.getParam('followUpInfo', null);
        if (followUpInfo) {
            dateTime = followUpInfo.dateTime;
            remarks = followUpInfo.remarks;
        }
        return { dateTime, remarks };
    }
    isReschedule() {
        return this.props.navigation.getParam('reschedule', false);
    }
    getInitData() {
        let isReschedule = this.isReschedule();
        let initData = {
            dateHeader: "Enter Date",
            remarksHeader: "Remarks",
        }
        let maxDate = new Date();
        maxDate.setDate(maxDate.getDate() + 10);
        switch (parseInt(this.props.info.stageId)) {
            case BOOKING_STAGES.STAGE_INIT:
                initData.dateHeader = "Post Booking Follow Up Date";
                initData.remarksHeader = "Scheme offered";

                initData.maximumDate = maxDate;
                break;
            case BOOKING_STAGES.STAGE_BOOKED:
                if (isReschedule) {
                    initData.dateHeader = "Post Booking Follow Up Date";
                    initData.maximumDate = maxDate;
                }
                else {
                    initData.dateHeader = "Expected Delivery Date";
                }
                break;
            case BOOKING_STAGES.STAGE_INVOICED:
                if (isReschedule) {
                    initData.dateHeader = "Expected Delivery Date";
                    initData.maximumDate = maxDate;
                }
        }
        return initData;
    }
    showAlert(msg) {
        this.refs.toast.show(msg);
    }
    onApiError(msg) {
        this.setState({ loading: false });
        this.showAlert(msg);
    }
    rescheduleTask(dateTime, remarks) {

        this.setState({ loading: true, requireRefreshC360: true })
        let { c360Information, stageId, leadCarId, booking_id } = this.props.info;

        let data = {
            lead_car_id: leadCarId,
            lead_car_stage_id: stageId,
            reschedule_date: dateTime,
            remarks,
            lead_id: '',
            booking_id: '',
            enquiry_id: '',
            invoice_id: ''
        }
        if (c360Information) {
            if (c360Information.customerDetails) {
                data.lead_id = parseInt(c360Information.customerDetails.lead_id);
            }
            if (c360Information.interestedCarsDetails) {
                c360Information.interestedCarsDetails.map((interestedCar) => {
                    if (interestedCar.lead_car_id == leadCarId) {
                        if (interestedCar.carDmsData && interestedCar.carDmsData.length > 0) {
                            dmsData = interestedCar.carDmsData.filter((dms => dms.booking_id == booking_id))[0];
                            data.booking_id = dmsData.booking_id;
                            data.enquiry_id = dmsData.enquiry_id;
                            data.invoice_id = dmsData.invoice_id;
                        }
                    }
                });
            }
        }
        new RestClient().rescheduleTask(data).then((data) => {
            if (data
                && data.statusCode == "2002"
                && data.result) {
                this.setState({
                    loading: false
                })
                this.showAlert("Task rescheduled");
                this.props.callbacks.goBack();
                this.props.callbacks.onApiSubmission();
            }
            else if (data
                && data.message) {
                this.onApiError(data.message);
            }
            else {
                this.onApiError("Failed to update, Please try again later");
            }
        }).
            catch((error) => {
                this.onApiError("Failed to update, Please try again later");
            })
    }
    onSubmit(dateTime, remarks) {
        if (this.isReschedule()) {
            //Submission here only
            this.rescheduleTask(dateTime, remarks);
        }
        else {
            this.props.navigation.state.params.onSubmit({ dateTime, remarks });
            this.props.callbacks.goBack(this.props.navigation);
        }
    }
    showAlert(message) {
        try {
            this.refs.toast.show(message);
        }
        catch (e) {

        }
    }
    showDatePicker() {

    }

    isDataValid(dateTime, remarks) {
        if (dateTime && remarks) {
            return true;
        }
        return false;
    }

    render() {
        let backImage = require("../../../images/ic_close_black.png");
        let {
            dateTimePicker,
            dateTime,
            remarks,
            initData,
            loading,
            requireRefreshC360
        } = this.state;
        return (
            <View style={styles.main}>
                <LinearGradient
                    colors={["#2e5e86", "#2e3486"]}
                    style={styles.linearGradient}>
                    <View>

                        <CustomDateTimePicker
                            date={dateTimePicker.selectedDate ? dateTimePicker.selectedDate : new Date()}
                            minimumDate={dateTimePicker.minimumDate}
                            maximumDate={dateTimePicker.maximumDate}
                            dateTimePickerVisibility={dateTimePicker.visibility}
                            datePickerMode={dateTimePicker.mode}
                            payload={dateTimePicker.payload}
                            handleDateTimePicked={(date, payload) => {
                                // this.onDateSelect(date, payload);
                                this.setState({
                                    dateTime: date,
                                    dateTimePicker: DEFAULT_DATE_TIME_PICKER
                                })
                            }}
                            hideDateTimePicked={() => {
                                this.setState({
                                    dateTimePicker: DEFAULT_DATE_TIME_PICKER
                                })
                            }}
                        />

                        <View style={styles.header}>
                            <TouchableOpacity
                                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                                style={styles.closeButton}
                                onPress={() => {
                                    if (this.isReschedule()) {
                                        //going back using parentNavigation
                                        this.props.callbacks.goBack()
                                        if (requireRefreshC360) {
                                            this.props.callbacks.onApiSubmission();
                                        }

                                    }
                                    else {
                                        this.props.callbacks.goBack(this.props.navigation)
                                    }
                                }}>
                                <Image
                                    source={backImage}
                                    style={{ width: 16, height: 16 }}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginTop: 20 }}>
                            <Text style={styles.title}>{initData.dateHeader}</Text>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        dateTimePicker: {
                                            selectedDate: dateTime ? dateTime : new Date(),
                                            maximumDate: initData.maximumDate,
                                            minimumDate: new Date(),
                                            visibility: true,
                                            mode: 'datetime',
                                        }
                                    })
                                }}
                            >
                                <Text style={styles.date}>{dateTime ? Utils.getReadableDateTime(dateTime) : "Pick a date"}</Text>
                            </TouchableOpacity>

                            <Text style={[styles.title, { marginBottom: -10, marginTop: 20 }]}>{initData.remarksHeader}</Text>

                            <CustomTextInput
                                lightmode={true}
                                showPlaceHolder={false}
                                hint={"Enter"}
                                text={remarks}
                                onInputTextChanged={(key, text, payload) => {
                                    // this.setValuesToItem(key, text, mainIndex)
                                    this.setState({ remarks: text })
                                }}
                                onClickErrorButton={() => {

                                }}
                                textAlign={'center'}
                            />
                        </View>

                        <View style={{ marginTop: 20 }}>
                            <Button
                                disabled={!this.isDataValid(dateTime, remarks)}
                                title={"Submit"}
                                onPress={() => {
                                    this.onSubmit(dateTime, remarks);
                                }}
                            />
                        </View>

                    </View>
                </LinearGradient>
                {loading && <Loader isLoading={loading} />}
                <Toast
                    positionValue={200}
                    ref="toast"
                    style={{ backgroundColor: "#FF0000" }}
                    position="bottom"
                    fadeInDuration={1000}
                    fadeOutDuration={1000}
                    opacity={0.8}
                    textStyle={{ color: "white" }}
                />
            </View>
        )
    }
}