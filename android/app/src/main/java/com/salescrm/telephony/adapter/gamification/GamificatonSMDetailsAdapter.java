package com.salescrm.telephony.adapter.gamification;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.gamification.GamificationSMProfileActivity;
import com.salescrm.telephony.activity.gamification.GamificationTeamLeaderProfileActivity;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.gamification.ManagerLeaderBoardResponse;
import com.salescrm.telephony.response.gamification.TeamLeaderBoardResponse;
import com.salescrm.telephony.utils.FabricUtils;
import com.salescrm.telephony.utils.Util;

import java.util.HashMap;

/**
 * Created by nndra on 04-Jan-18.
 */

public class GamificatonSMDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    Activity activity;
    AlertDialog alertDialog;
    ManagerLeaderBoardResponse managerLeaderBoardResponse;
    // RealmList<PendingStatsDB> pendingStatsDBRealmList;
    private Preferences pref;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_ITEM = 1;


    public GamificatonSMDetailsAdapter(Activity activity, ManagerLeaderBoardResponse managerLeaderBoardResponse) {
        this.activity = activity;
        this.managerLeaderBoardResponse = managerLeaderBoardResponse;
        this.pref = Preferences.getInstance();
        pref.load(activity);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.team_cons_game_row, null);
        return new GamificatonSMDetailsAdapter.ItemHolder(view);

    }
    @Override
    public int getItemViewType (int position) {
        return TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final int holderPosition = position ;
        if(holder instanceof GamificatonSMDetailsAdapter.ItemHolder){

            ManagerLeaderBoardResponse.Managers manager = managerLeaderBoardResponse.getResult().getManagers().get(position);

            ((ItemHolder) holder).dse_name_tv.setText(manager.getManager_name());
            ((ItemHolder) holder).txt_team_avg_point.setText(manager.getManager_average_points());
            ((ItemHolder) holder).badge_one.setVisibility(View.GONE);

            ((ItemHolder) holder).serial_number_tv.setText(""+(position+1));

            if(manager.getSpotlight().equalsIgnoreCase("1")){
              ((ItemHolder) holder).background_ll.setBackgroundColor(Color.parseColor("#384C69"));
                ((ItemHolder) holder).goarrow.setImageResource(R.drawable.arrow_grey_right);
            }else{
                ((ItemHolder) holder).background_ll.setBackgroundColor(Color.parseColor("#FF0D284D"));
                ((ItemHolder) holder).goarrow.setImageResource(R.drawable.blue_arrow);
            }

            ((ItemHolder) holder).text_user.setText(""+manager.getManager_name().toUpperCase().substring(0, 1));

            if(Util.isNotNull(manager.getManager_photo_url())) {
                ((ItemHolder) holder).imgUser.setVisibility(View.VISIBLE);

                Glide.with(activity).
                        load(manager.getManager_photo_url())
                        .asBitmap()
                        .centerCrop()
                        .listener(new RequestListener<String, Bitmap>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                ((ItemHolder) holder).imgUser.setVisibility(View.GONE);
                                ((ItemHolder) holder).text_user.setVisibility(View.VISIBLE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                ((ItemHolder) holder).imgUser.setVisibility(View.VISIBLE);
                                ((ItemHolder) holder).text_user.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(new BitmapImageViewTarget(((ItemHolder) holder).imgUser) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                if(resource!=null ) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    ((ItemHolder) holder).imgUser.setImageDrawable(circularBitmapDrawable);
                                }
                            }
                        });

            }
            else {
                ((ItemHolder) holder).text_user.setVisibility(View.VISIBLE);
                ((ItemHolder) holder).imgUser.setVisibility(View.GONE);

            }

            ((ItemHolder) holder).background_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Check if we're running on Android 5.0 or higher
                    Intent intent = new Intent(activity, GamificationSMProfileActivity.class);
                    intent.putExtra("manager_id",""+managerLeaderBoardResponse.getResult().getManagers().get(holderPosition).getManager_id());
                    intent.putExtra("called_from",1);
                    intent.putExtra("POINTS",manager.getManager_average_points());
                    intent.putExtra("USER_NAME",manager.getManager_name());
                    intent.putExtra("PROFILE_URL",manager.getManager_photo_url());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        // Apply activity transition
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(activity,
                                Pair.create( (View)((ItemHolder) holder).dse_name_tv, activity.getString(R.string.TRANSITION_LEADER_BOARD_NAME)),
                                Pair.create( (View)((ItemHolder) holder).imgUser, activity.getString(R.string.TRANSITION_LEADER_BOARD_PIC)),
                                Pair.create( (View)((ItemHolder) holder).txt_team_avg_point, activity.getString(R.string.TRANSITION_LEADER_BOARD_POINT)));
                           // create the transition animation - the images in the layouts
                        // of both activities are defined with android:transitionName="robot"
                        activity.startActivity(intent, options.toBundle());
                    } else {
                        // Swap without transition
                        activity.startActivity(intent);
                    }

                    String leaderBoardType;

                    if(manager.getSpotlight().equalsIgnoreCase("1")) {
                        leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_MANAGER_BREAK_UP_OWN;
                    }
                    else {
                        leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_MANAGER_BREAK_UP_OTHERS;
                    }

                    HashMap<String, Object> hashMap = new HashMap<String, Object>();
                    hashMap.put(CleverTapConstants.EVENT_LEADER_BOARD_KEY_TYPE,
                            leaderBoardType);
                    CleverTapPush.pushEvent(CleverTapConstants.EVENT_LEADER_BOARD, hashMap);
                    //Fabric Events
                    FabricUtils.sendEvent(pref, "LeaderBoard-"+pref.getDealerName(),
                            "Type", leaderBoardType);


                }
            });

        }
    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        if(managerLeaderBoardResponse!=null) {
           // System.out.println("Gamification Team - "+managerLeaderBoardResponse.getResult());
            return managerLeaderBoardResponse.getResult().getManagers().size();
        }
        return 0;
    }


    private class ItemHolder extends RecyclerView.ViewHolder {

        TextView dse_name_tv, text_user,serial_number_tv,txt_team_avg_point;
        ImageView imgUser,badge_one;
        LinearLayout background_ll;
        ImageView goarrow;
        private TextView tvBestEverCount;

        public ItemHolder(View itemView) {
            super(itemView);
            txt_team_avg_point = (TextView) itemView.findViewById(R.id.team_gamification_txt_team_avg_point);
            serial_number_tv = (TextView) itemView.findViewById(R.id.team_gamification_serial_number);
            dse_name_tv = (TextView) itemView.findViewById(R.id.team_gamification_dse_name);
            imgUser = (ImageView) itemView.findViewById(R.id.team_gamification_img_user);
            badge_one = (ImageView) itemView.findViewById(R.id.team_gamification_badge_one) ;
            goarrow = (ImageView) itemView.findViewById(R.id.team_gamification_go_arrow) ;
            text_user = (TextView) itemView.findViewById(R.id.team_gamification_text_user);
            background_ll = (LinearLayout) itemView.findViewById(R.id.team_gamification_background_ll);

        }
    }
}
