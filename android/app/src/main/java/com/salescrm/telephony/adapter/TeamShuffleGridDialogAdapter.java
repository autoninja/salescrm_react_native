package com.salescrm.telephony.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.salescrm.telephony.R;
import com.salescrm.telephony.model.TeamShuffleModel;
import com.salescrm.telephony.utils.CircleTransform;
import com.salescrm.telephony.utils.Util;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by bharath on 5/9/17.
 */

public class TeamShuffleGridDialogAdapter extends RecyclerView.Adapter<TeamShuffleGridDialogAdapter.GridViewHolder> {
    private Context mContext;
    private List<TeamShuffleModel.Item> list;
    private TeamShuffleGridListener listener;

    public TeamShuffleGridDialogAdapter(Context mContext, List<TeamShuffleModel.Item> list, TeamShuffleGridListener listener) {
        this.mContext = mContext;
        this.list = list;
        this.listener = listener;
    }


    @Override
    public GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GridViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_grid_card_user, parent, false));
    }

    @Override
    public void onBindViewHolder(GridViewHolder holder, final int position) {
        final TeamShuffleModel.Item item = list.get(holder.getAdapterPosition());

     /*   Picasso.with(mContext).load(item.getImgUrl())
                .placeholder(R.drawable.ic_person_white_48dp)
                .transform(new CircleTransform()).into(holder.imageUser);*/

        setImage(item.getImgUrl(),holder.imageUser,holder.tvUserInitial);

        if (item.getType() == 1) {
            holder.tvUserInitial.setBackground(ContextCompat.getDrawable(mContext, R.drawable.mail_background_oval));
            holder.imageUser.setBackground(ContextCompat.getDrawable(mContext, R.drawable.mail_background_oval));
        } else if (item.getType() == 2) {
            holder.tvUserInitial.setBackground(ContextCompat.getDrawable(mContext, R.drawable.call_background_oval));
            holder.imageUser.setBackground(ContextCompat.getDrawable(mContext, R.drawable.call_background_oval));
        }
        if (item.isClicked()) {
            holder.tvUser.setTextColor(Color.WHITE);
            holder.linearLayout.setBackgroundColor(Color.parseColor("#1F3B5B"));
        } else {
            holder.tvUser.setTextColor(Color.parseColor("#FF1F3C5B"));
            holder.linearLayout.setBackgroundColor(Color.parseColor("#D1DADADA"));
        }
        holder.tvUser.setText(item.getTitle());
        holder.tvUserInitial.setText(item.getTitle() != null && item.getTitle().length() > 0 ? String.valueOf(item.getTitle().charAt(0)) : "");
    /*    if (Util.isNotNull(item.getImgUrl())) {
            holder.imageUser.setVisibility(View.VISIBLE);
            holder.tvUserInitial.setVisibility(View.GONE);

        } else {
            holder.imageUser.setVisibility(View.GONE);
            holder.tvUserInitial.setVisibility(View.VISIBLE);
        }*/
        // setImage(item.getImgUrl(), holder.imageUser, holder.tvUserInitial);


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onTeamShuffleDialogGridClicked(position, item);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    private void setImage(String url, final ImageView imageView, final TextView tvInitial) {
        Glide.with(mContext).
                load(url)
                .asBitmap()
                .centerCrop()
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                        imageView.setVisibility(View.GONE);
                        tvInitial.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        imageView.setVisibility(View.VISIBLE);
                        tvInitial.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        if(resource!=null && mContext!=null && mContext.getResources()!=null) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            imageView.setImageDrawable(circularBitmapDrawable);
                        }
                    }
                });
    }

    public interface TeamShuffleGridListener {
        void onTeamShuffleDialogGridClicked(int position, TeamShuffleModel.Item item);
    }

    class GridViewHolder extends RecyclerView.ViewHolder {
        private TextView tvUser;
        private TextView tvUserInitial;
        private ImageView imageUser;
        private LinearLayout linearLayout;

        public GridViewHolder(View itemView) {
            super(itemView);
            tvUser = (TextView) itemView.findViewById(R.id.tv_team_shuffle_user_dialog);
            tvUserInitial = (TextView) itemView.findViewById(R.id.tv_team_shuffle_user_dialog_initial);
            imageUser = (ImageView) itemView.findViewById(R.id.img_team_shuffle_user_dialog);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.ll_team_shuffle_dialog_user);
        }
    }
}
