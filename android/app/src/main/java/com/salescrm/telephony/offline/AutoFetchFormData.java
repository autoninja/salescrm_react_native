package com.salescrm.telephony.offline;

import android.content.Context;

import com.google.gson.Gson;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.FormObjectDb;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.interfaces.AutoFetchFormListener;
import com.salescrm.telephony.model.FormObjectsInputs;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.FormObjectsRetrofit;
import com.salescrm.telephony.response.SplitFormObjectsRetrofit;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 6/12/16.
 * Call this to get the forms data and activities data when submitting form
 */
public class AutoFetchFormData {
    public AutoFetchFormListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private String TAG = "FetchFormData";

    public AutoFetchFormData(AutoFetchFormListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        this.pref.load(context);
    }

    public void call(RealmResults<SalesCRMRealmTable> data, boolean isDoneFormFirst) {

        //Getting form response not necessary, now form fetch happens in form-rendering only
        if(true){
            listener.onAutoFormsDataFetched(true, data);
            return;
        }

        data= data.where().equalTo("isLeadActive",1).findAll();
        if(data.size()==0){
            listener.onAutoFormsDataFetched(true, data);
            return;
        }
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            if(isDoneFormFirst){
                getFormsData(data);
            }
            else {
                getActivitiesData(data);
            }

        } else {
            System.out.println("No Internet Connection");
        }
    }


    private void getFormsData(final RealmResults<SalesCRMRealmTable> data) {
        FormObjectsInputs formObjectsInputs = new FormObjectsInputs();
        List<FormObjectsInputs.Lead_data> lead_dataList = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            SalesCRMRealmTable currentItem = data.get(i);

            if (currentItem.getActivityId()!=WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY_ID
                    && currentItem.getActivityId()!=WSConstants.TaskActivityName.ALLOT_DSE_ID) {

                FormObjectsInputs.Lead_data leadDataDone = new FormObjectsInputs().new Lead_data();
                leadDataDone.setAction_id(WSConstants.FormAction.PRE_DONE + "");
                leadDataDone.setLead_id(currentItem.getLeadId()+"");
                leadDataDone.setScheduled_activity_id(currentItem.getScheduledActivityId()+"");
                lead_dataList.add(leadDataDone);


            }


        }
        if(lead_dataList.size()==0){
           getActivitiesData(data);
        }
        formObjectsInputs.setLead_data(lead_dataList);

        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetSplitFormObjects(formObjectsInputs, new Callback<SplitFormObjectsRetrofit>() {
            @Override
            public void success(final SplitFormObjectsRetrofit formObjectsRetrofit, Response response) {
                Util.updateHeaders(response.getHeaders());
                if (!formObjectsRetrofit.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    System.out.println(TAG + "Success:0" + formObjectsRetrofit.getMessage());
                    listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.FORM_OBJECT);

                }else {
                    if (formObjectsRetrofit.getResult() != null) {
                        System.out.println(TAG + "Success:1" + formObjectsRetrofit.getMessage());


                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {


                                for (int i = 0; i < formObjectsRetrofit.getResult().size(); i++) {
                                    SplitFormObjectsRetrofit.Result.FormObject formObject = formObjectsRetrofit.getResult().get(i).getForm_object();
                                    if( formObject != null){
                                    final SplitFormObjectsRetrofit.Result.FormObject.InnerFormObject preFormObject = formObjectsRetrofit.getResult().get(i).getForm_object().getPre_form_object();
                                    final SplitFormObjectsRetrofit.Result.FormObject.InnerFormObject postFormObject = formObjectsRetrofit.getResult().get(i).getForm_object().getPost_form_object();

                                    if (preFormObject != null) {
                                        preFormObject.setLeadId(formObjectsRetrofit.getResult().get(i).getLead_id());

                                        preFormObject.setAction_id(WSConstants.FormAction.PRE_DONE);

                                        RealmResults<FormObjectDb> realmDelete = realm.where(FormObjectDb.class).equalTo("action_id",
                                                WSConstants.FormAction.PRE_DONE)
                                                .equalTo("scheduled_activity_id",
                                                        formObjectsRetrofit.getResult().get(i).getScheduled_activity_id())
                                                .findAll();
                                        System.out.println("Form data:size:" + realmDelete.size());
                                        if (realmDelete.size() > 0) {
                                            realmDelete.deleteAllFromRealm();
                                        }

                                        preFormObject.setScheduled_activity_id(formObjectsRetrofit.getResult().get(i).getScheduled_activity_id());
                                        realm.createObjectFromJson(FormObjectDb.class, new Gson().toJson(preFormObject));

                                        RealmResults<FormObjectDb> realmCount = realm.where(FormObjectDb.class).equalTo("action_id",
                                                WSConstants.FormAction.PRE_DONE)
                                                .equalTo("scheduled_activity_id",
                                                        formObjectsRetrofit.getResult().get(i).getScheduled_activity_id())
                                                .findAll();
                                        System.out.println("Pre form count::" + realmCount.size());
                                    }

                                    if (postFormObject != null) {
                                        postFormObject.setLeadId(formObjectsRetrofit.getResult().get(i).getLead_id());

                                        postFormObject.setAction_id(WSConstants.FormAction.POST_DONE);

                                        RealmResults<FormObjectDb> realmDelete = realm.where(FormObjectDb.class)
                                                .equalTo("action_id", WSConstants.FormAction.POST_DONE)
                                                .equalTo("scheduled_activity_id",
                                                        formObjectsRetrofit.getResult().get(i).getScheduled_activity_id())
                                                .findAll();
                                        if (realmDelete.size() > 0) {
                                            realmDelete.deleteAllFromRealm();
                                        }
                                        postFormObject.setScheduled_activity_id(formObjectsRetrofit.getResult().get(i).getScheduled_activity_id());
                                        realm.createObjectFromJson(FormObjectDb.class, new Gson().toJson(postFormObject));

                                    }

                                }
                            }
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                System.out.println("Success of hello!!!");
                                getActivitiesData(data);
                            }
                        }, new Realm.Transaction.OnError() {
                            @Override
                            public void onError(Throwable error) {
                                     System.out.println(error.toString());
                                    listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.FORM_OBJECT);

                            }
                        });



                    } else {
                        System.out.println(TAG + "Success:2" + formObjectsRetrofit.getMessage());
                        listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.FORM_OBJECT);

                    }
                }

            }

            @Override
            public void failure(RetrofitError error) {
                listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.FORM_OBJECT);

            }
        });


    }

    private void insertFormsData(final FormObjectsRetrofit formObjectsRetrofit, final int action_id) {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {


                for (int i = 0; i < formObjectsRetrofit.getResult().size(); i++) {
                    final FormObjectsRetrofit.Result.FormObject formObject = formObjectsRetrofit.getResult().get(i).getForm_object();
                    if (formObject != null) {
                        formObject.setLeadId(formObjectsRetrofit.getResult().get(i).getLead_id());
                        if (action_id == WSConstants.FormAction.ADD_ACTIVITY) {
                            formObject.setAction_id(WSConstants.FormAction.ADD_ACTIVITY);

                            RealmResults<FormObjectDb> realmDelete = realm.where(FormObjectDb.class).equalTo("action_id",
                                    WSConstants.FormAction.ADD_ACTIVITY)
                                    .equalTo("leadId", formObjectsRetrofit.getResult().get(i).getLead_id())
                                    .findAll();
                            System.out.println("Activity data:size:" + realmDelete.size());
                            if (realmDelete.size() > 0) {
                                realmDelete.deleteAllFromRealm();
                            }
                        } else {
                            formObject.setAction_id(formObjectsRetrofit.getResult().get(i).getAction_id());

                            RealmResults<FormObjectDb> realmDelete = realm.where(FormObjectDb.class).equalTo("action_id",
                                    formObjectsRetrofit.getResult().get(i).getAction_id())
                                    .equalTo("scheduled_activity_id", formObjectsRetrofit.getResult().get(i).getScheduled_activity_id())
                                    .findAll();
                            System.out.println("Form data:size:" + realmDelete.size());
                            if (realmDelete.size() > 0) {
                                realmDelete.deleteAllFromRealm();
                            }
                        }


                        formObject.setScheduled_activity_id(formObjectsRetrofit.getResult().get(i).getScheduled_activity_id());
                        realm.createObjectFromJson(FormObjectDb.class, new Gson().toJson(formObject));

                    }
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                if(action_id==WSConstants.FormAction.ADD_ACTIVITY){
                    listener.onAutoFormsDataFetched(true,null);
                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                if(action_id == WSConstants.FormAction.ADD_ACTIVITY){
                    listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.MANUAL_ACTIVITY);
                }
                if(action_id==-1){
                    listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.FORM_OBJECT);
                }

            }
        });
    }


    private void getActivitiesData(final RealmResults<SalesCRMRealmTable> data) {
        FormObjectsInputs formObjectsInputs = new FormObjectsInputs();
        List<FormObjectsInputs.Lead_data> lead_dataList = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            SalesCRMRealmTable currentItem = data.get(i);
            FormObjectsInputs.Lead_data leadData = new FormObjectsInputs().new Lead_data();
            leadData.setLead_id(currentItem.getLeadId()+"");
            if (currentItem.getActivityId()==WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY_ID) {
                leadData.setSchedule_type("2");
            } else {
                leadData.setSchedule_type("1");
            }
            lead_dataList.add(leadData);

        }
        if(lead_dataList.size()==0){
            listener.onAutoFormsDataFetched(true,data);
            return;
        }
        formObjectsInputs.setLead_data(lead_dataList);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetAllManualActivities(formObjectsInputs, new Callback<FormObjectsRetrofit>() {
            @Override
            public void success(FormObjectsRetrofit formObjectsRetrofit, Response response) {
                Util.updateHeaders(response.getHeaders());
                if (!formObjectsRetrofit.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    System.out.println(TAG + "Success:0" + formObjectsRetrofit.getMessage());
                    listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.MANUAL_ACTIVITY);

                } else {
                    if (formObjectsRetrofit.getResult() != null) {
                        System.out.println(TAG + "Success:1" + formObjectsRetrofit.getMessage());
                        insertFormsData(formObjectsRetrofit, WSConstants.FormAction.ADD_ACTIVITY);

                    } else {
                        System.out.println(TAG + "Success:2" + formObjectsRetrofit.getMessage());
                        listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.MANUAL_ACTIVITY);

                    }
                }

            }

            @Override
            public void failure(RetrofitError error) {
                listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.MANUAL_ACTIVITY);
            }
        });
    }

    public void downloadActivities(String lead_id, String s) {
        FormObjectsInputs formObjectsInputs = new FormObjectsInputs();
        List<FormObjectsInputs.Lead_data> lead_dataList = new ArrayList<>();
            FormObjectsInputs.Lead_data leadDataFirst = new FormObjectsInputs().new Lead_data();
            leadDataFirst.setLead_id(lead_id+"");
            leadDataFirst.setSchedule_type(s);
            lead_dataList.add(leadDataFirst);

            formObjectsInputs.setLead_data(lead_dataList);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetAllManualActivities(formObjectsInputs, new Callback<FormObjectsRetrofit>() {
            @Override
            public void success(FormObjectsRetrofit formObjectsRetrofit, Response response) {
                Util.updateHeaders(response.getHeaders());
                if (!formObjectsRetrofit.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    System.out.println(TAG + "Success:0" + formObjectsRetrofit.getMessage());
                    listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.MANUAL_ACTIVITY);

                } else {
                    if (formObjectsRetrofit.getResult() != null) {
                        System.out.println(TAG + "Success:1" + formObjectsRetrofit.getMessage());
                        insertFormsData(formObjectsRetrofit, WSConstants.FormAction.ADD_ACTIVITY);

                    } else {
                        System.out.println(TAG + "Success:2" + formObjectsRetrofit.getMessage());
                        listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.MANUAL_ACTIVITY);

                    }
                }

            }

            @Override
            public void failure(RetrofitError error) {
                listener.onAutoFormsDataError(null,WSConstants.OfflineAPIRequest.MANUAL_ACTIVITY);
            }
        });
    }
}
