package com.salescrm.telephony.db.ref_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by prateek on 8/11/17.
 */

public class RefLocation extends RealmObject {


    @SerializedName("branch_head")
    @Expose
    private Boolean branch_head;
    @SerializedName("location_id")
    @Expose
    private Integer location_id;
    @SerializedName("location_name")
    @Expose
    private String location_name;
    @SerializedName("location_abs")
    @Expose
    private RefLocationAbs location_abs;
    @SerializedName("location_rel")
    @Expose
    private RefLocationRel location_rel;
    @SerializedName("location_targets")
    @Expose
    private RefLocationTargets location_targets;
    @SerializedName("sales_managers")
    @Expose
    private RealmList<RefSalesManager> sales_managers = null;

    public Boolean getBranchHead() {
        return branch_head;
    }

    public void setBranchHead(Boolean branchHead) {
        this.branch_head = branchHead;
    }

    public Integer getLocationId() {
        return location_id;
    }

    public void setLocationId(Integer locationId) {
        this.location_id = locationId;
    }

    public String getLocationName() {
        return location_name;
    }

    public void setLocationName(String locationName) {
        this.location_name = locationName;
    }

    public RefLocationAbs getLocationAbs() {
        return location_abs;
    }

    public void setLocationAbs(RefLocationAbs locationAbs) {
        this.location_abs = locationAbs;
    }

    public RefLocationRel getLocationRel() {
        return location_rel;
    }

    public void setLocationRel(RefLocationRel locationRel) {
        this.location_rel = locationRel;
    }

    public RefLocationTargets getLocationTargets() {
        return location_targets;
    }

    public void setLocationTargets(RefLocationTargets locationTargets) {
        this.location_targets = locationTargets;
    }

    public RealmList<RefSalesManager> getSalesManagers() {
        return sales_managers;
    }

    public void setSalesManagers(RealmList<RefSalesManager> salesManagers) {
        this.sales_managers = salesManagers;
    }
}
