import React from 'react';
import {
    Text,
    View, Platform,
    ScrollView, TouchableOpacity, Image, ActivityIndicator,
    KeyboardAvoidingView,
    NativeModules
} from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import Toast, { DURATION } from 'react-native-easy-toast'
import _ from 'lodash'
import PickerList from '../../../utils/PickerList'
import CustomDateTimePicker from '../../../utils/DateTimePicker'
import RestClient from '../../../network/rest_client'
import styles from './BookVehicleMain.styles'
import BookingSectionView from './BookingSectionView';
import ItemPicker from '../../../components/createLead/item_picker'
import CustomTextInput from "../../../components/createLead/custom_text_input";
import RadioButton from '../../../components/uielements/RadioButton';
import Utils from '../../../utils/Utils'
import BookingModel from './BookingModel';
import SeverModel from './ServerModel'
import { BOOKING_ROUTES, BOOKING_STAGES } from '../BookingUtils/BookingUtils'

import Loader from "../../../components/uielements/Loader";
import SuccessView from "../../../components/uielements/SuccessView";

const DEFAULT_DATE_TIME_PICKER = {
    selectedDate: undefined,
    minimumDate: undefined,
    maximumDate: undefined,
    visibility: false,
    mode: 'date',
    payload: null
}
const DEFAULT_LIST_PICKER = {
    visibility: false,
    showFilter: false,
    data: {}
}
export default class BookVehicleMain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initApiCallFinished: false,
            loading: true,
            interestedVehicles: this.props.interestedVehicles,
            form_object: this.props.form_object,
            info: this.getInfo(),
            formData: this.getFormData(),
            bookingSections: [],
            totalBookings: this.props.navigation.getParam('totalBookings', 1),
            currentBookingPage: this.props.navigation.getParam('currentBookingPage', 1),
            dateTimePicker: DEFAULT_DATE_TIME_PICKER,
            listPicker: DEFAULT_LIST_PICKER,
            followUpInfo: null,
            actionVisibility: false,
            requireRefreshC360: false
        }
    }
    componentDidMount() {
        this.initApiCall();
    }
    getFormData() {
        let formData = new Array(this.props.navigation.getParam('totalBookings', 1));
        if (this.props.navigation.getParam('formData')) {
            formData = this.props.navigation.getParam('formData');
        }
        return formData;
    }
    action(action) {
        if (action == "exit") {
            let {
                currentBookingPage,
                requireRefreshC360
            } = this.state;
            if (currentBookingPage > 1) {
                this.props.callbacks.goBack(this.props.navigation)
            }
            else {
                this.props.callbacks.goBack()
                if (requireRefreshC360) {
                    this.props.callbacks.onApiSubmission();
                }

            }
        }
        else if (action == "init_api_call") {
            this.initApiCall();
        }
        else if (action == "update") {
            this.mainAction();
        }
    }
    showAlert(msg) {
        this.refs.toast.show(msg);
    }
    showErrorPopup(title, yes, no) {
        this.props.navigation.navigate('AutoDialog', {
            data: {
                title,
                yes: yes ? yes.title : "Yes",
                no: no ? no.title : "No"
            },
            onNoButtonPress: () => {
                if (no) {
                    this.action(no.action);
                }
            },
            onYesButtonPress: () => {
                if (yes) {
                    this.action(yes.action);

                }

            },

        });
    }
    initError() {
        this.setState({
            loading: false,
        })
        this.showErrorPopup(
            "Failed to initialize, Please try later", {
            title: "Try Again",
            action: "init_api_call"
        },
            {
                title: "Close",
                action: "exit"
            },
        )
    }
    initApiCall() {
        this.setState({ loading: true, failed: null })
        new RestClient().getVinAllocationStatus().then((data) => {
            if (data
                && data.statusCode == "2002"
                && data.result
                && data.result.vin_allocation_statuses) {
                let { info, currentBookingPage, formData } = this.state;
                info.vinAllocationStatuses = data.result.vin_allocation_statuses;
                let initModel = BookingModel.getBookingSections(
                    info,
                    this.props.interestedVehicles,
                    this.props.userRoles
                );
                formData[currentBookingPage - 1] = initModel.sections;
                this.setState({
                    initApiCallFinished: true,
                    loading: false,
                    info,
                    formData,
                    actionVisibility: initModel.actionVisibility
                })

            }
            else {
                this.initError();
            }
        }).
            catch((error) => {
                console.error(error);
                this.initError();
            })
    }
    getInfo() {
        return this.props.info;
    }
    expandField(expandItem) {
        let { formData, currentBookingPage } = this.state;
        formData[currentBookingPage - 1].map((item) => {
            if (item.id == expandItem.id) {
                item.expand = !item.expand
            }
            else {
                item.expand = false;
            }
        })
        this.setState({ formData });
    }
    setValuesToItem(key, value, mainIndex) {
        let { formData, currentBookingPage } = this.state;
        let updatedSection = formData[currentBookingPage - 1][mainIndex];
        updatedSection.items.map((item) => {
            if (item.key == key) {
                item.value = value;
                item.valid = true;
                if (item.mandatory) {
                    item.valid = value ? true : false;
                }
                if (item.validation && value) {
                    item.valid = new RegExp(item.validation).test(value);
                }
            }
        })
        formData[currentBookingPage - 1][mainIndex] = updatedSection
        this.setState({ formData })

    }
    onDateSelect(date, payload) {
        let { formData, currentBookingPage } = this.state;
        let { mainIndex, key } = payload;
        let updatedSection = formData[currentBookingPage - 1][mainIndex];
        updatedSection.items.map((item) => {
            if (item.key == key) {
                item.value = date;
                item.valid = true
            }
        })
        formData[currentBookingPage - 1][mainIndex] = updatedSection
        this.setState({
            formData,
            dateTimePicker: DEFAULT_DATE_TIME_PICKER
        })
    }

    onSelectListPicker = (key, value, payload) => {
        let mainIndex = payload.mainIndex;
        let { formData, currentBookingPage } = this.state;
        let updatedSection = formData[currentBookingPage - 1][mainIndex];
        let updatingItem;
        updatedSection.items.map((item) => {
            if (item.key == key) {
                item.value = value;
                item.valid = true;
                updatingItem = item;
            }

        })
        if (updatingItem
            && updatingItem.dependencies
            && updatingItem.dependencies.length > 0) {
            updatedSection.items.map((item) => {
                updatingItem.dependencies.map((dependentItem) => {
                    if (item.key == dependentItem.key) {
                        if (dependentItem.trigger == "update_list") {
                            item.listItems = dependentItem.getListItems(value);
                            item.value = null;
                            item.valid = false;
                        }
                    }
                })
            })
        }

        if (updatingItem
            && updatingItem.actionValues
            && updatingItem.actionValues.length > 0) {
            updatedSection.items.map((item) => {
                updatingItem.actionValues.map((actionValue) => {
                    if (value && value.id == actionValue.value) {
                        actionValue.actions.map((action) => {
                            if (item.key == action.key) {
                                item.visible = action.visible;
                            }
                        })
                    }
                })
            })
        }

        formData[currentBookingPage - 1][mainIndex] = updatedSection
        this.setState({ formData, listPicker: DEFAULT_LIST_PICKER })
    }
    showPicker(data, showFilter) {
        this.setState({
            listPicker: {
                visibility: true,
                showFilter: showFilter ? true : false,
                data
            }
        })
    }
    getView(bookingSectionItem, mainIndex) {
        if (bookingSectionItem.visible) {
            switch (bookingSectionItem.view) {
                case 'picker': {
                    let title = "Select";
                    if (bookingSectionItem.value && bookingSectionItem.value.text) {
                        title = bookingSectionItem.value.text;
                    }
                    return (
                        <ItemPicker
                            key={bookingSectionItem.key}
                            disabled={bookingSectionItem.valid && !bookingSectionItem.editable}
                            showError={!bookingSectionItem.valid}
                            title={title}
                            showHeaderText={bookingSectionItem.showHeader}
                            headerText={bookingSectionItem.headerText}
                            onClick={() => {
                                this.showPicker({
                                    title: bookingSectionItem.pickerTitle,
                                    fromId: bookingSectionItem.key,
                                    listItems: bookingSectionItem.listItems,
                                    payload: { mainIndex },
                                    selected: bookingSectionItem.value
                                }
                                )
                            }
                            }
                            onClickErrorButton={() => {
                            }}
                        />
                    )
                }
                case 'edit_text': {
                    return (
                        <CustomTextInput
                            key={bookingSectionItem.key}
                            autoCapitalize={bookingSectionItem.autoCapitalize ? bookingSectionItem.autoCapitalize : "sentences"}
                            keyboardType={bookingSectionItem.keyboardType ? bookingSectionItem.keyboardType : "default"}
                            maxLength={bookingSectionItem.maxLength ? bookingSectionItem.maxLength : Number.MAX_VALUE}
                            showPlaceHolder={bookingSectionItem.showHeader}
                            showError={!bookingSectionItem.valid}
                            editable={bookingSectionItem.editable || !bookingSectionItem.valid}
                            header={bookingSectionItem.headerText}
                            hint={bookingSectionItem.hint}
                            text={bookingSectionItem.value + ""}
                            from={bookingSectionItem.key}
                            onInputTextChanged={(key, text, payload) => {
                                if (bookingSectionItem.keyboardType
                                    && bookingSectionItem.keyboardType == "numeric") {
                                    if (Number.isInteger(Number(text))) {
                                        this.setValuesToItem(key, text.trim(), mainIndex)
                                    }
                                }
                                else {
                                    this.setValuesToItem(key, text, mainIndex)
                                }

                            }}
                            onClickErrorButton={() => {

                            }}
                        />
                    )
                }
                case 'date_picker': {
                    return (
                        <ItemPicker
                            key={bookingSectionItem.key}
                            disabled={bookingSectionItem.valid && !bookingSectionItem.editable}
                            showError={!bookingSectionItem.valid}
                            title={Utils.isValidDate(bookingSectionItem.value) ? Utils.getReadableDate(bookingSectionItem.value) : "Select"}
                            showHeaderText={bookingSectionItem.showHeader}
                            headerText={bookingSectionItem.headerText}
                            onClick={() => {
                                this.setState({
                                    dateTimePicker: {
                                        selectedDate: Utils.isValidDate(bookingSectionItem.value) ? bookingSectionItem.value : new Date(),
                                        minimumDate: bookingSectionItem.minimumDate,
                                        maximumDate: bookingSectionItem.maximumDate,
                                        visibility: true,
                                        mode: 'date',
                                        payload: { mainIndex, key: bookingSectionItem.key }
                                    }
                                })
                            }
                            }
                            onClickErrorButton={() => {

                            }}
                        />

                    )
                }
                case 'radio': {
                    return (
                        <View key={bookingSectionItem.key}>
                            <Text style={styles.radioHeader}>{bookingSectionItem.headerText}</Text>

                            {bookingSectionItem.items.map((radioButton) => {
                                return (
                                    <RadioButton
                                        disabled={bookingSectionItem.valid && !bookingSectionItem.editable}
                                        key={radioButton.key}
                                        selected={radioButton.key == bookingSectionItem.value}
                                        style={{ marginTop: 10, marginStart: 20 }}
                                        title={radioButton.title}
                                        onClick={() => {
                                            this.setValuesToItem(bookingSectionItem.key, radioButton.key, mainIndex);
                                        }}
                                    />
                                )
                            })}
                        </View>
                    )
                }

            }
        }
        return null;
    }
    getBookingSectionView(bookingSection, mainIndex) {
        return (
            <View key={bookingSection.id} style={styles.sectionView}>
                {bookingSection.items && bookingSection.items.map((item, index) => {
                    return this.getView(item, mainIndex, index);
                })}
            </View>
        )
        return null;
    }
    copyBookingFromPageOne() {
        let { formData, currentBookingPage } = this.state;
        BookingModel.shallowCopy(formData[currentBookingPage - 1], formData[0]);
        this.setState({ formData });
    }
    getMainAction(currentBookingPage, totalBookings) {
        if (currentBookingPage == totalBookings) {
            return "Save Booking Details"
        }
        else {
            return "Go to Booking " + (currentBookingPage + 1);
        }
    }
    getActionTitle(currentBookingPage, totalBookings) {
        if (totalBookings == 1) {
            return "Booking Details"
        }
        else {
            return "Booking " + currentBookingPage;
        }
    }
    onMainActionSuccess = (data) => {
        if (data
            && data.statusCode == "2002"
            && data.result) {
            this.setState({ loading: false, showSuccessView: true });
            setTimeout(
                function () {
                    this.setState({ showSuccessView: false });
                    this.props.callbacks.goBack();
                    this.props.callbacks.onApiSubmission();
                }.bind(this),
                1000
            );
            this.showAlert("Booking Details Saved");

        }
        else if (data
            && data.message) {
            this.onMainActionError(data.message);
        }

        else {
            this.onMainActionError();
        }
    }
    onMainActionError = (msg) => {
        this.setState({ loading: false })
        this.showAlert(msg ? msg : "Failed to update, Please try later")
    }
    mainAction() {
        let { currentBookingPage, totalBookings, info, formData, followUpInfo, form_object } = this.state;
        let bookingDetails;
        try {
            bookingDetails = SeverModel.getSeverData(formData, info);
        } catch (error) {
            this.onMainActionError();
            return;
        }
        if (info.action == "update_booking") {
            if (currentBookingPage == totalBookings) {
                //Saving booking details 
                if (info.stageId == BOOKING_STAGES.STAGE_INIT) {
                    this.props.navigation.navigate(BOOKING_ROUTES.Submission, {
                        followUpInfo,
                        onSubmit: (followUp) => {
                            this.setState({ followUpInfo: followUp, loading: true, requireRefreshC360: true })
                            bookingDetails.next_follow_date = followUp.dateTime;
                            bookingDetails.remarks = followUp.remarks;
                            if (form_object) {
                                bookingDetails.form_object = form_object;
                            }
                            new RestClient().bookCar(bookingDetails).then((data) => {
                                this.onMainActionSuccess(data);
                            }).
                                catch((error) => {
                                    this.onMainActionError();
                                })
                        }
                    });
                }
                else if (info.stageId == BOOKING_STAGES.STAGE_BOOKED) {
                    this.props.navigation.navigate(BOOKING_ROUTES.Submission, {
                        followUpInfo,
                        onSubmit: (followUp) => {
                            this.setState({ followUpInfo: followUp, loading: true, requireRefreshC360: true })
                            bookingDetails.exp_delivery_date = followUp.dateTime;
                            bookingDetails.remarks = followUp.remarks;
                            new RestClient().fullPaymentReceived(bookingDetails).then((data) => {
                                this.onMainActionSuccess(data);
                            }).
                                catch((error) => {
                                    this.onMainActionError();
                                })
                        }
                    });
                }
                else if (info.stageId == BOOKING_STAGES.STAGE_FULL_PAYMENT_RECEIVED) {
                    this.setState({ loading: true, requireRefreshC360: true })
                    new RestClient().updateInvoice(bookingDetails).then((data) => {
                        this.onMainActionSuccess(data);
                    }).
                        catch((error) => {
                            this.onMainActionError();
                        })
                }
                else if (info.stageId == BOOKING_STAGES.STAGE_INVOICED) {
                    this.setState({ loading: true, requireRefreshC360: true })

                    new RestClient().updateDelivery(bookingDetails).then((data) => {
                        this.onMainActionSuccess(data);
                    }).
                        catch((error) => {
                            this.onMainActionError();
                        })
                }
            }
            else {
                this.props.navigation.push(BOOKING_ROUTES.BookVehicleMain,
                    { formData, totalBookings, currentBookingPage: (currentBookingPage + 1) }
                );
            }
        }
        else if (info.action == "details_booking") {
            this.setState({ loading: true, requireRefreshC360: true })
            new RestClient().editBookingDetails(bookingDetails).then((data) => {
                this.onMainActionSuccess(data);
            }).
                catch((error) => {
                    this.onMainActionError();
                })
        }
    }
    render() {
        let {
            formData,
            currentBookingPage,
            totalBookings,
            dateTimePicker,
            listPicker,
            loading,
            initApiCallFinished,
            actionVisibility,
            info
        } = this.state;
        let isAllFieldsValid = true;
        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled={Platform.OS === 'ios'}>
                <LinearGradient
                    colors={["#2e5e86", "#2e3486"]}
                    style={styles.linearGradient}
                >
                    <View style={{ flex: 1 }}>
                        <View style={styles.header}>
                            <TouchableOpacity
                                hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                                style={styles.headerStart}
                                onPress={() => {
                                    if (info.stageId != BOOKING_STAGES.STAGE_DELIVERED) {
                                        this.showErrorPopup(
                                            "Are you sure want to close?", {
                                            title: "Close",
                                            action: "exit"
                                        },
                                            {
                                                title: "Dismiss",
                                            },
                                        )
                                    }
                                    else {
                                        this.action("exit");
                                    }

                                }}
                            >
                                <Image
                                    source={require('../../../images/ic_go_back.png')}
                                    style={styles.close}
                                />
                            </TouchableOpacity>
                            <Text style={styles.headerStartText}>{this.getActionTitle(currentBookingPage, totalBookings)}</Text>

                            {currentBookingPage > 1 && (
                                <TouchableOpacity
                                    style={styles.headerEnd}
                                    onPress={() => {
                                        this.copyBookingFromPageOne();
                                    }}
                                ><Text style={styles.headerEndText}>COPY FROM BOOKING 1</Text>
                                </TouchableOpacity>
                            )}


                        </View>
                        <PickerList
                            showFilter={listPicker.showFilter}
                            visible={listPicker.visibility}
                            onSelect={this.onSelectListPicker}
                            onCancel={() => {
                                this.setState({
                                    listPicker: DEFAULT_LIST_PICKER
                                });
                            }}
                            data={listPicker.data}
                        />
                        <CustomDateTimePicker
                            date={dateTimePicker.selectedDate ? dateTimePicker.selectedDate : new Date()}
                            minimumDate={dateTimePicker.minimumDate}
                            maximumDate={dateTimePicker.maximumDate}
                            dateTimePickerVisibility={dateTimePicker.visibility}
                            datePickerMode={dateTimePicker.mode}
                            payload={dateTimePicker.payload}
                            handleDateTimePicked={(date, payload) => {
                                this.onDateSelect(date, payload);
                            }}
                            hideDateTimePicked={() => {
                                this.setState({
                                    dateTimePicker: DEFAULT_DATE_TIME_PICKER
                                })
                            }}
                        />
                        {initApiCallFinished && (
                            <View style={{ flex: 1 }}>
                                <ScrollView>
                                    {
                                        formData[currentBookingPage - 1].map((item, index) => {
                                            let sectionValidation = item.getSectionValidation(item, formData[currentBookingPage - 1])
                                            let subTitle = typeof item.getTotal == "function" ? item.getTotal() : "";
                                            isAllFieldsValid &= (!(item.mandatory || sectionValidation.dataChanged) || sectionValidation.valid);
                                            return (
                                                <View key={index}>
                                                    <BookingSectionView
                                                        mandatory={item.mandatory || sectionValidation.dataChanged}
                                                        valid={sectionValidation.valid}
                                                        subTitle={subTitle}
                                                        expand={item.expand}
                                                        title={item.title}
                                                        key={index}
                                                        onPress={this.expandField.bind(this, item)} />
                                                    {item.expand && this.getBookingSectionView(item, index)}
                                                </View>
                                            )
                                        })
                                    }

                                </ScrollView>
                                {actionVisibility && (
                                    <TouchableOpacity
                                        disabled={!isAllFieldsValid}
                                        style={[styles.footer, isAllFieldsValid ? styles.footerValid : styles.footerInvalid]}
                                        onPress={() => {
                                            this.mainAction(currentBookingPage, totalBookings);
                                        }}>
                                        <Text style={styles.footerText}>{this.getMainAction(currentBookingPage, totalBookings)}</Text>
                                    </TouchableOpacity>
                                )}

                            </View>
                        )}


                        {loading && <Loader isLoading={loading} />}
                        {this.state.showSuccessView && (
                            <SuccessView text="Booking details saved" />
                        )}
                        <Toast
                            positionValue={200}
                            ref="toast"
                            style={{ backgroundColor: "#FF0000" }}
                            position="bottom"
                            fadeInDuration={1000}
                            fadeOutDuration={1000}
                            opacity={0.8}
                            textStyle={{ color: "white" }}
                        />

                    </View>
                </LinearGradient>
            </KeyboardAvoidingView>

        )
    }
}
String.prototype.isEmpty = function () {
    return (this.length === 0 || !this.trim());
};