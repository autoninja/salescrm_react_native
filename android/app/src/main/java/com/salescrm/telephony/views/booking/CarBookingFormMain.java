package com.salescrm.telephony.views.booking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.model.booking.BookingMainModel;
import com.salescrm.telephony.model.booking.CarBookingFormHolderModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.presenter.BookingMainPresenter;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.services.service_handlers.CarsFragmentServiceHandler;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;


public class CarBookingFormMain extends AppCompatActivity implements BookingMainPresenter.BookingMainView, CarBookingForm.CarBookingActionListener {
    private ImageButton ibClose;
    private TextView tvAction;
    private TextView tvTitle;
    private ImageButton ibBack;
    private TextView tvCopyFromBookingOne;
    private Realm realm;

    private BookingMainPresenter bookingMainPresenter;
    private FragmentManager fragmentManager;
    private FrameLayout frameAction;
    private int colorActionActive;
    private int colorActionInactive;
    private int stage_id;
    private Preferences pref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_car_form_main);
        realm = Realm.getDefaultInstance();
        int bookingQuantity = 1;
        if (getIntent() != null && getIntent().getExtras() != null) {
            bookingQuantity = getIntent().getExtras().getInt("booking_quantity", 1);
            stage_id = getIntent().getExtras().getInt("stage_id", WSConstants.CarBookingStages.BOOK_CAR);
        }

        pref = Preferences.getInstance();
        pref.load(this);
        bookingMainPresenter = new BookingMainPresenter(realm,CarBookingFormMain.this,this, bookingQuantity, stage_id);
        colorActionActive = Color.parseColor("#FF12A7FA");
        colorActionInactive = Color.parseColor("#BEFFFFFF");

        ibBack = (ImageButton) findViewById(R.id.ib_booking_main_back);
        tvTitle = (TextView) findViewById(R.id.ib_booking_main_title);
        ibClose = (ImageButton) findViewById(R.id.ib_booking_main_close);
        tvAction = (TextView) findViewById(R.id.tv_booking_main_action);
        frameAction = (FrameLayout) findViewById(R.id.frame_booking_main_action_bg);
        tvCopyFromBookingOne = (TextView) findViewById(R.id.tv_booking_main_copy_from);
        fragmentManager = getSupportFragmentManager();

        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookingMainPresenter.updateCurrentBookingPage(false);
            }
        });

        tvAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookingMainPresenter.updateCurrentBookingPage(true);
            }
        });
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CarBookingFormMain.this.finish();
            }
        });
        tvCopyFromBookingOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookingMainPresenter.copyFromBookingOne();
            }
        });
        /*if (!new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
            tvAction.setVisibility(View.GONE);
            findViewById(R.id.frame_booking_main_no_internet).setVisibility(View.VISIBLE);
        }*/
        if(!isFinishing()) {
            bookingMainPresenter.init();
        }

    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        bookingMainPresenter.updateCurrentBookingPage(false);
    }

    @Override
    public void onCopyFromBookingOne(CarBookingFormHolderModel carBookingFormHolderModel) {
        fragmentManager.beginTransaction()
                .replace(R.id.frame_booking_holder,
                        carBookingFormHolderModel.getCarBookingForm())
                .commit();
        carBookingFormHolderModel.getCarBookingForm().setCarBookingActionListener(this);
        carBookingFormHolderModel.getCarBookingForm().copyFromBookingOne(carBookingFormHolderModel.getBookingMainModel());

    }

    @Override
    public void init(BookingMainModel bookingMainModel) {
            if(bookingMainModel.getBookingEditWrapperModel().isSubmitButtonVisible()) {
                frameAction.setVisibility(View.VISIBLE);
            }
            else {
                frameAction.setVisibility(View.GONE);
            }
    }

    @Override
    public void updateViewOnAction(CarBookingFormHolderModel carBookingFormHolderModel, int totalBooking, int currentPage, boolean isClose, boolean isActionSubmit) {
        if (isClose) {
            CarBookingFormMain.this.finish();
            return;
        } else if (isActionSubmit) {
            callApi();
            return;
        } else {
            carBookingFormHolderModel.getCarBookingForm().setBookingMainModel(carBookingFormHolderModel.getBookingMainModel());
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_booking_holder,
                            carBookingFormHolderModel.getCarBookingForm())
                    .commit();
            carBookingFormHolderModel.getCarBookingForm().setCarBookingActionListener(this);
        }
        System.out.println("Total booking:" + totalBooking);
        if (totalBooking == 1 || currentPage == totalBooking - 1) {
            tvAction.setText("Save Booking Details");
        } else {
            tvAction.setText("Go to Booking " + (currentPage + 2));
        }
        if (currentPage > 0) {
            ibBack.setVisibility(View.VISIBLE);
            tvCopyFromBookingOne.setVisibility(View.VISIBLE);
        } else {
            ibBack.setVisibility(View.GONE);
            tvCopyFromBookingOne.setVisibility(View.GONE);
        }

        tvTitle.setText(totalBooking == 1 ? "Booking Details" : "Booking " + (currentPage + 1));

    }

    @Override
    public void restart() {
        pref.setReloadC360(true);
        this.finish();
        Toast.makeText(this, "Please try after sometimes", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onUpdateActionListener(boolean isAllComplete) {
        System.out.println("isAllComplete:" + isAllComplete);
        frameAction.setBackgroundColor(isAllComplete ? colorActionActive : colorActionInactive);
        tvAction.setEnabled(isAllComplete);
    }

    private void callApi() {
        if(!new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
            Toast.makeText(this, "Please connect to internet", Toast.LENGTH_SHORT).show();
            return;
        }
        switch (stage_id) {
            case WSConstants.CarBookingStages.BOOK_CAR:
                CarBookingSubmitForm.getInstance().show(this,
                        bookingMainPresenter.getCurrentBookingMainModel(),
                        new CarBookingSubmitForm.CarBookingSubmitFormListener() {
                            @Override
                            public void onCarBookingSubmitForm(BookingMainModel bookingMainModel) {
                                findViewById(R.id.frame_booking_loader).setVisibility(View.VISIBLE);
                                CarsFragmentServiceHandler.getInstance(CarBookingFormMain.this).callBookCarApi(
                                        CarsFragmentServiceHandler.getInstance(CarBookingFormMain.this).createBookCarObject(
                                                bookingMainPresenter.getCarBookingFormHolderModels(),
                                                bookingMainPresenter.getCurrentBookingMainModel(),
                                                bookingMainPresenter.getAllInterestedCars(),
                                                bookingMainPresenter.getC360InformationModel().getLocationId(),
                                                bookingMainPresenter.getC360InformationModel().getLeadSourceId(),
                                                bookingMainPresenter.getC360InformationModel().getFormSubmissionInput(),
                                                bookingMainPresenter.getC360InformationModel().getLeadLastUpdated()),
                                        bookingMainPresenter.getAllInterestedCars().getCarId(),
                                        "" + bookingMainPresenter.getAllInterestedCars().getLeadId(),
                                        pref.getAccessToken(),
                                        new CarsFragmentServiceHandler.CarBookingCallBack() {
                                            @Override
                                            public void onCallBookingCallBack(boolean success, String response) {
                                                pref.setShowBalloonAnimation(success);
                                                apiSubmitAcknowledgement(success, response);
                                            }
                                        }, pref);
                            }
                        });

                break;
            case WSConstants.CarBookingStages.CAR_BOOKED_HALF_PAYMENT:
                CarBookingSubmitForm.getInstance().show(this,
                        bookingMainPresenter.getCurrentBookingMainModel(),
                        new CarBookingSubmitForm.CarBookingSubmitFormListener() {
                            @Override
                            public void onCarBookingSubmitForm(BookingMainModel bookingMainModel) {
                                findViewById(R.id.frame_booking_loader).setVisibility(View.VISIBLE);

                                CarsFragmentServiceHandler.getInstance(CarBookingFormMain.this).fullPaymentReceived(pref.getAccessToken(),
                                        bookingMainPresenter.getAllInterestedCars().getCarId(),
                                        CarsFragmentServiceHandler.getInstance(CarBookingFormMain.this).createFullPaymentReceivedInputObject(
                                                bookingMainPresenter.getCurrentBookingMainModel().getRemarks(),
                                                Util.getInt(bookingMainPresenter.getAllInterestedCars().getBooking_id()),
                                                bookingMainPresenter.getAllInterestedCars().getBooking_number(),
                                                bookingMainPresenter.getAllInterestedCars().getIntrestedLeadCarId(),
                                                Util.getInt(bookingMainPresenter.getC360InformationModel().getLeadSourceId()),
                                                Util.getInt(bookingMainPresenter.getC360InformationModel().getLocationId()),
                                                bookingMainPresenter.getAllInterestedCars().getLeadId(),
                                                Util.getInt(bookingMainPresenter.getAllInterestedCars().getCar_stage_id()),
                                                bookingMainPresenter.getCurrentBookingMainModel().getDateAsSQLDate(),
                                                pref.getAccessToken(),
                                                bookingMainPresenter.getAllInterestedCars().getCarId(),
                                                Util.getInt(bookingMainPresenter.getAllInterestedCars().getEnquiry_id()),
                                                bookingMainPresenter.getC360InformationModel().getLeadLastUpdated(),
                                                bookingMainPresenter.getCurrentBookingMainModel().getApiInputDetails()),
                                        new CarsFragmentServiceHandler.CarBookingCallBack() {
                                            @Override
                                            public void onCallBookingCallBack(boolean success, String response) {
                                                apiSubmitAcknowledgement(success, response);
                                            }
                                        });
                            }
                        });
                break;
            case WSConstants.CarBookingStages.INVOICE_PENDING:
                findViewById(R.id.frame_booking_loader).setVisibility(View.VISIBLE);
/*

* */
                CarsFragmentServiceHandler.getInstance(this).updateInvoice(
                        CarsFragmentServiceHandler.getInstance(this).
                                createInvoiceModelObject(
                                        bookingMainPresenter.getCurrentBookingMainModel(),
                                        Util.getInt(bookingMainPresenter.getAllInterestedCars().getBooking_id()),
                                        bookingMainPresenter.getAllInterestedCars().getBooking_number(),
                                        bookingMainPresenter.getAllInterestedCars().getIntrestedLeadCarId(),
                                        Util.getInt(bookingMainPresenter.getAllInterestedCars().getEnquiry_id()),
                                        Util.getInt(bookingMainPresenter.getC360InformationModel().getLocationId()),
                                        bookingMainPresenter.getAllInterestedCars().getLeadId(),
                                        Util.getInt(bookingMainPresenter.getAllInterestedCars().getCar_stage_id()),
                                        bookingMainPresenter.getAllInterestedCars().getExpected_delivery_date(),
                                        bookingMainPresenter.getC360InformationModel().getLeadLastUpdated()),
                        bookingMainPresenter.getAllInterestedCars().getCarId(), pref.getAccessToken(),
                        bookingMainPresenter.getAllInterestedCars().getLeadId(),   new CarsFragmentServiceHandler.CarBookingCallBack() {
                            @Override
                            public void onCallBookingCallBack(boolean success, String response) {
                                apiSubmitAcknowledgement(success, response);
                            }
                        });
                    break;
            case WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING:
                findViewById(R.id.frame_booking_loader).setVisibility(View.VISIBLE);
                CarsFragmentServiceHandler.getInstance(this).updateDelivery(
                        CarsFragmentServiceHandler.getInstance(this).
                                createUpdateDeliveryModelObject(
                                        bookingMainPresenter.getCurrentBookingMainModel(),
                                        bookingMainPresenter.getAllInterestedCars().getInvoiceDetails().getInvoice_number().trim(),
                                        bookingMainPresenter.getAllInterestedCars().getInvoiceDetails().getInvoice_name(),
                                        bookingMainPresenter.getAllInterestedCars().getInvoiceDetails().getInvoice_mob_no(),
                                        bookingMainPresenter.getAllInterestedCars().getInvoiceDetails().getVin_no(),
                                        Util.getInt(bookingMainPresenter.getAllInterestedCars().getBooking_id()),
                                        bookingMainPresenter.getAllInterestedCars().getBooking_number(),
                                        bookingMainPresenter.getAllInterestedCars().getIntrestedLeadCarId(),
                                        Util.getInt(bookingMainPresenter.getAllInterestedCars().getEnquiry_id()),
                                        Util.getInt(bookingMainPresenter.getC360InformationModel().getLocationId()),
                                        bookingMainPresenter.getAllInterestedCars().getLeadId(),
                                        Util.getInt(bookingMainPresenter.getAllInterestedCars().getCar_stage_id()),
                                        bookingMainPresenter.getAllInterestedCars().getInvoiceDetails().getInvoiceId(),
                                        bookingMainPresenter.getC360InformationModel().getLeadLastUpdated()),
                        bookingMainPresenter.getAllInterestedCars().getCarId(), pref.getAccessToken(),
                        new CarsFragmentServiceHandler.CarBookingCallBack() {
                            @Override
                            public void onCallBookingCallBack(boolean success, String response) {
                                apiSubmitAcknowledgement(success, response);
                            }
                        });
                break;
            case WSConstants.CarBookingStages.CUSTOM_CAR_BOOKED_EDIT:
                findViewById(R.id.frame_booking_loader).setVisibility(View.VISIBLE);
                CarsFragmentServiceHandler.getInstance(this).callEditBookingApi(
                        CarsFragmentServiceHandler.getInstance(this).createEditBookingObject(
                                Util.getInt(bookingMainPresenter.getAllInterestedCars().getBooking_id()),
                                Util.getInt(bookingMainPresenter.getAllInterestedCars().getEnquiry_id()),
                                bookingMainPresenter.getCurrentBookingMainModel(),
                                bookingMainPresenter.getAllInterestedCars(),
                                bookingMainPresenter.getC360InformationModel().getLocationId(),
                                bookingMainPresenter.getC360InformationModel().getLeadSourceId(),
                                bookingMainPresenter.getC360InformationModel().getLeadLastUpdated()),
                        bookingMainPresenter.getAllInterestedCars().getCarId(),
                        "" + bookingMainPresenter.getAllInterestedCars().getLeadId(),
                        pref.getAccessToken(),
                        new CarsFragmentServiceHandler.CarBookingCallBack() {
                            @Override
                            public void onCallBookingCallBack(boolean success, String response) {
                                apiSubmitAcknowledgement(success, response);
                            }
                        });
                break;
        }
    }

    private void apiSubmitAcknowledgement(boolean success, String response) {
        pref.setReloadC360(true);
        if (success) {
            this.finish();
        } else {
            findViewById(R.id.frame_booking_loader).setVisibility(View.GONE);
            Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
        }
    }
}
