import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView, Alert } from 'react-native'


export default class ETVBRItemChild extends Component {
  constructor(props){
    super(props);
    this.state= {expand:false, showImagePlaceHolder:true};
  }

  render() {
    let imageSource;
    let placeHolder;
    let headerColor  = '#2D4F8B';
    let paddingLeft = 0;
    let viewHeight = 60;
    let headerFontSize = 15;
    if(this.props.isUnderTl) {
      headerColor = '#808080';
      paddingLeft = 4;
      viewHeight = 50;
      headerFontSize = 13;
    }
    if(this.props.from_location) {
      imageSource = require( '../../images/ic_location.png');
      placeHolder = null;

    }
    else {
      imageSource = {uri : this.props.info.dp_url? this.props.info.dp_url:'null'};
      placeHolder = <View style={{position:'absolute',display:(this.state.showImagePlaceHolder?'flex':'none'),width:36, height:36, borderRadius: 36/2,backgroundColor:'#FF8000', alignItems:'center', justifyContent:'center'}}><Text style={{color:'#fff'}}>{this.props.info.name.charAt(0).toUpperCase()}</Text></View>;;
      }



    if(this.props.showPercentage) {

          return(

            <View>
              <View style={{marginTop:4, marginBottom:4, flexDirection:'row', alignItems:'center'}}>
                  <Text style={{flex:1, fontSize:headerFontSize, color:headerColor,
                   paddingLeft }}>{this.props.info.name}</Text>
                  <TouchableOpacity 
                   hitSlop={{top: 20, bottom: 20, left: 20, right: 20}} 
                  onPress={()=>{this.props.leadSourceCarModelWiseETVBR()}}>
                  <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center',padding:3, }}>
                      <Text style={{fontSize:12, color:'#007FFF',}}>Source / Model</Text>
                      
                      </View>
                  </TouchableOpacity>
              </View> 
              <View style={{flexDirection: 'row', height:viewHeight, backgroundColor:'#fff', borderColor:'#e0e0e0', borderWidth:0.5, alignItems:'center'}}>

              
              <View style={{flex:1.5, alignItems:'center', justifyContent:'center'}}>
              {placeHolder}
              <TouchableOpacity
              style={{position:'absolute',}}
              onLongPress={() =>{
              this.props.onLongPress('user_info', 
              {info:{name:this.props.info.name, dp_url:this.props.info.dp_url}})
               }}>
              <Image
                source= {imageSource}
                style={{width:36, height:36, borderRadius: 36/2,}}
              />
              </TouchableOpacity>
                </View>






              <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#2D4F8B'}} >{this.props.rel.t}</Text>
                </View>


              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#2D4F8B'}} >{this.props.rel.v}</Text>
                </View>

              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#2D4F8B'}} >{this.props.rel.b}</Text>
                </View>


              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#2D4F8B'}} >{this.props.rel.r}</Text>
                </View>


              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
                borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

                  <Text style={{ color:'red'}} >{this.props.rel.l}</Text>
              </View>


              </View>
            </View>
          );
    }
    else {

    return(

      <View>
              <View style={{marginTop:4, marginBottom:4, flexDirection:'row', alignItems:'center'}}>
                <Text style={{flex:1, fontSize:headerFontSize, color:headerColor,
                  paddingLeft }}>{this.props.info.name}</Text>
                <TouchableOpacity 
                 hitSlop={{top: 20, bottom: 20, left: 20, right: 20}} 
                onPress={()=>{this.props.leadSourceCarModelWiseETVBR()}}>
                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center',padding:3, }}>
                    <Text style={{fontSize:12, color:'#007FFF',}}>Source / Model</Text>
                    </View>
                </TouchableOpacity>
            </View> 
      <View style={{flexDirection: 'row', height:viewHeight, backgroundColor:'#fff', borderColor:'#e0e0e0', borderWidth:0.5,alignItems:'center'}}>

       <View style={{flex:1.5, alignItems:'center', justifyContent:'center'}}>
          {placeHolder}
            <TouchableOpacity
              style={{position:'absolute',}}
              onLongPress={() =>{
              this.props.onLongPress('user_info', 
              {info:{name:this.props.info.name, dp_url:this.props.info.dp_url}})
          }}>
            <Image
              source= {imageSource}
              style={{width:36, height:36, borderRadius: 36/2,}}
            />
          </TouchableOpacity>
         </View>


      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Enquiries',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#2D4F8B', textDecorationLine:'underline'}} >{this.props.abs.e}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#2D4F8B'}} >{this.props.targets.e}</Text>
        </View>
      </View>




      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Test Drives',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>


          <Text style={{ color:'#2D4F8B',textDecorationLine:'underline'}} >{this.props.abs.t}</Text>

          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#2D4F8B'}} >{this.props.targets.t}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Visits',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#2D4F8B',textDecorationLine:'underline'}} >{this.props.abs.v}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#2D4F8B'}} >{this.props.targets.v}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Bookings',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#2D4F8B',textDecorationLine:'underline'}} >{this.props.abs.b}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#2D4F8B'}} >{this.props.targets.b}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Retails',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#2D4F8B',textDecorationLine:'underline'}} >{this.props.abs.r}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#2D4F8B'}} >{this.props.targets.r}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Lost Drop',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>


          <Text style={{ color:'red',textDecorationLine:'underline'}} >{this.props.abs.l}</Text>

          </TouchableOpacity>
      </View>


      </View>
      </View>
    );
  }
  }
}
