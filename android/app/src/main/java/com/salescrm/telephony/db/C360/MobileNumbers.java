package com.salescrm.telephony.db.C360;

import io.realm.RealmObject;

/**
 * Created by bannhi on 9/5/17.
 */

public class MobileNumbers extends RealmObject{

    private String phoneNumberID;
    private int customerID;
    private long phoneNumber;
    private long leadId;
    private String phoneNumberStatus;
    private String lead_phone_mapping_id;
    boolean deletedOffline = false;
    boolean createdOffline;
    boolean editedOffline;
   // boolean synced;



    public boolean isDeletedOffline() {
        return deletedOffline;
    }

    public void setDeletedOffline(boolean deletedOffline) {
        this.deletedOffline = deletedOffline;
    }

    public boolean isCreatedOffline() {
        return createdOffline;
    }

    public void setCreatedOffline(boolean createdOffline) {
        this.createdOffline = createdOffline;
    }

   /* public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }*/

    public String getPhoneNumberID() {
        return phoneNumberID;
    }

    public void setPhoneNumberID(String phoneNumberID) {
        this.phoneNumberID = phoneNumberID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public String getPhoneNumberStatus() {
        return phoneNumberStatus;
    }

    public void setPhoneNumberStatus(String phoneNumberStatus) {
        this.phoneNumberStatus = phoneNumberStatus;
    }

    public String getLead_phone_mapping_id() {
        return lead_phone_mapping_id;
    }

    public void setLead_phone_mapping_id(String lead_phone_mapping_id) {
        this.lead_phone_mapping_id = lead_phone_mapping_id;
    }

    public boolean isEditedOffline() {
        return editedOffline;
    }

    public void setEditedOffline(boolean editedOffline) {
        this.editedOffline = editedOffline;
    }
}
