package com.salescrm.telephony.db.ref_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by prateek on 8/11/17.
 */

public class RefLocationRel extends RealmObject {

    @SerializedName("enquiries")
    @Expose
    private Integer enquiries;
    @SerializedName("tdrives")
    @Expose
    private Integer tdrives;
    @SerializedName("visits")
    @Expose
    private Integer visits;
    @SerializedName("bookings")
    @Expose
    private Integer bookings;
    @SerializedName("retails")
    @Expose
    private Integer retails;
    @SerializedName("lost_drop")
    @Expose
    private Integer lost_drop;
    @SerializedName("exchanged")
    @Expose
    private Integer exchanged;
    @SerializedName("in_house_financed")
    @Expose
    private Integer in_house_financed;
    @SerializedName("out_house_financed")
    @Expose
    private Integer out_house_financed;

    @SerializedName("pending_bookings")
    @Expose
    private Integer pending_bookings;

    @SerializedName("live_enquiries")
    @Expose
    private Integer live_enquiries;

    public Integer getIn_house_financed() {
        return in_house_financed;
    }

    public void setIn_house_financed(Integer in_house_financed) {
        this.in_house_financed = in_house_financed;
    }

    public Integer getOut_house_financed() {
        return out_house_financed;
    }

    public void setOut_house_financed(Integer out_house_financed) {
        this.out_house_financed = out_house_financed;
    }

    public Integer getExchanged() {
        return exchanged;
    }

    public void setExchanged(Integer exchanged) {
        this.exchanged = exchanged;
    }

    public Integer getEnquiries() {
        return enquiries;
    }

    public void setEnquiries(Integer enquiries) {
        this.enquiries = enquiries;
    }

    public Integer getTdrives() {
        return tdrives;
    }

    public void setTdrives(Integer tdrives) {
        this.tdrives = tdrives;
    }

    public Integer getVisits() {
        return visits;
    }

    public void setVisits(Integer visits) {
        this.visits = visits;
    }

    public Integer getBookings() {
        return bookings;
    }

    public void setBookings(Integer bookings) {
        this.bookings = bookings;
    }

    public Integer getRetails() {
        return retails;
    }

    public void setRetails(Integer retails) {
        this.retails = retails;
    }

    public Integer getLost_drop() {
        return lost_drop;
    }

    public void setLost_drop(Integer lost_drop) {
        this.lost_drop = lost_drop;
    }

    public Integer getPending_bookings() {
        return pending_bookings;
    }

    public void setPending_bookings(Integer pending_bookings) {
        this.pending_bookings = pending_bookings;
    }

    public Integer getLive_enquiries() {
        return live_enquiries;
    }

    public void setLive_enquiries(Integer live_enquiries) {
        this.live_enquiries = live_enquiries;
    }
}
