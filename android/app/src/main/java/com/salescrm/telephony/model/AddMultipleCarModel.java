package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by bharath on 9/5/17.
 */

public class AddMultipleCarModel {
    private String lead_last_updated;
    private String lead_id;
    private List<InterestedCars> interestedCars;
    private List<OtherCars> exchangeCars;
    private List<OtherCars> additionalCars;

    public AddMultipleCarModel() {
    }

    public AddMultipleCarModel(String lead_last_updated,
                               String lead_id,
                               List<InterestedCars> interestedCars,
                               List<OtherCars> exchangeCars,
                               List<OtherCars> additionalCars) {
        this.lead_last_updated = lead_last_updated;
        this.lead_id = lead_id;
        this.interestedCars = interestedCars;
        this.exchangeCars = exchangeCars;
        this.additionalCars = additionalCars;
    }

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public List<InterestedCars> getInterestedCars() {
        return interestedCars;
    }

    public void setInterestedCars(List<InterestedCars> interestedCars) {
        this.interestedCars = interestedCars;
    }

    public List<OtherCars> getExchangeCars() {
        return exchangeCars;
    }

    public void setExchangeCars(List<OtherCars> exchangeCars) {
        this.exchangeCars = exchangeCars;
    }

    public List<OtherCars> getAdditionalCars() {
        return additionalCars;
    }

    public void setAdditionalCars(List<OtherCars> additionalCars) {
        this.additionalCars = additionalCars;
    }

    public class InterestedCars {
        private String carModel;
        private String model;
        private String carVariant;
        private String carFuelType;
        private String variant;
        private String fuel_type;
        private String carColorId;
        private String color;


        public String getCarModel() {
            return carModel;
        }

        public void setCarModel(String carModel) {
            this.carModel = carModel;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getCarVariant() {
            return carVariant;
        }

        public void setCarVariant(String carVariant) {
            this.carVariant = carVariant;
        }

        public String getCarFuelType() {
            return carFuelType;
        }

        public void setCarFuelType(String carFuelType) {
            this.carFuelType = carFuelType;
        }

        public String getVariant() {
            return variant;
        }

        public void setVariant(String variant) {
            this.variant = variant;
        }

        public String getFuel_type() {
            return fuel_type;
        }

        public void setFuel_type(String fuel_type) {
            this.fuel_type = fuel_type;
        }

        public String getCarColorId() {
            return carColorId;
        }

        public void setCarColorId(String carColorId) {
            this.carColorId = carColorId;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }
    }

    public class OtherCars {
        private Model model;
        private String purchase_date;
        private String reg_no;
        private String kms_run;
        private String name;

        public Model getModel() {
            return model;
        }

        public void setModel(Model model) {
            this.model = model;
        }

        public String getPurchase_date() {
            return purchase_date;
        }

        public void setPurchase_date(String purchase_date) {
            this.purchase_date = purchase_date;
        }

        public String getReg_no() {
            return reg_no;
        }

        public void setReg_no(String reg_no) {
            this.reg_no = reg_no;
        }

        public String getKms_run() {
            return kms_run;
        }

        public void setKms_run(String kms_run) {
            this.kms_run = kms_run;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public class Model{
            private String name;
            private String id;
            private String code;
            private String category;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }
        }

    }


}
