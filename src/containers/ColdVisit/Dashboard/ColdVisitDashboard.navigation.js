import React from "react";
import ColdVisitDashboard from "./index";
import ColdVisitDashboardDetails from "./ColdVisitDashboardDetails";
import UserInfo from "../../../containers/UserInfo/user_info";
import HamburgerIcon from "../../../components/uielements/HamburgerIcon";
import BackIcon from "../../../components/uielements/BackIcon";
import HeaderRight from "../../../components/uielements/HeaderRight";
import DateRangePickerDialog from "../../../components/date_range_picker";
import Search from "../../../containers/Search/index";
import SearchResult from "../../../containers/Search/SearchResult";
import { createStackNavigator, createAppContainer } from "react-navigation";

const getNavigationWithTitle = (navigation, title) => {
  return {
    headerStyle: {
      backgroundColor: "#2e5e86",
      elevation: 0
    },
    headerLeft: <BackIcon navigationProps={navigation} />,
    title: title,
    headerBackTitle: null,
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: 'bold',
      width: '100%',
      textAlign: 'left',
    },
  };
};

export const getColdVisitDashboardNavigator = (
  isUserBH,
  isUserSM,
  isUserTL,
  isUserSC,
  info
) => {
  return createAppContainer(
    createStackNavigator(
      {
        ColdVisitDashboard: {
          screen: props => (
            <ColdVisitDashboard
              {...props}
              isUserBH={isUserBH}
              isUserSM={isUserSM}
              isUserTL={isUserTL}
              isUserSC={isUserSC}
            />
          ),
          navigationOptions: ({ navigation }) => ({
            title: "Cold Visits",

            headerBackTitle: null,

            headerLeft: <HamburgerIcon navigationProps={navigation} />,

            headerRight: (
              <HeaderRight isUserBH={false} navigationProps={navigation} />
            ),

            headerStyle: {
              backgroundColor: "#2e5e86",
              elevation: 0
            },
            headerTransperent: true,
            headerTintColor: "#fff",
            headerTitleStyle: {
              width: "100%",
              textAlign: "left"
            }
          })
        },
        ColdVisitDashboardDetails: {
          screen: ColdVisitDashboardDetails,
          navigationOptions: ({ navigation }) => ({
            headerStyle: {
              backgroundColor: "#2e5e86",
              elevation: 0
            },
            headerLeft: <BackIcon navigationProps={navigation} />,
            headerBackTitle: null,
            headerTintColor: "#fff",
            headerTitleStyle: {
              fontWeight: "bold",
              textAlign: "left"
            }
          })
        },
        DateRangePickerDialog: {
          screen: DateRangePickerDialog,
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        UserInfo: {
          screen: UserInfo,
          navigationOptions: ({ navigation }) => ({
            header: null
          })
        },
        Search: {
          screen: Search,
          navigationOptions: ({ navigation }) => getNavigationWithTitle(navigation, "Add/ Search lead")
        },
        SearchResult: {
          screen: props => (
            <SearchResult
              {...props}
              uniqueAcrossDealerShip={info.uniqueAcrossDealerShip}
              addLeadEnabled={info.addLeadEnabled}
              coldVisitEnabled={info.coldVisitEnabled}
              showLeadCompetitor={info.showLeadCompetitor}

            />
          ),
          navigationOptions: ({ navigation }) => getNavigationWithTitle(navigation, "Search Result")
        }
      },

      {
        cardStyle: {
          backgroundColor: "#303030BE",
          opacity: 1
        },
        mode: "modal"
      },
      {
        initialRouteName: "ColdVisitDashboard"
      }
    )
  );
};
