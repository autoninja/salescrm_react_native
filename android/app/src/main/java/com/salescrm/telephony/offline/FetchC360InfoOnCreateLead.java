package com.salescrm.telephony.offline;

import android.content.Context;
import android.util.Log;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.car.AdditionalCarsDetails;
import com.salescrm.telephony.db.car.AllInterestedCars;
import com.salescrm.telephony.db.car.CarDmsDataDB;
import com.salescrm.telephony.db.CustomerEmailId;
import com.salescrm.telephony.db.CustomerPhoneNumbers;
import com.salescrm.telephony.db.DseDetails;
import com.salescrm.telephony.db.car.CarsDBHandler;
import com.salescrm.telephony.db.car.ExchangeCarDetails;
import com.salescrm.telephony.db.ExchangeStatusdb;
import com.salescrm.telephony.db.IntrestedCarActivityGroup;
import com.salescrm.telephony.db.IntrestedCarActivityGroupStages;
import com.salescrm.telephony.db.IntrestedCarDetails;
import com.salescrm.telephony.db.LeadStageProgressDB;
import com.salescrm.telephony.db.PlannedActivities;
import com.salescrm.telephony.db.PlannedActivitiesAction;
import com.salescrm.telephony.db.PlannedActivitiesDetail;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.model.booking.ApiInputBookingDetails;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AllLeadCustomerDetailsResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 6/12/16.
 * Call this to refresh c360 information for new lead
 */
public class FetchC360InfoOnCreateLead implements Callback<AllLeadCustomerDetailsResponse> {
    public FetchC360OnCreateLeadListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private int callCount;
    private String TAG = "FetchFormData";
    private ArrayList<String> leadData;
    private int dseId;
    private boolean isCreatedTemp =false;

    public FetchC360InfoOnCreateLead(FetchC360OnCreateLeadListener listener, Context context,ArrayList<String> leadData,int dseId) {
        this.listener = listener;
        this.context = context;
        this.leadData = leadData;
        this.callCount = 0;
        this.dseId = dseId;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);


        if (dseId != pref.getAppUserId()) {
            isCreatedTemp = true;
        }

    }


    public void call(boolean fetchActionPlanFirst) {
        if(leadData.size()==0){
            listener.onFetchC360OnCreateLeadSuccess();

            return;
        }

        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
                ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetAllLeadInformation(leadData, this);
//            listener.onC360InformationFetched(null);
        } else {
            System.out.println("No Internet Connection");
            listener.onFetchC360OnCreateLeadError();
        }

    }

    private void insert(Realm realm, AllLeadCustomerDetailsResponse.Result cardDetailResponseData) {
        realm.delete(AllInterestedCars.class);
        RealmList<CustomerPhoneNumbers> customerPhoneNumbers = new RealmList<CustomerPhoneNumbers>();
       /* CustomerPhoneNumbers customerPhoneNumbers = new CustomerPhoneNumbers();*/
        if (cardDetailResponseData.getCustomer_details().getCustomerDetails().getMobile_nos() != null) {
            for (int i = 0; i < cardDetailResponseData.getCustomer_details().getCustomerDetails().getMobile_nos().size(); i++) {
                CustomerPhoneNumbers customerPhoneNumbersObj = new CustomerPhoneNumbers();
                customerPhoneNumbersObj.setLeadId(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id()));
                customerPhoneNumbersObj.setCustomerID(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getCustomer_id()));
                customerPhoneNumbersObj.setPhoneNumber(Long.parseLong(cardDetailResponseData.getCustomer_details().getCustomerDetails().getMobile_nos().get(i).getNumber()));
                customerPhoneNumbersObj.setPhoneNumberID(cardDetailResponseData.getCustomer_details().getCustomerDetails().getMobile_nos().get(i).getId());
                customerPhoneNumbersObj.setPhoneNumberStatus(cardDetailResponseData.getCustomer_details().getCustomerDetails().getMobile_nos().get(i).getStatus());
                customerPhoneNumbersObj.setLead_phone_mapping_id(cardDetailResponseData.getCustomer_details().getCustomerDetails().getMobile_nos().get(i).getLead_phone_mapping_id());
                System.out.println("PHONE NUMBER ADDED" + cardDetailResponseData.getCustomer_details().getCustomerDetails().getMobile_nos().get(i).getNumber() + " id: " + cardDetailResponseData.getCustomer_details().getCustomerDetails().getMobile_nos().get(i).getLead_phone_mapping_id());
                customerPhoneNumbers.add(customerPhoneNumbersObj);

                System.out.println("CUSTOMER ID" + cardDetailResponseData.getCustomer_details().getCustomerDetails().getCustomer_id() + "CUSTOMER PHONE NUMBER SIZE" + customerPhoneNumbers.size());
            }
        }

        //realm.copyToRealmOrUpdate(customerPhoneNumbers);

        RealmList<CustomerEmailId> customerEmailId = new RealmList<CustomerEmailId>();

        if (cardDetailResponseData.getCustomer_details().getCustomerDetails().getEmail_ids() != null) {
            for (int i = 0; i < cardDetailResponseData.getCustomer_details().getCustomerDetails().getEmail_ids().size(); i++) {
                CustomerEmailId customerEmailIdObj = new CustomerEmailId();
                customerEmailIdObj.setLeadId(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id()));
                customerEmailIdObj.setCustomerID(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getCustomer_id()));
                customerEmailIdObj.setEmail(cardDetailResponseData.getCustomer_details().getCustomerDetails().getEmail_ids().get(i).getAddress());
                customerEmailIdObj.setEmailID(cardDetailResponseData.getCustomer_details().getCustomerDetails().getEmail_ids().get(i).getId());
                customerEmailIdObj.setEmailStatus(cardDetailResponseData.getCustomer_details().getCustomerDetails().getEmail_ids().get(i).getStatus());
                customerEmailIdObj.setLead_email_mapping_id(cardDetailResponseData.getCustomer_details().getCustomerDetails().getEmail_ids().get(i).getLead_email_mapping_id());
                customerEmailId.add(customerEmailIdObj);
                System.out.println("CUSTOMER ID" + cardDetailResponseData.getCustomer_details().getCustomerDetails().getCustomer_id() + "CUSTOMER PHONE NUMBER SIZE" + customerEmailId.size());
            }

        }
/*        RealmResults<PlannedActivities> deleteOldPlanned = realm.where(PlannedActivities.class).equalTo("leadID",
                Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id())).findAll();
        if (deleteOldPlanned.size() > 0) {
            deleteOldPlanned.deleteAllFromRealm();
        }*/

        PlannedActivities plannedActivities = new PlannedActivities();

        /// PlannedActivitiesDetail plannedActivitiesDetail = realm.createObject(PlannedActivitiesDetail.class);
        // PlannedActivitiesAction plannedActivitiesAction = realm.createObject(PlannedActivitiesAction.class);

        for (int i = 0; i < cardDetailResponseData.getCustomer_details().getPlannedActivities().size(); i++) {
            plannedActivities.setPlannedActivityScheduleId(Integer.parseInt(cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getScheduled_activity_id()));
            plannedActivities.setLeadID(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id()));
            plannedActivities.setPlannedActivitiesIconType(cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getIcon_type());
            plannedActivities.setPlannedActivitiesName(cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getName());
            plannedActivities.setPlannedActivitiesDateTime(cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getDate_time());
            plannedActivities.setActivityId(cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getActivity_id());
            plannedActivities.plannedActivitiesDetail.clear();
            /*for (int j = 0; j < cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getDetails().size(); j++) {*/
            for (int j = 0; j < (cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getDetails() == null ? 0 : cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getDetails().size()); j++) {
                Log.e("plannedActivitiesDetail", "plannedActivitiesDetail- " + cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getDetails().get(j).getIcon_type());
                PlannedActivitiesDetail plannedActivitiesDetail = new PlannedActivitiesDetail();
                plannedActivitiesDetail.setPlannedActivitiesDetailIconTypes(cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getDetails().get(j).getIcon_type());
                plannedActivitiesDetail.setPlannedActivitiesDetailKeys(cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getDetails().get(j).getKey());
                plannedActivitiesDetail.setPlannedActivitiesDetailValues(cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getDetails().get(j).getValue());
                //  realm.copyToRealmOrUpdate(plannedActivitiesDetail);
                plannedActivities.plannedActivitiesDetail.add(plannedActivitiesDetail);
                // plannedActivitiesDetail.deleteFromRealm();
            }

            // realm.copyToRealmOrUpdate(plannedActivitiesDetail);
            plannedActivities.plannedActivitiesAction.clear();
            /*for (int k = 0; k < cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getActions().size(); k++) {*/
            for (int k = 0; k < (cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getActions() == null ? 0 : cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getActions().size()); k++) {
                PlannedActivitiesAction plannedActivitiesAction = new PlannedActivitiesAction();
                plannedActivitiesAction.setPlannedActivitiesAction(cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getActions().get(k).getAction());
                plannedActivitiesAction.setPlannedActivitiesActionsIconType(cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getActions().get(k).getIcon_type());
                plannedActivitiesAction.setPlannedActivitiesActionsId(Integer.parseInt(cardDetailResponseData.getCustomer_details().getPlannedActivities().get(i).getActions().get(k).getAction_id()));

                // realm.copyToRealmOrUpdate(plannedActivitiesAction);
                plannedActivities.plannedActivitiesAction.add(plannedActivitiesAction);
                // plannedActivitiesAction.deleteFromRealm();
            }

            //plannedActivities.plannedActivitiesAction.add(plannedActivitiesAction);
        }
        // realm.copyToRealmOrUpdate(plannedActivitiesAction);


        realm.copyToRealmOrUpdate(plannedActivities);

        if (cardDetailResponseData.getCustomer_details().getCustomerDetails().getDse_details() != null) {
            DseDetails dseDetails = new DseDetails();
            dseDetails.setCustomerID(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id()));
            dseDetails.setDseDetailId(Util.getInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getDse_details().getId()));
            dseDetails.setDseId(cardDetailResponseData.getCustomer_details().getCustomerDetails().getDse_details().getDse_id());
            dseDetails.setDseMobile(cardDetailResponseData.getCustomer_details().getCustomerDetails().getDse_details().getDse_mobNo());
            dseDetails.setDseName(cardDetailResponseData.getCustomer_details().getCustomerDetails().getDse_details().getDse_name());

            realm.copyToRealmOrUpdate(dseDetails);
        }


        for (int i = 0; i < cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().size(); i++) {
            IntrestedCarDetails intrestedCarDetails = new IntrestedCarDetails();
            intrestedCarDetails.setLeadId(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id()));
            intrestedCarDetails.setIntrestedLeadCarId(Integer.parseInt(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getLead_car_id()));
            intrestedCarDetails.setLeadId(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id()));
            intrestedCarDetails.setIntrestedCarName(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getName());
            intrestedCarDetails.setIntrestedCarVariantCode(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getVariant_code());
            intrestedCarDetails.setIntrestedCarVariantId(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getVariant_id());
            intrestedCarDetails.setIntrestedCarColorId(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getColor_id());
            intrestedCarDetails.setIntrestedCarModelId(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getModel_id());
            intrestedCarDetails.setIntrestedModelName(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getModel());
            intrestedCarDetails.setIntrestedCarIsPrimary(Integer.parseInt(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getIs_primary()));
            intrestedCarDetails.setTestDriveStatus(Integer.parseInt(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getTest_drive_status()));
            intrestedCarDetails.setColor(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getColor());
            intrestedCarDetails.setVariant(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getVariant());
            intrestedCarDetails.setFuelType(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getFuel_type());
            intrestedCarDetails.setFuelId(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getFuel_type_id());
            for (int j = 0; j < cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCar_activity_groups().size(); j++) {

                IntrestedCarActivityGroup intrestedCarActivityGroup = new IntrestedCarActivityGroup();
                intrestedCarActivityGroup.setLeadId(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id()));
                intrestedCarActivityGroup.setIntrestedCarDetailActivityGroupName(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getGroup_name());
                for (int k = 0; k < cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getActivity_stages().size(); k++) {
                    IntrestedCarActivityGroupStages intrestedCarActivityGroupStages = new IntrestedCarActivityGroupStages();
                    System.out.println("ThirdSize" + cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getActivity_stages().get(k).getName());
                    intrestedCarActivityGroupStages.setLeadId(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id()));
                    intrestedCarActivityGroupStages.setIntrestedCarActivityStageID(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getActivity_stages().get(k).getStage_id());
                    intrestedCarActivityGroupStages.setIntrestedCarStageBarClass(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getActivity_stages().get(k).getBar_class());
                    intrestedCarActivityGroupStages.setIntrestedCarStageName(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getActivity_stages().get(k).getName());
                    intrestedCarActivityGroupStages.setIntrestedCarStageWidth(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCar_activity_groups().get(j).getActivity_stages().get(k).getWidth());
                    intrestedCarActivityGroup.IntrestedCarActivityGroupStages.add(intrestedCarActivityGroupStages);

                }

                intrestedCarDetails.intrestedCarActivityGroup.add(intrestedCarActivityGroup);
            }
            List<ApiInputBookingDetails> apiInputBookingDetails = new ArrayList<>();
            for (int x = 0; x < cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().size(); x++) {
                apiInputBookingDetails.add(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getBooking_details());
                CarDmsDataDB carDmsDataDB = new CarDmsDataDB();
                carDmsDataDB.setCarId(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getEnquiry_id());
                carDmsDataDB.setEnquiry_number(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getEnquiry_number());
                carDmsDataDB.setLocation_code(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getLocation_code());
                carDmsDataDB.setBooking_number(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getBooking_number());
                carDmsDataDB.setBooking_id(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getBooking_id());
                carDmsDataDB.setEnquiry_id(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getEnquiry_id());
                carDmsDataDB.setCar_stage_id(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getCar_stage_id());
                carDmsDataDB.setInvoice_number(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_number());
                carDmsDataDB.setInvoice_id(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_id());
                carDmsDataDB.setExpected_delivery_date(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getExpected_delivery_date());
                carDmsDataDB.setBooking_amount(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getBooking_amount());

                carDmsDataDB.setInvoice_name(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_name());
                carDmsDataDB.setInvoice_mobile_no(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_mob_no());
                carDmsDataDB.setInvoice_date(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_date());
                carDmsDataDB.setInvoice_vin_no(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getInvoice_vin_no());

                carDmsDataDB.setDelivery_date(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getDelivery_date());
                carDmsDataDB.setDelivery_challan(cardDetailResponseData.getCustomer_details().getInterestedCarsDetails().get(i).getCarDmsData().get(x).getDelivery_number());

                intrestedCarDetails.carDmsDataList.add(carDmsDataDB);
            }
            realm.copyToRealmOrUpdate(intrestedCarDetails);
            CarsDBHandler.getInstance().createAllCarsList(realm, intrestedCarDetails,
                    cardDetailResponseData.getCustomer_details().getCustomerDetails().getName(), apiInputBookingDetails);
        }


        for (int i = 0; i < cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().size(); i++) {
            ExchangeCarDetails exchangeCarDetails = new ExchangeCarDetails();
            System.out.println("ExchangeSizeone" + cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().size());
            exchangeCarDetails.setLeadId(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id()));
            exchangeCarDetails.setExchangecarname(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getName());
            exchangeCarDetails.setLead_exchange_car_id(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getLead_exchange_car_id());
            exchangeCarDetails.setExpected_price(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getExpected_price());
            exchangeCarDetails.setMarket_price(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getMarket_price());
            exchangeCarDetails.setModel_id(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getModel_id());
            exchangeCarDetails.setPrice_quoted(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getPrice_quoted());
            exchangeCarDetails.setPurchase_date(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getPurchase_date());
            exchangeCarDetails.setKms_run(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getKms_run());
            exchangeCarDetails.setReg_no(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getReg_no());
            exchangeCarDetails.setCar_stage_id(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getStage_id());
            exchangeCarDetails.setEvaluator_name(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getEvaluator_name());
            exchangeCarDetails.setMake_year(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getMake_year());
            exchangeCarDetails.setOwner_type(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getOwner_type());
            if(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getActivityDetails()!=null) {
               // exchangeCarDetails.setEvaluator_name(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getActivityDetails().getUser_name());
                exchangeCarDetails.setEvaluation_remarks(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getActivityDetails().getRemarks());
                exchangeCarDetails.setEvaluation_date_time(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getActivityDetails().getScheduled_at());
                exchangeCarDetails.setActivity_id(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getActivityDetails().getActivity()+"");
                exchangeCarDetails.setScheduled_at(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getActivityDetails().getScheduled_at());
                exchangeCarDetails.setRemarks(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getActivityDetails().getRemarks());
                exchangeCarDetails.setScheduled_activity_id(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getActivityDetails().getActivity_id()+"");
                exchangeCarDetails.setUser_name(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getActivityDetails().getUser_name());
            }

            for (int j = 0; j < cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getExchange_status().length; j++) {
                ExchangeStatusdb exchangeStatusdb = new ExchangeStatusdb();
                exchangeStatusdb.setLeadId(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id()));
                exchangeStatusdb.setBar_class(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getExchange_status()[j].getBar_class());
                exchangeStatusdb.setExchangestatusname(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getExchange_status()[j].getName());
                exchangeStatusdb.setWidth(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getExchange_status()[j].getWidth());
                exchangeStatusdb.setStage_id(cardDetailResponseData.getCustomer_details().getExchangeCarsDetails().get(i).getExchange_status()[j].getStage_id());
                exchangeCarDetails.exchangestatus.add(exchangeStatusdb);
            }
            realm.copyToRealmOrUpdate(exchangeCarDetails);

        }

        for (int i = 0; i < cardDetailResponseData.getCustomer_details().getAdditionalCarsDetails().size(); i++) {
            AdditionalCarsDetails additionalCarsDetails = new AdditionalCarsDetails();
            additionalCarsDetails.setLeadId(Long.parseLong(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_id()));
            additionalCarsDetails.setName(cardDetailResponseData.getCustomer_details().getAdditionalCarsDetails().get(i).getName());
            additionalCarsDetails.setModel_id(cardDetailResponseData.getCustomer_details().getAdditionalCarsDetails().get(i).getModel_id());
            additionalCarsDetails.setPrice_quoted(cardDetailResponseData.getCustomer_details().getAdditionalCarsDetails().get(i).getPrice_quoted());
            additionalCarsDetails.setMarket_price(cardDetailResponseData.getCustomer_details().getAdditionalCarsDetails().get(i).getMarket_price());
            additionalCarsDetails.setExpected_price(cardDetailResponseData.getCustomer_details().getAdditionalCarsDetails().get(i).getExpected_price());
            additionalCarsDetails.setPurchase_date(cardDetailResponseData.getCustomer_details().getAdditionalCarsDetails().get(i).getPurchase_date());
            additionalCarsDetails.setKms_run(cardDetailResponseData.getCustomer_details().getAdditionalCarsDetails().get(i).getKms_run());
            additionalCarsDetails.setReg_no(cardDetailResponseData.getCustomer_details().getAdditionalCarsDetails().get(i).getReg_no());

            realm.copyToRealmOrUpdate(additionalCarsDetails);

        }


        RealmResults<SalesCRMRealmTable> salesCRMRealmTableList = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(cardDetailResponseData.getLead_id()))
                .findAll();
        for (int i = 0; i < salesCRMRealmTableList.size(); i++) {
            SalesCRMRealmTable salesCRMRealmTable = salesCRMRealmTableList.get(i);
            if (salesCRMRealmTable != null) {
                salesCRMRealmTable.setTemplateExist(cardDetailResponseData.getCustomer_details().getCustomerDetails().isTemplate_exist());
                salesCRMRealmTable.setCustomerName(cardDetailResponseData.getCustomer_details().getCustomerDetails().getFirst_name() + " " + cardDetailResponseData.getCustomer_details().getCustomerDetails().getLast_name());
                salesCRMRealmTable.setFirstName(cardDetailResponseData.getCustomer_details().getCustomerDetails().getFirst_name());
                salesCRMRealmTable.setLastName(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLast_name());
                salesCRMRealmTable.setGender(cardDetailResponseData.getCustomer_details().getCustomerDetails().getGender());
                salesCRMRealmTable.setTitle(cardDetailResponseData.getCustomer_details().getCustomerDetails().getTitle());
                String customerAge = cardDetailResponseData.getCustomer_details().getCustomerDetails().getCustomer_age();
                salesCRMRealmTable.setCustomerAge(customerAge.equals("") ? null : Integer.parseInt(customerAge) + "");

                salesCRMRealmTable.setResidencePinCode(cardDetailResponseData.getCustomer_details().getCustomerDetails().getPin_code());
                salesCRMRealmTable.setResidenceAddress(cardDetailResponseData.getCustomer_details().getCustomerDetails().getAddress());
                salesCRMRealmTable.setResidenceLocality(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLocality());
                salesCRMRealmTable.setResidenceCity(cardDetailResponseData.getCustomer_details().getCustomerDetails().getCity());
                salesCRMRealmTable.setResidenceState(cardDetailResponseData.getCustomer_details().getCustomerDetails().getState());

                salesCRMRealmTable.setOfficePinCode(cardDetailResponseData.getCustomer_details().getCustomerDetails().getOffice_pin_code());
                salesCRMRealmTable.setOfficeAddress(cardDetailResponseData.getCustomer_details().getCustomerDetails().getOffice_address());
                salesCRMRealmTable.setOfficeLocality(cardDetailResponseData.getCustomer_details().getCustomerDetails().getOffice_locality());
                salesCRMRealmTable.setOfficeCity(cardDetailResponseData.getCustomer_details().getCustomerDetails().getOffice_city());
                salesCRMRealmTable.setOfficeState(cardDetailResponseData.getCustomer_details().getCustomerDetails().getState());

                System.out.println("Fetch 360 on ceate Company Before" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
                salesCRMRealmTable.setCompanyName(cardDetailResponseData.getCustomer_details().getCustomerDetails().getCompany_name());
                System.out.println("Fetch 360 on ceate Company after" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
                salesCRMRealmTable.setCustomerDesignation(cardDetailResponseData.getCustomer_details().getCustomerDetails().getDesignation());
                salesCRMRealmTable.setCustomerOccupation(cardDetailResponseData.getCustomer_details().getCustomerDetails().getOccupation());
                salesCRMRealmTable.setCustomerAddress(cardDetailResponseData.getCustomer_details().getCustomerDetails().getFull_address());
                salesCRMRealmTable.setOemSource(cardDetailResponseData.getCustomer_details().getCustomerDetails().getOem_source());
                salesCRMRealmTable.setCloseDate(cardDetailResponseData.getCustomer_details().getCustomerDetails().getClose_date());
                salesCRMRealmTable.setTypeOfCustomer(cardDetailResponseData.getCustomer_details().getCustomerDetails().getType_of_customer());
                salesCRMRealmTable.setModeOfPayment(cardDetailResponseData.getCustomer_details().getCustomerDetails().getMode_of_payment());
                salesCRMRealmTable.setBuyerType(cardDetailResponseData.getCustomer_details().getCustomerDetails().getBuyer_type_name());
                salesCRMRealmTable.setBuyerTypeId(cardDetailResponseData.getCustomer_details().getCustomerDetails().getBuyer_type_id());
                // System.out.println("ENQUIRY NUMBER FROM RESPONSE FETCH 360"+cardDetailResponseData.getCustomer_details().getCustomerDetails().getEnquiry_number());
                // salesCRMRealmTable.setEnqnumber(cardDetailResponseData.getCustomer_details().getCustomerDetails().getEnquiry_number());
                salesCRMRealmTable.customerPhoneNumbers.addAll(customerPhoneNumbers);
                salesCRMRealmTable.customerEmailId.addAll(customerEmailId);
                salesCRMRealmTable.setIsLeadActive(Integer.parseInt(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_active()));
                salesCRMRealmTable.setLeadAge(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_age());
                salesCRMRealmTable.setLeadLocationId(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_location().getId());
                salesCRMRealmTable.setLeadLocationName(cardDetailResponseData.getCustomer_details().getCustomerDetails().getLead_location().getName());
                salesCRMRealmTable.setDseId(dseId);
                salesCRMRealmTable.setCreatedOffline(isCreatedTemp);

            }
        }

    }

    @Override
    public void success(final AllLeadCustomerDetailsResponse allLeadCustomerDetailsResponse, Response response) {

        Util.updateHeaders(response.getHeaders());

        if (!allLeadCustomerDetailsResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("C360:Success:0" + allLeadCustomerDetailsResponse.getMessage());
            //showAlert(validateOtpResponse.getMessage());
        } else {
            if (allLeadCustomerDetailsResponse.getResult() != null) {
                System.out.println("C360:Success:1" + allLeadCustomerDetailsResponse.getMessage());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm1) {
                            insertIntoSalesFirst(realm1, allLeadCustomerDetailsResponse);

                    }
                });
               // listener.onC360InformationFetched(allLeadCustomerDetailsResponse);
            } else {
                // relLoader.setVisibility(View.GONE);
                System.out.println("C360:Success:2" + allLeadCustomerDetailsResponse.getMessage());

                //showAlert(validateOtpResponse.getMessage());
            }
        }

    }

    private void insertIntoSalesFirst(Realm realm, AllLeadCustomerDetailsResponse actionPlanResponseData) {
        if(actionPlanResponseData.getResult().size()>0) {
           /* if(actionPlanResponseData.getResult().get(0).getCustomer_details().getPlannedActivities().size()>0){
                RealmResults<SalesCRMRealmTable> dataRealm = realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", Integer.parseInt(actionPlanResponseData.getResult().get(0).getLead_id()))
                        .equalTo("isDone", false).findAll();
                for(int z=0;z<dataRealm.size();z++){
                    dataRealm.get(z).setDone(true);
                }
            }*/
            for (int j = 0; j < actionPlanResponseData.getResult().get(0).getCustomer_details().getPlannedActivities().size(); j++) {
                if (Util.isActionsValid(actionPlanResponseData.getResult().get(0).getCustomer_details().getPlannedActivities().get(j).getActions()) ||
                        isAllotDseOrPNT(actionPlanResponseData.getResult().get(0).getCustomer_details().getPlannedActivities())) {
                int i = 0;
                SalesCRMRealmTable salesCRMRealmTable = new SalesCRMRealmTable();
                salesCRMRealmTable.setScheduledActivityId(Integer.parseInt(actionPlanResponseData.getResult().get(i).getCustomer_details().getPlannedActivities().get(j).getScheduled_activity_id()));

                salesCRMRealmTable.setActivityName(actionPlanResponseData.getResult().get(i).getCustomer_details().getPlannedActivities().get(j).getName());
                salesCRMRealmTable.setActivityId(Util.getInt(actionPlanResponseData.getResult().get(i).getCustomer_details().getPlannedActivities().get(j).getActivity_id()));
                salesCRMRealmTable.setActivityScheduleDate(Util.getDate(actionPlanResponseData.getResult().get(i).getCustomer_details().getPlannedActivities().get(j).getDate_time()));


                salesCRMRealmTable.setActivityCreationDate(Util.getDate(actionPlanResponseData.getResult().get(i).getCustomer_details().getPlannedActivities().get(j).getActivity_creation_date()));

                salesCRMRealmTable.setCustomerId(Integer.parseInt(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getCustomer_id()));

                if (actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getMobile_nos().size() > 0 &&
                        actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getMobile_nos().get(0).getStatus().equalsIgnoreCase("PRIMARY")) {
                    salesCRMRealmTable.setCustomerNumber(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getMobile_nos().get(0).getNumber());

                }

                if (actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getEmail_ids().size() > 0 &&
                        actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getEmail_ids().get(0).getStatus().equalsIgnoreCase("PRIMARY")) {
                    salesCRMRealmTable.setCustomerEmail(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getEmail_ids().get(0).getAddress());

                }

                salesCRMRealmTable.setCustomerName(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getFirst_name()
                        + " " + actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLast_name());

                salesCRMRealmTable.setFirstName(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getFirst_name());
                salesCRMRealmTable.setLastName(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLast_name());
                salesCRMRealmTable.setGender(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getGender());
                salesCRMRealmTable.setTitle(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getTitle());
                salesCRMRealmTable.setTemplateExist(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().isTemplate_exist());
                String customerAge = actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getCustomer_age();
                salesCRMRealmTable.setCustomerAge(customerAge.equals("") ? "-1" : customerAge);

                salesCRMRealmTable.setResidencePinCode(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getPin_code());
                salesCRMRealmTable.setResidenceAddress(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getAddress());
                salesCRMRealmTable.setResidenceLocality(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLocality());
                salesCRMRealmTable.setResidenceCity(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getCity());
                salesCRMRealmTable.setResidenceState(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getState());

                salesCRMRealmTable.setOfficePinCode(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getOffice_pin_code());
                salesCRMRealmTable.setOfficeAddress(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getOffice_address());
                salesCRMRealmTable.setOfficeLocality(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getOffice_locality());
                salesCRMRealmTable.setOfficeCity(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getOffice_city());
                salesCRMRealmTable.setOfficeState(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getState());

                //  System.out.println("Fetch 360 on ceate Company Before 3" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
                salesCRMRealmTable.setCompanyName(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getCompany_name());
              //  System.out.println("Fetch 360 on ceate Company Before 3" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
                salesCRMRealmTable.setLeadId(Integer.parseInt(actionPlanResponseData.getResult().get(i).getLead_id()));

                for (int k = 0; k < actionPlanResponseData.getResult().get(i).getCustomer_details().getInterestedCarsDetails().size(); k++) {
                    if (actionPlanResponseData.getResult().get(i).getCustomer_details().getInterestedCarsDetails().get(k).getIs_primary().equalsIgnoreCase("1")) {
                        salesCRMRealmTable.setLeadCarVariantName(actionPlanResponseData.getResult().get(i).getCustomer_details().getInterestedCarsDetails().get(k).getVariant());
                        salesCRMRealmTable.setLeadCarModelName(actionPlanResponseData.getResult().get(i).getCustomer_details().getInterestedCarsDetails().get(k).getModel());
                        salesCRMRealmTable.setLeadCarModelId(actionPlanResponseData.getResult().get(i).getCustomer_details().getInterestedCarsDetails().get(k).getModel_id());
                        break;
                    }
                }


                if (actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_tags().size() > 0) {
                    salesCRMRealmTable.setLeadTagsName(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_tags().get(0).getName());
                    salesCRMRealmTable.setLeadTagsColor(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_tags().get(0).getColor());
                }

                if (actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getDse_details() != null) {
                    salesCRMRealmTable.setLeadDseName(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getDse_details().getDse_name());
                    salesCRMRealmTable.setLeadDsemobileNumber(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getDse_details().getDse_mobNo());
                }
                    RealmList<LeadStageProgressDB> leadStageProgressDBsList = new RealmList<>();
                    for (int k =0;
                         k < actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_stage_progress().length;
                         k++) {
                        AllLeadCustomerDetailsResponse.Result.Customer_details.CustomerDetails.Lead_stage_progress
                                currentData = actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_stage_progress()[k];
                        LeadStageProgressDB curProgressDB = realm.createObject(LeadStageProgressDB.class);
                        curProgressDB.setStageId(Util.getInt(currentData.getStage_id()));
                        curProgressDB.setName(currentData.getName());
                        curProgressDB.setWidth(currentData.getWidth());
                        curProgressDB.setBarClass(currentData.getBar_class());

                        leadStageProgressDBsList.add(curProgressDB);

                    }
                    salesCRMRealmTable.setLeadStageProgressDB(leadStageProgressDBsList);

                    for (int k = actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_stage_progress().length - 1;
                         k >= 0; k--) {
                        if (actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_stage_progress()[k].getWidth() == 100) {

                            salesCRMRealmTable.setLeadStageId(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_stage_progress()[k].getStage_id());
                            salesCRMRealmTable.setLeadStage(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_stage_progress()[k].getName());
                            break;
                        }
                    }


                salesCRMRealmTable.setLeadAge(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_age());
                salesCRMRealmTable.setLeadSourceName(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_source());
                salesCRMRealmTable.setVinRegNo(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getRef_vin_reg_no());
                salesCRMRealmTable.setLeadLastUpdated(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getLead_last_updated());
                salesCRMRealmTable.setLeadExpectedClosingDateAsNormalString(actionPlanResponseData.getResult().get(i).getCustomer_details().getCustomerDetails().getClose_date());
                salesCRMRealmTable.setDseId(dseId);
                salesCRMRealmTable.setCreatedOffline(isCreatedTemp);
                salesCRMRealmTable.setDone(false);
                if (actionPlanResponseData.getResult().get(i).getCustomer_details().getLastActivityOnLead() != null) {
                    salesCRMRealmTable.setActivityDescription(actionPlanResponseData.getResult().get(i).getCustomer_details().getLastActivityOnLead().getRemarks());

                }
                realm.copyToRealmOrUpdate(salesCRMRealmTable);
                insert(realm, actionPlanResponseData.getResult().get(i));
            }
        }

            listener.onFetchC360OnCreateLeadSuccess();
            createNotification();
        }
    }

    private boolean isAllotDseOrPNT(List<AllLeadCustomerDetailsResponse.Result.Customer_details.PlannedActivities> plannedActivities) {
        for (int i=0;i<plannedActivities.size();i++){
            if(plannedActivities.get(i).getActivity_id().equalsIgnoreCase(WSConstants.TaskActivityName.ALLOT_DSE_ID+"")
                    ||plannedActivities.get(i).getActivity_id().equalsIgnoreCase(WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY_ID+"")){
                return true;
            }
        }
        return false;
    }

    @Override
    public void failure(RetrofitError error) {
        System.out.println(error.toString());
        System.out.println("C360:Failed");
        System.out.println("C360:Error Kind:" + error.getKind());
        listener.onFetchC360OnCreateLeadError();
    }
    private void createNotification(){
        OrderedRealmCollection<SalesCRMRealmTable> realmResult = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", false)
                .greaterThanOrEqualTo("activityScheduleDate", Util.getTime(0))
                .lessThan("activityScheduleDate", Util.getTime(1))
                .equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("createdOffline", false)
                .findAll().sort("activityId", Sort.DESCENDING);

        for(int i=0;i<realmResult.size();i++){
            if(realmResult.get(i).getActivityScheduleDate().getTime()>System.currentTimeMillis()) {
                SalesCRMRealmTable salesCRMRealmTable = realmResult.get(i);
                Calendar appNotificationTime = Calendar.getInstance();
                appNotificationTime.setTime(salesCRMRealmTable.getActivityScheduleDate());
                switch (salesCRMRealmTable.getActivityId()){
                    case WSConstants.TaskActivityName.FOLLOWUP_CALL_ID:
                    case WSConstants.TaskActivityName.ARRANGE_CALL_FROM_DSE:
                        //Before five minutes!!
                        appNotificationTime.add(Calendar.MINUTE,-5);
                        break;
                    case WSConstants.TaskActivityName.HOME_VISIT_ID:
                        //Before 2Hr minutes!!
                    case WSConstants.TaskActivityName.SHOWROOM_VISIT_ID:
                        //Before 2Hr minutes!!
                    case WSConstants.TaskActivityName.EVALUATION_REQUEST_ID:
                        //Before 2Hr minutes!!
                    case WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID:
                        //Before 2Hr
                    case WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID:
                        if(pref.isClientILom()){
                            appNotificationTime.add(Calendar.MINUTE, -5);
                        }else {
                            appNotificationTime.add(Calendar.HOUR_OF_DAY, -2);
                        }
                        break;
                    case WSConstants.TaskActivityName.RECEIVE_PAYMENT_ID:
                        //Before 5min
                        appNotificationTime.add(Calendar.MINUTE,-5);
                        break;

                    case WSConstants.TaskActivityName.DELIVERY_REQUEST_ID:
                        //At 9AM
                        appNotificationTime.set(Calendar.HOUR_OF_DAY,9);
                        appNotificationTime.set(Calendar.MINUTE,0);
                        break;


                }
                if(appNotificationTime.getTime().getTime()>System.currentTimeMillis()) {
                    Util.setNotification(context,
                            salesCRMRealmTable.getActivityName(),
                            salesCRMRealmTable.getFirstName()+" "+salesCRMRealmTable.getLastName(),
                            salesCRMRealmTable.getLeadId(),
                            salesCRMRealmTable.getScheduledActivityId(),
                            appNotificationTime.getTime().getTime(),
                            salesCRMRealmTable.getActivityScheduleDate().getTime(), salesCRMRealmTable.getActivityId());

                    System.out.println("Setting alarm for " + salesCRMRealmTable.getScheduledActivityId() + " at:" +
                            appNotificationTime.getTime());
                }
            }
        }
    }

}
