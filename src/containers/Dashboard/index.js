import React, { Component } from 'react'
import {
  Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
  StyleSheet, Modal, NativeModules, TouchableWithoutFeedback, KeyboardAvoidingView, ScrollView, TextInput
} from 'react-native'
import { getDashboardNavigator } from '../../navigation/DashboardNavigator'
import NavigationService from '../../navigation/NavigationService'
const innerStyles = StyleSheet.create({
  selected: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 16,
    alignItems: 'center',
    marginTop: 5,
    fontWeight: 'bold',
  },

  unseleceted: {
    alignSelf: 'center',
    color: '#999999',
    fontSize: 14,
    alignItems: 'center',
    marginTop: 7,
    fontWeight: 'normal'
  },
  containerStyle: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 9,
    },
    shadowOpacity: 0.50,
    shadowRadius: 12.35,
    elevation: 19,
    backgroundColor: '#fff',
    flexDirection: 'row',
    height: 40
  },
  bottomMenuStyle: {
    flexDirection: 'row',
    height: 40,
    backgroundColor: '#53759D'
  }

});

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      used_car: false
    }
  }

  selectNewCar = () => {
    //let { used_car } = this.state;
    global.USEDCAR = false;
    this.setState({ used_car: false })
  }

  selectUsedCar = () => {
    //let { used_car } = this.state;
    global.USEDCAR = true;
    this.setState({ used_car: true })
  }

  render() {
    //console.error(this.props.isUserEM, this.props.isUserEV)
    let DASHBOARD_VIEW = getDashboardNavigator(this.props.navigation,
      this.props.isUserBH, this.props.isUserOps, this.props.isUserSM, this.props.isUserEM, this.props.isUserEV, this.props.isUserTL, this.props.isUserSC, this.props.isUserVF,
      {
        allLocations: this.props.allLocations,
        enquirySourceCategories: this.props.enquirySourceCategories,
        eventCategories: this.props.eventCategories,
        notification: this.props.notification ? this.props.notification : null,
        uniqueAcrossDealerShip: this.props.uniqueAcrossDealerShip,
        addLeadEnabled: this.props.addLeadEnabled,
        coldVisitEnabled: this.props.coldVisitEnabled,
        showLeadCompetitor: this.props.showLeadCompetitor,
        vehcileModel: this.props.vehcileModel,
        enquirySource: this.props.enquirySource,
        isClientILom: this.props.isClientILom,
        isClientILBank: this.props.isClientILBank,
        isClientTwoWheeler: this.props.isClientTwoWheeler,
        hotlineNo: this.props.hotlineNo
      },
      false
    );
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          <DASHBOARD_VIEW
          />
        </View>
      </View>

    );
  }
}