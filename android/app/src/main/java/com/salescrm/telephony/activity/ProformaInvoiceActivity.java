package com.salescrm.telephony.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.AutoCompleteTextViewAdapter;
import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandModelsDB;
import com.salescrm.telephony.interfaces.ProformaInvoiceInterface;
import com.salescrm.telephony.model.Item;
import com.salescrm.telephony.model.ProformaInvoice;
import com.salescrm.telephony.model.ProformaInvoiceAnswer;
import com.salescrm.telephony.model.ProformaPdfResponse;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by prateek on 14/5/18.
 */

public class ProformaInvoiceActivity extends AppCompatActivity implements View.OnClickListener,
        ProformaInvoiceInterface, View.OnFocusChangeListener{
    private RelativeLayout relLoading;
    private EditText edtName, edtAddress;
    private AutoCompleteTextView edtModel, edtVariant, edtColor;
    private RelativeLayout llProformaCustomerDetailHeader;
    private RelativeLayout llProformaInvoiceItemsHeader;
    private LinearLayout llProformaCustomerDetailContent;
    private LinearLayout llProformaInvoiceItemsContent;
    private boolean LL_PROFORMA_CUSTOMER_DETAIL = false;
    private boolean LL_PROFORMA_INVOICE_ITMES = false;
    private ImageView imgProformaCustomerDetail;
    private ImageView imgProformaInvoiceItems;
    private Drawable imgExpand, imgCollapse;
    private Drawable imgGreenCheck;
    private FrameLayout btnPreview;
    private TextView tvProformaCustomerDetailHeader;
    private TextView tvProformaInvoiceItmesHeader;
    private List<EditText> edtList;
    private List<LinearLayout> llList;
    private List<EditText> edtListPercent;
    private ProformaInvoiceInterface proformaInvoiceInterface;
    private TextView tvOnRoadPrice;
    private LinearLayout llOnRoadPrice;
    private ProformaInvoice proformaInvoiceGlobal;
    private Realm realm;
    private String  modelId, variantId, colorId;
    private List<Item> dataModel, dataVariant, dataColor;
    private HashMap<Integer, Boolean> customerDetailValidity;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private ProformaInvoiceAnswer proformaInvoiceAnswer;
    private ProformaInvoiceAnswer.CustomerAndVehicleDetail customerAndVehicleDetail;
    private List<ProformaInvoiceAnswer.InvoiceItemsAnswer> llInvoiceItemsAnswers;
    private String leadId, name, phone;
    private Toolbar toolbar;
    private TextView customTitle;
    private Preferences pref;
    private ScrollView scrollView;
    private static ProformaInvoiceActivity proformaInvoiceActivityInstance;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish(); // close this activity and return to previous activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    public static ProformaInvoiceActivity getInstance(){
        return  proformaInvoiceActivityInstance;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        setContentView(R.layout.proforma_invoice_layout);
        relLoading = (RelativeLayout) findViewById(R.id.rel_loading_frame);
        scrollView = (ScrollView) findViewById(R.id.proforma_scrollView);
        toolbar = (Toolbar) findViewById(R.id.proforma_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        customTitle = (TextView) findViewById(R.id.custom_title);
        customTitle.setText("Proforma Invoice");
        proformaInvoiceActivityInstance = this;
        llProformaCustomerDetailHeader = (RelativeLayout) findViewById(R.id.ll_proforma_customer_detail_header);
        llProformaCustomerDetailContent = (LinearLayout) findViewById(R.id.ll_proforma_customer_detail_content);
        llProformaInvoiceItemsHeader = (RelativeLayout) findViewById(R.id.ll_proforma_invoice_items_header);
        llProformaInvoiceItemsContent = (LinearLayout) findViewById(R.id.ll_proforma_invoice_itmes_content);
        imgProformaCustomerDetail = (ImageView) findViewById(R.id.img_proforma_customer_detail_header);
        imgProformaInvoiceItems = (ImageView) findViewById(R.id.img_proforma_invoice_items_header);
        btnPreview = (FrameLayout) findViewById(R.id.btn_preview);
        tvProformaCustomerDetailHeader  = (TextView) findViewById(R.id.tv_proforma_customer_detail_header);
        tvProformaInvoiceItmesHeader =  (TextView) findViewById(R.id.tv_proforma_invoice_items_header);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group_proforma_address);
        relLoading.setVisibility(View.VISIBLE);
        edtList = new ArrayList<>();
        llList = new ArrayList<>();
        edtListPercent = new ArrayList<>();
        customerDetailValidity = new HashMap<>();
        llInvoiceItemsAnswers = new ArrayList<>();
        proformaInvoiceAnswer = new ProformaInvoiceAnswer();
        proformaInvoiceInterface = (ProformaInvoiceInterface) this;
        llOnRoadPrice = (LinearLayout) findViewById(R.id.ll_onroad_price);
        tvOnRoadPrice = (TextView) findViewById(R.id.tv_onroad_price);
        edtName = (EditText) findViewById(R.id.edt_name);
        edtAddress = (EditText) findViewById(R.id.edt_address);
        edtModel = (AutoCompleteTextView) findViewById(R.id.edt_model);
        edtVariant = (AutoCompleteTextView) findViewById(R.id.edt_variant);
        edtColor = (AutoCompleteTextView) findViewById(R.id.edt_color);

        edtName.addTextChangedListener(new CustomTextWatcher(edtName));
        edtAddress.addTextChangedListener(new CustomTextWatcher(edtAddress));
        edtModel.addTextChangedListener(new CustomTextWatcher(edtModel));
        edtVariant.addTextChangedListener(new CustomTextWatcher(edtVariant));
        edtColor.addTextChangedListener(new CustomTextWatcher(edtColor));

        imgCollapse = VectorDrawableCompat.create(getResources(),
                R.drawable.ic_expand_less_24dp, getTheme());
        imgExpand = VectorDrawableCompat.create(getResources(),
                R.drawable.ic_expand_more_24dp, getTheme());
        imgGreenCheck = VectorDrawableCompat.create(getResources(),
                R.drawable.ic_check_color_24dp, getTheme());

        leadId = pref.getLeadID()!= null?pref.getLeadID():"";
        phone = getIntent().getStringExtra("phone") != null ? getIntent().getStringExtra("phone") : "";

        pref.setProformaSent(false);
        makeApiCall();
        setAdapterForModel();
        setInitialView();
        clickableObjects();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioButton = (RadioButton) findViewById(checkedId);
                switch (checkedId){
                    case (R.id.rb_home_address):
                        if(proformaInvoiceGlobal != null)
                        edtAddress.setText(""+proformaInvoiceGlobal.getResult().getCustomerDetails().getHomeAddress());
                        break;
                    case (R.id.rb_office_address):
                        if(proformaInvoiceGlobal != null)
                        edtAddress.setText(""+proformaInvoiceGlobal.getResult().getCustomerDetails().getOfficeAddress());
                        break;
                }
            }
        });

        edtModel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                modelId = ((Item) view.getTag()).getId()+"";
                edtVariant.setText("");
                edtColor.setText("");
                setAdapterForVariant(((Item) view.getTag()));
                edtVariant.requestFocus();
                Util.HideKeyboard(ProformaInvoiceActivity.this);
            }
        });

        edtVariant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                variantId = ((Item) view.getTag()).getId()+"";
                edtColor.setText("");
                setAdapterForColor(((Item) view.getTag()));
                edtColor.requestFocus();
                Util.HideKeyboard(ProformaInvoiceActivity.this);
            }
        });
        edtColor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                colorId = ((Item) view.getTag()).getId()+"";
                Util.HideKeyboard(ProformaInvoiceActivity.this);
            }
        });
    }

    private void fillUpCustomerAndVehicleDetail(ProformaInvoice proformaInvoice) {
        if(proformaInvoice.getResult().getCustomerDetails().getName()!= null && !proformaInvoice.getResult().getCustomerDetails().getName().equalsIgnoreCase("")){
            edtName.setText(proformaInvoice.getResult().getCustomerDetails().getName());
            customerDetailValidity.put(edtName.getId(), true);
        }else {
            customerDetailValidity.put(edtName.getId(), false);
        }
        if(proformaInvoice.getResult().getCustomerDetails().getHomeAddress()!= null && !proformaInvoice.getResult().getCustomerDetails().getHomeAddress().equalsIgnoreCase("")){
            edtAddress.setText(proformaInvoice.getResult().getCustomerDetails().getHomeAddress());
            customerDetailValidity.put(edtAddress.getId(), true);
            radioGroup.check(R.id.rb_home_address);
        }else if(proformaInvoice.getResult().getCustomerDetails().getOfficeAddress()!= null && !proformaInvoice.getResult().getCustomerDetails().getOfficeAddress().equalsIgnoreCase("")) {
            edtAddress.setText(proformaInvoice.getResult().getCustomerDetails().getOfficeAddress());
            customerDetailValidity.put(edtAddress.getId(), true);
            radioGroup.check(R.id.rb_office_address);
        }else{
            radioGroup.check(R.id.rb_home_address);
            customerDetailValidity.put(edtAddress.getId(), false);
        }
        if(proformaInvoice.getResult().getCustomerDetails().getModelId()!= null && !proformaInvoice.getResult().getCustomerDetails().getModelId().equalsIgnoreCase("")){
            modelId = proformaInvoice.getResult().getCustomerDetails().getModelId();
            edtModel.setText(proformaInvoice.getResult().getCustomerDetails().getModelName());
            customerDetailValidity.put(edtModel.getId(), true);
            setAdapterForVariantWithId(proformaInvoice.getResult().getCustomerDetails().getModelId());
        }else{
            customerDetailValidity.put(edtModel.getId(), false);
        }
        if(proformaInvoice.getResult().getCustomerDetails().getVariantId()!= null && !proformaInvoice.getResult().getCustomerDetails().getVariantId().equalsIgnoreCase("")){
            variantId = proformaInvoice.getResult().getCustomerDetails().getVariantId();
            edtVariant.setText(proformaInvoice.getResult().getCustomerDetails().getVariantName());
            customerDetailValidity.put(edtVariant.getId(), true);
            setAdapterForColorWithId(proformaInvoice.getResult().getCustomerDetails().getVariantId());
        }else {
            customerDetailValidity.put(edtVariant.getId(), false);
        }
        if(proformaInvoice.getResult().getCustomerDetails().getColorId()!= null && !proformaInvoice.getResult().getCustomerDetails().getColorId().equalsIgnoreCase("")){
            colorId = proformaInvoice.getResult().getCustomerDetails().getColorId();
            edtColor.setText(proformaInvoice.getResult().getCustomerDetails().getColorName());
            customerDetailValidity.put(edtColor.getId(), true);
        }else {
            colorId = null;
            customerDetailValidity.put(edtColor.getId(), true);
        }

        edtModel.setOnFocusChangeListener(this);
        edtVariant.setOnFocusChangeListener(this);
        edtColor.setOnFocusChangeListener(this);
    }

    private void setAdapterForVariantWithId(String modelId) {
        CarBrandModelsDB realmDataCarVariant = realm.where(CarBrandModelsDB.class)
                .equalTo("category", WSConstants.CAR_CATEGORY_BOTH)
                .findAll()
                .where().equalTo("model_id",
                        modelId + "")
                .findFirst();

        if (realmDataCarVariant != null && realmDataCarVariant.isValid()) {
            RealmResults<CarBrandModelVariantsDB> carBrandModelVariantsDBS = realmDataCarVariant.getCar_variants().where()
                    .distinct("variant_id");

            dataVariant = new ArrayList<>();

            for (int i = 0; i < carBrandModelVariantsDBS.size(); i++) {
                if (carBrandModelVariantsDBS.get(i) != null &&
                        carBrandModelVariantsDBS.get(i).getVariant() != null) {
                    dataVariant.add(new Item(carBrandModelVariantsDBS.get(i).getVariant(),
                            carBrandModelVariantsDBS.get(i).getVariant_id()));
                }

            }

            edtVariant.setAdapter(new AutoCompleteTextViewAdapter( this, dataVariant));
        }
    }
    private void setAdapterForColorWithId(String variantId) {
        if(realm.where(CarBrandModelsDB.class)
                .equalTo("category", WSConstants.CAR_CATEGORY_BOTH)
                .findAll().where().equalTo("model_id", modelId + "")
                .findFirst() != null) {
            RealmResults<CarBrandModelVariantsDB> carBrandModelVariantsDBS = realm.where(CarBrandModelsDB.class)
                    .equalTo("category", WSConstants.CAR_CATEGORY_BOTH)
                    .findAll().where().equalTo("model_id", modelId + "")
                    .findFirst().getCar_variants().where().equalTo("variant_id",
                            variantId + "").findAll();

            dataColor = new ArrayList<>();
            for (int i = 0; i < carBrandModelVariantsDBS.size(); i++) {
                if (carBrandModelVariantsDBS.get(i) != null && carBrandModelVariantsDBS.get(i).getColor() != null) {
                    dataColor.add(new Item(carBrandModelVariantsDBS.get(i).getColor(), carBrandModelVariantsDBS.get(i).getColor_id()));
                }
            }
            edtColor.setAdapter(new AutoCompleteTextViewAdapter(this, dataColor));
        }

    }

    private void setAdapterForColor(Item itemVariant) {
        if(realm.where(CarBrandModelsDB.class)
                .equalTo("category", WSConstants.CAR_CATEGORY_BOTH)
                .findAll().where().equalTo("model_id", modelId + "")
                .findFirst() != null) {
            RealmResults<CarBrandModelVariantsDB> carBrandModelVariantsDBS = realm.where(CarBrandModelsDB.class)
                    .equalTo("category", WSConstants.CAR_CATEGORY_BOTH)
                    .findAll().where().equalTo("model_id", modelId + "")
                    .findFirst().getCar_variants().where().equalTo("variant_id",
                            itemVariant.getId() + "").findAll();

            dataColor = new ArrayList<>();
            for (int i = 0; i < carBrandModelVariantsDBS.size(); i++) {
                if (carBrandModelVariantsDBS.get(i) != null && carBrandModelVariantsDBS.get(i).getColor() != null) {
                    dataColor.add(new Item(carBrandModelVariantsDBS.get(i).getColor(), carBrandModelVariantsDBS.get(i).getColor_id()));
                }
            }
            edtColor.setAdapter(new AutoCompleteTextViewAdapter(this, dataColor));
        }
    }

    private void setAdapterForVariant(Item item) {
        CarBrandModelsDB realmDataCarVariant = realm.where(CarBrandModelsDB.class)
                .equalTo("category", WSConstants.CAR_CATEGORY_BOTH)
                .findAll()
                .where().equalTo("model_id",
                        item.getId() + "")
                .findFirst();

        if (realmDataCarVariant != null && realmDataCarVariant.isValid()) {
            RealmResults<CarBrandModelVariantsDB> carBrandModelVariantsDBS = realmDataCarVariant.getCar_variants().where()
                    .distinct("variant_id");

            dataVariant = new ArrayList<>();

            for (int i = 0; i < carBrandModelVariantsDBS.size(); i++) {
                if (carBrandModelVariantsDBS.get(i) != null &&
                        carBrandModelVariantsDBS.get(i).getVariant() != null) {
                    dataVariant.add(new Item(carBrandModelVariantsDBS.get(i).getVariant(),
                            carBrandModelVariantsDBS.get(i).getVariant_id()));
                }

            }

            edtVariant.setAdapter(new AutoCompleteTextViewAdapter( this, dataVariant));
        }

    }

    private void setAdapterForModel() {
        RealmResults<CarBrandModelsDB> carBrandModelsDBS = realm.where(CarBrandModelsDB.class)
                .equalTo("category", WSConstants.CAR_CATEGORY_BOTH)
                .findAll();
        dataModel = new ArrayList<Item>();
        for (int i = 0; i < carBrandModelsDBS.size(); i++) {
            if (carBrandModelsDBS.get(i) != null
                    && carBrandModelsDBS.get(i).getModel_name() != null) {
                dataModel.add(new Item(carBrandModelsDBS.get(i).getModel_name(), carBrandModelsDBS.get(i).getModel_id()));
            }
        }
        edtModel.setAdapter(new AutoCompleteTextViewAdapter( this, dataModel));
    }

    private void setInitialView() {
        imgProformaCustomerDetail.setImageDrawable(imgExpand);
        imgProformaInvoiceItems.setImageDrawable(imgExpand);
        LL_PROFORMA_CUSTOMER_DETAIL = false;
        LL_PROFORMA_INVOICE_ITMES = false;
    }

    private void clickableObjects() {
        llProformaCustomerDetailHeader.setOnClickListener(this);
        llProformaInvoiceItemsHeader.setOnClickListener(this);
        btnPreview.setOnClickListener(this);
    }

    private void makeApiCall() {
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getProformaInvoiceDetails(leadId, new Callback<ProformaInvoice>() {
            @Override
            public void success(ProformaInvoice proformaInvoice, Response response) {
                if(proformaInvoice.getResult().getTemplateExist()) {
                    scrollView.setVisibility(View.VISIBLE);
                    btnPreview.setVisibility(View.VISIBLE);
                    relLoading.setVisibility(View.GONE);
                    proformaInvoiceGlobal = proformaInvoice;
                    if(proformaInvoiceGlobal.getResult().getInvoiceItems() == null){
                        ProformaInvoiceActivity.this.finish();
                    }
                    populateView(proformaInvoice);
                    setVisibilityForAllPercentViews(proformaInvoice, "0");
                }else {
                    relLoading.setVisibility(View.GONE);
                    btnPreview.setVisibility(View.GONE);
                    scrollView.setVisibility(View.GONE);
                    Util.showToast(ProformaInvoiceActivity.this, "Template for Proforma Invoice is not set!!", Toast.LENGTH_LONG);
                    //ProformaInvoiceActivity.this.finish();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                relLoading.setVisibility(View.GONE);
                ProformaInvoiceActivity.this.finish();
            }
        });
    }

    private void populateView(ProformaInvoice proformaInvoice) {

        fillUpCustomerAndVehicleDetail(proformaInvoice);
        llProformaInvoiceItemsContent.removeAllViews();
        LinearLayout.LayoutParams paramsLabel = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.WRAP_CONTENT, 4.5f);
        LinearLayout.LayoutParams paramsPercentage = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.WRAP_CONTENT, 2.0f);
        LinearLayout.LayoutParams paramsValues = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.WRAP_CONTENT, 3.5f);
        //params.weight = 1.0f;
        LinearLayout.LayoutParams llParam;
        if(Build.VERSION.SDK_INT > 23) {
            llParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
        }else{
            llParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100);
        }

        llParam.setMargins(0,5,0,5);

        clearAllLists();
        for(int i = 0; i<proformaInvoice.getResult().getInvoiceItems().size(); i++){
            LinearLayout ll = new LinearLayout(this);
            //ll.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            ll.setOrientation(LinearLayout.HORIZONTAL);
            ll.setLayoutParams(llParam);

            TextView tv = new TextView(this);
            tv.setText(proformaInvoice.getResult().getInvoiceItems().get(i).getLabel()+" "+
                    (!proformaInvoice.getResult().getInvoiceItems().get(i).getPercent().equalsIgnoreCase("0") &&
                            !proformaInvoice.getResult().getInvoiceItems().get(i).getPercent().equalsIgnoreCase("100")
                    ? "@" : ""));

            tv.setTextSize(14);
            tv.setTypeface(null, Typeface.NORMAL);
            tv.setLayoutParams(paramsLabel);
            ll.addView(tv);

            EditText edtPercent = new EditText(this);
            edtPercent.setTextSize(16);
            edtPercent.setSingleLine();
            //edtPercent.setFilters();
            edtPercent.setLayoutParams(paramsPercentage);
            edtPercent.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
            edtPercent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5), inputFilterPer});
            edtPercent.setHint("%");
            if(!proformaInvoice.getResult().getInvoiceItems().get(i).getPercent().equalsIgnoreCase("0")){
                edtPercent.setVisibility(View.VISIBLE);
                //edtListPercent.get(i).setClickable(false);
                //edtListPercent.get(i).setFocusable(false);
                //setEditTextPercentageListner(edtPercent, proformaInvoice);
            }else {
                edtPercent.setVisibility(View.INVISIBLE);
            }
            setEditTextPercentageListner(edtPercent, proformaInvoice, i);
            edtListPercent.add(edtPercent);
            ll.addView(edtPercent);

            TextView tvRs = new TextView(this);
            tvRs.setText("Rs");
            tvRs.setTextSize(14);
            ll.addView(tvRs);

            EditText edt = new EditText(this);
            //edt.setText((i==0? "0":""));
            edt.setLayoutParams(paramsValues);
            edt.setTextSize(16);
            edt.setGravity(Gravity.RIGHT);
            edt.setSelection(edt.getText().length());
            edt.setSingleLine();
            edt.setInputType(InputType.TYPE_CLASS_NUMBER);
            edt.setId(proformaInvoice.getResult().getInvoiceItems().get(i).getId());
            edt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(9), inputFilterEdt});
            if(!proformaInvoice.getResult().getInvoiceItems().get(i).getPercent().equalsIgnoreCase("0")){
                edt.setClickable(false);
                edt.setFocusable(false);
            }
            setEditTextChangeListener(edt, proformaInvoice, i);
            ll.addView(edt);
            edtList.add(edt);
            llList.add(ll);
            llProformaInvoiceItemsContent.addView(ll);
        }
    }

    private void setEditTextPercentageListner(final EditText edtPercent, final ProformaInvoice proformaInvoice, final int pos) {
        edtPercent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(edtPercent.getText().toString().length() ==1 && edtPercent.getText().toString().equalsIgnoreCase(".")){
                    edtPercent.setText("");
                }else {
                    setPercentage(proformaInvoice, s.toString(), edtPercent, pos);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setPercentage(ProformaInvoice proformaInvoice, String per, EditText edtPercent, int pos) {
        //for(int i = 1; i<proformaInvoice.getResult().getInvoiceItems().size(); i++){
            if(llList.get(pos).getVisibility() == View.VISIBLE) {
                if (!proformaInvoice.getResult().getInvoiceItems().get(pos).getPercent().equalsIgnoreCase("0")) {
                    if(!per.equalsIgnoreCase("") && !edtList.get(0).getText().toString().equalsIgnoreCase("")) {
                        Double percentage = Double.parseDouble(per);
                        Double value = (Integer.parseInt(edtList.get(0).getText().toString()) * percentage) / 100;
                        edtList.get(pos).setText(Math.round(value) + "");
                        //edtList.get(i).setEnabled(false);
                    }else {
                        edtList.get(pos).setText("");
                    }
                }
           }
        //}
    }

    private void clearAllLists() {
        edtList.clear();
        llList.clear();
        edtListPercent.clear();
    }

    private void setEditTextChangeListener(final EditText edt, final ProformaInvoice proformaInvoice, final int position) {
        edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(((Integer) edt.getId() == proformaInvoice.getResult().getInvoiceItems().get(0).getId())) {
                    if (!s.toString().equalsIgnoreCase("") && !s.toString().equalsIgnoreCase("0")) {
                        //Double showroomPrice = Double.parseDouble(s.toString());
                        setVisibilityForAllPercentViews(proformaInvoice, s.toString());
                    } else {
                        setVisibilityForAllPercentViews(proformaInvoice, "0");
                    }
                }else {

                }
                proformaInvoiceInterface.edtNotEmptyListner();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setVisibilityForAllPercentViews(ProformaInvoice proformaInvoice, String showroomPrice) {
        for(int j = 1; j<proformaInvoice.getResult().getInvoiceItems().size(); j++){
            //if(!showroomPrice.equalsIgnoreCase("0")) {
                edtListPercent.get(j).setText("");
                if (proformaInvoice.getResult().getInvoiceItems().get(j).getMinPrice() != 0
                        && Integer.parseInt(showroomPrice) < proformaInvoice.getResult().getInvoiceItems().get(j).getMinPrice()) {
                    edtList.get(j).setText("");
                    llList.get(j).setVisibility(View.GONE);
                } else {
                    llList.get(j).setVisibility(View.VISIBLE);
                }
           /* }else{
                edtListPercent.get(j).setText("");
            }*/

           if(!proformaInvoice.getResult().getInvoiceItems().get(j).getPercent().equalsIgnoreCase("0")){
               //edtList.get(j).setBackgroundColor(Color.TRANSPARENT);
               edtList.get(j).setBackground(null);
               edtList.get(j).setPadding(0, 0, 10, 0);
           }
        }
    }

    private ProformaInvoiceAnswer getAllInvoiceData() {
        customerAndVehicleDetail = proformaInvoiceAnswer.new CustomerAndVehicleDetail();
        if(radioGroup.getCheckedRadioButtonId() == R.id.rb_office_address){
            customerAndVehicleDetail.setOffice_address(edtAddress.getText().toString());
            customerAndVehicleDetail.setHome_address(null);
        }else{
            customerAndVehicleDetail.setOffice_address(null);
            customerAndVehicleDetail.setHome_address(edtAddress.getText().toString());
        }

        customerAndVehicleDetail.setName(edtName.getText().toString());
        customerAndVehicleDetail.setModel_id(modelId);
        customerAndVehicleDetail.setVariant_id(variantId);
        customerAndVehicleDetail.setColor_id(colorId);
        proformaInvoiceAnswer.setCustomer_details(customerAndVehicleDetail);

        proformaInvoiceAnswer.setOn_road_price(tvOnRoadPrice.getText().toString());
        proformaInvoiceAnswer.setLead_id(leadId);
        llInvoiceItemsAnswers.clear();
        for(int i =0; i<edtList.size(); i++){
            ProformaInvoiceAnswer.InvoiceItemsAnswer invoiceItemsAnswer = proformaInvoiceAnswer. new InvoiceItemsAnswer();
            if(!edtList.get(i).getText().toString().equalsIgnoreCase("")){
                invoiceItemsAnswer.setId(edtList.get(i).getId()+"");
                invoiceItemsAnswer.setValue(edtList.get(i).getText()+"");
                invoiceItemsAnswer.setPercentage(edtListPercent.get(i).getText()+"");
                llInvoiceItemsAnswers.add(invoiceItemsAnswer);
            }
        }
        proformaInvoiceAnswer.setInvoice_items(llInvoiceItemsAnswers);

        return proformaInvoiceAnswer;
    }

    private boolean validationDone() {
        for (boolean valid : customerDetailValidity.values()) {
            if(!valid)
                return false;
        }
        return true;
    }

    private void selectCarModel() {
        edtModel.showDropDown();
    }

    private void selectCarVariant() {
        edtVariant.showDropDown();
    }

    private void selectCarColor() {
        edtColor.showDropDown();
    }

    @Override
    public void edtNotEmptyListner() {
        if(getOnRoadPrice() != 0){
            //llOnRoadPrice.setVisibility(View.VISIBLE);
            tvOnRoadPrice.setText(getOnRoadPrice()+"");
        }else {
            //llOnRoadPrice.setVisibility(View.VISIBLE);
            tvOnRoadPrice.setText("0");
        }
    }

    private int getOnRoadPrice() {
        int sum = 0;
        for(int i =0; i<edtList.size(); i++){
            if(!edtList.get(i).getText().toString().equalsIgnoreCase("")){
                if(proformaInvoiceGlobal.getResult().getInvoiceItems().get(i).getSign().equalsIgnoreCase("1")){
                    sum = sum + Integer.parseInt(edtList.get(i).getText().toString());
                }else{
                    sum = sum - Integer.parseInt(edtList.get(i).getText().toString());
                }
            }
        }
        return sum;
    }

    private boolean isNotEmpty() {
        for(int i=0; i<edtList.size(); i++) {
            if (llList.get(i).getVisibility() == View.VISIBLE) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus && v instanceof AutoCompleteTextView
                && (((AutoCompleteTextView) v).getAdapter() != null)
                && (((AutoCompleteTextView) v).getText().toString().isEmpty())) {
            ((AutoCompleteTextView) v).showDropDown();
        }
    }

    private class CustomTextWatcher implements TextWatcher {
        private EditText editText;

        CustomTextWatcher(EditText et) {
            this.editText = et;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (editText.getId()) {
                case R.id.edt_name:
                    if(edtName.getText().toString().trim().length() == 0){
                        edtName.setError("Field can't be left blank");
                        customerDetailValidity.put(edtName.getId(), false);
                    }else {
                        customerDetailValidity.put(edtName.getId(), true);
                        edtModel.setError(null);
                    }
                    break;
                case R.id.edt_address:
                    if(edtAddress.getText().toString().trim().length() == 0){
                        edtAddress.setError("Field can't be left blank");
                        customerDetailValidity.put(edtAddress.getId(), false);
                    }else {
                        customerDetailValidity.put(edtAddress.getId(), true);
                        edtAddress.setError(null);
                    }
                    break;

                case R.id.edt_model:
                    if(checkModelAvailable(edtModel.getText().toString().trim())){
                        customerDetailValidity.put(edtModel.getId(), true);
                        edtModel.setError(null);
                    }else {
                        edtModel.setError("Entered model is wrong");
                        customerDetailValidity.put(edtModel.getId(), false);
                    }
                    break;
                case R.id.edt_variant:
                    if(checkVariantAvailable(edtVariant.getText().toString().trim())){
                        customerDetailValidity.put(edtVariant.getId(), true);
                        edtVariant.setError(null);
                    }else {
                        edtVariant.setError("Entered variant is wrong");
                        customerDetailValidity.put(edtVariant.getId(), false);
                    }
                    break;
                case R.id.edt_color:
                    if(checkColorAvailable(edtColor.getText().toString().trim())){
                        customerDetailValidity.put(edtColor.getId(), true);
                        edtColor.setError(null);
                    }else {
                        colorId = null;
                        edtColor.setError(null);
                        customerDetailValidity.put(edtColor.getId(), true);
                    }
                    break;

            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            System.out.println("After text changed");
        }
    }

    private boolean checkModelAvailable(String model) {
        if(dataModel != null) {
            for (int i = 0; i < dataModel.size(); i++) {
                if (dataModel.get(i).getText().equalsIgnoreCase(model)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkVariantAvailable(String variant) {
        if(dataVariant != null) {
            for (int i = 0; i < dataVariant.size(); i++) {
                if (dataVariant.get(i).getText().equalsIgnoreCase(variant)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkColorAvailable(String color) {
        if(dataColor != null) {
            for (int i = 0; i < dataColor.size(); i++) {
                if (dataColor.get(i).getText().equalsIgnoreCase(color)) {
                    return true;
                }
            }
        }
        return false;
    }


    InputFilter inputFilterPer = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            String blockCharacterSet = "~#^|$%'&*!;+()-/, ";
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    InputFilter inputFilterEdt = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            String blockCharacterSet = "~#^|$%'&*!;+()-/., ";
            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_proforma_customer_detail_header:
                if(LL_PROFORMA_CUSTOMER_DETAIL == false){
                    LL_PROFORMA_CUSTOMER_DETAIL = true;
                    LL_PROFORMA_INVOICE_ITMES = false;
                    imgProformaCustomerDetail.setImageDrawable(imgCollapse);
                    imgProformaInvoiceItems.setImageDrawable(imgExpand);
                    llProformaInvoiceItemsContent.setVisibility(View.GONE);
                    llProformaCustomerDetailContent.setVisibility(View.VISIBLE);
                    llOnRoadPrice.setVisibility(View.GONE);
                }else{
                    LL_PROFORMA_CUSTOMER_DETAIL = false;
                    imgProformaCustomerDetail.setImageDrawable(imgExpand);
                    llProformaCustomerDetailContent.setVisibility(View.GONE);
                }
                break;
            case R.id.ll_proforma_invoice_items_header:
                if(LL_PROFORMA_INVOICE_ITMES == false){
                    LL_PROFORMA_INVOICE_ITMES = true;
                    LL_PROFORMA_CUSTOMER_DETAIL = false;
                    imgProformaInvoiceItems.setImageDrawable(imgCollapse);
                    imgProformaCustomerDetail.setImageDrawable(imgExpand);
                    llProformaCustomerDetailContent.setVisibility(View.GONE);
                    llProformaInvoiceItemsContent.setVisibility(View.VISIBLE);
                    llOnRoadPrice.setVisibility(View.VISIBLE);
                }else{
                    LL_PROFORMA_INVOICE_ITMES = false;
                    imgProformaInvoiceItems.setImageDrawable(imgExpand);
                    llProformaInvoiceItemsContent.setVisibility(View.GONE);
                    llOnRoadPrice.setVisibility(View.GONE);
                }
                break;
            case R.id.btn_preview:
                Util.HideKeyboard(ProformaInvoiceActivity.this);
                if(validationDone()){
                    //Preview
                    if(!edtList.get(0).getText().toString().equalsIgnoreCase("")) {
                        if(Integer.parseInt(edtList.get(0).getText().toString())>0){
                            getAllInvoiceData();
                            System.out.println("ProformaJsonRespons:- " + new Gson().toJson(getAllInvoiceData()).toString());
                            sendInvoiceDataToserver(getAllInvoiceData());
                        }else{
                            Util.showToast(ProformaInvoiceActivity.this, "Ex-showroom price should not be 0", Toast.LENGTH_SHORT);
                        }
                    }else {
                        Util.showToast(ProformaInvoiceActivity.this, "Ex-showroom price should not be empty", Toast.LENGTH_SHORT);
                    }
                }else {
                    Util.showToast(ProformaInvoiceActivity.this, "Please enter all customer and vehicle detail", Toast.LENGTH_SHORT);
                }
                break;
            case R.id.edt_model:
                selectCarModel();
                break;
            case R.id.edt_variant:
                selectCarVariant();
                break;
            case R.id.edt_color:
                selectCarColor();
                break;
        }
    }

    private void sendInvoiceDataToserver(final ProformaInvoiceAnswer proformaInvoiceAnswer) {
        relLoading.setVisibility(View.VISIBLE);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getProformaInvoicePdfPreview(proformaInvoiceAnswer, new Callback<ProformaPdfResponse>() {
            @Override
            public void success(ProformaPdfResponse proformaPdfResponse, Response response) {
                relLoading.setVisibility(View.GONE);
                if(proformaPdfResponse.getResult()!= null && !proformaPdfResponse.getResult().equalsIgnoreCase("")) {
                    pref.setProformaPdfLink(proformaPdfResponse.getResult() + "");
                    Intent intent = new Intent(ProformaInvoiceActivity.this, ProformaPdfActivity.class);
                    intent.putExtra("from_logs", false);
                    intent.putExtra("customer_name", edtName.getText().toString());
                    intent.putExtra("phone", phone);
                    startActivity(intent);
                }else {
                    Util.showToast(ProformaInvoiceActivity.this, proformaPdfResponse.getMessage(), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                relLoading.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        return;
    }

}
