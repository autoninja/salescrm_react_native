package com.salescrm.telephony.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bharath on 31/10/16.
 */

public class FormSubmissionInputDataServer implements Serializable {

    //{Add activity input data
    private String lead_last_updated;
    private String lead_id;
    //Add activity input data}


    private String action_id;
    private String scheduled_activity_id;

    private String category_instance_id;

    private String activity_id;


    private List<Form_response> form_response;

    private LocationData location_data;

    public String getAction_id() {
        return action_id;
    }

    public void setAction_id(String action_id) {
        this.action_id = action_id;
    }

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }

    public String getScheduled_activity_id() {
        return scheduled_activity_id;
    }

    public void setScheduled_activity_id(String scheduled_activity_id) {
        this.scheduled_activity_id = scheduled_activity_id;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public List<Form_response> getForm_response() {
        return form_response;
    }

    public void setForm_response(List<Form_response> form_response) {
        this.form_response = form_response;
    }

    public String getCategory_instance_id() {
        return category_instance_id;
    }

    public void setCategory_instance_id(String category_instance_id) {
        this.category_instance_id = category_instance_id;
    }
    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    public LocationData getLocation_data() {
        return location_data;
    }

    public void setLocation_data(LocationData location_data) {
        this.location_data = location_data;
    }

    /* @Override
                public String toString() {
                    return "ClassPojo [action_id = " + action_id + ", lead_last_updated = " + lead_last_updated + ", scheduled_activity_id = " + scheduled_activity_id + ", lead_id = " + lead_id + ", form_response = " + form_response + "]";
                }*/
    public class Form_response implements Serializable
    {
        private String name;

        private Value value;

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public Value getValue ()
        {
            return value;
        }

        public void setValue (Value value)
        {
            this.value = value;
        }

       @Override
        public String toString()
        {
            return "{\\\"name\\\" : \\\""+name+"\\\", \\\"value\\\" : \\\""+value+"\\\"}";
        }


        public class Value
        {
            private String fAnsId;

            private String displayText;

            private String answerValue;

            public String getFAnsId ()
            {
                return fAnsId;
            }

            public void setFAnsId (String fAnsId)
            {
                this.fAnsId = fAnsId;
            }

            public String getDisplayText ()
            {
                return displayText;
            }

            public void setDisplayText (String displayText)
            {
                this.displayText = displayText;
            }

            public String getAnswerValue ()
            {
                return answerValue;
            }

            public void setAnswerValue (String answerValue)
            {
                this.answerValue = answerValue;
            }

            @Override
            public String toString()
            {
                return "{\\\\\\\"fAnsId\\\\\\\" : \\\\\\\""+fAnsId+"\\\\\\\", \\\\\\\"displayText\\\\\\\" : \\\\\\\""+displayText+"\\\\\\\", \\\\\\\"answerValue\\\\\\\" : \\\\\\\""+answerValue+"\\\\\\\"}";
            }

        }
    }

    public class LocationData implements Serializable{
        String locality;
        String latitude;
        String longitude;

        public String getLocality() {
            return locality;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }
    }

}