import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image} from 'react-native';


export default class VoidView extends React.Component {

	constructor(props)
		{
		    super(props);

		    this.state = {
		    	isLoading: false,
		    	isFetching:false,
		    	activeRadio:''
			}
		}
		

        generateView(innerObjectsValues){
            //console.log("innerObjectRadio questionChildren VoidView", JSON.stringify(innerObjectsValues))
            for(var j=0; j<innerObjectsValues.questionChildren.length; j++){
                
                    //console.log("Void for loop", innerObjectsValues.questionChildren[j].values.length, i)
                return innerObjectsValues.questionChildren[j].values.map(item =>{
                       return this.props.callMainGenerateViews(item, 
                            item.formInputType)
                    })
                
            }
        }

        render () {
			//console.log("innerObjects", "reached")
            //console.log("innerObjects", this.props.innerObjects.fQId)
            //console.error("innerObjectRadio")
            //console.log("innerObjectRadio", JSON.stringify(this.props.innerObjects))
            return (
                <View style= {{flex : 1}}>
					{this.generateView(this.props.radioObject)}
                </View>
	    	)
	  }
}