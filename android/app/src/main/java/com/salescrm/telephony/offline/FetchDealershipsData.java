package com.salescrm.telephony.offline;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.DealershipsDB;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.DealershipsResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 23/9/17.
 * Call this to get the user data
 */
public class FetchDealershipsData implements Callback<DealershipsResponse> {
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;

    public FetchDealershipsData(OfflineSupportListener listener, Context context) {
        this.listener =listener;
        this.context = context;
      //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }

    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getDealerships(this);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.DEALERSHIPS);

        }
    }


    @Override
    public void success(final DealershipsResponse data, Response response) {
        Util.updateHeaders(response.getHeaders());

        if (!data.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + data.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.DEALERSHIPS);

        } else {
            if (data.getResult() != null) {
                System.out.println("Success:1" + data.getMessage());
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        insertData(realm,data);
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        listener.onDealerShipFetched(data);
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.DEALERSHIPS);
                    }
                });


            } else {
                // relLoader.setVisibility(View.GONE);
                System.out.println("Success:2" + data.getMessage());
                listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.DEALERSHIPS);
                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }

    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.DEALERSHIPS);

    }

    private void insertData(Realm realm, DealershipsResponse dealershipsResponse) {

        realm.where(DealershipsDB.class).findAll().deleteAllFromRealm();
        for(int i=0;i<dealershipsResponse.getResult().size();i++) {
            DealershipsResponse.Result userInfo = dealershipsResponse.getResult().get(i);
            DealershipsDB dealershipsDB = new DealershipsDB();
            dealershipsDB.setDealer_id(userInfo.getDealer().getId());
            dealershipsDB.setDealer_name(userInfo.getDealer().getName());
            dealershipsDB.setDealer_db_name(userInfo.getDealer().getDb_name());

            dealershipsDB.setUser_id(userInfo.getUsers().getId());
            dealershipsDB.setUser_name(userInfo.getUsers().getName());
            dealershipsDB.setUser_dp_url(userInfo.getUsers().getImage_url());
            realm.copyToRealmOrUpdate(dealershipsDB);
        }
    }

}
