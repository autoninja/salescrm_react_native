import React, { Component } from 'react';
import { ScrollView, Text, TouchableOpacity, Image, View, ActivityIndicator } from 'react-native';
import styles from './settings.styles';
import Toast from 'react-native-easy-toast';
import Icon from 'react-native-ionicons';

class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            settingsList : [
                {
                    id : 1,
                    name : 'SMS / Email',
                    redirectTo : 'CommunicationSettings'
                }
            ],
            isLoading : false,
            error : false,
            errMsg : ''
        };
    }

    render()
    {
        const { settingsList, isLoading, error } = this.state;
        if(error) {
            return (<Toast
                style={{backgroundColor:'red'}}
                position='top'
                fadeInDuration={750}
                fadeOutDuration={750}
                opacity={0.8}
                textStyle={{color:'white'}}
            />)
        }

        if(isLoading) {
            return (
                <ActivityIndicator
                    animating={isLoading}
                    style={{flex: 1,   alignSelf:'center',}}
                    color="#fff"
                    size="large"
                />
            );
        }

        return (
            <View>
                <ScrollView style={styles.mainContainer}>
                    {
                        settingsList.map((item, index) => {
                            return (
                                <TouchableOpacity
                                    activeOpacity={1}
                                    key={index}
                                    onPress={() => {this.props.navigation.navigate(item.redirectTo)}}
                                    style={{
                                        marginTop: 5,
                                        marginBottom: 5
                                    }}
                                >
                                    <View key={item.name}
                                          style={styles.touchableContainer}
                                    >
                                        <View style={{
                                            ...styles.listItemColumn,
                                            flex: 0.1,
                                            borderBottomLeftRadius: 5,
                                            borderTopLeftRadius: 5,
                                        }}>
                                            <Icon name="ios-mail-open"  color={'#2e5e86'}/>
                                        </View>
                                        <View style={{
                                            ...styles.listItemColumn,
                                            paddingTop: 20,
                                            flex: 0.8,
                                        }}>
                                            <Text style={styles.listItemText}>{item.name.toUpperCase()}</Text>
                                        </View>
                                        <View style={{
                                            ...styles.listItemColumn,
                                            flex: 0.1,
                                            borderBottomRightRadius: 5,
                                            borderTopRightRadius: 5,
                                        }}>
                                            <Icon name="ios-arrow-forward" color={'#2e5e86'}/>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            );
                        })
                    }
                </ScrollView>
            </View>
        )

    }
}

export default Settings;
