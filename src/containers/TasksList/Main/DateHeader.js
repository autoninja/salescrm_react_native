import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Moment from 'moment';
import DateUtils from '../../../utils/DateUtils'
export default class DateHeader extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let date = this.props.date;
        let dateVal = date;
        let bgColor = "transparent";
        let todaysDate = new Date();
        todaysDate.setHours('0');
        todaysDate.setMinutes('0');
        todaysDate.setSeconds('0');
        todaysDate.setMilliseconds('0');
        if (DateUtils.isSameDate(date, todaysDate)) {
            dateVal = "(Today)";
        }
        else if (this.props.tabIndex != 2 && DateUtils.isBefore(date, todaysDate)) {
            dateVal = "Pending (" + Math.abs(DateUtils.diff(date, todaysDate)) + " days)";
            bgColor = "#D0021B";
        }
        else if (DateUtils.diff(date, todaysDate) == 1) {
            dateVal = Moment(date).format('ddd DD/MM') + " (Tomorrow)";

        }
        else {
            dateVal = Moment(date).format('ddd DD/MM');
        }
        return (
            <View style={{
                marginTop: 8,
                marginBottom: 8,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: bgColor
            }}>
                <Text style={{
                    color: "#fff",
                    fontSize: 15,
                    textAlignVertical: 'center',
                    paddingTop: 4,
                    paddingBottom: 4
                }}>{dateVal}</Text>
            </View>
        )
    }
}