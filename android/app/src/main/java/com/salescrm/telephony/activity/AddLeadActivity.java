package com.salescrm.telephony.activity;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.offline.C360ActivityOffline;
import com.salescrm.telephony.adapter.AddLeadPagerAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.offline.AutoFetchFormData;
import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.db.ActivitiesDB;
import com.salescrm.telephony.db.AllCarsDB;
import com.salescrm.telephony.db.AllMobileNumbersSearch;
import com.salescrm.telephony.db.BrandsDB;
import com.salescrm.telephony.db.BuyerTypeDB;
import com.salescrm.telephony.db.EnqSourceDB;
import com.salescrm.telephony.db.LocationsDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;
import com.salescrm.telephony.interfaces.AddLeadActivityToFragmentListener;
import com.salescrm.telephony.interfaces.AddLeadCommunication;
import com.salescrm.telephony.interfaces.AutoFetchFormListener;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.model.AddLeadActivityInputData;
import com.salescrm.telephony.model.AddLeadCarInputData;
import com.salescrm.telephony.model.AddLeadCustomerInputData;
import com.salescrm.telephony.model.AddLeadDSEInputData;
import com.salescrm.telephony.model.AddLeadInputPrimaryElements;
import com.salescrm.telephony.model.AddLeadLeadInputData;
import com.salescrm.telephony.model.CreateLeadInputData;
import com.salescrm.telephony.model.CreateLeadInputDataServer;
import com.salescrm.telephony.model.ViewPagerSwiped;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AddLeadElementsResponse;
import com.salescrm.telephony.response.CreateLeadResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.services.SyncDataService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.CustomViewPager;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bharath on 30/5/16.
 */
public class AddLeadActivity extends AppCompatActivity implements AddLeadCommunication, Callback<AddLeadElementsResponse>, FetchC360OnCreateLeadListener, AutoFetchFormListener {
    public CustomViewPager viewPager;
    public int mLastPage = 0;
    Button btNextForm;
    AddLeadPagerAdapter pagerAdapter;
    ProgressBar progressBar;
    FrameLayout frameProgressLead;
    TextView tvAddLeadAction;
    View progressOvals[];
    View leadProgressOval0, leadProgressOval1, leadProgressOval2, leadProgressOval3, leadProgressOval4;
    String addLeadActions[];
    private Preferences pref = null;
    private String TAG = "AddLeadActivity";
    private Realm realm;
    private RelativeLayout relLoader;

    public static AddLeadCustomerInputData customerInputData = null;
    public static AddLeadLeadInputData leadInputData = null;
    public static AddLeadCarInputData carInputData = null;
    public static AddLeadDSEInputData dseInputData = null;
    public static AddLeadActivityInputData activityInputData = null;

    private boolean movedFlag = true;
    private String enqSourceId, primaryCarId, locationId;
    private ProgressDialog progressDialog;
    private static CoordinatorLayout view;

    private AddLeadActivityToFragmentListener listenerToFragment;
    private String generatedLeadId;
    private boolean fetchActionPlanFirst = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lead);
        pref = Preferences.getInstance();
        pref.load(getApplicationContext());
        realm = Realm.getDefaultInstance();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Create lead !!!.. Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);

        view = (CoordinatorLayout) findViewById(R.id.coordinator_layout);

        viewPager = (CustomViewPager) findViewById(R.id.add_lead_view_pager);
        frameProgressLead = (FrameLayout) findViewById(R.id.frame_progress_bar_lead);
        btNextForm = (Button) findViewById(R.id.bt_add_lead_action);
        tvAddLeadAction = (TextView) findViewById(R.id.tv_add_lead_action);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar_add_lead);
        leadProgressOval0 = findViewById(R.id.lead_progress_oval_0);
        leadProgressOval1 = findViewById(R.id.lead_progress_oval_1);
        leadProgressOval2 = findViewById(R.id.lead_progress_oval_2);
        leadProgressOval3 = findViewById(R.id.lead_progress_oval_3);
        leadProgressOval4 = findViewById(R.id.lead_progress_oval_4);
        relLoader = (RelativeLayout) findViewById(R.id.rel_add_lead_loader);
        progressOvals = new View[]{leadProgressOval0, leadProgressOval1, leadProgressOval2, leadProgressOval3, leadProgressOval4};
        addLeadActions = new String[]{"Customer details", "Lead details", "Car details", "Assign lead to SC", "Select activity"};


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                init();
            }
        });

        btNextForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("lastPage:" + mLastPage);
                System.out.println("currentPage:" + (viewPager.getCurrentItem() + 1));
                System.out.println("EnqSourceId:" + enqSourceId);
                System.out.println("primaryCarId:" + primaryCarId);
                System.out.println("locationId:" + locationId);
                //   listenerToFragment.onViewPagerMoved(new ViewPagerSwiped(mLastPage, viewPager.getCurrentItem() + 1, enqSourceId, primaryCarId, locationId));
                SalesCRMApplication.getBus().post(new ViewPagerSwiped(mLastPage, viewPager.getCurrentItem() + 1, enqSourceId, primaryCarId, locationId));
            }
        });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                updateProgress(position);
                mLastPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void init() {
        checkLeadElements();
        updateProgress(0);
        ViewTreeObserver viewTreeObserver = leadProgressOval0.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < 16) {
                    leadProgressOval0.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    leadProgressOval0.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                }
                for (int i = 0; i < 5; i++) {
                    progressOvals[i].setX(leadProgressOval0.getX() + progressOvals[i].getWidth() / 2 + progressBar.getWidth() / 4 * i);
                }
            }
        });


    }

    private void checkLeadElements() {
        if (realm.where(BuyerTypeDB.class).findAll().isEmpty() ||
                realm.where(AllCarsDB.class).findAll().isEmpty() ||
                realm.where(LocationsDB.class).findAll().isEmpty() ||
                realm.where(EnqSourceDB.class).findAll().isEmpty() ||
                realm.where(ActivitiesDB.class).findAll().isEmpty()) {

            //CallApi
            if (new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
                fetchLeadElements();
            } else {
                relLoader.setVisibility(View.GONE);
                setPagerAdapter();
                Toast.makeText(getApplicationContext(), "No Internet connection", Toast.LENGTH_LONG).show();
            }


        } else {
            setPagerAdapter();
            relLoader.setVisibility(View.GONE);
        }
    }

    public void updateProgress(final int position) {

        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(progressBar, "progress", progressBar.getProgress(), position * 25);
        progressAnimator.setDuration(300);
        progressAnimator.setInterpolator(new DecelerateInterpolator());
        progressAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (mLastPage > position) {
                    tvAddLeadAction.setText(addLeadActions[position]);
                    for (int i = 0; i < 5; i++) {
                        if (i <= position) {
                            setBackground(progressOvals[i], ContextCompat.getDrawable(getApplicationContext(), R.drawable.progress_oval_lead_active));

                        } else {
                            setBackground(progressOvals[i], ContextCompat.getDrawable(getApplicationContext(), R.drawable.progress_oval_lead_inactive));
                        }
                    }
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (mLastPage <= position) {
                    tvAddLeadAction.setText(addLeadActions[position]);
                    for (int i = 0; i < 5; i++) {
                        if (i <= position) {
                            setBackground(progressOvals[i], ContextCompat.getDrawable(getApplicationContext(), R.drawable.progress_oval_lead_active));

                        } else {
                            setBackground(progressOvals[i], ContextCompat.getDrawable(getApplicationContext(), R.drawable.progress_oval_lead_inactive));
                        }
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


        progressAnimator.start();

    }

    @Override
    protected void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);
    }

    public void finishActivity(View view) {
        onBackPressed();
    }

    public void setBackground(View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }

    }

    @Override
    public void onBackPressed() {
        if (mLastPage > 0) {
            viewPager.setCurrentItem(mLastPage - 1, true);
            System.out.println(mLastPage);
        } else {
            super.onBackPressed();
        }

    }

    public void fetchLeadElements() {
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).FetchLeadElements("", this);

    }

    @Override
    public void success(final AddLeadElementsResponse addLeadElementsResponse, Response response) {
        List<Header> headerList = response.getHeaders();
        for (Header header : headerList) {
            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                ApiUtil.UpdateAccessToken(header.getValue());
            }
        }

        if(addLeadElementsResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
            ApiUtil.InvalidUserLogout(AddLeadActivity.this,0);
        }

        if (!addLeadElementsResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + addLeadElementsResponse.getMessage());
            //showAlert(validateOtpResponse.getMessage());
        } else {
            if (addLeadElementsResponse.getResult() != null) {
                System.out.println("Success:1" + addLeadElementsResponse.getMessage());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm1) {
                        insertData(realm1, addLeadElementsResponse.getResult().getBuyerType());
                        insertData(realm1, addLeadElementsResponse.getResult().getAllCars());
                        insertData(realm1, addLeadElementsResponse.getResult().getBrands());
                        insertData(realm1, addLeadElementsResponse.getResult().getLocations());
                       // insertData(realm1, addLeadElementsResponse.getResult().getEnqSource());
                        insertData(realm1, addLeadElementsResponse.getResult().getActivities());
                        setPagerAdapter();
                    }
                });

                relLoader.setVisibility(View.GONE);
            } else {
                // relLoader.setVisibility(View.GONE);
                System.out.println("Success:2" + addLeadElementsResponse.getMessage());
                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }

    public static void showAlert(String alert) {
        Snackbar snackbar = Snackbar.make(view, alert, Snackbar.LENGTH_LONG)
                .setAction("Close", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    private void insertData(Realm realm, List t) {
        System.out.println("Insert Data Called");
        BuyerTypeDB buyerTypeDBCreate;
        AllCarsDB allCarsDB;
        BrandsDB brands;
        LocationsDB locations;
        EnqSourceDB enqSourceDB;
        ActivitiesDB activities;

        for (int i = 0; i < t.size(); i++) {
            if (t.get(i) instanceof AddLeadElementsResponse.Result.BuyerType) {
                buyerTypeDBCreate = new BuyerTypeDB();
                buyerTypeDBCreate.setType(((AddLeadElementsResponse.Result.BuyerType) t.get(i)).getType());
                buyerTypeDBCreate.setId(Util.getInt(((AddLeadElementsResponse.Result.BuyerType) t.get(i)).getId()));
                System.out.println("Buyer Type:" + ((AddLeadElementsResponse.Result.BuyerType) t.get(i)).getType());
                realm.copyToRealmOrUpdate(buyerTypeDBCreate);

            }
            if (t.get(i) instanceof AddLeadElementsResponse.Result.AllCars) {
                allCarsDB = new AllCarsDB();
                allCarsDB.setId(Util.getInt(((AddLeadElementsResponse.Result.AllCars) t.get(i)).getId()));
                allCarsDB.setName(((AddLeadElementsResponse.Result.AllCars) t.get(i)).getName());
                allCarsDB.setCategory(((AddLeadElementsResponse.Result.AllCars) t.get(i)).getCategory());
                realm.copyToRealmOrUpdate(allCarsDB);


            }
            if (t.get(i) instanceof AddLeadElementsResponse.Result.Brands) {
                brands = new BrandsDB();
                brands.setId(Util.getInt(((AddLeadElementsResponse.Result.Brands) t.get(i)).getId()));
                brands.setName(((AddLeadElementsResponse.Result.Brands) t.get(i)).getName());
                brands.setCompany(((AddLeadElementsResponse.Result.Brands) t.get(i)).getCompany());
                realm.copyToRealmOrUpdate(brands);
            }
            if (t.get(i) instanceof AddLeadElementsResponse.Result.Locations) {
                locations = new LocationsDB();
                locations.setId(Util.getInt(((AddLeadElementsResponse.Result.Locations) t.get(i)).getId()));
                locations.setName(((AddLeadElementsResponse.Result.Locations) t.get(i)).getName());
                locations.setCode(((AddLeadElementsResponse.Result.Locations) t.get(i)).getCode());
                realm.copyToRealmOrUpdate(locations);
            }

            if (t.get(i) instanceof AddLeadElementsResponse.Result.Activities) {
                activities = new ActivitiesDB();
                activities.setId(Util.getInt(((AddLeadElementsResponse.Result.Activities) t.get(i)).getId()));
                activities.setCategory(Util.getInt(((AddLeadElementsResponse.Result.Activities) t.get(i)).getCategory()));
                activities.setId(Util.getInt(((AddLeadElementsResponse.Result.Activities) t.get(i)).getId()));
                activities.setName(((AddLeadElementsResponse.Result.Activities) t.get(i)).getName());
                realm.copyToRealmOrUpdate(activities);
            }
        }
        System.out.println(TAG + "Data:AfterUpdating Db Realm:: Buyer_Size:" + realm.where(BuyerTypeDB.class).findAll().size() + " AllCars_size:" + realm.where(AllCarsDB.class).findAll().size()
                + " A_size:" + realm.where(ActivitiesDB.class).findAll().size());

    }

    @Override
    public void failure(RetrofitError error) {
        error.printStackTrace();
        System.out.println(TAG + "Error:");
        Toast.makeText(getApplicationContext(),
                "Server not responding, please try again after some times", Toast.LENGTH_SHORT).show();
    }


    private void setPagerAdapter() {
        pagerAdapter = new AddLeadPagerAdapter(getSupportFragmentManager(), 5, getApplicationContext());
        viewPager.setPagingEnabled(false);
        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(pagerAdapter);
    }

    private void moveViewPagerTo(int position) {
        if (mLastPage < 5) {
            viewPager.setCurrentItem(position);
        }

    }

    @Override
    public void onFragmentCreated(AddLeadActivityToFragmentListener listenerToFragment) {
        System.out.println("Fragment created::::");
        this.listenerToFragment = listenerToFragment;
    }

    @Override
    public void onCustomerDetailsEdited(AddLeadCustomerInputData addLeadCustomerInputData, int position) {
        movedFlag = true;
        customerInputData = addLeadCustomerInputData;
        if (mLastPage < 5) {
            viewPager.setCurrentItem(mLastPage + 1);
        }
    }

    @Override
    public void onLeadDetailsEdited(AddLeadLeadInputData addLeadLeadInputData, int pos) {
        leadInputData = addLeadLeadInputData;
        this.enqSourceId = leadInputData.getEnqSourceId();
        SalesCRMApplication.getBus().post(new AddLeadInputPrimaryElements(enqSourceId, primaryCarId, locationId, carInputData));
        if (mLastPage < 5) {
            viewPager.setCurrentItem(mLastPage + 1);
        }
    }

    @Override
    public void onCarDetailsEdited(AddLeadCarInputData addLeadCarInputData, int pos) {

        carInputData = addLeadCarInputData;
        this.primaryCarId = carInputData.getNewCarInputData().size() == 0 ? "" : carInputData.getNewCarInputData().get(0).getModel_id();
        SalesCRMApplication.getBus().post(new AddLeadInputPrimaryElements(enqSourceId, primaryCarId, locationId, carInputData));
        if (mLastPage < 5) {
            viewPager.setCurrentItem(mLastPage + 1);
        }
    }

    @Override
    public void onAssignLeadDseEdited(AddLeadDSEInputData addLeadDSEInputData, int pos) {
        dseInputData = addLeadDSEInputData;
        this.locationId = dseInputData.getDseBranchId();
        SalesCRMApplication.getBus().post(new AddLeadInputPrimaryElements(enqSourceId, primaryCarId, locationId, carInputData));
        if (mLastPage < 5) {
            viewPager.setCurrentItem(mLastPage + 1);
        }
    }

    @Override
    public void onSelectActivityEdited(AddLeadActivityInputData addLeadActivityInputData, AddLeadCarInputData carInputData, int pos) {
        activityInputData = addLeadActivityInputData;
        System.out.println("Create Lead Called");
        createLead();
    }

    private void createLead() {
        if (!new ConnectionDetectorService(this).isConnectingToInternet()) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    CreateLeadInputDataDB data = realm.createOrUpdateObjectFromJson(CreateLeadInputDataDB.class, new Gson().toJson(getCreateLeadInputData()));
                    SalesCRMRealmTable table = new SalesCRMRealmTable();
                    table.setScheduledActivityId(data.getScheduledActivityId());
                    table.setActivityScheduleDate(data.getDataCreatedDate());
                    table.setCreatedOffline(true);
                    table.setDone(true);
                    table.setFirstName(data.getLead_data().getCustomer_details().getFirst_name());
                    table.setLastName(data.getLead_data().getCustomer_details().getLast_name());

                    //telephony
                    AllMobileNumbersSearch allMobileNumbersSearch ;
                    System.out.println("AddLead Mobile Size- "+data.getLead_data().getMobile_numbers().size());
                    if (data.getLead_data().getMobile_numbers().size() > 0) {
                        table.setCustomerNumber(data.getLead_data().getMobile_numbers().get(0).getNumber());

                        allMobileNumbersSearch = new AllMobileNumbersSearch();
                        allMobileNumbersSearch.setLeadId(0);
                        System.out.println("AddLead Mobile - "+data.getLead_data().getMobile_numbers().get(0).getNumber());
                        allMobileNumbersSearch.setName(data.getLead_data().getCustomer_details().getFirst_name());
                        allMobileNumbersSearch.setMobileNumber(data.getLead_data().getMobile_numbers().get(0).getNumber());
                        allMobileNumbersSearch.setDeleteStatus(0);
                        realm.copyToRealmOrUpdate(allMobileNumbersSearch);
                    }

                    if (data.getLead_data().getEmails().size() > 0) {
                        table.setCustomerEmail(data.getLead_data().getEmails().get(0).getAddress());
                    }
                    table.setCreateLeadInputDataDB(data);
                    realm.insertOrUpdate(table);





                    Intent intent = new Intent(AddLeadActivity.this, C360ActivityOffline.class);
                    System.out.println("Scheduled:id:" + table.getScheduledActivityId());
                    intent.putExtra("scheduled_activity_id", table.getScheduledActivityId());
                    startActivity(intent);
                }
            });
            System.out.println("Create Lead realm size:" + realm.where(CreateLeadInputDataDB.class).findAll().size());
            Toast.makeText(getApplicationContext(), "Lead has been created -Sync Pending", Toast.LENGTH_LONG).show();
            AddLeadActivity.this.finish();
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(new Intent(this, SyncDataService.class));
            }
            else {
                startService(new Intent(this, SyncDataService.class));
            }
            return;
        }
        realm.beginTransaction();
        CreateLeadInputDataDB data = realm.createOrUpdateObjectFromJson(CreateLeadInputDataDB.class, new Gson().toJson(getCreateLeadInputData()));
        data.setIs_synced(true);
        realm.commitTransaction();
        progressDialog.show();
        //getCreateLeadInputData();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).CreateLead(getCreateLeadInputDataServer(), new Callback<CreateLeadResponse>() {
            @Override
            public void success(CreateLeadResponse o, Response response) {
                progressDialog.dismiss();
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }
                if(o.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(AddLeadActivity.this,0);
                }

                if (!o.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    System.out.println("Success:0" + o.getMessage());
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
                    //showAlert(validateOtpResponse.getMessage());
                } else {
                    if (o.getResult() != null) {
                        System.out.println("Success:1" + o.getMessage());
                        System.out.println("Lead Id:" + o.getResult());
                        Toast.makeText(getApplicationContext(), "Lead has been created", Toast.LENGTH_LONG).show();
                        // AddLeadActivity.this.finish();
                        fetchC360(o.getResult(), getCreateLeadInputDataServer());
                        if(o.getScheduled_activity_id() != null) {
                            createAllotDseTask(o.getResult(),o.getScheduled_activity_id());
                        }
                    } else {
                        // relLoader.setVisibility(View.GONE);
                        System.out.println("Success:2" + o.getMessage());
                        Toast.makeText(getApplicationContext(), ""+o.getMessage(), Toast.LENGTH_LONG).show();
                        //showAlert(validateOtpResponse.getMessage());
                    }
                }

            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                System.out.println("CreateLead:Error");
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
//                System.out.println(error.getResponse().toString());
//                System.out.println(error.getBody());

                System.out.println(error.toString());


            }

        });
    }

    private void fetchC360(String leadId, CreateLeadInputDataServer createLeadInputData) {
        progressDialog.setMessage("Redirecting to C360 !!!.. Please wait");
        progressDialog.show();
        this.generatedLeadId = leadId;
        fetchActionPlanFirst = false;
        if (createLeadInputData.getActivity_data() == null
                || createLeadInputData.getActivity_data().getActivity() == null
                || createLeadInputData.getActivity_data().getActivity().equalsIgnoreCase("")) {
            fetchActionPlanFirst = true;
        }
        System.out.println("Fetch First" + (fetchActionPlanFirst));
        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(leadId);
        new FetchC360InfoOnCreateLead(this, getApplicationContext(), leadData, pref.getAppUserId()).call(fetchActionPlanFirst);

    }

    @Override
    public void onFetchC360OnCreateLeadSuccess() {
        new AutoFetchFormData(this, getApplicationContext())
                .call(realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", Util.getInt(generatedLeadId)).findAll(), !fetchActionPlanFirst);
    }

    @Override
    public void onFetchC360OnCreateLeadError() {
        System.out.println(TAG + "Failed");
        Toast.makeText(getApplicationContext(), "Failed fetching C360 data", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAutoFormsDataFetched(boolean b, RealmResults<SalesCRMRealmTable> data) {
        progressDialog.dismiss();
        pref.setLeadID("" + generatedLeadId);
        SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Util.getInt(generatedLeadId)).findFirst();
        if (salesCRMRealmTable != null) {
            pref.setScheduledActivityId(salesCRMRealmTable.getScheduledActivityId());

        }
        startActivity(new Intent(AddLeadActivity.this, C360Activity.class));
        AddLeadActivity.this.finish();
    }

    private void createAllotDseTask(String leadId, Integer scheduled_activity_id){
            realm.beginTransaction();
            SalesCRMRealmTable salesCRMRealmTable = new SalesCRMRealmTable();
            salesCRMRealmTable.setScheduledActivityId(scheduled_activity_id);
            salesCRMRealmTable.setLeadId(Long.parseLong(leadId));
            salesCRMRealmTable.setFirstName(getCreateLeadInputData().getLead_data().getCustomer_details().getFirst_name());
            salesCRMRealmTable.setFirstName(getCreateLeadInputData().getLead_data().getCustomer_details().getLast_name());
            salesCRMRealmTable.setGender(getCreateLeadInputData().getLead_data().getCustomer_details().getGender());
            salesCRMRealmTable.setTitle(getCreateLeadInputData().getLead_data().getCustomer_details().getTitle());
            salesCRMRealmTable.setActivityId(WSConstants.TaskActivityName.ALLOT_DSE_ID);
            salesCRMRealmTable.setActivityScheduleDate(Util.getDateTime(0));
            salesCRMRealmTable.setActivityName(WSConstants.TaskActivityName.ALLOT_DSE);
            salesCRMRealmTable.setIsLeadActive(1);
            String leadTagId = getCreateLeadInputData().getLead_data().getLead_tag_id();
            String leadTagName = "";
            if(leadTagId != null){
                if(leadTagId.equalsIgnoreCase(WSConstants.LEAD_TAG_HOT_ID)){
                    leadTagName = WSConstants.LEAD_TAG_HOT;
                }
                else if(leadTagId.equalsIgnoreCase(WSConstants.LEAD_TAG_WARM_ID)){
                    leadTagName = WSConstants.LEAD_TAG_WARM;
                }
                else if(leadTagId.equalsIgnoreCase(WSConstants.LEAD_TAG_COLD_ID)){
                    leadTagName = WSConstants.LEAD_TAG_COLD;
                }
            }

            salesCRMRealmTable.setLeadTagsName(getCreateLeadInputData().getLead_data().getLead_tag_id());
            salesCRMRealmTable.setDone(true);
            salesCRMRealmTable.setSync(true);
            realm.copyToRealmOrUpdate(salesCRMRealmTable);
            realm.commitTransaction();

    }

    @Override
    public void onAutoFormsDataError(RetrofitError error, int from) {
        Toast.makeText(getApplicationContext(), "Failed fetching form data", Toast.LENGTH_SHORT).show();
    }

    private CreateLeadInputData getCreateLeadInputData() {

        CreateLeadInputData createLeadInputData = new CreateLeadInputData();
        createLeadInputData.setScheduledActivityId((int) System.currentTimeMillis());
        CreateLeadInputData.Lead_data lead_data = createLeadInputData.new Lead_data();


        //Customer Details
        CreateLeadInputData.Lead_data.Customer_details customer_details = lead_data.new Customer_details();
        customer_details.setGender(customerInputData.getGender());
        customer_details.setTitle(customerInputData.getTitle());
        customer_details.setFirst_name(customerInputData.getFirstName());
        customer_details.setLast_name(customerInputData.getLastName());
        customer_details.setAge(String.valueOf(customerInputData.getAge()));
        if (customerInputData.getAddressType() == WSConstants.ADDRESS_TYPE_OFFICE) {
            customer_details.setOffice_address(customerInputData.getAddress());
            customer_details.setOffice_city(customerInputData.getCity());
            customer_details.setOffice_pin_code(customerInputData.getPinCode());
        } else if (customerInputData.getAddressType() == WSConstants.ADDRESS_TYPE_HOME) {
            customer_details.setResidence_address(customerInputData.getAddress());
            customer_details.setCity(customerInputData.getCity());
            customer_details.setPin_code(customerInputData.getPinCode());
        }
        customer_details.setCompany_name(customerInputData.getCompany_name());
        customer_details.setOccupation(customerInputData.getOccupation());

        //Set Customer details
        lead_data.setCustomer_details(customer_details);

        //Mobile Numbers
        lead_data.setMobile_numbers(customerInputData.getMobileNumberses());

        System.out.println("Customer Mobile Number");
        //Email Ids
        lead_data.setEmails(customerInputData.getEmails());
        //Customer Details --------------------------------------------------


        //Lead Details Screen
        lead_data.setLead_enq_date(leadInputData.getLeadDate());
        lead_data.setLead_source_id(leadInputData.getEnqSourceId());
        lead_data.setRef_vin_reg_no(leadInputData.getVinReg());
        lead_data.setLead_source_category_id(leadInputData.getSourceCategory());
        lead_data.setExp_closing_date(leadInputData.getExpectedPurDate());
        lead_data.setMode_of_pay(leadInputData.getModeOfPayment());
        //Lead Details Screen------------------------------------------------

        //Car Details
        lead_data.setCar_details(carInputData.getNewCarInputData());
        lead_data.setEx_car_details(carInputData.getExCarInputData());
        lead_data.setAd_car_details(carInputData.getAddCarInputData());
        //Car Details--------------------------------------------------------

        //Assign Lead to Dse

        if (Util.getInt(dseInputData.getDseBranchId()) > 0) {
            lead_data.setLocation_id(Util.getInt(dseInputData.getDseBranchId()));
        }
        System.out.println(lead_data.getLocation_id());
        lead_data.setUser_details(dseInputData.getUser_detailsList());
        lead_data.setLead_tag_id(dseInputData.getTagId());
        //Assign Lead to Dse-------------------------------------------------


        //Activity
        createLeadInputData.setActivity_data(activityInputData.getActivity_data());

        //Activity-----------------------------------------------------------

        //remarks
        createLeadInputData.setRemarks(activityInputData.getRemarks().toUpperCase());
        //
        //Final Lead_Data
        createLeadInputData.setLead_data(lead_data);
        return createLeadInputData;
    }

    private CreateLeadInputDataServer getCreateLeadInputDataServer() {

        CreateLeadInputDataServer createLeadInputData = new CreateLeadInputDataServer();
        CreateLeadInputDataServer.Lead_data lead_data = createLeadInputData.new Lead_data();


        //Customer Details
        CreateLeadInputDataServer.Lead_data.Customer_details customer_details = lead_data.new Customer_details();
        customer_details.setGender(customerInputData.getGender());
        customer_details.setTitle(customerInputData.getTitle());
        customer_details.setType_of_cust_id(customerInputData.getType_of_cust_id());
        customer_details.setFirst_name(customerInputData.getFirstName().toUpperCase());
        customer_details.setLast_name(customerInputData.getLastName().toUpperCase());
        customer_details.setAge(String.valueOf(customerInputData.getAge()));
        if (customerInputData.getAddressType() == WSConstants.ADDRESS_TYPE_OFFICE) {
            customer_details.setOffice_address(customerInputData.getAddress().toUpperCase());
            customer_details.setOffice_city(customerInputData.getCity().toUpperCase());
            customer_details.setOffice_pin_code(customerInputData.getPinCode());
        } else if (customerInputData.getAddressType() == WSConstants.ADDRESS_TYPE_HOME) {
            customer_details.setResidence_address(customerInputData.getAddress().toUpperCase());
            customer_details.setCity(customerInputData.getCity().toUpperCase());
            customer_details.setPin_code(customerInputData.getPinCode());
        }
        customer_details.setCompany_name(customerInputData.getCompany_name().toUpperCase());
        customer_details.setOccupation(customerInputData.getOccupation().toUpperCase());

        //Set Customer details
        lead_data.setCustomer_details(customer_details);

        //Mobile Numbers
        lead_data.setMobile_numbers(customerInputData.getMobileNumberses());

        //telephony
        AllMobileNumbersSearch allMobileNumbersSearch ;
        System.out.println("AddLead Mobile Size- "+customerInputData.getMobileNumberses().size());
        if (customerInputData.getMobileNumberses().size() > 0) {
            realm.beginTransaction();
            allMobileNumbersSearch = new AllMobileNumbersSearch();
            allMobileNumbersSearch.setLeadId(0);
            System.out.println("AddLead Mobile - "+customerInputData.getMobileNumberses().get(0).getNumber());
            allMobileNumbersSearch.setName(customerInputData.getFirstName());
            allMobileNumbersSearch.setMobileNumber(customerInputData.getMobileNumberses().get(0).getNumber());
            allMobileNumbersSearch.setDeleteStatus(0);
            realm.copyToRealmOrUpdate(allMobileNumbersSearch);
            realm.commitTransaction();
        }


        System.out.println("Customer Mobile Number");
        //Email Ids
        lead_data.setEmails(customerInputData.getEmails());
        //Customer Details --------------------------------------------------


        //Lead Details Screen
        lead_data.setLead_enq_date(leadInputData.getLeadDate());
        lead_data.setLead_source_id(leadInputData.getEnqSourceId());
        lead_data.setLead_source_category_id(leadInputData.getSourceCategory());
        lead_data.setRef_vin_reg_no(leadInputData.getVinReg());
        lead_data.setExp_closing_date(leadInputData.getExpectedPurDate());
        lead_data.setMode_of_pay(leadInputData.getModeOfPayment());
        //Lead Details Screen------------------------------------------------

        //Car Details
        //For new Car
        customer_details.setBuyer_type_id(carInputData.getBuyer_type_id());
        if (carInputData.getNewCarInputData().size() > 0) {
            List<CreateLeadInputDataServer.Lead_data.Car_details> newCarDetails = new ArrayList<>();
            for (int i = 0; i < carInputData.getNewCarInputData().size(); i++) {
                CreateLeadInputDataServer.Lead_data.Car_details newCar = lead_data.new Car_details();
                newCar.setModel_id(carInputData.getNewCarInputData().get(i).getModel_id());
                newCar.setVariant_id(carInputData.getNewCarInputData().get(i).getVariant_id());
                newCar.setColor_id(carInputData.getNewCarInputData().get(i).getColor_id());
                newCar.setFuel_type_id(carInputData.getNewCarInputData().get(i).getFuel_type_id());
                newCar.setIs_activity_target(carInputData.getNewCarInputData().get(i).getIs_activity_target());
                newCar.setIs_primary(carInputData.getNewCarInputData().get(i).getIs_primary());
                newCarDetails.add(newCar);
            }
            lead_data.setCar_details(newCarDetails);
        }


        //For Ex Car
        if (carInputData.getExCarInputData().size() > 0) {
            List<CreateLeadInputDataServer.Lead_data.Ex_car_details> exCarDetails = new ArrayList<>();
            for (int i = 0; i < carInputData.getExCarInputData().size(); i++) {
                CreateLeadInputDataServer.Lead_data.Ex_car_details exCar = lead_data.new Ex_car_details();
                exCar.setModel_id(carInputData.getExCarInputData().get(i).getModel_id());
                exCar.setIs_primary(carInputData.getExCarInputData().get(i).getIs_primary());
                exCar.setKms_run(carInputData.getExCarInputData().get(i).getKms_run());
                exCar.setIs_activity_target(carInputData.getExCarInputData().get(i).is_activity_target());
                exCar.setMilage(carInputData.getExCarInputData().get(i).getMilage());
                exCar.setPurchase_date(carInputData.getExCarInputData().get(i).getPurchase_date());
                exCarDetails.add(exCar);
            }
            lead_data.setEx_car_details(exCarDetails);
        }

        //For Add Car
        if (carInputData.getAddCarInputData().size() > 0) {
            List<CreateLeadInputDataServer.Lead_data.Ad_car_details> adCarDetails = new ArrayList<>();
            for (int i = 0; i < carInputData.getAddCarInputData().size(); i++) {
                CreateLeadInputDataServer.Lead_data.Ad_car_details addCar = lead_data.new Ad_car_details();
                addCar.setModel_id(carInputData.getAddCarInputData().get(i).getModel_id());
                addCar.setIs_primary(carInputData.getAddCarInputData().get(i).getIs_primary());
                addCar.setKms_run(carInputData.getAddCarInputData().get(i).getKms_run());
                addCar.setIs_activity_target(carInputData.getAddCarInputData().get(i).is_activity_target());
                addCar.setMilage(carInputData.getAddCarInputData().get(i).getMilage());
                addCar.setPurchase_date(carInputData.getAddCarInputData().get(i).getPurchase_date());
                adCarDetails.add(addCar);
            }
            lead_data.setAd_car_details(adCarDetails);
        }
        //Car Details--------------------------------------------------------

        //Assign Lead to Dse

        if (Util.getInt(dseInputData.getDseBranchId()) > 0) {
            lead_data.setLocation_id(Util.getInt(dseInputData.getDseBranchId()));
        }
        System.out.println(lead_data.getLocation_id());
        lead_data.setUser_details(dseInputData.getUser_detailsList());
        lead_data.setLead_tag_id(dseInputData.getTagId());
        //Assign Lead to Dse-------------------------------------------------


        //Activity
        createLeadInputData.setActivity_data(activityInputData.getActivity_data());

        //Activity-----------------------------------------------------------

        //remarks
        createLeadInputData.setRemarks(activityInputData.getRemarks().toUpperCase());
        //
        //Final Lead_Data
        createLeadInputData.setLead_data(lead_data);
        return createLeadInputData;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }


}
