package com.salescrm.telephony.adapter.gamification;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.gamification.GamificationSMProfileActivity;
import com.salescrm.telephony.activity.gamification.GamificationTeamLeaderProfileActivity;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.response.gamification.GamificationSMProfileResponse;
import com.salescrm.telephony.response.gamification.GamificationTeamProfileResponse;
import com.salescrm.telephony.utils.FabricUtils;
import com.salescrm.telephony.utils.Util;

import java.util.HashMap;

/**
 * Created by nndra on 30-Jan-18.
 */

public class GamificationSMProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private GamificationSMProfileResponse.Points mPoints;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_ITEM = 1;
    private String avgPoints = "";

    public GamificationSMProfileAdapter(GamificationSMProfileActivity context, GamificationSMProfileResponse.Points points, String avgPoints) {
        mContext = context;
        mPoints = points;
        this.avgPoints = avgPoints;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // View v = LayoutInflater.from(mContext).inflate(R.layout.activity_gamification_team_leader_profile_items,parent,false);

        if (viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sm_points_footer_item, parent, false);
            return new GamificationSMProfileAdapter.FooterViewHolder(v);
        } else if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_gamification_team_leader_profile_items, parent, false);
            return new GamificationSMProfileAdapter.MyViewHolder(view);
        }
        return null;
        // return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof GamificationSMProfileAdapter.FooterViewHolder) {
            GamificationSMProfileAdapter.FooterViewHolder footerHolder = (GamificationSMProfileAdapter.FooterViewHolder) holder;
            footerHolder.tv_avg_points.setText(avgPoints);

        } else if (holder instanceof GamificationSMProfileAdapter.MyViewHolder) {
            final GamificationSMProfileAdapter.MyViewHolder myViewHolder = (GamificationSMProfileAdapter.MyViewHolder) holder;
            myViewHolder.dse_name_tv.setText(mPoints.getUsers().get(position).getTeam_leader_name());
            myViewHolder.txt_team_avg_point.setText(mPoints.getUsers().get(position).getTeam_average_points());
            myViewHolder.serial_number_tv.setText("" + (position + 1));

            myViewHolder.text_user.setVisibility(View.VISIBLE);
            myViewHolder.text_user.setText("" + mPoints.getUsers().get(position).getTeam_leader_name().toUpperCase().substring(0, 1));

            myViewHolder.bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Check if we're running on Android 5.0 or higher
                    Intent intent = new Intent(mContext, GamificationTeamLeaderProfileActivity.class);
                    intent.putExtra("team_leader_id",""+mPoints.getUsers().get(position).getTeam_leader_id());
                    intent.putExtra("called_from",1);
                    intent.putExtra("POINTS",mPoints.getUsers().get(position).getTeam_average_points());
                    intent.putExtra("USER_NAME",mPoints.getUsers().get(position).getTeam_leader_name());
                    intent.putExtra("PROFILE_URL",mPoints.getUsers().get(position).getTeam_leader_photo_url());

                    mContext.startActivity(intent);

                    String leaderBoardType;

                    if(mPoints.getUsers().get(position).getSpotlight().equalsIgnoreCase("1")) {
                        leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_TEAM_BREAK_UP_OWN;
                    }
                    else {
                        leaderBoardType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_TEAM_BREAK_UP_OTHERS;

                    }

                    HashMap<String, Object> hashMap = new HashMap<String, Object>();
                    hashMap.put(CleverTapConstants.EVENT_LEADER_BOARD_KEY_TYPE,
                            leaderBoardType);
                    CleverTapPush.pushEvent(CleverTapConstants.EVENT_LEADER_BOARD, hashMap);

                }
            });

            Integer aboveAvg = mPoints.getUsers().get(position).getAbove_average();
            if (aboveAvg != null && aboveAvg == 1) {
                myViewHolder.imgAboveAvg.setVisibility(View.VISIBLE);
            } else {
                myViewHolder.imgAboveAvg.setVisibility(View.INVISIBLE);
            }

            if (Util.isNotNull(mPoints.getUsers().get(position).getTeam_leader_photo_url())) {
                myViewHolder.imgUser.setVisibility(View.VISIBLE);

                Glide.with(mContext).
                        load(mPoints.getUsers().get(position).getTeam_leader_photo_url())
                        .asBitmap()
                        .centerCrop()
                        .listener(new RequestListener<String, Bitmap>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                myViewHolder.imgUser.setVisibility(View.GONE);
                                myViewHolder.text_user.setVisibility(View.VISIBLE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                myViewHolder.imgUser.setVisibility(View.VISIBLE);
                                myViewHolder.text_user.setVisibility(View.GONE);
                                return false;
                            }
                        })
                        .into(new BitmapImageViewTarget(myViewHolder.imgUser) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                if (resource != null) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    myViewHolder.imgUser.setImageDrawable(circularBitmapDrawable);
                                }
                            }
                        });

            } else {
                myViewHolder.text_user.setVisibility(View.VISIBLE);
                myViewHolder.imgUser.setVisibility(View.GONE);

            }


        }

    }

    @Override
    public int getItemCount() {
        return (mPoints.getUsers().size() + 1);
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        TextView tv_avg_points;

        public FooterViewHolder(View itemView) {
            super(itemView);
            tv_avg_points = (TextView) itemView.findViewById(R.id.team_avg_pts);

        }
    }


    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionFooter(int position) {
        if (mPoints != null) {
            return position == mPoints.getUsers().size();
        }
        return false;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView dse_name_tv, text_user, txt_team_avg_point, serial_number_tv;
        ImageView imgUser;
        private ImageView imgAboveAvg;
        private View bg;

        public MyViewHolder(View itemView) {
            super(itemView);

            bg = itemView;

            txt_team_avg_point = (TextView) itemView.findViewById(R.id.team_profile_gamification_txt_team_avg_point);
            serial_number_tv = (TextView) itemView.findViewById(R.id.team_profile_gamification_serial_number);
            dse_name_tv = (TextView) itemView.findViewById(R.id.team_profile_gamification_dse_name);
            imgUser = (ImageView) itemView.findViewById(R.id.team_profile_gamification_img_user);
            text_user = (TextView) itemView.findViewById(R.id.team_profile_gamification_text_user);
            imgAboveAvg = itemView.findViewById(R.id.image_profile_gamification_txt_team_above_avg);
        }
    }

}
