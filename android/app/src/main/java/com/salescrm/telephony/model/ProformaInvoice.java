package com.salescrm.telephony.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.salescrm.telephony.response.Error;

import java.util.List;

/**
 * Created by prateek on 18/5/18.
 */

public class ProformaInvoice {

    @SerializedName("error")
    @Expose
    private Error error;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("result")
    @Expose
    private Result result;

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }


    public class Result {
        @SerializedName("template_exist")
        @Expose
        private Boolean templateExist;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("invoice_items")
        @Expose
        private List<InvoiceItem> invoiceItems = null;
        @SerializedName("on_road_price")
        @Expose
        private String onRoadPrice;
        @SerializedName("customer_details")
        @Expose
        private CustomerDetail customerDetails = null;

        public Boolean getTemplateExist() {
            return templateExist;
        }

        public void setTemplateExist(Boolean templateExist) {
            this.templateExist = templateExist;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<InvoiceItem> getInvoiceItems() {
            return invoiceItems;
        }

        public void setInvoiceItems(List<InvoiceItem> invoiceItems) {
            this.invoiceItems = invoiceItems;
        }

        public String getOnRoadPrice() {
            return onRoadPrice;
        }

        public void setOnRoadPrice(String onRoadPrice) {
            this.onRoadPrice = onRoadPrice;
        }

        public CustomerDetail getCustomerDetails() {
            return customerDetails;
        }

        public void setCustomerDetails(CustomerDetail customerDetails) {
            this.customerDetails = customerDetails;
        }

    }
    public class InvoiceItem {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("sign")
        @Expose
        private String sign;
        @SerializedName("percent")
        @Expose
        private String percent;
        @SerializedName("min_price")
        @Expose
        private Double minPrice;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getPercent() {
            return percent;
        }

        public void setPercent(String percent) {
            this.percent = percent;
        }

        public Double getMinPrice() {
            return minPrice;
        }

        public void setMinPrice(Double minPrice) {
            this.minPrice = minPrice;
        }

    }

    public class CustomerDetail {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("home_address")
        @Expose
        private String homeAddress;
        @SerializedName("office_address")
        @Expose
        private String officeAddress;
        @SerializedName("model_id")
        @Expose
        private String modelId;
        @SerializedName("model_name")
        @Expose
        private String modelName;
        @SerializedName("variant_id")
        @Expose
        private String variantId;
        @SerializedName("variant_name")
        @Expose
        private String variantName;
        @SerializedName("fuel_type_id")
        @Expose
        private String fuelTypeId;
        @SerializedName("color_id")
        @Expose
        private String colorId;
        @SerializedName("color_name")
        @Expose
        private String colorName;
        @SerializedName("is_car_primary")
        @Expose
        private String isCarPrimary;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getHomeAddress() {
            return homeAddress;
        }

        public void setHomeAddress(String homeAddress) {
            this.homeAddress = homeAddress;
        }

        public String getOfficeAddress() {
            return officeAddress;
        }

        public void setOfficeAddress(String officeAddress) {
            this.officeAddress = officeAddress;
        }

        public String getModelId() {
            return modelId;
        }

        public void setModelId(String modelId) {
            this.modelId = modelId;
        }

        public String getModelName() {
            return modelName;
        }

        public void setModelName(String modelName) {
            this.modelName = modelName;
        }

        public String getVariantId() {
            return variantId;
        }

        public void setVariantId(String variantId) {
            this.variantId = variantId;
        }

        public String getVariantName() {
            return variantName;
        }

        public void setVariantName(String variantName) {
            this.variantName = variantName;
        }

        public String getFuelTypeId() {
            return fuelTypeId;
        }

        public void setFuelTypeId(String fuelTypeId) {
            this.fuelTypeId = fuelTypeId;
        }

        public String getColorId() {
            return colorId;
        }

        public void setColorId(String colorId) {
            this.colorId = colorId;
        }

        public String getColorName() {
            return colorName;
        }

        public void setColorName(String colorName) {
            this.colorName = colorName;
        }

        public String getIsCarPrimary() {
            return isCarPrimary;
        }

        public void setIsCarPrimary(String isCarPrimary) {
            this.isCarPrimary = isCarPrimary;
        }

    }
}
