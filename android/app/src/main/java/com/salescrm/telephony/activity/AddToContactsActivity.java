package com.salescrm.telephony.activity;

import android.content.ComponentName;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by prateek on 9/4/18.
 */

public class AddToContactsActivity extends AppCompatActivity {
    TextView tvMobile;
    TextView tvCustomerName;
    Button btnAddContact;
    String number = "";
    String customerName = "";
    private Toolbar toolbar;
    private TextView customTitle;
    private Preferences pref;
    private Handler handler;
    private boolean isFromMyTask;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_contacts);
        pref = Preferences.getInstance();
        toolbar = (Toolbar) findViewById(R.id.addcontact_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        customTitle = (TextView) findViewById(R.id.custom_title);
        customTitle.setText("Whatsapp msg");
        tvMobile = (TextView) findViewById(R.id.mo_number);
        tvCustomerName = (TextView) findViewById(R.id.customer_name);
        btnAddContact = (Button) findViewById(R.id.add_to_contact);
        handler = new Handler();
        if(getIntent().getStringExtra("phono")!= null){
            number = getIntent().getStringExtra("phono");
            customerName = getIntent().getStringExtra("customer_name");
            isFromMyTask = getIntent().getBooleanExtra("from_my_task", false);
        }

        tvMobile.setText(number);
        tvCustomerName.setText(customerName);
        btnAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*ContentValues contentValues = new ContentValues();
                contentValues.put(ContactsContract.RawContacts.ACCOUNT_TYPE, "");
                contentValues.put(ContactsContract.RawContacts.ACCOUNT_NAME, "");
                Uri rawContactUri = getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, contentValues);
                long rawContactId = ContentUris.parseId(rawContactUri);

                contentValues.clear();
                contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
                contentValues.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
                contentValues.put(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, "Mike Sullivan");
                contentValues.put(ContactsContract.CommonDataKinds.Phone.NUMBER, "91"+number);
                getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues);*/
                ArrayList < ContentProviderOperation > ops = new ArrayList < ContentProviderOperation > ();

                ops.add(ContentProviderOperation.newInsert(
                        ContactsContract.RawContacts.CONTENT_URI)
                        .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                        .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                        .build());

                ops.add(ContentProviderOperation.newInsert(
                        ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                        .withValue(
                                ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                                customerName).build());

                ops.add(ContentProviderOperation.
                        newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, ""+tvMobile.getText())
                        .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                                ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                        .build());

                try {
                    getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(AddToContactsActivity.this, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(AddToContactsActivity.this, "Number("+number+")added to contact list", Toast.LENGTH_LONG).show();
                //AddToContactsActivity.this.finish();
                callWhatsappActivity();
            }
        });
    }

    private void openWhatsapp() {
        /** You can't send anything to whatsapp using this code. I'm using it just to open whatsapp*/
        Intent sendIntent = new Intent("android.intent.action.MAIN");
        sendIntent.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
        sendIntent.putExtra("jid",  PhoneNumberUtils.stripSeparators("91"+number)+"@s.whatsapp.net");//phone number without "+" prefix
        try {
            startActivity(sendIntent);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    AddToContactsActivity.this.finish();
                    sendPdf();
                }
            }, 200);
        } catch (android.content.ActivityNotFoundException ex) {
            Util.showToast(this, "Whatsapp not available, Please install!!", Toast.LENGTH_SHORT);
        }
    }

    private void callWhatsappActivity() {
        if(getIntent().getBooleanExtra("from_proforma", false)) {
            openWhatsapp();
        }else {
            Intent intent = new Intent(this, WhatsappActivity.class);
            intent.putExtra("phono", tvMobile.getText() + "");
            intent.putExtra("from_my_task", isFromMyTask);
            startActivity(intent);
        }
    }

    private void sendPdf() {
        if(getIntent().getStringExtra("pdf_file") != null) {
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("*/*");
            whatsappIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("91" + number) + "@s.whatsapp.net");
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(getIntent().getStringExtra("pdf_file")));
            whatsappIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put(CleverTapConstants.EVENT_PROFORMA_INVOICE_KEY_TYPE,
                        CleverTapConstants.EVENT_PROFORMA_INVOICE_TYPE_SENT);
                hashMap.put(CleverTapConstants.EVENT_PROFORMA_INVOICE_KEY_SENT_TYPE,
                        CleverTapConstants.EVENT_PROFORMA_INVOICE_SENT_TYPE_WHATSAPP);
                CleverTapPush.pushEvent(CleverTapConstants.EVENT_PROFORMA_INVOICE, hashMap);

                startActivity(whatsappIntent);
                pref.setProformaSent(true);
                ProformaPdfActivity.getInstance().finish();
                AddToContactsActivity.this.finish();
            } catch (android.content.ActivityNotFoundException ex) {
                Util.showToast(this, "Whatsapp not available, Please install!!", Toast.LENGTH_SHORT);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }
}
