package com.salescrm.telephony.activity.gamification;

import android.app.AlertDialog;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.luckywheel.ClaimBonusPojo;
import com.salescrm.telephony.luckywheel.LuckyWheel;
import com.salescrm.telephony.luckywheel.LuckyWheelPojo;
import com.salescrm.telephony.luckywheel.OnLuckyWheelReachTheTarget;
import com.salescrm.telephony.luckywheel.SpinDialogDoneListener;
import com.salescrm.telephony.luckywheel.SpinDoneDialog;
import com.salescrm.telephony.luckywheel.WheelItem;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.FabricUtils;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

public class GamificationLuckyWheelActivity extends AppCompatActivity {

    private List<WheelItem> wheelItems = new ArrayList<>();
    private LuckyWheel lw;
    private int max = 8, min = 1, randomValue;
    private Preferences pref;
    private TextView tv1, tv2;
    private LuckyWheelPojo luckyWheelContent;
    private Button spinButton;
    private ListView lv;
    private View convertView;
    private ArrayList<String> dseNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamification_lucky_wheel);
        tv1 = (TextView) findViewById(R.id.spin_txt1);
        tv2 = (TextView) findViewById(R.id.spin_txt2);
        spinButton = (Button) findViewById(R.id.start);
        pref = Preferences.getInstance();
        pref.load(this);
        convertView = (View) getLayoutInflater().inflate(R.layout.spin_dialog_custom, null);
        lv = (ListView) convertView.findViewById(R.id.listV);
        dseNames = new ArrayList<>();
        luckyWheelContent = new Gson().fromJson(getIntent().getExtras().getString("spinValues"), LuckyWheelPojo.class);
        getSpinWheelOptions(luckyWheelContent);

        lw = (LuckyWheel) findViewById(R.id.lwv);
        lw.addWheelItems(wheelItems);


        //lw.rotateWheelTo(2);

        lw.setLuckyWheelReachTheTarget(new OnLuckyWheelReachTheTarget() {
            @Override
            public void onReachTarget() {
                if (luckyWheelContent.getOptions().get(randomValue - 1).getOption_id().equalsIgnoreCase(WSConstants.SPIN_AGAIN)) {
                    spinButton.setVisibility(View.VISIBLE);
                } else {
                    if (luckyWheelContent.getOptions().get(randomValue - 1).getSpin_id().equalsIgnoreCase(WSConstants.SPIN_ID_TEAM_LEAD)) {
                        if (luckyWheelContent.getOptions().get(randomValue - 1).getOption_id().equalsIgnoreCase("19") ||
                                luckyWheelContent.getOptions().get(randomValue - 1).getOption_id().equalsIgnoreCase("21")) {
                            openDseList(luckyWheelContent.getOptions().get(randomValue - 1).getOption_id());
                        } else {
                            setSpinButtonAndCallApi();
                        }
                    } else {
                        setSpinButtonAndCallApi();

                    }
                }
            }
        });

        spinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (luckyWheelContent.getSelected_option() != null && !luckyWheelContent.getSelected_option().equalsIgnoreCase("")) {
                    randomValue = Integer.parseInt(luckyWheelContent.getSelected_option());
                    luckyWheelContent.setSelected_option("");
                } else {
                    randomValue = new Random().nextInt(max - min + 1) + min;
                    luckyWheelContent.setSelected_option("" + randomValue);
                }
                lw.rotateWheelTo(randomValue);
                spinButton.setVisibility(View.INVISIBLE);
            }
        });


    }

    private void setSpinButtonAndCallApi() {
        callClaimBonusApi();
        if (luckyWheelContent.getOptions().get(randomValue - 1).getOption_id().equalsIgnoreCase("9")) {
            spinButton.setVisibility(View.VISIBLE);
        } else {
            spinButton.setVisibility(View.INVISIBLE);
        }
    }

    private void openDseList(String option_id) {
        dseNames.clear();
        for (int i = 0; i < luckyWheelContent.getDse_under_me().size(); i++) {
            dseNames.add(luckyWheelContent.getDse_under_me().get(i).getUser_name());
        }
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setView(convertView);
        LayoutInflater inflater = getLayoutInflater();
        View view=inflater.inflate(R.layout.spinwheel_dse_dialog_titlebar, null);
        alertDialog.setCustomTitle(getViewForTitleBar(view, option_id));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dseNames);
        lv.setAdapter(adapter);
        final AlertDialog ad = alertDialog.show();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                ad.dismiss();
                callClaimBonusApiTeamLead(position);
            }
        });
    }


    private void callClaimBonusApi() {
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).sendClaimedBonus(luckyWheelContent.getLead_id(),
                luckyWheelContent.getScheduled_activity_id(),
                luckyWheelContent.getOptions().get(randomValue - 1).getOption_id(),
                luckyWheelContent.getOptions().get(randomValue - 1).getSpin_id(), null, null, new Callback<ClaimBonusPojo>() {
                    @Override
                    public void success(ClaimBonusPojo claimBonusPojo, Response response) {
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            if (header.getName().equalsIgnoreCase("Authorization")) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }
                        }
                        if (("" + claimBonusPojo.getStatusCode()).equalsIgnoreCase("2002")) {

                            pushCleverTap(luckyWheelContent.getOptions().get(randomValue -1).getSpin_id());

                            if (luckyWheelContent.getOptions().get(randomValue - 1).getOption_id().equalsIgnoreCase(WSConstants.SPIN_AGAIN_PLUS_POINT_ONE)
                                    || luckyWheelContent.getOptions().get(randomValue - 1).getOption_id().equalsIgnoreCase(WSConstants.SPIN_AGAIN_PLUS_POINT_TWO)
                                    || luckyWheelContent.getOptions().get(randomValue - 1).getOption_id().equalsIgnoreCase(WSConstants.SPIN_AGAIN)) {
                                new SpinDoneDialog(GamificationLuckyWheelActivity.this, claimBonusPojo.getResult().getTitle(),
                                        claimBonusPojo.getResult().getSub_title(), luckyWheelContent.getOptions().get(randomValue - 1).getBooster_sign(),
                                        claimBonusPojo.getResult().getHeading(),
                                        new SpinDialogDoneListener() {
                                            @Override
                                            public void onOkPressed() {

                                            }
                                        }).show();
                                spinButton.setVisibility(View.VISIBLE);
                            } else {
                                new SpinDoneDialog(GamificationLuckyWheelActivity.this, claimBonusPojo.getResult().getTitle(), claimBonusPojo.getResult().getSub_title(),
                                        luckyWheelContent.getOptions().get(randomValue - 1).getBooster_sign(),
                                        claimBonusPojo.getResult().getHeading(),
                                        new SpinDialogDoneListener() {
                                            @Override
                                            public void onOkPressed() {
                                                finish();
                                            }
                                        }).show();
                            }
                        } else {
                            spinButton.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Util.showToast(GamificationLuckyWheelActivity.this, error.getMessage(), Toast.LENGTH_SHORT);
                        spinButton.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void callClaimBonusApiTeamLead(int position) {
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).sendClaimedBonus(luckyWheelContent.getLead_id(),
                luckyWheelContent.getScheduled_activity_id(),
                luckyWheelContent.getOptions().get(randomValue - 1).getOption_id(),
                luckyWheelContent.getOptions().get(randomValue - 1).getSpin_id(),
                luckyWheelContent.getDse_under_me().get(position).getUser_id(),
                luckyWheelContent.getDse_under_me().get(position).getLocation_id(), new Callback<ClaimBonusPojo>() {
                    @Override
                    public void success(ClaimBonusPojo claimBonusPojo, Response response) {
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            if (header.getName().equalsIgnoreCase("Authorization")) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }
                        }
                        if (("" + claimBonusPojo.getStatusCode()).equalsIgnoreCase("2002")) {
                            pushCleverTap(luckyWheelContent.getOptions().get(randomValue -1).getSpin_id());
                            new SpinDoneDialog(GamificationLuckyWheelActivity.this, claimBonusPojo.getResult().getTitle(),
                                    claimBonusPojo.getResult().getSub_title(), luckyWheelContent.getOptions().get(randomValue - 1).getBooster_sign(),
                                    claimBonusPojo.getResult().getHeading(),
                                    new SpinDialogDoneListener() {
                                        @Override
                                        public void onOkPressed() {
                                            finish();
                                        }
                                    }).show();
                        } else {
                            spinButton.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Util.showToast(GamificationLuckyWheelActivity.this, error.getMessage(), Toast.LENGTH_SHORT);
                        spinButton.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void getSpinWheelOptions(LuckyWheelPojo luckyWheelPojo) {
        if (luckyWheelPojo != null) {
            tv1.setText(luckyWheelPojo.getHeading().getTitle());
            tv2.setText(luckyWheelPojo.getHeading().getDescription());
            wheelItems.clear();
            for (int i = 0; i < luckyWheelPojo.getOptions().size(); i++) {
                wheelItems.add(new WheelItem(Color.parseColor(luckyWheelPojo.getOptions().get(i).getColor_code()),
                        BitmapFactory.decodeResource(getResources(), R.drawable.booster_inside_wheel),
                        luckyWheelPojo.getOptions().get(i).getBooster_sign(),
                        luckyWheelPojo.getOptions().get(i).getTitle(), luckyWheelPojo.getOptions().get(i).getDescription()));
            }
        }
    }

    private void pushCleverTap(String id) {
        String eventType = null;
        if(id.equalsIgnoreCase("1")) {
            eventType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_CLAIM_BONUS_RETAIL;
        }
        else if(id.equalsIgnoreCase("2")) {
            eventType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_CLAIM_BONUS_RETAIL_TARGET;

        }
        else if(id.equalsIgnoreCase("3")) {
            eventType = CleverTapConstants.EVENT_LEADER_BOARD_TYPE_CLAIM_BONUS_TEAM_ZERO_PENDING;

        }
        if(eventType!=null) {
            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put(CleverTapConstants.EVENT_LEADER_BOARD_KEY_TYPE,
                    eventType);
            CleverTapPush.pushEvent(CleverTapConstants.EVENT_LEADER_BOARD, hashMap);
            //Fabric Events
            FabricUtils.sendEvent(pref, "LeaderBoard-"+pref.getDealerName(),
                    "Type", eventType);
        }

    }

    private View getViewForTitleBar(View view, String optionId) {
        String titleMain = luckyWheelContent.getOptions().get(randomValue - 1).getTitle().replace(",", "");
        String subtitle = "";
        if(optionId.equalsIgnoreCase("19")){
            subtitle = "1 day";
        }else {
            subtitle = "2 days";
        }
        String title =  "<big><font color=#66b1f0>Choose Consultant: </font></big> <br/> <small> who will get "
                +titleMain +" for "+subtitle+"</small>";
        ((TextView) view.findViewById(R.id.tv_heading)).setText(Util.fromHtml(title));
        return view;
    }
}
