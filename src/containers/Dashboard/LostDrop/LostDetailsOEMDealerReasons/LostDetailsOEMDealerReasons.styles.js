import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    container: {
      backgroundColor:'#ffffff',
      alignItems: 'center',
      backgroundColor:'rgba(0,0,0,0.1)',
      width: '100%',
      height : '100%',
      flex:10,
      paddingLeft:10,
      paddingRight:10,
     },
     top: {
       flex:4,
       width:'100%'
     },
     bottom: {
       backgroundColor:'#fff',
       width:'100%',
       flex:6,
       alignItems:'center',
       alignSelf:'center',
       borderTopLeftRadius:6,
       borderTopRightRadius:6,
       paddingTop:10,

     },
     title: {
       color:'#3A4F89',
       fontSize:20,
       alignSelf:'center',
       padding:4,
     },
     subTitle: {
       color:'#3A4F89',
       fontSize:16,
       alignSelf:'center',
     },
     name: {
       color:'#3A4F89',
       fontSize:18,
       alignSelf:'center',
       padding:10,
     },
     hr: {
       height:1,
       backgroundColor:'#D0D0D0',
       width:'100%',
       marginTop:14,
     },
     hrItem: {
       height:0.5,
       backgroundColor:'#D0D0D0',
       width:'100%',
       marginTop:10,
     },
     itemMain: {
       flex:1,
       paddingLeft:20,
       paddingRight:20,
       flexWrap:'wrap',
       alignSelf: 'stretch',
       alignItems:'stretch',

     },
     itemInner: {
       flex:1,
       alignSelf: 'stretch',
       flexDirection:'row',
       justifyContent: 'space-between',

     }

});
