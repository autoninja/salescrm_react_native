import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import ConsultantDetails from '../../components/leaderboard_consultant_detail';
import LeaderBoardTeamsDetails from '../../components/leader_board_teams_detail';
import GamificationConfigurationService from '../../storage/gamification_configuration_service';
import LeaderBoardHelp from '../../components/leader_board_help';
import GameLocationPickerDialog from '../../components/game_location_picker';
import LeaderBoardBottomNavigator from './leaderboard.navigationBottom';
import HamburgerIcon from '../../components/uielements/HamburgerIcon';

const LeaderBoardStackNavigator = createAppContainer(createStackNavigator({
    LeaderBoard: {
        screen: LeaderBoardBottomNavigator,
        navigationOptions: ({ navigation }) => ({

            title: 'Leader Board',

            headerBackTitle: null,

            headerLeft: <HamburgerIcon navigationProps={navigation} />,

            headerStyle: {
                backgroundColor: '#2e5e86'
            },
            headerRight: (
                <TouchableOpacity onPress={() => navigation.navigate('GameLocationPickerDialog', {
                    callback() {
                        navigation.replace('LeaderBoard', { isRefresh: true });
                    }
                })} >
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginRight: 5 }}>


                        <Text style={{ textAlign: 'center', color: '#fff' }}> {
                            GamificationConfigurationService.getSelectedGameLocationName(global.selectedGameLocationId)
                        }
                        </Text>

                        <Image
                            style={{ width: 10, height: 13, margin: 10, alignItems: 'center' }}
                            source={require('../../images/ic_location_right.png')}
                        />

                    </View>
                </TouchableOpacity>
            ),
            headerTransperent: true,
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
                width: '100%',
                textAlign: 'left',
            },
        }),
        /*{<Tabs/>}*/
    },

    LeaderBoardTeamsDetails: {
        screen: LeaderBoardTeamsDetails,
        navigationOptions: ({ navigation }) => ({
            header: null,
        })
    },

    ConsultantDetails: {
        screen: createStackNavigator({
            ConsultantDetail: {
                screen: ConsultantDetails,
                navigationOptions: ({ navigation }) => ({
                    header: null,
                })
            },
            LeaderBoardHelp: {
                screen: LeaderBoardHelp,
                navigationOptions: ({ navigation }) => ({
                    title: 'Help',
                    headerTitleStyle: { fontWeight: 'bold', color: '#fff', textAlign: 'center', marginLeft: 10 },
                    headerStyle: {
                        backgroundColor: '#0F2B52'
                    },
                    headerTintColor: '#fff',
                })
            },
        }, {
            initialRouteName: 'ConsultantDetail',
        }), navigationOptions: ({ navigation }) => ({
            header: null,
        })
    },
    GameLocationPickerDialog: {
        screen: GameLocationPickerDialog,
        navigationOptions: ({ navigation }) => ({
            header: null,
        })
    }
}));

export default LeaderBoardStackNavigator;
