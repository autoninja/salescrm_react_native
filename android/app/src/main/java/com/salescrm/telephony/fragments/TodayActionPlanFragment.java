package com.salescrm.telephony.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.ActionPlanRealmAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.dataitem.SectionModel;
import com.salescrm.telephony.db.ActivityDetails;
import com.salescrm.telephony.db.CustomerDetails;
import com.salescrm.telephony.db.CustomerPhoneNumbers;
import com.salescrm.telephony.db.LeadDetails;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.dbOperation.LeadMobileMappingDbOperations;
import com.salescrm.telephony.dbOperation.TasksDbOperation;
import com.salescrm.telephony.interfaces.ActionPlanHeaderUpdateListener;
import com.salescrm.telephony.interfaces.AdapterCommunication;
import com.salescrm.telephony.interfaces.AutoFetchFormListener;
import com.salescrm.telephony.interfaces.BadgeUpdater;
import com.salescrm.telephony.interfaces.TaskCardExpanderListener;
import com.salescrm.telephony.model.FilterDataChangedListener;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.model.TaskCardExpander;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.ActionPlanResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bharath on 23/06/16.
 */

public class TodayActionPlanFragment extends Fragment implements AdapterCommunication, Callback<ActionPlanResponse>,
        AutoFetchFormListener, TaskCardExpanderListener {


    public static ActionPlanHeaderUpdateListener actionPlanUpdateListener;
    public Integer dseId;
    SwipeRefreshLayout swipeRefreshLayout;
    List<SectionModel> sections;
    TaskCardExpander taskCardExpander;
    private String TAG = "PendingActionPlan";
    private RecyclerView mRecyclerView;
    private Preferences pref = null;
    private RelativeLayout relAlert;
    private Button btRefresh;
    private TextView tvAlert;
    private Realm realm;
    private CustomerDetails customerDetails;
    private LeadDetails leadDetails;
    private ActivityDetails activityDetails;
    private SalesCRMRealmTable salesCRMRealmTable;
    private int visibleItemCount, totalItemCount, pastVisiblesItems;
    private boolean loading;
    private int total;
    private int totalPage;
    private int currentPage = 1;
    private int prevPage;
    private int pageLimit = 50;
    private ProgressBar progressBottomLayout;
    private LinearLayoutManager mLayoutManager;
    private int apiCallCount = 0;
    private FilterSelectionHolder filterSelectionHolder;
    private ActionPlanRealmAdapter adapter;
    private boolean isCreatedTemp = false;
    private FrameLayout frameLoaderWrapper;
    private boolean refreshRequested =false;

    public static TodayActionPlanFragment newInstance(ActionPlanFragment context) {
        try {
            actionPlanUpdateListener = (ActionPlanHeaderUpdateListener) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        return new TodayActionPlanFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pending_action_plan, container, false);
        pref = Preferences.getInstance();
        filterSelectionHolder = FilterSelectionHolder.getInstance();
        pref.load(getActivity());
        sections = new ArrayList<SectionModel>();
        taskCardExpander = new TaskCardExpander();
        progressBottomLayout = (ProgressBar) rootView.findViewById(R.id.progress_bottom_pending_action);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_action_pending);
        frameLoaderWrapper = (FrameLayout) rootView.findViewById(R.id.frame_task_loader_wrapper);
        relAlert = (RelativeLayout) rootView.findViewById(R.id.frame_alert);
        btRefresh = (Button) rootView.findViewById(R.id.btAlertRefresh);
        tvAlert = (TextView) rootView.findViewById(R.id.tv_alert);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.progressRecycler);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        realm = Realm.getDefaultInstance();

        dseId = pref.getCurrentDseId();
        //
        if(pref.getCurrentDseId()!=null) {
            if (pref.getCurrentDseId().intValue() != pref.getAppUserId().intValue()) {
                isCreatedTemp = true;
            }
        }

        taskCardExpander = new TaskCardExpander();


        setupRealmAdapter();


        /*setSwipeRefreshView(true);
        loading = true;*/
        loading = true;
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading && currentPage != totalPage && filterSelectionHolder.isEmpty()) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            System.out.println(TAG + ":Last Item Wow !");
                            //Do pagination.
                            updateCurrentPage();
                            getPendingData(currentPage, true, WSConstants.PENDING);


                        }
                    }
                }

            }
        });
        btRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                    relAlert.setVisibility(View.GONE);
                    refreshRequested = true;
                    getPendingData(1, false, WSConstants.TODAY);
                } else {
                    Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                    setSwipeRefreshView(false);
                }
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                    if (filterSelectionHolder != null && !filterSelectionHolder.isEmpty()) {
                        Toast.makeText(getContext(), "Refresh not allowed during filter", Toast.LENGTH_SHORT).show();
                        setSwipeRefreshView(false);
                    } else {
                        refreshRequested = true;
                        getPendingData(1, false, WSConstants.TODAY);
                    }
                } else {
                    //Offline
                    Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                    setSwipeRefreshView(false);
                }
            }
        });
        System.out.println(TAG + "OnCreate");

        return rootView;
    }


    private void scrollTaskPositionForNotification(){
        if(getActivity()!=null&&getActivity().getIntent()!=null) {

            Bundle bundle = getActivity().getIntent().getExtras();
            if (bundle != null&&bundle.getBoolean(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY)) {
                taskCardExpander = new TaskCardExpander();
                int position = -1;
                int scheduledActivityId = bundle.getInt(WSConstants.SCHEDULED_ACTIVITY_ID);
                if (filterSelectionHolder.isEmpty() && !isCreatedTemp) {
                    OrderedRealmCollection<SalesCRMRealmTable> realmResult = realm.where(SalesCRMRealmTable.class)
                            .equalTo("isCreatedTemporary", isCreatedTemp)
                            .lessThan("activityScheduleDate", Util.getTime(1))
                            .equalTo("isDone", false)
                            .equalTo("isLeadActive", 1)
                            .equalTo("createdOffline", false)
                            .equalTo("dseId", dseId)
                            .findAllSorted("activityScheduleDate", Sort.DESCENDING);

                    for (int i = 0; i < realmResult.size(); i++) {
                        if (scheduledActivityId == realmResult.get(i).getScheduledActivityId()) {
                            position = i;
                            break;
                        }
                    }

                }
                taskCardExpander.setPosition(position);
                taskCardExpander.setOpen(position >= 0);
                taskCardExpander.setScheduledActivityId(scheduledActivityId);
                if (taskCardExpander.isOpen()) {
                    mLayoutManager.scrollToPosition(position);
                    adapter.notifyDataSetChanged();
                    taskCardExpander = null;

                }

                Intent intent = getActivity().getIntent();
                intent.putExtra(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY,false);
                getActivity().setIntent(intent);
            }
        }

    }

    private void setupRealmAdapter() {
        //Setting up adapter.
        OrderedRealmCollection<SalesCRMRealmTable> realmResult = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .lessThan("activityScheduleDate", Util.getTime(1))
                .equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("createdOffline", false)
                .equalTo("dseId", dseId)
                .findAllSorted("activityScheduleDate", Sort.DESCENDING);

        adapter = new ActionPlanRealmAdapter(getContext(),
                realmResult, true, sections,taskCardExpander,TodayActionPlanFragment.this);
        mRecyclerView.setAdapter(adapter);

        updateViews();
    }

    private void setupRealmAdapterWithFilter() {
        //Setting up adapter with filter.
        RealmQuery<SalesCRMRealmTable> realmQuery = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .lessThan("activityScheduleDate", Util.getTime(1))
                .equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("dseId", dseId)
                .equalTo("createdOffline", false);

        for (int i = 0; i < filterSelectionHolder.getAllMapData().size(); i++) {
            int insideKey = filterSelectionHolder.getAllMapData().keyAt(i);
            String key = Util.getAdvanceFilterKey(insideKey);
            System.out.println("Key::" + key);
            if (!TextUtils.isEmpty(key)) {

                //Special case
                if (key.equalsIgnoreCase("activityId")) {
                    List<String> activities = new ArrayList<>();
                    for (int j = 0; j < filterSelectionHolder.getMapData(insideKey).size(); j++) {
                        Collections.addAll(activities, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j)).split(","));
                    }

                    for (int z = 0; z < realmQuery.findAll().size(); z++) {
                        System.out.println("Activity ID of task in pending:" + realmQuery.findAll().get(z).getActivityId());
                    }
                    for (int activity = 0; activity < activities.size(); activity++) {
                        System.out.println("Activity Id:" + activities.get(activity));
                        if (activity == 0) {
                            realmQuery = realmQuery.beginGroup();
                        }
                        realmQuery = realmQuery.equalTo(key, Util.getInt(activities.get(activity)));
                        if (activity != activities.size() - 1) {
                            realmQuery = realmQuery.or();
                        } else {
                            realmQuery = realmQuery.endGroup();
                        }
                    }


                } else {

                    for (int j = 0; j < filterSelectionHolder.getMapData(insideKey).size(); j++) {

                        System.out.println("value at" + j + ":" + filterSelectionHolder.getMapData(insideKey).keyAt(j));

                        if (key.equalsIgnoreCase("date")) {
                            int dateKey = filterSelectionHolder.getMapData(insideKey).keyAt(j);
                            Date dateVal = Util.getCalender(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j))).getTime();
                            System.out.println("Key:" + dateKey);
                            System.out.println("Date Val:" + dateVal.toString());
                            if (dateKey == 0) {
                                realmQuery = realmQuery.greaterThanOrEqualTo("leadExpectedClosingDate", dateVal);
                            } else if (dateKey == 1) {
                                realmQuery = realmQuery.lessThanOrEqualTo("leadExpectedClosingDate", dateVal);

                            } else if (dateKey == 2) {
                                realmQuery = realmQuery.greaterThanOrEqualTo("activityScheduleDate", dateVal);

                            } else if (dateKey == 3) {
                                realmQuery = realmQuery.lessThanOrEqualTo("activityScheduleDate", dateVal);

                            }
                            continue;
                        }
                        if (key.equalsIgnoreCase("leadAge")) {
                            int valFrom = Integer.parseInt(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j)));
                            int valTo = Integer.parseInt(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j + 1)));
                            realmQuery = realmQuery.between(key, valFrom, valTo);
                            break;
                        }


                        if (j == 0) {
                            realmQuery = realmQuery.beginGroup();
                        }
                        if(key.equalsIgnoreCase("leadCarModelId")){
                            realmQuery = realmQuery.equalTo(key, String.valueOf(filterSelectionHolder.getMapData(insideKey).keyAt(j))+"");
                        }
                        else {
                            realmQuery = realmQuery.contains(key, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j)));

                        }

                        if (j != filterSelectionHolder.getMapData(insideKey).size() - 1) {
                            realmQuery = realmQuery.or();
                        } else {
                            realmQuery = realmQuery.endGroup();
                        }

                    }
                }

            }

        }
        OrderedRealmCollection<SalesCRMRealmTable> realmResult = realmQuery.findAllSorted("activityScheduleDate", Sort.DESCENDING);

        mRecyclerView.setAdapter(new ActionPlanRealmAdapter(getContext(),
                realmResult, true, null));

        updateViews();
        if (realmResult.size() == 0) {
            relAlert.setVisibility(View.VISIBLE);
            tvAlert.setText("No task in this filter");
            btRefresh.setVisibility(View.INVISIBLE);
        }
        if (actionPlanUpdateListener != null) {
            actionPlanUpdateListener.onFilterAppliedUpdateText(0, "Today " + realmQuery.findAll().size());
            //  SalesCRMApplication.getBus().post(new TabLayoutText(2, "Pending " + realmQuery.findAll().size(), realmQuery.findAll().size(), 0));
        }
    }

    public void getPendingData(int pageNumber, boolean fromBottom, String when) {
        frameLoaderWrapper.setVisibility(View.VISIBLE);
        if (fromBottom&&!refreshRequested) {
            setSwipeRefreshView(false);
            progressBottomLayout.setVisibility(View.VISIBLE);
        } else {
            setSwipeRefreshView(true);
            progressBottomLayout.setVisibility(View.GONE);
        }
        System.out.println(TAG + " GetDataCalled:" + pageNumber);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken())
                .GetActionPlanData(pageNumber, 1000, true, true, 0, null, null, "normal", when, null, WSConstants.ORDER_DESC, dseId,0, this);


    }

    private void updateCurrentPage() {
        currentPage = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("createdOffline", false)
                .equalTo("dseId", dseId)
                .lessThan("activityScheduleDate", Util.getTime(0))
                .findAll().size() / pageLimit + 1;

    }

    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
        System.out.println(TAG + "OnResume");
        if (!filterSelectionHolder.isEmpty()) {
            setupRealmAdapterWithFilter();
        } else {
            setupRealmAdapter();
        }
        scrollTaskPositionForNotification();

    }

    @Subscribe
    public void onFilterDataChanged(FilterDataChangedListener filterDataChangedListener){
        if(filterDataChangedListener.isUpdateRequired()){
            if (!filterSelectionHolder.isEmpty()) {
                setupRealmAdapterWithFilter();
            } else {
                setupRealmAdapter();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       /* realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                *//*realm.delete(LeadDetails.class);
                realm.delete(CustomerDetails.class);
                realm.delete(ActivityDetails.class);*//*
            }
        });*/
        // Clear out all instances.


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Override
    public void AllTaskFragmentCallBack(String LeadId) {
        replaceFragment(LeadId);
    }

    @Override
    public void FormActivityFragmentCallBack() {

    }

    @Override
    public void FormActivityRescheduleCallBack() {
    }

    @Override
    public void FormActivityEmailSmaCallBack() {
    }

    private void replaceFragment(String LeadId) {

    }


    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void setSwipeRefreshView(final boolean val) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                // swipeRefreshLayout.setEnabled(val);
                swipeRefreshLayout.setRefreshing(val);
            }
        });
    }

    @Override
    public void success(final ActionPlanResponse actionPlanResponse, Response response) {
        System.out.println(TAG + " Success");
        if(!refreshRequested) {
            frameLoaderWrapper.setVisibility(View.GONE);
        }
        List<Header> headerList = response.getHeaders();
        for (Header header : headerList) {
            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                ApiUtil.UpdateAccessToken(header.getValue());
            }
        }
        if (!actionPlanResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            loading = true;
            frameLoaderWrapper.setVisibility(View.GONE);
            progressBottomLayout.setVisibility(View.GONE);
            setSwipeRefreshView(false);

            System.out.println("Success:0" + actionPlanResponse.getMessage());
        } else {
            if (actionPlanResponse.getResult() != null) {
                System.out.println("Success:1" + actionPlanResponse.getMessage());

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        addDataToDB(realm, actionPlanResponse.getResult().getData());

                    }
                });

                TasksDbOperation.getInstance().refreshTasks(realm,
                        actionPlanResponse.getResult().getActiveTaskIds(),
                        isCreatedTemp,dseId);

                BadgeUpdater.updateBadge(getContext());

                if (actionPlanUpdateListener != null) {
                    actionPlanUpdateListener.onActionPlanHeaderUpdateRequired(0);
                } else {
                    System.out.println("Listener null inside Today");
                }

                total = Integer.parseInt(actionPlanResponse.getResult().getTotal());
                totalPage = Integer.parseInt(actionPlanResponse.getResult().getTotal_pages());
                pageLimit = Integer.parseInt(actionPlanResponse.getResult().getPage_limit());
                if(refreshRequested){
                    updateCurrentPage();
                    getPendingData(currentPage, true, WSConstants.PENDING);
                   refreshRequested =false;
                }
                else {
                    if (getContext() != null && dseId.intValue() == pref.getAppUserId().intValue()) {
                        createNotification();
                    /*new AutoFetchFormData(this, getContext()).call(realm.where(SalesCRMRealmTable.class)
                            .equalTo("isCreatedTemporary", false)
                            .lessThan("activityScheduleDate", Util.getTime(0))
                            .equalTo("isDone", false)
                            .equalTo("isLeadActive", 1)
                            .equalTo("dseId", dseId)
                            .equalTo("createdOffline", false).findAll(), true);*/
                        loading = true;

                        progressBottomLayout.setVisibility(View.GONE);
                        setSwipeRefreshView(false);
                        updateViews();
                    } else {
                        loading = true;
                        progressBottomLayout.setVisibility(View.GONE);
                        setSwipeRefreshView(false);
                        updateViews();
                    }
                }
            } else {
                loading = true;
                frameLoaderWrapper.setVisibility(View.GONE);
                progressBottomLayout.setVisibility(View.GONE);
                setSwipeRefreshView(false);

                System.out.println("Success:2" + actionPlanResponse.getMessage());
                //showAlert(validateOtpResponse.getMessage());
            }
            LeadMobileMappingDbOperations.updateCallingMobileNumbers(realm);

        }
    }

    private void createNotification(){

        // showAppNotification(this,123,"Testinger",1212,"Followup Call",0,WSConstants.TaskActivityName.FOLLOWUP_CALL_ID);

        OrderedRealmCollection<SalesCRMRealmTable> realmResult = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", false)
                .greaterThanOrEqualTo("activityScheduleDate", Util.getTime(0))
                .lessThan("activityScheduleDate", Util.getTime(1))
                .equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("createdOffline", false)
                .findAll().sort("activityId", Sort.DESCENDING);

        for(int i=0;i<realmResult.size();i++){
            if(realmResult.get(i).getActivityScheduleDate().getTime()>System.currentTimeMillis()) {
                SalesCRMRealmTable salesCRMRealmTable = realmResult.get(i);
                Calendar appNotificationTime = Calendar.getInstance();
                appNotificationTime.setTime(salesCRMRealmTable.getActivityScheduleDate());
                switch (salesCRMRealmTable.getActivityId()){
                    case WSConstants.TaskActivityName.FOLLOWUP_CALL_ID:
                        //Before five minutes!!
                        appNotificationTime.add(Calendar.MINUTE,-5);
                        break;
                    case WSConstants.TaskActivityName.HOME_VISIT_ID:
                        //Before 2Hr minutes!!
                    case WSConstants.TaskActivityName.SHOWROOM_VISIT_ID:
                        //Before 2Hr minutes!!
                    case WSConstants.TaskActivityName.EVALUATION_REQUEST_ID:
                        //Before 2Hr minutes!!
                    case WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID:
                        //Before 2Hr
                    case WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID:
                        //Before 2Hr
                        appNotificationTime.add(Calendar.HOUR_OF_DAY,-2);
                        break;
                    case WSConstants.TaskActivityName.RECEIVE_PAYMENT_ID:
                        //Before 5min
                        appNotificationTime.add(Calendar.MINUTE,-5);
                        break;

                    case WSConstants.TaskActivityName.DELIVERY_REQUEST_ID:
                        //At 9AM
                        appNotificationTime.set(Calendar.HOUR_OF_DAY,9);
                        appNotificationTime.set(Calendar.MINUTE,0);
                        break;


                }
                if(appNotificationTime.getTime().getTime()>System.currentTimeMillis()) {
                    Util.setNotification(getContext(),
                            salesCRMRealmTable.getActivityName(),
                            salesCRMRealmTable.getFirstName()+" "+salesCRMRealmTable.getLastName(),
                            salesCRMRealmTable.getLeadId(),
                            salesCRMRealmTable.getScheduledActivityId(),
                            appNotificationTime.getTime().getTime(),
                            salesCRMRealmTable.getActivityScheduleDate().getTime(),
                            salesCRMRealmTable.getActivityId());

                    System.out.println("Setting alarm for " + salesCRMRealmTable.getScheduledActivityId() + " at:" +
                            appNotificationTime.getTime());
                }
            }
        }
    }
    @Override
    public void onAutoFormsDataFetched(boolean b, RealmResults<SalesCRMRealmTable> data) {
        loading = true;

        progressBottomLayout.setVisibility(View.GONE);
        setSwipeRefreshView(false);
        updateViews();


    }

    @Override
    public void onAutoFormsDataError(RetrofitError error, int from) {
        loading = true;

        progressBottomLayout.setVisibility(View.GONE);
        setSwipeRefreshView(false);
        updateViews();
    }

    @Override
    public void failure(RetrofitError error) {
        setSwipeRefreshView(false);
        frameLoaderWrapper.setVisibility(View.GONE);
        progressBottomLayout.setVisibility(View.GONE);
        loading = true;
        System.out.println(TAG + "Error");
        onErrorApiCallController(error);

    }

    private void onErrorApiCallController(RetrofitError error) {
        if (error.getKind() == RetrofitError.Kind.NETWORK) {
            apiCallCount++;
            if (apiCallCount <= WSConstants.API_MAX_CALL) {
                Util.showToast(getContext(),"Something went wrong, Try again",Toast.LENGTH_SHORT);
            } else {
                apiCallCount = 0;
            }
        } else {
            Util.showToast(getContext(), "Something went wrong", Toast.LENGTH_SHORT);
            Util.systemPrint("Something ***ke* by our *****n* backend developer");
            apiCallCount = 0;
        }
    }


    private void updateViews() {
        int total = realm.where(SalesCRMRealmTable.class).equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .equalTo("createdOffline", false)
                .equalTo("dseId", dseId)
                .lessThan("activityScheduleDate",
                        Util.getTime(1)).findAll().size();

        if (total == 0) {
            relAlert.setVisibility(View.VISIBLE);
            tvAlert.setText("Hurray no task:)");
        } else {
            relAlert.setVisibility(View.GONE);
        }
        sortActivityDetails();
        adapter.notifyDataSetChanged();

    }

    public void addDataToDB(Realm realm, List<ActionPlanResponse.Result.Data> actionPlanResponseData) {


        for (int i = 0; i < actionPlanResponseData.size(); i++) {
            salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId",
                    Integer.parseInt(actionPlanResponseData.get(i).getActivity().getScheduled_activity_id())).findFirst();
            if (salesCRMRealmTable == null) {
                salesCRMRealmTable = new SalesCRMRealmTable();
                salesCRMRealmTable.setScheduledActivityId(Integer.parseInt(actionPlanResponseData.get(i).getActivity().getScheduled_activity_id()));

            }
            salesCRMRealmTable.setIsLeadActive(1);
            salesCRMRealmTable.setTemplateExist(actionPlanResponseData.get(i).getTemplate_exist());
            salesCRMRealmTable.setCustomerId(Integer.parseInt(actionPlanResponseData.get(i).getCustomer().getCustomer_id()));
            salesCRMRealmTable.setCustomerNumber(actionPlanResponseData.get(i).getCustomer().getMobile_number());
            salesCRMRealmTable.setCustomerName(actionPlanResponseData.get(i).getCustomer().getName());
            salesCRMRealmTable.setCustomerEmail(actionPlanResponseData.get(i).getCustomer().getEmail());
            salesCRMRealmTable.setFirstName(actionPlanResponseData.get(i).getCustomer().getFirst_name());
            salesCRMRealmTable.setLastName(actionPlanResponseData.get(i).getCustomer().getLast_name());
            salesCRMRealmTable.setGender(actionPlanResponseData.get(i).getCustomer().getGender());
            salesCRMRealmTable.setTitle(actionPlanResponseData.get(i).getCustomer().getTitle());
            String customerAge = actionPlanResponseData.get(i).getCustomer().getAge();
            salesCRMRealmTable.setCustomerAge(customerAge.equals("") ? null : (customerAge));
            salesCRMRealmTable.setCustomerAddress(actionPlanResponseData.get(i).getCustomer().getCustomer_address());
            salesCRMRealmTable.setLeadLocationName(actionPlanResponseData.get(i).getLead().getLocation_name());
            salesCRMRealmTable.setResidenceAddress(actionPlanResponseData.get(i).getCustomer().getCustomer_address());
            salesCRMRealmTable.setResidencePinCode(actionPlanResponseData.get(i).getCustomer().getResidence_pin_code());
            salesCRMRealmTable.setOfficeAddress(actionPlanResponseData.get(i).getCustomer().getOffice_address());
            salesCRMRealmTable.setOfficePinCode(actionPlanResponseData.get(i).getCustomer().getOffice_pin_code());
          //  System.out.println("All Action Plan  Company Before 2" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
            salesCRMRealmTable.setCompanyName(actionPlanResponseData.get(i).getCustomer().getCompany_name());
           // System.out.println("All Action Plan  Company After 2" + salesCRMRealmTable.getLeadId() + ", " + salesCRMRealmTable.getCompanyName());
            salesCRMRealmTable.setLeadId(Integer.parseInt(actionPlanResponseData.get(i).getLead().getLead_id()));
            salesCRMRealmTable.setLeadCarVariantName(actionPlanResponseData.get(i).getLead_car().getVariant_name());
            salesCRMRealmTable.setLeadCarModelName(actionPlanResponseData.get(i).getLead_car().getModel_name());
            salesCRMRealmTable.setLeadCarModelId(actionPlanResponseData.get(i).getLead_car().getModel_id());
            salesCRMRealmTable.setLeadTagsName(actionPlanResponseData.get(i).getLead().getLead_tags().getTag_names());
            salesCRMRealmTable.setLeadTagsColor(actionPlanResponseData.get(i).getLead().getLead_tags().getTag_colors());
            salesCRMRealmTable.setLeadDseName(actionPlanResponseData.get(i).getLead().getDseAlloted().getDseName());
            salesCRMRealmTable.setLeadDsemobileNumber(actionPlanResponseData.get(i).getLead().getDseAlloted().getDseMobNo());
            salesCRMRealmTable.setLeadStageId(actionPlanResponseData.get(i).getLead().getLead_stage().getId());
            salesCRMRealmTable.setLeadStage(actionPlanResponseData.get(i).getLead().getLead_stage().getStage());

            salesCRMRealmTable.setLeadAge(actionPlanResponseData.get(i).getLead().getLead_age());
            salesCRMRealmTable.setLeadSourceName(actionPlanResponseData.get(i).getLead().getLead_source_name());
            salesCRMRealmTable.setLeadSourceId(actionPlanResponseData.get(i).getLead().getLead_source_id());
            salesCRMRealmTable.setLeadLastUpdated(actionPlanResponseData.get(i).getLead().getLead_last_updated());
            salesCRMRealmTable.setLeadExpectedClosingDate(actionPlanResponseData.get(i).getLead().getExpected_closing_date());

            salesCRMRealmTable.setActivityId(Integer.parseInt(actionPlanResponseData.get(i).getActivity().getActivity_id()));
            salesCRMRealmTable.setActivityDescription(actionPlanResponseData.get(i).getActivity().getActivity_description());
            salesCRMRealmTable.setActivityCreationDate(Util.getDate(actionPlanResponseData.get(i).getActivity().getActivity_creation_date()));
            salesCRMRealmTable.setActivityScheduleDate(Util.getNormalDate(actionPlanResponseData.get(i).getActivity().getActivity_scheduled_date()));
            salesCRMRealmTable.setActivityTypeId(actionPlanResponseData.get(i).getActivity().getActivity_type_id());
            salesCRMRealmTable.setActivityType(actionPlanResponseData.get(i).getActivity().getType());
            salesCRMRealmTable.setActivityName(actionPlanResponseData.get(i).getActivity().getActivity_name());
            salesCRMRealmTable.setActivityGroupId(actionPlanResponseData.get(i).getActivity().getActivity_group_id());
            salesCRMRealmTable.setActivityIconType(actionPlanResponseData.get(i).getActivity().getIcon_type());
            salesCRMRealmTable.setDseId(dseId);
            salesCRMRealmTable.setCreatedTemporary(isCreatedTemp);
            salesCRMRealmTable.setDone(false);
            if(actionPlanResponseData.get(i).getCustomer().getSecondary_mobile_number() != null) {
                salesCRMRealmTable.setSecondaryMobileNumber(Long.parseLong(actionPlanResponseData.get(i).getCustomer().getSecondary_mobile_number()));
            }

            RealmList<CustomerPhoneNumbers> customerPhoneNumbers = new RealmList<CustomerPhoneNumbers>();
            /* CustomerPhoneNumbers customerPhoneNumbers = new CustomerPhoneNumbers();*/
            if (actionPlanResponseData.get(i).getCustomer().getMobile_nos() != null) {
                for (int j = 0; j < actionPlanResponseData.get(i).getCustomer().getMobile_nos().size(); i++) {
                    CustomerPhoneNumbers customerPhoneNumbersObj = new CustomerPhoneNumbers();
                    customerPhoneNumbersObj.setLeadId(Integer.parseInt(actionPlanResponseData.get(i).getLead().getLead_id()));
                    customerPhoneNumbersObj.setCustomerID(Integer.parseInt(actionPlanResponseData.get(i).getCustomer().getCustomer_id()));
                    customerPhoneNumbersObj.setPhoneNumber(Long.parseLong(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getNumber()));
                    customerPhoneNumbersObj.setPhoneNumberID(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getId());
                    customerPhoneNumbersObj.setPhoneNumberStatus(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getStatus());
                    customerPhoneNumbersObj.setLead_phone_mapping_id(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getLead_phone_mapping_id());

                    customerPhoneNumbers.add(customerPhoneNumbersObj);
                }
            }
            //storing Mobile numbers in db
            salesCRMRealmTable.customerPhoneNumbers.addAll(customerPhoneNumbers);

            realm.copyToRealmOrUpdate(salesCRMRealmTable);
        }
        System.out.println(TAG + ":AfterUpdating Db Realm:: salesCRMRealmTable:Pending" + realm.where(SalesCRMRealmTable.class).lessThan("activityScheduleDate",
                Util.getTime(0)).findAll().size());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    /**
     * logic for date wise sections
     */
    public void sortActivityDetails() {
        OrderedRealmCollection<SalesCRMRealmTable> list = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .lessThan("activityScheduleDate", Util.getTime(1))
                .equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("createdOffline", false)
                .equalTo("dseId", dseId)
                .findAllSorted("activityScheduleDate", Sort.DESCENDING);
        sections.clear();
        sections.addAll(Util.createDateSections(list,WSConstants.TODAY_TAB).getSections());
        // todayStartIndex = Util.createDateSections(list).getTodayIndex();

    }


    @Override
    public void onExpandCard(int position, int scheduledActivityId, boolean isClosed) {
        System.out.println("Position::"+position);
        System.out.println("scheduledActivityId::"+scheduledActivityId);
        System.out.println("isClosed::"+isClosed);
       /* taskCardExpander = null;
        adapter.notifyDataSetChanged();*/
    }

}
