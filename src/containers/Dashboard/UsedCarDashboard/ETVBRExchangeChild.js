import React, { Component } from 'react'
import { Platform, View, Text, Image, TouchableOpacity, YellowBox, Dimensions, FlatList, ActivityIndicator } from 'react-native'
import PercentageSwitch from '../../../components/uielements/PercentageSwitch'


import ETVBRHeader from '../../../components/etvbrl/etvbr_header'
import Toast from 'react-native-easy-toast'

import { eventTeamDashboard } from '../../../clevertap/CleverTapConstants';
import CleverTapPush from '../../../clevertap/CleverTapPush'
import ETVBRExchangeItem from '../../../components/etvbrl/etvbr_exchange_item';
import ETVBRExchangeResultItem from '../../../components/etvbrl/etvbr_exchange_results_items';
import ETVBRHeaderExchange from '../../../components/etvbrl/etvbr_exchange_header';
import ETVBRExchangeItemChild from '../../../components/etvbrl/etvbr_exchange_item_child';
import ETVBRExchangeItemExpandable from '../../../components/etvbrl/etvbr_exchange_item_expandable';

export default class ETVBRLExchangeChild extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('title', 'Evaluation Manager'),
        };
    };
    constructor(props) {

        super(props);


        this.state = {
            show_percentage: false,
            result: {}, location_selected: this.props.navigation.getParam('location_selected', 0)
        }


        YellowBox.ignoreWarnings([
            'Warning: componentWillMount is deprecated',
            'Warning: componentWillReceiveProps is deprecated',
        ]);

    }

    componentDidMount() {
        //CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type' : eventTeamDashboard.keyType.valuesType.etvbrTeams });
    }


    _showDetailsOnEvaluationManager(navigation, title, sales_manager, from, location_id) {
        navigation.push('ETVBRLExchangeChild', {
            title,
            sales_manager,
            from,
            location_id,
            location_selected: this.state.location_selected
        });
        //console.log("_showDetailsOnEvaluationManager");
    }

    _etvbrLongPress(navigation, from, data) {
        if (from === 'user_info') {
            navigation.navigate('UserInfo', {
                name: data.info.name,
                dp_url: data.info.dp_url

            });
        }
        else if (from == 'etvbr_details') {
            navigation.navigate('ETVBRDetails', {
                data, isExchange: true
            });
        }

        console.log("_etvbrLongPress" + from);
        console.log(data);
    }

    render() {

        const { navigation } = this.props;

        const from = navigation.getParam('from');

        const location = navigation.getParam('location', null);

        const evaluators_managers = location ? location.evaluation_managers : null;

        //console.log("mamaam", navigation.getParam('evaluation_manager'))
        //For sales_manager click
        const evaluatorsManagers = navigation.getParam('evaluation_manager');
        const evaluators = evaluatorsManagers ? evaluatorsManagers.evaluators : null;

        const parent_role_id = navigation.getParam('parent_role_id');

        const location_id = navigation.getParam('location_id');



        let list;
        //console.log('from:' + from + this.state.show_percentage);
        //console.log("locationlocation", location)
        //console.log("evaluatorsevaluators", evaluatorsManagers)
        if (from == 'location') {
            list = <FlatList
                showsVerticalScrollIndicator={false}
                extraData={this.state}
                data={evaluators_managers}
                renderItem={({ item, index }) => {

                    let mainViewExpandable = <ETVBRExchangeItemExpandable

                        onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                        showPercentage={this.state.show_percentage}
                        evaluation_manager={item} abs={
                            {
                                e: item.evaluation_managers_abs.enquiries,
                                ed: item.evaluation_managers_abs.evaluations,
                                pq: item.evaluation_managers_abs.price_negotiations,
                                r: item.evaluation_managers_abs.retails,

                            }}

                        rel={
                            {
                                e: item.evaluation_manager_rel.enquiries,
                                ed: item.evaluation_manager_rel.evaluations,
                                pq: item.evaluation_manager_rel.price_negotiations,
                                r: item.evaluation_manager_rel.retails,

                            }}

                        info={
                            {
                                dp_url: item.dp_url,
                                name: item.name,
                                user_id: item.id,
                                location_id: location_id,
                                user_role_id: item.user_role_id,
                            }
                        }


                    ></ETVBRExchangeItemExpandable>;

                    if (index == evaluators_managers.length - 1) {
                        return (<View>
                            {mainViewExpandable}

                            <ETVBRExchangeResultItem
                                onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                                showPercentage={this.state.show_percentage}
                                abs={
                                    {
                                        e: location.location_abs.enquiries,
                                        ed: location.location_abs.evaluations,
                                        pq: location.location_abs.price_negotiations,
                                        r: location.location_abs.retails,
                                    }
                                }
                                rel={
                                    {
                                        e: location.location_rel.enquiries,
                                        ed: location.location_rel.evaluations,
                                        pq: location.location_rel.price_negotiations,
                                        r: location.location_rel.retails,

                                    }}
                                info={
                                    {
                                        user_id: '',
                                        name: location.name + '\'s Data',
                                        location_id: location_id,
                                        user_role_id: location.role_id,
                                    }
                                }

                            ></ETVBRExchangeResultItem>

                        </View>);
                    }
                    else {
                        return (mainViewExpandable);
                    }


                }}
                keyExtractor={(item, index) => index + ''}
            />
        } else if (from == 'evaluation_manager') {

            list =
                <FlatList
                    showsVerticalScrollIndicator={false}
                    extraData={this.state}
                    data={evaluators}
                    renderItem={({ item, index }) => {

                        let mainView = <TouchableOpacity onPress={this._showDetailsOnEvaluationManager.bind(this, this.props.navigation, item.info.name + '\'s Team', item, 'sales_manager', location_id)}>
                            <ETVBRExchangeItemChild
                                onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                                showPercentage={this.state.show_percentage}
                                abs={
                                    {
                                        e: item.abs.enquiries,
                                        ed: item.abs.evaluations,
                                        pq: item.abs.price_negotiations,
                                        r: item.abs.retails,
                                    }
                                }

                                rel={
                                    {
                                        e: item.rel.enquiries,
                                        ed: item.rel.evaluations,
                                        pq: item.rel.price_negotiations,
                                        r: item.rel.retails,
                                    }
                                }


                                info={
                                    {
                                        dp_url: item.info.dp_url,
                                        name: item.info.name,
                                        user_id: item.info.id,
                                        location_id: location_id,
                                        user_role_id: item.info.user_role_id,
                                    }
                                }


                            ></ETVBRExchangeItemChild>
                        </TouchableOpacity>;

                        if (index == evaluators.length - 1) {
                            return (<View>
                                {mainView}

                                <ETVBRExchangeResultItem
                                    showPercentage={this.state.show_percentage}
                                    onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                                    abs={
                                        {
                                            e: evaluatorsManagers.evaluation_managers_abs.enquiries,
                                            ed: evaluatorsManagers.evaluation_managers_abs.evaluations,
                                            pq: evaluatorsManagers.evaluation_managers_abs.price_negotiations,
                                            r: evaluatorsManagers.evaluation_managers_abs.retails,
                                        }
                                    }

                                    rel={
                                        {
                                            e: evaluatorsManagers.evaluation_manager_rel.enquiries,
                                            ed: evaluatorsManagers.evaluation_manager_rel.evaluations,
                                            pq: evaluatorsManagers.evaluation_manager_rel.price_negotiations,
                                            r: evaluatorsManagers.evaluation_manager_rel.retails,
                                        }
                                    }

                                    info={
                                        {
                                            user_id: evaluatorsManagers.id,
                                            name: 'All Evaluators',
                                            location_id: location_id,
                                            user_role_id: parent_role_id,
                                        }
                                    }

                                ></ETVBRExchangeResultItem>

                            </View>);
                        }
                        else {
                            return (mainView);
                        }


                    }}
                    keyExtractor={(item, index) => index + ''}
                />
        }

        return (
            <View style={{
                flex: 1,
                paddingTop: (Platform.OS) === 'ios' ? 20 : 10,
                backgroundColor: '#fff'
            }} >
                <Toast
                    ref="toast"
                    position='top'
                    opacity={0.8}
                />

                <View style={{
                    flex: 1,
                    paddingBottom: 10, paddingLeft: 10, paddingRight: 10,
                }} >
                    <View style={{
                        flexDirection: 'row',
                        height: 30,
                    }}>
                        <View style={{ flex: 1 / 2, height: 30, flexDirection: 'row', justifyContent: 'flex-start' }}>
                            <View style={{ backgroundColor: '#F4F4F4', borderWidth: 1, borderColor: '#D0D8E4', borderRadius: 6, justifyContent: 'center' }}>
                                <Text style={{ fontSize: 14, padding: 4, textAlign: 'center', color: '#1B143C' }}>{date}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', height: 30, flex: 1 / 2, justifyContent: 'flex-end', }} >
                            <PercentageSwitch
                                value={this.state.show_percentage}
                                onValueChange={(value) => {
                                    this.setState({ show_percentage: value });
                                    // if (value) {
                                    //     CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrPercentage });
                                    // }
                                }}
                            />
                        </View>

                    </View>
                    <ETVBRHeaderExchange showPercentage={this.state.show_percentage} />
                    {list}
                </View>
            </View>
        );
    }
}
