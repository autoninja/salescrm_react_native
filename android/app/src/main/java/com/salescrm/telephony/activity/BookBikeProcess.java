package com.salescrm.telephony.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.dbOperation.TasksDbOperation;
import com.salescrm.telephony.interfaces.DialogCrm;
import com.salescrm.telephony.model.BookBikeModel;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.BookingConfirmResponse;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class BookBikeProcess {
    ProgressDialog progressDialog;
    Realm realm;
    Preferences pref;
    String leadId, scheduledActivityId, leadLastUpdated;
    Context context;
    Activity activity;
    boolean fromC360;

    public void show(Activity activity, Context context, String leadId, String scheduledActivityId, boolean fromC360) {
        this.activity = activity;
        this.context = context;
        progressDialog = new ProgressDialog(context);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        Preferences.load(context);

        this.leadId = leadId;
        this.scheduledActivityId = scheduledActivityId;
        this.leadLastUpdated = getLeadLastUpdated();
        this.fromC360 = fromC360;
        startBookingProcess();
    }

    private String getLeadLastUpdated() {
        SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Util.getInt(leadId))
                .findAllSorted("leadLastUpdated", Sort.DESCENDING).first();
        if (salesCRMRealmTable != null) {
            return salesCRMRealmTable.getLeadLastUpdated();
        }
        return null;
    }

    private void startBookingProcess() {
        new DialogCrm(context, new DialogCrm.DialogCrmListener() {
            @Override
            public void onDialogCrmYesClick() {
                bookBike();
            }

            private void bookBike() {
                progressDialog.setMessage("Please wait..");
                progressDialog.show();
                BookBikeModel bookBikeModel = new BookBikeModel();
                bookBikeModel.setLead_id(leadId);
                bookBikeModel.setLead_last_updated(leadLastUpdated);
                FormSubmissionInputData formSubmissionInputData = new FormSubmissionInputData();
                formSubmissionInputData.setLead_id(leadId);
                formSubmissionInputData.setLead_last_updated(leadLastUpdated);
                formSubmissionInputData.setScheduled_activity_id(scheduledActivityId);
                //bookBikeModel.setForm_object(formSubmissionInputData);
                ApiUtil.GetRestApiWithHeader(Preferences.getAccessToken()).bookBike(bookBikeModel, new Callback<BookingConfirmResponse>() {
                    @Override
                    public void success(BookingConfirmResponse o, Response response) {
                        Util.updateHeaders(response.getHeaders());
                        if (!o.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                            handleFailure();
                        } else {
                            if (o.getResult() != null) {
                                TasksDbOperation.getInstance().updateLeadLastUpdated(realm, leadId, o.getResult().getLead_last_updated());
                                handleSuccess();
                                Util.showToast(context, "Form has been submitted", Toast.LENGTH_LONG);
                            } else {
                                handleFailure();

                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        handleFailure();
                    }
                });

            }
        }).show();
    }

    private void handleFailure() {
        Util.showToast(context, "Form will be refreshed, Try again", Toast.LENGTH_SHORT);
        progressDialog.dismiss();
        openC360();
    }

    private void handleSuccess() {
        realm.beginTransaction();
        try {
            SalesCRMRealmTable data = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", Util.getInt(scheduledActivityId)).findFirst();
            if (data != null) {
                data.setDone(true);
            }
        } catch (Exception e) {
        }

        realm.commitTransaction();
        progressDialog.dismiss();
        openC360();
    }

    private void openC360() {
        if (fromC360) {
            new Handler().post(new Runnable() {

                @Override
                public void run() {
                    Intent intent = activity.getIntent();
                    activity.overridePendingTransition(0, 0);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    activity.finish();
                    activity.overridePendingTransition(0, 0);
                    activity.startActivity(intent);
                }
            });
        } else {
            Intent in = new Intent(context, C360Activity.class);
            context.startActivity(in);
        }

    }

}
