package com.salescrm.telephony.fragments.gamification;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.gamification.DSEPointsResponseModel;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.services.service_handlers.GamificationServiceHandler;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LeaderBoardDsePointsFragment extends Fragment {
    private RelativeLayout relLoading;
    private Preferences pref;
    ConnectionDetectorService connectionDetectorService;
    private TextView section_zero;
    private TextView tv_retail_points,tv_td_points,tv_visit_points,tv_zpd_points,tv_bonus_points,total_points, tv_exch_points, tv_fin_points;
    private TextView tv_retail_total_points,tv_td_total_points, tv_visit_total_points, tv_zpd_total_points,tv_bonus_total_points, tv_fin_total_points, tv_exch_total_points;
    private static CallBack callBack;
    private static String userId;
    private ScrollView scrollMain;
    private TextView tvError;


    public LeaderBoardDsePointsFragment() {
    }
    public static LeaderBoardDsePointsFragment newInstance(String id, CallBack listener) {
        LeaderBoardDsePointsFragment fragment = new LeaderBoardDsePointsFragment();
        Bundle args = new Bundle();
        callBack = listener;
        userId = id;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_leader_board_dse_points, container, false);

        pref = Preferences.getInstance();
        pref.load(getActivity());
       // section_zero = view.findViewById(R.id.section_zero);
        tv_retail_points = view.findViewById(R.id.tv_points) ;
        tv_td_points = view.findViewById(R.id.tv_td_points) ;
        tv_visit_points = view.findViewById(R.id.tv_visit_points) ;
        tv_zpd_points = view.findViewById(R.id.tv_zpd_points) ;
        tv_bonus_points = view.findViewById(R.id.tv_bonus_points) ;
        tv_retail_total_points =  view.findViewById(R.id.tv_total_points) ;
        tv_td_total_points =view.findViewById(R.id.tv_td_total_points) ;
        tv_visit_total_points =  view.findViewById(R.id.tv_visit_total_points) ;
        tv_zpd_total_points = view.findViewById(R.id.tv_zpd_total_points) ;
        tv_bonus_total_points = view.findViewById(R.id.tv_bonus_total_points) ;
        total_points = view.findViewById(R.id.total_pts) ;
        relLoading = view.findViewById(R.id.rel_loading_frame);
        scrollMain = view.findViewById(R.id.scroll_main);
        tvError = view.findViewById(R.id.tv_error);
        tv_exch_points = view.findViewById(R.id.tv_exchange_points);
        tv_fin_points = view.findViewById(R.id.tv_finance_points);
        tv_exch_total_points = view.findViewById(R.id.tv_exchange_total_points);
        tv_fin_total_points = view.findViewById(R.id.tv_finance_total_points);
        connectionDetectorService = new ConnectionDetectorService(getContext());

        tvError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi();
            }
        });
        callApi();
        return view;
    }

    private void callApi() {

        scrollMain.setVisibility(View.GONE);
        relLoading.setVisibility(View.VISIBLE);
        tvError.setVisibility(View.GONE);

        if (connectionDetectorService.isConnectingToInternet()) {
            relLoading.setVisibility(View.VISIBLE);
            GamificationServiceHandler.getInstance(getContext()).fetchDsePoints(pref.getAccessToken(), Integer.parseInt(userId), Util.getMonthYear(pref.getSelectedGameDate()),new CallBack() {
                @Override
                public void onCallBack() {

                }

                @Override
                public void onCallBack(Object data) {
                    DSEPointsResponseModel dsePointsResponseModel = (DSEPointsResponseModel) data;
                    callBack.onCallBack(dsePointsResponseModel.getResult());
                    setDsePointsDetails(dsePointsResponseModel.getResult());
                }

                @Override
                public void onError(String error) {
                    scrollMain.setVisibility(View.GONE);
                    relLoading.setVisibility(View.GONE);
                    tvError.setVisibility(View.VISIBLE);
                    tvError.setText(error);
                    System.out.println("Error Happened:"+error);

                }
            });
        }
        else {
            tvError.setVisibility(View.VISIBLE);
            tvError.setText("No Internet Connections");
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setDsePointsDetails(DSEPointsResponseModel.Result dsePointsResult){
        tvError.setVisibility(View.GONE);
        for(int i = 0 ; i < dsePointsResult.getCategory_points().size();i++){
            DSEPointsResponseModel.Category_points category_points = dsePointsResult.getCategory_points().get(i);

            if(category_points!=null) {
                if(category_points.getId()!=null) {
                    int id = Integer.parseInt(category_points.getId());
                    switch (id) {
                        case WSConstants.DSEPointsCategory.RETAIL:
                           /* tv_retail_points.setText("0");
                            tv_retail_total_points.setText("0");*/
                            for(int x = 0; x<category_points.getPoints().size(); x++) {
                                DSEPointsResponseModel.Category_points.Points point  = category_points.getPoints().get(x);
                                tv_retail_points.append(point.getValue()+" X "+point.getCount()+(x!=category_points.getPoints().size()-1?"\n":""));
                                tv_retail_total_points.append(point.getTotal()+(x!=category_points.getPoints().size()-1?"\n":""));
                            }
                            break;
                        case WSConstants.DSEPointsCategory.TESTDRIVE:
                            for(int x = 0; x<category_points.getPoints().size(); x++) {
                                DSEPointsResponseModel.Category_points.Points point  = category_points.getPoints().get(x);
                                tv_td_points.append(point.getValue()+" X "+point.getCount()+(x!=category_points.getPoints().size()-1?"\n":""));
                                tv_td_total_points.append(point.getTotal()+(x!=category_points.getPoints().size()-1?"\n":""));
                            }

                            break;
                        case WSConstants.DSEPointsCategory.VISIT:

                            for(int x = 0; x<category_points.getPoints().size(); x++) {
                                DSEPointsResponseModel.Category_points.Points point  = category_points.getPoints().get(x);
                                tv_visit_points.append(point.getValue()+" X "+point.getCount()+(x!=category_points.getPoints().size()-1?"\n":""));
                                tv_visit_total_points.append(point.getTotal()+(x!=category_points.getPoints().size()-1?"\n":""));
                            }
                            break;
                        case WSConstants.DSEPointsCategory.ZERO_PENDING:
                            for(int x = 0; x<category_points.getPoints().size(); x++) {
                                DSEPointsResponseModel.Category_points.Points point  = category_points.getPoints().get(x);
                                tv_zpd_points.append(point.getValue()+" X "+point.getCount()+(x!=category_points.getPoints().size()-1?"\n":""));
                                tv_zpd_total_points.append(point.getTotal()+(x!=category_points.getPoints().size()-1?"\n":""));
                            }
                            break;
                        case WSConstants.DSEPointsCategory.EXCHANGE:
                            for(int x = 0; x<category_points.getPoints().size(); x++) {
                                DSEPointsResponseModel.Category_points.Points point  = category_points.getPoints().get(x);
                                tv_exch_points.append(point.getValue()+" X "+point.getCount()+(x!=category_points.getPoints().size()-1?"\n":""));
                                tv_exch_total_points.append(point.getTotal()+(x!=category_points.getPoints().size()-1?"\n":""));
                            }
                            break;
                        case WSConstants.DSEPointsCategory.FINANCE:
                            for(int x = 0; x<category_points.getPoints().size(); x++) {
                                DSEPointsResponseModel.Category_points.Points point  = category_points.getPoints().get(x);
                                tv_fin_points.append(point.getValue()+" X "+point.getCount()+(x!=category_points.getPoints().size()-1?"\n":""));
                                tv_fin_total_points.append(point.getTotal()+(x!=category_points.getPoints().size()-1?"\n":""));
                            }
                            break;
                        case WSConstants.DSEPointsCategory.BONUS:
                            tv_bonus_total_points.append(category_points.getSum());

                            break;

                    }
                }

            }





        }

        total_points.setText(""+dsePointsResult.getTotal());
        relLoading.setVisibility(View.GONE);
        scrollMain.setVisibility(View.VISIBLE);


    }
}
