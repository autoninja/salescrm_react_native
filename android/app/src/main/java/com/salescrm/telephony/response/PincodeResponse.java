package com.salescrm.telephony.response;

import java.util.List;

public class PincodeResponse {

    private Result result;
    private Error error;
    private String message;
    private String statusCode;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public class Result {
        private List<PinData> pindata;

        public List<PinData> getPindata() {
            return pindata;
        }

        public void setPindata(List<PinData> pindata) {
            this.pindata = pindata;
        }

        public class PinData {
            private String id;
            private String locality;
            private String pincode;
            private String divisionname;
            private String regionname;
            private String taluk;
            private String districtname;
            private String statename;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getLocality() {
                return locality;
            }

            public void setLocality(String locality) {
                this.locality = locality;
            }

            public String getPincode() {
                return pincode;
            }

            public void setPincode(String pincode) {
                this.pincode = pincode;
            }

            public String getDivisionname() {
                return divisionname;
            }

            public void setDivisionname(String divisionname) {
                this.divisionname = divisionname;
            }

            public String getRegionname() {
                return regionname;
            }

            public void setRegionname(String regionname) {
                this.regionname = regionname;
            }

            public String getTaluk() {
                return taluk;
            }

            public void setTaluk(String taluk) {
                this.taluk = taluk;
            }

            public String getDistrictname() {
                return districtname;
            }

            public void setDistrictname(String districtname) {
                this.districtname = districtname;
            }

            public String getStatename() {
                return statename;
            }

            public void setStatename(String statename) {
                this.statename = statename;
            }
        }
    }

}
