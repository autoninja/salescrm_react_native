package com.salescrm.telephony.response;

/**
 * Created by Ravindra on 20-Jun-17.
 */

public class ImageUploadResponse {

    private String statusCode;

    private String message;

    private Result result;

    private String[] error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public String[] getError ()
    {
        return error;
    }

    public void setError (String[] error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private String dp_url;

        public String getDp_url ()
        {
            return dp_url;
        }

        public void setDp_url (String dp_url)
        {
            this.dp_url = dp_url;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [dp_url = "+dp_url+"]";
        }
    }
}
