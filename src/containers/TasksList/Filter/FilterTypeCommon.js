import React, { Component } from "react";
import { View, FlatList } from 'react-native';
import { CheckBox } from 'react-native-elements'
export default class FilterTypeCommon extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let { data } = this.props;
        return (
            <FlatList
                extraData={this.props}
                data={data}
                renderItem={({ item, index }) => (
                    <CheckBox
                        title={item.title}
                        checkedColor='#007fff'
                        iconRight
                        right
                        textStyle={{ flex: 1, textAlign: 'left', fontWeight: 'normal' }}
                        containerStyle={{
                            backgroundColor: '#fff',
                            borderWidth: 0, margin: 4
                        }}
                        checked={item.selected}
                        onPress={() => {
                            this.props.onPress(index)
                        }}
                    />
                )}
                keyExtractor={item => item.id}
            />
        )
    }
}