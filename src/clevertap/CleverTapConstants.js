export const eventTeamDashboard = {
  event: "Team Dashboard",
  keyType: {
    keyName : 'Dashboard Type',
    valuesType: {
    etvbrLocation: 'ETVBR Locations',
    etvbrTeams: 'ETVBR Teams',
    etvbrCars :'ETVBR Cars',
    etvbrLeadSource :'ETVBR Lead Source',
    etvbrPercentage:'ETVBR Percentage',
    carsPercentage:'Cars Percentage',
    etvbrDate:'ETVBR Date',
    liveEFLocation: 'EXCHFIN Locations',
    liveEFTeams: 'EXCHFIN Teams',
    liveEFCars :'EXCHFIN Cars',
    liveEFLeadSource :'EXCHFIN Lead Source',
    liveEFPercentage:'EXCHFIN Percentage',
    lostDropAnalysis:'Lost Drop Analysis',
    etvbrClick : 'ETVBR Click',
    todaysTasks: 'Today\'s Task',
    uncalledTasks : 'Uncalled Tasks Click',
    event_dashboard : 'Events'
    }
  },
}

export const eventETVBRFilters = {
  event: "ETVBR Filters",
  keyType: {
    keyName : 'Type',
  },
}

export const eventAddOrUpdateEvent = {
  event: "Events",
  keyType: {
    keyName : 'Type',
    valuesType: {
    addEvent: 'Add Event',
    editEvent: 'Edit Event',
    }
  },
}

export const eventLeaderBoard = {
  event: 'Leaderboard',
  keyType : {
    keyName : 'Leaderboard Type',
    valuesType: {
    TEAM_LEADERBOARD : "Team Leaderboard",
    TEAM_BREAK_UP_OWN : "Team Break Up Own",
    TEAM_BREAK_UP_OTHERS : "Team Break Up Others",
    CONSULTANT_LEADERBOARD_OWN : "Consultant Leaderboard Own",
    CONSULTANT_LEADERBOARD_OTHERS : "Consultant Leaderboard Others",
    CONSULTANT_POINT_BREAK_UP_OWN : "Consultant Point Break Up Own",
    CONSULTANT_POINT_BREAK_UP_OTHERS : "Consultant Point Break Up Others",
    BADGE_OWN : "Badge Own",
    BADGE_OTHERS : "Badge Others",
    HELP_OWN : "Help Own",
    HELP_OTHERS : "Help Others",
    WALL_OF_FAME : "Wall Of Fame",
    CLAIM_BONUS_RETAIL : "Claim Bonus Retail",
    CLAIM_BONUS_RETAIL_TARGET : "Claim Bonus Retail Target",
    CLAIM_BONUS_TEAM_ZERO_PENDING : "Claim Bonus Team Zero Pending",
    BADGES_EARNED : "Badges Earned",
    PREVIOUS_MONTH : "Previous Month"
    }
  },
}

export const fetchVehicleDetails = {
  event: "Fetch Vehicle Details",
}