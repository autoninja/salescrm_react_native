package com.salescrm.telephony.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.EnqSourceCategoryDB;
import com.salescrm.telephony.db.EnqSubInnerSourceDB;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.events.EventsCategoryDb;
import com.salescrm.telephony.preferences.Preferences;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class RNAddEventsActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {
    private ReactInstanceManager mReactInstanceManager;
    private ReactRootView mReactRootView;
    private Preferences pref;
    private Realm realm;
    private String eventId, isEventActive;
    private boolean isItEditEvent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(this);

        setContentView(R.layout.activity_rn_add_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.rn_add_event_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        eventId = getIntent().getStringExtra("eventId") != null ? getIntent().getStringExtra("eventId"):null;
        isEventActive = getIntent().getStringExtra("isEventActive") != null ? getIntent().getStringExtra("isEventActive") : null;
        isItEditEvent = getIntent().getBooleanExtra("isItEditEvent", false);
        if(isItEditEvent){
            ((TextView) toolbar.findViewById(R.id.tvTitle)).setText("Edit Event");
        }else {
            ((TextView) toolbar.findViewById(R.id.tvTitle)).setText("Add Event");
        }

        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();

        Bundle initialProps = new Bundle();
        initialProps.putBoolean("fromAndroid", true);
        initialProps.putString("token", pref.getAccessToken());
        initialProps.putString("module", "ADD_EVENTS");
        initialProps.putString("allLocations", new Gson().toJson(getAllLocations()));
        initialProps.putString("enquirySourceCategories", new Gson().toJson(getEnquirySourceCategories()));
        initialProps.putString("eventCategories", new Gson().toJson(getAllCategories()));
        initialProps.putString("eventId", eventId);
        initialProps.putString("isEventActive", isEventActive);
        initialProps.putBoolean("isItEditEvent", isItEditEvent);
        mReactRootView = findViewById(R.id.react_root_view_add_event);
        mReactRootView.startReactApplication(
                mReactInstanceManager,
                "NinjaCRMSales",
                initialProps
        );

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals("CLOSE_RNAddEventActivity")) {
                finish();
                overridePendingTransition(0,0);
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }
    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }

        try {
            registerReceiver(broadcastReceiver, new IntentFilter("CLOSE_RNAddEventActivity"));

        } catch (Exception e) {

        }
    }

    private Item[] getAllLocations() {
        RealmResults<UserLocationsDB> data =
                realm.where(UserLocationsDB.class).findAll();
        Item[] userLocations = new Item[data != null ? data.size() : 0];
        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i) != null) {
                    userLocations[i] = new Item(data.get(i).getLocation_id(), data.get(i).getName());
                }
            }
        }

        return userLocations;

    }

    private ItemCategory[] getAllCategories() {
        RealmResults<EventsCategoryDb> data =
                realm.where(EventsCategoryDb.class).findAll();
        ItemCategory[] eventCategory = new ItemCategory[data != null ? data.size() : 0];
        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i) != null) {
                    eventCategory[i] = new ItemCategory(data.get(i).getId(), data.get(i).getName());
                }
            }
        }

        return eventCategory;

    }

    private List<EnquiryCategory> getEnquirySourceCategories() {
        List<EnquiryCategory> enquirySourcesCategories = new ArrayList<>();
        RealmResults<EnqSourceCategoryDB> realmList = realm.where(EnqSourceCategoryDB.class).findAll();
        for (EnqSourceCategoryDB enqSourceCategoryDB : realmList) {
            List<Item> subEnquiries = new ArrayList<>();
            for (EnqSubInnerSourceDB subEnquiry : enqSourceCategoryDB.getSubLeadSources()) {
                subEnquiries.add(new Item(subEnquiry.getId()+"", subEnquiry.getName()));
            }
            enquirySourcesCategories.add(new EnquiryCategory(enqSourceCategoryDB.getId(), enqSourceCategoryDB.getName(), subEnquiries));

        }
        return enquirySourcesCategories;
    }

    class Item {
        String id;
        String name;

        public Item(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    class ItemCategory {
        String id;
        String name;

        public ItemCategory(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    class EnquiryCategory {
        int id;
        String name;
        List<Item> subEnquirySource;

        public EnquiryCategory(int id, String name, List<Item> subEnquirySource) {
            this.id = id;
            this.name = name;
            this.subEnquirySource = subEnquirySource;
        }
    }
}
