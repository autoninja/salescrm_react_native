package com.salescrm.telephony.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.DoneActionPlanRealmAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.dataitem.SectionModel;
import com.salescrm.telephony.db.ActivityDetails;
import com.salescrm.telephony.db.CustomerDetails;
import com.salescrm.telephony.db.LeadDetails;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.interfaces.ActionPlanHeaderUpdateListener;
import com.salescrm.telephony.interfaces.AdapterCommunication;
import com.salescrm.telephony.model.FilterDataChangedListener;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.model.TabLayoutText;
import com.salescrm.telephony.model.ViewPagerSwiped;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.ActionPlanResponse;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by bharath on 20/12/16.
 */

public class DoneActionPlanFragment extends Fragment implements AdapterCommunication {


    SwipeRefreshLayout swipeRefreshLayout;
    private String TAG = "DoneActionPlan";
    private RecyclerView mRecyclerView;
    List<SectionModel> sections;
    private List<ActionPlanResponse.Result.Data> actionPlanResponseData;
    private Preferences pref = null;
    private RelativeLayout relAlert;
    private Button btRefresh;
    private TextView tvAlert;
    private Realm realm;
    private CustomerDetails customerDetails;
    private LeadDetails leadDetails;
    private ActivityDetails activityDetails;
    private SalesCRMRealmTable salesCRMRealmTable;
    private int visibleItemCount, totalItemCount, pastVisiblesItems;
    private boolean loading;
    private int total;
    private int totalPage;
    private int currentPage = 1;
    private int prevPage;
    private int pageLimit = 10;

    private int todayStartIndex = -1;
    private int tomorrowStartIndex = -1;
    private int tomorrowStartIndex1 = -1;
    private int tomorrowStartIndex2 = -1;
    private int yesterdayStartIndex = -1;
    private int yesterdayStartIndex1 = -1;
    private int yesterdayStartIndex2 = -1;

    private ProgressBar progressBottomLayout;
    private LinearLayoutManager mLayoutManager;
    private int apiCallCount = 0;
    private FilterSelectionHolder filterSelectionHolder;
    private DoneActionPlanRealmAdapter adapter;
    public static ActionPlanHeaderUpdateListener actionPlanUpdateListener;
    private boolean isCreatedTemp = false;
    public Integer dseId;
    public static DoneActionPlanFragment newInstance(ActionPlanFragment context) {
        try {
            actionPlanUpdateListener = (ActionPlanHeaderUpdateListener)context;
        }
        catch (ClassCastException e){
            e.printStackTrace();
        }
        return new DoneActionPlanFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pending_action_plan, container, false);
        pref = Preferences.getInstance();
        filterSelectionHolder = FilterSelectionHolder.getInstance();
        pref.load(getActivity());
        actionPlanResponseData = new ArrayList<ActionPlanResponse.Result.Data>();
        progressBottomLayout = (ProgressBar) rootView.findViewById(R.id.progress_bottom_pending_action);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_action_pending);
        relAlert = (RelativeLayout) rootView.findViewById(R.id.frame_alert);
        btRefresh = (Button) rootView.findViewById(R.id.btAlertRefresh);
        tvAlert = (TextView) rootView.findViewById(R.id.tv_alert);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.progressRecycler);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        realm = Realm.getDefaultInstance();
        sections = new ArrayList<SectionModel>();

        dseId = pref.getCurrentDseId();
        //
        if(pref.getCurrentDseId()!=null) {
            if (pref.getCurrentDseId().intValue() != pref.getAppUserId().intValue()) {
                isCreatedTemp = true;
            }
        }

        setupRealmAdapter();

        realm.where(SalesCRMRealmTable.class).findAll().addChangeListener(new RealmChangeListener<RealmResults<SalesCRMRealmTable>>() {
            @Override
            public void onChange(RealmResults<SalesCRMRealmTable> element) {
                SalesCRMApplication.getBus().post(new TabLayoutText(4, "Done " + realm.where(SalesCRMRealmTable.class)
                        .equalTo("isCreatedTemporary", isCreatedTemp)
                        .equalTo("isDone", true).findAll().size(), realm.where(SalesCRMRealmTable.class).equalTo("isCreatedTemporary", false).findAll().size(), 0));
                //  System.out.println(Tag + ":Data change called:" + realm.where(SalesCRMRealmTable.class).findAll().size());
                sortActivityDetails();
                adapter.notifyDataSetChanged();
            }
        });

        /*setSwipeRefreshView(true);
        loading = true;*/
        loading = true;
        btRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setSwipeRefreshView(false);
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                setSwipeRefreshView(false);
            }
        });
        System.out.println(TAG + "OnCreate");
        return rootView;
    }

    private void setupRealmAdapter() {
        //Setting up adapter.
        OrderedRealmCollection<SalesCRMRealmTable> realmResult = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .equalTo("isDone", true)
                .equalTo("dseId", dseId)
                .findAllSorted("activityScheduleDate", Sort.ASCENDING);
        adapter = new DoneActionPlanRealmAdapter(getContext(),
                realmResult, true, sections);
        mRecyclerView.setAdapter(adapter);

        updateViews();
    }

    public void sortActivityDetails() {

        OrderedRealmCollection<SalesCRMRealmTable> list = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .equalTo("isDone", true)
                .equalTo("dseId", dseId)
                .findAllSorted("activityScheduleDate", Sort.ASCENDING);
        sections.clear();
        sections.addAll(Util.createDateSections(list, WSConstants.DONE_TAB).getSections());
    }


    private void setupRealmAdapterWithFilter() {
        //Setting up adapter with filter.
        RealmQuery<SalesCRMRealmTable> realmQuery = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .equalTo("isDone", true)
                .equalTo("dseId", dseId);

        for (int i = 0; i < filterSelectionHolder.getAllMapData().size(); i++) {
            int insideKey = filterSelectionHolder.getAllMapData().keyAt(i);
            String key = Util.getAdvanceFilterKey(insideKey);
            System.out.println("Key::" + key);
            if (!TextUtils.isEmpty(key)) {

                //Special case
                if (key.equalsIgnoreCase("activityId")) {
                    List<String> activities = new ArrayList<>();
                    for (int j = 0; j < filterSelectionHolder.getMapData(insideKey).size(); j++) {
                        Collections.addAll(activities, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j)).split(","));
                    }

                    for (int activity = 0; activity < activities.size(); activity++) {
                        if (activity == 0) {
                            realmQuery = realmQuery.beginGroup();
                        }
                        realmQuery = realmQuery.equalTo(key, Util.getInt(activities.get(activity)));
                        if (activity != activities.size() - 1) {
                            realmQuery = realmQuery.or();
                        } else {
                            realmQuery = realmQuery.endGroup();
                        }
                    }


                } else {

                    for (int j = 0; j < filterSelectionHolder.getMapData(insideKey).size(); j++) {

                        System.out.println("value at" + j + ":" + filterSelectionHolder.getMapData(insideKey).keyAt(j));

                        if (key.equalsIgnoreCase("date")) {
                            int dateKey = filterSelectionHolder.getMapData(insideKey).keyAt(j);
                            Date dateVal = Util.getCalender(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j))).getTime();
                            System.out.println("Key:" + dateKey);
                            System.out.println("Date Val:" + dateVal.toString());
                            if (dateKey == 0) {
                                realmQuery = realmQuery.greaterThanOrEqualTo("leadExpectedClosingDate", dateVal);
                            } else if (dateKey == 1) {
                                realmQuery = realmQuery.lessThanOrEqualTo("leadExpectedClosingDate", dateVal);

                            } else if (dateKey == 2) {
                                realmQuery = realmQuery.greaterThanOrEqualTo("activityScheduleDate", dateVal);

                            } else if (dateKey == 3) {
                                realmQuery = realmQuery.lessThanOrEqualTo("activityScheduleDate", dateVal);

                            }
                            continue;
                        }
                        if (key.equalsIgnoreCase("leadAge")) {
                            int valFrom = Integer.parseInt(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j)));
                            int valTo = Integer.parseInt(filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j + 1)));
                            realmQuery = realmQuery.between(key, valFrom, valTo);
                            break;
                        }


                        if (j == 0) {
                            realmQuery = realmQuery.beginGroup();
                        }

                        if(key.equalsIgnoreCase("leadCarModelId")){
                            realmQuery = realmQuery.equalTo(key, filterSelectionHolder.getMapData(insideKey).keyAt(j)+"");
                        }
                        else {
                            realmQuery = realmQuery.contains(key, filterSelectionHolder.getMapData(insideKey).get(filterSelectionHolder.getMapData(insideKey).keyAt(j)));
                        }

                        if (j != filterSelectionHolder.getMapData(insideKey).size() - 1) {
                            realmQuery = realmQuery.or();
                        } else {
                            realmQuery = realmQuery.endGroup();
                        }

                    }
                }

            }

        }
        OrderedRealmCollection<SalesCRMRealmTable> realmResult = realmQuery.findAllSorted("activityScheduleDate", Sort.DESCENDING);
        mRecyclerView.setAdapter(new DoneActionPlanRealmAdapter(getContext(),
                realmResult, true, null));


        updateViews();
        if (realmResult.size() == 0) {
            relAlert.setVisibility(View.VISIBLE);
            tvAlert.setText("No task in this filter");
            btRefresh.setVisibility(View.INVISIBLE);
        }
        if(actionPlanUpdateListener!=null) {
            actionPlanUpdateListener.onFilterAppliedUpdateText(2, "Done " + realmQuery.findAll().size());
            //  SalesCRMApplication.getBus().post(new TabLayoutText(2, "Pending " + realmQuery.findAll().size(), realmQuery.findAll().size(), 0));
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
        System.out.println(TAG + "OnResume");
        if (!filterSelectionHolder.isEmpty()) {
            setupRealmAdapterWithFilter();
        } else {
            setupRealmAdapter();
        }

    }
    @Subscribe
    public void onFilterDataChanged(FilterDataChangedListener filterDataChangedListener){
        if(filterDataChangedListener.isUpdateRequired()){
            if (!filterSelectionHolder.isEmpty()) {
                setupRealmAdapterWithFilter();
            } else {
                setupRealmAdapter();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       /* realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                *//*realm.delete(LeadDetails.class);
                realm.delete(CustomerDetails.class);
                realm.delete(ActivityDetails.class);*//*
            }
        });*/
        // Clear out all instances.

    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Override
    public void AllTaskFragmentCallBack(String LeadId) {
        replaceFragment(LeadId);
    }

    @Override
    public void FormActivityFragmentCallBack() {
    }

    @Override
    public void FormActivityRescheduleCallBack() {
    }

    @Override
    public void FormActivityEmailSmaCallBack() {
    }

    private void replaceFragment(String LeadId) {

    }


    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void setSwipeRefreshView(final boolean val) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                // swipeRefreshLayout.setEnabled(val);
                swipeRefreshLayout.setRefreshing(val);
            }
        });
    }


    private void updateViews() {
        int total = realm.where(SalesCRMRealmTable.class).equalTo("isCreatedTemporary", isCreatedTemp).equalTo("isDone", true)
                .equalTo("dseId", dseId)
                .findAll().size();
        SalesCRMApplication.getBus().post(new TabLayoutText(4, "Done " + total, total, 0));
        if (total == 0) {
            relAlert.setVisibility(View.VISIBLE);
            tvAlert.setText("No done task:)");
        } else {
            relAlert.setVisibility(View.GONE);
        }
        sortActivityDetails();
        adapter.notifyDataSetChanged();

    }


    @Subscribe
    public void ActionPlanViewPagerSwiped(ViewPagerSwiped viewPagerSwiped) {

        System.out.println(TAG + "ActionPlanSwiped:From" + viewPagerSwiped.getOldPosition() + " To:" + viewPagerSwiped.getNewPosition());
    }

}
