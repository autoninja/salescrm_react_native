package com.salescrm.telephony.model;

/**
 * Created by bharath on 18/1/18.
 */

public class BookBikeModel {
    private String lead_id;
    private String lead_last_updated;
    private FormSubmissionInputData form_object;

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }

    public FormSubmissionInputData getForm_object() {
        return form_object;
    }

    public void setForm_object(FormSubmissionInputData form_object) {
        this.form_object = form_object;
    }
}
