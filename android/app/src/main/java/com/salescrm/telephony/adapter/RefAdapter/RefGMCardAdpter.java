package com.salescrm.telephony.adapter.RefAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.DetailsOfEtvbr;
import com.salescrm.telephony.activity.EtvbrImgDialog;
import com.salescrm.telephony.activity.EtvbrSmActivity;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.ref_location.RefLocation;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.retExcFin.RefLocationChildFragment;
import com.salescrm.telephony.fragments.retExcFin.RefLocationFragment;
import com.salescrm.telephony.utils.CircleTransform;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.picasso.Picasso;

import io.realm.Realm;

/**
 * Created by prateek on 14/7/17.
 */

public class RefGMCardAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private Realm realm;
    private String userIdForAllSMData;
    private RefLocation locationResult;

    public RefGMCardAdpter(FragmentActivity activity, Realm realm, RefLocation locationResult) {
        this.context = activity;
        //this.resultSMs = locationResult.getSalesManagers();
        this.realm = realm;
        //this.resultEtvbrs = resultEtvbrs;
        this.locationResult = locationResult;

        this.userIdForAllSMData = DbUtils.isUserOperations() ? WSConstants.USER_ROLE_OPERATIONS : WSConstants.USER_ROLE_BRANCH_HEAD;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.ref_sm_card_adapter, parent, false);
        return new RefGMCardAdpter.EtvbrGMCardHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (RefLocationFragment.isLocationAvailable()) {
            if (RefLocationChildFragment.PERCENT) {
                viewCardsGMRel(holder, position);
            } else {
                viewCardsGMAbs(holder, position);
            }
        } else {
            if (RefLocationFragment.PERCENT) {
                viewCardsGMRel(holder, position);
            } else {
                viewCardsGMAbs(holder, position);
            }
        }
    }

    private void viewCardsGMAbs(RecyclerView.ViewHolder holder, final int position) {
        final EtvbrGMCardHolder cardHolder = (EtvbrGMCardHolder) holder;
        //final int position = pos;
        cardHolder.cardView.setVisibility(View.VISIBLE);
        cardHolder.llinearLayout.setVisibility(View.GONE);
        //absolute = results.get(position).getAbs();
        cardHolder.tvTargetExchCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetFinCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetFinOutCard.setVisibility(View.GONE);
        cardHolder.tvTargetPBCard.setVisibility(View.GONE);
        cardHolder.tvTargetLECard.setVisibility(View.GONE);
        cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetRetailCard.setVisibility(View.VISIBLE);
        cardHolder.viewTargetCard.setVisibility(View.VISIBLE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 0);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        if (locationResult.getSalesManagers() != null) {
            if (position < locationResult.getSalesManagers().size()) {

                Paint paint = new Paint();
                paint.setColor(Color.BLUE);
                paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);


                cardHolder.tvRetailCard.setPaintFlags(paint.getFlags());
                cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getRetails());

                cardHolder.tvExchCard.setPaintFlags(paint.getFlags());
                cardHolder.tvExchCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getExchanged());

                cardHolder.tvFinCard.setPaintFlags(paint.getFlags());
                cardHolder.tvFinCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getIn_house_financed());

                cardHolder.tvFinOutCard.setPaintFlags(paint.getFlags());
                cardHolder.tvFinOutCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getOut_house_financed());

                cardHolder.tvPBCard.setPaintFlags(paint.getFlags());
                cardHolder.tvPBCard.setText(getReadableData(locationResult.getSalesManagers().get(position).getInfo().getAbs().getPending_bookings()));

                cardHolder.tvLECard.setPaintFlags(paint.getFlags());
                cardHolder.tvLECard.setText(getReadableData(locationResult.getSalesManagers().get(position).getInfo().getAbs().getLive_enquiries()));

                cardHolder.tvTargetRetailCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getTargets().getRetails());
                cardHolder.tvTargetExchCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getTargets().getExchanged());
                cardHolder.tvTargetFinCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getTargets().getIn_house_financed());
                cardHolder.tvTargetFinOutCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getTargets().getOut_house_financed());
                cardHolder.tvTargetPBCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getTargets().getPending_bookings());
                cardHolder.tvTargetLECard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getTargets().getLive_enquiries());

                cardHolder.tvTargetTitleCard.setVisibility(View.GONE);

                if (locationResult.getSalesManagers().get(position).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(position).getInfo().getDpUrl().equalsIgnoreCase("")) {
                    cardHolder.tvNameCard.setVisibility(View.GONE);
                    cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                    cardHolder.refFooter.setVisibility(View.GONE);
                    cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                    String url = locationResult.getSalesManagers().get(position).getInfo().getDpUrl();
                    Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUserCard);
                } else if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                    cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                    cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                    cardHolder.refFooter.setVisibility(View.GONE);
                    cardHolder.imgUserCard.setVisibility(View.GONE);
                    cardHolder.tvNameCard.setText(locationResult.getSalesManagers().get(position).getInfo().getName().charAt(0) + "");
                } else {
                    cardHolder.imgUserCard.setVisibility(View.GONE);
                    cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                    cardHolder.tvNameCard.setText("N");
                }

                cardHolder.imgUserCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        Intent intent = new Intent(context, EtvbrImgDialog.class);
                        Bundle bundle = new Bundle();
                        if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                            bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(position).getInfo().getDpUrl());
                            bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(position).getInfo().getName());
                            intent.putExtras(bundle);
                            context.startActivity(intent);
                        }
                        return true;
                    }
                });

                cardHolder.tvNameCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        Intent intent = new Intent(context, EtvbrImgDialog.class);
                        Bundle bundle = new Bundle();
                        if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                            bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(position).getInfo().getDpUrl());
                            bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(position).getInfo().getName());
                            intent.putExtras(bundle);
                            context.startActivity(intent);
                        }
                        return true;
                    }
                });
                cardHolder.llInner.setBackgroundColor(Color.WHITE);
                cardHolder.tvRetailCard.setTextColor(Color.BLACK);
                cardHolder.tvUserTotalCard.setTextColor(Color.BLACK);
                cardHolder.tvExchCard.setTextColor(Color.BLACK);
                cardHolder.tvFinCard.setTextColor(Color.BLACK);
                cardHolder.tvFinOutCard.setTextColor(Color.BLACK);
                cardHolder.tvPBCard.setTextColor(Color.parseColor("#7ED321"));
                cardHolder.tvLECard.setTextColor(Color.parseColor("#7ED321"));


                cardHolder.tvRetailCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvUserTotalCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvExchCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvFinCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvFinOutCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvPBCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvLECard.setTypeface(null, Typeface.NORMAL);

                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
                cardHolder.viewFinCard.setVisibility(View.VISIBLE);
                cardHolder.viewFinOutCard.setVisibility(View.VISIBLE);
                cardHolder.viewPBCard.setVisibility(View.VISIBLE);
                cardHolder.viewLECard.setVisibility(View.VISIBLE);
                cardHolder.viewExchCard.setVisibility(View.VISIBLE);

                cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetFinCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetFinOutCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetPBCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetLECard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetExchCard.setBackgroundColor(Color.parseColor("#C1C1C1"));

                cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrSmActivity.class);
                        intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                        //intent.putExtra("sm_id", position);
                        intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                        intent.putExtra("etvbr", 1);
                        context.startActivity(intent);
                    }
                });

                cardHolder.tvRetailCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrSmActivity.class);
                        intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                        //intent.putExtra("sm_id", position);
                        intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                        intent.putExtra("etvbr", 1);
                        context.startActivity(intent);
                    }
                });

                cardHolder.tvExchCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrSmActivity.class);
                        intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                        //intent.putExtra("sm_id", position);
                        intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                        intent.putExtra("etvbr", 1);
                        context.startActivity(intent);
                    }
                });

                cardHolder.tvFinCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrSmActivity.class);
                        intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                        //intent.putExtra("sm_id", position);
                        intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                        intent.putExtra("etvbr", 1);
                        context.startActivity(intent);
                    }
                });
                cardHolder.tvFinOutCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrSmActivity.class);
                        intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                        //intent.putExtra("sm_id", position);
                        intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                        intent.putExtra("etvbr", 1);
                        context.startActivity(intent);
                    }
                });
                cardHolder.tvPBCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrSmActivity.class);
                        intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                        //intent.putExtra("sm_id", position);
                        intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                        intent.putExtra("etvbr", 1);
                        context.startActivity(intent);
                    }
                });
                cardHolder.tvLECard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrSmActivity.class);
                        intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                        //intent.putExtra("sm_id", position);
                        intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                        intent.putExtra("etvbr", 1);
                        context.startActivity(intent);
                    }
                });

                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(4, locationResult.getSalesManagers().get(position).getInfo().getName(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getId(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getRetails(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });

                cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(5, locationResult.getSalesManagers().get(position).getInfo().getName(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getId(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getExchanged(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });

                cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(6, locationResult.getSalesManagers().get(position).getInfo().getName(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getId(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getIn_house_financed(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });

                cardHolder.tvFinOutCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(7, locationResult.getSalesManagers().get(position).getInfo().getName(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getId(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getOut_house_financed(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });
                cardHolder.tvPBCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(8, locationResult.getSalesManagers().get(position).getInfo().getName(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getId(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getPending_bookings(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });
                cardHolder.tvLECard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(9, locationResult.getSalesManagers().get(position).getInfo().getName(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getId(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getLive_enquiries(),
                                "" + locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });


            } else {

                cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));

                Paint paint = new Paint();
                paint.setColor(Color.BLUE);
                paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);


                cardHolder.tvRetailCard.setPaintFlags(paint.getFlags());
                cardHolder.tvRetailCard.setText("" + locationResult.getLocationAbs().getRetails());

                cardHolder.tvExchCard.setPaintFlags(paint.getFlags());
                cardHolder.tvExchCard.setText("" + locationResult.getLocationAbs().getExchanged());

                cardHolder.tvFinCard.setPaintFlags(paint.getFlags());
                cardHolder.tvFinCard.setText("" + locationResult.getLocationAbs().getIn_house_financed());

                cardHolder.tvFinOutCard.setPaintFlags(paint.getFlags());
                cardHolder.tvFinOutCard.setText("" + locationResult.getLocationAbs().getOut_house_financed());

                cardHolder.tvPBCard.setPaintFlags(paint.getFlags());
                cardHolder.tvPBCard.setText(getReadableData(locationResult.getLocationAbs().getPending_bookings()));

                cardHolder.tvLECard.setPaintFlags(paint.getFlags());
                cardHolder.tvLECard.setText(getReadableData(locationResult.getLocationAbs().getLive_enquiries()));

                cardHolder.tvTargetRetailCard.setText("" + locationResult.getLocationTargets().getRetails());

                cardHolder.tvTargetTitleCard.setVisibility(View.VISIBLE);

                cardHolder.tvNameCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
                cardHolder.refFooter.setVisibility(View.VISIBLE);
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setText("Achieved");

                cardHolder.tvTargetTitleCard.setText("Target");

                cardHolder.tvRetailCard.setTextColor(Color.WHITE);
                cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);
                cardHolder.tvExchCard.setTextColor(Color.WHITE);
                cardHolder.tvFinCard.setTextColor(Color.WHITE);
                cardHolder.tvFinOutCard.setTextColor(Color.WHITE);
                cardHolder.tvPBCard.setTextColor(Color.WHITE);
                cardHolder.tvLECard.setTextColor(Color.WHITE);
                //cardHolder.tvTargetTitleCard.setTextColor(Color.WHITE);

                cardHolder.tvRetailCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvUserTotalCard.setTypeface(null, Typeface.BOLD);
                //cardHolder.tvTargetTitleCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvExchCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvFinCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvFinOutCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvPBCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvLECard.setTypeface(null, Typeface.BOLD);

                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
                cardHolder.viewExchCard.setVisibility(View.INVISIBLE);
                cardHolder.viewFinCard.setVisibility(View.INVISIBLE);
                cardHolder.viewFinOutCard.setVisibility(View.INVISIBLE);
                cardHolder.viewPBCard.setVisibility(View.INVISIBLE);
                cardHolder.viewLECard.setVisibility(View.INVISIBLE);

                cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#8C94AB"));
                cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#8C94AB"));

                final String userId = "" + realm.where(UserDetails.class).findFirst().getUserId();


                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(4, "All SM's Data",
                                "" + userId,
                                "" + locationResult.getLocationAbs().getRetails(),
                                userIdForAllSMData,
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });

                cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(5, "All SM's Data",
                                "" + userId,
                                "" + locationResult.getLocationAbs().getExchanged(),
                                userIdForAllSMData,
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });

                cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(6, "All SM's Data",
                                "" + userId,
                                "" + locationResult.getLocationAbs().getIn_house_financed(),
                                userIdForAllSMData,
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });

                cardHolder.tvFinOutCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(7, "All SM's Data",
                                "" + userId,
                                "" + locationResult.getLocationAbs().getOut_house_financed(),
                                userIdForAllSMData,
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });
                cardHolder.tvPBCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(8, "All SM's Data",
                                "" + userId,
                                "" + locationResult.getLocationAbs().getPending_bookings(),
                                userIdForAllSMData,
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });
                cardHolder.tvLECard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(9, "All SM's Data",
                                "" + userId,
                                "" + locationResult.getLocationAbs().getLive_enquiries(),
                                userIdForAllSMData,
                                "" + locationResult.getLocationId());
                        return true;
                    }
                });
            }
        }
    }


    private void viewCardsGMRel(RecyclerView.ViewHolder holder, final int position) {
        final EtvbrGMCardHolder cardHolder = (EtvbrGMCardHolder) holder;
        //final int position = pos;
        cardHolder.llRetailsCard.setVisibility(View.GONE);
        cardHolder.tvTargetRetailCard.setVisibility(View.GONE);
        cardHolder.tvTargetExchCard.setVisibility(View.GONE);
        cardHolder.tvTargetFinCard.setVisibility(View.GONE);
        cardHolder.tvTargetFinOutCard.setVisibility(View.GONE);
        cardHolder.tvTargetPBCard.setVisibility(View.GONE);
        cardHolder.tvTargetLECard.setVisibility(View.GONE);
        cardHolder.viewTargetCard.setVisibility(View.GONE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 6);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        cardHolder.llinearLayout.setVisibility(View.GONE);
        //rels = results.get(position).getRel();
        if (locationResult.getSalesManagers() != null) {
            if (position < locationResult.getSalesManagers().size()) {


                //System.out.println("resultSM " + locationResult.getSalesManagers().get(position).getInfo().getRel().getEnquiries());

                cardHolder.tvRetailCard.setPaintFlags(0);
                cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getRel().getRetails());

                cardHolder.tvExchCard.setPaintFlags(0);
                cardHolder.tvExchCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getRel().getExchanged());

                cardHolder.tvFinCard.setPaintFlags(0);
                cardHolder.tvFinCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getRel().getIn_house_financed());

                cardHolder.tvFinOutCard.setPaintFlags(0);
                cardHolder.tvFinOutCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getRel().getOut_house_financed());

                cardHolder.tvPBCard.setPaintFlags(0);
                cardHolder.tvPBCard.setText(getReadableData(locationResult.getSalesManagers().get(position).getInfo().getRel().getPending_bookings()));

                cardHolder.tvLECard.setPaintFlags(0);
                cardHolder.tvLECard.setText(getReadableData(locationResult.getSalesManagers().get(position).getInfo().getRel().getLive_enquiries()));

                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvTargetTitleCard.setVisibility(View.GONE);

                if (locationResult.getSalesManagers().get(position).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(position).getInfo().getDpUrl().equalsIgnoreCase("")) {
                    cardHolder.tvNameCard.setVisibility(View.GONE);
                    cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                    cardHolder.refFooter.setVisibility(View.GONE);
                    cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                    String url = locationResult.getSalesManagers().get(position).getInfo().getDpUrl();
                    Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUserCard);
                } else if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                    cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                    cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                    cardHolder.refFooter.setVisibility(View.GONE);
                    cardHolder.imgUserCard.setVisibility(View.GONE);
                    cardHolder.tvNameCard.setText(locationResult.getSalesManagers().get(position).getInfo().getName().charAt(0) + "");
                } else {
                    cardHolder.imgUserCard.setVisibility(View.GONE);
                    cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                    cardHolder.tvNameCard.setText("N");
                }

                cardHolder.imgUserCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        Intent intent = new Intent(context, EtvbrImgDialog.class);
                        Bundle bundle = new Bundle();
                        if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                            bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(position).getInfo().getDpUrl());
                            bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(position).getInfo().getName());
                            intent.putExtras(bundle);
                            context.startActivity(intent);
                        }
                        return true;
                    }
                });

                cardHolder.tvNameCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        Intent intent = new Intent(context, EtvbrImgDialog.class);
                        Bundle bundle = new Bundle();
                        if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                            bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(position).getInfo().getDpUrl());
                            bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(position).getInfo().getName());
                            intent.putExtras(bundle);
                            context.startActivity(intent);
                        }
                        return true;
                    }
                });

                cardHolder.llInner.setBackgroundColor(Color.WHITE);
                cardHolder.tvRetailCard.setTextColor(Color.BLACK);
                cardHolder.tvExchCard.setTextColor(Color.BLACK);
                cardHolder.tvFinCard.setTextColor(Color.BLACK);
                cardHolder.tvFinOutCard.setTextColor(Color.BLACK);
                cardHolder.tvPBCard.setTextColor(Color.parseColor("#7ED321"));
                cardHolder.tvLECard.setTextColor(Color.parseColor("#7ED321"));
                cardHolder.tvUserTotalCard.setTextColor(Color.BLACK);

                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
                cardHolder.viewExchCard.setVisibility(View.VISIBLE);
                cardHolder.viewFinCard.setVisibility(View.VISIBLE);
                cardHolder.viewFinOutCard.setVisibility(View.VISIBLE);
                cardHolder.viewPBCard.setVisibility(View.VISIBLE);
                cardHolder.viewLECard.setVisibility(View.VISIBLE);

                cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrSmActivity.class);
                        intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                        //intent.putExtra("sm_id", position);
                        intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                        intent.putExtra("etvbr", 1);
                        context.startActivity(intent);
                    }
                });

            } else {
                cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));


                cardHolder.tvRetailCard.setPaintFlags(0);
                cardHolder.tvRetailCard.setText("" + locationResult.getLocationRel().getRetails());

                cardHolder.tvExchCard.setPaintFlags(0);
                cardHolder.tvExchCard.setText("" + locationResult.getLocationRel().getExchanged());

                cardHolder.tvFinCard.setPaintFlags(0);
                cardHolder.tvFinCard.setText("" + locationResult.getLocationRel().getIn_house_financed());

                cardHolder.tvFinOutCard.setPaintFlags(0);
                cardHolder.tvFinOutCard.setText("" + locationResult.getLocationRel().getOut_house_financed());

                cardHolder.tvPBCard.setPaintFlags(0);
                cardHolder.tvPBCard.setText(getReadableData(locationResult.getLocationRel().getPending_bookings()));

                cardHolder.tvLECard.setPaintFlags(0);
                cardHolder.tvLECard.setText(getReadableData(locationResult.getLocationRel().getLive_enquiries()));

                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvFinOutCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvPBCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvLECard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvTargetTitleCard.setVisibility(View.GONE);

                cardHolder.tvNameCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
                cardHolder.refFooter.setVisibility(View.VISIBLE);
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setText("Total%");

                cardHolder.tvRetailCard.setTextColor(Color.WHITE);
                cardHolder.tvFinCard.setTextColor(Color.WHITE);
                cardHolder.tvExchCard.setTextColor(Color.WHITE);
                cardHolder.tvFinOutCard.setTextColor(Color.WHITE);
                cardHolder.tvPBCard.setTextColor(Color.WHITE);
                cardHolder.tvLECard.setTextColor(Color.WHITE);
                cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);

                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
                cardHolder.viewFinCard.setVisibility(View.INVISIBLE);
                cardHolder.viewFinOutCard.setVisibility(View.INVISIBLE);
                cardHolder.viewPBCard.setVisibility(View.INVISIBLE);
                cardHolder.viewLECard.setVisibility(View.INVISIBLE);
                cardHolder.viewExchCard.setVisibility(View.INVISIBLE);
            }
        }
    }

    private String getReadableData(Integer data) {
        if (data == null) {
            return "--";
        }
        return "" + data;
    }

    @Override
    public int getItemCount() {
        //resultSMs = realm.where(ResultSM.class).findAll();
        if (locationResult.isValid() && locationResult.getSalesManagers().isValid()) {
            return (locationResult.getSalesManagers().size() > 0) ? locationResult.getSalesManagers().size() + 1 : 0;
        } else {
            return 0;
        }
    }

    private void goToDetailsOfEtvbr(int position, String title_name, String user_id, String etvbrtotal, String user_role_id, String user_location_id) {

        Intent etvbrDetails = null;

        switch (position) {
            case 4:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Retails");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 5:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Exchanged");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 6:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "In House Financed");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 7:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Out House Financed");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 8:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Pending Bookings");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 9:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Live Enquiries");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
        }
        if (etvbrDetails != null)
            context.startActivity(etvbrDetails);
    }

    public class EtvbrGMCardHolder extends RecyclerView.ViewHolder {

        ImageView imgUser, imgUserCard;
        TextView tvNameCard, tvRetailCard, tvExchCard, tvFinCard, tvFinOutCard, tvPBCard, tvLECard;
        TextView tvName, tvEnquiry, tvTestDrive, tvVisit, tvBooking, tvRetail;
        View viewReatilCard, viewExchCard, viewFinCard, viewFinOutCard, viewPBCard, viewLECard;
        View viewEnquiry, viewTd, viewVisit, viewBooking, viewReatil;
        LinearLayout llinearLayout;
        LinearLayout llInner;
        TextView tvUserTotalCard;
        TextView tvUserTotal;
        CardView cardView;
        LinearLayout llRetailsCard, llExchCard, llFinCard, llFinOutCard, llPBCard, llLECard;
        TextView tvTargetTitleCard;
        TextView tvTargetRetailCard, tvTargetExchCard, tvTargetFinCard, tvTargetFinOutCard, tvTargetPBCard, tvTargetLECard;
        View viewTargetCard;
        RelativeLayout rlFrameLayoutCard;
        private LinearLayout refFooter;


        public EtvbrGMCardHolder(View itemView) {
            super(itemView);
            imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            tvName = (TextView) itemView.findViewById(R.id.text_user);
            tvEnquiry = (TextView) itemView.findViewById(R.id.txt_enquiry);
            tvTestDrive = (TextView) itemView.findViewById(R.id.txt_td);
            tvVisit = (TextView) itemView.findViewById(R.id.txt_visit);
            tvBooking = (TextView) itemView.findViewById(R.id.txt_booking);
            tvRetail = (TextView) itemView.findViewById(R.id.txt_retail);
            //llaLinearLayout = (LinearLayout) itemView.findViewById(R.id.ll_today_summary_main);
            tvUserTotal = (TextView) itemView.findViewById(R.id.text_user_total);
            viewEnquiry = (View) itemView.findViewById(R.id.enquiry_view);
            viewTd = (View) itemView.findViewById(R.id.td_view);
            viewVisit = (View) itemView.findViewById(R.id.visit_view);
            viewBooking = (View) itemView.findViewById(R.id.booking_view);
            viewReatil = (View) itemView.findViewById(R.id.retail_view);
            llinearLayout = (LinearLayout) itemView.findViewById(R.id.llinearLayout);
            llInner = (LinearLayout) itemView.findViewById(R.id.card_ll);

            imgUserCard = (ImageView) itemView.findViewById(R.id.img_user_card);
            tvNameCard = (TextView) itemView.findViewById(R.id.text_user_card);
            tvRetailCard = (TextView) itemView.findViewById(R.id.txt_retail_card);
            tvUserTotalCard = (TextView) itemView.findViewById(R.id.text_user_total_card);
            viewReatilCard = (View) itemView.findViewById(R.id.retail_view_card);
            cardView = (CardView) itemView.findViewById(R.id.cardview_etvbr);
            tvTargetTitleCard = (TextView) itemView.findViewById(R.id.tv_target_title_card);
            tvTargetRetailCard = (TextView) itemView.findViewById(R.id.txt_target_retail_card);
            viewTargetCard = (View) itemView.findViewById(R.id.target_view_card);
            rlFrameLayoutCard = (RelativeLayout) itemView.findViewById(R.id.rl_frame_layout_card);
            llRetailsCard = (LinearLayout) itemView.findViewById(R.id.retail_ll_card);
            viewExchCard = (View) itemView.findViewById(R.id.exch_view_card);
            viewFinCard = (View) itemView.findViewById(R.id.fin_view_card);
            viewFinOutCard = (View) itemView.findViewById(R.id.fin_out_view_card);
            viewPBCard = (View) itemView.findViewById(R.id.pending_bookings_view_card);
            viewLECard = (View) itemView.findViewById(R.id.live_enquiries_view_card);
            llExchCard = (LinearLayout) itemView.findViewById(R.id.exch_ll_card);
            llFinCard = (LinearLayout) itemView.findViewById(R.id.fin_ll_card);
            llFinOutCard = (LinearLayout) itemView.findViewById(R.id.fin_out_ll_card);
            llPBCard = (LinearLayout) itemView.findViewById(R.id.pending_bookings_ll_card);
            llLECard = (LinearLayout) itemView.findViewById(R.id.live_enquiries_ll_card);
            tvTargetExchCard = (TextView) itemView.findViewById(R.id.txt_target_exch__card);
            tvTargetFinCard = (TextView) itemView.findViewById(R.id.txt_target_fin_card);
            tvTargetFinOutCard = (TextView) itemView.findViewById(R.id.txt_target_fin_out_card);
            tvTargetPBCard = (TextView) itemView.findViewById(R.id.txt_target_pending_bookings_card);
            tvTargetLECard = (TextView) itemView.findViewById(R.id.txt_target_live_enquiries_card);
            tvExchCard = (TextView) itemView.findViewById(R.id.txt_exch_card);
            tvFinCard = (TextView) itemView.findViewById(R.id.txt_fin_card);
            tvFinOutCard = (TextView) itemView.findViewById(R.id.txt_fin_out_card);
            tvPBCard = (TextView) itemView.findViewById(R.id.txt_pending_bookings_card);
            tvLECard = (TextView) itemView.findViewById(R.id.txt_live_enquiries_card);
            refFooter = itemView.findViewById(R.id.ref_footer);

        }
    }


}
