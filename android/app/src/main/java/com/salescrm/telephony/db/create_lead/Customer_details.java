package com.salescrm.telephony.db.create_lead;

import io.realm.RealmObject;

/**
 * Created by bharath on 8/12/16.
 */

public class Customer_details extends RealmObject{
    private String office_pin_code;

    private String first_name;

    private String occupation;

    private String office_address;

    private String company_name;

    private String age;

    private String residence_address;

    private String last_name;

    private String pin_code;

    private String gender;

    private String title;

    private String office_city;

    private String city;

    public String getOffice_pin_code() {
        return office_pin_code;
    }

    public void setOffice_pin_code(String office_pin_code) {
        this.office_pin_code = office_pin_code;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOffice_address() {
        return office_address;
    }

    public void setOffice_address(String office_address) {
        this.office_address = office_address;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getResidence_address() {
        return residence_address;
    }

    public void setResidence_address(String residence_address) {
        this.residence_address = residence_address;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPin_code() {
        return pin_code;
    }

    public void setPin_code(String pin_code) {
        this.pin_code = pin_code;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOffice_city() {
        return office_city;
    }

    public void setOffice_city(String office_city) {
        this.office_city = office_city;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "ClassPojo [office_pin_code = " + office_pin_code + ", first_name = " + first_name + ", occupation = " + occupation + ", office_address = " + office_address + ", company_name = " + company_name + ", age = " + age + ", residence_address = " + residence_address + ", last_name = " + last_name + ", pin_code = " + pin_code + ", gender = " + gender + ", office_city = " + office_city + ", city = " + city + "]";
    }
}