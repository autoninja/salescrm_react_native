package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 16-08-2016.
 */
public class CustomerEmailId extends RealmObject {

    public static final String CUSTOMEREMAILID = "CustomerEmailId";

    @PrimaryKey
    private String emailID;
    private int customerID;
    private long leadId;
    private String  email;
    private String emailStatus;
    private String lead_email_mapping_id;

    public String getLead_email_mapping_id() {
        return lead_email_mapping_id;
    }

    public void setLead_email_mapping_id(String lead_email_mapping_id) {
        this.lead_email_mapping_id = lead_email_mapping_id;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(String emailStatus) {
        this.emailStatus = emailStatus;
    }
}
