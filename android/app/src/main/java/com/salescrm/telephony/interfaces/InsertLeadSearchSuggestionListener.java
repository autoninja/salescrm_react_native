package com.salescrm.telephony.interfaces;

/**
 * Created by bharath on 29/8/17.
 */

public interface InsertLeadSearchSuggestionListener{
    void onInsertLeadSuggestion(String lead, int position);
}
