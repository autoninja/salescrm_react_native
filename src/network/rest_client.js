import { Platform } from 'react-native';
import axios from "axios";
import querystring from "querystring";
import * as async_storage from "../storage/async_storage";
import NavigationService from "../navigation/NavigationService";

const HERE_MAP_API_END_POINT =
  "https://reverse.geocoder.api.here.com/6.2/reversegeocode.json";
const PINCODE_ADDRESS_MAP_API =
  "https://api.data.gov.in/resource/6176ee09-3d56-4a3b-8115-21841576b2f6";
const STAGING = "http://salesstagingapi.ninjacrm.com/";
const PRODUCTION = "https://salescmrapi.ninjacrm.com/";
const PRODUCTION_IL = "https://salescmrapi.ilgicdash.com/";
const API = PRODUCTION + "mobile/";
const COMMON_API = PRODUCTION + "common/";

// Add a response interceptor
axios.interceptors.response.use(
  function (response) {
    // Do something with response data
    let auth = null;
    if (response.headers && response.headers.authorization) {
      auth = response.headers.authorization.split(" ");
      if (auth[1]) {
        async_storage.setToken(auth[1]);
        global.token = auth[1];
      }
    }
    if (response.headers && response.headers.Authorization) {
      auth = response.headers.Authorization.split(" ");
      if (auth[1]) {
        async_storage.setToken(auth[1]);
        global.token = auth[1];
      }
    }
    return response;
  },
  error => {
    // Do something with response error
    if (error && error.response) {
      if (error.response.status == 401) {
        if (Platform.OS == "ios" && error.config.url.includes("salescmrapi")) {
          //console.log("401 in " + JSON.stringify(error.config.url))
          NavigationService.resetScreen("Login");
          return Promise.resolve(error);
        }
      }
    }
    return Promise.reject(error);
  }
);

export default class RestClient {
  constructor() {
    this.callCount = 1;
  }

  getCommonApiCall(path, params) {
    let uri;
    if (params) {
      uri = COMMON_API + path + "?" + params;
    } else {
      uri = COMMON_API + path;
    }
    console.log("Calling URI:" + uri);
    return axios.get(uri).then(response => response.data);
  }

  postCommonApiCall(path, input) {
    const uri = COMMON_API + path;
    console.log("Calling URI:" + uri);
    console.log(input);
    console.log(querystring.stringify(input));
    return axios
      .post(uri, querystring.stringify(input))
      .then(response => response.data);
  }

  getApiCall(path, params) {
    let uri;
    if (params) {
      uri = API + path + "?" + params;
    } else {
      uri = API + path;
    }
    console.log("Calling URI:" + uri);
    //console.log("Auth:" + global.token);
    return axios
      .get(uri, {
        headers: {
          Authorization: "Bearer " + global.token
        }
      })
      .then(response => {
        return response.data;
      });
  }

  /*
   getApiCall(path, params) {
   let uri;
   if(params) {
   uri = API+path+'?'+params;
   }
   else {
   uri = API+path;
   }
   console.log('Calling URI:'+uri);
   return axios.get(uri,
   {
   headers: {
   Authorization: 'Bearer '+global.token  }
   })
   .then( (response) => {
   async_storage.setToken(response.headers.authorization.split(' ')[1]);
   global.token = response.headers.authorization.split(' ')[1];
   if(response.data.statusCode == 4001 && this.callCount<2) {
   console.log("Calling::+"+this.callCount);
   //this.getApiCall(path, params);
   this.callCount++;
   return Promise.reject(response);
   };
   return Promise.resolve(response.data);
   }).
   catch(error => {
   console.log(error);
   });

   }
   */

  postApiCall(path, input) {
    const uri = API + path;
    console.log("Calling URI:" + uri);
    console.log(input);
    console.log(querystring.stringify(input));
    return axios
      .post(uri, input, {
        headers: {
          Authorization: "Bearer " + global.token
        }
      })
      .then(response => response.data);
  }
  postMultipartApiCall(path, input) {
    const uri = API + path;
    const formData = new FormData();
    formData.append("base64_image", input.base64_image);

    return axios
      .post(uri, formData, {
        headers: {
          Authorization: "Bearer " + global.token,
          "Content-Type": "multipart/form-data"
        }
      })
      .then(response => response.data);
  }
  patchApiCall(path, input) {
    const uri = API + path;
    console.log("Calling URI:" + uri);
    console.log(input);
    console.log(querystring.stringify(input));
    return axios
      .patch(uri, input, {
        headers: {
          Authorization: "Bearer " + global.token
        }
      })
      .then(response => response.data);
  }

  login(input_data) {
    const params =
      "mobile_no=" +
      input_data.mobile_no +
      "&device_id=" +
      input_data.device_id;
    return this.getCommonApiCall("login/otp", params);
  }
  validate_otp(input_data) {
    return this.postCommonApiCall("login/validate_otp", input_data);
  }

  getDealerships() {
    return this.getApiCall("users/dealerships");
  }
  checkAppStatus(input_data) {
    let params;
    if (input_data.ios) {
      params = "ios=" + true + "&api_version=" + input_data.version;
    } else {
      params = "api_version=4.4.5";
    }

    return this.getApiCall("checkVersion", params);
  }
  getUserInfo() {
    return this.getApiCall("users/info");
  }

  switchDealership(input) {
    return this.postApiCall("users/switch_dealership/" + input.dealer_id);
  }

  getETVBRDetails(input_data) {
    const params =
      "start_date=" +
      input_data.start_date +
      "&end_date=" +
      input_data.end_date +
      "&clicked_val=" +
      input_data.clicked_val +
      "&user_id=" +
      input_data.user_id +
      "&user_location_id=" +
      input_data.user_location_id +
      "&user_role_id=" +
      input_data.user_role_id +
      "&offset=" +
      input_data.offset +
      "&limit=" +
      input_data.limit +
      "&filters=" +
      input_data.filters;
    return this.getApiCall("reports/clickable_etvbr", params);
  }

  getETVBRTeam(input_data) {
    const params =
      "start_date=" +
      input_data.start_date +
      "&end_date=" +
      input_data.end_date +
      "&filters=" +
      input_data.filters;
    return this.getApiCall("reports/user_etvbr", params);
  }
  getETVBRCarModel(input_data) {
    const params =
      "start_date=" +
      input_data.start_date +
      "&end_date=" +
      input_data.end_date +
      "&filters=" +
      input_data.filters;
    return this.getApiCall("reports/model_etvbr", params);
  }

  getLostDropAnalysis(input_data) {
    const params =
      "start_date=" +
      input_data.start_date +
      "&end_date=" +
      input_data.end_date +
      "&filters=" +
      input_data.filters;
    return this.getApiCall("lost_drop_analysis", params);
  }

  getLostDropAnalysisBrandWise(input_data) {
    const params =
      "start_date=" +
      input_data.start_date +
      "&end_date=" +
      input_data.end_date +
      "&filters=" +
      input_data.filters;
    return this.getApiCall(
      "lost_drop_analysis/brand_wise_distribution",
      params
    );
  }
  getDropReasons(input_data) {
    const params =
      "start_date=" +
      input_data.start_date +
      "&end_date=" +
      input_data.end_date +
      "&filters=" +
      input_data.filters;
    return this.getApiCall("lost_drop_analysis/drop_reasons", params);
  }

  getTodaysDashboard(input_data) {
    const params = "date=" + input_data.date;
    return this.getApiCall("reports/tasks", params);
  }

  getETVBRFilters() {
    return this.getApiCall("etvbr_filters");
  }

  getLostDropFilters() {
    return this.getApiCall("lost_drop_filters");
  }
  uploadImage(input) {
    return this.postMultipartApiCall("upload_dp", input);
  }

  getSmsEmailSettings() {
    return this.getApiCall("settings/sms_email_configuration");
  }

  setSmsEmailSettings(input_data) {
    return this.postApiCall("settings/sms_email_configuration", input_data);
  }

  getGamificationConfiguration() {
    return this.getApiCall("gamification/configuration");
  }

  getConsultantData(input_data) {
    return this.getApiCall("gamification/team_leaderboard/" + input_data);
  }

  getLeaderBoardConsultantData(input_data) {
    const params = "month_year=" + input_data.date;
    return this.getApiCall(
      "gamification/dse_leaderboard/" + input_data.locationId,
      params
    );
  }

  getLeaderBoardTeams(input_data) {
    const params = "month_year=" + input_data.date;
    return this.getApiCall(
      "gamification/team_leaderboard/" + input_data.locationId,
      params
    );
  }
  getTeamLeaderDetails(input_data) {
    const params = "month_year=" + input_data.date;
    return this.getApiCall(
      "gamification/team_point_details/" + input_data.teamLeaderId,
      params
    );
  }

  getDsePoints(input_data) {
    const params = "month_year=" + input_data.date;
    return this.getApiCall(
      "gamification/dse_point_details/" + input_data.userId,
      params
    );
  }

  getDseBadges(input_data) {
    const params = "month_year=" + input_data.date;
    return this.getApiCall(
      "gamification/user_badge_details/" + input_data.userId,
      params
    );
  }

  getWallOfFame(input_data) {
    const params = "month_year=" + input_data.date;
    return this.getApiCall(
      "gamification/wall_of_fame/" + input_data.locationId,
      params
    );
  }

  getDsePointHelp(input_data) {
    const params = "month_year=" + input_data.date;
    return this.getApiCall(
      "gamification/dse_point_help/" + input_data.userId,
      params
    );
  }

  getModelsAndVariants(input_data) {
    const params = "brand_id[0]=-1";
    return this.getApiCall("getModelsAndVariants", params);
  }

  getAppConf() {
    return this.getApiCall("appconf");
  }

  getTeamUserDetails(input_data) {
    const params =
      "leadSourceIds[]=" +
      input_data.leadSourceIds +
      "&leadNewCar=0" +
      "&locationId=" +
      input_data.locationId;
    console.log("TeamParamas:" + params);
    return this.getApiCall("get_team_user_details", params);
  }

  getLeadElements() {
    return this.postApiCall("add_task/fetch_lead_elements");
  }

  createEnquiry(input_data) {
    return this.postApiCall("createLead", input_data);
  }
  getEventEtvbr(input_data) {
    const params =
      "start_date=" + input_data.start_date + "&filters=" + input_data.filters;
    return this.getApiCall("reports/event_etvbr", params);
  }
  getEventCategories() {
    //const params = 'start_date='+input_data.start_date+'&filters='+input_data.filters;
    return this.getApiCall("event/categories");
  }
  getEventETVBRDetails(input_data) {
    const params =
      "start_date=" +
      input_data.start_date +
      "&clicked_val=" +
      input_data.clicked_val +
      "&event_location_id=" +
      input_data.location_id +
      "&offset=" +
      input_data.offset +
      "&limit=" +
      input_data.limit +
      "&event_id=" +
      input_data.event_id;
    return this.getApiCall("reports/event_clickable_etvbr", params);
  }

  getEventInfo(input_data) {
    const params = input_data.event_id;
    return this.getApiCall("event/get_event/" + params);
  }

  createEvent(input_data) {
    return this.postApiCall("event/create", input_data);
  }
  updateEvent(input_data) {
    return this.postApiCall("event/update", input_data);
  }
  getActiveEvents() {
    return this.getApiCall("event/active_events")
  }
  getETVBRLeadSourceModelWise(input_data) {
    let params =
      "start_date=" +
      input_data.start_date +
      "&end_date=" +
      input_data.end_date +
      "&location_id=" +
      input_data.location_id +
      "&user_id=" +
      input_data.user_id +
      "&user_role_id=" +
      input_data.role_id +
      "&filters=" +
      input_data.filters;
    return this.getApiCall("reports/source_model_etvbr", params);
  }

  getLeadHistory(input_data) {
    let param = "lead_id=" + input_data.lead_id;
    return this.getApiCall("eventHistory/getLeadHistory", param);
  }

  getC360LeadInformation(input_data) {
    return this.postApiCall("getAllLeadInformation", input_data);
  }

  getPincodeDetails(params) {
    let uri =
      PINCODE_ADDRESS_MAP_API +
      "?" +
      "api-key=" +
      params.apiKey +
      "&format=json&offset=0&limit=100&filters[pincode]=" +
      params.pincode;
    return axios.get(uri).then(response => response.data);
  }

  getPincodeDetailsInternal(pincode) {
    return this.getCommonApiCall("pincode/" + pincode);
  }

  getReverseGeoCode(params) {
    let uri =
      HERE_MAP_API_END_POINT +
      "?" +
      "app_id=" +
      params.appId +
      "&app_code=" +
      params.appCode +
      "&gen=9" +
      "&maxresults=1" +
      "&mode=retrieveAddresses" +
      "&prox=" +
      params.CSVLatLong;
    return axios.get(uri).then(response => response.data);
  }
  getEvaluationManagersConf() {
    return this.getApiCall("hasEvaluationManagers");
  }
  getAllEvaluators(input_data) {
    let param = "location_id=" + input_data.location_id;
    return this.getApiCall("getAllEvaluators", param);
  }

  getExchangeDetails(input_data) {
    let param = "lead_id=" + input_data.lead_id;
    return this.getApiCall("getExchangeDetails", param);
  }

  updateExchangeCar(input_data) {
    return this.postApiCall("addExchangeDetails", input_data);
  }

  addExchangeCar(input_data) {
    return this.postApiCall("addExchangeCar", input_data);
  }

  getETVBRUsedCarExchange(input_data) {
    const params =
      "start_date=" +
      input_data.start_date +
      "&end_date=" +
      input_data.end_date;
    // "&filters=" +
    // input_data.filters;
    return this.getApiCall("usedCarDashboard/exchange", params);
  }

  getETVBRForUsedCarExchangeDetails(input_data) {
    const params =
      "start_date=" +
      input_data.start_date +
      "&end_date=" +
      input_data.end_date +
      "&clicked_val=" +
      input_data.clicked_val +
      "&user_id=" +
      input_data.user_id +
      "&user_location_id=" +
      input_data.user_location_id +
      "&user_role_id=" +
      input_data.user_role_id;
    return this.getApiCall("usedCarDashboard/exchangeDetailed", params);
  }

  getTodaysDashboardExchange(input_data) {
    const params = "date=" + input_data.date;
    return this.getApiCall("usedCarDashboard/tasks", params);
  }

  getRegDetails(input_data) {
    const param = "reg_no=" + input_data.reg_no;
    return this.getApiCall("getRegInfo", param);
  }

  //ColdVisit API's
  getColdVisitGenerateOTP(mobile_no) {
    let params = "mobile_number=" + mobile_no;
    return this.getApiCall("cold_visits/generateOTP", params);
  }
  postColdVisitSubmit(input_data) {
    return this.postApiCall("cold_visits/addDetails", input_data);
  }
  getColdVisitCounts(input_data) {
    let params =
      "start_date=" +
      input_data.start_date +
      "&end_date=" +
      input_data.end_date;
    return this.getApiCall("cold_visits/getCount", params);
  }
  getColdVisitDetails(input_data) {
    let params =
      "start_date=" +
      input_data.start_date +
      "&end_date=" +
      input_data.end_date +
      "&user_id=" +
      input_data.user_id +
      "&user_role_id=" +
      input_data.role_id +
      "&location_id=" +
      input_data.location_id;
    return this.getApiCall("cold_visits/getDetails", params);
  }
  postSearchResult(input_data) {
    return this.postApiCall("add_task/fetch_search_result", input_data)
  }
  postBookBike(input_data) {
    return this.postApiCall("bookBike", input_data);
  }
  getExternalAPIKeys() {
    return this.getApiCall("external_api_keys");
  }
  getTasksList(input_data, filters) {
    let params =
      "page=" + input_data.page +
      "&count=10" +
      "&date_filter%5Btype%5D=normal" +
      "&date_filter%5BfilterDate%5D=" + input_data.when +
      "&order=" + input_data.order +
      "&dseId=" + input_data.dseId +
      "&isLeadStatusDone=" + input_data.isLeadStatusDone +
      filters
    return this.getApiCall("tasks", params)
  }
  getFilterElements() {
    return this.getApiCall("tasks/loadSearchElements");
  }


  saveLeadEditedData(input_data) {
    return this.postApiCall("EditCustomer360Controller/saveLeadEdit", input_data);
  }

  saveCustomerEditedData(input_data) {
    return this.postApiCall("EditCustomer360Controller/saveEditedClientDetails", input_data);
  }

  addMobileNumber(input_data) {
    return this.postApiCall("EditCustomer360Controller/saveNewLeadMobileNumber", input_data);
  }

  addEmailId(input_data) {
    return this.postApiCall("EditCustomer360Controller/saveNewLeadMailId", input_data);
  }

  deleteMobileNumber(input_data) {
    return this.postApiCall("EditCustomer360Controller/deleteMobileNumber", input_data);
  }

  deleteEmailAddress(input_data) {
    return this.postApiCall("EditCustomer360Controller/deleteEmailAddr", input_data);
  }

  changePrimaryMobile(input_data) {
    return this.postApiCall("EditCustomer360Controller/changePrimaryMobile", input_data);
  }

  changePrimaryEmail(input_data) {
    return this.postApiCall("EditCustomer360Controller/changePrimaryEmail", input_data);
  }

  editMobileNumber(input_data) {
    return this.postApiCall("EditCustomer360Controller/saveEditLeadPhone", input_data);
  }

  editEmailAddress(input_data) {
    return this.postApiCall("EditCustomer360Controller/saveEditLeadMailId", input_data);
  }

  changePrimaryCar(input_data) {
    return this.postApiCall("changePrimaryCar", input_data);
  }

  changeEditedInterestedCar(input_data) {
    return this.postApiCall("EditCustomer360Controller/saveInterestedCarEdit", input_data);
  }

  rescheduleExchange(input_data) {
    return this.patchApiCall("rescheduleExchange", input_data);
  }

  cancelExchange(input_data) {
    return this.patchApiCall("cancelExchange", input_data);
  }
  getManualActivities(input_data) {
    let params =
      "lead_id=" + input_data.lead_id +
      "&schedule_type=" + input_data.schedule_type;
    return this.getApiCall("c360/manualActivitiesToSchedule", params);
  }

  getSplitFormObjects(input_data) {
    return this.postApiCall("newFormObjs", input_data)
  }
  exchangeLostDropCheck(lead_id) {
    let params = "lead_id=" + lead_id;
    return this.getApiCall("exchangeLostDropCheck", params)
  }
  getPntOtp(activityId, input_data) {
    return this.postApiCall(`activity/${activityId}/sendOTP`, input_data)
  }
  validatePntOtp(input_data) {
    let params = "lead_id=" + input_data.leadId +
      "&otp=" + input_data.otp;
    return this.getApiCall("activity/validateOTP", params)
  }
  submitForm(input_data) {
    return this.postApiCall('mformSubmission', input_data)
  }
  bookBike(input_data) {
    return this.postApiCall('bookBike', input_data);
  }
  ilBookRenewel(input_data) {
    return this.postApiCall('il/book_renewal', input_data);
  }
  ilBookSaleConfirmed(input_data) {
    return this.postApiCall('il/book_sale_confirmed', input_data);
  }
  ilSendToVerification(input_data) {
    return this.postApiCall('il/verify', input_data)
  }
  getManualActivitiesForPnt(leadId) {
    let params = "lead_id=" + leadId +
      "&schedule_type=2";
    return this.getApiCall('c360/manualActivitiesToSchedule', params)
  }
  submitManualActivityForPnt(input_data) {
    return this.postApiCall('c360/mnewActivitySubmission', input_data)
  }
  getAllManagerHierarchyUsersApi(input_data) {
    return this.getApiCall("teamsController/get_all_team_details/" + input_data.location_id);
  }

  deactivateUser(input_data) {
    return this.getApiCall("makeUserInactive/" + input_data.id);
  }

  moveDse(input_data) {
    return this.postApiCall("transferDSEToDifferentTeam", input_data);
  }

  moveTeamLead(input_data) {
    return this.postApiCall("teamsController/change_manager_of_tl", input_data);
  }
  getVinAllocationStatus() {
    return this.getApiCall("vin_allocation_statuses")
  }
  bookCar(input_data) {
    return this.postApiCall("bookCar", input_data)
  }
  cancelBooking(input_data) {
    return this.postApiCall("cancelBooking", input_data)
  }
  fullPaymentReceived(input_data) {
    return this.postApiCall("fullPaymentRecieved", input_data)
  }
  editBookingDetails(input_data) {
    return this.postApiCall("editBookingDetails", input_data)
  }
  cancelPayment(input_data) {
    return this.postApiCall("cancelPayment", input_data);
  }
  updateInvoice(input_data) {
    return this.postApiCall("updateInvoice", input_data);
  }
  updateDelivery(input_data) {
    return this.postApiCall("updateDelivery", input_data);
  }
  rescheduleTask(input_data) {
    return this.postApiCall("rescheduleTask", input_data);
  }
  getPostBookingActivityForm(input_data) {
    let params = "lead_id=" + input_data.leadId +
      "&activity_id=" + input_data.activity_id;
    return this.getApiCall("post_booking_activity_form", params);
  }

}
