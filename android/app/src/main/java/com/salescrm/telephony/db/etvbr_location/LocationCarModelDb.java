package com.salescrm.telephony.db.etvbr_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 13/11/17.
 */

public class LocationCarModelDb extends RealmObject {

    @PrimaryKey
    @SerializedName("location_id")
    @Expose
    private Integer location_id;
    @SerializedName("location_abs")
    @Expose
    private LocationAbsCarModel location_abs;
    @SerializedName("location_rel")
    @Expose
    private LocationRelCarModel location_rel;
    @SerializedName("car_models")
    @Expose
    private RealmList<CarModelList> car_models = null;

    public Integer getLocationId() {
        return location_id;
    }

    public void setLocationId(Integer locationId) {
        this.location_id = locationId;
    }

    public LocationAbsCarModel getLocationAbs() {
        return location_abs;
    }

    public void setLocationAbs(LocationAbsCarModel locationAbs) {
        this.location_abs = locationAbs;
    }

    public LocationRelCarModel getLocationRel() {
        return location_rel;
    }

    public void setLocationRel(LocationRelCarModel locationRel) {
        this.location_rel = locationRel;
    }

    public RealmList<CarModelList> getCarModels() {
        return car_models;
    }

    public void setCarModels(RealmList<CarModelList> carModels) {
        this.car_models = carModels;
    }

}
