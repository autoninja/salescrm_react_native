package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by prateek on 2/1/17.
 */

public class DealerDataDb extends RealmObject {

    private String custSupportEmailId;

    private String ccmEmailId;

    private String ccmMobNo;

    private String dealerCity;

    private String hotlineNo;

    private String dname;

    private String dbrand;

    private String crmMobNo;

    private String tdFeedbackLink;

    private String dealerWebsite;

    private String dealerAddress;

    private String pocName;

    public String getCustSupportEmailId ()
    {
        return custSupportEmailId;
    }

    public void setCustSupportEmailId (String custSupportEmailId)
    {
        this.custSupportEmailId = custSupportEmailId;
    }

    public String getCcmEmailId ()
    {
        return ccmEmailId;
    }

    public void setCcmEmailId (String ccmEmailId)
    {
        this.ccmEmailId = ccmEmailId;
    }

    public String getCcmMobNo ()
    {
        return ccmMobNo;
    }

    public void setCcmMobNo (String ccmMobNo)
    {
        this.ccmMobNo = ccmMobNo;
    }

    public String getDealerCity ()
    {
        return dealerCity;
    }

    public void setDealerCity (String dealerCity)
    {
        this.dealerCity = dealerCity;
    }

    public String getHotlineNo ()
    {
        return hotlineNo;
    }

    public void setHotlineNo (String hotlineNo)
    {
        this.hotlineNo = hotlineNo;
    }

    public String getDname ()
    {
        return dname;
    }

    public void setDname (String dname)
    {
        this.dname = dname;
    }

    public String getDbrand ()
    {
        return dbrand;
    }

    public void setDbrand (String dbrand)
    {
        this.dbrand = dbrand;
    }

    public String getCrmMobNo ()
    {
        return crmMobNo;
    }

    public void setCrmMobNo (String crmMobNo)
    {
        this.crmMobNo = crmMobNo;
    }

    public String getTdFeedbackLink ()
    {
        return tdFeedbackLink;
    }

    public void setTdFeedbackLink (String tdFeedbackLink)
    {
        this.tdFeedbackLink = tdFeedbackLink;
    }

    public String getDealerWebsite ()
    {
        return dealerWebsite;
    }

    public void setDealerWebsite (String dealerWebsite)
    {
        this.dealerWebsite = dealerWebsite;
    }

    public String getDealerAddress ()
    {
        return dealerAddress;
    }

    public void setDealerAddress (String dealerAddress)
    {
        this.dealerAddress = dealerAddress;
    }

    public String getPocName ()
    {
        return pocName;
    }

    public void setPocName (String pocName)
    {
        this.pocName = pocName;
    }
}
