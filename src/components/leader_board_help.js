import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image, ScrollView} from 'react-native';
import RestClient from '../network/rest_client'

export default class LeaderBoardHelp extends React.Component {

	constructor(props)
		{
		    super(props);

		    this.state = {
		    	isLoading: true,
		    	//isFetching:false,
		    	result: []
			}
		}

		componentDidMount(){

					if(global._userId) {
						  this.makeRemoteRequest(global._userId);
					}
					else {
							Alert.alert("Failed to get user info");
					}

  			}

  		makeRemoteRequest = (userId) => {
	  			this.setState({ isLoading: true});
	  			new RestClient().getDsePointHelp({userId,date:'12-2018'}).then( (data) => {
			      if(data.statusCode == 4001 || data.statusCode == 5001) {
			                  Alert.alert(
			                'Failed',
			                'Please try again',
			                [
			                  {text: 'Logout', onPress: () => this.logout()},
			                  {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
			                ],
			                { cancelable: false }
			                )
			                  return;
			                }
			               console.log("uwala: "+JSON.stringify(data));
			               this.setState({
							    		isLoading: false,
							    		result:data.result
										});


			          }).catch((error) => {
			            console.error("Error Team Details"+ error)
			            if (error.response && error.response.status == 401) {
			                Alert.alert(
			              'Failed',
			              'Please try again',
			              [
			                {text: 'Logout', onPress: () => this.logout()},
			                {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
			              ],
			              { cancelable: false }
			            )
			              }
			          });
			}	

	render () {
			if (this.state.isLoading) {
			      return (
			        <View style={{flex: 1, backgroundColor:'#fff', alignItems : 'center', justifyContent: 'center'}}>
			          <ActivityIndicator />
			        </View>
			      );
	   			 }

	    return (
	    	<View style={{flex:1, justifyContent: 'center', backgroundColor: '#0F2B52' }}>
		    	<ScrollView>
		    		<View style={{flexDirection: 'column', justifyContent: 'center' }}>
			    		<View style={{flexDirection: 'column', justifyContent: 'center', backgroundColor: '#566781', padding: 5}}>
			    			<Text style={{color: '#fff', fontSize: 16, padding: 5}}> Points Before Target Completion </Text>
			    		</View>
			    		<View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center', padding: 5}}>
			    			<View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
				    			<Text style={{flex: 1, color: '#fff', fontSize: 13, textAlign: 'center'}}> Activity </Text>
				    			<Text style={{flex: 1, color: '#fff', fontSize: 13, textAlign: 'center'}}> Points for Achieving Target </Text>
				    			<Text style={{flex: 1, color: '#fff', fontSize: 13, textAlign: 'center'}}> Target </Text>
				    			<Text style={{flex: 1, color: '#fff', fontSize: 13, textAlign: 'center'}}> Points per Activity </Text>
			    			</View>
			    			
			    			{this.state.result.map((info, i) =>
			    				<View key={i} style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
					    			<Text style={{flex: 1, color: '#88A1C7', fontSize: 13, textAlign: 'center'}}> {info.name} </Text>
					    			<Text style={{flex: 1, color: '#88A1C7', fontSize: 13, textAlign: 'center'}}> {info.base_point} </Text>
					    			<Text style={{flex: 1, color: '#88A1C7', fontSize: 13, textAlign: 'center'}}> {info.target} </Text>
					    			<Text style={{flex: 1, color: '#fff', fontSize: 13, fontWeight: 'bold', textAlign: 'center'}}> {info.points_per_activity} </Text>
			    				</View>
			    				)}
			    		</View>
		    		</View>
		    		<View style={{ flexDirection: 'column', justifyContent: 'center', marginTop: 10}}>
			    		<View style={{flexDirection: 'column', justifyContent: 'center', backgroundColor: '#566781', padding: 5}}>
			    			<Text style={{color: '#fff', fontSize: 16, padding: 5}}> Points After Target Completion </Text>
			    		</View>
			    		<View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center', padding: 5}}>
			    			<View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
				    			<Text style={{flex: 1, color: '#fff', fontSize: 13, textAlign: 'center'}}> Activity </Text>
				    			<Text style={{flex: 1, color: '#fff', fontSize: 13, textAlign: 'center'}}> Points per Activity </Text>
			    			</View>
			    			
			    			{this.state.result.map((info, i) =>
			    				<View key={i} style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
					    			<Text style={{flex: 1, color: '#88A1C7', fontSize: 13, textAlign: 'center'}}> {info.name} </Text>
					    			<Text style={{flex: 1, color: '#fff', fontSize: 13, textAlign: 'center'}}> {info.base_point} </Text>
			    				</View>
			    				)}
			    			
			    		</View>
		    		</View>
		    		<View style={{ flexDirection: 'column', justifyContent: 'center' }}>
		    			<Text style={{flex: 1, color: '#fff', fontSize: 13}}> Spin The Wheel </Text>
						<Text style={{flex: 1, color: '#fff', fontSize: 13}}> Consultant - On Retail </Text>
						<Text style={{flex: 1, color: '#fff', fontSize: 13}}> Consultant - On Achieving Retail Target </Text>
						<Text style={{flex: 1, color: '#fff', fontSize: 13}}> Team Lead - Every Monday if his team has 0 pendings each day in last 7 days </Text>
						<Text style={{flex: 1, color: '#fff', fontSize: 13}}> No Pendings - Get 50 points each day for having no pendings </Text>
						<Text style={{flex: 1, color: '#fff', fontSize: 13}}> Booster Period - Earn double points for each activity </Text>
		    		</View>
		    		</ScrollView>
	    	</View>
	    	)
	}
}