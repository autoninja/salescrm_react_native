import { realm } from './realm_schemas';
import {TeamUserModel, TeamModel, UserModel} from '../models/team_user_model'
const TeamUserDBService = {
  findAll: function() {
    return realm.objects('TeamUserDB');
  },
  save: function(teamUser) {
    realm.write(() => {
      realm.create('TeamUserDB', teamUser, true);
    })
    let teamUserDB = realm.objects('TeamUserDB');
    console.log('TeamUserDB.length', teamUserDB.length);
  },
  getTeamUser: () => {
    let teamUserDB = realm.objects('TeamUserDB');
    let teams = [];
    if(teamUserDB && teamUserDB.length>0) {
      teamUserDB.map((teamMain)=> {
        let teamModels = [];
        let userModels = [];
        teamMain.teams.map((team)=> {
          teamModels.push(new TeamModel(team.manager_id, team.team_leader_id, team.user_id));

        });

        teamMain.users.map((user)=> {
          userModels.push(new UserModel(user.id, user.name, user.mobile_number, user.role_id));

        });
        teams.push(new TeamUserModel(teamMain.location_id , teamModels, userModels));
      })


    }
    return teams;
  },
  deleteAll: function() {
    realm.write(() => {
      realm.delete(realm.objects('TeamDB'));
      realm.delete(realm.objects('UserDB'));
      realm.delete(realm.objects('TeamUserDB'));
    })
  },
}

export default TeamUserDBService;
