package com.salescrm.telephony.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.salescrm.telephony.fragments.TodayTasksSummaryChildFragment;
import com.salescrm.telephony.model.TodaySummaryModel;
import com.salescrm.telephony.model.TodaySummaryResponse;

import java.util.List;

/**
 * Created by bharath on 20/11/17.
 */

public class TodayTasksSummaryMainPagerAdapter extends FragmentPagerAdapter {

    private List<TodaySummaryResponse.Result> todaySummaryList;
    private boolean isUserTlOnly;

    public TodayTasksSummaryMainPagerAdapter(FragmentManager fm,
                                             boolean isUserTlOnly) {
        super(fm);
        this.isUserTlOnly = isUserTlOnly;
        System.out.println("New Instance CalledInit");

    }

    @Override
    public Fragment getItem(int position) {
        System.out.println("New Instance Called GetItem");
        return TodayTasksSummaryChildFragment.newInstance(isUserTlOnly, position);
    }

    @Override
    public int getCount() {
        return TodaySummaryModel.getInstance().getResultList().size();
    }
}
