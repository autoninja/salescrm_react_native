package com.salescrm.telephony.db;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 *  Created by bharath on 29/6/17.
 */
public class EnqSourceCategoryDB extends RealmObject {

    private int id;
    private String name;

    private RealmList<EnqSubInnerSourceDB> subLeadSources;

    public EnqSourceCategoryDB() {
    }

    public EnqSourceCategoryDB(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<EnqSubInnerSourceDB> getSubLeadSources() {
        return subLeadSources;
    }

    public void setSubLeadSources(RealmList<EnqSubInnerSourceDB> subLeadSources) {
        this.subLeadSources = subLeadSources;
    }
}
