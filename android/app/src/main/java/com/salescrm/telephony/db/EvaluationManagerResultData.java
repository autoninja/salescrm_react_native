package com.salescrm.telephony.db;

import io.realm.RealmObject;

public class EvaluationManagerResultData extends RealmObject {

    String locationId;
    boolean hasEvaluationManager;

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public boolean isHasEvaluationManager() {
        return hasEvaluationManager;
    }

    public void setHasEvaluationManager(boolean hasEvaluationManager) {
        this.hasEvaluationManager = hasEvaluationManager;
    }
}
