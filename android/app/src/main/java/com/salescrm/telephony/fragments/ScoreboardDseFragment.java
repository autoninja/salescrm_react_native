package com.salescrm.telephony.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.HomeActivity;
import com.salescrm.telephony.db.ScoreboardDseDb;
import com.salescrm.telephony.db.etvbr_location.AbsoluteValue;
import com.salescrm.telephony.db.etvbr_location.Info;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.db.etvbr_location.LocationAbs;
import com.salescrm.telephony.db.etvbr_location.LocationRel;
import com.salescrm.telephony.db.etvbr_location.LocationTargets;
import com.salescrm.telephony.db.etvbr_location.RelationalValue;
import com.salescrm.telephony.db.etvbr_location.ResultEtvbrLocation;
import com.salescrm.telephony.db.etvbr_location.SalesConsultant;
import com.salescrm.telephony.db.etvbr_location.SalesManager;
import com.salescrm.telephony.db.etvbr_location.TargetValue;
import com.salescrm.telephony.db.etvbr_location.TeamLeader;
import com.salescrm.telephony.db.etvbr_location.TotalAbs;
import com.salescrm.telephony.db.etvbr_location.TotalRel;
import com.salescrm.telephony.db.etvbr_location.TotalTargets;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.model.EtvbrLocationResponse;
import com.salescrm.telephony.model.ScoreboardDsePojo;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by prateek on 24/8/17.
 */

public class ScoreboardDseFragment extends Fragment {

    TextView tvEnquiry, tvEnquiryTarget;
    View viewTD, viewVisit, viewBooking, viewRetail;
    TextView tvTDrive, tvTDriveTarget , tvTDriveTitle;
    TextView tvVisit, tvVisitTarget, tvVisitTitle;
    TextView tvBooking, tvBookingTarget, tvBookingTitle;
    TextView tvRetail, tvRetailTarget, tvRetailTitle;
    ProgressBar progressBarEnquiry, progressBarTd, progressBarVisit, progressBarBooking, progressBarRetail;
    TextView tvMyTask;
    ImageView datePrevious, dateNext;
    Calendar calendar;
    SimpleDateFormat sdf, sdf2;
    TextView tvMonthYear;
    String selectedMonth = "", previousMonth ="", nextMonth = "", currentDate = "";
    private Preferences pref;
    private Realm realm;
    private SalesConsultant realmScoreboardDseDb;
    private ProgressDialog mProgressDialog;
    private CallHomeActivity callHomeActivity;
    private LinearLayout dseScoreParentll;

    public static Fragment newInstance(int i, String scoreboard) {
        Bundle args = new Bundle();
        args.putInt("someInt", i);
        args.putString("someTitle", scoreboard);
        ScoreboardDseFragment fragment = new ScoreboardDseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity a;
        if(context instanceof HomeActivity) {
            a = (HomeActivity) context;
            try {
                callHomeActivity = (CallHomeActivity) a;
            } catch (ClassCastException e) {
                throw new ClassCastException(a.toString() + " must implement OnDataPassSMS");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.scoreboard_dse_fragment, container, false);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        dseScoreParentll = (LinearLayout) rootView.findViewById(R.id.dse_score_parent_ll);
        viewTD = rootView.findViewById(R.id.ll_score_td);
        viewVisit = rootView.findViewById(R.id.ll_score_visit);
        viewBooking = rootView.findViewById(R.id.ll_score_booking);
        viewRetail = rootView.findViewById(R.id.ll_score_retail);
        progressBarEnquiry = (ProgressBar) rootView.findViewById(R.id.enquiry_progress);
        progressBarTd = (ProgressBar) viewTD.findViewById(R.id.all_progress);
        progressBarVisit = (ProgressBar) viewVisit.findViewById(R.id.all_progress);
        progressBarBooking = (ProgressBar) viewBooking.findViewById(R.id.all_progress);
        progressBarRetail = (ProgressBar) viewRetail.findViewById(R.id.all_progress);
        tvMyTask = (TextView) rootView.findViewById(R.id.tv_goto_mytask);
        dateNext = (ImageView) rootView.findViewById(R.id.next_image);
        datePrevious = (ImageView) rootView.findViewById(R.id.previous_image);
        tvMonthYear = (TextView) rootView.findViewById(R.id.tv_month_year);
        tvEnquiry = (TextView) rootView.findViewById(R.id.tv_score_enquiry);
        tvEnquiryTarget = (TextView) rootView.findViewById(R.id.tv_score_enquiry_target);
        tvTDrive = (TextView) viewTD.findViewById(R.id.dse_score);
        tvTDriveTarget = (TextView) viewTD.findViewById(R.id.dse_target);
        tvVisitTitle = (TextView) viewVisit.findViewById(R.id.dse_title);
        tvVisit = (TextView) viewVisit.findViewById(R.id.dse_score);
        tvVisitTarget = (TextView) viewVisit.findViewById(R.id.dse_target);
        tvTDriveTitle = (TextView) viewTD.findViewById(R.id.dse_title);
        tvBooking = (TextView) viewBooking.findViewById(R.id.dse_score);
        tvBookingTarget = (TextView) viewBooking.findViewById(R.id.dse_target);
        tvBookingTitle = (TextView) viewBooking.findViewById(R.id.dse_title);
        tvRetail = (TextView) viewRetail.findViewById(R.id.dse_score);
        tvRetailTarget = (TextView) viewRetail.findViewById(R.id.dse_target);
        tvRetailTitle = (TextView) viewRetail.findViewById(R.id.dse_title);

        if(DbUtils.isBike()){
            viewVisit.setVisibility(View.INVISIBLE);
            viewRetail.setVisibility(View.INVISIBLE);
        }else {
            viewVisit.setVisibility(View.VISIBLE);
            viewRetail.setVisibility(View.VISIBLE);
        }
        Drawable drawablell = VectorDrawableCompat.create(getActivity().getResources(), R.drawable.ic_scoreboard_background_white, null);
        dseScoreParentll.setBackground(drawablell);

        Drawable drawable = VectorDrawableCompat
                .create(getActivity().getResources(), R.drawable.ic_scoreboard_target, null);
        tvEnquiryTarget.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        tvTDriveTarget.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        tvVisitTarget.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        tvBookingTarget.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        tvRetailTarget.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        tvVisitTitle.setText("Visit");
        if(DbUtils.isBike()){
            tvBookingTitle.setText("Converted Bikes");
            tvTDriveTitle.setText("Test Ride");
        }else {
            tvBookingTitle.setText("Booking");
            tvTDriveTitle.setText("Test Drive");
        }
        tvRetailTitle.setText("Retail");
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage("Fetching...");
            mProgressDialog.setCanceledOnTouchOutside(false);
        }

        calendar = Calendar.getInstance();
        sdf = new SimpleDateFormat("MMM yyyy");
        sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        if(new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            currentMonth();
        }else {
            Util.showToast(getContext(), "No internet connection", Toast.LENGTH_SHORT);
        }

        datePrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                    calendar.add(Calendar.MONTH, -1);
                    //selectedMonth = sdf.format(calendar.getTime());
                    tvMonthYear.setText(sdf.format(calendar.getTime()));
                    previousMonth = sdf2.format(calendar.getTime());
                    serverCall(previousMonth);
                    dateNext.setVisibility(View.VISIBLE);
                    //fillUpTheViews();
                }else {
                    Util.showToast(getContext(), "No internet connection", Toast.LENGTH_SHORT);
                }
            }
        });

        dateNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                    Calendar calender2 = Calendar.getInstance();
                    calendar.add(Calendar.MONTH, +1);
                    selectedMonth = sdf.format(calendar.getTime());
                    tvMonthYear.setText(sdf.format(calendar.getTime()));
                    nextMonth = sdf2.format(calendar.getTime());
                    String otherMonth = sdf.format(calender2.getTime());
                    if (selectedMonth.equalsIgnoreCase(otherMonth)) {
                        dateNext.setVisibility(View.INVISIBLE);
                    }
                    serverCall(nextMonth);
                    //fillUpTheViews();
                }else{
                    Util.showToast(getContext(), "No internet connection", Toast.LENGTH_SHORT);
                }
            }
        });

        tvMyTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getContext(), "Will take you to My task", Toast.LENGTH_LONG).show();
                callHomeActivity.callMyTask(2);
            }
        });

        return rootView;
    }

    public interface CallHomeActivity{
         void callMyTask(int value);
    }

    private void currentMonth() {
        tvMonthYear.setText(sdf.format(calendar.getTime()));
        dateNext.setVisibility(View.INVISIBLE);
        currentDate = sdf2.format(calendar.getTime());
        serverCall(currentDate);
        //fillUpTheViews();
    }

    private void serverCall(String currentDate) {
        showDialog();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getEtvbrInfoWithLocation(currentDate, currentDate, null, new Callback<EtvbrLocationResponse>() {
            @Override
            public void success(final EtvbrLocationResponse scoreboardDsePojo, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(scoreboardDsePojo.getStatusCode().equals(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                       //realm.delete(ScoreboardDseDb.class);
                       //addScoreToDb( realm, scoreboardDsePojo);
                       emptyDb();
                       realm.createOrUpdateObjectFromJson(ResultEtvbrLocation.class, new Gson().toJson(scoreboardDsePojo.getResult()));
                       fillUpTheViews();
                    }
                });
                hideDialog();

            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println("Erro bro!! "+error.getMessage());
                hideDialog();
                Util.showToast(getContext(), "Server not responding - "+ error.getMessage(), Toast.LENGTH_SHORT);
            }
        });
    }

    private void emptyDb() {
        realm.delete(ResultEtvbrLocation.class);
        realm.delete(Location.class);
        realm.delete(SalesManager.class);
        realm.delete(Info.class);
        realm.delete(SalesConsultant.class);
        realm.delete(LocationAbs.class);
        realm.delete(LocationRel.class);
        realm.delete(LocationTargets.class);
        realm.delete(TeamLeader.class);
        realm.delete(TotalAbs.class);
        realm.delete(TotalRel.class);
        realm.delete(TotalTargets.class);
        realm.delete(AbsoluteValue.class);
        realm.delete(RelationalValue.class);
        realm.delete(TargetValue.class);
    }

    private void addScoreToDb(Realm realm, ScoreboardDsePojo scoreboardDsePojo) {
        if(scoreboardDsePojo.getResult().getResult() != null) {
            ScoreboardDseDb scoreboard = new ScoreboardDseDb();
            scoreboard.setUser_id(scoreboardDsePojo.getResult().getResult().getUser_activities().getUser_id());
            scoreboard.setUser_name(scoreboardDsePojo.getResult().getResult().getUser_activities().getUser_name());
            scoreboard.setEnquiries(scoreboardDsePojo.getResult().getResult().getUser_activities().getEnquiries());
            scoreboard.setEnquiries_target(scoreboardDsePojo.getResult().getResult().getUser_activities().getEnquiries_target());
            scoreboard.setTdrives(scoreboardDsePojo.getResult().getResult().getUser_activities().getTdrives());
            scoreboard.setTdrives_target(scoreboardDsePojo.getResult().getResult().getUser_activities().getTdrives_target());
            scoreboard.setVisits(scoreboardDsePojo.getResult().getResult().getUser_activities().getVisits());
            scoreboard.setVisits_target(scoreboardDsePojo.getResult().getResult().getUser_activities().getVisits_target());
            scoreboard.setBookings(scoreboardDsePojo.getResult().getResult().getUser_activities().getBookings());
            scoreboard.setBookings_target(scoreboardDsePojo.getResult().getResult().getUser_activities().getBookings_target());
            scoreboard.setRetail(scoreboardDsePojo.getResult().getResult().getUser_activities().getRetail());
            scoreboard.setRetail_target(scoreboardDsePojo.getResult().getResult().getUser_activities().getRetail_target());
            realm.copyToRealm(scoreboard);
        }
    }

    private void fillUpTheViews() {
        realmScoreboardDseDb = realm.where(SalesConsultant.class).findFirst();

        if(realmScoreboardDseDb != null) {
            tvEnquiry.setText(""+realmScoreboardDseDb.getInfo().getAbs().getEnquiries());
            tvEnquiryTarget.setText(""+realmScoreboardDseDb.getInfo().getTargets().getEnquiries());
            progressBarEnquiry.setMax(realmScoreboardDseDb.getInfo().getTargets().getEnquiries());
            progressBarEnquiry.setProgress(realmScoreboardDseDb.getInfo().getAbs().getEnquiries());

            tvTDrive.setText(""+realmScoreboardDseDb.getInfo().getAbs().getTdrives());
            tvTDriveTarget.setText(""+realmScoreboardDseDb.getInfo().getTargets().getTdrives());
            progressBarTd.setMax(realmScoreboardDseDb.getInfo().getTargets().getTdrives());
            progressBarTd.setProgress(realmScoreboardDseDb.getInfo().getAbs().getTdrives());

            tvVisit.setText(""+realmScoreboardDseDb.getInfo().getAbs().getVisits());
            tvVisitTarget.setText(""+realmScoreboardDseDb.getInfo().getTargets().getVisits());
            progressBarVisit.setMax(realmScoreboardDseDb.getInfo().getTargets().getVisits());
            progressBarVisit.setProgress(realmScoreboardDseDb.getInfo().getAbs().getVisits());

            tvBooking.setText(""+realmScoreboardDseDb.getInfo().getAbs().getBookings());
            tvBookingTarget.setText(""+realmScoreboardDseDb.getInfo().getTargets().getBookings());
            progressBarBooking.setMax(realmScoreboardDseDb.getInfo().getTargets().getBookings());
            progressBarBooking.setProgress(realmScoreboardDseDb.getInfo().getAbs().getBookings());

            tvRetail.setText(""+realmScoreboardDseDb.getInfo().getAbs().getRetails());
            tvRetailTarget.setText(""+realmScoreboardDseDb.getInfo().getTargets().getRetails());
            progressBarRetail.setMax(realmScoreboardDseDb.getInfo().getTargets().getRetails());
            progressBarRetail.setProgress(realmScoreboardDseDb.getInfo().getAbs().getRetails());
        }else{

        }
    }

    void showDialog(){
       if(mProgressDialog!= null && !mProgressDialog.isShowing()){
           mProgressDialog.show();
       }
    }

    void hideDialog(){
        if(mProgressDialog!= null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }
}
