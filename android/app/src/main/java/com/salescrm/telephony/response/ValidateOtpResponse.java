package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by bharath on 30-06-2016.
 */
public class ValidateOtpResponse {

    private String responseMessage;

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result{
        private String message;

        private String success;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getSuccess() {
            return success;
        }

        public void setSuccess(String success) {
            this.success = success;
        }
    }
}