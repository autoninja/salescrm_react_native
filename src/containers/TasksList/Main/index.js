import React, { Component } from "react";
import { View, Alert, NativeModules, Platform } from 'react-native';
import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation'
import Moment from 'moment';

import TasksTab from './tasksTab';
import ClearFilterFooter from './ClearFilterFooter';
import styles from './tasksList.styles'

import { connect } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient'
import RestClient from '../../../network/rest_client'
import DateUtils from '../../../utils/DateUtils'
import { Linking } from 'react-native'
import { getFilterInitialData } from "../Filter/FilterInitialState";
import { updateTasksListFilter, updateTasksListFilterUpdate } from '../../../actions/tasks_list_actions';
import Utils from '../../../utils/Utils'
import NavigationService from '../../../navigation/NavigationService'
class TasksListMain extends Component {
    state = {
        filters: [],
        filtersUpdate: false,
        dseId: this.getUserId(),
        page: 1,
        tabs: [
            {
                tabIndex: 0,
                tabName: 'Today',
                when: 'today_with_pending',
                order: 'DESC',
                doneLeadStatus: 0,
                loading: true,
                data: [],
                total: 0,
                doneCount: 0,
                page: 1,
                totalPages: 1,
                paginating: false,
                scrollToIndex: 0,

            },
            {
                tabIndex: 1,
                tabName: 'Future',
                when: 'future',
                order: 'ASC',
                doneLeadStatus: 0,
                loading: true,
                data: [],
                total: 0,
                doneCount: 0,
                page: 1,
                totalPages: 1,
                paginating: false,
                scrollToIndex: 0,
            },
            {
                tabIndex: 2,
                tabName: 'Done',
                when: 'today',
                order: 'ASC',
                doneLeadStatus: 1,
                loading: true,
                data: [],
                total: 0,
                doneCount: 0,
                page: 1,
                totalPages: 1,
                paginating: false,
                scrollToIndex: 0,

            },
        ],
    }
    // static navigationOptions = ({ navigation }) => {
    //     let title = "My Tasks";
    //     if (navigation && navigation.getParam("name") && navigation.getParam("id") != global.appUserId) {
    //         title = navigation.getParam("name") + "'s Tasks";
    //     }
    //     return {
    //         title
    //     };
    // };
    getUserId() {
        if (this.props.navigation && this.props.navigation.getParam("id")) {
            return this.props.navigation.getParam("id");
        }
        else {
            return global.appUserId;
        }
    }
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        console.log("Component Mounted::");
        //  this.props.navigation.setOptions({ title: 'HomeScreen Header' });
        this.getAllTasksData();

    }
    onClickAction(action, payload) {
        switch (action) {
            case "navigate_c360":
                if (Platform.OS == "android" && false) {
                    NativeModules.ReactNativeToAndroid.navigateToC360FromDetailsDashboard(
                        payload.leadId + "",
                        payload.scheduledActivityId + ""
                    );
                }
                else {
                    //NavigationService.navigate("C360MainScreen", { leadId: payload.leadId });
                    this.props.navigation.navigate("C360MainScreen", { leadId: payload.leadId });
                }
                break;
            case "call": {
                this.props.navigation.navigate('AutoDialog', {
                    data: {
                        title: "Are you sure want to call?\n" + payload.mobileNumber,
                        yes: "Yes",
                        no: "No"
                    },
                    onNoButtonPress: () => {

                    },
                    onYesButtonPress: () => {
                        Linking.openURL(`tel:${payload.mobileNumber}`);
                    },

                });
            }
            case "update": {
                NavigationService.navigate("FormRenderingRouter", {
                    leadId: payload.leadId, scheduledActivityId: payload.scheduledActivityId, actionId: payload.actionId,
                    form_title: payload.form_title, lead_last_updated: payload.leadLastUpdated, activity_id: payload.activityId
                });
                break;
            }
        }

    }
    failedCase() {
        // Alert.alert(
        //     'Failed',
        //     'Please try again',
        //     [
        //         { text: 'Retry', onPress: () => this.getAllTasksData() },
        //     ],
        //     { cancelable: false }
        // )
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.filtersUpdate) {
            this.props.updateFiltersUpdate(false);
            this.getAllTasksData();
        }
    }
    getAllTasksData() {
        let { tabs, page } = this.state;
        for (i = 0; i < tabs.length; i++) {
            tabs[i].loading = true;
            tabs[i].data = [];
            tabs[i].page = 1;
            tabs[i].totalPages = 1;
        }
        this.setState({ tabs })
        tabs.map((tab, index) => {
            this.getTaskList(
                index,
                tab.when,
                tab.order,
                tab.doneLeadStatus,
                tab.page
            );
        })

    }
    getDoneCount(tasks) {
        let count = 0;
        let today = new Date();
        today.setHours("0");
        today.setMinutes("0");
        today.setSeconds("0");
        today.setMilliseconds("0");
        tasks.map((task) => {
            if (DateUtils.isBefore(
                task.activity.activity_scheduled_date,
                Moment(today).add(1, 'days')
            )) {
                count++;
            }
        });
        return count;
    }
    getSelectedFilterServer() {
        //written like this just to match bad legacy backend code!!
        let filters = "";
        let selectedFilter = this.props.filters;
        selectedFilter.map((filter) => {
            let advanced_filter_selected = 0;
            switch (filter.key) {
                case "task_type":
                    filter.values.join().split(',').map(val => {
                        filters += `filter_activity_type%5B%5D=${val}&`
                    })
                    break;
                case "tags":
                    filters += `advanced_filters%5B${advanced_filter_selected}%5D%5BdisplayText%5D=Tags&`
                    filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bname%5D=tag&`
                    for (i = 0; i < filter.values.length; i++) {
                        let id, name, tag_id;
                        if (filter.values[i] == 1) {
                            id = 1;
                            name = "Hot";
                            tag_id = 6;
                        }
                        else if (filter.values[i] == 2) {
                            id = 2;
                            name = "Warm";
                            tag_id = 7;
                        }
                        else if (filter.values[i] == 3) {
                            id = 3;
                            name = "Cold";
                            tag_id = 8;
                        }
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Bid%5D=${id}&`
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Bname%5D=${name}&`
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Btag_id%5D=${tag_id}&`
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Bactive%5D=1&`
                    }
                    ++advanced_filter_selected;
                    break;
                case "vehicle_models":
                    filters += `advanced_filters%5B${advanced_filter_selected}%5D%5BdisplayText%5D=Interested+Cars&`
                    filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bname%5D=cars&`
                    for (i = 0; i < filter.values.length; i++) {
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Bid%5D=${filter.values[i]}&`
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Bname%5D=${filter.names[i].split(" ").join("+")}&`
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Bactive%5D=1&`
                    }
                    ++advanced_filter_selected;
                    break;
                case "lead_source":
                    filters += `advanced_filters%5B${advanced_filter_selected}%5D%5BdisplayText%5D=Lead+Source&`
                    filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bname%5D=leadSource&`
                    for (i = 0; i < filter.values.length; i++) {
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Bid%5D=${filter.values[i]}&`
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Bname%5D=${filter.names[i].split(" ").join("+")}&`
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Bactive%5D=1&`
                    }
                    ++advanced_filter_selected;
                    break;
                case "lead_age":
                    filters += `advanced_filters%5B${advanced_filter_selected}%5D%5BdisplayText%5D=Lead+Age&`
                    filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bname%5D=closeDate&`
                    for (i = 0; i < filter.values.length; i++) {
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Bname%5D=${filter.names[i]}&`
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B${i}%5D%5Bactive%5D=1&`
                    }
                    ++advanced_filter_selected;
                    break;
                case "lead_stage":
                    filter.values.map(val => {
                        filters += `filter_lead_stage%5B%5D=${val}&`
                    })
                    break;
                case "buyer_type":
                    break;
                case "date":
                    let filteredExpectedBuyingDates = filter.values.filter((val) => val.key == "expected_buying_date_from" || val.key == "expected_buying_date_to");
                    if (filteredExpectedBuyingDates.length > 0) {
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5BdisplayText%5D=Lead+Closing+Date&`
                        filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bname%5D=closeDate&`
                        if (filteredExpectedBuyingDates.length == 1 && filteredExpectedBuyingDates[0].key == "expected_buying_date_to") {
                            filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B0%5D%5Bname%5D=default&`
                            filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B0%5D%5Bactive%5D=1&`
                        }
                        filteredExpectedBuyingDates.map((val) => {
                            let dateValue = Utils.getReadableDate(val.value).split(" ").join("+")
                            if (val.key == "expected_buying_date_from") {
                                filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B0%5D%5Bname%5D=${dateValue}&`
                                filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B0%5D%5Bactive%5D=1&`
                            }
                            else {
                                filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B1%5D%5Bname%5D=${dateValue}&`
                                filters += `advanced_filters%5B${advanced_filter_selected}%5D%5Bvalue%5D%5B1%5D%5Bactive%5D=1&`
                            }
                        });

                    }
                    ++advanced_filter_selected;
                    break;

            }
        })
        if (filters.length > 0) {
            filters = "&" + filters.slice(0, filters.length - 1);
        }
        return filters;
    }

    getTaskList(tabIndex, when, order, isLeadStatusDone, page, isPagination) {
        let { tabs, dseId } = this.state;
        if (isPagination) {
            tabs[tabIndex].paginating = true;
            this.setState({ tabs })
        }
        new RestClient().getTasksList({
            page,
            when,
            order,
            dseId,
            isLeadStatusDone
        }, this.getSelectedFilterServer()).then((data) => {
            let { tabs } = this.state;
            tabs[tabIndex].data.push.apply(tabs[tabIndex].data, data.result.data);
            if (tabIndex == 0 && data.result.total_completed_today) {
                tabs[tabIndex].doneCount = data.result.total_completed_today;
            }
            tabs[tabIndex].total = data.result.total;
            tabs[tabIndex].page = Number(data.result.page);
            tabs[tabIndex].totalPages = data.result.total_pages;
            tabs[tabIndex].loading = false;
            tabs[tabIndex].paginating = false;
            // if (tabIndex == 2) {
            //     tabs[0].total = tabs[0].total + data.result.total;
            // }
            if (isPagination) {
                tabs[tabIndex].scrollToIndex = Number(data.result.page) * data.result.page_limit;
            }
            else {

            }
            this.setState({ tabs })


        }).catch((error) => {
            console.error(error);
            if (error.response && error.response.status == 401) {
                this.failedCase();
            }
        });
    }

    getTabs() {
        var tabsVal = {};
        let { tabs } = this.state;
        tabs.map((tab, index) => {
            let count = tab.total;
            if (index == 0) {
                count = tab.doneCount + "/" + (tab.total + tab.doneCount);
            }
            tabsVal[tab.tabName + " " + count] = {
                screen: props => (
                    <TasksTab
                        info={this.props.info}
                        onClickAction={this.onClickAction.bind(this)}
                        pagination={this.getTaskList.bind(this)}
                        refresh={this.getAllTasksData.bind(this)}
                        tab={tab}
                    />
                )
            };
        });
        return createAppContainer(
            createMaterialTopTabNavigator(tabsVal, {
                backBehavior: "none",
                swipeEnabled: true,
                tabBarOptions: {
                    style: { backgroundColor: "transperent", elevation: 0 },
                    labelStyle: {
                        fontSize: 13,
                        fontWeight: 'bold'
                    },
                    inactiveTintColor: "#81a8c8",
                    activeTintColor: "#fff",
                    upperCaseLabel: false,
                    indicatorStyle: { height: 0, backgroundColor: "#2e5e86" },
                }
            })
        );
    }
    render() {
        let TABS = this.getTabs();
        // let { loading } = this.state;
        return (
            <View style={styles.MainContainer}>
                <LinearGradient
                    colors={["#2e5e86", "#2e3486"]}
                    style={{ flex: 1, justifyContent: 'center' }}>
                    <TABS />
                    {
                        this.props.filters.length > 0 && (
                            <ClearFilterFooter navigation={this.props.navigation} />
                        )

                    }
                </LinearGradient>
            </View>
        )
    }
}
const mapStateToProps = state => {
    return {
        todaysTabTotal: state.tasksListReducer.todaysTabTotal,
        futureTabTotal: state.tasksListReducer.futureTabTotal,
        doneTabTotal: state.tasksListReducer.doneTabTotal,
        filters: state.tasksListReducer.filters,
        filtersUpdate: state.tasksListReducer.filtersUpdate,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateFilters: (filters, filtersUpdate) => {
            dispatch(updateTasksListFilter({ filters, filtersUpdate }))
        },
        updateFiltersUpdate: (filtersUpdate) => {
            dispatch(updateTasksListFilterUpdate({ filtersUpdate }))
        }
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(TasksListMain)