package com.salescrm.telephony.db.crm_user;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 11/1/17.
 */
public class UserNewCarDB extends RealmObject{
    private int leadNewCar;
    private int leadSource;
    private RealmList<TeamData> team_data;
    private RealmList<UsersDB> users;
    public int getLeadNewCar() {
        return leadNewCar;
    }

    public int getLeadSource() {
        return leadSource;
    }

    public void setLeadSource(int leadSource) {
        this.leadSource = leadSource;
    }

    public void setLeadNewCar(int leadNewCar) {
        this.leadNewCar = leadNewCar;
    }

    public RealmList<TeamData> getTeam_data() {
        return team_data;
    }

    public void setTeam_data(RealmList<TeamData> team_data) {
        this.team_data = team_data;
    }

    public RealmList<UsersDB> getUsers() {
        return users;
    }

    public void setUsers(RealmList<UsersDB> users) {
        this.users = users;
    }
}
