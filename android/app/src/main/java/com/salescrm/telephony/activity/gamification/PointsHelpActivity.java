package com.salescrm.telephony.activity.gamification;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.UserPointHelpResponse;
import com.salescrm.telephony.services.service_handlers.GamificationServiceHandler;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.views.AutoDialog;

import java.util.List;

public class PointsHelpActivity extends AppCompatActivity {
    private String userId = null;
    private Preferences pref;
    private RelativeLayout relLoader;
    private LinearLayout llBeforeTargetParent, llAfterTargetParent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Preferences.getInstance();
        pref.load(this);
        setContentView(R.layout.activity_points_help);
        relLoader = findViewById(R.id.rel_loading_frame);
        llBeforeTargetParent = findViewById(R.id.ll_points_details_before_target);
        llAfterTargetParent = findViewById(R.id.ll_points_details_after_target);
        if (getIntent() != null && getIntent().getExtras() != null) {
            userId = getIntent().getExtras().getString("user_id", null);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(Color.parseColor("#FF0F2B51"));
        }
        if (userId != null) {
            callApi();
        } else {
            System.out.println("User Id Is Missing");
        }

        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private void callApi() {
        relLoader.setVisibility(View.VISIBLE);
        GamificationServiceHandler.getInstance(this)
                .fetchUserHelpData(pref.getAccessToken(), userId, Util.getMonthYear(pref.getSelectedGameDate()), new CallBack() {
                    @Override
                    public void onCallBack() {

                    }

                    @Override
                    public void onCallBack(Object data) {
                        if (data != null && data instanceof UserPointHelpResponse) {
                            UserPointHelpResponse userPointHelpResponse = (UserPointHelpResponse) data;
                            showData(userPointHelpResponse.getResult());
                        }
                    }

                    @Override
                    public void onError(String error) {
                        if (!isFinishing()) {
                            new AutoDialog(PointsHelpActivity.this, new AutoDialogClickListener() {
                                @Override
                                public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
                                    callApi();
                                }

                                @Override
                                public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {
                                    PointsHelpActivity.this.finish();
                                }
                            }, new AutoDialogModel("Error while fetching the data", "Try Again", "Close")).show();
                        }
                    }
                });

    }

    private void showData(List<UserPointHelpResponse.Result> result) {
        llBeforeTargetParent.removeAllViews();
        llAfterTargetParent.removeAllViews();
        relLoader.setVisibility(View.GONE);

        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i).getName());
            System.out.println(result.get(i).getBase_point());
            UserPointHelpResponse.Result resultData = result.get(i);



            LinearLayout linearLayoutBefore= new LinearLayout(this);
            linearLayoutBefore.setOrientation(LinearLayout.VERTICAL);

            View viewBefore = LayoutInflater.from(this).inflate(R.layout.layout_help_points_before_target, linearLayoutBefore, false);

            TextView tvBeforeTargetActivity = viewBefore.findViewById(R.id.tv_help_before_target_activity);
            TextView tvBeforeTargetBase = viewBefore.findViewById(R.id.tv_help_before_target_base);
            TextView tvBeforeTargetValue = viewBefore.findViewById(R.id.tv_help_before_target_value);
            TextView tvBeforeTargetPointsPerActivity = viewBefore.findViewById(R.id.tv_help_before_target_points_per_activity);

            tvBeforeTargetActivity.setText(resultData.getName());
            tvBeforeTargetBase.setText(resultData.getBase_point() + "");
            tvBeforeTargetValue.setText(resultData.getTarget() + "");
            tvBeforeTargetPointsPerActivity.setText(resultData.getPoints_per_activity() + "");

            linearLayoutBefore.addView(viewBefore);
            if(i<result.size()-1) {
                View viewBeforeHr = LayoutInflater.from(this).inflate(R.layout.layout_hr, linearLayoutBefore, false);
                linearLayoutBefore.addView(viewBeforeHr);
            }

            llBeforeTargetParent.addView(linearLayoutBefore);



            LinearLayout linearLayoutAfter= new LinearLayout(this);
            linearLayoutAfter.setOrientation(LinearLayout.VERTICAL);
            View viewAfter = LayoutInflater.from(this).inflate(R.layout.layout_help_points_after_target, linearLayoutAfter, false);
            TextView tvAfterTargetActivity = viewAfter.findViewById(R.id.tv_help_after_target_activity);
            TextView tvAfterTargetPointsPerActivity = viewAfter.findViewById(R.id.tv_help_after_target_points_per_activity);

            tvAfterTargetActivity.setText(resultData.getName());
            tvAfterTargetPointsPerActivity.setText(resultData.getBase_point() + "");


            linearLayoutAfter.addView(viewAfter);
            if(i<result.size()-1) {
                View viewAfterHr = LayoutInflater.from(this).inflate(R.layout.layout_hr, linearLayoutAfter, false);
                linearLayoutAfter.addView(viewAfterHr);
            }
            llAfterTargetParent.addView(linearLayoutAfter);
        }


    }
}
