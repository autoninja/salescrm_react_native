import React, { Component } from 'react';
import { View, Text,TouchableWithoutFeedback, FlatList } from 'react-native';

import styles from './LostDetailsOEMDealerReasons.styles.js'

export default class LostDetailsOEMDealerReasons extends Component {

  constructor(props) {
    super(props);
  }

  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
}

  render() {
    let lostToDealer = "Dealer";
    let lostCount = 0;
    let reasons = [];
    if(this.props.navigation.getParam('data')) {
      lostToDealer = this.props.navigation.getParam('data').name;
      lostCount = this.props.navigation.getParam('data').leads_lost;
      if(this.props.navigation.getParam('data').sub_reasons) {
        reasons =  this.props.navigation.getParam('data').sub_reasons;
      }
    }
    return (
        <View style={styles.container}>
          <TouchableWithoutFeedback
          style={styles.top}
          onPress = {()=>this.handleBackButtonClick()}>
            <View style={styles.top}/>
          </TouchableWithoutFeedback>

          <View style={styles.bottom}>
            <Text style={styles.title}>
            {lostToDealer+" - "+lostCount}
            </Text>
            <Text style={styles.subTitle}s>(Lost Reason)</Text>
            <View style={styles.hr}/>

            {reasons.length==0 &&(
                <View style={{alignItems:'center',alignSelf:'center', flex:1, justifyContent: 'center',}}>
                  <Text style={{alignSelf:'center', fontSize:20}}> No Data </Text>
                </View>
              )
            }
            {reasons.length>0 && (
              <FlatList
                style={{alignSelf:'stretch'}}
                data = {reasons}
                renderItem = {({item, index}) => {
                                return (
                                  <View style={styles.itemMain}>
                                    <View style={styles.itemInner}>
                                      <Text style={styles.name}>{item.name?item.name.trim():''}</Text>
                                      <Text style={styles.name}>{item.leads_lost}</Text>
                                    </View>
                                    <View style={styles.hrItem}/>
                                  </View>
                                );
                                }
                             }
                keyExtractor={(item, index) => index+''}
              />
            )
            }



        </View>
      </View>



    );
  }
}
