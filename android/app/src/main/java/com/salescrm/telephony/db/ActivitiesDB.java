package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 31/8/16.
 */
public class ActivitiesDB extends RealmObject {

    @PrimaryKey
    private int id;

    private int category;
    private String name;

    public ActivitiesDB() {
    }

    public ActivitiesDB(int id, int category, int common_id, String name) {
        this.id = id;
        this.category = category;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }


}
