package com.salescrm.telephony.db.team_dashboard_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by prateek on 20/9/17.
 */

public class EtvbrCarsAbsolute extends RealmObject {
    @SerializedName("modelName")
    @Expose
    private String modelName;
    @SerializedName("modelId")
    @Expose
    private Integer modelId;
    @SerializedName("enquiries")
    @Expose
    private Integer enquiries;
    @SerializedName("tdrives")
    @Expose
    private Integer tdrives;
    @SerializedName("visits")
    @Expose
    private Integer visits;
    @SerializedName("bookings")
    @Expose
    private Integer bookings;
    @SerializedName("retail")
    @Expose
    private Integer retail;

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getEnquiries() {
        return enquiries;
    }

    public void setEnquiries(Integer enquiries) {
        this.enquiries = enquiries;
    }

    public Integer getTdrives() {
        return tdrives;
    }

    public void setTdrives(Integer tdrives) {
        this.tdrives = tdrives;
    }

    public Integer getVisits() {
        return visits;
    }

    public void setVisits(Integer visits) {
        this.visits = visits;
    }

    public Integer getBookings() {
        return bookings;
    }

    public void setBookings(Integer bookings) {
        this.bookings = bookings;
    }

    public Integer getRetail() {
        return retail;
    }

    public void setRetail(Integer retail) {
        this.retail = retail;
    }
}
