package com.salescrm.telephony.model;

/**
 * Created by bharath on 1/9/16.
 */
public class AddLeadActivityInputData {
    private CreateLeadInputData.Activity_data activity_data;
    private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public AddLeadActivityInputData(CreateLeadInputData.Activity_data activity_data,String remarks) {
        this.activity_data = activity_data;
        this.remarks = remarks;
    }

    public CreateLeadInputData.Activity_data getActivity_data() {
        return activity_data;
    }

    public void setActivity_data(CreateLeadInputData.Activity_data activity_data) {
        this.activity_data = activity_data;
    }
}
