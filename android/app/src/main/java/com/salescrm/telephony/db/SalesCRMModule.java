package com.salescrm.telephony.db;

import com.salescrm.telephony.db.car.ExchangeCarDetails;

import io.realm.annotations.RealmModule;

/**
 * Created by Ravindra P on 23-11-2016.
 */

@RealmModule(classes = {ActivitiesDB.class, ActivityDetails.class,AllCarsDB.class,BrandsDB.class,BuyerTypeDB.class,
        CustomerDetails.class,CustomerEmailId.class,CustomerPhoneNumbers.class,DseDetails.class,EnqSourceDB.class,
        ExchangeCarDetails.class,ExchangeStatusdb.class,FilterActivityType.class,FilterEnquiryStage.class,FormAnswerDB.class,
        FormAnswerValueDB.class,FormObjectAnswerChildren.class,FormObjectDb.class,FormObjectQuestionChildren.class,
        FormObjectQuestionValues.class,FormObjectQuestionValuesDefaultFAId.class,FormResponseDB.class,IntrestedCarActivityGroup.class,
        IntrestedCarActivityGroupStages.class,IntrestedCarDetails.class,LeadDetails.class,LeadHistory.class,LocationsDB.class,
        PlannedActivities.class,PlannedActivitiesAction.class,PlannedActivitiesDetail.class,SalesCRMRealmTable.class,UserDetails.class})
public class SalesCRMModule {
}
