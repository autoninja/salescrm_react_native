package com.salescrm.telephony.interfaces;

/**
 * Created by prateek on 3/3/17.
 */

public interface OnLoadLeadListListener  {

    void onLoadMoreLeads();
}
