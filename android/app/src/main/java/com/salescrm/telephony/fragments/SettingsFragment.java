package com.salescrm.telephony.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.SettingsActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;

import java.util.HashMap;


/**
 * Created by bharath on 22/01/19.
 */

public class SettingsFragment extends Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedLayout = inflater.inflate(R.layout.fragment_settings, container, false);
        inflatedLayout.findViewById(R.id.card_settings_email_sms).setOnClickListener(this);
        return inflatedLayout;
    }

    @Override
    public void onClick(View v) {
        String module = null;
        switch (v.getId()) {
            case R.id.card_settings_email_sms :
                module = "SMS_EMAIL_SETTINGS";
                break;
        }
        Context context = null;
        if(getContext()!=null) {
            context = getContext();
        }
        else if(getActivity()!=null) {
            context = getActivity();
        }
        else if(SalesCRMApplication.GetAppContext()!=null) {
            context = SalesCRMApplication.GetAppContext();
        }
        if(context!=null &&  null != module) {
            Intent intent = new Intent(getContext(), SettingsActivity.class);
            intent.putExtra("MODULE", module);
            startActivity(intent);
            pushCleverTap();
        }




    }

    private void pushCleverTap() {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_SETTINGS_KEY_TYPE,
                CleverTapConstants.EVENT_SETTINGS_TYPE_SMS_EMAIL);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_SETTINGS, hashMap);
    }
}