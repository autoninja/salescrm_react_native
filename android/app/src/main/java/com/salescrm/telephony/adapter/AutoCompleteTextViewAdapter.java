package com.salescrm.telephony.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.model.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bharath on 8/3/18.
 */

public class AutoCompleteTextViewAdapter extends ArrayAdapter<Item> implements Filterable {

    private ArrayList<Item> fullList;
    private ArrayList<Item> mOriginalValues;
    private ArrayFilter mFilter;

    public AutoCompleteTextViewAdapter(Context context, List<Item> objects) {

        super(context, R.layout.dialog_textview, R.id.item, objects);
        fullList = (ArrayList<Item>) objects;
        mOriginalValues = new ArrayList<Item>(fullList);

    }

    @Override
    public int getCount() {
        return fullList.size();
    }

    @Override
    public Item getItem(int position) {
        return fullList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item item = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_textview, parent, false);
        }
        TextView txt1 = (TextView) convertView.findViewById(R.id.item);
        txt1.setText(item.getText());
        convertView.setTag(item);
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ArrayFilter();
        }
        return mFilter;
    }


    private class ArrayFilter extends Filter {
        private Object lock;

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (mOriginalValues == null) {
                mOriginalValues = new ArrayList<Item>(fullList);
            }

            if (prefix == null || prefix.length() == 0) {
                    ArrayList<Item> list = new ArrayList<Item>(mOriginalValues);
                    results.values = list;
                    results.count = list.size();
            } else {
                final String prefixString = prefix.toString().toLowerCase();

                ArrayList<Item> values = mOriginalValues;
                int count = values.size();

                ArrayList<Item> newValues = new ArrayList<Item>(count);

                for (int i = 0; i < count; i++) {
                    Item item = values.get(i);
                    if (item.getText().toLowerCase().contains(prefixString)) {
                        newValues.add(item);
                    }

                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            if(results.values!=null){
                fullList = (ArrayList<Item>) results.values;
            }else{
                fullList = new ArrayList<Item>();
            }
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}