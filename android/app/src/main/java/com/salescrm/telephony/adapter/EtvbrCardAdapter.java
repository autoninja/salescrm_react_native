package com.salescrm.telephony.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.DetailsOfEtvbr;
import com.salescrm.telephony.activity.EtvbrImgDialog;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.db.etvbr_location.Info;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.db.etvbr_location.SalesConsultant;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.EtvbrLocationChildFragment;
import com.salescrm.telephony.fragments.EtvbrLocationFragment;
import com.salescrm.telephony.utils.CircleTransform;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.picasso.Picasso;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by prateek on 26/6/17.
 */

public class EtvbrCardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private Realm realm;
    private Location locationsResult;
    private RealmList<SalesConsultant> salesConsultants;

    public EtvbrCardAdapter(FragmentActivity activity, Realm realm, Location results) {
        this.context = activity;
        this.realm = realm;
        this.locationsResult = results;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.etvbr_card_adapter, parent, false);
        return new EtvbrCardHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final RecyclerView.ViewHolder iViewHolder = holder;

        if(EtvbrLocationFragment.isLocationAvailable()) {
            if (EtvbrLocationChildFragment.PERCENT) {
                viewForRel(holder, position);
            } else {
                viewForAbsolute(holder, position);
            }
        }else{
            if (EtvbrLocationFragment.PERCENT) {
                viewForRel(holder, position);
            } else {
                viewForAbsolute(holder, position);
            }
        }

    }

    private void viewForRel(RecyclerView.ViewHolder holder, final int pos) {
        EtvbrCardHolder cardHolder = (EtvbrCardHolder) holder;
        final int position = pos;
        salesConsultants = locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getSalesConsultants();

        cardHolder.tvTargetEnquiry.setVisibility(View.GONE);
        cardHolder.tvTargetTd.setVisibility(View.GONE);
        cardHolder.tvTargetVisit.setVisibility(View.GONE);
        cardHolder.tvTargetBooking.setVisibility(View.GONE);
        cardHolder.tvTargetRetail.setVisibility(View.GONE);
        cardHolder.viewTarget.setVisibility(View.GONE);
        cardHolder.tvTarget.setVisibility(View.GONE);
        cardHolder.rlFrameLayout.setPadding(0, 6, 0, 6);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_grey, null);
        cardHolder.tvTarget.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        if (pos % 2 == 0) {
            cardHolder.llaLinearLayout.setBackgroundColor(Color.parseColor("#243F6D"));
        } else {
            //Transparent
            cardHolder.llaLinearLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        if (pos < salesConsultants.size()) {

            cardHolder.tvEnquiry.setPaintFlags(0);
            cardHolder.tvEnquiry.setText("" + salesConsultants.get(pos).getInfo().getRel().getEnquiries());

            cardHolder.tvTestDrive.setPaintFlags(0);
            cardHolder.tvTestDrive.setText("" + salesConsultants.get(pos).getInfo().getRel().getTdrives());

            cardHolder.tvVisit.setPaintFlags(0);
            cardHolder.tvVisit.setText("" + salesConsultants.get(pos).getInfo().getRel().getVisits());

            cardHolder.tvBooking.setPaintFlags(0);
            cardHolder.tvBooking.setText("" + salesConsultants.get(pos).getInfo().getRel().getBookings());

            cardHolder.tvRetail.setPaintFlags(0);
            cardHolder.tvRetail.setText("" + salesConsultants.get(pos).getInfo().getRel().getRetails());

            cardHolder.tvLost.setText(""+salesConsultants.get(pos).getInfo().getRel().getLost_drop());

            cardHolder.tvEnquiry.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvTestDrive.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvVisit.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvBooking.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvRetail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvEnquiry.setVisibility(View.GONE);
            cardHolder.viewEnquiry.setVisibility(View.GONE);
            cardHolder.llEnquiry.setVisibility(View.GONE);

            if (salesConsultants.get(pos).getInfo().getDpUrl() != null && !salesConsultants.get(pos).getInfo().getDpUrl().equalsIgnoreCase("")) {
                String url =salesConsultants.get(pos).getInfo().getDpUrl();
                cardHolder.imgUser.setVisibility(View.VISIBLE);
                cardHolder.tvName.setVisibility(View.GONE);
                cardHolder.tvUserTotal.setVisibility(View.GONE);
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUser);
            } else {
                if (salesConsultants.get(pos).getInfo().getName() != null && !salesConsultants.get(pos).getInfo().getName().equalsIgnoreCase("")) {
                    cardHolder.tvName.setText(salesConsultants.get(pos).getInfo().getName().charAt(0) + "");
                } else {
                    cardHolder.tvName.setText("N");
                }
                cardHolder.imgUser.setVisibility(View.GONE);
                cardHolder.tvName.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotal.setVisibility(View.GONE);
            }



            cardHolder.viewEnquiry.setVisibility(View.GONE);
            cardHolder.viewTd.setVisibility(View.VISIBLE);
            cardHolder.viewVisit.setVisibility(View.VISIBLE);
            cardHolder.viewBooking.setVisibility(View.VISIBLE);
            cardHolder.viewReatil.setVisibility(View.VISIBLE);
            cardHolder.viewLost.setVisibility(View.VISIBLE);

            // Text Normal
            cardHolder.tvEnquiry.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvTestDrive.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvVisit.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvBooking.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvRetail.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvUserTotal.setTypeface(null, Typeface.NORMAL);

            if (DbUtils.isBike()){
                ((EtvbrCardHolder) holder).llVisit.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).llRetail.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).viewReatil.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).viewVisit.setVisibility(View.GONE);
            }else {
                ((EtvbrCardHolder) holder).llVisit.setVisibility(View.VISIBLE);
                ((EtvbrCardHolder) holder).llRetail.setVisibility(View.VISIBLE);
                ((EtvbrCardHolder) holder).viewReatil.setVisibility(View.VISIBLE);
                ((EtvbrCardHolder) holder).viewVisit.setVisibility(View.VISIBLE);
            }
        }else {

            cardHolder.tvEnquiry.setPaintFlags(0);
            cardHolder.tvEnquiry.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getEnquiries());

            cardHolder.tvTestDrive.setPaintFlags(0);
            cardHolder.tvTestDrive.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getTdrives());

            cardHolder.tvVisit.setPaintFlags(0);
            cardHolder.tvVisit.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getVisits());

            cardHolder.tvBooking.setPaintFlags(0);
            cardHolder.tvBooking.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getBookings());

            cardHolder.tvRetail.setPaintFlags(0);
            cardHolder.tvRetail.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getRetails());

            cardHolder.tvLost.setText(""+ locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getLost_drop());

            cardHolder.tvEnquiry.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvTestDrive.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvVisit.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvBooking.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvRetail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.imgUser.setVisibility(View.GONE);
            cardHolder.tvName.setVisibility(View.GONE);
            cardHolder.tvUserTotal.setText("Total%");
            cardHolder.tvUserTotal.setVisibility(View.VISIBLE);

            cardHolder.viewEnquiry.setVisibility(View.GONE);
            cardHolder.viewTd.setVisibility(View.GONE);
            cardHolder.viewVisit.setVisibility(View.GONE);
            cardHolder.viewBooking.setVisibility(View.GONE);
            cardHolder.viewReatil.setVisibility(View.GONE);
            cardHolder.viewLost.setVisibility(View.GONE);
            cardHolder.llEnquiry.setVisibility(View.GONE);

            // Text Bold
            cardHolder.tvEnquiry.setTypeface(null, Typeface.BOLD);
            cardHolder.tvTestDrive.setTypeface(null, Typeface.BOLD);
            cardHolder.tvVisit.setTypeface(null, Typeface.BOLD);
            cardHolder.tvBooking.setTypeface(null, Typeface.BOLD);
            cardHolder.tvRetail.setTypeface(null, Typeface.BOLD);
            cardHolder.tvUserTotal.setTypeface(null, Typeface.BOLD);

            if (DbUtils.isBike()){
                ((EtvbrCardHolder) holder).llVisit.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).llRetail.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).viewReatil.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).viewVisit.setVisibility(View.GONE);
            }else {
                ((EtvbrCardHolder) holder).llVisit.setVisibility(View.VISIBLE);
                ((EtvbrCardHolder) holder).llRetail.setVisibility(View.VISIBLE);
                ((EtvbrCardHolder) holder).viewReatil.setVisibility(View.INVISIBLE);
                ((EtvbrCardHolder) holder).viewVisit.setVisibility(View.INVISIBLE);
            }
        }

        cardHolder.imgUser.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return openImageRel(salesConsultants.get(pos).getInfo());
            }
        });


        cardHolder.tvName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return openImageRel(salesConsultants.get(pos).getInfo());
            }
        });
    }

    boolean openImageRel(Info info){
        Intent intent = new Intent(context, EtvbrImgDialog.class);
        Bundle bundle = new Bundle();
        bundle.putString("imgUrlEtvbr", info.getDpUrl());
        bundle.putString("dseNameEtvbr", info.getName());
        intent.putExtras(bundle);
        context.startActivity(intent);
        return true;
    }
    private void viewForAbsolute(RecyclerView.ViewHolder holder, final int pos) {
        EtvbrCardHolder cardHolder = (EtvbrCardHolder) holder;
        final int position = pos;

        cardHolder.tvTargetEnquiry.setVisibility(View.VISIBLE);
        cardHolder.tvTargetTd.setVisibility(View.VISIBLE);
        cardHolder.tvTargetVisit.setVisibility(View.VISIBLE);
        cardHolder.tvTargetBooking.setVisibility(View.VISIBLE);
        cardHolder.tvTargetRetail.setVisibility(View.VISIBLE);
        cardHolder.viewTarget.setVisibility(View.VISIBLE);

        cardHolder.rlFrameLayout.setPadding(0, 6, 0, 0);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_grey, null);
        cardHolder.tvTarget.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        if (pos % 2 == 0) {
            //cardHolder.llaLinearLayout.setBackgroundColor(Color.parseColor("#243F6D"));
            cardHolder.llaLinearLayout.setBackgroundColor(Color.TRANSPARENT);
        } else {
            //Transparent
            cardHolder.llaLinearLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        if (pos < salesConsultants.size()) {

            cardHolder.tvEnquiry.setPaintFlags(cardHolder.tvEnquiry.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvEnquiry.setText("" + salesConsultants.get(pos).getInfo().getAbs().getEnquiries());

            cardHolder.tvTestDrive.setPaintFlags(cardHolder.tvTestDrive.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvTestDrive.setText("" + salesConsultants.get(pos).getInfo().getAbs().getTdrives());

            cardHolder.tvVisit.setPaintFlags(cardHolder.tvVisit.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvVisit.setText("" + salesConsultants.get(pos).getInfo().getAbs().getVisits());

            cardHolder.tvBooking.setPaintFlags(cardHolder.tvBooking.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvBooking.setText("" + salesConsultants.get(pos).getInfo().getAbs().getBookings());

            cardHolder.tvRetail.setPaintFlags(cardHolder.tvRetail.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvRetail.setText("" + salesConsultants.get(pos).getInfo().getAbs().getRetails());

            cardHolder.tvLost.setPaintFlags(cardHolder.tvLost.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvLost.setText(""+ salesConsultants.get(pos).getInfo().getAbs().getLost_drop());

            cardHolder.tvEnquiry.setVisibility(View.VISIBLE);
            cardHolder.llEnquiry.setVisibility(View.VISIBLE);

            cardHolder.tvTargetEnquiry.setText(""+salesConsultants.get(pos).getInfo().getTargets().getEnquiries());
            cardHolder.tvTargetTd.setText(""+salesConsultants.get(pos).getInfo().getTargets().getTdrives());
            cardHolder.tvTargetVisit.setText(""+salesConsultants.get(pos).getInfo().getTargets().getVisits());
            cardHolder.tvTargetBooking.setText(""+salesConsultants.get(pos).getInfo().getTargets().getBookings());
            cardHolder.tvTargetRetail.setText(""+salesConsultants.get(pos).getInfo().getTargets().getRetails());


            if (salesConsultants.get(pos).getInfo().getDpUrl() != null && !salesConsultants.get(pos).getInfo().getDpUrl().equalsIgnoreCase("")) {
                String url = salesConsultants.get(pos).getInfo().getDpUrl();
                cardHolder.imgUser.setVisibility(View.VISIBLE);
                cardHolder.tvName.setVisibility(View.GONE);
                cardHolder.tvUserTotal.setVisibility(View.GONE);
                cardHolder.tvTarget.setVisibility(View.GONE);
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUser);
            } else {
                if (salesConsultants.get(pos).getInfo().getName() != null && !salesConsultants.get(pos).getInfo().getName().equalsIgnoreCase("")) {
                    cardHolder.tvName.setText(salesConsultants.get(pos).getInfo().getName().charAt(0) + "");
                } else {
                    cardHolder.tvName.setText("N");
                }
                cardHolder.imgUser.setVisibility(View.GONE);
                cardHolder.tvName.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotal.setVisibility(View.GONE);
                cardHolder.tvTarget.setVisibility(View.GONE);
            }



            cardHolder.viewEnquiry.setVisibility(View.VISIBLE);
            cardHolder.viewTd.setVisibility(View.VISIBLE);
            cardHolder.viewVisit.setVisibility(View.VISIBLE);
            cardHolder.viewBooking.setVisibility(View.VISIBLE);
            cardHolder.viewReatil.setVisibility(View.VISIBLE);
            cardHolder.viewLost.setVisibility(View.VISIBLE);
            cardHolder.tvTarget.setVisibility(View.INVISIBLE);

            // Text Normal
            cardHolder.tvEnquiry.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvTestDrive.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvVisit.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvBooking.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvRetail.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvLost.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvUserTotal.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvTarget.setTypeface(null, Typeface.NORMAL);


            cardHolder.tvEnquiry.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(0,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getEnquiries()
                            ,""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvTestDrive.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(1,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getTdrives(),
                            ""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvVisit.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(2,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getVisits(),
                            ""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvBooking.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(3,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getBookings(),
                            ""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvRetail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getRetails(),
                            ""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvLost.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getLost_drop(),
                            ""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            if (DbUtils.isBike()){
                ((EtvbrCardHolder) holder).llVisit.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).llRetail.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).viewReatil.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).viewVisit.setVisibility(View.GONE);
            }else {
                ((EtvbrCardHolder) holder).llVisit.setVisibility(View.VISIBLE);
                ((EtvbrCardHolder) holder).llRetail.setVisibility(View.VISIBLE);
                ((EtvbrCardHolder) holder).viewReatil.setVisibility(View.VISIBLE);
                ((EtvbrCardHolder) holder).viewVisit.setVisibility(View.VISIBLE);
            }

        }else{
            if(locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo()!=null) {
                cardHolder.tvEnquiry.setPaintFlags(cardHolder.tvEnquiry.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvEnquiry.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getEnquiries());

                cardHolder.tvTestDrive.setPaintFlags(cardHolder.tvTestDrive.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvTestDrive.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getTdrives());

                cardHolder.tvVisit.setPaintFlags(cardHolder.tvVisit.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvVisit.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getVisits());

                cardHolder.tvBooking.setPaintFlags(cardHolder.tvBooking.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvBooking.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getBookings());

                cardHolder.tvRetail.setPaintFlags(cardHolder.tvRetail.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvRetail.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getRetails());

                cardHolder.tvLost.setPaintFlags(cardHolder.tvLost.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvLost.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getLost_drop());

            }
            cardHolder.tvEnquiry.setVisibility(View.VISIBLE);
            cardHolder.llEnquiry.setVisibility(View.VISIBLE);
            if(locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo()!=null) {
                cardHolder.tvTargetEnquiry.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getTargets().getEnquiries());
                cardHolder.tvTargetTd.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getTargets().getTdrives());
                cardHolder.tvTargetVisit.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getTargets().getVisits());
                cardHolder.tvTargetBooking.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getTargets().getBookings());
                cardHolder.tvTargetRetail.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getTargets().getRetails());
            }
            cardHolder.imgUser.setVisibility(View.GONE);
            cardHolder.tvName.setVisibility(View.GONE);
            cardHolder.tvUserTotal.setText("Achieved");
            cardHolder.tvUserTotal.setVisibility(View.VISIBLE);
            cardHolder.tvTarget.setVisibility(View.VISIBLE);
            cardHolder.tvTarget.setText("Target");

            cardHolder.viewEnquiry.setVisibility(View.GONE);
            cardHolder.viewTd.setVisibility(View.GONE);
            cardHolder.viewVisit.setVisibility(View.GONE);
            cardHolder.viewBooking.setVisibility(View.GONE);
            cardHolder.viewReatil.setVisibility(View.GONE);
            cardHolder.viewLost.setVisibility(View.GONE);
            cardHolder.tvTarget.setVisibility(View.VISIBLE);

            // Text Bold
            cardHolder.tvEnquiry.setTypeface(null, Typeface.BOLD);
            cardHolder.tvTestDrive.setTypeface(null, Typeface.BOLD);
            cardHolder.tvVisit.setTypeface(null, Typeface.BOLD);
            cardHolder.tvBooking.setTypeface(null, Typeface.BOLD);
            cardHolder.tvRetail.setTypeface(null, Typeface.BOLD);
            cardHolder.tvLost.setTypeface(null, Typeface.BOLD);
            cardHolder.tvUserTotal.setTypeface(null, Typeface.BOLD);
            cardHolder.tvTarget.setTypeface(null, Typeface.NORMAL);

            cardHolder.tvEnquiry.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(0,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getEnquiries()
                            ,""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvTestDrive.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(1,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getTdrives(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvVisit.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(2,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getVisits(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvBooking.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(3,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getBookings(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvRetail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getRetails(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvLost.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getLost_drop(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            if (DbUtils.isBike()){
                ((EtvbrCardHolder) holder).llVisit.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).llRetail.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).viewReatil.setVisibility(View.GONE);
                ((EtvbrCardHolder) holder).viewVisit.setVisibility(View.GONE);
            }else {
                ((EtvbrCardHolder) holder).llVisit.setVisibility(View.VISIBLE);
                ((EtvbrCardHolder) holder).llRetail.setVisibility(View.VISIBLE);
                ((EtvbrCardHolder) holder).viewReatil.setVisibility(View.INVISIBLE);
                ((EtvbrCardHolder) holder).viewVisit.setVisibility(View.INVISIBLE);
            }

        }

        cardHolder.imgUser.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return openImageAbsolute(salesConsultants.get(pos).getInfo());
            }
        });

        cardHolder.tvName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return openImageAbsolute(salesConsultants.get(pos).getInfo());
            }
        });

    }

    boolean openImageAbsolute(Info info){
        Intent intent = new Intent(context, EtvbrImgDialog.class);
        Bundle bundle = new Bundle();
        bundle.putString("imgUrlEtvbr", info.getDpUrl());
        bundle.putString("dseNameEtvbr", info.getName());
        intent.putExtras(bundle);
        context.startActivity(intent);
        return true;
    }
    @Override
    public int getItemCount() {
        if(locationsResult.isValid()
                && locationsResult.getSalesManagers().isValid()
                && locationsResult.getSalesManagers().get(0).getTeamLeaders().isValid()
                && locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getSalesConsultants().isValid()) {
            salesConsultants = locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getSalesConsultants();
            return isUserOnlyDse()?(salesConsultants.size() > 0 ? salesConsultants.size() : 0):(salesConsultants.size() > 0 ? salesConsultants.size() + 1 : 0);
        }else {
            return 0;
        }

    }

    public boolean isUserOnlyDse () {
        boolean isUserOnlyDse =false;
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            if(userRoles!=null && userRoles.size() == 1) {
                isUserOnlyDse = (Util.getInt(userRoles.get(0).getId()) == WSConstants.USER_ROLES.DSE);
            }
        }
        return isUserOnlyDse;
    }

    public class EtvbrCardHolder extends RecyclerView.ViewHolder{

        ImageView imgUser;
        TextView tvName, tvEnquiry, tvTestDrive, tvVisit, tvBooking, tvRetail, tvLost;
        View viewEnquiry, viewTd, viewVisit, viewBooking, viewReatil, viewLost;
        LinearLayout llaLinearLayout;
        TextView tvUserTotal;
        LinearLayout llEnquiry, llTd, llVisit, llBooking, llRetail, llLost;
        TextView tvTarget;
        TextView tvTargetEnquiry, tvTargetTd, tvTargetVisit, tvTargetBooking, tvTargetRetail;
        View viewTarget;
        RelativeLayout rlFrameLayout;

        public EtvbrCardHolder(View itemView) {
            super(itemView);

            imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            tvName = (TextView) itemView.findViewById(R.id.text_user);
            tvEnquiry = (TextView) itemView.findViewById(R.id.txt_enquiry);
            tvTestDrive = (TextView) itemView.findViewById(R.id.txt_td);
            tvVisit = (TextView) itemView.findViewById(R.id.txt_visit);
            tvBooking = (TextView) itemView.findViewById(R.id.txt_booking);
            tvRetail = (TextView) itemView.findViewById(R.id.txt_retail);
            llaLinearLayout = (LinearLayout) itemView.findViewById(R.id.ll_today_summary_main);
            tvUserTotal = (TextView) itemView.findViewById(R.id.text_user_total);
            viewEnquiry = (View) itemView.findViewById(R.id.enquiry_view);
            viewTd = (View) itemView.findViewById(R.id.td_view);
            viewVisit = (View) itemView.findViewById(R.id.visit_view);
            viewBooking = (View) itemView.findViewById(R.id.booking_view);
            viewReatil = (View)  itemView.findViewById(R.id.retail_view);
            llEnquiry = (LinearLayout) itemView.findViewById(R.id.enquiry_ll);
            tvTarget = (TextView) itemView.findViewById(R.id.tv_target_title);
            tvTargetEnquiry = (TextView) itemView.findViewById(R.id.txt_target_enquiry);
            tvTargetTd = (TextView) itemView.findViewById(R.id.txt_target_td);
            tvTargetVisit = (TextView) itemView.findViewById(R.id.txt_target_visit);
            tvTargetBooking = (TextView) itemView.findViewById(R.id.txt_target_booking);
            tvTargetRetail = (TextView) itemView.findViewById(R.id.txt_target_retail);
            viewTarget = (View) itemView.findViewById(R.id.target_view);
            rlFrameLayout = (RelativeLayout) itemView.findViewById(R.id.rl_frame_layout);
            llTd = (LinearLayout) itemView.findViewById(R.id.td_ll);
            llVisit = (LinearLayout) itemView.findViewById(R.id.visit_ll);
            llBooking = (LinearLayout) itemView.findViewById(R.id.booking_ll);
            llRetail = (LinearLayout) itemView.findViewById(R.id.retail_ll);
            llLost = (LinearLayout) itemView.findViewById(R.id.lost_ll);
            tvLost = (TextView) itemView.findViewById(R.id.txt_lost);
            viewLost = (View) itemView.findViewById(R.id.lost_view);
        }
    }

    private void goToDetailsOfEtvbr(int position, String title_name, String user_id, String etvbrtotal, String user_role_id, String user_location_id) {

        Intent etvbrDetails =  null;

        switch (position){
            case 0:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Enquiries");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 1:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Test Drives");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 2:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Visits");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 3:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Bookings");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 4:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Retails");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 5:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Lost Drop");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
        }
        if(etvbrDetails != null)
            context.startActivity(etvbrDetails);
    }


}
