import React, { Component } from "react";
import { View, FlatList, Text, TouchableOpacity } from "react-native";
import ColdVisitItem from "./ColdVisitItem";
import ColdVisitTotal from "./ColdVisitTotal";

const USER_TYPE = {
  SC: 4,
  TL: 6,
  SM: 8,
  BH: 21,
  OPS: 30
};

export default class ColdVisitDashboardChild extends Component {
  constructor(props) {
    super(props);
    this.state = { isRefreshing: false };
  }
  refresh() {
    this.setState({ isRefreshing: true });
    this.props.refresh();
    console.log("IS USER BH:" + this.props.isUserBH);
  }
  getChild(item) {
    let location_id = this.props.data.location_id;
    let returnArray = [];
    if (item.users && item.users.length > 0) {
      returnArray = item.users.map(item => {
        return {
          user_id: item.id,
          user_role_id: item.user_role_id,
          name: item.name,
          image_url: item.image_url,
          count: item.count,
          location_id
        };
      });
    } else if (item.team_leaders && item.team_leaders.length > 0) {
      returnArray = item.team_leaders.map(item => {
        return {
          user_id: item.team_leader_id,
          user_role_id: item.user_role_id,
          name: item.team_leader_name,
          image_url: item.image_url,
          count: item.count,
          location_id,
          child: this.getChild(item)
        };
      });
    }
    return returnArray;
  }
  getNoDataView() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          backgroundColor: "#fff",
          justifyContent: "center"
        }}
      >
        <Text>No data</Text>
        <TouchableOpacity
          onPress={() => {
            this.refresh();
          }}
        >
          <Text
            style={{
              marginTop: 10,
              padding: 10,
              borderColor: "#c4c4c4",
              borderRadius: 4,
              borderWidth: 1
            }}
          >
            {" "}
            Reload{" "}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  render() {
    let data = this.props.data;
    let location_id = this.props.data.location_id;
    let user_role_id = this.props.user_role_id;
    let flatList = this.getNoDataView();
    if (this.props.isUserBH) {
      let listSMs = [];
      if (data && data.managers && data.managers.length > 0)
        listSMs = data.managers;
      //show sms
      flatList = (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={listSMs}
          renderItem={({ item, index }) => {
            return (
              <View>
                <ColdVisitItem
                  onLongPress={this.props.onLongPress.bind(this)}
                  info={{
                    user_id: item.manager_id,
                    user_role_id: item.user_role_id,
                    name: item.name,
                    image_url: item.image_url,
                    count: item.count,
                    location_id,
                    child: this.getChild(item)
                  }}
                />
                {index == listSMs.length - 1 && (
                  <ColdVisitTotal
                    onLongPress={this.props.onLongPress.bind(this)}
                    info={{
                      user_id: global.appUserId,
                      user_role_id: user_role_id,
                      count: data.count,
                      location_id
                    }}
                  />
                )}
              </View>
            );
          }}
          keyExtractor={(item, index) => index + ""}
          onRefresh={() => this.refresh()}
          refreshing={this.state.isRefreshing}
        />
      );
    } else if (this.props.isUserSM) {
      let listTLs = [];
      if (
        data &&
        data.managers &&
        data.managers.length > 0 &&
        data.managers[0] &&
        data.managers[0].team_leaders &&
        data.managers[0].team_leaders.length > 0
      )
        listTLs = data.managers[0].team_leaders;
      //show tls
      flatList = (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={listTLs}
          renderItem={({ item, index }) => {
            return (
              <View>
                <ColdVisitItem
                  onLongPress={this.props.onLongPress.bind(this)}
                  info={{
                    user_id: item.team_leader_id,
                    user_role_id: item.user_role_id,
                    name: item.team_leader_name,
                    image_url: item.image_url,
                    count: item.count,
                    location_id,
                    child: this.getChild(item)
                  }}
                />
                {index == listTLs.length - 1 && (
                  <ColdVisitTotal
                    onLongPress={this.props.onLongPress.bind(this)}
                    info={{
                      user_id: global.appUserId,
                      user_role_id: user_role_id,
                      count: data.managers[0].count,
                      location_id
                    }}
                  />
                )}
              </View>
            );
          }}
          keyExtractor={(item, index) => index + ""}
          onRefresh={() => this.refresh()}
          refreshing={this.state.isRefreshing}
        />
      );
    } else if (this.props.isUserTL) {
      let listSCs = [];
      if (
        data &&
        data.managers &&
        data.managers.length == 1 &&
        data.managers[0] &&
        data.managers[0].team_leaders &&
        data.managers[0].team_leaders.length == 1 &&
        data.managers[0].team_leaders[0] &&
        data.managers[0].team_leaders[0].users &&
        data.managers[0].team_leaders[0].users.length > 0
      )
        listSCs = data.managers[0].team_leaders[0].users;
      //show SalesConsultants
      flatList = (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={listSCs}
          renderItem={({ item, index }) => {
            return (
              <View>
                <ColdVisitItem
                  onLongPress={this.props.onLongPress.bind(this)}
                  info={{
                    user_id: item.id,
                    user_role_id: item.user_role_id,
                    name: item.name,
                    image_url: item.image_url,
                    count: item.count,
                    location_id,
                    child: []
                  }}
                />
                {index == listSCs.length - 1 && (
                  <ColdVisitTotal
                    onLongPress={this.props.onLongPress.bind(this)}
                    info={{
                      user_id: global.appUserId,
                      user_role_id: user_role_id,
                      count: data.managers[0].team_leaders[0].count,
                      location_id
                    }}
                  />
                )}
              </View>
            );
          }}
          keyExtractor={(item, index) => index + ""}
          onRefresh={() => this.refresh()}
          refreshing={this.state.isRefreshing}
        />
      );
    } else if (this.props.isUserSC) {
      let listSC = [];
      if (
        data &&
        data.managers &&
        data.managers.length == 1 &&
        data.managers[0] &&
        data.managers[0].team_leaders &&
        data.managers[0].team_leaders.length == 1 &&
        data.managers[0].team_leaders[0] &&
        data.managers[0].team_leaders[0].users &&
        data.managers[0].team_leaders[0].users.length == 1 &&
        data.managers[0].team_leaders[0].users[0]
      )
        listSC[0] = data.managers[0].team_leaders[0].users[0];
      //show one SalesConsultant
      flatList = (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={listSC}
          renderItem={({ item, index }) => {
            return (
              <ColdVisitItem
                onLongPress={this.props.onLongPress.bind(this)}
                info={{
                  user_id: item.id,
                  user_role_id: item.user_role_id,
                  name: item.name,
                  image_url: item.image_url,
                  count: item.count,
                  location_id,
                  child: []
                }}
              />
            );
          }}
          keyExtractor={(item, index) => index + ""}
          onRefresh={() => this.refresh()}
          refreshing={this.state.isRefreshing}
        />
      );
    }
    return <View style={{ flex: 1 }}>{flatList}</View>;
  }
}
