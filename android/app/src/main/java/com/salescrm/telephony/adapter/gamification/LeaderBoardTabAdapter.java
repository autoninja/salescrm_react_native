package com.salescrm.telephony.adapter.gamification;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.salescrm.telephony.fragments.gamification.LeaderBoardDseFragment;
import com.salescrm.telephony.fragments.gamification.LeaderBoardTabFragment;
import com.salescrm.telephony.interfaces.RefreshCallBack;
import com.salescrm.telephony.response.gamification.ConsultantLeaderBoardResponse;

import java.util.ArrayList;

/**
 * Created by bannhi on 14/12/17.
 */

public class LeaderBoardTabAdapter extends FragmentPagerAdapter {

    ArrayList<ConsultantLeaderBoardResponse.Categories> categories;
    ArrayList<String> titleList = new ArrayList<>();
    RefreshCallBack refreshCallBack;
    public LeaderBoardTabAdapter(FragmentManager fm, RefreshCallBack refreshCallBack) {
        super(fm);
        this.refreshCallBack = refreshCallBack;
    }
    public void setTab(String title,ArrayList<ConsultantLeaderBoardResponse.Categories> categories ){
     this.categories = categories;
     titleList.add(title);

    }
    /**
     * This method may be called by the ViewPager to obtain a title string
     * to describe the specified page. This method may return null
     * indicating no title for this page. The default implementation returns
     * null.
     *
     * @param position The position of the title requested
     * @return A title for the requested page
     */

    @Override
    public CharSequence getPageTitle(int position)
    {
       System.out.println("WSConstants.GROUPS.get(position).getGroupName(): "+categories.get(position).getName());
        return titleList.get(position);

    }

    @Override
    public Fragment getItem(int position) {

       // System.out.println("GET ITEM IS CALLEd"+position+""+categories.get(position).getUsers().get(0).getName());
        return LeaderBoardTabFragment.newInstance(position,categories.get(position), refreshCallBack);
    }

    @Override
    public int getCount() {

        return  titleList.size();
    }
    public void updateView(){
        this.notifyDataSetChanged();
    }
}
