package com.salescrm.telephony.fragments.retExcFin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.CalendarDialogActivity;
import com.salescrm.telephony.activity.NewFilterActivity;
import com.salescrm.telephony.adapter.RefAdapter.RefCardAdapter;
import com.salescrm.telephony.adapter.RefAdapter.RefGMCardAdpter;
import com.salescrm.telephony.adapter.RefAdapter.RefLocationAdapter;
import com.salescrm.telephony.adapter.RefAdapter.RefSMCardAdpter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.etvbr_filter_db.EtvbrFilterDb;
import com.salescrm.telephony.db.ref_location.RefAbsoluteValue;
import com.salescrm.telephony.db.ref_location.RefInfo;
import com.salescrm.telephony.db.ref_location.RefLocation;
import com.salescrm.telephony.db.ref_location.RefLocationAbs;
import com.salescrm.telephony.db.ref_location.RefLocationRel;
import com.salescrm.telephony.db.ref_location.RefLocationTargets;
import com.salescrm.telephony.db.ref_location.RefRelationalValue;
import com.salescrm.telephony.db.ref_location.RefResultLocation;
import com.salescrm.telephony.db.ref_location.RefSalesConsultant;
import com.salescrm.telephony.db.ref_location.RefSalesManager;
import com.salescrm.telephony.db.ref_location.RefTargetValue;
import com.salescrm.telephony.db.ref_location.RefTeamLeader;
import com.salescrm.telephony.db.ref_location.RefTotalAbs;
import com.salescrm.telephony.db.ref_location.RefTotalRel;
import com.salescrm.telephony.db.ref_location.RefTotalTargets;
import com.salescrm.telephony.model.NewFilter.EtvbrFilterApply;
import com.salescrm.telephony.interfaces.SelectDateRange;
import com.salescrm.telephony.model.EtvbrLocationResponse;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by prateek on 7/11/17.
 */

public class RefLocationFragment extends Fragment{

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView tvEnquiry, tvTestDrive, tvV, tvBooking, tvR, tvL;
    private Switch swtchSwitch;
    public static boolean PERCENT = false;
    private Preferences pref;
    private RelativeLayout rel_loading_frame;
    private Realm realm;
    private ImageView tvDateIcon;
    private TextView tvDate;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RefSMCardAdpter etvbrSMCardAdpter;
    private RefCardAdapter etvbrCardAdapter;
    private RefGMCardAdpter etvbrGMCardAdpter;
    private RefLocationAdapter etvbrLocationAdapter;
    private String startDateString = "";
    private String endDateString = "";
    private ImageView imgFilter;
    private String selectedFilter = "";
    private EtvbrFilterDb etvbrFilterDb;
    private TextView tvFilterNumber;
    public static RefContainerFragment.EtvbrFooterChangeListener etvbrFooterChange;

    public static boolean isLocationAvailable() {
        return isLocationAvailable;
    }

    public static void setIsLocationAvailable(boolean isLocationAvailable) {
        RefLocationFragment.isLocationAvailable = isLocationAvailable;
    }

    public static boolean isLocationAvailable = true;

    public static RefLocationFragment newInstance(int i, String etvbr) {
        Bundle args = new Bundle();
        args.putInt("someInt", i);
        args.putString("someTitle", etvbr);
        RefLocationFragment fragment = new RefLocationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static RefLocationFragment newInstance(RefContainerFragment.EtvbrFooterChangeListener etvbrFooterChangeListener) {
        etvbrFooterChange = etvbrFooterChangeListener;
        RefLocationFragment fragment = new RefLocationFragment();
        return fragment;
    }


    @Override
    public void onStart() {
        super.onStart();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        setFilterNumbers();
        tvDate.setText(pref.getShowDateEtvbr());
    }

    private int getFilterNumbers() {
        int value = 0;
        for (int i = 0; i < WSConstants.selectedFilters.getFilters().size(); i++) {
            if (!WSConstants.selectedFilters.getFilters().get(i).getValues().isEmpty()) {
                value++;
            }
        }
        return value;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.etvbr_fragment, container, false);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        mRecyclerView = (RecyclerView) rView.findViewById(R.id.recyclerViewEtvbr);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        tvEnquiry = (TextView) rView.findViewById(R.id.tv_enq);
        tvTestDrive = (TextView) rView.findViewById(R.id.tv_td);
        tvV = (TextView) rView.findViewById(R.id.tv_v);
        tvBooking = (TextView) rView.findViewById(R.id.tv_b);
        tvR = (TextView) rView.findViewById(R.id.tv_r);
        tvL = (TextView) rView.findViewById(R.id.tv_l);
        tvDate = (TextView) rView.findViewById(R.id.date_date);
        swtchSwitch = (Switch) rView.findViewById(R.id.swtch_etvbr);
        rel_loading_frame = (RelativeLayout) rView.findViewById(R.id.rel_loading_frame);
        tvDateIcon = (ImageView) rView.findViewById(R.id.date_icon);
        imgFilter = (ImageView) rView.findViewById(R.id.filter_etvbr);
        etvbrFilterDb = realm.where(EtvbrFilterDb.class).findFirst();
        tvFilterNumber = (TextView) rView.findViewById(R.id.tv_filter_number);
        tvDateIcon.setImageDrawable(VectorDrawableCompat.create(getActivity().getResources(), R.drawable.ic_calendar_icon, null));
        setUpRef();

        swipeRefreshLayout = (SwipeRefreshLayout) rView.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
                    //callServer();
                    rel_loading_frame.setVisibility(View.VISIBLE);
                    getEtvbrLocationFromServerOnDateSelection(pref.getStartdateEtvbr(), pref.getEndDateEtvbr());
                    //getEtvbrLocationFromServer(startDateString, endDateString);
                    //etvbrLocationAdapter.notifyDataSetChanged();
                }else{
                    Util.showToast(getContext(), "No internet connection", Toast.LENGTH_SHORT);
                    setSwipeRefreshView(false);
                }
            }
        });

        tvDateIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CalendarDialogActivity.class);
                intent.putExtra("fragment", "ref");
                startActivity(intent);
            }
        });

        if(new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
            Date date = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, 1);

            getEtvbrLocationFromServer(pref.getStartdateEtvbr(), pref.getEndDateEtvbr());
            tvDate.setText(pref.getShowDateEtvbr());
            setUpAdapter();
        }else{
            setUpAdapter();
        }

        PERCENT = false;

        swtchSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                System.out.println("Swiched: "+isChecked);
                tvEnquiry.setVisibility(View.VISIBLE);
                tvTestDrive.setVisibility(View.VISIBLE);
                tvV.setVisibility(View.VISIBLE);
                tvBooking.setVisibility(View.VISIBLE);
                tvR.setVisibility(View.VISIBLE);
                tvL.setVisibility(View.VISIBLE);
                    //tvR.setVisibility(View.VISIBLE);
                    //tvV.setVisibility(View.VISIBLE);
                    if (isChecked) {
                        PERCENT = true;
                        //tvEnquiry.setVisibility(View.GONE);
                        tvEnquiry.setVisibility(View.GONE);
                        tvEnquiry.setText("Retail%");
                        tvTestDrive.setText("Exch%");
                        tvV.setText("Fin/I%");
                        tvBooking.setText("Fin/O%");
                        tvR.setText("PB%");
                        tvL.setText("LE%");
                        logPercentageEvent();
                    } else {
                        PERCENT = false;
                        tvEnquiry.setVisibility(View.VISIBLE);
                        tvEnquiry.setVisibility(View.VISIBLE);
                        tvEnquiry.setText("Retail");
                        tvTestDrive.setText("Exch");
                        tvV.setText("Fin/I");
                        tvBooking.setText("Fin/O");
                        tvR.setText("PB");
                        tvL.setText("LE");
                    }

                //notifyAdapter();
                setUpAdapter();
            }
        });

        imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etvbrFilterDb!= null && etvbrFilterDb.isValid()) {
                    Intent intent = new Intent(getActivity(), NewFilterActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                }else {
                    Util.showToast(getActivity(), "Filter is ready, Please logout and try again!!", Toast.LENGTH_SHORT);
                }
            }
        });
        return rView;
    }

    private void setUpRef() {
        tvEnquiry.setVisibility(View.VISIBLE);
        tvBooking.setVisibility(View.GONE);
        tvR.setVisibility(View.VISIBLE);
        tvL.setVisibility(View.VISIBLE);
        tvBooking.setVisibility(View.VISIBLE);
        tvEnquiry.setText("Retail");
        tvTestDrive.setText("Exch");
        tvV.setText("Fin/I");
        tvBooking.setText("Fin/O");
        tvR.setText("PB");
        tvL.setText("LE");
    }

    private void getEtvbrLocationFromServer(String startDate, String endDate) {
        rel_loading_frame.setVisibility(View.VISIBLE);
        selectedFilter = new Gson().toJson(WSConstants.selectedFilters.getFilters()).toString();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getEtvbrInfoWithLocation(startDate, endDate, selectedFilter, new Callback<EtvbrLocationResponse>() {
            @Override
            public void success(final EtvbrLocationResponse etvbrLocationResponse, Response response) {

                setSwipeRefreshView(false);

                if(etvbrLocationResponse!=null && etvbrLocationResponse.getResult()!=null&&etvbrLocationResponse.getResult().getLocations() != null && !etvbrLocationResponse.getResult().getLocations().isEmpty()) {
                    setFooter(etvbrLocationResponse.getResult().getLocations().size(), etvbrLocationResponse.getResult().getLocations().get(0).getLocationId());
                }

                rel_loading_frame.setVisibility(View.GONE);

                if((""+etvbrLocationResponse.getStatusCode()).equals(WSConstants.RESPONSE_OK)) {

                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }
                    if(getContext() != null) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                if (etvbrLocationResponse.getResult().getLocations() != null && !etvbrLocationResponse.getResult().getLocations().isEmpty()) {
                                    emptyDb();
                                    realm.createOrUpdateObjectFromJson(RefResultLocation.class, new Gson().toJson(etvbrLocationResponse.getResult()));
                                    setUpAdapter();
                                }

                            }
                        });
                    }
                }else{
                    Util.showToast(getContext(), ""+ etvbrLocationResponse.getMessage(), Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                rel_loading_frame.setVisibility(View.GONE);
                setSwipeRefreshView(false);
                if(error.getResponse()!= null) {
                    Util.showToast(getContext(), "Server not responding - " + error.getResponse().getStatus(), Toast.LENGTH_SHORT);
                }
            }
        });
    }

    private void setFooter(int locations, Integer locationId) {
        if(etvbrFooterChange!= null) {
            if (locations == 1) {
                pref.setLocationId(locationId);
                // EtvbrContainerFragment.setFooterVisible(true);
                etvbrFooterChange.setFooterVisible(true);
            } else if (locations > 1) {
                // EtvbrContainerFragment.setFooterVisible(false);
                etvbrFooterChange.setFooterVisible(false);
            }
        }
    }

    private void setSwipeRefreshView(final boolean val) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(val);
            }
        });
    }

    private void setUpAdapter() {

        if(realm.where(RefResultLocation.class).findFirst() != null) {
            if (realm.where(RefResultLocation.class).findFirst().getLocations() != null && !realm.where(RefResultLocation.class).findFirst().getLocations().isEmpty()) {
                if (realm.where(RefResultLocation.class).findFirst().getLocations().size() > 1) {
                    setIsLocationAvailable(true);
                 //   pushCleverTap(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_LOCATIONS);
                    etvbrLocationAdapter = new RefLocationAdapter(getActivity(), realm.where(RefResultLocation.class).findFirst(), realm);
                    mRecyclerView.setAdapter(etvbrLocationAdapter);
                } else {
                    setIsLocationAvailable(false);
                    if (realm.where(RefResultLocation.class).findFirst().getLocations().get(0).getBranchHead() == true) {
                        etvbrGMCardAdpter = new RefGMCardAdpter(getActivity(), realm, realm.where(RefResultLocation.class).findFirst().getLocations().get(0));
                        mRecyclerView.setAdapter(etvbrGMCardAdpter);
                    } else if (realm.where(RefResultLocation.class).findFirst().getLocations().get(0).getSalesManagers().get(0).getInfo() != null) {
                        etvbrSMCardAdpter = new RefSMCardAdpter(getActivity(), realm, realm.where(RefResultLocation.class).findFirst().getLocations().get(0));
                        mRecyclerView.setAdapter(etvbrSMCardAdpter);
                    } else {
                        etvbrCardAdapter = new RefCardAdapter(getActivity(), realm, realm.where(RefResultLocation.class).findFirst().getLocations().get(0));
                        mRecyclerView.setAdapter(etvbrCardAdapter);
                    }

                }
            }
        }
    }

    private void pushCleverTap(String event) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, event);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
    }

    private void notifyAdapter(){
        if(realm.where(RefResultLocation.class).findFirst() != null) {
            if(realm.where(RefResultLocation.class).findFirst().getLocations() != null && !realm.where(RefResultLocation.class).findFirst().getLocations().isEmpty()) {
                if (realm.where(RefResultLocation.class).findFirst().getLocations().size() > 1 && etvbrLocationAdapter != null) {
                    etvbrLocationAdapter.notifyDataSetChanged();
                } else {
                    if (realm.where(RefResultLocation.class).findFirst().getLocations().get(0).getBranchHead() == true && etvbrGMCardAdpter!=null) {
                        etvbrGMCardAdpter.notifyDataSetChanged();
                    } else if (realm.where(RefResultLocation.class).findFirst().getLocations().get(0).getSalesManagers().get(0).getInfo() != null && etvbrSMCardAdpter!= null) {
                        etvbrSMCardAdpter.notifyDataSetChanged();
                    } else {
                        if(etvbrCardAdapter != null)
                        etvbrCardAdapter.notifyDataSetChanged();
                    }

                }
            }
        }
    }

    @Subscribe
    public void dateRangeTeamDashboard(SelectDateRange selectDateRange){
            //System.out.println("Yes I'm Range Listener: "+ selectDateRange.getDateArrayList().get(0)+"  and  "+selectDateRange.getDateArrayList().get(selectDateRange.getDateArrayList().size()-1));
            String startFirst = new SimpleDateFormat("dd/MM").format(selectDateRange.getDateArrayList().get(0));
            String endLast = new SimpleDateFormat("dd/MM").format(selectDateRange.getDateArrayList().get(selectDateRange.getDateArrayList().size() - 1));

            String startDate = new SimpleDateFormat("yyyy-MM-dd").format(selectDateRange.getDateArrayList().get(0));
            String endDate = new SimpleDateFormat("yyyy-MM-dd").format(selectDateRange.getDateArrayList().get(selectDateRange.getDateArrayList().size() - 1));
            tvDate.setText(startFirst + "-" + endLast);
            pref.setShowDateEtvbr(startFirst + "-" + endLast);
            pref.setStartdateEtvbr(startDate);
            pref.setEndDateEtvbr(endDate);
            getEtvbrLocationFromServerOnDateSelection(startDate, endDate);
    }

    private void getEtvbrLocationFromServerOnDateSelection(String startDate, String endDate) {
        rel_loading_frame.setVisibility(View.VISIBLE);
        selectedFilter = new Gson().toJson(WSConstants.selectedFilters.getFilters()).toString();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getEtvbrInfoWithLocation(startDate, endDate, selectedFilter, new Callback<EtvbrLocationResponse>() {
            @Override
            public void success(final EtvbrLocationResponse etvbrLocationResponse, Response response) {
                setSwipeRefreshView(false);

                if(etvbrLocationResponse.getResult()!=null && etvbrLocationResponse.getResult().getLocations() != null && !etvbrLocationResponse.getResult().getLocations().isEmpty()) {
                    setFooter(etvbrLocationResponse.getResult().getLocations().size(), etvbrLocationResponse.getResult().getLocations().get(0).getLocationId());
                }

                rel_loading_frame.setVisibility(View.GONE);
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }
                if(getContext() != null) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            if (etvbrLocationResponse.getResult()!=null && etvbrLocationResponse.getResult().getLocations() != null) {
                                emptyDb();
                                realm.createOrUpdateObjectFromJson(RefResultLocation.class, new Gson().toJson(etvbrLocationResponse.getResult()));
                                //notifyAdapter();
                                setUpAdapter();
                            }
                        }
                    });
                }
            }

            @Override
            public void failure(RetrofitError error) {
                setSwipeRefreshView(false);
                rel_loading_frame.setVisibility(View.GONE);
                Util.showToast(getContext(), "Server not responding - "+ error.getMessage(), Toast.LENGTH_SHORT);
            }
        });
    }

    private void emptyDb() {
        realm.delete(RefResultLocation.class);
        realm.delete(RefLocation.class);
        realm.delete(RefSalesManager.class);
        realm.delete(RefInfo.class);
        realm.delete(RefSalesConsultant.class);
        realm.delete(RefLocationAbs.class);
        realm.delete(RefLocationRel.class);
        realm.delete(RefLocationTargets.class);
        realm.delete(RefTeamLeader.class);
        realm.delete(RefTotalAbs.class);
        realm.delete(RefTotalRel.class);
        realm.delete(RefTotalTargets.class);
        realm.delete(RefAbsoluteValue.class);
        realm.delete(RefRelationalValue.class);
        realm.delete(RefTargetValue.class);
    }
    private void logPercentageEvent(){
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_PERCENTAGE);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
    }

    @Subscribe
    public void applyFilter(EtvbrFilterApply etvbrFilterApply){
        if(etvbrFilterApply.isApplyFilter()){
            setFilterNumbers();
                if (new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
                    //callServer();
                    rel_loading_frame.setVisibility(View.VISIBLE);
                    getEtvbrLocationFromServerOnDateSelection(pref.getStartdateEtvbr(), pref.getEndDateEtvbr());
                    //getEtvbrLocationFromServer(startDateString, endDateString);
                    //etvbrLocationAdapter.notifyDataSetChanged();
                } else {
                    Util.showToast(getContext(), "No internet connection", Toast.LENGTH_SHORT);
                    setSwipeRefreshView(false);
                }
            }
    }

    private void setFilterNumbers() {
        if(getFilterNumbers() > 0){
            tvFilterNumber.setVisibility(View.VISIBLE);
            tvFilterNumber.setText(""+getFilterNumbers());
        }else {
            tvFilterNumber.setVisibility(View.GONE);
        }
    }
}
