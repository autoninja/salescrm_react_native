package com.salescrm.telephony.offline;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.ActivitiesDB;
import com.salescrm.telephony.db.AllCarsDB;
import com.salescrm.telephony.db.BrandsDB;
import com.salescrm.telephony.db.BuyerTypeDB;
import com.salescrm.telephony.db.EnqSourceCategoryDB;
import com.salescrm.telephony.db.EnqSourceDB;
import com.salescrm.telephony.db.EnqSourceMainDB;
import com.salescrm.telephony.db.EnqSubInnerSourceDB;
import com.salescrm.telephony.db.LocationsDB;
import com.salescrm.telephony.interfaces.FetchLeadElementsListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AddLeadElementsResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 4/8/16.
 * Call this to get the lead basic elements
 */
public class FetchLeadElements implements Callback<AddLeadElementsResponse> {
    public FetchLeadElementsListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;

    public FetchLeadElements(FetchLeadElementsListener listener,Context context) {
        this.listener =listener;
        this.context = context;
      //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }
    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).FetchLeadElements("", this);
        } else {
            System.out.println("No Internet Connection");
            listener.onFetchLeadElementsError(null);
        }
    }


    @Override
    public void success(final AddLeadElementsResponse addLeadElementsResponse, Response response) {
        Util.updateHeaders(response.getHeaders());

        if (!addLeadElementsResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + addLeadElementsResponse.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            listener.onFetchLeadElementsError(null);
        } else {
            if (addLeadElementsResponse.getResult() != null) {
                System.out.println("Success:1" + addLeadElementsResponse.getMessage());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm1) {
                        deleteDb(realm1);
                        insertData(realm1, addLeadElementsResponse.getResult().getBuyerType());
                        insertData(realm1, addLeadElementsResponse.getResult().getAllCars());
                        insertData(realm1, addLeadElementsResponse.getResult().getBrands());
                        insertData(realm1, addLeadElementsResponse.getResult().getLocations());

                        insertEnqSourceDb(realm1, addLeadElementsResponse.getResult().getEnqSource());

                        insertData(realm1, addLeadElementsResponse.getResult().getActivities());

                    }
                });
                listener.onLeadElementsFetched(addLeadElementsResponse);
            } else {
                // relLoader.setVisibility(View.GONE);

                System.out.println("Success:2" + addLeadElementsResponse.getMessage());
                listener.onFetchLeadElementsError(null);
                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }

    private void deleteDb(Realm realm) {
        realm.delete(EnqSourceMainDB.class);
        realm.delete(EnqSourceDB.class);
        realm.delete(EnqSourceCategoryDB.class);
        realm.delete(EnqSubInnerSourceDB.class);

        realm.delete(BuyerTypeDB.class);
        realm.delete(AllCarsDB.class);
        realm.delete(BrandsDB.class);
        realm.delete(LocationsDB.class);
        realm.delete(ActivitiesDB.class);

    }

    private void insertEnqSourceDb(Realm realmVar, AddLeadElementsResponse.Result.EnqSource enqSourceServer) {


        EnqSourceMainDB enqSourceMainDB = new EnqSourceMainDB();

        if(enqSourceServer!=null){

            //Setting sources
            RealmList<EnqSourceDB> enqSourceDBs = new RealmList<EnqSourceDB>();
            for (int i=0;i<enqSourceServer.getLead_sources().size();i++){
                EnqSourceDB enqSourceDB = realmVar.createObject(EnqSourceDB.class);
                enqSourceDB.setId(enqSourceServer.getLead_sources().get(i).getId());
                enqSourceDB.setName(enqSourceServer.getLead_sources().get(i).getName());
                enqSourceDBs.add(enqSourceDB);
            }
            enqSourceMainDB.setLeadSources(enqSourceDBs);


            //Setting sub sources
            RealmList<EnqSourceCategoryDB> enqSourceCategoryDBs = new RealmList<EnqSourceCategoryDB>();
            for (int i=0;i<enqSourceServer.getLead_source_categories().size();i++){
                EnqSourceCategoryDB enqSourceCategoryDB = realmVar.createObject(EnqSourceCategoryDB.class);
                enqSourceCategoryDB.setId(enqSourceServer.getLead_source_categories().get(i).getId());
                enqSourceCategoryDB.setName(enqSourceServer.getLead_source_categories().get(i).getName());

                RealmList<EnqSubInnerSourceDB> enqSourceDBForInner = new RealmList<EnqSubInnerSourceDB>();

                if(enqSourceServer.getLead_source_categories().get(i).getLead_sources()!=null){
                    for (int j=0;j<enqSourceServer.getLead_source_categories().get(i).getLead_sources().getData().size();j++){
                        EnqSubInnerSourceDB enqSourceDB = realmVar.createObject(EnqSubInnerSourceDB.class);
                        enqSourceDB.setId(enqSourceServer.getLead_source_categories().get(i).getLead_sources().getData().get(j).getId());
                        enqSourceDB.setName(enqSourceServer.getLead_source_categories().get(i).getLead_sources().getData().get(j).getName());
                        enqSourceDBForInner.add(enqSourceDB);
                    }
                }

                enqSourceCategoryDB.setSubLeadSources(enqSourceDBForInner);


                enqSourceCategoryDBs.add(enqSourceCategoryDB);
            }

            enqSourceMainDB.setLead_source_categories(enqSourceCategoryDBs);

            realmVar.copyToRealm(enqSourceMainDB);


        }
    }

    @Override
    public void failure(RetrofitError error) {
        listener.onFetchLeadElementsError(error);

    }
    private void insertData(Realm realm, List t) {
        System.out.println("Insert Data Called");
        BuyerTypeDB buyerTypeDBCreate;
        AllCarsDB allCarsDB;
        BrandsDB brands;
        LocationsDB locations;
        ActivitiesDB activities;

        for (int i = 0; i < t.size(); i++) {
            if (t.get(i) instanceof AddLeadElementsResponse.Result.BuyerType) {
                buyerTypeDBCreate = new BuyerTypeDB();
                buyerTypeDBCreate.setType(((AddLeadElementsResponse.Result.BuyerType) t.get(i)).getType());
                buyerTypeDBCreate.setId(Util.getInt(((AddLeadElementsResponse.Result.BuyerType) t.get(i)).getId()));
                System.out.println("Buyer Type:" + ((AddLeadElementsResponse.Result.BuyerType) t.get(i)).getType());
                realm.copyToRealmOrUpdate(buyerTypeDBCreate);

            }
            if (t.get(i) instanceof AddLeadElementsResponse.Result.AllCars) {
                allCarsDB = new AllCarsDB();
                allCarsDB.setId(Util.getInt(((AddLeadElementsResponse.Result.AllCars) t.get(i)).getId()));
                allCarsDB.setName(((AddLeadElementsResponse.Result.AllCars) t.get(i)).getName());
                allCarsDB.setCategory(((AddLeadElementsResponse.Result.AllCars) t.get(i)).getCategory());
                realm.copyToRealmOrUpdate(allCarsDB);


            }
            if (t.get(i) instanceof AddLeadElementsResponse.Result.Brands) {
                brands = new BrandsDB();
                brands.setId(Util.getInt(((AddLeadElementsResponse.Result.Brands) t.get(i)).getId()));
                brands.setName(((AddLeadElementsResponse.Result.Brands) t.get(i)).getName());
                brands.setCompany(((AddLeadElementsResponse.Result.Brands) t.get(i)).getCompany());
                realm.copyToRealmOrUpdate(brands);
            }
            if (t.get(i) instanceof AddLeadElementsResponse.Result.Locations) {
                locations = new LocationsDB();
                locations.setId(Util.getInt(((AddLeadElementsResponse.Result.Locations) t.get(i)).getId()));
                locations.setName(((AddLeadElementsResponse.Result.Locations) t.get(i)).getName());
                locations.setCode(((AddLeadElementsResponse.Result.Locations) t.get(i)).getCode());
                realm.copyToRealmOrUpdate(locations);
            }

            if (t.get(i) instanceof AddLeadElementsResponse.Result.Activities) {
                activities = new ActivitiesDB();
                activities.setId(Util.getInt(((AddLeadElementsResponse.Result.Activities) t.get(i)).getId()));
                activities.setCategory(Util.getInt(((AddLeadElementsResponse.Result.Activities) t.get(i)).getCategory()));
                activities.setId(Util.getInt(((AddLeadElementsResponse.Result.Activities) t.get(i)).getId()));
                activities.setName(((AddLeadElementsResponse.Result.Activities) t.get(i)).getName());
                realm.copyToRealmOrUpdate(activities);
            }
        }

    }

}
