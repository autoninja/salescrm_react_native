import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, NativeModules, TouchableWithoutFeedback, KeyboardAvoidingView, ScrollView, TextInput, BackHandler, Modal
} from 'react-native'
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';
import LinearGradient from 'react-native-linear-gradient'

const styles = StyleSheet.create({
    submit_title: {
        color: '#ffffff',
        textAlign: 'center',
        fontSize: 16,
        alignSelf: 'center',
        marginTop: 5
    },
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: '#000000',
        padding: 20

    },
    submit_button: {
        width: 200,
        marginTop: 10,
        marginBottom: 8,
        marginLeft: 20,
        marginRight: 20,
        height: 30,
        alignItems: 'center'
    },
    footerInvalid: {
        backgroundColor: '#808080',
    },
    footerValid: {
        backgroundColor: '#007fff',
    },
});
export default class C360CancelRescheduleActivity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: '',
            isInnerModalVisible: false,
            date_time_picker_visible: false,
            show_date: 'Select date*',
            end_remarks: '',
        }
    }

    generateView = () => {
        let remarks = ""
        let { viewId } = this.props;
        let { show_date } = this.state;
        let { end_remarks } = this.state;

        if (end_remarks) {
            remarks = end_remarks;
        }
        let allDone = false
        if (viewId == 1) {
            allDone = show_date != "Select date*" && remarks
        } else {
            allDone = remarks
        }

        switch (viewId) {
            case 1:
                return (
                    <View style={{ alignItems: 'center' }}>
                        <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={() => this.cancelEvaluation()}>
                            <Image
                                style={{ width: 20, height: 20, }}
                                source={require('../../images/ic_close_white.png')} />
                        </TouchableOpacity>
                        <LinearGradient colors={['#2e5e86', '#2e3486']} style={{ borderRadius: 5, padding: 10 }}>
                            <View style={{ justifyContent: "center", alignItems: "center", paddingBottom: 10, padding: 10, }}>
                                {/* <Text style={{ padding: 10, fontSize: 16, marginTop: 8, color: '#fff' }}>Schedule Date</Text> */}

                                <TouchableOpacity style={{ width: '100%', marginTop: 10 }} onPress={() => this._date_time_picker_visible()} >
                                    <Text style={{ color: '#fff', fontWeight: 'normal', fontSize: 16, borderBottomWidth: 1, borderColor: '#CFCFCF', padding: 3 }}>{show_date}</Text>
                                    <View style={(Platform.OS === 'ios') ? { color: '#CFCFCF', height: 1 } : { color: '#8a8a8a', height: 1 }}></View>
                                </TouchableOpacity>
                                <TextInput
                                    style={{ padding: 3, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, marginBottom: 10, color: '#fff', width: '100%' }}
                                    placeholder='Remarks*'
                                    placeholderTextColor='#abb2b9'
                                    value={remarks}
                                    onChangeText={(text) => {
                                        end_remarks = text;
                                        this.setState({ end_remarks });
                                    }}
                                />
                                <TouchableOpacity disabled={(!allDone)}
                                    style={[(allDone) ? styles.footerValid : styles.footerInvalid, styles.submit_button]} onPress={() => this.props.performCancelOrReschedule({ "remarks": end_remarks, "date": show_date }, viewId)}>

                                    <Text style={styles.submit_title}>Submit</Text>

                                </TouchableOpacity>
                            </View>
                        </LinearGradient>
                    </View>
                )
            case 2:
                return (
                    <View style={{ alignItems: 'center' }}>
                        <TouchableOpacity style={{ alignSelf: 'flex-end' }} onPress={() => this.cancelEvaluation()}>
                            <Image
                                style={{ width: 20, height: 20, }}
                                source={require('../../images/ic_close_white.png')} />
                        </TouchableOpacity>
                        <LinearGradient colors={['#2e5e86', '#2e3486']} style={{ borderRadius: 5, padding: 10 }}>
                            <View style={{ paddingBottom: 10, padding: 10, }}>
                                <Text style={{ fontSize: 16, marginTop: 8, color: '#fff' }}>What is the reason?</Text>
                                <TextInput
                                    style={{ padding: 3, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 10, marginBottom: 10, color: '#fff', width: '100%' }}
                                    placeholder='Remarks*'
                                    placeholderTextColor='#abb2b9'
                                    value={remarks}
                                    onChangeText={(text) => {
                                        end_remarks = text;
                                        this.setState({ end_remarks });
                                    }}
                                />
                                <TouchableOpacity disabled={(!allDone)}
                                    style={[(allDone) ? styles.footerValid : styles.footerInvalid, styles.submit_button]} onPress={() => this.props.performCancelOrReschedule({ "remarks": end_remarks }, viewId)}>

                                    <Text style={styles.submit_title}>Submit</Text>

                                </TouchableOpacity>
                            </View>
                        </LinearGradient>
                    </View>
                )
        }

    }

    _date_time_picker_visible = () => {
        //console.error("Occured")
        this.setState({ date_time_picker_visible: true });
    }

    _handleDatePicked = (date) => {
        this.setState({ show_date: Moment(date).format('MMM DD YYYY, hh:mm:ss a'), date_time_picker_visible: false });
        //this._hideDateTimePicker();

    }

    _hideDateTimePicker = () => {
        this.setState({ date_time_picker_visible: false });
    }

    render() {

        let selectedOptions = this.props.exchangeCarDetail;
        //console.error(JSON.stringify(selectedOptions))
        let maxDate = new Date();
        maxDate.setDate(maxDate.getDate() + 10);
        if (this.props.isInnerModalVisible) {
            return (
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled={Platform.OS === 'ios'}>
                    <Modal
                        visible={this.props.isInnerModalVisible}
                        animationType={'slide'}
                        transparent={true}
                        onRequestClose={() => this.cancelEvaluation()}
                    >
                        <View style={styles.overlay}>
                            {this.generateView()}
                        </View>
                        <DateTimePicker
                            isVisible={this.state.date_time_picker_visible}
                            is24Hour={false}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                            mode={'datetime'}
                            minimumDate={new Date()}
                            maximumDate={maxDate}
                        />
                    </Modal>
                </KeyboardAvoidingView>);
        } else {
            return null;
        }

    }

    cancelEvaluation() {
        this.props.onCloseInnerModal()
        this.setState({ end_remarks: '', show_date: 'Select date*', })
    }
}