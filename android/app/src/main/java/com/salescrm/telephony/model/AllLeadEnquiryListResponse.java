package com.salescrm.telephony.model;

import com.salescrm.telephony.db.LeadListStageProgress;

import java.util.List;

/**
 * Created by prateek on 16/2/17.
 */

public class AllLeadEnquiryListResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    public class Result
    {
        private String count;
        private List<LeadListDetails> leadDetails;

        public List<LeadListDetails> getLeadDetails() {
            return leadDetails;
        }

        public void setLeadDetails(List<LeadListDetails> leadDetails) {
            this.leadDetails = leadDetails;
        }


        public String getCount ()
        {
            return count;
        }

        public void setCount (String count)
        {
            this.count = count;
        }
    }

    public class LeadListDetails
    {
        private String activity_id;

        private String scheduled_at;

        private String lead_stage;

        private String activity_name;

        private String number;

        private String scheduled_activity_id;

        private String lead_id;

        private String activity_description;

        private String variant_name;

        private String first_name;

        private String category_instance_id;

        private String category;

        private String lead_tag_name;

        private String email;

        private String color;

        private String activity_type_id;

        private String last_name;

        private String activity_user_asignee;

        private String activity_group;

        private String activity_type;

        private String activity_group_id;

        private String stage_id;

        private String assigned_to_id;

        private String customer_id;

        private String lead_source_id;

        private String lead_creation_date_time;

        private String gender;

        private String lead_age;

        private String stage_age;

        private String buyer_type;

        private String expected_closing_date;

        private String lead_source_name;

        private String stage_updated;

        private String dse_name;

        private String closing_reason;

        public String getStage_updated() {
            return stage_updated;
        }

        public void setStage_updated(String stage_updated) {
            this.stage_updated = stage_updated;
        }

        public String getStage_age() {
            return stage_age;
        }

        public void setStage_age(String stage_age) {
            this.stage_age = stage_age;
        }

        public String getBuyer_type() {
            return buyer_type;
        }

        public void setBuyer_type(String buyer_type) {
            this.buyer_type = buyer_type;
        }

        public String getExpected_closing_date() {
            return expected_closing_date;
        }

        public void setExpected_closing_date(String expected_closing_date) {
            this.expected_closing_date = expected_closing_date;
        }

        public String getLead_source_name() {
            return lead_source_name;
        }

        public void setLead_source_name(String lead_source_name) {
            this.lead_source_name = lead_source_name;
        }

        private List<LeadListStage> lead_stage_progress;

        public String getActivity_id ()
        {
            return activity_id;
        }

        public void setActivity_id (String activity_id)
        {
            this.activity_id = activity_id;
        }

        public String getScheduled_at ()
        {
            return scheduled_at;
        }

        public void setScheduled_at (String scheduled_at)
        {
            this.scheduled_at = scheduled_at;
        }

        public String getLead_stage ()
        {
            return lead_stage;
        }

        public void setLead_stage (String lead_stage)
        {
            this.lead_stage = lead_stage;
        }

        public String getActivity_name ()
        {
            return activity_name;
        }

        public void setActivity_name (String activity_name)
        {
            this.activity_name = activity_name;
        }

        public String getNumber ()
        {
            return number;
        }

        public void setNumber (String number)
        {
            this.number = number;
        }

        public String getScheduled_activity_id ()
        {
            return scheduled_activity_id;
        }

        public void setScheduled_activity_id (String scheduled_activity_id)
        {
            this.scheduled_activity_id = scheduled_activity_id;
        }

        public String getLead_id ()
        {
            return lead_id;
        }

        public void setLead_id (String lead_id)
        {
            this.lead_id = lead_id;
        }

        public String getActivity_description ()
    {
        return activity_description;
    }

        public void setActivity_description (String activity_description)
        {
            this.activity_description = activity_description;
        }

        public String getVariant_name ()
        {
            return variant_name;
        }

        public void setVariant_name (String variant_name)
        {
            this.variant_name = variant_name;
        }

        public String getFirst_name ()
        {
            return first_name;
        }

        public void setFirst_name (String first_name)
        {
            this.first_name = first_name;
        }

        public String getCategory_instance_id ()
        {
            return category_instance_id;
        }

        public void setCategory_instance_id (String category_instance_id)
        {
            this.category_instance_id = category_instance_id;
        }

        public String getCategory ()
        {
            return category;
        }

        public void setCategory (String category)
        {
            this.category = category;
        }

        public String getLead_tag_name ()
        {
            return lead_tag_name;
        }

        public void setLead_tag_name (String lead_tag_name)
        {
            this.lead_tag_name = lead_tag_name;
        }

        public String getEmail ()
        {
            return email;
        }

        public void setEmail (String email)
        {
            this.email = email;
        }

        public String getColor ()
        {
            return color;
        }

        public void setColor (String color)
        {
            this.color = color;
        }

        public String getActivity_type_id ()
        {
            return activity_type_id;
        }

        public void setActivity_type_id (String activity_type_id)
        {
            this.activity_type_id = activity_type_id;
        }

        public String getLast_name ()
        {
            return last_name;
        }

        public void setLast_name (String last_name)
        {
            this.last_name = last_name;
        }

        public String getActivity_user_asignee ()
        {
            return activity_user_asignee;
        }

        public void setActivity_user_asignee (String activity_user_asignee)
        {
            this.activity_user_asignee = activity_user_asignee;
        }

        public String getActivity_group ()
        {
            return activity_group;
        }

        public void setActivity_group (String activity_group)
        {
            this.activity_group = activity_group;
        }

        public String getActivity_type ()
        {
            return activity_type;
        }

        public void setActivity_type (String activity_type)
        {
            this.activity_type = activity_type;
        }

        public String getActivity_group_id ()
        {
            return activity_group_id;
        }

        public void setActivity_group_id (String activity_group_id)
        {
            this.activity_group_id = activity_group_id;
        }

        public String getStage_id ()
        {
            return stage_id;
        }

        public void setStage_id (String stage_id)
        {
            this.stage_id = stage_id;
        }

        public String getAssigned_to_id ()
        {
            return assigned_to_id;
        }

        public void setAssigned_to_id (String assigned_to_id)
        {
            this.assigned_to_id = assigned_to_id;
        }

        public String getCustomer_id ()
        {
            return customer_id;
        }

        public void setCustomer_id (String customer_id)
        {
            this.customer_id = customer_id;
        }

        public String getLead_source_id ()
        {
            return lead_source_id;
        }

        public void setLead_source_id (String lead_source_id)
        {
            this.lead_source_id = lead_source_id;
        }

        public String getLead_creation_date_time() {
            return lead_creation_date_time;
        }

        public void setLead_creation_date_time(String lead_creation_date_time) {
            this.lead_creation_date_time = lead_creation_date_time;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getLead_age() {
            return lead_age;
        }

        public void setLead_age(String lead_age) {
            this.lead_age = lead_age;
        }

        public List<LeadListStage> getLead_stage_progress() {
            return lead_stage_progress;
        }

        public void setLead_stage_progress(List<LeadListStage> lead_stage_progress) {
            this.lead_stage_progress = lead_stage_progress;
        }

        public String getDse_name() {
            return dse_name;
        }

        public void setDse_name(String dse_name) {
            this.dse_name = dse_name;
        }

        public String getClosing_reason() {
            return closing_reason;
        }

        public void setClosing_reason(String closing_reason) {
            this.closing_reason = closing_reason;
        }

        public class LeadListStage{
            private String width;

            private String name;

            private String bar_class;

            private String stage_id;

            public String getWidth ()
            {
                return width;
            }

            public void setWidth (String width)
            {
                this.width = width;
            }

            public String getName ()
            {
                return name;
            }

            public void setName (String name)
            {
                this.name = name;
            }

            public String getBar_class ()
            {
                return bar_class;
            }

            public void setBar_class (String bar_class)
            {
                this.bar_class = bar_class;
            }

            public String getStage_id ()
            {
                return stage_id;
            }

            public void setStage_id (String stage_id)
            {
                this.stage_id = stage_id;
            }
        }
    }

    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

    }
}
