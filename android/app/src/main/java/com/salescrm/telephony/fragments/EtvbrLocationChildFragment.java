package com.salescrm.telephony.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.EtvbrCardAdapter;
import com.salescrm.telephony.adapter.EtvbrGMCardAdpter;
import com.salescrm.telephony.adapter.EtvbrSMCardAdpter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.FabricUtils;

import java.util.HashMap;

import io.realm.Realm;

/**
 * Created by prateek on 13/11/17.
 */

public class EtvbrLocationChildFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView tvEnquiry, tvTestDrive, tvV, tvBooking, tvR, tvL;
    private Switch swtchSwitch;
    public static boolean PERCENT = false;
    private Preferences pref;
    private Realm realm;
    private TextView tvDateIcon;
    private TextView tvDate;
    private SwipeRefreshLayout swipeRefreshLayout;
    private EtvbrSMCardAdpter etvbrSMCardAdpter;
    private EtvbrCardAdapter etvbrCardAdapter;
    private EtvbrGMCardAdpter etvbrGMCardAdpter;
    private Location locationResult;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.etvbr_location_child_fragment, container, false);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(getActivity());
        mRecyclerView = (RecyclerView) rView.findViewById(R.id.recyclerViewEtvbr);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        tvEnquiry = (TextView)rView. findViewById(R.id.tv_enq);
        tvTestDrive = (TextView)rView. findViewById(R.id.tv_td);
        tvV = (TextView)rView. findViewById(R.id.tv_v);
        tvBooking = (TextView)rView. findViewById(R.id.tv_b);
        tvR = (TextView)rView. findViewById(R.id.tv_r);
        tvL = (TextView)rView.findViewById(R.id.tv_l);
        tvDate = (TextView)rView. findViewById(R.id.date_date);
        tvDateIcon = (TextView)rView. findViewById(R.id.date_icon);
        tvDateIcon.setVisibility(View.INVISIBLE);
        tvDate.setText(pref.getShowDateEtvbr());

        if(DbUtils.isBike()){
            tvR.setVisibility(View.GONE);
            tvV.setVisibility(View.GONE);
        }else {
            tvR.setVisibility(View.VISIBLE);
            tvV.setVisibility(View.VISIBLE);
        }

        swtchSwitch = (Switch)rView. findViewById(R.id.swtch_etvbr);
        locationResult = realm.where(Location.class).equalTo("location_id", getActivity().getIntent().getExtras().getInt("location_id")).findFirst();
        setUpAdapter(locationResult);
        PERCENT = false;

        swtchSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                System.out.println("Swiched: "+isChecked);
                if(DbUtils.isBike()){
                    tvR.setVisibility(View.GONE);
                    tvV.setVisibility(View.GONE);
                    if (isChecked) {
                        PERCENT = true;
                        tvEnquiry.setVisibility(View.GONE);
                        tvTestDrive.setText("T%");
                        tvBooking.setText("B%");
                        tvV.setText("V%");
                        tvR.setText("R%");
                        tvL.setText("L%");
                        logPercentageEvent();
                    } else {
                        PERCENT = false;
                        tvEnquiry.setVisibility(View.VISIBLE);
                        tvTestDrive.setText("T");
                        tvBooking.setText("B");
                        tvV.setText("V");
                        tvR.setText("R");
                        tvL.setText("L");
                    }
                }else {
                    tvR.setVisibility(View.VISIBLE);
                    tvV.setVisibility(View.VISIBLE);
                    if (isChecked) {
                        PERCENT = true;
                        tvEnquiry.setVisibility(View.GONE);
                        tvTestDrive.setText("T%");
                        tvBooking.setText("B%");
                        tvV.setText("V%");
                        tvR.setText("R%");
                        tvL.setText("L%");
                        logPercentageEvent();
                    } else {
                        PERCENT = false;
                        tvEnquiry.setVisibility(View.VISIBLE);
                        tvTestDrive.setText("T");
                        tvBooking.setText("B");
                        tvV.setText("V");
                        tvR.setText("R");
                        tvL.setText("L");
                    }
                }
                notifyAdapter();

            }
        });

        return rView;
    }

    private void setUpAdapter(Location locationResult) {
        if(this.locationResult.getBranchHead() == true) {
            etvbrGMCardAdpter = new EtvbrGMCardAdpter(getActivity(), realm, locationResult);
            mRecyclerView.setAdapter(etvbrGMCardAdpter);
            etvbrGMCardAdpter.notifyDataSetChanged();
        }else if(this.locationResult.getSalesManagers().get(0).getInfo() != null){
            etvbrSMCardAdpter = new EtvbrSMCardAdpter(getActivity(), realm, locationResult);
            mRecyclerView.setAdapter(etvbrSMCardAdpter);
            etvbrSMCardAdpter.notifyDataSetChanged();
        }else {
            etvbrCardAdapter = new EtvbrCardAdapter(getActivity(), realm, locationResult);
            mRecyclerView.setAdapter(etvbrCardAdapter);
            etvbrCardAdapter.notifyDataSetChanged();
        }
    }

    private void notifyAdapter(){
        if(this.locationResult.getBranchHead() == true) {
            etvbrGMCardAdpter.notifyDataSetChanged();
        }else if(this.locationResult.getSalesManagers().get(0).getInfo() != null){
            etvbrSMCardAdpter.notifyDataSetChanged();
        }else {
            etvbrCardAdapter.notifyDataSetChanged();
        }
    }
    private void logPercentageEvent(){
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_PERCENTAGE);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
    }
}
