package com.salescrm.telephony.db.events;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class EventsCategoryDb extends RealmObject {


    private String id;
    private String name;

    public EventsCategoryDb() {

    }

    public EventsCategoryDb(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
