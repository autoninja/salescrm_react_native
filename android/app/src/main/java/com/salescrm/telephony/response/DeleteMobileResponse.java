package com.salescrm.telephony.response;

public class DeleteMobileResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    private String leadMappingId;

    DeleteMobileResponse deleteMobileResponse;
    private String mobile;

    public String getLeadMappingId() {
        return leadMappingId;
    }

    public void setLeadMappingId(String leadMappingId) {
        this.leadMappingId = leadMappingId;
    }

    public DeleteMobileResponse getDeleteMobileResponse() {
        return deleteMobileResponse;
    }

    public void setDeleteMobileResponse(DeleteMobileResponse deleteMobileResponse) {
        this.deleteMobileResponse = deleteMobileResponse;
    }

   /* public DeleteMobileResponse(DeleteMobileResponse delDeleteMobileResponse){
        this.deleteMobileResponse = deleteMobileResponse;

    }*/
    public DeleteMobileResponse(DeleteMobileResponse deleteMobileResponse){
        this.deleteMobileResponse = deleteMobileResponse;
      /*  this.deleteMobileResponse.setLeadMappingId(leadMappingId);*/

    }
    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public class Result
    {
        private String lead_last_updated;

        private String deleteData;

        private String lead_id;

        public String getLead_last_updated ()
        {
            return lead_last_updated;
        }
        public void setLead_last_updated (String lead_last_updated)
        {
            this.lead_last_updated = lead_last_updated;
        }

        public String getDeleteData ()
        {
            return deleteData;
        }

        public void setDeleteData (String deleteData)
        {
            this.deleteData = deleteData;
        }

        public String getLead_id ()
        {
            return lead_id;
        }

        public void setLead_id (String lead_id)
        {
            this.lead_id = lead_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [lead_last_updated = "+lead_last_updated+", deleteData = "+deleteData+", lead_id = "+lead_id+"]";
        }
    }
    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }

}
