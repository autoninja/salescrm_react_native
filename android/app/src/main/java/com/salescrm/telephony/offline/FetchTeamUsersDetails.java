package com.salescrm.telephony.offline;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.EnqSourceDB;
import com.salescrm.telephony.db.LocationsDB;
import com.salescrm.telephony.db.crm_user.TeamData;
import com.salescrm.telephony.db.crm_user.TeamUserDB;
import com.salescrm.telephony.db.crm_user.UserNewCarDB;
import com.salescrm.telephony.db.crm_user.UsersDB;
import com.salescrm.telephony.db.teams.TeamLocationDB;
import com.salescrm.telephony.db.teams.TeamMainDB;
import com.salescrm.telephony.db.teams.TeamUserDetailsDB;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.LeadTeamUserResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 9/12/16.
 * Call this to get the team_user details for assign dse;
 */
public class FetchTeamUsersDetails implements Callback<LeadTeamUserResponse> {
    private final ArrayList<Integer> dataLeadSource;
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private int countCall=0;
    private RealmResults<LocationsDB> locationsList;

    public FetchTeamUsersDetails(OfflineSupportListener listener, Context context) {
        this.listener =listener;
        this.context = context;
      //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
        realm.beginTransaction();

        realm.where(TeamUserDB.class).findAll().deleteAllFromRealm();
        realm.where(TeamMainDB.class).findAll().deleteAllFromRealm();
        realm.where(TeamLocationDB.class).findAll().deleteAllFromRealm();

        realm.where(TeamUserDB.class).findAll().deleteAllFromRealm();
        realm.where(UserNewCarDB.class).findAll().deleteAllFromRealm();
        realm.where(TeamData.class).findAll().deleteAllFromRealm();
        realm.where(UsersDB.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        locationsList = realm.where(LocationsDB.class).findAll();
        RealmResults<EnqSourceDB> dataDb = realm.where(EnqSourceDB.class).findAll();
        dataLeadSource = new ArrayList<>();
        for (int i = 0; i < dataDb.size(); i++) {
            dataLeadSource.add(dataDb.get(i).getId());
        }
    }
    public void call() {

        if(countCall<locationsList.size()){
            getTeamUserData(locationsList.get(countCall).getId());
            countCall++;
        }



    }
    private void getTeamUserData(int locationId){

        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetTeamUserDetails(dataLeadSource,0, locationId, this);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.TEAM_USERS);
        }
    }


    @Override
    public void success(final LeadTeamUserResponse leadTeamUserResponse, Response response) {
        Util.updateHeaders(response.getHeaders());

        if (!leadTeamUserResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + leadTeamUserResponse.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.TEAM_USERS);
        } else {
            if (leadTeamUserResponse.getResult() != null) {
                System.out.println("Success:1" + leadTeamUserResponse.getMessage());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm1) {
                        insertData(locationsList.get(countCall-1).getId(),realm1,leadTeamUserResponse.getResult());
                        System.out.println("location count:"+locationsList.get(countCall-1).getId());
                    }
                });
                System.out.println("Count call::"+countCall);

                if(countCall==locationsList.size()){
                    listener.onTeamUsersFetched(leadTeamUserResponse);
                }
                else {
                    call();
                }

//                listener.onTeamUsersFetched(leadTeamUserResponse);
            } else {

                // relLoader.setVisibility(View.GONE);
                System.out.println("Success:2" + leadTeamUserResponse.getMessage());
                listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.TEAM_USERS);

                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }

    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(error,WSConstants.OfflineAPIRequest.TEAM_USERS);

    }
    private void insertData(int location_id, Realm realm, LeadTeamUserResponse.Result t) {
        t.setLocationId(location_id);

        //revamped db for react native
        TeamLocationDB teamLocationDB = new TeamLocationDB();
        teamLocationDB.setLocation_id(location_id);



        TeamUserDB teamUserDB = new TeamUserDB();
        teamUserDB.setLocationId(location_id);


        RealmList<UserNewCarDB> userNewCarDBRealmList = new RealmList<UserNewCarDB>();
        UserNewCarDB userNewCarDB = realm.createObject(UserNewCarDB.class);
        userNewCarDB.setLeadNewCar(0);
        userNewCarDB.setLeadSource(0);

        RealmList<TeamData> teamUserDBData = new RealmList<TeamData>();

        //Revamped db for react-native
        RealmList<TeamMainDB> teamMainDBS = new RealmList<TeamMainDB>();

        for(int i=0;i<t.getTeam_data().size();i++){
            TeamData current = realm.createObject(TeamData.class);
            current.setUser_id(t.getTeam_data().get(i).getUser_id());
            current.setManager_id(t.getTeam_data().get(i).getManager_id());
            current.setTeam_leader_id(t.getTeam_data().get(i).getTeam_leader_id());
            teamUserDBData.add(current);

            //Revamped db for react-native
            teamMainDBS.add(new TeamMainDB(Util.getInt(t.getTeam_data().get(i).getManager_id()),
                    Util.getInt(t.getTeam_data().get(i).getTeam_leader_id()),
                    Util.getInt(t.getTeam_data().get(i).getUser_id())));
        }

        RealmList<UsersDB> userDBData = new RealmList<UsersDB>();

        //Revamped db for react-native
        RealmList<TeamUserDetailsDB> teamUserDetailsDBS = new RealmList<TeamUserDetailsDB>();

        for(int i=0;i<t.getUsers().size();i++){
            UsersDB current = realm.createObject(UsersDB.class);
            current.setId(t.getUsers().get(i).getId());
            current.setMobile_number(t.getUsers().get(i).getMobile_number());
            current.setName(t.getUsers().get(i).getName());
            userDBData.add(current);

            //Revamped db for react-native
            teamUserDetailsDBS.add(new TeamUserDetailsDB(Util.getInt(t.getUsers().get(i).getId()),
                    t.getUsers().get(i).getName(), t.getUsers().get(i).getMobile_number()));
        }

        userNewCarDB.setTeam_data(teamUserDBData);
        userNewCarDB.setUsers(userDBData);


        userNewCarDBRealmList.add(userNewCarDB);
        teamUserDB.setUserNewCarDBList(userNewCarDBRealmList);
        realm.insertOrUpdate(teamUserDB);

        //revamped db for react native
        teamLocationDB.setTeams(teamMainDBS);
        teamLocationDB.setUsers(teamUserDetailsDBS);
        realm.insertOrUpdate(teamLocationDB);
    }

}
