package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by bannhi on 10/2/17.
 */

public class AddExchangeCarModel {

    private List<Cars> cars;

    private String lead_id;
    private String lead_last_updated;

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }
    /*  public AddExchangeCarModel(List<Cars> cars, String lead_id){
        this.cars = cars;
        this.lead_id = lead_id;
    }*/

    public List<Cars> getCars ()
    {
        return cars;
    }

    public void setCars (List<Cars> cars)
    {
        this.cars = cars;
    }

    public String getLead_id ()
    {
        return lead_id;
    }

    public void setLead_id (String lead_id)
    {
        this.lead_id = lead_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cars = "+cars+", lead_id = "+lead_id+"]";
    }



}
