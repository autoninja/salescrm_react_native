package com.salescrm.telephony.response;

import com.salescrm.telephony.utils.Util;

import java.util.List;

public class HereReverseGeoResponse {
    private Response Response;

    public Response getResponse() {
        return Response;
    }

    public void setResponse(Response Response) {
        this.Response = Response;
    }

    public String getAddress() {
        if (this.Response != null
                && this.Response.getView() != null
                && this.Response.getView().size() > 0
                && this.Response.getView().get(0) != null
                && this.Response.getView().get(0).getResult() != null
                && this.Response.getView().get(0).getResult().size() > 0
                && this.Response.getView().get(0).getResult().get(0) != null
                && this.Response.getView().get(0).getResult().get(0).getLocation() != null
                && this.Response.getView().get(0).getResult().get(0).getLocation().getAddress() != null

        ) {
            Address address = this.Response.getView().get(0).getResult().get(0).getLocation().getAddress();
            if (Util.isNotNull(address.getLabel())) {
                return address.getLabel() + (Util.isNotNull(address.getPostalCode()) ? ", " + address.getPostalCode() : "");
            }

        }
        return null;
    }

    public class Response {

        private List<View> View;

        public List<View> getView() {
            return View;
        }

        public void setView(List<View> view) {
            View = view;
        }

    }

    public class View {

        private List<Result> Result;

        public List<Result> getResult() {
            return Result;
        }

        public void setResult(List<Result> result) {
            Result = result;
        }
    }

    public class Result {
        private MatchQuality MatchQuality;
        private Location Location;


        public MatchQuality getMatchQuality() {
            return MatchQuality;
        }

        public void setMatchQuality(MatchQuality MatchQuality) {
            this.MatchQuality = MatchQuality;
        }

        public Location getLocation() {
            return Location;
        }

        public void setLocation(Location Location) {
            this.Location = Location;
        }
    }

    public class MatchQuality {
        private int State;

        private int Country;

        private int PostalCode;

        private int City;

        private int County;

        private int District;

        public int getState() {
            return State;
        }

        public void setState(int state) {
            State = state;
        }

        public int getCountry() {
            return Country;
        }

        public void setCountry(int country) {
            Country = country;
        }

        public int getPostalCode() {
            return PostalCode;
        }

        public void setPostalCode(int postalCode) {
            PostalCode = postalCode;
        }

        public int getCity() {
            return City;
        }

        public void setCity(int city) {
            City = city;
        }

        public int getCounty() {
            return County;
        }

        public void setCounty(int county) {
            County = county;
        }

        public int getDistrict() {
            return District;
        }

        public void setDistrict(int district) {
            District = district;
        }
    }

    public class Location {
        private Address Address;

        public Address getAddress() {
            return Address;
        }

        public void setAddress(Address address) {
            this.Address = address;
        }
    }

    public class Address {
        private List<AdditionalData> AdditionalData;

        private String State;

        private String Label;

        private String Country;

        private String PostalCode;

        private String City;

        private String County;

        private String District;

        public List<AdditionalData> getAdditionalData() {
            return AdditionalData;
        }

        public void setAdditionalData(List<AdditionalData> AdditionalData) {
            this.AdditionalData = AdditionalData;
        }

        public String getState() {
            return State;
        }

        public void setState(String State) {
            this.State = State;
        }

        public String getLabel() {
            return Label;
        }

        public void setLabel(String Label) {
            this.Label = Label;
        }

        public String getCountry() {
            return Country;
        }

        public void setCountry(String Country) {
            this.Country = Country;
        }

        public String getPostalCode() {
            return PostalCode;
        }

        public void setPostalCode(String PostalCode) {
            this.PostalCode = PostalCode;
        }

        public String getCity() {
            return City;
        }

        public void setCity(String City) {
            this.City = City;
        }

        public String getCounty() {
            return County;
        }

        public void setCounty(String County) {
            this.County = County;
        }

        public String getDistrict() {
            return District;
        }

        public void setDistrict(String District) {
            this.District = District;
        }

        @Override
        public String toString() {
            return "ClassPojo [AdditionalData = " + AdditionalData + ", State = " + State + ", Label = " + Label + ", Country = " + Country + ", PostalCode = " + PostalCode + ", City = " + City + ", County = " + County + ", District = " + District + "]";
        }
    }

    public class AdditionalData {   //CountryName, StateName, CountyName {Example : India, Karnataka, Bengaluru}

        private String value;

        private String key;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        @Override
        public String toString() {
            return "ClassPojo [value = " + value + ", key = " + key + "]";
        }
    }

}
