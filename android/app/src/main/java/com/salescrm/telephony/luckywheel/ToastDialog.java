package com.salescrm.telephony.luckywheel;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.response.MobileAppDataResponse;
import com.salescrm.telephony.utils.Util;

import java.util.List;
import java.util.logging.Handler;

/**
 * Created by prateek on 3/7/18.
 */

public class ToastDialog {
    private TestDialogInterface testDialogInterface;
    private Context context;
    private Dialog dialog;
    private LinearLayout llParent;
    private ScrollView scrollView;
    private List<MobileAppDataResponse.Points> points;

    public ToastDialog(Context context, List<MobileAppDataResponse.Points> points, TestDialogInterface testDialogInterface) {
        this.context = context;
        this.testDialogInterface = testDialogInterface;
        this.points = points;
    }

    public void show(){
        dialog = new Dialog(context, R.style.TransparentDialog);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
        }

        dialog.setContentView(R.layout.game_toast_points);
        llParent = (LinearLayout) dialog.findViewById(R.id.custom_toast_layout);
        scrollView = (ScrollView) dialog.findViewById(R.id.scroll_view_taost_dailog);
        scrollView.setPadding(0,200, 0, 0);


        getToastLayout(points);
        dialog.show();
        testDialogInterface.endTestDialog(dialog);
    }

    private LinearLayout getToastLayout(List<MobileAppDataResponse.Points> points) {

        LinearLayout.LayoutParams paramsLabel = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 3f);
        paramsLabel.gravity = Gravity.CENTER;

        LinearLayout.LayoutParams paramsPoints = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 7f);
        paramsPoints.gravity = Gravity.CENTER;

        LinearLayout.LayoutParams paramsllInner = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT); // If any device this dont look good then change it to WRAP_CONTENT
        paramsllInner.gravity = Gravity.CENTER;

        llParent.removeAllViews();
        //llParent.setPadding(20, 5, 20, 5);
        //ll.setLayoutParams(paramsllParent);
        for(int i=0; i<points.size(); i++){
            LinearLayout llInner = new LinearLayout(context);
            //llInner.setPadding(15, 15, 15, 15);
            llInner.setOrientation(LinearLayout.VERTICAL);
            llInner.setBackground(ContextCompat.getDrawable(context, R.drawable.point_background));
            llInner.setLayoutParams(paramsllInner);

            TextView tvLable = new TextView(context);
            tvLable.setText(""+points.get(i).getTitle());
            tvLable.setGravity(Gravity.CENTER);
            tvLable.setTextSize(30);
            tvLable.setTextColor(Color.WHITE);
            tvLable.setLayoutParams(paramsLabel);
            llInner.addView(tvLable);

            TextView tvPoints = new TextView(context);
            String pointsString = "<b><big>+"+points.get(i).getPoint()+"</big></b><small> Points<small>";
            tvPoints.setText(Util.fromHtml(pointsString));
            tvPoints.setTextSize(40);
            tvPoints.setGravity(Gravity.CENTER);
            tvPoints.setTextColor(Color.WHITE);
            tvPoints.setTypeface(null, Typeface.BOLD_ITALIC);
            tvPoints.setLayoutParams(paramsPoints);
            llInner.addView(tvPoints);
            llParent.addView(llInner);
        }
        return llParent;
    }

    public void dismiss(){
        dialog.dismiss();
    }
}
