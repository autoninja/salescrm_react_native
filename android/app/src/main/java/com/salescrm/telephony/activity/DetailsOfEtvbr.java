package com.salescrm.telephony.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.common.LifecycleState;
import com.facebook.react.shell.MainReactPackage;
import com.google.gson.Gson;
import com.horcrux.svg.SvgPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.salescrm.telephony.BuildConfig;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.DetailsOfEtvbrAdapter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.model.ApiGenericModelResponse;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.reactNative.ReactNativeToAndroidPackage;
import com.salescrm.telephony.response.EtvbrDetailsResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.zmxv.RNSound.RNSoundPackage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by nndra on 23-Nov-17.
 */

public class DetailsOfEtvbr extends AppCompatActivity {

    private RecyclerView recyclerEtvbrDetails;
    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RelativeLayout relLoading;
    private DetailsOfEtvbrAdapter mDetailsOfEtvbrAdapter;
    private Toolbar toolbar;
    private TextView customTitleName, customTitleDate;
    private Preferences pref;
    private List<EtvbrDetailsResponse.LeadDetails> etvbrDetails;
    private TextView tvNoLeadMsg, tvEtvbrDetailsTitle;
    private String clickedPosition = "", titleName = "", userId = "", etvbrtotal = "", userRoleId = "", userLocationId = "", startDate = "";
    private String mOffset = "0";

    private ReactRootView mReactRootView;
    private ReactInstanceManager mReactInstanceManager;
    private boolean ROUTE_EVENT = false;
    private String locationId, eventId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_of_etvbr);
        pref = Preferences.getInstance();
        pref.load(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ROUTE_EVENT = extras.getBoolean("route_event");
            if(extras.getBoolean("route_event")){
                clickedPosition = extras.getString("clicked_position");
                titleName = extras.getString("title_name");
                locationId = extras.getString("location_id");
                eventId = extras.getString("event_id");
                startDate = extras.getString("date");
            }else {
                clickedPosition = extras.getString("clicked_position");
                titleName = extras.getString("title_name");
                userId = extras.getString("user_id");
                etvbrtotal = extras.getString("etvbrtotal");
                userRoleId = extras.getString("user_role_id");
                userLocationId = extras.getString("user_location_id");
                startDate = extras.getString("start_date");
                System.out.println("EtvbrDetails userLocationId 00 - " + userLocationId);
                if (userLocationId.equalsIgnoreCase("-1") || userLocationId.equalsIgnoreCase("")) {
                    pref.setLocationId(-1);
                } else {
                    pref.setLocationId(Integer.parseInt(userLocationId));
                }
            }
        }

        mOffset = "0";
        System.out.println("EventEtvbrDetails date - " + startDate);
        System.out.println("EventEtvbrDetails eventId - " + eventId);
        System.out.println("EventEtvbrDetails locationId - " + locationId);
        System.out.println("EventEtvbrDetails clickedPosition - " + clickedPosition);
        System.out.println("EtvbrDetails titleName - " + titleName);
        System.out.println("EtvbrDetails StartDate - " + pref.getStartdateEtvbr());
        System.out.println("EtvbrDetails EndDate - " + pref.getEndDateEtvbr());
        System.out.println("EtvbrDetails userRoleId - " + userRoleId);
        System.out.println("EtvbrDetails userId - " + userId);
        System.out.println("EtvbrDetails userLocationId - " + userLocationId);
        System.out.println("EtvbrDetails userLocationId 0 - " + pref.getLocationId());

        toolbar = (Toolbar) findViewById(R.id.etvbr_details_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mReactRootView = findViewById(R.id.react_root_view_details_dashboard);

        customTitleName = (TextView) findViewById(R.id.details_custom_title_name);
        customTitleDate = (TextView) findViewById(R.id.details_custom_title_date);
        tvNoLeadMsg = (TextView) findViewById(R.id.tv_no_lead);
        tvEtvbrDetailsTitle = (TextView) findViewById(R.id.tv_etvbr_details_title);

        if (startDate.equalsIgnoreCase("-1") || startDate.equalsIgnoreCase("") || startDate.equalsIgnoreCase("null")) {
            startDate = "" + pref.getStartdateEtvbr();
            customTitleDate.setText(pref.getShowDateEtvbr());
        } else {
            customTitleDate.setText("");
        }
        System.out.println("EtvbrDetails startDate2 - " + startDate);

        mReactInstanceManager = ReactInstanceManager.builder()
                .setApplication(getApplication())
                .setBundleAssetName("index.android.bundle")
                .setJSMainModulePath("index.android")
                .addPackage(new MainReactPackage())
                .addPackage(new ReactNativeToAndroidPackage())
                .addPackage(new SvgPackage())
                .addPackage(new RNSoundPackage())
                .addPackage(new VectorIconsPackage())
                .setUseDeveloperSupport(BuildConfig.DEBUG)
                .setInitialLifecycleState(LifecycleState.RESUMED)
                .build();

        Bundle initialProps = new Bundle();
        initialProps.putBoolean("fromAndroid", true);
        initialProps.putString("token",pref.getAccessToken());
        initialProps.putString("module", "TEAM_DASHBOARD_DETAILS");
        String userLocationId = "";
        if (pref.getLocationId() == -1) {
            userLocationId = "";
        } else {
            userLocationId = "" + pref.getLocationId();
        }
        String selectedFilters = new Gson().toJson(WSConstants.selectedFilters.getFilters()).toString();

        DashboardDetails dashboardDetails = new DashboardDetails(
                startDate,
                pref.getEndDateEtvbr(),
                selectedFilters,
                clickedPosition,
                userId,
                userLocationId,
                userRoleId

        );

        DashboardDetailsForEvent dashboardDetailsForEvent = new DashboardDetailsForEvent(
                startDate,
                clickedPosition,
                locationId,
                eventId,
                ROUTE_EVENT
        );

        if(ROUTE_EVENT){
            initialProps.putString("info", new Gson().toJson(dashboardDetailsForEvent));
        }else {
            initialProps.putString("info", new Gson().toJson(dashboardDetails));
        }
        mReactRootView.startReactApplication(mReactInstanceManager, "NinjaCRMSales", initialProps);


        customTitleName.setText(titleName);
        tvEtvbrDetailsTitle.setText(clickedPosition + " (" + etvbrtotal + ")");

        recyclerEtvbrDetails = (RecyclerView) findViewById(R.id.recyclerEtvbrDetails);
        recyclerEtvbrDetails.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerEtvbrDetails.setLayoutManager(mLayoutManager);


        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        //mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        //recyclerEtvbrDetails.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        relLoading = (RelativeLayout) findViewById(R.id.rel_loading_frame);
        // recyclerEtvbrDetails.setAdapter(mDetailsOfEtvbrAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                if (new ConnectionDetectorService(DetailsOfEtvbr.this).isConnectingToInternet()) {
                    relLoading.setVisibility(View.VISIBLE);
                    mOffset = "0";
                    //getUpdatedLeadList();
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        //getUpdatedLeadList();
        logFabric();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }

    private void getUpdatedLeadList() {
        System.out.println("EtvbrDetails mOffset - " + mOffset);
        String userLocationId = "";
        if (pref.getLocationId() == -1) {
            userLocationId = "";
        } else {
            userLocationId = "" + pref.getLocationId();
        }
        String selectedFilters = new Gson().toJson(WSConstants.selectedFilters.getFilters()).toString();
        System.out.println("EtvbrDetails userLocationId2 - " + userLocationId + ", "+selectedFilters);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getEtvbrDetails(startDate, pref.getEndDateEtvbr(),
                clickedPosition, userId, userLocationId, "" + userRoleId,
                "" + mOffset, "400", selectedFilters, new Callback<EtvbrDetailsResponse>() {
                    @Override
                    public void success(EtvbrDetailsResponse etvbrDetailsResponse, Response response) {
                        System.out.println("EtvbrDetails 0 - " + etvbrDetailsResponse);
                        progressBar.setVisibility(View.GONE);
                        recyclerEtvbrDetails.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        relLoading.setVisibility(View.GONE);
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            if (header.getName().equalsIgnoreCase("Authorization")) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }
                        }

                        mOffset = etvbrDetailsResponse.getResult().getOffset();

                        populateView(etvbrDetailsResponse.getResult());

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        System.out.println("EtvbrDetails 1 - " + error);
                        progressBar.setVisibility(View.GONE);
                        recyclerEtvbrDetails.setVisibility(View.VISIBLE);
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        relLoading.setVisibility(View.GONE);
                        tvNoLeadMsg.setText("Error occoured.");
                    }
                });

    }

    private boolean isFromUnCalledTask() {
        return clickedPosition.equalsIgnoreCase("Uncalled Tasks");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void populateView(EtvbrDetailsResponse.Result result) {

        etvbrDetails = new ArrayList<>();
        if (etvbrDetails != null) {
            etvbrDetails.clear();
        }

        if (result.getLeadDetails() != null) {
            etvbrDetails.addAll(result.getLeadDetails());
        } else {
            tvNoLeadMsg.setText("No Leads :)");
            tvNoLeadMsg.setVisibility(View.VISIBLE);
            recyclerEtvbrDetails.setVisibility(View.GONE);
            mSwipeRefreshLayout.setVisibility(View.GONE);
        }

        System.out.println("EtvbrDetails 2 - " + etvbrDetails.size());

        if (etvbrDetails.size() == 0) {
            tvNoLeadMsg.setText("No Leads :)");
            tvNoLeadMsg.setVisibility(View.VISIBLE);
            recyclerEtvbrDetails.setVisibility(View.GONE);
            mSwipeRefreshLayout.setVisibility(View.GONE);
        } else {
            tvNoLeadMsg.setVisibility(View.GONE);
            recyclerEtvbrDetails.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        }
        mDetailsOfEtvbrAdapter = new DetailsOfEtvbrAdapter(etvbrDetails, DetailsOfEtvbr.this);
        recyclerEtvbrDetails.setAdapter(mDetailsOfEtvbrAdapter);

    }

    private void logFabric() {
        if(Util.isNotNull(clickedPosition)) {
            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put(CleverTapConstants.EVENT_CLICKS_KEY_TYPE, clickedPosition);
            CleverTapPush.pushEvent(CleverTapConstants.EVENT_CLICKS, hashMap);
        }
    }

    private class DashboardDetails {
        String startDate;
        String endDate;
        String filterSelected;
        String clicked_val;
        String user_id;
        String user_location_id;
        String user_role_id;

        public DashboardDetails(String startDate, String endDate, String filterSelected, String clicked_val, String user_id, String user_location_id, String user_role_id) {
            this.startDate = startDate;
            this.endDate = endDate;
            this.filterSelected = filterSelected;
            this.clicked_val = clicked_val;
            this.user_id = user_id;
            this.user_location_id = user_location_id;
            this.user_role_id = user_role_id;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getFilterSelected() {
            return filterSelected;
        }

        public void setFilterSelected(String filterSelected) {
            this.filterSelected = filterSelected;
        }

        public String getClicked_val() {
            return clicked_val;
        }

        public void setClicked_val(String clicked_val) {
            this.clicked_val = clicked_val;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_location_id() {
            return user_location_id;
        }

        public void setUser_location_id(String user_location_id) {
            this.user_location_id = user_location_id;
        }

        public String getUser_role_id() {
            return user_role_id;
        }

        public void setUser_role_id(String user_role_id) {
            this.user_role_id = user_role_id;
        }
    }

    private class DashboardDetailsForEvent{
        String startDate;
        String clicked_val;
        String locationId;
        String eventId;
        boolean route_event;

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getClicked_val() {
            return clicked_val;
        }

        public void setClicked_val(String clicked_val) {
            this.clicked_val = clicked_val;
        }

        public String getLocationId() {
            return locationId;
        }

        public void setLocationId(String locationId) {
            this.locationId = locationId;
        }

        public String getEventId() {
            return eventId;
        }

        public void setEventId(String eventId) {
            this.eventId = eventId;
        }

        public boolean isRoute() {
            return route_event;
        }

        public void setRoute(boolean route) {
            this.route_event = route;
        }

        public DashboardDetailsForEvent(String startDate, String clicked_val, String location_id, String event_id, boolean route) {
            this.startDate = startDate;
            this.clicked_val = clicked_val;
            this.locationId = location_id;
            this.eventId = event_id;
            this.route_event = route;
        }
    }


}

