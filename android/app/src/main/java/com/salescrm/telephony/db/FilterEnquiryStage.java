package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bharath on 14/10/16.
 */

public class FilterEnquiryStage extends RealmObject {
    private String stage_id;
    private String stage;

    public String getStage_id() {
        return stage_id;
    }

    public void setStage_id(String stage_id) {
        this.stage_id = stage_id;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }
}
