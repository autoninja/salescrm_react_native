package com.salescrm.telephony.interfaces;

public class ReactNativeToAndroidConnect {
    private static final com.salescrm.telephony.interfaces.ReactNativeToAndroidConnect ourInstance = new com.salescrm.telephony.interfaces.ReactNativeToAndroidConnect();
    private static ReactNativeToAndroidConnectListener listener = null;

    public static ReactNativeToAndroidConnect getInstance() {
        return ourInstance;
    }

    public static ReactNativeToAndroidConnectListener getListener() {
        return listener;
    }

    public static void setListener(ReactNativeToAndroidConnectListener listener) {
        com.salescrm.telephony.interfaces.ReactNativeToAndroidConnect.listener = listener;
    }


    public void postRNComponentDidMount(String view) {
        if (listener != null) {
            listener.onRNComponentDidMount(view);

        }
    }

    public void postRNComponentWillUnMount(String view) {
        if (listener != null) {
            listener.onRNComponentWillUnMount(view);

        }
    }
    public interface ReactNativeToAndroidConnectListener {
        void onRNComponentDidMount(String view);
        void onRNComponentWillUnMount(String view);
    }
}
