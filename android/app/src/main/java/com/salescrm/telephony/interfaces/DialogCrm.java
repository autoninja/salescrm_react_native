package com.salescrm.telephony.interfaces;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.model.AutoDialogModel;

/**
 * Created by bharath on 18/1/18.
 */

public class DialogCrm {
    private Context context;
    private DialogCrmListener listener;

    public DialogCrm( Context context,DialogCrmListener listener) {
        this.context =  context;
        this.listener = listener;
    }
    public void show() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if( dialog.getWindow()!=null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.custom_dialog_new_layout);

        TextView tvYes = (TextView) dialog.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvNo = (TextView) dialog.findViewById(R.id.bt_custom_dialog_no);

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //your business logic
                dialog.dismiss();
                listener.onDialogCrmYesClick();
            }
        });
        tvNo = (TextView)dialog.findViewById(R.id.bt_custom_dialog_no);

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }
    public interface DialogCrmListener{
        void onDialogCrmYesClick();
    }

}
