package com.salescrm.telephony.fragments.gamification;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.gamification.ConsultantDetailsAdapter;
import com.salescrm.telephony.interfaces.RefreshCallBack;
import com.salescrm.telephony.response.gamification.ConsultantLeaderBoardResponse;
import com.salescrm.telephony.utils.WSConstants;

import java.lang.reflect.Field;


public class LeaderBoardTabFragment extends Fragment {

    private RecyclerView recyclerView;
    private ConsultantDetailsAdapter consultantDetailsAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ConsultantLeaderBoardResponse.Categories categories;
    private static RefreshCallBack refreshCallBack;

    public LeaderBoardTabFragment() {
        // Required empty public constructor
    }



    // TODO: Rename and change types and number of parameters
    public static LeaderBoardTabFragment newInstance(int sectionNumber, ConsultantLeaderBoardResponse.Categories ctgries, RefreshCallBack refresh) {
        Bundle args = new Bundle();
        args.putInt("someInt", sectionNumber);
        args.putSerializable("dseList", ctgries);
        LeaderBoardTabFragment fragment = new LeaderBoardTabFragment();
        fragment.setArguments(args);
        refreshCallBack = refresh;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_leader_board_tab, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.leader_board_tab_swipe_container);
        recyclerView = (RecyclerView) view.findViewById(R.id.leader_board_tab_recycler_view);


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
         /*       mSwipeRefreshLayout.setRefreshing(false);
                Intent intent = new Intent(WSConstants.dseDataFetchedIntent);
                Bundle args = new Bundle();
                args.putSerializable("dseDataResponse", null);
                args.putInt("homeActivity",1);
                intent.putExtra("DATA", args);
                getActivity().sendBroadcast(intent);*/
                refreshCallBack.onRefresh();
            }
        });
        Bundle bundle = this.getArguments();
        final ConsultantLeaderBoardResponse.Categories categoriesList =
                (ConsultantLeaderBoardResponse.Categories) bundle.getSerializable("dseList");
        recyclerView.setHasFixedSize(true);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        consultantDetailsAdapter = new ConsultantDetailsAdapter(this.getActivity(),categoriesList);
        recyclerView.setAdapter(consultantDetailsAdapter);

        recyclerView.post(new Runnable() {
            public void run() {
                int spotLight = getSpotLight(categoriesList);
                if(spotLight>mLayoutManager.findLastCompletelyVisibleItemPosition()) {
                    recyclerView.scrollToPosition(spotLight);
                }
            }
        });

        consultantDetailsAdapter.notifyDataSetChanged();
        return view;
    }
    private int getSpotLight(ConsultantLeaderBoardResponse.Categories categoriesList) {
       /* if(categoriesList == null || categoriesList.getSpotlight()!=1) {
            return 0;
        }*/
        for (int i=0;i<categoriesList.getUsers().size();i++) {
            if(categoriesList.getUsers().get(i).getSpotlight() == 1) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

}
