package com.salescrm.telephony.interfaces;

/**
 * Created by bharath on 24/8/17.
 */

public interface TransferEnquiriesDistributeListener {
    void onDataChanged();
}
