import React from 'react';
import C360MainScreen from './c360_main_screen';
import {View } from 'react-native';
export default class C360MainHolder extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return(
            <C360MainScreen 
            {...this.props}
            />
        )
    }
}