package com.salescrm.telephony.presenter;

import android.support.graphics.drawable.VectorDrawableCompat;
import android.util.SparseArray;

import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandModelsDB;
import com.salescrm.telephony.model.Item;
import com.salescrm.telephony.model.booking.BookingCarModel;
import com.salescrm.telephony.model.booking.BookingCustomerModel;
import com.salescrm.telephony.model.booking.BookingDiscountModel;
import com.salescrm.telephony.model.booking.BookingExchFinModel;
import com.salescrm.telephony.model.booking.BookingMainModel;
import com.salescrm.telephony.model.booking.BookingPriceBreakupModel;
import com.salescrm.telephony.model.booking.BookingRealmModel;
import com.salescrm.telephony.model.booking.BookingSectionModel;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by bharath on 23/2/18.
 */

public class BookingPresenter {

    public static final int CAR_BOOKING_RESET_ALL = 0;
    public static final int CAR_BOOKING_RESET_AFTER_MODEL = 1;
    public static final int CAR_BOOKING_RESET_AFTER_VARIANT = 2;
    public static final int CAR_BOOKING_RESET_AFTER_FUEL_TYPE = 3;
    public static final int CAR_BOOKING_RESET_AFTER_COLOR = 4;

    private static final int CAR_DETAILS = 1;
    private static final int CUSTOMER_DETAILS = 2;
    private static final int PRICE_DETAILS = 3;
    private static final int DISCOUNT_DETAILS = 4;

    private static final int CUSTOMER_TYPE_CORPORATE_COMPANY = 4;
    private static final int CUSTOMER_TYPE_FLEET_COMPANY = 6;


    private BookingView bookingView;

    private BookingMainModel bookingMainModel;
    private BookingCarModel bookingCarModel;
    private BookingCustomerModel bookingCustomerModel;
    private BookingPriceBreakupModel bookingPriceBreakupModel;
    private BookingDiscountModel bookingDiscountModel;
    private BookingSectionModel bookingSectionModel;
    private BookingRealmModel bookingRealmModel;
    private Realm realm;
    private boolean carDetailsCompleteStatus;
    private boolean customerDetailsDoneStatus;
    private boolean priceDetailsDoneStatus;
    private boolean discountDetailsDoneStatus;
    private long priceBreakupTotal = 0;
    private boolean isBookStage;
    private BookingExchFinModel bookingExchFinModel;
    private boolean exchFinDetailDoneStatus;
    private boolean bookingCarAllocationStatus = true;


    public BookingPresenter(BookingView bookingView, BookingMainModel bookingMainModel) {
        this.bookingView = bookingView;

        this.bookingMainModel = bookingMainModel;

        bookingCarModel = bookingMainModel.getBookingCarModel();
        bookingCustomerModel = bookingMainModel.getBookingCustomerModel();
        bookingPriceBreakupModel = bookingMainModel.getBookingPriceBreakupModel();
        bookingDiscountModel = bookingMainModel.getBookingDiscountModel();
        bookingSectionModel = bookingMainModel.getBookingSectionModel();
        bookingExchFinModel = bookingMainModel.getBookingExchFinModel();
        bookingRealmModel = new BookingRealmModel();
        isBookStage = bookingMainModel.getStageId() == WSConstants.CarBookingStages.BOOK_CAR;
        realm = bookingMainModel.getRealm();

    }

    public BookingMainModel getBookingMainModel() {
        return bookingMainModel;
    }

    private void updateSection(int sectionId, int sectionImageId, boolean visibility, boolean invertVisibility) {
        SparseArray<BookingSectionModel.BookingSectionInnerModel> sparseArray =
                bookingSectionModel.getBookingSectionInnerSparseArray();
        int i = 0;
        while (i < sparseArray.size()) {
            BookingSectionModel.BookingSectionInnerModel data = sparseArray.valueAt(i);
            if (data.isVisibility() && data.getSectionId() != sectionId) {
                data.setVisibility(false);
                bookingView.updateSectionVisibility(data);
            }
            i++;
        }
        BookingSectionModel.BookingSectionInnerModel bookingSectionInnerModel =
                bookingSectionModel.getBookingSectionInnerValue(sectionId);
        bookingSectionInnerModel.setSectionId(sectionId);
        bookingSectionInnerModel.setSectionImageId(sectionImageId);
        bookingSectionModel.putBookingSectionValue(sectionId, bookingSectionInnerModel);
        if (invertVisibility) {
            bookingSectionInnerModel.setVisibility(!bookingSectionInnerModel.isVisibility());
        } else {
            bookingSectionInnerModel.setVisibility(visibility);
        }
        bookingView.updateSectionVisibility(bookingSectionInnerModel);
    }

    public void init() {
        initCarDetails();

        bookingView.init(bookingMainModel);

        //Special Case
        bookingView.isDateOfBirthValid(isValid(bookingMainModel.getBookingCustomerModel().getDateOfBirth()));
        bookingView.isInvoiceDateValid(isValid(bookingMainModel.getBookingCarModel().getInvoiceDate()));
        if (!isBookStage) {
            bookingView.isDeliveryDateValid(isValid(bookingMainModel.getBookingCarModel().getDeliveryDate()));
        }


    }


    public void updateSection(int sectionId, int sectionImageId, boolean visibility) {
        updateSection(sectionId, sectionImageId, visibility, false);


    }

    ;

    public void updateSection(int sectionId, int sectionImageId) {
        updateSection(sectionId, sectionImageId, false, true);
    }

    ;


    //Car details for booking ----- Starts Here

    private void updateCarModelAdapter(final Item itemModel) {
        RealmResults<CarBrandModelsDB> carBrandModelsDBS = realm.where(CarBrandModelsDB.class)
                .equalTo("category", WSConstants.CAR_CATEGORY_BOTH)
                .findAll();
        List<Item> data = new ArrayList<Item>();
        for (int i = 0; i < carBrandModelsDBS.size(); i++) {
            if (carBrandModelsDBS.get(i) != null
                    && carBrandModelsDBS.get(i).getModel_name() != null) {
                data.add(new Item(carBrandModelsDBS.get(i).getModel_name(), carBrandModelsDBS.get(i).getModel_id()));
            }
        }
        bookingRealmModel.setCarModelList(carBrandModelsDBS);
        bookingCarModel.setModelList(data);
        bookingView.updateCarModelAdapter(data);
        updateCarModelSelectedInternal(itemModel, true);


    }

    private void updateCarModelSelectedInternal(Item item, final boolean isInit) {
        bookingCarModel.setModelId(item.getId());
        bookingCarModel.setModelName(item.getText());
        System.out.println("Model SelectedName:" + bookingCarModel.getModelName());
        System.out.println("Model SelectedId:" + bookingCarModel.getModelId());


        /**
         * For With Model Editor
         CarBrandModelsDB realmData = bookingRealmModel.getCarModelList()
         .where().equalTo("model_id",
         item.getId()+"")
         .findFirst();*/


        CarBrandModelsDB realmData = bookingRealmModel.getCarModelList()
                .where().equalTo("model_id",
                        item.getId() + "")
                .findFirst();

        bookingRealmModel.setCarRealmModelSelected(realmData);
        if (realmData != null && realmData.isValid()) {
            RealmResults<CarBrandModelVariantsDB> carBrandModelVariantsDBS = realmData.getCar_variants().where()
                    .distinct("variant_id");

            List<Item> data = new ArrayList<>();

            for (int i = 0; i < carBrandModelVariantsDBS.size(); i++) {
                if (carBrandModelVariantsDBS.get(i) != null &&
                        carBrandModelVariantsDBS.get(i).getVariant() != null) {
                    data.add(new Item(carBrandModelVariantsDBS.get(i).getVariant(),
                            carBrandModelVariantsDBS.get(i).getVariant_id()));
                }

            }

            bookingRealmModel.setCarVariantDistinctList(carBrandModelVariantsDBS);
            bookingCarModel.setVariantList(data);
            bookingView.updateCarVariantAdapter(bookingCarModel.getVariantList(), isInit);
            if (isInit) {
                updateCarVariantSelected(new Item(bookingCarModel.getVariantName(),
                        bookingCarModel.getVariantId()), isInit);
            }
        }


    }

    public void updateCarModelSelected(Item item) {
        updateCarModelSelectedInternal(item, false);
        bookingView.isCarModelValid(true);
    }

    public void updateCarVariantSelected(Item item, final boolean isInit) {
        bookingCarModel.setVariantId(item.getId());
        bookingCarModel.setVariantName(item.getText());

        CarBrandModelVariantsDB fuelType = bookingRealmModel.getCarVariantDistinctList().where().equalTo("variant_id",
                item.getId() + "").findFirst();
        if (fuelType != null) {
            List<Item> dataFuel = new ArrayList<>();
            dataFuel.add(new Item(fuelType.getFuel_type(),
                    Util.getInt(fuelType.getFuel_type_id())));
            bookingCarModel.setFuelName(fuelType.getFuel_type());
            bookingCarModel.setFuelTypeId(Util.getInt(fuelType.getFuel_type_id()));
            bookingCarModel.setFuelTypeList(dataFuel);
            bookingView.updateCarFuelTypeAdapter(dataFuel);

        }
        RealmResults<CarBrandModelVariantsDB> carBrandModelVariantsDBS = bookingRealmModel.getCarRealmModelSelected().getCar_variants().where().equalTo("variant_id",
                item.getId() + "").findAll();


        List<Item> data = new ArrayList<>();
        for (int i = 0; i < carBrandModelVariantsDBS.size(); i++) {
            if (carBrandModelVariantsDBS.get(i) != null && carBrandModelVariantsDBS.get(i).getColor() != null) {
                data.add(new Item(carBrandModelVariantsDBS.get(i).getColor(), carBrandModelVariantsDBS.get(i).getColor_id()));
            }
        }
        bookingRealmModel.setColorList(carBrandModelVariantsDBS);
        bookingCarModel.setColorList(data);
        bookingView.updateCarColorTypeAdapter(bookingCarModel.getColorList(), isInit);
        bookingView.isCarVariantValid(true);
        isCarDetailsComplete();


    }

    public void updateCarFuelType(Item data) {
        bookingCarModel.setFuelTypeId(data.getId());
        bookingCarModel.setFuelName(data.getText());
        bookingView.hideKeyBoard();
        bookingView.isCarFuelTypeValid(true);
        isCarDetailsComplete();
    }

    public void updateCarColor(Item data) {

        bookingCarModel.setColorTypeId(data.getId());
        bookingCarModel.setColorName(data.getText());

        bookingView.hideKeyBoard();
        isCarDetailsComplete();

    }

    public void resetCarDetails(int from, String text) {
        if(bookingMainModel.getStageId() == WSConstants.CarBookingStages.DELIVERED || bookingMainModel.getStageId() == WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING) {
            return;
        }
        {
            switch (from) {
                case BookingPresenter.CAR_BOOKING_RESET_ALL:
                    bookingRealmModel.setCarModelList(null);
                    bookingCarModel.setVariantList(null);
                    break;
                case BookingPresenter.CAR_BOOKING_RESET_AFTER_MODEL:
                    boolean isModelValueChanged = true;
                    for (int i = 0; i < (bookingCarModel.getModelList() == null ? 0 :
                            bookingCarModel.getModelList().size()); i++) {
                        if (text != null && text.equalsIgnoreCase(bookingCarModel.getModelName())) {
                            isModelValueChanged = false;
                            break;
                        }
                    }
                    if (isModelValueChanged) {
                        bookingCarModel.setModelName(null);
                        bookingCarModel.setModelId(-1);
                        bookingCarModel.setVariantName(null);
                        bookingCarModel.setVariantId(-1);
                        bookingCarModel.setFuelName(null);
                        bookingCarModel.setFuelTypeId(-1);
                        bookingCarModel.setColorName(null);
                        bookingCarModel.setColorTypeId(-1);
                        bookingRealmModel.setCarVariantDistinctList(null);
                        bookingCarModel.setVariantList(null);
                        bookingCarModel.setColorList(null);
                        bookingView.resetCarDetails(from);
                    }


                    break;
                case BookingPresenter.CAR_BOOKING_RESET_AFTER_VARIANT:
                    boolean isVariantValueChanged = true;
                    for (int i = 0; i < (bookingCarModel.getVariantList() == null ? 0 :
                            bookingCarModel.getVariantList().size()); i++) {
                        if (text != null && text.equalsIgnoreCase(bookingCarModel.getVariantName())) {
                            isVariantValueChanged = false;
                            break;
                        }
                    }
                    if (isVariantValueChanged) {
                        bookingCarModel.setVariantName(null);
                        bookingCarModel.setVariantId(-1);
                        bookingCarModel.setFuelName(null);
                        bookingCarModel.setFuelTypeId(-1);
                        bookingCarModel.setColorName(null);
                        bookingCarModel.setColorTypeId(-1);
                        bookingCarModel.setColorList(null);
                        bookingView.resetCarDetails(from);
                    }

                    break;
                case BookingPresenter.CAR_BOOKING_RESET_AFTER_FUEL_TYPE:

                    boolean isFuelValueChanged = true;
                    for (int i = 0; i < (bookingCarModel.getVariantList() == null ? 0 :
                            bookingCarModel.getVariantList().size()); i++) {
                        if (text != null && text.equalsIgnoreCase(bookingCarModel.getFuelName())) {
                            isFuelValueChanged = false;
                            break;
                        }
                    }
                    if (isFuelValueChanged) {
                        bookingCarModel.setFuelName(null);
                        bookingCarModel.setFuelTypeId(-1);
                        bookingView.resetCarDetails(from);
                    }
                    break;
                case BookingPresenter.CAR_BOOKING_RESET_AFTER_COLOR:

                    boolean isColorValueChanged = true;
                    for (int i = 0; i < (bookingCarModel.getColorList() == null ? 0 :
                            bookingCarModel.getColorList().size()); i++) {
                        if (text != null && text.equalsIgnoreCase(bookingCarModel.getColorName())) {
                            isColorValueChanged = false;
                            break;
                        }
                    }
                    if (isColorValueChanged) {
                        bookingCarModel.setColorName(null);
                        bookingCarModel.setColorTypeId(-1);
                        bookingView.resetCarDetails(from);
                    }
                    break;

            }
        }

        isCarDetailsComplete();
    }

    public void addCarQuantity() {
        bookingCarModel.setQuantity(bookingCarModel.getQuantity() + 1);
        bookingView.updateCarQuantity(bookingCarModel.getQuantity());
    }

    public void setCarQuantity(int quantity) {
        bookingCarModel.setQuantity(quantity);
        bookingView.updateCarQuantity(bookingCarModel.getQuantity());
    }

    public void removeCarQuantity() {
        if (bookingCarModel.getQuantity() > 1) {
            bookingCarModel.setQuantity(bookingCarModel.getQuantity() - 1);
        }
        bookingView.updateCarQuantity(bookingCarModel.getQuantity());
    }

    public void updateCarDeliveryDate(String date) {
        bookingCarModel.setDeliveryDate(date);
        bookingView.updateDeliveryDate(date);
        bookingView.isDeliveryDateValid(isValid(date));
        isCarDetailsComplete();

    }

    public void updateCarBookingAmount(String amount) {
        bookingCarModel.setBookingAmount(amount);
        if(bookingMainModel.getStageId() == WSConstants.CarBookingStages.BOOK_CAR) {
            boolean amountCheck;
            try{
                amountCheck = Util.getLongDefaultZero(amount)>0;
            }
            catch (Exception ignored){
                amountCheck = true;
            }
            bookingView.isBookingAmountValid(amountCheck);
        }

        isCarDetailsComplete();
    }
    public void updateCarInvoiceVinNumber(String vinNumber) {
        bookingCarModel.setVinNumber(vinNumber);
        if(bookingMainModel.getStageId() == WSConstants.CarBookingStages.INVOICE_PENDING) {
            bookingView.isInvoiceVinNumberValid(isValid(vinNumber));
        }

        isCarDetailsComplete();
    }

    public void updateCarInvoiceMobileNumber(String invoiceNumber) {
        bookingCarModel.setInvoiceMobileNumber(invoiceNumber);
        if(bookingMainModel.getStageId() == WSConstants.CarBookingStages.INVOICE_PENDING) {
            bookingView.isInvoiceMobileNumberValid(isValid(invoiceNumber, 10));
        }
        isCarDetailsComplete();
    }

    public void updateCarInvoiceDate(String date) {
        bookingCarModel.setInvoiceDate(date);
        bookingView.updateInvoiceDate(date);
        if(bookingMainModel.getStageId() == WSConstants.CarBookingStages.INVOICE_PENDING) {
            bookingView.isInvoiceDateValid(isValid(date));
        }

        isCarDetailsComplete();

    }
    public void updateCarDeliveryChallan(String challan) {
        bookingCarModel.setDeliveryChallan(challan);
        bookingView.isDeliveryChallanValid(isValid(challan));
        isCarDetailsComplete();
    }

    private void initCarDetails() {

        String modelNameTemp = bookingCarModel.getModelName();
        int modelIdTemp = bookingCarModel.getModelId();

        String variantNameTemp = bookingCarModel.getVariantName();
        int variantIdTemp = bookingCarModel.getVariantId();

        String fuelTypeNameTemp = bookingCarModel.getFuelName();
        int fuelTypeIdTemp = bookingCarModel.getFuelTypeId();

        String colorNameTemp = bookingCarModel.getColorName();
        int colorIdTemp = bookingCarModel.getColorTypeId();


        System.out.println("bookingMainModel.getStageId():" + bookingMainModel.getStageId());


        bookingView.initCarDetails(modelNameTemp,
                variantNameTemp,
                fuelTypeNameTemp,
                colorNameTemp,
                bookingMainModel.getStageId() == WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING ||
                        bookingMainModel.getStageId() == WSConstants.CarBookingStages.DELIVERED,
                bookingMainModel.getStageId() != WSConstants.CarBookingStages.BOOK_CAR,
                bookingMainModel.getStageId() == WSConstants.CarBookingStages.INVOICE_PENDING ||
                bookingMainModel.getStageId() == WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING ||
                        bookingMainModel.getStageId() == WSConstants.CarBookingStages.DELIVERED
        );


        bookingCarModel.setModelId(modelIdTemp);
        bookingCarModel.setModelName(modelNameTemp);


        bookingCarModel.setVariantId(variantIdTemp);
        bookingCarModel.setVariantName(variantNameTemp);

        bookingCarModel.setFuelTypeId(fuelTypeIdTemp);
        bookingCarModel.setFuelName(fuelTypeNameTemp);

        bookingCarModel.setColorTypeId(colorIdTemp);
        bookingCarModel.setColorName(colorNameTemp);

        Item itemModel = new Item(modelNameTemp,
                modelIdTemp);

        System.out.println("ColorTypeIdAfter:" + bookingCarModel.getColorTypeId());
        updateCarModelAdapter(itemModel);


    }

    private void isExchFinDetailComplete(){
        System.out.println("Called bro!! ");
        if(bookingMainModel.getStageId() == WSConstants.CarBookingStages.BOOK_CAR) {
            exchFinDetailDoneStatus = false;
        }else{
            exchFinDetailDoneStatus = isValid(bookingExchFinModel.getFinance()) && isValid(bookingExchFinModel.getExchange());
            bookingView.isExchFinValid(true ,getIcon(exchFinDetailDoneStatus), true);
        }
        isAllDetailsDone();
    }

    private void isCarDetailsComplete() {
        boolean deliveryCheck = true;
        boolean invoiceCheck = true;
        boolean bookingAmountCheck = true;
        if(bookingMainModel.getStageId() == WSConstants.CarBookingStages.BOOK_CAR) {
            try {
                bookingAmountCheck = Util.getLongDefaultZero(bookingCarModel.getBookingAmount()) > 0;
            }
            catch (Exception ignored) {
                bookingAmountCheck = true;
            }

        }
        if(bookingMainModel.getStageId() ==  WSConstants.CarBookingStages.INVOICE_PENDING) {
            invoiceCheck = isValid(bookingCarModel.getInvoiceDate())
                    && isValid(bookingCarModel.getInvoiceMobileNumber(),10)
                    && isValid(bookingCarModel.getVinNumber());
        }
        if (bookingMainModel.getStageId() == WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING ||
                bookingMainModel.getStageId() == WSConstants.CarBookingStages.DELIVERED) {
            deliveryCheck = isValid(bookingCarModel.getDeliveryDate()) && isValid(bookingCarModel.getDeliveryChallan());
        }
        if(bookingMainModel.getStageId() == WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING) {
            carDetailsCompleteStatus = deliveryCheck
                    && bookingAmountCheck
                    && invoiceCheck;
        }
        else {
            carDetailsCompleteStatus = isValid(bookingCarModel.getModelName())
                    && isValid(bookingCarModel.getModelId())
                    && isValid(bookingCarModel.getVariantName())
                    && isValid(bookingCarModel.getVariantId())
                    && isValid(bookingCarModel.getFuelName())
                    && isValid(bookingCarModel.getFuelTypeId())
                    && isValid(bookingCarModel.getColorName())
                    && isValid(bookingCarModel.getColorTypeId())
                    && deliveryCheck
                    && bookingAmountCheck
                    && invoiceCheck;
        }


        bookingView.isCarDetailsComplete(carDetailsCompleteStatus, getIcon(carDetailsCompleteStatus));
        isAllDetailsDone();
        System.out.println(bookingAmountCheck+"BookingAmount:" + bookingCarModel.getBookingAmount());
    }

    private VectorDrawableCompat getIcon(boolean isSuccess) {
        return isSuccess ? bookingMainModel.getIconSuccess() : bookingMainModel.getIconError();
    }

    //Car details for booking ----- Ends Here


    //Customer details for the booking ------Starts Here
    public void updateCustomerBookingName(String s) {
        bookingView.isBookingNameValid(isValid(s));
        bookingCustomerModel.setBookingName(s);
        isCustomerDetailsComplete();

    }

    public void updateCustomerDOB(String dob) {
        bookingCustomerModel.setDateOfBirth(dob);
        bookingView.updateDateOfBirth(dob);

        bookingView.isDateOfBirthValid(isValid(dob));

        isCustomerDetailsComplete();

    }

    public void updateAddress(String address) {
        bookingCustomerModel.setAddress(address);
        if (!isBookStage) {
            bookingView.isAddressValid(isValid(address));
        }
        isCustomerDetailsComplete();

    }

    public void updatePinCode(String pin) {
        bookingCustomerModel.setPinCode(pin);
        if (!isBookStage || isValid(bookingCustomerModel.getPinCode())) {
            bookingView.isPinCodeValid(isValid(pin, 6));
        }
        isCustomerDetailsComplete();

    }

    public void setCareOfType(String careOfType) {
        bookingCustomerModel.setCareOfType(careOfType);
        isCustomerDetailsComplete();

    }

    public void setCareOf(String careOf) {
        bookingCustomerModel.setCareOf(careOf);
        if (!isBookStage) {
            bookingView.isCareOfValueValid(isValid(careOf));
        }
        isCustomerDetailsComplete();

    }

    public void setCustomerType(int id) {
        bookingCustomerModel.setCustomerType(id);
        bookingView.updateOnCustomerTypeChange(isGstRequired(), isAadhaarRequired(), isDateOfBirthRequired(), isCustomerCareOfVisible());
        isCustomerDetailsComplete();

    }

    public void setPanNumber(String panNumber) {
        bookingCustomerModel.setPanNumber(panNumber);
        bookingView.isPanValid(isValid(panNumber, 10));
        isCustomerDetailsComplete();

    }

    public void setAadhaar(String aadhaar) {
        bookingCustomerModel.setAadhaar(aadhaar);
        bookingView.isAadhaarValid(isValid(aadhaar, 12));
        isCustomerDetailsComplete();

    }

    public void setGST(String gst) {
        bookingCustomerModel.setGst(gst);
        bookingView.isGstValid(isValid(gst, 15));
        isCustomerDetailsComplete();
    }

    private void isCustomerDetailsComplete() {
        boolean isDOBRequiredCheck = true;
        boolean isCustomerCareOfTypeCheck = true;
        boolean isCustomerCareOfCheck = true;
        if(isDateOfBirthRequired()) {
            isDOBRequiredCheck = isValid(bookingCustomerModel.getDateOfBirth());
        }
        if(isCustomerCareOfVisible()) {
            isCustomerCareOfTypeCheck = isValid(bookingCustomerModel.getCareOfType());
            isCustomerCareOfCheck =isValid(bookingCustomerModel.getCareOf());
        }
        if (bookingMainModel.getStageId() == WSConstants.CarBookingStages.BOOK_CAR) {
            boolean checkRequired = true;
            if (isValid(bookingCustomerModel.getPinCode())) {
                checkRequired = isValid(bookingCustomerModel.getPinCode(), 6);
            }

            customerDetailsDoneStatus = checkRequired
                    && isValid(bookingCustomerModel.getBookingName())
                    && isDOBRequiredCheck
                    && bookingCustomerModel.getCustomerType() > 0
                    && (!isAadhaarRequired() || isValid(bookingCustomerModel.getAadhaar(), 12))
                    && (!isGstRequired() || isValid(bookingCustomerModel.getGst(), 15))
                    && isValid(bookingCustomerModel.getPanNumber(), 10);
        } else {
            customerDetailsDoneStatus = isValid(bookingCustomerModel.getBookingName())
                    && isDOBRequiredCheck
                    && isValid(bookingCustomerModel.getAddress())
                    && isValid(bookingCustomerModel.getPinCode(), 6)
                    && isCustomerCareOfTypeCheck
                    && isCustomerCareOfCheck
                    && bookingCustomerModel.getCustomerType() > 0
                    && isValid(bookingCustomerModel.getPanNumber(), 10)
                    && (!isAadhaarRequired() || isValid(bookingCustomerModel.getAadhaar(), 12))
                    && (!isGstRequired() || isValid(bookingCustomerModel.getGst(), 15));
        }
        bookingView.isCustomerDetailsComplete(customerDetailsDoneStatus, customerDetailsDoneStatus ? bookingMainModel.getIconSuccess() : bookingMainModel.getIconError());
        isAllDetailsDone();

    }

    private boolean isDateOfBirthRequired() {
        return bookingCustomerModel.getCustomerType() != CUSTOMER_TYPE_CORPORATE_COMPANY &&
                bookingCustomerModel.getCustomerType() != CUSTOMER_TYPE_FLEET_COMPANY;
    }

    private boolean isCustomerCareOfVisible() {
        return bookingCustomerModel.getCustomerType() != CUSTOMER_TYPE_CORPORATE_COMPANY &&
                bookingCustomerModel.getCustomerType() != CUSTOMER_TYPE_FLEET_COMPANY;
    }

    private boolean isGstRequired() {
        return bookingCustomerModel.getCustomerType() == CUSTOMER_TYPE_CORPORATE_COMPANY ||
                bookingCustomerModel.getCustomerType() == CUSTOMER_TYPE_FLEET_COMPANY;
    }

    private boolean isAadhaarRequired() {
        return bookingCustomerModel.getCustomerType() != CUSTOMER_TYPE_CORPORATE_COMPANY &&
                bookingCustomerModel.getCustomerType() != CUSTOMER_TYPE_FLEET_COMPANY;
    }

    //Customer details for the booking ------Ends Here


    //Price Breakup for booking ------Starts Here
    public void updatePriceExShowRoom(String s) {
        bookingPriceBreakupModel.setExShowRoomPrice(s);
        updatePriceBreakup();
    }

    public void updatePriceInsurance(String s) {
        bookingPriceBreakupModel.setInsurance(s);
        updatePriceBreakup();
    }

    public void updatePriceRegistration(String s) {
        bookingPriceBreakupModel.setRegistration(s);
        updatePriceBreakup();
    }

    public void updatePriceAccessories(String s) {
        bookingPriceBreakupModel.setAccessories(s);
        updatePriceBreakup();
    }

    public void updatePriceExtendedWarranty(String s) {
        bookingPriceBreakupModel.setExtendedWarranty(s);
        updatePriceBreakup();
    }

    public void updatePriceOthers(String s) {
        bookingPriceBreakupModel.setOthers(s);
        updatePriceBreakup();
    }

    private void updatePriceBreakup() {
        boolean priceRequired = true;
        if (bookingMainModel.getStageId() == WSConstants.CarBookingStages.BOOK_CAR) {
            priceRequired = false;
        }
        priceBreakupTotal = 0;
        long exShowRoomPrice = 0;
        long insurance = 0;
        long registration = 0;
        long accessories = 0;
        long extendedWarranty = 0;
        long other = 0;
        try {
            exShowRoomPrice = Util.getLongDefaultZero(bookingPriceBreakupModel.getExShowRoomPrice());
            insurance = Util.getLongDefaultZero(bookingPriceBreakupModel.getInsurance());
            registration = Util.getLongDefaultZero(bookingPriceBreakupModel.getRegistration());
            accessories = Util.getLongDefaultZero(bookingPriceBreakupModel.getAccessories());
            extendedWarranty = Util.getLongDefaultZero(bookingPriceBreakupModel.getExtendedWarranty());
            other = Util.getLongDefaultZero(bookingPriceBreakupModel.getOthers());
            priceBreakupTotal = exShowRoomPrice
                    + insurance
                    + registration
                    + accessories
                    + extendedWarranty
                    + other;
        } catch (Exception e) {
            priceBreakupTotal = -1;
            e.printStackTrace();
        }
        priceDetailsDoneStatus = exShowRoomPrice > 0 && priceBreakupTotal > 0;

        if (isBookStage) {
            if (priceBreakupTotal > 0 && exShowRoomPrice <= 0) {
                priceRequired = true;
                bookingView.isExShowRoomPriceValid(false);
            } else {
                bookingView.isExShowRoomPriceValid(true);
            }
        } else {
            bookingView.isExShowRoomPriceValid(priceDetailsDoneStatus);
        }

        bookingView.updatePriceBreakup(priceBreakupTotal == -1 ? "Tooooo looooooong" : priceBreakupTotal + "", priceDetailsDoneStatus, getIcon(priceDetailsDoneStatus), priceRequired);

        if (getTotalDiscount() >= 0 && getTotalDiscount() < priceBreakupTotal) {
            updateTotalDiscount();
        }
        isAllDetailsDone();
    }

    //Price Breakup for booking ------End Here


    //Discount for booking ------Starts Here
    public void updateDiscountCorporate(String s) {
        bookingDiscountModel.setCorporate(s);
        updateTotalDiscount();
    }

    public void updateDiscountExBonus(String s) {
        bookingDiscountModel.setExchangeBonus(s);
        updateTotalDiscount();
    }

    public void updateDiscountLoyalty(String s) {
        bookingDiscountModel.setLoyaltyBonus(s);
        updateTotalDiscount();
    }

    public void updateDiscountOemScheme(String s) {
        bookingDiscountModel.setOemScheme(s);
        updateTotalDiscount();
    }

    public void updateDiscountDealer(String s) {
        bookingDiscountModel.setDealer(s);
        updateTotalDiscount();
    }

    public void updateDiscountAccessories(String s) {
        bookingDiscountModel.setAccessories(s);
        updateTotalDiscount();
    }

    private long getTotalDiscount() {
        long total;
        try {
            total = Util.getLongDefaultZero(bookingDiscountModel.getCorporate())
                    + Util.getLongDefaultZero(bookingDiscountModel.getExchangeBonus())
                    + Util.getLongDefaultZero(bookingDiscountModel.getLoyaltyBonus())
                    + Util.getLongDefaultZero(bookingDiscountModel.getOemScheme())
                    + Util.getLongDefaultZero(bookingDiscountModel.getDealer())
                    + Util.getLongDefaultZero(bookingDiscountModel.getAccessories());
        } catch (Exception e) {
            total = -1;
            e.printStackTrace();
        }
        return total;
    }

    private void updateTotalDiscount() {
        boolean priceRequired = true;
        long total = getTotalDiscount();
        if (bookingMainModel.getStageId() == WSConstants.CarBookingStages.BOOK_CAR) {
            priceRequired = false;
        }
        if (priceBreakupTotal < total) {
            priceRequired = true;
        }

        discountDetailsDoneStatus = priceBreakupTotal - total > 0;
        bookingView.updateTotalDiscount(total == -1 ? "Tooooo looooooong" : -total + "",
                total != -1 && discountDetailsDoneStatus,
                getIcon(total != -1 && discountDetailsDoneStatus), priceRequired);
        isAllDetailsDone();
    }

    /*Deallocate*/
    public void updateDeallocation(String allocation_id, String vinNum, boolean mandatoryVin){
        /*if(isChecked){
            bookingMainModel.setVin_allocation_status_id("2");
        }else{
            bookingMainModel.setVin_allocation_status_id("3");
        }*/
        System.out.println("allocationId: "+allocation_id + ", "+vinNum + ", "+mandatoryVin);
        if(mandatoryVin) {
            if(vinNum!= null && !vinNum.equalsIgnoreCase("")) {
                bookingCarAllocationStatus = true;
                bookingMainModel.setVin_allocation_status_id(allocation_id);
                bookingMainModel.setVin_no(vinNum);
            }else{
                bookingCarAllocationStatus = false;
            }
        }else{
            bookingCarAllocationStatus = true;
            bookingMainModel.setVin_allocation_status_id(allocation_id);
            bookingMainModel.setVin_no(vinNum);
        }

        System.out.println("bookingCarAllocationStatus "+ bookingCarAllocationStatus);
        isAllDetailsDone();

    }

    //Discount for booking ------End Here

    //Exchange-finance starts here

    public void updateFinance(String s){
        bookingExchFinModel.setFinance(s);
        isExchFinDetailComplete();
    }

    public void updateExchange(String s){
        bookingExchFinModel.setExchange(s);
        isExchFinDetailComplete();
    }

    //Exchange-finance ends here

    private void isAllDetailsDone() {
        System.out.println("bookingMainModel.getStageId(): "+ bookingMainModel.getStageId());
        switch (bookingMainModel.getStageId()) {
            case WSConstants.CarBookingStages.BOOK_CAR:
                boolean priceValid = true;
                long exShowRoom;
                try {
                    exShowRoom = Util.getLongDefaultZero(bookingPriceBreakupModel.getExShowRoomPrice());
                } catch (Exception e) {
                    exShowRoom = 0;
                }
                if (priceBreakupTotal > 0 && exShowRoom <= 0) {
                    priceValid = false;
                } else if (getTotalDiscount() > priceBreakupTotal) {
                    priceValid = false;
                }

                bookingView.isEnableSaveBookingDetails(carDetailsCompleteStatus
                        && customerDetailsDoneStatus && priceValid && bookingCarAllocationStatus);
                break;
            case WSConstants.CarBookingStages.CAR_BOOKED_HALF_PAYMENT:
            case WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING:
                bookingView.isEnableSaveBookingDetails(carDetailsCompleteStatus
                        && customerDetailsDoneStatus
                        && priceDetailsDoneStatus
                        && discountDetailsDoneStatus && exchFinDetailDoneStatus && bookingCarAllocationStatus

                );
                break;

            default:
                bookingView.isEnableSaveBookingDetails(carDetailsCompleteStatus
                        && customerDetailsDoneStatus
                        && priceDetailsDoneStatus
                        && discountDetailsDoneStatus && bookingCarAllocationStatus
                );
        }

    }

    public void copyFromBookingOne(BookingMainModel bookingMainModel) {
        this.bookingMainModel = bookingMainModel;
        bookingCarModel = bookingMainModel.getBookingCarModel();
        bookingCustomerModel = bookingMainModel.getBookingCustomerModel();
        bookingPriceBreakupModel = bookingMainModel.getBookingPriceBreakupModel();
        bookingDiscountModel = bookingMainModel.getBookingDiscountModel();
        bookingExchFinModel = bookingMainModel.getBookingExchFinModel();
        init();
    }

    private boolean isValid(String text, int count) {
        return text != null && text.trim().length() == count;
    }

    private boolean isValidMoreThan(String text, int count) {
        return text != null && text.trim().length() == count;
    }

    private boolean isValid(String text) {
        return text != null && !text.trim().isEmpty();
    }

    private boolean isValid(Integer text) {
        return text != null && text > 0;
    }

    public interface BookingView {
        void init(BookingMainModel bookingMainModel);

        void initCarDetails(String carMode, String variant, String fuelType, String color, boolean showDeliveryDetails, boolean hideQuantity, boolean showInvoiceDetails);

        void initCustomerType(int customerType);

        void updateCarModelAdapter(List<Item> data);

        void updateCarVariantAdapter(List<Item> data, boolean isInit);

        void updateCarFuelTypeAdapter(List<Item> data);

        void updateCarColorTypeAdapter(List<Item> data, boolean isInit);

        void updateCarQuantity(Integer quantity);

        void updateInvoiceDate(String date);

        void updateDeliveryDate(String date);

        void resetCarDetails(int from);

        void isCarModelValid(boolean isValid);

        void isCarVariantValid(boolean isValid);

        void isCarFuelTypeValid(boolean isValid);


        void isInvoiceVinNumberValid(boolean isValid);

        void isInvoiceMobileNumberValid(boolean isValid);

        void isInvoiceDateValid(boolean isValid);

        void isBookingAmountValid(boolean isValid);

        void isDeliveryDateValid(boolean isValid);


        void isDeliveryChallanValid(boolean isValid);

        void isCarDetailsComplete(boolean isComplete, VectorDrawableCompat icon);

        void updateSectionVisibility(BookingSectionModel.BookingSectionInnerModel bookingSectionModel);

        void updateDateOfBirth(String dob);

        void isBookingNameValid(boolean valid);

        void isDateOfBirthValid(boolean isValid);

        void isAddressValid(boolean isValid);

        void isPinCodeValid(boolean isValid);

        void isCareOfValueValid(boolean isValid);

        void updateOnCustomerTypeChange(boolean gstVisibility, boolean aadhaarVisibility,
                                        boolean dobRequired, boolean customerCareOfVisibility);

        void isPanValid(boolean isValid);

        void isAadhaarValid(boolean isValid);

        void isGstValid(boolean isValid);

        void isCustomerDetailsComplete(boolean isComplete, VectorDrawableCompat vectorDrawableCompat);

        void updatePriceBreakup(String price, boolean isComplete, VectorDrawableCompat icon, boolean priceRequired);

        void isExShowRoomPriceValid(boolean isValid);

        void updateTotalDiscount(String discount, boolean complete, VectorDrawableCompat icon, boolean isRequired);

        void isExchFinValid(boolean isValid, VectorDrawableCompat icon,boolean isRequired);

        void isEnableSaveBookingDetails(boolean isAllComplete);


        void hideKeyBoard();

    }


}
