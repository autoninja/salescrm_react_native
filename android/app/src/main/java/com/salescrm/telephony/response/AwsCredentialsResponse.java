package com.salescrm.telephony.response;

import java.util.List;

public class AwsCredentialsResponse {
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private String s3_user_name;
        private String s3_password;
        private String s3_bucket_name;
        private String SessionToken;

        public String getS3_user_name() {
            return s3_user_name;
        }

        public void setS3_user_name(String s3_user_name) {
            this.s3_user_name = s3_user_name;
        }

        public String getS3_password() {
            return s3_password;
        }

        public void setS3_password(String s3_password) {
            this.s3_password = s3_password;
        }

        public String getS3_bucket_name() {
            return s3_bucket_name;
        }

        public void setS3_bucket_name(String s3_bucket_name) {
            this.s3_bucket_name = s3_bucket_name;
        }

        public String getSessionToken() {
            return SessionToken;
        }

        public void setSessionToken(String sessionToken) {
            SessionToken = sessionToken;
        }
    }

}
