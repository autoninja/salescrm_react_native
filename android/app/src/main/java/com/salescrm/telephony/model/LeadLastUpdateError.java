package com.salescrm.telephony.model;

public class LeadLastUpdateError {
    private String statusCode;

    private String message;


    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", error = "+error+"]";
    }

    public class Error
    {
        private Details details;

        private String type;

        public Details getDetails ()
        {
            return details;
        }

        public void setDetails (Details details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }

        public class Details
        {
            private String lead_last_updated;

            public String getLead_last_updated ()
            {
                return lead_last_updated;
            }

            public void setLead_last_updated (String lead_last_updated)
            {
                this.lead_last_updated = lead_last_updated;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [lead_last_updated = "+lead_last_updated+"]";
            }
        }
    }
}

