import React, { Component } from 'react'
import { View, Text, Image, Alert} from 'react-native'
import styles from '../styles/styles'


export default class GameIntroWelcome extends Component {


  constructor(props) {
    super(props);
  }
  componentDidMount () {

  }

  render() {
    return(
      <View style={styles.MainContainer}>
      <View style= {styles.MainContainerYellow}>
        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
          <Text style={{fontSize:30,fontStyle:'italic',color:'#fff'}}>Welcome to </Text>
          <Text style={{fontSize:30,fontStyle:'italic',fontWeight:'bold',color:'#fff'}}>NinjaX @</Text>
          <Text style={{fontSize:30,fontStyle:'italic',fontWeight:'bold',color:'#fff', paddingLeft:2, paddingRight:2}}>{this.props.payload.dealername}</Text>
        </View>

        <View style={{flex:1, backgroundColor:'rgba(0, 0, 0, 0.4);', alignItems:'center'}}>

        <View style={{flexDirection:'row', padding:10}}>
        <View style={{height:24, width:24, borderRadius:24/2, backgroundColor:'#fff', alignItems:'center', justifyContent:'center'}}>
        <Text style={{color:'#4B551E', fontSize:16}}>1</Text>
        </View>
        <Text style={{color:'#fff',fontWeight:'400',fontSize:19, paddingLeft:10}}>Make your team win</Text>
        </View>
        <Image source={require('../images/team_win.png')}
        style={{flex:1,margin:10,}}
        resizeMode = 'contain'
         />


        </View>

        <View style={{flex:1, borderBottomLeftRadius:6, alignItems:'center', borderBottomRightRadius:6,backgroundColor:'rgba(0, 0, 0, 0.7);'}}>
        <View style={{flexDirection:'row', padding:10}}>
        <View style={{height:24, width:24, borderRadius:24/2, backgroundColor:'#fff', alignItems:'center', justifyContent:'center'}}>
        <Text style={{color:'#4B551E', fontSize:16}}>2</Text>
        </View>
        <Text style={{color:'#fff',fontWeight:'400',fontSize:19, paddingLeft:10}}>Fight to be the best consultant</Text>
        </View>
        <Image source={require('../images/best_consultant.png')}
        style={{flex:1,margin:10,}}
        resizeMode = 'contain'
         />
        </View>

      </View>
      <View style={{alignItems: 'center', padding:10,flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
      {this.props.bottomViews}
      </View>
      </View>
    )
  }
}
