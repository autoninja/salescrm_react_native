package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 17-08-2016.
 */
public class LeadDetails extends RealmObject {

    public static final String LEADDETAILS = "LeadDetails";

    @PrimaryKey
    private int leadId;
    private int customerID;
    private String leadTagNames;
    private String leadTagColors;
    private String leadDseNames;
    @Index
    private String leadDseMobileNumber;
    private String leadStageId;
    private String leadStage;
    private String leadAge;
    private String leadSourceName;
    private String leadLastUpdated;
    private String leadExpectedClosingDate;
    private String leadLocationId;
    private String leadLocationName;
    private String leadEnquiryNumber;
    private String leadCloseDate;
    private int leadActive;
    private String leadCheckListId;
    private String leadCheckListValue;
    private String leadCheckListTitle;
    private String leadCarVariantName;
    private String leadCarModelName;

    public int getLeadActive() {
        return leadActive;
    }

    public String getLeadCarVariantName() {
        return leadCarVariantName;
    }

    public void setLeadCarVariantName(String leadCarVariantName) {
        this.leadCarVariantName = leadCarVariantName;
    }

    public String getLeadCarModelName() {
        return leadCarModelName;
    }

    public void setLeadCarModelName(String leadCarModelName) {
        this.leadCarModelName = leadCarModelName;
    }

    public String getLeadCheckListId() {
        return leadCheckListId;
    }

    public void setLeadCheckListId(String leadCheckListId) {
        this.leadCheckListId = leadCheckListId;
    }

    public String getLeadCheckListValue() {
        return leadCheckListValue;
    }

    public void setLeadCheckListValue(String leadCheckListValue) {
        this.leadCheckListValue = leadCheckListValue;
    }

    public String getLeadCheckListTitle() {
        return leadCheckListTitle;
    }

    public void setLeadCheckListTitle(String leadCheckListTitle) {
        this.leadCheckListTitle = leadCheckListTitle;
    }

    public int getLeadId() {
        return leadId;
    }

    public void setLeadId(int leadId) {
        this.leadId = leadId;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getLeadTagNames() {
        return leadTagNames;
    }

    public void setLeadTagNames(String leadTagNames) {
        this.leadTagNames = leadTagNames;
    }

    public String getLeadTagColors() {
        return leadTagColors;
    }

    public void setLeadTagColors(String leadTagColors) {
        this.leadTagColors = leadTagColors;
    }

    public String getLeadDseNames() {
        return leadDseNames;
    }

    public void setLeadDseNames(String leadDseNames) {
        this.leadDseNames = leadDseNames;
    }

    public String getLeadDseMobileNumber() {
        return leadDseMobileNumber;
    }

    public void setLeadDseMobileNumber(String leadDseMobileNumber) {
        this.leadDseMobileNumber = leadDseMobileNumber;
    }

    public String getLeadStageId() {
        return leadStageId;
    }

    public void setLeadStageId(String leadStageId) {
        this.leadStageId = leadStageId;
    }

    public String getLeadStage() {
        return leadStage;
    }

    public void setLeadStage(String leadStage) {
        this.leadStage = leadStage;
    }

    public String getLeadAge() {
        return leadAge;
    }

    public void setLeadAge(String leadAge) {
        this.leadAge = leadAge;
    }

    public String getLeadSourceName() {
        return leadSourceName;
    }

    public void setLeadSourceName(String leadSourceName) {
        this.leadSourceName = leadSourceName;
    }

    public String getLeadLastUpdated() {
        return leadLastUpdated;
    }

    public void setLeadLastUpdated(String leadLastUpdated) {
        this.leadLastUpdated = leadLastUpdated;
    }

    public String getLeadExpectedClosingDate() {
        return leadExpectedClosingDate;
    }

    public void setLeadExpectedClosingDate(String leadExpectedClosingDate) {
        this.leadExpectedClosingDate = leadExpectedClosingDate;
    }

    public String getLeadLocationId() {
        return leadLocationId;
    }

    public void setLeadLocationId(String leadLocationId) {
        this.leadLocationId = leadLocationId;
    }

    public String getLeadLocationName() {
        return leadLocationName;
    }

    public void setLeadLocationName(String leadLocationName) {
        this.leadLocationName = leadLocationName;
    }

    public String getLeadEnquiryNumber() {
        return leadEnquiryNumber;
    }

    public void setLeadEnquiryNumber(String leadEnquiryNumber) {
        this.leadEnquiryNumber = leadEnquiryNumber;
    }

    public String getLeadCloseDate() {
        return leadCloseDate;
    }

    public void setLeadCloseDate(String leadCloseDate) {
        this.leadCloseDate = leadCloseDate;
    }

    public int isLeadActive() {
        return leadActive;
    }

    public void setLeadActive(int leadActive) {
        this.leadActive = leadActive;
    }
}
