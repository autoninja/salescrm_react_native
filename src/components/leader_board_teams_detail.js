import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image, ScrollView} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import RestClient from '../network/rest_client'

const img = "https://tineye.com/images/widgets/mona.jpg";

export default class LeaderBoardTeamsDetails extends React.Component {

		constructor(props)
		{
		    super(props);
		    this.state = {
		    	isLoading: true,
		    	result: {},
					teamPointDetails : {},
			}
		}

		componentDidMount(){
					var teamPointDetails = this.props.navigation.getParam('item');
					if(teamPointDetails) {
						this.setState({teamPointDetails});
					}

					if(this.state.teamPointDetails) {
						  this.makeRemoteRequest(teamPointDetails.team_leader_id);
					}
					else {
							Alert.alert("Failed to get user info");
					}

  	}


  	makeRemoteRequest = (teamLeaderId) => {
  			this.setState({ isLoading: true});
  			new RestClient().getTeamLeaderDetails({teamLeaderId,date:getSelectedGameDate()}).then( (data) => {
		      if(data.statusCode == 4001 || data.statusCode == 5001) {
		                  Alert.alert(
		                'Failed',
		                'Please try again',
		                [
		                  {text: 'Logout', onPress: () => this.logout()},
		                  {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
		                ],
		                { cancelable: false }
		                )
		                  return;
		                }

		               this.setState({
						    		isLoading: false,
						    		result:data.result
									});


		          }).catch((error) => {
		            console.error("Error Team Details"+ error)
		            if (error.response && error.response.status == 401) {
		                Alert.alert(
		              'Failed',
		              'Please try again',
		              [
		                {text: 'Logout', onPress: () => this.logout()},
		                {text: 'Retry', onPress: () =>  this.makeRemoteRequest()},
		              ],
		              { cancelable: false }
		            )
		              }
		          });
			}

		_showUserInfo(navigation, name, dp_url) {
			 navigation.navigate('UserInfo', {
			   name,
			   dp_url,

		 });}	

		render () {
			let users = [];
			let navigation = this.props.navigation;
			if(this.state.result && this.state.result.points && this.state.result.points.users) {
				users =  this.state.result.points.users;
			}
			let userName = "";
			let userPoint = 0;
			let userPhoto = "";
			if(this.state.teamPointDetails) {
				userName = this.state.teamPointDetails.team_leader_name;
				userPoint = this.state.teamPointDetails.team_average_points;
				userPhoto = this.state.teamPointDetails.team_leader_photo_url;

			}
			if (this.state.isLoading) {
		      return (
		        <View style={{flex: 1, backgroundColor:'#fff', alignItems : 'center', justifyContent: 'center'}}>
		          <ActivityIndicator />
		        </View>
		      );
   			 }
   		let placeHolderTop = <View style={{position:'absolute',width:66, height:66, borderRadius: 66/2, alignItems:'center', backgroundColor: '#FF8000', justifyContent:'center'}}><Text style={{color:'#fff',alignItems: 'center', justifyContent:'center', textAlign: 'center', fontSize: 20}}>{userName.charAt(0).toUpperCase()}</Text></View>;;
		return (
			<View style={{flex: 1,  backgroundColor:'#2e5e86'}}>
		    	<View style={{width: '100%', backgroundColor:'#2e5e86',flexDirection: 'row', padding: 5}}>
		    	<TouchableOpacity onPress = { ()=> this.props.navigation.goBack(null)}>
						<View style={{flex:1, justifyContent: 'center' , alignItems: 'center', marginLeft: 5, marginTop: 10}}>
			    			<Image
						        style={{width: 12, height: 16, alignItems: 'center', justifyContent: 'center'}}
		                      	source={require('../images/ic_left_arrow.png')}
						        />
					     </View>
				     </TouchableOpacity>
			        <View style={{flex:8, marginTop:10, flexDirection: 'row', marginLeft: 15}}>
			        {placeHolderTop}
			        	<Image
		                  style={{width: 66, height: 66, borderRadius: 66/2, alignItems: 'center'}}
		                  source={{uri: userPhoto}}
		                />

		             <View style={{flexDirection: 'column', marginTop: 5, marginLeft: 5}}>
		             	<Text style={{color: '#fff', fontWeight: 'bold', fontSize: 15}}> {userName} </Text>
		             	<Text style={{color: '#fff', fontSize: 15}}> {userPoint+' Points'} </Text>
		             </View>
			        </View>
	    		</View>

	    		<View style={{width: '100%', height: 30, alignItems : 'center', backgroundColor:'#566781',justifyContent: 'center', flexDirection: 'row', marginTop: 10}}>
				    	<Text style={{flex: 2,  color: '#fff', textAlign: 'center'}}>Sales Consultant</Text>
				    	<Text style={{flex: 2, 	color: '#63ffa3', textAlign: 'center'}}>Points</Text>
			    </View>
			    <ScrollView>
				    <FlatList
				    	//ItemSeparatorComponent = {this.FlatListItemSeparator}
				    	data={users}
				    	renderItem= {({item, index}) =>{
				    		let placeHolder = <View style={{position:'absolute',width:36, height:36, borderRadius: 36/2,backgroundColor:'#FF8000', alignItems:'center', justifyContent:'center'}}><Text style={{color:'#fff'}}>{item.name.charAt(0).toUpperCase()}</Text></View>;
				    		return(
				    		<TouchableOpacity onPress = { () => this.props.navigation.navigate('ConsultantDetails',{item})}>
					    		<View style={{width: '100%', flex : 10, height: 50,flexDirection: 'row', alignItems : 'center', justifyContent: 'center', backgroundColor: '#09273b'}}>
						    		 <View style={{flex : 1, alignItems : 'center', justifyContent: 'center'}}>
						    		 	<Text style={{color: '#fff'}}> {index+1}</Text>
						    		 </View>
						    		 <TouchableOpacity /*onLongPress={this._showUserInfo.bind(this, navigation, item.name, item.dse_photo_url)}*/>
						    		 {placeHolder}
						    		 <Image
				                          style={{width: 36, height: 36, borderRadius: 36/2, alignItems: 'center'}}
				                          source={{uri: item.dse_photo_url}}
				                      />
				                      </TouchableOpacity>
				                      <View style={{flex : 4, alignItems : 'flex-start', justifyContent: 'center', paddingLeft: 15}}>
						    		 	<Text style={{color: '#fff', fontSize: 14}}> {item.name} </Text>
						    		 </View>

						    		 <View style={{flex : 4, alignItems : 'center', justifyContent: 'center'}}>
						    		 	<Text style={{color: '#63ffa3', fontSize: 12}}> {item.points} </Text>
						    		 </View>

						    		 <View style={{flex:1}}>
						    		 	{(item.above_average == 1)?
						    		 		<Image
				                          style={{width: 24, height: 24, alignItems: 'center'}}
				                          source={require('../images/ic_image_above_average.png')}
				                      />
				                      : null}
						    		 </View>

					    		</View>
					    		<View
								        style={{
								          height: 0.5,
								          width: "100%",
								          backgroundColor: "#d3d3d3",
								        }}
								      />
				    		</TouchableOpacity>
				    		)}}
				    	keyExtractor= {(item, index) => index.toString() }
			    	/>
			    	<View style={{width: '100%', height: 30, alignItems : 'center', backgroundColor:'#566781',justifyContent: 'center', flexDirection: 'row'}}>
					    	<Text style={{flex: 1,  color: '#fff', textAlign: 'center', fontWeight: 'bold'}}>Avg Team Point</Text>
					    	<Text style={{flex: 1, 	color: '#63ffa3', textAlign: 'center', fontWeight: 'bold', marginRight: 5, fontSize: 14}}>{userPoint}</Text>
					 </View>
			    </ScrollView>
		    	</View>
			);
	}
}

    FlatListItemSeparator = () => {
	    return (
	      <View
	        style={{
	          height: 1,
	          width: "100%",
	          backgroundColor: "#fff",

	        }}
	      />
	    );
  }
