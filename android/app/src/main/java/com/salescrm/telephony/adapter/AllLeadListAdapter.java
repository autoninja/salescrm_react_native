package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.db.LeadListDetails;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by prateek on 20/2/17.
 */

public class AllLeadListAdapter extends RecyclerView.Adapter<AllLeadListAdapter.ViewHolder>{

    private List<LeadListDetails> leadListDetailses;
    private Activity activity;
    private Preferences pref = null;
    private Realm realm;

    public AllLeadListAdapter(List<LeadListDetails> leadListDetails, Activity activity, Realm realm) {
        this.leadListDetailses = leadListDetails;
        this.activity = activity;
        this.pref = Preferences.getInstance();
        this.pref.load(activity);
        this.realm = realm;
    }

    @Override
    public AllLeadListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.enquiry_list_all_card, parent, false);
        return new AllLeadListAdapter.ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        if(leadListDetailses.get(position).getFirst_name() != null && !leadListDetailses.get(position).getFirst_name().equalsIgnoreCase("")) {
            viewHolder.tvLeadListName.setText(leadListDetailses.get(position).getFirst_name());
        }
        if(leadListDetailses.get(position).getVariant_name()!= null) {
            viewHolder.tvLeadListCar.setText(leadListDetailses.get(position).getVariant_name());
        }

        if(leadListDetailses.get(position).getLead_stage() != null) {
            viewHolder.tvLeadListStageName.setText(leadListDetailses.get(position).getLead_stage()+" ("+leadListDetailses.get(position).getStage_age()+" Days)");
        }

        if(showDseName()) {
            viewHolder.tvLeadListDseName.setVisibility(View.VISIBLE);
            if (leadListDetailses.get(position).getDse_name() != null && !leadListDetailses.get(position).getDse_name().equalsIgnoreCase("")) {
                viewHolder.tvLeadListDseName.setText("Dse : " + leadListDetailses.get(position).getDse_name());
            } else {
                viewHolder.tvLeadListDseName.setText("Dse : Not Available");
            }
        }else {
            viewHolder.tvLeadListDseName.setVisibility(View.GONE);
        }

        viewHolder.tvLeadListActionStageDays.setText(leadListDetailses.get(position).getLead_age()+"");

        if(leadListDetailses.get(position).getGender() != null) {
            if (leadListDetailses.get(position).getGender().equalsIgnoreCase("male")) {
                viewHolder.tvLeadListNameInitial.setText("Mr.");
            } else if (leadListDetailses.get(position).getGender().equalsIgnoreCase("female")) {
                viewHolder.tvLeadListNameInitial.setText("Ms.");
            }else{
                viewHolder.tvLeadListNameInitial.setText("Mr.");
            }
        }else {
            viewHolder.tvLeadListNameInitial.setText("Mr.");
        }

        if(leadListDetailses.get(position).getLead_tag_name() != null){
            String leadColor = leadListDetailses.get(position).getLead_tag_name();
            if(leadColor.contains("Hot")){
                viewHolder.cardView.setCardBackgroundColor(Color.parseColor("#f7412d"));
            }else if(leadColor.contains("Cold")){
                viewHolder.cardView.setCardBackgroundColor(Color.parseColor("#4DD0E1"));
            }else if(leadColor.contains("Warm")){
                viewHolder.cardView.setCardBackgroundColor(Color.parseColor("#ff9900"));
            }
        }

        for(int i=0; i < viewHolder.rLayoutLeadListProgressOval.getChildCount(); i++){
            if(viewHolder.rLayoutLeadListProgressOval.getChildAt(i) instanceof  ImageView &&
                    viewHolder.rLayoutLeadListProgressOval.getChildAt(i).getId() != R.id.tv_enquiry_list_indicator){
                viewHolder.imgList.add((ImageView) viewHolder.rLayoutLeadListProgressOval.getChildAt(i));
            }
        }

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewHolder.imgAllEnquiryListIndicator.getLayoutParams();
        int indicator = 0;
        if(leadListDetailses.get(position).getStage_id() != null) {
            if(leadListDetailses.get(position).getStage_id().equalsIgnoreCase("8")||leadListDetailses.get(position).getStage_id().equalsIgnoreCase("9")){
                indicator = Integer.parseInt(leadListDetailses.get(position).getStage_id()) - 2;
            }else {
                indicator = Integer.parseInt(leadListDetailses.get(position).getStage_id()) - 1;
            }
            System.out.println("IdicatorId:- "+indicator);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            params.addRule(RelativeLayout.ALIGN_START, activity.getResources().getIdentifier("oval_view_"+ indicator, "id", activity.getPackageName()));
        } else {
            params.addRule(RelativeLayout.ALIGN_LEFT, activity.getResources().getIdentifier("oval_view_"+indicator, "id", activity.getPackageName()));
        }
        viewHolder.imgAllEnquiryListIndicator.setLayoutParams(params);

        for(int j=0; j<leadListDetailses.get(position).getLead_stage_progress().size(); j++){

                int imageID;
                if(leadListDetailses.get(position).getLead_stage_progress().get(j).getStage_id() != 7 ){
                if(leadListDetailses.get(position).getLead_stage_progress().get(j).getStage_id() == 8 ||
                        leadListDetailses.get(position).getLead_stage_progress().get(j).getStage_id() == 9) {
                    imageID = leadListDetailses.get(position).getLead_stage_progress().get(j).getStage_id() -2;
                }else {
                    imageID = leadListDetailses.get(position).getLead_stage_progress().get(j).getStage_id() -1;
                }
                if(leadListDetailses.get(position).getLead_stage_progress().get(j).getWidth() == 100) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        viewHolder.imgList.get(imageID).setBackground(viewHolder.activeDraw);
                    } else {
                        viewHolder.imgList.get(imageID).setBackgroundDrawable(viewHolder.activeDraw);
                    }
                }else if(leadListDetailses.get(position).getLead_stage_progress().get(j).getWidth() == 0){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        viewHolder.imgList.get(imageID).setBackground(viewHolder.inActiveDraw);
                    } else {
                        viewHolder.imgList.get(imageID).setBackgroundDrawable(viewHolder.inActiveDraw);
                    }
                }
            }else{

                }
        }

    }

    @Override
    public int getItemCount() {
        return leadListDetailses.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        public View view;
        public ImageView imgCardStatus, imgMultipleOptions;

        public int currentItem;
        ImageView imgAllEnquiryListIndicator;
        RelativeLayout relAllEnquiryListOvalHolder, rlayoutLeadList, rLayoutLeadListProgressOval;
        TextView tvLeadListName, tvLeadListCar, tvLeadListStageName, tvLeadListNameInitial, tvLeadListActionStageDays, tvLeadListDseName;
        List<ImageView> imgList;
        Drawable activeDraw, inActiveDraw;
        CardView cardView;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            view = itemLayoutView;


            rlayoutLeadList = (RelativeLayout) itemLayoutView.findViewById(R.id.rel_enquiry_list);
            imgAllEnquiryListIndicator = (ImageView) itemLayoutView.findViewById(R.id.tv_enquiry_list_indicator);
            relAllEnquiryListOvalHolder = (RelativeLayout) itemLayoutView.findViewById(R.id.rel_enquiry_list_progress_oval);
            tvLeadListName = (TextView) itemLayoutView.findViewById(R.id.tv_enquiry_list_name);
            tvLeadListCar = (TextView) itemLayoutView.findViewById(R.id.tv_enquiry_list_car);
            rLayoutLeadListProgressOval = (RelativeLayout) itemLayoutView.findViewById(R.id.rel_enquiry_list_progress_oval);
            imgList = new ArrayList<>();
            activeDraw = ContextCompat.getDrawable(activity, R.drawable.progress_oval_active);
            inActiveDraw = ContextCompat.getDrawable(activity, R.drawable.progress_oval_inactive);
            tvLeadListStageName = (TextView) itemLayoutView.findViewById(R.id.leadStage);
            cardView = (CardView) itemLayoutView.findViewById(R.id.leadlist_cardviewall);
            tvLeadListNameInitial = (TextView) itemLayoutView.findViewById(R.id.tv_enquiry_list_name_mr);
            tvLeadListActionStageDays = (TextView) itemLayoutView.findViewById(R.id.tv_enquiry_list_action_stage);
            tvLeadListDseName = (TextView) itemLayoutView.findViewById(R.id.tv_enquiry_list_dse_name);

            rlayoutLeadList.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            pref.setLeadID(leadListDetailses.get(getAdapterPosition()).getLead_id());
            pref.setScheduledActivityId(Util.getInt(leadListDetailses.get(getAdapterPosition()).getScheduled_activity_id()));
            System.out.println("SC id:"+Util.getInt(leadListDetailses.get(getAdapterPosition()).getScheduled_activity_id()));
            if(pref.getLeadID().equalsIgnoreCase("")){

            }else {
                Intent intent = new Intent(activity, C360Activity.class);
                activity.startActivity(intent);
            }
        }
    }

    public boolean showDseName() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if(data!=null){
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i=0; i<userRoles.size();i++){
                if(userRoles.get(i).getId().equalsIgnoreCase(WSConstants.TEAM_LEAD_ROLE_ID) || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.MANAGER_ROLE_ID)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_BRANCH_HEAD)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_OPERATIONS) || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR)){
                    return true;
                }
            }
        }
        return false;
    }

}
