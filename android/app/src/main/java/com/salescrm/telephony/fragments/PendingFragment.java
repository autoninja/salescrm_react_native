package com.salescrm.telephony.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.PendingTabAdapter;
import com.salescrm.telephony.db.AnalyticsDB;
import com.salescrm.telephony.db.PendingStatsDB;
import com.salescrm.telephony.db.WeeklyData;
import com.salescrm.telephony.model.Locations;
import com.salescrm.telephony.model.PendingStatsResponse;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.lang.reflect.Field;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

import static com.salescrm.telephony.R.id.blank_msg;
import static com.salescrm.telephony.R.id.swipeContainer;

/**
 * Created by bannhi on 11/9/17.
 */

public class PendingFragment extends Fragment {
   static  private TabLayout tabLayout;
    static private ViewPager viewPager;
    static TextView blank_msg_tv;
    int dummyNumberOfLocations = 20;
    private RelativeLayout rel_loading_frame;
    static ConnectionDetectorService connectionDetectorService;
    static SwipeRefreshLayout swipeRefreshLayout;
    static Preferences pref;
    static Realm realm;
    static PendingTabAdapter adapter;
    // static ArrayList<String> Locations = new ArrayList<String>();
    public static PendingFragment newInstance(int i, String pending) {
        Bundle args = new Bundle();
        args.putInt("someInt", i);
        args.putString("someTitle", pending);
        PendingFragment fragment = new PendingFragment();
        fragment.setArguments(args);
        System.out.println("PENDING IS CALLED");
        return fragment;
    }

    @Override
    public void onResume() {
        System.out.println("PENDING IS CALLED");
        super.onResume();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.pending_fragment, container, false);
        System.out.println("PENDING IS CALLED");
        viewPager = (ViewPager) rView.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) rView.findViewById(R.id.sliding_tabs);
        blank_msg_tv = (TextView) rView.findViewById(blank_msg);
        rel_loading_frame = (RelativeLayout) rView.findViewById(R.id.rel_loading_frame);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        adapter = new PendingTabAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(5);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //do stuff here
                viewPager.setCurrentItem(tab.getPosition());

                // viewPager.getAdapter().notifyDataSetChanged();
                System.out.println("TAB SELECTED");
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                System.out.println("TAB SELECTED UN");
                //adapter.notifyDataSetChanged();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                System.out.println("TAB SELECTED RE");
            }
        });
        adapter.updateView();
        connectionDetectorService = new ConnectionDetectorService(getActivity());
        if (connectionDetectorService.isConnectingToInternet()) {
            rel_loading_frame.setVisibility(View.VISIBLE);
            fetchPendingStats();
        } else {
            Toast.makeText(getContext(), "PLease enable internet", Toast.LENGTH_LONG);
        }

        swipeRefreshLayout = (SwipeRefreshLayout) rView.findViewById(swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
                    fetchPendingStats();
                } else {
                    Util.showToast(getContext(), "No internet connection", Toast.LENGTH_SHORT);
                    setSwipeRefreshView(false);
                }
            }
        });

        return rView;
    }
    private void setSwipeRefreshView(final boolean val) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(val);
            }
        });
    }

    public void fetchPendingStats() {
        System.out.println("FETCHH CALLED");
        pref = Preferences.getInstance();
        pref.load(getActivity());
        final Realm realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(getActivity());
        if (connectionDetectorService.isConnectingToInternet()) {
            if (rel_loading_frame != null) {
                rel_loading_frame.setVisibility(View.VISIBLE);
            }

            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getPendingStats(new Callback<PendingStatsResponse>() {
                @Override
                public void success(PendingStatsResponse pendingStatsResponse, Response response) {
                    if (pendingStatsResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {

                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }
                        }

                        if(!Util.isFragmentSafe(PendingFragment.this)){
                            return;
                        }
                        addtoDB(pendingStatsResponse.getResult(), realm);
                        switchTabOnNotification();

                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    rel_loading_frame.setVisibility(View.GONE);
                    Util.showToast(getContext(), "Server not responding - " + error.getMessage(), Toast.LENGTH_SHORT);
                }
            });
        } else {
        }

    }

    public void addtoDB(final PendingStatsResponse.Result[] result, Realm realm) {
        WSConstants.Locations.clear();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(PendingStatsDB.class);
                realm.delete(AnalyticsDB.class);
                realm.delete(WeeklyData.class);
                for (int i = 0; i < result.length; i++) {
                    PendingStatsResponse.Result resultObj = result[i];
                    WSConstants.Locations.add(new Locations(resultObj.getLocation_id(), resultObj.getLocation_name()));
                    for (int j = 0; j < resultObj.getUsers().length; j++) {
                        PendingStatsDB pendingStatus = new PendingStatsDB();
                        pendingStatus.setLocId(resultObj.getLocation_id());
                        pendingStatus.setLocName(resultObj.getLocation_name());
                        PendingStatsResponse.Users usersObj = resultObj.getUsers()[j];
                        pendingStatus.setUserId(usersObj.getId());
                        pendingStatus.setTeam_member(usersObj.getTeam_member());
                        pendingStatus.setUserName(usersObj.getName());
                        pendingStatus.setUserImage(usersObj.getImage());
                        pendingStatus.setVisibleTimes(usersObj.getVisible_times());
                        pendingStatus.setMedianDelay(usersObj.getMedian_delay());
                        pendingStatus.setPendingCount(usersObj.getPending_count());
                        pendingStatus.setPendingCountDiff(usersObj.getPending_count_difference());
                        if(usersObj.getCalls()!=null){
                            for (int k = 0; k < usersObj.getCalls().length; k++) {
                                if (("" + usersObj.getCalls()[k].getRole_id()).equalsIgnoreCase(WSConstants.TEAM_LEAD_ROLE_ID)) {
                                    pendingStatus.setTlName(usersObj.getCalls()[k].getName());
                                    pendingStatus.setTlImage(usersObj.getCalls()[k].getImage());
                                    pendingStatus.setTlNum(usersObj.getCalls()[k].getPhone());
                                } else if (("" + usersObj.getCalls()[k].getRole_id()).equalsIgnoreCase(WSConstants.MANAGER_ROLE_ID)) {
                                    pendingStatus.setManagerName(usersObj.getCalls()[k].getName());
                                    pendingStatus.setManagerImage(usersObj.getCalls()[k].getImage());
                                    pendingStatus.setManagerNum(usersObj.getCalls()[k].getPhone());
                                } else if (("" + usersObj.getCalls()[k].getRole_id()).equalsIgnoreCase(WSConstants.DSE_ROLE_ID)) {
                                    pendingStatus.setUserPhone(usersObj.getCalls()[k].getPhone());
                                }
                            }
                        }

                        realm.copyToRealm(pendingStatus);

                    }


                    AnalyticsDB analyticsDB = new AnalyticsDB();
                    analyticsDB.setLocId(resultObj.getLocation_id());
                    analyticsDB.setTodayCount(resultObj.getStats().getToday_count());
                    analyticsDB.setLastWkCount(resultObj.getStats().getLast_week_count());

                    RealmList<WeeklyData> weeklyDataList = new RealmList<WeeklyData>();
                    for (int x = 0; x < resultObj.getStats().getWeekly_data().length; x++) {
                        PendingStatsResponse.Weekly_data weeklyObj = resultObj.getStats().getWeekly_data()[x];
                        WeeklyData weeklyDataDB = new WeeklyData();
                        weeklyDataDB.setAvgPending(weeklyObj.getAvg_pending());
                        weeklyDataDB.setWeekNo(weeklyObj.getWeek_number());
                        weeklyDataList.add(weeklyDataDB);
                    }
                    analyticsDB.setWeeklyData(weeklyDataList);
                    realm.copyToRealm(analyticsDB);

                }

                    adapter = new PendingTabAdapter(getChildFragmentManager());
                    viewPager.setAdapter(adapter);
                    viewPager.setOffscreenPageLimit(5);
                    tabLayout.setupWithViewPager(viewPager);
                    tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            //do stuff here
                            viewPager.setCurrentItem(tab.getPosition());

                            // viewPager.getAdapter().notifyDataSetChanged();
                            System.out.println("TAB SELECTED");
                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {
                            System.out.println("TAB SELECTED UN");
                            //adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {
                            System.out.println("TAB SELECTED RE");
                        }
                    });
                adapter.updateView();
                if (rel_loading_frame != null) {
                    rel_loading_frame.setVisibility(View.GONE);

                    //displayData();

                }
                setSwipeRefreshView(false);
               // WSConstants.Locations.clear();
                if(WSConstants.Locations==null || WSConstants.Locations.size()==0){
                    blank_msg_tv.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                    tabLayout.setVisibility(View.GONE);
                    viewPager.setVisibility(View.GONE);
                }else{
                    blank_msg_tv.setVisibility(View.GONE);
                    swipeRefreshLayout.setVisibility(View.GONE);
                    tabLayout.setVisibility(View.VISIBLE);
                    viewPager.setVisibility(View.VISIBLE);
                }



            }
        });

    }

    private void switchTabOnNotification(){
        if(getActivity()!=null
                && getActivity().getIntent()!=null
                && getActivity().getIntent().getExtras()!=null
                && getActivity().getIntent().getExtras().getBoolean(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY)
                && getActivity().getIntent().getExtras().getString(WSConstants.NOTIFICATION_CLICKED_NAME,"")
                .equalsIgnoreCase(WSConstants.FirebaseEvent.PENDING_ALERT)) {
            int position = 0;
            System.out.println("IdLLL"+getActivity()
                    .getIntent().getExtras().getInt(WSConstants.NOTIFICATION_LOCATION_ID,-1));
            for(int i=0; i<WSConstants.Locations.size();i++){
                if(WSConstants.Locations.get(i).getId()
                        == getActivity()
                        .getIntent().getExtras().getInt(WSConstants.NOTIFICATION_LOCATION_ID,-1)) {
                    position = i;
                    break;
                }

            }
            getActivity().getIntent().putExtra(WSConstants.NOTIFICATION_CLICKED_NAME,"");
            viewPager.setCurrentItem(position);

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
