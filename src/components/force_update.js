import React, { Component } from 'react'
import {  Platform, View, Text, Image, TouchableOpacity, YellowBox, ScrollView, FlatList, Button, Linking} from 'react-native'


import styles from '../styles/styles'

import DeviceInfo from 'react-native-device-info';
import RestClient from '../network/rest_client';
import Toast, {DURATION} from 'react-native-easy-toast';
import { StackActions, NavigationActions } from 'react-navigation';

import * as async_storage from '../storage/async_storage';


export default class ForceUpdate extends Component {

  constructor(props) {

    super(props);
    this.state = {loading:false, mobile:''};



    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);

  }
  componentDidMount() {
    console.log("Datatat");


  }
  update() {
     const APP_STORE_LINK = 'itms-apps://itunes.apple.com/us/app/apple-store/id1439435218?mt=8';
    //  const PLAY_STORE_LINK = 'market://details?id=myandroidappid';
    //  Alert.alert(
    //     'Update Available',
    //     'This version of the app is outdated. Please update app from the '+(Platform.OS =='ios' ? 'app store' : 'play store')+'.',
    //     [
    //         {text: 'Update Now', onPress: () => {
    //             if(Platform.OS =='ios'){
    //                 Linking.openURL(APP_STORE_LINK).catch(err => console.error('An error occurred', err));
    //             }
    //             else{
    //                 Linking.openURL(PLAY_STORE_LINK).catch(err => console.error('An error occurred', err));
    //             }
    //         }},
    //     ]
    // );

    Linking.openURL(APP_STORE_LINK).catch(err => console.error('An error occurred', err));
  }

  render() {

    const { navigation } = this.props;

    let release_notes = navigation.getParam('release_notes');
    if(!release_notes) {
      release_notes = ["Bug fixes"];
    }
      

    return <View style= {styles.MainContainerWhite}>






           <View style={{flex:2,height:'100%', justifyContent:'center', alignItems:'center'}}>
           <Image source={require( '../images/ic_log_ninja_crm.png')} style={{marginTop:40,width:227, height:55.5,}}/>
           <Text

               style={{textAlign:'center', fontSize:20,textAlignVertical: 'center', color:'#000',marginTop:20}}>
           We have updated the app!
           </Text>


           <View style={{flex:1,alignItems:'center', justifyContent:'center'}}>

           <Text
               style={{fontSize:16, textAlign:'left', color:'#000',marginTop:20}}>
           Following changes have been made:
           </Text>


           <FlatList
           style={{alignSelf:'stretch'}}
           extraData = {this.state}
           data = {release_notes}
           renderItem={({item,index}) => <Text
                  style={{fontSize:16, textAlign:'left', color:'#000',marginTop:10}}>{(index+1)+'. '+item}
                </Text>}
           keyExtractor={(item, index) => index+''}
           />



           </View>

           </View>



           <TouchableOpacity  style={{width:'100%', marginBottom:20,alignItems: "center",}} onPress ={()=> this.update()}>
            <View style={{height:40, width:240,justifyContent: "center", marginTop:40,backgroundColor:'#007fff', borderRadius:6}}>

            <Text adjustsFontSizeToFit={true}
                 numberOfLines={1} style={{textAlign:'center', fontSize:18,textAlignVertical: 'center', color:'#fff'}}>
            Update
            </Text>



            </View>
            </TouchableOpacity>

          <View style={{flexDirection:'row',justifyContent:'center'}}>
          <Text style= {{color:'#808080'}}>Powered by </Text>
          <Image source={require( '../images/ic_log_autoninja.png')} style={{width:67.5, height:15,}}/>
          </View>


            </View>




  }
}
