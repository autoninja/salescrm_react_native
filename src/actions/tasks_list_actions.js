import { UPDATE_TAB_COUNT, UPDATE_TASKS_LIST_FILTER, UPDATE_TASKS_LIST_FILTER_UPDATE } from './tasks_list_types';


export const updateTasksList = payload => {
    return {
        type: UPDATE_TAB_COUNT,
        payload: payload
    }
}
export const updateTasksListFilter = payload => {
    return {
        type: UPDATE_TASKS_LIST_FILTER,
        payload
    }
}
export const updateTasksListFilterUpdate = payload => {
    return {
        type: UPDATE_TASKS_LIST_FILTER_UPDATE,
        payload
    }
}