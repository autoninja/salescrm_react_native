package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.dataitem.Detail_Email_model;
import com.salescrm.telephony.dataitem.Detail_Mobile_model;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;

import java.util.List;

import io.realm.Realm;

/**
 * Created by akshata on 5/8/16.
 */
public class Detail_Email_Adapter extends ArrayAdapter<Detail_Email_model> {
    Context context;
    EmailAdapterInterface emailbuttonListener;
    SalesCRMRealmTable salesCRMRealmTable;
    Preferences pref;
    Realm realm;

    List<Detail_Email_model> email_items = null;
    ImageButton tvstaremail,tvdeleteemail,tveditemail;
    private boolean isLeadActive;

    public Detail_Email_Adapter(Context context, List<Detail_Email_model> resource, EmailAdapterInterface emailbuttonListener,boolean isLeadActive) {
        super(context, R.layout.c360_email_address_layout,resource);
        this.context = context;
        this.email_items = resource;
        this.emailbuttonListener = emailbuttonListener;
        this.isLeadActive = isLeadActive;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        convertView = inflater.inflate(R.layout.c360_email_address_layout, parent, false);
        pref = Preferences.getInstance();
        realm= Realm.getDefaultInstance();
        pref.load(getContext());
        TextView nameemail = (TextView) convertView.findViewById(R.id.tvemail_profile);
        tvstaremail = (ImageButton) convertView.findViewById(R.id.tvstaremail);
        tveditemail=(ImageButton) convertView.findViewById(R.id.tvedit_email);
        tvdeleteemail=(ImageButton)convertView.findViewById(R.id.tvdelete_email);
        nameemail.setText(email_items.get(position).getEmail());
        System.out.println("EMAIL STATUS"+email_items.get(position).getEmailvalue());
        if (email_items.get(position).getEmailvalue() ==1) {
            tvstaremail.setImageResource(R.drawable.ic_filled_star_img_24);
            tveditemail.setVisibility(View.INVISIBLE);
            tvdeleteemail.setVisibility(View.INVISIBLE);
        } else {
            //name.setText(mobile_items.get(position).getNumber());
            tvstaremail.setImageResource(R.drawable.ic_star_border_black_24dp);
            tveditemail.setVisibility(View.VISIBLE);
            tvstaremail.setVisibility(View.VISIBLE);
            tvdeleteemail.setVisibility(View.VISIBLE);
        }
        if(email_items.size()>1 && email_items.get(position).getEmailvalue() ==0) {
            tvstaremail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (email_items.get(position).getEmailvalue() == 0) {
                        if(isLeadActive) {
                            emailbuttonListener.CallPrimaryEmail(position);
                        }
                        //tvstaremail.setImageResource(R.drawable.ic_filled_star_img_24);
                    } else {
                       // tvstaremail.setImageResource(R.drawable.ic_star_border_black_24dp);
                    }

                }
            });
        }

        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();

        tveditemail.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(isLeadActive) {
                    emailbuttonListener.Calleditemail(position);
                }
            }
        });
        tvdeleteemail.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(isLeadActive) {
                    if (salesCRMRealmTable.customerEmailId != null) {
                        emailbuttonListener.Calldelete_email_api(position);

                    }
                }

            }
        });




    return convertView;
    }
    public void insertEmailData(Detail_Email_model itemclasses)
    {
        System.out.println(getCount());
        add(itemclasses);
    }

    public interface EmailAdapterInterface{
        public void CallPrimaryEmail(int position);
        public void Calleditemail(int position);
        public void Calldelete_email_api(int position);
    }

    @Override
    public int getCount()
    {
        return email_items.size();
    }
}
