package com.salescrm.telephony.db.car;

import io.realm.RealmObject;

/**
 * Created by bannhi on 21/6/17.
 */

public class InvoiceDetailsDB extends RealmObject{

        String invoice_number;
        int invoiceId;
        String invoice_date;
        String invoice_amount;
        String invoice_name;
        String invoice_address;
        String invoice_mob_no;
        String vin_no;
        String deliveryNo;
        int deliveryId;

        public String getInvoice_number() {
            return invoice_number;
        }

        public void setInvoice_number(String invoice_number) {
            this.invoice_number = invoice_number;
        }

        public String getInvoice_date() {
            return invoice_date;
        }

        public void setInvoice_date(String invoice_date) {
            this.invoice_date = invoice_date;
        }

        public String getInvoice_amount() {
            return invoice_amount;
        }

        public void setInvoice_amount(String invoice_amount) {
            this.invoice_amount = invoice_amount;
        }

        public String getInvoice_name() {
            return invoice_name;
        }

        public void setInvoice_name(String invoice_name) {
            this.invoice_name = invoice_name;
        }

        public String getInvoice_address() {
            return invoice_address;
        }

        public void setInvoice_address(String invoice_address) {
            this.invoice_address = invoice_address;
        }

        public String getInvoice_mob_no() {
            return invoice_mob_no;
        }

        public void setInvoice_mob_no(String invoice_mob_no) {
            this.invoice_mob_no = invoice_mob_no;
        }

        public String getVin_no() {
            return vin_no;
        }

        public void setVin_no(String vin_no) {
            this.vin_no = vin_no;
        }

        public int getInvoiceId() {
            return invoiceId;
        }

        public void setInvoiceId(int invoiceId) {
            this.invoiceId = invoiceId;
        }

        public String getDeliveryNo() {
            return deliveryNo;
        }

        public void setDeliveryNo(String deliveryNo) {
            this.deliveryNo = deliveryNo;
        }

        public int getDeliveryId() {
            return deliveryId;
        }

        public void setDeliveryId(int deliveryId) {
            this.deliveryId = deliveryId;
        }
}
