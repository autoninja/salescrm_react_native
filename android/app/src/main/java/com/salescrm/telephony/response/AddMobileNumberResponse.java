package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by akshata on 11/7/16.
 */
public class AddMobileNumberResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    AddMobileNumberResponse addMobileNumberResponse;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public AddMobileNumberResponse(AddMobileNumberResponse addMobileNumberResponse) {
        this.addMobileNumberResponse = addMobileNumberResponse;
    }

    public AddMobileNumberResponse getAddMobileNumberResponse() {
        return addMobileNumberResponse;
    }

    public void setAddMobileNumberResponse(AddMobileNumberResponse addMobileNumberResponse) {
        this.addMobileNumberResponse = addMobileNumberResponse;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private String lead_last_updated;

        private MobileData[] mobileData;

        private String lead_id;

        public String getLead_last_updated() {
            return lead_last_updated;
        }

        public void setLead_last_updated(String lead_last_updated) {
            this.lead_last_updated = lead_last_updated;
        }

        public MobileData[] getMobileData() {
            return mobileData;
        }

        public void setMobileData(MobileData[] mobileData) {
            this.mobileData = mobileData;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [lead_last_updated = " + lead_last_updated + ", mobileData = " + mobileData + ", lead_id = " + lead_id + "]";
        }
    }

    public class MobileData {
        private String reliable;

        private String id;

        private String status;

        private String mobile_number_id;

        private String lead_phone_mapping_id;

        private String number;

        private String lead_id;

        public String getReliable() {
            return reliable;
        }

        public void setReliable(String reliable) {
            this.reliable = reliable;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getMobile_number_id() {
            return mobile_number_id;
        }

        public void setMobile_number_id(String mobile_number_id) {
            this.mobile_number_id = mobile_number_id;
        }

        public String getLead_phone_mapping_id() {
            return lead_phone_mapping_id;
        }

        public void setLead_phone_mapping_id(String lead_phone_mapping_id) {
            this.lead_phone_mapping_id = lead_phone_mapping_id;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [reliable = " + reliable + ", id = " + id + ", status = " + status + ", mobile_number_id = " + mobile_number_id + ", lead_phone_mapping_id = " + lead_phone_mapping_id + ", number = " + number + ", lead_id = " + lead_id + "]";
        }
    }

    public class Error {
        private String details;

        private String type;

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "ClassPojo [details = " + details + ", type = " + type + "]";
        }
    }

}

