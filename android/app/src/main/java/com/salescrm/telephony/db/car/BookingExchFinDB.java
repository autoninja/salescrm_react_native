package com.salescrm.telephony.db.car;

import io.realm.RealmObject;

public class BookingExchFinDB extends RealmObject {
    private String finance, exchange;

    public BookingExchFinDB() {
    }

    public BookingExchFinDB(String finance, String exchange) {
        this.finance = finance;
        this.exchange = exchange;
    }

    public String getFinance() {
        return finance;
    }

    public void setFinance(String finance) {
        this.finance = finance;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }
}
