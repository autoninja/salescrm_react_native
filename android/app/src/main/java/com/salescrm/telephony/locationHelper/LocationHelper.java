package com.salescrm.telephony.locationHelper;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.views.AutoDialog;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationHelper {
    private static final String TAG = "Location Helper";
    private static final int LOCATION_HELPER_REQUEST_CODE = 2001;
    private static LocationHelper instance;
    private CallBack callback;
    private Activity activity;
    //Location
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    private Task<LocationSettingsResponse> currentTask = null;
    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                System.out.print("LocationHelper: onLocationResult Latitude:" + location.getLatitude());
                System.out.println("::Longitude:" + location.getLongitude());
                callback.onCallBack(location);
                if(activity==null || activity.isFinishing()) {
                    abort();
                }
                try {
                    if (currentTask != null) {
                        LocationSettingsResponse response = currentTask.getResult(ApiException.class);
                    }
                } catch (ApiException e) {
                    gpsExceptionHandler(e);
                }
                //Do what you want with location
                //like update camera
                // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 16f));
            }

        }
    };

    private LocationHelper(Activity activity, CallBack callBack) {
        this.callback = callBack;
        this.activity = activity;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        buildGoogleApiClient();
        createLocationRequest();
    }

    public static LocationHelper getInstance(Activity activity, CallBack callBack) {
        instance = new LocationHelper(activity, callBack);
        return instance;
    }


    private synchronized void buildGoogleApiClient() {
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
        //LOG
        new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    if (location != null) {
                                        LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
                                        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
                                        System.out.println("LocationHelper: lastKnownLocation Latitude:" + location.getLatitude());
                                        System.out.println("LocationHelper: lastKnownLocation Longitude:" + location.getLongitude());
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        //LOG
                    }
                })
                .addApi(LocationServices.API)
                .build();
    }

    private String getAddress(Location location) {
        String address = "";
        Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            System.out.println("Address:" + addresses.get(0).getPostalCode());
            Log.d("LOCATION", "Location PostalCode:" + addresses.get(0).getPostalCode());
            address = String.format("%s, %s, %s\n%s",
                    addresses.get(0).getSubLocality(),
                    addresses.get(0).getLocality(),
                    addresses.get(0).getAdminArea(),
                    addresses.get(0).getPostalCode());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public LocationHelper listen() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(activity).checkLocationSettings(builder.build());
        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                currentTask = task;
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, null);
                    }
                } catch (ApiException exception) {
                    gpsExceptionHandler(exception);
                }
            }
        });
        return instance;
    }

    private void gpsExceptionHandler(ApiException exception) {
        switch (exception.getStatusCode()) {
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // Location settings are not satisfied. But could be fixed by showing the
                // user a dialog.
                try {
                    // Cast to a resolvable exception.
                    ResolvableApiException resolvable = (ResolvableApiException) exception;
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    resolvable.startResolutionForResult(activity, LOCATION_HELPER_REQUEST_CODE);
                    break;
                } catch (IntentSender.SendIntentException e) {
                    // Ignore the error.
                } catch (ClassCastException e) {
                    // Ignore, should be an impossible error.
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog.

                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOCATION_HELPER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                listen();
            } else {
                //Need location Please give the location access
                new AutoDialog(activity, new AutoDialogClickListener() {
                    @Override
                    public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
                        listen();
                    }

                    @Override
                    public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {

                    }
                }, new AutoDialogModel("Please enable the location to continue", "Try Again", "no_button")).show();

            }
        }
    }

    public void abort() {
        System.out.println("Location updated aborted");
        mFusedLocationClient.removeLocationUpdates(locationCallback);
    }

}
