package com.salescrm.telephony.views.booking;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.AutoCompleteTextViewAdapter;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.db.booking_allocation.BookingAllocationResult;
import com.salescrm.telephony.model.Item;
import com.salescrm.telephony.model.booking.BookingCustomerModel;
import com.salescrm.telephony.model.booking.BookingDiscountModel;
import com.salescrm.telephony.model.booking.BookingExchFinModel;
import com.salescrm.telephony.model.booking.BookingMainModel;
import com.salescrm.telephony.model.booking.BookingPriceBreakupModel;
import com.salescrm.telephony.model.booking.BookingSectionModel;
import com.salescrm.telephony.model.booking.BookingVinAllocationModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.presenter.BookingPresenter;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by bharath on 29/12/17.
 */

public class CarBookingForm extends Fragment implements BookingPresenter.BookingView, View.OnFocusChangeListener {
    public static CarBookingForm instance = null;
    private Context context;
    private Drawable imageExpand;
    private Drawable imageCollapse;

    private BookingPresenter bookingPresenter;
    private View rootView;

    private ScrollView scrollViewMain;
    private ImageButton ibClose;

    private EditText etCustomerBookingName, etCustomerAddress, etCustomerPinCode,
            etCustomerCareOf, etCustomerPanNumber, etCustomerAadhaarNumber, etCustomerGST;
    private LinearLayout llCustomerDOB, llCustomerCareOf, llCustomerAadhaarNumber, llCustomerGST;
    private TextView tvCustomerDOB;
    private ImageView imageCustomerDOBStatus;
    private Spinner spinnerCustomerCareOfType, spinnerCustomerType;
    private ImageView imageViewCustomerDetailsDone;

    private ImageView imageViewCarDetailsDone;
    private AutoCompleteTextView autoTvCarModel, autoTvCarVariant, autoTvCarFuelType,
            autoTvCarColor;

    private TextView tvInvoiceDate,tvDeliveryDate;
    private EditText etCarBookingAmount, etVinNumber, etMobileNumber,etDeliveryChallan;
    private ImageView ivInvoiceDateDone,ivDeliveryDateDone;

    private ImageView imageViewPriceBreakUpDone;
    private EditText etPriceExShowroom, etPriceInsurance, etPriceRegistration,
            etPriceAccessories, etPriceExtendedWarranty, etPriceOthers;
    private TextView tvPriceBreakUpValue;

    private ImageView imageViewDiscountDone;
    private EditText etDiscountCorporate, etDiscountExBonus, etDiscountLoyaltyBonus,
            etDiscountOemScheme, etDiscountDealer, etDiscountAccessories;
    private TextView tvDiscountValue;

    private ImageView imageViewExchFinDone;
    private RadioGroup radioGroupFin, radioGroupExch;
    private RadioButton radioButtonFinInHouse, radioButtonFinOutHouse, radioButtonFinFullCash,
            radioButtonExchYes, radioButtonExchNo;

    private FrameLayout frameAction;
    private int colorActionActive;
    private int colorActionInactive;
    private int colorTextInactive;
    private LinearLayout llCarInvoiceDetails,llCarDeliveryDetails;
    private static boolean isFromBookingDetails;
    private BookingMainModel bookingMainModel;
    private CarBookingActionListener carBookingActionListener;
    private Realm realm;
    private TextView tvBookingCarDetailHeader;
    private FrameLayout frameLayoutAllocationStage;
    private Spinner spinnerAllocationStage;
    private List<String> allocationStageName;
    private List<String> allocationStatusId;
    private BookingAllocationResult bookingAllocationResult;
    private int SELECTED_INDEX = 0;
    private String currentAllocationId;
    private EditText edtVinNumber;
    private Preferences pref;

    private FrameLayout frameCarDetailsWrapper, frameDeliveryDetailsWrapper,
            frameCustomerDetailsWrapper, framePriceBreakupWrapper, frameDiscountsWrapper;

    public void setCarBookingActionListener(CarBookingActionListener carBookingActionListener) {
        this.carBookingActionListener = carBookingActionListener;

    }

    public void setBookingMainModel(BookingMainModel bookingMainModel) {
        this.bookingMainModel = bookingMainModel;

    }
    public void copyFromBookingOne(BookingMainModel bookingMainModel) {
        this.bookingMainModel = bookingMainModel;
        bookingPresenter.copyFromBookingOne(bookingMainModel);
        Toast.makeText(getContext(), "Copied from Booking 1",Toast.LENGTH_SHORT).show();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        System.out.println("onCreate Called of Fragment");
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        bookingPresenter = new BookingPresenter(this, bookingMainModel);
        context = getActivity();
        rootView = inflater.inflate(R.layout.booking_car_form, container, false);
        colorActionActive = Color.parseColor("#FF12A7FA");
        colorActionInactive = Color.parseColor("#BEFFFFFF");
        colorTextInactive = Color.parseColor("#9D9FA2");
        imageExpand = VectorDrawableCompat.create(context.getResources(),
                R.drawable.ic_expand_less_24dp, context.getTheme());
        imageCollapse = VectorDrawableCompat.create(context.getResources(),
                R.drawable.ic_expand_more_24dp, context.getTheme());
        scrollViewMain = (ScrollView) rootView.findViewById(R.id.scrollview_booking);
        frameAction = (FrameLayout) rootView.findViewById(R.id.frame_booking_action_bg);

        RelativeLayout frameCarDetails = (RelativeLayout)
                rootView.findViewById(R.id.frame_booking_car_details);
        RelativeLayout frameCustomerDetails = (RelativeLayout)
                rootView.findViewById(R.id.frame_booking_customer_details);
        RelativeLayout framePriceBreakUp = (RelativeLayout)
                rootView.findViewById(R.id.frame_booking_price_breakup);
        RelativeLayout frameDiscounts = (RelativeLayout)
                rootView.findViewById(R.id.frame_booking_discounts);
        RelativeLayout frameExchFin = (RelativeLayout)
                rootView.findViewById(R.id.frame_booking_exch_fin);
        frameLayoutAllocationStage = (FrameLayout) rootView.findViewById(R.id.frame_layout_allocation_stages);
        spinnerAllocationStage = (Spinner) rootView.findViewById(R.id.allocation_stage_spinner);
        edtVinNumber = (EditText) rootView.findViewById(R.id.et_vin_number);
        

        /*Car details ids.*/
        autoTvCarModel = (AutoCompleteTextView) rootView.findViewById(R.id.auto_tv_booking_car_model);
        autoTvCarVariant = (AutoCompleteTextView) rootView.findViewById(R.id.auto_tv_booking_car_variant);
        autoTvCarFuelType = (AutoCompleteTextView) rootView.findViewById(R.id.auto_tv_booking_car_fuel_type);
        autoTvCarColor = (AutoCompleteTextView) rootView.findViewById(R.id.auto_tv_booking_car_color);
        tvInvoiceDate = (TextView) rootView.findViewById(R.id.tv_booking_car_invoice_date);
        ivInvoiceDateDone = (ImageView) rootView.findViewById(R.id.iv_booking_car_invoice_date_status);
        tvDeliveryDate = (TextView) rootView.findViewById(R.id.tv_booking_car_delivery_date);
        ivDeliveryDateDone = (ImageView) rootView.findViewById(R.id.iv_booking_car_delivery_date_status);
        etDeliveryChallan = (EditText) rootView.findViewById(R.id.et_booking_car_delivery_challan);
        etVinNumber = (EditText) rootView.findViewById(R.id.et_booking_car_invoice_vin_no);
        etMobileNumber = (EditText) rootView.findViewById(R.id.et_booking_car_invoice_mobile_no);
        etDeliveryChallan = (EditText) rootView.findViewById(R.id.et_booking_car_delivery_challan);
        etCarBookingAmount = (EditText) rootView.findViewById(R.id.et_booking_car_booking_amount);
        llCarInvoiceDetails = (LinearLayout) rootView.findViewById(R.id.ll_booking_car_invoice);
        llCarDeliveryDetails = (LinearLayout) rootView.findViewById(R.id.ll_booking_car_delivery);
        imageViewCarDetailsDone = (ImageView) rootView.findViewById(R.id.iv_booking_car_details_done);
        frameCarDetailsWrapper = (FrameLayout) rootView.findViewById(R.id.frame_booking_car_details_wrapper);
        frameDeliveryDetailsWrapper = (FrameLayout) rootView.findViewById(R.id.frame_booking_car_delivery_wrapper);

        /*Customer details IDS*/
        etCustomerBookingName = (EditText) rootView.findViewById(R.id.et_booking_customer_name);
        tvCustomerDOB = (TextView) rootView.findViewById(R.id.tv_booking_customer_dob);
        llCustomerDOB = rootView.findViewById(R.id.ll_booking_customer_dob);
        imageCustomerDOBStatus = (ImageView) rootView.findViewById(R.id.iv_booking_customer_dob_status);
        etCustomerAddress = (EditText) rootView.findViewById(R.id.et_booking_customer_address);
        etCustomerPinCode = (EditText) rootView.findViewById(R.id.et_booking_customer_pin_code);
        spinnerCustomerCareOfType = (Spinner) rootView.findViewById(R.id.spinner_customer_adr_type_of);
        etCustomerCareOf = (EditText) rootView.findViewById(R.id.et_booking_customer_address_type_value);
        llCustomerCareOf = rootView.findViewById(R.id.ll_booking_customer_care_of);
        spinnerCustomerType = (Spinner) rootView.findViewById(R.id.spinner_booking_customer_type);
        etCustomerPanNumber = (EditText) rootView.findViewById(R.id.et_booking_customer_pan);
        etCustomerAadhaarNumber = (EditText) rootView.findViewById(R.id.et_booking_customer_aadhaar);
        llCustomerAadhaarNumber = (LinearLayout) rootView.findViewById(R.id.ll_booking_customer_aadhaar);
        etCustomerGST = (EditText) rootView.findViewById(R.id.et_booking_customer_gst);
        llCustomerGST = (LinearLayout) rootView.findViewById(R.id.ll_booking_customer_gst);
        imageViewCustomerDetailsDone = (ImageView) rootView.findViewById(R.id.iv_booking_customer_details_done);
        frameCustomerDetailsWrapper = (FrameLayout) rootView.findViewById(R.id.frame_booking_customer_details_wrapper);

        /*Price details IDS*/
        tvPriceBreakUpValue = (TextView) rootView.findViewById(R.id.tv_car_booking_price_breakup_value);
        imageViewPriceBreakUpDone = (ImageView) rootView.findViewById(R.id.iv_booking_price_break_up_done);
        etPriceExShowroom = (EditText) rootView.findViewById(R.id.et_booking_price_ex_showroom);
        etPriceInsurance = (EditText) rootView.findViewById(R.id.et_booking_price_insurance);
        etPriceRegistration = (EditText) rootView.findViewById(R.id.et_booking_price_registration);
        etPriceAccessories = (EditText) rootView.findViewById(R.id.et_booking_price_accessories);
        etPriceExtendedWarranty = (EditText) rootView.findViewById(R.id.et_booking_price_extended_warranty);
        etPriceOthers = (EditText) rootView.findViewById(R.id.et_booking_price_others);
        framePriceBreakupWrapper = (FrameLayout) rootView.findViewById(R.id.frame_booking_price_breakup_wrapper);


        tvDiscountValue = (TextView) rootView.findViewById(R.id.tv_car_booking_discount_value);
        imageViewDiscountDone = (ImageView) rootView.findViewById(R.id.iv_booking_discount_done);
        etDiscountCorporate = (EditText) rootView.findViewById(R.id.et_booking_discount_corporate);
        etDiscountExBonus = (EditText) rootView.findViewById(R.id.et_booking_discount_ex_bonus);
        etDiscountLoyaltyBonus = (EditText) rootView.findViewById(R.id.et_booking_discount_loyalty_bonus);
        ;
        etDiscountOemScheme = (EditText) rootView.findViewById(R.id.et_booking_discount_oem_scheme);
        ;
        etDiscountDealer = (EditText) rootView.findViewById(R.id.et_booking_discount_dealer);
        etDiscountAccessories = (EditText) rootView.findViewById(R.id.et_booking_discount_accessories);;
        frameDiscountsWrapper = (FrameLayout) rootView.findViewById(R.id.frame_booking_discounts_wrapper);


        /* Exchange-Finance Ids*/
        imageViewExchFinDone = (ImageView) rootView.findViewById(R.id.iv_booking_exch_fin_done);
        radioGroupFin = (RadioGroup) rootView.findViewById(R.id.radio_group_finance);
        radioGroupExch = (RadioGroup) rootView.findViewById(R.id.radio_group_exchange);
        radioButtonFinInHouse = (RadioButton) rootView.findViewById(R.id.radio_inhouse_finance);
        radioButtonFinOutHouse = (RadioButton) rootView.findViewById(R.id.radio_outhouse_finance);
        radioButtonFinFullCash = (RadioButton) rootView.findViewById(R.id.radio_no_finance);
        radioButtonExchYes = (RadioButton) rootView.findViewById(R.id.radio_exchange_yes);
        radioButtonExchNo = (RadioButton) rootView.findViewById(R.id.radio_exchange_no);
        radioGroupFin.clearCheck();
        radioGroupExch.clearCheck();

        radioGroupFin.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                RadioButton rb = (RadioButton) radioGroup.findViewById(checkedId);
                if (null != rb && !(checkedId == -1) ) {
                    switch (checkedId) {
                        case R.id.radio_inhouse_finance:
                            bookingPresenter.updateFinance("In House Loan");
                            return;
                        case R.id.radio_outhouse_finance:
                            bookingPresenter.updateFinance("Out House Loan");
                            return;
                        case R.id.radio_no_finance:
                            bookingPresenter.updateFinance("Full Cash");
                            return;
                    }
                }
            }
        });

        System.out.println("bookingMainModel.getBookingCustomerModel().getBuyerType(), " + pref.getBuyerType());
        if(pref.getBuyerType() != null
                && pref.getBuyerType().equalsIgnoreCase("3")){
            radioGroupExch.setVisibility(View.VISIBLE);
            ((TextView) rootView.findViewById(R.id.exchange_title)).setVisibility(View.VISIBLE);
        }else{
            radioGroupExch.setVisibility(View.GONE);
            bookingPresenter.updateExchange("0");
            ((TextView) rootView.findViewById(R.id.exchange_title)).setVisibility(View.GONE);
        }

        radioGroupExch.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                RadioButton rb = (RadioButton) radioGroup.findViewById(checkedId);
                if (null != rb && !(checkedId == -1) ) {
                    switch (checkedId) {
                        case R.id.radio_exchange_yes:
                            bookingPresenter.updateExchange("1");
                            return;
                        case R.id.radio_exchange_no:
                            bookingPresenter.updateExchange("0");
                            return;
                    }
                }
            }
        });

        tvBookingCarDetailHeader = (TextView) rootView.findViewById(R.id.tv_booking_car_details_header);
        currentAllocationId = bookingMainModel.getVin_allocation_status_id();
        bookingAllocationResult = realm.where(BookingAllocationResult.class).findFirst();
        System.out.println("bookingggggg "+bookingMainModel.getStageId() +", "+ bookingMainModel.getVin_allocation_status_id());
        if(bookingAllocationResult!= null && bookingMainModel.getVin_allocation_status_id()!= null
                && (bookingMainModel.getStageId() == WSConstants.CarBookingStages.CUSTOM_CAR_BOOKED_EDIT
                || bookingMainModel.getStageId() == WSConstants.CarBookingStages.BOOK_CAR
                || bookingMainModel.getStageId() == WSConstants.CarBookingStages.CAR_BOOKED_HALF_PAYMENT
                || bookingMainModel.getStageId() == WSConstants.CarBookingStages.CAR_BOOKED_FULL_PAYMENT_NO)) {
            allocationStageName = new ArrayList<>();
            allocationStatusId = new ArrayList<>();
            allocationStageName.clear();
            allocationStatusId.clear();

            if(isUserHasBillingExecutive()) {
                for (int i = 0; i < bookingAllocationResult.getVinAlllocationStatuses().size(); i++) {
                    //roleExist(bookingAllocationResult.getVinAlllocationStatuses().get(i).getVinAllocationRoleIds());
                    if(roleExist(bookingAllocationResult.getVinAlllocationStatuses().get(i).getVinAllocationRoleIds())) {
                        if (bookingMainModel.getVin_allocation_status_id().equalsIgnoreCase("1")) {
                            allocationStageName.add(bookingAllocationResult.getVinAlllocationStatuses().get(i).getName());
                            allocationStatusId.add(bookingAllocationResult.getVinAlllocationStatuses().get(i).getId());
                        } else {
                            if (!bookingAllocationResult.getVinAlllocationStatuses().get(i).getId().equalsIgnoreCase("1")) {
                                allocationStageName.add(bookingAllocationResult.getVinAlllocationStatuses().get(i).getName());
                                allocationStatusId.add(bookingAllocationResult.getVinAlllocationStatuses().get(i).getId());
                            }
                        }
                    }
                }
            //}else if(userMaxRole() >= Integer.parseInt(WSConstants.TEAM_LEAD_ROLE_ID) && currentAllocationId.equalsIgnoreCase("3")){
            }else if(doesRoleExist() && currentAllocationId.equalsIgnoreCase("3")){
                /*for (int i = 0; i < bookingAllocationResult.getVinAlllocationStatuses().size(); i++) {
                    roleExist(bookingAllocationResult.getVinAlllocationStatuses().get(i).getVinAllocationRoleIds());
                    if(bookingAllocationResult.getVinAlllocationStatuses().get(i).getId().equalsIgnoreCase("2") ||
                            bookingAllocationResult.getVinAlllocationStatuses().get(i).getId().equalsIgnoreCase("3")){
                        allocationStageName.add(bookingAllocationResult.getVinAlllocationStatuses().get(i).getName());
                        allocationStatusId.add(bookingAllocationResult.getVinAlllocationStatuses().get(i).getId());
                    }
                }*/
                frameLayoutAllocationStage.setVisibility(View.VISIBLE);
            }else {
                frameLayoutAllocationStage.setVisibility(View.GONE);
            }
            ArrayAdapter<String> arrayAdapter =  new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, allocationStageName);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerAllocationStage.setAdapter(arrayAdapter);
            spinnerAllocationStage.setSelection(getSelectedIndex());
        }else{
            frameLayoutAllocationStage.setVisibility(View.GONE);
        }

        spinnerAllocationStage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("userMaxRole(): "+allocationStatusId.get(position)+", "+allocationStageName.get(position));
                final int pos= position;

                if(isUserHasBillingExecutive() && allocationStatusId.get(position).equalsIgnoreCase("3")){

                    edtVinNumber.setVisibility(View.VISIBLE);
                    System.out.println("bookingMainModel.getVin_no()  "+ bookingMainModel.getVin_no() +", "+bookingMainModel.getBookingVinAllocationModel().getVin_no());
                    edtVinNumber.setText(bookingMainModel.getBookingVinAllocationModel().getVin_no());
                    bookingPresenter.updateDeallocation(allocationStatusId.get(position), edtVinNumber.getText().toString().trim(), true);
                }else{
                    edtVinNumber.setVisibility(View.GONE);
                    bookingPresenter.updateDeallocation(allocationStatusId.get(position), null, false);
                }

                //Util.showToast(getContext(), allocationStageName.get(position), Toast.LENGTH_SHORT);
                edtVinNumber.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        bookingPresenter.updateDeallocation(allocationStatusId.get(pos), edtVinNumber.getText().toString().trim(), true);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        /*Car details view update listeners*/
        autoTvCarModel.setThreshold(1);
        autoTvCarVariant.setThreshold(0);

        autoTvCarModel.setOnFocusChangeListener(this);
        autoTvCarModel.addTextChangedListener(new CustomTextWatcher(autoTvCarModel));
        autoTvCarModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoTvCarModel.showDropDown();
            }
        });
        autoTvCarModel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() != null) {
                    System.out.println(((Item) view.getTag()).getText());
                    bookingPresenter.updateCarModelSelected(((Item) view.getTag()));
                }

            }
        });


        autoTvCarVariant.setOnFocusChangeListener(this);
        autoTvCarVariant.addTextChangedListener(new CustomTextWatcher(autoTvCarVariant));
        autoTvCarVariant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoTvCarVariant.showDropDown();
            }
        });
        autoTvCarVariant.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() != null) {
                    System.out.println(((Item) view.getTag()).getText());
                    bookingPresenter.updateCarVariantSelected((Item) view.getTag(), false);
                }
            }
        });



        autoTvCarFuelType.setOnFocusChangeListener(this);
        autoTvCarFuelType.setThreshold(0);
        autoTvCarFuelType.addTextChangedListener(new CustomTextWatcher(autoTvCarFuelType));

        autoTvCarFuelType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Util.isNotNull(autoTvCarFuelType.getText().toString())) {
                    autoTvCarFuelType.showDropDown();
                }
            }
        });
        autoTvCarFuelType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() != null) {
                    System.out.println(((Item) view.getTag()).getText());
                    bookingPresenter.updateCarFuelType((Item) view.getTag());
                }
            }
        });


        autoTvCarColor.setOnFocusChangeListener(this);
        autoTvCarColor.setThreshold(0);
        autoTvCarColor.addTextChangedListener(new CustomTextWatcher(autoTvCarColor));
        autoTvCarColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoTvCarColor.showDropDown();
            }
        });
        autoTvCarColor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (view.getTag() != null) {
                    System.out.println(((Item) view.getTag()).getText());
                    bookingPresenter.updateCarColor((Item) view.getTag());
                }
            }
        });



        etCarBookingAmount.addTextChangedListener(new CustomTextWatcher(etCarBookingAmount));
        etVinNumber.addTextChangedListener(new CustomTextWatcher(etVinNumber));
        etMobileNumber.addTextChangedListener(new CustomTextWatcher(etMobileNumber));
        etDeliveryChallan.addTextChangedListener(new CustomTextWatcher(etDeliveryChallan));


        tvInvoiceDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendarMin = Calendar.getInstance();
                calendarMin.add(Calendar.DAY_OF_MONTH, -2);
                Util.showDatePicker(new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        bookingPresenter.updateCarInvoiceDate(dayOfMonth + "/" + (monthOfYear+1) + "/" + year);
                    }
                }, (Activity) context, v.getId(), null, calendarMin, Calendar.getInstance());

            }
        });
        tvDeliveryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendarMin = Calendar.getInstance();
                calendarMin.add(Calendar.DAY_OF_MONTH, -2);
                Util.showDatePicker(new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        bookingPresenter.updateCarDeliveryDate(dayOfMonth + "/" + (monthOfYear+1) + "/" + year);
                    }
                }, (Activity) context, v.getId(), null, calendarMin, Calendar.getInstance());

            }
        });


        /*Customer details view updates listener*/
        setSpinnerArrayAdapter(spinnerCustomerCareOfType, WSConstants.customerCareOfType);
        setSpinnerArrayAdapter(spinnerCustomerType, WSConstants.customerTypes);

        etCustomerBookingName.addTextChangedListener(new CustomTextWatcher(etCustomerBookingName));
        tvCustomerDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.showDatePicker(new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        bookingPresenter.updateCustomerDOB(dayOfMonth + "/" + (monthOfYear+1) + "/" + year);
                    }
                }, (Activity) context, v.getId(), null, null, Calendar.getInstance());

            }
        });

        etCustomerAddress.addTextChangedListener(new CustomTextWatcher(etCustomerAddress));
        etCustomerPinCode.addTextChangedListener(new CustomTextWatcher(etCustomerPinCode));
        spinnerCustomerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bookingPresenter.setCustomerType(position+1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        etCustomerCareOf.addTextChangedListener(new CustomTextWatcher(etCustomerCareOf));
        spinnerCustomerCareOfType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bookingPresenter.setCareOfType((String) spinnerCustomerCareOfType.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        etCustomerPanNumber.addTextChangedListener(new CustomTextWatcher(etCustomerPanNumber));
        etCustomerAadhaarNumber.addTextChangedListener(new CustomTextWatcher(etCustomerAadhaarNumber));
        etCustomerGST.addTextChangedListener(new CustomTextWatcher(etCustomerGST));



        /*Price breakup view update listeners*/
        etPriceExShowroom.addTextChangedListener(new CustomTextWatcher(etPriceExShowroom));
        etPriceInsurance.addTextChangedListener(new CustomTextWatcher(etPriceInsurance));
        etPriceRegistration.addTextChangedListener(new CustomTextWatcher(etPriceRegistration));
        etPriceAccessories.addTextChangedListener(new CustomTextWatcher(etPriceAccessories));
        etPriceExtendedWarranty.addTextChangedListener(new CustomTextWatcher(etPriceExtendedWarranty));
        etPriceOthers.addTextChangedListener(new CustomTextWatcher(etPriceOthers));


        etDiscountCorporate.addTextChangedListener(new CustomTextWatcher(etDiscountCorporate));
        etDiscountExBonus.addTextChangedListener(new CustomTextWatcher(etDiscountExBonus));
        etDiscountLoyaltyBonus.addTextChangedListener(new CustomTextWatcher(etDiscountLoyaltyBonus));
        etDiscountOemScheme.addTextChangedListener(new CustomTextWatcher(etDiscountOemScheme));
        etDiscountDealer.addTextChangedListener(new CustomTextWatcher(etDiscountDealer));
        etDiscountAccessories.addTextChangedListener(new CustomTextWatcher(etDiscountAccessories));


        frameCarDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookingPresenter.updateSection(R.id.ll_booking_car_details,
                        R.id.ib_booking_car_details);

            }
        });
        frameCustomerDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookingPresenter.updateSection(
                        R.id.ll_booking_customer_details,
                        R.id.ib_booking_customer_details);
            }
        });
        framePriceBreakUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookingPresenter.updateSection(
                        R.id.ll_booking_price_breakup,
                        R.id.ib_booking_price_breakup);
            }
        });
        frameDiscounts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookingPresenter.updateSection(
                        R.id.ll_booking_discounts,
                        R.id.ib_booking_discounts);
            }
        });

        frameExchFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bookingPresenter.updateSection(R.id.ll_booking_exch_fin,
                        R.id.ib_booking_exch_fin);
            }
        });



        bookingPresenter.init();

        return rootView;
    }

    private boolean doesRoleExist() {
        boolean exist =false;
        for (int i = 0; i < bookingAllocationResult.getVinAlllocationStatuses().size(); i++) {
            if(bookingMainModel.getVin_allocation_status_id()!= null
                    && bookingMainModel.getVin_allocation_status_id().equalsIgnoreCase("3")
                    && bookingAllocationResult.getVinAlllocationStatuses().get(i).getId().equalsIgnoreCase("3")){
                allocationStageName.add(bookingAllocationResult.getVinAlllocationStatuses().get(i).getName());
                allocationStatusId.add(bookingAllocationResult.getVinAlllocationStatuses().get(i).getId());
            }
            if(roleExist(bookingAllocationResult.getVinAlllocationStatuses().get(i).getVinAllocationRoleIds())){
                allocationStageName.add(bookingAllocationResult.getVinAlllocationStatuses().get(i).getName());
                allocationStatusId.add(bookingAllocationResult.getVinAlllocationStatuses().get(i).getId());
                exist = true;
            }
        }
        return exist?true:false;
    }

    private boolean roleExist(RealmList<String> vinAllocationRoleIds) {
        /*for(int i=0; i<vinAllocationRoleIds.size(); i++){
            if(vinAllocationRoleIds.get(i).equalsIgnoreCase(userMaxRole()+"")){
                System.out.println("roleExist"+ true);
            }
        }*/
        System.out.println("roleExist  called");
        if(vinAllocationRoleIds.contains(userMaxRole()+"")){
            System.out.println("roleExist"+ true);
            return true;
        }
        return false;
    }

    private int getSelectedIndex() {
        for(int i = 0; i< allocationStatusId.size(); i++) {
            System.out.println("allocationStatusId.get(i) "+allocationStatusId.get(i));
            if (allocationStatusId.get(i).equalsIgnoreCase(bookingMainModel.getVin_allocation_status_id())) {
                SELECTED_INDEX = i;
            }
        }
        return SELECTED_INDEX;
    }


    @Override
    public void init(BookingMainModel bookingMainModel) {


        //EditWrapper
        if(!bookingMainModel.getBookingEditWrapperModel().isCarDetailsEditable()){
            autoTvCarModel.setEnabled(false);
            autoTvCarVariant.setEnabled(false);
            autoTvCarColor.setEnabled(false);
            autoTvCarFuelType.setEnabled(false);
        }
        if(!bookingMainModel.getBookingEditWrapperModel().isBookingAmountEditable()) {
            etCarBookingAmount.setEnabled(false);
        }
        if(!bookingMainModel.getBookingEditWrapperModel().isInvoiceEditable()) {
            etVinNumber.setEnabled(false);
            etMobileNumber.setEnabled(false);
            tvInvoiceDate.setEnabled(false);
            tvInvoiceDate.setTextColor(colorTextInactive);
        }
       // frameCarDetailsWrapper.setVisibility(getVisibility(bookingMainModel.getBookingEditWrapperModel().isCarDetailsEditable()));
        if(!bookingMainModel.getBookingEditWrapperModel().isDeliveryDetailsEditable()) {
            tvDeliveryDate.setEnabled(false);
            tvDeliveryDate.setTextColor(colorTextInactive);
            etDeliveryChallan.setEnabled(false);
        }
        frameDeliveryDetailsWrapper.setVisibility(getVisibility(bookingMainModel.getBookingEditWrapperModel().isDeliveryDetailsEditable()));

        //frameCustomerDetailsWrapper.setVisibility(getVisibility(bookingMainModel.getBookingEditWrapperModel().isCustomerDetailsEditable()));
        if(!bookingMainModel.getBookingEditWrapperModel().isCustomerDetailsEditable()) {
            etCustomerBookingName.setEnabled(false);
            tvCustomerDOB.setEnabled(false);
            tvCustomerDOB.setTextColor(colorTextInactive);
            etCustomerAddress.setEnabled(false);
            etCustomerPinCode.setEnabled(false);
            etCustomerCareOf.setEnabled(false);
            spinnerCustomerCareOfType.setEnabled(false);
            spinnerCustomerType.setEnabled(false);
            etCustomerPanNumber.setEnabled(false);
            etCustomerAadhaarNumber.setEnabled(false);
            etCustomerGST.setEnabled(false);
        }
        //framePriceBreakupWrapper.setVisibility(getVisibility(bookingMainModel.getBookingEditWrapperModel().isPriceBreakupEditable()));
        if(!bookingMainModel.getBookingEditWrapperModel().isPriceBreakupEditable()) {
            etPriceExShowroom.setEnabled(false);
            etPriceInsurance.setEnabled(false);
            etPriceRegistration.setEnabled(false);
            etPriceAccessories.setEnabled(false);
            etPriceExtendedWarranty.setEnabled(false);
            etPriceOthers.setEnabled(false);
        }
        //frameDiscountsWrapper.setVisibility(getVisibility(bookingMainModel.getBookingEditWrapperModel().isDiscountEditable()));
        if(!bookingMainModel.getBookingEditWrapperModel().isDiscountEditable()) {
            etDiscountCorporate.setEnabled(false);
            etDiscountExBonus.setEnabled(false);
            etDiscountLoyaltyBonus.setEnabled(false);
            etDiscountOemScheme.setEnabled(false);
            etDiscountDealer.setEnabled(false);
            etDiscountAccessories.setEnabled(false);
        }

        if(!bookingMainModel.getBookingEditWrapperModel().isExchFinEditable()){
            radioGroupFin.setEnabled(false);
            radioGroupExch.setEnabled(false);
            radioButtonFinInHouse.setEnabled(false);
            radioButtonFinOutHouse.setEnabled(false);
            radioButtonFinFullCash.setEnabled(false);
            radioButtonExchNo.setEnabled(false);
            radioButtonExchYes.setEnabled(false);
        }


        BookingCustomerModel bookingCustomerModel = bookingMainModel.getBookingCustomerModel();
        BookingPriceBreakupModel bookingPriceBreakupModel = bookingMainModel.getBookingPriceBreakupModel();
        BookingDiscountModel bookingDiscountModel = bookingMainModel.getBookingDiscountModel();
        BookingExchFinModel bookingExchFinModel = bookingMainModel.getBookingExchFinModel();
        BookingVinAllocationModel bookingVinAllocationModel = bookingMainModel.getBookingVinAllocationModel();


        tvBookingCarDetailHeader.setText((bookingVinAllocationModel.getVin_allocation_status()!= null)
                ? "Vehicle Details("+bookingVinAllocationModel.getVin_allocation_status()+")" :"Vehicle Details");

        etCarBookingAmount.setText(bookingMainModel.getBookingCarModel().getBookingAmount());
        //edtVinNumber.setText(bookingVinAllocationModel.getVin_no());

        etVinNumber.setText((bookingMainModel.getBookingCarModel().getVinNumber()!= null)
                ? bookingMainModel.getBookingCarModel().getVinNumber()
                :bookingMainModel.getBookingVinAllocationModel().getVin_no());

        etMobileNumber.setText(bookingMainModel.getBookingCarModel().getInvoiceMobileNumber());
        try{
            String[] date = bookingMainModel.getBookingCarModel().getInvoiceDate().split("-");
            tvInvoiceDate.setText(String.format(Locale.getDefault(),"%s/%s/%s", date[2].split(" ")[0], date[1], date[0]));

        }
        catch (Exception e){
            tvInvoiceDate.setText(bookingMainModel.getBookingCarModel().getInvoiceDate());
        }

        //Set Delivery Details
        etDeliveryChallan.setText(bookingMainModel.getBookingCarModel().getDeliveryChallan());

        try{
            String[] date = bookingMainModel.getBookingCarModel().getDeliveryDate().split("-");
            tvDeliveryDate.setText(String.format(Locale.getDefault(),"%s/%s/%s", date[2].split(" ")[0], date[1], date[0]));

        }
        catch (Exception e){
            tvDeliveryDate.setText(bookingMainModel.getBookingCarModel().getDeliveryDate());
        }


        //Setting Customer Details
        etCustomerBookingName.setText(bookingCustomerModel.getBookingName());

        try{
            String[] date = bookingCustomerModel.getDateOfBirth().split("-");
            tvCustomerDOB.setText(String.format(Locale.getDefault(),"%s/%s/%s", date[2].split(" ")[0], date[1], date[0]));

        }
        catch (Exception e){
            e.printStackTrace();
            tvCustomerDOB.setText(bookingCustomerModel.getDateOfBirth());
        }


        etCustomerAddress.setText(bookingCustomerModel.getAddress());
        etCustomerPinCode.setText(bookingCustomerModel.getPinCode());
        spinnerCustomerCareOfType.setSelection(WSConstants.customerCareOfType.indexOf(bookingCustomerModel.getCareOfType()));
        etCustomerCareOf.setText(bookingCustomerModel.getCareOf());

        if(bookingCustomerModel.getCustomerType()>0 && bookingCustomerModel.getCustomerType()<=WSConstants.customerTypes.size()) {
            spinnerCustomerType.setSelection(bookingCustomerModel.getCustomerType() - 1);
        }

        etCustomerPanNumber.setText(bookingCustomerModel.getPanNumber());
        etCustomerAadhaarNumber.setText(bookingCustomerModel.getAadhaar());
        etCustomerGST.setText(bookingCustomerModel.getGst());

        //Setting Price Details
        etPriceExShowroom.setText(bookingPriceBreakupModel.getExShowRoomPrice());
        etPriceInsurance.setText(bookingPriceBreakupModel.getInsurance());
        etPriceRegistration.setText(bookingPriceBreakupModel.getRegistration());
        etPriceAccessories.setText(bookingPriceBreakupModel.getAccessories());
        etPriceAccessories.setText(bookingPriceBreakupModel.getAccessories());
        etPriceExtendedWarranty.setText(bookingPriceBreakupModel.getExtendedWarranty());
        etPriceOthers.setText(bookingPriceBreakupModel.getOthers());


        //Setting Discount
        etDiscountCorporate.setText(bookingDiscountModel.getCorporate());
        etDiscountExBonus.setText(bookingDiscountModel.getExchangeBonus());
        etDiscountLoyaltyBonus.setText(bookingDiscountModel.getLoyaltyBonus());
        etDiscountOemScheme.setText(bookingDiscountModel.getOemScheme());
        etDiscountDealer.setText(bookingDiscountModel.getDealer());
        etDiscountAccessories.setText(bookingDiscountModel.getAccessories());


        if(bookingExchFinModel.getFinance() != null) {
            if (bookingExchFinModel.getFinance().equalsIgnoreCase("In House Loan")) {
                radioButtonFinInHouse.setChecked(true);
            } else if (bookingExchFinModel.getFinance().equalsIgnoreCase("Out House Loan")) {
                radioButtonFinOutHouse.setChecked(true);
            } else if (bookingExchFinModel.getFinance().equalsIgnoreCase("Full Cash")) {
                radioButtonFinFullCash.setChecked(true);
            }
            bookingPresenter.updateFinance(bookingExchFinModel.getFinance());
        }else {
            bookingPresenter.updateFinance(null);
        }
        if(bookingExchFinModel.getExchange() != null) {
            if (bookingExchFinModel.getExchange().equalsIgnoreCase("1")) {
                radioButtonExchYes.setChecked(true);
            } else if (bookingExchFinModel.getExchange().equalsIgnoreCase("0")) {
                radioButtonExchNo.setChecked(true);
            }
            bookingPresenter.updateExchange(bookingExchFinModel.getExchange());
        }else {
            bookingPresenter.updateExchange(null);
        }

        /*if(bookingMainModel.getVin_allocation_status_id()!= null && bookingMainModel.getVin_allocation_status_id().equalsIgnoreCase("3") && isUserHasTeamLead()){
            spinnerAllocationStage.setVisibility(View.VISIBLE);
        }else {
            //ll_deallocate.setVisibility(View.GONE);
            spinnerAllocationStage.setVisibility(View.VISIBLE);
        }*/
    }

    public boolean isUserHasTeamLead () {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.TEAM_LEAD_ROLE_ID)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isUserHasBillingExecutive () {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.BILLING_EXECUTIVE)) {
                    return true;
                }
            }
        }
        return false;
    }

    public int userMaxRole () {
        int role = 0;
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (Integer.parseInt(userRoles.get(i).getId())>role) {
                    role = Integer.parseInt(userRoles.get(i).getId());
                }
            }
        }
        return role;
    }

    private int getVisibility(boolean isEditable) {
        if(isEditable) {
            return View.GONE;
        }
        else {
            return View.VISIBLE;
        }
    }

    @Override
    public void initCarDetails(String carModel, String variant, String fuelType, String color, boolean showDeliveryDetails, boolean hideQuantity, boolean showInvoiceDetails) {
        autoTvCarModel.setText(carModel);
        autoTvCarVariant.setText(variant);
        autoTvCarFuelType.setText(fuelType);
        autoTvCarColor.setText(color);
        if (showDeliveryDetails) {
            llCarDeliveryDetails.setVisibility(View.VISIBLE);
        }
        if(showInvoiceDetails){
            llCarInvoiceDetails.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void initCustomerType(int customerType) {
        if(customerType>0 && customerType<=WSConstants.customerTypes.size()) {
            spinnerCustomerType.setSelection(customerType - 1);
        }
    }

    @Override
    public void updateCarModelAdapter(List<Item> data) {
        autoTvCarModel.setAdapter(new AutoCompleteTextViewAdapter(context, data));
        //bookingPresenter.initCarDetails();
    }


    @Override
    public void updateCarVariantAdapter(List<Item> data, boolean isInit) {
        Util.HideKeyboard(context);
        autoTvCarVariant.setAdapter(new AutoCompleteTextViewAdapter(context, data));
        if(!isInit) {
            autoTvCarVariant.requestFocus();
        }
    }

    @Override
    public void updateCarFuelTypeAdapter(List<Item> data) {
        Util.HideKeyboard(context);
        if (data.size() > 0) {
            autoTvCarFuelType.setText(data.get(0).getText());
        }
        autoTvCarFuelType.setAdapter(new AutoCompleteTextViewAdapter(context, data));
    }

    @Override
    public void updateCarColorTypeAdapter(List<Item> data, boolean isInit) {
        Util.HideKeyboard(context);
        autoTvCarColor.setAdapter(new AutoCompleteTextViewAdapter(context, data));
        if(!isInit) {
            autoTvCarColor.requestFocus();
        }

    }

    @Override
    public void updateCarQuantity(Integer quantity) {
    }

    @Override
    public void updateInvoiceDate(String date) {
        tvInvoiceDate.setText(date);
    }

    @Override
    public void updateDeliveryDate(String date) {
        tvDeliveryDate.setText(date);
    }

    @Override
    public void resetCarDetails(int from) {
        switch (from) {
            case BookingPresenter.CAR_BOOKING_RESET_ALL:
                autoTvCarModel.setText("");
                autoTvCarModel.setAdapter(null);
                autoTvCarVariant.setText("");
                autoTvCarVariant.setAdapter(null);
                autoTvCarFuelType.setText("");
                autoTvCarFuelType.setAdapter(null);
                autoTvCarColor.setText("");
                autoTvCarColor.setAdapter(null);
                break;
            case BookingPresenter.CAR_BOOKING_RESET_AFTER_MODEL:
                autoTvCarVariant.setText("");
                autoTvCarVariant.setAdapter(null);
                autoTvCarFuelType.setText("");
                autoTvCarFuelType.setAdapter(null);
                autoTvCarColor.setText("");
                autoTvCarColor.setAdapter(null);
                autoTvCarFuelType.setError("Required");
                autoTvCarVariant.setError("Required");
                break;
            case BookingPresenter.CAR_BOOKING_RESET_AFTER_VARIANT:
                autoTvCarFuelType.setText("");
                autoTvCarFuelType.setAdapter(null);
                autoTvCarColor.setText("");
                autoTvCarColor.setAdapter(null);
                autoTvCarFuelType.setError("Required");
                autoTvCarVariant.setError("Required");
                break;
            case BookingPresenter.CAR_BOOKING_RESET_AFTER_FUEL_TYPE:
                autoTvCarFuelType.setError("Required");
                break;

        }
    }

    @Override
    public void isCarModelValid(boolean isValid) {
        autoTvCarModel.setError(isValid?null:"Required");
        if(!isValid && !autoTvCarModel.isEnabled()) {
            autoTvCarModel.setEnabled(true);
        }
    }
     @Override
        public void isCarVariantValid(boolean isValid) {
            autoTvCarVariant.setError(isValid?null:"Required");
         if(!isValid && !autoTvCarVariant.isEnabled()) {
             autoTvCarVariant.setEnabled(true);
         }
        }

    @Override
    public void isCarFuelTypeValid(boolean isValid) {
        autoTvCarFuelType.setError(isValid?null:"Required");
        if(!isValid && !autoTvCarFuelType.isEnabled()) {
            autoTvCarFuelType.setEnabled(true);
        }
    }

    @Override
    public void isInvoiceVinNumberValid(boolean isValid) {
        etVinNumber.setError(isValid?null:"Required");
        if(!isValid && !etVinNumber.isEnabled()) {
            etVinNumber.setEnabled(true);
        }
    }

    @Override
    public void isInvoiceMobileNumberValid(boolean isValid) {
        etMobileNumber.setError(isValid?null:"Required");
        if(!isValid && !etMobileNumber.isEnabled()) {
            etMobileNumber.setEnabled(true);
        }
    }

    @Override
    public void isInvoiceDateValid(boolean isValid) {
        ivInvoiceDateDone.setVisibility(isValid?View.GONE : View.VISIBLE);
    }

    @Override
    public void isBookingAmountValid(boolean isValid) {
        etCarBookingAmount.setError(isValid?null:"Invalid booking amount");
        if(!isValid && !etCarBookingAmount.isEnabled()) {
            etCarBookingAmount.setEnabled(true);
        }
    }

    @Override
    public void isDeliveryDateValid(boolean isValid) {
            ivDeliveryDateDone.setVisibility(isValid?View.GONE : View.VISIBLE);
    }

    @Override
    public void isDeliveryChallanValid(boolean isValid) {
            etDeliveryChallan.setError(isValid?null:"Required");
    }

    @Override
    public void isCarDetailsComplete(boolean isComplete, VectorDrawableCompat icon) {
        imageViewCarDetailsDone.setImageDrawable(icon);
    }

    @Override
    public void updatePriceBreakup(String priceBreakUp, boolean isComplete, VectorDrawableCompat icon, boolean priceRequired) {
        tvPriceBreakUpValue.setText(priceBreakUp);

        imageViewPriceBreakUpDone.setImageDrawable(icon);
        imageViewPriceBreakUpDone.setVisibility(priceRequired || isComplete ? View.VISIBLE : View.GONE);


    }

    @Override
    public void isExchFinValid(boolean isValid, VectorDrawableCompat icon, boolean isRequired) {
        imageViewExchFinDone.setImageDrawable(icon);
        imageViewExchFinDone.setVisibility(isRequired || isValid ? View.VISIBLE : View.GONE);
    }

    @Override
    public void isExShowRoomPriceValid(boolean isValid) {
        if(!isValid && !etPriceExShowroom.isEnabled()) {
            etPriceExShowroom.setEnabled(true);
        }
        etPriceExShowroom.setError(isValid?null:"Required");
        if(!isValid && !etPriceExShowroom.isEnabled()) {
            etPriceExShowroom.setEnabled(true);
        }
    }


    @Override
    public void updateSectionVisibility(BookingSectionModel.BookingSectionInnerModel bookingSectionModel) {
        rootView.findViewById(bookingSectionModel.getSectionId()).setVisibility(bookingSectionModel.isVisibility() ? View.VISIBLE : View.GONE);
        ((ImageButton) rootView.findViewById(bookingSectionModel.getSectionImageId()))
                .setImageDrawable(bookingSectionModel.isVisibility() ? imageExpand : imageCollapse);
//        //TranslateAnimation translateAnimation = new TranslateAnimation(0,0,0,10);
//        View view = ((View)(rootView.findViewById(bookingSectionModel.getSectionId()).getParent().getParent()));
//        view.animate().translationY(-(view.getTop()-appBarHeight));

    }

    @Override
    public void updateDateOfBirth(String dob) {
        tvCustomerDOB.setText(dob);
    }

    @Override
    public void isBookingNameValid(boolean isValid) {
        etCustomerBookingName.setError(isValid ? null : "Booking name required");
        if(!isValid && !etCustomerBookingName.isEnabled()) {
            etCustomerBookingName.setEnabled(true);
        }
    }

    @Override
    public void isDateOfBirthValid(boolean isValid) {
        imageCustomerDOBStatus.setVisibility(isValid?View.GONE :View.VISIBLE);
        if(!isValid && !tvCustomerDOB.isEnabled()) {
            tvCustomerDOB.setEnabled(true);
            tvCustomerDOB.setTextColor(Color.BLACK);
        }
    }

    @Override
    public void isAddressValid(boolean isValid) {
        etCustomerAddress.setError(isValid?null : "Address required");
        if(!isValid && !etCustomerAddress.isEnabled()) {
            etCustomerAddress.setEnabled(true);
        }
    }

    @Override
    public void isPinCodeValid(boolean isValid) {
        etCustomerPinCode.setError(isValid ? null : "Enter a valid 6 digit number");
        if(!isValid && !etCustomerPinCode.isEnabled()) {
            etCustomerPinCode.setEnabled(true);
        }
    }

    @Override
    public void isCareOfValueValid(boolean isValid) {
        etCustomerCareOf.setError(isValid?null:"Care of required");
        if(!isValid && !etCustomerCareOf.isEnabled()) {
            etCustomerCareOf.setEnabled(true);
        }
    }

    @Override
    public void updateOnCustomerTypeChange(boolean gstVisibility, boolean aadhaarVisibility,
                                           boolean dobRequired,
                                           boolean customerCareOfVisibility) {
        llCustomerGST.setVisibility(gstVisibility ? View.VISIBLE : View.GONE);
        llCustomerAadhaarNumber.setVisibility(aadhaarVisibility ? View.VISIBLE : View.GONE);
        llCustomerDOB.setVisibility(dobRequired ? View.VISIBLE : View.GONE);
        llCustomerCareOf.setVisibility(customerCareOfVisibility? View.VISIBLE : View.GONE);
    }

    @Override
    public void isPanValid(boolean isValid) {
        if (isValid) {
            etCustomerPanNumber.setError(null);
        } else {
            etCustomerPanNumber.setError("Enter a 10 digit PAN Number");
        }
        if(!isValid && !etCustomerPanNumber.isEnabled()) {
            etCustomerPanNumber.setEnabled(true);
        }
    }

    @Override
    public void isAadhaarValid(boolean isValid) {
        if (isValid) {
            etCustomerAadhaarNumber.setError(null);
        } else {
            etCustomerAadhaarNumber.setError("Enter a 12 digit Aadhaar number");
        }
        if(!isValid && !etCustomerAadhaarNumber.isEnabled()) {
            etCustomerAadhaarNumber.setEnabled(true);
        }
    }

    @Override
    public void isGstValid(boolean isValid) {
        if (isValid) {
            etCustomerGST.setError(null);
        } else {
            etCustomerGST.setError("Invalid");
        }
        if(!isValid && !etCustomerGST.isEnabled()) {
            etCustomerGST.setEnabled(true);
        }
    }

    @Override
    public void isCustomerDetailsComplete(boolean isComplete, VectorDrawableCompat vectorDrawableCompat) {

      imageViewCustomerDetailsDone.setImageDrawable(vectorDrawableCompat);

    }

    @Override
    public void updateTotalDiscount(String discount, boolean isComplete, VectorDrawableCompat icon, boolean isRequired) {
        tvDiscountValue.setText(discount);
        imageViewDiscountDone.setImageDrawable(icon);
        imageViewDiscountDone.setVisibility(isRequired || isComplete ? View.VISIBLE : View.GONE);

    }

    @Override
    public void isEnableSaveBookingDetails(boolean isAllComplete) {
        carBookingActionListener.onUpdateActionListener(isAllComplete);
    }

    @Override
    public void hideKeyBoard() {
        Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                Util.HideKeyboard(context);
            }
        });

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus && v instanceof AutoCompleteTextView
                && (((AutoCompleteTextView) v).getAdapter() != null)
                && (((AutoCompleteTextView) v).getText().toString().isEmpty())) {
            ((AutoCompleteTextView) v).showDropDown();
        }
    }

    /**
     * string array
     *
     * @param spinner
     * @param data
     */
    public void setSpinnerArrayAdapter(Spinner spinner, String data[]) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_dropdown_item, data);
        spinner.setAdapter(adapter);
    }

    public void setSpinnerArrayAdapter(Spinner spinner, List<String> data) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_dropdown_item, data);
        spinner.setAdapter(adapter);
    }

    public interface CarBookingListener {
        void onCarBookingSubmit(BookingMainModel bookingMainModel);
    }

    private class CustomTextWatcher implements TextWatcher {
        private EditText editText;

        CustomTextWatcher(EditText et) {
            this.editText = et;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            System.out.println("Before text changed");
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (editText.getId()) {
                case R.id.auto_tv_booking_car_model:
                    bookingPresenter.resetCarDetails(BookingPresenter.CAR_BOOKING_RESET_AFTER_MODEL, s.toString());
                    break;
                case R.id.auto_tv_booking_car_variant:
                    bookingPresenter.resetCarDetails(BookingPresenter.CAR_BOOKING_RESET_AFTER_VARIANT, s.toString());
                    break;
                case R.id.auto_tv_booking_car_fuel_type:
                    bookingPresenter.resetCarDetails(BookingPresenter.CAR_BOOKING_RESET_AFTER_FUEL_TYPE, s.toString());
                    break;
                case R.id.auto_tv_booking_car_color:
                    bookingPresenter.resetCarDetails(BookingPresenter.CAR_BOOKING_RESET_AFTER_COLOR, s.toString());
                    break;
                case R.id.et_booking_car_booking_amount:
                    bookingPresenter.updateCarBookingAmount(s.toString().trim());
                    break;
                case R.id.et_booking_car_invoice_vin_no:
                    bookingPresenter.updateCarInvoiceVinNumber(s.toString().trim());
                    break;
                case R.id.et_booking_car_invoice_mobile_no:
                    bookingPresenter.updateCarInvoiceMobileNumber(s.toString().trim());
                    break;
                case R.id.et_booking_car_delivery_challan:
                    bookingPresenter.updateCarDeliveryChallan(s.toString().trim());
                    break;
                case R.id.et_booking_customer_name:
                    bookingPresenter.updateCustomerBookingName(s.toString().trim());
                    break;
                case R.id.et_booking_customer_address:
                    bookingPresenter.updateAddress(s.toString().trim());
                    break;
                case R.id.et_booking_customer_pin_code:
                    bookingPresenter.updatePinCode(s.toString().trim());
                    break;
                case R.id.et_booking_customer_address_type_value:
                    bookingPresenter.setCareOf(s.toString().trim());
                    break;
                case R.id.et_booking_customer_pan:
                    bookingPresenter.setPanNumber(s.toString().trim());
                    break;
                case R.id.et_booking_customer_aadhaar:
                    bookingPresenter.setAadhaar(s.toString().trim());
                    break;
                case R.id.et_booking_customer_gst:
                    bookingPresenter.setGST(s.toString().trim());
                    break;

                case R.id.et_booking_price_ex_showroom:
                    bookingPresenter.updatePriceExShowRoom(s.toString().trim());
                    break;
                case R.id.et_booking_price_insurance:
                    bookingPresenter.updatePriceInsurance(s.toString().trim());
                    break;
                case R.id.et_booking_price_registration:
                    bookingPresenter.updatePriceRegistration(s.toString().trim());
                    break;
                case R.id.et_booking_price_accessories:
                    bookingPresenter.updatePriceAccessories(s.toString().trim());
                    break;
                case R.id.et_booking_price_extended_warranty:
                    bookingPresenter.updatePriceExtendedWarranty(s.toString().trim());
                    break;
                case R.id.et_booking_price_others:
                    bookingPresenter.updatePriceOthers(s.toString().trim());
                    break;

                case R.id.et_booking_discount_corporate:
                    bookingPresenter.updateDiscountCorporate(s.toString().trim());
                    break;
                case R.id.et_booking_discount_ex_bonus:
                    bookingPresenter.updateDiscountExBonus(s.toString().trim());
                    break;
                case R.id.et_booking_discount_loyalty_bonus:
                    bookingPresenter.updateDiscountLoyalty(s.toString().trim());
                    break;
                case R.id.et_booking_discount_oem_scheme:
                    bookingPresenter.updateDiscountOemScheme(s.toString().trim());
                    break;
                case R.id.et_booking_discount_dealer:
                    bookingPresenter.updateDiscountDealer(s.toString().trim());
                    break;
                case R.id.et_booking_discount_accessories:
                    bookingPresenter.updateDiscountAccessories(s.toString().trim());
                    break;

            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            System.out.println("After text changed");
        }
    }

    public interface CarBookingActionListener{
        void onUpdateActionListener(boolean isEnable);
    }


}
