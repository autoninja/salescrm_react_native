package com.salescrm.telephony.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.WindowManager;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.google.gson.Gson;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandModelsDB;
import com.salescrm.telephony.db.car.CarBrandsDB;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.dbOperation.TasksDbOperation;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;


public class RNSearchActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {

    private ReactInstanceManager mReactInstanceManager;
    private ReactRootView mReactRootView;
    private Preferences pref;
    private ProgressDialog progressDialog;
    private Realm realm;
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals("ON_BOOK_BIKE_RNBookBikeActivity")) {
                finish();
            }

        }
    };


    private void disableScreenCapture() {
        if(pref.isScreenshotEnabled()) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        Preferences.load(this);
        disableScreenCapture();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Redirecting to C360 !!!.. Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();
        Bundle initialProps = new Bundle();
        initialProps.putString("token", Preferences.getAccessToken());
        initialProps.putString("module", "SEARCH");
        initialProps.putInt("appUserId", pref.getAppUserId());
        initialProps.putBoolean("uniqueAcrossDealerShip", DbUtils.isAppConfEnabled(WSConstants.AppConfig.UNIQUE_ACROSS_DEALERSHIP));
        initialProps.putBoolean("addLeadEnabled",DbUtils.isAddLeadEnabled());
        initialProps.putBoolean("coldVisitEnabled",DbUtils.isAppConfEnabled(WSConstants.AppConfig.COLD_VISIT_ENABLED));
        initialProps.putBoolean("showLeadCompetitor",DbUtils.isAppConfEnabled(WSConstants.AppConfig.SHOW_LEAD_COMPETITOR));

        System.out.println("Add Lead Enabled:"+DbUtils.isAddLeadEnabled());

        mReactRootView = new ReactRootView(this);
        mReactRootView.startReactApplication(
                mReactInstanceManager,
                "NinjaCRMSales",
                initialProps
        );
        setContentView(mReactRootView);

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }


    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
        try {
            registerReceiver(broadcastReceiver, new IntentFilter("ON_BOOK_BIKE_RNBookBikeActivity"));

        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


}
