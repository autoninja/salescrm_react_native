package com.salescrm.telephony.reactNative;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.salescrm.telephony.activity.AddSearchActivity;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.activity.DetailsOfEtvbr;
import com.salescrm.telephony.activity.RNAddEventsActivity;
import com.salescrm.telephony.activity.RNColdVisitActivity;
import com.salescrm.telephony.activity.RNEventDashboardChildActivity;
import com.salescrm.telephony.activity.RNLostDropAnalysisActivity;
import com.salescrm.telephony.activity.createEnquiry.RNCreateLeadActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.interfaces.EventActionDone;
import com.salescrm.telephony.interfaces.ReactNativeToAndroidConnect;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;

public class ReactNativeToAndroid extends ReactContextBaseJavaModule {
    public ReactNativeToAndroid(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "ReactNativeToAndroid";
    }

    @ReactMethod
    void navigateToC360FromDetailsDashboard(String leadId, String scheduledActivityId) {
        Preferences pref = Preferences.getInstance();
        pref.load(getReactApplicationContext());
        pref.setLeadID(leadId);
        pref.setScheduledActivityId(Util.getInt(scheduledActivityId));

        ReactApplicationContext context = getReactApplicationContext();
        Intent intent = new Intent(context, C360Activity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @ReactMethod
    void navigateToLostDropDetails(String fromId, String startDate, String endDate, String filters, String modelData,
            String color) {

        ReactApplicationContext context = getReactApplicationContext();
        Intent intent = new Intent(context, RNLostDropAnalysisActivity.class);
        intent.putExtra("fromId", fromId);
        intent.putExtra("startDate", startDate);
        intent.putExtra("endDate", endDate);
        intent.putExtra("filters", filters);
        intent.putExtra("modelData", modelData);
        intent.putExtra("color", color);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    @ReactMethod
    void navigateToEventsChild(String info) {
        ReactApplicationContext context = getReactApplicationContext();
        Intent intent = new Intent(context, RNEventDashboardChildActivity.class);
        intent.putExtra("info", info);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @ReactMethod
    void closeRNLostDropAnalysis() {
        Intent intent = new Intent("CLOSE_RNLostDropAnalysisActivity");
        ReactApplicationContext context = getReactApplicationContext();
        context.sendBroadcast(intent);
    }

    @ReactMethod
    void closeRNEventDashboardChildActivity() {
        Intent intent = new Intent("CLOSE_RNEventDashboardChildActivity");
        ReactApplicationContext context = getReactApplicationContext();
        context.sendBroadcast(intent);
    }

    @ReactMethod
    void closeRNAddEventActivity(boolean isEdit) {
        Intent intent = new Intent("CLOSE_RNAddEventActivity");
        ReactApplicationContext context = getReactApplicationContext();
        context.sendBroadcast(intent);
        logCleverTap(isEdit);
        try {
            Preferences pref = Preferences.getInstance();
            pref.load(context);
            pref.setEventDashboardUpdateRequired(true);
        } catch (Exception e) {

        }

        EventActionDone.getInstance().onEventActionDone();
        // context.sendBroadcast( new Intent("CLOSE_RNEventDashboardChildActivity"));
    }

    @ReactMethod
    void openRNEditEvents(String eventId, String isEventActive) {
        ReactApplicationContext context = getReactApplicationContext();
        Intent intent = new Intent(context, RNAddEventsActivity.class);
        intent.putExtra("eventId", eventId);
        intent.putExtra("isEventActive", isEventActive);
        intent.putExtra("isItEditEvent", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    @ReactMethod
    void eventEtvbrDetails(String clickedName, String locationId, String eventId, String title, String startDate) {
        ReactApplicationContext context = getReactApplicationContext();
        Intent intent = new Intent(context, DetailsOfEtvbr.class);
        intent.putExtra("clicked_position", clickedName);
        intent.putExtra("title_name", title);
        intent.putExtra("location_id", locationId);
        intent.putExtra("location_id", locationId);
        intent.putExtra("event_id", eventId);
        intent.putExtra("route_event", true);
        intent.putExtra("date", startDate);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @ReactMethod
    void onEnquiryCreate(String leadId, String scheduledActivityId, String firstName, String lastName, String title,
            String leadTagId) {

        Context context = getReactApplicationContext();
        if (context == null) {
            context = SalesCRMApplication.GetAppContext();
        }
        Intent intent = new Intent("ON_CREATE_LEAD_RNCreateLeadActivity");
        intent.putExtra("leadId", leadId);
        intent.putExtra("scheduledActivityId", scheduledActivityId);
        intent.putExtra("firstName", firstName);
        intent.putExtra("lastName", lastName);
        intent.putExtra("title", title);
        intent.putExtra("leadTagId", leadTagId);
        context.sendBroadcast(intent);

    }
    @ReactMethod
    void onColdVisitAdded() {
        Context context = getReactApplicationContext();
        if (context == null) {
            context = SalesCRMApplication.GetAppContext();
        }
        Intent intent = new Intent("ON_CREATE_COLD_VISIT_RNColdVisitActivity");
        context.sendBroadcast(intent);

    }
    @ReactMethod
    void onBookBikeClose(String leadId, String scheduledActivityIds, String leadLastUpdated) {
        Context context = getReactApplicationContext();
        if (context == null) {
            context = SalesCRMApplication.GetAppContext();
        }

        Intent intent = new Intent("ON_BOOK_BIKE_RNBookBikeActivity");
        intent.putExtra("scheduledActivityIds", scheduledActivityIds);
        System.out.println("scheduledActivityIds:"+scheduledActivityIds);
        intent.putExtra("leadLastUpdated", leadLastUpdated);
        intent.putExtra("leadId", leadId);
        context.sendBroadcast(intent);

    }

    @ReactMethod
    void onAddedExchangeCar(String leadId, String scheduleActivityId) {
        Context context = getReactApplicationContext();
        if (context == null) {
            context = SalesCRMApplication.GetAppContext();
        }
        Intent intent = new Intent("ON_EXCHANGE_CAR_ADDED_RNCreateLeadActivity");
        intent.putExtra("leadId", leadId);
        intent.putExtra("scheduledActivityId", scheduleActivityId);
        context.sendBroadcast(intent);
    }

    @ReactMethod
    void onUpdateExchangeCar(String leadId, String scheduleActivityId) {
        Context context = getReactApplicationContext();
        if (context == null) {
            context = SalesCRMApplication.GetAppContext();
        }
        Intent intent = new Intent("ON_UPDATE_RNUpdateExchangeCarDetailActivity");
        intent.putExtra("leadId", leadId);
        intent.putExtra("scheduled_activity_id", scheduleActivityId);
        context.sendBroadcast(intent);

    }

    @ReactMethod
    void closeUpdateExchangeCar() {
        Context context = getReactApplicationContext();
        if (context == null) {
            context = SalesCRMApplication.GetAppContext();
        }
        Intent intent = new Intent("CLOSE_RNUpdateExchangeCarDetailActivity");
        context.sendBroadcast(intent);

    }

    @ReactMethod
    void startAddSearchActivity() {
        ReactApplicationContext context = getReactApplicationContext();
        Intent intent = new Intent(context, AddSearchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @ReactMethod
    void startColdVisitActivity(String mobile_number) {
        ReactApplicationContext context = getReactApplicationContext();
        Intent intent = new Intent(context, RNColdVisitActivity.class);
        intent.putExtra("MOBILE_NUMBER", mobile_number);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @ReactMethod
    void startCreateLeadActivity(String mobileNumber) {
        ReactApplicationContext context = getReactApplicationContext();
        Intent intent = new Intent(context, RNCreateLeadActivity.class);
        intent.putExtra("mobileNumber", mobileNumber);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @ReactMethod
    void openDrawerMenu() {
        Context context = getReactApplicationContext();
        if (context == null) {
            context = SalesCRMApplication.GetAppContext();
        }
        Intent intent = new Intent("REACT_NATIVE_TO_HOME_ACTIVITY");
        intent.putExtra("FROM", WSConstants.REACT_NATIVE_FUNCTIONS.OPEN_DRAWER);
        context.sendBroadcast(intent);
    }

    @ReactMethod
    void openTaskList(int dseId, String name, String dpUrl) {
        Context context = getReactApplicationContext();
        if (context == null) {
            context = SalesCRMApplication.GetAppContext();
        }
        Intent intent = new Intent("REACT_NATIVE_TO_HOME_ACTIVITY");
        intent.putExtra("FROM", WSConstants.REACT_NATIVE_FUNCTIONS.OPEN_TASK_LIST);
        intent.putExtra("DSE_ID", dseId);
        intent.putExtra("NAME", name);
        intent.putExtra("DP_URL", dpUrl);
        context.sendBroadcast(intent);
    }

    void logCleverTap(boolean isEdit) {
        String eventType = CleverTapConstants.EVENT_DEALER_EVENTS_TYPE_ADD_EVENT;
        if (isEdit) {
            eventType = CleverTapConstants.EVENT_DEALER_EVENTS_TYPE_EDIT_EVENT;
        }
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_DEALER_EVENTS_KEY_TYPE, eventType);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_DEALER_EVENTS, hashMap);
    }

    @ReactMethod
    void componentDidMount(String viewName) {
        ReactNativeToAndroidConnect.getInstance().postRNComponentDidMount(viewName);
    }
    @ReactMethod
    void exit(String view) {
        Context context = getReactApplicationContext();
        if (context == null) {
            context = SalesCRMApplication.GetAppContext();
        }
        Intent intent = new Intent("REACT_NATIVE_EXIT");
        intent.putExtra("VIEW", view);
        context.sendBroadcast(intent);
    }

    @ReactMethod
    void componentWillUnmount(String viewName) {
        ReactNativeToAndroidConnect.getInstance().postRNComponentWillUnMount(viewName);
    }

    @ReactMethod
    void showToast(String toast) {

        Context context = getReactApplicationContext();
        if (context == null) {
            context = SalesCRMApplication.GetAppContext();
        }
        if (Util.isNotNull(toast)) {
            Util.showToast(context, toast, Toast.LENGTH_SHORT);
        }
    }

    @ReactMethod
    void makePhoneCall(String number) {

        Context context = getReactApplicationContext();
        if (context == null) {
            context = SalesCRMApplication.GetAppContext();
        }
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+number));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(callIntent);

        } catch (Exception e) {

        }
    }

}
