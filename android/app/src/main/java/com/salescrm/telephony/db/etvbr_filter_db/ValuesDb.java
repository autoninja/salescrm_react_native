package com.salescrm.telephony.db.etvbr_filter_db;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by prateek on 8/8/18.
 */

public class ValuesDb extends RealmObject {

    private String id;
    private String name;
    private boolean has_children;
    private RealmList<SubcategoriesDb> subcategories = new RealmList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<SubcategoriesDb> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(RealmList<SubcategoriesDb> subcategories) {
        this.subcategories = subcategories;
    }

    public boolean getHas_children() {
        return has_children;
    }

    public void setHas_children(boolean has_children) {
        this.has_children = has_children;
    }
}
