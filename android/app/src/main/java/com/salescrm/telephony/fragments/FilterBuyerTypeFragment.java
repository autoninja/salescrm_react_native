package com.salescrm.telephony.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.FilterActivityTypeCustomAdapter;
import com.salescrm.telephony.dataitem.FilterActivityTypeModel;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.utils.CleverTapUtil;
import com.salescrm.telephony.utils.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ravindra P on 03-06-2016.
 */
public class FilterBuyerTypeFragment extends Fragment {


    private View rootView;
    private ListView listView;
    private int lastSelectedListItem = 0;
    private String[] items;
    private List<FilterActivityTypeModel> modelItems;
    private static final Integer FILTER_BUYER_TYPE_POS = 6;
    FilterSelectionHolder selectionHolder;
    private FilterActivityTypeCustomAdapter adapter;
    private SparseArray<String> selectedValues;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.filter_activity_type_fragment, container, false);

        selectionHolder = FilterSelectionHolder.getInstance();

        listView = (ListView) rootView.findViewById(R.id.lvfilter_activity_type_item_list);


        modelItems = new ArrayList<>();
        showAdapter();
        if (selectedValues == null) {
            selectedValues = new SparseArray<String>();
        }

        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FilterActivityTypeModel model = modelItems.get(position);
                if (model.getValue() == 1) {
                    model.setValue(0);
                    if (selectedValues.get(position) != null) {
                        selectedValues.remove(position);
                        selectionHolder.setMapData(FILTER_BUYER_TYPE_POS, selectedValues);
                        if (selectedValues.size() == 0) {
                            selectionHolder.removeMapData(FILTER_BUYER_TYPE_POS);
                        }
                    }

                } else {
                    model.setValue(1);
                    selectedValues.put(position, model.getName());
                    selectionHolder.setMapData(FILTER_BUYER_TYPE_POS, selectedValues);
                }
                modelItems.set(position, model);
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
                lastSelectedListItem = position;
            }
        });
        return rootView;

    }

    private void showAdapter() {
        modelItems.add(new FilterActivityTypeModel("First Time", 0));
        modelItems.add(new FilterActivityTypeModel("Additional", 0));
        modelItems.add(new FilterActivityTypeModel("Exchange", 0));
        adapter = new FilterActivityTypeCustomAdapter(getActivity(), modelItems);



        selectedValues = selectionHolder.getMapData(FILTER_BUYER_TYPE_POS);
        if (selectedValues != null) {
            for (int i = 0; i < selectedValues.size(); i++) {
                FilterActivityTypeModel model = modelItems.get(selectedValues.keyAt(i));
                if (model != null) {
                    model.setValue(1);
                    modelItems.set(selectedValues.keyAt(i), model);
                }
            }
        }


        listView.setAdapter(adapter);

    }
}
