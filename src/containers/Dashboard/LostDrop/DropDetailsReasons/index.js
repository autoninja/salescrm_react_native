import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native';

import LostDropReasonHeader from '../../../../components/lostDrop/lost_drop_reason_header'
import LostDropReasonItem from '../../../../components/lostDrop/lost_drop_reason_item'
import LostDropRevenueLoss from '../../../../components/lostDrop/lost_drop_revenue_loss'
import RestClient from '../../../../network/rest_client'

import styles from './DropDetailsReasons.style.js'

class DropDetailsReasons extends Component {

  static navigationOptions = ({ navigation }) => {
    let color = '#2e5e86';
    // if(navigation.getParam("color")) {
    //   color = navigation.getParam("color");
    // }
    return {
      headerStyle: {
           backgroundColor: color,
           elevation:0
         },
    };
  };

  constructor(props) {
    super(props)
    this.state = {
      reasonsData : [1, 2, 4],
      isLoading: true,
      result:null,
    }
  }

  componentDidMount() {
    this.getDropReasons();
    this._mounted = true;
  }


  componentWillUnmount () {
    this._mounted = false;
  }

  getDropReasons() {
    var filters = [];
    for(var i=0;i<global.global_lost_drop_filters_selected.length;i++) {
      filters.push(global.global_lost_drop_filters_selected[i]);
    }
    // if(global.fromAndroid && this.props.modelData) {
    //   filters.push({key:'car_model',values:[{id:JSON.parse(this.props.modelData).id}]})
    // }
    // else
    if(this.props.navigation.getParam('modelData')) {
      filters.push({key:'car_model',values:[{id:this.props.navigation.getParam('modelData').id}]})
    }

    new RestClient().getDropReasons({
      start_date:global.global_filter_date.startDateStr,
      end_date:global.global_filter_date.endDateStr,
      filters:encodeURIComponent(JSON.stringify(filters))})
      .then( (data) => {
        if(!this._mounted){
          return;
        }
        console.log(JSON.stringify(data));

        if(!this.state.isApiCalledAtleastOnce) {
          //this.pushCleverTapEvent({'Dashboard Type': eventTeamDashboard.keyType.valuesType.liveEFTeams});
        }
        let reasonsData = [];
        if(data && data.result && data.result.reasons) {
          reasonsData = data.result.reasons;
        }
        this.setState({ isApiCalledAtleastOnce:true, isLoading: false, result: data.result, reasonsData})

      }).catch(error => {
        //console.error(error);
        if(!this._mounted){
          return;
        }
        this.setState({ isLoading: false});
        if (!error.status) {
            //this.refs.toast.show('Connection Error');
        }
      });
  }



  render() {
    let modelData
    // if(global.fromAndroid && this.props.modelData) {
    //   modelData = JSON.parse(this.props.modelData);
    // }
    // else {
      
    // }
    modelData =  this.props.navigation.getParam("modelData");
    let color;
    // if(global.fromAndroid && this.props.color) {
    //     color = this.props.color;
    // }
    // else {
        
    // }
    color = this.props.navigation.getParam("color","#808080");

    let titleBottom = "Total loss in revenue"
    if(modelData) {
      titleBottom +=" from "+modelData.name;
    }
    return (
      <View style= {styles.container}>

        <View style= {[styles.header,{display:(modelData?'flex':'none'), backgroundColor:color}]}>
          <View style= {styles.headerLeft}>

            <View style= {styles.titleContainer}>
              <Text style= {styles.title}> {modelData?modelData.name:null} </Text>
            </View>

            <Text style= {[styles.title, {fontWeight:'500'}]}> {modelData?modelData.abs:null} </Text>
          </View>

        </View>

        {this.state.isLoading && (
            <View style = {{display:(this.state.isLoading?'flex':'none'), flex:1,justifyContent: 'center',
              alignItems: 'center',backgroundColor:'#fff'}}>
                <ActivityIndicator
                animating={this.state.isLoading}
                style={{flex: 1,   alignSelf:'center',}}
                color="#2e5e86"
                size="large"
                hidesWhenStopped={true}

                />
            </View>
          )
        }

        {!this.state.isLoading && this.state.reasonsData.length==0 && (
          <View style={{alignItems:'center', flex:1,justifyContent: 'center',}}>
            <Text style={{alignSelf:'center', fontSize:20}}> No Data </Text>
          </View>
          )
        }

        {!this.state.isLoading && this.state.reasonsData.length>0 && (
          <View style= {styles.listContainer}>
            <LostDropReasonHeader isDrop={true} />
            <FlatList
                data = {this.state.reasonsData}
                renderItem = {({item, index}) => {
                  if(index == this.state.reasonsData.length-1) {
                    return(
                      <View style={{paddingBottom:100}}>
                      <LostDropReasonItem data={item}/>
                      <LostDropRevenueLoss title={titleBottom} loss={this.state.result.loss_in_revenue}/>
                      </View>
                    )
                  }
                  return (
                    <LostDropReasonItem data={item}/>
                  );
                }
                }
                keyExtractor={(item, index) => index+''}
            />
          </View>
          )
        }


      </View>
    )
  }
}

export default DropDetailsReasons;
