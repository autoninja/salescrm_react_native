package com.salescrm.telephony.db.etvbr_filter_db;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by prateek on 8/8/18.
 */

public class EtvbrFilterDb extends RealmObject {
    private RealmList<FiltersDb> filters = new RealmList<>();

    public RealmList<FiltersDb> getFilters() {
        return filters;
    }

    public void setFilters(RealmList<FiltersDb> filters) {
        this.filters = filters;
    }
}
