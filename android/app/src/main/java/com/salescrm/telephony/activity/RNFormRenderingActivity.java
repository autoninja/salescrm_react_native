package com.salescrm.telephony.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.FormObjectDb;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.interfaces.ReactNativeToAndroidConnect;
import com.salescrm.telephony.locationHelper.LocationHelper;
import com.salescrm.telephony.preferences.Preferences;

import io.realm.Realm;
import io.realm.Sort;

public class RNFormRenderingActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {
    private ReactInstanceManager mReactInstanceManager;
    private ReactRootView mReactRootView;
    private Realm realm;
    private Preferences pref;
    private ProgressDialog progressDialog;
    private LocationHelper locationHelper = null;
    private boolean isReactViewRendered;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        Preferences.load(this);
        setContentView(R.layout.rn_activity_form_rendering);

        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();

        mReactRootView = findViewById(R.id.react_root_view_form_render);
        Bundle initialProps = new Bundle();
        initialProps.putString("leadId", pref.getLeadID());
        initialProps.putString("token", pref.getAccessToken());
        initialProps.putBoolean("fromAndroid", true);
        initialProps.putString("module", "FormRenderingActivity");
        initialProps.putString("form_action",getIntent().getExtras().getString("form_action", "Action"));
        initialProps.putString("form_title", getIntent().getExtras().getString("form_title", "Title"));
        initialProps.putString("action_id", getIntent().getExtras().getInt("action_id", -1)+"");
        initialProps.putString("scheduled_activity_id", pref.getScheduledActivityId()+"");
//        initialProps.putString("salesCRMRealmTableCurrent", realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId",
//                pref.getScheduledActivityId()).findFirst()+"");
        initialProps.putString("dseId", getIntent().getExtras().getInt("dseId", pref.getAppUserId())+"");
        initialProps.putString("activity_id", getIntent().getExtras().getInt("activity_id", -1)+"");
        initialProps.putString("lead_last_updated", realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findAllSorted("leadLastUpdated", Sort.DESCENDING).first().getLeadLastUpdated());
        initialProps.putString("HERE_MAP_APP_ID",pref.getHereMapAppId());
        initialProps.putString("HERE_MAP_APP_CODE", pref.getHereMapAppCode());
        initialProps.putBoolean("isClientRoyalEnfield", DbUtils.isBike());
        initialProps.putBoolean("isClientTwoWheeler", pref.isTwoWheelerClient());
        initialProps.putBoolean("isClientILom", pref.isClientILom());
        initialProps.putBoolean("isClientILomBank", pref.isClientILBank());
        initialProps.putBoolean("isUserVerifier", DbUtils.isUserVerifier());
        //initialProps.putString("formObject", ""+realm.where(FormObjectDb.class).equalTo("action_id", getIntent().getExtras().getInt("action_id", -1)).equalTo("scheduled_activity_id", pref.getScheduledActivityId()).findFirst());
        mReactRootView.startReactApplication(
                mReactInstanceManager,
                "NinjaCRMSales",
                initialProps
        );
        initLocation();

        ReactNativeToAndroidConnect.setListener(new ReactNativeToAndroidConnect.ReactNativeToAndroidConnectListener() {
            @Override
            public void onRNComponentDidMount(String view) {
                if(view.equalsIgnoreCase("FORM_RENDERING")) {
                    isReactViewRendered = true;
                }
            }

            @Override
            public void onRNComponentWillUnMount(String view) {
                if(view.equalsIgnoreCase("FORM_RENDERING")) {
                    isReactViewRendered = false;
                }
            }
        });
    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
    }

    private void initLocation() {
        locationHelper = LocationHelper.getInstance(this, new CallBack() {
            @Override
            public void onCallBack() {

            }

            @Override
            public void onCallBack(Object data) {
                if (data instanceof Location) {
                    Location location = (Location) data;
                    new Handler(getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (mReactInstanceManager != null && mReactInstanceManager.getCurrentReactContext() != null && isReactViewRendered) {
                                WritableMap params = Arguments.createMap();
                                params.putString("LATITUDE", String.valueOf(location.getLatitude()));
                                params.putString("LONGITUDE", String.valueOf(location.getLongitude()));
                                sendEvent(mReactInstanceManager.getCurrentReactContext(),
                                        "onGettingLocationFormRendering", params);
                                locationHelper.abort();
                            }
                        }
                    });


                }
            }

            @Override
            public void onError(String error) {

            }
        }).listen();
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (locationHelper != null) {
            locationHelper.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
    }
}
