import React from 'react';
import { createStackNavigator } from 'react-navigation';
import CreateEnquiry from './index'
import ExchangeCar from "./exchange_car";
import BackIcon from '../../../components/uielements/BackIcon';
import UserInfoService from '../../../storage/user_info_service';
import ShowRoomLocationDBService from '../../../storage/showroom_location_db_service';
import VehicleDBService from '../../../storage/vehicle_db_service';
import EnquirySourceDBService from '../../../storage/enquiry_source_db_service'
import AppConfDBService from '../../../storage/app_conf_db_service'
import ActivityDBService from '../../../storage/activity_db_service'
import TeamUserDBService from '../../../storage/team_user_db_service';
import ActiveEventsDBService from '../../../storage/active_events_db_service';
import EvaluationManagersConfDBService from '../../../storage/evaluation_managers_conf_db_service';

const CreateEnquiryRouter = createStackNavigator({
    AddEnquiry: {
        screen: props => (
            <CreateEnquiry
                {...props}
                userLocations={UserInfoService.getLocationsList()}
                allLocations={ShowRoomLocationDBService.getShowRooms()}
                interestedVehicle={VehicleDBService.getInterestedVehicle()}
                allVehicles={VehicleDBService.getAllVehicle()}
                enquirySourceMain={EnquirySourceDBService.getEnquirySourceMain()}
                enquirySourceCategories={EnquirySourceDBService.getEnquirySourceCategories()}
                isClientTwoWheeler={AppConfDBService.isEnabled('two_wheeler_conf')}
                isShowEnquirySourceCategory={AppConfDBService.isEnabled('show_lead_source_categories')}
                activities={ActivityDBService.getActivities()}
                teams={TeamUserDBService.getTeamUser()}
                activeEvents={ActiveEventsDBService.getActiveEvents()}
                govtAPIKeys={global.GOVT_API_KEYS}
                evaluationManagers={EvaluationManagersConfDBService.getEvaluationManagerConf()}
                HERE_MAP_APP_ID={global.HERE_MAP_APP_ID}
                HERE_MAP_APP_CODE={global.HERE_MAP_APP_CODE}
                FIRST_CALL_ADVANCE_DAYS={AppConfDBService.getVal('first_call_advance_days')}
            />
        ),

        navigationOptions: ({ navigation }) => ({
            title: "Add Enquiry",
            headerBackTitle: null,
            headerLeft: <BackIcon navigationProps={navigation} invert={true} />,
            headerStyle: {
                backgroundColor: '#fff',
                elevation: 0,
            },
            headerTransperent: true,
            headerTintColor: '#303030',
            headerTitleStyle: {
                fontWeight: 'bold',
                width: '100%',
                textAlign: 'left',
            },
        })
    },
    ExchangeCar: {
        screen: ExchangeCar,
        navigationOptions: ({ navigation }) => ({
            title: "Add Enquiry"
        })
    }
});

export default CreateEnquiryRouter;
