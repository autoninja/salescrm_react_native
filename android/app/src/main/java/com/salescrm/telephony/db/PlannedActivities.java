package com.salescrm.telephony.db;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 29-08-2016.
 */
public class PlannedActivities extends RealmObject {

    public static final String PLANNEDACTIVITIES = "PlannedActivities";

    @PrimaryKey
    private int plannedActivityScheduleId;
    private String activityId;

    private long leadID;
    private String plannedActivitiesIconType;
    private String plannedActivitiesName;
    private String plannedActivitiesDateTime;
    private boolean isDone = false;

    public RealmList<PlannedActivitiesDetail> getPlannedActivitiesDetail() {
        return plannedActivitiesDetail;
    }

    public void setPlannedActivitiesDetail(RealmList<PlannedActivitiesDetail> plannedActivitiesDetail) {
        this.plannedActivitiesDetail = plannedActivitiesDetail;
    }

    public RealmList<PlannedActivitiesDetail> plannedActivitiesDetail = new RealmList<>();
    public RealmList<PlannedActivitiesAction> plannedActivitiesAction = new RealmList<>();

    public int getPlannedActivityScheduleId() {
        return plannedActivityScheduleId;
    }

    public void setPlannedActivityScheduleId(int plannedActivityScheduleId) {
        this.plannedActivityScheduleId = plannedActivityScheduleId;
    }

    public long getLeadID() {
        return leadID;
    }

    public void setLeadID(long leadID) {
        this.leadID = leadID;
    }

    public String getPlannedActivitiesIconType() {
        return plannedActivitiesIconType;
    }

    public void setPlannedActivitiesIconType(String plannedActivitiesIconType) {
        this.plannedActivitiesIconType = plannedActivitiesIconType;
    }

    public String getPlannedActivitiesName() {
        return plannedActivitiesName;
    }

    public void setPlannedActivitiesName(String plannedActivitiesName) {
        this.plannedActivitiesName = plannedActivitiesName;
    }

    public String getPlannedActivitiesDateTime() {
        return plannedActivitiesDateTime;
    }

    public void setPlannedActivitiesDateTime(String plannedActivitiesDateTime) {
        this.plannedActivitiesDateTime = plannedActivitiesDateTime;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }
}
