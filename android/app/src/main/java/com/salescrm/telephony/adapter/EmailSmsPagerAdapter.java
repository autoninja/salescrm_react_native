package com.salescrm.telephony.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.salescrm.telephony.db.EmailResult;
import com.salescrm.telephony.db.SMSResult;
import com.salescrm.telephony.db.DealerDataDb;
import com.salescrm.telephony.fragments.EmailsFragment;
import com.salescrm.telephony.fragments.SmsFragment;
import com.salescrm.telephony.response.GetAllSmsEmailResponse;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by akshata on 27/5/16.
 */
public class EmailSmsPagerAdapter extends FragmentPagerAdapter {
    int mNumOfTabs;
    boolean isClientILom;
    private GetAllSmsEmailResponse SmsEmailResponse;
    private List<GetAllSmsEmailResponse.Result.Template_data> EmailResponse = new ArrayList<>();
    private List<GetAllSmsEmailResponse.Result.Template_data> SmsResponse = new ArrayList<>();
    private Realm realm;
    private EmailResult emailResult;
    private SMSResult smsResult;
    private RealmResults<EmailResult> emailRealmResults;
    private RealmResults<SMSResult> smsRealmResults;
    private DealerDataDb dealerDataDb;


    public EmailSmsPagerAdapter(FragmentManager fm, int mNumOfTabs, GetAllSmsEmailResponse SmsEmailResponse, boolean isClientILom) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
        this.SmsEmailResponse = SmsEmailResponse;
        realm = Realm.getDefaultInstance();
        this.isClientILom = isClientILom;
    }

    @Override
    public Fragment getItem(int position) {
        if(SmsEmailResponse!=null) {
            realm.beginTransaction();
            realm.delete(SMSResult.class);
            realm.delete(EmailResult.class);
            realm.delete(DealerDataDb.class);
            realm.commitTransaction();
            for (int j = 0; j < SmsEmailResponse.getResult().getTemplate_data().length; j++) {
                int notification_type = Integer.parseInt(SmsEmailResponse.getResult().getTemplate_data()[j].getNotification_type_id());
                if (notification_type == WSConstants.NOTIFICATION_TYPE_SMS) {
                    realm.beginTransaction();
                    smsResult = realm.createObject(SMSResult.class);
                    smsResult.setContent(SmsEmailResponse.getResult().getTemplate_data()[j].getContent());
                    smsResult.setId(SmsEmailResponse.getResult().getTemplate_data()[j].getId());
                    smsResult.setName(SmsEmailResponse.getResult().getTemplate_data()[j].getName());
                    //smsResult.setNotification_cat_id(SmsEmailResponse.getResult().getTemplate_data()[j].getNotification_type_id());
                    smsResult.setNotification_type_id(SmsEmailResponse.getResult().getTemplate_data()[j].getNotification_type_id());
                    smsResult.setRecipient_type(SmsEmailResponse.getResult().getTemplate_data()[j].getRecipient_type());
                    smsResult.setRecipient_type_id(SmsEmailResponse.getResult().getTemplate_data()[j].getRecipient_type_id()+"");
                    realm.commitTransaction();
                    SmsResponse.add(SmsEmailResponse.getResult().getTemplate_data()[j]);
                } else if (notification_type == WSConstants.NOTIFICATION_TYPE_EMAIL) {
                    realm.beginTransaction();
                    emailResult = realm.createObject(EmailResult.class);
                    emailResult.setContent(SmsEmailResponse.getResult().getTemplate_data()[j].getContent());
                    emailResult.setId(SmsEmailResponse.getResult().getTemplate_data()[j].getId());
                    emailResult.setName(SmsEmailResponse.getResult().getTemplate_data()[j].getName());
                    //emailResult.setNotification_cat_id(SmsEmailResponse.getResult().getTemplate_data()[j].getNotification_cat_id());
                    emailResult.setNotification_type_id(SmsEmailResponse.getResult().getTemplate_data()[j].getNotification_type_id());
                    emailResult.setRecipient_type(SmsEmailResponse.getResult().getTemplate_data()[j].getRecipient_type());
                    emailResult.setSubject(SmsEmailResponse.getResult().getTemplate_data()[j].getSubject());
                    emailResult.setRecipient_type_id(SmsEmailResponse.getResult().getTemplate_data()[j].getRecipient_type_id()+"");
                    realm.commitTransaction();
                    EmailResponse.add(SmsEmailResponse.getResult().getTemplate_data()[j]);
                }
            }

            realm.beginTransaction();
            dealerDataDb = realm.createObject(DealerDataDb.class);
            dealerDataDb.setCustSupportEmailId(SmsEmailResponse.getResult().getDealer_data().getCustSupportEmailId());
            dealerDataDb.setCcmEmailId(SmsEmailResponse.getResult().getDealer_data().getCcmEmailId());
            dealerDataDb.setCcmMobNo(SmsEmailResponse.getResult().getDealer_data().getCcmMobNo());
            dealerDataDb.setDealerCity(SmsEmailResponse.getResult().getDealer_data().getDealerCity());
            dealerDataDb.setHotlineNo(SmsEmailResponse.getResult().getDealer_data().getHotlineNo());
            dealerDataDb.setDname(SmsEmailResponse.getResult().getDealer_data().getDname());
            dealerDataDb.setDbrand(SmsEmailResponse.getResult().getDealer_data().getDbrand());
            dealerDataDb.setCrmMobNo(SmsEmailResponse.getResult().getDealer_data().getCrmMobNo());
            dealerDataDb.setTdFeedbackLink(SmsEmailResponse.getResult().getDealer_data().getTdFeedbackLink());
            dealerDataDb.setDealerWebsite(SmsEmailResponse.getResult().getDealer_data().getDealerWebsite());
            dealerDataDb.setDealerAddress(SmsEmailResponse.getResult().getDealer_data().getDealerAddress());
            dealerDataDb.setPocName(SmsEmailResponse.getResult().getDealer_data().getPocName());
            realm.commitTransaction();

            emailRealmResults = realm.where(EmailResult.class).findAll();
            smsRealmResults = realm.where(SMSResult.class).findAll();
            System.out.println("PRT: Email- "+emailRealmResults.size()+"SMS- "+smsRealmResults.size());
            for(int l=0; l<emailRealmResults.size(); l++){
                System.out.println("Hello Bhau:- "+emailRealmResults.get(l).getName());
            }

        }
        if(isClientILom) {
            return SmsFragment.newInstance(smsRealmResults);
        }
        switch (position) {
            case 0:
                return  EmailsFragment.newInstance(emailRealmResults);
            case 1:
                return SmsFragment.newInstance(smsRealmResults);

            default:
                return null;
        }
    }
    @Override
    public int getCount() {

        return mNumOfTabs;
    }
}


