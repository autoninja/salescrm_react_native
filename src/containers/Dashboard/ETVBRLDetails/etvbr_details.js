import React, { Component } from "react";
import {
  Platform,
  View,
  Text,
  Image,
  TouchableOpacity,
  YellowBox,
  ScrollView,
  Switch,
  Alert,
  FlatList,
  ActivityIndicator,
  NativeModules
} from "react-native";

import axios from "axios";
import styles from "../../../styles/styles";
import ETVBRDetailsItem from "../../../components/etvbrlDetails/etvbr_details_item";
import ETVBRDetailsPendingItem from "../../../components/etvbrlDetails/etvbr_details_pending_item";
import RestClient from "../../../network/rest_client";
import Toast, { DURATION } from "react-native-easy-toast";
import CleverTapPush from "../../../clevertap/CleverTapPush";
import NavigationService from '../../../navigation/NavigationService'
var recordings = [];
export default class ETVBRDetails extends Component {
  static navigationOptions = ({ navigation }) => {
    if (navigation) {
      let title = navigation.getParam("data").info.title;
      let startDateData = global.global_filter_date.startDateStr.split("-");
      let endDateData = global.global_filter_date.endDateStr.split("-");

      date =
        ("0" + startDateData[2]).slice(-2) +
        "/" +
        ("0" + startDateData[1]).slice(-2) +
        "-" +
        ("0" + endDateData[2]).slice(-2) +
        "/" +
        ("0" + endDateData[1]).slice(-2);
      let clicked_val = navigation.getParam("data").info.clicked_val;
      if (
        clicked_val == "Pending Bookings" ||
        clicked_val == "Live Enquiries"
      ) {
        date = "";
      }
      return {
        title,
        headerRight: (
          <Text
            style={{
              color: "#fff",
              fontSize: 14,
              paddingRight: 20,
              width: 140,
              textAlign: "right"
            }}
          >
            {date}
          </Text>
        )
      };
    }
  };

  constructor(props) {
    console.log("Mounted");
    //CleverTap.recordEvent('testEvent');
    super(props);
    this.state = { loading: true, etvbr_details: null, status: false, user_role: "" };

    YellowBox.ignoreWarnings([
      "Warning: componentWillMount is deprecated",
      "Warning: componentWillReceiveProps is deprecated"
    ]);
  }
  updateRecordings(index, recording) {
    recordings[index + ""] = recording;
    console.log("Recording Size:" + recordings.length);
  }

  releaseRecordings() {
    for (var i = 0; i < recordings.length; i++) {
      if (recordings) {
        try {
          recordings[i].release();
          //recordings[i] = null;
        } catch (err) { }
      }
    }
  }

  componentWillUnmount() {
    console.log("Unmounted");
    this.releaseRecordings();
  }

  getDetails() {
    if (this.props.navigation.getParam("isExchange")) {
      this.getExchangeDetail();
    } else if (this.props.navigation.getParam("isEvents")) {
      this.getEventETVBRDetails();
    } else {
      this.getETVBRDetails();
    }
  }
  componentDidMount() {
    CleverTapPush.pushEvent("Clicks", {
      Type: this.props.navigation.getParam("data").info.clicked_val
    });

    this.getDetails();
  }

  showAlert(msg) {
    this.refs.toast.show(msg);
  }

  getExchangeDetail() {
    const { navigation } = this.props;
    let data;
    let startDate;
    let endDate;
    let filterSelected;
    data = navigation.getParam("data").info;
    startDate = global.global_filter_date.startDateStr;
    endDate = global.global_filter_date.endDateStr;
    this.setState({ user_role: data.user_role_id })
    new RestClient()
      .getETVBRForUsedCarExchangeDetails({
        start_date: startDate,
        end_date: endDate,
        clicked_val: data.clicked_val,
        user_id: data.user_id ? data.user_id : "",
        user_location_id: data.user_location_id ? data.user_location_id : "",
        user_role_id: data.user_role_id ? data.user_role_id : ""
      })
      .then(data => {
        this.setState({ loading: false, etvbr_details: data.result });
      })
      .catch(error => {
        if (!error.status) {
          this.refs.toast.show("Connection Error");
        }
      });
  }

  getETVBRDetails() {
    const { navigation } = this.props;
    let data;
    let startDate;
    let endDate;
    let filterSelected;
    data = navigation.getParam("data").info;
    this.setState({ user_role: data.user_role_id })
    startDate = global.global_filter_date.startDateStr;
    endDate = global.global_filter_date.endDateStr;
    filterSelected = JSON.stringify(global.global_etvbr_filters_selected);
    if (data.clicked_val == "Uncalled Tasks") {
      startDate = global.global_filter_date.endDateStr;
    }
    new RestClient()
      .getETVBRDetails({
        start_date: startDate,
        end_date: endDate,
        clicked_val: data.clicked_val,
        user_id: data.user_id,
        user_location_id: data.user_location_id,
        user_role_id: data.user_role_id,
        offset: 0,
        limit: 400,
        filters: filterSelected
      })
      .then(data => {
        this.setState({ loading: false, etvbr_details: data.result });
      })
      .catch(error => {
        if (!error.status) {
          this.refs.toast.show("Connection Error");
        }
      });
  }

  getEventETVBRDetails() {
    const { navigation } = this.props;
    let data;
    data = navigation.getParam("data").info;
    this.setState({ user_role: data.user_role_id })
    new RestClient()
      .getEventETVBRDetails({
        start_date: data.start_date,
        clicked_val: data.clicked_val,
        event_id: data.eventId,
        location_id: data.locationId,
        offset: 0,
        limit: 20
      })
      .then(data => {
        console.log("etvbrDetailsss", data.result);
        this.setState({ loading: false, etvbr_details: data.result });
      })
      .catch(error => {
        if (!error.status) {
          console.error(error);
          this.refs.toast.show("Error");
        }
      });
  }

  openC360(fromAndroid, navigation, leadId, scheduledActivityId) {
    if (fromAndroid) {
      this.setState({ status: !this.state.status });
      this.releaseRecordings();
      NativeModules.ReactNativeToAndroid.navigateToC360FromDetailsDashboard(
        leadId + "",
        scheduledActivityId + ""
      );
    } else {
      this.setState({ status: !this.state.status });
      this.releaseRecordings();
      // NavigationService.navigate("C360MainScreen", { leadId: leadId, user_role: this.state.user_role });
      this.props.navigation.navigate("C360MainScreen", { leadId: leadId, user_role: this.state.user_role });
    }
  }

  render() {
    const { navigation } = this.props;
    let data;
    data = navigation.getParam("data").info;

    if (this.state.loading) {
      return (
        <View style={styles.MainContainer}>
          <View
            style={{
              display: this.state.loading ? "flex" : "none",
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <ActivityIndicator
              animating={this.state.loading}
              style={{ flex: 1, alignSelf: "center" }}
              color="#fff"
              size="large"
              hidesWhenStopped={true}
            />
            <Toast
              ref="toast"
              style={{ backgroundColor: "red" }}
              position="top"
              fadeInDuration={750}
              fadeOutDuration={750}
              opacity={0.8}
              textStyle={{ color: "white" }}
            />
          </View>
        </View>
      );
    }

    let dataItems = [];
    var count = 0;
    if (this.state.etvbr_details) {
      dataItems = this.state.etvbr_details.leadDetails;
      count = this.state.etvbr_details.count;
    }
    console.log(dataItems.length);
    console.log("leadDetail::: " + dataItems);
    return (
      <View style={styles.MainContainer}>
        <Toast
          ref="toast"
          style={{ backgroundColor: "#303030" }}
          position="top"
          fadeInDuration={750}
          fadeOutDuration={750}
          opacity={0.9}
          textStyle={{ color: "white" }}
        />

        <Text style={{ color: "#fff", fontSize: 18, textAlign: "center" }}>
          {data.clicked_val}({count})
        </Text>
        <FlatList
          extraData={this.state.status}
          data={dataItems}
          renderItem={({ item, index }) => {
            if (data && data.clicked_val == "Pending Bookings") {
              return (
                <ETVBRDetailsPendingItem
                  showAlert={this.showAlert.bind(this)}
                  updateRecordings={this.updateRecordings.bind(this, index)}
                  openC360={this.openC360.bind(
                    this,
                    global.fromAndroid,
                    this.props.navigation
                  )}
                  leadDetails={item}
                />
              );
            } else {
              return (
                <ETVBRDetailsItem
                  openC360={this.openC360.bind(
                    this,
                    global.fromAndroid,
                    this.props.navigation
                  )}
                  leadDetails={item}
                />
              );
            }
          }}
          keyExtractor={(item, index) => index + ""}
          onRefresh={() => this.getDetails()}
          refreshing={this.state.loading}
        />
      </View>
    );
  }
}
