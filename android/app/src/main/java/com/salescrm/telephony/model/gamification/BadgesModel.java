package com.salescrm.telephony.model.gamification;

/**
 * Created by bannhi on 28/2/18.
 */

public class BadgesModel {
    String id;
    String name;
    String description;
    int singleIconId;
    int multiIconId;


    public BadgesModel(String id,String name, String description,int singleIconId,int multiIconId){
        this.description = description;
        this.name = name;
        this.id = id;
        this.singleIconId = singleIconId;
        this.multiIconId = multiIconId;
    }
    public BadgesModel(String id,int singleIconId,int multiIconId){
        this.description = description;
        this.name = name;
        this.id = id;
        this.singleIconId = singleIconId;
        this.multiIconId = multiIconId;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSingleIconId() {
        return singleIconId;
    }

    public void setSingleIconId(int singleIconId) {
        this.singleIconId = singleIconId;
    }

    public int getMultiIconId() {
        return multiIconId;
    }

    public void setMultiIconId(int multiIconId) {
        this.multiIconId = multiIconId;
    }
}
