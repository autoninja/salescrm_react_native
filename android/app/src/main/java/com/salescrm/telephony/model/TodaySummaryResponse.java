package com.salescrm.telephony.model;

import com.salescrm.telephony.response.Error;

import java.util.List;

/**
 * Created by bharath on 23/11/17.
 */

public class TodaySummaryResponse {

    private String message;

    private List<Result> result;

    private Error error;

    private Integer statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private List<Team_leaders> team_leaders;

        private String location_name;

        private Integer location_id;

        private Abs location_abs;

        public List<Team_leaders> getTeam_leaders() {
            return team_leaders;
        }

        public void setTeam_leaders(List<Team_leaders> team_leaders) {
            this.team_leaders = team_leaders;
        }

        public String getLocation_name() {
            return location_name;
        }

        public void setLocation_name(String location_name) {
            this.location_name = location_name;
        }

        public Integer getLocation_id() {
            return location_id;
        }

        public void setLocation_id(Integer location_id) {
            this.location_id = location_id;
        }

        public Abs getLocation_abs() {
            return location_abs;
        }

        public void setLocation_abs(Abs location_abs) {
            this.location_abs = location_abs;
        }

        @Override
        public String toString() {
            return "ClassPojo [team_leaders = " + team_leaders + ", location_name = " + location_name + ", location_id = " + location_id + "]";
        }

        public class Team_leaders {

            private boolean expand =false;

            private List<Sales_consultants> sales_consultants;

            private Info info;

            public List<Sales_consultants> getSales_consultants() {
                return sales_consultants;
            }

            public void setSales_consultants(List<Sales_consultants> sales_consultants) {
                this.sales_consultants = sales_consultants;
            }

            public Info getInfo() {
                return info;
            }

            public void setInfo(Info info) {
                this.info = info;
            }

            public boolean isExpand() {
                return expand;
            }

            public void setExpand(boolean expand) {
                this.expand = expand;
            }

            @Override
            public String toString() {
                return "ClassPojo [sales_consultants = " + sales_consultants + ", info = " + info + "]";
            }

            public class Sales_consultants {
                private Info info;

                public Info getInfo() {
                    return info;
                }

                public void setInfo(Info info) {
                    this.info = info;
                }

                @Override
                public String toString() {
                    return "ClassPojo [info = " + info + "]";
                }
            }


        }
    }

    public class Info {
        private Integer id;

        private Abs abs;

        private String name;

        private String dp_url;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Abs getAbs() {
            return abs;
        }

        public void setAbs(Abs abs) {
            this.abs = abs;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDp_url() {
            return dp_url;
        }

        public void setDp_url(String dp_url) {
            this.dp_url = dp_url;
        }

        @Override
        public String toString() {
            return "ClassPojo [id = " + id + ", abs = " + abs + ", name = " + name + ", dp_url = " + dp_url + "]";
        }
    }

    public class Abs {
        private Integer post_bookings;

        private Integer tdrives;

        private Integer uncalled_tasks;

        private Integer visits;

        private Integer calls;

        private Integer deliveries;

        private Integer pending_total_count;

        public Integer getPending_total_count() {
            return pending_total_count;
        }

        public void setPending_total_count(Integer pending_total_count) {
            this.pending_total_count = pending_total_count;
        }

        public Integer getPost_bookings() {
            return post_bookings;
        }

        public void setPost_bookings(Integer post_bookings) {
            this.post_bookings = post_bookings;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getUncalled_tasks() {
            return uncalled_tasks;
        }

        public void setUncalled_tasks(Integer uncalled_tasks) {
            this.uncalled_tasks = uncalled_tasks;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getCalls() {
            return calls;
        }

        public void setCalls(Integer calls) {
            this.calls = calls;
        }

        public Integer getDeliveries() {
            return deliveries;
        }

        public void setDeliveries(Integer deliveries) {
            this.deliveries = deliveries;
        }

        @Override
        public String toString() {
            return "ClassPojo [post_bookings = " + post_bookings + ", tdrives = " + tdrives + ", uncalled_tasks = " + uncalled_tasks + ", visits = " + visits + ", calls = " + calls + ", deliveries = " + deliveries + "]";
        }
    }

}
