package com.salescrm.telephony.model;

/**
 * Created by bharath on 28/10/16.
 */
public class AutoDialogModel {
    private  String header;
    private String yesTitle,noTitle,dialogTitle;
    int fromId;
    boolean isSkip = false;
    boolean isCancellable = false;
    public AutoDialogModel() {
    }
    public AutoDialogModel(String dialogTitle,String yesTitle, String noTitle,int fromId) {
        this.dialogTitle = dialogTitle;
        this.yesTitle = yesTitle;
        this.noTitle = noTitle;
        this.fromId = fromId;
    }
    public AutoDialogModel(String dialogTitle,String yesTitle, String noTitle,int fromId,boolean isSkip) {
        this.dialogTitle = dialogTitle;
        this.yesTitle = yesTitle;
        this.noTitle = noTitle;
        this.fromId = fromId;
        this.isSkip = isSkip;
    }
     public AutoDialogModel(String dialogTitle,String yesTitle, String noTitle, boolean isCancellable) {
            this.dialogTitle = dialogTitle;
            this.yesTitle = yesTitle;
            this.noTitle = noTitle;
            this.isCancellable  = isCancellable;
    }

   public boolean isSkip() {
        return isSkip;
    }

    public AutoDialogModel(String header,String dialogTitle, String yesTitle, String noTitle) {
        this.header = header;
        this.dialogTitle = dialogTitle;
        this.yesTitle = yesTitle;
        this.noTitle = noTitle;
        this.fromId = 0;
    }
    public AutoDialogModel(String dialogTitle, String yesTitle, String noTitle) {
        this.dialogTitle = dialogTitle;
        this.yesTitle = yesTitle;
        this.noTitle = noTitle;
        this.fromId = 0;
    }

    public AutoDialogModel(String dialogTitle) {
        this.dialogTitle = dialogTitle;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getYesTitle() {
        return yesTitle;
    }

    public void setYesTitle(String yesTitle) {
        this.yesTitle = yesTitle;
    }

    public String getNoTitle() {
        return noTitle;
    }

    public void setNoTitle(String noTitle) {
        this.noTitle = noTitle;
    }

    public String getDialogTitle() {
        return dialogTitle;
    }

    public void setDialogTitle(String dialogTitle) {
        this.dialogTitle = dialogTitle;
    }

    public int getFromId() {
        return fromId;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }

    public void setSkip(boolean skip) {
        isSkip = skip;
    }

    public boolean isCancellable() {
        return isCancellable;
    }

    public void setCancellable(boolean cancellable) {
        isCancellable = cancellable;
    }
}
