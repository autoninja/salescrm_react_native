import React, { Component } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
export default class SuccessView extends Component {
  render() {
    return (
      <View style={styles.loaderMain}>
        <View style={styles.loaderInner}>
          <Image
            source={require("../../images/ic_check_mark_white.png")}
            style={styles.image}
          />
          <Text style={styles.text}>{this.props.text}</Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  loaderMain: {
    backgroundColor: "rgba(0, 0, 0, 0.6)",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    alignSelf: "center",
    justifyContent: "center"
  },
  loaderInner: {
    height: 200,
    width: 200,
    opacity: 1,
    padding: 10,
    backgroundColor: "#007fff",
    borderRadius: 10,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    marginTop: 8,
    color: "#fff",
    fontSize: 20,
    textAlign:'center'
  },
  image: {
    height: 40,
    width: 40
  }
});
