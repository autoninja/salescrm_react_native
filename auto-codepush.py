import os
def quit():
	print "---Process termination----"
	exit()
def getAppName(val):
	if val == "Android":
		return "Autoninja/NinjaCRM-Sales-Android"
	else:
		return "Autoninja/NinjaCRM-Sales-IOS"
print "----Welcome to codepush run----"
platform = raw_input("Android or IOS? (a/i): ")
if platform.lower() == "a" or platform.lower() == "android":
	platform = "Android"
elif platform.lower() == "i" or platform.lower() == "ios":
	platform = "iOS"
else:
	quit()
deployment = raw_input("Staging or Production? (s/p): ")
if deployment.lower() == "s" or deployment.lower() == "staging":
	deployment = "Staging"
elif deployment.lower() == "p" or deployment.lower() == "production":
	deployment = "Production"
else:
	quit()
description = raw_input("Enter release note: ")

print("\n----Release request raised---\nPlatform: "+platform+"\nDeployment: "+deployment+"\nDescription: "+description)

command = "appcenter codepush release-react -a "+getAppName(platform)+" -m -d "+deployment+" --description \'"+description+"\'"
print("\n<"+command+">")

yn = raw_input("\nAre you sure want to release? (y/n): ")
if yn.lower() == "y" or yn.lower() == "yes":
	print("Loading...")
	os.system(command)
else:
	quit()
