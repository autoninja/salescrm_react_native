import React, { Component } from 'react'
import { Keyboard, Platform, View, Text, Image, TouchableOpacity, YellowBox, ScrollView, Switch, FlatList, ActivityIndicator, TextInput, Button, KeyboardAvoidingView} from 'react-native'

import styles from '../styles/styles'

import DeviceInfo from 'react-native-device-info';
import RestClient from '../network/rest_client'
import Toast, {DURATION} from 'react-native-easy-toast'

import * as async_storage from '../storage/async_storage';
import DealershipService from '../storage/dealership_service'
import DealershipModel from '../models/dealership_model'

import { StackActions, NavigationActions } from 'react-navigation';



export default class ValidateOtp extends Component {

  constructor(props) {

    super(props);
    this.state = {loading:false, otp:''};



    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);

  }

  request_otp() {
    Keyboard.dismiss()
    this.setState({loading:true});
    let { navigation } = this.props;
    new RestClient().login({
      mobile_no: navigation.getParam('mobile'),
      device_id:DeviceInfo.getUniqueID()}).then( (data) => {
            this.setState({ loading: false})
            if(data) {
              if(data.result && data.result.success) {
                  this.refs.toast.show('New otp sent');
              }
              else if(data.message) {
                  this.refs.toast.show(data.message);
              }
              else {
                  this.refs.toast.show('Error in sending otp');
              }

            }
            else {
                this.refs.toast.show('Error in sending otp');
            }

        }).catch(error => {
          this.setState({ loading: false});
          if (!error.status) {
              this.refs.toast.show('No Internet Connection');
          }
        });

  }

  validate_otp() {
    Keyboard.dismiss()
    let { navigation } = this.props;

    // navigation.navigate('Home')
    if(this.state.otp && this.state.otp.length!=6) {
       this.refs.toast.show('Please enter 6 digit otp');
       return;
    }
    this.setState({loading:true});
    new RestClient().validate_otp({
      mobile_no: navigation.getParam('mobile'),
      device_id: navigation.getParam('device_id'),
      fcm_id:'12345',
      otp:this.state.otp}).then( (data) => {
            this.setState({ loading: false})
            console.log(data);
            if(data) {
              if(data.statusCode == '2002' && data.result && data.result.user_info.length>0) {
                  console.log(JSON.stringify(data));
                  global.token = data.result.token;
                  async_storage.setToken(data.result.token);
                  DealershipService.deleteAll();
                  data.result.user_info.forEach(function(user_info) {
                    DealershipService.save(
                        new DealershipModel(user_info.dealer.id,
                                            user_info.dealer.name,
                                            user_info.dealer.db_name,
                                            user_info.users.id,
                                            user_info.users.name,
                                            user_info.users.image_url,
                                            false
                                            )
                                        );
                  });

                  async_storage.setLoggedIn('1').then(()=>{
                    var resetAction = StackActions.reset({
                       index: 0, // <-- currect active route from actions array
                       key: null,
                       actions: [
                         NavigationActions.navigate({ routeName: 'SwitchDealership' }),
                       ],
                     });
                     this.props.navigation.dispatch(resetAction);

                  });

              }
              else if(data.message) {
                  this.refs.toast.show(data.message);
              }
              else {
                  this.refs.toast.show('Error validating otp');
              }

            }
            else {
                this.refs.toast.show('Error validating otp');
            }

        }).catch(error => {
          console.log(error);
          if (!error.status) {
              this.refs.toast.show('No Internet Connection');
          }
          else {
          this.refs.toast.show('Error validating otp');
        }
          this.setState({ loading: false})});

  }



  render() {
    const { navigation } = this.props;
    let mobile = navigation.getParam('mobile');

    let loaderStyle = this.state.loading?styles.overlay:{display:'none'};

    return <View style= {styles.MainContainerWhite}>
    <Toast
                    ref="toast"
                     style={{backgroundColor:'red'}}
                     position='bottom'
                     fadeInDuration={750}
                     fadeOutDuration={750}
                     opacity={0.8}
                     textStyle={{color:'white'}}
                 />
    <ScrollView keyboardShouldPersistTaps="handled" contentContainerStyle={{flexGrow: 1}}>


           <View style={{flex:2,height:'100%', justifyContent:'center', alignItems:'center'}}>
           <Image source={require( '../images/ic_log_ninja_crm.png')} style={{marginTop:40,width:227, height:55.5,}}/>



           <View style={{flex:1,alignItems:'center', justifyContent:'center'}}>

           <TextInput
       style={{textAlign:'center',height: 50, marginLeft:20,marginRight:20,width:240, padding:2, borderColor: 'gray', borderBottomWidth: 1,}}
       onChangeText={(otp) => this.setState({otp})}
       maxLength = {6}
       placeholder="Please enter the otp"
       placeholderTextColor="#808080"
       value={this.state.otp}
       keyboardType ='numeric'

     />





  <TouchableOpacity  style={{width:'100%', alignItems: "center",}} onPress ={()=> this.validate_otp()}>
   <View style={{height:40, width:240,justifyContent: "center", marginTop:40,backgroundColor:'#007fff', borderRadius:6}}>

   <Text adjustsFontSizeToFit={true}
        numberOfLines={1} style={{textAlign:'center', fontSize:18,textAlignVertical: 'center', color:'#fff'}}>
   Submit
   </Text>





   </View>
   </TouchableOpacity>


   <TouchableOpacity  style={{width:'100%', alignItems: "center",}} onPress ={()=> this.request_otp()}>
    <View style={{height:40, width:240,justifyContent: "center", marginTop:10, borderRadius:6}}>

    <Text adjustsFontSizeToFit={true}
         numberOfLines={1} style={{textAlign:'center', textDecorationLine:'underline', fontSize:18,textAlignVertical: 'center', color:'#9b9b9b'}}>
    Resend
    </Text>

    </View>
    </TouchableOpacity>







           </View>

           </View>

           </ScrollView>




          <View style={{flexDirection:'row',justifyContent:'center'}}>
          <Text style= {{color:'#808080'}}>Powered by </Text>
          <Image source={require( '../images/ic_log_autoninja.png')} style={{width:67.5, height:15,}}/>
          </View>
          <View style={loaderStyle} >
          <ActivityIndicator
                animating={this.state.loading}
                style={{flex: 1,   alignSelf:'center',}}
                color="#fff"
                size="large"
                hidesWhenStopped={true}

          />
          </View>

            </View>




  }
}
