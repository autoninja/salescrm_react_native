import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';


export default class TextAreaView extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			isFetching: false,
			remarks: (this.props.radioObject.defaultFAId) ? this.props.radioObject.defaultFAId.answerValue : "",
		}
	}

	componentWillUnmount() {
		if (this.props.radioObject && this.props.radioObject.fQId) {
			this.props.pushToarray({ name: this.props.radioObject.fQId }, -1)
		}
		if (this.props.radioObject.ifVisibleMandatory) {
			this.props.mandatoryField({ id: this.props.radioObject.fQId }, -1)
		}
	}

	generateView(innerObjectsValues) {
		console.log("innerObjectRadio questionChildren TextAreaView", JSON.stringify(innerObjectsValues))
		//this.props.callMainGenerateViews(item.values[i], item.values[i].formInputType)
		let { remarks } = this.state;

		if (remarks) {
			this.props.pushToarray({
				name: innerObjectsValues.fQId,
				value: { fAnsId: (innerObjectsValues.defaultFAId) ? innerObjectsValues.defaultFAId.fAnsId : '', answerValue: remarks }, questionId: innerObjectsValues.questionId
			})
			if (innerObjectsValues.ifVisibleMandatory) {
				this.props.mandatoryField({ id: innerObjectsValues.fQId, isSelected: true })
			}
		}

		return (
			<View>
				{(innerObjectsValues.title) ?
					<Text style={{ fontSize: 18, marginTop: 10, fontWeight: 'bold' }}>{(innerObjectsValues.ifVisibleMandatory) ? innerObjectsValues.title + "*" : ''}</Text> : null}

				<TextInput
					style={{ paddingTop: 10, paddingLeft: 10, paddingBottom: 10, flex: 1, borderColor: '#CFCFCF', fontSize: 16, borderBottomWidth: 1, marginTop: 8, color: '#303030' }}
					placeholder='Enter here'
					value={remarks}
					onChangeText={(text) => {
						remarks = text
						if (innerObjectsValues.ifVisibleMandatory && !remarks) {
							this.props.mandatoryField({ id: innerObjectsValues.fQId, isSelected: false })
						}
						this.setState({ remarks });
					}}
				/>
			</View>
		)


	}

	render() {
		//console.log("innerObjects", "reached")
		//console.log("innerObjects", this.props.innerObjects.fQId)
		//console.error("innerObjectRadio")
		//console.log("innerObjectRadio", JSON.stringify(this.props.innerObjects))
		return (
			<View style={{ flex: 1 }}>
				{this.generateView(this.props.radioObject)}
			</View>
		)
	}
}