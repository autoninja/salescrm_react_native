package com.salescrm.telephony.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bharath on 28/11/17.
 */

public class TodaySummaryModel {
    private static TodaySummaryModel todaySummaryModel = new TodaySummaryModel();
    private List<TodaySummaryResponse.Result> resultList = new ArrayList<>();

    public static TodaySummaryModel getInstance() {
        return todaySummaryModel;
    }

    public List<TodaySummaryResponse.Result> getResultList() {
        if (resultList == null) {
            resultList = new ArrayList<>();
        }
        return resultList;
    }

    public void setResultList(List<TodaySummaryResponse.Result> resultList) {
        this.resultList = resultList;
    }

}
