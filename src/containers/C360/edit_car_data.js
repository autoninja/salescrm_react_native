import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, NativeModules, TouchableWithoutFeedback, KeyboardAvoidingView, ScrollView, TextInput, BackHandler, Modal
} from 'react-native'
//import VehicleDBService from '../../storage/vehicle_db_service';

const styles = StyleSheet.create({
    heading: {
        fontSize: 16,
        color: '#000000',
        fontWeight: 'bold',
        marginTop: 5, marginBottom: 5,
        marginRight: 5
    },
    title: {
        fontSize: 16,
        color: '#000000',
        margin: 5
    },
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        padding: 20

    },
    listItem: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
        paddingBottom: 20,
        fontSize: 16,
        color: '#303030'
    },
});
export default class EditCarData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            interestedCar: this.props.interestedCar,
            isPickerVisible: false,
            selectedOptions: this.props.interestedCar
        }
    }


    closeInnerPopUp = () => {
        this.setState({ isPickerVisible: false, selectedOptions: this.props.interestedCar })
    }

    openInnerPopUp() {
        this.setState({ isPickerVisible: true })
    }

    getVariantList = () => {
        //let variantList = VehicleDBService.getVehicleVariants(this.props.interestedCar.model_id);

       // return this.getUnique(variantList, 'variant_id');
       return [];
    }

    getUnique(arr, index) {

        const unique = arr
            .map(e => e[index])

            // store the keys of the unique objects
            .map((e, i, final) => final.indexOf(e) === i && i)

            // eliminate the dead keys & store unique objects
            .filter(e => arr[e]).map(e => arr[e]);

        return unique;
    }

    selectedData = (item) => {
        this.setState({ selectedOptions: item, isPickerVisible: false })
    }

    submitSelection = () => {
        this.props.submitEditedCarData(this.state.selectedOptions, this.props.interestedCar)
        this.setState({ isPickerVisible: false, selectedOptions: '' })
    }

    cancelEdit() {
        this.props.closeEditCarDetail();
        this.setState({ selectedOptions: this.props.interestedCar })
    }

    generateView = () => {
        //console.log("this.getVariantList()", this.getVariantList())
        return (
            <View style={{ backgroundColor: '#ffffff' }}>
                <FlatList
                    data={this.getVariantList()}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity onPress={() => this.selectedData(item)}>
                                <View>
                                    <Text style={styles.listItem}>{item.variant}</Text>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                    keyExtractor={(item, index) => index.toString()}
                />
                <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                    <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080', padding: 10 }} onPress={() => this.closeInnerPopUp()}>
                        <Text style={{ textAlign: 'center', color: "#fff" }}>Cancel</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {
        let { selectedOptions } = this.state;
        //console.log("this.props.interestedCar", JSON.stringify(VehicleDBService.getAllVehicle()))
        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled={Platform.OS === 'ios'}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Modal
                        visible={this.props.visible}
                        animationType={'slide'}
                        transparent={true}
                        onRequestClose={() => this.props.closeEditCarDetail()}
                    >

                        <Modal
                            visible={this.state.isPickerVisible}
                            animationType={'slide'}
                            transparent={true}
                            onRequestClose={() => this.closeInnerPopUp()}
                        >
                            <View style={styles.overlay}>
                                {this.generateView()}
                            </View>
                        </Modal>
                        <View style={styles.overlay}>
                            <View style={{ justifyContent: 'center', backgroundColor: '#ffffff', padding: 10 }}>
                                <Text style={styles.title}>{this.props.interestedCar.name}</Text>

                                <Text style={styles.heading}>Variant</Text>

                                <TouchableOpacity onPress={() => this.openInnerPopUp()}>
                                    <Text style={styles.title}>{(selectedOptions) ? selectedOptions.variant : this.props.interestedCar.variant}</Text>
                                </TouchableOpacity>

                                <Text style={styles.heading}>Fuel Type</Text>
                                <TouchableOpacity onPress={null}>
                                    <Text style={styles.title}>{(selectedOptions) ? selectedOptions.fuel_type : this.props.interestedCar.fuel_type}</Text>
                                </TouchableOpacity>

                                <Text style={styles.heading}>Color/Description</Text>
                                <TouchableOpacity onPress={null}>
                                    <Text style={styles.title}>{(selectedOptions) ? selectedOptions.color : this.props.interestedCar.color}</Text>
                                </TouchableOpacity>

                                <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                                    <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080' }} onPress={() => this.cancelEdit()}>
                                        <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Close</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: "#007fff" }} onPress={() => this.submitSelection()}>
                                        <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Save</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>
            </KeyboardAvoidingView>);
    }
}