import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
export default class ColdVisitItem extends Component {
  constructor(props) {
    super(props);
    this.state = { showImagePlaceHolder: true, expand: false };
  }
  expand() {
    this.setState({ expand: !this.state.expand });
  }
  render() {
    let {
      user_id,
      user_role_id,
      name,
      image_url,
      count,
      location_id,
      child
    } = this.props.info;

    let imageSource;
    let placeHolder;
    imageSource = { uri: image_url };
    imageStyle = {
      position: "absolute",
      width: 36,
      height: 36,
      borderRadius: 36 / 2
    };
    placeHolder = (
      <View
        style={{
          position: "absolute",
          display: this.state.showImagePlaceHolder ? "flex" : "none",
          width: 36,
          height: 36,
          borderRadius: 36 / 2,
          backgroundColor: "#FF8000",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Text style={{ color: "#fff" }}>{name.charAt(0).toUpperCase()}</Text>
      </View>
    );

    return (
      <View>
        <TouchableOpacity onPress={() => this.expand()}>
          <View
            style={{
              marginTop: 10,
              flexDirection: "row",
              height: 50,
              backgroundColor: "#eceff1",
              alignItems: "center",
              borderRadius: 6
            }}
          >
            <View
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              {placeHolder}
              <TouchableOpacity
                style={{ position: "absolute" }}
                onPress={() => this.expand()}
                onLongPress={() => {
                  this.props.onLongPress("user_info", {
                    info: {
                      name,
                      dp_url: image_url
                    }
                  });
                }}
              >
                <Image
                  source={imageSource}
                  style={{ width: 36, height: 36, borderRadius: 36 / 2 }}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                flex: 3,
                height: "100%"
              }}
            >
              <View
                style={{
                  flex: 2,
                  alignItems: "flex-start",
                  justifyContent: "center"
                }}
              >
                <Text
                  style={{
                    color: "#303030",
                    fontSize: 16,
                    paddingLeft: 8,
                    paddingRight: 10
                  }}
                >
                  {name}
                </Text>
              </View>
            </View>

            <View
              style={{
                flex: 1,
                height: "100%",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                onPress={() => this.expand()}
                onLongPress={() =>
                  this.props.onLongPress("cold_visit_details", {
                    info: {
                      user_id,
                      user_role_id,
                      title: name,
                      location_id
                    }
                  })
                }
              >
                <Text
                  style={{ color: "#303030", textDecorationLine: "underline" }}
                >
                  {count}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
        {this.state.expand && child && child.length > 0 && (
          <View
            style={{
              marginLeft: 10,
              borderLeftColor: "#eceff1",
              borderBottomColor: "#eceff1",
              borderLeftWidth: 0.8,
              borderBottomWidth: 0.8,
              borderBottomLeftRadius: 8,
              paddingLeft: 8,
              paddingBottom: 8
            }}
          >
            {child.map((item, index) => {
              return (
                <ColdVisitItem
                  onLongPress={this.props.onLongPress.bind(this)}
                  info={{
                    user_id: item.user_id,
                    user_role_id: item.user_role_id,
                    name: item.name,
                    image_url: item.image_url,
                    count: item.count,
                    location_id: item.location_id,
                    child: item.child
                  }}
                  key={index + ""}
                />
              );
            })}
          </View>
        )}
      </View>
    );
  }
}
