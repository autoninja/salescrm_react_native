package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 31/8/16.
 */
public class DealershipsDB extends RealmObject {

    @PrimaryKey
    private int dealer_id;
    private String dealer_name;
    private String dealer_db_name;
    private int user_id;
    private String user_name;
    private String user_dp_url;

    public DealershipsDB() {

    }

    public int getDealer_id() {
        return dealer_id;
    }

    public void setDealer_id(int dealer_id) {
        this.dealer_id = dealer_id;
    }

    public String getDealer_name() {
        return dealer_name;
    }

    public void setDealer_name(String dealer_name) {
        this.dealer_name = dealer_name;
    }

    public String getDealer_db_name() {
        return dealer_db_name;
    }

    public void setDealer_db_name(String dealer_db_name) {
        this.dealer_db_name = dealer_db_name;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_dp_url() {
        return user_dp_url;
    }

    public void setUser_dp_url(String user_dp_url) {
        this.user_dp_url = user_dp_url;
    }
}
