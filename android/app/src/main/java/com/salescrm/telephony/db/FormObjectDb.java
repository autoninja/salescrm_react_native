package com.salescrm.telephony.db;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 14-10-2016.
 */

public class FormObjectDb extends RealmObject {

    private int scheduled_activity_id;
    private int action_id;

    private String fQId;

    private int leadId;
    private String title;
    private String popupTitle;
    private String formInputType;
    private String questionIndent;
    private String validationRegex;
    private String ifVisibleMandatory;
    private String ifMeOneOfTheseMandatory;
    private String hidden;
    private Integer editable;
    private DefaultFAId defaultFAId;
    private ValidationObject validationObject;
    private String newFormCondition;
    private String dependent_form_question_id;

    public RealmList<FormObjectAnswerChildren> answerChildren;


    public int getScheduled_activity_id() {
        return scheduled_activity_id;
    }

    public void setScheduled_activity_id(int scheduled_activity_id) {
        this.scheduled_activity_id = scheduled_activity_id;
    }

    public int getAction_id() {
        return action_id;
    }

    public void setAction_id(int action_id) {
        this.action_id = action_id;
    }


    @SerializedName("questionChildren")
    public RealmList<FormObjectQuestionChildren> questionChildren;

    public String getIfVisibleMandatory() {
        return ifVisibleMandatory;
    }

    public int getLeadId() {
        return leadId;
    }

    public void setLeadId(int leadId) {
        this.leadId = leadId;
    }

    public String getPopupTitle() {
        return popupTitle;
    }

    public void setPopupTitle(String popupTitle) {
        this.popupTitle = popupTitle;
    }

    public String getQuestionIndent() {
        return questionIndent;
    }

    public void setQuestionIndent(String questionIndent) {
        this.questionIndent = questionIndent;
    }

    public String getValidationRegex() {
        return validationRegex;
    }

    public void setValidationRegex(String validationRegex) {
        this.validationRegex = validationRegex;
    }

    public String getHidden() {
        return hidden;
    }

    public void setHidden(String hidden) {
        this.hidden = hidden;
    }

    public DefaultFAId getDefaultFAId() {
        return defaultFAId;
    }

    public void setDefaultFAId(DefaultFAId defaultFAId) {
        this.defaultFAId = defaultFAId;
    }

    public ValidationObject getValidationObject() {
        return validationObject;
    }

    public void setValidationObject(ValidationObject validationObject) {
        this.validationObject = validationObject;
    }

    public String getNewFormCondition() {
        return newFormCondition;
    }

    public void setNewFormCondition(String newFormCondition) {
        this.newFormCondition = newFormCondition;
    }

    public String getDependent_form_question_id() {
        return dependent_form_question_id;
    }

    public void setDependent_form_question_id(String dependent_form_question_id) {
        this.dependent_form_question_id = dependent_form_question_id;
    }

    public String getfQId() {
        return fQId;
    }

    public void setfQId(String fQId) {
        this.fQId = fQId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFormInputType() {
        return formInputType;
    }

    public void setFormInputType(String formInputType) {
        this.formInputType = formInputType;
    }

    public String isIfVisibleMandatory() {
        return ifVisibleMandatory;
    }

    public void setIfVisibleMandatory(String ifVisibleMandatory) {
        this.ifVisibleMandatory = ifVisibleMandatory;
    }

    public String getIfMeOneOfTheseMandatory() {
        return ifMeOneOfTheseMandatory;
    }

    public void setIfMeOneOfTheseMandatory(String ifMeOneOfTheseMandatory) {
        this.ifMeOneOfTheseMandatory = ifMeOneOfTheseMandatory;
    }

    public RealmList<FormObjectAnswerChildren> getAnswerChildren() {
        return answerChildren;
    }

    public void setAnswerChildren(RealmList<FormObjectAnswerChildren> formObjectAnswerChildren) {
        this.answerChildren = formObjectAnswerChildren;
    }

    public RealmList<FormObjectQuestionChildren> getQuestionChildren() {
        return questionChildren;
    }

    public void setQuestionChildren(RealmList<FormObjectQuestionChildren> questionChildren) {
        this.questionChildren = questionChildren;
    }

    public Integer getEditable() {
        return editable;
    }

    public void setEditable(Integer editable) {
        this.editable = editable;
    }
}
