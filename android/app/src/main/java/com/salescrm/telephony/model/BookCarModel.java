package com.salescrm.telephony.model;

import com.salescrm.telephony.model.booking.ApiInputBookingDetails;

import java.util.List;

/**
 * Created by bannhi on 16/6/17.
 */


public class BookCarModel {

    private String amount_recieved;
    private int location_id;
    private long lead_id;
    private int lead_car_stage_id;
    private String next_follow_date;
    private int lead_source_id;

    private String booking_name;
    private String dob;
    private String pan_number;
    private String aadhar_number;
    private String remarks;
    private String gst;
    private String lead_last_updated;


    private List<ApiInputBookingDetails> booking_details;

    private FormSubmissionInputData form_object;

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public FormSubmissionInputData getForm_object() {
        return form_object;
    }

    public void setForm_object(FormSubmissionInputData form_object) {
        this.form_object = form_object;
    }

    public int getLead_source_id() {
        return lead_source_id;
    }

    public void setLead_source_id(int lead_source_id) {
        this.lead_source_id = lead_source_id;
    }

    public String getAmount_recieved() {
        return amount_recieved;
    }

    public void setAmount_recieved(String amount_recieved) {
        this.amount_recieved = amount_recieved;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public long getLead_id() {
        return lead_id;
    }

    public void setLead_id(long lead_id) {
        this.lead_id = lead_id;
    }

    public String getBooking_name() {
        return booking_name;
    }

    public void setBooking_name(String booking_name) {
        this.booking_name = booking_name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPan_number() {
        return pan_number;
    }

    public void setPan_number(String pan_number) {
        this.pan_number = pan_number;
    }

    public String getAadhar_number() {
        return aadhar_number;
    }

    public void setAadhar_number(String aadhar_number) {
        this.aadhar_number = aadhar_number;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public int getLead_car_stage_id() {
        return lead_car_stage_id;
    }

    public void setLead_car_stage_id(int lead_car_stage_id) {
        this.lead_car_stage_id = lead_car_stage_id;
    }

    public String getNext_follow_date() {
        return next_follow_date;
    }

    public void setNext_follow_date(String next_follow_date) {
        this.next_follow_date = next_follow_date;
    }

    public class LeadCarDetails {
        private String carVariant;

        private String carFuelType;

        private String carColorId;

        private String carModel;

        private int lead_car_id;

        public int getLead_car_id() {
            return lead_car_id;
        }

        public void setLead_car_id(int lead_car_id) {
            this.lead_car_id = lead_car_id;
        }

        public String getCarVariant() {
            return carVariant;
        }

        public void setCarVariant(String carVariant) {
            this.carVariant = carVariant;
        }

        public String getCarFuelType() {
            return carFuelType;
        }

        public void setCarFuelType(String carFuelType) {
            this.carFuelType = carFuelType;
        }

        public String getCarColorId() {
            return carColorId;
        }

        public void setCarColorId(String carColorId) {
            this.carColorId = carColorId;
        }

        public String getCarModel() {
            return carModel;
        }

        public void setCarModel(String carModel) {
            this.carModel = carModel;
        }


        @Override
        public String toString() {
            return "ClassPojo [carVariant = " + carVariant + ", carFuelType = " + carFuelType + ", carColorId = " + carColorId + ", carModel = " + carModel + "]";
        }
    }

    public List<ApiInputBookingDetails> getBooking_details() {
        return booking_details;
    }

    public void setBooking_details(List<ApiInputBookingDetails> booking_details) {
        this.booking_details = booking_details;
    }
}
