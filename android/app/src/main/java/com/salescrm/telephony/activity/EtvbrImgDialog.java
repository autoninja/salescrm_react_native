package com.salescrm.telephony.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.utils.Util;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by prateek on 6/7/17.
 */

public class EtvbrImgDialog extends AppCompatActivity {

    ImageView imageView;
    TextView tvDseName;
    String dseName = "";
    ProgressBar progressBarImageLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.etvbr_img_dialog);
        imageView = (ImageView) findViewById(R.id.dse_img);
        tvDseName = (TextView) findViewById(R.id.dse_name);
        progressBarImageLoading = (ProgressBar) findViewById(R.id.progressBarImageLoading);


        Bundle extras = getIntent().getExtras();
        String imgeUrl = extras.getString("imgUrlEtvbr");
        dseName = extras.getString("dseNameEtvbr");
        if (extras != null) {
            //imageView.setVisibility(View.GONE);
            if(Util.isNotNull(imgeUrl)) {
                progressBarImageLoading.setVisibility(View.VISIBLE);
                Picasso.with(this).load(imgeUrl).error(R.drawable.ic_person_white_48dp).fit().centerInside().into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        System.out.println("Success");
                        imageView.setVisibility(View.VISIBLE);
                        progressBarImageLoading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        System.out.println("Error");
                        imageView.setVisibility(View.VISIBLE);
                        progressBarImageLoading.setVisibility(View.GONE);
                    }
                });
            }
            else {
                Picasso.with(this).load(R.drawable.ic_person_white_48dp).into(imageView);
            }

            tvDseName.setText(dseName);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
