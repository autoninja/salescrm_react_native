package com.salescrm.telephony.db;

import java.io.File;

import io.realm.RealmObject;

/**
 * Created by prateek on 9/12/16.
 */

public class SendMailObjects extends RealmObject {

    private String address;
    private String mailType;
    private String body;
    private String subject;
    private String leadId;
    private String customer;
    private int mailSync;

    private String attachment;

    public int getMailSync() {
        return mailSync;
    }

    public void setMailSync(int mailSync) {
        this.mailSync = mailSync;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMailType() {
        return mailType;
    }

    public void setMailType(String mailType) {
        this.mailType = mailType;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }
}
