import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
export default class FilterKeyItem extends Component {
    constructor(props) {
        super(props);
    }
    getSelectedCount() {
        let { children } = this.props;
        let selectedCount = 0;
        for (i = 0; i < children.length; i++) {
            if (children[i].selected) {
                return " •";
            }
        }
        return "";
    }

    render() {
        let { title, selected, hasFilters, isLastItem } = this.props;
        return (
            <TouchableOpacity
                onPress={() => {
                    this.props.onPress();
                }}
            >
                <View style={{
                    height: 60,
                    padding: 20,
                    backgroundColor: selected ? "#fff" : "#ebebeb",
                    borderBottomWidth: isLastItem ? 0 : 0.3,
                    borderBottomColor: '#808080',
                    justifyContent: 'center',
                }}>
                    <Text style={{
                        color: selected ? "#007fff" : '#3f3f3f',
                        fontSize: 16,
                        fontWeight: selected ? "bold" : "normal"
                    }}>{title + this.getSelectedCount()}</Text>


                </View>
            </TouchableOpacity>
        )
    }
}