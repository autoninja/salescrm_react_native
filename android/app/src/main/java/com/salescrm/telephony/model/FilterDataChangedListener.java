package com.salescrm.telephony.model;

/**
 * Created by bharath on 23/8/16.
 */
public class FilterDataChangedListener {
    private boolean isUpdateRequired =false;

    public FilterDataChangedListener(boolean isUpdateRequired) {
        this.isUpdateRequired = isUpdateRequired;
    }

    public boolean isUpdateRequired() {
        return isUpdateRequired;
    }

    public void setUpdateRequired(boolean updateRequired) {
        isUpdateRequired = updateRequired;
    }
}
