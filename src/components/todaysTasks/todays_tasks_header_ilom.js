import React, { Component } from 'react'
import { View, Text } from 'react-native'


export default class TodayTasksHeaderILom extends Component {
  constructor(props) {
    super(props);
    this.state = { expand: false, showImagePlaceHolder: true };
  }

  render() {

    return (
      <View style={{ flexDirection: 'row', height: 30, backgroundColor: '#fff', borderColor: '#babdbe', borderWidth: 0.8, alignItems: 'center' }}>

        <View style={{ flex: 1, alignItems: 'center' }} >

        </View>


        <View style={{ flex: 1, height: '100%' }}>
          <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#2e5e86', fontWeight: '700' }} >1st Call</Text>
          </View>

        </View>

        <View style={{ flex: 1, height: '100%', }}>
          <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#2e5e86', fontWeight: '800' }} >Call Back</Text>
          </View>
        </View>



        <View style={{ flex: 1, height: '100%', }}>
          <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ color: '#2e5e86', fontWeight: '800' }} >Prospect</Text>
          </View>
        </View>





        <View style={{ flex: 1, height: '100%', justifyContent: "center", alignItems: "center" }}>
          <Text style={{ color: '#F5A623', fontWeight: '800' }} >Pending</Text>
        </View>

      </View>
    );
  }
}
