package com.salescrm.telephony.interfaces;

public class EventActionDone {
    private static final EventActionDone ourInstance = new EventActionDone();
    private static EventActionDoneInterface eventActionDoneInterface = null;

    public static EventActionDone getInstance(EventActionDoneInterface interfaceAction) {
        eventActionDoneInterface = interfaceAction;
        return ourInstance;
    }
     public static EventActionDone getInstance() {
        return ourInstance;
    }


    public void onEventActionDone() {
        if(eventActionDoneInterface!=null) {
            eventActionDoneInterface.onEventActionDone();
        }
    }

    private EventActionDone() {
    }
    public interface EventActionDoneInterface {
        void onEventActionDone();
    }
}
