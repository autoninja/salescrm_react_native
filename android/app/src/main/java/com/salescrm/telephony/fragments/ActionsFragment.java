package com.salescrm.telephony.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.adapter.TimeLineOneRecyclerAdapter;
import com.salescrm.telephony.db.PlannedActivities;
import com.salescrm.telephony.interfaces.MediaPlayerChecker;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.LeadHistory;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ActionsFragment extends Fragment implements TimeLineOneRecyclerAdapter.callToParentListener {

    private static RecyclerView mRecyclerView;
    private static String mleadId;
    private Realm realm;
    private Preferences pref;
    private RealmList<PlannedActivities> plannedActivitiesRealmList = new RealmList<>();
    private List<LeadHistory.Result> leadHistoryResponseData;
    private List<PlannedActivities> plannedActivitiesList;
    private TimeLineOneRecyclerAdapter mTimeLineAdapterOneRecycler;
    private TextView tvWaitForResponse;
    private ConnectionDetectorService connectionDetectorService;
    private TimeLineOneRecyclerAdapter mediaPlayerChecker;
    //public static boolean FRAGMENT_DESTROYED = false;
    //private boolean HISTORY_RECEIVED = false;
    private ArrayList<Boolean> historyResponse;


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.card_detail_action_fragment, container, false);

        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(getActivity());
        leadHistoryResponseData = new ArrayList<>();
        mRecyclerView = (RecyclerView) view.findViewById(R.id.progressRecycler);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        tvWaitForResponse = (TextView) view.findViewById(R.id.wait_response);
        connectionDetectorService = new ConnectionDetectorService(getActivity());
        historyResponse = new ArrayList<>();
        plannedActivitiesList = new ArrayList<>();
        initView();
        return view;
    }


    private void initView() {
        historyResponse.clear();
        if (plannedActivitiesList != null) {
            plannedActivitiesList.clear();
        }

        /*plannedActivitiesRealmList.addAll(realm.where(PlannedActivities.class)
                .equalTo("leadID", Integer.parseInt(pref.getLeadID())).equalTo("isDone", false)
                .findAllSorted("plannedActivitiesDateTime", Sort.DESCENDING));*/

        if(realm.where(PlannedActivities.class)
                .equalTo("leadID", Integer.parseInt(pref.getLeadID())).equalTo("isDone", false)
                .findAllSorted("plannedActivitiesDateTime", Sort.DESCENDING).isValid()) {
            plannedActivitiesList.addAll(realm.where(PlannedActivities.class)
                    .equalTo("leadID", Integer.parseInt(pref.getLeadID())).equalTo("isDone", false)
                    .findAllSorted("plannedActivitiesDateTime", Sort.DESCENDING));
        }

        if(plannedActivitiesList != null && plannedActivitiesList.size() <= 0){
            tvWaitForResponse.setText("  No Tasks found!!\nPlease plan a Task");
            tvWaitForResponse.setVisibility(View.VISIBLE);
        }

        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getLeadHistory(pref.getLeadID(), new Callback<LeadHistory>() {
            @Override
            public void success(LeadHistory leadHistory, Response response) {
                Util.updateHeaders(response.getHeaders());

                if(leadHistory.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }

                if (leadHistory.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    historyResponse.add(true);
                    if (leadHistoryResponseData != null && leadHistoryResponseData.size() > 0) {
                        leadHistoryResponseData.clear();
                    }
                    System.out.println("ACtionFragmentGSOn:- "+new Gson().toJson(leadHistory.getResult()).toString());
                    leadHistoryResponseData.addAll(leadHistory.getResult());

                    if (leadHistoryResponseData.size() > 0) {
                        tvWaitForResponse.setVisibility(View.GONE);
                    }
                    // Collections.reverse(leadHistoryResponseData);
                    mTimeLineAdapterOneRecycler.notifyDataSetChanged();

                }


            }

            @Override
            public void failure(RetrofitError error) {
                historyResponse.add(true);
                mTimeLineAdapterOneRecycler.notifyDataSetChanged();
                System.out.println("Retrofit Error : Action Fragment- " + pref.getLeadID());
            }
        });

        //HISTORY_RECEIVED = false;
        mTimeLineAdapterOneRecycler = new TimeLineOneRecyclerAdapter(getActivity(),plannedActivitiesList, leadHistoryResponseData,
                getContext(), connectionDetectorService.isConnectingToInternet(), historyResponse,this);
        mRecyclerView.setAdapter(mTimeLineAdapterOneRecycler);
    }

    @Override
    public void onChangeTabPosition(int i) {
        //Change tab position to Cars TAB
        if(C360Activity.viewPager!=null){
            C360Activity.viewPager.setCurrentItem(1);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("audioSeekBar 0 - onPause() ");
        mediaPlayerChecker = new TimeLineOneRecyclerAdapter();
        mediaPlayerChecker.onPlayerClose(true);
        TimeLineOneRecyclerAdapter.FRAGMENT_DESTROYED = true;
    }
}
