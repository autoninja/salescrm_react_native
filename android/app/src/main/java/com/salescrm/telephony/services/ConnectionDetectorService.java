package com.salescrm.telephony.services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Ravindra P on 16-03-2016.
 */
public class ConnectionDetectorService {

    private Context _context;

    public ConnectionDetectorService(Context context) {
        this._context = context;
    }

    public boolean isConnectingToInternet() {

        if(_context != null) {
            ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null)
                    for (int i = 0; i < info.length; i++)
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }

            }
        }
        return false;
    }
}