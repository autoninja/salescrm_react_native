package com.salescrm.telephony.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.adapter.AdditionalCarAdapter;
import com.salescrm.telephony.adapter.ExchangeCarsAdapter;
import com.salescrm.telephony.adapter.NewCarsRecyclerAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.car.AdditionalCarsDetails;
import com.salescrm.telephony.db.car.AllInterestedCars;
import com.salescrm.telephony.db.car.ExchangeCarDetails;
import com.salescrm.telephony.interfaces.NewCarsProgressLoader;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by bannhi on 13/6/17.
 */

public class NewCarsFragment extends Fragment implements NewCarsProgressLoader {

    private static RecyclerView mIntrestedCarsRecyclerView;
    private static RecyclerView mExchangeCarsRecyclerView;
    private static RecyclerView mAdditionalCarsRecyclerView;
    private NewCarsRecyclerAdapter newCarsRecyclerAdapter;
    private ExchangeCarsAdapter exchnExchangeCarsAdapter;
    private AdditionalCarAdapter additionalCarsAdapter;
    Preferences pref;
    // ProgressDialog progressDialog;
    SalesCRMRealmTable salesCRMRealmTable;
    Realm realm;
    private RealmList<AllInterestedCars> allInterestedCarsRealmList = new RealmList<>();
    private RealmList<ExchangeCarDetails> exchangeCarDetailsRealmList = new RealmList<>();
    private RealmList<AdditionalCarsDetails> additionalCarsDetailsRealmList = new RealmList<>();
    private static RelativeLayout new_car_frag_progress_loader;
    private TextView exchangeCarsTitleTxtView, newCarsTitleView, additionalCarsTitleView;
    private RelativeLayout no_cars;
    private static ScrollView scrollCarsFrag;
    private FormSubmissionInputData formSubmissionInputData = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SalesCRMApplication.getBus().register(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_cars_frag, container, false);
        mIntrestedCarsRecyclerView = (RecyclerView) view.findViewById(R.id.carsRecyclerView);
        mIntrestedCarsRecyclerView.setHasFixedSize(true);


        mExchangeCarsRecyclerView = (RecyclerView) view.findViewById(R.id.exchangeCarsRecyclerView);
        mExchangeCarsRecyclerView.setHasFixedSize(true);

        mAdditionalCarsRecyclerView = (RecyclerView) view.findViewById(R.id.additionalCarsRecyclerView);
        mAdditionalCarsRecyclerView.setHasFixedSize(true);

        new_car_frag_progress_loader = (RelativeLayout) view.findViewById(R.id.new_car_frag_progress_loader);
        scrollCarsFrag = (ScrollView) view.findViewById(R.id.scroll_cars_frag);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mIntrestedCarsRecyclerView.setLayoutManager(mLayoutManager);

        RecyclerView.LayoutManager mExchangeCarsLayoutManager = new LinearLayoutManager(getActivity());
        mExchangeCarsRecyclerView.setLayoutManager(mExchangeCarsLayoutManager);

        RecyclerView.LayoutManager mAdditionalCarsLayoutManager = new LinearLayoutManager(getActivity());
        mAdditionalCarsRecyclerView.setLayoutManager(mAdditionalCarsLayoutManager);

        exchangeCarsTitleTxtView = (TextView) view.findViewById(R.id.exchangeTitle);
        newCarsTitleView = (TextView) view.findViewById(R.id.newTitle);
        additionalCarsTitleView = (TextView) view.findViewById(R.id.additionalTitle);

        //no_cars = (RelativeLayout) view.findViewById(R.id.no_cars_lyt);

        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        int i = 0;
        String locationId = "0";
        String leadSrcId = "0";
        String leadLastUpdated = "";

        allInterestedCarsRealmList.clear();
        exchangeCarDetailsRealmList.clear();
        additionalCarsDetailsRealmList.clear();

        RealmResults<AllInterestedCars> allInterestedCarsRealmResults = realm.where(AllInterestedCars.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findAll();

        if (allInterestedCarsRealmResults.size() > 0) {
            newCarsTitleView.setVisibility(View.VISIBLE);
        } else {
            newCarsTitleView.setVisibility(View.GONE);
        }
        if (allInterestedCarsRealmResults != null) {
            for (int k = 0; k < allInterestedCarsRealmResults.size(); k++) {
                allInterestedCarsRealmList.add(allInterestedCarsRealmResults.get(k));
            }

        }
        RealmResults<ExchangeCarDetails> exchangeCarDetailsRealmResults = realm.where(ExchangeCarDetails.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findAll();

        if (exchangeCarDetailsRealmResults.size() > 0) {
            exchangeCarsTitleTxtView.setVisibility(View.VISIBLE);
        } else {
            exchangeCarsTitleTxtView.setVisibility(View.GONE);
        }

        if (exchangeCarDetailsRealmResults != null) {
            for (int j = 0; j < exchangeCarDetailsRealmResults.size(); j++) {
                exchangeCarDetailsRealmList.add(exchangeCarDetailsRealmResults.get(j));
            }

        } else {
            exchangeCarsTitleTxtView.setVisibility(View.GONE);
        }
      /*  if(exchangeCarDetailsRealmResults!=null && exchangeCarDetailsRealmResults.size()>0 && allInterestedCarsRealmResults!=null && allInterestedCarsRealmResults.size()>0){
            no_cars.setVisibility(View.GONE);
        }else{
            no_cars.setVisibility(View.VISIBLE);
        }*/

        RealmResults<AdditionalCarsDetails> additionalCarsDetailsRealmResults = realm.where(AdditionalCarsDetails.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findAll();

        if (additionalCarsDetailsRealmResults.size() > 0) {
            additionalCarsTitleView.setVisibility(View.VISIBLE);
        } else {
            additionalCarsTitleView.setVisibility(View.GONE);
        }

        if (additionalCarsDetailsRealmResults != null) {
            for (int j = 0; j < additionalCarsDetailsRealmResults.size(); j++) {
                additionalCarsDetailsRealmList.add(additionalCarsDetailsRealmResults.get(j));
            }

        } else {
            additionalCarsTitleView.setVisibility(View.GONE);
        }

        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findFirst();

        if (salesCRMRealmTable != null) {
            locationId = salesCRMRealmTable.getLeadLocationId();
            leadSrcId = salesCRMRealmTable.getLeadSourceId();
            leadLastUpdated = salesCRMRealmTable.getLeadLastUpdated();
        }
        if (getActivity() != null && getActivity().getIntent() != null && getActivity().getIntent().getStringExtra(WSConstants.C360_PREV_FORM_FOR_BOOK_CAR) != null) {
            formSubmissionInputData = new Gson().fromJson(getActivity().getIntent().getStringExtra(WSConstants.C360_PREV_FORM_FOR_BOOK_CAR),
                    FormSubmissionInputData.class);

        }


        newCarsRecyclerAdapter = new NewCarsRecyclerAdapter(getChildFragmentManager(), formSubmissionInputData,
                allInterestedCarsRealmList, locationId, leadSrcId,leadLastUpdated,pref.getLeadID(), getActivity(),isLeadActive());
        mIntrestedCarsRecyclerView.setAdapter(newCarsRecyclerAdapter);

        exchnExchangeCarsAdapter = new ExchangeCarsAdapter(exchangeCarDetailsRealmList, leadLastUpdated, getActivity());
        mExchangeCarsRecyclerView.setAdapter(exchnExchangeCarsAdapter);

        additionalCarsAdapter = new AdditionalCarAdapter(additionalCarsDetailsRealmList, getActivity());
        mAdditionalCarsRecyclerView.setAdapter(additionalCarsAdapter);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Subscribe
    public void callBack(Activity activity) {
        /*System.out.println("book car api is called 2");
        Intent filterActivity = new Intent(activity, C360Activity.class);
        filterActivity.putExtra(WSConstants.C360_TAB_POSITION,1);
        activity.startActivity(filterActivity);
        activity.finish();*/
    }

    public void progressButtonClicked() {
        scrollCarsFrag.setVisibility(View.GONE);
        new_car_frag_progress_loader.setVisibility(View.VISIBLE);
    }

    public static class C360ActivityKillerBroadcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("book car api is called 2");

            Bundle bundle = intent.getExtras();
            boolean api_response = bundle.getBoolean("API_RESPONSE");
            if(api_response) {
                Intent filterActivity = new Intent(context, C360Activity.class);
                filterActivity.putExtra(WSConstants.C360_TAB_POSITION, 1);
                filterActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(filterActivity);
            }else{
                new_car_frag_progress_loader.setVisibility(View.GONE);
                scrollCarsFrag.setVisibility(View.VISIBLE);
            }
        }
    }
    private boolean isLeadActive(){
        RealmResults<SalesCRMRealmTable> data = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID())).findAll();
        for(int i=0;i<data.size();i++){
            if(data.get(i).getIsLeadActive()==0){
                return false;
            }
        }
        return true;
    }

}
