package com.salescrm.telephony.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.salescrm.telephony.response.Error;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prateek on 13/7/17.
 */

public class EtvbrGMPojo {


    @SerializedName("message")
    @Expose
    private String message;

    //private List<Error> error;
    private Error error;

    @SerializedName("result")
    @Expose
    private Result result;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    @SerializedName("userId")
    @Expose
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }


    public class ResultSM {

        @SerializedName("message")
        @Expose
        private String message;

        //private List<Error> error;
        private Error error;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Integer getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(Integer statusCode) {
            this.statusCode = statusCode;
        }

        @SerializedName("abs_total")
        @Expose
        private AbsTotal absTotal;
        @SerializedName("rel_total")
        @Expose
        private RelTotal relTotal;

        /*@SerializedName("result")
        @Expose
        private Result result;

        public Result getResult() {
            return result;
        }

        public void setResult(Result result) {
            this.result = result;
        }*/

        @SerializedName("statusCode")
        @Expose
        private Integer statusCode;

        @SerializedName("sm_id")
        @Expose
        private Integer smId;
        @SerializedName("sm_username")
        @Expose
        private String smUsername;
        @SerializedName("sm_dp_url")
        @Expose
        private String smDpUrl;
        @SerializedName("teams")
        @Expose
        private List<TeamSM> teams = new ArrayList<>();

        public Integer getSmId() {
            return smId;
        }

        public void setSmId(Integer smId) {
            this.smId = smId;
        }

        public String getSmUsername() {
            return smUsername;
        }

        public void setSmUsername(String smUsername) {
            this.smUsername = smUsername;
        }

        public String getSmDpUrl() {
            return smDpUrl;
        }

        public void setSmDpUrl(String smDpUrl) {
            this.smDpUrl = smDpUrl;
        }

        public List<TeamSM> getTeams() {
            return teams;
        }

        public void setTeams(List<TeamSM> teams) {
            this.teams = teams;
        }

        public Error getError() {
            return error;
        }

        public void setError(Error error) {
            this.error = error;
        }

        public AbsTotal getAbsTotal() {
            return absTotal;
        }

        public void setAbsTotal(AbsTotal absTotal) {
            this.absTotal = absTotal;
        }

        public RelTotal getRelTotal() {
            return relTotal;
        }

        public void setRelTotal(RelTotal relTotal) {
            this.relTotal = relTotal;
        }
    }

    public class Result{
        @SerializedName("sales_manager")
        @Expose
        private List<ResultSM> salesManager = new ArrayList<>();

        public List<ResultSM> getSalesManager() {
            return salesManager;
        }

        public void setSalesManager(List<ResultSM> salesManager) {
            this.salesManager = salesManager;
        }

        @SerializedName("abs_total")
        @Expose
        private AbsTotal absTotal;
        @SerializedName("rel_total")
        @Expose
        private RelTotal relTotal;

        @SerializedName("teams")
        @Expose
        private List<TeamSM> teams = new ArrayList<>();

        public List<TeamSM> getTeams() {
            return teams;
        }

        public void setTeams(List<TeamSM> teams) {
            this.teams = teams;
        }

        public AbsTotal getAbsTotal() {
            return absTotal;
        }

        public void setAbsTotal(AbsTotal absTotal) {
            this.absTotal = absTotal;
        }

        public RelTotal getRelTotal() {
            return relTotal;
        }

        public void setRelTotal(RelTotal relTotal) {
            this.relTotal = relTotal;
        }
    }

    public class TeamSM {

        @SerializedName("message")
        @Expose
        private String message;

        private Error error;

        @SerializedName("statusCode")
        @Expose
        private Integer statusCode;

        @SerializedName("team_id")
        @Expose
        private Integer teamId;
        @SerializedName("team_name")
        @Expose
        private String teamName;
        @SerializedName("tl_dp_url")
        @Expose
        private String tlDpUrl;
        @SerializedName("abs")
        @Expose
        private List<Absolute> abs = new ArrayList<>();
        @SerializedName("rel")
        @Expose
        private List<Relational> rel = new ArrayList<>();
        @SerializedName("abs_total")
        @Expose
        private AbsTotal absTotal;
        @SerializedName("rel_total")
        @Expose
        private RelTotal relTotal;

        public Integer getTeamId() {
            return teamId;
        }

        public void setTeamId(Integer teamId) {
            this.teamId = teamId;
        }

        public String getTeamName() {
            return teamName;
        }

        public void setTeamName(String teamName) {
            this.teamName = teamName;
        }

        public String getTlDpUrl() {
            return tlDpUrl;
        }

        public void setTlDpUrl(String tlDpUrl) {
            this.tlDpUrl = tlDpUrl;
        }

        public List<Absolute> getAbs() {
            return abs;
        }

        public void setAbs(List<Absolute> abs) {
            this.abs = abs;
        }

        public List<Relational> getRel() {
            return rel;
        }

        public void setRel(List<Relational> rel) {
            this.rel = rel;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Integer getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(Integer statusCode) {
            this.statusCode = statusCode;
        }

        public Error getError() {
            return error;
        }

        public void setError(Error error) {
            this.error = error;
        }

        public AbsTotal getAbsTotal() {
            return absTotal;
        }

        public void setAbsTotal(AbsTotal absTotal) {
            this.absTotal = absTotal;
        }

        public RelTotal getRelTotal() {
            return relTotal;
        }

        public void setRelTotal(RelTotal relTotal) {
            this.relTotal = relTotal;
        }
    }

    public class Absolute {

        @SerializedName("location")
        @Expose
        private String location;
        @SerializedName("location_id")
        @Expose
        private Integer locationId;
        /*@SerializedName("Sales Manager")
        @Expose
        private String salesManager;*/
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("user_name")
        @Expose
        private String userName;
        @SerializedName("dp_url")
        @Expose
        private String dpUrl;
        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("enquiries_target")
        @Expose
        private Integer enquiries_target;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("tdrives_target")
        @Expose
        private Integer tdrives_target;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("visits_target")
        @Expose
        private Integer visits_target;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("bookings_target")
        @Expose
        private Integer bookings_target;
        @SerializedName("retail")
        @Expose
        private Integer retail;
        @SerializedName("retail_target")
        @Expose
        private Integer retail_target;
        @SerializedName("exchange_target")
        @Expose
        private Integer exchange_target;
        @SerializedName("finance_target")
        @Expose
        private Integer finance_target;

        public Integer getExchange_target() {
            return exchange_target;
        }

        public void setExchange_target(Integer exchange_target) {
            this.exchange_target = exchange_target;
        }

        public Integer getFinance_target() {
            return finance_target;
        }

        public void setFinance_target(Integer finance_target) {
            this.finance_target = finance_target;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public Integer getLocationId() {
            return locationId;
        }

        public void setLocationId(Integer locationId) {
            this.locationId = locationId;
        }

        /* public String getSalesManager() {
            return salesManager;
        }

        public void setSalesManager(String salesManager) {
            this.salesManager = salesManager;
        }*/

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getDpUrl() {
            return dpUrl;
        }

        public void setDpUrl(String dpUrl) {
            this.dpUrl = dpUrl;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetail() {
            return retail;
        }

        public void setRetail(Integer retail) {
            this.retail = retail;
        }

        public Integer getEnquiries_target() {
            return enquiries_target;
        }

        public void setEnquiries_target(Integer enquiries_target) {
            this.enquiries_target = enquiries_target;
        }

        public Integer getTdrives_target() {
            return tdrives_target;
        }

        public void setTdrives_target(Integer tdrives_target) {
            this.tdrives_target = tdrives_target;
        }

        public Integer getVisits_target() {
            return visits_target;
        }

        public void setVisits_target(Integer visits_target) {
            this.visits_target = visits_target;
        }

        public Integer getBookings_target() {
            return bookings_target;
        }

        public void setBookings_target(Integer bookings_target) {
            this.bookings_target = bookings_target;
        }

        public Integer getRetail_target() {
            return retail_target;
        }

        public void setRetail_target(Integer retail_target) {
            this.retail_target = retail_target;
        }
    }

    public class Relational {

        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("user_name")
        @Expose
        private String userName;
        @SerializedName("dp_url")
        @Expose
        private String dpUrl;
        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retail")
        @Expose
        private Integer retail;
        @SerializedName("location")
        @Expose
        private String location;
        /*@SerializedName("Sales Manager")
        @Expose
        private String salesManager;*/

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getDpUrl() {
            return dpUrl;
        }

        public void setDpUrl(String dpUrl) {
            this.dpUrl = dpUrl;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetail() {
            return retail;
        }

        public void setRetail(Integer retail) {
            this.retail = retail;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        /*public String getSalesManager() {
            return salesManager;
        }

        public void setSalesManager(String salesManager) {
            this.salesManager = salesManager;
        }*/

    }

    public class RelTotal {

        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retail")
        @Expose
        private Integer retail;

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetail() {
            return retail;
        }

        public void setRetail(Integer retail) {
            this.retail = retail;
        }
    }

    public class AbsTotal {

        @SerializedName("enquiries")
        @Expose
        private Integer enquiries;
        @SerializedName("tdrives")
        @Expose
        private Integer tdrives;
        @SerializedName("visits")
        @Expose
        private Integer visits;
        @SerializedName("bookings")
        @Expose
        private Integer bookings;
        @SerializedName("retail")
        @Expose
        private Integer retail;
        @SerializedName("enquiries_target")
        @Expose
        private Integer enquiriesTarget;
        @SerializedName("tdrives_target")
        @Expose
        private Integer tdrivesTarget;
        @SerializedName("visits_target")
        @Expose
        private Integer visitsTarget;
        @SerializedName("bookings_target")
        @Expose
        private Integer bookingsTarget;
        @SerializedName("retail_target")
        @Expose
        private Integer retailTarget;

        public Integer getEnquiriesTarget() {
            return enquiriesTarget;
        }

        public void setEnquiriesTarget(Integer enquiriesTarget) {
            this.enquiriesTarget = enquiriesTarget;
        }

        public Integer getTdrivesTarget() {
            return tdrivesTarget;
        }

        public void setTdrivesTarget(Integer tdrivesTarget) {
            this.tdrivesTarget = tdrivesTarget;
        }

        public Integer getVisitsTarget() {
            return visitsTarget;
        }

        public void setVisitsTarget(Integer visitsTarget) {
            this.visitsTarget = visitsTarget;
        }

        public Integer getBookingsTarget() {
            return bookingsTarget;
        }

        public void setBookingsTarget(Integer bookingsTarget) {
            this.bookingsTarget = bookingsTarget;
        }

        public Integer getRetailTarget() {
            return retailTarget;
        }

        public void setRetailTarget(Integer retailTarget) {
            this.retailTarget = retailTarget;
        }

        public Integer getEnquiries() {
            return enquiries;
        }

        public void setEnquiries(Integer enquiries) {
            this.enquiries = enquiries;
        }

        public Integer getTdrives() {
            return tdrives;
        }

        public void setTdrives(Integer tdrives) {
            this.tdrives = tdrives;
        }

        public Integer getVisits() {
            return visits;
        }

        public void setVisits(Integer visits) {
            this.visits = visits;
        }

        public Integer getBookings() {
            return bookings;
        }

        public void setBookings(Integer bookings) {
            this.bookings = bookings;
        }

        public Integer getRetail() {
            return retail;
        }

        public void setRetail(Integer retail) {
            this.retail = retail;
        }

    }
}
