package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AddSearchActivity;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.activity.offline.C360ActivityOffline;
import com.salescrm.telephony.dataitem.SectionModel;
import com.salescrm.telephony.db.ActivityDetails;
import com.salescrm.telephony.db.LeadDetails;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.create_lead.User_details;
import com.salescrm.telephony.db.crm_user.UsersDB;
import com.salescrm.telephony.interfaces.AdapterCommunication;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.SearchResponse;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by bharath on 9/8/16.
 */
public class AddSearchRealmAdapter extends RealmRecyclerViewAdapter<SalesCRMRealmTable, AddSearchRealmAdapter.ViewHolder> {

    private static AdapterCommunication mlistener;
    private Context mContext;
    private Preferences pref = null;
    // int size;
    private ViewGroup classParent;
    private List<SearchResponse.Result> searchResponse;

    public AddSearchRealmAdapter(AddSearchActivity context, OrderedRealmCollection<SalesCRMRealmTable> searchResponse) {
        super(context, searchResponse, true);
        //this.searchResponse = searchResponse;
        this.mContext = context;
        System.out.println("Called:++" + searchResponse.size());
    }


    @Override
    public AddSearchRealmAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = null;
        classParent = parent;
        pref = Preferences.getInstance();
        pref.load(parent.getContext());
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_card_layout, parent, false);
        return new ViewHolder(itemLayoutView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(AddSearchRealmAdapter.ViewHolder viewHolder, int position) {
        if (getData() != null && getData().get(position) != null) {
            final SalesCRMRealmTable salesCRMRealmTable = getData().get(position);
            viewHolder.searchResponse = salesCRMRealmTable;
            if (TextUtils.isEmpty(salesCRMRealmTable.getFirstName()) || TextUtils.isEmpty(salesCRMRealmTable.getFirstName())) {
                viewHolder.tvSearchName.setText("Customer Name");
            } else {
                viewHolder.tvSearchName.setText(String.format("%s %s", salesCRMRealmTable.getFirstName(), salesCRMRealmTable.getLastName()));

            }
            if(salesCRMRealmTable.isCreatedOffline()){
              //  holder.tvActionPlanMrMs.setText(Util.getGender(salesCRMRealmTable.getCreateLeadInputDataDB().getLead_data().getCustomer_details().getGender()));
                String name = salesCRMRealmTable.getCreateLeadInputDataDB().getLead_data().getCustomer_details().getFirst_name()
                        +" "+salesCRMRealmTable.getCreateLeadInputDataDB().getLead_data().getCustomer_details().getLast_name();
                if (Util.isNotNull(name)) {
                    viewHolder.tvSearchName.setText(String.format("%s%s", name.substring(0, 1).toUpperCase(), name.substring(1)));
                }
                User_details realmData = salesCRMRealmTable.getCreateLeadInputDataDB().getLead_data().getUser_details().where().equalTo("role_id", WSConstants.DSE_ROLE_ID).findFirst();
                if(realmData!=null){
                    viewHolder.tvSearchAssigned.setText(String.format("Assigned: %s",
                            Realm.getDefaultInstance().where(UsersDB.class).equalTo("id",realmData.getUser_id()).findFirst().getName()));

                }
                else {
                    viewHolder.tvSearchAssigned.setText("Assigned: Self");
                }

                setTagColorId(viewHolder,salesCRMRealmTable.getCreateLeadInputDataDB().getLead_data().getLead_tag_id());
                SimpleDateFormat dateFormatter = new SimpleDateFormat("E dd/MM", Locale.getDefault());
                if(salesCRMRealmTable.getCreateLeadInputDataDB().getActivity_data().getActivityName()!=null
                        &&salesCRMRealmTable.getCreateLeadInputDataDB().getActivity_data().getScheduledDateTime()!=null){
                    viewHolder.tvSearchFollowDate.setVisibility(View.VISIBLE);
                    viewHolder.viewStatusOval.setVisibility(View.VISIBLE);
                    viewHolder.tvSearchFollowDate.setText(String.format("%s  %s", salesCRMRealmTable.getCreateLeadInputDataDB().getActivity_data().getActivityName(),
                            dateFormatter.format(Util.getDate(salesCRMRealmTable.getCreateLeadInputDataDB().getActivity_data().getScheduledDateTime()))));}
                else {
                    viewHolder.tvSearchFollowDate.setVisibility(View.GONE);
                    viewHolder.viewStatusOval.setVisibility(View.GONE);
                }

            }
            else {
                if (salesCRMRealmTable.getLeadDseName() == null || salesCRMRealmTable.getLeadDseName().equalsIgnoreCase(pref.getDseName())) {
                    viewHolder.tvSearchAssigned.setText("Assigned: Self");
                } else {
                    viewHolder.tvSearchAssigned.setText(String.format("Assigned: %s", salesCRMRealmTable.getLeadDseName()));

                }
                setTagColor(viewHolder, salesCRMRealmTable.getLeadTagsName());

                SimpleDateFormat dateFormatter = new SimpleDateFormat("E dd/MM", Locale.getDefault());
                if (!salesCRMRealmTable.isDone() && !TextUtils.isEmpty(salesCRMRealmTable.getActivityName())) {
                    viewHolder.tvSearchFollowDate.setVisibility(View.VISIBLE);
                    viewHolder.viewStatusOval.setVisibility(View.VISIBLE);
                    viewHolder.tvSearchFollowDate.setText(String.format("%s  %s", salesCRMRealmTable.getActivityName(), dateFormatter.format(salesCRMRealmTable.getActivityScheduleDate().getTime())));
                } else {
                    viewHolder.tvSearchFollowDate.setVisibility(View.GONE);
                    viewHolder.viewStatusOval.setVisibility(View.GONE);
                }
            }
            viewHolder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!salesCRMRealmTable.isCreatedOffline()) {
                        pref.setLeadID("" + salesCRMRealmTable.getLeadId());
                        Intent filterActivity = new Intent(mContext, C360Activity.class);
                        filterActivity.putExtra("customerID", "" + salesCRMRealmTable.getCustomerId());
                        mContext.startActivity(filterActivity);
                    }
                    else {
                        Intent intent = new Intent(mContext, C360ActivityOffline.class);
                        intent.putExtra("scheduled_activity_id",salesCRMRealmTable.getScheduledActivityId());
                        mContext.startActivity(intent);
                    }

                }
            });


        }






       /* SearchResponse.Result result = searchResponse.get(position);
        viewHolder.tvSearchName.setText(String.format("%s %s", result.getFirstName(), result.getLastName()));

        //View Settings for Status
        if (result.getActive().equals(WSConstants.RESULT_STATUS_OPEN)) {
            viewHolder.viewStatusOval.setVisibility(View.VISIBLE);
            viewHolder.tvSearchFollowDate.setVisibility(View.VISIBLE);
            viewHolder.tvSearchAssigned.setText("Assigned: Self");
        } else if (result.getActive().equals(WSConstants.RESULT_STATUS_CLOSED)) {
            viewHolder.viewStatusOval.setVisibility(View.GONE);
            viewHolder.tvSearchFollowDate.setVisibility(View.GONE);
            viewHolder.tvSearchAssigned.setText("Assigned: Self (closed)");
        }*/

    }

    private void setTagColorId(ViewHolder viewHolder, String tagID) {
        // System.out.println("inter" + indicatorPoint);
        if (tagID.equalsIgnoreCase(WSConstants.LEAD_TAG_HOT_ID)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.hot));
        } else if (tagID.equalsIgnoreCase(WSConstants.LEAD_TAG_WARM_ID)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.warm));
        } else if (tagID.equalsIgnoreCase(WSConstants.LEAD_TAG_COLD_ID)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cold));
        } else if (tagID.equalsIgnoreCase(WSConstants.LEAD_TAG_OVERDUE_ID)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(context, R.color.overdue));

        }
    }

    private void setTagColor(ViewHolder viewHolder, String tagName) {
        if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_HOT)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.hot));
        } else if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_WARM)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.warm));
        } else if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_COLD)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.cold));
        } else if (tagName.equalsIgnoreCase(WSConstants.LEAD_TAG_OVERDUE)) {
            viewHolder.cardTag.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.overdue));

        }
    }

    // inner class to hold a reference to each item of RecyclerView
    public class ViewHolder extends RecyclerView.ViewHolder {
        public View view;
        TextView tvSearchName;
        ImageView viewStatusOval;
        TextView tvSearchFollowDate;
        TextView tvSearchAssigned;
        SalesCRMRealmTable searchResponse;
        CardView cardTag;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            view = itemLayoutView;
            tvSearchName = (TextView) view.findViewById(R.id.tv_search_name);
            viewStatusOval = (ImageView) view.findViewById(R.id.iV_search);
            tvSearchFollowDate = (TextView) view.findViewById(R.id.tv_search_follow_date);
            tvSearchAssigned = (TextView) view.findViewById(R.id.tv_search_assigned);
            cardTag = (CardView) view.findViewById(R.id.cardview_search);


        }


    }
}
