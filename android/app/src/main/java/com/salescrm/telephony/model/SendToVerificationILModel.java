package com.salescrm.telephony.model;

/**
 * Created by bharath on 15/4/20.
 */

public class SendToVerificationILModel {
    private String lead_id;
    private String lead_last_updated;
    private FormSubmissionInputData form_object;
    private String proposal_id;
    private String lang_code;

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }

    public FormSubmissionInputData getForm_object() {
        return form_object;
    }

    public void setForm_object(FormSubmissionInputData form_object) {
        this.form_object = form_object;
    }

    public String getProposal_id() {
        return proposal_id;
    }

    public void setProposal_id(String proposal_id) {
        this.proposal_id = proposal_id;
    }

    public String getLang_code() {
        return lang_code;
    }

    public void setLang_code(String lang_code) {
        this.lang_code = lang_code;
    }
}
