package com.salescrm.telephony.offline;

import android.content.Context;
import android.text.TextUtils;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.AllotDseDB;
import com.salescrm.telephony.db.CustomerPhoneNumbers;
import com.salescrm.telephony.db.FormAnswerDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;
import com.salescrm.telephony.interfaces.FetchDseTasksListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.ActionPlanResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bharath on 6/12/16.
 * Call this to get the action_plan(tasks)
 */
public class FetchDseTasks implements Callback<ActionPlanResponse> {
    public  FetchDseTasksListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private SalesCRMRealmTable salesCRMRealmTable;
    private int callCount;
    private List<ActionPlanResponse.Result.Data> allActionPlanData;
    private Integer dseId;

    public FetchDseTasks(FetchDseTasksListener listener, Context context, Integer dseId) {
        this.listener = listener;
        this.context = context;
        this.callCount = 0;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
        allActionPlanData = new ArrayList<>();
        this.dseId = dseId;
        RealmResults<SalesCRMRealmTable> deleteInvalidData = realm.where(SalesCRMRealmTable.class).equalTo("isCreatedTemporary", true).findAll();
        if(deleteInvalidData.size()>0){
            realm.beginTransaction();
            deleteInvalidData.deleteAllFromRealm();
            realm.commitTransaction();
        }
    }

    public void call(Context data) {
        startActionPlanApi();
    }

    private void startActionPlanApi(){
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            getActionPlanData();
        } else {
            System.out.println("No Internet Connection");
        }
    }

    private void getActionPlanData() {
        String day = "";
        int isLeadTaskDone = 0;
        String order = WSConstants.ORDER_ASC;
        switch (callCount) {
            case 0:
                day = WSConstants.TODAY;
                break;
            case 1:
                day = WSConstants.TOMORROW;
                break;
            case 2:
                day = WSConstants.PENDING;
                order = WSConstants.ORDER_DESC;
                break;
            case 3:
                day = WSConstants.FUTURE;
                break;

            case 4:
                day = WSConstants.TODAY;
                isLeadTaskDone = 1;
                break;

        }
        if (!TextUtils.isEmpty(day)) {
            System.out.println("ActionPlanRequested :: "+day);
            ++callCount;
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken())
                    .GetActionPlanData(
                            0,
                            1000,
                            true,
                            true,
                            0,
                            null,
                            null,
                            "normal",
                            day,
                            null,
                            order,
                            dseId,
                            isLeadTaskDone,
                            this);
        } else {
            listener.onFetchDseTaskSuccess(allActionPlanData);
        }

    }


    @Override
    public void success(final ActionPlanResponse data, Response response) {
        List<Header> headerList = response.getHeaders();
        for (Header header : headerList) {
            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                ApiUtil.UpdateAccessToken(header.getValue());
            }
        }
        if (!data.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + data.getMessage());
            listener.onFetchDseTaskError(null,WSConstants.OfflineAPIRequest.ACTION_PLAN);
            //showAlert(validateOtpResponse.getMessage());
        } else {
            if (data.getResult() != null) {
                System.out.println("Success:1" + data.getMessage());
                allActionPlanData.addAll(data.getResult().getData());
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm1) {
                        insertData(realm1, data.getResult().getData());

                    }
                });

                getActionPlanData();
            } else {
                // relLoader.setVisibility(View.GONE);

                System.out.println("Success:2" + data.getMessage());
                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }

    @Override
    public void failure(RetrofitError error) {
        listener.onFetchDseTaskError(error,WSConstants.OfflineAPIRequest.ACTION_PLAN);

    }

    private void insertData(Realm realm, List<ActionPlanResponse.Result.Data> actionPlanResponseData) {
        salesCRMRealmTable = new SalesCRMRealmTable();

        for (int i = 0; i < actionPlanResponseData.size(); i++) {
            salesCRMRealmTable.setTemplateExist(actionPlanResponseData.get(i).getTemplate_exist());
            salesCRMRealmTable.setScheduledActivityId(Integer.parseInt(actionPlanResponseData.get(i).getActivity().getScheduled_activity_id()));
            salesCRMRealmTable.setCustomerId(Integer.parseInt(actionPlanResponseData.get(i).getCustomer().getCustomer_id()));
            salesCRMRealmTable.setCustomerNumber(actionPlanResponseData.get(i).getCustomer().getMobile_number());
            salesCRMRealmTable.setCustomerName(actionPlanResponseData.get(i).getCustomer().getName());
            salesCRMRealmTable.setCustomerEmail(actionPlanResponseData.get(i).getCustomer().getEmail());
            salesCRMRealmTable.setFirstName(actionPlanResponseData.get(i).getCustomer().getFirst_name());
            salesCRMRealmTable.setLastName(actionPlanResponseData.get(i).getCustomer().getLast_name());
            salesCRMRealmTable.setGender(actionPlanResponseData.get(i).getCustomer().getGender());
            salesCRMRealmTable.setTitle(actionPlanResponseData.get(i).getCustomer().getTitle());
            String customerAge = actionPlanResponseData.get(i).getCustomer().getAge();
            salesCRMRealmTable.setCustomerAge(customerAge.equals("") ? null : customerAge);
            salesCRMRealmTable.setResidenceAddress(actionPlanResponseData.get(i).getCustomer().getCustomer_address());
            salesCRMRealmTable.setResidencePinCode(actionPlanResponseData.get(i).getCustomer().getResidence_pin_code());
            salesCRMRealmTable.setOfficeAddress(actionPlanResponseData.get(i).getCustomer().getOffice_address());
            salesCRMRealmTable.setOfficePinCode(actionPlanResponseData.get(i).getCustomer().getOffice_pin_code());
//            System.out.println("Fetch action plan Company Before"+salesCRMRealmTable.getLeadId()+", "+ salesCRMRealmTable.getCompanyName());
            salesCRMRealmTable.setCompanyName(actionPlanResponseData.get(i).getCustomer().getCompany_name());
           // System.out.println("Fetch action plan Company After"+salesCRMRealmTable.getLeadId()+", "+ salesCRMRealmTable.getCompanyName());
            salesCRMRealmTable.setLeadId(Integer.parseInt(actionPlanResponseData.get(i).getLead().getLead_id()));
            salesCRMRealmTable.setLeadCarVariantName(actionPlanResponseData.get(i).getLead_car().getVariant_name());
            salesCRMRealmTable.setLeadCarModelName(actionPlanResponseData.get(i).getLead_car().getModel_name());
            salesCRMRealmTable.setLeadCarModelId(actionPlanResponseData.get(i).getLead_car().getModel_id());
            salesCRMRealmTable.setLeadTagsName(actionPlanResponseData.get(i).getLead().getLead_tags().getTag_names());
            salesCRMRealmTable.setLeadTagsColor(actionPlanResponseData.get(i).getLead().getLead_tags().getTag_colors());
            salesCRMRealmTable.setLeadDseName(actionPlanResponseData.get(i).getLead().getDseAlloted().getDseName());
            salesCRMRealmTable.setLeadDsemobileNumber(actionPlanResponseData.get(i).getLead().getDseAlloted().getDseMobNo());
            salesCRMRealmTable.setLeadStageId(actionPlanResponseData.get(i).getLead().getLead_stage().getId());
            salesCRMRealmTable.setLeadStage(actionPlanResponseData.get(i).getLead().getLead_stage().getStage());
            salesCRMRealmTable.setLeadAge(actionPlanResponseData.get(i).getLead().getLead_age());
            salesCRMRealmTable.setLeadSourceName(actionPlanResponseData.get(i).getLead().getLead_source_name());
            salesCRMRealmTable.setVinRegNo(actionPlanResponseData.get(i).getLead().getRef_vin_reg_no());
            salesCRMRealmTable.setLeadSourceId(actionPlanResponseData.get(i).getLead().getLead_source_id());
            salesCRMRealmTable.setLeadLastUpdated(actionPlanResponseData.get(i).getLead().getLead_last_updated());
            salesCRMRealmTable.setLeadExpectedClosingDate(actionPlanResponseData.get(i).getLead().getExpected_closing_date());
            salesCRMRealmTable.setActivityId(Integer.parseInt(actionPlanResponseData.get(i).getActivity().getActivity_id()));
            salesCRMRealmTable.setActivityDescription(actionPlanResponseData.get(i).getActivity().getActivity_description());
            salesCRMRealmTable.setActivityCreationDate(Util.getDate(actionPlanResponseData.get(i).getActivity().getActivity_creation_date()));
            salesCRMRealmTable.setActivityScheduleDate(Util.getNormalDate(actionPlanResponseData.get(i).getActivity().getActivity_scheduled_date()));
            salesCRMRealmTable.setActivityTypeId(actionPlanResponseData.get(i).getActivity().getActivity_type_id());
            salesCRMRealmTable.setActivityType(actionPlanResponseData.get(i).getActivity().getType());
            salesCRMRealmTable.setActivityName(actionPlanResponseData.get(i).getActivity().getActivity_name());
            salesCRMRealmTable.setActivityGroupId(actionPlanResponseData.get(i).getActivity().getActivity_group_id());
            salesCRMRealmTable.setActivityIconType(actionPlanResponseData.get(i).getActivity().getIcon_type());
            salesCRMRealmTable.setIsLeadActive(1);
            salesCRMRealmTable.setDseId(dseId);
            salesCRMRealmTable.setCreatedTemporary(true);
            salesCRMRealmTable.setCustomerAddress(actionPlanResponseData.get(i).getCustomer().getCustomer_address());
            if(actionPlanResponseData.get(i).getCustomer().getSecondary_mobile_number() != null) {
                salesCRMRealmTable.setSecondaryMobileNumber(Long.parseLong(actionPlanResponseData.get(i).getCustomer().getSecondary_mobile_number()));
            }
            salesCRMRealmTable.setLeadLocationName(actionPlanResponseData.get(i).getLead().getLocation_name());
            if(actionPlanResponseData.get(i).getLead().getTaskStatus()!=null){
                //1,5,6,7
                if(actionPlanResponseData.get(i).getLead().getTaskStatus() ==1
                        || actionPlanResponseData.get(i).getLead().getTaskStatus() ==5
                        || actionPlanResponseData.get(i).getLead().getTaskStatus() ==6
                        || actionPlanResponseData.get(i).getLead().getTaskStatus() ==7 ) {
                    salesCRMRealmTable.setDone(false);
                }
                else {
                    salesCRMRealmTable.setDone(true);
                }
            }
            else {
                salesCRMRealmTable.setDone(true);
            }



            //Storing lead's all Mobile numbers
            RealmList<CustomerPhoneNumbers> customerPhoneNumbers = new RealmList<CustomerPhoneNumbers>();
            /* CustomerPhoneNumbers customerPhoneNumbers = new CustomerPhoneNumbers();*/
            if (actionPlanResponseData.get(i).getCustomer().getMobile_nos() != null) {
                for (int j = 0; j < actionPlanResponseData.get(i).getCustomer().getMobile_nos().size(); i++) {
                    CustomerPhoneNumbers customerPhoneNumbersObj = new CustomerPhoneNumbers();
                    customerPhoneNumbersObj.setLeadId(Integer.parseInt(actionPlanResponseData.get(i).getLead().getLead_id()));
                    customerPhoneNumbersObj.setCustomerID(Integer.parseInt(actionPlanResponseData.get(i).getCustomer().getCustomer_id()));
                    customerPhoneNumbersObj.setPhoneNumber(Long.parseLong(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getNumber()));
                    customerPhoneNumbersObj.setPhoneNumberID(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getId());
                    customerPhoneNumbersObj.setPhoneNumberStatus(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getStatus());
                    customerPhoneNumbersObj.setLead_phone_mapping_id(actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getLead_phone_mapping_id());
                    System.out.println("PHONE NUMBER ADDED In FetchDseTasks "+actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getNumber()+" id: "+actionPlanResponseData.get(i).getCustomer().getMobile_nos().get(i).getLead_phone_mapping_id());

                    customerPhoneNumbers.add(customerPhoneNumbersObj);
                }
            }
            //storing Mobile numbers in db
            salesCRMRealmTable.customerPhoneNumbers.addAll(customerPhoneNumbers);

            realm.copyToRealmOrUpdate(salesCRMRealmTable);

        }

    }

}
