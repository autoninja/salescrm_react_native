package com.salescrm.telephony.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bharath on 16/9/16.
 */
public class LeadTeamUserResponse {

    private String message;

    private String statusCode;

    private Result result;

    private Error error;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", statusCode = " + statusCode + ", result = " + result + ", error = " + error + "]";
    }

    public class Result implements Serializable {
        private int locationId;
        private List<Team_data> team_data;

        private List<Users> users;

        public List<Team_data> getTeam_data() {
            return team_data;
        }

        public void setTeam_data(List<Team_data> team_data) {
            this.team_data = team_data;
        }

        public List<Users> getUsers() {
            return users;
        }

        public void setUsers(List<Users> users) {
            this.users = users;
        }
        public int getLocationId() {
            return locationId;
        }

        public void setLocationId(int locationId) {
            this.locationId = locationId;
        }


        @Override
        public String toString() {
            return "ClassPojo [team_data = " + team_data + ", users = " + users + "]";
        }

        public class Users implements Serializable{
            private String id;

            private String mobile_number;

            private String name;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getMobile_number() {
                return mobile_number;
            }

            public void setMobile_number(String mobile_number) {
                this.mobile_number = mobile_number;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            @Override
            public String toString() {
                return "ClassPojo [id = " + id + ", mobile_number = " + mobile_number + ", name = " + name + "]";
            }

        }

        public class Team_data implements Serializable {
            private String team_leader_id;

            private String manager_id;

            private String user_id;

            public String getTeam_leader_id() {
                return team_leader_id;
            }

            public void setTeam_leader_id(String team_leader_id) {
                this.team_leader_id = team_leader_id;
            }

            public String getManager_id() {
                return manager_id;
            }

            public void setManager_id(String manager_id) {
                this.manager_id = manager_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            @Override
            public String toString() {
                return "ClassPojo [team_leader_id = " + team_leader_id + ", manager_id = " + manager_id + ", user_id = " + user_id + "]";
            }
        }
    }
}


