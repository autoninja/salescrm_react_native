package com.salescrm.telephony.telephonyModule;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Window;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.response.GCMDialogResponse;
import com.squareup.otto.Subscribe;

//import com.ninjacrm.telephony.db.DBHelper;

public class GcmDialog extends Activity {

   // private static DBHelper dBHelper;
    private static String calledNumber, extrasNumber;
    private Handler handler = new Handler(Looper.getMainLooper());
    static Context context = null;
    static AlertDialog alert;
    static AlertDialog.Builder dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        context = this;
        dialog = new AlertDialog.Builder(context);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            extrasNumber = extras.getString("mobile");
        }
        if (extrasNumber.startsWith("0")) {
            calledNumber = extrasNumber.substring(1);
        }
        else if (extrasNumber.contains("+91")) {
            calledNumber = extrasNumber.substring(3);
        }
        System.out.println("NUMBER TO SHOW"+calledNumber);
        SalesCRMApplication.getBus().register(this);
        //TelephonyDbHandler.getInstance().getLeadFromMobileNumber(calledNumber,context);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private class ShowDialog extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... voids) {

            return null;
        }

        protected void onProgressUpdate(Void mvoid) {
        }

        protected void onPostExecute(Void mvoid) {
        }
    }

    void showMesage(final String messageToShow) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.d("Dialog Started", "yes");

                dialog.setTitle("Call Details").setMessage(messageToShow);
                dialog.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                                GcmDialog.this.finish();
                            }
                        });
                alert = dialog.create();
                if(!messageToShow.equalsIgnoreCase("") && !((Activity) context).isFinishing()){
                    alert.show();
                }


                new CountDownTimer(3000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                    }

                    @Override
                    public void onFinish() {
                        if ((alert != null) && alert.isShowing())
                            alert.dismiss();
                        finish();
                    }
                }.start();
            }
        });
    }

    @Subscribe
    public void onSuccessFromDB(GCMDialogResponse obj) {
        showMesage(obj.getDialog());
    }
}
