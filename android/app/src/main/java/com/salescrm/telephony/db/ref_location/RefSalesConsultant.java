package com.salescrm.telephony.db.ref_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by prateek on 8/11/17.
 */

public class RefSalesConsultant extends RealmObject {

    @SerializedName("info")
    @Expose
    private RefInfo info;

    public RefInfo getInfo() {
        return info;
    }

    public void setInfo(RefInfo info) {
        this.info = info;
    }
}
