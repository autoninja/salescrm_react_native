package com.salescrm.telephony.db.car;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 22/3/18.
 */

public class BookingDiscountDB extends RealmObject {
    @PrimaryKey
    private String bookingId;
    private String corporate;
    private String exchangeBonus;
    private String loyaltyBonus;
    private String oemScheme;
    private String dealer;
    private String accessories;

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getCorporate() {
        return corporate;
    }

    public void setCorporate(String corporate) {
        this.corporate = corporate;
    }

    public String getExchangeBonus() {
        return exchangeBonus;
    }

    public void setExchangeBonus(String exchangeBonus) {
        this.exchangeBonus = exchangeBonus;
    }

    public String getLoyaltyBonus() {
        return loyaltyBonus;
    }

    public void setLoyaltyBonus(String loyaltyBonus) {
        this.loyaltyBonus = loyaltyBonus;
    }

    public String getOemScheme() {
        return oemScheme;
    }

    public void setOemScheme(String oemScheme) {
        this.oemScheme = oemScheme;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public String getAccessories() {
        return accessories;
    }

    public void setAccessories(String accessories) {
        this.accessories = accessories;
    }
}
