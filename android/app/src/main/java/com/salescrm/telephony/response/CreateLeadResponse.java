package com.salescrm.telephony.response;

/**
 * Created by bharath on 19/9/16.
 */
public class CreateLeadResponse {
    private String statusCode;

    private String message;

    private String result;
    private Integer scheduled_activity_id;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getResult ()
    {
        return result;
    }

    public void setResult (String result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    public Integer getScheduled_activity_id() {
        return scheduled_activity_id;
    }

    public void setScheduled_activity_id(Integer scheduled_activity_id) {
        this.scheduled_activity_id = scheduled_activity_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }
}
