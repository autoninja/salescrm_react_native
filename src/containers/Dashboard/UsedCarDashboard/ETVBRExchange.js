import React, { Component } from 'react'
import { Platform, View, Text, Image, TouchableOpacity, YellowBox, FlatList, ActivityIndicator } from 'react-native'
import PercentageSwitch from '../../../components/uielements/PercentageSwitch'
import Toast from 'react-native-easy-toast'
import styles from '../../../styles/styles'
import ETVBRHeaderExchange from '../../../components/etvbrl/etvbr_exchange_header'
import ETVBRExchangeItem from '../../../components/etvbrl/etvbr_exchange_item'

import RestClient from '../../../network/rest_client'
import { connect } from 'react-redux';
import ETVBRExchangeResultItem from '../../../components/etvbrl/etvbr_exchange_results_items';
import ETVBRExchangeItemChild from '../../../components/etvbrl/etvbr_exchange_item_child';

export default class ETVBRExchange extends Component {

    constructor(props) {

        super(props);

        var endDate = new Date();
        var startDate = new Date();
        startDate.setDate(1);

        this.state = {
            isApiCalledAtleastOnce: false, showPercentage: false, isLoading: true, result: {}, carModelResult: {}, dateRangeVisibilty: false,
            dateRange: { startDate, endDate }, bottomItem: { visibility: true, selected: 1 }, show_car_percentage: false, location_selected: 0,
            initDate: { startDate: global_filter_date.startDateStr, endDate: global_filter_date.endDateStr }
        }

        YellowBox.ignoreWarnings([
            'Warning: componentWillMount is deprecated',
            'Warning: componentWillReceiveProps is deprecated',
        ]);

    }

    componentDidMount() {

        this.getETVBRData();
        this._mounted = true;

    }

    getETVBRData() {
        console.log("getETVBRDataCalled");
        try {
            new RestClient().getETVBRUsedCarExchange({
                start_date: global.global_filter_date.startDateStr,
                end_date: global.global_filter_date.endDateStr
                //filters: encodeURIComponent(JSON.stringify(global.global_etvbr_filters_selected))
            })
                .then((data) => {
                    if (!this._mounted) {
                        return;
                    }
                    //console.log("getETVBRDataCalled Inner", data);
                    //console.log("Datat", da);
                    //console.log("DatatDatat", JSON.stringify(data));
                    let showBottom = false;
                    if (data.result.locations.length == 1) {
                        showBottom = true;
                        // if (!this.state.isApiCalledAtleastOnce) {
                        //     this.pushCleverTapEvent({ 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrTeams });
                        // }

                    }
                    else {
                        showBottom = false;
                        // if (!this.state.isApiCalledAtleastOnce) {
                        //     this.pushCleverTapEvent({ 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrLocation });
                        // }
                    }
                    this.setState({ isApiCalledAtleastOnce: true, isLoading: false, result: data.result, bottomItem: { visibility: showBottom, selected: 1 } })

                }).catch(error => {
                    console.error(error);
                    if (!this._mounted) {
                        return;
                    }
                    this.setState({ isLoading: false });
                    if (!error.status) {
                        this.refs.toast.show('Connection Error');
                    }
                });
        } catch (e) {
            this.refs.toast.show('Network Error');
        }
    }

    _showDetailsOnLocation(navigation, title, location, from, parent_role_id, location_id, location_selected) {
        let showBottom = false;
        console.log('parent_role_id' + parent_role_id);
        if (this.state.result && this.state.result.locations.length > 1) {
            showBottom = true;
        }

        navigation.navigate('ETVBRLExchangeChild', {
            title,
            location,
            from,
            showBottom,
            parent_role_id,
            location_id,
            location_selected
        });


        console.log("_showDetailsOnLocation");
    }

    _showDetailsOnEvaluatorManager(navigation, title, evaluation_manager, from, location_id, parent_role_id) {
        console.log('parent_role_id' + parent_role_id);
        let showBottom = false;
        if (this.state.result && this.state.result.locations.length > 1) {
            showBottom = true;
        }
        navigation.navigate('ETVBRLExchangeChild', {
            title,
            evaluation_manager,
            from,
            showBottom,
            location_id,
        });
        console.log("_showDetailsOnEvaluatorManager");
    }

    _showDateRangePicker(navigation) {
        navigation.navigate('DateRangePickerDialog', {
            initDate: this.state.dateRange, onFilterApply: (s, e) => {

                if (s && e) {
                    start = s.split('-');
                    end = e.split('-')

                    localDateRange = {};

                    localStartDate = new Date();
                    localStartDate.setDate(start[2]);
                    localStartDate.setMonth(start[1] - 1);
                    localStartDate.setYear(start[0]);
                    localDateRange.startDate = localStartDate;

                    localEndDate = new Date();
                    localEndDate.setDate(end[2]);
                    localEndDate.setMonth(end[1] - 1);
                    localEndDate.setYear(end[0]);
                    localDateRange.endDate = localEndDate;

                    endDate = new Date();

                    console.log("showDateRangePickerrr" + s + ':::' + e);
                    this.setState({ dateRange: localDateRange, initDate: { startDate: global_filter_date.startDateStr, endDate: global_filter_date.endDateStr } })
                    this._onRefresh();
                    //CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrDate });

                }

            }
        })

        console.log("showDateRangePickerrr");
    }

    _onRefresh(itemSelected, clickedEvent) {
        // if(clickedEvent) {
        // if(itemSelected == 1 ) {
        //   CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type' : eventTeamDashboard.keyType.valuesType.etvbrTeams });

        // }
        // else if (itemSelected == 2) {
        //   CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type' : eventTeamDashboard.keyType.valuesType.etvbrCars });

        // }
        // }
        let selectedItem = 1;
        if (itemSelected) {
            selectedItem = itemSelected;
        }
        this.setState({ isLoading: true, bottomItem: { selected: selectedItem }, initDate: { startDate: global_filter_date.startDateStr, endDate: global_filter_date.endDateStr } }, function () { this.getETVBRData() });
    }

    _etvbrLongPress(navigation, from, data) {
        console.log("datatada", data)
        if (from === 'user_info') {
            navigation.navigate('UserInfo', {
                name: data.info.name,
                dp_url: data.info.dp_url

            });
        }
        else if (from == 'etvbr_details') {
            navigation.navigate('ETVBRDetails', {
                data, isExchange: true
            });
        }

        console.log("_etvbrLongPress" + from);
        console.log(data.info.title);
    }

    render() {

        if (this.state.isLoading) {
            return (
                <View style={{
                    display: (this.state.isLoading ? 'flex' : 'none'), flex: 1, justifyContent: 'center',
                    alignItems: 'center', backgroundColor: '#fff'
                }}>

                    <ActivityIndicator
                        animating={this.state.isLoading}
                        style={{ flex: 1, alignSelf: 'center', }}
                        color="#2e5e86"
                        size="small"
                        hidesWhenStopped={true}

                    />

                    {/* <NavigationEvents
          onWillFocus={payload => {
            this.updateOnFocusChange();
          }}
        /> */}

                </View>

            )
        }
        let flat_list;
        let { result } = this.state;
        if (result
            && result.locations
            && result.locations.length > 0) {
            //console.log('result.locations is >0');
            if (result.locations.length > 1) {
                //console.log('result.locations is >1');
                flat_list =

                    <FlatList
                        showsVerticalScrollIndicator={false}
                        extraData={this.state}
                        data={result.locations}
                        renderItem={({ item, index }) => {

                            let mainView = <TouchableOpacity onPress={this._showDetailsOnLocation.bind(this, this.props.navigation, item.name, item, 'location', result.role_id, item.id, index)}>
                                {//<Text style={{ color:'#2e5e86', fontSize:15, marginTop:10}}> {item.location_name} </Text>
                                }

                                <ETVBRExchangeItem
                                    onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                                    onPress={this._showDetailsOnLocation.bind(this, this.props.navigation, item.name, item, 'location', result.role_id, item.id, index)}
                                    from_location={true} showPercentage={this.state.showPercentage}
                                    abs={
                                        {
                                            e: item.location_abs.enquiries,
                                            ed: item.location_abs.evaluations,
                                            pq: item.location_abs.price_negotiations,
                                            r: item.location_abs.retails,
                                        }
                                    }

                                    rel={
                                        {
                                            e: item.location_rel.enquiries,
                                            ed: item.location_rel.evaluations,
                                            pq: item.location_rel.price_negotiations,
                                            r: item.location_rel.retails,
                                        }
                                    }

                                    info={
                                        {
                                            user_id: global.appUserId,
                                            name: item.name,
                                            location_id: item.id,
                                            user_role_id: result.role_id,
                                        }
                                    }
                                >

                                </ETVBRExchangeItem>
                            </TouchableOpacity>;

                            if (index == result.locations.length - 1) {
                                return (<View>
                                    {mainView}
                                    <ETVBRExchangeResultItem

                                        onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}

                                        showPercentage={this.state.showPercentage} result={result}

                                        abs={
                                            {
                                                e: result.total_abs.enquiries,
                                                ed: result.total_abs.evaluations,
                                                pq: result.total_abs.price_negotiations,
                                                r: result.total_abs.retails,
                                            }
                                        }
                                        rel={
                                            {
                                                e: result.total_rel.enquiries,
                                                ed: result.total_rel.evaluations,
                                                pq: result.total_rel.price_negotiations,
                                                r: result.total_rel.retails,
                                            }
                                        }

                                        info={
                                            {
                                                user_id: global.appUserId,
                                                name: 'All Location',
                                                location_id: '',
                                                user_role_id: result.role_id,
                                            }
                                        }

                                    ></ETVBRExchangeResultItem>
                                </View>);
                            }
                            else {
                                return (mainView);
                            }


                        }}
                        keyExtractor={(item, index) => index + ''}
                        onRefresh={() => this._onRefresh()}
                        refreshing={this.state.isLoading}
                    />
            } else if (result.locations[0].evaluation_managers.length > 1) {
                let evaluation_managers = result.locations[0].evaluation_managers;
                let location = result.locations[0];
                flat_list = <FlatList
                    showsVerticalScrollIndicator={false}
                    extraData={this.state}
                    data={evaluation_managers}
                    renderItem={({ item, index }) => {

                        let mainView = <TouchableOpacity onPress={this._showDetailsOnEvaluatorManager.bind(this, this.props.navigation, item.name, item, 'evaluation_manager', location.id, item.user_role_id)}>
                            <ETVBRExchangeItem
                                onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                                onPress={this._showDetailsOnEvaluatorManager.bind(this, this.props.navigation, item.name, item, 'evaluation_manager', location.id)}
                                showPercentage={this.state.showPercentage}
                                abs={
                                    {
                                        e: item.evaluation_managers_abs.enquiries,
                                        ed: item.evaluation_managers_abs.evaluations,
                                        pq: item.evaluation_managers_abs.price_negotiations,
                                        r: item.evaluation_managers_abs.retails,
                                    }
                                }

                                rel={
                                    {
                                        e: item.evaluation_manager_rel.enquiries,
                                        ed: item.evaluation_manager_rel.evaluations,
                                        pq: item.evaluation_manager_rel.price_negotiations,
                                        r: item.evaluation_manager_rel.retails,
                                    }
                                }


                                info={
                                    {
                                        dp_url: item.dp_url,
                                        name: item.name,
                                        user_id: item.id,
                                        location_id: location.id,
                                        user_role_id: item.user_role_id,
                                    }
                                }


                            ></ETVBRExchangeItem>
                        </TouchableOpacity>;

                        if (index == evaluation_managers.length - 1) {
                            return (<View>
                                {mainView}

                                <ETVBRExchangeResultItem
                                    showPercentage={this.state.showPercentage}
                                    onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                                    abs={
                                        {
                                            e: location.location_abs.enquiries,
                                            ed: location.location_abs.evaluations,
                                            pq: location.location_abs.price_negotiations,
                                            r: location.location_abs.retails,
                                        }
                                    }

                                    rel={
                                        {
                                            e: location.location_rel.enquiries,
                                            ed: location.location_rel.evaluations,
                                            pq: location.location_rel.price_negotiations,
                                            r: location.location_rel.retails,
                                        }
                                    }

                                    info={
                                        {
                                            user_id: '',
                                            name: 'All Evaluation',
                                            location_id: location.id,
                                            user_role_id: result.role_id,
                                        }
                                    }

                                ></ETVBRExchangeResultItem>

                            </View>);
                        }
                        else {
                            return (mainView);
                        }


                    }}
                    keyExtractor={(item, index) => index + ''}
                    onRefresh={() => this._onRefresh()}
                    refreshing={this.state.isLoading}
                />
            } else {
                let evaluators = result.locations[0].evaluation_managers[0].evaluators;
                let location = result.locations[0];
                flat_list = <FlatList
                    showsVerticalScrollIndicator={false}
                    extraData={this.state}
                    data={evaluators}
                    renderItem={({ item, index }) => {

                        let mainView = <TouchableOpacity onPress={null}>
                            <ETVBRExchangeItemChild
                                onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                                showPercentage={this.state.showPercentage}
                                abs={
                                    {
                                        e: item.abs.enquiries,
                                        ed: item.abs.evaluations,
                                        pq: item.abs.price_negotiations,
                                        r: item.abs.retails,
                                    }
                                }

                                rel={
                                    {
                                        e: item.rel.enquiries,
                                        ed: item.rel.evaluations,
                                        pq: item.rel.price_negotiations,
                                        r: item.rel.retails,
                                    }
                                }


                                info={
                                    {
                                        dp_url: item.info.dp_url,
                                        name: item.info.name,
                                        user_id: item.info.id,
                                        location_id: location.id,
                                        user_role_id: item.info.user_role_id,
                                    }
                                }


                            ></ETVBRExchangeItemChild>
                        </TouchableOpacity>;

                        if (index == evaluators.length - 1) {
                            return (<View>
                                {mainView}

                                <ETVBRExchangeResultItem
                                    showPercentage={this.state.showPercentage}
                                    onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                                    abs={
                                        {
                                            e: location.location_abs.enquiries,
                                            ed: location.location_abs.evaluations,
                                            pq: location.location_abs.price_negotiations,
                                            r: location.location_abs.retails,
                                        }
                                    }

                                    rel={
                                        {
                                            e: location.location_rel.enquiries,
                                            ed: location.location_rel.evaluations,
                                            pq: location.location_rel.price_negotiations,
                                            r: location.location_rel.retails,
                                        }
                                    }

                                    info={
                                        {
                                            user_id: result.locations[0].evaluation_managers[0].id,
                                            name: 'All Evaluation',
                                            location_id: location.id,
                                            user_role_id: result.role_id,
                                        }
                                    }

                                ></ETVBRExchangeResultItem>

                            </View>);
                        }
                        else {
                            return (mainView);
                        }


                    }}
                    keyExtractor={(item, index) => index + ''}
                    onRefresh={() => this._onRefresh()}
                    refreshing={this.state.isLoading}
                />
            }
        } else {
            flat_list = <View style={{ alignItems: 'center', flex: 1, alignSelf: 'center', justifyContent: 'center' }}>
                <Text>No data from server</Text>
                <TouchableOpacity onPress={() => { this._onRefresh() }}>
                    <Text style={{ marginTop: 10, padding: 10, borderColor: '#c4c4c4', borderRadius: 4, borderWidth: 1, }}> Reload </Text>
                </TouchableOpacity>
            </View>
            //console.log('result || result.locations is empty ');
        }

        let startDateData = global.global_filter_date.startDateStr.split('-');
        let endDateData = global.global_filter_date.endDateStr.split('-');

        date = ("0" + startDateData[2]).slice(-2) + '/' + ("0" + startDateData[1]).slice(-2)
            + '-' + ("0" + endDateData[2]).slice(-2) + '/' + ("0" + endDateData[1]).slice(-2);

        return (
            <View style={{
                flex: 1,
                paddingTop: (Platform.OS) === 'ios' ? 20 : 10,
                backgroundColor: '#fff'
            }} >

                <Toast
                    ref="toast"
                    position='top'
                    opacity={0.8}
                />


                <View style={{
                    flex: 1,
                    paddingBottom: 10,
                    paddingLeft: 10,
                    paddingRight: 10,
                }} >

                    <View style={{
                        flexDirection: 'row',
                        height: 30,
                    }}>


                        <View style={{ flex: 1 / 2, height: 30, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
                            <View style={{ backgroundColor: '#F4F4F4', borderWidth: 1, borderColor: '#D0D8E4', borderRadius: 6, justifyContent: 'center' }}>
                                <Text style={{ fontSize: 14, padding: 4, color: '#1B143C', textAlign: 'center' }}>{date}</Text>
                            </View>
                            <TouchableOpacity onPress={this._showDateRangePicker.bind(this, this.props.navigation)}>

                                <Image source={require('../../../images/ic_calender_blue.png')}
                                    style={[styles.icon, { marginLeft: 10 }]}
                                />
                            </TouchableOpacity>

                        </View>


                        <View style={{ flexDirection: 'row', height: 30, flex: 1 / 2, alignItems: 'center', justifyContent: 'flex-end', }} >


                            <PercentageSwitch
                                value={this.state.showPercentage}
                                onValueChange={(value) => {
                                    this.setState({ showPercentage: value });
                                    // if (value) {
                                    //     CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrPercentage });
                                    // }
                                }}
                            />
                            {/* <TouchableOpacity onPress={this._showETVBRFilterScreen.bind(this, this.props.navigation)}>
                                <View style={{ flexDirection: 'row' }}>

                                    <Image source={require('../../../images/ic_etvbr_filter.png')}
                                        style={[styles.icon, { marginLeft: 10, }]} />


                                    <View style={{ display: (isFilterSelected() ? 'flex' : 'none'), marginLeft: -14, marginTop: -6, backgroundColor: '#FFCA28', height: 16, width: 16, alignItems: 'center', borderRadius: 10, justifyContent: "center", alignItems: "center" }}>
                                        <Text adjustsFontSizeToFit={true}
                                            numberOfLines={1} style={{ textAlign: 'center', fontSize: 8, textAlignVertical: 'center', color: '#303030' }}>{getFilterSelectedCount()}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity> */}
                        </View>
                    </View>

                    <ETVBRHeaderExchange showPercentage={this.state.showPercentage} />


                    {flat_list}
                    {/* <NavigationEvents
                        onWillFocus={payload => {
                            this.updateOnFocusChange();
                        }}
                    /> */}

                </View>
            </View>
        );
    }
}