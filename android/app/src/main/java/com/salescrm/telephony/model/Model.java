package com.salescrm.telephony.model;

/**
 * Created by bannhi on 10/2/17.
 */

public class Model
{
    private String id;

    public Model(String id){
        this.id = id;
    }
    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+"]";
    }
}
