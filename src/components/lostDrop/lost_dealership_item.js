import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'

export default class LostDealershipItem extends Component {
  render() {
    let distributorName;
    let leadLost;
    let loss;
    if(this.props.data) {
      distributorName = this.props.data.name;
      leadLost = this.props.data.leads_lost;
      loss = this.props.data.loss;
    }
    return (
      <View style= {{
        flex: 10,
        alignItems:'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingTop:20,
        paddingLeft:10,
        paddingBottom:20,
        paddingRight:10,
        borderBottomColor:'#E4E4E4',
        borderBottomWidth:1,
        alignItems:'center'}}>

        <View style={{justifyContent: "center",alignItems: "center", paddingTop:4, paddingBottom:4, flex:3, borderRightColor:'#E4E4E4', borderRightWidth:1}}>
          <Text style= {{color:'#6C759B', fontSize:15}}>{distributorName+"  "}</Text>
        </View>
        <View style={{justifyContent: "center",alignItems: "center", paddingTop:4, paddingBottom:4,  flex:2, borderRightColor:'#E4E4E4', borderRightWidth:1}}>
          <Text style= {{color:'#6C759B', fontSize:15}}>{leadLost+"  "}</Text>
        </View>
        <View style={{justifyContent: "center",alignItems: "center", paddingTop:4, paddingBottom:4,  flex:3, borderRightColor:'#E4E4E4', borderRightWidth:1}}>
          <Text style= {{color:'#F20000', fontSize:15, textAlign:'center'}}>&#8377;{" "+loss+"  "}</Text>
        </View>
        <View style={{justifyContent: "center",alignItems: "center", paddingTop:4, paddingBottom:4,  flex:2}}>
          <TouchableOpacity onPress={()=>{this.props.showReasons(this.props.data)}}>
            <Image resize='center' style={{height:24, width:24,}} source={require('../../images/ic_info.png')}/>
           </TouchableOpacity>
        </View>

      </View>


    );
  }
}
