package com.salescrm.telephony.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.TransferEnquiriesPagerAdapter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.AllCarsDB;
import com.salescrm.telephony.db.CustomerTypeDB;
import com.salescrm.telephony.db.EnqSourceDB;
import com.salescrm.telephony.db.FilterEnquiryStage;
import com.salescrm.telephony.fragments.transferenquiry.TransferEnquiriesFiltersFragment;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.TransferEnqModelChangeListener;
import com.salescrm.telephony.model.AllTeamsModel;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.model.DistributeLeadInput;
import com.salescrm.telephony.model.FilteredLeadIdsModel;
import com.salescrm.telephony.model.TransferEnquiriesModel;
import com.salescrm.telephony.model.ApiGenericModelResponse;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;
import com.salescrm.telephony.views.CustomViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 14/8/17.
 */
public class TransferEnquiriesActivity extends AppCompatActivity implements TransferEnqModelChangeListener,TransferEnquiriesFiltersFragment.ApplyFilterListener {
    private CustomViewPager viewPager;
    private TextView tvTitle;
    private TextView tvSteps;
    private TextView tvActionPrev;
    private TextView tvActionNext;
    private FrameLayout frameActionPrev;
    private RelativeLayout relLoader;
    private int mLastPage=0;
    private String[] title= new String[]{"From SC","Choose criteria","Transfer to","Distribute","Transfer Enquiries"};
    private String[] titleToolbar= new String[]{"From SC","Choose criteria","To SC","Distribute","Transfer Enquiries"};
    private VectorDrawableCompat submitButton;
    private Preferences pref;
    private TransferEnquiriesModel transferEnquiriesModel;
    private ProgressDialog progressDialog;
    private int enquirySelectedCount = 0;
    private int leadTransferType = 0;
    private Realm realm;
    private String userLocationId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_enquiries);

        pref = Preferences.getInstance();
        pref.load(this);
        realm = Realm.getDefaultInstance();
        transferEnquiriesModel = TransferEnquiriesModel.getInstance(this);
        transferEnquiriesModel.setSelectedLead(null);
        transferEnquiriesModel.setOrderedSelectedLeadList(new ArrayList<String>());

        viewPager          = (CustomViewPager)   findViewById(R.id.transfer_enq_view_pager);
        tvTitle            = (TextView)    findViewById(R.id.tv_transfer_enq_title);
        tvSteps            = (TextView)    findViewById(R.id.tv_transfer_enq_steps);
        tvActionPrev       = (TextView)    findViewById(R.id.tv_transfer_enq_action_prev);
        tvActionNext       = (TextView)    findViewById(R.id.tv_transfer_enq_action_next);
        frameActionPrev    = (FrameLayout) findViewById(R.id.frame_transfer_enq_action_prev);
        relLoader          = (RelativeLayout) findViewById(R.id.rel_transfer_enq_loader);
        submitButton       =  VectorDrawableCompat.create(getResources(), R.drawable.ic_chevron_right_24dp, getTheme());

        leadTransferType = getIntent().getExtras()!=null?getIntent().getExtras().getInt("lead_transfer_type",0):0;
        userLocationId   = getIntent().getExtras()!=null?getIntent().getExtras().getString("lead_transfer_user_location",""):"";

        transferEnquiriesModel.setLocationId(userLocationId);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        
        viewPager.setPagingEnabled(false);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateUiOnPageChange(position);
                mLastPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tvActionNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewPager.getCurrentItem()==2 && getUserToList()==0){
                    showAlert("Select at least one user");
                }
                else if(isOneLeadSelected()) {
                    moveNext();
                }
                else {
                    showAlert("No enquires selected");
                }
            }
        });

/*        tvActionPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSelectedLeads();
            }
        });*/


        getData();

    }

    private int getUserToList() {
        int sum=0;
        List<TransferEnquiriesModel.Item> data = transferEnquiriesModel.getUserListTo();
        for (int i=0;i<(data==null?0:data.size());i++){
            if(!data.get(i).isHeader() && data.get(i).isChecked()) {
                sum+=1;
            }
        }
        return sum;
    }

    private void showAlert(String text) {
        Util.showToast(this,text, Toast.LENGTH_SHORT);
    }

    /**
     *Get user lists data
     */
    private void getData() {
            relLoader.setVisibility(View.VISIBLE);
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getAllTeams(Util.getInt(userLocationId), new Callback<AllTeamsModel>() {
                @Override
                public void success(AllTeamsModel allTeamsModel, Response response) {
                    Util.updateHeaders(response.getHeaders());

                    if(allTeamsModel.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                        ApiUtil.InvalidUserLogout(TransferEnquiriesActivity.this,0);
                    }

                    if (allTeamsModel.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)
                            && allTeamsModel.getResult() != null && allTeamsModel.getResult().length > 0) {
                        updateUserList(allTeamsModel.getResult());
                        updateFilterData();
                        init();
                    }else {
                        showErrorDialog("Failed to load data", 0);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showErrorDialog("Failed to load data", 0);
                }
            });

    }

    private void updateLeadSelected(FilteredLeadIdsModel.Result[] result){
        dismissProgressDialog();
        if(result==null &&transferEnquiriesModel.getSelectedLeadList()!=null){
            enquirySelectedCount = transferEnquiriesModel.getSelectedLeadList().size();
            tvActionPrev.setText(String.format(Locale.getDefault(),"%d enquiries selected", getEnquiriesSelectedCount()));
            return;
        }
        Set<String> selectedLeadList = new HashSet<>();
        for (FilteredLeadIdsModel.Result aResult : result) {
            selectedLeadList.add(aResult.getLead_id());
        }
        transferEnquiriesModel.setSelectedLead(selectedLeadList);
        enquirySelectedCount = selectedLeadList.size();
        tvActionPrev.setText(String.format(Locale.getDefault(),"%d enquiries selected", getEnquiriesSelectedCount()));

    }


    private void updateUserList(AllTeamsModel.Result[] allTeamsModel) {
        List<TransferEnquiriesModel.Item> userItemList = new ArrayList<>();
        for(int i=0;i<allTeamsModel.length;i++){
            AllTeamsModel.Result resultData = allTeamsModel[i];
            if(null != resultData) {
                TransferEnquiriesModel.Item item = transferEnquiriesModel.new Item();
                item.setId(resultData.getId());
                item.setTitle(resultData.getName());
                item.setHeader(true);
                userItemList.add(item);
                for(int j=0;j<resultData.getUsers().length;j++){
                    if(null !=resultData.getUsers()[j]) {
                        TransferEnquiriesModel.Item itemUser = transferEnquiriesModel.new Item();
                        itemUser.setId(resultData.getUsers()[j].getUser_id());
                        itemUser.setTitle(resultData.getUsers()[j].getUserName());
                        itemUser.setHeader(false);
                        itemUser.setImgUrl(resultData.getUsers()[j].getImage_url());
                        userItemList.add(itemUser);
                    }
                }
            }
        }
        transferEnquiriesModel.setUserList(userItemList);
        transferEnquiriesModel.setUserListTo(userItemList);
    }
    private void updateFilterData(){
        List<TransferEnquiriesModel.Item> filterDataCar = new ArrayList<>();
        for (int i = 0; i < realm.where(AllCarsDB.class).equalTo("category", WSConstants.CAR_CATEGORY_BOTH).findAll().size(); i++) {
            AllCarsDB allCarsDB = realm.where(AllCarsDB.class).equalTo("category", WSConstants.CAR_CATEGORY_BOTH).findAll().get(i);
            TransferEnquiriesModel.Item item = transferEnquiriesModel.new Item();
            item.setId(allCarsDB.getId()+"");
            item.setTitle(allCarsDB.getName());
            item.setHeader(false);
            item.setImgUrl(null);
            filterDataCar.add(item);

        }
        transferEnquiriesModel.setCarModelList(filterDataCar);



        List<TransferEnquiriesModel.Item> filterEnqSource = new ArrayList<>();
        for (int i = 0; i < realm.where(EnqSourceDB.class).findAll().size(); i++) {
            EnqSourceDB enqSourceDB = realm.where(EnqSourceDB.class).findAll().get(i);
            TransferEnquiriesModel.Item item = transferEnquiriesModel.new Item();
            item.setId(enqSourceDB.getId()+"");
            item.setTitle(enqSourceDB.getName());
            item.setHeader(false);
            item.setImgUrl(null);
            filterEnqSource.add(item);
        }
        transferEnquiriesModel.setEnqSourceList(filterEnqSource);


        List<TransferEnquiriesModel.Item> filterCustomerType= new ArrayList<>();
        for (int i = 0; i < realm.where(CustomerTypeDB.class).findAll().size(); i++) {
            CustomerTypeDB customerTypeDB = realm.where(CustomerTypeDB.class).findAll().get(i);
            TransferEnquiriesModel.Item item = transferEnquiriesModel.new Item();
            item.setId(customerTypeDB.getId());
            item.setTitle(customerTypeDB.getType());
            item.setHeader(false);
            item.setImgUrl(null);
            filterCustomerType.add(item);
        }
        transferEnquiriesModel.setCustomerTypeList(filterCustomerType);


        List<TransferEnquiriesModel.Item> filterEnqStage = new ArrayList<>();
        for (int i = 0; i < realm.where(FilterEnquiryStage.class).findAll().size(); i++) {
            FilterEnquiryStage filterEnquiryStage = realm.where(FilterEnquiryStage.class).findAll().get(i);
            TransferEnquiriesModel.Item item = transferEnquiriesModel.new Item();
            item.setId(filterEnquiryStage.getStage_id()+"");
            item.setTitle(filterEnquiryStage.getStage());
            item.setHeader(false);
            item.setImgUrl(null);
            filterEnqStage.add(item);
        }
        transferEnquiriesModel.setEnquiryStageList(filterEnqStage);





    }
    private void dismissProgressDialog(){
        if(!isFinishing() &&  progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }
    private void showErrorDialog(String error,final int from){
        dismissProgressDialog();
        new AutoDialog(TransferEnquiriesActivity.this, new AutoDialogClickListener() {
            @Override
            public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
                if(from == 0) {
                    getData();
                }
                else if(from ==1){
                    getFilteredLeads();
                }
                else if(from ==2){
                    distributeLeads();
                }
            }

            @Override
            public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {
                if(from == 0) {
                    finish();
                }
            }
        },new AutoDialogModel(error,"Retry","Close")).show();

    }

    private void moveNext() {
        if (viewPager.getCurrentItem() < 3) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
        }
        else if(viewPager.getCurrentItem()==3 ){
            distributeLeads();
        }

    }

    private void distributeLeads() {
        int sum=0;
        for(int i=0;i<transferEnquiriesModel.getDistributeList().size();i++){
           sum += Util.getInt(transferEnquiriesModel.getDistributeList().get(i).getTitle());
        }
        if(sum!=enquirySelectedCount){
            Util.showToast(this,"Invalid lead distribution",Toast.LENGTH_SHORT);
            return;
        }

        logEvent(leadTransferType==0?CleverTapConstants.EVENT_PROFORMA_INVOICE_TRANSFER_ENQ_FILTER:CleverTapConstants.EVENT_PROFORMA_INVOICE_TRANSFER_ENQ_LEAD_ID);
        progressDialog.setMessage("Distributing Leads.. Please wait!");
        progressDialog.show();

        DistributeLeadInput distributeLeadInput = new DistributeLeadInput();

        //setting selected lead ids;
        List<Integer> leadList = new ArrayList<Integer>();
        for(String s : transferEnquiriesModel.getSelectedLeadList()){
            leadList.add(Util.getInt(s));
        }
        distributeLeadInput.setLeadIds(leadList);


        //setting distribute data;
        List<DistributeLeadInput.DSEIds> dseIdsList = new ArrayList<>();
        for(int i=0;i<transferEnquiriesModel.getDistributeList().size();i++){
            DistributeLeadInput.DSEIds dseId = distributeLeadInput.new DSEIds();
            dseId.setUser_id(Util.getInt(transferEnquiriesModel.getDistributeList().get(i).getId()));
            dseId.setLeads_to_distribute(Util.getInt(transferEnquiriesModel.getDistributeList().get(i).getTitle()));
            dseIdsList.add(dseId);
        }
        distributeLeadInput.setDSEIds(dseIdsList);


        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).distributeLeads(distributeLeadInput, new Callback<ApiGenericModelResponse>() {
            @Override
            public void success(ApiGenericModelResponse dataResponse, Response response) {
                Util.updateHeaders(response.getHeaders());

                if(dataResponse!=null && dataResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(TransferEnquiriesActivity.this,0);
                }

                if(dataResponse!=null && dataResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)
                        && dataResponse.getResult()!=null && dataResponse.getResult().equalsIgnoreCase("1") ) {
                    dismissProgressDialog();
                    Util.showToast(getApplicationContext(),"Leads successfully transferred",Toast.LENGTH_LONG);
                    finish();
                }else {
                    showErrorDialog("Error while distributing leads",2);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                showErrorDialog("Error while distributing leads",2);
            }
        });
    }

    public void finishActivity(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if(viewPager.getCurrentItem()==1 && leadTransferType==1){
            super.onBackPressed();
        }
        else if (viewPager.getCurrentItem() > 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
        else {
            super.onBackPressed();
        }

    }

    private void init(){
        viewPager.setAdapter(new TransferEnquiriesPagerAdapter(getSupportFragmentManager()));
        relLoader.setVisibility(View.GONE);
        if(leadTransferType ==0) {
            updateUiOnPageChange(0);
            viewPager.setCurrentItem(0);
        }
        else {
            updateUiOnPageChange(1);
            viewPager.setCurrentItem(1);
        }
    }

    private void updateUiOnPageChange(int position){
        tvTitle.setText(titleToolbar[position]);
        tvSteps.setText(String.format(Locale.getDefault(),"Step %d of %d",leadTransferType==0?position+1:position,
                leadTransferType==0?4:3));
        tvActionNext.setText(title[position+1]);
        if(position == 3){
            frameActionPrev.setVisibility(View.GONE);
            tvActionNext.setText(String.format(Locale.getDefault(),"Transfer %d Enquiries",getEnquiriesSelectedCount()));
            tvActionNext.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        }
        else {
            Drawable rightDrawable = VectorDrawableCompat
                    .create(getResources(), R.drawable.ic_chevron_right_white_24dp, null);

            tvActionNext.setCompoundDrawablesWithIntrinsicBounds(null,null,rightDrawable,null);
            frameActionPrev.setVisibility(View.VISIBLE);

        }
        tvActionPrev.setText(String.format(Locale.getDefault(),"%d enquiries selected",getEnquiriesSelectedCount()));
    }

    private int getEnquiriesSelectedCount() {
        return enquirySelectedCount;
    }

    @Override
    public void onDataChanged() {

    }

    @Override
    public void onChangeSelectedItem() {
        getFilteredLeads();
    }

   private void getFilteredLeads(){
       ArrayList<String> userData = new ArrayList<>();
       for (int i = 0; i < transferEnquiriesModel.getUserList().size(); i++) {
           if(transferEnquiriesModel.getUserList().get(i).isChecked() && !transferEnquiriesModel.getUserList().get(i).isHeader() ) {
               userData.add(transferEnquiriesModel.getUserList().get(i).getId());
           }
       }
       if(userData.size() == 0){
           enquirySelectedCount = 0;
           tvActionPrev.setText(String.format(Locale.getDefault(),"%d enquiries selected",getEnquiriesSelectedCount()));
           return;
       }
       progressDialog.setMessage("Fetching enquires.. Please wait!");
       progressDialog.show();

       ArrayList<String> models = new ArrayList<>();
       for (int i = 0; i < transferEnquiriesModel.getCarModelList().size(); i++) {
           if (transferEnquiriesModel.getCarModelList().get(i).isChecked()) {
               models.add(transferEnquiriesModel.getCarModelList().get(i).getId());
           }
       }

       ArrayList<String> enqSources = new ArrayList<>();
       for (int i = 0; i < transferEnquiriesModel.getEnqSourceList().size(); i++) {
           if (transferEnquiriesModel.getEnqSourceList().get(i).isChecked()) {
               enqSources.add(transferEnquiriesModel.getEnqSourceList().get(i).getId());
           }
       }

       //Selected customer type of filter
       ArrayList<String> customerTypes = new ArrayList<>();
       for (int i = 0; i < transferEnquiriesModel.getCustomerTypeList().size(); i++) {
           if (transferEnquiriesModel.getCustomerTypeList().get(i).isChecked()) {
               customerTypes.add(transferEnquiriesModel.getCustomerTypeList().get(i).getId());
           }
       }

       //Selected stage ids' of filter
       ArrayList<String> stageIds = new ArrayList<>();
       for (int i = 0; i < transferEnquiriesModel.getEnquiryStageList().size(); i++) {
           if (transferEnquiriesModel.getEnquiryStageList().get(i).isChecked()) {
               stageIds.add(transferEnquiriesModel.getEnquiryStageList().get(i).getId());
           }
       }

       ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getFilteredLeadIds(userData,
               models, enqSources, customerTypes, stageIds, new Callback<FilteredLeadIdsModel>() {
                   @Override
           public void success(FilteredLeadIdsModel filteredLeadIdsModel, Response response) {
               Util.updateHeaders(response.getHeaders());

                       if(filteredLeadIdsModel.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                           ApiUtil.InvalidUserLogout(TransferEnquiriesActivity.this,0);
                       }

               if(filteredLeadIdsModel.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)
                       && filteredLeadIdsModel.getResult()!=null ) {
                   updateLeadSelected(filteredLeadIdsModel.getResult());
               }else {
                   showErrorDialog("Error while fetching leads",1);
               }
           }

           @Override
           public void failure(RetrofitError error) {
               showErrorDialog("Error while fetching leads",1);
           }
       });
   }
    private boolean isOneLeadSelected() {
        return enquirySelectedCount > 0;
    }

    @Override
    public void onApplyFilter(FilteredLeadIdsModel.Result[] result) {
        updateLeadSelected(result);
    }

    private void showSelectedLeads(){
        if(leadTransferType == 0
                && transferEnquiriesModel.getSelectedLeadList()!=null
                && transferEnquiriesModel.getSelectedLeadList().size()>0) {
            final CharSequence[] items =new CharSequence[transferEnquiriesModel.getSelectedLeadList().size()];
            int i=0;
            for(String s: transferEnquiriesModel.getSelectedLeadList()){
                items[i] = s;
                i++;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Selected Enquiries");
            builder.setItems(items,null);
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void logEvent(String event){
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_PROFORMA_INVOICE_KEY_TRANSFER_ENQ_TYPE,
                event);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_PROFORMA_TRANSFER_ENQUIRIES, hashMap);

    }
}
