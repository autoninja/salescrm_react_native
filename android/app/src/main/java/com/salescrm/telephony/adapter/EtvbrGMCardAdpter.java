package com.salescrm.telephony.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.DetailsOfEtvbr;
import com.salescrm.telephony.activity.EtvbrImgDialog;
import com.salescrm.telephony.activity.EtvbrSmActivity;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.EtvbrLocationChildFragment;
import com.salescrm.telephony.fragments.EtvbrLocationFragment;
import com.salescrm.telephony.utils.CircleTransform;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.picasso.Picasso;

import io.realm.Realm;

/**
 * Created by prateek on 14/7/17.
 */

public class EtvbrGMCardAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private Realm realm;
    //private RealmResults<ResultSM> resultSMs;
    //private RealmResults<ResultEtvbr> resultEtvbrs;
    private Location locationResult;

    private String userIdForAllSMData;

    public EtvbrGMCardAdpter(FragmentActivity activity, Realm realm, Location locationResult) {
        this.context = activity;
        //this.resultSMs = locationResult.getSalesManagers();
        this.realm = realm;
        //this.resultEtvbrs = resultEtvbrs;
        this.locationResult = locationResult;
        this.userIdForAllSMData = DbUtils.isUserOperations() ? WSConstants.USER_ROLE_OPERATIONS : WSConstants.USER_ROLE_BRANCH_HEAD;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.etvbr_sm_card_adpter, parent, false);
        return new EtvbrGMCardAdpter.EtvbrGMCardHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(EtvbrLocationFragment.isLocationAvailable()) {
            if (EtvbrLocationChildFragment.PERCENT) {
                viewCardsGMRel(holder, position);
            } else {
                viewCardsGMAbs(holder, position);
            }
        }else{
            if (EtvbrLocationFragment.PERCENT) {
                viewCardsGMRel(holder, position);
            } else {
                viewCardsGMAbs(holder, position);
            }
        }
    }

    private void viewCardsGMAbs(RecyclerView.ViewHolder holder, final int position) {
        final EtvbrGMCardHolder cardHolder = (EtvbrGMCardHolder) holder;
        //final int position = pos;
        cardHolder.cardView.setVisibility(View.VISIBLE);
        cardHolder.llinearLayout.setVisibility(View.GONE);
        //absolute = results.get(position).getAbs();
        cardHolder.tvTargetRetailCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetEnquiyCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetTDCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetVisitCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetBookingCard.setVisibility(View.VISIBLE);
        cardHolder.viewTargetCard.setVisibility(View.VISIBLE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 0);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        if(locationResult.getSalesManagers() != null){
        if(position < locationResult.getSalesManagers().size()) {

            Paint paint = new Paint();
            paint.setColor(Color.BLUE);
            paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);

                cardHolder.tvEnquiryCard.setPaintFlags(paint.getFlags());
                cardHolder.tvEnquiryCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getEnquiries());

                cardHolder.tvTestDriveCard.setPaintFlags(paint.getFlags());
                cardHolder.tvTestDriveCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getTdrives());

                cardHolder.tvVisitCard.setPaintFlags(paint.getFlags());
                cardHolder.tvVisitCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getVisits());

                cardHolder.tvBookingCard.setPaintFlags(paint.getFlags());
                cardHolder.tvBookingCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getBookings());

                cardHolder.tvRetailCard.setPaintFlags(paint.getFlags());
                cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getAbs().getRetails());

                cardHolder.tvLostCard.setPaintFlags(paint.getFlags());
                cardHolder.tvLostCard.setText(""+ locationResult.getSalesManagers().get(position).getInfo().getAbs().getLost_drop());

                cardHolder.tvTargetEnquiyCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getTargets().getEnquiries());
                cardHolder.tvTargetTDCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getTargets().getTdrives());
                cardHolder.tvTargetVisitCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getTargets().getVisits());
                cardHolder.tvTargetBookingCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getTargets().getBookings());
                cardHolder.tvTargetRetailCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getTargets().getRetails());

                cardHolder.tvEnquiryCard.setVisibility(View.VISIBLE);
                cardHolder.viewEnquiryCard.setVisibility(View.VISIBLE);
                cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
                cardHolder.llEnquriyCard.setVisibility(View.VISIBLE);

                if (locationResult.getSalesManagers().get(position).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(position).getInfo().getDpUrl().equalsIgnoreCase("")) {
                    cardHolder.tvNameCard.setVisibility(View.GONE);
                    cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                    cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                    String url = locationResult.getSalesManagers().get(position).getInfo().getDpUrl();
                    Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUserCard);
                } else if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                    cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                    cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                    cardHolder.imgUserCard.setVisibility(View.GONE);
                    cardHolder.tvNameCard.setText(locationResult.getSalesManagers().get(position).getInfo().getName().charAt(0) + "");
                } else {
                    cardHolder.imgUserCard.setVisibility(View.GONE);
                    cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                    cardHolder.tvNameCard.setText("N");
                }

                cardHolder.imgUserCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                        bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(position).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(position).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                    }
                });

                cardHolder.tvNameCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                        bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(position).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(position).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                    }
                });
                cardHolder.llInner.setBackgroundColor(Color.WHITE);
                cardHolder.tvEnquiryCard.setTextColor(Color.BLACK);
                cardHolder.tvTestDriveCard.setTextColor(Color.BLACK);
                cardHolder.tvVisitCard.setTextColor(Color.BLACK);
                cardHolder.tvBookingCard.setTextColor(Color.BLACK);
                cardHolder.tvRetailCard.setTextColor(Color.BLACK);
                cardHolder.tvUserTotalCard.setTextColor(Color.BLACK);

                cardHolder.tvEnquiryCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvTestDriveCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvVisitCard.setTypeface(null, Typeface.NORMAL);

                cardHolder.tvBookingCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvRetailCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvUserTotalCard.setTypeface(null, Typeface.NORMAL);

                cardHolder.viewTdCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
                cardHolder.viewBookingCard.setVisibility(View.VISIBLE);
                cardHolder.viewLostCard.setVisibility(View.VISIBLE);

                cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetEnquiyCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetTDCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetVisitCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetBookingCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#C1C1C1"));

                cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EtvbrSmActivity.class);
                    intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                    //intent.putExtra("sm_id", position);
                    intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                    intent.putExtra("etvbr", 0);
                    context.startActivity(intent);
                    }
                });

            cardHolder.tvEnquiryCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EtvbrSmActivity.class);
                    intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                    //intent.putExtra("sm_id", position);
                    intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                    intent.putExtra("etvbr", 0);
                    context.startActivity(intent);
                }
            });
            cardHolder.tvTestDriveCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EtvbrSmActivity.class);
                    intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                    //intent.putExtra("sm_id", position);
                    intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                    intent.putExtra("etvbr", 0);
                    context.startActivity(intent);
                }
            });
            cardHolder.tvVisitCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EtvbrSmActivity.class);
                    intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                    //intent.putExtra("sm_id", position);
                    intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                    intent.putExtra("etvbr", 0);
                    context.startActivity(intent);
                }
            });
            cardHolder.tvBookingCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EtvbrSmActivity.class);
                    intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                    //intent.putExtra("sm_id", position);
                    intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                    intent.putExtra("etvbr", 0);
                    context.startActivity(intent);
                }
            });
            cardHolder.tvRetailCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EtvbrSmActivity.class);
                    intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                    //intent.putExtra("sm_id", position);
                    intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                    intent.putExtra("etvbr", 0);
                    context.startActivity(intent);
                }
            });
            cardHolder.tvLostCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EtvbrSmActivity.class);
                    intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                    //intent.putExtra("sm_id", position);
                    intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                    intent.putExtra("etvbr", 0);
                    context.startActivity(intent);
                }
            });
            cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(0,""+locationResult.getSalesManagers().get(position).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getAbs().getEnquiries(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });
            cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(1,locationResult.getSalesManagers().get(position).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getAbs().getTdrives(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),
                            ""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(2,locationResult.getSalesManagers().get(position).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getAbs().getVisits(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),
                            ""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(3,locationResult.getSalesManagers().get(position).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getAbs().getBookings(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),
                            ""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,locationResult.getSalesManagers().get(position).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getAbs().getRetails(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),
                            ""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvLostCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,locationResult.getSalesManagers().get(position).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getAbs().getLost_drop(),
                            ""+locationResult.getSalesManagers().get(position).getInfo().getUserRoleId(),
                            ""+locationResult.getLocationId());
                    return true;
                }
            });

            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
            }
            } else{

            cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));

            Paint paint = new Paint();
            paint.setColor(Color.BLUE);
            paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);

            cardHolder.tvEnquiryCard.setPaintFlags(paint.getFlags());
            cardHolder.tvEnquiryCard.setText("" + locationResult.getLocationAbs().getEnquiries());

            cardHolder.tvTestDriveCard.setPaintFlags(paint.getFlags());
            cardHolder.tvTestDriveCard.setText("" + locationResult.getLocationAbs().getTdrives());

            cardHolder.tvVisitCard.setPaintFlags(paint.getFlags());
            cardHolder.tvVisitCard.setText("" + locationResult.getLocationAbs().getVisits());

            cardHolder.tvBookingCard.setPaintFlags(paint.getFlags());
            cardHolder.tvBookingCard.setText("" + locationResult.getLocationAbs().getBookings());

            cardHolder.tvRetailCard.setPaintFlags(paint.getFlags());
            cardHolder.tvRetailCard.setText("" + locationResult.getLocationAbs().getRetails());

            cardHolder.tvLostCard.setPaintFlags(paint.getFlags());
            cardHolder.tvLostCard.setText(""+locationResult.getLocationAbs().getLost_drop());

            cardHolder.tvTargetEnquiyCard.setText(""+locationResult.getLocationTargets().getEnquiries());
            cardHolder.tvTargetTDCard.setText(""+locationResult.getLocationTargets().getTdrives());
            cardHolder.tvTargetVisitCard.setText(""+locationResult.getLocationTargets().getVisits());
            cardHolder.tvTargetBookingCard.setText(""+locationResult.getLocationTargets().getBookings());
            cardHolder.tvTargetRetailCard.setText(""+locationResult.getLocationTargets().getRetails());

            cardHolder.tvEnquiryCard.setVisibility(View.VISIBLE);
            cardHolder.viewEnquiryCard.setVisibility(View.INVISIBLE);
            cardHolder.tvTargetTitleCard.setVisibility(View.VISIBLE);
            cardHolder.llEnquriyCard.setVisibility(View.VISIBLE);

            cardHolder.tvNameCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
            cardHolder.imgUserCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setText("Achieved");

            cardHolder.tvTargetTitleCard.setText("Target");

            cardHolder.tvEnquiryCard.setTextColor(Color.WHITE);
            cardHolder.tvTestDriveCard.setTextColor(Color.WHITE);
            cardHolder.tvVisitCard.setTextColor(Color.WHITE);
            cardHolder.tvBookingCard.setTextColor(Color.WHITE);
            cardHolder.tvRetailCard.setTextColor(Color.WHITE);
            cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);
            //cardHolder.tvTargetTitleCard.setTextColor(Color.WHITE);

            cardHolder.tvEnquiryCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvTestDriveCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvVisitCard.setTypeface(null, Typeface.BOLD);;
            cardHolder.tvBookingCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvRetailCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvLostCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvUserTotalCard.setTypeface(null, Typeface.BOLD);
            //cardHolder.tvTargetTitleCard.setTypeface(null, Typeface.BOLD);

            cardHolder.viewTdCard.setVisibility(View.INVISIBLE);
            cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
            cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
            cardHolder.viewBookingCard.setVisibility(View.INVISIBLE);
            cardHolder.viewLostCard.setVisibility(View.INVISIBLE);

            cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetEnquiyCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetTDCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetVisitCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetBookingCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#8C94AB"));

            final String userId = ""+realm.where(UserDetails.class).findFirst().getUserId();

            cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(0,"All SM's Data",
                            ""+userId,
                            ""+locationResult.getLocationAbs().getEnquiries(),
                            userIdForAllSMData,
                            ""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(1,"All SM's Data",
                            ""+userId,
                            ""+locationResult.getLocationAbs().getTdrives(),
                            userIdForAllSMData,
                            ""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(2,"All SM's Data",
                            ""+userId,
                            ""+locationResult.getLocationAbs().getVisits(),
                            userIdForAllSMData,
                            ""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(3,"All SM's Data",
                            ""+userId,
                            ""+locationResult.getLocationAbs().getBookings(),
                            userIdForAllSMData,
                            ""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,"All SM's Data",
                            ""+userId,
                            ""+locationResult.getLocationAbs().getRetails(),
                            userIdForAllSMData,
                            ""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvLostCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,"All SM's Data",
                            ""+userId,
                            ""+locationResult.getLocationAbs().getLost_drop(),
                            userIdForAllSMData,
                            ""+locationResult.getLocationId());
                    return true;
                }
            });


            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
            }
        }
        }
    }



    private void viewCardsGMRel(RecyclerView.ViewHolder holder, final  int position) {
        final EtvbrGMCardHolder cardHolder = (EtvbrGMCardHolder) holder;
        //final int position = pos;
        cardHolder.tvTargetRetailCard.setVisibility(View.GONE);
        cardHolder.tvTargetEnquiyCard.setVisibility(View.GONE);
        cardHolder.tvTargetTDCard.setVisibility(View.GONE);
        cardHolder.tvTargetVisitCard.setVisibility(View.GONE);
        cardHolder.tvTargetBookingCard.setVisibility(View.GONE);
        cardHolder.viewTargetCard.setVisibility(View.GONE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 6);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        cardHolder.llinearLayout.setVisibility(View.GONE);
        //rels = results.get(position).getRel();
        if(locationResult.getSalesManagers() != null){
        if(position < locationResult.getSalesManagers().size()) {


                //System.out.println("resultSM " + locationResult.getSalesManagers().get(position).getInfo().getRel().getEnquiries());
            cardHolder.tvEnquiryCard.setPaintFlags(0);
            cardHolder.tvEnquiryCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getRel().getEnquiries());

            cardHolder.tvTestDriveCard.setPaintFlags(0);
            cardHolder.tvTestDriveCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getRel().getTdrives());

            cardHolder.tvVisitCard.setPaintFlags(0);
            cardHolder.tvVisitCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getRel().getVisits());

            cardHolder.tvBookingCard.setPaintFlags(0);
            cardHolder.tvBookingCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getRel().getBookings());

            cardHolder.tvRetailCard.setPaintFlags(0);
            cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getRel().getRetails());

            cardHolder.tvLostCard.setPaintFlags(0);
            cardHolder.tvLostCard.setText("" + locationResult.getSalesManagers().get(position).getInfo().getRel().getLost_drop());

            cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvLostCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

                cardHolder.tvEnquiryCard.setVisibility(View.GONE);
                cardHolder.viewEnquiryCard.setVisibility(View.GONE);
                cardHolder.tvEnquiryCard.setVisibility(View.GONE);
                cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
                cardHolder.llEnquriyCard.setVisibility(View.GONE);

                if (locationResult.getSalesManagers().get(position).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(position).getInfo().getDpUrl().equalsIgnoreCase("")) {
                    cardHolder.tvNameCard.setVisibility(View.GONE);
                    cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                    cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                    String url = locationResult.getSalesManagers().get(position).getInfo().getDpUrl();
                    Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUserCard);
                } else if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                    cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                    cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                    cardHolder.imgUserCard.setVisibility(View.GONE);
                    cardHolder.tvNameCard.setText(locationResult.getSalesManagers().get(position).getInfo().getName().charAt(0) + "");
                } else {
                    cardHolder.imgUserCard.setVisibility(View.GONE);
                    cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                    cardHolder.tvNameCard.setText("N");
                }

                cardHolder.imgUserCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        Intent intent = new Intent(context, EtvbrImgDialog.class);
                        Bundle bundle = new Bundle();
                        if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                            bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(position).getInfo().getDpUrl());
                            bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(position).getInfo().getName());
                            intent.putExtras(bundle);
                            context.startActivity(intent);
                        }
                        return true;
                    }
                });

                cardHolder.tvNameCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        Intent intent = new Intent(context, EtvbrImgDialog.class);
                        Bundle bundle = new Bundle();
                        if (locationResult.getSalesManagers().get(position).getInfo().getName() != null && !locationResult.getSalesManagers().get(position).getInfo().getName().equalsIgnoreCase("")) {
                            bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(position).getInfo().getDpUrl());
                            bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(position).getInfo().getName());
                            intent.putExtras(bundle);
                            context.startActivity(intent);
                        }
                        return true;
                    }
                });

                cardHolder.llInner.setBackgroundColor(Color.WHITE);
                cardHolder.tvEnquiryCard.setTextColor(Color.BLACK);
                cardHolder.tvTestDriveCard.setTextColor(Color.BLACK);
                cardHolder.tvVisitCard.setTextColor(Color.BLACK);
                cardHolder.tvBookingCard.setTextColor(Color.BLACK);
                cardHolder.tvRetailCard.setTextColor(Color.BLACK);
                cardHolder.tvUserTotalCard.setTextColor(Color.BLACK);

                cardHolder.viewTdCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
                cardHolder.viewBookingCard.setVisibility(View.VISIBLE);
                cardHolder.viewLostCard.setVisibility(View.VISIBLE);

                cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrSmActivity.class);
                        intent.putExtra("sm_id", locationResult.getSalesManagers().get(position).getId());
                        //intent.putExtra("sm_id", position);
                        intent.putExtra("sm_name", locationResult.getSalesManagers().get(position).getInfo().getName());
                        intent.putExtra("etvbr", 0);
                        context.startActivity(intent);
                    }
                });

            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
            }
            }
        else{
            cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));

            cardHolder.tvEnquiryCard.setPaintFlags(0);
            cardHolder.tvEnquiryCard.setText("" + locationResult.getLocationRel().getEnquiries());

            cardHolder.tvTestDriveCard.setPaintFlags(0);
            cardHolder.tvTestDriveCard.setText("" + locationResult.getLocationRel().getTdrives());

            cardHolder.tvVisitCard.setPaintFlags(0);
            cardHolder.tvVisitCard.setText("" + locationResult.getLocationRel().getVisits());

            cardHolder.tvBookingCard.setPaintFlags(0);
            cardHolder.tvBookingCard.setText("" + locationResult.getLocationRel().getBookings());

            cardHolder.tvRetailCard.setPaintFlags(0);
            cardHolder.tvRetailCard.setText("" + locationResult.getLocationRel().getRetails());

            cardHolder.tvLostCard.setPaintFlags(0);
            cardHolder.tvLostCard.setText(""+locationResult.getLocationRel().getLost_drop());

            cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvLostCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvEnquiryCard.setVisibility(View.GONE);
            cardHolder.viewEnquiryCard.setVisibility(View.GONE);
            cardHolder.tvEnquiryCard.setVisibility(View.GONE);
            cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
            cardHolder.llEnquriyCard.setVisibility(View.GONE);

            cardHolder.tvNameCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
            cardHolder.imgUserCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setText("Total%");

            cardHolder.tvEnquiryCard.setTextColor(Color.WHITE);
            cardHolder.tvTestDriveCard.setTextColor(Color.WHITE);
            cardHolder.tvVisitCard.setTextColor(Color.WHITE);
            cardHolder.tvBookingCard.setTextColor(Color.WHITE);
            cardHolder.tvRetailCard.setTextColor(Color.WHITE);
            cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);

            cardHolder.viewTdCard.setVisibility(View.INVISIBLE);
            cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
            cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
            cardHolder.viewBookingCard.setVisibility(View.INVISIBLE);
            cardHolder.viewLostCard.setVisibility(View.INVISIBLE);

            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
            }
        }
        }
    }

    @Override
    public int getItemCount() {
        //resultSMs = realm.where(ResultSM.class).findAll();
        if(locationResult.isValid() && locationResult.getSalesManagers().isValid()) {
            return (locationResult.getSalesManagers().size() > 0) ? locationResult.getSalesManagers().size() + 1 : 0;
        }else {
            return 0;
        }
    }

    public class EtvbrGMCardHolder extends RecyclerView.ViewHolder {

        ImageView imgUser, imgUserCard;
        TextView tvNameCard, tvEnquiryCard, tvTestDriveCard, tvVisitCard, tvBookingCard, tvRetailCard, tvLostCard;
        TextView tvName, tvEnquiry, tvTestDrive, tvVisit, tvBooking, tvRetail;
        View viewEnquiryCard, viewTdCard, viewVisitCard, viewBookingCard, viewReatilCard, viewLostCard;
        View viewEnquiry, viewTd, viewVisit, viewBooking, viewReatil;
        LinearLayout llaLinearLayout, llinearLayout;
        LinearLayout llInner;
        TextView tvUserTotalCard;
        TextView tvUserTotal;
        CardView cardView;
        LinearLayout llEnquriyCard, llTdCard, llVisitCard, llBookingCard, llRetailsCard, llLostCard;
        TextView tvTargetTitleCard;
        TextView tvTargetEnquiyCard, tvTargetTDCard, tvTargetVisitCard, tvTargetBookingCard, tvTargetRetailCard;
        View viewTargetCard;
        RelativeLayout rlFrameLayoutCard;


        public EtvbrGMCardHolder(View itemView) {
            super(itemView);
            imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            tvName = (TextView) itemView.findViewById(R.id.text_user);
            tvEnquiry = (TextView) itemView.findViewById(R.id.txt_enquiry);
            tvTestDrive = (TextView) itemView.findViewById(R.id.txt_td);
            tvVisit = (TextView) itemView.findViewById(R.id.txt_visit);
            tvBooking = (TextView) itemView.findViewById(R.id.txt_booking);
            tvRetail = (TextView) itemView.findViewById(R.id.txt_retail);
            //llaLinearLayout = (LinearLayout) itemView.findViewById(R.id.ll_today_summary_main);
            tvUserTotal = (TextView) itemView.findViewById(R.id.text_user_total);
            viewEnquiry = (View) itemView.findViewById(R.id.enquiry_view);
            viewTd = (View) itemView.findViewById(R.id.td_view);
            viewVisit = (View) itemView.findViewById(R.id.visit_view);
            viewBooking = (View) itemView.findViewById(R.id.booking_view);
            viewReatil = (View) itemView.findViewById(R.id.retail_view);
            llinearLayout = (LinearLayout) itemView.findViewById(R.id.llinearLayout);
            llInner = (LinearLayout) itemView.findViewById(R.id.card_ll);

            imgUserCard = (ImageView) itemView.findViewById(R.id.img_user_card);
            tvNameCard = (TextView) itemView.findViewById(R.id.text_user_card);
            tvEnquiryCard = (TextView) itemView.findViewById(R.id.txt_enquiry_card);
            tvTestDriveCard = (TextView) itemView.findViewById(R.id.txt_td_card);
            tvVisitCard = (TextView) itemView.findViewById(R.id.txt_visit_card);
            tvBookingCard = (TextView) itemView.findViewById(R.id.txt_booking_card);
            tvRetailCard = (TextView) itemView.findViewById(R.id.txt_retail_card);
            tvUserTotalCard = (TextView) itemView.findViewById(R.id.text_user_total_card);
            viewEnquiryCard = (View) itemView.findViewById(R.id.enquiry_view_card);
            viewTdCard = (View) itemView.findViewById(R.id.td_view_card);
            viewVisitCard = (View) itemView.findViewById(R.id.visit_view_card);
            viewBookingCard = (View) itemView.findViewById(R.id.booking_view_card);
            viewReatilCard = (View) itemView.findViewById(R.id.retail_view_card);
            cardView = (CardView) itemView.findViewById(R.id.cardview_etvbr);
            llEnquriyCard = (LinearLayout) itemView.findViewById(R.id.enquiry_ll_card);
            tvTargetTitleCard = (TextView) itemView.findViewById(R.id.tv_target_title_card);
            tvTargetEnquiyCard = (TextView) itemView.findViewById(R.id.txt_target_enquiry_card);
            tvTargetTDCard = (TextView) itemView.findViewById(R.id.txt_target_td_card);
            tvTargetVisitCard = (TextView) itemView.findViewById(R.id.txt_target_visit_card);
            tvTargetBookingCard = (TextView) itemView.findViewById(R.id.txt_target_booking_card);
            tvTargetRetailCard = (TextView) itemView.findViewById(R.id.txt_target_retail_card);
            viewTargetCard = (View) itemView.findViewById(R.id.target_view_card);
            rlFrameLayoutCard = (RelativeLayout) itemView.findViewById(R.id.rl_frame_layout_card);
            llTdCard = (LinearLayout) itemView.findViewById(R.id.enquiry_td_card);
            llVisitCard = (LinearLayout) itemView.findViewById(R.id.enquiry_visit_card);
            llBookingCard = (LinearLayout) itemView.findViewById(R.id.enquiry_booking_card);
            llRetailsCard = (LinearLayout) itemView.findViewById(R.id.enquiry_retail_card);
            llLostCard = (LinearLayout) itemView.findViewById(R.id.enquiry_lost_card);
            tvLostCard = (TextView) itemView.findViewById(R.id.txt_lost_card);
            viewLostCard = (View) itemView.findViewById(R.id.lost_view_card);

        }
    }

    private void goToDetailsOfEtvbr(int position, String title_name, String user_id, String etvbrtotal, String user_role_id, String user_location_id) {

        Intent etvbrDetails =  null;

        switch (position){
            case 0:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Enquiries");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 1:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Test Drives");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 2:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Visits");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 3:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Bookings");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 4:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Retails");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 5:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Lost Drop");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
        }
        if(etvbrDetails != null)
            context.startActivity(etvbrDetails);
    }


}
