package com.salescrm.telephony.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.offline.AutoFetchFormData;
import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.offline.FetchLeadElements;
import com.salescrm.telephony.db.AllotDseDB;
import com.salescrm.telephony.db.EnqSourceDB;
import com.salescrm.telephony.db.IntrestedCarDetails;
import com.salescrm.telephony.db.LocationsDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.crm_user.TeamData;
import com.salescrm.telephony.db.crm_user.TeamUserDB;
import com.salescrm.telephony.db.crm_user.UserNewCarDB;
import com.salescrm.telephony.db.crm_user.UsersDB;
import com.salescrm.telephony.interfaces.AutoFetchFormListener;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.interfaces.FetchLeadElementsListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AddLeadElementsResponse;
import com.salescrm.telephony.response.DSETeamLeadsAndDSEsResponse;
import com.salescrm.telephony.response.GeneralResponse;
import com.salescrm.telephony.response.LeadTeamUserResponse;
import com.salescrm.telephony.response.SalesManagerDseResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.text.TextUtils.isEmpty;

/**
 * Created by bharath on 8/11/16.
 */

public class AllotDseActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener, FetchLeadElementsListener,
        FetchC360OnCreateLeadListener, AutoFetchFormListener {
    private int leadId = 0;
    private Realm realm;
    private RelativeLayout relProgress;
    private int apiCallCount = 0;
    private Preferences pref;
    private AutoCompleteTextView spinnerDseBranch, autoTvSalesManager, autoTvTeamLead, autoTvDseName;
    private ProgressDialog progressDialog;
    private int apiCallType = 0;
    private List<SalesManagerDseResponse.Result.Sales_managers> salesManagerData;
    private List<SalesManagerDseResponse.Result.Dses> dseData;
    private List<DSETeamLeadsAndDSEsResponse.Result.Dse_team_leads> teamLead;
    private List<DSETeamLeadsAndDSEsResponse.Result.Dses> dseFiltered;
    private String leadLastUpdated = "";
    private Button btSubmit;
    private int scheduledActivityId;

    private ArrayAdapter<Item> managerAdapter, teamLeadAdapter, dseAdapter;

    private Map<String, String> mangerMap, teamLeadMap, dseMap;

    private Item managerSelectedItem, teamLeadSelectedItem, dseSelectedItem;
    private AllotDseDB newAllotDse;
    private UserNewCarDB teamDateForLocation;
    private boolean isFromDone;
    private AllotDseDB allotDseDBOffline;
    private String generatedLeadId;
    private int leadNewCarId = 0;
    private int leadSource = 0;
    private boolean isFromC360 = false;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allot_dse);

        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(getApplicationContext());

        mangerMap = new HashMap<String, String>();
        teamLeadMap = new HashMap<String, String>();
        dseMap = new HashMap<String, String>();



       /* //
        RealmResults<TeamUserDB> teamUserDB = realm.where(TeamUserDB.class).findAll();
        System.out.println("DSEcount:"+teamUserDB.size());
        for(int i=0;i<teamUserDB.size();i++){
            System.out.println("DSE-LoactionId:"+teamUserDB.get(i).getLocation_id());
            System.out.println("DSE-TeamData:"+teamUserDB.get(i).getTeam_data().size());
            System.out.println("DSE-UserDB size:"+teamUserDB.get(i).getUsers().size());
        }
*/

        spinnerDseBranch = (AutoCompleteTextView) findViewById(R.id.spinner_lead_dse_branch);
        autoTvSalesManager = (AutoCompleteTextView) findViewById(R.id.spinner_lead_sales_man);
        autoTvTeamLead = (AutoCompleteTextView) findViewById(R.id.spinner_lead_team_lead);
        autoTvDseName = (AutoCompleteTextView) findViewById(R.id.spinner_lead_dse_name);
        relProgress = (RelativeLayout) findViewById(R.id.rel_add_lead_loader);
        btSubmit = (Button) findViewById(R.id.bt_add_lead_action);
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("isFieldsEmpty()" + isFieldsEmpty());
                if (isFieldsEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please enter valid data", Toast.LENGTH_SHORT).show();
                } else {
                    submitAllotDse();
                }
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        init();
    }

    private void init() {
        salesManagerData = new ArrayList<>();
        dseData = new ArrayList<>();
        teamLead = new ArrayList<>();
        dseFiltered = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            leadId = bundle.getInt("lead_id", Integer.parseInt(pref.getLeadID()));
            leadLastUpdated = bundle.getString("lead_last_updated", pref.getLead_last_updated());
            scheduledActivityId = bundle.getInt("scheduledActivityId", pref.getScheduledActivityId());
            isFromDone = bundle.getBoolean("from_done");
            isFromC360 = bundle.getBoolean("from_c360",false);
            if (isFromDone) {
                btSubmit.setText("Update");
            }

        }

        if (realm.where(EnqSourceDB.class).findAll().isEmpty() && new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
            getDseForLocationApi();
        } else {
            setDseLocationAdapter();
            if (isFromDone) {
                allotDseDBOffline = realm.where(AllotDseDB.class).equalTo("scheduledActivityId", scheduledActivityId).equalTo("is_synced", false).findFirst();
                if (allotDseDBOffline != null) {
                    spinnerDseBranch.setText(getDseLocationName(allotDseDBOffline.getLocationId()));
                    teamDateForLocation = getCrmUser();
                    // populateAdapters(teamDateForLocation);
                    setupAdapterForSaved();
                }
            }
        }


    }

    private void setupAdapterForSaved() {
        populateAdapters(teamDateForLocation);

        for (int i = 0; i < managerAdapter.getCount(); i++) {
            if (managerAdapter.getItem(i).getId() == allotDseDBOffline.getManager_id()) {
                managerSelectedItem = managerAdapter.getItem(i);
                autoTvSalesManager.setText(managerSelectedItem.getText());
                populateAdaptersForTeam(teamDateForLocation);
                break;
            }
        }

        for (int i = 0; i < (teamLeadAdapter == null ? 0 : teamLeadAdapter.getCount()); i++) {
            if (teamLeadAdapter.getItem(i).getId() == allotDseDBOffline.getDse_team_lead_id()) {
                teamLeadSelectedItem = teamLeadAdapter.getItem(i);
                autoTvTeamLead.setText(teamLeadSelectedItem.getText());
                populateAdaptersForDse(teamDateForLocation);


                break;
            }
        }

        for (int i = 0; i < (dseAdapter == null ? 0 : dseAdapter.getCount()); i++) {
            if (dseAdapter.getItem(i).getId() == allotDseDBOffline.getDseId()) {
                dseSelectedItem = dseAdapter.getItem(i);
                autoTvDseName.setText(dseSelectedItem.getText());
                // populateAdaptersForDse(teamDateForLocation);


                break;
            }
        }


    }

    private void setDseLocationAdapter() {
        relProgress.setVisibility(View.GONE);
        //setDseBranch
        // setSpinnerArrayAdapter(spinnerDseBranch,getDseLocationData());
        //setDseBranch
        // setSpinnerArrayAdapter(spinnerDseBranch,getDseLocationData());
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getDseLocationData());
        spinnerDseBranch.setAdapter(adapter);
        spinnerDseBranch.setThreshold(0);
        spinnerDseBranch.setOnFocusChangeListener(this);
        spinnerDseBranch.setOnClickListener(this);

        autoTvSalesManager.setThreshold(0);
        autoTvSalesManager.setOnFocusChangeListener(this);
        autoTvSalesManager.setOnClickListener(this);

        autoTvDseName.setThreshold(0);
        autoTvDseName.setOnFocusChangeListener(this);
        autoTvDseName.setOnClickListener(this);

        autoTvTeamLead.setThreshold(0);
        autoTvTeamLead.setOnFocusChangeListener(this);
        autoTvTeamLead.setOnClickListener(this);


        spinnerDseBranch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                autoTvSalesManager.setText("");
                autoTvTeamLead.setText("");
                autoTvDseName.setText("");

                // progressDialog.show();
                if (new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
                    leadSource = getLeadSource();
                    leadNewCarId = getPrimaryCarId();
                    progressDialog.show();
                    ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetTeamUserDetails(getLeadSource(), getPrimaryCarId(),
                            Util.getInt(getDseLocationId()), new Callback<LeadTeamUserResponse>() {
                                @Override
                                public void success(final LeadTeamUserResponse leadTeamUserResponse, Response response) {
                                    progressDialog.dismiss();
                                    Util.updateHeaders(response.getHeaders());

                                    if(leadTeamUserResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                                        ApiUtil.InvalidUserLogout(AllotDseActivity.this,0);
                                    }

                                    if (!leadTeamUserResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                        System.out.println("Success:0" + leadTeamUserResponse.getMessage());
                                        //showAlert(validateOtpResponse.getMessage())
                                        Toast.makeText(getApplicationContext(), "Data error", Toast.LENGTH_SHORT).show();
                                    } else {
                                        if (leadTeamUserResponse.getResult() != null) {
                                            System.out.println("Success:1" + leadTeamUserResponse.getMessage());
                                            realm.executeTransaction(new Realm.Transaction() {
                                                @Override
                                                public void execute(Realm realm1) {
                                                    insertData(realm1, leadTeamUserResponse.getResult());
                                                }
                                            });
                                            teamDateForLocation = getCrmUser();
                                            populateAdapters(teamDateForLocation);
                                            autoTvSalesManager.requestFocus();
                                        } else {

                                            // relLoader.setVisibility(View.GONE);
                                            System.out.println("Success:2" + leadTeamUserResponse.getMessage());
                                            Toast.makeText(getApplicationContext(), "Data error", Toast.LENGTH_SHORT).show();
                                            //showAlert(validateOtpResponse.getMessage());
                                        }
                                    }
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Data error", Toast.LENGTH_SHORT).show();
                                }
                            });
                } else {
                    leadSource = 0;
                    leadNewCarId = 0;
                    teamDateForLocation = getCrmUser();
                    populateAdapters(teamDateForLocation);
                    autoTvSalesManager.requestFocus();
                }
            }
        });
        autoTvSalesManager.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(AllotDseActivity.this);
                // progressDialog.show();
                //getTeamLeadsAndDSEs();
                managerSelectedItem = managerAdapter.getItem(position);
                autoTvTeamLead.setText("");
                autoTvDseName.setText("");
                populateAdaptersForTeam(teamDateForLocation);
                autoTvTeamLead.requestFocus();
            }
        });

        autoTvTeamLead.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(AllotDseActivity.this);
                // progressDialog.show();
                //getTeamLeadsAndDSEs();
                autoTvDseName.setText("");

                teamLeadSelectedItem = teamLeadAdapter.getItem(position);
         /*       System.out.println("Why not calling");*/
                populateAdaptersForDse(teamDateForLocation);
                autoTvDseName.requestFocus();
            }
        });

        autoTvDseName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(AllotDseActivity.this);
                dseSelectedItem = dseAdapter.getItem(position);
                populateDataOnDseSelect();
            }


        });
    }

    private int getLeadSource() {
        if (realm.where(EnqSourceDB.class).equalTo("name",
                realm.where(SalesCRMRealmTable.class).equalTo("leadId", leadId).findFirst().getLeadSourceName()).findFirst() != null) {
            return realm.where(EnqSourceDB.class).equalTo("name",
                    realm.where(SalesCRMRealmTable.class).equalTo("leadId", leadId).findFirst().getLeadSourceName()).findFirst().getId();
        }
        return 0;
    }

    private void populateDataOnDseSelect() {

        if (dseSelectedItem != null) {
            TeamData realmData = realm.where(TeamData.class).equalTo("user_id", dseSelectedItem.getId() + "").findFirst();
            if (realmData != null) {
                String managerId = realmData.getManager_id();
                String teamId = realmData.getTeam_leader_id();
                UsersDB realmManagerData = realm.where(UsersDB.class).equalTo("id", managerId).findFirst();
                UsersDB realmTeamData = realm.where(UsersDB.class).equalTo("id", teamId).findFirst();
                if (realmManagerData != null) {
                    managerSelectedItem = new Item(realmManagerData.getName(), realmManagerData.getId());
                    autoTvSalesManager.setText(managerSelectedItem.getText());
                }
                if (realmTeamData != null) {
                    teamLeadSelectedItem = new Item(realmTeamData.getName(), realmTeamData.getId());
                    autoTvTeamLead.setText(teamLeadSelectedItem.getText());
                    populateAdaptersForDse(teamDateForLocation);
                }
            }
        }
    }

    private UserNewCarDB getCrmUser() {

        return realm.where(TeamUserDB.class).equalTo("locationId", Util.getInt(getDseLocationId())).findFirst().
                getUserNewCarDBList().where().equalTo("leadNewCar", leadNewCarId).equalTo("leadSource", leadSource).findFirst();

    }


    private void getDseForLocationApi() {
        relProgress.setVisibility(View.VISIBLE);
        new FetchLeadElements(this, getApplicationContext()).call();
    }

    public void finishActivity(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if(isFromC360){
            startActivity(new Intent(AllotDseActivity.this,C360Activity.class));
        }
        super.onBackPressed();
    }

    private String[] getDseLocationData() {
        String[] arr = new String[realm.where(LocationsDB.class).findAll().size()];
        for (int i = 0; i < realm.where(LocationsDB.class).findAll().size(); i++) {
            arr[i] = realm.where(LocationsDB.class).findAll().get(i).getName();
        }
        return arr;
    }

    @Override
    public void onClick(View v) {
        if (v instanceof AutoCompleteTextView) {
            ((AutoCompleteTextView) v).showDropDown();
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus && v instanceof AutoCompleteTextView && isEmpty(((AutoCompleteTextView) v).getText().toString())) {
            ((AutoCompleteTextView) v).showDropDown();
        }
    }

    @Override
    public void onLeadElementsFetched(AddLeadElementsResponse addLeadElementsResponse) {
        setDseLocationAdapter();
    }

    @Override
    public void onFetchLeadElementsError(RetrofitError error) {
        System.out.println("Error");
        onErrorApiCallController(error);
    }

    private void onErrorApiCallController(RetrofitError error) {
        if (error.getKind() == RetrofitError.Kind.NETWORK) {
            apiCallCount++;
            if (apiCallCount <= WSConstants.API_MAX_CALL) {
                // new OnFailureGetToken(this, pref.getUserName(), pref.getPassword(), "").callGetToken();
                Toast.makeText(getApplicationContext(), "Server not responding,please try again", Toast.LENGTH_SHORT).show();
            } else {
                apiCallCount = 0;
            }
        } else {
            relProgress.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            Util.systemPrint("Something happened in the server");
            apiCallCount = 0;
        }
    }


    private void populateAdapters(UserNewCarDB data) {
        mangerMap.clear();
        dseMap.clear();
        if (data != null) {

            for (int i = 0; i < data.getTeam_data().size(); i++) {
                String managerId = data.getTeam_data().get(i).getManager_id();
                String teamLeadId = data.getTeam_data().get(i).getTeam_leader_id();
                String dseId = data.getTeam_data().get(i).getUser_id();
                RealmResults<UsersDB> userData = data.getUsers().where().equalTo("id", managerId).findAll();
                if (userData.size() > 0) {
                    mangerMap.put(managerId, userData.get(0).getName());
                }
                RealmResults<UsersDB> dseDataData = data.getUsers().where().equalTo("id", dseId).findAll();
                if (userData.size() > 0) {
                    dseMap.put(dseId, dseDataData.get(0).getName());
                }

            }
            List<Item> dataForTheAdapter = new ArrayList<Item>();
            for (Object o : mangerMap.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                dataForTheAdapter.add(new Item(pair.getValue().toString(), pair.getKey().toString()));
            }
            managerAdapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, dataForTheAdapter);
            autoTvSalesManager.setAdapter(managerAdapter);


            List<Item> dataForTheAdapterDse = new ArrayList<Item>();
            for (Object o : dseMap.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                dataForTheAdapterDse.add(new Item(pair.getValue().toString(), pair.getKey().toString()));
            }
            dseAdapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, dataForTheAdapterDse);
            autoTvDseName.setAdapter(dseAdapter);


//            autoTvSalesManager.requestFocus();
        }

    }


    private void populateAdaptersForTeam(UserNewCarDB data) {
        teamLeadMap.clear();
        if (data != null) {
            RealmResults<TeamData> teamData = data.getTeam_data().where().equalTo("manager_id", getSalesManagerId()).findAll();
            for (int i = 0; i < teamData.size(); i++) {
                String teamLeadId = teamData.get(i).getTeam_leader_id();
                RealmResults<UsersDB> userData = data.getUsers().where().equalTo("id", teamLeadId).findAll();
                if (userData.size() > 0) {
                    teamLeadMap.put(teamLeadId, userData.get(0).getName());
                }

            }
            List<Item> dataForTheAdapter = new ArrayList<Item>();
            for (Object o : teamLeadMap.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                dataForTheAdapter.add(new Item(pair.getValue().toString(), pair.getKey().toString()));
            }
            teamLeadAdapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, dataForTheAdapter);
            autoTvTeamLead.setAdapter(teamLeadAdapter);
//            autoTvTeamLead.requestFocus();
        }

    }

    private void populateAdaptersForDse(UserNewCarDB data) {
        System.out.println("Called");
        dseMap.clear();
        if (data != null) {
            System.out.println("Called");

            RealmResults<TeamData> teamData = data.getTeam_data().where().equalTo("manager_id", getSalesManagerId()).equalTo("team_leader_id", getTeamLeadId()).findAll();
            for (int i = 0; i < teamData.size(); i++) {
                String dseId = teamData.get(i).getUser_id();
                RealmResults<UsersDB> userData = data.getUsers().where().equalTo("id", dseId).findAll();
                if (userData.size() > 0) {
                    dseMap.put(dseId, userData.get(0).getName());
                }

            }
            List<Item> dataForTheAdapter = new ArrayList<Item>();
            for (Object o : dseMap.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                dataForTheAdapter.add(new Item(pair.getValue().toString(), pair.getKey().toString()));
            }
            dseAdapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, dataForTheAdapter);
            autoTvDseName.setAdapter(dseAdapter);
            // autoTvDseName.requestFocus();
        }

    }

    private String getDseLocationId() {
        if (realm.where(LocationsDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, spinnerDseBranch.getText().toString()).findAll().size() > 0) {
            return String.valueOf(realm.where(LocationsDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, spinnerDseBranch.getText().toString()).findFirst().getId());
        } else {
            return "";
        }
    }

    private String getDseLocationName(int id) {
        if (realm.where(LocationsDB.class).equalTo("id", id).findAll().size() > 0) {
            return String.valueOf(realm.where(LocationsDB.class).equalTo("id", id).findFirst().getName());
        } else {
            return "";
        }
    }

    private String getTeamLeadId() {
        if (TextUtils.isEmpty(autoTvTeamLead.getText().toString())) {
            return "";
        }
        if (teamLeadSelectedItem != null && autoTvTeamLead.getText().toString().equalsIgnoreCase(teamLeadSelectedItem.getText())) {
            return String.valueOf(teamLeadSelectedItem.getId());
        }
        return "";
    }

    private String getDseId() {
        if (TextUtils.isEmpty(autoTvDseName.getText().toString())) {
            return "";
        }
        if (dseSelectedItem != null && autoTvDseName.getText().toString().equalsIgnoreCase(dseSelectedItem.getText())) {
            return String.valueOf(dseSelectedItem.getId());
        }
        return "";
    }

    private String getSalesManagerId() {
        if (TextUtils.isEmpty(autoTvSalesManager.getText().toString())) {
            return "";
        }
        if (managerSelectedItem != null && autoTvSalesManager.getText().toString().equalsIgnoreCase(managerSelectedItem.getText())) {
            return String.valueOf(managerSelectedItem.getId());
        }
        return "";
    }


    private void submitAllotDse() {
        if (isFromDone) {
            AllotDseDB data = realm.where(AllotDseDB.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
            if (data != null) {
                realm.beginTransaction();
                data.setDataCreatedDate(Util.getDateTime(0));
                data.setLeadId(leadId);
                data.setLocationId(Util.getInt(getDseLocationId()));
                data.setDse_team_lead_id(Util.getInt(getTeamLeadId()));
                data.setDseId(Util.getInt(getDseId()));
                data.setManager_id(Util.getInt(getSalesManagerId()));
                data.setLeadLastUpdated(leadLastUpdated);
                realm.insertOrUpdate(data);
                realm.commitTransaction();
            }

            Toast.makeText(getApplicationContext(), "DSE Updated", Toast.LENGTH_LONG).show();
            AllotDseActivity.this.finish();
            return;


        }

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                AllotDseDB data = new AllotDseDB();
                data.setDataCreatedDate(Util.getDateTime(0));
                data.setScheduledActivityId(scheduledActivityId);
                data.setLeadId(leadId);
                data.setLocationId(Util.getInt(getDseLocationId()));
                data.setDse_team_lead_id(Util.getInt(getTeamLeadId()));
                data.setDseId(Util.getInt(getDseId()));
                data.setManager_id(Util.getInt(getSalesManagerId()));
                data.setLeadLastUpdated(leadLastUpdated);
                realm.insertOrUpdate(data);

            }
        });
        newAllotDse = realm.where(AllotDseDB.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
        if (new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
            Util.HideKeyboard(this);
            progressDialog.show();

            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).allotDSE(leadId + "",
                    getDseLocationId(), getTeamLeadId(), getDseId(), getSalesManagerId(), leadLastUpdated, new Callback<GeneralResponse>() {
                        @Override
                        public void success(GeneralResponse generalResponse, Response response) {
                            Util.updateHeaders(response.getHeaders());
                            progressDialog.dismiss();

                            if(generalResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                                ApiUtil.InvalidUserLogout(AllotDseActivity.this,0);
                            }

                            if (!generalResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                System.out.println("Success:0" + generalResponse.getMessage());
                                updateTaskEntry(false);
                                //showAlert(validateOtpResponse.getMessage());
                            }else {
                                if (generalResponse.getResult().equalsIgnoreCase("1")) {

                                    updateTaskEntry(true);
                                    System.out.println("Success:1" + generalResponse.getMessage());
                                    Toast.makeText(getApplicationContext(), "DSE Assigned and Synced", Toast.LENGTH_SHORT).show();
                                    fetchC360(leadId);

                                } else {
                                    updateTaskEntry(false);
                                    // relLoader.setVisibility(View.GONE);
                                    System.out.println("Success:2" + generalResponse.getMessage());
                                    //showAlert(validateOtpResponse.getMessage());
                                }
                            }


                        }

                        @Override
                        public void failure(RetrofitError error) {
                            updateTaskEntry(false);
                        }
                    });
        } else {

            System.out.println("leadId:");
            updateTaskEntry(false);
            Toast.makeText(getApplicationContext(), "DSE Assigned Sync Pending", Toast.LENGTH_LONG).show();
            AllotDseActivity.this.finish();

        }
    }

    private void fetchC360(int leadId) {
        progressDialog.setMessage("Please wait");
        progressDialog.show();
        this.generatedLeadId = leadId + "";
        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(generatedLeadId);
        new FetchC360InfoOnCreateLead(this, getApplicationContext(), leadData, pref.getCurrentDseId()).call(true);

    }

    @Override
    public void onFetchC360OnCreateLeadSuccess() {
        new AutoFetchFormData(this, getApplicationContext()).call(realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Util.getInt(generatedLeadId)).equalTo("isDone", false).findAll(), true);

    }

    @Override
    public void onFetchC360OnCreateLeadError() {

        // System.out.println(TAG+"Failed");
    }

    @Override
    public void onAutoFormsDataFetched(boolean b, RealmResults<SalesCRMRealmTable> data) {
        progressDialog.dismiss();
        pref.setLeadID("" + generatedLeadId);
        AllotDseActivity.this.finish();
    }

    @Override
    public void onAutoFormsDataError(RetrofitError error, int from) {
        Toast.makeText(getApplicationContext(), "Failed fetching form data", Toast.LENGTH_SHORT).show();
    }

    private void updateTaskEntry(final boolean b) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                SalesCRMRealmTable data = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
                data.setDone(true);
                newAllotDse.setIs_synced(b);
                data.setAllotDseAnswer(newAllotDse);
            }
        });
    }


    public boolean isFieldsEmpty() {
        return TextUtils.isEmpty(getDseLocationId())
                || TextUtils.isEmpty(getDseId())
                || TextUtils.isEmpty(getTeamLeadId())
                || TextUtils.isEmpty(getSalesManagerId());
    }

    public class Item {

        private String mText;
        private int mId;

        public Item(String text, String id) {

            this.mText = text;
            this.mId = Integer.parseInt(id);
        }

        public int getId() {

            return mId;
        }

        public void setId(String id) {

            this.mId = Integer.parseInt(id);
        }

        public String getText() {

            return mText;
        }

        public void setText(String text) {

            this.mText = text;
        }

        @Override
        public String toString() {

            return this.mText;
        }
    }

    private void insertData(Realm realm, LeadTeamUserResponse.Result t) {
        TeamUserDB teamUserDB = new TeamUserDB();
        teamUserDB.setLocationId(Util.getInt(getDseLocationId()));

        RealmList<UserNewCarDB> userNewCarDBRealmList = new RealmList<UserNewCarDB>();
        if (realm.where(TeamUserDB.class).equalTo("locationId", Util.getInt(getDseLocationId())).findFirst() != null) {
            userNewCarDBRealmList.addAll(realm.where(TeamUserDB.class).equalTo("locationId", Util.getInt(getDseLocationId())).findFirst().getUserNewCarDBList());
        }
        UserNewCarDB userNewCarDB = realm.createObject(UserNewCarDB.class);
        userNewCarDB.setLeadNewCar(getPrimaryCarId());
        userNewCarDB.setLeadSource(getLeadSource());

        RealmList<TeamData> teamUserDBData = new RealmList<TeamData>();
        for (int i = 0; i < t.getTeam_data().size(); i++) {
            TeamData current = realm.createObject(TeamData.class);
            current.setUser_id(t.getTeam_data().get(i).getUser_id());
            current.setManager_id(t.getTeam_data().get(i).getManager_id());
            current.setTeam_leader_id(t.getTeam_data().get(i).getTeam_leader_id());
            teamUserDBData.add(current);
        }

        RealmList<UsersDB> userDBData = new RealmList<UsersDB>();
        for (int i = 0; i < t.getUsers().size(); i++) {
            UsersDB current = realm.createObject(UsersDB.class);
            current.setId(t.getUsers().get(i).getId());
            current.setMobile_number(t.getUsers().get(i).getMobile_number());
            current.setName(t.getUsers().get(i).getName());
            userDBData.add(current);
        }

        userNewCarDB.setTeam_data(teamUserDBData);
        userNewCarDB.setUsers(userDBData);


        userNewCarDBRealmList.add(userNewCarDB);
        teamUserDB.setUserNewCarDBList(userNewCarDBRealmList);
        realm.insertOrUpdate(teamUserDB);
    }

    private int getPrimaryCarId() {
        IntrestedCarDetails intrestedCarDetails = realm.where(IntrestedCarDetails.class).equalTo("leadId", leadId).equalTo("intrestedCarIsPrimary", 1).findFirst();
        if (intrestedCarDetails != null) {
            return Util.getInt(intrestedCarDetails.getIntrestedCarModelId());
        }
        return 0;
    }
}
