package com.salescrm.telephony.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.model.StandardPickerModel;

import java.util.List;

/**
 * Created by bharath on 14/3/18.
 */

public class StandardPickerGridAdapter extends BaseAdapter {

    private final List<StandardPickerModel> standardPickerModels;
    private Context mContext;

    public StandardPickerGridAdapter(Context c,
                                     List<StandardPickerModel> standardPickerModels) {
        mContext = c;
        this.standardPickerModels = standardPickerModels;

    }

    @Override
    public int getCount() {
        return standardPickerModels.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;
        if (convertView == null) {
            gridView = new View(mContext);
            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.layout_grid_card, null);
            // set value into textview
            TextView textView = (TextView)
                    gridView.findViewById(R.id.tv_grid_caption);
            textView.setText(standardPickerModels.get(position).getTitle());
            // set image based on selected text
            ImageView imageView = (ImageView)
                    gridView.findViewById(R.id.img_grid);
            imageView.setImageResource(standardPickerModels.get(position).getImageSrc());
        } else {
            gridView = (View) convertView;
        }
        return gridView;
    }
}
