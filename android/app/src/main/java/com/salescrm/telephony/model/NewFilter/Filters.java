package com.salescrm.telephony.model.NewFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prateek on 4/8/18.
 */

public class Filters {

    private List<Values> values = new ArrayList<>();
    private String key;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Values> getValues() {
        return values;
    }

    public void setValues(List<Values> values) {
        this.values = values;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
