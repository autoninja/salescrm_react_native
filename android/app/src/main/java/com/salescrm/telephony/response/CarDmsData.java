package com.salescrm.telephony.response;

import com.salescrm.telephony.model.booking.ApiInputBookingDetails;

/**
 * Created by bannhi on 13/1/17.
 */

public class CarDmsData {


    private String enquiry_number;

    private String enquiry_id;

    private String booking_number;

    private String location_code;

    private String car_stage_id;

    private String invoice_number;

    private String invoice_id;

    private String booking_id;

    private String invoice_name;

    private String invoice_mob_no;

    private String invoice_vin_no;

    private String expected_delivery_date;

    private String booking_amount;

    private String delivery_number;

    private String delivery_date;

    private String invoice_date;


    private ApiInputBookingDetails booking_details;

    public ApiInputBookingDetails getBooking_details() {
        return booking_details;
    }

    public void setBooking_details(ApiInputBookingDetails booking_details) {
        this.booking_details = booking_details;
    }

    public String getInvoice_name() {
        return invoice_name;
    }

    public void setInvoice_name(String invoice_name) {
        this.invoice_name = invoice_name;
    }

    public String getInvoice_mob_no() {
        return invoice_mob_no;
    }

    public void setInvoice_mob_no(String invoice_mob_no) {
        this.invoice_mob_no = invoice_mob_no;
    }

    public String getInvoice_vin_no() {
        return invoice_vin_no;
    }

    public void setInvoice_vin_no(String invoice_vin_no) {
        this.invoice_vin_no = invoice_vin_no;
    }

    public String getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(String invoice_id) {
        this.invoice_id = invoice_id;
    }

    public String getBooking_amount() {
        return booking_amount;
    }

    public void setBooking_amount(String booking_amount) {
        this.booking_amount = booking_amount;
    }

    public String getExpected_delivery_date() {
        return expected_delivery_date;
    }

    public void setExpected_delivery_date(String expected_delivery_date) {
        this.expected_delivery_date = expected_delivery_date;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getEnquiry_number ()
    {
        return enquiry_number;
    }

    public void setEnquiry_number (String enquiry_number)
    {
        this.enquiry_number = enquiry_number;
    }

    public String getBooking_number ()
    {
        return booking_number;
    }

    public void setBooking_number (String booking_number)
    {
        this.booking_number = booking_number;
    }

    public String getLocation_code ()
    {
        return location_code;
    }

    public void setLocation_code (String location_code)
    {
        this.location_code = location_code;
    }

    public String getEnquiry_id() {
        return enquiry_id;
    }

    public void setEnquiry_id(String enquiry_id) {
        this.enquiry_id = enquiry_id;
    }

    public String getCar_stage_id() {
        return car_stage_id;
    }

    public void setCar_stage_id(String car_stage_id) {
        this.car_stage_id = car_stage_id;
    }

    public String getInvoice_number() {
        return invoice_number;
    }

    public void setInvoice_number(String invoice_number) {
        this.invoice_number = invoice_number;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [enquiry_number = "+enquiry_number+", booking_number = "+booking_number+", location_code = "+location_code+"]";
    }

    public String getDelivery_number() {
        return delivery_number;
    }

    public void setDelivery_number(String delivery_number) {
        this.delivery_number = delivery_number;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public String getInvoice_date() {
        return invoice_date;
    }

    public void setInvoice_date(String invoice_date) {
        this.invoice_date = invoice_date;
    }
}
