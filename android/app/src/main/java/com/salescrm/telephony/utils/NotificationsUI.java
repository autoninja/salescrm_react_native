package com.salescrm.telephony.utils;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.HomeActivity;
import com.salescrm.telephony.activity.SplashActivity;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;

import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by bharath on 14/11/17.
 */

public class NotificationsUI {

    public static final String CHANNEL_ID = "autoninja_notification";
    public static final String CHANNEL_ID_BACKGROUND = "autoninja_background";
    public static final String CHANNEL_ID_MAIN = "autoninja_background_main";
    public static final int BACKGROUND_ID = 12435;
    public static final int BACKGROUND_ID_MAIN = 124356;

    public static void showNotification(Context context, String title, String content) {
        int icon =  R.drawable.ic_notifications_white_24dp;

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE,WSConstants.DEFAULT_NOTIFICATION_BEFORE_TIME);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(content)
                .setAutoCancel(true)
                .setSmallIcon(icon)
                .setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.mipmap.ninja_crm)).getBitmap())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setColor(Color.parseColor("#FF0000"))
                .setPriority(Notification.PRIORITY_HIGH);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |
                PowerManager.ACQUIRE_CAUSES_WAKEUP |
                PowerManager.ON_AFTER_RELEASE, "WakeLock");
        wakeLock.acquire();

        Intent intent;
        intent = new Intent(context, HomeActivity.class);
        intent.putExtra(WSConstants.NOTIFICATION_CLICKED_NAME,title);
        intent.putExtra(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY,true);

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_TYPE, CleverTapConstants.EVENT_NOTIFICATION_TYPE_SENT);
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_FROM, CleverTapConstants.EVENT_NOTIFICATION_FROM_VALUE_IN_APP);
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_EVENT, title);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_NOTIFICATION, hashMap);


        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent activity = PendingIntent.getActivity(context, 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(activity);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager!=null &&Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(NotificationsUI.getNotificationChannel());
        }
        if (notificationManager != null) {
            notificationManager.notify(1, builder.build());
        }

    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static NotificationChannel getNotificationChannel() {
        CharSequence channelName = "NinjaCRM Sales";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, channelName, importance);
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.RED);
        notificationChannel.enableVibration(true);
        notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        return notificationChannel;
    }


    public static Notification showAlwaysForegroundNotification(Context context) {


        Intent notificationIntent = new Intent(context, SplashActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);
        NotificationCompat.Builder notificationBuilder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelName = "Call Recording";
            NotificationChannel chan = new NotificationChannel( NotificationsUI.CHANNEL_ID_MAIN, channelName, NotificationManager.IMPORTANCE_HIGH);
            chan.setLightColor(Color.parseColor("#53759D"));
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(chan);
            notificationBuilder = new NotificationCompat.Builder(context,  NotificationsUI.CHANNEL_ID_MAIN);

        }
        else {
            notificationBuilder =  new NotificationCompat.Builder(context, NotificationsUI.CHANNEL_ID_MAIN);
        }

        if (Build.VERSION.SDK_INT >= 17){
            notificationBuilder.setShowWhen(false);
        }

        notificationBuilder
                .setOngoing(true)
                .setContentTitle("Ninja Call Recording Service")
                .setContentText("Ensures NO Personal call recording")
                .setSmallIcon( R.drawable.ic_notifications_white_24dp)
                .setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.mipmap.ninja_crm)).getBitmap())
                .setColor(Color.parseColor("#53759D"))
                .setAutoCancel(false)
                .setContentIntent(pendingIntent);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setCategory(Notification.CATEGORY_SERVICE);
        }
        Notification notificationData = notificationBuilder.build();
        notificationData.flags |= Notification.FLAG_NO_CLEAR
                | Notification.FLAG_ONGOING_EVENT;
        return notificationData;

    }

    public static boolean isNotificationChannelEnabled(Context context, @Nullable String channelId){
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if(!TextUtils.isEmpty(channelId)) {
                    NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    NotificationChannel channel = manager.getNotificationChannel(channelId);
                    if(channel!=null) {
                        return channel.getImportance() != NotificationManager.IMPORTANCE_NONE;
                    }

                }
                return false;
            } else {
                return NotificationManagerCompat.from(context).areNotificationsEnabled();
            }
        }
        catch (Exception e) {
            return false;
        }

    }

}
