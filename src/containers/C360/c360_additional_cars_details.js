import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback, ScrollView
} from 'react-native'

const styles = StyleSheet.create({
    top_view: {
        alignItems: 'center',
    },
    inner_main_block: {
        backgroundColor: "#ffffff",
        alignSelf: 'stretch',
        padding: 10,
        borderRadius: 2,
        marginRight: 10,
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 10
    },
    view_straight_line: {
        width: '100%',
        height: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#a9a9a9',
        marginBottom: 5,
        marginTop: 5
    },
    car_name_title: {
        fontSize: 14,
        color: '#000000',
    },
    stage_name: {
        fontSize: 14,
        color: '#a9a9a9',
    }
});
export default class AdditionalCarsDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //interestedCarsDetails : this.props.interestedCarsDetails,
        }
    }

    render() {
        let additionalCarsDetails = this.props.additionalCarsDetails;
        return (
            <View style={styles.top_view}>
                {(additionalCarsDetails) ? additionalCarsDetails.map((item, index) => {
                    return (
                        <View style={styles.inner_main_block} key={index}>

                            <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                                <Text style={styles.car_name_title}>{item.name}</Text>
                                {/* <Image 
                                    style={{ width: 24, height: 24, alignItems: 'center', margin:10}}
                                    source={require('../../images/ic_edit_blue_24dp.png')} /> */}
                            </View>

                            {/* <View style={styles.view_straight_line}/>
                             <Text style={styles.stage_name}>{(item.test_drive_status == 0)? "Test Drive: Not Conducted" : "Test Drive: Conducted"}</Text> */}
                        </View>
                    )
                }) : null}
            </View>
        )
    }
}
