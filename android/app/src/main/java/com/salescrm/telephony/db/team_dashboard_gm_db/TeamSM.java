package com.salescrm.telephony.db.team_dashboard_gm_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 13/7/17.
 */

public class TeamSM extends RealmObject{

    @SerializedName("team_id")
    @Expose
    private Integer teamId;
    @SerializedName("team_name")
    @Expose
    private String teamName;
    @SerializedName("tl_dp_url")
    @Expose
    private String tlDpUrl;
    @SerializedName("abs")
    @Expose
    public RealmList<Absolute> abs = new RealmList<>();
    @SerializedName("rel")
    @Expose
    public RealmList<Relational> rel = new RealmList<>();

    @SerializedName("abs_total")
    @Expose
    private AbsTotal absTotal;
    @SerializedName("rel_total")
    @Expose
    private RelTotal relTotal;

    public AbsTotal getAbsTotal() {
        return absTotal;
    }

    public void setAbsTotal(AbsTotal absTotal) {
        this.absTotal = absTotal;
    }

    public RelTotal getRelTotal() {
        return relTotal;
    }

    public void setRelTotal(RelTotal relTotal) {
        this.relTotal = relTotal;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTlDpUrl() {
        return tlDpUrl;
    }

    public void setTlDpUrl(String tlDpUrl) {
        this.tlDpUrl = tlDpUrl;
    }

    public RealmList<Absolute> getAbs() {
        return abs;
    }

    public void setAbs(RealmList<Absolute> abs) {
        this.abs = abs;
    }

    public RealmList<Relational> getRel() {
        return rel;
    }

    public void setRel(RealmList<Relational> rel) {
        this.rel = rel;
    }

}
