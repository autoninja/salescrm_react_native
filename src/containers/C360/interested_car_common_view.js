import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback, ScrollView
} from 'react-native'

const styles = StyleSheet.create({
    top_view: {
        alignItems: 'center',
    },
    inner_main_block: {
        backgroundColor: "#ffffff",
        alignSelf: 'stretch',
        padding: 5,
        borderRadius: 2,
        marginRight: 10,
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 10,
    },
    view_straight_line: {
        width: '100%',
        height: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#a9a9a9',
        marginBottom: 10,
        marginTop: 10
    },
    car_name_title: {
        flex: 8,
        fontSize: 14,
        color: '#000000',
    },
    stage_name: {
        fontSize: 14,
        color: '#a9a9a9',
    },
    booking_detail: {
        padding: 5,
        borderRadius: 5,
        borderColor: '#2196f3',
        borderWidth: 1,
        marginRight: 10,
        marginBottom: 5,
        marginTop: 5,
    },
    test_drive: {
        backgroundColor: '#48b04b',
        padding: 5,
        borderRadius: 5,
        borderColor: '#48b04b',
        borderWidth: 1,
        marginRight: 10,
        marginBottom: 5,
        marginTop: 5,
    },
    button_next_stage: {
        width: 150,
        backgroundColor: '#2196f3',
        padding: 5,
        alignSelf: 'center',
        borderColor: '#2196f3',
        marginRight: 10,
        marginBottom: 5,
        marginTop: 5,
    },
});

const BOOK_CAR = 0;
const CAR_BOOKED_HALF_PAYMENT = 1;
const INVOICE_PENDING = 2;
const INVOICED_DELIVERY_PENDING = 3;
const DELIVERED = 4;
const CAR_CANCELLED = 5;

export default class InterestedCarsCommonView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //interestedCarsDetails : this.props.interestedCarsDetails,
        }
    }

    render() {
        let interestedCarsDetails = this.props.interestedCarsDetails;
        return (
            <View style={styles.inner_main_block} key={index}>

                <View style={{ flexDirection: 'row', marginBottom: 10, alignContent: 'center' }}>
                    {(item.is_primary == 1) ? <Image
                        style={{ flex: 1, width: 24, height: 24 }}
                        resizeMode='center'
                        source={require('../../images/fame_blue.png')} />
                        : <Image
                            style={{ flex: 1, width: 24, height: 24 }}
                            resizeMode='center'
                            source={require('../../images/fame_white.png')} />
                    }
                    <Text style={styles.car_name_title}>{item.name}</Text>
                    <Image
                        style={{ flex: 1, width: 24, height: 24 }}
                        resizeMode='center'
                        source={require('../../images/ic_edit_blue_24dp.png')} />
                </View>

                <View style={styles.view_straight_line} />

                <View style={{ flexDirection: 'row', alignContent: 'center' }}>

                    <Image
                        style={{ alignItems: 'flex-start' }}
                        source={require('../../images/booked_tag.png')} />

                    {/* <Image 
                                source={require('../../images/booked_tag.png')} />
                                <Text style={styles.car_name_title}>{item.name}</Text> */}

                    <Text style={{ fontSize: 14, alignItems: 'flex-end' }}>Cancel Booking</Text>
                </View>

                <Text style={{ fontSize: 14, alignItems: 'flex-end' }}>BD-01010101</Text>

                <View style={{ flexDirection: 'row', alignContent: 'center' }}>

                    <View style={styles.booking_detail, { flex: 1 }} >
                        <Text style={{ color: '#2196f3', fontSize: 12 }}>Booking Details</Text>
                    </View>
                    {/* <Image 
                                source={require('../../images/booked_tag.png')} />
                                <Text style={styles.car_name_title}>{item.name}</Text> */}

                    <View style={styles.test_drive, { flex: 1 }} >
                        <Text style={{ color: '#ffffff', fontSize: 12 }}>Test Drive / Visit</Text>
                    </View>
                </View>

                <Text style={styles.stage_name}>{(item.test_drive_status == 0) ? "Test Drive: Not Conducted" : "Test Drive: Conducted"}</Text>

                <TouchableOpacity style={styles.button_next_stage} onPress={null}>
                    <Text style={{ color: '#ffffff', fontSize: 14, alignSelf: 'center' }}>Update Delivery</Text>
                </TouchableOpacity>
            </View>
        )
    }
}