package com.nll.nativelibs.callrecording;

import android.content.Context;
import android.media.AudioRecord;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

/*
    Error list:

    ERR_NO_ERROR  0
    ERR_LIBRARY_ERROR                   1000
    ERR_SYSTEM_ERROR                    2000
    ERR_INVALID_PACKAGE                 3000
    ERR_INVALID_SIGNATURE               7000
    ERR_OBJECT_NULL                     4000
    ERR_INVALID_MEMORY_POINTER          5000
    ERR_LICENSE_EXPIRED                 6000
    ERR_NATIVE_CRASH                    9000


 */
public class AudioRecordWrapper extends AudioRecord {


    private static String TAG = "AudioRecordWrapper";
    private boolean useApi3;

    public AudioRecordWrapper(Context context, int audioSource, int sampleRateInHz, int channelConfig, int audioFormat, int bufferSizeInBytes, long serial, String key) {
        super(audioSource, sampleRateInHz, channelConfig, audioFormat, bufferSizeInBytes);
        useApi3 = DeviceHelper.useApi3();


        Log.i(TAG, "Remaining license time in seconds  " + Native.getExpiry(serial, key));
        Log.i(TAG, "Package and cert check result  " + Native.checkPackageAndCert(context));

        /* Fix android 7.1 issues. Must be called before start7. Not implemented for API3 and would crash if used together with start3()*/
        if (DeviceHelper.isAndroid71FixRequired()) {
            DeviceHelper.sleepForAndroid71();
            Native.fixAndroid71(Native.FIX_ANDROID_7_1_ON);
        }
        int result = useApi3 ? Native.start3(context, this, serial, key) : Native.start7(context, this, serial, key);
        Log.d(TAG, "Start result " + result);

    }

    @Override
    public void startRecording() {
        super.startRecording();
        Log.i(TAG, "Start recording");
        //Looper.getMainLooper() is really important! It might not work on some phones if you don't use it
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                int res = useApi3 ? Native.stop3() : Native.stop7();
                Log.d(TAG, "Stop result " + res);
            }
        }, 500);

    }
}