import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  NativeModules,
  Text
} from "react-native";
import { NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import { updateMainHeaderCurrentView } from "../../actions/main_header_actions";

class HeaderRight extends Component {
  state = {
    current_view: null,
    isEventTabActive: false,
    filters: []
  };
  constructor(props) {
    super(props);
    this.state = { isEventTabActive: false };
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.isEventTabActive != prevProps.isEventTabActive) {
      this.setState({ isEventTabActive: this.props.isEventTabActive });
    }
  }

  render() {
    let isClientILom = false;
    if(this.props.isClientILom || this.props.isClientILBank) {
      isClientILom = true;
    }
    return (
      <View
        style={{
          flexDirection: "row",
          margin: 10,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        {(this.props.isUserBH && !isClientILom) && (
          <TouchableOpacity
            onPress={() => {
              this.props.navigationProps.navigate("AddEvents", {
                isEdit: false
              });
            }}
          >
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginRight: 20
              }}
            >
              <Text style={{ color: "#fff", fontSize: 14 }}>Add Event</Text>
            </View>
          </TouchableOpacity>
        )}

        <TouchableOpacity
          hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
          onPress={() => {
            this.props.navigationProps.navigate("Search", {
              isEdit: false
            });
            //NativeModules.ReactNativeToAndroid.startAddSearchActivity();
          }}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Image
              style={{
                width: 20,
                height: 20,
                marginRight: 8,
                alignItems: "center"
              }}
              source={require("../../images/ic_search_white.png")}
            />

          </View>
        </TouchableOpacity>

        {this.props.showFilter && (
          <TouchableOpacity
            hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
            onPress={() => {
              this.props.navigationProps.navigate("TaskListFilter")
            }}
          >
            <View style={{ flexDirection: 'row' }}>
              <Image source={require('../../images/ic_etvbr_filter.png')}
                style={{
                  width: 20,
                  height: 20,
                  marginRight: 8,
                  marginLeft: 10,
                }} />
              {this.props.filters.length > 0 && (
                <View style={{ display: ('flex'), marginLeft: -14, marginTop: -6, backgroundColor: '#FFCA28', height: 16, width: 16, alignItems: 'center', borderRadius: 10, justifyContent: "center", alignItems: "center" }}>
                  <Text adjustsFontSizeToFit={true}
                    numberOfLines={1} style={{ textAlign: 'center', fontSize: 8, textAlignVertical: 'center', color: '#303030' }}>{this.props.filters.length}</Text>
                </View>
              )}

            </View>
          </TouchableOpacity>
        )
        }
      </View>
    );

  }
}
const mapStateToProps = state => {
  return {
    current_view: state.updateMainHeaderCurrentView.current_view,
    isEventTabActive: state.updateMainHeaderCurrentView.isEventTabActive,
    filters: state.tasksListReducer.filters
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setCurrentView: (current_view, isEventTabActive) => {
      dispatch(updateMainHeaderCurrentView({ current_view, isEventTabActive }));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderRight);
