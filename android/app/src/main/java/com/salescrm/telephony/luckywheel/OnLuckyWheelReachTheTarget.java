package com.salescrm.telephony.luckywheel;

/**
 * Created by nndra on 01-Mar-18.
 */

public interface OnLuckyWheelReachTheTarget {

    void onReachTarget();

}