package com.salescrm.telephony.db;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 16-08-2016.
 */
public class ActivityDetails extends RealmObject {

    public static final String ACTIVITYDETAILS = "ActivityDetails";

    @PrimaryKey
    private int scheduledActivityId;

    private int activityId;
    private int leadID;
    private int customerID;
    private String activityDescription;
    private Date activityScheduleDate;
    private String activityTypeId;
    private String activityType;
    private String activityName;
    private String activityGroupId;
    private String activityIconType;
    public RealmList<PlannedActivities> plannedActivities = new RealmList<>();
    public RealmList<LeadDetails> leadDetails = new RealmList<>();
    public RealmList<CustomerDetails> customerDetails = new RealmList<>();

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public int getLeadID() {
        return leadID;
    }

    public void setLeadID(int leadID) {
        this.leadID = leadID;
    }

    public int getScheduledActivityId() {
        return scheduledActivityId;
    }

    public void setScheduledActivityId(int scheduledActivityId) {
        this.scheduledActivityId = scheduledActivityId;
    }

    public String getActivityDescription() {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    public Date getActivityScheduleDate() {
        return activityScheduleDate;
    }

    public void setActivityScheduleDate(Date activityScheduleDate) {
        this.activityScheduleDate = activityScheduleDate;
    }

    public String getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(String activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityGroupId() {
        return activityGroupId;
    }

    public void setActivityGroupId(String activityGroupId) {
        this.activityGroupId = activityGroupId;
    }

    public String getActivityIconType() {
        return activityIconType;
    }

    public void setActivityIconType(String activityIconType) {
        this.activityIconType = activityIconType;
    }

}
