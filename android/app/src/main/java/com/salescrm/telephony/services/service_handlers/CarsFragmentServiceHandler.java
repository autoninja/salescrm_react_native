package com.salescrm.telephony.services.service_handlers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.gamification.BadgesNotificationActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.PlannedActivities;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.car.AllInterestedCars;
import com.salescrm.telephony.db.car.CarsDBHandler;
import com.salescrm.telephony.db.car.ExchangeCarDetails;
import com.salescrm.telephony.db.crm_user.EvaluatorsDB;
import com.salescrm.telephony.fragments.NewCarsFragment;
import com.salescrm.telephony.interfaces.PostBookingDbHandler;
import com.salescrm.telephony.model.BookCarModel;
import com.salescrm.telephony.model.C360InformationModel;
import com.salescrm.telephony.model.EditCarModel;
import com.salescrm.telephony.model.EvaluationSubmitRequestModel;
import com.salescrm.telephony.model.ExchangeCarEditModel;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.model.LeadLastUpdateError;
import com.salescrm.telephony.model.UpdateDeliveryModel;
import com.salescrm.telephony.model.UpdateInvoiceModel;
import com.salescrm.telephony.model.booking.ApiInputBookingDetails;
import com.salescrm.telephony.model.booking.BookingMainModel;
import com.salescrm.telephony.model.booking.CarBookingFormHolderModel;
import com.salescrm.telephony.model.booking.FullPaymentReceivedInput;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.BookCarResponse;
import com.salescrm.telephony.response.CancelBookingResponse;
import com.salescrm.telephony.response.CancelInvoiceResponse;
import com.salescrm.telephony.response.CancelPaymentReponse;
import com.salescrm.telephony.response.EvaluatorNamesListResponse;
import com.salescrm.telephony.response.FormSubmissionActivityResponse;
import com.salescrm.telephony.response.FullPaymentRecievedResponse;
import com.salescrm.telephony.response.InterestedCarEditPrimaryResponse;
import com.salescrm.telephony.response.RescheduleTaskResponse;
import com.salescrm.telephony.response.SaveCarDetailsResponse;
import com.salescrm.telephony.response.SaveExchangeCarEditResponse;
import com.salescrm.telephony.response.UpdateDeliveryResponse;
import com.salescrm.telephony.response.UpdateInvoiceResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by bannhi on 15/6/17.
 */

public class CarsFragmentServiceHandler implements PostBookingDbHandler {

    ConnectionDetectorService connectionDetectorService;
    static Activity activity;
    Realm realm;
    static String errorMessage = "Please try again.";


    static CarsFragmentServiceHandler newInstance = new CarsFragmentServiceHandler();

    public static CarsFragmentServiceHandler getInstance(Activity ctx) {
        activity = ctx;
        // realm = rlm;
        if (newInstance == null)
            newInstance = new CarsFragmentServiceHandler();
        return newInstance;
    }

    private CarsFragmentServiceHandler() {

    }


    public void markCarPrimary(final String accessToken, final String leadId, final String leadCarId, final String leadLastUpdated) {
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).makePrimaryInterestedCar(leadId, leadCarId, leadLastUpdated, new Callback<InterestedCarEditPrimaryResponse>() {
                @Override
                public void success(InterestedCarEditPrimaryResponse interestedCarEditPrimaryResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());

                        }
                    }
                    if (interestedCarEditPrimaryResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        callC360Activity(true,"");
                    } else {
                        callC360Activity(false,interestedCarEditPrimaryResponse.getError().getDetails());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    SalesCRMApplication.getBus().post(true);
                }
            });

        } else {

        }
    }

    /**
     * api to book a car
     * @param bookCarModelOb
     * @param carID
     * @param accesstoken
     * @param carBookingCallBack
     * @param pref
     */
    public void callBookCarApi(final BookCarModel bookCarModelOb, final String carID, final String leadId, String accesstoken, final CarBookingCallBack carBookingCallBack, final Preferences pref) {
      /*  BookCarResponse bookCarResponseTest = new BookCarResponse();
        SalesCRMApplication.getBus().post(bookCarResponseTest);*/
        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        System.out.println("book car api is called 1- " +bookCarModelOb.getForm_object());
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accesstoken).bookCar(bookCarModelOb, new Callback<BookCarResponse>() {
                @Override
                public void success(final BookCarResponse bookCarResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }
                    if (bookCarResponse != null && bookCarResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {


                        if (bookCarResponse.getResult() != null &&
                                bookCarResponse.getResult().getBooking_details() != null &&
                                bookCarResponse.getResult().getBooking_details().length > 0) {

                            for (int i = 0; i < bookCarResponse.getResult().getScheduled_activity_ids().length; i++) {
                                Util.postBookingDbHandler(realm,
                                        CarsFragmentServiceHandler.this,
                                        bookCarResponse.getResult().getScheduled_activity_ids()[i],
                                        bookCarResponse.getResult().getAction_id(),
                                        -1);

                            }

                            AllInterestedCars allInterestedCar = realm.where(AllInterestedCars.class)
                                    .equalTo("carId", carID)
                                    .findFirst();
                            if (allInterestedCar != null && allInterestedCar.isValid()) {
                                realm.beginTransaction();
                                allInterestedCar.deleteFromRealm();
                                realm.commitTransaction();

                            }
                            SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                                    .equalTo("leadId", Integer.parseInt(leadId))
                                    .findFirst();
                            if(salesCRMRealmTable!=null && salesCRMRealmTable.isValid()){
                                realm.beginTransaction();
                                salesCRMRealmTable.setLeadLastUpdated(bookCarResponse.getResult().getLead_last_updated());
                                realm.commitTransaction();
                            }
                        }

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                if (bookCarModelOb.getForm_object() != null) {
                                    int scheduledActivityId = Util.getInt(bookCarModelOb.getForm_object().getScheduled_activity_id());
                                    SalesCRMRealmTable data = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
                                    PlannedActivities plannedActivitiesData = realm.where(PlannedActivities.class).equalTo("plannedActivityScheduleId", scheduledActivityId).findFirst();
                                    if (data != null) {
                                        data.setDone(true);
                                        if (plannedActivitiesData != null) {
                                            plannedActivitiesData.setDone(true);
                                        }
                                    }
                                }

                            }
                        });

                        if(bookCarResponse.getResult().getGame_data()!= null && pref.getmGameLocationId()!=-1) {
                            if(bookCarResponse.getResult().getGame_data().getBadge_details()!= null && bookCarResponse.getResult().getGame_data().getBadge_details().size()!=0){
                                final String jsonString = new Gson().toJson(bookCarResponse.getResult().getGame_data()).toString();
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //fetchC360(Util.getInt(formSubmissionInputData.getLead_id()));
                                        callBadgesActivity(jsonString);
                                        //finish();
                                    }
                                }, 2000);
                            }
                            if (bookCarResponse.getResult().getGame_data().getPoints_earned()!= null
                                    && !bookCarResponse.getResult().getGame_data().getPoints_earned().equalsIgnoreCase("0")) {
                                showPointsToaster(bookCarResponse.getResult().getGame_data().getPoints_earned(), bookCarResponse.getResult().getGame_data().getActivity());
                                carBookingCallBack.onCallBookingCallBack(true, "Car Booked");
                            }
                        }else {
                            carBookingCallBack.onCallBookingCallBack(true, "Car Booked");
                        }
                        carBookingCallBack.onCallBookingCallBack(true, "Car Booked");

                    }else{
                        carBookingCallBack.onCallBookingCallBack(false,(bookCarResponse==null || bookCarResponse.getError()==null)?errorMessage: bookCarResponse.getError().getDetails());
                    }

                }

                @Override
                public void failure(RetrofitError error) {

                    error.printStackTrace();
                    //SalesCRMApplication.getBus().post(true);
                   // Log.e("book car api is ","-"+error.toString());
                    System.out.println("book car api is called 1- " +error.toString());
                    tryUpdateLeadLastUpdatedOnError(error);
                    carBookingCallBack.onCallBookingCallBack(false,"Please try again");

                }
            });
        }


    }

    private void callBadgesActivity(String jsonString) {
        Intent intent = new Intent(activity, BadgesNotificationActivity.class);
        intent.putExtra("badges_notification", jsonString);
        activity.startActivity(intent);
    }

    private void showPointsToaster(String points, String activityName){
        LayoutInflater llayout = activity.getLayoutInflater();
        View layout = llayout.inflate(R.layout.game_toast_points,
                (ViewGroup) activity.findViewById(R.id.custom_toast_layout));
        TextView tvPoint = (TextView) layout.findViewById(R.id.custom_toast_message_one);
        String pointString = "<b><big>+"+points+"</big></b><small> Points<small>";
        tvPoint.setText(Util.fromHtml(pointString));
        TextView tvTitleActivity = (TextView) layout.findViewById(R.id.tv_title_activity);
        tvTitleActivity.setText(activityName);
        Toast toast = new Toast(activity);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP|Gravity.FILL_HORIZONTAL, 0, 200);
        toast.setView(layout);//setting the view of custom toast layout
        toast.show();
    }

    private void tryUpdateLeadLastUpdatedOnError(RetrofitError error){
        try {
            String json =  new String(((TypedByteArray)error.getResponse().getBody()).getBytes());
            LeadLastUpdateError errorResponse = new Gson().fromJson(json, LeadLastUpdateError.class);
            System.out.println("Response:"+errorResponse.getError().getDetails().getLead_last_updated());
            if(errorResponse.getError().getDetails().getLead_last_updated()!=null) {
                C360InformationModel.getInstance().setLeadLastUpdated(errorResponse.getError().getDetails().getLead_last_updated());
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public void callEditBookingApi(final EditCarModel editCarModel, final String carID, final String leadId, String accesstoken, final CarBookingCallBack carBookingCallBack) {
        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accesstoken).editBookingDetails(editCarModel, new Callback<BookCarResponse>() {
                @Override
                public void success(final BookCarResponse bookCarResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }
                    if (bookCarResponse != null && bookCarResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {


                        if (bookCarResponse.getResult() != null &&
                                bookCarResponse.getResult().getBooking_details() != null &&
                                bookCarResponse.getResult().getBooking_details().length > 0) {

                            AllInterestedCars allInterestedCar = realm.where(AllInterestedCars.class)
                                    .equalTo("carId", carID)
                                    .findFirst();
                            if (allInterestedCar != null && allInterestedCar.isValid()) {
                                realm.beginTransaction();
                                allInterestedCar.deleteFromRealm();
                                realm.commitTransaction();

                            }
                            SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                                    .equalTo("leadId", Integer.parseInt(leadId))
                                    .findFirst();
                            if(salesCRMRealmTable!=null && salesCRMRealmTable.isValid()){
                                realm.beginTransaction();
                                salesCRMRealmTable.setLeadLastUpdated(bookCarResponse.getResult().getLead_last_updated());
                                realm.commitTransaction();
                            }
                        }

                        carBookingCallBack.onCallBookingCallBack(true, "Car Edited");


                    }else{

                        carBookingCallBack.onCallBookingCallBack(false,(bookCarResponse==null || bookCarResponse.getError()==null)?errorMessage: bookCarResponse.getError().getDetails());

                        // SalesCRMApplication.getBus().post(true);
                    }

                }

                @Override
                public void failure(RetrofitError error) {
                    error.printStackTrace();
                    System.out.println("book car api is called 1- " +error.toString());
                    tryUpdateLeadLastUpdatedOnError(error);
                    carBookingCallBack.onCallBookingCallBack(false,"Please try again");

                }
            });
        }


    }

    /**
     * cancelling a payment
     *
     * @param leadId
     * @param leadCarId
     * @param bookingId
     * @param stageId
     * @param enquiryId
     */
    public void callCancelPaymentApi(final String accessToken, final long leadId, final int leadCarId, final int bookingId,
                                     final int stageId, final int enquiryId, final String leadLastUpdated) {
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).cancelPayment(bookingId,
                    enquiryId, leadCarId, leadId, stageId,leadLastUpdated, new Callback<CancelPaymentReponse>() {
                        @Override
                        public void success(final CancelPaymentReponse cancelPaymentReponse, Response response) {
                            System.out.println("Accesstoken" + accessToken + "SWIFT" + null);
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }
                            // SaveCarDetailsResponse savecardetailsresponse = (SaveCarDetailsResponse) saveCarDetailsResponse;
                            if (cancelPaymentReponse != null && cancelPaymentReponse.getStatusCode() != null
                                    && cancelPaymentReponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {

                                //SalesCRMApplication.getBus().post(activity);
                                Util.postBookingDbHandler(realm, CarsFragmentServiceHandler.this,
                                        cancelPaymentReponse.getResult().getScheduled_activity_id(),
                                        cancelPaymentReponse.getResult().getAction_id(),
                                        cancelPaymentReponse.getResult().getActivity_id());

                                SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                                        .equalTo("leadId", (int)leadId)
                                        .findFirst();
                                if(salesCRMRealmTable!=null && salesCRMRealmTable.isValid()){
                                    realm.beginTransaction();
                                    salesCRMRealmTable.setLeadLastUpdated(cancelPaymentReponse.getResult().getLead_last_updated());
                                    realm.commitTransaction();
                                }
                                callC360Activity(true,"");
                            }else{
                               // SalesCRMApplication.getBus().post(true);
                                callC360Activity(false,cancelPaymentReponse.getError().getDetails());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            //SalesCRMApplication.getBus().post(true);
                         //   Log.e("edit car api", "edit car - " + error.getMessage());
                            callC360Activity(false,errorMessage);
                        }
                    });
        }

    }

    /**
     * edit exchange car api
     *
     *
     * @param accessToken
     * @param exchangeCarEditModel
     */
    public void editExchangeCarApiCall(final String accessToken, final ExchangeCarEditModel exchangeCarEditModel) {
        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).SaveEdittedExchangeCar(exchangeCarEditModel,
                    new Callback<SaveExchangeCarEditResponse>() {
                        @Override
                        public void success(final SaveExchangeCarEditResponse saveEditExchangeCarResponse, Response response) {
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }
                            if (saveEditExchangeCarResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                //  SalesCRMApplication.getBus().post(activity);
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        CarsDBHandler.getInstance().updateExchangeEdittedCar(realm, exchangeCarEditModel);
                                    }
                                });

                                callC360Activity(true, "");
                            } else {
                                // SalesCRMApplication.getBus().post(true);
                                callC360Activity(false, saveEditExchangeCarResponse.getError().getDetails());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                           // SalesCRMApplication.getBus().post(true);
                            callC360Activity(false,errorMessage);
                        }

                    });
        }

        }

    /**
     * api to call for edit car
     *
     * @param leadId
     * @param leadCarId
     * @param modelId
     * @param variantId
     * @param variantName
     * @param colorId
     * @param color
     * @param fuelId
     * @param fuel
     * @param bookingID
     * @param amtReceived
     * @param accessToken
     * @param carId
     */
    public void callEditCarApi(String leadId, final String leadCarId, String modelId, final String variantId,
                               final String variantName, final String colorId, final String color,
                               final String fuelId, final String fuel, final String bookingID,
                               final String amtReceived, final String accessToken, final String carId) {
        realm = Realm.getDefaultInstance();

        System.out.println("book car api is called 1- " +leadId + " "+leadCarId+" "+modelId+" "+variantId+" "+variantName+" "
        +colorId+" "+color+" "+fuelId+" "+fuel+" "+bookingID+" "+amtReceived+" "+accessToken+
        " "+carId);

        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).saveEdittedCarDetails(leadId,
                    leadCarId, modelId, variantId, colorId, fuelId, bookingID, amtReceived, new Callback<SaveCarDetailsResponse>() {
                        @Override
                        public void success(final SaveCarDetailsResponse saveCarDetailsResponse, Response response) {
                            System.out.println("Accesstoken" + accessToken + "SWIFT" + null);
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                Log.e("saveeditcardetails", "savecar -  " + header.getName() + " - " + header.getValue());
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }
                            // SaveCarDetailsResponse savecardetailsresponse = (SaveCarDetailsResponse) saveCarDetailsResponse;
                            if (saveCarDetailsResponse != null && saveCarDetailsResponse.getStatusCode() != null
                                    && saveCarDetailsResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                //SalesCRMApplication.getBus().post(activity);
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        CarsDBHandler.getInstance().updateEdittedCarToDB(realm, variantName,
                                                variantId, colorId, color, fuelId, fuel, carId, bookingID, amtReceived);
                                    }
                                });
                                callC360Activity(true,"");
                            }else{
                                //SalesCRMApplication.getBus().post(true);
                                callC360Activity(false,saveCarDetailsResponse.getError().getDetails());
                                System.out.println("book car api is called 1- " +saveCarDetailsResponse.getStatusCode());
                                System.out.println("book car api is called 1- " +saveCarDetailsResponse.getError().getDetails());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            //Log.e("edit car api", "edit car - " + error.getMessage());
                            //SalesCRMApplication.getBus().post(true);
                            System.out.println("book car api is called 1- " +error.toString());
                            callC360Activity(false,errorMessage);
                        }
                    });
        }
    }

    /**
     * api to cancel a booking
     *
     * @param accessToken
     * @param bookingId
     * @param leadCarId
     * @param locationID
     * @param leadId
     * @param stageId
     */
    public void cancelBooking(final String accessToken, final int bookingId, final int leadCarId, final int locationID, final long leadId, final int stageId,
                              final String presentCarID, final int enquiryId, final String reason, final String invoiceId, final String leadLastUpdated) {

        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).cancelBooking(bookingId,invoiceId,
                    leadCarId, locationID, enquiryId, leadId, stageId, reason, leadLastUpdated, new Callback<CancelBookingResponse>() {
                        @Override
                        public void success(final CancelBookingResponse cancelBookingResponse, Response response) {
                            System.out.println("Accesstoken" + accessToken + "SWIFT" + null);
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }
                            if (cancelBookingResponse != null && cancelBookingResponse.getStatusCode() != null
                                    && cancelBookingResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) && cancelBookingResponse.getResult() != null) {
                                //SalesCRMApplication.getBus().post(activity);

                                if(cancelBookingResponse.getResult().getScheduled_activity_ids().length
                                        == cancelBookingResponse.getResult().getActivity_ids().length) {
                                    for (int i = 0; i < cancelBookingResponse.getResult().getScheduled_activity_ids().length; i++) {
                                        Util.postBookingDbHandler(realm,
                                                CarsFragmentServiceHandler.this,
                                                cancelBookingResponse.getResult().getScheduled_activity_ids()[i],
                                                cancelBookingResponse.getResult().getAction_id(),
                                                cancelBookingResponse.getResult().getActivity_ids()[i]);

                                    }

                                }
                                SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                                        .equalTo("leadId",(int)leadId)
                                        .findFirst();
                                if(salesCRMRealmTable!=null && salesCRMRealmTable.isValid()){
                                    realm.beginTransaction();
                                    salesCRMRealmTable.setLeadLastUpdated(cancelBookingResponse.getResult().getLead_last_updated());
                                    realm.commitTransaction();
                                }
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        AllInterestedCars allInterestedCars = realm.where(AllInterestedCars.class)
                                                .equalTo("carId", "" + presentCarID)
                                                .findFirst();

                                        if (allInterestedCars != null && !cancelBookingResponse.getResult().getNext_stage_id().isEmpty()) {
                                            allInterestedCars.setCar_stage_id(cancelBookingResponse.getResult().getNext_stage_id());
                                        }
                                        //realm.copyToRealmOrUpdate(allInterestedCars);
                                    }
                                });
                                callC360Activity(true,"");
                            }else{
                                //SalesCRMApplication.getBus().post(true);
                                callC360Activity(false,cancelBookingResponse.getError().getDetails());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                           // Log.e("Cancel booking", "cancel booking - " + error.getMessage());
                           // SalesCRMApplication.getBus().post(true);
                            callC360Activity(false,errorMessage);
                        }
                    });
        }

    }

    /**
     * @param leadId
     * @param accessToken
     */
    public void getEvaluationList(long leadId, final String accessToken) {
        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).getAllEvaluatorNames(leadId,
                    new Callback<EvaluatorNamesListResponse>() {
                        @Override
                        public void success(final EvaluatorNamesListResponse evaluatorNamesListResponse, Response response) {
                            System.out.println("Accesstoken" + accessToken + "SWIFT" + null);
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }

                            if (evaluatorNamesListResponse != null && evaluatorNamesListResponse.getStatusCode() != null
                                    && evaluatorNamesListResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) &&
                                    evaluatorNamesListResponse.getResult() != null) {

                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        for (int i = 0; i < evaluatorNamesListResponse.getResult().size(); i++) {
                                            EvaluatorsDB evaluatorsDB = new EvaluatorsDB();
                                            evaluatorsDB.setEvaluatorName(evaluatorNamesListResponse.getResult().get(i).getUser_name());
                                            evaluatorsDB.setEvaluatorId(evaluatorNamesListResponse.getResult().get(i).getUser_id());
                                            realm.copyToRealmOrUpdate(evaluatorsDB);

                                        }
                                    }
                                });


                            }

                        }

                        @Override
                        public void failure(RetrofitError error) {
                        }
                    });
        }

    }

    /**
     * submit exchange car evaluation form
     *
     * @param evaluationSubmitRequestModel
     * @param accessToken
     */
    public void evaluationFormSubmit(EvaluationSubmitRequestModel evaluationSubmitRequestModel, final String accessToken) {
        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).submitEvaluationForm(evaluationSubmitRequestModel,
                    new Callback<FormSubmissionActivityResponse>() {
                        @Override
                        public void success(final FormSubmissionActivityResponse formSubmissionActivityResponse, Response response) {
                            System.out.println("Accesstoken" + accessToken + "SWIFT" + null);
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }

                            if (formSubmissionActivityResponse != null && formSubmissionActivityResponse.getStatusCode() != null
                                    && formSubmissionActivityResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) &&
                                    formSubmissionActivityResponse.getResult() != null) {
                               // SalesCRMApplication.getBus().post(activity);
                                callC360Activity(true,"");
                            }else{
                                //SalesCRMApplication.getBus().post(true);
                                callC360Activity(false,formSubmissionActivityResponse.getError().getDetails());
                            }

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            //SalesCRMApplication.getBus().post(true);
                            callC360Activity(false,errorMessage);
                        }
                    });
        }

    }

    /**
     * api to cancel an invoice
     *
     * @param accessToken
     * @param bookingId
     * @param leadCarId
     * @param locationID
     * @param leadId
     * @param stageId
     */
    public void cancelInvoice(final String accessToken, final int bookingId, final int leadCarId,
                              final int locationID, final long leadId, final int stageId,
                              final String presentCarID, final int enquiryId, final int invoiceId, final String leadLastUpdated) {
        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).cancelInvoice(bookingId,
                    leadCarId, locationID, enquiryId, leadId, stageId, invoiceId,leadLastUpdated, new Callback<CancelInvoiceResponse>() {
                        @Override
                        public void success(final CancelInvoiceResponse cancelInvoiceResponse, Response response) {
                            System.out.println("Accesstoken" + accessToken + "SWIFT" + null);
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }
                            //CancelBookingResponse cancelBookingResponse1 = (CancelBookingResponse) saveCarDetailsResponse;
                            if (cancelInvoiceResponse != null && cancelInvoiceResponse.getStatusCode() != null
                                    && cancelInvoiceResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) && cancelInvoiceResponse.getResult() != null) {
                               // SalesCRMApplication.getBus().post(activity);

                                  if(cancelInvoiceResponse.getResult()!=null) {
                                    Util.postBookingDbHandler(realm, CarsFragmentServiceHandler.this,
                                            cancelInvoiceResponse.getResult().getScheduled_activity_id(),
                                            cancelInvoiceResponse.getResult().getAction_id(),
                                            cancelInvoiceResponse.getResult().getActivity_id());
                                    }
                                SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                                        .equalTo("leadId", (int)leadId)
                                        .findFirst();
                                if(salesCRMRealmTable!=null && salesCRMRealmTable.isValid()){
                                    realm.beginTransaction();
                                    salesCRMRealmTable.setLeadLastUpdated(cancelInvoiceResponse.getResult().getLead_last_updated());
                                    realm.commitTransaction();
                                }
                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        AllInterestedCars allInterestedCars = realm.where(AllInterestedCars.class)
                                                .equalTo("carId", presentCarID)
                                                .findFirst();

                                        if (allInterestedCars != null && !cancelInvoiceResponse.getResult().getNext_stage_id().isEmpty()) {
                                            allInterestedCars.setCar_stage_id(cancelInvoiceResponse.getResult().getNext_stage_id());

                                        }
                                        // realm.copyToRealmOrUpdate(allInterestedCars);
                                    }
                                });
                                callC360Activity(true,"");
                            }else{
                               // SalesCRMApplication.getBus().post(true);
                                callC360Activity(false,cancelInvoiceResponse.getError().getDetails());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            //Log.e("CancelInvoice", "cancel invoice - " + error.getMessage());
                            //SalesCRMApplication.getBus().post(true);
                            callC360Activity(false,errorMessage);
                        }
                    });
        }

    }

    /**
     * api to cancel an invoice
     *
     * @param accessToken
     * @param bookingId
     * @param leadCarId
     * @param leadId
     * @param stageId
     */
    public void rescheduleTask(final String accessToken, final int bookingId, final int leadCarId,
                               final long leadId, final int stageId, final String presentCarID, final int enquiryId,
                               final int invoiceId, final String date, String remarks) {
        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).rescheduleTask(date, bookingId, enquiryId,
                    leadCarId, leadId, stageId, invoiceId, remarks,  new Callback<RescheduleTaskResponse>() {
                        @Override
                        public void success(final RescheduleTaskResponse rescheduleTaskResponse, Response response) {
                            System.out.println("Accesstoken" + accessToken + "SWIFT" + null);
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }
                            //CancelBookingResponse cancelBookingResponse1 = (CancelBookingResponse) saveCarDetailsResponse;
                            if (rescheduleTaskResponse != null && rescheduleTaskResponse.getStatusCode() != null
                                    && rescheduleTaskResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                                // will see if we have to do anything;
                                // SalesCRMApplication.getBus().post(activity);
                                if(rescheduleTaskResponse.getResult()!=null){
                                    Date dateUpdated = Util.getDate(date);
                                    if(dateUpdated == null){
                                        dateUpdated = Util.getDateFormatOnly(date);
                                    }
                                    Util.postBookingDbReschedule(realm,
                                            rescheduleTaskResponse.getResult().getScheduled_activity_id(),
                                            rescheduleTaskResponse.getResult().getAction_id(),
                                            dateUpdated);
                                }

                                callC360Activity(true,"");
                            }else{
                                //SalesCRMApplication.getBus().post(true);
                                callC360Activity(false,rescheduleTaskResponse.getError().getDetails());
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            // Log.e("Reschedule response", "reschedule task - " + error.getMessage());
                            // SalesCRMApplication.getBus().post(true);
                            callC360Activity(false,errorMessage);
                        }
                    });
        }

    }

    /**
     * api to confirm full payment recieved
     *
     */
    public void fullPaymentReceived(String accessToken, final String presentCarId, final FullPaymentReceivedInput fullPaymentReceivedInput, final CarBookingCallBack carBookingCallBack) {
        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).fullPaymentRecieved(fullPaymentReceivedInput, new Callback<FullPaymentRecievedResponse>() {

                        @Override
                        public void success(final FullPaymentRecievedResponse fullPaymentRecievedResponse, Response response) {
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }
                            //CancelBookingResponse cancelBookingResponse1 = (CancelBookingResponse) saveCarDetailsResponse;
                            if (fullPaymentRecievedResponse != null && fullPaymentRecievedResponse.getStatusCode() != null
                                    && fullPaymentRecievedResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) && fullPaymentRecievedResponse.getResult() != null) {
                                // SalesCRMApplication.getBus().post(activity);

                              Util.postBookingDbHandler(realm, CarsFragmentServiceHandler.this,
                                        fullPaymentRecievedResponse.getResult().getScheduled_activity_id(),
                                        fullPaymentRecievedResponse.getResult().getAction_id(),
                                        fullPaymentRecievedResponse.getResult().getActivity_id());


                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        AllInterestedCars allInterestedCars = realm.where(AllInterestedCars.class)
                                                .equalTo("carId",presentCarId)
                                                .findFirst();

                                        if (allInterestedCars != null && !fullPaymentRecievedResponse.getResult().getNext_stage_id().isEmpty()) {
                                            allInterestedCars.setCar_stage_id(fullPaymentRecievedResponse.getResult().getNext_stage_id());
                                            allInterestedCars.setExpected_delivery_date(fullPaymentReceivedInput.getExp_delivery_date().trim());
                                        }
                                        //realm.copyToRealmOrUpdate(allInterestedCars);
                                    }
                                });
                                SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                                        .equalTo("leadId", (int)fullPaymentReceivedInput.getLead_id())
                                        .findFirst();
                                if(salesCRMRealmTable!=null && salesCRMRealmTable.isValid()){
                                    realm.beginTransaction();
                                    salesCRMRealmTable.setLeadLastUpdated(fullPaymentRecievedResponse.getResult().getLead_last_updated());
                                    realm.commitTransaction();
                                }
                                carBookingCallBack.onCallBookingCallBack(true,"Full Payment Received Confirmed");

                            } else {
                                //SalesCRMApplication.getBus().post(true);

                                carBookingCallBack.onCallBookingCallBack(false,(fullPaymentRecievedResponse==null || fullPaymentRecievedResponse.getError()==null)?errorMessage: fullPaymentRecievedResponse.getError().getDetails());
                            }

                        }
                        @Override
                        public void failure(RetrofitError error) {
                           // Log.e("CancelInvoice", "cancel invoice - " + error.getMessage());
                           // SalesCRMApplication.getBus().post(true);
                            tryUpdateLeadLastUpdatedOnError(error);
                            carBookingCallBack.onCallBookingCallBack(false,errorMessage);
                        }
                    });
        }


    }

    /**
     *  @param updateInvoiceModel
     * @param presentCarId
     * @param accesstoken
     * @param leadId
     * @param carBookingCallBack
     */
    public void updateInvoice(final UpdateInvoiceModel updateInvoiceModel, final String presentCarId, String accesstoken, final long leadId, final CarBookingCallBack carBookingCallBack) {

        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accesstoken).updateInvoice(updateInvoiceModel, new Callback<UpdateInvoiceResponse>() {
                @Override
                public void success(final UpdateInvoiceResponse updateInvoiceResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }
                    // if () {
                    if (updateInvoiceResponse != null && updateInvoiceResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) &&
                            updateInvoiceResponse.getResult() != null &&
                            updateInvoiceResponse.getResult().getInvoice_id() != null &&
                            updateInvoiceResponse.getResult().getNext_stage_id() != null &&
                            !updateInvoiceResponse.getResult().getInvoice_id().isEmpty() &&
                            !updateInvoiceResponse.getResult().getNext_stage_id().isEmpty()) {


                        Util.postBookingDbHandler(realm, CarsFragmentServiceHandler.this,
                                updateInvoiceResponse.getResult().getScheduled_activity_id(),
                                updateInvoiceResponse.getResult().getAction_id(),
                                updateInvoiceResponse.getResult().getActivity_id());

                        SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                                .equalTo("leadId", (int)leadId)
                                .findFirst();
                        if(salesCRMRealmTable!=null && salesCRMRealmTable.isValid()){
                            realm.beginTransaction();
                            salesCRMRealmTable.setLeadLastUpdated(updateInvoiceResponse.getResult().getLead_last_updated());
                            realm.commitTransaction();
                        }

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                // SalesCRMApplication.getBus().post(activity);
                                AllInterestedCars allInterestedCars = realm.where(AllInterestedCars.class)
                                        .equalTo("carId", presentCarId)
                                        .findFirst();
                                allInterestedCars.setCar_stage_id(updateInvoiceResponse.getResult().getNext_stage_id());
                                allInterestedCars.getInvoiceDetails().setInvoiceId(Integer.parseInt(updateInvoiceResponse.getResult().getInvoice_id()));
                                // realm.copyToRealmOrUpdate(allInterestedCars);
                            }
                        });
                        carBookingCallBack.onCallBookingCallBack(true,"Invoice Confirmed");
                    }else if(updateInvoiceResponse!=null && !updateInvoiceResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)){
                       // SalesCRMApplication.getBus().post(true);
                        carBookingCallBack.onCallBookingCallBack(true,"Invoice Confirmed");

                    }else{
                        carBookingCallBack.onCallBookingCallBack(false,(updateInvoiceResponse==null || updateInvoiceResponse.getError()==null)?errorMessage: updateInvoiceResponse.getError().getDetails());
                    }

                    //    }

                }

                @Override
                public void failure(RetrofitError error) {
                    error.printStackTrace();
                    //SalesCRMApplication.getBus().post(true);
                    carBookingCallBack.onCallBookingCallBack(false,errorMessage);
                }
            });
        }

    }

    /**
     * api to update delivery to the server
     *  @param updateDeliveryModel
     * @param presentCarId
     * @param accesstoken
     * @param carBookingCallBack
     */
    public void updateDelivery(final UpdateDeliveryModel updateDeliveryModel, final String presentCarId, String accesstoken, final CarBookingCallBack carBookingCallBack) {
        realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accesstoken).updateDelivery(updateDeliveryModel, new Callback<UpdateDeliveryResponse>() {
                @Override
                public void success(final UpdateDeliveryResponse updateDeliveryResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }
                    // if () {

                    if (updateDeliveryResponse != null && updateDeliveryResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) &&
                            updateDeliveryResponse.getResult() != null &&
                            updateDeliveryResponse.getResult().getDelivery_id() != null &&
                            updateDeliveryResponse.getResult().getNext_stage_id() != null &&
                            !updateDeliveryResponse.getResult().getDelivery_id().isEmpty() &&
                            !updateDeliveryResponse.getResult().getNext_stage_id().isEmpty()) {



                            Util.postBookingDbHandler(realm, CarsFragmentServiceHandler.this,
                                    updateDeliveryResponse.getResult().getScheduled_activity_id(),
                                    updateDeliveryResponse.getResult().getAction_id(),
                                    updateDeliveryResponse.getResult().getActivity_id());

                        SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                                .equalTo("leadId", (int)updateDeliveryModel.getLead_id())
                                .findFirst();
                        if(salesCRMRealmTable!=null && salesCRMRealmTable.isValid()){
                            realm.beginTransaction();
                            salesCRMRealmTable.setLeadLastUpdated(updateDeliveryResponse.getResult().getLead_last_updated());
                            realm.commitTransaction();
                        }
                        //SalesCRMApplication.getBus().post(activity);
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                AllInterestedCars allInterestedCars = realm.where(AllInterestedCars.class)
                                        .equalTo("carId", presentCarId)
                                        .findFirst();
                                //check what all to update
                                allInterestedCars.setCar_stage_id(updateDeliveryResponse.getResult().getNext_stage_id());
                                if (allInterestedCars.getInvoiceDetails() != null) {
                                    allInterestedCars.getInvoiceDetails().setDeliveryNo(updateDeliveryModel.getDelivery_details().getDelivery_number());
                                    if (updateDeliveryResponse.getResult().getDelivery_id() != null && updateDeliveryResponse.getResult().getDelivery_id().isEmpty()) {
                                        allInterestedCars.getInvoiceDetails().setDeliveryId(Integer.parseInt(updateDeliveryResponse.getResult().getDelivery_id()));
                                    }

                                    // allInterestedCars.getInvoiceDetails().setDeliveryId(Integer.parseInt(updateDeliveryResponse.getResult().getDelivery_id()));
                                    //allInterestedCars.getInvoiceDetails().setDeliveryNo(updateDeliveryModel.getDelivery_details().getDelivery_number());
                                }

                                // realm.copyToRealmOrUpdate(allInterestedCars);
                            }
                        });
                        carBookingCallBack.onCallBookingCallBack(true, "Delivery Confirmed");
                    }else if(updateDeliveryResponse != null && !updateDeliveryResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)){
                        //SalesCRMApplication.getBus().post(true);
                        carBookingCallBack.onCallBookingCallBack(true,"Delivery Confirmed");

                    }else{
                        carBookingCallBack.onCallBookingCallBack(false,(updateDeliveryResponse==null || updateDeliveryResponse.getError()==null)?errorMessage: updateDeliveryResponse.getError().getDetails());
                    }

                    // }

                }

                @Override
                public void failure(RetrofitError error) {
                    error.printStackTrace();
                    //SalesCRMApplication.getBus().post(true);
                    tryUpdateLeadLastUpdatedOnError(error);
                    carBookingCallBack.onCallBookingCallBack(false,errorMessage);
                }
            });
        }

    }


    //////////////////////////////////////////////////////////////////object creations for server apis//////////////////////////////////////////////////////////////////////

    /**
     * method to create a new car model object to send to server to add a new car
     *
     * @param amtRecvd
     * @param allInterestedCar
     * @param locationId
     * @param followUpDate
     * @param formSubmissionInputData
     * @return
     */
    public BookCarModel createBookCarObject(String bookingRemarks,String bookingName, String dob, String pan,
                                            String aadhar, String gst, ArrayList<EditText> allBookEds,
                                            String amtRecvd, AllInterestedCars allInterestedCar,
                                            String locationId, String followUpDate, String leadSrcId,
                                            FormSubmissionInputData formSubmissionInputData,String leadLastUpdated) {
        BookCarModel newBookCar = new BookCarModel();
        newBookCar.setForm_object(formSubmissionInputData);
        newBookCar.setBooking_name(bookingName);
        newBookCar.setDob(dob);
        newBookCar.setPan_number(pan);
        newBookCar.setAadhar_number(aadhar);
        newBookCar.setRemarks(bookingRemarks);
        newBookCar.setGst(gst);
        newBookCar.setLead_last_updated(leadLastUpdated);
        BookCarModel.LeadCarDetails carDetails = newBookCar.new LeadCarDetails();
        carDetails.setCarColorId(allInterestedCar.getIntrestedCarColorId());
        carDetails.setCarFuelType(allInterestedCar.getFuelType());
        carDetails.setCarModel(allInterestedCar.getIntrestedCarModelId());
        carDetails.setCarVariant(allInterestedCar.getIntrestedCarVariantId());
        carDetails.setLead_car_id(allInterestedCar.getIntrestedLeadCarId());


        ArrayList<String> bookingIds = new ArrayList<String>();
        for (int i = 0; i < allBookEds.size(); i++) {
            if (!allBookEds.get(i).getText().toString().isEmpty()) {
                bookingIds.add(allBookEds.get(i).getText().toString().trim());
            }
        }
       // newBookCar.setCar_details(carDetails);
       // newBookCar.setBooking_details(bookingIds);
        newBookCar.setAmount_recieved(amtRecvd);
        newBookCar.setLead_id(allInterestedCar.getLeadId());
        if (locationId != null && !locationId.isEmpty()) {
            newBookCar.setLocation_id(Integer.parseInt(locationId));
        } else {
            newBookCar.setLocation_id(0);
        }
        if (leadSrcId != null && !leadSrcId.isEmpty()) {
            newBookCar.setLead_source_id(Integer.parseInt(leadSrcId));
        } else {
            newBookCar.setLocation_id(0);
        }
        if (allInterestedCar.getCar_stage_id() != null && !allInterestedCar.getCar_stage_id().isEmpty()) {
            newBookCar.setLead_car_stage_id(Integer.parseInt(allInterestedCar.getCar_stage_id()));
        }

        newBookCar.setNext_follow_date(followUpDate);


        return newBookCar;


    }

    /**
     *
     * @param carBookingFormHolderModels
     * @param bookingMainModel
     * @param allInterestedCar
     * @param locationId
     * @param leadSrcId
     * @param formSubmissionInputData
     * @param leadLastUpdated
     * @return
     */
    public BookCarModel createBookCarObject(List<CarBookingFormHolderModel> carBookingFormHolderModels,
                                            BookingMainModel bookingMainModel,
                                            AllInterestedCars allInterestedCar,
                                            String locationId,
                                            String leadSrcId,
                                            FormSubmissionInputData formSubmissionInputData,
                                            String leadLastUpdated) {
        List<ApiInputBookingDetails> apiInputBookingDetails = new ArrayList<>();
        for (int i=0;i<carBookingFormHolderModels.size();i++) {
            apiInputBookingDetails.add(carBookingFormHolderModels
                    .get(i)
                    .getBookingMainModel()
                    .getApiInputDetails());
        }

        BookCarModel newBookCar = new BookCarModel();
        newBookCar.setForm_object(formSubmissionInputData);
        newBookCar.setLead_last_updated(leadLastUpdated);
        newBookCar.setLead_id(allInterestedCar.getLeadId());
        if (locationId != null && !locationId.isEmpty()) {
            newBookCar.setLocation_id(Integer.parseInt(locationId));
        } else {
            newBookCar.setLocation_id(0);
        }
        if (leadSrcId != null && !leadSrcId.isEmpty()) {
            newBookCar.setLead_source_id(Integer.parseInt(leadSrcId));
        } else {
            newBookCar.setLocation_id(0);
        }
        if (allInterestedCar.getCar_stage_id() != null && !allInterestedCar.getCar_stage_id().isEmpty()) {
            newBookCar.setLead_car_stage_id(Integer.parseInt(allInterestedCar.getCar_stage_id()));
        }

        newBookCar.setBooking_details(apiInputBookingDetails);

        newBookCar.setNext_follow_date(bookingMainModel.getDateAsSQLDate());
        newBookCar.setRemarks(bookingMainModel.getRemarks());


        return newBookCar;


    }


    /**
     *
     * @param bookingMainModel
     * @param allInterestedCar
     * @param locationId
     * @param leadSrcId
     * @param leadLastUpdated
     * @return
     */
    public EditCarModel createEditBookingObject(int bookingId, int enquiryId, BookingMainModel bookingMainModel,
                                            AllInterestedCars allInterestedCar,
                                            String locationId,
                                            String leadSrcId,
                                            String leadLastUpdated) {
        return new  EditCarModel(
                bookingId,
                enquiryId,
                Util.getInt(locationId),
                allInterestedCar.getLeadId(),
                allInterestedCar.getIntrestedLeadCarId(),
                Util.getInt(leadSrcId),
                leadLastUpdated,
                bookingMainModel.getApiInputDetails());

    }

    /**
     *
     * @param remarks
     * @param anInt
     * @param booking_number
     * @param intrestedLeadCarId
     * @param i
     * @param anInt1
     * @param leadId
     * @param i1
     * @param dateAsSQLDate
     * @param accessToken
     * @param carId
     * @param anInt2
     * @param leadLastUpdated
     * @param apiInputDetails
     * @return
     */
    public FullPaymentReceivedInput createFullPaymentReceivedInputObject(String remarks, int anInt, String booking_number, int intrestedLeadCarId, int i, int anInt1, long leadId, int i1, String dateAsSQLDate, String accessToken, String carId, int anInt2, String leadLastUpdated, ApiInputBookingDetails apiInputDetails) {
        return new FullPaymentReceivedInput(remarks, anInt,  booking_number,  intrestedLeadCarId,  i,  anInt1,  leadId,  i1,  dateAsSQLDate,  accessToken,  carId,  anInt2,  leadLastUpdated, apiInputDetails);
    }

    /*
     * creating the invoice model object to send to server
     * @param invoiceNo
     * @param invoiceDate
     * @param invoiceName
     * @param invoiceAddress
     * @param invoiceAmount
     * @param mobileNo
     * @param vinNo
     * @return
     */
    public UpdateInvoiceModel createInvoiceModelObject(String invoiceNo, String invoiceDate, String invoiceName, String invoiceAddress,
                                                       String invoiceAmount, String mobileNo, String vinNo, int bookingId, String bookingNo,
                                                       int leadCarId, int enquiryId, int locId, long leadId, int leadCarStageId,
                                                       String expectedDate, String leadLastUpdated) {


        UpdateInvoiceModel updateInvoiceModel = new UpdateInvoiceModel();
        UpdateInvoiceModel.InvoiceDetails invoiceDetails = updateInvoiceModel.new InvoiceDetails();

        invoiceDetails.setInvoice_number(invoiceNo);
        invoiceDetails.setInvoice_date(invoiceDate);
        invoiceDetails.setInvoice_name(invoiceName);
        invoiceDetails.setInvoice_address(invoiceAddress);
        invoiceDetails.setInvoice_amount(invoiceAmount);
        invoiceDetails.setInvoice_mob_no(mobileNo);
        invoiceDetails.setVin_no(vinNo);

        updateInvoiceModel.setInvoice_details(invoiceDetails);
        updateInvoiceModel.setBooking_id(bookingId);
        updateInvoiceModel.setBooking_number(bookingNo);
        updateInvoiceModel.setLead_car_id(leadCarId);
        updateInvoiceModel.setEnquiry_id(enquiryId);
        updateInvoiceModel.setLocation_id(locId);
        updateInvoiceModel.setLead_id(leadId);
        updateInvoiceModel.setLead_car_stage_id(leadCarStageId);
        updateInvoiceModel.setExpected_delivery_date(expectedDate);
        updateInvoiceModel.setLead_last_updated(leadLastUpdated);

        return updateInvoiceModel;


    }


    public UpdateInvoiceModel createInvoiceModelObject(BookingMainModel bookingMainModel,
                                                       int bookingId,
                                                       String bookingNo,
                                                       int leadCarId,
                                                       int enquiryId,
                                                       int locId, long leadId, int leadCarStageId,
                                                       String expectedDate, String leadLastUpdated) {


        UpdateInvoiceModel updateInvoiceModel = new UpdateInvoiceModel();
        UpdateInvoiceModel.InvoiceDetails invoiceDetails = updateInvoiceModel.new InvoiceDetails();

        invoiceDetails.setInvoice_number("");
        invoiceDetails.setInvoice_date(Util.getSQLDateTime(bookingMainModel.getBookingCarModel().getInvoiceDate()));
        invoiceDetails.setInvoice_name("");
        invoiceDetails.setInvoice_address("");
        invoiceDetails.setInvoice_amount("0");
        invoiceDetails.setInvoice_mob_no(bookingMainModel.getBookingCarModel().getInvoiceMobileNumber());
        invoiceDetails.setVin_no(bookingMainModel.getBookingCarModel().getVinNumber());

        updateInvoiceModel.setInvoice_details(invoiceDetails);
        updateInvoiceModel.setBooking_id(bookingId);
        updateInvoiceModel.setBooking_number(bookingNo);
        updateInvoiceModel.setLead_car_id(leadCarId);
        updateInvoiceModel.setEnquiry_id(enquiryId);
        updateInvoiceModel.setLocation_id(locId);
        updateInvoiceModel.setLead_id(leadId);
        updateInvoiceModel.setLead_car_stage_id(leadCarStageId);
        updateInvoiceModel.setExpected_delivery_date(expectedDate);
        updateInvoiceModel.setLead_last_updated(leadLastUpdated);
        updateInvoiceModel.setBooking_details(bookingMainModel.getApiInputDetails());

        return updateInvoiceModel;


    }



    /**
     * creating the delivery object to update delivery to the server
     *
     * @param invoiceNo
     * @param invoiceName
     * @param mobileNo
     * @param vinNo
     * @param bookingId
     * @param bookingNo
     * @param leadCarId
     * @param enquiryId
     * @param locId
     * @param leadId
     * @param leadCarStageId
     * @return
     */
    public UpdateDeliveryModel createUpdateDeliveryModelObject(String deliveryChallanNumber, String invoiceNo, String invoiceName,
                                                               String mobileNo, String vinNo, int bookingId, String bookingNo,
                                                               int leadCarId, int enquiryId, int locId, long leadId, int leadCarStageId,
                                                               int invoiceId, final String leadLastUpdated) {


        UpdateDeliveryModel updateDeliveryModel = new UpdateDeliveryModel();
        UpdateDeliveryModel.DeliveryDetails deliveryDetails = updateDeliveryModel.new DeliveryDetails();

        deliveryDetails.setDelivery_number(deliveryChallanNumber);
        deliveryDetails.setInvoice_number(invoiceNo);
        deliveryDetails.setInvoice_name(invoiceName);
        deliveryDetails.setInvoice_id("" + invoiceId);
        // deliveryDetails.setInvoice_address(invoiceAddress);
        // deliveryDetails.setInvoice_amount(invoiceAmount);
        deliveryDetails.setInvoice_mob_no(mobileNo);
        deliveryDetails.setVin_no(vinNo);

        updateDeliveryModel.setDelivery_details(deliveryDetails);
        updateDeliveryModel.setBooking_id(bookingId);
        updateDeliveryModel.setBooking_number(bookingNo);
        updateDeliveryModel.setLead_car_id(leadCarId);
        updateDeliveryModel.setEnquiry_id(enquiryId);
        updateDeliveryModel.setLocation_id(locId);
        updateDeliveryModel.setLead_id(leadId);
        updateDeliveryModel.setLead_car_stage_id(leadCarStageId);
        updateDeliveryModel.setLead_last_updated(leadLastUpdated);
        return updateDeliveryModel;


    }


    /**
     * creating the delivery object to update delivery to the server
     *
     * @param invoiceNo
     * @param invoiceName
     * @param mobileNo
     * @param vinNo
     * @param bookingId
     * @param bookingNo
     * @param leadCarId
     * @param enquiryId
     * @param locId
     * @param leadId
     * @param leadCarStageId
     * @return
     */
    public UpdateDeliveryModel createUpdateDeliveryModelObject(BookingMainModel bookingMainModel, String invoiceNo, String invoiceName,
                                                               String mobileNo, String vinNo, int bookingId, String bookingNo,
                                                               int leadCarId, int enquiryId, int locId, long leadId, int leadCarStageId,
                                                               int invoiceId, final String leadLastUpdated) {


        UpdateDeliveryModel updateDeliveryModel = new UpdateDeliveryModel();
        UpdateDeliveryModel.DeliveryDetails deliveryDetails = updateDeliveryModel.new DeliveryDetails();

        deliveryDetails.setDelivery_number(bookingMainModel.getBookingCarModel().getDeliveryChallan());
        deliveryDetails.setInvoice_number(invoiceNo);
        deliveryDetails.setInvoice_name(invoiceName);
        deliveryDetails.setInvoice_id("" + invoiceId);
        deliveryDetails.setDelivery_date(Util.getSQLDateTime(bookingMainModel.getBookingCarModel().getDeliveryDate()));
        // deliveryDetails.setInvoice_address(invoiceAddress);
        // deliveryDetails.setInvoice_amount(invoiceAmount);
        deliveryDetails.setInvoice_mob_no(mobileNo);
        deliveryDetails.setVin_no(vinNo);

        updateDeliveryModel.setDelivery_details(deliveryDetails);
        updateDeliveryModel.setBooking_id(bookingId);
        updateDeliveryModel.setBooking_number(bookingNo);
        updateDeliveryModel.setLead_car_id(leadCarId);
        updateDeliveryModel.setEnquiry_id(enquiryId);
        updateDeliveryModel.setLocation_id(locId);
        updateDeliveryModel.setLead_id(leadId);
        updateDeliveryModel.setLead_car_stage_id(leadCarStageId);
        updateDeliveryModel.setLead_last_updated(leadLastUpdated);
        updateDeliveryModel.setBooking_details(bookingMainModel.getApiInputDetails());
        return updateDeliveryModel;


    }

    /**
     * @param leadId
     * @param leadLastUpdated
     * @param carName
     * @param evaluatorName
     * @param evaluationAddress
     * @param evaluationRemarks
     * @param exchangeCarId
     * @param evaluatorId
     * @param dateTime
     * @return
     */
    public EvaluationSubmitRequestModel createEvaluationModel(String leadId, String leadLastUpdated,
                                                              String carName, String evaluatorName,
                                                              String evaluationAddress, String evaluationRemarks, String exchangeCarId,
                                                              String evaluatorId, String dateTime, String addressTypeId, String locationId,String locationName) {

        int addTypeId = 0;

        EvaluationSubmitRequestModel evaluationSubmitRequestModel = new EvaluationSubmitRequestModel();
        EvaluationSubmitRequestModel.Form_response address = evaluationSubmitRequestModel.new Form_response();
        EvaluationSubmitRequestModel.Form_response showRoom = evaluationSubmitRequestModel.new Form_response();
        evaluationSubmitRequestModel.setLead_id(leadId);
        evaluationSubmitRequestModel.setLead_last_updated(leadLastUpdated);
        evaluationSubmitRequestModel.setScheduled_type(1);

        List<EvaluationSubmitRequestModel.Form_response> form_responseList = new ArrayList<>();

        // adding exchange car
        EvaluationSubmitRequestModel.Form_response exchangeCar = evaluationSubmitRequestModel.new Form_response();
        exchangeCar.setName("376");

        EvaluationSubmitRequestModel.Form_response.Value exchangeValue = exchangeCar.new Value();
        exchangeValue.setAnswerValue(exchangeCarId);
        exchangeValue.setDisplayText(carName);
        exchangeValue.setFAnsId("");

        exchangeCar.setValue(exchangeValue);
        //assign to

        EvaluationSubmitRequestModel.Form_response assignTo = evaluationSubmitRequestModel.new Form_response();
        assignTo.setName("358");

        EvaluationSubmitRequestModel.Form_response.Value assignToValue = assignTo.new Value();
        assignToValue.setAnswerValue(evaluatorId);
        assignToValue.setDisplayText(evaluatorName);
        assignToValue.setFAnsId("");
        assignTo.setValue(assignToValue);

        //address type
        EvaluationSubmitRequestModel.Form_response addressType = evaluationSubmitRequestModel.new Form_response();
        addressType.setName("588");

        EvaluationSubmitRequestModel.Form_response.Value addressTypeValue = addressType.new Value();
        if(addressTypeId!=null && !addressTypeId.isEmpty()) {
             addTypeId = Integer.parseInt(addressTypeId);
            switch (addTypeId){
                case 1:
                    addressTypeValue.setAnswerValue(addressTypeId);
                    addressTypeValue.setDisplayText("Home");
                    addressTypeValue.setFAnsId("464");
                    addressType.setValue(addressTypeValue);
                    break;
                case 2:
                    addressTypeValue.setAnswerValue(addressTypeId);
                    addressTypeValue.setDisplayText("Office");
                    addressTypeValue.setFAnsId("465");
                    addressType.setValue(addressTypeValue);
                    break;
                case 3:
                    addressTypeValue.setAnswerValue(addressTypeId);
                    addressTypeValue.setDisplayText("Showroom");
                    addressTypeValue.setFAnsId("531");
                    addressType.setValue(addressTypeValue);
                   // EvaluationSubmitRequestModel.Form_response showRoom = evaluationSubmitRequestModel.new Form_response();
                    showRoom.setName("624");
                    EvaluationSubmitRequestModel.Form_response.Value showRoomValue = showRoom.new Value();
                    showRoomValue.setAnswerValue(locationId);
                    showRoomValue.setDisplayText(locationName);
                    showRoomValue.setFAnsId("1");
                    showRoom.setValue(showRoomValue);

                    break;
                case 4:
                    addressTypeValue.setAnswerValue(addressTypeId);
                    addressTypeValue.setDisplayText("Other");
                    addressTypeValue.setFAnsId("466");
                    addressType.setValue(addressTypeValue);
                    break;
            }
        }



        if(addTypeId!= 0 && addTypeId!=3){
            //address
            // address = evaluationSubmitRequestModel.new Form_response();
             address.setName("589");

            EvaluationSubmitRequestModel.Form_response.Value addressValue = address.new Value();
            addressValue.setAnswerValue(evaluationAddress);
            addressValue.setDisplayText("");
            addressValue.setFAnsId("");

            address.setValue(addressValue);
        }


        //remarks
        EvaluationSubmitRequestModel.Form_response remarks = evaluationSubmitRequestModel.new Form_response();
        remarks.setName("349");

        EvaluationSubmitRequestModel.Form_response.Value remarksValue = remarks.new Value();
        remarksValue.setAnswerValue(evaluationRemarks);
        remarksValue.setDisplayText("");
        remarksValue.setFAnsId("");
        remarks.setValue(remarksValue);

        //scheduledAt
        EvaluationSubmitRequestModel.Form_response scheduled = evaluationSubmitRequestModel.new Form_response();
        scheduled.setName("352");

        EvaluationSubmitRequestModel.Form_response.Value dateTimeValue = scheduled.new Value();
        dateTimeValue.setAnswerValue(dateTime);
        dateTimeValue.setDisplayText("");
        dateTimeValue.setFAnsId("");

        scheduled.setValue(dateTimeValue);

        //extra
        EvaluationSubmitRequestModel.Form_response extra = evaluationSubmitRequestModel.new Form_response();
        extra.setName("351");

        EvaluationSubmitRequestModel.Form_response.Value extraValue = extra.new Value();
        extraValue.setAnswerValue("10");
        extraValue.setDisplayText("Old Car Evaluation");
        extraValue.setFAnsId("");

        extra.setValue(extraValue);


        form_responseList.add(exchangeCar);
        form_responseList.add(assignTo);
        form_responseList.add(addressType);
        if(addTypeId==3){
            form_responseList.add(showRoom);
        }else {
            form_responseList.add(address);
        }

        form_responseList.add(remarks);
        form_responseList.add(scheduled);
        form_responseList.add(extra);


        evaluationSubmitRequestModel.setForm_response(form_responseList);


        return evaluationSubmitRequestModel;


    }

    public ExchangeCarEditModel createExchangeEditObject(ExchangeCarDetails exchangeCarDetails,
                                                         String selectedYear, String selectedModelId,
                                                         String selectedExpectedPrice, String selectedMarketPrice,
                                                         String selectedQuotedPrice,
                                                         String selectedRegNum,
                                                         String selectedTotalRuns) {
        ExchangeCarEditModel exchangeCarEditModel = new ExchangeCarEditModel();
        exchangeCarEditModel.setLead_id("" + exchangeCarDetails.getLeadId());

        ExchangeCarEditModel.Car car = exchangeCarEditModel.new Car();
        car.setEdit("1");
        car.setLead_exchange_car_id(exchangeCarDetails.getLead_exchange_car_id());
        car.setModel_id(selectedModelId);
        car.setName(exchangeCarDetails.getExchangecarname());
        car.setPurchase_date(selectedYear);
        car.setNewExpectedPrice(selectedExpectedPrice);
        car.setNewMarketPrice(selectedMarketPrice);
        car.setNewPriceQuoted(selectedQuotedPrice);
        car.setReg_no(selectedRegNum);
        car.setKms_run(selectedTotalRuns);
        exchangeCarEditModel.setCar(car);

        return exchangeCarEditModel;
    }

    private void callC360Activity(boolean status,String response) {
        Intent intent = new Intent(activity,NewCarsFragment.C360ActivityKillerBroadcast.class);
        intent.setAction("com.salescrm.c360ActivityKiller");
        Bundle bundle = new Bundle();
        if(status) {
            bundle.putBoolean("API_RESPONSE", true);
        }else {
            bundle.putBoolean("API_RESPONSE", false);
        }
        intent.putExtras(bundle);
        activity.sendBroadcast(intent);
        if(status) {
            activity.finish();
        }else{
            Toast.makeText(activity,response,Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onPostBookingDbHandler(int postBookingType) {

    }

    public interface CarBookingCallBack {
        void onCallBookingCallBack(boolean success, String response);
    }
}
