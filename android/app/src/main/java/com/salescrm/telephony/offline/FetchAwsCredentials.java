package com.salescrm.telephony.offline;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AwsCredentialsResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.NinjaEncryption;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 26/05/20.
 * Call this to get aws temporary access
 */
public class FetchAwsCredentials implements Callback<AwsCredentialsResponse> {
    public OfflineSupportListener offlineSupportListener = null;
    public FetchAwsCredentialsCallBack fetchAwsCredentialsCallBack = null;
    private Preferences pref = null;

    public FetchAwsCredentials(OfflineSupportListener offlineSupportListener, Context context) {
        this.offlineSupportListener = offlineSupportListener;
        this.pref = Preferences.getInstance();
        pref.load(context);
    }
    public FetchAwsCredentials(FetchAwsCredentialsCallBack fetchAwsCredentialsCallBack, Context context) {
        this.fetchAwsCredentialsCallBack = fetchAwsCredentialsCallBack;
        this.pref = Preferences.getInstance();
        pref.load(context);
    }

    public void callAsync() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getAwsTemporaryAccess(this);
        } else {
            callbackError();
        }
    }
    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            AwsCredentialsResponse awsCredentialsResponse =
                    ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getAwsTemporaryAccess();
            if(awsCredentialsResponse!=null && awsCredentialsResponse.getResult()!=null) {
                setPref(awsCredentialsResponse);
                callbackSuccess();
            }
        } else {
            callbackError();
        }
    }



    void setPref(AwsCredentialsResponse data) {
        if (Util.isNotNull(data.getResult().getS3_user_name())) {
            pref.setS3UserName(new NinjaEncryption().encrypt(data.getResult().getS3_user_name()));
        }
        if (Util.isNotNull(data.getResult().getS3_password())) {
            pref.setS3Password(new NinjaEncryption().encrypt(data.getResult().getS3_password()));
        }
        if (Util.isNotNull(data.getResult().getS3_bucket_name())) {
            pref.setS3BucketName(new NinjaEncryption().encrypt(data.getResult().getS3_bucket_name()));
        }
        if (Util.isNotNull(data.getResult().getSessionToken())) {
            pref.setS3SessionToken(new NinjaEncryption().encrypt(data.getResult().getSessionToken()));
        }
    }
    @Override
    public void success(final AwsCredentialsResponse data, Response response) {
        Util.updateHeaders(response.getHeaders());
        if (data.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            if(data.getResult()!=null) {
                setPref(data);
                callbackSuccess();
            }
            else {
                callbackError();
                CleverTapPush.pushEvent("AWS_API_ERROR");
            }
        }
        else {
            callbackError();
            CleverTapPush.pushEvent("AWS_API_ERROR");
        }
    }
    private void callbackError() {
        if(offlineSupportListener!=null) {
            offlineSupportListener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.AWS_CREDENTIALS);
        }
        if(fetchAwsCredentialsCallBack!=null) {
            fetchAwsCredentialsCallBack.onFetchAwsCredentialsError();
        }

    }
    private void callbackSuccess() {
        if(offlineSupportListener!=null) {
            offlineSupportListener.onFetchAwsCredentials();
        }
        if(fetchAwsCredentialsCallBack!=null) {
            fetchAwsCredentialsCallBack.onFetchAwsCredentialsSuccess();
        }
    }

    @Override
    public void failure(RetrofitError error) {
        callbackError();
    }

    public interface FetchAwsCredentialsCallBack {
        void onFetchAwsCredentialsSuccess();
        void onFetchAwsCredentialsError();
    }

}
