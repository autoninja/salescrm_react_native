import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, Platform, Button, FlatList, Alert } from 'react-native';
//import { VictoryPie } from 'react-native-svg'
import { VictoryPie } from "victory-native";

 
export default class LostDrop extends Component {
  constructor(props) {
    super(props);
 
    this.state = {
     simpleData : [
		    { label: "12%", y: 120,},
		    { label: "3%", y: 30,},
		    { label: "40%", y: 400,},
		    { label: "7%", y: 70,},
		  ],
	colors : [
		   "#FFAF59",
		   "#E28300",
		   "#F6A57F",
		   "#FF885F"
		 ]	  

    };
  }

  gotToDetails(id) {
    switch(id) {
      case 0:
        this.props.navigation.navigate("LostDetailsNewUsed");
        break;
      case 1:
        this.props.navigation.navigate("DropDetailsReasons");
        break;


    }

 
  render() {
  
    return (
      <View style={styles.container}>
      <View style={{flex:2}}>
	        <VictoryPie
	  		data={this.state.simpleData}
			  labelRadius={80}
			  style={{ labels: { fontSize: 16, fill: "white" } }}
			  colorScale={this.state.colors}
	        />
        </View>
        <View style={styles.subContainer}>
			{(this.state.simpleData && this.state.simpleData.length>0)?
				<FlatList
				data = {this.state.simpleData}
				renderItem={({item, index}) =>(
					<View style= {{flex:1, justifyContent: 'center', alignItems: 'center', margin: 5}}>
	    				<Button 
	    					style={styles.button}
	    					onPress={() => this.gotToDetails(0)}
					        title = {item.label}
				        />
					</View>)}
				numColumns={3}
				keyExtractor= {(item, index) => index.toString() }
				/>
				:null}
			
		    	</View>
      </View>
    )
  }
}
 
const styles = StyleSheet.create({
  container: { 
  	flex: 1, 
  	justifyContent: 'center', 
  	alignItems: 'center', 
  	backgroundColor: '#fff'},
  heading: {
    fontSize: 20,
    textAlign: 'center',
    marginBottom: 10
  },
  subContainer: {
    flex: 1,
    width: '100%'
  },
  button: {
  	alignSelf: 'stretch',
    width: '100'
  },
});
