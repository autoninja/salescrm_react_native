package com.salescrm.telephony.application;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.multidex.MultiDex;

import com.airbnb.android.react.maps.MapsPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.clevertap.android.sdk.ActivityLifecycleCallback;
import com.clevertap.android.sdk.CleverTapAPI;
//import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
//import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.clevertap.react.CleverTapPackage;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.horcrux.svg.SvgPackage;
import com.imagepicker.ImagePickerPackage;
import com.microsoft.appcenter.reactnative.analytics.AppCenterReactNativeAnalyticsPackage;
import com.microsoft.appcenter.reactnative.appcenter.AppCenterReactNativePackage;
import com.microsoft.codepush.react.ReactInstanceHolder;
import com.oblador.vectoricons.VectorIconsPackage;
import com.reactnativecommunity.geolocation.GeolocationPackage;
import com.salescrm.telephony.BuildConfig;
import com.salescrm.telephony.R;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.reactNative.ReactNativeToAndroidPackage;
import com.salescrm.telephony.telephonyModule.service.ServiceBackgroundRun;
import com.smixx.fabric.FabricPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.zmxv.RNSound.RNSoundPackage;

import java.util.Arrays;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.sentry.RNSentryPackage;

// 1. Import the plugin class.
import com.microsoft.codepush.react.CodePush;

/**
 * Created by Ravindra P on 27-11-2015.
 */
public class SalesCRMApplication extends android.app.Application implements ReactApplication {
    private static final EventBus bus = new EventBus();
    public static CleverTapAPI cleverTapAPI = null;
    private static Context context;
    private static FirebaseAnalytics mFirebaseAnalytics;
    private ReactInstanceHolder instanceHolder = new ReactInstanceHolder() {
        @Override
        public ReactInstanceManager getReactInstanceManager() {
            return reactNativeHost.getReactInstanceManager();
        }
    };
    protected final ReactNativeHost reactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        public List<ReactPackage> getPackages() {
            // 3. Instantiate an instance of the CodePush runtime and add it to the list of
            // existing packages, specifying the right deployment key. If you don't already
            // have it, you can run "appcenter codepush deployment list -a <ownerName>/<appName> --displayKeys" to retrieve your key.

            return Arrays.asList(
                    new FabricPackage(),
                    new MainReactPackage(),
                    new ReactNativeToAndroidPackage(),
                    new SvgPackage(),
                    new RNSoundPackage(),
                    new VectorIconsPackage(),
                    new CleverTapPackage(),
                    new RNGestureHandlerPackage(),
                    new AppCenterReactNativeAnalyticsPackage(SalesCRMApplication.this, getResources().getString(R.string.appCenterCrashes_whenToSendCrashes)),
                    new AppCenterReactNativePackage(SalesCRMApplication.this),
                    new CodePush(BuildConfig.CODEPUSH_KEY, SalesCRMApplication.this, BuildConfig.DEBUG),
                    new MapsPackage(),
                    new ImagePickerPackage(),
                    new LinearGradientPackage(),
                    new RNSentryPackage(),
                    new GeolocationPackage()


            );
        }
        // 2. Override the getJSBundleFile method in order to let
        // the CodePush runtime determine where to get the JS
        // bundle location from on each app start
        @Override
        protected String getJSMainModuleName() {
            return "index.android";
        }

        @Override
        protected String getJSBundleFile() {
            return CodePush.getJSBundleFile();
        }

        @Override
        protected String getBundleAssetName() {
            return "index.android.bundle";
        }
    };

    private Preferences pref = null;

    public static EventBus getBus() {
        return bus;
    }

    public static Context GetAppContext() {
        return SalesCRMApplication.context;
    }

    public void onCreate() {
        CodePush.setReactInstanceHolder(instanceHolder);
        ActivityLifecycleCallback.register(this);
        super.onCreate();

        SalesCRMApplication.context = getApplicationContext();
        bus.register(this);

        Realm.init(this);
        pref = Preferences.getInstance();
        Preferences.load(this);
        Preferences.setFabricsNeeded(!BuildConfig.DEBUG);
        if (Preferences.isFabricsNeeded()) {
            Fabric.with(this, new Crashlytics());
            Fabric.with(this, new Answers());
        } else {
            Fabric.with(this, new Crashlytics.Builder().core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build());

        }

        //Firebase instance
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Telephony disabled
//        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                Intent service = new Intent(this, ServiceBackgroundRun.class);
//                startForegroundService(service);
//            } else {
//                Intent service = new Intent(context, ServiceBackgroundRun.class);
//                startService(service);
//            }
//        } catch (Exception e) {
//
//        }


        RealmConfiguration config1 = new RealmConfiguration.Builder()
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config1);

        try {
            cleverTapAPI = CleverTapAPI.getDefaultInstance(getApplicationContext());
        } catch (Exception e) {
            // handle appropriately
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public ReactNativeHost getReactNativeHost() {
        return reactNativeHost;
    }


    @Override
    public void onTerminate() {
        Realm.getDefaultInstance().close();
        super.onTerminate();
    }
}