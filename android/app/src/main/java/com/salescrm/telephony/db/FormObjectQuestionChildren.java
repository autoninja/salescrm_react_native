package com.salescrm.telephony.db;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Ravindra P on 14-10-2016.
 */

public class FormObjectQuestionChildren extends RealmObject {


    private String key;

    @SerializedName("values")
    public RealmList<FormObjectQuestionValues> values;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public RealmList<FormObjectQuestionValues> getValues() {
        return values;
    }

    public void setValues(RealmList<FormObjectQuestionValues> formObjectQuestionValues) {
        this.values = formObjectQuestionValues;
    }
}
