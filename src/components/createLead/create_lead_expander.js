import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
export default class CreateLeadExpander extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    let title = "Title";
    title = this.props.item.title;
    let imageSourceExpand = require('../../images/ic_down_light.png');
    if (this.props.item.isExpanded) {
      imageSourceExpand = require('../../images/ic_up_light.png');
    }
    let successImage = require('../../images/ic_error_mark.png');
    if (this.props.showSuccessMark) {
      successImage = require('../../images/ic_check_mark.png');
    }
    if (this.props.showSuccessMark == 'gone') {
      successImage = null;
    }
    return (
      <TouchableOpacity onPress={() => this.props.onClick()}>
        <View style={[{
          backgroundColor: '#2B3973', height: 60, flexDirection: 'row',
          justifyContent: 'space-between', alignItems: 'center', padding: 10, paddingLeft: 20, paddingRight: 20
        }, this.props.style]}>
          <View style={{ alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row' }}>
            <Text style={{ color: '#fff', fontSize: 16 }}>{title}</Text>
            <Image
              source={successImage}
              style={{ width: 20, marginLeft: 16, height: 20, }}
            />

          </View>
          <Image
            source={imageSourceExpand}
            style={{ width: 36, height: 36, borderRadius: 36 / 2, opacity: .2 }}
          />
        </View>
      </TouchableOpacity>
    )
  }

}
