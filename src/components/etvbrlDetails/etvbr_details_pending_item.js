import React, { Component } from "react";
import {
  Slider,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ScrollView,
  Alert,
  ActivityIndicator
} from "react-native";

// Import the react-native-sound module
var Sound = require("react-native-sound");

import styles from "../../styles/styles";

export default class ETVBRDetailsPendingItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPlaying: false,
      duration: null,
      isLoading: false,
      currentVal: 0,
      maxVal: 0
    };
    this.recording = null;
    this.timeOut = null;
  }

  getMinutes(seconds) {
    console.log("duration::" + seconds);
    let minute = 0;
    if (seconds) {
      minute = (seconds / 60) | 0;
      return minute + ":" + (seconds % 60 | 0);
    }
    return "00:00";
  }

  playSong(url) {
    if (!url) {
      this.props.showAlert("No recording url found");
      return;
    }
    console.log("Recording value:" + (this.recording == null ? true : false));
    if (!this.recording || this.recording == null) {
      Sound.setCategory("Playback");
      this.setState({ isLoading: true });
      this.recording = new Sound(url, null, error => {
        if (error) {
          console.log("failed to load the sound", error);
          this.setState({ isPlaying: false });
          return;
        }
        this.setState({
          isLoading: false,
          isPlaying: true,
          maxVal: this.recording.getDuration(),
          duration: this.getMinutes(this.recording.getDuration())
        });
        this.updateIndicator();
        this.recording.play(success => {
          if (success) {
            console.log("successfully finished playing");
            this.setState({ isPlaying: false, currentVal: 0 });
            if (this.timeOut) {
              clearInterval(this.timeOut);
              this.timeOut = null;
              this.recording = null;
            }
          } else {
            console.log("playback failed due to audio decoding errors");
            this.setState({ isPlaying: false });
            this.recording.reset();
          }
        });
      });
      return;
    }

    if (this.state.isPlaying) {
      this.recording.pause();
      this.setState({ isPlaying: !this.state.isPlaying });
      return;
    }
    this.setState({ isPlaying: !this.state.isPlaying });

    this.recording.play(success => {
      if (success) {
        console.log("successfully finished playing");
        this.setState({ isPlaying: false, currentVal: 0 });
        if (this.timeOut) {
          clearInterval(this.timeOut);
          this.timeOut = null;
          this.recording = null;
        }
      } else {
        console.log("playback failed due to audio decoding errors");
        // reset the player to its uninitialized state (android only)
        // this is the only option to recover after an error occured and use the player again
        this.setState({ isPlaying: false });
        this.recording.reset();
      }
    });
  }
  updateIndicator() {
    this.timeOut = setInterval(
      function() {
        //  console.log('Hello:'+this.recording.isPlaying());
        if (this.recording && this.state.isPlaying) {
          console.log("Current Value: " + this.state.currentVal);
          this.recording.getCurrentTime(seconds =>
            this.setState({ currentVal: seconds })
          );
        }
      }.bind(this),
      1000
    );
  }
  render() {
    let leadDetails = this.props.leadDetails;
    console.log("item_log " + JSON.stringify(leadDetails));
    //this.loadSong(leadDetails.recording_url);
    let bgColor = "#f7412d";
    if (leadDetails.lead_tag === "Cold") {
      bgColor = "#00a7f7";
    } else if (leadDetails.lead_tag == "Warm") {
      bgColor = "#ff9900";
    }
    let imagePlayStatus = (
      <Image
        source={require("../../images/ic_sound_play.png")}
        style={{ alignSelf: "center", width: 36, height: 36 }}
      />
    );
    if (this.state.isPlaying) {
      imagePlayStatus = (
        <Image
          source={require("../../images/ic_sound_pause.png")}
          style={{ alignSelf: "center", width: 36, height: 36 }}
        />
      );
    }
    if (this.state.isLoading) {
      imagePlayStatus = (
        <ActivityIndicator
          animating={this.state.isLoading}
          style={{ flex: 1, alignSelf: "center", width: 26, height: 36 }}
          color="#00a7f7"
          size="large"
          hidesWhenStopped={true}
        />
      );
    }
    let imageCallType = null;
    if (
      leadDetails.call_direction &&
      leadDetails.call_direction == "crm_outgoing"
    ) {
      imageCallType = (
        <Image
          resize="contain"
          source={require("../../images/ic_call_outgoing.png")}
          style={{
            alignSelf: "center",
            width: 16,
            height: 16,
            marginRight: 10
          }}
        />
      );
    } else if (
      leadDetails.call_direction &&
      leadDetails.call_direction == "incoming"
    ) {
      imageCallType = (
        <Image
          resize="contain"
          source={require("../../images/ic_call_incoming.png")}
          style={{
            alignSelf: "center",
            width: 16,
            height: 16,
            marginRight: 10
          }}
        />
      );
    }

    return (
      <View
        style={{
          flexDirection: "row",
          backgroundColor: "#fff",
          borderRadius: 6,
          marginTop: 10
        }}
      >
        <View
          style={{
            flexDirection: "row",
            width: 4,
            backgroundColor: bgColor,
            borderBottomLeftRadius: 6,
            borderTopLeftRadius: 6
          }}
        />

        <View
          style={{
            paddingRight: 8,
            flexDirection: "column",
            paddingLeft: 8,
            paddingTop: 10,
            paddingBottom: 10,
            flex: 1
          }}
        >
          <TouchableOpacity
            onPress={() =>
              this.props.openC360(
                leadDetails.lead_id,
                leadDetails.schedule_activity_id
              )
            }
            style={{ marginTop: 10 }}
          >
            <View style={{ flex: 1 }}>
              <View
                style={{ flex: 2, flexDirection: "row", alignItems: "center" }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: "flex-start",
                    flexDirection: "row"
                  }}
                >
                  <Text
                    style={{
                      fontSize: 12,
                      color: "#9b9b9b",
                      marginTop: 4,
                      height: 24
                    }}
                  >
                    {leadDetails.title ? leadDetails.title + ". " : ""}
                  </Text>
                  <Text
                    style={{ fontSize: 18, color: "#45446d" }}
                    numberOfLines={1}
                  >
                    {leadDetails.customer_name}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "flex-end"
                  }}
                >
                  <Text style={{ fontSize: 14, color: "#00a7f7" }}>
                    {leadDetails.vin_allocation_status}
                  </Text>
                </View>
              </View>

              <View
                style={{ flex: 2, flexDirection: "row", alignItems: "center" }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: "flex-start",
                    flexDirection: "row"
                  }}
                >
                  <Text
                    style={{ fontSize: 15, marginTop: 6, color: "#494949" }}
                    numberOfLines={1}
                  >
                    {" "}
                    {leadDetails.variant
                      ? leadDetails.variant + ", " + leadDetails.color
                      : leadDetails.car_model_name}
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "flex-end"
                  }}
                >
                  <Text style={{ color: "#00a7f7", fontSize: 14 }}>
                    Booking Age -{" "}
                  </Text>
                  <Text
                    style={{
                      fontSize: 12,
                      color: "#5e5e5e",
                      backgroundColor: "#c6c6c6",
                      borderRadius: 4,
                      padding: 2
                    }}
                  >
                    {leadDetails.booking_age + " Days"}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>

          <View
            style={{
              height: 1,
              marginTop: 10,
              backgroundColor: "#c6c6c6",
              width: "100%"
            }}
          />

          {leadDetails.recording_url ? (
            <View
              style={{
                marginTop: 10,
                padding: 5,
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  borderRadius: 3,
                  flexDirection: "row",
                  flex: 1,
                  paddingLeft: 18,
                  marginStart: 18,
                  paddingTop: 10,
                  paddingBottom: 10,
                  backgroundColor: "#eeeeee"
                }}
              >
                <View style={{ flex: 1, paddingTop: 5, paddingBottom: 5 }}>
                  <Slider
                    style={{ flex: 1 }}
                    minimumValue={0}
                    maximumValue={this.state.maxVal}
                    value={this.state.currentVal}
                    maximumTrackTintColor="#808080"
                    minimumTintColor="#00a7f7"
                  />
                  <Text
                    style={{
                      position: "absolute",
                      bottom: 0,
                      paddingRight: 18,
                      textAlign: "right",
                      width: "100%",
                      fontSize: 9,
                      color: "#808080"
                    }}
                  >
                    {this.state.duration ? this.state.duration : ""}
                  </Text>
                </View>

                {imageCallType}
              </View>

              <View
                style={{
                  marginLeft: 5,
                  position: "absolute",
                  width: 36,
                  height: 36,
                  borderRadius: 36 / 2,
                  backgroundColor: "#fff",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <TouchableWithoutFeedback>
                  <TouchableOpacity
                    onPress={() => {
                      this.playSong(leadDetails.recording_url);
                      this.props.updateRecordings(this.recording);
                    }}
                  >
                    {imagePlayStatus}
                  </TouchableOpacity>
                </TouchableWithoutFeedback>
              </View>
            </View>
          ) : (
            <View
              style={{
                marginTop: 10,
                padding: 8,
                flexDirection: "row",
                alignItems: "center",
                backgroundColor: "#eeeeee"
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  width: "100%",
                  fontSize: 14,
                  color: "#808080"
                }}
              >
                No Recordings Found!!
              </Text>
            </View>
          )}

          <Text style={{ fontSize: 14, color: "#303030", marginTop: 5 }}>
            {"Remarks : " + leadDetails.booking_remarks}
          </Text>
        </View>
      </View>
    );
  }
}
