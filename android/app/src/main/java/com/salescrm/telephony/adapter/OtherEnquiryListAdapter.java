package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.db.LeadListDetails;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.interfaces.OnLoadLeadListListener;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class OtherEnquiryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    int strokeWidth;

    private Activity activity;
    private Preferences pref = null;
    private String[] formatStrings = {"dd MMM yyyy hh:mm aa", "yyyy-MM-dd hh:mm:ss", "yyyy-MM-dd HH:mm:ss.SSS"};
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private OnLoadLeadListListener onLoadLeadListListener;
    private boolean isLoading;
    private int visibleThreshold = 5;
    int lastVisibleItem, totalItemCount;
    private List<LeadListDetails> leadListDetailsList;
    private Date parsedDate = null;
    private FilterSelectionHolder filterSelectionHolder;
    private Realm realm;

    public OtherEnquiryListAdapter(List<LeadListDetails> leadListDetails, final Activity activity, RecyclerView recyclerView, Realm realm) {
        this.leadListDetailsList = leadListDetails;
        this.activity = activity;
        this.pref = Preferences.getInstance();
        this.pref.load(activity);
        this.realm = realm;
        strokeWidth = (int) Util.convertDpToPixel(1, activity.getApplicationContext());
        filterSelectionHolder = FilterSelectionHolder.getInstance();
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        //System.out.println("EMptyOrNOt: "+filterSelectionHolder.isEmpty());
                        //System.out.println("HeyLeadList:- TAC:-"+totalItemCount+ ", LVI:- "+lastVisibleItem+", VT:- "+visibleThreshold);
                        if(filterSelectionHolder.isEmpty()) {
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold) && new ConnectionDetectorService(activity).isConnectingToInternet()) {
                            if (onLoadLeadListListener != null) {
                                onLoadLeadListListener.onLoadMoreLeads();
                            }
                            isLoading = true;
                          }
                        }
                    }
                });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        pref = Preferences.getInstance();
        pref.load(parent.getContext());
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                view = View.inflate(parent.getContext(), R.layout.enquiry_list_other_card, null);
                return new ItemHolder(view);
            case VIEW_TYPE_LOADING:
                view = View.inflate(parent.getContext(), R.layout.progress_loader, null);
                return new ProgressHolder(view);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {


        if (viewHolder instanceof ItemHolder) {
            if(leadListDetailsList.get(position).getFirst_name() != null && !leadListDetailsList.get(position).getFirst_name().equalsIgnoreCase("")){
                ((ItemHolder)viewHolder).tvLeadListName.setText(leadListDetailsList.get(position).getFirst_name());
            }else{
                ((ItemHolder)viewHolder).tvLeadListName.setText("Customer Name");
            }

            if(leadListDetailsList.get(position).getVariant_name() != null){
                ((ItemHolder)viewHolder).tvLeadListCarName.setText(leadListDetailsList.get(position).getVariant_name());
            }else{
                ((ItemHolder)viewHolder).tvLeadListCarName.setText("No Car");
            }

            if(showDseName()) {
                ((ItemHolder) viewHolder).tvLeadListDseName.setVisibility(View.VISIBLE);
                if (leadListDetailsList.get(position).getDse_name() != null && !leadListDetailsList.get(position).getDse_name().equalsIgnoreCase("")) {
                    ((ItemHolder) viewHolder).tvLeadListDseName.setText("Dse : " + leadListDetailsList.get(position).getDse_name());
                } else {
                    ((ItemHolder) viewHolder).tvLeadListDseName.setText("Dse : Not Available");
                }
            }else {
                ((ItemHolder) viewHolder).tvLeadListDseName.setVisibility(View.GONE);
            }

            if(leadListDetailsList.get(position).getActivity_name() != null){
                parsedDate = tryParse(leadListDetailsList.get(position).getScheduled_at());
                SimpleDateFormat formatter = new SimpleDateFormat("EEE dd/MM");
                String formattedTime = formatter.format(parsedDate);
                ((ItemHolder) viewHolder).tvActivityName.setText(leadListDetailsList.get(position).getActivity_name() + " " + formattedTime);
            }

            if (leadListDetailsList.get(position).getGender() != null) {
                if (leadListDetailsList.get(position).getGender().equalsIgnoreCase("male")) {
                    ((ItemHolder)viewHolder).tvLeadListNameInitial.setText("Mr.");
                } else if (leadListDetailsList.get(position).getGender().equalsIgnoreCase("female")) {
                    ((ItemHolder)viewHolder).tvLeadListNameInitial.setText("Ms.");
                } else {
                    ((ItemHolder)viewHolder).tvLeadListNameInitial.setText("Mr.");
                }
                //((ItemHolder)viewHolder).tvLeadListNameInitial.setText(Util.getGender(leadListDetailsList.get(position).getGender()));
            } else {
                ((ItemHolder)viewHolder).tvLeadListNameInitial.setText("Mr.");
            }

            if (leadListDetailsList.get(position).getLead_tag_name() != null) {
                String leadColor = leadListDetailsList.get(position).getLead_tag_name();
                if (leadColor.contains("Hot")) {
                    ((ItemHolder)viewHolder).cardView.setCardBackgroundColor(Color.parseColor("#f7412d"));
                } else if (leadColor.contains("Cold")) {
                    ((ItemHolder)viewHolder).cardView.setCardBackgroundColor(Color.parseColor("#4DD0E1"));
                } else if (leadColor.contains("Warm")) {
                    ((ItemHolder)viewHolder).cardView.setCardBackgroundColor(Color.parseColor("#ff9900"));
                }
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = new Date();
            String todayString = dateFormat.format(date);
            Date today = tryParse(todayString);

            GradientDrawable drawable = (GradientDrawable) ((ItemHolder)viewHolder).imageViewOvalIndicator.getBackground();
            if(leadListDetailsList.get(position).getActivity_id() != null) {
                if (leadListDetailsList.get(position).getActivity_id().equalsIgnoreCase("22") ||
                        leadListDetailsList.get(position).getActivity_id().equalsIgnoreCase("23")) {
                    //Yellow
                    drawable.setColor(Color.parseColor("#FFF795"));
                    drawable.setStroke(strokeWidth, Color.parseColor("#978b00"));
                    ((ItemHolder) viewHolder).imageViewOvalIndicator.setColorFilter(Color.parseColor("#978b00"), PorterDuff.Mode.MULTIPLY);
                    ((ItemHolder) viewHolder).imageViewOvalIndicator.setImageResource(R.drawable.ic_exclamation_24);
                }else if (parsedDate != null) {
                    if (parsedDate.before(today)) {
                        //Red
                        drawable.setColor(Color.parseColor("#fd9c9c"));
                        drawable.setStroke(strokeWidth, Color.parseColor("#f74b4b"));
                        ((ItemHolder) viewHolder).imageViewOvalIndicator.setColorFilter(Color.parseColor("#f74b4b"), PorterDuff.Mode.MULTIPLY);
                        ((ItemHolder) viewHolder).imageViewOvalIndicator.setImageResource(R.drawable.ic_chevron_right_24dp);
                    } else if (parsedDate.after(today)) {
                        //Green
                        drawable.setColor(Color.parseColor("#c2f58e"));
                        drawable.setStroke(strokeWidth, Color.parseColor("#48b04b"));
                        ((ItemHolder) viewHolder).imageViewOvalIndicator.setColorFilter(Color.parseColor("#48b04b"), PorterDuff.Mode.MULTIPLY);
                        ((ItemHolder) viewHolder).imageViewOvalIndicator.setImageResource(R.drawable.ic_chevron_right_24dp);

                    } else {
                        drawable.setColor(Color.parseColor("#c2f58e"));
                        drawable.setStroke(strokeWidth, Color.parseColor("#48b04b"));
                        ((ItemHolder) viewHolder).imageViewOvalIndicator.setColorFilter(Color.parseColor("#48b04b"), PorterDuff.Mode.MULTIPLY);
                        ((ItemHolder) viewHolder).imageViewOvalIndicator.setImageResource(R.drawable.ic_chevron_right_24dp);
                    }
                }else{
                    drawable.setColor(Color.parseColor("#c2f58e"));
                    drawable.setStroke(strokeWidth, Color.parseColor("#48b04b"));
                    ((ItemHolder) viewHolder).imageViewOvalIndicator.setColorFilter(Color.parseColor("#48b04b"), PorterDuff.Mode.MULTIPLY);
                    ((ItemHolder) viewHolder).imageViewOvalIndicator.setImageResource(R.drawable.ic_chevron_right_24dp);
                }
            }else{
                    drawable.setColor(Color.parseColor("#c2f58e"));
                    drawable.setStroke(strokeWidth, Color.parseColor("#48b04b"));
                    ((ItemHolder) viewHolder).imageViewOvalIndicator.setColorFilter(Color.parseColor("#48b04b"), PorterDuff.Mode.MULTIPLY);
                    ((ItemHolder) viewHolder).imageViewOvalIndicator.setImageResource(R.drawable.ic_chevron_right_24dp);
                }

                //Hiding few things for closed stage
            if(leadListDetailsList.get(position).getStage_id().equalsIgnoreCase("9")){
                //System.out.println("Babuji: "+leadListDetailsList.get(position).getClosing_reason());
                if(leadListDetailsList.get(position).getClosing_reason() != null && !leadListDetailsList.get(position).getClosing_reason().equalsIgnoreCase("")) {
                    ((ItemHolder) viewHolder).tvActivityName.setText(leadListDetailsList.get(position).getClosing_reason());
                    ((ItemHolder) viewHolder).tvActivityName.setVisibility(View.VISIBLE);
                    ((ItemHolder) viewHolder).imageViewOvalIndicator.setVisibility(View.GONE);
                }else{
                    ((ItemHolder) viewHolder).tvActivityName.setText("Lead Closed");
                    ((ItemHolder) viewHolder).tvActivityName.setVisibility(View.VISIBLE);
                    ((ItemHolder) viewHolder).imageViewOvalIndicator.setVisibility(View.GONE);
                }
            }else if(leadListDetailsList.get(position).getActivity_name() == null && !leadListDetailsList.get(position).getStage_id().equalsIgnoreCase("9")){
                ((ItemHolder) viewHolder).tvActivityName.setText("Schedule Next Activity");
                drawable.setColor(Color.parseColor("#FFF795"));
                drawable.setStroke(strokeWidth, Color.parseColor("#978b00"));
                ((ItemHolder) viewHolder).imageViewOvalIndicator.setColorFilter(Color.parseColor("#978b00"), PorterDuff.Mode.MULTIPLY);
                ((ItemHolder) viewHolder).imageViewOvalIndicator.setImageResource(R.drawable.ic_exclamation_24);
                ((ItemHolder) viewHolder).tvActivityName.setVisibility(View.VISIBLE);
                ((ItemHolder) viewHolder).imageViewOvalIndicator.setVisibility(View.VISIBLE);
            }else {
                ((ItemHolder) viewHolder).tvActivityName.setVisibility(View.VISIBLE);
                ((ItemHolder) viewHolder).imageViewOvalIndicator.setVisibility(View.VISIBLE);
            }

        }else{
            if(!WSConstants.RECYCLER_STATUS && new ConnectionDetectorService(activity).isConnectingToInternet()) {
                    ((ProgressHolder) viewHolder).progressBar.setIndeterminate(true);
                    ((ProgressHolder) viewHolder).progressBar.setVisibility(View.VISIBLE);
            }else{
                ((ProgressHolder) viewHolder).progressBar.setVisibility(View.GONE);
            }
        }

    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
            return leadListDetailsList.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if(position >= leadListDetailsList.size()){
            return VIEW_TYPE_LOADING;
        }else{
            return VIEW_TYPE_ITEM;
        }
        //return leadListDetailses.get(position) != null ? VIEW_TYPE_ITEM : VIEW_TYPE_LOADING;
    }

    public void setOnLoadMoreListener(OnLoadLeadListListener onLoadLeadListListener) {
        this.onLoadLeadListListener = onLoadLeadListListener;
    }

    public void setLoding(){
        isLoading = false;
    }

    // inner class to hold a reference to each item of RecyclerView
    public class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public View view;

        public ImageView imgCardStatus, imgMultipleOptions;
        public int currentItem;
        ImageView imageViewOvalIndicator;
        TextView tvLeadListName, tvLeadListCarName, tvActivityName, tvLeadListNameInitial, tvLeadListDseName;
        RelativeLayout rLayoutLeadList;
        CardView cardView;

        public ItemHolder(View itemView) {
            super(itemView);
            imageViewOvalIndicator = (ImageView) itemView.findViewById(R.id.imageView3);
            tvLeadListName = (TextView) itemView.findViewById(R.id.tv_enquiry_list_name);
            tvLeadListCarName = (TextView) itemView.findViewById(R.id.tv_enquiry_list_car);
            rLayoutLeadList = (RelativeLayout) itemView.findViewById(R.id.rel_enquiry_list);
            tvActivityName = (TextView) itemView.findViewById(R.id.tv_activity_name);
            cardView = (CardView) itemView.findViewById(R.id.leadlist_cardview);
            tvLeadListNameInitial = (TextView) itemView.findViewById(R.id.tv_enquiry_list_name_mr);
            tvLeadListDseName = (TextView) itemView.findViewById(R.id.tv_enquiry_list_dse_name);

            rLayoutLeadList.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            if(getAdapterPosition() != -1 && getAdapterPosition() < leadListDetailsList.size()) {
                //System.out.println("Ye Lead Id hai:- "+leadListDetailses.get(getAdapterPosition()).getLead_id());
                pref.setLeadID(leadListDetailsList.get(getAdapterPosition()).getLead_id());
                pref.setScheduledActivityId(Util.getInt(leadListDetailsList.get(getAdapterPosition()).getScheduled_activity_id()));
                System.out.println("SC id:" + Util.getInt(leadListDetailsList.get(getAdapterPosition()).getScheduled_activity_id()));
            }

            if(pref.getLeadID().equalsIgnoreCase("")){

            }else {
                Intent intent = new Intent(activity, C360Activity.class);
                activity.startActivity(intent);
            }
        }
    }

    // Class to show loader
    public class ProgressHolder extends RecyclerView.ViewHolder{

        ProgressBar progressBar;
        public ProgressHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBarLoader);
        }
    }


    Date tryParse(String dateString) {
            for (String formatString : formatStrings) {
                try {
                    return new SimpleDateFormat(formatString).parse(dateString);
                } catch (ParseException e) {
                }
            }
        return null;
    }

    public boolean showDseName() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if(data!=null){
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i=0; i<userRoles.size();i++){
                if(userRoles.get(i).getId().equalsIgnoreCase(WSConstants.TEAM_LEAD_ROLE_ID) || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.MANAGER_ROLE_ID)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_BRANCH_HEAD)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_OPERATIONS) || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR)){
                    return true;
                }
            }
        }
        return false;
    }


}