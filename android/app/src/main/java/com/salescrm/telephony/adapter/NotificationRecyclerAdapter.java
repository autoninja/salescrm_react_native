package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.SystemClock;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AppNotificationActivity;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.activity.offline.C360ActivityOffline;
import com.salescrm.telephony.db.FcmNotificationObject;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.FCMNotification;

import java.util.Collections;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;

/**
 * Created by prateek on 10/12/16.
 */

public class NotificationRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    RealmList<FcmNotificationObject> notificationObjects = new RealmList<>();
    Realm realm = Realm.getDefaultInstance();
    RealmResults<FcmNotificationObject> fcmNotificationObject;
    private Context context;
    private Preferences pref = null;
    private long mLastClickTime = 0;
    public NotificationRecyclerAdapter(RealmList<FcmNotificationObject> notificationObjects, AppNotificationActivity appNotificationActivity) {
        this.notificationObjects = notificationObjects;
        this.context = appNotificationActivity;
        this.pref = Preferences.getInstance();
        this.pref.load(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View view = View.inflate(parent.getContext(), R.layout.notificationlist, null);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notificationlist, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
        return new VHNotification(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        //final RecyclerView.ViewHolder iViewHolder = holder;
        /*if(notificationObjects.get(position).isDeletedOffline() == true){
            ((VHNotification)holder).cardviewNotification.setVisibility(View.GONE);
        }else{
            ((VHNotification)holder).cardviewNotification.setVisibility(View.VISIBLE);
        }*/
        ((VHNotification)holder).tvSubject.setText(notificationObjects.get(position).getNotifiacation_message());
        if(notificationObjects.get(position).getLeadId()!= null && !notificationObjects.get(position).getLeadId().equalsIgnoreCase("")
                &&!notificationObjects.get(position).getLeadId().equalsIgnoreCase("")) {
            ((VHNotification) holder).tvMessage.setText("Lead Id:- "+notificationObjects.get(position).getLeadId());
        }else{
            ((VHNotification) holder).tvMessage.setText("");
        }
        if(!notificationObjects.get(position).isNotification_read()){
            ((VHNotification)holder).cardviewNotification.setBackgroundColor(Color.parseColor("#CDDBFD"));
            ((VHNotification)holder).tvSubject.setTextColor(Color.parseColor("#57ADF6"));
            ((VHNotification)holder).tvMessage.setTextColor(Color.BLACK);
            ((VHNotification)holder).tvSubject.setTypeface(null, Typeface.BOLD);
            ((VHNotification)holder).tvMessage.setTypeface(null, Typeface.BOLD);
        }else {
            ((VHNotification)holder).cardviewNotification.setBackgroundColor(Color.parseColor("#FFFFFF"));
            ((VHNotification)holder).tvSubject.setTextColor(Color.parseColor("#57ADF6"));
            ((VHNotification)holder).tvMessage.setTextColor(Color.parseColor("#8A8A8A"));
            ((VHNotification)holder).tvSubject.setTypeface(null, Typeface.NORMAL);
            ((VHNotification)holder).tvMessage.setTypeface(null, Typeface.NORMAL);
        }

    }

    @Override
    public int getItemCount() {
        return notificationObjects.size();
    }

    class VHNotification extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView tvSubject;
        private TextView tvMessage;
        private CardView cardviewNotification;
        private UnreadCount unreadCount;
        private ImageView imgDelete;

        public VHNotification(View itemView) {
            super(itemView);
            tvSubject = (TextView) itemView.findViewById(R.id.subject);
            tvMessage = (TextView) itemView.findViewById(R.id.message);
            cardviewNotification = (CardView) itemView.findViewById(R.id.cardview_notification);
            imgDelete = (ImageView) itemView.findViewById(R.id.img_delete);


            cardviewNotification.setOnClickListener(this);
            imgDelete.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.cardview_notification:
                    realm.beginTransaction();
                    fcmNotificationObject = realm.where(FcmNotificationObject.class).findAll();
                    fcmNotificationObject.get(notificationObjects.size() - 1 - getAdapterPosition()).setNotification_read(true);
                    realm.commitTransaction();
                    cardviewNotification.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    tvMessage.setTextColor(Color.parseColor("#8A8A8A"));
                    tvSubject.setTypeface(null, Typeface.NORMAL);
                    tvMessage.setTypeface(null, Typeface.NORMAL);
                    int count = realm.where(FcmNotificationObject.class).equalTo("notification_read", false).findAll().size();
                    if (context instanceof AppNotificationActivity) {
                        try {
                            unreadCount = (UnreadCount) context;
                            unreadCount.sendUnreadCount(count);
                        } catch (ClassCastException e) {
                            throw new ClassCastException(context.toString() + " must implement UnreadCount");
                        }

                    }
                    if (fcmNotificationObject.get(notificationObjects.size() - 1 - getAdapterPosition()).getLeadId() != null &&
                            !fcmNotificationObject.get(notificationObjects.size() - 1 - getAdapterPosition()).getLeadId().equalsIgnoreCase("")) {
                        pref.setLeadID(fcmNotificationObject.get(notificationObjects.size() - 1 - getAdapterPosition()).getLeadId());
                        if (pref.getLeadID().equalsIgnoreCase("0")) {

                        } else {
                            Intent intent = new Intent(context, C360Activity.class);
                            context.startActivity(intent);
                        }
                    }
                    break;

                case R.id.img_delete:
                    // To stop two quick clicks
                    /*if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();*/
                    /*realm.beginTransaction();
                    fcmNotificationObject = realm.where(FcmNotificationObject.class).findAll();
                    fcmNotificationObject.get(notificationObjects.size() - 1 - getAdapterPosition()).deleteFromRealm();
                    realm.commitTransaction();
                    int counts = realm.where(FcmNotificationObject.class).equalTo("notification_read", false).findAll().size();
                    if (context instanceof AppNotificationActivity) {
                        try {
                            unreadCount = (UnreadCount) context;
                            unreadCount.sendUnreadCount(counts);
                        } catch (ClassCastException e) {
                            throw new ClassCastException(context.toString() + " must implement UnreadCount");
                        }

                    }
                    notifyItemRemoved(getAdapterPosition());
                    //notifyItemRangeChanged(getAdapterPosition(), notificationObjects.size());
                    notificationObjects.clear();
                    notificationObjects.addAll(realm.where(FcmNotificationObject.class).findAll());
                    Collections.reverse(notificationObjects);
                    if(realm.where(FcmNotificationObject.class).findAll().size() == 0){
                        ((Activity)activity).finish();
                    }*/
                    realm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            fcmNotificationObject = realm.where(FcmNotificationObject.class).findAll();
                            if(fcmNotificationObject.get(notificationObjects.size() - 1 - getAdapterPosition()) != null) {
                                fcmNotificationObject.get(notificationObjects.size() - 1 - getAdapterPosition()).deleteFromRealm();
                            }

                        }
                    }, new Realm.Transaction.OnSuccess(){

                        @Override
                        public void onSuccess() {
                            int counts = realm.where(FcmNotificationObject.class).equalTo("notification_read", false).findAll().size();
                            if (context instanceof AppNotificationActivity) {
                                try {
                                    unreadCount = (UnreadCount) context;
                                    unreadCount.sendUnreadCount(counts);
                                } catch (ClassCastException e) {
                                    throw new ClassCastException(context.toString() + " must implement UnreadCount");
                                }

                            }
                            notifyItemRemoved(getAdapterPosition());
                            //notifyItemRangeChanged(getAdapterPosition(), notificationObjects.size());
                            notificationObjects.clear();
                            notificationObjects.addAll(realm.where(FcmNotificationObject.class).findAll());
                            Collections.reverse(notificationObjects);
                            if(realm.where(FcmNotificationObject.class).findAll().size() == 0){
                                ((Activity)context).finish();
                            }
                        }
                    }, new Realm.Transaction.OnError(){

                        @Override
                        public void onError(Throwable error) {

                        }
                    });
                    break;
            }
        }
    }

    public interface UnreadCount {
        void sendUnreadCount(int count);
    }
}
