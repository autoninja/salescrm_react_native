
import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image, Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import RestClient from '../../network/rest_client';
import { ScrollView } from 'react-native-gesture-handler';
import GenerateViews from './generate_views'
import SuccessView from "../uielements/SuccessView";
import { TaskConstants, FormQuestionIdConstants } from '../../utils/Constants';
import Loader from "../../components/uielements/Loader";
import Toast, { DURATION } from 'react-native-easy-toast';
import NavigationService from '../../navigation/NavigationService'
import { NavigationActions } from 'react-navigation';

const styles = StyleSheet.create({
	topbar: {
		backgroundColor: '#fff',
		height: 50,
		flexDirection: 'row'
	},
	innerView: {
		flex: 1,
		margin: 5,
		backgroundColor: "#fff",
		borderRadius: 4,
	},
	actionButtonView: {
		height: 50,
		backgroundColor: "#0076ff",
		justifyContent: "center",
		margin: 10,
		borderRadius: 6
	}
});

export default class FormRenderMain extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			isFetching: false,
			pre_form_object: null,
			post_form_object: this.props.navigation.getParam('post_form_object', null),
			should_render_post_form: this.props.navigation.getParam('should_render_post_form', false),
			pntModel: this.props.navigation.getParam('pntModel', null),
			allMandatoryFieldsSelected: false,
			info: this.props.info,
			responseArray: [],
			previousFormAnswers: this.props.navigation.getParam("previousFormAnswers", []),
			allMandatoryFieldsDone: [],
			leadLastUpdate: this.props.info.leadLastUpdated,
			postBookingDriveOrVisit: this.props.navigation.getParam("postBookingDriveOrVisit", false)
		}
	}

	componentDidMount() {
		//console.error("getManualActivities")
		if (!this.state.should_render_post_form || this.state.post_form_object == null) {
			this.makeRemoteRequestToGetForm();
		}
	}



	makeRemoteRequestToGetForm() {
		this.setState({ isLoading: true, isFetching: true })
		let { info, postBookingDriveOrVisit } = this.state
		let input_data = {
			leadId: info.leadId,
			activity_id: info.actionId
		}

		if (postBookingDriveOrVisit) {
			new RestClient().getPostBookingActivityForm(input_data).then((data) => {
				//console.error(data)
				if (data.statusCode == "2002") {
					this.setState({
						isLoading: false,
						isFetching: false,
						post_form_object: data.result,
					})
				}
				else {
					//Error handling case should be there here
				}

			}).catch(error => {
				//Error handling case should be there here
			});
		}
		else if (this.state.info.activityId == TaskConstants.PLAN_NEXT_TASK.id) {
			new RestClient().getManualActivitiesForPnt(this.state.info.leadId).then((data) => {
				if (data.statusCode == "2002") {
					this.setState({
						isLoading: false,
						isFetching: false,
						post_form_object: data.result.form_object,
					})
				}
				else {
					//Error handling case should be there here
				}

			}).catch(error => {
				//Error handling case should be there here
			});
		}
		else {
			let leaddata = {
				lead_data: [{
					lead_id: this.state.info.leadId,
					action_id: this.state.info.actionId,
					scheduled_activity_id: this.state.info.scheduledActivityId
				}]
			}
			new RestClient().getSplitFormObjects(leaddata).then((data) => {
				//console.error('getSplitFormObjects')
				//console.log("getSplitFormObjects", JSON.stringify(data.result[0].form_object.form_object))
				if (data.statusCode == "2002") {
					this.setState({
						isLoading: false,
						isFetching: false,
						post_form_object: data.result[0].form_object.post_form_object,
						pre_form_object: data.result[0].form_object.pre_form_object
					})
				}
				else {
					//Error handling case should be there here
				}

			}).catch(error => {
				console.log(error);
				//console.error(error); 
				//this.setState({ loading: !this.state.loading });
				if (!error.response) {
					//this.showAlert('No Interent Connection');
				}
				//Error handling case should be there here
			});
		}

	}

	generateDynamicView() {
		let pntModel = this.state.pntModel
		let form_object = null;
		let fAnswerId = null;
		if (this.state.should_render_post_form) {
			form_object = this.state.post_form_object;
			fAnswerId = this.state.pntModel.fAnswerId;
		}
		else {
			form_object = this.state.pre_form_object;
		}
		console.log('fAnswerId:' + fAnswerId)
		if (form_object) {
			let defaultCases = ''
			for (var i = 0; i < form_object.questionChildren.length; i++) {

				return form_object.questionChildren[i].values.map((item, key) => {
					if (this.setMainViewVisibility(item, fAnswerId)) {
						return (<GenerateViews fAnswerId={fAnswerId} key={key} innerObjects={item}
							formResponse={this.getFormResponse}
							mandatoryFields={this.allMandatoryFields} />)
					}
				})

			}
		}
	}

	setMainViewVisibility(item, fAnswerId) {
		if (fAnswerId == 5 || fAnswerId == 6 || fAnswerId == 24) {
			if (item.questionId == 3) {
				return false
			}
		}

		return true;
	}

	submit(action) {
		let { responseArray, post_form_object, pntModel, info } = this.state
		switch (action.name) {
			case 'reschedule':
			case 'submit':
				//Final Submission of only form
				//console.error("Final Submission of Only Forms", this.state.leadLastUpdate)
				if (info.activityId == TaskConstants.PLAN_NEXT_TASK.id) {
					this.submitManualActivity()
				}
				else {
					this.callSubmitForm()
				}
				break;
			case 'confirm_otp':
				this.props.navigation.push('OtpPicker', { post_form_object, previousFormAnswers: responseArray })
				break;
			case 'pnt':
				this.props.navigation.push('PlanNextTask', { post_form_object, previousFormAnswers: responseArray, pntOpendFromForm: true })
				break;

		}

	}
	getServerFormResponse(responseArray) {
		responseArray.map((item) => {
			delete item.questionId;
			return item;
		});
		return responseArray;
	}

	submitManualActivity() {
		let { responseArray } = this.state;
		let { info } = this.state;
		let input_data = {
			lead_id: info.leadId,
			lead_last_updated: info.leadLastUpdated,
			schedule_activity_id: info.scheduledActivityId,
			scheduled_type: "2",
			form_response: this.getServerFormResponse(responseArray),
		}
		this.setState({ isLoading: true })
		new RestClient().submitManualActivityForPnt(input_data).then((data) => {
			console.log("submitForm", JSON.stringify(data))
			if (data.statusCode == "2002") {
				this.onSuccessFormSubmission("Task has been created");
			}

		}).catch(error => {
			console.log(error);
			if (!error.response) {

			}
		});
	}

	onSuccessFormSubmission(message) {
		this.setState({ isFormSubmitSuccess: true, successMessage: message, isLoading: false });
		setTimeout(
			function () {
				//redirect
				this.setState({ isFormSubmitSuccess: false });
				if (Platform.OS == "ios") {
					NavigationService.resetScreen({
						routeName: "Home",
						action: NavigationActions.navigate({ routeName: "MyTasks" })
					});
				}
				else if (Platform.OS == "android") {
					//redirect to specific activity;
				}
			}.bind(this),
			1000
		);
	}

	callSubmitForm() {
		let { responseArray, previousFormAnswers } = this.state;
		let { info } = this.state;
		let input_data = {
			lead_id: info.leadId,
			scheduled_activity_id: info.scheduledActivityId,
			action_id: info.actionId,
			lead_last_updated: info.leadLastUpdated,
			form_response: [...previousFormAnswers, ...responseArray]
		}
		//console.log("Final Submission of Only Forms", JSON.stringify(input_data))
		this.setState({ isLoading: true, isFetching: true })

		new RestClient().submitForm(input_data).then((data) => {
			//console.log('Final Submission of Only Forms submitForm', JSON.stringify(data))
			if (data.statusCode == "2002") {
				this.onSuccessFormSubmission("Form has been submitted");
			}

		}).catch(error => {
			console.log(error);
			//console.error(error); 
			//this.setState({ loading: !this.state.loading });
			if (!error.response) {
				//this.showAlert('No Interent Connection');
			}
		});

	}

	getAction() {
		let { info, should_render_post_form, responseArray, postBookingDriveOrVisit } = this.state
		let { activityId } = info;
		let action;
		let forTestDriveAndVisits = responseArray.find((data) => data.questionId == '74' || '72')
		let enableSendOtpforTDAndVisit = forTestDriveAndVisits?forTestDriveAndVisits.value.answerValue == 1:false
		//responseArray.find((data) => data.questionId == '74')
		//console.log("getAction()",enableSendOtpforTDAndVisit)
		if (postBookingDriveOrVisit) {
			action = { name: "confirm_otp", text: "Send OTP" }
		}
		else if (should_render_post_form) {
			action = { name: "submit", text: "Submit" }
		}
		else {

			if (activityId == TaskConstants.HOME_VISIT.id
				|| activityId == TaskConstants.SHOWROOM_VISIT.id
				|| activityId == TaskConstants.TEST_DRIVE_HOME.id
				|| activityId == TaskConstants.TEST_DRIVE_SHOWROOM.id) {
				if (info.isClientILomBank || info.isClientILom) {
					action = { name: "pnt", text: "Plan Next Task" }
				}
				else {
					if(enableSendOtpforTDAndVisit){
						action = { name: "confirm_otp", text: "Send OTP" }
					}else{
						action = { name: "pnt", text: "Plan Next Task" }
					}
					
				}
			}
			else if (activityId == TaskConstants.LOST_DROP.id
				|| activityId == TaskConstants.EVALUATION_REQUEST.id) {
				action = { name: "submit", text: "Submit" }
			}
			else {
				action = { name: "pnt", text: "Plan Next Task" }
			}
		}
		for (let i = 0; i < responseArray.length; i++) {
			if (responseArray[i].questionId == FormQuestionIdConstants.RESCHEDULED
				&& responseArray[i].value.answerValue == 1) {
				action = { name: "reschedule", text: "Reschedule" }
				break;
			}
			if (responseArray[i].questionId == FormQuestionIdConstants.LOST_DROP_NO
				&& responseArray[i].value.answerValue == 0) {
				action = { name: "pnt", text: "Plan Next Task" }
			}

		}
		return action;
	}

	getFormResponse = (item, update) => {
		let { responseArray } = this.state;
		let itemExists = false;
		let itemPosition = 0;
		//console.log("Item Received item" ,JSON.stringify(item), update)
		if (update && update == -1 && responseArray.length > 0) {
			for (i = 0; i < responseArray.length; i++) {
				if (responseArray[i].name == item.name) {
					responseArray.splice(i, 1)
				}
			}
		} else {
			//console.log("Item Received item+ " ,JSON.stringify(item))
			if (responseArray.length == 0) {
				responseArray.push(item)
			} else {
				for (var i = 0; i < responseArray.length; i++) {
					if (responseArray[i].name == item.name) {
						itemExists = true;
						itemPosition = i;
					}
				}
				if (itemExists) {
					responseArray[itemPosition] = item
				} else {
					responseArray.push(item)
				}

			}
		}
		//console.log("Item Received responseArray" ,JSON.stringify(responseArray))
		//{()=>this.setState({responseArray})}
		this.setState({ responseArray })

	}

	allMandatoryFields = (item, update) => {
		//console.log("allMandatoryFieldDone", JSON.stringify(item), update)
		let { allMandatoryFieldsDone } = this.state
		let itemExists = false;
		let itemPosition = 0;
		if (update && update == -1 && allMandatoryFieldsDone.length > 0) {
			for (var i = 0; i < allMandatoryFieldsDone.length; i++) {
				if (allMandatoryFieldsDone[i].id == item.id) {
					allMandatoryFieldsDone.splice(i, 1)
				}
			}
		} else {
			if (allMandatoryFieldsDone.length == 0) {
				allMandatoryFieldsDone.push(item)
			} else {
				for (var i = 0; i < allMandatoryFieldsDone.length; i++) {
					if (allMandatoryFieldsDone[i].id == item.id) {
						itemExists = true;
						itemPosition = i;
					}
				}
				if (itemExists) {
					allMandatoryFieldsDone[itemPosition] = item
				} else {
					allMandatoryFieldsDone.push(item)
				}

			}
		}
		//console.log("Item Received allMandatoryFieldsDone" ,JSON.stringify(allMandatoryFieldsDone))
		{ () => this.setState({ allMandatoryFieldsDone }) }
	}

	isAllFieldDone() {
		let { allMandatoryFieldsDone } = this.state;
		for (var i = 0; i < allMandatoryFieldsDone.length; i++) {
			if (!allMandatoryFieldsDone[i].isSelected) {
				return false;
			}
		}
		return true
	}

	showAlert(alert) {
		this.refs.alert.show(alert);
	}

	render() {
		let action = this.getAction();
		return (
			<View style={{ flex: 1, backgroundColor: '#2e5e86' }}>
				<Toast
					ref="alert"
					style={{ backgroundColor: 'red' }}
					position='top'
					fadeInDuration={1000}
					fadeOutDuration={1000}
					opacity={0.8}
					textStyle={{ color: 'white' }}
				/>



				<ScrollView>
					<View style={styles.innerView}>
						{this.generateDynamicView()}
					</View>

				</ScrollView>

				<TouchableOpacity style={styles.actionButtonView}
					onPress={() => {
						(this.isAllFieldDone()) ? this.submit(action) : this.showAlert("Please select all mandatory fields");
					}}
				>
					<Text style={{ fontSize: 18, color: "#fff", textAlign: "center" }}>{action.text}</Text>
				</TouchableOpacity>
				{this.state.isLoading && <Loader lightMode={false} isLoading={this.state.isLoading} />}
				{this.state.isFormSubmitSuccess && (
					<SuccessView text={this.state.successMessage} />
				)}
			</View>
		)
	}
}

