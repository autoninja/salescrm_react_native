import { realm } from './realm_schemas';
import { EventsMain, EventsLeadSourceCategory, EventsLeadSource, Events } from '../models/active_events_model'
const ActiveEventsDBService = {

  save: function (activeEvents) {
    realm.write(() => {
      activeEvents.map((event) => {
        realm.create('EventsMainDB', event, true);
      })
    })
    let events = realm.objects('EventsMainDB');
    console.log('EventsMainDB.length', events.length);
  },
  getActiveEvents: () => {
    let eventsMainDB = realm.objects('EventsMainDB');
    let events = [];
    if (eventsMainDB && eventsMainDB.length > 0) {
      eventsMainDB.map((eventsMain) => {
        let eventsLeadSourceCategoriesVal = [];
        eventsMain.eventsLeadSourceCategories.map((eventsLeadSourceCategory) => {
          let leadSourcesVal = [];
          eventsLeadSourceCategory.leadSources.map((leadSource) => {
            let eventsVal = []
            leadSource.events.map((eventData) => {
              eventsVal.push(new Events(eventData.id, eventData.name, eventData.start_date, eventData.end_date));
            })
            leadSourcesVal.push(new EventsLeadSource(leadSource.id, leadSource.name, eventsVal));
          })
          eventsLeadSourceCategoriesVal.push(new EventsLeadSourceCategory(eventsLeadSourceCategory.id, eventsLeadSourceCategory.name, leadSourcesVal));
        })
        events.push(new EventsMain(eventsMain.locationId, eventsMain.locationName, eventsLeadSourceCategoriesVal));
      })


    }
    return events;
  },
  deleteAll: function () {
    realm.write(() => {
      realm.delete(realm.objects('EventDB'));
      realm.delete(realm.objects('EventLeadSourceDB'));
      realm.delete(realm.objects('EventLeadSourceCategoryDB'));
      realm.delete(realm.objects('EventsMainDB'));
    })
  },
}

export default ActiveEventsDBService;