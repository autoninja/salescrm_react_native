import React, { Component } from 'react';

import { View, Text, ActivityIndicator, Alert } from 'react-native';
import axios from 'axios';
import RestClient from '../network/rest_client'
import UserInfoService from '../storage/user_info_service'
import StorageUtils from '../storage/storage_utils'

import GamificationConfigurationService from '../storage/gamification_configuration_service';
import DealershipService from '../storage/dealership_service';
import VehicleDBService from '../storage/vehicle_db_service';
import AppConfDBService from '../storage/app_conf_db_service';
import ShowRoomLocationDBService from '../storage/showroom_location_db_service';
import ActivityDBService from '../storage/activity_db_service';
import EnquirySourceDBService from '../storage/enquiry_source_db_service';
import TeamUserDBService from '../storage/team_user_db_service';
import EventCategoriesDBService from '../storage/event_categories_db_service';
import ActiveEventsDBService from '../storage/active_events_db_service';
import EvaluationManagersConfDBService from '../storage/evaluation_managers_conf_db_service';


import DealershipModel from '../models/dealership_model'
import { GameLocationsModel } from '../models/gamification_configuration_model'


import { UserInfoModel, UserBrandsModel, UserRolesModel, UserLocationsModel } from '../models/user_info_model'
import { VehicleModel, VehicleModelsModel, VehicleModelsVariantModel } from '../models/vehicle_model'
import { AppConfModel } from '../models/app_conf_model'
import { CommonModel } from '../models/common_model'
import { ShowRoomLocationModel, ActvityModel, EnquirySourceMainModel, EnquirySourceCategoryModel, SubEnquirySourceModel } from '../models/lead_elements_model'
import { TeamUserModel, TeamModel, UserModel } from '../models/team_user_model'
import { EventsMain, EventsLeadSourceCategory, EventsLeadSource, Events } from '../models/active_events_model'
import { EvaluationManagerConfModel } from '../models/evaluation_manager_conf_model'
import { StackActions, NavigationActions } from 'react-navigation';
import styles from '../styles/styles'

import * as async_storage from '../storage/async_storage';
import CleverTapPush from '../clevertap/CleverTapPush'

export default class RefreshData extends Component {

  constructor(props) {
    super(props);
    this.state = { loading: true };
    this.teamUserCallCount = 0;
  }

  componentWillMount() {
    this.getDelearInfo();

  }

  componentDidMount() {
    this._mounted = true
  }

  componentWillUnmount() {
    this._mounted = false
  }

  getDelearInfo() {
    new RestClient().getDealerships().then((data) => {

      if (data.statusCode === 4001 || data.statusCode == 5001) {
        Alert.alert(
          'Failed',
          'Please try again',
          [
            { text: 'Logout', onPress: () => this.logout() },
            { text: 'Retry', onPress: () => this.getDelearInfo() },
          ],
          { cancelable: false }
        )
        return;
      }

      DealershipService.deleteAll();
      data.result.forEach(function (result) {
        DealershipService.save(
          new DealershipModel(result.dealer.id,
            result.dealer.name,
            result.dealer.db_name,
            result.users.id,
            result.users.name,
            result.users.image_url,
            false
          )
        );
      });
      //this.getUserInfo();
      this.getGetGamificationConfiguration();
    }).catch((error) => {
      if (error.response && error.response.status == 401) {
        Alert.alert(
          'Failed',
          'Please try again',
          [
            { text: 'Logout', onPress: () => this.logout() },
            { text: 'Retry', onPress: () => this.getDelearInfo() },
          ],
          { cancelable: false }
        )
      }
    });;


  }

  logout() {
    var logout = StackActions.reset({
      index: 0, // <-- currect active route from actions array
      key: null,
      actions: [
        NavigationActions.navigate({ routeName: 'Login' }),
      ],
    });
    this.props.navigation.dispatch(logout);
  }

  getGetGamificationConfiguration() {
    new RestClient().getGamificationConfiguration().then((data) => {
      if (data.statusCode === 4001 || data.statusCode == 5001) {
        Alert.alert(
          'Failed',
          'Please try again',
          [
            { text: 'Logout', onPress: () => this.logout() },
            { text: 'Retry', onPress: () => this.getGetGamificationConfiguration() },
          ],
          { cancelable: false }
        )
        return;
      }

      GamificationConfigurationService.deleteAll();

      let gameLocations = [];
      data.result.locations.forEach(function (result) {
        console.log(result);
        GamificationConfigurationService.save(new GameLocationsModel(
          result.id,
          result.name,
          result.gamification,
          result.targets_locked,
          result.start_date));
      });
      let systemDate;
      var serverDate = data.result.date;
      console.log('SystemDate:' + serverDate);
      if (serverDate) {
        serverDate = serverDate.split(" ")[0].split("-");
        console.log('SystemDate:' + serverDate);
        if (serverDate && serverDate.length == 3) {
          systemDate = new Date(serverDate[0], (serverDate[1] - 1), serverDate[2], 0, 0, 0, 0);
        }
      }
      if (systemDate) {
        async_storage.setSystemDate(new Date(systemDate).toString());
        global.selectedGameDate = new Date(systemDate);
        global.systemDate = new Date(systemDate);
      }
      else {
        async_storage.setSystemDate(new Date().toString());
        global.selectedGameDate = new Date();
        global.systemDate = new Date();;
      }

      if (GamificationConfigurationService.getLocations().length > 0) {
        global.selectedGameLocationId = GamificationConfigurationService.getLocations()[0].id;
        async_storage.setSelectedGameLocationId(global.selectedGameLocationId.toString()).then(() => {
        });
      }

      this.getUserInfo();
    }).catch((error) => {
      console.error(error)

      Alert.alert(
        'Failed',
        'Please try again',
        [
          { text: 'Logout', onPress: () => this.logout() },
          { text: 'Retry', onPress: () => this.getGetGamificationConfiguration() },
        ],
        { cancelable: false }
      )

    });
  }

  navigateHome() {
    CleverTapPush.pushProfile(UserInfoService.findFirst(), UserInfoService.getRoles(), UserInfoService.getLocations());
    this.setState({ loading: false });
    async_storage.setLastOpenedDate(new Date().getTime());
    var resetAction = StackActions.reset({
      index: 0, // <-- currect active route from actions array
      key: null,
      actions: [
        NavigationActions.navigate({ routeName: 'SplashScreen' }),
      ],
    });;
    async_storage.getGameIntroSeen().then((seen) => {
      if ((!seen || seen == "0") && StorageUtils.isGamificationEnabled()) {
        resetAction = StackActions.reset({
          index: 0, // <-- currect active route from actions array
          key: null,
          actions: [
            NavigationActions.navigate({ routeName: 'GameIntro' }),
          ],
        });

      }
      this.props.navigation.dispatch(resetAction);
    });
  }

  getUserInfo() {
    new RestClient().getUserInfo().then((data) => {

      UserInfoService.deleteAll();
      console.log('Hello Wrols');
      console.log(JSON.stringify(data));

      let userBrandModels = [];
      let userRolesModels = [];
      let userLocationsModels = [];

      data.result.roles_user.forEach(function (result) {
        userRolesModels.push(new UserRolesModel(result.id, result.name))
      })
      data.result.locations.forEach(function (result) {
        userLocationsModels.push(new UserLocationsModel(result.location_id, result.name))
      })
      var id = data.result.data.id;

      async_storage.setAppUserId(id.toString()).then(() => {
        global.appUserId = id;
      });
      console.log("ID:::" + data.result.data.id);
      let userInfoModel = new UserInfoModel(
        data.result.data.id,
        data.result.data.name,
        data.result.data.user_name,
        data.result.data.mobile,
        data.result.data.email,
        data.result.data.dealer,
        data.result.data.dp_url,
        data.result.team_id,
        data.result.brands.brand_id,
        data.result.brands.brand_name,
        userRolesModels,
        userLocationsModels
      )
      UserInfoService.save(userInfoModel);
      global.DEALER_NAME = data.result.data.dealer;
      console.log("Daa");
      console.log(JSON.stringify(userInfoModel));
      if (!this._mounted) {
        return;
      }
      this.getAppConf();



    });

  }

  getAppConf() {
    new RestClient().getAppConf().then((data) => {

      AppConfDBService.deleteAll();
      let appConfModels = [];
      data.result.forEach(function (appConf) {
        appConfModels.push(new AppConfModel(appConf.id, appConf.key, appConf.value, appConf.is_edit, appConf.is_active))
      });
      AppConfDBService.save(appConfModels);
      if (!this._mounted) {
        return;
      }
      this.getEventCategories();
    });
  }

  getEventCategories() {
    new RestClient().getEventCategories().then((data) => {
      EventCategoriesDBService.deleteAll();
      data.result.categories.forEach(function (category) {
        EventCategoriesDBService.save(new CommonModel(category.id, category.name));
      });
      if (!this._mounted) {
        return;
      }
      this.getLeadElements();
    });
  }

  getLeadElements() {
    new RestClient().getLeadElements().then((data) => {

      ShowRoomLocationDBService.deleteAll();
      ActivityDBService.deleteAll();
      EnquirySourceDBService.deleteAll();


      // Showrooms
      let showrooms = [];
      data.result.locations.forEach(function (location) {
        showrooms.push(new ShowRoomLocationModel(location.id, location.name))
      });
      ShowRoomLocationDBService.save(showrooms);

      //Actvities
      let activities = [];
      data.result.activities.forEach(function (activity) {
        activities.push(new ActvityModel(activity.id, activity.name))
      });
      ActivityDBService.save(activities);

      //EnquirySource:

      let enquirySourcesMain = [];
      let enquirySourcesCategories = [];

      data.result.enqSource.lead_sources.map((item) => {
        enquirySourcesMain.push(new EnquirySourceMainModel(item.id, item.name));
      })

      data.result.enqSource.lead_source_categories.map((category) => {
        let subSources = [];
        category.lead_sources.data.map((subSource) => {
          subSources.push(new SubEnquirySourceModel(subSource.id, subSource.name))
        })
        enquirySourcesCategories.push(new EnquirySourceCategoryModel(category.id, category.name, subSources))
      })

      EnquirySourceDBService.save(enquirySourcesMain, enquirySourcesCategories);

      if (!this._mounted) {
        return;
      }
      this.getTeamUserDetails();
    });
  }

  callTeamUsers(leadSourceIds, locationList) {
    if (this.teamUserCallCount < locationList.length) {
      new RestClient().getTeamUserDetails({ leadSourceIds, locationId: locationList[this.teamUserCallCount].id }).then((data) => {
        let teamModels = [];
        let userModels = [];
        data.result.team_data.map((team) => {
          teamModels.push(new TeamModel(team.manager_id, team.team_leader_id, team.user_id));
        });

        data.result.users.map((user) => {
          userModels.push(new UserModel(user.id, user.name, user.mobile_number, user.role_id));
        });
        TeamUserDBService.save(new TeamUserModel(locationList[this.teamUserCallCount].id, teamModels, userModels));
        this.teamUserCallCount++;
        this.callTeamUsers(leadSourceIds, locationList);
        if (!this._mounted) {
          return;
        }
      });
    }
    else {
      this.getCarModelsInfo();
      this.teamUserCallCount = 0;
    }

  }


  getTeamUserDetails() {
    let enquirySources = EnquirySourceDBService.getEnquirySourceMain();
    let leadSourceIds = [];
    for (var i = 0; i < enquirySources.length; i++) {
      leadSourceIds.push(enquirySources[i].id);
    }
    TeamUserDBService.deleteAll();
    this.callTeamUsers(leadSourceIds, UserInfoService.getLocationsList());

  }
  getCarModelsInfo() {
    new RestClient().getModelsAndVariants().then((data) => {

      VehicleDBService.deleteAll();
      console.log('Hello Wrols');
      console.log(JSON.stringify(data));

      let vehicles = [];

      data.result.map((brand) => {
        let vehicleModels = [];
        brand.brand_models.map((model) => {
          let vehicleVariants = [];
          model.car_variants.map((variant) => {
            vehicleVariants.push(
              new VehicleModelsVariantModel(variant.variant_id, variant.variant,
                variant.fuel_type_id, variant.fuel_type, variant.color_id, variant.color))
          })
          vehicleModels.push(new VehicleModelsModel(model.model_id, model.model_name, model.category, vehicleVariants))
        })
        // VehicleModel, VehicleModelsModel, VehicleModelsVariantModel
        vehicles.push(new VehicleModel(brand.brand_id, brand.brand_name, vehicleModels));
      })

      VehicleDBService.save(vehicles);
      if (!this._mounted) {
        return;
      }

      this.getActiveEvents();


    });
  }
  getActiveEvents() {
    new RestClient().getActiveEvents().then((data) => {

      ActiveEventsDBService.deleteAll();
      console.log('Hello Wrols');
      console.log(JSON.stringify(data));

      let events = [];
      //EventsMain, EventsLeadSourceCategory, EventsLeadSource, Events
      data.result.locations.map((location) => {
        let leadSourceCategoriesVal = [];
        location.lead_source_categories.map((leadSourceCategory) => {
          let leadSourcesVal = []
          leadSourceCategory.lead_sources.map((leadSource) => {
            let eventsVal = [];
            leadSource.events.map((event) => {
              eventsVal.push(new Events(event.id, event.name, event.start_date, event.end_date));
            })
            leadSourcesVal.push(new EventsLeadSource(leadSource.id, leadSource.name, eventsVal));
          })
          leadSourceCategoriesVal.push(new EventsLeadSourceCategory(leadSourceCategory.id, leadSourceCategory.name, leadSourcesVal));
        })
        events.push(new EventsMain(location.id, location.name, leadSourceCategoriesVal));
      })

      ActiveEventsDBService.save(events);
      if (!this._mounted) {
        return;
      }

      this.getEvaluationManagersConf();


    }).catch((error) => {
      console.error(error)

      Alert.alert(
        'Failed to get events data',
        'Please try again',
        [
          { text: 'Logout', onPress: () => this.logout() },
          { text: 'Retry', onPress: () => this.getActiveEvents() },
        ],
        { cancelable: false }
      )

    });;
  }
  getEvaluationManagersConf() {
    new RestClient().getEvaluationManagersConf().then((data) => {
      EvaluationManagersConfDBService.deleteAll();
      let evaluationMangersVal = [];
      data.result.map((result) => {
        evaluationMangersVal.push(new EvaluationManagerConfModel(result.location_id, result.has_evaluation_managers));
      })
      EvaluationManagersConfDBService.save(evaluationMangersVal);

      if (!this._mounted) {
        return;
      }
      this.getExternalApiKeys();


    }).catch((error) => {
      console.error(error)

      Alert.alert(
        'Failed to get evaluation managers configuration',
        'Please try again',
        [
          { text: 'Logout', onPress: () => this.logout() },
          { text: 'Retry', onPress: () => this.getEvaluationManagersConf() },
        ],
        { cancelable: false }
      )

    });;
  }

  getExternalApiKeys() {
    new RestClient().getExternalAPIKeys().then((data) => {
      if (data.result && data.result.gov_data_api_keys) {
        async_storage.setGovtApiKeys(data.result.gov_data_api_keys);
        global.GOVT_API_KEYS = data.result.gov_data_api_keys;
      }
      if (!this._mounted) {
        return;
      }
      this.navigateHome();


    }).catch((error) => {
      console.error(error)

      Alert.alert(
        'Failed to get external api keys',
        'Please try again',
        [
          { text: 'Logout', onPress: () => this.logout() },
          { text: 'Retry', onPress: () => this.getExternalApiKeys() },
        ],
        { cancelable: false }
      )

    });;;
  }

  render() {
    return (
      <View style={{
        backgroundColor: '#fff', flex: 1, justifyContent: 'center',
        alignItems: 'center',
      }}>

        <ActivityIndicator
          animating={this.state.loading}
          style={{ alignSelf: 'center', }}
          color="#007fff"
          size="large"
          hidesWhenStopped={true}

        />
        <Text style={{ fontSize: 18, color: '#303030', marginTop: 10 }}> Setting up data..</Text>
      </View>)


  }
}

// class GameLocationsModel{
//   constructor(id, name, gamification, targets_locked, start_date){
//     this.id = id;
//     this.name = name;
//     this.gamification = gamification;
//     this.targets_locked = targets_locked;
//     this.start_date = start_date;
//   }
// }
