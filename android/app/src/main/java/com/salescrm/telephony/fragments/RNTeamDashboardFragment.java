package com.salescrm.telephony.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.EnqSourceCategoryDB;
import com.salescrm.telephony.db.EnqSubInnerSourceDB;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.events.EventsCategoryDb;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.dbOperation.VehicleDBUtils;
import com.salescrm.telephony.interfaces.TeamDashboardSwipedListener;
import com.salescrm.telephony.model.NotificationModel;
import com.salescrm.telephony.model.TeamDashboardSwiped;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.WSConstants;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class RNTeamDashboardFragment extends Fragment
        implements DefaultHardwareBackBtnHandler, TeamDashboardSwipedListener {
    private ReactRootView mReactRootView;
    private ReactInstanceManager mReactInstanceManager = null;
    private Preferences pref;
    private Realm realm;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View inflatedLayout = inflater.inflate(R.layout.fragment_rn_lost_drop_main, container, false);
        mReactRootView = inflatedLayout.findViewById(R.id.react_root_view_lost_drop_main);
        return inflatedLayout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        pref = Preferences.getInstance();
        pref.load(context);
        realm = Realm.getDefaultInstance();
        // mReactRootView = new ReactRootView(getContext());
        if (getActivity() != null) {
            mReactInstanceManager = ((SalesCRMApplication) getActivity().getApplication()).getReactNativeHost()
                    .getReactInstanceManager();
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ;
        Bundle initialProps = new Bundle();
        initialProps.putBoolean("fromAndroid", true);
        initialProps.putString("token", pref.getAccessToken());
        initialProps.putInt("appUserId", pref.getAppUserId());
        initialProps.putString("module", "DASHBOARD");
        initialProps.putBoolean("isUserBH", DbUtils.isUserBranchHead());
        initialProps.putBoolean("isUserOps", DbUtils.isUserOperations());
        initialProps.putBoolean("isUserSM", DbUtils.isUserSalesManager());
        initialProps.putBoolean("isUserEM", DbUtils.isUserEvaluatorManager());
        initialProps.putBoolean("isUserEV", DbUtils.isUserEvaluator());
        initialProps.putBoolean(("isUserTL"), DbUtils.isUserTeamLead());
        initialProps.putBoolean(("isUserSC"), DbUtils.isUserSalesConsultant());
        initialProps.putBoolean(("isUserVF"), DbUtils.isUserVarifier());
        initialProps.putString("allLocations", new Gson().toJson(getAllLocations()));
        initialProps.putString("enquirySourceCategories", new Gson().toJson(getEnquirySourceCategories()));
        initialProps.putString("eventCategories", new Gson().toJson(getAllCategories()));
        initialProps.putString("notification", new Gson().toJson(getNotificationInfo()));
        initialProps.putBoolean("uniqueAcrossDealerShip",
                DbUtils.isAppConfEnabled(WSConstants.AppConfig.UNIQUE_ACROSS_DEALERSHIP));
        initialProps.putBoolean("addLeadEnabled", DbUtils.isAddLeadEnabled());
        initialProps.putBoolean("coldVisitEnabled", DbUtils.isAppConfEnabled(WSConstants.AppConfig.COLD_VISIT_ENABLED));
        initialProps.putBoolean("showLeadCompetitor",
                DbUtils.isAppConfEnabled(WSConstants.AppConfig.SHOW_LEAD_COMPETITOR));
        initialProps.putBoolean("isClientILom", pref.isClientILom());
        initialProps.putBoolean("isClientILBank", pref.isClientILBank());
        initialProps.putBoolean("isClientTwoWheeler", pref.isTwoWheelerClient());
        // initialProps.putString("interestedVehicle", new
        // Gson().toJson(VehicleDBUtils.getInterestedVehicleModel(realm)));

        if (mReactInstanceManager != null && mReactRootView != null) {
            mReactRootView.startReactApplication(mReactInstanceManager, "NinjaCRMSales", initialProps);
        }
        getActivity().getIntent().putExtra(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY, false);

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }

    }

    private TeamDashboardSwiped teamDashboardSwiped = TeamDashboardSwiped.getInstance(this, 2);

    @Override
    public void onTeamDashboardSwiped(int position, int tabCount) {
        if (position == 2) {
            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE,
                    CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_LOST_DROP_ANALYSIS);
            CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private NotificationModel getNotificationInfo() {
        NotificationModel notificationModel = new NotificationModel();
        if (getActivity() != null && getActivity().getIntent().getExtras() != null
                && getActivity().getIntent().getExtras().getBoolean(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY)) {
            Bundle extras = getActivity().getIntent().getExtras();
            notificationModel.setName(extras.getString(WSConstants.NOTIFICATION_CLICKED_NAME, ""));
            notificationModel
                    .setPayload(notificationModel.new Payload(extras.getInt(WSConstants.NOTIFICATION_LOCATION_ID, 0)));
            getActivity().getIntent().putExtra(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY, false);
        }
        return notificationModel;
    }

    private Item[] getAllLocations() {
        RealmResults<UserLocationsDB> data = realm.where(UserLocationsDB.class).findAll();
        Item[] userLocations = new Item[data != null ? data.size() : 0];
        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i) != null) {
                    userLocations[i] = new Item(data.get(i).getLocation_id(), data.get(i).getName());
                }
            }
        }

        return userLocations;

    }

    private ItemCategory[] getAllCategories() {
        RealmResults<EventsCategoryDb> data = realm.where(EventsCategoryDb.class).findAll();
        ItemCategory[] eventCategory = new ItemCategory[data != null ? data.size() : 0];
        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i) != null) {
                    eventCategory[i] = new ItemCategory(data.get(i).getId(), data.get(i).getName());
                }
            }
        }

        return eventCategory;

    }

    private List<EnquiryCategory> getEnquirySourceCategories() {
        List<EnquiryCategory> enquirySourcesCategories = new ArrayList<>();
        RealmResults<EnqSourceCategoryDB> realmList = realm.where(EnqSourceCategoryDB.class).findAll();
        for (EnqSourceCategoryDB enqSourceCategoryDB : realmList) {
            List<Item> subEnquiries = new ArrayList<>();
            for (EnqSubInnerSourceDB subEnquiry : enqSourceCategoryDB.getSubLeadSources()) {
                subEnquiries.add(new Item(subEnquiry.getId() + "", subEnquiry.getName()));
            }
            enquirySourcesCategories
                    .add(new EnquiryCategory(enqSourceCategoryDB.getId(), enqSourceCategoryDB.getName(), subEnquiries));

        }
        return enquirySourcesCategories;
    }

    class Item {
        String id;
        String name;

        public Item(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    class ItemCategory {
        String id;
        String name;

        public ItemCategory(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    class EnquiryCategory {
        int id;
        String name;
        List<Item> subEnquirySource;

        public EnquiryCategory(int id, String name, List<Item> subEnquirySource) {
            this.id = id;
            this.name = name;
            this.subEnquirySource = subEnquirySource;
        }
    }
}