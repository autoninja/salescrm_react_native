package com.salescrm.telephony.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.salescrm.telephony.utils.WSConstants;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by bharath on 1/8/16.
 */
public class IncomingOtpSms extends BroadcastReceiver {
    private IncomingOtpSmsListener listener = null;
    public static IncomingOtpSms incomingOtpSms = new IncomingOtpSms();
    public static IncomingOtpSms getInstance(){
        return incomingOtpSms;
    }
    public void registerListener(IncomingOtpSmsListener listener){
        incomingOtpSms.listener = listener;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();

        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

            switch(status.getStatusCode()) {
                case CommonStatusCodes.SUCCESS:
                    // Get SMS message contents
                    String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
                    // Extract one-time code from the message and complete verification
                    // by sending the code back to your server.
                    if(incomingOtpSms.listener!=null){
                        incomingOtpSms.listener.onOtpSmsReceived(parseMessage(message));
                    }
                    break;
                case CommonStatusCodes.TIMEOUT:
                    // Waiting for SMS timed out (5 minutes)
                    // Handle the error ...
                    break;
            }
        }
        /*Deprecated*/
        /*     try {
            if (bundle != null) {
                SmsMessage smsMessage = null;
                if (Build.VERSION.SDK_INT >= 19) { //KITKAT
                    SmsMessage[] msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                    smsMessage = msgs[0];
                } else {
                    Object pdus[] = (Object[]) bundle.get("pdus");
                    if (pdus != null) {
                        smsMessage = SmsMessage.createFromPdu((byte[]) pdus[0]);
                    }
                }
                if (smsMessage != null) {
                    if (smsMessage.getDisplayOriginatingAddress().toUpperCase().contains(WSConstants.AUTONINJA_SMS)) {
                         //System.out.println("Hello:" + smsMessage.getDisplayMessageBody());
                        if(incomingOtpSms.listener!=null){
                            incomingOtpSms.listener.onOtpSmsReceived(parseMessage(smsMessage.getDisplayMessageBody()));
                        }

                    }

                }

            }

        } catch (Exception e) {

        }*/
    }

    private String parseMessage(String message) {
        System.out.println("message:"+message);
        String result = "";
        Pattern p = Pattern.compile("-?\\d+");
        Matcher matcher = p.matcher(message);
        while (matcher.find()) {
            if(matcher.group().length()==WSConstants.MAX_OTP_LENGTH){
                return matcher.group();
            }
        }
        return result;
    }

    public interface IncomingOtpSmsListener{
        void onOtpSmsReceived(String sms);
    }
}
