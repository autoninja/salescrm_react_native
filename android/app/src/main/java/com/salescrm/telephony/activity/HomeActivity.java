package com.salescrm.telephony.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.camera.CropImageIntentBuilder;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.google.gson.Gson;
import com.salescrm.telephony.BuildConfig;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.gamification.BadgesNotificationActivity;
import com.salescrm.telephony.activity.gamification.GamificationLuckyWheelActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.AllotDseDB;
import com.salescrm.telephony.db.DealershipsDB;
import com.salescrm.telephony.db.EvaluationManagerResult;
import com.salescrm.telephony.db.EvaluationManagerResultData;
import com.salescrm.telephony.db.FcmNotificationObject;
import com.salescrm.telephony.db.FormAnswerDB;
import com.salescrm.telephony.db.GameLocationDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.db.booking_allocation.BookingAllocationResult;
import com.salescrm.telephony.db.booking_allocation.VinAlllocationStatus;
import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;
import com.salescrm.telephony.db.events.EventsCategoryDb;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.ActionPlanFragment;
import com.salescrm.telephony.fragments.EnquiryListFragment;
import com.salescrm.telephony.fragments.MyPerformanceFragment;
import com.salescrm.telephony.fragments.RNColdVisitsDashboardFragment;
import com.salescrm.telephony.fragments.RNTeamDashboardFragment;
import com.salescrm.telephony.fragments.ScoreboardDseFragment;
import com.salescrm.telephony.fragments.SettingsFragment;
import com.salescrm.telephony.fragments.TeamDashboard;
import com.salescrm.telephony.fragments.TeamShuffleFragment;
import com.salescrm.telephony.fragments.gamification.LeaderBoardMainFragment;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.BadgeUpdater;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.interfaces.GameLocationPickerCallBack;
import com.salescrm.telephony.interfaces.TaskOtpResultListener;
import com.salescrm.telephony.interfaces.TeamSummaryToTaskListener;
import com.salescrm.telephony.interfaces.UserLocationPickerCallBack;
import com.salescrm.telephony.locationHelper.LocationHelper;
import com.salescrm.telephony.luckywheel.TestDialogInterface;
import com.salescrm.telephony.luckywheel.ToastDialog;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.model.BookingAllocationModel;
import com.salescrm.telephony.model.DseModelTask;
import com.salescrm.telephony.model.FilterDataChangedListener;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.model.HomeNavigationModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.DealershipsResponse;
import com.salescrm.telephony.response.FCMTokenRefreshResponse;
import com.salescrm.telephony.response.GeneralResponse;
import com.salescrm.telephony.response.ImageUploadResponse;
import com.salescrm.telephony.response.MobileAppDataResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.telephonyModule.db.TelephonyDbHandler;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.MediaStoreUtils;
import com.salescrm.telephony.utils.NinjaEncryption;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;
import com.salescrm.telephony.views.GameLocationPicker;
import com.salescrm.telephony.views.UserLocationPicker;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

import static java.security.AccessController.getContext;

public class HomeActivity extends AppCompatActivity implements AutoDialogClickListener, TeamSummaryToTaskListener, ScoreboardDseFragment.CallHomeActivity, DefaultHardwareBackBtnHandler, LeaderBoardMainFragment.LeaderBoardHomeCommunication {

    private static final int ACTION_PLAN_POSITION = 1;
    private static final int ENQUIRY_LIST_POSITION = 3;
    private static final int SELECT_PICTURE = 1888;
    private static final int CAMERA_REQUEST = 0, REQUEST_CROP_PICTURE = 1;
    static AlertDialog.Builder dialog;
    private static Context context = null;
    private static int REQUEST_PICTURE = 2;
    public final String APP_TAG = "MyCustomApp";
    public String photoFileName = "photo.jpg";
    String optionSelected = "";
    Uri selectedImage;
    private Preferences pref = null;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private RelativeLayout mDrawerMain;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private Realm realm;
    private ArrayList<HomeNavigationModel> navDrawerItems;
    private DrawerListAdapter adapter;
    private Bundle rootSavedInstanceState;
    private TextView tvFilterCount, tvNotificationCount;
    private int prevPos = -1;
    private FloatingActionButton fab;
    private RealmResults<FcmNotificationObject> notificationRead;
    private PackageInfo pInfo;
    private String version;
    private int verCode;
    private TextView tvNewIndicator;
    private AlertDialog alert;
    private Toolbar toolbar;
    private boolean otherDseFlag = false;
    // private MenuItem menuItemNotification;
    private MenuItem menuItemFilter, menuSearch, menuLocationName, menuBoosterDaysLeft;//menuItemNotification;
    private ImageView profileImage;
    private String imgbase64;
    private Bitmap photo;
    private int positionToHideFilter = 0;
    private RelativeLayout rlRankLayout;

    private HomeNavigationModel navProfile, navLeaderBoard, navTeamDashboard,
            navTargets, navMyTasks,
            navTeamShuffle, navTransferEnquiries, navSettings, navLogout, navColdVisits;
    private HomeNavigationModel currentNavigationModule = null;
    private CoordinatorLayout coordinatorLayout;
    private TextView tvBoosterRank;
    private TextView tvLocationName;
    //private ImageView imgSearch;
    private TextView tvBoosterDaysLeft;
    //private LinearLayout llBoosterDaysLeft;
    private BroadcastReceiver receiverForAddEvents, receiverForRNFunctions;
    private TextView tvAddEvent;
    private Intent intentAddevent = new Intent("show_addEventsButtonForBroadcastReceiver");


    private ReactInstanceManager mReactInstanceManager;
    private void disableScreenCapture() {
        if(pref.isScreenshotEnabled()) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootSavedInstanceState = savedInstanceState;
        pref = Preferences.getInstance();
        pref.load(HomeActivity.this);
        disableScreenCapture();
        setContentView(R.layout.activity_home);

        /**
         * Get the reference to the ReactInstanceManager
         */
        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();

        context = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        tvNewIndicator = (TextView) findViewById(R.id.tv_new_update_indicator);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        rlRankLayout = (RelativeLayout) findViewById(R.id.rl_booster_lyt);
        tvBoosterRank = (TextView) findViewById(R.id.booster_rank);
        realm = Realm.getDefaultInstance();
        tvAddEvent = (TextView) findViewById(R.id.tv_add_event);
        //imgSearch = (ImageView) toolbar.findViewById(R.id.img_search);
        //tvBoosterDaysLeft = (TextView) toolbar.findViewById(R.id.tv_booster_days);
        //llBoosterDaysLeft = (LinearLayout) toolbar.findViewById(R.id.ll_booster_days_left);
        setSupportActionBar(toolbar);
        tvAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, RNAddEventsActivity.class));
            }
        });


        pref.setIsLogedIn(true);


        init();

        pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        verCode = pInfo.versionCode;
        version = pInfo.versionName;

        if (realm.where(UserDetails.class).findFirst() != null) {
            pref.setAppUserId(realm.where(UserDetails.class).findFirst().getUserId() + "");
        }

        pref.setVersionCode(verCode);
        pref.setVersionName(version);
        pref.setLastVisitedTab(0);

        if (pref.isFabricsNeeded()) {
            Fabric.with(this, new Crashlytics());
            if(Util.isNotNull(pref.getImeiNumber())){
                Crashlytics.setUserIdentifier(pref.getImeiNumber());
            }
            else {
                Crashlytics.setUserIdentifier(pref.getDseName());
            }
            Crashlytics.setUserEmail(pref.getDealerName());
            Crashlytics.setUserName(pref.getDseName());
        }

        mTitle = mDrawerTitle = getTitle();
       /* if(!pref.isMyLeadViewed()){
           tvNewIndicator.setVisibility(View.VISIBLE);
        }*/

        setDrawer();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otherDseFlag) {
                    onBackPressed();
                } else {
                    mDrawerLayout.openDrawer(mDrawerMain);


                }

            }
        });

        receiverForAddEvents = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action != null && action.equals("show_addEventsButtonForBroadcastReceiver")) {
                    System.out.println("show_addEvent"+ intent.getExtras().getBoolean("showAddEvent"));
//                    if(intent.getExtras().getBoolean("showAddEvent") && DbUtils.isUserBranchHead()){
//                        tvAddEvent.setVisibility(View.VISIBLE);
//                        menuSearch.setVisible(false);
//                    }else {
//                        tvAddEvent.setVisibility(View.GONE);
//                        menuSearch.setVisible(true);
//                    }
                }
            }
        };

        receiverForRNFunctions = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action != null && action.equals("REACT_NATIVE_TO_HOME_ACTIVITY")) {
                    switch (intent.getIntExtra("FROM", 0)) {
                        case WSConstants.REACT_NATIVE_FUNCTIONS.OPEN_DRAWER:
                            openDrawer();
                            break;
                        case WSConstants.REACT_NATIVE_FUNCTIONS.OPEN_TASK_LIST:
                            int id = intent.getIntExtra("DSE_ID", 0);
                            String name = intent.getStringExtra("NAME");
                            String dpUrl = intent.getStringExtra("DP_URL");
                            if (id != 0) {
                                DseModelTask dseModelTask = new DseModelTask(id, 2,
                                        name, dpUrl);
                                HomeNavigationModel homeNavigationModel = new HomeNavigationModel(WSConstants.NavigationDrawerId.MY_TASKS,
                                        dseModelTask.getDseName() != null ? dseModelTask.getDseName() + "'s Tasks" : "Task", R.drawable.ic_action_plan_24dp, true, true, false);
                                displayView(homeNavigationModel, dseModelTask);
                            } else {
                                Util.showToast(context, "User information not available", Toast.LENGTH_SHORT);
                            }
                            break;

                    }

                }
            }
        };

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

        pref.setUsersTopRole(getUsersTopRole());

        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY)) {
            if (getIntent().getExtras().getString(WSConstants.NOTIFICATION_CLICKED_NAME) == null) {
                ButtonClicked("Notification Clicked");
            } else {
                ButtonClickedNotification(getIntent().getExtras().getString(WSConstants.NOTIFICATION_CLICKED_NAME));

            }
        }

        if (pref.isDpTakenFromDevice()) {
            sendDpToServer();
        } else {

        }


        // getBookingAllocationData();


        rlRankLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DbUtils.isGamificationEnabled()) {
                    SparseArray<Object> payLoad = new SparseArray<>();
                    payLoad.put(WSConstants.NavigationDrawerId.LEADER_BOARD, "FROM_TASK_BOTTOM_RANK");
                    navLeaderBoard.setPayLoad(payLoad);
                    displayView(navLeaderBoard, null);
                    navLeaderBoard.setPayLoad(null);
                }

            }
        });

        pushCleverTapTelephony();
    }

    private void pushCleverTapTelephony() {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_TYPE, CleverTapConstants.EVENT_TELEPHONY_TYPE_STATUS);
        hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_CDR_SYNC_PERCENTAGE, TelephonyDbHandler.getInstance().getCDRSyncPercentage(realm));
        hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_FILE_SYNC_PERCENTAGE, TelephonyDbHandler.getInstance().getFileSyncPercentage(realm));
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TELEPHONY, hashMap);

    }

    private void getBookingAllocationData() {
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).fetchBookingAllocationData(new Callback<BookingAllocationModel>() {
            @Override
            public void success(final BookingAllocationModel bookingAllocationModel, Response response) {
                System.out.println("getBookingAllocationData" + new Gson().toJson(bookingAllocationModel));
                if (getContext() != null) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.delete(BookingAllocationResult.class);
                            realm.delete(VinAlllocationStatus.class);
                            setToDb(realm, bookingAllocationModel);
                        }
                    });
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void setToDb(Realm realm, BookingAllocationModel bookingAllocationModel) {
        BookingAllocationResult bookingAllocationResult = new BookingAllocationResult();
        for (int i = 0; i < bookingAllocationModel.getResult().getVin_allocation_statuses().size(); i++) {
            VinAlllocationStatus vinAlllocationStatus = new VinAlllocationStatus();
            vinAlllocationStatus.setId(bookingAllocationModel.getResult().getVin_allocation_statuses().get(i).getId());
            vinAlllocationStatus.setName(bookingAllocationModel.getResult().getVin_allocation_statuses().get(i).getName());
            vinAlllocationStatus.getVinAllocationRoleIds().addAll(bookingAllocationModel.getResult().getVin_allocation_statuses().get(i).getRoleId());
            bookingAllocationResult.getVinAlllocationStatuses().add(vinAlllocationStatus);
        }
        realm.copyToRealm(bookingAllocationResult);
    }

    private void showPointsToaster(List<MobileAppDataResponse.Points> points) {
        new ToastDialog(this, points, new TestDialogInterface() {
            @Override
            public void endTestDialog(final Dialog dialog) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 3000);
            }
        }).show();
    }

    private void sendDpToServer() {

        if (!pref.getDpUri().equalsIgnoreCase("") && !pref.getEncodedUri().equalsIgnoreCase("")) {
            String encodedImgbase64 = pref.getEncodedUri();
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).imageUpload(encodedImgbase64, new Callback<ImageUploadResponse>() {
                @Override
                public void success(ImageUploadResponse imageUploadResponse, Response response) {

                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }

                    if (imageUploadResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)) {
                        ApiUtil.InvalidUserLogout(HomeActivity.this, 0);
                    }

                    pref.setDpTakenFromDevice(false);
                    UserDetails targetImage = realm.where(UserDetails.class).findFirst();
                    realm.beginTransaction();
                    targetImage.setImgProfileUrl(imageUploadResponse.getResult().getDp_url());
                    callDealershipsApi();
                    realm.commitTransaction();

                    updateImageUrl(imageUploadResponse.getResult().getDp_url());
                    addImage();
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("img", " error " + error.toString());
                }
            });
        }
    }

    private void callDealershipsApi() {
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getDealerships(new Callback<DealershipsResponse>() {
            @Override
            public void success(DealershipsResponse data, Response response) {
                Util.updateHeaders(response.getHeaders());

                if (!data.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    System.out.println("Success:0" + data.getMessage());
                    //showAlert(validateOtpResponse.getMessage());
                } else {
                    if (data.getResult() != null) {
                        System.out.println("Success:1" + data.getMessage());
                        realm.beginTransaction();
                        for (int i = 0; i < data.getResult().size(); i++) {
                            DealershipsResponse.Result userInfo = data.getResult().get(i);
                            DealershipsDB dealershipsDB = new DealershipsDB();
                            dealershipsDB.setDealer_id(userInfo.getDealer().getId());
                            dealershipsDB.setDealer_name(userInfo.getDealer().getName());
                            dealershipsDB.setDealer_db_name(userInfo.getDealer().getDb_name());

                            dealershipsDB.setUser_id(userInfo.getUsers().getId());
                            dealershipsDB.setUser_name(userInfo.getUsers().getName());
                            dealershipsDB.setUser_dp_url(userInfo.getUsers().getImage_url());
                            realm.copyToRealmOrUpdate(dealershipsDB);
                        }
                        realm.commitTransaction();

                    } else {
                        // relLoader.setVisibility(View.GONE);
                        System.out.println("Success:2" + data.getMessage());
                        //showAlert(validateOtpResponse.getMessage());
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void createNotification() {

        // showAppNotification(this,123,"Testinger",1212,"Followup Call",0,WSConstants.TaskActivityName.FOLLOWUP_CALL_ID);

        OrderedRealmCollection<SalesCRMRealmTable> realmResult = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", false)
                .greaterThanOrEqualTo("activityScheduleDate", Util.getTime(0))
                .lessThan("activityScheduleDate", Util.getTime(1))
                .equalTo("isDone", false)
                .equalTo("isLeadActive", 1)
                .equalTo("createdOffline", false)
                .findAll().sort("activityId", Sort.DESCENDING);

        for (int i = 0; i < realmResult.size(); i++) {
            if (realmResult.get(i).getActivityScheduleDate().getTime() > System.currentTimeMillis()) {
                SalesCRMRealmTable salesCRMRealmTable = realmResult.get(i);
                Calendar appNotificationTime = Calendar.getInstance();
                appNotificationTime.setTime(salesCRMRealmTable.getActivityScheduleDate());
                switch (salesCRMRealmTable.getActivityId()) {
                    case WSConstants.TaskActivityName.FOLLOWUP_CALL_ID:
                    case WSConstants.TaskActivityName.ARRANGE_CALL_FROM_DSE:
                        //Before five minutes!!
                        appNotificationTime.add(Calendar.MINUTE, -5);
                        break;
                    case WSConstants.TaskActivityName.HOME_VISIT_ID:
                        //Before 2Hr minutes!!
                    case WSConstants.TaskActivityName.SHOWROOM_VISIT_ID:
                        //Before 2Hr minutes!!
                    case WSConstants.TaskActivityName.EVALUATION_REQUEST_ID:
                        //Before 2Hr minutes!!
                    case WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID:
                        //Before 2Hr
                    case WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID:
                        //Before 2Hr
                        if(pref.isClientILom() || pref.isClientILBank()){
                            appNotificationTime.add(Calendar.MINUTE, -5);
                        }else {
                            appNotificationTime.add(Calendar.HOUR_OF_DAY, -2);
                        }
                        break;
                    case WSConstants.TaskActivityName.RECEIVE_PAYMENT_ID:
                        //Before 5min
                        appNotificationTime.add(Calendar.MINUTE, -5);
                        break;
                    case WSConstants.TaskActivityName.DELIVERY_REQUEST_ID:
                        //At 9AM
                        appNotificationTime.set(Calendar.HOUR_OF_DAY, 9);
                        appNotificationTime.set(Calendar.MINUTE, 0);
                        break;

                }
                if (appNotificationTime.getTime().getTime() > System.currentTimeMillis()) {
                    Util.setNotification(getApplicationContext(),
                            salesCRMRealmTable.getActivityName(),
                            salesCRMRealmTable.getFirstName() + " " + salesCRMRealmTable.getLastName(),
                            salesCRMRealmTable.getLeadId(),
                            salesCRMRealmTable.getScheduledActivityId(),
                            appNotificationTime.getTime().getTime(),
                            salesCRMRealmTable.getActivityScheduleDate().getTime(),
                            salesCRMRealmTable.getActivityId());

                    System.out.println("Setting alarm for " + salesCRMRealmTable.getScheduledActivityId() + " at:" +
                            appNotificationTime.getTime());
                }
            }
        }
    }

    public void init() {

        //Login for action plan data clean up:::
        System.out.println("Pref isSynced:" + pref.isActionPlanSynced());
        pref.setAppLastOpenedDate(Util.getTime(0).getTime());
        pref.setVersionCode(BuildConfig.VERSION_CODE);
        if (pref.getmGameLocationId() == -1 || !Util.isNotNull(pref.getGameLocationName())) {
            if (DbUtils.getActiveGameLocations().size() > 0) {
                pref.setmGameLocationId(DbUtils.getActiveGameLocations().get(0).getId());
                pref.setGameLocationName(DbUtils.getActiveGameLocations().get(0).getName());
            }
        }
    }

    @SuppressLint("NewApi")
    void setDrawer() {
        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.drawer_items);

        // nav drawer icons from resources
        navMenuIcons = getResources().obtainTypedArray(R.array.drawer_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.home_drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
        mDrawerMain = (RelativeLayout) findViewById(R.id.drawer_main_child_layout);
        navDrawerItems = new ArrayList<HomeNavigationModel>();

        LinkedHashSet<HomeNavigationModel> homeNavigationModelLinkedHashSet = new LinkedHashSet<>();

        //Profile
        navProfile = new HomeNavigationModel(WSConstants.NavigationDrawerId.PROFILE,
                "Profile",
                -1);

        //LeaderBoard
        navLeaderBoard = new HomeNavigationModel(WSConstants.NavigationDrawerId.LEADER_BOARD,
                "LeaderBoard", R.drawable.ic_leader_board, false, false, DbUtils.getActiveGameLocations().size() > 0);

        //TeamDashboard
        navTeamDashboard = new HomeNavigationModel(WSConstants.NavigationDrawerId.TEAM_DASHBOARD,
                "Dashboard", R.drawable.ic_scoreboard_24dp, false, false, false);

        //Targets
        navTargets = new HomeNavigationModel(WSConstants.NavigationDrawerId.TARGETS,
                "Targets", R.drawable.ic_target_icon, false, false, false);

        //MyTasks
        navMyTasks = new HomeNavigationModel(WSConstants.NavigationDrawerId.MY_TASKS,
                "My Tasks", R.drawable.ic_action_plan_24dp, true, DbUtils.getActiveGameLocations().size() <= 0, false, true, true);

        //My Enquiries
//        navMyEnquiries = new HomeNavigationModel(WSConstants.NavigationDrawerId.MY_ENQUIRIES,
//                "My Enquiries", R.drawable.ic_enquiry_list_24dp, true, true, false);

        //My Performance
//        navMyPerformance = new HomeNavigationModel(WSConstants.NavigationDrawerId.MY_PERFORMANCE,
//                "My Performance", R.drawable.ic_scoreboard_24dp, false, false, false);

        //TeamShuffle
        navTeamShuffle = new HomeNavigationModel(WSConstants.NavigationDrawerId.TEAM_SHUFFLE,
                "Team Shuffle", R.drawable.ic_team_shuffle, false, false, false);

        //TeamShuffle
        navTransferEnquiries = new HomeNavigationModel(WSConstants.NavigationDrawerId.TRANSFER_ENQUIRY,
                "Transfer Enquiry", R.drawable.ic_transfer_enquiry_24dp, false, false, false);


        //Settings
        navSettings = new HomeNavigationModel(WSConstants.NavigationDrawerId.SETTINGS,
                "Settings", R.drawable.ic_settings_white_24dp);

        //Cold Visits
        navColdVisits = new HomeNavigationModel(WSConstants.NavigationDrawerId.COLD_VISITS,
                "Cold Visits", R.drawable.ic_cold_visit);

        //Logout
        navLogout = new HomeNavigationModel(WSConstants.NavigationDrawerId.LOGOUT,
                "Logout", R.drawable.ic_logout);


        //Items for SlidingDrawer

        homeNavigationModelLinkedHashSet.add(navProfile);


        if (isUserBranchHeadOperations()) {
            if (DbUtils.isGamificationEnabled()) {
                homeNavigationModelLinkedHashSet.add(navLeaderBoard);
            }
            if (DbUtils.isBike()) {
                homeNavigationModelLinkedHashSet.add(navTeamDashboard);
                homeNavigationModelLinkedHashSet.add(navTargets);
                homeNavigationModelLinkedHashSet.add(navSettings);
                //homeNavigationModelLinkedHashSet.add(navMyEnquiries);
                //navDrawerItems.add(navTeamShuffle);
            } else {
                homeNavigationModelLinkedHashSet.add(navTeamDashboard);
                homeNavigationModelLinkedHashSet.add(navTeamShuffle);
                if(!pref.isClientILom() && !pref.isClientILBank()) {
                    homeNavigationModelLinkedHashSet.add(navTargets);
                    //homeNavigationModelLinkedHashSet.add(navMyEnquiries);
                    homeNavigationModelLinkedHashSet.add(navTransferEnquiries);
                    homeNavigationModelLinkedHashSet.add(navSettings);
                }

            }
        }
        if (isUserSalesManager()) {
            if (DbUtils.isGamificationEnabled()) {
                homeNavigationModelLinkedHashSet.add(navLeaderBoard);
            }
            if (DbUtils.isBike()) {
                homeNavigationModelLinkedHashSet.add(navTeamDashboard);
                //homeNavigationModelLinkedHashSet.add(navMyEnquiries);
            } else {
                homeNavigationModelLinkedHashSet.add(navTeamDashboard);
                if(!pref.isClientILom() && !pref.isClientILBank()) {
                    homeNavigationModelLinkedHashSet.add(navTransferEnquiries);

                }
                //homeNavigationModelLinkedHashSet.add(navMyEnquiries);
            }
        }

        if (isUserHasTeamLead()) {
            //Items for Gamification
            if (DbUtils.isGamificationEnabled()) {
                homeNavigationModelLinkedHashSet.add(navLeaderBoard);
            }
            homeNavigationModelLinkedHashSet.add(navTeamDashboard);
            homeNavigationModelLinkedHashSet.add(navMyTasks);
            //homeNavigationModelLinkedHashSet.add(navMyEnquiries);
        }
        if (isUserDse()) {
            //Items for Gamification
            if (DbUtils.isGamificationEnabled()) {
                homeNavigationModelLinkedHashSet.add(navLeaderBoard);
            }
            /*if(isUserOnlyDse()) {
                homeNavigationModelLinkedHashSet.add(navMyPerformance);
            }*/
            homeNavigationModelLinkedHashSet.add(navTeamDashboard);
            homeNavigationModelLinkedHashSet.add(navMyTasks);
            //homeNavigationModelLinkedHashSet.add(navMyEnquiries);
        }
        if (isUserBillingExecutive()) {
            homeNavigationModelLinkedHashSet.add(navMyTasks);
            //homeNavigationModelLinkedHashSet.add(navMyEnquiries);
        }
        if (isUserEvaluator()) {
            homeNavigationModelLinkedHashSet.add(navTeamDashboard);
            homeNavigationModelLinkedHashSet.add(navMyTasks);
            //homeNavigationModelLinkedHashSet.add(navMyEnquiries);
        }
        if (isUserVerifier()) {
            homeNavigationModelLinkedHashSet.add(navMyTasks);
        }

        if(DbUtils.isAppConfEnabled(WSConstants.AppConfig.COLD_VISIT_ENABLED)) {
            homeNavigationModelLinkedHashSet.add(navColdVisits);
        }
        homeNavigationModelLinkedHashSet.add(navLogout);
        navDrawerItems.addAll(new ArrayList(new LinkedHashSet(homeNavigationModelLinkedHashSet)));


        navMenuIcons.recycle();

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // setting the nav drawer list adapter
        adapter = new DrawerListAdapter(getApplicationContext(), navDrawerItems);
//        mDrawerList.addHeaderView(header, null, false);
        mDrawerList.setAdapter(adapter);

        // enabling action bar app icon and behaving it as toggle button

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.toolbartitle, R.string.toolbartitle) {
            public void onDrawerClosed(View view) {
                // getSupportActionBar().setTitle("Action plan");
                // calling onPrepareOptionsMenu() to show action bar icons
                // invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                // getSupportActionBar().setTitle("Action plan");
                // calling onPrepareOptionsMenu() to hide action bar icons
                //invalidateOptionsMenu();
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY)) {

            if (getIntent().getExtras().getString(WSConstants.NOTIFICATION_CLICKED_NAME, "")
                    .equalsIgnoreCase(WSConstants.FirebaseEvent.PENDING_ALERT)
                    || getIntent().getExtras().getString(WSConstants.NOTIFICATION_CLICKED_NAME, "")
                    .equalsIgnoreCase(WSConstants.FirebaseEvent.EOD_SUMMARY)) {
                displayView(navTeamDashboard, null);
            } else {
                displayView(navMyTasks, null);
            }
        }
        else {
            if (isUserDse() || isUserBranchHeadOperations() || isUserSalesManager() || isUserHasTeamLead()) {
                displayView(navTeamDashboard, null);
            } else if (isUserBillingExecutive()) {
                displayView(navMyTasks, null);
            } else if (isUserEvaluator()) {
                displayView(navTeamDashboard, null);
            }
            else if(isUserVerifier()) {
                displayView(navMyTasks, null);
            }
            else {
                displayView(navTeamDashboard, null);
            }
        }


        mDrawerList.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                mDrawerList.setSelection(0);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        realm.close();
    }


    @Override
    public void onStop() {
        super.onStop();
    }

    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerMain);
        //menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    private void displayView(HomeNavigationModel homeNavigationModel, DseModelTask dseModelTask) {
        System.out.println("HomeActivity : Display View ");
        Fragment fragment = null;
        boolean tempOpenTodayDash = otherDseFlag;
        otherDseFlag = false;
        if (currentNavigationModule != null
                && currentNavigationModule.getId() != WSConstants.NavigationDrawerId.PROFILE
                && currentNavigationModule.getId() != WSConstants.NavigationDrawerId.MY_ENQUIRIES
                && currentNavigationModule.getId() != WSConstants.NavigationDrawerId.LOGOUT) {
            pref.setLastVisitedTab(0);
        }

        if(homeNavigationModel.getId()!= WSConstants.NavigationDrawerId.TEAM_DASHBOARD){
            intentAddevent.putExtra("showAddEvent", false);
            this.sendBroadcast(intentAddevent);
        }
        switch (homeNavigationModel.getId()) {
            case WSConstants.NavigationDrawerId.PROFILE:
                return;
            case WSConstants.NavigationDrawerId.LEADER_BOARD:
                getSupportActionBar().show();
                fragment = new LeaderBoardMainFragment();
                if (homeNavigationModel.getPayLoad() != null) {
                    Object payLoad = homeNavigationModel.getPayLoad().get(WSConstants.NavigationDrawerId.LEADER_BOARD, null);
                    if (payLoad != null && payLoad instanceof String) {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean((String) payLoad, true);
                        fragment.setArguments(bundle);
                    }
                }

                break;
            case WSConstants.NavigationDrawerId.TEAM_DASHBOARD:
                fragment = new RNTeamDashboardFragment();
                Bundle bundleDash = new Bundle();
                bundleDash.putBoolean("is_team_lead", isUserOnlyTeamLead());
                bundleDash.putBoolean("is_branch_head", isUserBranchHeadOperations());
                bundleDash.putBoolean("is_sales_manager", isUserSalesManager());
                if (tempOpenTodayDash) {
                    bundleDash.putBoolean("team_dashboard_open_todays", true);
                    fragment.setArguments(bundleDash);
                } else {
                    fragment.setArguments(bundleDash);
                }
                getSupportActionBar().hide();
                break;
            case WSConstants.NavigationDrawerId.COLD_VISITS:
                fragment = new RNColdVisitsDashboardFragment();
                getSupportActionBar().hide();
                break;
            case WSConstants.NavigationDrawerId.TARGETS:
                getSupportActionBar().show();
                fragment = new SetTargetActivity();
                break;
            case WSConstants.NavigationDrawerId.MY_TASKS:
                getSupportActionBar().show();
                fragment = new ActionPlanFragment();
                if (dseModelTask != null) {
                    pref.setCurrentDseId(dseModelTask.getDseId() + "");
                    otherDseFlag = true;
                    Bundle bundle = new Bundle();
                    bundle.putInt(WSConstants.USER_ID, dseModelTask.getDseId());
                    bundle.putString(WSConstants.USER_ROLE_ID, dseModelTask.getDseRoleId() + "");
                    bundle.putString(WSConstants.USER_NAME, dseModelTask.getDseName());
                    bundle.putString(WSConstants.USER_PHOTO_URL, dseModelTask.getDsePhotoUrl());
                    fragment.setArguments(bundle);
                } else {
                    pref.setCurrentDseId(pref.getAppUserId() + "");
                }
                break;
            case WSConstants.NavigationDrawerId.MY_ENQUIRIES:
                getSupportActionBar().show();
                fragment = new EnquiryListFragment();
                break;
            case WSConstants.NavigationDrawerId.MY_PERFORMANCE:
                getSupportActionBar().show();
                fragment = new MyPerformanceFragment();
                break;
            case WSConstants.NavigationDrawerId.TEAM_SHUFFLE:
                getSupportActionBar().show();
                fragment = new TeamShuffleFragment();
                break;
            case WSConstants.NavigationDrawerId.TRANSFER_ENQUIRY:
                getSupportActionBar().show();
                new UserLocationPicker().show(HomeActivity.this, new UserLocationPickerCallBack() {
                    @Override
                    public void onPickLocation(UserLocationsDB locationsDB) {
                        if (locationsDB != null) {
                            showTransferEnqDialog(locationsDB);
                        }
                    }
                });
                break;
            case WSConstants.NavigationDrawerId.SETTINGS:
                getSupportActionBar().show();
                fragment = new SettingsFragment();
                break;
            case WSConstants.NavigationDrawerId.LOGOUT:
                mDrawerLayout.closeDrawer(mDrawerMain);
                logout("Are you sure want to logout");
                return;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.home_body_view, fragment).commitAllowingStateLoss();
            setTitle(homeNavigationModel.getTitle());
            mDrawerLayout.closeDrawer(mDrawerMain);
        }

        if (menuItemFilter != null) {
            menuItemFilter.setVisible(homeNavigationModel.isShowFilter());
        }
        if (menuSearch != null) {
            menuSearch.setVisible(homeNavigationModel.isShowSearch());
        }
        if (menuLocationName != null) {
            menuLocationName.setVisible(homeNavigationModel.isShowLocation());
        }

        if (menuBoosterDaysLeft != null) {
            menuBoosterDaysLeft.setVisible(DbUtils.isGamificationEnabled() && homeNavigationModel.isShowBoosterDaysLeft() && pref.isBoosterMode());
        }
        if (otherDseFlag) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
        } else {
            toolbar.setNavigationIcon(R.drawable.ic_menu_24dp);
        }
        rlRankLayout.setVisibility(DbUtils.isGamificationEnabled() && homeNavigationModel.isShowBottomBooster() && pref.isRankVisible() ? View.VISIBLE : View.GONE);
        currentNavigationModule = homeNavigationModel;


    }


    private boolean isUserOnlyTeamLead() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.TEAM_LEAD_ROLE_ID)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void ButtonClicked(String mButtonName) {
        if (pref.isFabricsNeeded()) {
            Answers.getInstance().logCustom(new CustomEvent(mButtonName)
                    //  .putCustomAttribute("Name", mButtonName)
                    .putCustomAttribute("Dealer Name", pref.getDealerName())
                    .putCustomAttribute("IMEI", pref.getImeiNumber())
                    .putCustomAttribute("User Name", pref.getUserName()));
        }
    }

    private void ButtonClickedNotification(String notificationType) {

        boolean isNotificationServer =
                getIntent() != null
                        && getIntent().getExtras() != null
                        && getIntent().getExtras().getBoolean(WSConstants.NOTIFICATION_TRIGGERED_FROM, false);
        String notificationTriggeredFrom;
        if (isNotificationServer) {
            notificationTriggeredFrom = "FCM";
        } else {
            notificationTriggeredFrom = "InApp";
        }

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_TYPE, CleverTapConstants.EVENT_NOTIFICATION_TYPE_CLICK);
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_FROM, notificationTriggeredFrom);
        hashMap.put(CleverTapConstants.EVENT_NOTIFICATION_KEY_EVENT, notificationType);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_NOTIFICATION, hashMap);

    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(Gravity.LEFT);
//        invalidateOptionsMenu();
    }
    public void openDrawer() {
        mDrawerLayout.openDrawer(mDrawerMain);
//        invalidateOptionsMenu();
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        CleverTapPush.pushProfile(pref);
        //menuItemNotification = menu.findItem(R.id.action_notification);
        // MenuItemCompat.setActionView(menuItemNotification, R.layout.notification_layout);

        menuSearch = menu.findItem(R.id.action_search);
        menuLocationName = menu.findItem(R.id.action_location_name);
        menuItemFilter = menu.findItem(R.id.action_filter);
        menuBoosterDaysLeft = menu.findItem(R.id.action_booster_days_left);

        realm = Realm.getDefaultInstance();
        System.out.println("OnCreateOptionMenu ActionPlan");

        if (currentNavigationModule != null) {
            rlRankLayout.setVisibility(DbUtils.isGamificationEnabled() && currentNavigationModule.isShowBottomBooster() && pref.isRankVisible() ? View.VISIBLE : View.GONE);
            menuSearch.setVisible(currentNavigationModule.isShowSearch());
            menuItemFilter.setVisible(currentNavigationModule.isShowFilter());
            menuLocationName.setVisible(currentNavigationModule.isShowLocation());
            MenuItemCompat.setActionView(menuLocationName, R.layout.location_leader_raction_layout);
            tvLocationName = menuLocationName.getActionView().findViewById(R.id.tv_leader_board_location);
            tvLocationName.setText(pref.getGameLocationName());

            menuLocationName.getActionView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    System.out.println("Clicked");
                    new GameLocationPicker().show(HomeActivity.this, new GameLocationPickerCallBack() {
                        @Override
                        public void onPickGameLocation(GameLocationDB gameLocationDB) {
                            if (gameLocationDB != null) {
                                pref.setmGameLocationId(gameLocationDB.getId());
                                pref.setGameLocationName(gameLocationDB.getName());
                                tvLocationName.setText(pref.getGameLocationName());
                                displayView(navLeaderBoard, null);
                            }
                        }
                    });

                }
            });

            MenuItemCompat.setActionView(menuBoosterDaysLeft, R.layout.menu_booster_days_left_layout);
            menuBoosterDaysLeft.setVisible(DbUtils.isGamificationEnabled() && currentNavigationModule.isShowBoosterDaysLeft() && pref.isBoosterMode());
            tvBoosterDaysLeft = menuBoosterDaysLeft.getActionView().findViewById(R.id.tv_booster_days);
        }
        MenuItemCompat.setActionView(menuItemFilter, R.layout.filter_action_layout);

        tvFilterCount = (TextView) menuItemFilter.getActionView().findViewById(R.id.tv_filter_count);

        //tvNotificationCount = (TextView) menuItemNotification.getActionView().findViewById(R.id.bt_notification_count);
        //  tvNotificationCount.setVisibility(View.GONE);
        if (FilterSelectionHolder.getInstance().getCount() == 0) {
            tvFilterCount.setVisibility(View.GONE);
        } else {
            tvFilterCount.setVisibility(View.VISIBLE);

        }
        notificationRead = realm.where(FcmNotificationObject.class).equalTo("notification_read", false).findAll();
        if (notificationRead.size() == 0) {
            //tvNotificationCount.setVisibility(View.GONE);
        } else {
            //tvNotificationCount.setVisibility(View.VISIBLE);

        }
        tvFilterCount.setText(String.format(Locale.getDefault(), "%d", FilterSelectionHolder.getInstance().getCount()));
        //  tvNotificationCount.setText(String.format(Locale.getDefault(), "%d", notificationRead.size()));
        //   View view = MenuItemCompat.getActionView(item);
        menuItemFilter.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SalesCRMApplication.sendEventToCleverTap("Mob Button Filter");
                Intent filterActivity = new Intent(HomeActivity.this, FiltersActivity.class);

                if (FilterSelectionHolder.getInstance().getCount() > 0) {
                    filterActivity.putExtra(WSConstants.IS_FILTER_ALREADY_APPLIED, true);
                } else {
                    filterActivity.putExtra(WSConstants.IS_FILTER_ALREADY_APPLIED, false);
                }
                startActivity(filterActivity);
            }
        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_search) {
            startActivity(new Intent(this, RNSearchActivity.class));
        }

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(mDrawerMain)) {
            closeDrawer();
        } else if (otherDseFlag) {
            displayView(navTeamDashboard, null);
            toolbar.setNavigationIcon(R.drawable.ic_menu_24dp);
        } else if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    public AutoDialogModel getDialogData(String title) {
        RealmResults<CreateLeadInputDataDB> createLeadInputDataDBList = realm.where(CreateLeadInputDataDB.class).equalTo("is_synced", false).findAll();
        RealmResults<FormAnswerDB> formAnswerList = realm.where(FormAnswerDB.class).equalTo("is_synced", false).findAll();
        RealmResults<AllotDseDB> allotDseList = realm.where(AllotDseDB.class).equalTo("is_synced", false).findAll();

        /*AutoDialogModel model = new AutoDialogModel();
        if (createLeadInputDataDBList.size() == 0 && formAnswerList.size() == 0 && allotDseList.size() == 0) {
            model.setDialogTitle("Are you sure want to logout");
        } else {
            StringBuilder st = new StringBuilder();
            if (createLeadInputDataDBList.size() != 0) {
                st.append("Lead:" + createLeadInputDataDBList.size() + "\n");*/
        AutoDialogModel model = new AutoDialogModel();
        if (createLeadInputDataDBList.size() == 0 && formAnswerList.size() == 0 && allotDseList.size() == 0) {
            model.setDialogTitle(title);
        } else {
            StringBuilder st = new StringBuilder();
            if (createLeadInputDataDBList.size() != 0) {
                st.append("Lead:" + createLeadInputDataDBList.size() + "\n");
            }
            if (formAnswerList.size() != 0) {
                st.append("Forms:" + formAnswerList.size() + "\n");
            }
            if (allotDseList.size() != 0) {
                st.append("DSE Allot:" + allotDseList.size() + "\n");
            }
            model.setDialogTitle(title + "\nSync Pending" + "\n" + st.toString().trim());
        }

        model.setYesTitle("Logout");
        model.setNoTitle("Close");
        return model;
    }

    @Override
    public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).logout(new Callback<Object>() {
            @Override
            public void success(Object generalResponse, Response response) {
                try{
                    progressDialog.dismiss();
                }
                catch (Exception ignore) {}
                clearDataOnLogout();
            }

            @Override
            public void failure(RetrofitError error) {
                try{
                    progressDialog.dismiss();
                }
                catch (Exception ignore) {}
            }
        });



    }

    void clearDataOnLogout() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                try {
                    realm.deleteAll();
                } catch (Exception ignored) {

                }
            }

        });
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_LOGOUT);
        String fcmId = pref.getFcmId();
        pref.setIsLogedIn(false);
        pref.deleteAll(HomeActivity.this);
        pref.setFcmId(fcmId);
        startActivity(new Intent(HomeActivity.this, SplashActivity.class));
        finish();
    }

    @Override
    public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {

    }

    @Override
    public void onDseTaskSummaryClicked(DseModelTask dseModelTask, boolean isUserTlOnly) {
        HomeNavigationModel homeNavigationModel = new HomeNavigationModel(WSConstants.NavigationDrawerId.MY_TASKS,
                dseModelTask != null ? dseModelTask.getDseName() + "'s Tasks" : "Task", R.drawable.ic_action_plan_24dp, true, true, false);
        displayView(homeNavigationModel, dseModelTask);
    }


    public boolean isUserHasTeamLead() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.TEAM_LEAD_ROLE_ID)) {
                    return true;
                }
            }
        }
        return false;
    }


    public boolean getUsersTopRole() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_BRANCH_HEAD)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_OPERATIONS)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.MANAGER_ROLE_ID)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.TEAM_LEAD_ROLE_ID)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.DSE_ROLE_ID)) {
                    return true;
                }
            }

        }
        return false;
    }

    public boolean isUserDse() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.DSE_ROLE_ID)) {
                    return true;
                }
            }

        }
        return false;
    }

    public boolean isUserOnlyDse() {
        boolean isUserOnlyDse = false;
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            if (userRoles != null && userRoles.size() == 1) {
                isUserOnlyDse = (Util.getInt(userRoles.get(0).getId()) == WSConstants.USER_ROLES.DSE);
            }
        }
        return isUserOnlyDse;
    }


    public boolean isUserOnlyEvaluator() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean userHasOnlyOneRole() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            if (userRoles.size() == 1) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public boolean isUserBranchHeadOperations() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_BRANCH_HEAD)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_OPERATIONS)) {
                    return true;
                }
            }
        }
        return false;
    }
    public boolean isUserSalesManager() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.MANAGER_ROLE_ID)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isUserBillingExecutive() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.BILLING_EXECUTIVE)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isUserEvaluator() {
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            for (int i = 0; i < userRoles.size(); i++) {
                if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR)
                        || userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_EVALUATOR_MANAGER)) {
                    return true;
                }
            }
        }
        return false;
    }
    public boolean isUserVerifier() {
            UserDetails data = realm.where(UserDetails.class).findFirst();
            if (data != null) {
                RealmList<UserRolesDB> userRoles = data.getUserRoles();
                for (int i = 0; i < userRoles.size(); i++) {
                    if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_VERIFIER)) {
                        return true;
                    }
                }
            }
            return false;
        }


    @Override
    public void callMyTask(int value) {
        displayView(navMyTasks, null);
    }


    private void addImage() {

        UserDetails targetImage = realm.where(UserDetails.class).findFirst();
        try {
            String hex = targetImage.getImgProfileUrl();
            if (hex != null && !hex.isEmpty()) {
                String subStrin = hex.substring(hex.length() - 4, hex.length());
                if (!subStrin.equals("null")) {
                    //Loading Image from URL
                    Picasso.with(context)
                            .load(targetImage.getImgProfileUrl())
                            // .placeholder(R.mipmap.ic_user)   // optional
                            .error(R.mipmap.dummy_dude)      // optional
                            .resize(100, 100)                        // optional
                            .into(profileImage);
                } else {
                    Picasso.with(context)
                            .load(R.mipmap.dummy_dude)
                            .placeholder(R.mipmap.dummy_dude)   // optional
                            .error(R.mipmap.dummy_dude)        // optional
                            .resize(100, 100)                        // optional
                            .into(profileImage);
                }
            }
        } catch (NullPointerException nex) {
            nex.printStackTrace();
        }
    }

    private void logout(String title) {
        new AutoDialog(this, this, getDialogData(title)).show();

    }

    @Subscribe
    public void onFilterDataChanged(FilterDataChangedListener filterDataChangedListener) {
        if (filterDataChangedListener.isUpdateRequired()) {
            updateFilterUi();
        }
    }

    private void updateFilterUi() {
        if (tvFilterCount != null) {
            if (FilterSelectionHolder.getInstance().getCount() == 0) {
                tvFilterCount.setVisibility(View.GONE);
            } else {
                tvFilterCount.setVisibility(View.VISIBLE);

            }
            tvFilterCount.setText(String.format(Locale.getDefault(), "%d", FilterSelectionHolder.getInstance().getCount()));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }

        try {
            unregisterReceiver(networkChangeReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            unregisterReceiver(receiverForAddEvents);
        } catch (Exception e) {

        } try {
            unregisterReceiver(receiverForRNFunctions);
        } catch (Exception e) {

        }



        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }

        try {
            registerReceiver(receiverForAddEvents, new IntentFilter("show_addEventsButtonForBroadcastReceiver"));

        } catch (Exception ignored) {

        }
        try{
            registerReceiver(receiverForRNFunctions, new IntentFilter("REACT_NATIVE_TO_HOME_ACTIVITY"));
        }
        catch (Exception ignored) {

        }
        try{

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(networkChangeReceiver, intentFilter);
        }
        catch (Exception ignored) {

        }
        SalesCRMApplication.getBus().register(this);
        if (Util.getDifferenceBetweenDatesAll(Util.getTime(0), new Date(pref.getAppLastOpenedDate())) > 0) {
            Intent intent = new Intent(this, OfflineSupportActivity.class);
            intent.putExtra(WSConstants.IS_USER_DATA_FETCH_REQUIRED, true);
            startActivity(intent);
            finish();
            return;

        }
        BadgeUpdater.updateBadge(this);
        checkForForceUpdate();
        createNotification();
        updateFilterUi();
        if (tvNotificationCount != null) {
            notificationRead = realm.where(FcmNotificationObject.class).equalTo("notification_read", false).findAll();

            if (notificationRead.size() == 0) {
                // tvNotificationCount.setVisibility(View.GONE);
            } else {
                //tvNotificationCount.setVisibility(View.VISIBLE);

            }
            // tvNotificationCount.setText(String.format(Locale.getDefault(), "%d", notificationRead.size()));

        }

        if (pref.getFcmSync() == 1) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).refreshFcmToken(pref.getFcmId(), new Callback<FCMTokenRefreshResponse>() {
                @Override
                public void success(FCMTokenRefreshResponse fcmTokenRefreshResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }

                    if (fcmTokenRefreshResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)) {
                        ApiUtil.InvalidUserLogout(HomeActivity.this, 0);
                    }

                    if (fcmTokenRefreshResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        pref.setFcmSync(0);
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }


//        Util.showAppNotification(getApplicationContext(), 0,
//                "Dummy Notification", 123, "Dummy Notification", Calendar.getInstance().getTimeInMillis(), 123, true,
//                WSConstants.FirebaseEvent.EOD_SUMMARY,
//                0);

    }

    BroadcastReceiver networkChangeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            pref.load(getApplicationContext());
            if (intent.getAction().equalsIgnoreCase("android.net.conn.CONNECTIVITY_CHANGE")) {
                if (new ConnectionDetectorService(context).isConnectingToInternet()) {
                    sendMessage(true);
                } else {
                    sendMessage(false);
                }

            }

        }
    };

    private void sendMessage(boolean val) {
        Intent intent = new Intent(WSConstants.NINJA_BROADCAST_MESSAGE);
        intent.putExtra("isConnected", val);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void checkForForceUpdate() {
        //handleUpdateOptions();
        //showPointsToaster(null);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getDealerCodes(pref.getVersionName(), "0", new Callback<MobileAppDataResponse>() {
            @Override
            public void success(MobileAppDataResponse mobileAppDataResponse, Response response) {

                System.out.println("HomeForceUpdateSuccess - " + mobileAppDataResponse.getStatusCode());

                if (mobileAppDataResponse.getResult() != null) {
                    if (mobileAppDataResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)) {
                        ApiUtil.InvalidUserLogout(HomeActivity.this, 0);
                    }
                }


                if (mobileAppDataResponse.getResult() != null
                        && mobileAppDataResponse.getResult().getForce_update() != null
                        && mobileAppDataResponse.getResult().getForce_update() == 1) {

                    StringBuilder releaseNotes = new StringBuilder();
                    for (int i = 0; i < (mobileAppDataResponse.getResult().getRelease_notes() == null
                            ? 0 : mobileAppDataResponse.getResult().getRelease_notes().size()); i++) {
                        releaseNotes.append(i + 1);
                        releaseNotes.append(". ");
                        releaseNotes.append(mobileAppDataResponse.getResult().getRelease_notes().get(i));
                        releaseNotes.append("\n\n");
                    }
                    pref.setNewUpdateNotice(releaseNotes.toString());
                    handleUpdateOptions();
                } else if (mobileAppDataResponse.getResult() != null) {
                    setGamificationData(mobileAppDataResponse.getResult().getGame_data());
                }

                if (mobileAppDataResponse.getResult() != null
                        && mobileAppDataResponse.getResult().getAcr_license_details() != null) {
                    if (Util.isNotNull(mobileAppDataResponse.getResult().getAcr_license_details().getLicense())) {
                        pref.setAcrLicenseKey(mobileAppDataResponse.getResult().getAcr_license_details().getLicense());
                    }

                    if (Util.isNotNull(mobileAppDataResponse.getResult().getAcr_license_details().getSerial())) {
                        pref.setAcrSerial(mobileAppDataResponse.getResult().getAcr_license_details().getSerial());
                    }

                }
                if (mobileAppDataResponse.getResult() != null && mobileAppDataResponse.getResult().getHere_maps_credentials() != null) {
                    MobileAppDataResponse.HereMapsCredentials here_maps_credentials = mobileAppDataResponse.getResult().getHere_maps_credentials();
                    if (Util.isNotNull(here_maps_credentials.getApp_id())) {
                        pref.setHereMapAppId(here_maps_credentials.getApp_id());
                    }
                    if (Util.isNotNull(here_maps_credentials.getApp_code())) {
                        pref.setHereMapAppCode(here_maps_credentials.getApp_code());
                    }
                }

                pref.setDeviceSupportedForNLL(false);
                if (mobileAppDataResponse.getResult() != null && mobileAppDataResponse.getResult().getAcr_models() != null) {
                    for (int i = 0; i < mobileAppDataResponse.getResult().getAcr_models().length; i++) {
                        if (Build.MODEL != null && Build.MODEL.equalsIgnoreCase(mobileAppDataResponse.getResult().getAcr_models()[i])) {
                            pref.setDeviceSupportedForNLL(true);
                            break;
                        }
                    }
                }
                System.out.println("setDeviceSupportedForNLL:" + pref.isIsDeviceSupportedForNLL());

            }

            @Override
            public void failure(RetrofitError error) {

                if (error.getResponse() != null) {
                    System.out.println("HomeForceUpdateError - " + error.getResponse().getStatus());

                    if (error.getResponse().getStatus() == 401) {
                        ApiUtil.InvalidUserLogout(HomeActivity.this, 0);
                    }
                }
            }
        });

    }

    private void setGamificationData(MobileAppDataResponse.GameData game_data) {
        if (game_data != null && pref.getmGameLocationId() != -1) {
            pref.setBoosterMode(game_data.isBooster());
            setBoosterMode();
            setBoosterDaysLeft(game_data);
            if (game_data.getRank() != null && !game_data.getRank().equalsIgnoreCase("0")) {
                pref.setRankVisible(true);
                String stringRank = "";
                if (game_data.getRank().equalsIgnoreCase("1")) {
                    stringRank = "<b><big>" + game_data.getRank() + "</big></b><small><sup>st</sup></small>" + "<small> Rank</small>";
                } else if (game_data.getRank().equalsIgnoreCase("2")) {
                    stringRank = "<b><big>" + game_data.getRank() + "</big></b><small><sup>nd</sup></small>" + "<small> Rank</small>";
                } else if (game_data.getRank().equalsIgnoreCase("3")) {
                    stringRank = "<b><big>" + game_data.getRank() + "</big></b><small><sup>rd</sup></small>" + "<small> Rank</small>";
                } else {
                    stringRank = "<b><big>" + game_data.getRank() + "</big></b><small><sup>th</sup></small>" + "<small> Rank</small>";
                }
                tvBoosterRank.setText(Util.fromHtml(stringRank));
            } else {
                pref.setRankVisible(false);
            }
            setUsersRankVisiblity();
            if (game_data.getSpin_the_wheels() != null) {
                setSpinnerWheelData(game_data.getSpin_the_wheels());
            } else if (game_data.getPoints() != null && !game_data.getPoints().isEmpty()) {
                showPointsToaster(game_data.getPoints());
            } else if (game_data.getBadge_details() != null && !game_data.getBadge_details().isEmpty()) {
                String jsonString = new Gson().toJson(game_data).toString();
                callBadgesActivity(jsonString);
            }

        } else {
            pref.setBoosterMode(false);
            pref.setRankVisible(false);
            setBoosterMode();
            setBoosterDaysLeft(game_data);
            setUsersRankVisiblity();
        }
    }

    private void setBoosterDaysLeft(MobileAppDataResponse.GameData game_data) {
        if (menuBoosterDaysLeft != null && currentNavigationModule != null) {
            menuBoosterDaysLeft.setVisible(DbUtils.isGamificationEnabled() && currentNavigationModule.isShowBoosterDaysLeft() && pref.isBoosterMode());
            if (game_data != null) {
                if (game_data.getDays_left() != null && tvBoosterDaysLeft != null)
                    tvBoosterDaysLeft.setText("" + game_data.getDays_left());
            }
        }
    }

    private void setUsersRankVisiblity() {
        if (currentNavigationModule != null) {
            rlRankLayout.setVisibility(DbUtils.isGamificationEnabled() && currentNavigationModule.isShowBottomBooster() && pref.isRankVisible() ? View.VISIBLE : View.GONE);
        }
    }

    public void setBoosterMode() {
        if (pref.isBoosterMode()) {
            coordinatorLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.booster_mode_on));
        } else {
            coordinatorLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.gradient_primary));
        }
    }

    private void callBadgesActivity(String jsonString) {
        Intent intent = new Intent(getApplicationContext(), BadgesNotificationActivity.class);
        intent.putExtra("badges_notification", jsonString);
        startActivity(intent);
    }

    private void setSpinnerWheelData(MobileAppDataResponse.LuckyWheelContent spin_the_wheels) {
        if (spin_the_wheels != null) {
            String gson = new Gson().toJson(spin_the_wheels);
            Intent spinIntent = new Intent(this, GamificationLuckyWheelActivity.class);
            spinIntent.putExtra("spinValues", gson);
            startActivity(spinIntent);
        }
    }

    /**
     * Method to give options to the user to update the app properly
     */
    private void handleUpdateOptions() {

        if (new ConnectionDetectorService(getApplicationContext()).isConnectingToInternet()) {
            System.out.println("SAVING DATA");
            pref.setRefreshOfflineData(true);
            Intent i = new Intent(HomeActivity.this, SplashActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else {
            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();
        }
        startActivity(new Intent(HomeActivity.this, ForceUpdateActivity.class));
        this.finish();
    }

    private void versionDialog() {

        if (context != null) {
            new AutoDialog(this, new AutoDialogClickListener() {
                @Override
                public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.salescrm.telephony")));
                    } catch (ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.salescrm.telephony")));
                    }
                }

                @Override
                public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {

                }
            }, new AutoDialogModel("Update", "New Update is Available, update app.", "Update", "Cancel")).show();

        }
    }

    /**
     * called when phone is low on memory
     */
    private void showLowMemoryAlert() {
        dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Memory Alert").setMessage("Your device is running low in storage");
        dialog.setPositiveButton("ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        alert.dismiss();
                    }
                });
        alert = dialog.create();
        alert.show();

    }

    public void openCameraDialog() {
        if (HomeActivity.this != null) {
            android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(HomeActivity.this);
            alertDialog.setTitle("Choose an Option.");
            alertDialog.setCancelable(false);

            alertDialog.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    optionSelected = "Gallery";
                    startActivityForResult(MediaStoreUtils.getPickImageIntent(HomeActivity.this), REQUEST_PICTURE);

                }
            });

            alertDialog.setNegativeButton("Camera", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    optionSelected = "Camera";
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, getPhotoFileUri(photoFileName)); // set the image file name

                    // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
                    // So as long as the result is not null, it's safe to use the intent.
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        // Start the image capture intent to take photo
                        startActivityForResult(intent, CAMERA_REQUEST);
                    }


                }
            });

            alertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            if (!HomeActivity.this.isFinishing()) {
                alertDialog.show();
                //show dialog
            } else {
                Toast.makeText(HomeActivity.this, "Please try again", Toast.LENGTH_LONG).show();
            }

        }
    }

    // Returns the Uri for a photo stored on disk given the fileName
    public Uri getPhotoFileUri(String fileName) {
        // Only continue if the SD Card is mounted
        if (isExternalStorageAvailable()) {
            // Get safe storage directory for photos
            // Use `getExternalFilesDir` on Context to access package-specific directories.
            // This way, we don't need to request external read/write runtime permissions.
            File mediaStorageDir = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), APP_TAG);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
                Log.d(APP_TAG, "failed to create directory");
            }

            // Return the file target for the photo based on filename
            return FileProvider.getUriForFile(this, this.getPackageName() + ".provider", new File(mediaStorageDir.getPath() + File.separator + fileName));
        }
        return null;
    }

    // Returns true if external storage for photos is available
    private boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        File croppedImageFile = new File(getFilesDir(), "test.jpg");
        if ((requestCode == REQUEST_PICTURE) && (resultCode == RESULT_OK)) {
            // When the user is done picking a picture, let's start the CropImage Activity,
            // setting the output image file and size to 200x200 pixels square.
            Uri croppedImage = Uri.fromFile(croppedImageFile);
            CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
            cropImage.setOutlineColor(0xFF03A9F4);
            cropImage.setSourceImage(data.getData());

            startActivityForResult(cropImage.getIntent(this), REQUEST_CROP_PICTURE);
        } else if ((requestCode == REQUEST_CROP_PICTURE) && (resultCode == RESULT_OK)) {

            photo = BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath());

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 40, stream);

            profileImage.setImageBitmap(photo);
            byte[] byteArray1 = stream.toByteArray();
            imgbase64 = Base64.encodeToString(byteArray1, Base64.DEFAULT);
            Log.d("img", "id-" + pref.getAppUserId() + " base-" + imgbase64);
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).imageUpload(imgbase64, new Callback<ImageUploadResponse>() {
                @Override
                public void success(ImageUploadResponse imageUploadResponse, Response response) {

                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }

                    if (imageUploadResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)) {
                        ApiUtil.InvalidUserLogout(HomeActivity.this, 0);
                    }

                    Log.d("img", " camera " + imageUploadResponse.toString());

                    UserDetails targetImage = realm.where(UserDetails.class).findFirst();
                    realm.beginTransaction();
                    targetImage.setImgProfileUrl(imageUploadResponse.getResult().getDp_url());
                    callDealershipsApi();
                    realm.commitTransaction();

                    updateImageUrl(imageUploadResponse.getResult().getDp_url());
                    addImage();


                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("img", " error " + error.toString());
                }
            });
            // When we are done cropping, display it in the ImageView.
//			imageView.setImageBitmap(BitmapFactory.decodeFile(croppedImageFile.getAbsolutePath()));
        } else if ((requestCode == CAMERA_REQUEST) && (resultCode == RESULT_OK)) {

            Uri takenPhotoUri = getPhotoFileUri(photoFileName);
            Uri croppedImage = Uri.fromFile(croppedImageFile);

            CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, croppedImage);
            cropImage.setOutlineColor(0xFF03A9F4);
            cropImage.setSourceImage(takenPhotoUri);

            startActivityForResult(cropImage.getIntent(this), REQUEST_CROP_PICTURE);
        }
    }

    public void updateImageUrl(String imageUrl) {
        UserDetails toEdit = realm.where(UserDetails.class).findFirst();
        realm.beginTransaction();
        toEdit.setImgProfileUrl(imageUrl);
        realm.commitTransaction();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.getExtras() != null && intent.getExtras().getBoolean(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY)) {

            if (intent.getExtras().getString(WSConstants.NOTIFICATION_CLICKED_NAME, "")
                    .equalsIgnoreCase(WSConstants.FirebaseEvent.PENDING_ALERT)
                    || intent.getExtras().getString(WSConstants.NOTIFICATION_CLICKED_NAME, "")
                    .equalsIgnoreCase(WSConstants.FirebaseEvent.EOD_SUMMARY)) {
                Intent intentNewAct = new Intent(HomeActivity.this, NotificationRedirectActivity.class);
                intentNewAct.putExtras(intent.getExtras());
                HomeActivity.this.finish();
                startActivity(intentNewAct);
                return;
            }
            if (intent.getExtras().getString(WSConstants.NOTIFICATION_CLICKED_NAME) == null) {
                ButtonClicked("Notification Clicked");
            } else {
                ButtonClickedNotification(intent.getExtras().getString(WSConstants.NOTIFICATION_CLICKED_NAME));

            }
            displayView(navMyTasks, null);
        }

        setIntent(intent);
        super.onNewIntent(intent);

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void onLeaderBoardTabChange(int pos) {
        if (pos == 0) {
            if(menuLocationName!=null) {
                menuLocationName.getActionView().setEnabled(false);
            }
            if(tvLocationName!=null) {
                tvLocationName.setText("All");
            }

        } else {
            if(menuLocationName!=null) {
                menuLocationName.getActionView().setEnabled(true);
            }
            if(tvLocationName!=null) {
                tvLocationName.setText(pref.getGameLocationName());
            }
        }
    }

    private class SlideMenuClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            displayView(navDrawerItems.get(position), null);
        }
    }

    class DrawerListAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<HomeNavigationModel> navDrawerItems;

        public DrawerListAdapter(Context context, ArrayList<HomeNavigationModel> navDrawerItems) {
            this.context = context;
            this.navDrawerItems = navDrawerItems;
        }

        @Override
        public int getCount() {
            return navDrawerItems.size();
        }

        @Override
        public Object getItem(int position) {
            return navDrawerItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (position == 0) {
                if (convertView == null) {
                    LayoutInflater mInflater = (LayoutInflater) getApplicationContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    convertView = mInflater.inflate(R.layout.drawer_list_profile_info, parent, false);
                    TextView tvDseName = (TextView) convertView.findViewById(R.id.tvDseName);
                    TextView tvDseRole = (TextView) convertView.findViewById(R.id.tvDseRole);
                    TextView tvAppVersion = (TextView) convertView.findViewById(R.id.tvAppVersion);
                    profileImage = (ImageView) convertView.findViewById(R.id.img_profileImage);

                    if (Util.isNotNull(DbUtils.getRolesCombination())) {
                        tvDseRole.setText(DbUtils.getRolesCombination().replaceAll(",", ", ").replace("[", "").replace("]", ""));
                        tvDseRole.append(", " + Util.getProperName(pref.getDealerName()));
                        tvDseRole.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startActivity(new Intent(HomeActivity.this, SwitchDealershipsActivity.class));
                            }
                        });
                    }

                    tvDseName.setText(pref.getDseName());

                    tvAppVersion.setText("App Version - " + version);

                    addImage();

                    profileImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openCameraDialog();
                        }
                    });

                }

            } else {
                if (convertView == null) {
                    LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    convertView = mInflater.inflate(R.layout.drawer_list_item, parent, false);
                }

                ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
                TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
                RelativeLayout relDrawer = convertView.findViewById(R.id.rel_main_drawer_list);


                if (navDrawerItems.get(position).getId() == WSConstants.NavigationDrawerId.LEADER_BOARD) {
                    if (imgIcon != null) {
                        imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        relDrawer.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.gradient_leader_board));
                    } else {
                        relDrawer.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.gradient_leader_board));
                    }
                } else {
                    if (imgIcon != null) {
                        imgIcon.setImageDrawable(VectorDrawableCompat.create(getResources(), navDrawerItems.get(position).getIcon(), getTheme()));
                    }
                    relDrawer.setBackgroundColor(Color.TRANSPARENT);
                }
                txtTitle.setText(navDrawerItems.get(position).getTitle());

            }
            return convertView;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void showTransferEnqDialog(final UserLocationsDB locationsDB) {
        final Dialog dialog = new Dialog(HomeActivity.this, R.style.AppTheme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
            //  dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }

        dialog.setContentView(R.layout.layout_tasks_picker);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tv_pnt_title);
        tvTitle.setText("Transfer enquiries by");
        GridView gridView = (GridView) dialog.findViewById(R.id.grid_view_pnt);
        dialog.findViewById(R.id.card_pnt_book_car).setVisibility(View.GONE);

        String[] imageCaption = new String[]{"Filters", "Lead ID"};
        int[] imgSource = new int[]{R.drawable.ic_filter_24dp, R.drawable.ic_filter_lead};

        final int[] fAnswerId = new int[]{0, 1};
        gridView.setAdapter(new PlanNextTaskPicker().new TaskPickerGridAdapter(this, imgSource, imageCaption, fAnswerId));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openTransferEnqActivity(fAnswerId[position], locationsDB.getLocation_id());
                dialog.dismiss();
            }

            private void openTransferEnqActivity(int from, String userLocation) {
                Intent intent = new Intent(HomeActivity.this, TransferEnquiriesActivity.class);
                intent.putExtra("lead_transfer_type", from);
                intent.putExtra("lead_transfer_user_location", userLocation);
                startActivity(intent);
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


    }
}
