package com.salescrm.telephony.luckywheel;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.salescrm.telephony.R;

/**
 * Created by prateek on 27/6/18.
 */

public class SpinDoneDialog {
    private SpinDialogDoneListener spinDialogDoneListener;
    private Context context;
    private Dialog dialog;
    private String title1;
    private String title2;
    private TextView tvTitle1, tvTitle2;
    private ImageView imgBooster;
    private TextView tvBtnOk;
    private String boosterSign;
    private String heading;
    private TextView tvYouHaveWon;


    public SpinDoneDialog(Context context, String text1, String text2, String boosterSign, String heading, SpinDialogDoneListener spinDialogDoneListener) {
        this.context = context;
        this.title1 = text1;
        this.title2 = text2;
        this.boosterSign = boosterSign;
        this.heading = heading;
        this.spinDialogDoneListener = spinDialogDoneListener;
    }

    public void show(){
        dialog = new Dialog(context, R.style.Transparent);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
        }

        dialog.setContentView(R.layout.spin_done_dialog_layout);
        tvTitle1 = (TextView) dialog.findViewById(R.id.tv_title1_spindone);
        tvTitle2 = (TextView) dialog.findViewById(R.id.tv_title2_spindone);
        imgBooster = (ImageView) dialog.findViewById(R.id.img_spindone);
        tvBtnOk = (TextView) dialog.findViewById(R.id.btn_ok_spindone);
        tvYouHaveWon = (TextView) dialog.findViewById(R.id.tv_you_have_won);
        if(boosterSign.equalsIgnoreCase("1")) {
            imgBooster.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.booster));
            imgBooster.setVisibility(View.VISIBLE);
        }else{
            imgBooster.setVisibility(View.GONE);
        }

        tvYouHaveWon.setText(heading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        tvTitle1.setText(title1);
        tvTitle2.setText(title2);

        tvBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinDialogDoneListener.onOkPressed();
                dialog.dismiss();
            }
        });
    }
}
