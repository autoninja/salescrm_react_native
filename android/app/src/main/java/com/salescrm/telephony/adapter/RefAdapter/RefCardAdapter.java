package com.salescrm.telephony.adapter.RefAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.DetailsOfEtvbr;
import com.salescrm.telephony.activity.EtvbrImgDialog;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.db.etvbr_location.SalesConsultant;
import com.salescrm.telephony.db.ref_location.RefInfo;
import com.salescrm.telephony.db.ref_location.RefLocation;
import com.salescrm.telephony.db.ref_location.RefSalesConsultant;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.EtvbrLocationChildFragment;
import com.salescrm.telephony.fragments.EtvbrLocationFragment;
import com.salescrm.telephony.fragments.retExcFin.RefLocationChildFragment;
import com.salescrm.telephony.fragments.retExcFin.RefLocationFragment;
import com.salescrm.telephony.utils.CircleTransform;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.picasso.Picasso;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by prateek on 26/6/17.
 */

public class RefCardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context context;
    private Realm realm;
    private RefLocation locationsResult;
    private RealmList<RefSalesConsultant> salesConsultants;

    public RefCardAdapter(FragmentActivity activity, Realm realm, RefLocation results) {
        this.context = activity;
        this.realm = realm;
        this.locationsResult = results;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.ref_card_adapter, parent, false);
        return new EtvbrCardHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final RecyclerView.ViewHolder iViewHolder = holder;

        if(RefLocationFragment.isLocationAvailable()) {
            if (RefLocationChildFragment.PERCENT) {
                viewForRel(holder, position);
            } else {
                viewForAbsolute(holder, position);
            }
        }else{
            if (RefLocationFragment.PERCENT) {
                viewForRel(holder, position);
            } else {
                viewForAbsolute(holder, position);
            }
        }

    }

    private void viewForRel(RecyclerView.ViewHolder holder, final int pos) {
        EtvbrCardHolder cardHolder = (EtvbrCardHolder) holder;
        final int position = pos;
        salesConsultants = locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getSalesConsultants();

        cardHolder.tvTargetExch.setVisibility(View.GONE);
        cardHolder.tvTargetFin.setVisibility(View.GONE);
        cardHolder.tvTargetFinOut.setVisibility(View.GONE);
        cardHolder.tvTargetPB.setVisibility(View.GONE);
        cardHolder.tvTargetLE.setVisibility(View.GONE);
        cardHolder.tvTargetRetail.setVisibility(View.GONE);
        cardHolder.viewTarget.setVisibility(View.GONE);
        cardHolder.tvTarget.setVisibility(View.GONE);
        cardHolder.llRetail.setVisibility(View.GONE);
        cardHolder.rlFrameLayout.setPadding(0, 6, 0, 6);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_grey, null);
        cardHolder.tvTarget.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        if (pos % 2 == 0) {
            cardHolder.llaLinearLayout.setBackgroundColor(Color.parseColor("#243F6D"));
        } else {
            //Transparent
            cardHolder.llaLinearLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        if (pos < salesConsultants.size()) {


            cardHolder.tvRetail.setPaintFlags(0);
            cardHolder.tvRetail.setText("" + salesConsultants.get(pos).getInfo().getRel().getRetails());

            cardHolder.tvExch.setPaintFlags(0);
            cardHolder.tvExch.setText(""+salesConsultants.get(pos).getInfo().getRel().getExchanged());

            cardHolder.tvFin.setPaintFlags(0);
            cardHolder.tvFin.setText(""+salesConsultants.get(pos).getInfo().getRel().getIn_house_financed());

            cardHolder.tvFinOut.setPaintFlags(0);
            cardHolder.tvFinOut.setText(""+salesConsultants.get(pos).getInfo().getRel().getOut_house_financed());

            cardHolder.tvPB.setPaintFlags(0);
            cardHolder.tvPB.setText(getReadableData(salesConsultants.get(pos).getInfo().getRel().getPending_bookings()));

            cardHolder.tvLE.setPaintFlags(0);
            cardHolder.tvLE.setText(getReadableData(salesConsultants.get(pos).getInfo().getRel().getLive_enquiries()));

            cardHolder.tvRetail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvExch.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvFin.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvFinOut.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvPB.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvLE.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvRetail.setVisibility(View.GONE);
            cardHolder.viewReatil.setVisibility(View.GONE);
            cardHolder.llRetail.setVisibility(View.GONE);

            if (salesConsultants.get(pos).getInfo().getDpUrl() != null && !salesConsultants.get(pos).getInfo().getDpUrl().equalsIgnoreCase("")) {
                String url =salesConsultants.get(pos).getInfo().getDpUrl();
                cardHolder.imgUser.setVisibility(View.VISIBLE);
                cardHolder.tvName.setVisibility(View.GONE);
                cardHolder.tvUserTotal.setVisibility(View.GONE);
                cardHolder.refFooter.setVisibility(View.GONE);
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUser);
            } else {
                if (salesConsultants.get(pos).getInfo().getName() != null && !salesConsultants.get(pos).getInfo().getName().equalsIgnoreCase("")) {
                    cardHolder.tvName.setText(salesConsultants.get(pos).getInfo().getName().charAt(0) + "");
                } else {
                    cardHolder.tvName.setText("N");
                }
                cardHolder.imgUser.setVisibility(View.GONE);
                cardHolder.tvName.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotal.setVisibility(View.GONE);
                cardHolder.refFooter.setVisibility(View.GONE);
            }



            cardHolder.viewExch.setVisibility(View.VISIBLE);
            cardHolder.viewFin.setVisibility(View.VISIBLE);
            cardHolder.viewFinOut.setVisibility(View.VISIBLE);
            cardHolder.viewPB.setVisibility(View.VISIBLE);
            cardHolder.viewLE.setVisibility(View.VISIBLE);

            // Text Normal
            cardHolder.tvRetail.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvExch.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvFin.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvFinOut.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvPB.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvLE.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvUserTotal.setTypeface(null, Typeface.NORMAL);

            if(isUserOnlyDse() && (pos == (salesConsultants.size() -1))){
                System.out.println("Hello mate "+pos+", "+salesConsultants.size());
                cardHolder.refFooter.setVisibility(View.VISIBLE);
            }

        }else {

            cardHolder.tvRetail.setPaintFlags(0);
            cardHolder.tvRetail.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getRetails());

            cardHolder.tvExch.setPaintFlags(0);
            cardHolder.tvExch.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getExchanged());

            cardHolder.tvFin.setPaintFlags(0);
            cardHolder.tvFin.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getIn_house_financed());

            cardHolder.tvFinOut.setPaintFlags(0);
            cardHolder.tvFinOut.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getOut_house_financed());

            cardHolder.tvPB.setPaintFlags(0);
            cardHolder.tvPB.setText(getReadableData(locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getPending_bookings()));

            cardHolder.tvLE.setPaintFlags(0);
            cardHolder.tvLE.setText(getReadableData(locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getRel().getLive_enquiries()));

            cardHolder.tvRetail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvExch.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvFin.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvFinOut.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvPB.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvLE.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.imgUser.setVisibility(View.GONE);
            cardHolder.tvName.setVisibility(View.GONE);
            cardHolder.tvUserTotal.setText("Total%");
            cardHolder.tvUserTotal.setVisibility(View.VISIBLE);
            cardHolder.refFooter.setVisibility(View.VISIBLE);

            cardHolder.viewExch.setVisibility(View.GONE);
            cardHolder.viewFin.setVisibility(View.GONE);
            cardHolder.viewFinOut.setVisibility(View.GONE);
            cardHolder.viewPB.setVisibility(View.GONE);
            cardHolder.viewLE.setVisibility(View.GONE);
            cardHolder.viewReatil.setVisibility(View.GONE);

            // Text Bold
            cardHolder.tvExch.setTypeface(null, Typeface.BOLD);
            cardHolder.tvFin.setTypeface(null, Typeface.BOLD);
            cardHolder.tvFinOut.setTypeface(null, Typeface.BOLD);
            cardHolder.tvPB.setTypeface(null, Typeface.BOLD);
            cardHolder.tvLE.setTypeface(null, Typeface.BOLD);
            cardHolder.tvRetail.setTypeface(null, Typeface.BOLD);
            cardHolder.tvUserTotal.setTypeface(null, Typeface.BOLD);
        }

        cardHolder.imgUser.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return openImageRel(salesConsultants.get(pos).getInfo());
            }
        });


        cardHolder.tvName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return openImageRel(salesConsultants.get(pos).getInfo());
            }
        });
    }

    private String getReadableData(Integer data) {
        if(data==null) {
            return "--";
        }
        return ""+data;
    }

    boolean openImageRel(RefInfo info){
        Intent intent = new Intent(context, EtvbrImgDialog.class);
        Bundle bundle = new Bundle();
        bundle.putString("imgUrlEtvbr", info.getDpUrl());
        bundle.putString("dseNameEtvbr", info.getName());
        intent.putExtras(bundle);
        context.startActivity(intent);
        return true;
    }

    private void viewForAbsolute(RecyclerView.ViewHolder holder, final int pos) {
        EtvbrCardHolder cardHolder = (EtvbrCardHolder) holder;
        final int position = pos;

        cardHolder.tvTargetExch.setVisibility(View.VISIBLE);
        cardHolder.tvTargetFin.setVisibility(View.VISIBLE);
        cardHolder.tvTargetFinOut.setVisibility(View.GONE);
        cardHolder.tvTargetPB.setVisibility(View.GONE);
        cardHolder.tvTargetLE.setVisibility(View.GONE);
        cardHolder.tvTargetRetail.setVisibility(View.VISIBLE);
        cardHolder.viewTarget.setVisibility(View.VISIBLE);

        cardHolder.rlFrameLayout.setPadding(0, 6, 0, 0);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_grey, null);
        cardHolder.tvTarget.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        if (pos % 2 == 0) {
            //cardHolder.llaLinearLayout.setBackgroundColor(Color.parseColor("#243F6D"));
            cardHolder.llaLinearLayout.setBackgroundColor(Color.TRANSPARENT);
        } else {
            //Transparent
            cardHolder.llaLinearLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        if (pos < salesConsultants.size()) {

            cardHolder.tvRetail.setPaintFlags(cardHolder.tvRetail.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvRetail.setText("" + salesConsultants.get(pos).getInfo().getAbs().getRetails());

            cardHolder.tvExch.setPaintFlags(cardHolder.tvExch.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvExch.setText(""+ salesConsultants.get(pos).getInfo().getAbs().getExchanged());

            cardHolder.tvFin.setPaintFlags(cardHolder.tvExch.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvFin.setText(""+ salesConsultants.get(pos).getInfo().getAbs().getIn_house_financed());

            cardHolder.tvFinOut.setPaintFlags(cardHolder.tvExch.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvFinOut.setText(""+ salesConsultants.get(pos).getInfo().getAbs().getOut_house_financed());
            cardHolder.tvPB.setPaintFlags(cardHolder.tvExch.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvPB.setText(getReadableData(salesConsultants.get(pos).getInfo().getAbs().getPending_bookings()));
            cardHolder.tvLE.setPaintFlags(cardHolder.tvExch.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvLE.setText(getReadableData(salesConsultants.get(pos).getInfo().getAbs().getLive_enquiries()));

            cardHolder.tvRetail.setVisibility(View.VISIBLE);
            cardHolder.llRetail.setVisibility(View.VISIBLE);

            cardHolder.tvTargetExch.setText(""+salesConsultants.get(pos).getInfo().getTargets().getExchanged());
            cardHolder.tvTargetFin.setText(""+salesConsultants.get(pos).getInfo().getTargets().getIn_house_financed());
            cardHolder.tvTargetFinOut.setText(""+salesConsultants.get(pos).getInfo().getTargets().getOut_house_financed());
            cardHolder.tvTargetPB.setText(""+salesConsultants.get(pos).getInfo().getTargets().getPending_bookings());
            cardHolder.tvTargetLE.setText(""+salesConsultants.get(pos).getInfo().getTargets().getLive_enquiries());
            cardHolder.tvTargetRetail.setText(""+salesConsultants.get(pos).getInfo().getTargets().getRetails());


            if (salesConsultants.get(pos).getInfo().getDpUrl() != null && !salesConsultants.get(pos).getInfo().getDpUrl().equalsIgnoreCase("")) {
                String url = salesConsultants.get(pos).getInfo().getDpUrl();
                cardHolder.imgUser.setVisibility(View.VISIBLE);
                cardHolder.tvName.setVisibility(View.GONE);
                cardHolder.tvUserTotal.setVisibility(View.GONE);
                cardHolder.refFooter.setVisibility(View.GONE);
                cardHolder.tvTarget.setVisibility(View.GONE);
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUser);
            } else {
                if (salesConsultants.get(pos).getInfo().getName() != null && !salesConsultants.get(pos).getInfo().getName().equalsIgnoreCase("")) {
                    cardHolder.tvName.setText(salesConsultants.get(pos).getInfo().getName().charAt(0) + "");
                } else {
                    cardHolder.tvName.setText("N");
                }
                cardHolder.imgUser.setVisibility(View.GONE);
                cardHolder.tvName.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotal.setVisibility(View.GONE);
                cardHolder.refFooter.setVisibility(View.GONE);
                cardHolder.tvTarget.setVisibility(View.GONE);
            }



            if(isUserOnlyDse() && (pos == (salesConsultants.size() -1))){
                System.out.println("Hello mate "+pos+", "+salesConsultants.size());
                cardHolder.refFooter.setVisibility(View.VISIBLE);
            }

            cardHolder.viewExch.setVisibility(View.VISIBLE);
            cardHolder.viewFin.setVisibility(View.VISIBLE);
            cardHolder.viewFinOut.setVisibility(View.VISIBLE);
            cardHolder.viewPB.setVisibility(View.VISIBLE);
            cardHolder.viewLE.setVisibility(View.VISIBLE);
            cardHolder.viewReatil.setVisibility(View.VISIBLE);
            cardHolder.tvTarget.setVisibility(View.INVISIBLE);

            // Text Normal
            cardHolder.tvExch.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvFin.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvFinOut.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvPB.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvLE.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvRetail.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvUserTotal.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvTarget.setTypeface(null, Typeface.NORMAL);

            cardHolder.tvRetail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getRetails(),
                            ""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvExch.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getExchanged(),
                            ""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvFin.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(6,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getIn_house_financed(),
                            ""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvFinOut.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(7,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getOut_house_financed(),
                            ""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvPB.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(8,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getPending_bookings(),
                            ""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvLE.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(9,salesConsultants.get(pos).getInfo().getName(),
                            ""+salesConsultants.get(pos).getInfo().getId(),
                            ""+salesConsultants.get(pos).getInfo().getAbs().getLive_enquiries(),
                            ""+salesConsultants.get(pos).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

        }else{
            if(locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo()!=null) {

                cardHolder.tvRetail.setPaintFlags(cardHolder.tvRetail.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvRetail.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getRetails());

                cardHolder.tvExch.setPaintFlags(cardHolder.tvExch.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvExch.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getExchanged());

                cardHolder.tvFin.setPaintFlags(cardHolder.tvExch.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvFin.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getIn_house_financed());

                cardHolder.tvFinOut.setPaintFlags(cardHolder.tvExch.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvFinOut.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getOut_house_financed());

                cardHolder.tvPB.setPaintFlags(cardHolder.tvExch.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvPB.setText(getReadableData(locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getPending_bookings()));

                cardHolder.tvLE.setPaintFlags(cardHolder.tvExch.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                cardHolder.tvLE.setText(getReadableData(locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getLive_enquiries()));

            }
            cardHolder.tvRetail.setVisibility(View.VISIBLE);
            cardHolder.llRetail.setVisibility(View.VISIBLE);
            if(locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo()!=null) {
                cardHolder.tvTargetExch.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getTargets().getExchanged());
                cardHolder.tvTargetFin.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getTargets().getIn_house_financed());
                cardHolder.tvTargetFinOut.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getTargets().getOut_house_financed());
                cardHolder.tvTargetPB.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getTargets().getPending_bookings());
                cardHolder.tvTargetLE.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getTargets().getLive_enquiries());
                cardHolder.tvTargetRetail.setText("" + locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getTargets().getRetails());
            }
            cardHolder.imgUser.setVisibility(View.GONE);
            cardHolder.tvName.setVisibility(View.GONE);
            cardHolder.tvUserTotal.setText("Achieved");
            cardHolder.tvUserTotal.setVisibility(View.VISIBLE);
            cardHolder.refFooter.setVisibility(View.VISIBLE);
            cardHolder.tvTarget.setVisibility(View.VISIBLE);
            cardHolder.tvTarget.setText("Target");

            cardHolder.viewExch.setVisibility(View.GONE);
            cardHolder.viewFin.setVisibility(View.GONE);
            cardHolder.viewFinOut.setVisibility(View.GONE);
            cardHolder.viewPB.setVisibility(View.GONE);
            cardHolder.viewLE.setVisibility(View.GONE);
            cardHolder.viewReatil.setVisibility(View.GONE);
            cardHolder.tvTarget.setVisibility(View.VISIBLE);

            // Text Bold
            cardHolder.tvExch.setTypeface(null, Typeface.BOLD);
            cardHolder.tvFin.setTypeface(null, Typeface.BOLD);
            cardHolder.tvFinOut.setTypeface(null, Typeface.BOLD);
            cardHolder.tvPB.setTypeface(null, Typeface.BOLD);
            cardHolder.tvLE.setTypeface(null, Typeface.BOLD);
            cardHolder.tvRetail.setTypeface(null, Typeface.BOLD);
            cardHolder.tvUserTotal.setTypeface(null, Typeface.BOLD);
            cardHolder.tvTarget.setTypeface(null, Typeface.NORMAL);

            cardHolder.tvRetail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getRetails(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvExch.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getExchanged(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvFin.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(6,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getIn_house_financed(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvFinOut.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(7,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getOut_house_financed(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });
            cardHolder.tvPB.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(8,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getPending_bookings(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });
            cardHolder.tvLE.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(9,"All DSE's Data",
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getId(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getAbs().getLive_enquiries(),
                            ""+locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getInfo().getUserRoleId(),""+locationsResult.getLocationId());
                    return true;
                }
            });

        }

        cardHolder.imgUser.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return openImageAbsolute(salesConsultants.get(pos).getInfo());
            }
        });

        cardHolder.tvName.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return openImageAbsolute(salesConsultants.get(pos).getInfo());
            }
        });

    }

    boolean openImageAbsolute(RefInfo info){
        Intent intent = new Intent(context, EtvbrImgDialog.class);
        Bundle bundle = new Bundle();
        bundle.putString("imgUrlEtvbr", info.getDpUrl());
        bundle.putString("dseNameEtvbr", info.getName());
        intent.putExtras(bundle);
        context.startActivity(intent);
        return true;
    }
    @Override
    public int getItemCount() {
        if(locationsResult.isValid()
                && locationsResult.getSalesManagers().isValid()
                && locationsResult.getSalesManagers().get(0).getTeamLeaders().isValid()
                && locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getSalesConsultants().isValid()) {
            salesConsultants = locationsResult.getSalesManagers().get(0).getTeamLeaders().get(0).getSalesConsultants();
            return isUserOnlyDse()?(salesConsultants.size() > 0 ? salesConsultants.size() : 0):(salesConsultants.size() > 0 ? salesConsultants.size() + 1 : 0);
        }else {
            return 0;
        }

    }

    public boolean isUserOnlyDse () {
        boolean isUserOnlyDse =false;
        UserDetails data = realm.where(UserDetails.class).findFirst();
        if (data != null) {
            RealmList<UserRolesDB> userRoles = data.getUserRoles();
            if(userRoles!=null && userRoles.size() == 1) {
                isUserOnlyDse = (Util.getInt(userRoles.get(0).getId()) == WSConstants.USER_ROLES.DSE);
            }
        }
        return isUserOnlyDse;
    }

    public class EtvbrCardHolder extends RecyclerView.ViewHolder{

        ImageView imgUser;
        TextView tvName, tvRetail, tvExch, tvFin, tvFinOut, tvPB, tvLE;
        View viewReatil, viewExch, viewFin, viewFinOut, viewPB, viewLE;
        LinearLayout llaLinearLayout;
        TextView tvUserTotal;
        LinearLayout llRetail, llExch, llFin, llFinOut;
        TextView tvTarget;
        TextView tvTargetRetail, tvTargetExch, tvTargetFin, tvTargetFinOut, tvTargetPB, tvTargetLE;
        View viewTarget;
        RelativeLayout rlFrameLayout;
        private LinearLayout refFooter;

        public EtvbrCardHolder(View itemView) {
            super(itemView);

            imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            tvName = (TextView) itemView.findViewById(R.id.text_user);
            tvFin = (TextView) itemView.findViewById(R.id.txt_fin);
            tvFinOut = (TextView) itemView.findViewById(R.id.txt_fin_out);
            tvPB = (TextView) itemView.findViewById(R.id.txt_pending_bookings);
            tvLE = (TextView) itemView.findViewById(R.id.txt_live_enquiries);
            tvExch = (TextView) itemView.findViewById(R.id.txt_exch);
            tvRetail = (TextView) itemView.findViewById(R.id.txt_retail);
            llaLinearLayout = (LinearLayout) itemView.findViewById(R.id.ll_today_summary_main);
            tvUserTotal = (TextView) itemView.findViewById(R.id.text_user_total);
            viewExch = (View) itemView.findViewById(R.id.exch_view);
            viewFin = (View) itemView.findViewById(R.id.fin_view);
            viewFinOut = (View) itemView.findViewById(R.id.fin_out_view);
            viewPB = (View) itemView.findViewById(R.id.pending_bookings_view);
            viewLE = (View) itemView.findViewById(R.id.live_enquiries_view);
            viewReatil = (View)  itemView.findViewById(R.id.retail_view);
            tvTarget = (TextView) itemView.findViewById(R.id.tv_target_title);
            tvTargetExch = (TextView) itemView.findViewById(R.id.txt_target_exch);
            tvTargetFin = (TextView) itemView.findViewById(R.id.txt_target_fin);
            tvTargetFinOut = (TextView) itemView.findViewById(R.id.txt_target_fin_out);
            tvTargetPB = (TextView) itemView.findViewById(R.id.txt_target_pending_bookings);
            tvTargetLE = (TextView) itemView.findViewById(R.id.txt_target_live_enquiries);
            tvTargetRetail = (TextView) itemView.findViewById(R.id.txt_target_retail);
            viewTarget = (View) itemView.findViewById(R.id.target_view);
            rlFrameLayout = (RelativeLayout) itemView.findViewById(R.id.rl_frame_layout);
            llExch = (LinearLayout) itemView.findViewById(R.id.exch_ll);
            llFin = (LinearLayout) itemView.findViewById(R.id.fin_ll);
            llFinOut = (LinearLayout) itemView.findViewById(R.id.fin_out_ll);
            llRetail = (LinearLayout) itemView.findViewById(R.id.retail_ll);
            refFooter = itemView.findViewById(R.id.ref_footer);
        }
    }

    private void goToDetailsOfEtvbr(int position, String title_name, String user_id, String etvbrtotal, String user_role_id, String user_location_id) {

        Intent etvbrDetails =  null;

        switch (position){
            case 4:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Retails");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 5:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Exchanged");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 6:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "In House Financed");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 7:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Out House Financed");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 8:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Pending Bookings");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 9:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Live Enquiries");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
        }
        if(etvbrDetails != null)
            context.startActivity(etvbrDetails);
    }


}
