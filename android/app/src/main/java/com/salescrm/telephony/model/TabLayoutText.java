package com.salescrm.telephony.model;

/**
 * Created by bharath on 4/8/16.
 */
public class TabLayoutText {
    private int pos;
    private String text;
    private int max;
    private int progress;

    public TabLayoutText(int pos, String text,int max,int progress) {
        this.pos = pos;
        this.text = text;
        this.max=max;
        this.progress=progress;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
