package com.salescrm.telephony.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ravindra P on 01-09-2016.
 */
public class Activity_to_schedule {

    private String submission_id;

    private Form_object form_object;

    public String getSubmission_id ()
    {
        return submission_id;
    }

    public void setSubmission_id (String submission_id)
    {
        this.submission_id = submission_id;
    }

    public Form_object getForm_object ()
    {
        return form_object;
    }

    public void setForm_object (Form_object form_object)
    {
        this.form_object = form_object;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [submission_id = "+submission_id+", form_object = "+form_object+"]";
    }

    public class Form_object
    {
        private String questionIndent;

        private List<QuestionChildren> questionChildren;

        private String answerIndent;

        private String ifVisibleMandatory;

        private String newFormCondition;

        private String fQId;

        private DefaultFAId defaultFAId;

        private List<AnswerChildren> answerChildren;

        private ValidationObject validationObject;

        private String[] ifMeOneOfTheseMandatory;

        private String title;

        private String formInputType;

        private String hidden;

        private String answersInLine;

        private String validationRegex;

        private String answerNewLine;

        private String popupTitle;

        private String dependent_form_question_id;

        private String[] formatting;

        private Integer questionId;

        public String getfQId() {
            return fQId;
        }

        public void setfQId(String fQId) {
            this.fQId = fQId;
        }

        public Integer getQuestionId() {
            return questionId;
        }

        public void setQuestionId(Integer questionId) {
            this.questionId = questionId;
        }

        public String getQuestionIndent ()
        {
            return questionIndent;
        }

        public void setQuestionIndent (String questionIndent)
        {
            this.questionIndent = questionIndent;
        }

        public List<QuestionChildren> getQuestionChildren ()
        {
            return questionChildren;
        }

        public void setQuestionChildren (List<QuestionChildren> questionChildren)
        {
            this.questionChildren = questionChildren;
        }

        public String getAnswerIndent ()
        {
            return answerIndent;
        }

        public void setAnswerIndent (String answerIndent)
        {
            this.answerIndent = answerIndent;
        }

        public String getIfVisibleMandatory ()
        {
            return ifVisibleMandatory;
        }

        public void setIfVisibleMandatory (String ifVisibleMandatory)
        {
            this.ifVisibleMandatory = ifVisibleMandatory;
        }

        public String getNewFormCondition ()
    {
        return newFormCondition;
    }

        public void setNewFormCondition (String newFormCondition)
        {
            this.newFormCondition = newFormCondition;
        }

        public String getFQId ()
        {
            return fQId;
        }

        public void setFQId (String fQId)
        {
            this.fQId = fQId;
        }

        public DefaultFAId getDefaultFAId ()
    {
        return defaultFAId;
    }

        public void setDefaultFAId (DefaultFAId defaultFAId)
        {
            this.defaultFAId = defaultFAId;
        }

        public List<AnswerChildren> getAnswerChildren ()
        {
            return answerChildren;
        }

        public void setAnswerChildren (List<AnswerChildren> answerChildren)
        {
            this.answerChildren = answerChildren;
        }

        public ValidationObject getValidationObject ()
    {
        return validationObject;
    }

        public void setValidationObject (ValidationObject validationObject)
        {
            this.validationObject = validationObject;
        }

        public String[] getIfMeOneOfTheseMandatory ()
        {
            return ifMeOneOfTheseMandatory;
        }

        public void setIfMeOneOfTheseMandatory (String[] ifMeOneOfTheseMandatory)
        {
            this.ifMeOneOfTheseMandatory = ifMeOneOfTheseMandatory;
        }

        public String getTitle ()
        {
            return title;
        }

        public void setTitle (String title)
        {
            this.title = title;
        }

        public String getFormInputType ()
        {
            return formInputType;
        }

        public void setFormInputType (String formInputType)
        {
            this.formInputType = formInputType;
        }

        public String getHidden ()
        {
            return hidden;
        }

        public void setHidden (String hidden)
        {
            this.hidden = hidden;
        }

        public String getAnswersInLine ()
        {
            return answersInLine;
        }

        public void setAnswersInLine (String answersInLine)
        {
            this.answersInLine = answersInLine;
        }

        public String getValidationRegex ()
        {
            return validationRegex;
        }

        public void setValidationRegex (String validationRegex)
        {
            this.validationRegex = validationRegex;
        }

        public String getAnswerNewLine ()
        {
            return answerNewLine;
        }

        public void setAnswerNewLine (String answerNewLine)
        {
            this.answerNewLine = answerNewLine;
        }

        public String getPopupTitle ()
    {
        return popupTitle;
    }

        public void setPopupTitle (String popupTitle)
        {
            this.popupTitle = popupTitle;
        }

        public String getDependent_form_question_id ()
    {
        return dependent_form_question_id;
    }

        public void setDependent_form_question_id (String dependent_form_question_id)
        {
            this.dependent_form_question_id = dependent_form_question_id;
        }

        public String[] getFormatting ()
        {
            return formatting;
        }

        public void setFormatting (String[] formatting)
        {
            this.formatting = formatting;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [questionIndent = "+questionIndent+", questionChildren = "+questionChildren+", answerIndent = "+answerIndent+", ifVisibleMandatory = "+ifVisibleMandatory+", newFormCondition = "+newFormCondition+", fQId = "+fQId+", defaultFAId = "+defaultFAId+", answerChildren = "+answerChildren+", validationObject = "+validationObject+", ifMeOneOfTheseMandatory = "+ifMeOneOfTheseMandatory+", title = "+title+", formInputType = "+formInputType+", hidden = "+hidden+", answersInLine = "+answersInLine+", validationRegex = "+validationRegex+", answerNewLine = "+answerNewLine+", popupTitle = "+popupTitle+", dependent_form_question_id = "+dependent_form_question_id+", formatting = "+formatting+"]";
        }

        public class QuestionChildren implements Serializable
        {
            private List<Values> values;

            private String key;

            public List<Values> getValues ()
            {
                return values;
            }

            public void setValues (List<Values> values)
            {
                this.values = values;
            }

            public String getKey ()
            {
                return key;
            }

            public void setKey (String key)
            {
                this.key = key;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [values = "+values+", key = "+key+"]";
            }

            public class Values implements Serializable
            {
                private String questionIndent;

                private List<QuestionChildren> questionChildren;

                private String answerIndent;

                private String ifVisibleMandatory;

                private String newFormCondition;

                private String fQId;


                private DefaultFAId defaultFAId;

                private List<AnswerChildren> answerChildren;

                private ValidationObject validationObject;

                private String[] ifMeOneOfTheseMandatory;

                private String title;

                private String formInputType;

                private String hidden;

                private String answersInLine;

                private String validationRegex;

                private String answerNewLine;

                private String popupTitle;

                private String dependent_form_question_id;

                private Formatting formatting;

                private String questionId;

                private List<DefaultCases> defaultCases;
                private Integer editable;

                public Integer getEditable() {
                    return editable;
                }

                public void setEditable(Integer editable) {
                    this.editable = editable;
                }

                public List<DefaultCases> getDefaultCases() {
                    return defaultCases;
                }

                public void setDefaultCases(List<DefaultCases> defaultCases) {
                    this.defaultCases = defaultCases;
                }

                public String getfQId() {
                    return fQId;
                }

                public void setfQId(String fQId) {
                    this.fQId = fQId;
                }

                public String getQuestionId() {
                    return questionId;
                }

                public void setQuestionId(String questionId) {
                    this.questionId = questionId;
                }

                public String getQuestionIndent ()
                {
                    return questionIndent;
                }

                public void setQuestionIndent (String questionIndent)
                {
                    this.questionIndent = questionIndent;
                }

                public List<QuestionChildren> getQuestionChildren ()
                {
                    return questionChildren;
                }

                public void setQuestionChildren (List<QuestionChildren> questionChildren)
                {
                    this.questionChildren = questionChildren;
                }

                public String getAnswerIndent ()
                {
                    return answerIndent;
                }

                public void setAnswerIndent (String answerIndent)
                {
                    this.answerIndent = answerIndent;
                }

                public String getIfVisibleMandatory ()
                {
                    return ifVisibleMandatory;
                }

                public void setIfVisibleMandatory (String ifVisibleMandatory)
                {
                    this.ifVisibleMandatory = ifVisibleMandatory;
                }

                public String getNewFormCondition ()
            {
                return newFormCondition;
            }

                public void setNewFormCondition (String newFormCondition)
                {
                    this.newFormCondition = newFormCondition;
                }

                public String getFQId ()
                {
                    return fQId;
                }

                public void setFQId (String fQId)
                {
                    this.fQId = fQId;
                }

                public DefaultFAId getDefaultFAId ()
                {
                    return defaultFAId;
                }

                public void setDefaultFAId (DefaultFAId defaultFAId)
                {
                    this.defaultFAId = defaultFAId;
                }

                public List<AnswerChildren> getAnswerChildren ()
                {
                    return answerChildren;
                }

                public void setAnswerChildren (List<AnswerChildren> answerChildren)
                {
                    this.answerChildren = answerChildren;
                }

                public ValidationObject getValidationObject ()
            {
                return validationObject;
            }

                public void setValidationObject (ValidationObject validationObject)
                {
                    this.validationObject = validationObject;
                }

                public String[] getIfMeOneOfTheseMandatory ()
                {
                    return ifMeOneOfTheseMandatory;
                }

                public void setIfMeOneOfTheseMandatory (String[] ifMeOneOfTheseMandatory)
                {
                    this.ifMeOneOfTheseMandatory = ifMeOneOfTheseMandatory;
                }

                public String getTitle ()
                {
                    return title;
                }

                public void setTitle (String title)
                {
                    this.title = title;
                }

                public String getFormInputType ()
                {
                    return formInputType;
                }

                public void setFormInputType (String formInputType)
                {
                    this.formInputType = formInputType;
                }

                public String getHidden ()
                {
                    return hidden;
                }

                public void setHidden (String hidden)
                {
                    this.hidden = hidden;
                }

                public String getAnswersInLine ()
                {
                    return answersInLine;
                }

                public void setAnswersInLine (String answersInLine)
                {
                    this.answersInLine = answersInLine;
                }

                public String getValidationRegex ()
                {
                    return validationRegex;
                }

                public void setValidationRegex (String validationRegex)
                {
                    this.validationRegex = validationRegex;
                }

                public String getAnswerNewLine ()
                {
                    return answerNewLine;
                }

                public void setAnswerNewLine (String answerNewLine)
                {
                    this.answerNewLine = answerNewLine;
                }

                public String getPopupTitle ()
                {
                    return popupTitle;
                }

                public void setPopupTitle (String popupTitle)
                {
                    this.popupTitle = popupTitle;
                }

                public String getDependent_form_question_id ()
            {
                return dependent_form_question_id;
            }

                public void setDependent_form_question_id (String dependent_form_question_id)
                {
                    this.dependent_form_question_id = dependent_form_question_id;
                }

                public Formatting getFormatting ()
                {
                    return formatting;
                }

                public void setFormatting (Formatting formatting)
                {
                    this.formatting = formatting;
                }

                @Override
                public String toString()
                {
                    return "ClassPojo [questionIndent = "+questionIndent+", questionChildren = "+questionChildren+", answerIndent = "+answerIndent+", ifVisibleMandatory = "+ifVisibleMandatory+", newFormCondition = "+newFormCondition+", fQId = "+fQId+", defaultFAId = "+defaultFAId+", answerChildren = "+answerChildren+", validationObject = "+validationObject+", ifMeOneOfTheseMandatory = "+ifMeOneOfTheseMandatory+", title = "+title+", formInputType = "+formInputType+", hidden = "+hidden+", answersInLine = "+answersInLine+", validationRegex = "+validationRegex+", answerNewLine = "+answerNewLine+", popupTitle = "+popupTitle+", dependent_form_question_id = "+dependent_form_question_id+", formatting = "+formatting+"]";
                }
            }

        }

    }

    public class AnswerChildren
    {
        private String fAnsId;

        private String displayText;

        private String answerValue;

        public String getFAnsId ()
        {
            return fAnsId;
        }

        public void setFAnsId (String fAnsId)
        {
            this.fAnsId = fAnsId;
        }

        public String getDisplayText ()
        {
            return displayText;
        }

        public void setDisplayText (String displayText)
        {
            this.displayText = displayText;
        }

        public String getAnswerValue ()
        {
            return answerValue;
        }

        public void setAnswerValue (String answerValue)
        {
            this.answerValue = answerValue;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [fAnsId = "+fAnsId+", displayText = "+displayText+", answerValue = "+answerValue+"]";
        }
    }

    public class DefaultCases{
        private String id;
        private String dependent_question_answer_id;
        private String dependent_question_id;
        private String possible_answer_id;
        private Integer sequence;
        private String target_question_id;
        private Integer is_edit;

        public Integer getIs_edit() {
            return is_edit;
        }

        public void setIs_edit(Integer is_edit) {
            this.is_edit = is_edit;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDependent_question_answer_id() {
            return dependent_question_answer_id;
        }

        public void setDependent_question_answer_id(String dependent_question_answer_id) {
            this.dependent_question_answer_id = dependent_question_answer_id;
        }

        public String getDependent_question_id() {
            return dependent_question_id;
        }

        public void setDependent_question_id(String dependent_question_id) {
            this.dependent_question_id = dependent_question_id;
        }

        public String getPossible_answer_id() {
            return possible_answer_id;
        }

        public void setPossible_answer_id(String possible_answer_id) {
            this.possible_answer_id = possible_answer_id;
        }

        public Integer getSequence() {
            return sequence;
        }

        public void setSequence(Integer sequence) {
            this.sequence = sequence;
        }

        public String getTarget_question_id() {
            return target_question_id;
        }

        public void setTarget_question_id(String target_question_id) {
            this.target_question_id = target_question_id;
        }
    }
    public class DefaultFAId
    {
        private String fAnsId;

        private String displayText;

        private String answerValue;

        public String getFAnsId ()
        {
            return fAnsId;
        }

        public void setFAnsId (String fAnsId)
        {
            this.fAnsId = fAnsId;
        }

        public String getDisplayText ()
        {
            return displayText;
        }

        public void setDisplayText (String displayText)
        {
            this.displayText = displayText;
        }

        public String getAnswerValue ()
        {
            return answerValue;
        }

        public void setAnswerValue (String answerValue)
        {
            this.answerValue = answerValue;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [fAnsId = "+fAnsId+", displayText = "+displayText+", answerValue = "+answerValue+"]";
        }
    }

    public class ValidationObject {
        private String message;

        private Integer id;

        private String dataType;

        private Integer max_val;

        private Integer min_val;

        public String getMessage ()
        {
            return message;
        }

        public void setMessage (String message)
        {
            this.message = message;
        }

        public Integer getId ()
        {
            return id;
        }

        public void setId (Integer id)
        {
            this.id = id;
        }

        public String getDataType ()
        {
            return dataType;
        }

        public void setDataType (String dataType)
        {
            this.dataType = dataType;
        }

        public Integer getMax_val ()
    {
        return max_val;
    }

        public void setMax_val (Integer max_val)
        {
            this.max_val = max_val;
        }

        public Integer getMin_val ()
    {
        return min_val;
    }

        public void setMin_val (Integer min_val)
        {
            this.min_val = min_val;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [message = "+message+", id = "+id+", dataType = "+dataType+", max_val = "+max_val+", min_val = "+min_val+"]";
        }
    }




    public class Formatting{

    }


}
