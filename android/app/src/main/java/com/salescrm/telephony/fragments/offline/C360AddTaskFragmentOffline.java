package com.salescrm.telephony.fragments.offline;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.offline.OfflineC360LeadProcessActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.ActivitiesDB;
import com.salescrm.telephony.db.AllCarsDB;
import com.salescrm.telephony.db.LocationsDB;
import com.salescrm.telephony.db.create_lead.Activity_data;
import com.salescrm.telephony.db.create_lead.Activity_owner;
import com.salescrm.telephony.db.create_lead.Ad_car_details;
import com.salescrm.telephony.db.create_lead.Car_details;
import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;
import com.salescrm.telephony.db.create_lead.Ex_car_details;
import com.salescrm.telephony.db.create_lead.Lead_data;
import com.salescrm.telephony.db.crm_user.ActivityUserDB;
import com.salescrm.telephony.interfaces.AddLeadCommunication;
import com.salescrm.telephony.model.AddLeadActivityInputData;
import com.salescrm.telephony.model.AddLeadCarInputData;
import com.salescrm.telephony.model.CreateLeadInputData;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.CreForActivityResponse;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

import static android.text.TextUtils.isEmpty;
import static com.salescrm.telephony.utils.Util.showDatePicker;
import static com.salescrm.telephony.utils.Util.showTimePicker;

/**
 * Created by bharath on 30/5/16.
 */
public class C360AddTaskFragmentOffline extends Fragment implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener, View.OnFocusChangeListener, View.OnTouchListener {

    AutoCompleteTextView activityName;
    Realm realm;
    private AddLeadCommunication addLeadCommunication;
    private String TAG = "AddLeadActFrag";
    private TextView tvScheduledAt, tvDynamic;
    private String locationId = "-1";
    private String enqSourceId;
    private ProgressDialog progressDialog;
    private Preferences pref = null;
    private int apiCallCount = 0;
    private List<CreForActivityResponse.Result> creForActivity;
    private AutoCompleteTextView autoTvAssignTo;
    private Button btCreateLead;
    private EditText etRemarks, etDynamic;
    private LinearLayout llDynamic, llTestDriveDynamic;
    private AddLeadCarInputData carInputData;
    private Spinner carSpinner, carTestVisitSpinner;
    private TextView etTestDriveDynamic;
    private TextView tvScheduledAtTestDrive;
    private RealmResults<ActivityUserDB> activityUserDBList;

    private RadioGroup radioGroupVisitType;
    private LinearLayout llShowroom,llShowRoomVisit;
    private Spinner spinnerShowroom;
    private Spinner spinnerShowroomVisit;
    private RadioButton radioHome,radioOffice,radioOther;
    private EditText etHome,etOffice,etOther;
    private TextView radioVisitTypeHeader;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lead_act, container, false);


        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(getContext());
        if(OfflineC360LeadProcessActivity.salesCRMRealmTable.getCreateLeadInputDataDB().getLead_data().getLocation_id()!=null) {
            locationId = String.valueOf(OfflineC360LeadProcessActivity.salesCRMRealmTable.getCreateLeadInputDataDB().getLead_data().getLocation_id());
        }
        enqSourceId = OfflineC360LeadProcessActivity.salesCRMRealmTable.getCreateLeadInputDataDB().getLead_data().getLead_source_id();
        assignCarInputData();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);

        llShowRoomVisit = (LinearLayout) view.findViewById(R.id.ll_add_lead_showroom_visit);
        radioGroupVisitType = (RadioGroup) view.findViewById(R.id.radio_group_add_lead_visit_type);
        radioVisitTypeHeader = (TextView) view.findViewById(R.id.tv_lead_visit_adr_header);
        radioHome = (RadioButton)view.findViewById(R.id.radio_add_lead_visit_home) ;
        etHome = (EditText) view.findViewById(R.id.et_add_lead_visit_home) ;
        radioOffice = (RadioButton)view.findViewById(R.id.radio_add_lead_visit_office);
        etOffice = (EditText) view.findViewById(R.id.et_add_lead_visit_office) ;
        llShowroom = (LinearLayout) view.findViewById(R.id.ll_add_lead_showroom);
        spinnerShowroom = (Spinner) view.findViewById(R.id.spinner_add_lead_showroom);
        spinnerShowroomVisit = (Spinner) view.findViewById(R.id.spinner_add_lead_showroom_visit);
        radioOther = (RadioButton)view.findViewById(R.id.radio_add_lead_visit_other) ;
        etOther = (EditText) view.findViewById(R.id.et_add_lead_visit_other) ;
        getShowRoom(spinnerShowroom);
        getShowRoom(spinnerShowroomVisit);
        hideVisitTypeViews();
        radioGroupVisitType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                hideVisitTypeViews();
                switch (checkedId){
                    case R.id.radio_add_lead_visit_home:
                        etHome.setVisibility(View.VISIBLE);
                        break;
                    case R.id.radio_add_lead_visit_office:
                        etOffice.setVisibility(View.VISIBLE);
                        if(getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID+"")) {
                            llShowroom.setVisibility(View.VISIBLE);
                            spinnerShowroom.setVisibility(View.VISIBLE);
                            getShowRoom(spinnerShowroom);
                        }
                        break;
                    case R.id.radio_add_lead_visit_other:
                        etOther.setVisibility(View.VISIBLE);
                        break;

                }
            }
        });



        llDynamic = (LinearLayout) view.findViewById(R.id.ll_add_lead_act_dynamic);
        llTestDriveDynamic = (LinearLayout) view.findViewById(R.id.ll_add_lead_act_test_drive_dynamic);
        tvDynamic = (TextView) view.findViewById(R.id.tv_add_lead_act_dynamic);
        etDynamic = (EditText) view.findViewById(R.id.et_add_lead_act_dynamic);
        carSpinner = (Spinner) view.findViewById(R.id.spinner_add_lead_act_dynamic);
        carTestVisitSpinner = (Spinner) view.findViewById(R.id.spinner_add_lead_act_test_drive_dynamic);
        etTestDriveDynamic = (TextView) view.findViewById(R.id.et_add_lead_act_test_addr_dynamic);
        tvScheduledAtTestDrive = (TextView) view.findViewById(R.id.tv_lead_scheduled_test_drive_date_val);
        setSpinnerAdapter(carTestVisitSpinner, new String[]{"HOME VISIT", "SHOWROOM VISIT"});

        activityName = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_add_lead_act_name);
        autoTvAssignTo = (AutoCompleteTextView) view.findViewById(R.id.auto_tv_add_lead_assign_to);
        tvScheduledAt = (TextView) view.findViewById(R.id.tv_lead_scheduled_date_val);
        btCreateLead = (Button) view.findViewById(R.id.bt_add_lead);
        etRemarks = (EditText) view.findViewById(R.id.et_tv_add_lead_remarks);
        btCreateLead.setVisibility(View.GONE);
        OfflineC360LeadProcessActivity.tvAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etRemarks.getText())) {
                    Toast.makeText(getContext(), "Remarks should not be empty", Toast.LENGTH_SHORT).show();
                    etRemarks.requestFocus();
                }
                else if(getScheduledDateTime().equalsIgnoreCase("")){
                    Toast.makeText(getContext(), "Scheduled date should not be empty", Toast.LENGTH_SHORT).show();
                }else {
                    updateCarDetails();
                    updateActivityDetails();
                }
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, getActivityNames());
        activityName.setAdapter(adapter);
        activityName.setOnFocusChangeListener(this);
        autoTvAssignTo.setOnFocusChangeListener(this);
        autoTvAssignTo.setOnTouchListener(this);
        activityName.setOnTouchListener(this);
        activityName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                handleViewChanges();
                System.out.println("Acticity: size::" + realm.where(ActivityUserDB.class).findAll().size());
                populateAdaptersOffline();
               /* if (new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                    Util.HideKeyboard(getActivity());
                    progressDialog.show();
                    handleViewChanges();

                    //getCreForActivity();
                } else {
                    System.out.println("No Internet connection");
                }*/
            }
        });

        tvScheduledAt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(C360AddTaskFragmentOffline.this, getActivity(), R.id.tv_lead_scheduled_date_val, Calendar.getInstance(), null);
            }
        });

        tvScheduledAtTestDrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(C360AddTaskFragmentOffline.this, getActivity(), R.id.tv_lead_scheduled_test_drive_date_val, Calendar.getInstance(), null);
            }
        });

        return view;


    }

    private void hideVisitTypeViews() {
        etHome.setText("");
        etOffice.setText("");
       /* if(AddLeadActivity.customerInputData!=null) {
            if (AddLeadActivity.customerInputData.getAddressType() == WSConstants.ADDRESS_TYPE_OFFICE) {
                etOffice.setText(AddLeadActivity.customerInputData.getFull_address());
            } else if (AddLeadActivity.customerInputData.getAddressType() == WSConstants.ADDRESS_TYPE_HOME) {
                etHome.setText(AddLeadActivity.customerInputData.getFull_address());
            }
        }*/
        etOther.setText("");
        etOffice.setVisibility(View.GONE);
        etHome.setVisibility(View.GONE);
        etOther.setVisibility(View.GONE);
        llShowroom.setVisibility(View.GONE);
        spinnerShowroom.setVisibility(View.GONE);
    }

    private void getShowRoom(Spinner spinner) {

            String[] arr = new String[realm.where(LocationsDB.class).findAll().size()+1];
            arr[0]="Select showroom";
            int position=0;
            for (int i = 0; i < realm.where(LocationsDB.class).findAll().size(); i++) {
                arr[i+1] = realm.where(LocationsDB.class).findAll().get(i).getName();
                if(locationId!=null) {
                    if ((realm.where(LocationsDB.class).findAll().get(i).getId()+"").equalsIgnoreCase(locationId)){
                        position = i+1;
                    }
                }

            }
            spinner.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, arr));
            spinner.setSelection(position);

    }

    private void updateActivityDetails() {
        realm.beginTransaction();
        CreateLeadInputDataDB realmData = OfflineC360LeadProcessActivity.salesCRMRealmTable.getCreateLeadInputDataDB();
        if (realmData != null) {
            Activity_data dbActivity_data = realm.createObject(Activity_data.class);
            Activity_owner dbActivity_owner = realm.createObject(Activity_owner.class);
            CreateLeadInputData.Activity_data actData = getActivityInputData().getActivity_data();
            CreateLeadInputData.Activity_data.Activity_owner activity_owner = getActivityInputData().getActivity_data().getActivity_owner();
            if (activity_owner != null) {
                dbActivity_owner.setRole_id(activity_owner.getRole_id());
                dbActivity_owner.setUser_id(activity_owner.getUser_id());
            }


            dbActivity_data.setActivity_owner(dbActivity_owner);
            dbActivity_data.setVisit_type(actData.getVisit_type());
            dbActivity_data.setScheduledDateTime(actData.getScheduledDateTime());
            dbActivity_data.setLocation_id(actData.getLocation_id());
            dbActivity_data.setActivity(actData.getActivity());
            dbActivity_data.setAddress(actData.getAddress());
            dbActivity_data.setVisit_time(actData.getVisit_time());
            dbActivity_data.setCarId(actData.getCarId());
            dbActivity_data.setCarName(actData.getCarName());
            dbActivity_data.setAssignName(actData.getAssignName());
            dbActivity_data.setActivityName(actData.getActivityName());
            dbActivity_data.setRemarks(actData.getRemarks());

            realmData.setActivity_data(dbActivity_data);
            realmData.setRemarks(actData.getRemarks());

        }
        realm.commitTransaction();
        getActivity().onBackPressed();
    }

    private void updateCarDetails() {
        realm.beginTransaction();
        Lead_data data = OfflineC360LeadProcessActivity.salesCRMRealmTable.getCreateLeadInputDataDB().getLead_data();
        AddLeadCarInputData carCurrentData = carInputData;
        if (data != null && carCurrentData != null) {
            if (carCurrentData.getNewCarInputData().size() > 0) {
                RealmList<Car_details> carDetailsList = new RealmList<Car_details>();

                for (int i = 0; i < carCurrentData.getNewCarInputData().size(); i++) {
                    Car_details car_details = realm.createObject(Car_details.class); //
                    car_details.setBrand_id(carCurrentData.getNewCarInputData().get(i).getBrand_id());
                    car_details.setFuel_type_id(carCurrentData.getNewCarInputData().get(i).getFuel_type_id());
                    car_details.setColor_id(carCurrentData.getNewCarInputData().get(i).getColor_id());
                    car_details.setModel_id(carCurrentData.getNewCarInputData().get(i).getModel_id());
                    car_details.setVariant_id(carCurrentData.getNewCarInputData().get(i).getVariant_id());
                    car_details.setIs_activity_target(carCurrentData.getNewCarInputData().get(i).getIs_activity_target());
                    car_details.setIs_primary(carCurrentData.getNewCarInputData().get(i).getIs_primary());
                    car_details.setBrand_name(carCurrentData.getNewCarInputData().get(i).getBrand_name());
                    car_details.setModel_name(carCurrentData.getNewCarInputData().get(i).getModel_name());
                    car_details.setVariant_name(carCurrentData.getNewCarInputData().get(i).getVariant_name());
                    car_details.setColor_name(carCurrentData.getNewCarInputData().get(i).getColor_name());
                    car_details.setFuel_name(carCurrentData.getNewCarInputData().get(i).getFuel_name());
                    carDetailsList.add(car_details);
                }
                data.setCar_details(carDetailsList);
            }

            if (carCurrentData.getExCarInputData().size() > 0) {
                RealmList<Ex_car_details> exCarDetailsList = new RealmList<Ex_car_details>();

                for (int i = 0; i < carCurrentData.getExCarInputData().size(); i++) {
                    Ex_car_details ex_car_details = realm.createObject(Ex_car_details.class);

                    ex_car_details.setMilage(carCurrentData.getExCarInputData().get(i).getMilage());
                    ex_car_details.setKms_run(carCurrentData.getExCarInputData().get(i).getKms_run());
                    ex_car_details.setModel_id(carCurrentData.getExCarInputData().get(i).getModel_id());
                    ex_car_details.setIs_primary(carCurrentData.getExCarInputData().get(i).getIs_primary());
                    ex_car_details.setPurchase_date(carCurrentData.getExCarInputData().get(i).getPurchase_date());
                    ex_car_details.setIs_activity_target(carCurrentData.getExCarInputData().get(i).is_activity_target());
                    ex_car_details.setBrand_id(carCurrentData.getExCarInputData().get(i).getBrand_id());
                    ex_car_details.setBrand_name(carCurrentData.getExCarInputData().get(i).getBrand_name());
                    ex_car_details.setModel_name(carCurrentData.getExCarInputData().get(i).getModel_name());


                    exCarDetailsList.add(ex_car_details);
                }
                data.setEx_car_details(exCarDetailsList);
            }

            if (carCurrentData.getAddCarInputData().size() > 0) {
                RealmList<Ad_car_details> adCarDetailsList = new RealmList<Ad_car_details>();

                for (int i = 0; i < carCurrentData.getAddCarInputData().size(); i++) {
                    Ad_car_details ad_car_details = realm.createObject(Ad_car_details.class);

                    ad_car_details.setMilage(carCurrentData.getAddCarInputData().get(i).getMilage());
                    ad_car_details.setKms_run(carCurrentData.getAddCarInputData().get(i).getKms_run());
                    ad_car_details.setModel_id(carCurrentData.getAddCarInputData().get(i).getModel_id());
                    ad_car_details.setIs_primary(carCurrentData.getAddCarInputData().get(i).getIs_primary());
                    ad_car_details.setPurchase_date(carCurrentData.getAddCarInputData().get(i).getPurchase_date());
                    ad_car_details.setIs_activity_target(carCurrentData.getAddCarInputData().get(i).is_activity_target());
                    ad_car_details.setBrand_id(carCurrentData.getAddCarInputData().get(i).getBrand_id());
                    ad_car_details.setBrand_name(carCurrentData.getAddCarInputData().get(i).getBrand_name());
                    ad_car_details.setModel_name(carCurrentData.getAddCarInputData().get(i).getModel_name());


                    adCarDetailsList.add(ad_car_details);
                }
                data.setAd_car_details(adCarDetailsList);
            }
        }
        realm.commitTransaction();
    }

    private void assignCarInputData() {
        carInputData = new AddLeadCarInputData();
        CreateLeadInputDataDB realmCarData = OfflineC360LeadProcessActivity.salesCRMRealmTable.getCreateLeadInputDataDB();
        if (realmCarData != null && realmCarData.getLead_data() != null) {
            List<CreateLeadInputData.Lead_data.Car_details> carList = new ArrayList<>();
            for (int i = 0; i < realmCarData.getLead_data().getCar_details().size(); i++) {

                CreateLeadInputData.Lead_data.Car_details currentCar = new CreateLeadInputData().new Lead_data().new Car_details();
                Car_details dbCurrentCar = realmCarData.getLead_data().getCar_details().get(i);
                currentCar.setBrand_id(dbCurrentCar.getBrand_id());
                currentCar.setFuel_type_id(dbCurrentCar.getFuel_type_id());
                currentCar.setColor_id(dbCurrentCar.getColor_id());
                currentCar.setModel_id(dbCurrentCar.getModel_id());
                currentCar.setVariant_id(dbCurrentCar.getVariant_id());
                currentCar.setIs_activity_target(dbCurrentCar.getIs_activity_target());
                currentCar.setIs_primary(dbCurrentCar.getIs_primary());
                currentCar.setBrand_name(dbCurrentCar.getBrand_name());
                currentCar.setModel_name(dbCurrentCar.getModel_name());
                currentCar.setVariant_name(dbCurrentCar.getVariant_name());
                currentCar.setColor_name(dbCurrentCar.getColor_name());
                currentCar.setFuel_name(dbCurrentCar.getFuel_name());
                carList.add(currentCar);
            }
            carInputData.setNewCarInputData(carList);


            List<CreateLeadInputData.Lead_data.Ex_car_details> exCarList = new ArrayList<>();
            for (int i = 0; i < realmCarData.getLead_data().getEx_car_details().size(); i++) {

                CreateLeadInputData.Lead_data.Ex_car_details currentCar = new CreateLeadInputData().new Lead_data().new Ex_car_details();
                Ex_car_details dbCurrentCar = realmCarData.getLead_data().getEx_car_details().get(i);
                currentCar.setBrand_name(dbCurrentCar.getBrand_name());
                currentCar.setModel_name(dbCurrentCar.getModel_name());
                currentCar.setMilage(dbCurrentCar.getMilage());
                currentCar.setKms_run(dbCurrentCar.getKms_run());
                currentCar.setBrand_id(dbCurrentCar.getBrand_id());
                currentCar.setModel_id(dbCurrentCar.getModel_id());
                currentCar.setIs_primary(dbCurrentCar.getIs_primary());
                currentCar.setIs_activity_target(dbCurrentCar.is_activity_target());
                currentCar.setPurchase_date(dbCurrentCar.getPurchase_date());
                exCarList.add(currentCar);
            }
            carInputData.setExCarInputData(exCarList);


            List<CreateLeadInputData.Lead_data.Ad_car_details> addCarList = new ArrayList<>();
            for (int i = 0; i < realmCarData.getLead_data().getAd_car_details().size(); i++) {

                CreateLeadInputData.Lead_data.Ad_car_details currentCar = new CreateLeadInputData().new Lead_data().new Ad_car_details();
                Ad_car_details dbCurrentCar = realmCarData.getLead_data().getAd_car_details().get(i);
                currentCar.setBrand_name(dbCurrentCar.getBrand_name());
                currentCar.setModel_name(dbCurrentCar.getModel_name());
                currentCar.setMilage(dbCurrentCar.getMilage());
                currentCar.setKms_run(dbCurrentCar.getKms_run());
                currentCar.setBrand_id(dbCurrentCar.getBrand_id());
                currentCar.setModel_id(dbCurrentCar.getModel_id());
                currentCar.setIs_primary(dbCurrentCar.getIs_primary());
                currentCar.setIs_activity_target(dbCurrentCar.is_activity_target());
                currentCar.setPurchase_date(dbCurrentCar.getPurchase_date());
                addCarList.add(currentCar);
            }
            carInputData.setAddCarInputData(addCarList);


        }

    }

    private void populateAdaptersOffline() {
        if (!TextUtils.isEmpty(getActivityId())) {
            System.out.println("location id:"+locationId);
            if(locationId==null) {
                locationId = "-1";
            }
            System.out.println("location id After:"+locationId);
            activityUserDBList = realm.where(ActivityUserDB.class)
                    .equalTo("location_id", locationId)
                    .equalTo("activity_id", getActivityId())
                    .beginGroup()
                    .equalTo("lead_source_id", enqSourceId)
                    .or()
                    .equalTo("lead_source_id", "-1")
                    .endGroup().findAll();

            String[] arr = new String[activityUserDBList.size()];
            for (int i = 0; i < activityUserDBList.size(); i++) {
                arr[i] = activityUserDBList.get(i).getName();
            }
            autoTvAssignTo.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, arr));
            autoTvAssignTo.requestFocus();


        }
    }

    private void handleViewChanges() {
        if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.FOLLOWUP_CALL_ID+"")) {
            radioGroupVisitType.clearCheck();
            llDynamic.setVisibility(View.GONE);
            llShowRoomVisit.setVisibility(View.GONE);
        } else {
            radioGroupVisitType.setVisibility(View.VISIBLE);
            radioVisitTypeHeader.setText("Visit Type");
            llShowRoomVisit.setVisibility(View.GONE);
            radioGroupVisitType.clearCheck();
            etDynamic.setText("");
            autoTvAssignTo.setText("");
            llDynamic.setVisibility(View.VISIBLE);
            etDynamic.setVisibility(View.VISIBLE);
            tvDynamic.setVisibility(View.VISIBLE);
            carSpinner.setVisibility(View.VISIBLE);
            llTestDriveDynamic.setVisibility(View.GONE);

            if(getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID+"")||
                    getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID+"")){
                setSpinnerAdapter(carSpinner,getNewCarData());
            }
            else if(getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID+"")){
                setSpinnerAdapter(carSpinner,getExCarData());
            }

            if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.HOME_VISIT_ID+"")) {
                radioVisitTypeHeader.setText("Visit address");
                llTestDriveDynamic.setVisibility(View.VISIBLE);
                carSpinner.setVisibility(View.GONE);
                etDynamic.setVisibility(View.GONE);
                tvDynamic.setVisibility(View.GONE);

            }
            else if(getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.SHOWROOM_VISIT_ID+"")){
                llShowRoomVisit.setVisibility(View.VISIBLE);
                getShowRoom(spinnerShowroomVisit);
                carSpinner.setVisibility(View.GONE);
                etDynamic.setVisibility(View.GONE);
                tvDynamic.setVisibility(View.GONE);
            }
            else if(getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID+"")){
                carSpinner.setVisibility(View.VISIBLE);
                llTestDriveDynamic.setVisibility(View.VISIBLE);
                etDynamic.setVisibility(View.GONE);
                tvDynamic.setVisibility(View.GONE);
            }
            else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID+"")) {
                llShowRoomVisit.setVisibility(View.VISIBLE);
                getShowRoom(spinnerShowroomVisit);
                carSpinner.setVisibility(View.VISIBLE);
                llTestDriveDynamic.setVisibility(View.VISIBLE);
                etDynamic.setVisibility(View.GONE);
                tvDynamic.setVisibility(View.GONE);
                radioGroupVisitType.setVisibility(View.GONE);
            } else if (getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID+"")) {
                llTestDriveDynamic.setVisibility(View.VISIBLE);
                carSpinner.setVisibility(View.VISIBLE);
                etDynamic.setVisibility(View.GONE);
                tvDynamic.setVisibility(View.GONE);
            }
        }
    }

   /* private void getCreForActivity() {
        System.out.println(TAG);
        System.out.println("EnqSourceId:" + enqSourceId);
        System.out.println("locationId:" + locationId);
        System.out.println("locationId:" + locationId);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetCreForActivity(getActivityId(), enqSourceId, locationId, "", this);

    }*/

    private String getActivityId() {
        if (realm.where(ActivitiesDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, activityName.getText().toString()).findAll().size() > 0) {
            return String.valueOf(realm.where(ActivitiesDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, activityName.getText().toString()).findFirst().getId());
        } else {
            return "";
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, monthOfYear, dayOfMonth);
        if ((Integer.parseInt(view.getTag())) == R.id.tv_lead_scheduled_date_val) {
            tvScheduledAt.setText(String.format(Locale.getDefault(), "%d %s %d", dayOfMonth, calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), year));
            tvScheduledAt.setTag(R.id.autoninja_date, calendar);
            showTimePicker(C360AddTaskFragmentOffline.this, getActivity(), R.id.tv_lead_scheduled_date_val);
        } else {
            tvScheduledAtTestDrive.setText(String.format(Locale.getDefault(), "%d %s %d", dayOfMonth, calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()).toUpperCase().substring(0, 3), year));
            tvScheduledAtTestDrive.setTag(R.id.autoninja_date, calendar);

        }


    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {

        Calendar calDate = (Calendar) tvScheduledAt.getTag(R.id.autoninja_date);
        calDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calDate.set(Calendar.MINUTE, minute);
        calDate.set(Calendar.SECOND, second);
        tvScheduledAt.setTag(R.id.autoninja_date, calDate);
        tvScheduledAt.append(String.format(Locale.getDefault(), ", %s", Util.getAmPmTime(calDate)));

    }


    private String[] getActivityNames() {
        String[] arr = new String[realm.where(ActivitiesDB.class).findAll().size() + 1];
        arr[0] = "Select";
        for (int i = 0; i < realm.where(ActivitiesDB.class).findAll().size(); i++) {
            arr[i + 1] = realm.where(ActivitiesDB.class).findAll().get(i).getName();

        }
        return arr;
    }

    public void setSpinnerAdapter(Spinner spinner, String data[]) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, data);
        spinner.setAdapter(adapter);
    }


    @Override
    public void onAttach(Context context) {
        try {
            addLeadCommunication = (AddLeadCommunication) context;
        } catch (Exception e) {
            System.out.println("ClassCastException");
        }
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);

    }


    private AddLeadActivityInputData getActivityInputData() {
        CreateLeadInputData.Activity_data activity_data = new CreateLeadInputData().new Activity_data();

        if (!TextUtils.isEmpty(getActivityId())) {

            activity_data.setAddress(etDynamic.getText().toString());
            activity_data.setActivity(getActivityId());
            if(getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_HOME_ID+"")
                    ||getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.CONDUCT_TEST_DRIVE_SHOWROOM_ID+"")){
                setCarActivityTarget(0);
            }
            else if(getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID+"")){
                setCarActivityTarget(1);
            }

            if(llTestDriveDynamic.getVisibility()==View.VISIBLE){
                if(radioGroupVisitType.getVisibility()==View.VISIBLE) {
                    if(llShowroom.getVisibility()==View.VISIBLE){
                        activity_data.setLocation_id(getLocationId(spinnerShowroom));
                    }
                    switch (radioGroupVisitType.getCheckedRadioButtonId()) {
                        case R.id.radio_add_lead_visit_home:
                            activity_data.setVisit_type("1");
                            activity_data.setAddress(etHome.getText().toString());
                            break;
                        case R.id.radio_add_lead_visit_office:
                            activity_data.setVisit_type("2");
                            activity_data.setAddress(etOffice.getText().toString());
                            break;
                        case R.id.radio_add_lead_visit_other:
                            activity_data.setVisit_type(null);
                            activity_data.setAddress(etOther.getText().toString());
                            break;
                    }
                }

            }
            activity_data.setVisit_time(getScheduledDateTime());
            if(llShowRoomVisit.getVisibility()==View.VISIBLE){
                activity_data.setLocation_id(getLocationId(spinnerShowroomVisit));
            }
            activity_data.setActivityName(activityName.getText().toString());
            activity_data.setAssignName(autoTvAssignTo.getText().toString());
            if(carSpinner.getSelectedItem()!=null){
                activity_data.setCarName(carSpinner.getSelectedItem().toString());
            }
            activity_data.setRemarks(etRemarks.getText().toString());
            if (getActivityOwnerOffline().getRole_id() != null) {
                activity_data.setActivity_owner(getActivityOwnerOffline());
            }
            if (!TextUtils.isEmpty(getScheduledDateTime())) {
                activity_data.setScheduledDateTime(getScheduledDateTime());
            }
        }




        System.out.println("getScheduled Date:" + getScheduledDateTime());
        return new AddLeadActivityInputData(activity_data, etRemarks.getText().toString());

    }

    private String getLocationId(Spinner spinner) {
        if (realm.where(LocationsDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, spinner.getSelectedItem().toString()).findAll().size() > 0) {
            return String.valueOf(realm.where(LocationsDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, spinner.getSelectedItem().toString()).findFirst().getId());
        } else {
            return null;
        }
    }

    private String getScheduledDateTime() {
        if (tvScheduledAt.getTag(R.id.autoninja_date) != null) {
            Calendar cal = (Calendar) tvScheduledAt.getTag(R.id.autoninja_date);
            System.out.println(TAG + "SQLTime:" + Util.getSQLDateTime(cal.getTime()));
            return Util.getSQLDateTime((cal.getTime()));
        }
        return "";
    }


    /*@Subscribe
    public void onAddLeadPrimaryElements(AddLeadInputPrimaryElements data) {
        if (data.getEnqSourceId() != null) {
            this.enqSourceId = data.getEnqSourceId();
        }

        if (data.getLocation_id() != null) {
            this.locationId = data.getLocation_id();
        }
        this.carInputData = data.getCarInputData();


    }*/

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus && v instanceof AutoCompleteTextView && isEmpty(((AutoCompleteTextView) v).getText().toString())) {
            ((AutoCompleteTextView) v).showDropDown();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (TextUtils.isEmpty(((AutoCompleteTextView) v).getText().toString())) {
            ((AutoCompleteTextView) v).showDropDown();
        }
        return false;
    }


    private CreateLeadInputData.Activity_data.Activity_owner getActivityOwner() {
        CreateLeadInputData.Activity_data.Activity_owner activity_owner = new CreateLeadInputData().new Activity_data().new Activity_owner();
        if (activityUserDBList != null) {
            for (int i = 0; i < activityUserDBList.size(); i++) {
                if (autoTvAssignTo.getText().toString().equalsIgnoreCase(activityUserDBList.get(i).getName())) {
                    activity_owner.setRole_id(activityUserDBList.get(i).getRole_id());
                    activity_owner.setUser_id(activityUserDBList.get(i).getId());
                    return activity_owner;
                }
            }
        }
        return activity_owner;
    }

    public CreateLeadInputData.Activity_data.Activity_owner getActivityOwnerOffline() {
        CreateLeadInputData.Activity_data.Activity_owner activity_owner = new CreateLeadInputData().new Activity_data().new Activity_owner();

        if (!TextUtils.isEmpty(getActivityId())) {
            if (TextUtils.isEmpty(locationId) || locationId.equalsIgnoreCase("0")) {
                locationId = "-1";
            }
            ActivityUserDB activityUserDB = realm.where(ActivityUserDB.class)
                    .equalTo("location_id", locationId)
                    .equalTo("activity_id", getActivityId())
                    .beginGroup()
                    .equalTo("lead_source_id", enqSourceId)
                    .or()
                    .equalTo("lead_source_id", "-1")
                    .endGroup()
                    .equalTo("name", autoTvAssignTo.getText().toString())
                    .findFirst();
            if (activityUserDB != null) {
                activity_owner.setRole_id(activityUserDB.getRole_id());
                activity_owner.setUser_id(activityUserDB.getId());
                return activity_owner;
            }

        }
        return activity_owner;
    }

    public String[] getNewCarData() {
        String data[] = new String[carInputData.getNewCarInputData().size() + 1];
        data[0] = "Select Car";
        for (int i = 0; i < carInputData.getNewCarInputData().size(); i++) {
            data[i + 1] = getCarModelName(Integer.parseInt(carInputData.getNewCarInputData().get(i).getModel_id()));
        }
        return data;
    }

    private String getCarModelName(int id) {
        if (realm.where(AllCarsDB.class).equalTo("id", id).findAll().size() > 0) {
            return String.valueOf(realm.where(AllCarsDB.class).equalTo("id", id).findFirst().getName());
        } else {
            return "";
        }
    }

    private String getCarModelId(String name) {
        if (realm.where(AllCarsDB.class).equalTo("name", name).findAll().size() > 0) {
            return String.valueOf(realm.where(AllCarsDB.class).equalTo("name", name).findFirst().getId());
        } else {
            return "";
        }
    }

    public String[] getExCarData() {
        String data[] = new String[carInputData.getExCarInputData().size() + 1];
        data[0] = "Select Exchange Car";
        for (int i = 0; i < carInputData.getExCarInputData().size(); i++) {
            data[i + 1] = getCarModelName(Integer.parseInt(carInputData.getExCarInputData().get(i).getModel_id()));
        }
        return data;
    }


    public String getScheduledTestDriveTime() {
        if (tvScheduledAtTestDrive.getTag(R.id.autoninja_date) != null) {
            Calendar cal = (Calendar) tvScheduledAtTestDrive.getTag(R.id.autoninja_date);
            System.out.println(TAG + "SQLTime:" + Util.getSQLDateTime(cal.getTime()));
            return Util.getSQLDateTime((cal.getTime()));
        }
        return "";
    }


    private void setCarActivityTarget(int from) {
        String carId = getCarModelId(carSpinner.getSelectedItem().toString());
        if (from == 0) {
            //TestDrive //Proposal
            for (int i = 0; i < carInputData.getNewCarInputData().size(); i++) {
                if (carInputData.getNewCarInputData().get(i).getModel_id().equalsIgnoreCase(carId)) {
                    carInputData.getNewCarInputData().get(i).setIs_activity_target(true);
                    break;
                }
            }
        } else if (from == 1) {
            //Evaluation
            for (int i = 0; i < carInputData.getExCarInputData().size(); i++) {
                if (carInputData.getExCarInputData().get(i).getModel_id().equalsIgnoreCase(carId)) {
                    carInputData.getExCarInputData().get(i).setIs_activity_target(true);
                    break;
                }
            }

        }
    }
}
