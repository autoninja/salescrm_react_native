package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by bharath on 1/9/16.
 */
public class AddLeadCustomerInputData {

    private String gender;
    private String title;
    private String type_of_cust_id;

    private String firstName;
    private String lastName;
    private String age;
    private List<CreateLeadInputData.Lead_data.Mobile_numbers> mobileNumberses;
    private List<CreateLeadInputData.Lead_data.Emails> emails;
    private String address;
    private String city;
    private String pinCode;
    private int addressType;
    private String company_name;
    private String occupation;



    public AddLeadCustomerInputData() {
    }

    public AddLeadCustomerInputData(
                                    String title,
                                    String type_of_cust_id,
                                    String firstName,
                                    String lastName,
                                    String age,
                                    List<CreateLeadInputData.Lead_data.Mobile_numbers> mobileNumberses,
                                    List<CreateLeadInputData.Lead_data.Emails> emails, String address,
                                    String city,
                                    String pinCode,
                                    int addressType,
                                    String company_name,
                                    String occupation) {
        this.title = title;
        this.type_of_cust_id = type_of_cust_id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.mobileNumberses = mobileNumberses;
        this.emails = emails;
        this.address = address;
        this.city = city;
        this.pinCode = pinCode;
        this.addressType = addressType;
        this.company_name = company_name;
        this.occupation = occupation;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType_of_cust_id() {
        return type_of_cust_id;
    }

    public void setType_of_cust_id(String type_of_cust_id) {
        this.type_of_cust_id = type_of_cust_id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public List<CreateLeadInputData.Lead_data.Mobile_numbers> getMobileNumberses() {
        return mobileNumberses;
    }

    public void setMobileNumberses(List<CreateLeadInputData.Lead_data.Mobile_numbers> mobileNumberses) {
        this.mobileNumberses = mobileNumberses;
    }

    public List<CreateLeadInputData.Lead_data.Emails> getEmails() {
        return emails;
    }

    public void setEmails(List<CreateLeadInputData.Lead_data.Emails> emails) {
        this.emails = emails;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public int getAddressType() {
        return addressType;
    }

    public void setAddressType(int addressType) {
        this.addressType = addressType;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
}
