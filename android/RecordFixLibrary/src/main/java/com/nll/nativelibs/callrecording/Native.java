package com.nll.nativelibs.callrecording;


import android.content.Context;
import android.media.AudioRecord;

public class Native {
    static {
        System.loadLibrary("acr");
    }

    //Do not change values of these! They tied to values in native binary
    public static int FIX_ANDROID_7_1_OFF = 0;
    public static int FIX_ANDROID_7_1_ON = 1;


    /**
     * It may not always work! Try restarting the phone if it fails. Must be called before start7. Not implemented on start3.
     * @param value
     * @return
     */
    public static native int fixAndroid71(int value);

    public static native int start7(Context context, AudioRecord audioRecord, long serial, String key);

    public static native int stop7();

    public static native int start3(Context context, AudioRecord audioRecord, long serial, String key);

    public static native int stop3();

    public static native long getExpiry(long serial, String key);

    public static native int checkPackageAndCert(Context context);


}
