package com.salescrm.telephony.db;

import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;
import com.salescrm.telephony.utils.Util;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 27-06-2016.
 */
public class SalesCRMRealmTable extends RealmObject {

    @PrimaryKey
    private int             scheduledActivityId;
    private int             oldScheduledActivityId;
    private int             activityId;
    private String          activityDescription;
    private Date            activityCreationDate = Util.getDateTime(0);
    private Date            activityScheduleDate=Util.getDateTime(0);
    private String          activityTypeId;
    private String          activityType;
    private String          activityName;
    private String          activityGroupId;
    private String          activityIconType;
    @Index
    private int             customerId;
    @Index
    private String          customerNumber;
    @Index
    private String          customerName;
    private String          customerEmail;
    private String          customerDesignation;
    private String          customerOccupation;
    private String          customerAddress;
    private String          oemSource;
    private String          typeOfCustomer;
    private String          modeOfPayment;
    private String          buyerType;
    private String          firstName;
    private String          lastName;
    private String          buyerTypeId;
    private String          gender;
    private String          title;
    private String          enqnumber;
    private String          enquiryDate;

    private String          residencePinCode;
    private String          residenceAddress;
    private String          residenceLocality;
    private String          residenceCity;
    private String          residenceState;

    private String          officePinCode;
    private String          officeAddress;
    private String          officeLocality;
    private String          officeCity;
    private String          officeState;


    private String          companyName;


    private String          customerAge;
    private String          closeDate;


    @Index
    private Long             leadId=0L;
    private String          leadTagsName;
    private String          leadTagsColor;
    private String          leadDseName;
    private String          leadCarVariantName;
    private String          leadCarModelName;
    private String          leadCarModelId;
    private String          leadDsemobileNumber;
    private String          leadStageId;
    private String          leadStage;
    private int             leadAge;
    private String          leadSourceName;
    private String          vinRegNo;
    private String          leadSourceId;
    private String          leadLastUpdated;
    private Date            leadExpectedClosingDate;
    private String          leadLocationId;
    private String          leadLocationName;
    private String          leadEnquiryNumber;
    private String          leadCloseDate;
    private int             isLeadActive;
    private String          leadStageProgressId;
    private String          leadStageProgressName;
    private String          leadStageProgressWidth;
    private String          leadStageProgressBarClass;
    private String          leadChecklisId;
    private String          leadChecklistValueTitle;
    private int             leadActive;
    private String          leadTagNames;
    private Integer         dseId;
    private boolean         templateExist;
    private long            secondaryMobileNumber;


    private boolean createdOffline;
    private int taskCompleted;
    private boolean isSync=false;

    private boolean c360EditOffline;

    private boolean isDone=false;
    private Date dataEntryDate;

    public RealmList<CustomerPhoneNumbers> customerPhoneNumbers = new RealmList<>();
    public RealmList<CustomerEmailId> customerEmailId = new RealmList<>();

    public RealmList<PlannedActivities> plannedActivities = new RealmList<>();
    private FormObjectDb formQuestionDB;
    private FormAnswerDB formAnswerDB;
    private AllotDseDB allotDseAnswer;
    private CreateLeadInputDataDB createLeadInputDataDB;
    private boolean isCreatedTemporary=false;
    private RealmList<LeadStageProgressDB> leadStageProgressDB;
   /* public RealmList<LeadDetails> leadDetails = new RealmList<>();
    public RealmList<CustomerDetails> customerDetails = new RealmList<>();
    */

    public Integer getDseId() {
        return dseId;
    }

    public void setDseId(Integer dseId) {
        this.dseId = dseId;
    }

    public boolean isCreatedTemporary() {
        return isCreatedTemporary;
    }

    public void setCreatedTemporary(boolean createdTemporary) {
        isCreatedTemporary = createdTemporary;
    }

    public int getOldScheduledActivityId() {
        return oldScheduledActivityId;
    }

    public void setOldScheduledActivityId(int oldScheduledActivityId) {
        this.oldScheduledActivityId = oldScheduledActivityId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setLeadAge(int leadAge) {
        this.leadAge = leadAge;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getResidenceAddress() {
        return residenceAddress;
    }

    public void setResidenceAddress(String residenceAddress) {
        this.residenceAddress = residenceAddress;
    }

    public String getResidencePinCode() {
        return residencePinCode;
    }

    public void setResidencePinCode(String residencePinCode) {
        this.residencePinCode = residencePinCode;
    }

    public String getResidenceLocality() {
        return residenceLocality;
    }

    public void setResidenceLocality(String residenceLocality) {
        this.residenceLocality = residenceLocality;
    }

    public String getResidenceCity() {
        return residenceCity;
    }

    public void setResidenceCity(String residenceCity) {
        this.residenceCity = residenceCity;
    }

    public String getResidenceState() {
        return residenceState;
    }

    public void setResidenceState(String residenceState) {
        this.residenceState = residenceState;
    }

    public String getOfficeLocality() {
        return officeLocality;
    }

    public void setOfficeLocality(String officeLocality) {
        this.officeLocality = officeLocality;
    }

    public String getOfficeCity() {
        return officeCity;
    }

    public void setOfficeCity(String officeCity) {
        this.officeCity = officeCity;
    }

    public String getOfficeState() {
        return officeState;
    }

    public void setOfficeState(String officeState) {
        this.officeState = officeState;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public String getOfficePinCode() {
        return officePinCode;
    }

    public void setOfficePinCode(String officePinCode) {
        this.officePinCode = officePinCode;
    }

    public String getCustomerAge() {
        return customerAge;
    }

    public void setCustomerAge(String customerAge) {
        this.customerAge = customerAge;
    }

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public Date getActivityScheduleDate() {
        return activityScheduleDate;
    }

    public void setActivityScheduleDate(Date activityScheduleDate) {
        this.activityScheduleDate = activityScheduleDate;
    }

    public Date getActivityCreationDate() {
        return activityCreationDate;
    }

    public void setActivityCreationDate(Date activityCreationDate) {
        this.activityCreationDate = activityCreationDate;
    }

    public String getCustomerDesignation() {
        return customerDesignation;
    }

    public void setCustomerDesignation(String customerDesignation) {
        this.customerDesignation = customerDesignation;
    }

    public String getCustomerOccupation() {
        return customerOccupation;
    }

    public void setCustomerOccupation(String customerOccupation) {
        this.customerOccupation = customerOccupation;
    }

    public String getEnqnumber() {
        return enqnumber;
    }

    public void setEnqnumber(String enqnumber) {
        if(enqnumber!=null || this.enqnumber == null){
            this.enqnumber = enqnumber;
        }

    }

    public long getSecondaryMobileNumber() {
        return secondaryMobileNumber;
    }

    public void setSecondaryMobileNumber(long secondaryMobileNumber) {
        this.secondaryMobileNumber = secondaryMobileNumber;
    }

    public String getEnquiryDate() {
        return enquiryDate;
    }

    public void setEnquiryDate(String enquiryDate) {
        this.enquiryDate = enquiryDate;
    }

    public String getBuyerTypeId() {
        return buyerTypeId;
    }

    public void setBuyerTypeId(String buyerTypeId) {
        this.buyerTypeId = buyerTypeId;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getOemSource() {
        return oemSource;
    }

    public void setOemSource(String oemSource) {
        this.oemSource = oemSource;
    }

    public String getTypeOfCustomer() {
        return typeOfCustomer;
    }

    public void setTypeOfCustomer(String typeOfCustomer) {
        this.typeOfCustomer = typeOfCustomer;
    }

    public String getModeOfPayment() {
        return modeOfPayment;
    }

    public void setModeOfPayment(String modeOfPayment) {
        this.modeOfPayment = modeOfPayment;
    }

    public String getBuyerType() {
        return buyerType;
    }

    public void setBuyerType(String buyerType) {
        this.buyerType = buyerType;
    }

    public String getLeadCarVariantName() {
        return leadCarVariantName;
    }

    public void setLeadCarVariantName(String leadCarVariantName) {
        this.leadCarVariantName = leadCarVariantName;
    }

    public String getLeadCarModelId() {
        return leadCarModelId;
    }

    public void setLeadCarModelId(String leadCarModelId) {
        this.leadCarModelId = leadCarModelId;
    }

    public String getLeadCarModelName() {
        return leadCarModelName;
    }

    public void setLeadCarModelName(String leadCarModelName) {
        this.leadCarModelName = leadCarModelName;
    }

    public String getLeadDsemobileNumber() {
        return leadDsemobileNumber;
    }

    public void setLeadDsemobileNumber(String leadDsemobileNumber) {
        this.leadDsemobileNumber = leadDsemobileNumber;
    }

    public String getLeadLocationId() {
        return leadLocationId;
    }

    public void setLeadLocationId(String leadLocationId) {
        this.leadLocationId = leadLocationId;
    }

    public String getLeadLocationName() {
        return leadLocationName;
    }

    public void setLeadLocationName(String leadLocationName) {
        this.leadLocationName = leadLocationName;
    }

    public int getLeadActive() {
        return leadActive;
    }

    public void setLeadActive(int leadActive) {
        this.leadActive = leadActive;
    }

    public String getLeadTagNames() {
        return leadTagNames;
    }

    public void setLeadTagNames(String leadTagNames) {
        this.leadTagNames = leadTagNames;
    }

    public String getLeadEnquiryNumber() {
        return leadEnquiryNumber;
    }

    public void setLeadEnquiryNumber(String leadEnquiryNumber) {
        this.leadEnquiryNumber = leadEnquiryNumber;
    }

    public String getLeadCloseDate() {
        return leadCloseDate;
    }

    public void setLeadCloseDate(String leadCloseDate) {
        this.leadCloseDate = leadCloseDate;
    }

    public int getIsLeadActive() {
        return isLeadActive;
    }

    public void setIsLeadActive(int isLeadActive) {
        this.isLeadActive = isLeadActive;
    }

    public String getLeadStageProgressId() {
        return leadStageProgressId;
    }

    public void setLeadStageProgressId(String leadStageProgressId) {
        this.leadStageProgressId = leadStageProgressId;
    }

    public String getLeadStageProgressName() {
        return leadStageProgressName;
    }

    public void setLeadStageProgressName(String leadStageProgressName) {
        this.leadStageProgressName = leadStageProgressName;
    }

    public String getLeadStageProgressWidth() {
        return leadStageProgressWidth;
    }

    public void setLeadStageProgressWidth(String leadStageProgressWidth) {
        this.leadStageProgressWidth = leadStageProgressWidth;
    }

    public String getLeadStageProgressBarClass() {
        return leadStageProgressBarClass;
    }

    public void setLeadStageProgressBarClass(String leadStageProgressBarClass) {
        this.leadStageProgressBarClass = leadStageProgressBarClass;
    }

    public String getLeadChecklisId() {
        return leadChecklisId;
    }

    public void setLeadChecklisId(String leadChecklisId) {
        this.leadChecklisId = leadChecklisId;
    }

    public String getLeadChecklistValueTitle() {
        return leadChecklistValueTitle;
    }

    public void setLeadChecklistValueTitle(String leadChecklistValueTitle) {
        this.leadChecklistValueTitle = leadChecklistValueTitle;
    }

    public boolean isCreatedOffline() {
        return createdOffline;
    }

    public void setCreatedOffline(boolean createdOffline) {
        this.createdOffline = createdOffline;
    }

    public int getTaskCompleted() {
        return taskCompleted;
    }

    public void setTaskCompleted(int taskCompleted) {
        this.taskCompleted = taskCompleted;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public int getScheduledActivityId() {
        return scheduledActivityId;
    }

    public void setScheduledActivityId(int scheduledActivityId) {
        this.scheduledActivityId = scheduledActivityId;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String number) {
        this.customerNumber = number;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String name) {
        this.customerName = name;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getLeadTagsName() {
        return leadTagsName;
    }

    public void setLeadTagsName(String leadTagsName) {
        this.leadTagsName = leadTagsName;
    }

    public String getLeadTagsColor() {
        return leadTagsColor;
    }

    public void setLeadTagsColor(String leadTagsColor) {
        this.leadTagsColor = leadTagsColor;
    }

    public String getActivityDescription() {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    public String getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(String activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityGroupId() {
        return activityGroupId;
    }

    public void setActivityGroupId(String activityGroupId) {
        this.activityGroupId = activityGroupId;
    }

    public String getActivityIconType() {
        return activityIconType;
    }

    public void setActivityIconType(String activityIconType) {
        this.activityIconType = activityIconType;
    }

    public String getLeadStageId() {
        return leadStageId;
    }

    public void setLeadStageId(String leadStageId) {
        this.leadStageId = leadStageId;
    }

    public String getLeadStage() {
        return leadStage;
    }

    public void setLeadStage(String leadStage) {
        this.leadStage = leadStage;
    }

    public String getLeadAge() {
        return leadAge+"";
    }

    public void setLeadAge(String leadAge) {

        this.leadAge = Util.getInt(leadAge);
    }

    public Date getDataEntryDate() {
        return dataEntryDate;
    }

    public void setDataEntryDate(Date dataEntryDate) {
        this.dataEntryDate = dataEntryDate;
    }

    public String getLeadSourceName() {
        return leadSourceName;
    }

    public void setLeadSourceName(String leadSourceName) {
        this.leadSourceName = leadSourceName;
    }

    public String getVinRegNo() {
        return vinRegNo;
    }

    public void setVinRegNo(String vinRegNo) {
        this.vinRegNo = vinRegNo;
    }

    public String getLeadSourceId() {
        return leadSourceId;
    }

    public void setLeadSourceId(String leadSourceId) {
        this.leadSourceId = leadSourceId;
    }

    public String getLeadLastUpdated() {
        return leadLastUpdated;
    }

    public void setLeadLastUpdated(String leadLastUpdated) {
        this.leadLastUpdated = leadLastUpdated;
    }

    public Date getLeadExpectedClosingDate() {
        return leadExpectedClosingDate;
    }

    public void setLeadExpectedClosingDate(String unixLeadExpectedClosingDate) {
        this.leadExpectedClosingDate =  Util.getNormalDate(unixLeadExpectedClosingDate);
    }
    public void setLeadExpectedClosingDateAsNormalString(String leadExpectedClosingDate) {
        this.leadExpectedClosingDate =  Util.getDateAsJs(leadExpectedClosingDate);
    }
    public void setLeadExpectedClosingDateAsDate(Date leadExpectedClosingDate) {
        this.leadExpectedClosingDate = leadExpectedClosingDate;
    }
  /*  public void setLeadExpectedClosingDate(String leadExpectedClosingDate) {
        this.leadExpectedClosingDate =  leadExpectedClosingDate;;
    }*/
    public String getLeadDseName() {
        return leadDseName;
    }

    public void setLeadDseName(String leadDseName) {
        this.leadDseName = leadDseName;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setFormQuestionDB(FormObjectDb formQuestionDB) {
        this.formQuestionDB = formQuestionDB;
    }

    public void setFormAnswerDB(FormAnswerDB formAnswerDB) {
        this.formAnswerDB = formAnswerDB;
    }

    public FormAnswerDB getFormAnswerDB() {
        return formAnswerDB;
    }

    public FormObjectDb getFormQuestionDB() {
        return formQuestionDB;
    }

    public AllotDseDB getAllotDseAnswer() {
        return allotDseAnswer;
    }

    public void setAllotDseAnswer(AllotDseDB allotDseAnswer) {
        this.allotDseAnswer = allotDseAnswer;
    }

    public CreateLeadInputDataDB getCreateLeadInputDataDB() {
        return createLeadInputDataDB;
    }

    public void setCreateLeadInputDataDB(CreateLeadInputDataDB dataDB) {
        this.createLeadInputDataDB = dataDB;
    }

    public void setLeadStageProgressDB(RealmList<LeadStageProgressDB> leadStageProgressDB) {
        this.leadStageProgressDB = leadStageProgressDB;
    }

    public RealmList<LeadStageProgressDB> getLeadStageProgressDB() {
        return leadStageProgressDB;
    }

    public boolean isTemplateExist() {
        return templateExist;
    }

    public void setTemplateExist(boolean templateExist) {
        this.templateExist = templateExist;
    }

    public String getFullResidenceAddress() {
        String address ="";
        if(Util.isNotNull(this.residenceAddress)) {
            address = this.residenceAddress+", ";
        }
        if(Util.isNotNull(this.residenceLocality)) {
            address = address + this.residenceLocality+", ";
        }
        if(Util.isNotNull(this.residenceCity)) {
            address = address + this.residenceCity+", ";
        }
        if(Util.isNotNull(address) && Util.isNotNull(this.residenceState)) {
            address = address +this.residenceState;
        }
        address = address.trim();
        if(address.endsWith(",")){
            address = address.substring(0, address.length()-1);
        }
        return address;
    }

    public String getFullOfficeAddress() {
        String address ="";
        if(Util.isNotNull(this.officeAddress)) {
            address = this.officeAddress+", ";
        }
        if(Util.isNotNull(this.officeLocality)) {
            address = address + this.officeLocality+", ";
        }
        if(Util.isNotNull(this.officeCity)) {
            address = address + this.officeCity+", ";
        }
        if(Util.isNotNull(address) && Util.isNotNull(this.officeState)) {
            address = address +this.officeState;
        }
        address = address.trim();
        if(address.endsWith(",")){
            address = address.substring(0, address.length()-1);
        }
        return address;
    }
}
