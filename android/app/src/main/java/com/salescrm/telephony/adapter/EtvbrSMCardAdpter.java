package com.salescrm.telephony.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.DetailsOfEtvbr;
import com.salescrm.telephony.activity.EtvbrImgDialog;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.db.etvbr_location.SalesConsultant;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.EtvbrLocationChildFragment;
import com.salescrm.telephony.fragments.EtvbrLocationFragment;
import com.salescrm.telephony.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by prateek on 9/7/17.
 */

public class EtvbrSMCardAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private Realm realm;
    private Location locationResult;
    private RealmList<SalesConsultant> salesConsutants;

    public EtvbrSMCardAdpter(FragmentActivity activity, Realm realm, Location resultSMs) {
        this.context = activity;
        this.realm = realm;
        this.locationResult = resultSMs;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.etvbr_sm_card_adpter, parent, false);
        return new EtvbrSMCardAdpter.EtvbrSMCardHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final RecyclerView.ViewHolder iViewHolder = holder;
        ((EtvbrSMCardHolder) holder).llinearLayout.setVisibility(View.GONE);

        if(EtvbrLocationFragment.isLocationAvailable()) {
            if (EtvbrLocationChildFragment.PERCENT) {
                viewCardsForRel(holder, position);
            } else {
                viewCardsForAbsolute(holder, position);
            }
        }else{
            if (EtvbrLocationFragment.PERCENT) {
                viewCardsForRel(holder, position);
            } else {
                viewCardsForAbsolute(holder, position);
            }
        }
    }

    private void viewCardsForAbsolute(RecyclerView.ViewHolder holder, int position) {
        final EtvbrSMCardHolder cardHolder = (EtvbrSMCardHolder) holder;
        final int pos = position;
        cardHolder.cardView.setVisibility(View.VISIBLE);
        cardHolder.llinearLayout.setVisibility(View.GONE);

        cardHolder.tvTargetEnquiyCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetTDCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetVisitCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetBookingCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetRetailCard.setVisibility(View.VISIBLE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 0);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        if(pos < locationResult.getSalesManagers().get(0).getTeamLeaders().size()) {
            //absolute = results.get(position).getAbs();
            cardHolder.tvEnquiryCard.setPaintFlags(cardHolder.tvEnquiryCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvEnquiryCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getEnquiries());

            cardHolder.tvTestDriveCard.setPaintFlags(cardHolder.tvTestDriveCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvTestDriveCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getTdrives());

            cardHolder.tvVisitCard.setPaintFlags(cardHolder.tvVisitCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvVisitCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getVisits());

            cardHolder.tvBookingCard.setPaintFlags(cardHolder.tvBookingCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvBookingCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getBookings());

            cardHolder.tvRetailCard.setPaintFlags(cardHolder.tvRetailCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getRetails());

            cardHolder.tvLostCard.setPaintFlags(cardHolder.tvLostCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvLostCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getLost_drop());

            cardHolder.tvEnquiryCard.setVisibility(View.VISIBLE);
            cardHolder.viewEnquiryCard.setVisibility(View.VISIBLE);
            cardHolder.tvEnquiryCard.setVisibility(View.VISIBLE);
            cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
            cardHolder.llEnquriyCard.setVisibility(View.VISIBLE);

            cardHolder.tvTargetEnquiyCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getTargets().getEnquiries());
            cardHolder.tvTargetTDCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getTargets().getTdrives());
            cardHolder.tvTargetVisitCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getTargets().getVisits());
            cardHolder.tvTargetBookingCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getTargets().getBookings());
            cardHolder.tvTargetRetailCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getTargets().getRetails());

            if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl().equalsIgnoreCase("")){
                cardHolder.tvNameCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                String url = locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl();
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUserCard);
            }else if (locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().equalsIgnoreCase("")) {
                cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvNameCard.setText(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().charAt(0) + "");
            } else {
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                cardHolder.tvNameCard.setText("N");
            }

            cardHolder.llInner.setBackgroundColor(Color.WHITE);
            cardHolder.tvEnquiryCard.setTextColor(Color.BLACK);
            cardHolder.tvTestDriveCard.setTextColor(Color.BLACK);
            cardHolder.tvVisitCard.setTextColor(Color.BLACK);
            cardHolder.tvBookingCard.setTextColor(Color.BLACK);
            cardHolder.tvRetailCard.setTextColor(Color.BLACK);

            cardHolder.tvEnquiryCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvTestDriveCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvVisitCard.setTypeface(null, Typeface.NORMAL);;
            cardHolder.tvBookingCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvRetailCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvUserTotalCard.setTypeface(null, Typeface.NORMAL);

            cardHolder.viewTdCard.setVisibility(View.VISIBLE);
            cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
            cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
            cardHolder.viewBookingCard.setVisibility(View.VISIBLE);
            cardHolder.viewLostCard.setVisibility(View.VISIBLE);

            cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
            cardHolder.tvTargetEnquiyCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
            cardHolder.tvTargetTDCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
            cardHolder.tvTargetVisitCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
            cardHolder.tvTargetBookingCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
            cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#C1C1C1"));

            cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });

            cardHolder.tvEnquiryCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        System.out.println("Hello there "+ pos);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });
            cardHolder.tvTestDriveCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        System.out.println("Hello there "+ pos);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });
            cardHolder.tvVisitCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        System.out.println("Hello there "+ pos);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });
            cardHolder.tvBookingCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        System.out.println("Hello there "+ pos);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });
            cardHolder.tvRetailCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        System.out.println("Hello there "+ pos);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });

            cardHolder.tvLostCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        System.out.println("Hello there "+ pos);
                        viewForAbsolute(cardHolder, pos);
                    }
                }
            });

            cardHolder.imgUserCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            cardHolder.tvNameCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(0,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getEnquiries(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(1,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getTdrives(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(2,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getVisits(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(3,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getBookings(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getRetails(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvLostCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getId(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getAbs().getLost_drop(),
                            ""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
            }
        }else{

            cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));

            cardHolder.tvEnquiryCard.setPaintFlags(cardHolder.tvEnquiryCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvEnquiryCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getAbs().getEnquiries());

            cardHolder.tvTestDriveCard.setPaintFlags(cardHolder.tvTestDriveCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvTestDriveCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getAbs().getTdrives());

            cardHolder.tvVisitCard.setPaintFlags(cardHolder.tvVisitCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvVisitCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getAbs().getVisits());

            cardHolder.tvBookingCard.setPaintFlags(cardHolder.tvBookingCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvBookingCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getAbs().getBookings());

            cardHolder.tvRetailCard.setPaintFlags(cardHolder.tvRetailCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getAbs().getRetails());

            cardHolder.tvLostCard.setPaintFlags(cardHolder.tvLostCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            cardHolder.tvLostCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getLost_drop());

            cardHolder.tvTargetEnquiyCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getTargets().getEnquiries());
            cardHolder.tvTargetTDCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getTargets().getTdrives());
            cardHolder.tvTargetVisitCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getTargets().getVisits());
            cardHolder.tvTargetBookingCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getTargets().getBookings());
            cardHolder.tvTargetRetailCard.setText(""+locationResult.getSalesManagers().get(0).getInfo().getTargets().getRetails());

            cardHolder.tvEnquiryCard.setVisibility(View.VISIBLE);
            cardHolder.viewEnquiryCard.setVisibility(View.INVISIBLE);
            cardHolder.tvTargetTitleCard.setVisibility(View.VISIBLE);
            cardHolder.llEnquriyCard.setVisibility(View.VISIBLE);

            cardHolder.tvNameCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
            cardHolder.imgUserCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setText("Achieved");

            cardHolder.tvTargetTitleCard.setText("Target");

            cardHolder.tvEnquiryCard.setTextColor(Color.WHITE);
            cardHolder.tvTestDriveCard.setTextColor(Color.WHITE);
            cardHolder.tvVisitCard.setTextColor(Color.WHITE);
            cardHolder.tvBookingCard.setTextColor(Color.WHITE);
            cardHolder.tvRetailCard.setTextColor(Color.WHITE);
            cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);

            cardHolder.tvEnquiryCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvTestDriveCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvVisitCard.setTypeface(null, Typeface.BOLD);;
            cardHolder.tvBookingCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvRetailCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvLostCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvUserTotalCard.setTypeface(null, Typeface.BOLD);

            cardHolder.viewTdCard.setVisibility(View.INVISIBLE);
            cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
            cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
            cardHolder.viewBookingCard.setVisibility(View.INVISIBLE);
            cardHolder.viewLostCard.setVisibility(View.INVISIBLE);

            cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetEnquiyCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetTDCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetVisitCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.tvTargetBookingCard.setBackgroundColor(Color.parseColor("#8C94AB"));
            cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#8C94AB"));

            cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(0,"All TL's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getEnquiries(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(1,"All TL's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getTdrives(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(2,"All TL's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getVisits(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(3,"All TL's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getBookings(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,"All SM's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getRetails(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            cardHolder.tvLostCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,"All SM's Data",
                            ""+locationResult.getSalesManagers().get(0).getId(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getAbs().getLost_drop(),
                            ""+locationResult.getSalesManagers().get(0).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void viewCardsForRel(RecyclerView.ViewHolder holder, int position) {
        final EtvbrSMCardHolder cardHolder = (EtvbrSMCardHolder) holder;
        final int pos = position;
        cardHolder.llinearLayout.setVisibility(View.GONE);

        cardHolder.cardView.setVisibility(View.VISIBLE);
        cardHolder.llinearLayout.setVisibility(View.GONE);
        cardHolder.tvTargetEnquiyCard.setVisibility(View.GONE);
        cardHolder.tvTargetTDCard.setVisibility(View.GONE);
        cardHolder.tvTargetVisitCard.setVisibility(View.GONE);
        cardHolder.tvTargetBookingCard.setVisibility(View.GONE);
        cardHolder.tvTargetRetailCard.setVisibility(View.GONE);
        cardHolder.viewTargetCard.setVisibility(View.GONE);
        cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
        cardHolder.llEnquriyCard.setVisibility(View.GONE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 6);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);


        if(pos < locationResult.getSalesManagers().get(0).getTeamLeaders().size()) {
            //rels = results.get(position).getRel();
            cardHolder.tvEnquiryCard.setPaintFlags(0);
            cardHolder.tvEnquiryCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getEnquiries());

            cardHolder.tvTestDriveCard.setPaintFlags(0);
            cardHolder.tvTestDriveCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getTdrives());

            cardHolder.tvVisitCard.setPaintFlags(0);
            cardHolder.tvVisitCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getVisits());

            cardHolder.tvBookingCard.setPaintFlags(0);
            cardHolder.tvBookingCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getBookings());

            cardHolder.tvRetailCard.setPaintFlags(0);
            cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getRetails());

            cardHolder.tvLostCard.setPaintFlags(0);
            cardHolder.tvLostCard.setText(""+locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getRel().getLost_drop());

            cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvLostCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.viewEnquiryCard.setVisibility(View.GONE);
            cardHolder.tvEnquiryCard.setVisibility(View.GONE);
            cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
            cardHolder.llEnquriyCard.setVisibility(View.GONE);

            if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl().equalsIgnoreCase("")){
                cardHolder.tvNameCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                String url = locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl();
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(cardHolder.imgUserCard);
            }else if (locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().equalsIgnoreCase("")) {
                cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvNameCard.setText(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().charAt(0) + "");
            } else {
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvNameCard.setVisibility(View.VISIBLE);
                cardHolder.tvNameCard.setText("N");
            }

            cardHolder.llInner.setBackgroundColor(Color.WHITE);
            cardHolder.tvEnquiryCard.setTextColor(Color.BLACK);
            cardHolder.tvTestDriveCard.setTextColor(Color.BLACK);
            cardHolder.tvVisitCard.setTextColor(Color.BLACK);
            cardHolder.tvBookingCard.setTextColor(Color.BLACK);
            cardHolder.tvRetailCard.setTextColor(Color.BLACK);

            cardHolder.tvEnquiryCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvTestDriveCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvVisitCard.setTypeface(null, Typeface.NORMAL);;
            cardHolder.tvBookingCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvRetailCard.setTypeface(null, Typeface.NORMAL);
            cardHolder.tvLostCard.setTypeface(null, Typeface.NORMAL);

            cardHolder.viewTdCard.setVisibility(View.VISIBLE);
            cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
            cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
            cardHolder.viewBookingCard.setVisibility(View.VISIBLE);
            cardHolder.viewLostCard.setVisibility(View.VISIBLE);

            cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(cardHolder.llinearLayout.getVisibility() == View.VISIBLE){
                        cardHolder.llinearLayout.setVisibility(View.GONE);
                    }else {
                        cardHolder.llinearLayout.setVisibility(View.VISIBLE);
                        viewForRel(cardHolder, pos);
                    }
                }
            });

            cardHolder.imgUserCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            cardHolder.tvNameCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName() != null && !locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.VISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
            }

        }else{
            cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));

            cardHolder.tvEnquiryCard.setPaintFlags(0);
            cardHolder.tvEnquiryCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getRel().getEnquiries());

            cardHolder.tvTestDriveCard.setPaintFlags(0);
            cardHolder.tvTestDriveCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getRel().getTdrives());

            cardHolder.tvVisitCard.setPaintFlags(0);
            cardHolder.tvVisitCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getRel().getVisits());

            cardHolder.tvBookingCard.setPaintFlags(0);
            cardHolder.tvBookingCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getRel().getBookings());

            cardHolder.tvRetailCard.setPaintFlags(0);
            cardHolder.tvRetailCard.setText("" + locationResult.getSalesManagers().get(0).getInfo().getRel().getRetails());

            cardHolder.tvLostCard.setPaintFlags(0);
            cardHolder.tvLostCard.setText(""+ locationResult.getSalesManagers().get(0).getInfo().getRel().getLost_drop());

            cardHolder.tvEnquiryCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvTestDriveCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvVisitCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvBookingCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvLostCard.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return true;
                }
            });

            cardHolder.tvEnquiryCard.setVisibility(View.GONE);
            cardHolder.viewEnquiryCard.setVisibility(View.GONE);

            cardHolder.tvNameCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
            cardHolder.imgUserCard.setVisibility(View.GONE);
            cardHolder.tvUserTotalCard.setText("Total%");


            cardHolder.tvEnquiryCard.setTextColor(Color.WHITE);
            cardHolder.tvTestDriveCard.setTextColor(Color.WHITE);
            cardHolder.tvVisitCard.setTextColor(Color.WHITE);
            cardHolder.tvBookingCard.setTextColor(Color.WHITE);
            cardHolder.tvRetailCard.setTextColor(Color.WHITE);
            cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);

            cardHolder.tvEnquiryCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvTestDriveCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvVisitCard.setTypeface(null, Typeface.BOLD);;
            cardHolder.tvBookingCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvRetailCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvLostCard.setTypeface(null, Typeface.BOLD);
            cardHolder.tvUserTotalCard.setTypeface(null, Typeface.BOLD);

            cardHolder.viewTdCard.setVisibility(View.INVISIBLE);
            cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
            cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
            cardHolder.viewBookingCard.setVisibility(View.INVISIBLE);
            cardHolder.viewLostCard.setVisibility(View.INVISIBLE);

            if(DbUtils.isBike()){
                cardHolder.llVisitCard.setVisibility(View.GONE);
                cardHolder.llRetailsCard.setVisibility(View.GONE);
                cardHolder.viewVisitCard.setVisibility(View.GONE);
                cardHolder.viewReatilCard.setVisibility(View.GONE);
            }else{
                cardHolder.llVisitCard.setVisibility(View.VISIBLE);
                cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
                cardHolder.viewVisitCard.setVisibility(View.INVISIBLE);
                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void viewForAbsolute(RecyclerView.ViewHolder holder, int pos) {
        final EtvbrSMCardHolder cardHolder = (EtvbrSMCardHolder) holder;
        final int position = pos;
        cardHolder.llinearLayout.removeAllViews();
        salesConsutants = locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getSalesConsultants();
        for (int a = 0; a < salesConsutants.size(); a++) {
            final int positionInner = a;
            LinearLayout ll = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.etvbr_sm_inner_layout, null, false);

            ((TextView) ll.findViewById(R.id.txt_enquiry)).setPaintFlags(cardHolder.tvEnquiryCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_enquiry)).setText(""+salesConsutants.get(a).getInfo().getAbs().getEnquiries());

            ((TextView) ll.findViewById(R.id.txt_td)).setPaintFlags(cardHolder.tvEnquiryCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_td)).setText(""+salesConsutants.get(a).getInfo().getAbs().getTdrives());

            ((TextView) ll.findViewById(R.id.txt_visit)).setPaintFlags(cardHolder.tvEnquiryCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_visit)).setText(""+salesConsutants.get(a).getInfo().getAbs().getVisits());

            ((TextView) ll.findViewById(R.id.txt_booking)).setPaintFlags(cardHolder.tvEnquiryCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_booking)).setText(""+salesConsutants.get(a).getInfo().getAbs().getBookings());

            ((TextView) ll.findViewById(R.id.txt_retail)).setPaintFlags(cardHolder.tvEnquiryCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_retail)).setText(""+salesConsutants.get(a).getInfo().getAbs().getRetails());

            ((TextView) ll.findViewById(R.id.txt_lost)).setPaintFlags(cardHolder.tvEnquiryCard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            ((TextView) ll.findViewById(R.id.txt_lost)).setText(""+salesConsutants.get(a).getInfo().getAbs().getLost_drop());

            ((TextView) ll.findViewById(R.id.txt_enquiry)).setVisibility(View.VISIBLE);
            ((View) ll.findViewById(R.id.enquiry_view)).setVisibility(View.VISIBLE);
            ((TextView) ll.findViewById(R.id.tv_target_title)).setVisibility(View.INVISIBLE);
            ((LinearLayout) ll.findViewById(R.id.enquiry_ll)).setVisibility(View.VISIBLE);

            ((TextView) ll.findViewById(R.id.txt_target_enquiry)).setText(""+salesConsutants.get(a).getInfo().getTargets().getEnquiries());
            ((TextView) ll.findViewById(R.id.txt_target_td)).setText(""+salesConsutants.get(a).getInfo().getTargets().getTdrives());
            ((TextView) ll.findViewById(R.id.txt_target_visit)).setText(""+salesConsutants.get(a).getInfo().getTargets().getVisits());
            ((TextView) ll.findViewById(R.id.txt_target_booking)).setText(""+salesConsutants.get(a).getInfo().getTargets().getBookings());
            ((TextView) ll.findViewById(R.id.txt_target_retail)).setText(""+salesConsutants.get(a).getInfo().getTargets().getRetails());
            ((TextView) ll.findViewById(R.id.txt_target_enquiry)).setVisibility(View.VISIBLE);
            ((TextView) ll.findViewById(R.id.txt_target_td)).setVisibility(View.VISIBLE);
            ((TextView) ll.findViewById(R.id.txt_target_visit)).setVisibility(View.VISIBLE);
            ((TextView) ll.findViewById(R.id.txt_target_booking)).setVisibility(View.VISIBLE);
            ((TextView) ll.findViewById(R.id.txt_target_retail)).setVisibility(View.VISIBLE);
            ((View) ll.findViewById(R.id.target_view)).setVisibility(View.VISIBLE);
            ((RelativeLayout) ll.findViewById(R.id.rl_frame_layout)).setPadding(0, 6, 0, 0);

            if(salesConsutants.get(a).getInfo().getDpUrl() != null && !salesConsutants.get(a).getInfo().getDpUrl().equalsIgnoreCase("")){
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.GONE);
                String url = salesConsutants.get(a).getInfo().getDpUrl();
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(((ImageView) ll.findViewById(R.id.img_user)));
            }else if (salesConsutants.get(positionInner).getInfo().getName() != null && !salesConsutants.get(positionInner).getInfo().getName().equalsIgnoreCase("")) {
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.GONE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setText(""+salesConsutants.get(positionInner).getInfo().getName().charAt(0));
            } else {
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.GONE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setText("N");
            }

            ((ImageView) ll.findViewById(R.id.img_user)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(salesConsutants.get(positionInner).getInfo().getDpUrl() != null && !salesConsutants.get(positionInner).getInfo().getDpUrl().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", salesConsutants.get(positionInner).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", salesConsutants.get(positionInner).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.text_user)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(salesConsutants.get(positionInner).getInfo().getName() != null && !salesConsutants.get(positionInner).getInfo().getName().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", salesConsutants.get(positionInner).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", salesConsutants.get(positionInner).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.txt_enquiry)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(0,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getEnquiries(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.txt_td)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(1,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getTdrives(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.txt_visit)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(2,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getVisits(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.txt_booking)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(3,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getBookings(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.txt_retail)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(4,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getRetails(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.txt_lost)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    goToDetailsOfEtvbr(5,salesConsutants.get(positionInner).getInfo().getName(),
                            ""+salesConsutants.get(positionInner).getInfo().getId(),
                            ""+salesConsutants.get(positionInner).getInfo().getAbs().getLost_drop(),
                            ""+salesConsutants.get(positionInner).getInfo().getUserRoleId(),""+locationResult.getLocationId());
                    return true;
                }
            });


            if(DbUtils.isBike()){
                ((LinearLayout) ll.findViewById(R.id.visit_ll)).setVisibility(View.GONE);
                ((LinearLayout) ll.findViewById(R.id.retail_ll)).setVisibility(View.GONE);
                ((View) ll.findViewById(R.id.retail_view)).setVisibility(View.GONE);
                ((View) ll.findViewById(R.id.visit_view)).setVisibility(View.GONE);
            }else{
                ((LinearLayout) ll.findViewById(R.id.visit_ll)).setVisibility(View.VISIBLE);
                ((LinearLayout) ll.findViewById(R.id.retail_ll)).setVisibility(View.VISIBLE);
                ((View) ll.findViewById(R.id.retail_view)).setVisibility(View.VISIBLE);
                ((View) ll.findViewById(R.id.visit_view)).setVisibility(View.VISIBLE);
            }
            cardHolder.llinearLayout.addView(ll);
        }
    }


    private void viewForRel(RecyclerView.ViewHolder holder, int pos) {
        EtvbrSMCardHolder cardHolder = (EtvbrSMCardHolder) holder;
        final int position = pos;
        cardHolder.llinearLayout.removeAllViews();
        salesConsutants = locationResult.getSalesManagers().get(0).getTeamLeaders().get(pos).getSalesConsultants();
        for (int a = 0; a < salesConsutants.size(); a++) {
            final int positionInner = a;
            LinearLayout ll = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.etvbr_sm_inner_layout, null, false);
            ((TextView) ll.findViewById(R.id.txt_enquiry)).setText(""+salesConsutants.get(a).getInfo().getRel().getEnquiries());
            ((TextView) ll.findViewById(R.id.txt_td)).setText(""+salesConsutants.get(a).getInfo().getRel().getTdrives());
            ((TextView) ll.findViewById(R.id.txt_visit)).setText(""+salesConsutants.get(a).getInfo().getRel().getVisits());
            ((TextView) ll.findViewById(R.id.txt_booking)).setText(""+salesConsutants.get(a).getInfo().getRel().getBookings());
            ((TextView) ll.findViewById(R.id.txt_retail)).setText(""+salesConsutants.get(a).getInfo().getRel().getRetails());
            ((TextView) ll.findViewById(R.id.txt_lost)).setText(""+salesConsutants.get(a).getInfo().getRel().getLost_drop());
            ((TextView) ll.findViewById(R.id.txt_enquiry)).setVisibility(View.GONE);
            ((View)ll.findViewById(R.id.enquiry_view)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.tv_target_title)).setVisibility(View.GONE);
            ((LinearLayout) ll.findViewById(R.id.enquiry_ll)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.txt_target_enquiry)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.txt_target_td)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.txt_target_visit)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.txt_target_booking)).setVisibility(View.GONE);
            ((TextView) ll.findViewById(R.id.txt_target_retail)).setVisibility(View.GONE);
            ((View) ll.findViewById(R.id.target_view)).setVisibility(View.GONE);
            ((RelativeLayout) ll.findViewById(R.id.rl_frame_layout)).setPadding(0, 6, 0, 6);

            if(salesConsutants.get(a).getInfo().getDpUrl() != null && !salesConsutants.get(a).getInfo().getDpUrl().equalsIgnoreCase("")){
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.GONE);
                String url = salesConsutants.get(a).getInfo().getDpUrl();
                Picasso.with(context).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(((ImageView) ll.findViewById(R.id.img_user)));
            }else if (salesConsutants.get(a).getInfo().getName() != null && !salesConsutants.get(a).getInfo().getName().equalsIgnoreCase("")) {
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.GONE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setText(""+salesConsutants.get(a).getInfo().getName().charAt(0));
            } else {
                ((ImageView) ll.findViewById(R.id.img_user)).setVisibility(View.GONE);
                ((TextView) ll.findViewById(R.id.text_user)).setVisibility(View.VISIBLE);
                ((TextView) ll.findViewById(R.id.text_user)).setText("N");
            }


            ((ImageView) ll.findViewById(R.id.img_user)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(salesConsutants.get(positionInner).getInfo().getDpUrl() != null && !salesConsutants.get(positionInner).getInfo().getDpUrl().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", salesConsutants.get(positionInner).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", salesConsutants.get(positionInner).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });

            ((TextView) ll.findViewById(R.id.text_user)).setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Intent intent = new Intent(context, EtvbrImgDialog.class);
                    Bundle bundle = new Bundle();
                    if(salesConsutants.get(positionInner).getInfo().getName() != null && !salesConsutants.get(positionInner).getInfo().getName().equalsIgnoreCase("") ) {
                        bundle.putString("imgUrlEtvbr", salesConsutants.get(positionInner).getInfo().getDpUrl());
                        bundle.putString("dseNameEtvbr", salesConsutants.get(positionInner).getInfo().getName());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                    return true;
                }
            });
            if(DbUtils.isBike()){
                ((LinearLayout) ll.findViewById(R.id.visit_ll)).setVisibility(View.GONE);
                ((LinearLayout) ll.findViewById(R.id.retail_ll)).setVisibility(View.GONE);
                ((View) ll.findViewById(R.id.retail_view)).setVisibility(View.GONE);
                ((View) ll.findViewById(R.id.visit_view)).setVisibility(View.GONE);
            }else{
                ((LinearLayout) ll.findViewById(R.id.visit_ll)).setVisibility(View.VISIBLE);
                ((LinearLayout) ll.findViewById(R.id.retail_ll)).setVisibility(View.VISIBLE);
                ((View) ll.findViewById(R.id.retail_view)).setVisibility(View.VISIBLE);
                ((View) ll.findViewById(R.id.visit_view)).setVisibility(View.VISIBLE);
            }
            cardHolder.llinearLayout.addView(ll);
        }

    }

    @Override
    public int getItemCount() {
        if(locationResult.isValid()
                && locationResult.getSalesManagers().isValid()
                && locationResult.getSalesManagers().get(0).getTeamLeaders().isValid()) {
            return (locationResult.getSalesManagers().get(0).getTeamLeaders().size() > 0) ? locationResult.getSalesManagers().get(0).getTeamLeaders().size() + 1 : 0;
        }else{
            return 0;
        }
    }

    public class EtvbrSMCardHolder extends RecyclerView.ViewHolder {

        ImageView imgUser, imgUserCard;
        TextView tvNameCard, tvEnquiryCard, tvTestDriveCard, tvVisitCard, tvBookingCard, tvRetailCard, tvLostCard;
        TextView tvName, tvEnquiry, tvTestDrive, tvVisit, tvBooking, tvRetail;
        View viewEnquiryCard, viewTdCard, viewVisitCard, viewBookingCard, viewReatilCard, viewLostCard;
        View viewEnquiry, viewTd, viewVisit, viewBooking, viewReatil;
        LinearLayout llaLinearLayout, llinearLayout;
        LinearLayout llInner;
        TextView tvUserTotalCard;
        TextView tvUserTotal;
        CardView cardView;
        LinearLayout llEnquriyCard, llTdCard, llVisitCard, llBookingCard, llRetailsCard, llLostCard;
        TextView tvTargetTitleCard;
        TextView tvTargetEnquiyCard, tvTargetTDCard, tvTargetVisitCard, tvTargetBookingCard, tvTargetRetailCard;
        View viewTargetCard;
        RelativeLayout rlFrameLayoutCard;

        public EtvbrSMCardHolder(View itemView) {
            super(itemView);
            imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            tvName = (TextView) itemView.findViewById(R.id.text_user);
            tvEnquiry = (TextView) itemView.findViewById(R.id.txt_enquiry);
            tvTestDrive = (TextView) itemView.findViewById(R.id.txt_td);
            tvVisit = (TextView) itemView.findViewById(R.id.txt_visit);
            tvBooking = (TextView) itemView.findViewById(R.id.txt_booking);
            tvRetail = (TextView) itemView.findViewById(R.id.txt_retail);
            //llaLinearLayout = (LinearLayout) itemView.findViewById(R.id.ll_today_summary_main);
            tvUserTotal = (TextView) itemView.findViewById(R.id.text_user_total);
            viewEnquiry = (View) itemView.findViewById(R.id.enquiry_view);
            viewTd = (View) itemView.findViewById(R.id.td_view);
            viewVisit = (View) itemView.findViewById(R.id.visit_view);
            viewBooking = (View) itemView.findViewById(R.id.booking_view);
            viewReatil = (View) itemView.findViewById(R.id.retail_view);
            llinearLayout = (LinearLayout) itemView.findViewById(R.id.llinearLayout);
            llInner = (LinearLayout) itemView.findViewById(R.id.card_ll);

            imgUserCard = (ImageView) itemView.findViewById(R.id.img_user_card);
            tvNameCard = (TextView) itemView.findViewById(R.id.text_user_card);
            tvEnquiryCard = (TextView) itemView.findViewById(R.id.txt_enquiry_card);
            tvTestDriveCard = (TextView) itemView.findViewById(R.id.txt_td_card);
            tvVisitCard = (TextView) itemView.findViewById(R.id.txt_visit_card);
            tvBookingCard = (TextView) itemView.findViewById(R.id.txt_booking_card);
            tvRetailCard = (TextView) itemView.findViewById(R.id.txt_retail_card);
            tvUserTotalCard = (TextView) itemView.findViewById(R.id.text_user_total_card);
            viewEnquiryCard = (View) itemView.findViewById(R.id.enquiry_view_card);
            viewTdCard = (View) itemView.findViewById(R.id.td_view_card);
            viewVisitCard = (View) itemView.findViewById(R.id.visit_view_card);
            viewBookingCard = (View) itemView.findViewById(R.id.booking_view_card);
            viewReatilCard = (View) itemView.findViewById(R.id.retail_view_card);
            cardView = (CardView) itemView.findViewById(R.id.cardview_etvbr);
            llEnquriyCard = (LinearLayout) itemView.findViewById(R.id.enquiry_ll_card);
            tvTargetTitleCard = (TextView) itemView.findViewById(R.id.tv_target_title_card);
            tvTargetEnquiyCard = (TextView) itemView.findViewById(R.id.txt_target_enquiry_card);
            tvTargetTDCard = (TextView) itemView.findViewById(R.id.txt_target_td_card);
            tvTargetVisitCard = (TextView) itemView.findViewById(R.id.txt_target_visit_card);
            tvTargetBookingCard = (TextView) itemView.findViewById(R.id.txt_target_booking_card);
            tvTargetRetailCard = (TextView) itemView.findViewById(R.id.txt_target_retail_card);
            viewTargetCard = (View) itemView.findViewById(R.id.target_view_card);
            rlFrameLayoutCard = (RelativeLayout) itemView.findViewById(R.id.rl_frame_layout_card);
            llTdCard = (LinearLayout) itemView.findViewById(R.id.enquiry_td_card);
            llVisitCard = (LinearLayout) itemView.findViewById(R.id.enquiry_visit_card);
            llBookingCard = (LinearLayout) itemView.findViewById(R.id.enquiry_booking_card);
            llRetailsCard = (LinearLayout) itemView.findViewById(R.id.enquiry_retail_card);
            llLostCard = (LinearLayout) itemView.findViewById(R.id.enquiry_lost_card);
            tvLostCard = (TextView)itemView.findViewById(R.id.txt_lost_card);
            viewLostCard = (View) itemView.findViewById(R.id.lost_view_card);
        }
    }

    private void goToDetailsOfEtvbr(int position, String title_name, String user_id, String etvbrtotal, String user_role_id, String user_location_id) {

        Intent etvbrDetails =  null;

        switch (position){
            case 0:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Enquiries");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 1:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Test Drives");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 2:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Visits");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 3:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Bookings");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 4:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Retails");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;

            case 5:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Lost Drop");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
        }
        if(etvbrDetails != null)
            context.startActivity(etvbrDetails);
    }

}
