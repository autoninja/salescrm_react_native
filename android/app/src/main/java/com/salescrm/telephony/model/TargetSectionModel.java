package com.salescrm.telephony.model;

/**
 * Created by bannhi on 24/8/17.
 */

public class TargetSectionModel {

    String teamLeadName;
    int sectionPostion;

    public String getTeamLeadName() {
        return teamLeadName;
    }

    public void setTeamLeadName(String teamLeadName) {
        this.teamLeadName = teamLeadName;
    }

    public int getSectionPostion() {
        return sectionPostion;
    }

    public void setSectionPostion(int sectionPostion) {
        this.sectionPostion = sectionPostion;
    }
}
