package com.salescrm.telephony.activity.createEnquiry;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.activity.HomeActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.ActivitiesDB;
import com.salescrm.telephony.db.EnqSourceCategoryDB;
import com.salescrm.telephony.db.EnqSourceDB;
import com.salescrm.telephony.db.EnqSubInnerSourceDB;
import com.salescrm.telephony.db.EvaluationManagerResultData;
import com.salescrm.telephony.db.LocationsDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandModelsDB;
import com.salescrm.telephony.db.car.CarBrandsDB;
import com.salescrm.telephony.db.car.ExchangeCarBrandModelsDB;
import com.salescrm.telephony.db.car.ExchangeCarBrandsDB;
import com.salescrm.telephony.db.events.EventDB;
import com.salescrm.telephony.db.events.EventsLeadSourceCategoryDB;
import com.salescrm.telephony.db.events.EventsLeadSourceDB;
import com.salescrm.telephony.db.events.EventsMainDB;
import com.salescrm.telephony.db.teams.TeamLocationDB;
import com.salescrm.telephony.db.teams.TeamMainDB;
import com.salescrm.telephony.db.teams.TeamUserDetailsDB;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.interfaces.ReactNativeToAndroidConnect;
import com.salescrm.telephony.locationHelper.LocationHelper;
import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;


public class RNCreateLeadActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {

    private ReactInstanceManager mReactInstanceManager;
    private ReactRootView mReactRootView;
    private Preferences pref;
    private Realm realm;
    private String generatedLeadId;
    private ProgressDialog progressDialog;
    private boolean isReactViewRendered;
    private boolean wouldYouLikeToAddExchangeCar = false;
    private String leadLocationId ="";
    private IntentFilter intentFilter;
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals("ON_CREATE_LEAD_RNCreateLeadActivity")) {
                progressDialog.show();
                String leadId = intent.getStringExtra("leadId");
                String scheduledActivityId = intent.getStringExtra("scheduledActivityId");
                fetchC360(leadId);
                createAllotDseTask(leadId,
                        Util.getInt(scheduledActivityId),
                        intent.getStringExtra("firstName"),
                        intent.getStringExtra("lastName"),
                        intent.getStringExtra("title"),
                        intent.getStringExtra("leadTagId")
                );
            }else if(action != null && action.equals("ON_EXCHANGE_CAR_ADDED_RNCreateLeadActivity")){
                progressDialog.show();
                String leadId = intent.getStringExtra("leadId");
                String scheduledActivityId = intent.getStringExtra("scheduledActivityId");
                fetchC360AfterExchangeCar(leadId);
            }

        }
    };
    private LocationHelper locationHelper = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        mReactInstanceManager.onActivityResult(this, requestCode, resultCode, data);
        if (locationHelper != null) {
            locationHelper.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        Preferences.load(this);

        setContentView(R.layout.activity_rn_create_lead);
        Toolbar toolbar = findViewById(R.id.rn_create_lead_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Redirecting to C360 !!!.. Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        intentFilter = new IntentFilter();
        intentFilter.addAction("ON_CREATE_LEAD_RNCreateLeadActivity");
        intentFilter.addAction("ON_EXCHANGE_CAR_ADDED_RNCreateLeadActivity");

        if(getIntent().getExtras() != null){
            wouldYouLikeToAddExchangeCar = getIntent().getExtras().getBoolean("isExchangeCar");
        }

        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();

        if(pref.getLeadID()!= null && !pref.getLeadID().equalsIgnoreCase("") && realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst() != null)
        leadLocationId = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst().getLeadLocationId();



        Bundle initialProps = new Bundle();
        initialProps.putBoolean("fromAndroid", true);
        initialProps.putString("token", Preferences.getAccessToken());
        initialProps.putString("module", wouldYouLikeToAddExchangeCar ? "ADD_EXCHANGE_CAR":"CREATE_LEAD");
        initialProps.putInt("appUserId", pref.getAppUserId());
        initialProps.putString("primaryMobile", getIntent().getStringExtra("mobileNumber"));
        initialProps.putString("userLocations", new Gson().toJson(getLocationList()));
        initialProps.putString("allLocations", new Gson().toJson(getAllLocations()));
        initialProps.putString("interestedVehicle", new Gson().toJson(getInterestedVehicleModel()));
        initialProps.putString("allVehicles", new Gson().toJson(getAllVehicles()));
        initialProps.putString("enquirySourceMain", new Gson().toJson(getEnquirySourceMain()));
        initialProps.putString("enquirySourceCategories", new Gson().toJson(getEnquirySourceCategories()));

        initialProps.putBoolean("isClientTwoWheeler", DbUtils.isClientTwoWheeler() || pref.isTwoWheelerClient());
        initialProps.putBoolean("isShowEnquirySourceCategory", DbUtils.isShowLeadSourceCategory());

        initialProps.putString("activities", new Gson().toJson(getActivities()));
        initialProps.putString("teams", new Gson().toJson(getTeams()));
        initialProps.putString("activeEvents", new Gson().toJson(getActiveEvents()));
        initialProps.putString("govtAPIKeys", new Gson().toJson(pref.getGovtApiKeys()));
        initialProps.putString("evaluationManagers", new Gson().toJson(getEvaluationManager()));
        initialProps.putString("leadLocationId", leadLocationId);
        initialProps.putString("leadId", pref.getLeadID());
        initialProps.putString("HERE_MAP_APP_ID",pref.getHereMapAppId());
        initialProps.putString("HERE_MAP_APP_CODE", pref.getHereMapAppCode());
        initialProps.putInt("FIRST_CALL_ADVANCE_DAYS", DbUtils.getFirstCallAdvanceDate());
        mReactRootView = findViewById(R.id.react_root_view_create_lead);
        mReactRootView.startReactApplication(
                mReactInstanceManager,
                "NinjaCRMSales",
                initialProps
        );

        ReactNativeToAndroidConnect.setListener(new ReactNativeToAndroidConnect.ReactNativeToAndroidConnectListener() {
            @Override
            public void onRNComponentDidMount(String view) {
                if(view.equalsIgnoreCase("CREATE_LEAD")) {
                    isReactViewRendered = true;
                }
            }

            @Override
            public void onRNComponentWillUnMount(String view) {
                if(view.equalsIgnoreCase("CREATE_LEAD")) {
                    isReactViewRendered = false;
                }
            }
        });

        initLocation();

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }


    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
        try {
            registerReceiver(broadcastReceiver, new IntentFilter("ON_CREATE_LEAD_RNCreateLeadActivity"));
            registerReceiver(broadcastReceiver, new IntentFilter("RN_COMPONENT_DID_MOUNT"));
            registerReceiver(broadcastReceiver, new IntentFilter("RN_COMPONENT_WILL_UNMOUNT"));
            registerReceiver(broadcastReceiver, intentFilter);

        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
           // this.finish(); // close this activity and return to preview activity (if there is any)
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initLocation() {
        locationHelper = LocationHelper.getInstance(this, new CallBack() {
            @Override
            public void onCallBack() {

            }

            @Override
            public void onCallBack(Object data) {
                if (data instanceof Location) {
                    Location location = (Location) data;
                    new Handler(getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (mReactInstanceManager != null && mReactInstanceManager.getCurrentReactContext()!=null && isReactViewRendered) {
                                WritableMap params = Arguments.createMap();
                                params.putString("LATITUDE", String.valueOf(location.getLatitude()));
                                params.putString("LONGITUDE", String.valueOf(location.getLongitude()));
                                sendEvent(mReactInstanceManager.getCurrentReactContext(),
                                        "onGettingLocation", params);
                                locationHelper.abort();
                            }
                        }
                    });



                }
            }

            @Override
            public void onError(String error) {

            }
        }).listen();
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    private Locations[] getLocationList() {
        RealmResults<UserLocationsDB> data =
                realm.where(UserLocationsDB.class).distinct("location_id");
        Locations[] userLocations = new Locations[data != null ? data.size() : 0];
        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i) != null) {
                    userLocations[i] = new Locations(Util.getInt(data.get(i).getLocation_id()), data.get(i).getName());
                }
            }
        }

        return userLocations;

    }

    private Item[] getAllLocations() {
        RealmResults<LocationsDB> data =
                realm.where(LocationsDB.class).findAll();
        Item[] userLocations = new Item[data != null ? data.size() : 0];
        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i) != null) {
                    userLocations[i] = new Item(data.get(i).getId(), data.get(i).getName());
                }
            }
        }

        return userLocations;

    }

    private VehicleModel getInterestedVehicleModel() {
        UserDetails userDetails = realm.where(UserDetails.class).findFirst();
        if (userDetails != null && userDetails.getUserCarBrands().size() > 0) {
            CarBrandsDB brand = realm.where(CarBrandsDB.class).equalTo("brand_id", userDetails.getUserCarBrands().get(0).getBrand_id()).findFirst();
            if (brand != null) {
                List<VehicleModelsModel> models = new ArrayList<>();
                for (CarBrandModelsDB model : brand.getBrand_models()) {

                    if (model.getCategory().equalsIgnoreCase(WSConstants.CAR_CATEGORY_BOTH)) {

                        List<VehicleModelsVariantModel> variants = new ArrayList<>();
                        for (CarBrandModelVariantsDB variant : model.getCar_variants()) {
                            variants.add(new VehicleModelsVariantModel(
                                    Util.getInt(variant.getVariant_id()),
                                    variant.getVariant(),
                                    Util.getInt(variant.getFuel_type_id()),
                                    variant.getFuel_type(),
                                    Util.getInt(variant.getColor_id()),
                                    variant.getColor()));
                        }

                        models.add(new VehicleModelsModel(Util.getInt(model.getModel_id()),
                                model.getModel_name(), model.getCategory(), variants));
                    }
                }
                return new VehicleModel(Util.getInt(brand.getBrand_id()), brand.getBrand_name(), models);
            }

        }

        return null;
    }

    private List<VehicleModel> getAllVehicles() {
        List<VehicleModel> brands = new ArrayList<>();
        RealmResults<ExchangeCarBrandsDB> brandsDB = realm.where(ExchangeCarBrandsDB.class).findAll();
        System.out.println("BrandsDBLength:" + brandsDB.size());
        if (brandsDB != null) {
            for (ExchangeCarBrandsDB brand : brandsDB) {
                List<VehicleModelsModel> models = new ArrayList<>();
                for (ExchangeCarBrandModelsDB model : brand.getBrand_models()) {
                    if (model.getCategory().equalsIgnoreCase(WSConstants.CAR_CATEGORY_BOTH)
                            || model.getCategory().equalsIgnoreCase(WSConstants.CAR_CATEGORY_OLD_CAR)) {

                        List<VehicleModelsVariantModel> variants = new ArrayList<>();
                        for (CarBrandModelVariantsDB variant : model.getCar_variants()) {
                            variants.add(new VehicleModelsVariantModel(
                                    Util.getInt(variant.getVariant_id()),
                                    variant.getVariant(),
                                    Util.getInt(variant.getFuel_type_id()),
                                    variant.getFuel_type(),
                                    Util.getInt(variant.getColor_id()),
                                    variant.getColor()));
                        }

                        models.add(new VehicleModelsModel(Util.getInt(model.getModel_id()),
                                model.getModel_name(), model.getCategory(), variants));
                    }
                }
                brands.add(new VehicleModel(Util.getInt(brand.getBrand_id()), brand.getBrand_name(), models));
            }

        }
        return brands;
    }

    private List<Item> getEnquirySourceMain() {
        List<Item> enquirySources = new ArrayList<>();
        RealmResults<EnqSourceDB> realmList = realm.where(EnqSourceDB.class).findAll();
        for (EnqSourceDB enquirySource : realmList) {
            enquirySources.add(new Item(enquirySource.getId(), enquirySource.getName()));
        }
        return enquirySources;
    }

    private List<EnquiryCategory> getEnquirySourceCategories() {
        List<EnquiryCategory> enquirySourcesCategories = new ArrayList<>();
        RealmResults<EnqSourceCategoryDB> realmList = realm.where(EnqSourceCategoryDB.class).findAll();
        for (EnqSourceCategoryDB enqSourceCategoryDB : realmList) {
            List<Item> subEnquiries = new ArrayList<>();
            for (EnqSubInnerSourceDB subEnquiry : enqSourceCategoryDB.getSubLeadSources()) {
                subEnquiries.add(new Item(subEnquiry.getId(), subEnquiry.getName()));
            }
            enquirySourcesCategories.add(new EnquiryCategory(enqSourceCategoryDB.getId(), enqSourceCategoryDB.getName(), subEnquiries));

        }
        return enquirySourcesCategories;
    }

    private List<Item> getActivities() {
        List<Item> activities = new ArrayList<>();
        RealmResults<ActivitiesDB> realmList = realm.where(ActivitiesDB.class).findAll();
        for (ActivitiesDB activity : realmList) {
            activities.add(new Item(activity.getId(), activity.getName()));
        }
        return activities;
    }

    private List<TeamLocationModel> getTeams() {
        List<TeamLocationModel> teamsMainList = new ArrayList<>();
        RealmResults<TeamLocationDB> realmList = realm.where(TeamLocationDB.class).findAll();
        for (TeamLocationDB teamLocationDB : realmList) {
            List<TeamLocationModel.TeamModel> teamModels = new ArrayList<>();
            List<TeamLocationModel.UserModel> userModels = new ArrayList<>();

            RealmList<TeamMainDB> teamsDB = teamLocationDB.getTeams();
            for (TeamMainDB teamMainDB : teamsDB) {
                teamModels.add(new TeamLocationModel().new TeamModel(teamMainDB.getManager_id(),
                        teamMainDB.getTeam_leader_id(),
                        teamMainDB.getUser_id()));
            }

            RealmList<TeamUserDetailsDB> usersDB = teamLocationDB.getUsers();
            for (TeamUserDetailsDB teamUserDetailsDB : usersDB) {
                userModels.add(new TeamLocationModel().new UserModel(teamUserDetailsDB.getId(),
                        teamUserDetailsDB.getName(),
                        teamUserDetailsDB.getMobile_number(),
                        teamUserDetailsDB.getRole_id()));
            }


            teamsMainList.add(new TeamLocationModel(teamLocationDB.getLocation_id(), teamModels, userModels));
        }
        return teamsMainList;
    }

    private List<EventsMain> getActiveEvents() {
        List<EventsMain> activeEvents = new ArrayList<>();

        RealmResults<EventsMainDB> eventsMainDBS = realm.where(EventsMainDB.class).findAll();
        for (EventsMainDB eventsMainDB : eventsMainDBS) {
            List<EventsMain.EventsLeadSourceCategory> eventsLeadSourceCategories = new ArrayList<>();
            for (EventsLeadSourceCategoryDB eventsLeadSourceCategoryDB :
                    eventsMainDB.getEventsLeadSourceCategoryDBS()) {
                List<EventsMain.EventsLeadSource> eventsLeadSources = new ArrayList<>();

                for (EventsLeadSourceDB eventsLeadSourceDB :
                        eventsLeadSourceCategoryDB.getEventsLeadSourceDBS()) {
                    List<EventsMain.Event> events = new ArrayList<>();
                    for (EventDB eventDB : eventsLeadSourceDB.getEvents()) {
                        events.add(new EventsMain().new Event(eventDB.getId(),
                                eventDB.getName(),
                                eventDB.getStart_date(),
                                eventDB.getEnd_date()));
                    }
                    eventsLeadSources.add(new EventsMain().new EventsLeadSource(eventsLeadSourceDB.getId(),
                            eventsLeadSourceDB.getName(), events));

                }
                eventsLeadSourceCategories.add(
                        new EventsMain().new EventsLeadSourceCategory(
                                eventsLeadSourceCategoryDB.getId(),
                                eventsLeadSourceCategoryDB.getName(),
                                eventsLeadSources
                        ));
            }
            activeEvents.add(new EventsMain(eventsMainDB.getLocationId(),
                    eventsMainDB.getLocationName(),
                    eventsLeadSourceCategories));
        }

        return activeEvents;
    }

    private List<EvaluationManager> getEvaluationManager(){
        List<EvaluationManager> evaluationManagers = new ArrayList<>();
        RealmResults<EvaluationManagerResultData> evaluationManagerResultData = realm.where(EvaluationManagerResultData.class).findAll();
        for(int i =0; i< evaluationManagerResultData.size(); i++){
            evaluationManagers.add(new EvaluationManager(evaluationManagerResultData.get(i).getLocationId(), evaluationManagerResultData.get(i).isHasEvaluationManager()));
        }
        return evaluationManagers;
    }

    private void fetchC360(String leadId) {
        progressDialog.setMessage("Redirecting to C360 !!!.. Please wait");
        progressDialog.show();
        this.generatedLeadId = leadId;

        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(leadId);
        new FetchC360InfoOnCreateLead(new FetchC360OnCreateLeadListener() {
            @Override
            public void onFetchC360OnCreateLeadSuccess() {
                progressDialog.dismiss();
                Preferences.setLeadID("" + generatedLeadId);
                SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", Util.getInt(generatedLeadId)).findFirst();
                if (salesCRMRealmTable != null) {
                    Preferences.setScheduledActivityId(salesCRMRealmTable.getScheduledActivityId());

                }

                startActivity(new Intent(RNCreateLeadActivity.this, C360Activity.class));
                RNCreateLeadActivity.this.finish();
            }

            @Override
            public void onFetchC360OnCreateLeadError() {
                progressDialog.dismiss();
                Util.showToast(getApplicationContext(), "Failed to fetch C360 information", Toast.LENGTH_SHORT);
                RNCreateLeadActivity.this.finish();

            }
        }, getApplicationContext(), leadData, pref.getAppUserId()).call(true);

    }

    private void fetchC360AfterExchangeCar(String leadId) {
        progressDialog.setMessage("Redirecting to C360 !!!.. Please wait");
        progressDialog.show();
        this.generatedLeadId = leadId;

        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(leadId);
        new FetchC360InfoOnCreateLead(new FetchC360OnCreateLeadListener() {
            @Override
            public void onFetchC360OnCreateLeadSuccess() {
                progressDialog.dismiss();
                Preferences.setLeadID("" + generatedLeadId);
                SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", Util.getInt(generatedLeadId)).findFirst();
                if (salesCRMRealmTable != null) {
                    Preferences.setScheduledActivityId(salesCRMRealmTable.getScheduledActivityId());

                }

                startActivity(new Intent(RNCreateLeadActivity.this, HomeActivity.class));
                RNCreateLeadActivity.this.finish();
            }

            @Override
            public void onFetchC360OnCreateLeadError() {
                progressDialog.dismiss();
                Util.showToast(getApplicationContext(), "Failed to fetch C360 information", Toast.LENGTH_SHORT);
                RNCreateLeadActivity.this.finish();

            }
        }, getApplicationContext(), leadData, pref.getAppUserId()).call(true);

    }

    private void createAllotDseTask(String leadId, Integer scheduled_activity_id,
                                    String firsName, String lastName,
                                    String title,
                                    String leadTagId) {
        realm.beginTransaction();
        SalesCRMRealmTable salesCRMRealmTable = new SalesCRMRealmTable();
        salesCRMRealmTable.setScheduledActivityId(scheduled_activity_id);
        salesCRMRealmTable.setLeadId(Long.parseLong(leadId));
        salesCRMRealmTable.setFirstName(firsName);
        salesCRMRealmTable.setLastName(lastName);
        salesCRMRealmTable.setTitle(title);
        salesCRMRealmTable.setActivityId(WSConstants.TaskActivityName.ALLOT_DSE_ID);
        salesCRMRealmTable.setActivityScheduleDate(Util.getDateTime(0));
        salesCRMRealmTable.setActivityName(WSConstants.TaskActivityName.ALLOT_DSE);
        salesCRMRealmTable.setIsLeadActive(1);
        salesCRMRealmTable.setLeadTagsName(leadTagId);
        salesCRMRealmTable.setDone(true);
        salesCRMRealmTable.setSync(true);
        realm.copyToRealmOrUpdate(salesCRMRealmTable);
        realm.commitTransaction();

    }

    private class Locations {
        private int id;
        private String text;

        public Locations(int id, String text) {
            this.id = id;
            this.text = text;
        }
    }

    class VehicleModel {
        int brand_id;
        String brand_name;
        List<VehicleModelsModel> brand_models;

        VehicleModel(int brand_id, String brand_name, List<VehicleModelsModel> brand_models) {
            this.brand_id = brand_id;
            this.brand_name = brand_name;
            this.brand_models = brand_models;
        }
    }

    class VehicleModelsModel {
        int model_id;
        String model_name;
        String category;
        List<VehicleModelsVariantModel> car_variants;

        VehicleModelsModel(int model_id, String model_name, String category,
                           List<VehicleModelsVariantModel> car_variants) {
            this.model_id = model_id;
            this.model_name = model_name;
            this.category = category;
            this.car_variants = car_variants;
        }
    }

    class VehicleModelsVariantModel {
        int variant_id;
        String variant;
        int fuel_type_id;
        String fuel_type;
        int color_id;
        String color;

        VehicleModelsVariantModel(int variant_id, String variant,
                                  int fuel_type_id, String fuel_type, int color_id, String color) {
            this.variant_id = variant_id;
            this.variant = variant;
            this.fuel_type_id = fuel_type_id;
            this.fuel_type = fuel_type;
            this.color_id = color_id;
            this.color = color;
        }
    }

    class Item {
        int id;
        String name;

        public Item(int id, String name) {
            this.id = id;
            this.name = name;
        }
    }

    class EnquiryCategory {
        int id;
        String name;
        List<Item> subEnquirySource;

        public EnquiryCategory(int id, String name, List<Item> subEnquirySource) {
            this.id = id;
            this.name = name;
            this.subEnquirySource = subEnquirySource;
        }
    }

    class TeamLocationModel {
        int location_id;
        List<TeamModel> teams;
        List<UserModel> users;

        public TeamLocationModel() {
        }

        TeamLocationModel(int location_id, List<TeamModel> teams, List<UserModel> users) {
            this.location_id = location_id;
            this.teams = teams;
            this.users = users;
        }

        class TeamModel {
            int manager_id;
            int team_leader_id;
            int user_id;

            TeamModel(int manager_id, int team_leader_id, int user_id) {
                this.manager_id = manager_id;
                this.team_leader_id = team_leader_id;
                this.user_id = user_id;
            }
        }

        class UserModel {
            int id;
            String name;
            String mobile_number;
            int role_id;

            UserModel(int id, String name, String mobile_number, int role_id) {
                this.id = id;
                this.name = name;
                this.mobile_number = mobile_number;
                this.role_id = role_id;
            }
        }
    }

    class EventsMain {
        private int locationId;
        private String locationName;
        private List<EventsLeadSourceCategory> eventsLeadSourceCategories;

        public EventsMain() {
        }

        public EventsMain(int locationId, String locationName, List<EventsLeadSourceCategory> eventsLeadSourceCategories) {
            this.locationId = locationId;
            this.locationName = locationName;
            this.eventsLeadSourceCategories = eventsLeadSourceCategories;
        }

        class EventsLeadSourceCategory {
            private int id;
            private String name;
            private List<EventsLeadSource> leadSources;

            public EventsLeadSourceCategory(int id, String name, List<EventsLeadSource> leadSources) {
                this.id = id;
                this.name = name;
                this.leadSources = leadSources;
            }
        }

        class EventsLeadSource {
            private int id;
            private String name;
            private List<Event> events;

            public EventsLeadSource(int id, String name, List<Event> events) {
                this.id = id;
                this.name = name;
                this.events = events;
            }
        }

        class Event {
            private int id;
            private String name;
            private String start_date;
            private String end_date;

            public Event(int id, String name, String start_date, String end_date) {
                this.id = id;
                this.name = name;
                this.start_date = start_date;
                this.end_date = end_date;
            }
        }
    }

    class EvaluationManager{
        private String locationId;
        private boolean hasEvaluationManager;

        public EvaluationManager(String locationId, boolean hasEvaluationManager) {
            this.locationId = locationId;
            this.hasEvaluationManager = hasEvaluationManager;
        }
    }


}
