package com.salescrm.telephony.db.team_dashboard_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by prateek on 11/6/17.
 */

public class Error extends RealmObject {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("details")
    @Expose
    private String details;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
