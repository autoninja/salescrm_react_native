// store.js

import { createStore, combineReducers } from 'redux';
import teamDashboardFiltersReducer from './reducers/team_dashboard_filters_reducer';
import gameIntroReducer from './reducers/game_intro_reducer';
import mainHeaderReducer from './reducers/main_header_reducer';
import eventUpdateReducer from './reducers/event_update_reducer';
import closeRecordings from './reducers/close_recordings';
import tasksListReducer from './reducers/tasks_list_reducer'

const rootReducer = combineReducers({
  updateTeamDashboard: teamDashboardFiltersReducer,
  gameIntro: gameIntroReducer,
  updateMainHeaderCurrentView: mainHeaderReducer,
  updateEventDasboard: eventUpdateReducer,
  closeRecodringPlayer: closeRecordings,
  tasksListReducer: tasksListReducer
});

const configureStore = () => {
  return createStore(rootReducer);
}

export default configureStore;
