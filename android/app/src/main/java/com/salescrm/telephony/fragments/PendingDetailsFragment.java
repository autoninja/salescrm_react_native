package com.salescrm.telephony.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.PendingDetailsAdapter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.AnalyticsDB;
import com.salescrm.telephony.db.PendingStatsDB;
import com.salescrm.telephony.db.WeeklyData;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.TeamDashboardSwipedListener;
import com.salescrm.telephony.model.Locations;
import com.salescrm.telephony.model.PendingStatsResponse;
import com.salescrm.telephony.model.TeamDashboardSwiped;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

import static com.salescrm.telephony.fragments.PendingFragment.adapter;

/**
 * Created by bannhi on 12/9/17.
 */

public class PendingDetailsFragment extends Fragment implements TeamDashboardSwipedListener {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private int sectionNumber;
    LinearLayout max_pending_lyt, analytics_lyt,noteLL;
    LinearLayout max_pending_button, analytics_lyt_button;
    PendingDetailsAdapter pendingDetailsAdapter;
    TextView todays_time_tv, pending_till_now_tv, diff_pending_tv, dialog_view_1;
    View view1, view2, view3, view4;
    TextView weekCount1, weekCount2, weekCount3, weekCount4;
    RelativeLayout rll_1, rll_2, rll_3, rll_4;
    int week1Count, week2Count, week3Count, week4Count;
    RecyclerView recyclerView;
    static private RelativeLayout rel_loading_frame;
    ImageView pendingIcon, analyticsIcon;
    TextView pendingText, analyticsText;
    static int TAB = 0;
    Preferences pref;
    Realm realm;
    ArrayList<String> Locations = new ArrayList<String>();
    private ConnectionDetectorService connectionDetectorService;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView pending_date_tv;
    RealmList<PendingStatsDB> pendingStatsResults = new RealmList<PendingStatsDB>();
    AnalyticsDB analyticsDBResults = new AnalyticsDB();

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * Activity's lifecycle.
     */
    @Override
    public void onResume() {
        displayData();
        super.onResume();
    }

    public static PendingDetailsFragment newInstance(int sectionNumber, int tab) {
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        PendingDetailsFragment fragment = new PendingDetailsFragment();
        fragment.setArguments(args);
        TAB = tab;
        return fragment;
    }

    public PendingDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        System.out.println("ON CREATE IS CALLED");
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        System.out.println("ON CREATE VIEW IS CALLED");
        View rootView = inflater.inflate(R.layout.pending_main_fragment, container, false);
        sectionNumber = getArguments().getInt(ARG_SECTION_NUMBER);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        /*realm = Realm.getDefaultInstance();*/
        connectionDetectorService = new ConnectionDetectorService(getActivity());
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewPending);
        rel_loading_frame = (RelativeLayout) rootView.findViewById(R.id.rel_loading_frame);
        max_pending_lyt = (LinearLayout) rootView.findViewById(R.id.max_pending_lyt);
        analytics_lyt = (LinearLayout) rootView.findViewById(R.id.analytics_lyt);
        pending_date_tv = (TextView) rootView.findViewById(R.id.date);
        pendingIcon = (ImageView) rootView.findViewById(R.id.pending_icon);
        analyticsIcon = (ImageView) rootView.findViewById(R.id.analytics_icon);
        pendingText = (TextView) rootView.findViewById(R.id.pending_text);
        analyticsText = (TextView) rootView.findViewById(R.id.analytics_text);
        max_pending_button = (LinearLayout) rootView.findViewById(R.id.leftll);
        analytics_lyt_button = (LinearLayout) rootView.findViewById(R.id.rightll);
        noteLL = (LinearLayout) rootView.findViewById(R.id.note);
        Calendar calendar = Calendar.getInstance();
        pending_date_tv.setText("" + new SimpleDateFormat("dd MMM").format(calendar.getTime()));
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (new ConnectionDetectorService(getActivity()).isConnectingToInternet()) {
                    fetchPendingStats(getActivity(), true);
                    rel_loading_frame.setVisibility(View.VISIBLE);
                    max_pending_lyt.setVisibility(View.GONE);
                    //noteLL.setVisibility(View.GONE);
                    analytics_lyt.setVisibility(View.GONE);
                } else {
                    Util.showToast(getContext(), "No internet connection", Toast.LENGTH_SHORT);
                    setSwipeRefreshView(false);
                }
            }
        });
        ///////////////////for analytics//////////////////////
        todays_time_tv = (TextView) analytics_lyt.findViewById(R.id.today_time);
        pending_till_now_tv = (TextView) analytics_lyt.findViewById(R.id.pending_now_no);
        diff_pending_tv = (TextView) analytics_lyt.findViewById(R.id.differnce_pending);
        dialog_view_1 = (TextView) analytics_lyt.findViewById(R.id.dialog_view_1);

        rll_1 = (RelativeLayout) analytics_lyt.findViewById(R.id.rll_1);
        rll_2 = (RelativeLayout) analytics_lyt.findViewById(R.id.rll_2);
        rll_3 = (RelativeLayout) analytics_lyt.findViewById(R.id.rll_3);
        rll_4 = (RelativeLayout) analytics_lyt.findViewById(R.id.rll_4);

        view1 = rll_1.findViewById(R.id.bar1);
        view2 = rll_2.findViewById(R.id.bar2);
        view3 = rll_3.findViewById(R.id.bar3);
        view4 = rll_4.findViewById(R.id.bar4);

        weekCount1 = (TextView) rll_1.findViewById(R.id.count1);
        weekCount2 = (TextView) rll_2.findViewById(R.id.count2);
        weekCount3 = (TextView) rll_3.findViewById(R.id.count3);
        weekCount4 = (TextView) rll_4.findViewById(R.id.count4);
        /////////////////////////////////////
        setTab();
        max_pending_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logEvent("Pending Team");
                WSConstants.LAST_PENDING_TAB = 0;
               // WSConstants.LAST_PENDING_TAB = TAB;
                displayData();
                max_pending_lyt.setVisibility(View.VISIBLE);
                //noteLL.setVisibility(View.VISIBLE);
                analytics_lyt.setVisibility(View.GONE);
                pendingIcon.setImageResource(R.drawable.pending_selected);
                analyticsIcon.setImageResource(R.drawable.analytics);
                pendingText.setTextColor(Color.parseColor("#FF1E3B64"));
                analyticsText.setTextColor(Color.parseColor("#FF606060"));
                pendingText.setTextSize(13);
                analyticsText.setTextSize(11);

            }
        });
        analytics_lyt_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //WSConstants.LAST_PENDING_TAB = 0;

                logEvent("Pending Analytics");
                WSConstants.LAST_PENDING_TAB = 1;

               // WSConstants.LAST_PENDING_TAB = TAB;
                displayData();
                max_pending_lyt.setVisibility(View.GONE);
               // noteLL.setVisibility(View.GONE);
                analytics_lyt.setVisibility(View.VISIBLE);
                pendingIcon.setImageResource(R.drawable.pending);
                analyticsIcon.setImageResource(R.drawable.analytics_selected);
                pendingText.setTextColor(Color.parseColor("#FF606060"));
                analyticsText.setTextColor(Color.parseColor("#FF1E3B64"));
                pendingText.setTextSize(11);
                analyticsText.setTextSize(13);

            }
        });
        return rootView;

    }

    private void setSwipeRefreshView(final boolean val) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(val);
            }
        });
    }

    private void setTab() {
        if (WSConstants.LAST_PENDING_TAB == 0) {
            max_pending_lyt.setVisibility(View.VISIBLE);
           // noteLL.setVisibility(View.VISIBLE);
            analytics_lyt.setVisibility(View.GONE);
            pendingIcon.setImageResource(R.drawable.pending_selected);
            analyticsIcon.setImageResource(R.drawable.analytics);
            pendingText.setTextColor(Color.parseColor("#FF1E3B64"));
            analyticsText.setTextColor(Color.parseColor("#FF606060"));
            pendingText.setTextSize(13);
            analyticsText.setTextSize(11);
        } else {
            max_pending_lyt.setVisibility(View.GONE);
          //  noteLL.setVisibility(View.GONE);
            analytics_lyt.setVisibility(View.VISIBLE);
            pendingIcon.setImageResource(R.drawable.pending);
            analyticsIcon.setImageResource(R.drawable.analytics_selected);
            pendingText.setTextColor(Color.parseColor("#FF606060"));
            analyticsText.setTextColor(Color.parseColor("#FF1E3B64"));
            pendingText.setTextSize(11);
            analyticsText.setTextSize(13);
        }

    }

    private void displayData() {
        getAnalyticsStats();
        getPendingOfDSEs();
        if (rel_loading_frame!=null) {
            rel_loading_frame.setVisibility(View.GONE);
        }
            if (WSConstants.LAST_PENDING_TAB == 0) {
                recyclerView.setHasFixedSize(true);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
                recyclerView.setLayoutManager(mLayoutManager);
                pendingDetailsAdapter = new PendingDetailsAdapter(getActivity(), pendingStatsResults);
                recyclerView.setAdapter(pendingDetailsAdapter);
                pendingDetailsAdapter.notifyDataSetChanged();
            } else {
                for (int i = 0; i < analyticsDBResults.getWeeklyData().size(); i++) {
                    switch (analyticsDBResults.getWeeklyData().get(i).getWeekNo()) {
                        case 1:
                            week1Count = analyticsDBResults.getWeeklyData().get(i).getAvgPending();
                            weekCount1.setText("" + week1Count);
                            break;
                        case 2:
                            week2Count = analyticsDBResults.getWeeklyData().get(i).getAvgPending();
                            weekCount2.setText("" + week2Count);
                            break;
                        case 3:
                            week3Count = analyticsDBResults.getWeeklyData().get(i).getAvgPending();
                            weekCount3.setText("" + week3Count);
                            break;
                        case 4:
                            week4Count = analyticsDBResults.getWeeklyData().get(i).getAvgPending();
                            weekCount4.setText("" + week4Count);
                            break;
                    }
                }

                int arrWeeks[] = {week1Count, week2Count, week3Count, week4Count};

                int gcd = Util.findGCD(arrWeeks);
                int arrWeeksAvg[] = {week1Count / gcd, week2Count / gcd, week3Count / gcd, week4Count / gcd};

                int largest = Util.getLargestNumber(arrWeeksAvg);
                System.out.println("GCD" + gcd + "LARGEST" + largest);

                pending_till_now_tv.setText(("" + analyticsDBResults.getTodayCount()).toString());
                dialog_view_1.setText(("It was " + analyticsDBResults.getLastWkCount() + " last").toString());

                int diff = analyticsDBResults.getLastWkCount() - analyticsDBResults.getTodayCount();
                if (diff > 0) {
                    // up arrow
                    diff_pending_tv.setText(("" + diff).toString());
                } else {
                    //down arrow
                    diff_pending_tv.setText(("" + Math.abs(diff)).toString());
                    diff_pending_tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.up_arrao, 0, 0, 0);
                }
                RelativeLayout.LayoutParams rllParams1 = (RelativeLayout.LayoutParams) view1.getLayoutParams();
               // rllParams1.height = (480 / largest) * (week1Count / gcd);
                if(largest == week1Count){
                    rllParams1.height = 480;
                }else if(week1Count==0){
                    rllParams1.height = 0;
                }else{
                    rllParams1.height = (480 * week1Count)/largest;
                }
                view1.setLayoutParams(rllParams1);

                RelativeLayout.LayoutParams rllParams2 = (RelativeLayout.LayoutParams) view2.getLayoutParams();
               // rllParams2.height = (480 / largest) * (week2Count / gcd);
                if(largest == week2Count){
                    rllParams2.height = 480;
                }else if(week2Count==0){
                    rllParams2.height = 0;
                }else{
                    rllParams2.height = (480 * week2Count)/largest;
                }
                view2.setLayoutParams(rllParams2);

                RelativeLayout.LayoutParams rllParams3 = (RelativeLayout.LayoutParams) view3.getLayoutParams();
               // rllParams3.height = (480 / largest) * (week3Count / gcd);
                if(largest == week3Count){
                    rllParams3.height = 480;
                }else if(week3Count==0){
                    rllParams3.height = 0;
                }else{
                    rllParams3.height = (480 * week3Count)/largest;
                }
                view3.setLayoutParams(rllParams3);

                RelativeLayout.LayoutParams rllParams4 = (RelativeLayout.LayoutParams) view4.getLayoutParams();
               // rllParams4.height = (480 / largest) * (week4Count / gcd);
                if(largest == week4Count){
                    rllParams4.height = 480;
                }else{
                    rllParams4.height = (480 * week4Count)/largest;
                }
                view4.setLayoutParams(rllParams4);


            }

    }


    /**
     * getting all dses' pending details
     *
     * @return
     */
    public void getPendingOfDSEs() {
        pendingStatsResults = new RealmList<PendingStatsDB>();
        realm = Realm.getDefaultInstance();
        if(sectionNumber<WSConstants.Locations.size()) {
            RealmResults<PendingStatsDB> results = realm.where(PendingStatsDB.class)
                    .equalTo("locId", WSConstants.Locations.get(sectionNumber).getId())
                    .findAll();
            pendingStatsResults.addAll(results.subList(0, results.size()));
        }

    }

    /**
     * getting all analytics details
     *
     * @return
     */
    public void getAnalyticsStats() {
        analyticsDBResults = new AnalyticsDB();
        realm = Realm.getDefaultInstance();
        int locationId = 0;
        if(WSConstants.Locations.size()>0){
           locationId  = WSConstants.Locations.get(sectionNumber).getId();
        }
        analyticsDBResults = realm.where(AnalyticsDB.class)
                .equalTo("locId",locationId)
                .findFirst();
    }

    public void fetchPendingStats(Activity activity, final boolean pulled) {
        System.out.println("FETCHH CALLED");
        pref = Preferences.getInstance();
        pref.load(activity);
        final Realm realm = Realm.getDefaultInstance();
        connectionDetectorService = new ConnectionDetectorService(activity);
        if (connectionDetectorService.isConnectingToInternet()) {
            if (rel_loading_frame != null) {
                rel_loading_frame.setVisibility(View.VISIBLE);
            }

            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getPendingStats(new Callback<PendingStatsResponse>() {
                @Override
                public void success(PendingStatsResponse pendingStatsResponse, Response response) {
                    if(!Util.isFragmentSafe(PendingDetailsFragment.this)){
                        return;
                    }
                    if (pendingStatsResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        addtoDB(pendingStatsResponse.getResult(), realm, pulled);
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }
                        }

                    }else{
                        Util.showToast(getContext(), ""+ pendingStatsResponse.getMessage(), Toast.LENGTH_SHORT);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if(error.getResponse()!= null) {
                        Util.showToast(getContext(), "Server not responding - " + error.getResponse().getStatus(), Toast.LENGTH_SHORT);
                    }
                }
            });
        } else {
        }

    }

    public void addtoDB(final PendingStatsResponse.Result[] result, Realm realm, final boolean pulled) {
        WSConstants.Locations.clear();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(PendingStatsDB.class);
                realm.delete(AnalyticsDB.class);
                realm.delete(WeeklyData.class);
                for (int i = 0; i < result.length; i++) {
                    PendingStatsResponse.Result resultObj = result[i];
                    WSConstants.Locations.add(new Locations(resultObj.getLocation_id(), resultObj.getLocation_name()));
                    for (int j = 0; j < resultObj.getUsers().length; j++) {
                        PendingStatsDB pendingStatus = new PendingStatsDB();
                        pendingStatus.setLocId(resultObj.getLocation_id());
                        pendingStatus.setLocName(resultObj.getLocation_name());
                        PendingStatsResponse.Users usersObj = resultObj.getUsers()[j];
                        pendingStatus.setUserId(usersObj.getId());
                        pendingStatus.setTeam_member(usersObj.getTeam_member());
                        pendingStatus.setUserName(usersObj.getName());
                        pendingStatus.setUserImage(usersObj.getImage());
                        pendingStatus.setVisibleTimes(usersObj.getVisible_times());
                        pendingStatus.setMedianDelay(usersObj.getMedian_delay());
                        pendingStatus.setPendingCount(usersObj.getPending_count());
                        pendingStatus.setPendingCountDiff(usersObj.getPending_count_difference());
                        if(usersObj.getCalls()!=null){
                            for (int k = 0; k < usersObj.getCalls().length; k++) {
                                if (("" + usersObj.getCalls()[k].getRole_id()).equalsIgnoreCase(WSConstants.TEAM_LEAD_ROLE_ID)) {
                                    pendingStatus.setTlName(usersObj.getCalls()[k].getName());
                                    pendingStatus.setTlImage(usersObj.getCalls()[k].getImage());
                                    pendingStatus.setTlNum(usersObj.getCalls()[k].getPhone());
                                } else if (("" + usersObj.getCalls()[k].getRole_id()).equalsIgnoreCase(WSConstants.MANAGER_ROLE_ID)) {
                                    pendingStatus.setManagerName(usersObj.getCalls()[k].getName());
                                    pendingStatus.setManagerImage(usersObj.getCalls()[k].getImage());
                                    pendingStatus.setManagerNum(usersObj.getCalls()[k].getPhone());
                                } else if (("" + usersObj.getCalls()[k].getRole_id()).equalsIgnoreCase(WSConstants.DSE_ROLE_ID)) {
                                    pendingStatus.setUserPhone(usersObj.getCalls()[k].getPhone());
                                }
                            }
                        }

                        realm.copyToRealm(pendingStatus);

                    }


                    AnalyticsDB analyticsDB = new AnalyticsDB();
                    analyticsDB.setLocId(resultObj.getLocation_id());
                    analyticsDB.setTodayCount(resultObj.getStats().getToday_count());
                    analyticsDB.setLastWkCount(resultObj.getStats().getLast_week_count());

                    RealmList<WeeklyData> weeklyDataList = new RealmList<WeeklyData>();
                    for (int x = 0; x < resultObj.getStats().getWeekly_data().length; x++) {
                        PendingStatsResponse.Weekly_data weeklyObj = resultObj.getStats().getWeekly_data()[x];
                        WeeklyData weeklyDataDB = new WeeklyData();
                        weeklyDataDB.setAvgPending(weeklyObj.getAvg_pending());
                        weeklyDataDB.setWeekNo(weeklyObj.getWeek_number());
                        weeklyDataList.add(weeklyDataDB);
                    }
                    analyticsDB.setWeeklyData(weeklyDataList);
                    realm.copyToRealm(analyticsDB);

                }
                if (rel_loading_frame != null) {
                    rel_loading_frame.setVisibility(View.GONE);
                    //displayData();

                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }

                if(pulled){
                    setSwipeRefreshView(false);
                    setTab();
                    displayData();
                }
            }
        });

    }

    private void logEvent(String event){
       /* if(Answers.getInstance()!=null) {
            Answers.getInstance().logCustom(new CustomEvent("Team Dashboard")
                    .putCustomAttribute("Dealer Name", pref.getDealerName())
                    .putCustomAttribute("IMEI", pref.getImeiNumber())
                    .putCustomAttribute("User Name", pref.getUserName())
                    .putCustomAttribute("Dashboard Type", event)
                    .putCustomAttribute("Roles",   DbUtils.getRolesCombination()));

        }*/
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, event);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
    }



    private TeamDashboardSwiped teamDashboardSwiped = TeamDashboardSwiped.getInstance(this,2);

    @Override
    public void onTeamDashboardSwiped(int position,int tabCount) {
        System.out.println("TeamDashboard::"+tabCount);
        if(position == 1&&tabCount == 2){
            position=2;
        }
        if(position == 2) {
            if (WSConstants.LAST_PENDING_TAB == 0) {
                logEvent("Pending Team");
                System.out.println("TeamDashboard Pending Team");
            }
            else {
                logEvent("Pending Analytics");
                System.out.println("TeamDashboard Pending Analytics");
            }
        }
    }

}
