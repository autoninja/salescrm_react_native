package com.salescrm.telephony.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.CalendarDialogActivity;
import com.salescrm.telephony.adapter.EtvbrGmSmCardAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.db.etvbr_location.SalesManager;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.preferences.Preferences;

import java.util.ArrayList;
import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by prateek on 14/7/17.
 */

public class EtvbrGmSmFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView tvEnquiry, tvTestDrive, tvV, tvBooking, tvR;
    private Switch swtchSwitch;
    public static boolean PERCENT = false;
    private Preferences pref;
    private HashMap<String, String> hashMap;
    private RelativeLayout rel_loading_frame;
    private Realm realm;
    private ImageView tvDateIcon;
    private TextView tvDate;
    private ArrayList<String> teamIds;
    private SwipeRefreshLayout swipeRefreshLayout;
    private EtvbrGmSmCardAdapter etvbrGmSmCardAdpter;
    private RealmResults<SalesManager> resultSM;
    //private RealmResults<Location> locationsResult;
    private Location locationsResult;
    private ImageView filterImage;

    public static EtvbrGmSmFragment newInstance(int i, String etvbr) {
        Bundle args = new Bundle();
        args.putInt("sm_id", i);
        args.putString("someTitle", etvbr);
        EtvbrGmSmFragment fragment = new EtvbrGmSmFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.etvbr_fragment, container, false);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        mRecyclerView = (RecyclerView) rView.findViewById(R.id.recyclerViewEtvbr);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        tvEnquiry = (TextView) rView.findViewById(R.id.tv_enq);
        tvTestDrive = (TextView) rView.findViewById(R.id.tv_td);
        tvV = (TextView) rView.findViewById(R.id.tv_v);
        tvBooking = (TextView) rView.findViewById(R.id.tv_b);
        tvR = (TextView) rView.findViewById(R.id.tv_r);
        tvDate = (TextView) rView.findViewById(R.id.date_date);
        swtchSwitch = (Switch) rView.findViewById(R.id.swtch_etvbr);
        tvDate.setVisibility(View.VISIBLE);
        tvDate.setText(pref.getShowDateEtvbr());

        hashMap = new HashMap<>();
        hashMap.put("", "");
        pref.setTeamId("19");
        rel_loading_frame = (RelativeLayout) rView.findViewById(R.id.rel_loading_frame);
        tvDateIcon = (ImageView) rView.findViewById(R.id.date_icon);
        tvDateIcon.setVisibility(View.INVISIBLE);
        filterImage = (ImageView) rView.findViewById(R.id.filter_etvbr);
        filterImage.setVisibility(View.INVISIBLE);
        swipeRefreshLayout = (SwipeRefreshLayout) rView.findViewById(R.id.swipeContainer);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                etvbrGmSmCardAdpter.notifyDataSetChanged();
                setSwipeRefreshView(false);

            }
        });
        tvDateIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CalendarDialogActivity.class);
                startActivity(intent);
            }
        });

        setUpAdapter();

        PERCENT = false;

        if(DbUtils.isBike()){
            tvR.setVisibility(View.GONE);
            tvV.setVisibility(View.GONE);
        }else {
            tvR.setVisibility(View.VISIBLE);
            tvV.setVisibility(View.VISIBLE);
        }
        swtchSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                System.out.println("Swiched: "+isChecked);
                if(DbUtils.isBike()){
                    tvR.setVisibility(View.GONE);
                    tvV.setVisibility(View.GONE);
                    if (isChecked) {
                        PERCENT = true;
                        tvEnquiry.setVisibility(View.GONE);
                        tvTestDrive.setText("T%");
                        tvBooking.setText("B%");
                        tvV.setText("V%");
                        tvR.setText("R%");
                        logPercentageEvent();
                    } else {
                        PERCENT = false;
                        tvEnquiry.setVisibility(View.VISIBLE);
                        tvTestDrive.setText("T");
                        tvBooking.setText("B");
                        tvV.setText("V");
                        tvR.setText("R");
                    }
                }else {
                    tvR.setVisibility(View.VISIBLE);
                    tvV.setVisibility(View.VISIBLE);
                    if (isChecked) {
                        PERCENT = true;
                        tvEnquiry.setVisibility(View.GONE);
                        tvTestDrive.setText("T%");
                        tvBooking.setText("B%");
                        tvV.setText("V%");
                        tvR.setText("R%");
                        logPercentageEvent();
                    } else {
                        PERCENT = false;
                        tvEnquiry.setVisibility(View.VISIBLE);
                        tvTestDrive.setText("T");
                        tvBooking.setText("B");
                        tvV.setText("V");
                        tvR.setText("R");
                    }
                }
                if(etvbrGmSmCardAdpter != null) {
                    etvbrGmSmCardAdpter.notifyDataSetChanged();
                }

            }
        });
        return rView;
    }

    private void setSwipeRefreshView(final boolean val) {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(val);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        SalesCRMApplication.getBus().unregister(this);
    }

    private void setUpAdapter() {
            //resultSM = realm.where(SalesManager.class).findAll();
            locationsResult = realm.where(Location.class).equalTo("location_id", pref.getLocationId()).findFirst();
            int smId = getArguments().getInt("sm_id");
            //resultSM = realm.where(SalesManager.class).equalTo("id", smId).findAll();


            for (int i = 0; i< locationsResult.getSalesManagers().size(); i++){
                if(locationsResult.getSalesManagers().get(i).getId() == smId){
                    etvbrGmSmCardAdpter = new EtvbrGmSmCardAdapter(getActivity(), realm, locationsResult.getSalesManagers().get(i).getInfo(), locationsResult.getSalesManagers().get(i).getTeamLeaders(), ""+locationsResult.getSalesManagers().get(i).getId(),
                            ""+locationsResult.getLocationId());
                    mRecyclerView.setAdapter(etvbrGmSmCardAdpter);
                }
            }
            /*if(resultSM != null) {
                etvbrGmSmCardAdpter = new EtvbrGmSmCardAdapter(getActivity(), realm, resultSM.get(0).getInfo(), resultSM.get(0).getTeamLeaders());
                mRecyclerView.setAdapter(etvbrGmSmCardAdpter);
                //etvbrGmSmCardAdpter.notifyDataSetChanged();
            }*/
    }
    private void logPercentageEvent(){
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_PERCENTAGE);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
    }
}
