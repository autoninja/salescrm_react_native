package com.salescrm.telephony.telephonyModule.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by bannhi on 28/3/17.
 */

public class TelephonyModel {

    @SerializedName("data")
    @Expose
    private ArrayList<RecordingsModel> data;

    public ArrayList<RecordingsModel> getData() {
        return data;
    }

    public void setData(ArrayList<RecordingsModel> data) {
        this.data = data;
    }
}
