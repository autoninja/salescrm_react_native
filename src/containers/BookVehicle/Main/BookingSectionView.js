import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
export default class BookingSectionView extends Component {
  constructor(props) {
    super(props)
  }

  render() {

    let title = this.props.title;
    let subTitle = this.props.subTitle;
    let expand = this.props.expand;

    let imageSourceExpand = require('../../../images/ic_down_light.png');
    if (expand) {
      imageSourceExpand = require('../../../images/ic_up_light.png');
    }

    let imageSourceValid = null;
    if (this.props.mandatory) {
      if (this.props.valid) {
        imageSourceValid = require('../../../images/ic_check_mark.png');
      }
      else {
        imageSourceValid = require('../../../images/ic_error_mark.png');
      }
    }
    return (
      <TouchableOpacity onPress={() => this.props.onPress()}>
        <View style={[{
          backgroundColor: 'rgba(0,0,0,0.2)', height: 60, flexDirection: 'row',
          marginBottom: 2,
          justifyContent: 'space-between', alignItems: 'center', padding: 6, paddingLeft: 16, paddingRight: 16
        }, this.props.style]}>
          <View style={{ alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row' }}>
            <Text style={{ color: '#fff', fontSize: 15 }}>{title}</Text>
            <Image
              source={imageSourceValid}
              style={{ width: 20, marginLeft: 16, height: 20, }}
            />

          </View>
          <View style={{ alignItems: 'center', justifyContent: 'flex-end', flexDirection: 'row' }}>
            <Text style={{ color: '#e6e3e3', fontSize: 12, paddingEnd: 8 }}>{subTitle}</Text>
            <Image
              source={imageSourceExpand}
              style={{ width: 24, height: 24, borderRadius: 24 / 2, opacity: .2 }}
            />
          </View>
        </View>
      </TouchableOpacity>
    )
  }

}
