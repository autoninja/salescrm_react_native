
class TeamUserModel {
  constructor(location_id, teams, users) {
    this.location_id = location_id;
    this.teams = teams;
    this.users = users;
  }
}

class TeamModel {
  constructor(manager_id, team_leader_id, user_id) {
    this.manager_id = manager_id;
    this.team_leader_id = team_leader_id;
    this.user_id = user_id;
  }
}
class UserModel {
  constructor(id, name, mobile_number, role_id) {
    this.id = id;
    this.name = name;
    this.mobile_number = mobile_number;
    this.role_id = role_id;
  }
}

module.exports = {TeamUserModel, TeamModel, UserModel};
