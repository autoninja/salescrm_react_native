package com.salescrm.telephony.views.booking;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.salescrm.telephony.R;

import java.util.Locale;

/**
 * Created by bharath on 5/4/18.
 */

public class CarBookingQuantityForm {
    private static final CarBookingQuantityForm ourInstance = new CarBookingQuantityForm();
    private ImageButton ibAdd, ibRemove;
    private TextView tvTitle, tvQuantity;
    private Button btAction;
    private int quantity;

    public static CarBookingQuantityForm getInstance() {
        return ourInstance;
    }

    public void show(final Context context,
                     String carModelName,
                     @NonNull final CarBookingQuantitySelectListener listener) {

        final Dialog dialog = new Dialog(context, R.style.AppTheme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
        }
        dialog.setContentView(R.layout.booking_car_quantity_form);
        quantity = 1;
        tvTitle = (TextView) dialog.findViewById(R.id.tv_booking_quantity_title);
        ibAdd = (ImageButton) dialog.findViewById(R.id.ib_booking_car_quantity_add);
        ibRemove = (ImageButton) dialog.findViewById(R.id.ib_booking_car_quantity_remove);
        tvQuantity = (TextView) dialog.findViewById(R.id.tv_booking_car_quantity);
        btAction = (Button) dialog.findViewById(R.id.bt_car_booking_quantity_select_action);

        tvTitle.setText(carModelName);
        tvQuantity.setText(String.format(Locale.getDefault(), " %02d", quantity));
        ibAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ++quantity;
                tvQuantity.setText(String.format(Locale.getDefault(), " %02d", quantity));
            }
        });

        ibRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quantity > 1) {
                    --quantity;
                    tvQuantity.setText(String.format(Locale.getDefault(), " %02d", quantity));
                }
            }
        });
        dialog.findViewById(R.id.bt_car_booking_quantity_select_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                listener.onCarQuantitySelect(quantity);
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }

    public interface CarBookingQuantitySelectListener {
        void onCarQuantitySelect(int quantity);
    }
}
