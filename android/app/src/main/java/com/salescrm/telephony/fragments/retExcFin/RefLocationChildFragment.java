package com.salescrm.telephony.fragments.retExcFin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.EtvbrCardAdapter;
import com.salescrm.telephony.adapter.EtvbrGMCardAdpter;
import com.salescrm.telephony.adapter.EtvbrSMCardAdpter;
import com.salescrm.telephony.adapter.RefAdapter.RefCardAdapter;
import com.salescrm.telephony.adapter.RefAdapter.RefGMCardAdpter;
import com.salescrm.telephony.adapter.RefAdapter.RefSMCardAdpter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.db.ref_location.RefLocation;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.FabricUtils;

import java.util.HashMap;

import io.realm.Realm;

/**
 * Created by prateek on 13/11/17.
 */

public class RefLocationChildFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView tvEnquiry, tvTestDrive, tvV, tvBooking, tvR, tvL;
    private Switch swtchSwitch;
    public static boolean PERCENT = false;
    private Preferences pref;
    private Realm realm;
    private TextView tvDateIcon;
    private TextView tvDate;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RefSMCardAdpter etvbrSMCardAdpter;
    private RefCardAdapter etvbrCardAdapter;
    private RefGMCardAdpter etvbrGMCardAdpter;
    private RefLocation locationResult;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.etvbr_location_child_fragment, container, false);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(getActivity());
        mRecyclerView = (RecyclerView) rView.findViewById(R.id.recyclerViewEtvbr);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        tvEnquiry = (TextView)rView. findViewById(R.id.tv_enq);
        tvTestDrive = (TextView)rView. findViewById(R.id.tv_td);
        tvV = (TextView)rView. findViewById(R.id.tv_v);
        tvBooking = (TextView)rView. findViewById(R.id.tv_b);
        tvR = (TextView)rView. findViewById(R.id.tv_r);
        tvL = (TextView)rView.findViewById(R.id.tv_l);
        tvDate = (TextView)rView. findViewById(R.id.date_date);
        tvDateIcon = (TextView)rView. findViewById(R.id.date_icon);
        tvDateIcon.setVisibility(View.INVISIBLE);
        tvDate.setText(pref.getShowDateEtvbr());

        swtchSwitch = (Switch)rView. findViewById(R.id.swtch_etvbr);
        locationResult = realm.where(RefLocation.class).equalTo("location_id", getActivity().getIntent().getExtras().getInt("location_id")).findFirst();
        setUpAdapter(locationResult);
        PERCENT = false;
        setUpRef();
        swtchSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                System.out.println("Swiched: "+isChecked);
                    tvR.setVisibility(View.VISIBLE);
                    tvL.setVisibility(View.VISIBLE);
                    tvV.setVisibility(View.VISIBLE);
                    tvBooking.setVisibility(View.VISIBLE);
                if (isChecked) {
                    PERCENT = true;
                    //tvEnquiry.setVisibility(View.GONE);
                    tvEnquiry.setVisibility(View.GONE);
                    tvEnquiry.setText("Retail%");
                    tvTestDrive.setText("Exch%");
                    tvV.setText("Fin/I%");
                    tvBooking.setText("Fin/O%");
                    tvR.setText("PB%");
                    tvL.setText("LE%");
                    //logPercentageEvent();
                } else {
                    PERCENT = false;
                    tvEnquiry.setVisibility(View.VISIBLE);
                    tvEnquiry.setText("Retail");
                    tvTestDrive.setText("Exch");
                    tvV.setText("Fin/I");
                    tvBooking.setText("Fin/O");
                    tvR.setText("PB");
                    tvL.setText("LE");
                }
                notifyAdapter();

            }
        });

        return rView;
    }

    private void setUpRef() {
        tvEnquiry.setVisibility(View.VISIBLE);
        tvBooking.setVisibility(View.VISIBLE);
        tvR.setVisibility(View.VISIBLE);
        tvL.setVisibility(View.VISIBLE);
        tvEnquiry.setText("Retail");
        tvTestDrive.setText("Exch");
        tvV.setText("Fin/I");
        tvBooking.setText("Fin/O");
        tvR.setText("PB");
        tvL.setText("LE");
    }

    private void setUpAdapter(RefLocation locationResult) {
        if(this.locationResult.getBranchHead() == true) {
            etvbrGMCardAdpter = new RefGMCardAdpter(getActivity(), realm, locationResult);
            mRecyclerView.setAdapter(etvbrGMCardAdpter);
            etvbrGMCardAdpter.notifyDataSetChanged();
        }else if(this.locationResult.getSalesManagers().get(0).getInfo() != null){
            etvbrSMCardAdpter = new RefSMCardAdpter(getActivity(), realm, locationResult);
            mRecyclerView.setAdapter(etvbrSMCardAdpter);
            etvbrSMCardAdpter.notifyDataSetChanged();
        }else {
            etvbrCardAdapter = new RefCardAdapter(getActivity(), realm, locationResult);
            mRecyclerView.setAdapter(etvbrCardAdapter);
            etvbrCardAdapter.notifyDataSetChanged();
        }
    }

    private void notifyAdapter(){
        if(this.locationResult.getBranchHead() == true) {
            etvbrGMCardAdpter.notifyDataSetChanged();
        }else if(this.locationResult.getSalesManagers().get(0).getInfo() != null){
            etvbrSMCardAdpter.notifyDataSetChanged();
        }else {
            etvbrCardAdapter.notifyDataSetChanged();
        }
    }
    private void logPercentageEvent(){
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_PERCENTAGE);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
    }
}
