package com.salescrm.telephony.fragments.offline;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.offline.C360ActivityOffline;
import com.salescrm.telephony.db.BrandsDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.create_lead.Car_details;
import com.salescrm.telephony.db.create_lead.CreateLeadInputDataDB;
import com.salescrm.telephony.db.create_lead.Lead_data;
import com.salescrm.telephony.preferences.Preferences;

import io.realm.Realm;


/**
 * Created by bharath on 28/12/16.
 */

public class CarsFragmentOffline extends Fragment {
    private Preferences pref = null;
    private Realm realm;
    LinearLayout llNewCarHolder,llNewCarViewParent;
    LinearLayout llExCarHolder,llExCarViewParent;
    LinearLayout llAddCarHolder,llAddCarViewParent;
    FrameLayout frameNoCar;
    private View view;
    private Lead_data data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.c360_cars_fragment_offline, container, false);
        pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(getActivity());
        llNewCarHolder = (LinearLayout) view.findViewById(R.id.ll_c360_new_car);
        llNewCarViewParent = (LinearLayout) view.findViewById(R.id.ll_c360_new_car_views);

        llExCarHolder = (LinearLayout) view.findViewById(R.id.ll_c360_ex_car);
        llExCarViewParent = (LinearLayout) view.findViewById(R.id.ll_c360_ex_car_views);

        llAddCarHolder = (LinearLayout) view.findViewById(R.id.ll_c360_add_car);
        llAddCarViewParent = (LinearLayout) view.findViewById(R.id.ll_c360_add_car_views);

        frameNoCar = (FrameLayout)view.findViewById(R.id.frame_c360_no_car);



        return view;
    }

    @Override
    public void onResume() {
        updateViews();
        super.onResume();
    }

    private void updateViews() {
        view.invalidate();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            int scheduledActivityId = bundle.getInt("scheduledActivityId", 0);
            SalesCRMRealmTable realmData = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
            if(realmData!=null){
                data = realmData.getCreateLeadInputDataDB().getLead_data();
            }
        }
        System.out.println("Size Car:"+data.getCar_details().size());
        System.out.println("Size Ex:"+data.getEx_car_details().size());
        System.out.println("Size Add:"+data.getAd_car_details().size());
        if(data!=null){
            if(data.getCar_details().size()==0&&data.getEx_car_details().size()==0&&data.getAd_car_details().size()==0){
                frameNoCar.setVisibility(View.VISIBLE);
                llExCarHolder.setVisibility(View.GONE);
                llNewCarHolder.setVisibility(View.GONE);
                llAddCarHolder.setVisibility(View.GONE);
            }
            else {
                frameNoCar.setVisibility(View.GONE);
                addViewCarView(llNewCarViewParent,0);
                addViewCarView(llExCarViewParent,1);
                addViewCarView(llAddCarViewParent,2);


            }
        }
    }

    private void addViewCarView(final LinearLayout parent, int from) {
        parent.removeAllViews();
        if(data.getCar_details().size()==0&&data.getEx_car_details().size()==0&&data.getAd_car_details().size()==0){
            frameNoCar.setVisibility(View.VISIBLE);
            llExCarHolder.setVisibility(View.GONE);
            llNewCarHolder.setVisibility(View.GONE);
            llAddCarHolder.setVisibility(View.GONE);
        }
        switch (from){
            case 0:
                //From new Car
                if(data.getCar_details().size()==0){
                    llNewCarHolder.setVisibility(View.GONE);
                }
                else {
                    llNewCarHolder.setVisibility(View.VISIBLE);
                }
                for(int i=0;i<data.getCar_details().size();i++){
                    LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View v = vi.inflate(R.layout.lead_child_car_layout, null);
                    TextView tvMain = (TextView) v.findViewById(R.id.tvLeadMain);
                    final ImageButton btRemove = (ImageButton) v.findViewById(R.id.bt_remove_parent_lead);
                    btRemove.setTag(i);
                    btRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            realm.beginTransaction();
                            data.getCar_details().get(Integer.parseInt(btRemove.getTag().toString())).deleteFromRealm();
                            realm.commitTransaction();
                           addViewCarView(parent,0);
                        }
                    });
                    final TextView tvSub = (TextView) v.findViewById(R.id.tvLeadSub);
                    String main = data.getCar_details().get(i).getVariant_name();
                    if(data.getCar_details().get(i).getVariant_name()!=null){
                        main = data.getCar_details().get(i).getModel_name();
                    }


                    String sub2 = data.getCar_details().get(i).getColor_name();
                    String sub1 = data.getCar_details().get(i).getFuel_name();
                    if(TextUtils.isEmpty(sub1)){
                       sub1 = "Unknown FuelType";
                    }
                    if(TextUtils.isEmpty(sub2)){
                        sub2 = "Unknown Color";
                    }

                    tvMain.setText(i+1+". "+main);
                    tvSub.setText(sub1+" • "+sub2);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    parent.addView(v, lp);
                }

                break;
            case 1:
                //From Ex Car
                if(data.getEx_car_details().size()==0){
                    llExCarHolder.setVisibility(View.GONE);
                } else {
                    llExCarHolder.setVisibility(View.VISIBLE);
                }
                for(int i=0;i<data.getEx_car_details().size();i++){
                    LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View v = vi.inflate(R.layout.lead_child_car_layout, null);
                    TextView tvMain = (TextView) v.findViewById(R.id.tvLeadMain);
                    final TextView tvSub = (TextView) v.findViewById(R.id.tvLeadSub);

                    final ImageButton btRemove = (ImageButton) v.findViewById(R.id.bt_remove_parent_lead);
                    btRemove.setTag(i);
                    btRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            realm.beginTransaction();
                            data.getEx_car_details().get(Integer.parseInt(v.getTag().toString())).deleteFromRealm();
                            realm.commitTransaction();
                            addViewCarView(parent,1);
                        }
                    });

                    String main = data.getEx_car_details().get(i).getModel_name()+"";

                    String sub2 = data.getEx_car_details().get(i).getKms_run();
                    String sub1 = data.getEx_car_details().get(i).getPurchase_date();
                    if(TextUtils.isEmpty(sub1)){
                        sub1 = "Unknown";
                    }
                    else {
                       sub1 = sub1.split(" ")[0];
                    }
                    if(TextUtils.isEmpty(sub2)){
                        sub2 = "Unknown Date";
                    }

                    tvMain.setText(i+1+". "+main);
                    tvSub.setText(sub1+" • "+sub2+" Km");

                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    parent.addView(v, lp);
                }
                break;
            case 2:
                //From Add Car
                if(data.getAd_car_details().size()==0){
                    llAddCarHolder.setVisibility(View.GONE);
                } else {
                    llAddCarHolder.setVisibility(View.VISIBLE);
                }
                for(int i=0;i<data.getAd_car_details().size();i++){
                    LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View v = vi.inflate(R.layout.lead_child_car_layout, null);
                    TextView tvMain = (TextView) v.findViewById(R.id.tvLeadMain);
                    final TextView tvSub = (TextView) v.findViewById(R.id.tvLeadSub);
                    ImageButton btRemove = (ImageButton) v.findViewById(R.id.bt_remove_parent_lead);
                    btRemove.setTag(i);
                    btRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            realm.beginTransaction();
                            data.getAd_car_details().get(Integer.parseInt(v.getTag().toString())).deleteFromRealm();
                            realm.commitTransaction();
                            addViewCarView(parent,2);
                        }
                    });

                    String main = data.getAd_car_details().get(i).getModel_name()+"";

                    String sub2 = data.getAd_car_details().get(i).getKms_run();
                    String sub1 = data.getAd_car_details().get(i).getPurchase_date();
                    if(TextUtils.isEmpty(sub1)){
                        sub1 = "Unknown";
                    }
                    else {
                        sub1 = sub1.split(" ")[0];
                    }
                    if(TextUtils.isEmpty(sub2)){
                        sub2 = "Unknown Date";
                    }

                    tvMain.setText(i+1+". "+main);
                    tvSub.setText(sub1+" • "+sub2+" Km");
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    parent.addView(v, lp);
                }
                break;
        }
    }

}

