package com.salescrm.telephony.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.FilterActivityTypeCustomAdapter;
import com.salescrm.telephony.offline.FetchLeadElements;
import com.salescrm.telephony.dataitem.FilterActivityTypeModel;
import com.salescrm.telephony.db.AllCarsDB;
import com.salescrm.telephony.interfaces.FetchLeadElementsListener;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AddLeadElementsResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit.RetrofitError;

/**
 * Created by Ravindra P on 03-06-2016.
 */
public class FilterCarModelsFragment extends Fragment implements FetchLeadElementsListener {

    private View rootView;
    private ListView listView;
    private int lastSelectedListItem = 0;
    private String[] items;
    private List<FilterActivityTypeModel> modelItems;
    private FilterActivityTypeCustomAdapter adapter;
    private Realm realm;
    private FrameLayout frameProgress;
    private int apiCallCount = 0;
    private Preferences pref;
    private EditText etFilterSearch;
    private SparseArray<String> selectedValues;
    FilterSelectionHolder selectionHolder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        realm = Realm.getDefaultInstance();

        selectionHolder = FilterSelectionHolder.getInstance();

        rootView = inflater.inflate(R.layout.filter_activity_type_fragment, container, false);
        listView = (ListView) rootView.findViewById(R.id.lvfilter_activity_type_item_list);
        frameProgress = (FrameLayout) rootView.findViewById(R.id.filter_progress_bar);
        etFilterSearch = (EditText) rootView.findViewById(R.id.et_filter_search);
        etFilterSearch.setVisibility(View.VISIBLE);

        etFilterSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                adapter.filter(cs.toString());
                //   adapter.notifyDataSetChanged();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

        pref = Preferences.getInstance();
        pref.load(getActivity());
        init();

        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FilterActivityTypeModel model = modelItems.get(position);
                if (model.getValue() == 1) {
                    model.setValue(0);
                    if (selectedValues.get(Util.getInt(model.getId())) != null) {
                        selectedValues.remove(Util.getInt(model.getId()));
                        if (selectedValues.size() == 0) {
                            selectionHolder.removeMapData(2);
                        }
                    }
                } else {
                    model.setValue(1);
                    selectedValues.put(Util.getInt(model.getId()), model.getName());
                    selectionHolder.setMapData(2, selectedValues);

                }
                modelItems.set(position, model);
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
                lastSelectedListItem = position;
            }
        });
        return rootView;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }
    private void init() {
        if (realm.where(AllCarsDB.class).findAll().isEmpty() && new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            makeAPICall();
        } else {
            showAdapter();
        }
        if (selectedValues == null) {
            selectedValues = new SparseArray<String>();
        }

    }

    private void makeAPICall() {
        frameProgress.setVisibility(View.VISIBLE);
        new FetchLeadElements(this, getContext()).call();
    }

    @Override
    public void onLeadElementsFetched(AddLeadElementsResponse addLeadElementsResponse) {
        showAdapter();
    }

    private void showAdapter() {

        frameProgress.setVisibility(View.GONE);
        modelItems = new ArrayList<>();
        for (int i = 0; i < realm.where(AllCarsDB.class).equalTo("category", WSConstants.CAR_CATEGORY_BOTH).findAll().size(); i++) {
            AllCarsDB allCarsDB = realm.where(AllCarsDB.class).equalTo("category", WSConstants.CAR_CATEGORY_BOTH).findAll().get(i);
            modelItems.add(new FilterActivityTypeModel(allCarsDB.getName(), 0, allCarsDB.getId() + ""));
        }

        selectedValues = selectionHolder.getMapData(2);
        if (selectedValues != null) {
            for (int j = 0; j < selectedValues.size(); j++) {
                for (int i = 0; i < modelItems.size(); i++) {
                    FilterActivityTypeModel model = modelItems.get(i);
                    if (Integer.parseInt(model.getId()) == selectedValues.keyAt(j)) {
                        model.setValue(1);
                        modelItems.set(i, model);
                    }
                }
            }
        }

        adapter = new FilterActivityTypeCustomAdapter(getActivity(), modelItems);
        listView.setAdapter(adapter);

    }

    @Override
    public void onFetchLeadElementsError(RetrofitError error) {
        System.out.println("Error");
        onErrorApiCallController(error);
    }

    private void onErrorApiCallController(RetrofitError error) {
        if (error.getKind() == RetrofitError.Kind.NETWORK) {
            apiCallCount++;
            if (apiCallCount <= WSConstants.API_MAX_CALL) {
                Toast.makeText(getContext(), "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
            } else {
                apiCallCount = 0;
            }
        } else {
            frameProgress.setVisibility(View.GONE);
            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            Util.systemPrint("Something happened in the server");
            apiCallCount = 0;
        }
    }


}
