import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ScrollView,
  Alert
} from "react-native";

import styles from "../../styles/styles";

export default class ETVBRDetailsItem extends Component {
  constructor(props) {
    super(props);
  }
  _onLongPressButton() {
    console.log("Long press worked");
    Alert.alert("You long-pressed the button!");
  }
  render() {
    let leadDetails = this.props.leadDetails;
    let bgColor = "#f7412d";
    if (leadDetails.lead_tag === "Cold") {
      bgColor = "#00a7f7";
    } else if (leadDetails.lead_tag == "Warm") {
      bgColor = "#ff9900";
    }
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.openC360(
            leadDetails.lead_id,
            leadDetails.schedule_activity_id
          )
        }
        style={{ marginTop: 10 }}
      >
        <View
          style={{
            flexDirection: "row",
            backgroundColor: "#fff",
            borderRadius: 6
          }}
        >
          <View
            style={{
              flexDirection: "row",
              width: 4,
              backgroundColor: bgColor,
              borderBottomLeftRadius: 6,
              borderTopLeftRadius: 6
            }}
          />
          <View
            style={{
              paddingRight: 8,
              paddingLeft: 8,
              paddingTop: 6,
              paddingBottom: 6
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <Text
                style={{
                  fontSize: 16,
                  color: "#9b9b9b",
                  marginTop: 4,
                  height: 24
                }}
              >
                {" "}
                {leadDetails.title}{" "}
              </Text>
              <View>
                <Text style={{ fontSize: 20, color: "#45446d" }}>
                  {" "}
                  {leadDetails.customer_name}{" "}
                </Text>
                <Text style={{ fontSize: 17, marginTop: 6, color: "#494949" }}>
                  {" "}
                  {leadDetails.car_model_name}
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 6,
                    alignItems: "center"
                  }}
                >
                  <Image
                    source={require("../../images/ic_next_grey.png")}
                    style={{ width: 24, height: 24 }}
                  />
                  <Text style={{ fontSize: 17, color: "#494949" }}>
                    {" "}
                    {leadDetails.activity_name}{" "}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
