package com.salescrm.telephony.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.TeamDashboradGMPagerAdapter;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.EtvbrGmSmFragment;

/**
 * Created by prateek on 13/7/17.
 */

public class EtvbrSmActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TeamDashboradGMPagerAdapter teamDashboradPagerAdapter;
    private int prevTab = -1;
    Toolbar toolbar;
    public static int SELECTED_SM;
    TextView customTitle;
    int isItEtvbr = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.etvbr_sm_activity);

        toolbar = (Toolbar) findViewById(R.id.etvbr_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        customTitle = (TextView) findViewById(R.id.custom_title);
        //getSupportActionBar().setTitle(""+getIntent().getExtras().getString("sm_name") + "'s Team");
        customTitle.setText(""+getIntent().getExtras().getString("sm_name") + "'s Team");
        viewPager = (ViewPager) findViewById(R.id.dashboardviewpager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout_team_dashboard);
        SELECTED_SM = getIntent().getExtras().getInt("sm_id");
        isItEtvbr = getIntent().getExtras().getInt("etvbr");


        if(DbUtils.isBike()) {
            tabLayout.addTab(tabLayout.newTab().setText("ETB"));
        }else {
            tabLayout.addTab(tabLayout.newTab().setText("ETVBRL"));
        }
        tabLayout.setVisibility(View.GONE);
        //tabLayout.addTab(tabLayout.newTab().setText("Today's Tasks"));

        teamDashboradPagerAdapter = new TeamDashboradGMPagerAdapter(getSupportFragmentManager(), getIntent().getExtras().getInt("sm_id"), isItEtvbr);
        viewPager.setAdapter(teamDashboradPagerAdapter);
        populateViewpager();
        /*if(getArguments()!=null&&getArguments().getBoolean("team_dashboard_open_todays")){
            changeTabTextStyle(1);
            viewPager.setCurrentItem(1);
        }
        else {
            changeTabTextStyle(0);
        }*/
        //changeTabTextStyle(1);
        viewPager.setCurrentItem(0);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Bundle bundle = new Bundle();
        bundle.putBoolean("EtvbrSmActivity", true);
        bundle.putInt("smanager_id", getIntent().getExtras().getInt("sm_id"));
        // set Fragmentclass Arguments
        EtvbrGmSmFragment fragobj = new EtvbrGmSmFragment();
        fragobj.setArguments(bundle);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Bundle bundle = new Bundle();
        bundle.putBoolean("EtvbrSmActivity", false);
        // set Fragmentclass Arguments
        EtvbrGmSmFragment fragobj = new EtvbrGmSmFragment();
        fragobj.setArguments(bundle);
    }

    private void populateViewpager() {
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        //viewPager.setCurrentItem(pref.getLastVisitedTab());
        //viewPager.setOffscreenPageLimit(8);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                changeTabTextStyle(tab.getPosition());
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void changeTabTextStyle(int pos) {
        if (prevTab == -1) {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            prevTab = 0;

        } else {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(prevTab)))).getChildAt(1))).setTypeface(null, Typeface.NORMAL);
            prevTab=pos;
        }
    }
}
