package com.salescrm.telephony.model.booking;

/**
 * Created by bharath on 22/2/18.
 */

public class BookingPriceBreakupModel {
    private String exShowRoomPrice;
    private String insurance;
    private String registration;
    private String accessories;
    private String extendedWarranty;
    private String others;

    public String getExShowRoomPrice() {
        return exShowRoomPrice;
    }

    public void setExShowRoomPrice(String exShowRoomPrice) {
        this.exShowRoomPrice = exShowRoomPrice;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getAccessories() {
        return accessories;
    }

    public void setAccessories(String accessories) {
        this.accessories = accessories;
    }

    public String getExtendedWarranty() {
        return extendedWarranty;
    }

    public void setExtendedWarranty(String extendedWarranty) {
        this.extendedWarranty = extendedWarranty;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public void copy(BookingPriceBreakupModel bookingPriceBreakupModel) {
        this.exShowRoomPrice = bookingPriceBreakupModel.getExShowRoomPrice();
        this.insurance = bookingPriceBreakupModel.getInsurance();
        this.registration = bookingPriceBreakupModel.getRegistration();
        this.accessories = bookingPriceBreakupModel.getAccessories();
        this.extendedWarranty = bookingPriceBreakupModel.getExtendedWarranty();
        this.others = bookingPriceBreakupModel.getOthers();
    }
}
