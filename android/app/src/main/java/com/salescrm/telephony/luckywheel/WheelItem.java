package com.salescrm.telephony.luckywheel;

import android.graphics.Bitmap;

/**
 * Created by nndra on 01-Mar-18.
 */

public class WheelItem {

    public int color;
    public Bitmap bitmap;
    public String title;
    public String boosterSign;
    public String description;

    public WheelItem(int color, Bitmap bitmap, String boosterSign, String title, String description) {
        this.color = color;
        this.bitmap = bitmap;
        this.title = title;
        this.boosterSign = boosterSign;
        this.description = description;
    }

}