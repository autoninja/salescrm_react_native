import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'

import styles from '../../styles/styles'

export default class TodayTaskItemResult extends Component {
  constructor(props){
    super(props);
    this.state= {expand:false, showImagePlaceHolder:true};
  }


  render() {

          return(
            <View style={{flexDirection: 'row', height:60, backgroundColor:'#fff', alignItems:'center'}}>

            <View style={{flex:1.5, alignItems:'center'}} >
            <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
              <Text style={{ color:'#2e5e86', fontSize:16, fontWeight:'bold'}} >Total</Text>
            </View>
            </View>





            <View style= {{flex:1,height:'100%',}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#2e5e86', fontSize:16, fontWeight:'bold'}} >{this.props.abs.calls}</Text>
              </View>


            </View>

            <View style= {{flex:1,height:'100%',}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#2e5e86',fontSize:16, fontWeight:'bold'}} >{this.props.abs.tdrives}</Text>
              </View>

            </View>

            <View style= {{flex:1,height:'100%',justifyContent: "center",alignItems: "center"}}>

                <Text style={{ color:'#2e5e86', fontSize:16,fontWeight:'bold'}} >{this.props.abs.visits}</Text>
            </View>

            <View style= {{flex:1,height:'100%',}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#2e5e86',fontSize:16, fontWeight:'bold'}} >{this.props.abs.post_bookings}</Text>
              </View>


            </View>

            <View style= {{flex:1,height:'100%',}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#2e5e86', fontSize:16,fontWeight:'bold'}} >{this.props.abs.deliveries}</Text>
              </View>


            </View>

            <View style= {{flex:1,height:'100%',justifyContent: "center",alignItems: "center"}}>

                <Text style={{ color:'#2e5e86',fontSize:16, fontWeight:'bold'}} >{this.props.abs.pending_total_count}</Text>
            </View>

            {
              /*
              <View style= {{flex:1,height:'100%',justifyContent: "center",alignItems: "center"}}>
                    <Text style={{ color:'#2e5e86',fontSize:16, fontWeight:'bold'}} >{this.props.abs.uncalled_tasks}</Text>
              </View>
              */
            }





            </View>
          );
  }
}
