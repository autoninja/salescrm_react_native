import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'
import LiveEFFooter from './live_ef_footer'


export default class LiveEFResultItem extends Component {
  render() {
    if(this.props.showPercentage) {
      return(

              <View>
              <View style={{flexDirection: 'row', height:60, backgroundColor:'#fff', marginTop:10, alignItems:'center', borderRadius:6}}>

              <View style={{flex:1.5,height:'100%'}} >
              <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#2D4F8B', fontWeight:'bold'}} >Total%</Text>
              </View>


              </View>






              <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#1B075A', fontWeight:'bold'}} >{this.props.rel.e}</Text>
                </View>


              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#1B075A', fontWeight:'bold'}} >{this.props.rel.f_i}</Text>
                </View>

              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#1B075A', fontWeight:'bold'}} >{this.props.rel.f_o}</Text>
                </View>

              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
                borderLeftWidth: 0.3}}>
                <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                  <Text style={{ color:'#1B075A', fontWeight:'bold'}} >-</Text>
                </View>


              </View>

              <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
                borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

                  <Text style={{ color:'#1B075A', fontWeight:'bold'}} >-</Text>
              </View>


              </View>
              <LiveEFFooter />
              </View>
      );
    }
    else {
    return(

      <View>
      <View style={{flexDirection: 'row', height:60, backgroundColor:'#fff', marginTop:10, alignItems:'center', borderRadius:6}}>

      <View style={{flex:1.5,height:'100%'}} >
      <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
        <Text style={{ color:'#2D4F8B', fontWeight:'bold', }} >Achieved</Text>
      </View>

      <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center',}}>
        <Text style={{ color:'#303030'}} >Target</Text>
      </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Retails',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#1B075A',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.r}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030'}} >{this.props.targets.r}</Text>
        </View>
      </View>




      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Exchanged',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#1B075A',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.e}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030'}} >{this.props.targets.e}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'In House Financed',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#1B075A',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.f_i}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030'}} >{this.props.targets.f_i}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Out House Financed',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#1B075A',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.f_o}</Text>
          </TouchableOpacity>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Pending Bookings',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#00bfa5',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.p_b}</Text>
          </TouchableOpacity>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#fff',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Live Enquiries',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#00bfa5',textDecorationLine:'underline', fontWeight:'bold'}} >{this.props.abs.l_e}</Text>
          </TouchableOpacity>
      </View>


      </View>
      <LiveEFFooter />
      </View>

    );
  }
  }
}
