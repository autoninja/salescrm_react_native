package com.salescrm.telephony.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.retExcFin.RefContainerFragment;
import com.salescrm.telephony.interfaces.TeamDashboardSwipedListener;
import com.salescrm.telephony.model.TeamDashboardSwiped;

import java.lang.reflect.Field;
import java.util.HashMap;

public class ETVBRLMainFragment extends Fragment {
    private ViewPager viewPager = null;
    private TabLayout tabLayout;
    private int prevTab = -1;
    private Intent intentAddevent = new Intent("show_addEventsButtonForBroadcastReceiver");
    private int TAB_POSITION = 0;
    private TeamDashboardSwiped teamDashboardSwiped = TeamDashboardSwiped.getInstance(new TeamDashboardSwipedListener() {
        @Override
        public void onTeamDashboardSwiped(int position, int tabCount) {
            if (position == 1) {
                if (viewPager != null) {
                    logClevertap(viewPager.getCurrentItem());
                }

            }
        }
    }, 1);

    private void logClevertap(int tab) {

        switch (tab) {
            case 0:
                if (getActivity() != null) {
                    getActivity().sendBroadcast(new Intent("LOG_ETVBR"));
                }
                break;
            case 1:
                if (getActivity() != null) {
                    getActivity().sendBroadcast(new Intent("LOG_LIVE_EF"));
                }
                break;
            case 2:
                if (getActivity() != null) {
                    intentAddevent.putExtra("showAddEvent", true);
                    getActivity().sendBroadcast(intentAddevent);
                }
                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_EVENTS);
                CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
                break;
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.etvbrl_layout_main, container, false);
        viewPager = rootView.findViewById(R.id.view_pager_layout_etvbrl_main);
        tabLayout = rootView.findViewById(R.id.tab_layout_etvbrl_main);
        setTabOptions();
        populateViewpager();
        changeTabTextStyle(0);
        return rootView;
    }

    void setTabOptions() {
        if (DbUtils.isBike()) {
            tabLayout.addTab(tabLayout.newTab().setText("ETB"));
        } else {
            tabLayout.addTab(tabLayout.newTab().setText("ETVBRL"));
        }
        tabLayout.addTab(tabLayout.newTab().setText("Live / E-F"));
        if (DbUtils.isUserBranchHeadOperationsOrSM()) {
            tabLayout.addTab(tabLayout.newTab().setText("Events"));
        }


    }

    public void changeTabTextStyle(int pos) {

        if (prevTab == -1) {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            prevTab = 0;

        } else if (prevTab != pos) {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(prevTab)))).getChildAt(1))).setTypeface(null, Typeface.NORMAL);
            prevTab = pos;
        }
    }

    private void populateViewpager() {
        viewPager.setAdapter(new ETVBRLMAINPagerAdapter(getChildFragmentManager(), DbUtils.isUserBranchHeadOperationsOrSM() ? 3 : 2));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                changeTabTextStyle(tab.getPosition());
                TAB_POSITION = tab.getPosition();
                if (tab.getPosition() == 2) {
                    intentAddevent.putExtra("showAddEvent", true);
                    getActivity().sendBroadcast(intentAddevent);
                } else {
                    intentAddevent.putExtra("showAddEvent", false);
                    getActivity().sendBroadcast(intentAddevent);
                }
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private class ETVBRLMAINPagerAdapter extends FragmentStatePagerAdapter {
        int tabCount = 2;

        ETVBRLMAINPagerAdapter(FragmentManager fm, int i) {
            super(fm);
            tabCount = i;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return EtvbrContainerFragment.newInstance(0, "ETVBR");
                case 1:
                    return new RefContainerFragment();
                case 2:
                    return new RNEventsDashboardFragment();
                default:
                    return null;
            }
        }


        @Override
        public int getCount() {
            return tabCount;
        }


    }
    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
