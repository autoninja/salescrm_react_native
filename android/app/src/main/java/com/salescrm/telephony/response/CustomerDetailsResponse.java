package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by Ravindra P on 30-06-2016.
 */

public class CustomerDetailsResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private Customer_details customer_details;

        private String manualActivityForm;

        private Has_lead_owner has_lead_owner;

        private Activity_to_schedule activity_to_schedule;

        public Activity_to_schedule getActivity_to_schedule ()
        {
            return activity_to_schedule;
        }

        public void setActivity_to_schedule (Activity_to_schedule activity_to_schedule)
        {
            this.activity_to_schedule = activity_to_schedule;
        }

        public Customer_details getCustomer_details ()
        {
            return customer_details;
        }

        public void setCustomer_details (Customer_details customer_details)
        {
            this.customer_details = customer_details;
        }

        public String getManualActivityForm ()
        {
            return manualActivityForm;
        }

        public void setManualActivityForm (String manualActivityForm)
        {
            this.manualActivityForm = manualActivityForm;
        }

        public Has_lead_owner getHas_lead_owner ()
        {
            return has_lead_owner;
        }

        public void setHas_lead_owner (Has_lead_owner has_lead_owner)
        {
            this.has_lead_owner = has_lead_owner;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [customer_details = "+customer_details+", manualActivityForm = "+manualActivityForm+", has_lead_owner = "+has_lead_owner+", activity_to_schedule = "+activity_to_schedule+"]";
        }

        public class Customer_details
        {
            private List<PlannedActivities> plannedActivities;

            private List<ExchangeCarsDetails> exchangeCarsDetails;

            private CustomerDetails customerDetails;

            private LastActivityOnLead lastActivityOnLead;

            private List<InterestedCarsDetails> interestedCarsDetails;

            public List<PlannedActivities> getPlannedActivities ()
            {
                return plannedActivities;
            }

            public void setPlannedActivities (List<PlannedActivities> plannedActivities)
            {
                this.plannedActivities = plannedActivities;
            }

            public List<ExchangeCarsDetails> getExchangeCarsDetails ()
            {
                return exchangeCarsDetails;
            }

            public void setExchangeCarsDetails (List<ExchangeCarsDetails> exchangeCarsDetails)
            {
                this.exchangeCarsDetails = exchangeCarsDetails;
            }

            public CustomerDetails getCustomerDetails ()
            {
                return customerDetails;
            }

            public void setCustomerDetails (CustomerDetails customerDetails)
            {
                this.customerDetails = customerDetails;
            }

            public LastActivityOnLead getLastActivityOnLead ()
            {
                return lastActivityOnLead;
            }

            public void setLastActivityOnLead (LastActivityOnLead lastActivityOnLead)
            {
                this.lastActivityOnLead = lastActivityOnLead;
            }

            public List<InterestedCarsDetails> getInterestedCarsDetails ()
            {
                return interestedCarsDetails;
            }

            public void setInterestedCarsDetails (List<InterestedCarsDetails> interestedCarsDetails)
            {
                this.interestedCarsDetails = interestedCarsDetails;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [plannedActivities = "+plannedActivities+", exchangeCarsDetails = "+exchangeCarsDetails+", customerDetails = "+customerDetails+", lastActivityOnLead = "+lastActivityOnLead+", interestedCarsDetails = "+interestedCarsDetails+"]";
            }

            public class PlannedActivities
            {
                private String icon_type;

                private List<Details> details;

                private String name;

                private String scheduled_activity_id;

                private String date_time;

                private List<Actions> actions;

                public String getIcon_type ()
                {
                    return icon_type;
                }

                public void setIcon_type (String icon_type)
                {
                    this.icon_type = icon_type;
                }

                public List<Details> getDetails ()
                {
                    return details;
                }

                public void setDetails (List<Details> details)
                {
                    this.details = details;
                }

                public String getName ()
                {
                    return name;
                }

                public void setName (String name)
                {
                    this.name = name;
                }

                public String getScheduled_activity_id ()
                {
                    return scheduled_activity_id;
                }

                public void setScheduled_activity_id (String scheduled_activity_id)
                {
                    this.scheduled_activity_id = scheduled_activity_id;
                }

                public String getDate_time ()
                {
                    return date_time;
                }

                public void setDate_time (String date_time)
                {
                    this.date_time = date_time;
                }

                public List<Actions> getActions ()
                {
                    return actions;
                }

                public void setActions (List<Actions> actions)
                {
                    this.actions = actions;
                }

                @Override
                public String toString()
                {
                    return "ClassPojo [icon_type = "+icon_type+", details = "+details+", name = "+name+", scheduled_activity_id = "+scheduled_activity_id+", date_time = "+date_time+", actions = "+actions+"]";
                }

                public class Actions
                {
                    private String action_id;

                    private String icon_type;

                    private String action;

                    public String getAction_id ()
                    {
                        return action_id;
                    }

                    public void setAction_id (String action_id)
                    {
                        this.action_id = action_id;
                    }

                    public String getIcon_type ()
                    {
                        return icon_type;
                    }

                    public void setIcon_type (String icon_type)
                    {
                        this.icon_type = icon_type;
                    }

                    public String getAction ()
                    {
                        return action;
                    }

                    public void setAction (String action)
                    {
                        this.action = action;
                    }

                    @Override
                    public String toString()
                    {
                        return "ClassPojo [action_id = "+action_id+", icon_type = "+icon_type+", action = "+action+"]";
                    }
                }

                public class Details
                {
                    private String icon_type;

                    private String value;

                    private String key;

                    public String getIcon_type ()
                    {
                        return icon_type;
                    }

                    public void setIcon_type (String icon_type)
                    {
                        this.icon_type = icon_type;
                    }

                    public String getValue ()
                    {
                        return value;
                    }

                    public void setValue (String value)
                    {
                        this.value = value;
                    }

                    public String getKey ()
                    {
                        return key;
                    }

                    public void setKey (String key)
                    {
                        this.key = key;
                    }

                    @Override
                    public String toString()
                    {
                        return "ClassPojo [icon_type = "+icon_type+", value = "+value+", key = "+key+"]";
                    }
                }

            }
            public class CustomerDetails
            {
                private String buyer_type_name;

                private Dse_details dse_details;

                private String lead_active;

                private String type_of_customer;

                private Lead_location lead_location;

                private String designation;

                private List<Email_ids> email_ids;
                private String customer_age;
                private String gender;

                private String title;

                private String buyer_type_id;

                private String close_date;

                private String lead_id;

                private  List<Lead_tags> lead_tags;

                private String lead_last_updated;

                private String oem_source;

                private Lead_stage_progress[] lead_stage_progress;

                private String lead_source;

                private String address;

                private String enquiry_number;

                private String name;

                private List<Phone_nos> phone_nos;

                private String mode_of_payment;

                private String lead_age;

                private Lead_checklists[] lead_checklists;

                private String customer_id;

                private String[] finance_stage_progress;

                private String first_name;
                private String last_name;
                private String pin_code;
                private String office_address;
                private String office_pin_code;
                private String company_name;

                public String getCompany_name() {
                    return company_name;
                }

                public void setCompany_name(String company_name) {
                    this.company_name = company_name;
                }

                public String getOffice_address() {
                    return office_address;
                }

                public void setOffice_address(String office_address) {
                    this.office_address = office_address;
                }

                public String getOffice_pin_code() {
                    return office_pin_code;
                }

                public void setOffice_pin_code(String office_pin_code) {
                    this.office_pin_code = office_pin_code;
                }

                public String getPin_code() {
                    return pin_code;
                }

                public void setPin_code(String pin_code) {
                    this.pin_code = pin_code;
                }

                public String getFirst_name() {
                    return first_name;
                }

                public void setFirst_name(String first_name) {
                    this.first_name = first_name;
                }

                public String getLast_name() {
                    return last_name;
                }

                public void setLast_name(String last_name) {
                    this.last_name = last_name;
                }

                public Dse_details getDse_details ()
                {
                    return dse_details;
                }

                public void setDse_details (Dse_details dse_details)
                {
                    this.dse_details = dse_details;
                }

                public String getBuyer_type_name() {
                    return buyer_type_name;
                }

                public void setBuyer_type_name(String buyer_type_name) {
                    this.buyer_type_name = buyer_type_name;
                }

                public String getLead_active ()
                {
                    return lead_active;
                }

                public void setLead_active (String lead_active)
                {
                    this.lead_active = lead_active;
                }

                public String getType_of_customer ()
                {
                    return type_of_customer;
                }

                public void setType_of_customer (String type_of_customer)
                {
                    this.type_of_customer = type_of_customer;
                }

                public Lead_location getLead_location ()
                {
                    return lead_location;
                }

                public void setLead_location (Lead_location lead_location)
                {
                    this.lead_location = lead_location;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }
                public String getCustomer_age() {
                    return customer_age;
                }

                public void setCustomer_age(String customer_age) {
                    this.customer_age = customer_age;
                }

                public String getDesignation ()
                {
                    return designation;
                }

                public void setDesignation (String designation)
                {
                    this.designation = designation;
                }

                public List<Email_ids> getEmail_ids ()
                {
                    return email_ids;
                }

                public void setEmail_ids (List<Email_ids> email_ids)
                {
                    this.email_ids = email_ids;
                }

                public String getBuyer_type_id ()
                {
                    return buyer_type_id;
                }

                public void setBuyer_type_id (String buyer_type_id)
                {
                    this.buyer_type_id = buyer_type_id;
                }

                public String getClose_date ()
                {
                    return close_date;
                }

                public void setClose_date (String close_date)
                {
                    this.close_date = close_date;
                }

                public String getLead_id ()
                {
                    return lead_id;
                }

                public void setLead_id (String lead_id)
                {
                    this.lead_id = lead_id;
                }

                public List<Lead_tags> getLead_tags ()
                {
                    return lead_tags;
                }

                public void setLead_tags (List<Lead_tags> lead_tags)
                {
                    this.lead_tags = lead_tags;
                }

                public String getLead_last_updated ()
                {
                    return lead_last_updated;
                }

                public void setLead_last_updated (String lead_last_updated)
                {
                    this.lead_last_updated = lead_last_updated;
                }

                public String getOem_source ()
                {
                    return oem_source;
                }

                public void setOem_source (String oem_source)
                {
                    this.oem_source = oem_source;
                }

                public Lead_stage_progress[] getLead_stage_progress ()
                {
                    return lead_stage_progress;
                }

                public void setLead_stage_progress (Lead_stage_progress[] lead_stage_progress)
                {
                    this.lead_stage_progress = lead_stage_progress;
                }

                public String getLead_source ()
                {
                    return lead_source;
                }

                public void setLead_source (String lead_source)
                {
                    this.lead_source = lead_source;
                }

                public String getAddress ()
                {
                    return address;
                }

                public void setAddress (String address)
                {
                    this.address = address;
                }

                public String getEnquiry_number ()
                {
                    return enquiry_number;
                }

                public void setEnquiry_number (String enquiry_number)
                {
                    this.enquiry_number = enquiry_number;
                }

                public String getName ()
                {
                    return name;
                }

                public void setName (String name)
                {
                    this.name = name;
                }

                public List<Phone_nos> getPhone_nos ()
                {
                    return phone_nos;
                }

                public void setPhone_nos (List<Phone_nos> phone_nos)
                {
                    this.phone_nos = phone_nos;
                }

                public String getMode_of_payment ()
                {
                    return mode_of_payment;
                }

                public void setMode_of_payment (String mode_of_payment)
                {
                    this.mode_of_payment = mode_of_payment;
                }

                public String getLead_age ()
                {
                    return lead_age;
                }

                public void setLead_age (String lead_age)
                {
                    this.lead_age = lead_age;
                }

                public Lead_checklists[] getLead_checklists ()
                {
                    return lead_checklists;
                }

                public void setLead_checklists (Lead_checklists[] lead_checklists)
                {
                    this.lead_checklists = lead_checklists;
                }

                public String getCustomer_id ()
                {
                    return customer_id;
                }

                public void setCustomer_id (String customer_id)
                {
                    this.customer_id = customer_id;
                }

                public String[] getFinance_stage_progress ()
                {
                    return finance_stage_progress;
                }

                public void setFinance_stage_progress (String[] finance_stage_progress)
                {
                    this.finance_stage_progress = finance_stage_progress;
                }

                @Override
                public String toString()
                {
                    return "ClassPojo [dse_details = "+dse_details+", lead_active = "+lead_active+", type_of_customer = "+type_of_customer+", lead_location = "+lead_location+", designation = "+designation+", email_ids = "+email_ids+", buyer_type_id = "+buyer_type_id+", close_date = "+close_date+", lead_id = "+lead_id+", lead_tags = "+lead_tags+", lead_last_updated = "+lead_last_updated+", oem_source = "+oem_source+", lead_stage_progress = "+lead_stage_progress+", lead_source = "+lead_source+", address = "+address+", enquiry_number = "+enquiry_number+", name = "+name+", gender = "+gender+", phone_nos = "+phone_nos+", mode_of_payment = "+mode_of_payment+", lead_age = "+lead_age+", lead_checklists = "+lead_checklists+", customer_id = "+customer_id+", customer_age = "+customer_age+", finance_stage_progress = "+finance_stage_progress+"]";
                }



                public class Email_ids
                {
                    private String id;

                    private String status;

                    private String address;

                    private String lead_email_mapping_id;

                    public String getLead_email_mapping_id() {
                        return lead_email_mapping_id;
                    }

                    public void setLead_email_mapping_id(String lead_email_mapping_id) {
                        this.lead_email_mapping_id = lead_email_mapping_id;
                    }

                    public String getId ()
                    {
                        return id;
                    }

                    public void setId (String id)
                    {
                        this.id = id;
                    }

                    public String getStatus ()
                    {
                        return status;
                    }

                    public void setStatus (String status)
                    {
                        this.status = status;
                    }

                    public String getAddress ()
                    {
                        return address;
                    }

                    public void setAddress (String address)
                    {
                        this.address = address;
                    }

                    @Override
                    public String toString()
                    {
                        return "ClassPojo [id = "+id+", status = "+status+", address = "+address+"]";
                    }
                }

                public class Lead_tags
                {
                    private String color;

                    private String name;

                    public String getColor ()
                    {
                        return color;
                    }

                    public void setColor (String color)
                    {
                        this.color = color;
                    }

                    public String getName ()
                    {
                        return name;
                    }

                    public void setName (String name)
                    {
                        this.name = name;
                    }

                    @Override
                    public String toString()
                    {
                        return "ClassPojo [color = "+color+", name = "+name+"]";
                    }
                }

                public class Dse_details
                {
                    private String dse_id;

                    private String id;

                    private String dse_name;

                    private String dse_mobNo;

                    public String getDse_id ()
                    {
                        return dse_id;
                    }

                    public void setDse_id (String dse_id)
                    {
                        this.dse_id = dse_id;
                    }

                    public String getId ()
                    {
                        return id;
                    }

                    public void setId (String id)
                    {
                        this.id = id;
                    }

                    public String getDse_name ()
                    {
                        return dse_name;
                    }

                    public void setDse_name (String dse_name)
                    {
                        this.dse_name = dse_name;
                    }

                    public String getDse_mobNo ()
                    {
                        return dse_mobNo;
                    }

                    public void setDse_mobNo (String dse_mobNo)
                    {
                        this.dse_mobNo = dse_mobNo;
                    }

                    @Override
                    public String toString()
                    {
                        return "ClassPojo [dse_id = "+dse_id+", id = "+id+", dse_name = "+dse_name+", dse_mobNo = "+dse_mobNo+"]";
                    }
                }

                public class Lead_location
                {
                    private String id;

                    private String name;

                    public String getId ()
                    {
                        return id;
                    }

                    public void setId (String id)
                    {
                        this.id = id;
                    }

                    public String getName ()
                    {
                        return name;
                    }

                    public void setName (String name)
                    {
                        this.name = name;
                    }

                    @Override
                    public String toString()
                    {
                        return "ClassPojo [id = "+id+", name = "+name+"]";
                    }
                }

                public class Lead_stage_progress
                {
                    private String width;

                    private String name;

                    private String bar_class;

                    private String stage_id;

                    public String getWidth ()
                    {
                        return width;
                    }

                    public void setWidth (String width)
                    {
                        this.width = width;
                    }

                    public String getName ()
                    {
                        return name;
                    }

                    public void setName (String name)
                    {
                        this.name = name;
                    }

                    public String getBar_class ()
                    {
                        return bar_class;
                    }

                    public void setBar_class (String bar_class)
                    {
                        this.bar_class = bar_class;
                    }

                    public String getStage_id ()
                    {
                        return stage_id;
                    }

                    public void setStage_id (String stage_id)
                    {
                        this.stage_id = stage_id;
                    }

                    @Override
                    public String toString()
                    {
                        return "ClassPojo [width = "+width+", name = "+name+", bar_class = "+bar_class+", stage_id = "+stage_id+"]";
                    }
                }

                public class Phone_nos
                {
                    private String id;

                    private String status;

                    private String number;

                    public String getId ()
                    {
                        return id;
                    }

                    public void setId (String id)
                    {
                        this.id = id;
                    }

                    public String getStatus ()
                    {
                        return status;
                    }

                    public void setStatus (String status)
                    {
                        this.status = status;
                    }

                    public String getNumber ()
                    {
                        return number;
                    }

                    public void setNumber (String number)
                    {
                        this.number = number;
                    }

                    @Override
                    public String toString()
                    {
                        return "ClassPojo [id = "+id+", status = "+status+", number = "+number+"]";
                    }
                }

                public class Lead_checklists
                {
                    private String title;

                    private String lead_checklist_id;

                    private String value;

                    public String getTitle ()
                    {
                        return title;
                    }

                    public void setTitle (String title)
                    {
                        this.title = title;
                    }

                    public String getLead_checklist_id ()
                    {
                        return lead_checklist_id;
                    }

                    public void setLead_checklist_id (String lead_checklist_id)
                    {
                        this.lead_checklist_id = lead_checklist_id;
                    }

                    public String getValue ()
                    {
                        return value;
                    }

                    public void setValue (String value)
                    {
                        this.value = value;
                    }

                    @Override
                    public String toString()
                    {
                        return "ClassPojo [title = "+title+", lead_checklist_id = "+lead_checklist_id+", value = "+value+"]";
                    }
                }
            }

            public class LastActivityOnLead
            {
                private String remarks_edited;

                private String icon_type;

                private String logged_time;

                private String title;

                private String edit_flag;

                private List<Events> events;

                private String remarks;

                private String log_type;

                private String log_id;

                public String getRemarks_edited ()
                {
                    return remarks_edited;
                }

                public void setRemarks_edited (String remarks_edited)
                {
                    this.remarks_edited = remarks_edited;
                }

                public String getIcon_type ()
                {
                    return icon_type;
                }

                public void setIcon_type (String icon_type)
                {
                    this.icon_type = icon_type;
                }

                public String getLogged_time ()
                {
                    return logged_time;
                }

                public void setLogged_time (String logged_time)
                {
                    this.logged_time = logged_time;
                }

                public String getTitle ()
                {
                    return title;
                }

                public void setTitle (String title)
                {
                    this.title = title;
                }

                public String getEdit_flag ()
                {
                    return edit_flag;
                }

                public void setEdit_flag (String edit_flag)
                {
                    this.edit_flag = edit_flag;
                }

                public List<Events> getEvents ()
                {
                    return events;
                }

                public void setEvents (List<Events> events)
                {
                    this.events = events;
                }

                public String getRemarks ()
                {
                    return remarks;
                }

                public void setRemarks (String remarks)
                {
                    this.remarks = remarks;
                }

                public String getLog_type ()
                {
                    return log_type;
                }

                public void setLog_type (String log_type)
                {
                    this.log_type = log_type;
                }

                public String getLog_id ()
                {
                    return log_id;
                }

                public void setLog_id (String log_id)
                {
                    this.log_id = log_id;
                }

                @Override
                public String toString()
                {
                    return "ClassPojo [remarks_edited = "+remarks_edited+", icon_type = "+icon_type+", logged_time = "+logged_time+", title = "+title+", edit_flag = "+edit_flag+", events = "+events+", remarks = "+remarks+", log_type = "+log_type+", log_id = "+log_id+"]";
                }
            }

            public class Events
            {
                private String[] detail;

                private String action;

                public String[] getDetail ()
                {
                    return detail;
                }

                public void setDetail (String[] detail)
                {
                    this.detail = detail;
                }

                public String getAction ()
                {
                    return action;
                }

                public void setAction (String action)
                {
                    this.action = action;
                }

                @Override
                public String toString()
                {
                    return "ClassPojo [detail = "+detail+", action = "+action+"]";
                }
            }

            public class InterestedCarsDetails
            {
                private String color_id;

                private String model_id;

                private List<Car_activity_groups> car_activity_groups;

                private String lead_car_id;

                private String name;

                private String variant_id;

                private String is_primary;

                private String variant_code;

                public String getColor_id ()
                {
                    return color_id;
                }

                public void setColor_id (String color_id)
                {
                    this.color_id = color_id;
                }

                public String getModel_id ()
                {
                    return model_id;
                }

                public void setModel_id (String model_id)
                {
                    this.model_id = model_id;
                }

                public List<Car_activity_groups> getCar_activity_groups ()
                {
                    return car_activity_groups;
                }

                public void setCar_activity_groups (List<Car_activity_groups> car_activity_groups)
                {
                    this.car_activity_groups = car_activity_groups;
                }

                public String getLead_car_id ()
                {
                    return lead_car_id;
                }

                public void setLead_car_id (String lead_car_id)
                {
                    this.lead_car_id = lead_car_id;
                }

                public String getName ()
                {
                    return name;
                }

                public void setName (String name)
                {
                    this.name = name;
                }

                public String getVariant_id ()
                {
                    return variant_id;
                }

                public void setVariant_id (String variant_id)
                {
                    this.variant_id = variant_id;
                }

                public String getIs_primary ()
                {
                    return is_primary;
                }

                public void setIs_primary (String is_primary)
                {
                    this.is_primary = is_primary;
                }

                public String getVariant_code ()
                {
                    return variant_code;
                }

                public void setVariant_code (String variant_code)
                {
                    this.variant_code = variant_code;
                }

                @Override
                public String toString()
                {
                    return "ClassPojo [color_id = "+color_id+", model_id = "+model_id+", car_activity_groups = "+car_activity_groups+", lead_car_id = "+lead_car_id+", name = "+name+", variant_id = "+variant_id+", is_primary = "+is_primary+", variant_code = "+variant_code+"]";
                }

                public class Car_activity_groups
                {
                    private String group_name;

                    private List<Activity_stages> activity_stages;

                    public String getGroup_name ()
                    {
                        return group_name;
                    }

                    public void setGroup_name (String group_name)
                    {
                        this.group_name = group_name;
                    }

                    public List<Activity_stages> getActivity_stages ()
                    {
                        return activity_stages;
                    }

                    public void setActivity_stages (List<Activity_stages> activity_stages)
                    {
                        this.activity_stages = activity_stages;
                    }

                    @Override
                    public String toString()
                    {
                        return "ClassPojo [group_name = "+group_name+", activity_stages = "+activity_stages+"]";
                    }

                    public class Activity_stages
                    {
                        private String width;

                        private String name;

                        private String bar_class;

                        private String stage_id;

                        public String getWidth ()
                        {
                            return width;
                        }

                        public void setWidth (String width)
                        {
                            this.width = width;
                        }

                        public String getName ()
                        {
                            return name;
                        }

                        public void setName (String name)
                        {
                            this.name = name;
                        }

                        public String getBar_class ()
                        {
                            return bar_class;
                        }

                        public void setBar_class (String bar_class)
                        {
                            this.bar_class = bar_class;
                        }

                        public String getStage_id ()
                        {
                            return stage_id;
                        }

                        public void setStage_id (String stage_id)
                        {
                            this.stage_id = stage_id;
                        }

                        @Override
                        public String toString()
                        {
                            return "ClassPojo [width = "+width+", name = "+name+", bar_class = "+bar_class+", stage_id = "+stage_id+"]";
                        }
                    }

                }

            }

            public class ExchangeCarsDetails{

                private String expected_price;

                private String market_price;

                private String model_id;

                private String lead_exchange_car_id;

                private String name;

                private String price_quoted;

                private String purchase_date;

                private Exchange_status[] exchange_status;

                public String getExpected_price ()
                {
                    return expected_price;
                }

                public void setExpected_price (String expected_price)
                {
                    this.expected_price = expected_price;
                }

                public String getMarket_price ()
                {
                    return market_price;
                }

                public void setMarket_price (String market_price)
                {
                    this.market_price = market_price;
                }

                public String getModel_id ()
                {
                    return model_id;
                }

                public void setModel_id (String model_id)
                {
                    this.model_id = model_id;
                }

                public String getLead_exchange_car_id ()
                {
                    return lead_exchange_car_id;
                }

                public void setLead_exchange_car_id (String lead_exchange_car_id)
                {
                    this.lead_exchange_car_id = lead_exchange_car_id;
                }

                public String getName ()
                {
                    return name;
                }

                public void setName (String name)
                {
                    this.name = name;
                }

                public String getPrice_quoted ()
                {
                    return price_quoted;
                }

                public void setPrice_quoted (String price_quoted)
                {
                    this.price_quoted = price_quoted;
                }

                public String getPurchase_date ()
                {
                    return purchase_date;
                }

                public void setPurchase_date (String purchase_date)
                {
                    this.purchase_date = purchase_date;
                }

                public Exchange_status[] getExchange_status ()
                {
                    return exchange_status;
                }

                public void setExchange_status (Exchange_status[] exchange_status)
                {
                    this.exchange_status = exchange_status;
                }

                @Override
                public String toString()
                {
                    return "ClassPojo [expected_price = "+expected_price+", market_price = "+market_price+", model_id = "+model_id+", lead_exchange_car_id = "+lead_exchange_car_id+", name = "+name+", price_quoted = "+price_quoted+", purchase_date = "+purchase_date+", exchange_status = "+exchange_status+"]";
                }

                public class Exchange_status {
                    private String width;

                    private String name;

                    private String bar_class;

                    private String stage_id;

                    public String getWidth() {
                        return width;
                    }

                    public void setWidth(String width) {
                        this.width = width;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getBar_class() {
                        return bar_class;
                    }

                    public void setBar_class(String bar_class) {
                        this.bar_class = bar_class;
                    }

                    public String getStage_id() {
                        return stage_id;
                    }

                    public void setStage_id(String stage_id) {
                        this.stage_id = stage_id;
                    }

                    @Override
                    public String toString() {
                        return "ClassPojo [width = " + width + ", name = " + name + ", bar_class = " + bar_class + ", stage_id = " + stage_id + "]";
                    }
                }



            }

        }

        public class Has_lead_owner
        {
            private String message;

            private String response;

            public String getMessage ()
            {
                return message;
            }

            public void setMessage (String message)
            {
                this.message = message;
            }

            public String getResponse ()
            {
                return response;
            }

            public void setResponse (String response)
            {
                this.response = response;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [message = "+message+", response = "+response+"]";
            }
        }

    }
}






