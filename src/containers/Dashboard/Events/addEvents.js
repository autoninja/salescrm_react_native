import React, { Component } from 'react';
import {Modal, Text, View, Image, TouchableOpacity, TextInput,
		Alert, Picker, FlatList, CheckBox, StyleSheet, Button,
		ActivityIndicator, NativeModules,
		TouchableWithoutFeedback, Platform} from 'react-native';
/*import { Dropdown } from 'react-native-material-dropdown';*/
/*import Modal from 'react-native-modal';*/
import DateTimePicker from 'react-native-modal-datetime-picker';
import Moment from 'moment';
import RestClient from '../../../network/rest_client'

import { connect } from 'react-redux';
import { updateEventDashboard } from '../../../actions/event_update_actions';

import CleverTapPush from '../../../clevertap/CleverTapPush'
import {eventAddOrUpdateEvent} from '../../../clevertap/CleverTapConstants';


class AddEvents extends Component {

	state = {
		update: false,
	}
	constructor(props){
		super(props);
		this.state = {
	    leadSourceTypeModalVisible: false,
	    leadSourceText:'Select Lead Source Category',
	    leadSourceId:'',
	    openInnerLeadSource:[],
	    leadSourceCategoryIds:[],
	    //eventInformationResult: eventIdInformationHard,
	    leadSourceArrayMain:this.getLeadSourceCategory(),
	    leadSourceCategoryChecked: this.populateleadSourceCategoryChecked(),


	    eventCategoryTypeModalVisible: false,
	    eventCategoryText:'Select Event Category',
	    eventCategoryId:'',
	    eventCategoryArrayMain:this.getCategory(),

	    locationTypeModalVisible: false,
	    locationSelectedText:'Select Location',
	    locationSelectedIds:[],
	    locationArrayMain: this.getAllLocations(),
	    locationChecked:this.populateLocationCategoryChecked(),

	    eventNameText:'',

	    startDate:'Start Date',
	    endDate:'End Date',
	    openCalendar:false,
	    isItStartDate:true,
	    isBranchHeadCreator: true,

	    //eventInformationResult: eventIdInformationHard,

	  };
	}

		componentDidMount(){
			if(this.isItEditEvent()){
        		this.makeRequestToGetEventInfo();
        }/*else{
				this.setState({
                isLoading: false,
                isFetching:false,
                //eventInformationResult: eventIdInformationHard,
                //leadSourceCategoryChecked: this.populateforEditLeadSourceCategoryChecked(),
            	})
        	}*/
    	}

		makeRequestToGetEventInfo = () => {
			this.setState({ isLoading: true, isFetching: true });
			new RestClient().getEventInfo({event_id:this.props.navigation.getParam('eventId')})
			.then( (responseJson) => {
            console.log("called -----"+JSON.stringify(responseJson))
            if(responseJson.statusCode == 2002){
            this.setState({
                isLoading: false,
                isFetching:false,
                //eventInformationResult: eventIdInformationHard,
                leadSourceCategoryChecked: this.populateforEditLeadSourceCategoryChecked(responseJson.result),
                leadSourceCategoryIds:this.getLeadSourceCategoryIds(responseJson.result),
                leadSourceText: this.getLeadSourceCategorySelectionCounts(responseJson.result),
                locationChecked: this.populateEditLocationCategoryChecked(responseJson.result),
                locationSelectedIds:this.getLocSelectedIds(responseJson.result),
                locationSelectedText:this.getLocationText(responseJson.result),
                eventCategoryText: this.getEventCategoryName(responseJson.result),
                eventCategoryId: responseJson.result.event_category_id,
                eventNameText: responseJson.result.name,
                startDate: Moment(responseJson.result.start_date).format('YYYY-MM-DD'),
                endDate:Moment(responseJson.result.end_date).format('YYYY-MM-DD'),
                isBranchHeadCreator: responseJson.result.is_creater,

            });
        }else if(responseJson.statusCode == 4000){
        		 this.setState({
                isLoading: false,
                isFetching:false,});
        		return Alert.alert(responseJson.message)
        }
        }).catch(error => {
        	this.setState({
                isLoading: false,
                isFetching:false,});
        		return Alert.alert('Someting went wrong')
        });
		}

		getAllLocations = () =>{
			console.log("helloHello "+ this.isItEditEvent() +", "+ this.isEventActive()+", "+this.props.eventId);
			return this.props.allLocations;
		}

		getCategory = () =>{

			return this.props.eventCategories;
		}

		getLeadSourceCategory = () =>{

			return this.props.enquirySourceCategories;
		}

   		_leadSourceType = () => {
   			console.log("hellooo"+this.state.leadSourceTypeModalVisible)
   			this.setState({leadSourceTypeModalVisible: !this.state.leadSourceTypeModalVisible});
   		}

   		_leadSourceText = (id, label) => {
   			console.log("hellooo ID "+id)
   			this.setState({leadSourceTypeModalVisible: !this.state.leadSourceTypeModalVisible, leadSourceId:id, leadSourceText:label});
   		}

   		_eventCategoryType = () => {
   			console.log("hellooo Event"+this.state.eventCategoryTypeModalVisible)
   			let changedValue = !this.state.eventCategoryTypeModalVisible;
   			this.setState({eventCategoryTypeModalVisible: changedValue});
   		}

   		_eventCategoryText = (id, label) => {
   			console.log("hellooo Event ID "+id)
   			this.setState({eventCategoryTypeModalVisible: !this.state.eventCategoryTypeModalVisible, eventCategoryId:id, eventCategoryText:label});
   		}

   		_locationType = () => {
   			console.log("hellooo"+this.state.locationTypeModalVisible)
   			this.setState({locationTypeModalVisible: !this.state.locationTypeModalVisible, locationSelectedText: this.state.locationSelectedText});
   		}

   		_locationSubmit = () => {
   			console.log("hellooo"+this.state.locationTypeModalVisible)
   				this.state.locationSelectedText = '';
   				this.state.locationSelectedIds =  [];
   				let { locationSelectedIds } = this.state;
   				var j = 0;
	   			for (var i=0; i < this.state.locationArrayMain.length; i++) {

	        		if(this.state.locationChecked[i].checked){
	        			j++;
	        			console.log("it is...." +this.state.locationChecked[i].checked)
	        			//this.state.locationSelectedText = this.state.locationSelectedText + this.state.locationArrayMain[i].name +",";
	        			locationSelectedIds.push(this.state.locationArrayMain[i].id)
	        		}
	    		}
	    		if(j > 0){
	    			this.state.locationSelectedText = "Location("+j+")";
	    		}else{
	    			this.state.locationSelectedText = "Select Location";
	    		}
   			this.setState({locationTypeModalVisible: !this.state.locationTypeModalVisible, locationSelectedText: this.state.locationSelectedText,locationSelectedIds: locationSelectedIds});
   		}

   		/*_locationText = (id, label) => {
   			console.log("hellooo Event ID "+id)
   			this.setState({locationTypeModalVisible: !this.state.locationTypeModalVisible, locationId:id, locationText:label});
   		}*/

   		toggleLocationCheckbox = (index) => {
   			console.log("isItChecked: "+this.state.locationChecked[index].checked)
   			let {locationChecked} = this.state;
   			locationChecked[index].checked = !locationChecked[index].checked
		    this.setState({ locationChecked : locationChecked});
		  }

		togglesourceCategoryCheckbox = (index) => {
			console.log("isItChecked: "+this.state.leadSourceCategoryChecked[index].checked)
			/*this.state.leadSourceCategoryChecked[index]. = !this.state.leadSourceCategoryChecked[index]*/
			this.state.openInnerLeadSource[index] = !this.state.openInnerLeadSource[index]

			let {leadSourceCategoryChecked} = this.state;

			leadSourceCategoryChecked[index].checked = !leadSourceCategoryChecked[index].checked
				for(var i=0; i < leadSourceCategoryChecked[index].subEnquirySource.length; i++) {
					if(leadSourceCategoryChecked[index].checked){
						leadSourceCategoryChecked[index].subEnquirySource[i].checked = true
					}else{
						leadSourceCategoryChecked[index].subEnquirySource[i].checked = false
					}
				}
		    this.setState({ leadSourceCategoryChecked, openInnerLeadSource : this.state.openInnerLeadSource });
		  }

		togglesourceCategoryCheckboxInner = (indexInner, indexOuter) => {
			//console.log("InnerisItChecked: "+this.state.leadSourceCategoryChecked[indexOuter].source[indexInner].checked)
			let { leadSourceCategoryChecked } = this.state
   			leadSourceCategoryChecked[indexOuter].subEnquirySource[indexInner].checked = !leadSourceCategoryChecked[indexOuter].subEnquirySource[indexInner].checked
   			/*if(leadSourceCategoryChecked[indexOuter].subEnquirySource[indexInner].checked){
   				if(this.allSelected(leadSourceCategoryChecked[indexOuter].subEnquirySource)){
   					leadSourceCategoryChecked[indexOuter].checked = true
   					console.log("InnerisItChecked: "+"true true")
   				}else{
   					leadSourceCategoryChecked[indexOuter].checked = false
   					console.log("InnerisItChecked: "+"false")
   				}
   			}else{
   				leadSourceCategoryChecked[indexOuter].checked = false
   				console.log("InnerisItChecked: "+"false false")
   			}*/
		    this.setState({ leadSourceCategoryChecked});
		}

		allSelected = (leadeSourceInnerArray) => {
			var allSelected = false;
			for(var i=0; i < leadeSourceInnerArray.length; i++) {
				console.log("hellorama: "+leadeSourceInnerArray[i].checked);
					if(leadeSourceInnerArray[i].checked == false){
						return allSelected = false;
					}
				}
			return allSelected = true;
		}

		openInnerLeadSource = (index) => {
			console.log("leadSourceInner: "+index + ", "+ this.state.openInnerLeadSource[index])
			this.state.openInnerLeadSource[index] = !this.state.openInnerLeadSource[index]
			this.setState({ openInnerLeadSource : this.state.openInnerLeadSource});

		}

		populateLocationCategoryChecked = () => {
			let locationChecked = [];
			this.props.allLocations.map((data) =>{
				locationChecked.push({checked:false, id:data.id})
			})

			return locationChecked;
		}

		getEventCategoryName = (result) => {

			let categoryName = '';
			for(var i = 0; i<this.state.eventCategoryArrayMain.length; i++){
				if(this.state.eventCategoryArrayMain[i].id+"" === result.event_category_id+"" ){
					categoryName = this.state.eventCategoryArrayMain[i].name;
					//this.setState = ({eventCategoryId:result.event_category_id+""})
				}
			}

			return categoryName;

		}

		populateEditLocationCategoryChecked = (locationResult) => {
			console.log("InnerLoc", JSON.stringify(this.state.locationChecked))
			console.log("InnerLoc 2", JSON.stringify(locationResult))
			let {locationChecked} = this.state;
			for(var i =0; i<locationResult.location_ids.length; i++){
				for(var j =0; j<locationChecked.length; j++){
					if(locationResult.location_ids[i]+"" == locationChecked[j].id){
						console.log("InnerLoc 3"+ locationChecked[j].id)
						locationChecked[j].checked = true;
					}
				}
			}
			return locationChecked;
		}

		getLocSelectedIds = (locationResult) => {
			let locationIds = [];
			for(var i = 0; i<locationResult.location_ids.length; i++){
				locationIds.push(locationResult.location_ids[i]+"")
			}
			return locationIds;
		}

		getLocationText = (locationResult) => {
			return "Location("+locationResult.location_ids.length+")";
		}


		populateleadSourceCategoryChecked = () => {
			let leadSourceCategoryChecked = [];
			this.props.enquirySourceCategories.map((data) =>{
				let leadeSourceCategoryCheckedInner = [];
				data.subEnquirySource.map((data1) =>{
					leadeSourceCategoryCheckedInner.push({checked:false, id:data1.id})
				})
				//{checked} = this.state;
				leadSourceCategoryChecked.push({checked:false, id:data.id, subEnquirySource:leadeSourceCategoryCheckedInner})

			})
			console.log("InnerPause", JSON.stringify(leadSourceCategoryChecked))

			return leadSourceCategoryChecked;
		}

		populateforEditLeadSourceCategoryChecked = (eventIdInformationHard) => {

				console.log("InnerPause", JSON.stringify(this.state.leadSourceCategoryChecked))

				let {leadSourceCategoryChecked} = this.state;

				for(var i = 0; i<eventIdInformationHard.lead_source_category.length; i++){
					for(var j = 0; j<leadSourceCategoryChecked.length; j++){
						if(eventIdInformationHard.lead_source_category[i].id == leadSourceCategoryChecked[j].id){
							for(var k =0; k<leadSourceCategoryChecked[j].subEnquirySource.length; k++){
								for(var l = 0; l<eventIdInformationHard.lead_source_category[i].lead_sources.length; l++){
									if(eventIdInformationHard.lead_source_category[i].lead_sources[l]+"" == leadSourceCategoryChecked[j].subEnquirySource[k].id){
										leadSourceCategoryChecked[j].subEnquirySource[k].checked = true;
										leadSourceCategoryChecked[j].checked = true;
									}
								}
							}
						}
					}
				}

				return leadSourceCategoryChecked;

		}


		_leadSourceCategorySubmit = () => {
			//let {leadSourceCategoryChecked} = this.state;
			let leadSourceCategoryIds = [];
			for(var i = 0; i<this.state.leadSourceCategoryChecked.length; i++){
				console.log('yoyoyo: '+JSON.stringify(this.state.leadSourceCategoryChecked));
				let lead_sources = [];
				for(var j = 0; j<this.state.leadSourceCategoryChecked[i].subEnquirySource.length; j++){
					if(this.state.leadSourceCategoryChecked[i].subEnquirySource[j].checked == true){
						console.log("yoyoyo")
						lead_sources.push(this.state.leadSourceArrayMain[i].subEnquirySource[j].id)
					}
				}
				if(lead_sources.length > 0){
					leadSourceCategoryIds.push({id:this.state.leadSourceArrayMain[i].id, lead_sources:lead_sources})
				}
			}
			console.log("hehhehe "+ JSON.stringify(leadSourceCategoryIds));
			let selectedItems = 'Select Lead Source Category'
			if(leadSourceCategoryIds.length>0){
				selectedItems = "Lead Source Category("+ leadSourceCategoryIds.length+")"
			}else{
				selectedItems = 'Select Lead Source Category'
			}
			this.setState({leadSourceTypeModalVisible: !this.state.leadSourceTypeModalVisible,
				leadSourceCategoryIds:leadSourceCategoryIds, leadSourceText:selectedItems})
		}

		getLeadSourceCategoryIds = (result) => {
			let leadSourceCategoryIds = [];

				for(var i = 0; i<result.lead_source_category.length; i++){
					let lead_sources = [];
					for(var j =0; j<result.lead_source_category[i].lead_sources.length; j++){
						lead_sources.push(result.lead_source_category[i].lead_sources[j]+"")
					}
					leadSourceCategoryIds.push({id:result.lead_source_category[i].id+"", lead_sources:lead_sources})
				}

				return leadSourceCategoryIds;
		}

		getLeadSourceCategorySelectionCounts = (result) => {
			    var leadSourceText = 'Lead Source Category('+result.lead_source_category.length+')';

				return leadSourceText;
		}

		_handleDatePicked = (date) => {
		    console.log('A date has been picked: ', date, this.state.endDatePressed, new Date(Moment(new Date).add(1, 'day').format('YYYY-MM-DD')));
		    this._hideDateTimePicker();
		    if(!this.state.isItStartDate && this.state.startDate>Moment(date).format('YYYY-MM-DD')){
		    	return Alert.alert('End date should not be before start date')
		    }
		    if(this.state.isItStartDate && this.state.endDate<Moment(date).format('YYYY-MM-DD')){
		    	return Alert.alert('Start date should not be after end date')
		    }
		    (this.state.isItStartDate)?
		    this.setState({startDate:Moment(date).format('YYYY-MM-DD')})
		    :
		    this.setState({endDate:Moment(date).format('YYYY-MM-DD')})
		  }

		_showDateTimePickerStartDate = () => this.setState({ isDateTimePickerVisible: true, isItStartDate:true })

		_showDateTimePickerEndDate = () => this.setState({ isDateTimePickerVisible: true, isItStartDate:false })

  		_hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false })

  		_notUpdateMessage = () => {
  			return Alert.alert('You can not update this event as u are not the creator!')
  		}

  		_createEventSubmit = () => {
  			//console.log("this.state.locationSelectedIds.length "+ this.state.locationSelectedIds.length)
  			let data = '';
  			if(this.isItEditEvent() && this.isEventActive() === 'true'){
  				 data = {
  				 	event_id : this.props.navigation.getParam('eventId'),
  					end_date : this.state.endDate
  				}
  			}else{
	  			if(this.state.locationSelectedIds && this.state.locationSelectedIds.length > 0){

	  			} else{
	  				return Alert.alert('No Location Selected')
	  			}

	  			if(this.state.leadSourceCategoryIds && this.state.leadSourceCategoryIds.length > 0){

	  			}else{
	  				return Alert.alert('No Lead Source Category Selected')
	  			}

	  			if(this.state.eventCategoryId){

	  			}else{
	  				return Alert.alert('No Events Category Selected')
	  			}
	  			if(this.isItEditEvent()){

	  			}else{
			  			if(this.state.eventNameText){

			  			}else{
			  				return Alert.alert('Event name is empty')
			  			}
	  			}

	  			if(this.state.startDate && this.state.startDate !== 'Start Date'){

	  			}else{
	  				return Alert.alert('Please select start date')
	  			}

	  			if(this.state.endDate && this.state.endDate !== 'End Date'){

	  			}else{
	  				return Alert.alert('Please select end date')
	  			}


  			/*let data = {
  					event_name: this.state.eventNameText,
  					location_ids:this.state.locationSelectedIds,
  					event_category_id: this.state.eventCategoryId,
  					lead_source_category: this.state.leadSourceCategoryIds,
  					start_date : this.state.startDate,
  					end_date : this.state.endDate
  				};*/
  				if(this.isItEditEvent()){
  					data = {
	  					location_ids:this.state.locationSelectedIds,
	  					event_category_id: this.state.eventCategoryId,
	  					lead_source_category: this.state.leadSourceCategoryIds,
	  					start_date : this.state.startDate,
	  					end_date : this.state.endDate,
	  					event_id : this.props.navigation.getParam('eventId')
  					}
  				}else{
  				 data = {
	  					event_name: this.state.eventNameText,
	  					location_ids:this.state.locationSelectedIds,
	  					event_category_id: this.state.eventCategoryId,
	  					lead_source_category: this.state.leadSourceCategoryIds,
	  					start_date : this.state.startDate,
	  					end_date : this.state.endDate
  					}
  				}

  			}

  				this.makeApiCallToCreateEvents(data);

  		}

  		

			onCreateOnUpdateEvent(isEdit) {
				//this.setState({ isLoading: false, isFetching: false });
			
			
				if(this.props.navigation.getParam('fromChild')) {
					this.props.navigation.state.params.onUpdateEvent();
				}
				this.props.navigation.goBack(null);
				this.props.updateEventDashboard(true);
				if(isEdit) {
					CleverTapPush.pushEvent(eventAddOrUpdateEvent.event, {'Type': eventAddOrUpdateEvent.keyType.valuesType.editEvent});
				}
				else {
					CleverTapPush.pushEvent(eventAddOrUpdateEvent.event, {'Type': eventAddOrUpdateEvent.keyType.valuesType.addEvent});
				}
				

			}

  		makeApiCallToCreateEvents = (data) =>{
  			this.setState({ isLoading: true, isFetching: true });
					if(this.isItEditEvent()){
						new RestClient().updateEvent(data)
						.then( (responseJson) => {
							if(responseJson.statusCode == 2002){
							this.onCreateOnUpdateEvent(true);
							}else if(responseJson.statusCode == 4000){
					        		 this.setState({
					                isLoading: false,
					                isFetching:false,});
					        		return Alert.alert(responseJson.message)
					        }
						})
						.catch(error => {
							console.log(error);
							this.setState({
			                isLoading: false,
			                isFetching:false,});
			        		return Alert.alert('Something went wrong')
						});
					}
					else {
						new RestClient().createEvent(data)
						.then( (responseJson) => {
							if(responseJson.statusCode == 2002){
							this.onCreateOnUpdateEvent(false);
							}else if(responseJson.statusCode == 4000){
					        		 this.setState({
					                isLoading: false,
					                isFetching:false,});
					        		return Alert.alert(responseJson.message)
					        }
						})
						.catch(error => {
							this.setState({
				                isLoading: false,
				                isFetching:false,});
				        		return Alert.alert('Someting went wrong')
						});
					}

  		}

   		FlatListItemSeparator = () => {
		    return (
		      <View
		        style={{
		          height: 0.6,
		          width: "100%",
		          backgroundColor: "#CFCFCF",

		        }}
		      />
		    );
		  }

		  straightLine = () => {
		    return (
		      <View
		        style={{
		          height: 1,
		          width: "100%",
		          backgroundColor: "#C0C0C0",

		        }}
		      />
		    );
		  }

	isItEditEvent() {
		return this.props.navigation.getParam("isEdit", false);
	}
	isEventActive() {
		return this.props.navigation.getParam("isEventActive", false);
	}

  render() {
		let {navigation} = this.props.navigation;
	

  	if (this.state.isLoading) {
            return (
                <View style={{flex: 1, backgroundColor:'#fff', alignItems : 'center', justifyContent: 'center'}}>
                    <ActivityIndicator />
                </View>
            );
        }

  	if(this.state.locationTypeModalVisible){
	  		return(

				  			<Modal
					          animationType="slide"
					          transparent={true}
					          visible={this.state.locationTypeModalVisible}
					          onRequestClose={() => this._locationType()}>
										<TouchableWithoutFeedback onPress={()=> {
											// this.setState({filter:''})
											// this.props.onCancel()
											this._locationType();
										}}>
										<View style={styles.overlay}>
										<TouchableWithoutFeedback onPress={()=> {

										}}>
										<View style={[styles.main]}>


						          <FlatList
						          	ItemSeparatorComponent = {this.FlatListItemSeparator}
						            data={this.state.locationArrayMain}
						            extraData={this.state}
						            renderItem={({ item, index }) =>

						            	<TouchableOpacity disabled={(this.isEventActive() === 'true')?true:false} onPress = {this.toggleLocationCheckbox.bind(this, index)}>

							            	<View style={{flexDirection: 'row', marginTop: 10, marginBottom: 10}}>

							            		{this.state.locationChecked[index].checked ?
							            		<Image
										        style={{ width: 30, height: 30, alignItems: 'center'}}
								              	source={require('../../../images/ic_checkbox_checked.png')}
										        /> :
										    	<Image
										        style={{ width: 30, height: 30, alignItems: 'center'}}
								              	source={require('../../../images/ic_checkbox_unchecked.png')}
										        />}
							            		<Text style= {{color: '#000000', fontWeight: 'normal', textAlign: 'left', paddingLeft: 10,fontSize: 18}}>{item.name}</Text>

							            	</View>
						            	</TouchableOpacity>

						        	}
						       	 keyExtractor= {(item, index) => index.toString() }
						          />

					          <TouchableOpacity style = {[(this.isEventActive() === 'true')?styles.submit_disabled:styles.submit, {marginTop:8}]} onPress={(this.isEventActive() === 'true')?null: this._locationSubmit}>
						          <View>
						          	<Text style = {{alignSelf: 'center', color: '#fff', fontSize: 18, alignItems: 'center', justifyContent: "center", marginTop: 7}}>Submit</Text>
						          </View>
					          </TouchableOpacity>

										</View>
										</TouchableWithoutFeedback>
			          </View>
			          </TouchableWithoutFeedback>
					        </Modal>

		  		);
  		}

  	if(this.state.leadSourceTypeModalVisible){
  		return(

				        <Modal
					          animationType="slide"
					          transparent={true}
					          visible={this.state.leadSourceTypeModalVisible}
					          onRequestClose={() => this._leadSourceType()}>
										<TouchableWithoutFeedback onPress={()=> {
 										 // this.setState({filter:''})
 										 // this.props.onCancel()
 										 this._leadSourceType();
 									 }}>
 									 <View style={styles.overlay}>
 									 <TouchableWithoutFeedback onPress={()=> {

 									 }}>
 									 <View style={[styles.main]}>
						          <FlatList
						          	ItemSeparatorComponent = {this.FlatListItemSeparator}
						            data={this.state.leadSourceArrayMain}
						            extraData={this.state}
						            renderItem={({ item, index:indexOut }) =>

							            	<View style={{ marginTop: 10, marginBottom: 10}}>
							            		<View style={{flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
								            		<TouchableOpacity onPress = {this.togglesourceCategoryCheckbox.bind(this, indexOut)}>
									            		{this.state.leadSourceCategoryChecked[indexOut].checked ?
									            		<Image
												        style={{ width: 30, height: 30, alignItems: 'center'}}
										              	source={require('../../../images/ic_checkbox_checked.png')}
												        /> :
												    	<Image
												        style={{ width: 30, height: 30, alignItems: 'center'}}
										              	source={require('../../../images/ic_checkbox_unchecked.png')}
												        />}
											        </TouchableOpacity>
											        <TouchableOpacity onPress = {this.openInnerLeadSource.bind(this, indexOut)}>
								            			<Text style= {{color: '#000000', fontWeight: 'normal', textAlign: 'left', paddingLeft: 10,fontSize: 18}}>{item.name}</Text>
								            		</TouchableOpacity>
							            		</View>

							            		{this.state.openInnerLeadSource[indexOut] ?

							            			<FlatList
											              data={item.subEnquirySource}
											              extraDate={this.state}
											              renderItem={({ item, index }) =>
											              <View>
											                <TouchableOpacity onPress = {this.togglesourceCategoryCheckboxInner.bind(this, index, indexOut)}>

													            	<View style={{flexDirection: 'row', marginTop: 10, marginBottom: 10, marginLeft: 15}}>

													            		{this.state.leadSourceCategoryChecked[indexOut].subEnquirySource[index].checked ?
													            		<Image
																        style={{ width: 30, height: 30, alignItems: 'center'}}
														              	source={require('../../../images/ic_checkbox_checked.png')}
																        /> :
																    	<Image
																        style={{ width: 30, height: 30, alignItems: 'center'}}
														              	source={require('../../../images/ic_checkbox_unchecked.png')}
																        />}
													            		<Text style= {{color: '#000000', fontWeight: 'normal', textAlign: 'left', paddingLeft: 10,fontSize: 18}}>{item.name}</Text>

													            	</View>
												            	</TouchableOpacity>
											              </View>
											            }
											            keyExtractor={(item, index) => index.toString()}
											          />
							            			: null
							            		}

							            	</View>


						        	}
						       	 keyExtractor= {(item, indexOut) => indexOut.toString() }
						          />

					          <TouchableOpacity style = {(this.isEventActive() === 'true')?styles.submit_disabled:styles.submit} onPress={(this.isEventActive() === 'true')?null:this._leadSourceCategorySubmit}>
						          <View style = {styles.submit} >
						          	<Text style = {{alignSelf: 'center', color: '#fff', fontSize: 18, alignItems: 'center', justifyContent: "center", marginTop: 7}}>Submit</Text>
						          </View>
					          </TouchableOpacity>
										</View>
	 								 </TouchableWithoutFeedback>
	 						 </View>
	 						 </TouchableWithoutFeedback>
	 							 </Modal>

		  		);
  		}
  		if(this.state.eventCategoryTypeModalVisible){
	  		return(

				  			<Modal
					          animationType="slide"
					          transparent={true}
					          visible={this.state.eventCategoryTypeModalVisible}
					          onRequestClose={() => this._eventCategoryType()}>
										<TouchableWithoutFeedback onPress={()=> {
 										 // this.setState({filter:''})
 										 // this.props.onCancel()
 										 this._eventCategoryType();
 									 }}>
 									 <View style={styles.overlay}>
 									 <TouchableWithoutFeedback onPress={()=> {

 									 }}>
 									 <View style={[styles.main]}>
						          <FlatList
						          	ItemSeparatorComponent = {this.FlatListItemSeparator}
						            data={this.state.eventCategoryArrayMain}
						            renderItem={({ item, index }) =>
						           		<TouchableOpacity onPress = {this._eventCategoryText.bind(this, item.id, item.name)}>
						            		<Text style= {{color: '#000000', fontWeight: 'normal', textAlign: 'left', fontSize: 18, paddingTop:10, paddingBottom:10}}>{item.name}</Text>
						            	</TouchableOpacity>

						        	}
						       	 keyExtractor= {(item, index) => index.toString() }
						          />
										</View>
	 								</TouchableWithoutFeedback>
	 							</View>
	 							</TouchableWithoutFeedback>
	 								</Modal>

		  		);
  		}

  		if(this.state.isDateTimePickerVisible){
  			return(
  				<View style={{ flex: 1 }}>
				        <DateTimePicker
				          isVisible={this.state.isDateTimePickerVisible}
				          onConfirm={this._handleDatePicked}
				          onCancel={this._hideDateTimePicker}
				          //minimumDate={Moment(new Date()).add(1,'day')}
				          minimumDate={new Date(Moment(new Date()).add(1, 'day').format('YYYY-MM-DD'))}
				        />
		      	</View>
  				);
  		}
  	//console.log("does this work", this.state.isBranchHeadCreator)
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#ffffff', paddingTop: (Platform.OS) === 'ios' ? 20 : 10,}}>
      		{
	      	  <View style={{ flex: 1, justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'row', width: '100%', marginLeft: 10}}>
						{<TouchableOpacity onPress = {() => {
							this.props.navigation.goBack(null)
							//	this.props.updateEventDashboard(true);
							}
						}>
	      		<Image
			        style={{ width: 30, height: 30, alignItems: 'center'}}
	              	source={require('../../../images/ic_go_back.png')}
			        />
			        </TouchableOpacity>}
	         	<Text style= {{color: '#000000', fontWeight: 'bold', textAlign: 'left', fontSize: 20}}>{this.isItEditEvent()?'Update Event' :'Add Events'}</Text>
		      </View>}
					{ this.straightLine()}
					
					

	      <View style={{ flex: 9, justifyContent: 'flex-start', alignItems: "flex-start", width: '100%', padding: 10}}>
							<View style={{justifyContent: "center", alignItems: "center",marginTop:10,paddingBottom:4, backgroundColor: '#ffffff', borderBottomWidth: 1, borderBottomColor: '#CFCFCF', width: '100%', marginTop: 20}}>
									<TextInput style={{width: '100%',height: 40, fontSize: 18, color:'#303030',borderBottomColor: '#8a8a8a', paddingLeft: 10}}
									placeholder="Enter Event Name"
									editable = {(this.isItEditEvent())?false:true}
									onChangeText={(eventNameText) => this.setState({eventNameText})}>{this.state.eventNameText}</TextInput>
							</View>

								<View style={{justifyContent: "center", alignItems: "center",marginTop:10,paddingBottom:10, backgroundColor: '#ffffff', borderBottomWidth: 1, borderBottomColor: '#CFCFCF', width: '100%', marginTop: 20}}>
		      	{<TouchableOpacity style={{alignSelf: 'stretch', flexDirection: 'row'}} onPress = {(this.isEventActive() === 'true')?null:this._eventCategoryType} >
		      		<Text style= {{flex:9, width:'100%', color: '#303030', fontWeight: 'normal', fontSize: 18, textAlign: 'left', paddingLeft: 10}}>{this.state.eventCategoryText}</Text>
		      		<Image
				        style={{flex:1, width: 24, height: 24, alignItems: 'flex-end', opacity:.2}}
		              	source={require('../../../images/ic_down_black.png')}
				        />
		      		</TouchableOpacity>}
		      	</View>

						<View style={{justifyContent: "center", alignItems: "center",paddingBottom:10, backgroundColor: '#ffffff', borderBottomWidth: 1, borderBottomColor: '#CFCFCF', width: '100%', marginTop: 20,}}>
		      		{<TouchableOpacity style={{alignSelf: 'stretch', flexDirection: 'row'}} onPress = {this._locationType} >
		      			{console.log("LocationId:"+ this.state.locationSelectedIds.length)}
			      		<Text style= {{flex:9, color: '#303030', fontWeight: 'normal', fontSize: 18, textAlign: 'left', paddingLeft: 10}}>{this.state.locationSelectedText}</Text>
			      		<Image
				        style={{flex:1, width: 24, height: 24, alignItems: 'flex-end', 	opacity:.2}}
		              	source={require('../../../images/ic_down_black.png')}

				        />
			      	</TouchableOpacity>}

		      	</View>

		      	<View style={{justifyContent: "center", marginTop:10,alignItems: "center", paddingBottom:10,backgroundColor: '#ffffff', borderBottomWidth: 1, borderBottomColor: '#CFCFCF', width: '100%', marginTop: 20}}>
			      	{<TouchableOpacity style={{alignSelf: 'stretch', flexDirection: 'row'}} onPress = {this._leadSourceType} >
			      		<Text style= {{flex:9, width: '100%', color: '#303030', fontWeight: 'normal', fontSize: 18, textAlign: 'left', paddingLeft: 10}}>{this.state.leadSourceText}</Text>
			      		<Image
				        style={{flex:1, width: 24, height: 24, alignItems: 'flex-end',	opacity:.2}}
		              	source={require('../../../images/ic_down_black.png')}

				        />
			      	</TouchableOpacity>}
		      	</View>

		      

		      	

		      	<View style={{justifyContent: "center", alignItems: "center", backgroundColor: '#ffffff', flexDirection: 'row', marginTop: 20}}>
			      	<View style={{flex:1 , justifyContent: "center", alignItems: "center", paddingBottom:10, backgroundColor: '#ffffff', borderBottomWidth: 1, borderBottomColor: '#CFCFCF'}}>
			      		{<TouchableOpacity onPress = {(this.isEventActive() === 'true')?null:this._showDateTimePickerStartDate} >
			      				<Text style= {{color: '#303030', fontWeight: 'normal', fontSize: 18}}>{this.state.startDate}</Text>
			      				<View style={{color: '#8a8a8a', height: 1}}></View>
		      				</TouchableOpacity>}
		      		</View>
		      		<View style={{flex:0.3}}>
		      		</View>
		      		<View style={{flex:1 , justifyContent: "center", paddingBottom:10, alignItems: "center", backgroundColor: '#ffffff', backgroundColor: '#ffffff', borderBottomWidth: 1, borderBottomColor: '#CFCFCF'}}>
		      		{<TouchableOpacity onPress = {this._showDateTimePickerEndDate} >
			      			<Text style= {{color: '#303030', fontWeight: 'normal', fontSize: 18}}>{this.state.endDate}</Text>
			      			<View style={{color: '#8a8a8a', height: 1}}></View>
			      			</TouchableOpacity>}
		      		</View>
		      	</View>
		      	{/*<View style={{flex:5 , justifyContent: "center", alignItems: "center", backgroundColor: '#ffffff'}}>

		      	</View>*/}
	      	</View>

		      <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#007fff', width: '100%'}}>
		      <TouchableOpacity style={{ flex:1, justifyContent: "center", alignItems: "center", width: '100%'}} onPress = {(this.state.isBranchHeadCreator)? this._createEventSubmit:this._notUpdateMessage}>
		      	{(this.isItEditEvent()) ?
		      	 <Text style= {{color: '#ffffff', fontWeight: 'normal', fontSize: 20}}>Update Event</Text>
			    :<Text style= {{color: '#ffffff', fontWeight: 'normal', fontSize: 20}}>Create Event</Text>
			 	}
			  </TouchableOpacity>
			  </View>
      </View>
    );
  }
}

/*const eventIdInformationHard = {
"event_name":"Event TEst Testing",
"location_ids":[1,2,3],
"event_category_id":1,
"lead_source_category":[
{"id":56 ,"lead_sources":[42]}, {"id":58, "lead_sources":[53,54]}
],
"start_date":"2019-04-13",
"end_date":"2019-04-18"
}*/


const styles = StyleSheet.create({
  container: {

  },
  submit: {
  	height: 40,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: '#007fff',
  },
  submit_disabled: {
  	height: 40,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: '#D3D3D3',
  },
	overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.8)',
    alignItems:'center',
    justifyContent:'center',
    padding:30

  },
	main: {
		backgroundColor:'#fff',
		borderRadius:5,
		width:'100%',
		maxHeight: '90%',
		padding:10,
	},

});


const mapStateToProps = state => {
	return {
			update: state.updateEventDasboard.update,
	}
}

const mapDispatchToProps = dispatch => {
return {
	updateEventDashboard: (update) => {
	dispatch(updateEventDashboard({update}))
}
}
}


export default connect(mapStateToProps, mapDispatchToProps)(AddEvents)