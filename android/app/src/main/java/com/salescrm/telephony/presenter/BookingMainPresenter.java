package com.salescrm.telephony.presenter;

import android.content.Context;
import android.support.graphics.drawable.VectorDrawableCompat;

import com.crashlytics.android.Crashlytics;
import com.salescrm.telephony.R;
import com.salescrm.telephony.db.car.AllInterestedCars;
import com.salescrm.telephony.model.C360InformationModel;
import com.salescrm.telephony.model.booking.BookingCarModel;
import com.salescrm.telephony.model.booking.BookingCustomerModel;
import com.salescrm.telephony.model.booking.BookingDiscountModel;
import com.salescrm.telephony.model.booking.BookingEditWrapperModel;
import com.salescrm.telephony.model.booking.BookingExchFinModel;
import com.salescrm.telephony.model.booking.BookingMainModel;
import com.salescrm.telephony.model.booking.BookingPriceBreakupModel;
import com.salescrm.telephony.model.booking.BookingSectionModel;
import com.salescrm.telephony.model.booking.BookingVinAllocationModel;
import com.salescrm.telephony.model.booking.CarBookingFormHolderModel;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.booking.CarBookingForm;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class BookingMainPresenter {
    private static final String TAG = "BookingMainPresenter";
    private BookingMainView bookingMainView;
    private int totalBooking = 1;
    private int currentBookingPage = 0;
    private List<CarBookingFormHolderModel> carBookingFormHolderModels = new ArrayList<>();
    private AllInterestedCars allInterestedCar;
    private C360InformationModel c360InformationModel;
    private VectorDrawableCompat iconFilled;
    private VectorDrawableCompat iconNotFilled;
    private int stageId;
    private BookingEditWrapperModel editWrapperModel;
    private Realm realm;

    public BookingMainPresenter(Realm realm ,Context context, BookingMainView bookingMainView, int totalBooking, int stage_id) {
        this.realm = realm;
        this.bookingMainView = bookingMainView;
        this.totalBooking = totalBooking;
        this.stageId = stage_id;
        this.allInterestedCar = C360InformationModel.getInstance().getAllInterestedCars();
        this.c360InformationModel = C360InformationModel.getInstance();
        this.iconFilled = VectorDrawableCompat.create(context.getResources(), R.drawable.ic_check_color_24dp, context.getTheme());
        this.iconNotFilled = VectorDrawableCompat.create(context.getResources(), R.drawable.ic_error_24dp, context.getTheme());
        if(allInterestedCar == null) {
            bookingMainView.restart();
            return;
        }
        updateBookingEditWrapper();
        updateStageForEditId();
    }

    private void updateBookingEditWrapper() {
        boolean isCarEditable = true,
                isBookingAmountEditable = stageId == WSConstants.CarBookingStages.BOOK_CAR,
                isInvoiceEditable = stageId == WSConstants.CarBookingStages.INVOICE_PENDING,
                isDeliveryDetailsEditable = false,
                isCustomerDetailsEditable = true,
                isPriceDetailsEditable = true,
                isDiscountEditable = true,
                isExchFinEditable = true,
                isSubmitButtonVisible = true;
        if(stageId == WSConstants.CarBookingStages.CUSTOM_CAR_BOOKED_EDIT) {
            switch (Util.getInt(allInterestedCar.getCar_stage_id())) {
                case WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING:
                case WSConstants.CarBookingStages.DELIVERED:
                case WSConstants.CarBookingStages.CAR_CANCELLED:
                        isCarEditable = false;
                        isDeliveryDetailsEditable  = false;
                        isCustomerDetailsEditable = false;
                        isPriceDetailsEditable = false;
                        isDiscountEditable = false;
                        isExchFinEditable = false;
                        isSubmitButtonVisible =false;
                        break;

            }
        }
        else if(stageId == WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING) {
            isCarEditable = false;
            isDeliveryDetailsEditable = true;
            isCustomerDetailsEditable = false;
            isPriceDetailsEditable = false;
            isDiscountEditable =false;
            isExchFinEditable = false;
            isSubmitButtonVisible = true;
        }
        editWrapperModel = new BookingEditWrapperModel(isCarEditable,
                isBookingAmountEditable,
                isInvoiceEditable,
                isDeliveryDetailsEditable,
                isCustomerDetailsEditable,
                isPriceDetailsEditable,
                isDiscountEditable,
                isExchFinEditable,
                isSubmitButtonVisible);
    }

    private void updateStageForEditId() {
        if(stageId == WSConstants.CarBookingStages.CUSTOM_CAR_BOOKED_EDIT){
            switch (Util.getInt(allInterestedCar.getCar_stage_id())) {
                case WSConstants.CarBookingStages.CAR_BOOKED_HALF_PAYMENT:
                    stageId = WSConstants.CarBookingStages.BOOK_CAR;
                    break;
                case WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING:
                    stageId = WSConstants.CarBookingStages.INVOICE_PENDING;
                    break;
                case WSConstants.CarBookingStages.DELIVERED:
                    stageId = WSConstants.CarBookingStages.INVOICED_DELIVERY_PENDING;
                    break;
            }
        }
    }

    public void init() {

        if(allInterestedCar == null) {
            bookingMainView.restart();
            return;
        }
        for (int i=0;i<totalBooking;i++) {
            CarBookingFormHolderModel carBookingFormHolderModel = new CarBookingFormHolderModel();
            carBookingFormHolderModel.setBookingMainModel(getBookingModel());
            carBookingFormHolderModel.setCarBookingForm(new CarBookingForm());
            carBookingFormHolderModels.add(carBookingFormHolderModel);
        }
        bookingMainView.init(getCurrentBookingMainModel());
        bookingMainView.updateViewOnAction(carBookingFormHolderModels.get(0), totalBooking,currentBookingPage, false, false);

    }

    private BookingMainModel getBookingModel() {
        BookingMainModel bookingMainModel = new BookingMainModel();

        BookingCarModel bookingCarModel =  new BookingCarModel();
        BookingCustomerModel bookingCustomerModel =  new BookingCustomerModel();
        BookingPriceBreakupModel bookingPriceBreakupModel =  new BookingPriceBreakupModel();
        BookingDiscountModel bookingDiscountModel =  new BookingDiscountModel();
        BookingExchFinModel bookingExchFinModel =  new BookingExchFinModel();
        BookingVinAllocationModel bookingVinAllocationModel = new BookingVinAllocationModel();


        bookingMainModel.setBookingEditWrapperModel(editWrapperModel);
        bookingMainModel.setRealm(realm);

        bookingMainModel.setIconSuccess(iconFilled);
        bookingMainModel.setIconError(iconNotFilled);
        bookingMainModel.setBookingCarModel(bookingCarModel);
        bookingMainModel.setBookingCustomerModel(bookingCustomerModel);
        bookingMainModel.setBookingPriceBreakupModel(bookingPriceBreakupModel);
        bookingMainModel.setBookingDiscountModel(bookingDiscountModel);
        bookingMainModel.setBookingExchFinModel(bookingExchFinModel);
        bookingMainModel.setBookingSectionModel( new BookingSectionModel());
        bookingMainModel.setBookingVinAllocationModel(bookingVinAllocationModel);


        bookingMainModel.setStageId(stageId);
        bookingCarModel.setModelName(allInterestedCar.getModel());
        bookingCarModel.setModelId(Util.getInt(allInterestedCar.getIntrestedCarModelId()));

        bookingCarModel.setVariantId(Util.getInt(allInterestedCar.getIntrestedCarVariantId()));
        bookingCarModel.setVariantName(allInterestedCar.getVariant());

        bookingCarModel.setFuelTypeId(Util.getInt(allInterestedCar.getFuelId()));
        bookingCarModel.setFuelName(allInterestedCar.getFuelType());

        bookingCarModel.setColorTypeId(Util.getInt(allInterestedCar.getIntrestedCarColorId()));
        bookingCarModel.setColorName(allInterestedCar.getColor());

        bookingCarModel.setDeliveryChallan(allInterestedCar.getDeliveryChallan());
        bookingCarModel.setDeliveryDate(allInterestedCar.getDeliveryDate());
        if(allInterestedCar.getInvoiceDetails()!=null) {
            bookingCarModel.setInvoiceMobileNumber(allInterestedCar.getInvoiceDetails().getInvoice_mob_no());
            bookingCarModel.setInvoiceDate(allInterestedCar.getInvoiceDetails().getInvoice_date());
            System.out.println("Invoice date:"+allInterestedCar.getInvoiceDetails().getInvoice_date());
            bookingCarModel.setVinNumber(allInterestedCar.getInvoiceDetails().getVin_no());

        }
        bookingCarModel.setBookingAmount(allInterestedCar.getBooking_amount());

        if(C360InformationModel.getInstance().getCustomerDetails()!=null) {
            //Special case
            bookingCustomerModel.setCustomerType(WSConstants.customerType.indexOf(C360InformationModel
                    .getInstance()
                    .getCustomerDetails()
                    .getCustomerDetails()
                    .getType_of_customer()));
        }

        if(allInterestedCar.getBookingCustomerDB()!=null) {
            bookingCustomerModel.setBookingName(allInterestedCar.getBookingCustomerDB().getBookingName());
            bookingCustomerModel.setDateOfBirth(allInterestedCar.getBookingCustomerDB().getDateOfBirth());
            bookingCustomerModel.setAddress(allInterestedCar.getBookingCustomerDB().getAddress());
            bookingCustomerModel.setPinCode(allInterestedCar.getBookingCustomerDB().getPinCode());
            bookingCustomerModel.setCareOfType(allInterestedCar.getBookingCustomerDB().getCareOfType());
            bookingCustomerModel.setCareOf(allInterestedCar.getBookingCustomerDB().getCareOf());



            bookingCustomerModel.setPanNumber(allInterestedCar.getBookingCustomerDB().getPanNumber());
            bookingCustomerModel.setAadhaar(allInterestedCar.getBookingCustomerDB().getAadhaar());
            bookingCustomerModel.setGst(allInterestedCar.getBookingCustomerDB().getGst());
        }
        if(allInterestedCar.getBookingPriceBreakupDB()!=null) {
            bookingPriceBreakupModel.setExShowRoomPrice(allInterestedCar.getBookingPriceBreakupDB().getExShowRoomPrice());
            bookingPriceBreakupModel.setInsurance(allInterestedCar.getBookingPriceBreakupDB().getInsurance());
            bookingPriceBreakupModel.setRegistration(allInterestedCar.getBookingPriceBreakupDB().getRegistration());
            bookingPriceBreakupModel.setAccessories(allInterestedCar.getBookingPriceBreakupDB().getAccessories());
            bookingPriceBreakupModel.setExtendedWarranty(allInterestedCar.getBookingPriceBreakupDB().getExtendedWarranty());
            bookingPriceBreakupModel.setOthers(allInterestedCar.getBookingPriceBreakupDB().getOthers());

        }

        if(allInterestedCar.getBookingDiscountDB()!=null) {
            bookingDiscountModel.setCorporate(allInterestedCar.getBookingDiscountDB().getCorporate());
            bookingDiscountModel.setExchangeBonus(allInterestedCar.getBookingDiscountDB().getExchangeBonus());
            bookingDiscountModel.setLoyaltyBonus(allInterestedCar.getBookingDiscountDB().getLoyaltyBonus());
            bookingDiscountModel.setOemScheme(allInterestedCar.getBookingDiscountDB().getOemScheme());
            bookingDiscountModel.setDealer(allInterestedCar.getBookingDiscountDB().getDealer());
            bookingDiscountModel.setAccessories(allInterestedCar.getBookingDiscountDB().getAccessories());
        }

        if(allInterestedCar.getExchFinDB()!=null) {
            bookingExchFinModel.setExchange(allInterestedCar.getExchFinDB().getExchange());
            bookingExchFinModel.setFinance(allInterestedCar.getExchFinDB().getFinance());

        }


        if(allInterestedCar.getBookingAllocationDB()!=null) {
            bookingVinAllocationModel.setVin_no(allInterestedCar.getBookingAllocationDB().getVin_no());
            bookingVinAllocationModel.setVin_allocation_status(allInterestedCar.getBookingAllocationDB().getVin_allocation_status());
            bookingVinAllocationModel.setVin_allocation_status_id(allInterestedCar.getBookingAllocationDB().getVin_allocation_status_id());

        }
        System.out.println("allInterestedCar.getVin_allocation_status_id(): "+allInterestedCar.getVin_allocation_status_id());
        bookingMainModel.setVin_allocation_status_id(allInterestedCar.getVin_allocation_status_id());
        bookingMainModel.setVin_allocation_status(allInterestedCar.getVin_allocation_status());
        bookingMainModel.setVin_no(allInterestedCar.getVin_no());

        return bookingMainModel;
    }

    public BookingMainModel getCurrentBookingMainModel() {
        return carBookingFormHolderModels.get(currentBookingPage).getBookingMainModel();
    }

    public AllInterestedCars getAllInterestedCars() {
        return allInterestedCar;
    }

    public C360InformationModel getC360InformationModel() {
        return c360InformationModel;
    }


    public void updateCurrentBookingPage(boolean isActionNext){
        boolean isClose = false;
        boolean isActionSubmit = false;
        System.out.println(TAG+"Action:"+isActionNext);
        System.out.println(TAG+"CurrentBookingPage:"+currentBookingPage);
        System.out.println(TAG+"TotalBookingPage:"+totalBooking);
        if(isActionNext) {
            if(currentBookingPage<totalBooking-1) {
                currentBookingPage++;
            }
            else {
                isActionSubmit = true;
            }
        }
        else {
            if(currentBookingPage>0) {
                currentBookingPage--;
            }
            else {
                isClose = true;
            }
        }

        bookingMainView.updateViewOnAction(
                carBookingFormHolderModels.get(currentBookingPage),
                totalBooking,
                currentBookingPage,
                isClose,
                isActionSubmit);


    }

    public List<CarBookingFormHolderModel> getCarBookingFormHolderModels() {
        return carBookingFormHolderModels;
    }



    public void copyFromBookingOne() {
        BookingMainModel bookingMainModelSource = carBookingFormHolderModels.get(0).getBookingMainModel();

        BookingMainModel bookingMainModel = carBookingFormHolderModels.get(currentBookingPage).getBookingMainModel();

        bookingMainModel.getBookingCarModel().copy(bookingMainModelSource.getBookingCarModel());
        bookingMainModel.getBookingCustomerModel().copy(bookingMainModelSource.getBookingCustomerModel());
        bookingMainModel.getBookingPriceBreakupModel().copy(bookingMainModelSource.getBookingPriceBreakupModel());
        bookingMainModel.getBookingDiscountModel().copy(bookingMainModelSource.getBookingDiscountModel());

        carBookingFormHolderModels.get(currentBookingPage)
                .setBookingMainModel(bookingMainModel);
        bookingMainView.onCopyFromBookingOne(carBookingFormHolderModels.get(currentBookingPage));
    }

    public interface BookingMainView{
        void onCopyFromBookingOne(CarBookingFormHolderModel carBookingFormHolderModel);
        void init(BookingMainModel bookingMainModel);
        void updateViewOnAction(CarBookingFormHolderModel carBookingForm, int totalBooking, int current, boolean isClose, boolean isActionSubmit);

        void restart();
    }
}
