package com.salescrm.telephony.response;

/**
 * Created by bharath on 28/7/16.
 */
public class Error
{
    private String details;

    private String type;

    public String getDetails ()
    {
        return details;
    }

    public void setDetails (String details)
    {
        this.details = details;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [details = "+details+", type = "+type+"]";
    }
}
