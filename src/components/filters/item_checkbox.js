import React, { Component } from 'react'
import { View, Text, Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Alert} from 'react-native'

import { CheckBox } from 'react-native-elements'

export default class ItemCheckBox extends Component {
  constructor(props) {
    super(props);
    this.state= {checked:isFilterItemExists(this.props.key_name, this.props.id, this.props.sub_category)};

  }
  render() {
    if(this.props.select_all) {
      return(
      <CheckBox
      title='Select All'
      checkedColor ='#007fff'
      iconRight
      right
      textStyle = {{ color:'#007fff',fontWeight:'bold'}}
      containerStyle= {{backgroundColor:'#fff', borderWidth:0, margin:4}}
      checked={this.state.checked}
      onPress={() => {
        this.props.selectAllFilter(!this.state.checked);
        this.setState({checked: !this.state.checked});
      }
      }
      />);
    }
    return(
      <CheckBox
        title={this.props.title}
        checkedColor ='#007fff'
        iconRight
        right
        textStyle = {{ flex:1,textAlign:'left'}}
        containerStyle= {{backgroundColor:'#fff', borderWidth:0, margin:4}}
        checked={isFilterItemExists(this.props.key_name, this.props.id, this.props.sub_category)}
        onPress={() => {
          this.setState({checked: !this.state.checked})
          this.props.update();
          addOrRemoveETVBRFilterItem(this.props.key_name, this.props.id, this.props.sub_category,!isFilterItemExists(this.props.key_name, this.props.id, this.props.sub_category));
      }}
        />
      );
  }
}
