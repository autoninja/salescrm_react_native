package com.salescrm.telephony.activity;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.EtvbrCardAdapter;
import com.salescrm.telephony.adapter.EtvbrGMCardAdpter;
import com.salescrm.telephony.adapter.EtvbrGmSmCardAdapter;
import com.salescrm.telephony.adapter.EtvbrSMCardAdpter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.db.team_dashboard_gm_db.ResultSM;
import com.salescrm.telephony.fragments.EtvbrCarsFragment;
import com.salescrm.telephony.fragments.EtvbrLocationChildFragment;
import com.salescrm.telephony.fragments.EtvbrLocationFragment;
import com.salescrm.telephony.fragments.retExcFin.RefCarsFragment;
import com.salescrm.telephony.fragments.retExcFin.RefLocationChildFragment;
import com.salescrm.telephony.preferences.Preferences;

import java.util.ArrayList;
import java.util.HashMap;

import io.realm.Realm;

/**
 * Created by prateek on 9/11/17.
 */

public class EtvbrLocationActivity extends AppCompatActivity {
    Fragment fragment;
    FragmentTransaction transaction;
    private TextView humanImage;
    private TextView carImage;
    private Drawable drawableHumanColor, drawableCarColor, drawableHumanTrans, drawableCarTrans;
    private Preferences pref;
    private Realm realm;
    Toolbar toolbar;
    TextView customTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.etvbr_container_activity_layout);
        realm = Realm.getDefaultInstance();
        toolbar = (Toolbar) findViewById(R.id.etvbr_toolbar);
        toolbar.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        customTitle = (TextView) findViewById(R.id.custom_title);
        //getSupportActionBar().setTitle("" + getIntent().getExtras().getString("location_name") + "");
        customTitle.setText("" + getIntent().getExtras().getString("location_name") + "");
        humanImage = (TextView)findViewById(R.id.human_image);
        carImage = (TextView) findViewById(R.id.car_image);
        drawableHumanColor = VectorDrawableCompat
                .create(this.getResources(), R.drawable.ic_human_color, null);
        drawableHumanTrans = VectorDrawableCompat
                .create(this.getResources(), R.drawable.ic_human_transparent, null);
        drawableCarColor = VectorDrawableCompat
                .create(this.getResources(), R.drawable.ic_car_color, null);
        drawableCarTrans = VectorDrawableCompat
                .create(this.getResources(), R.drawable.ic_car_transparent, null);

        humanImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableHumanColor, null, null);
        carImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableCarTrans, null, null);
        humanImage.setTextColor(Color.parseColor("#FF113059"));
        carImage.setTextColor(Color.parseColor("#FF787878"));
        //fragment = new EtvbrGMFragment();
        if(this.getIntent().getExtras() != null) {
            pref.setLocationId(this.getIntent().getExtras().getInt("location_id"));
        }
        if(this.getIntent().getExtras() != null && this.getIntent().getExtras().getString("adapter").equalsIgnoreCase("etvbr")) {
            fragment = new EtvbrLocationChildFragment();
        }else{
            fragment = new RefLocationChildFragment();
        }
        callFragment(fragment);
        carImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TEAM_DASHBOARD_FLAG = 1;
                //logEvent("ETVBR Cars");

                if(getIntent().getExtras() != null &&getIntent().getExtras().getString("adapter").equalsIgnoreCase("etvbr")) {
                    fragment = new EtvbrCarsFragment();
                }else{
                    fragment = new RefCarsFragment();
                }
                callFragment(fragment);
                humanImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableHumanTrans, null, null);
                carImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableCarColor, null, null);
                humanImage.setTextColor(Color.parseColor("#FF787878"));
                carImage.setTextColor(Color.parseColor("#FF113059"));
            }
        });

        humanImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TEAM_DASHBOARD_FLAG = 0;
               // logEvent("ETVBR Teams");
                //fragment = new EtvbrGMFragment();

                if(getIntent().getExtras() != null &&getIntent().getExtras().getString("adapter").equalsIgnoreCase("etvbr")) {
                    fragment = new EtvbrLocationChildFragment();
                }else{
                    fragment = new RefLocationChildFragment();
                }
                humanImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableHumanColor, null, null);
                carImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableCarTrans, null, null);
                humanImage.setTextColor(Color.parseColor("#FF113059"));
                carImage.setTextColor(Color.parseColor("#FF787878"));
                callFragment(fragment);

            }
        });
    }

        private void callFragment(Fragment fragment) {
            transaction = getSupportFragmentManager().beginTransaction();
            if(getIntent().getExtras() != null &&getIntent().getExtras().getString("adapter").equalsIgnoreCase("etvbr")) {
                transaction.replace(R.id.etvbr_contaner, fragment); // give your fragment container id in first parameter
            }else {
                transaction.replace(R.id.etvbr_contaner, fragment);
            }
            transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
            transaction.commit();
            if(fragment instanceof EtvbrLocationChildFragment) {
                pushCleverTap(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_TEAMS);

            }
            else if(fragment instanceof EtvbrCarsFragment) {
                pushCleverTap(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_CARS);
            }
            else if(fragment instanceof RefLocationChildFragment) {
                pushCleverTap(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_TEAMS);

            }
            else if(fragment instanceof RefCarsFragment) {
                pushCleverTap(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_CARS);

            }
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void pushCleverTap(String event) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, event);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
    }

}
