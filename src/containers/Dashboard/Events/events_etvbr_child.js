import React, { Component } from 'react'
import { Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList, StyleSheet, Modal, NativeModules, TouchableWithoutFeedback} from 'react-native'
import styless from '../../../styles/styles'
import PercentageSwitch from '../../../components/uielements/PercentageSwitch'

const styles = StyleSheet.create({
  container: {
  	flex: 1,
  	justifyContent: 'center',
  	alignItems: 'center',
		backgroundColor: '#fff',
		paddingTop: (Platform.OS) === 'ios' ? 20 : 10
  },
  etvbrTitle:{
	flex:1,
	textAlign: 'center',
  },
  locationTitle:{
	flex:1,
	textAlign: 'left',
	fontSize: 16,
	fontWeight: 'bold',
	color : '#2e5e86',
	textDecorationLine:'underline'
  },
  etvbrNumbers:{
	flex:1,
	textAlign: 'center',
	fontSize: 18,
	textDecorationLine:'underline',
  },
  etvbrTotal:{
	flex:1,
	textAlign: 'center',
	fontSize: 18,
	fontWeight: 'bold',
	color : '#2e5e86',
  },
  etvbrTotalTitle:{
	flex:1,
	textAlign: 'left',
	fontSize: 16,
  paddingLeft:5,
	fontWeight: 'bold',
	color : '#2e5e86',
  },
  locationHeader:{
	fontSize: 16,
	marginLeft: 10,
	fontWeight: 'bold',
	color : '#ffffff',
  },
  changeCategory:{
	textAlign: 'right',
	fontSize: 16,
	fontWeight: 'bold',
	color : '#2e5e86',
	textDecorationLine:'underline'
  },
  submit: {
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: '#007fff',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.8)',
    alignItems:'center',
    justifyContent:'center',
    padding:30

  },
  main: {
    backgroundColor:'#fff',
    borderRadius:5,
    width:'100%',
    maxHeight: '90%',
    padding:10,
  },
  etvbrTitle:{
  flex:1,
  textAlign: 'center',
  },
  locationTitle:{
  flex:1,
  textAlign: 'left',
  fontSize: 16,
  color : '#007fff',
  },
  etvbrNumbers:{
  textAlign: 'center',
  fontSize: 18,
  textDecorationLine:'underline',
  },
  etvbrNumberHolder:{
  flex:1,
  borderLeftColor: '#CFCFCF',
  borderLeftWidth: 0.3,
  justifyContent: 'center',
  alignItems: 'center'
  },

});


export default class EventsEtvbrChild extends Component {

constructor(props){
		super(props);
		this.state = {
			isEventChangedPopUpOpen: false,
			eventCategoryIndex:0,
			show_event_percentage:false,
		};


	}

	_editEvents(eventId, isEventActive, isDead){
		if(isDead === 'false'){
			// if(this.props.fromAndroid) {
	    //               NativeModules.ReactNativeToAndroid.openRNEditEvents(eventId, isEventActive);
	    //             }
			this.props.navigation.navigate('AddEvents',{eventId, isEventActive, isEdit:true, fromChild : true, onUpdateEvent:()=> {
				this.props.navigation.goBack();
			}},)
		}
    }

    getDetails(navigation, clicked_val, locationId, eventId, title){
			var data = {info:{start_date: navigation.getParam('date'), locationId, clicked_val, eventId, title}}
			navigation.navigate('ETVBRDetails', {data, isEvents:true});
    		// if(this.props.fromAndroid) {

        //                 NativeModules.ReactNativeToAndroid.eventEtvbrDetails(text, locationId+"", eventId+"", name, this.props.info.date);
        //               }
        //               else {
        //                 //this.props.navigation.goBack()
        //               }
    }

     _closePopUp = () => {
   			//console.log("hellooo Event"+this.state.isEventChangedPopUpOpen)
   			this.setState({isEventChangedPopUpOpen: !this.state.isEventChangedPopUpOpen});
   		}

	_selectedEventCategory = (index) => {
		this.setState({isEventChangedPopUpOpen: !this.state.isEventChangedPopUpOpen, eventCategoryIndex:index});
	}
  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.6,
          width: "100%",
          backgroundColor: "#CFCFCF",

        }}
      />
    );
  }

	render(){

		const {navigation} = this.props;
		let showButtons = false;
    if(this.props.fromAndroid) {
      showButtons = this.props.info.showButtons;
      eventsSingleLocation = this.props.info.locationArray;
    }
    else {
      showButtons = navigation.getParam('showButtons', false);
      eventsSingleLocation = navigation.getParam('locationArray', null);
    }


		if(this.state.isEventChangedPopUpOpen){
				return(

				  			<Modal
					          animationType="slide"
					          transparent={true}
					          visible={this.state.isEventChangedPopUpOpen}
					          onRequestClose={() => this._closePopUp()}>
                    <TouchableWithoutFeedback onPress={()=> {
											// this.setState({filter:''})
											// this.props.onCancel()
											this._closePopUp();
										}}>
										<View style={styles.overlay}>
										<TouchableWithoutFeedback onPress={()=> {

										}}>
										<View style={[styles.main]}>

						          <FlatList
						          	ItemSeparatorComponent = {this.FlatListItemSeparator}
						            data={eventsSingleLocation.event_categories}
						            renderItem={({ item, index }) =>
						           		<TouchableOpacity onPress = {this._selectedEventCategory.bind(this, index)}>
						            		<Text style= {{color: '#000000', fontWeight: 'normal', textAlign: 'left', fontSize: 18, paddingTop:10, paddingBottom:10}}>{item.event_category_name}</Text>
						            	</TouchableOpacity>

						        	}
						       	 keyExtractor= {(item, index) => index.toString() }
						          />
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                  </TouchableWithoutFeedback>
                    </Modal>

		  		);
			}

		return(
			<View style= {styles.container}>
						{(showButtons) ?
								<View style={{justifyContent: "flex-start", alignItems: 'center', backgroundColor: '#2e5e86', width: '100%', flexDirection: 'row', padding:20}}>
									<View style={{justifyContent: "center", alignItems: 'center'}}>
									{<TouchableOpacity
                    onPress = {() => {
                        this.props.navigation.goBack()
                    }}>
						      		<Image
								        style={{ width: 24, height: 24, alignItems: 'center'}}
						              	source={require('../../../images/ic_back_white.png')}
								        />
								        </TouchableOpacity>}
								    </View>
									<Text style={styles.locationHeader}> {eventsSingleLocation.loation_name}</Text>
								</View>
								: null}
              <View style={{flex:1, padding:4, width:'100%'}}>
						{(showButtons) ?
							<View style={{ width: '100%', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', padding:10}}>
									<View />

									<View style={{ alignItems: 'center', justifyContent: 'center',alignSelf:'flex-end', flex:2, alignItems: 'flex-end', marginRight:1}}>
									
												<PercentageSwitch 
												 value={ this.state.show_event_percentage }
												onValueChange={(show_event_percentage) => {
													this.setState({show_event_percentage});
										 		}}
												/>

									</View>
								</View> : null}

						{(showButtons) ?
								<View style={{ width: '100%', alignItems: 'center', justifyContent: 'space-between', padding:10,flexDirection: 'row', borderWidth: 1,borderColor: '#C0C0C0', backgroundColor: '#dae7f2'}}>
									<Text style={{alignItems: 'center', fontSize: 18}}> {eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_name}</Text>
									<View style={{justifyContent: 'center'}}>
										<TouchableOpacity onPress = {this._closePopUp}>
											<Text style={styles.changeCategory}>Change</Text>
										</TouchableOpacity>
									</View>
								</View>  : null}

						{(showButtons) ? 	<View style={{ width: '100%', alignItems: 'center', backgroundColor:'#CFCFCF', justifyContent: 'center', flexDirection: 'row', borderWidth: 1,borderColor: '#C0C0C0', height:40}}>
  							<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage)? 'E%':'E'}</Text>
  									<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage)? 'T%':'T'}</Text>
  									<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage)? 'V%':'V'}</Text>
  									<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage)? 'B%':'B'}</Text>
  									<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage)? 'R%':'R'}</Text>
  									<Text style={styles.etvbrTitle}>{(this.state.show_event_percentage)? 'L%':'L'}</Text>
  						</View> : null}



						<View style={{ flex:1, width: '100%', justifyContent: 'center'}}>

							<FlatList
					              data = {eventsSingleLocation.event_categories[this.state.eventCategoryIndex].events}
					              extraDate={this.state}
					              renderItem={({item, index}) => {
					              	return(
						              	<View style={{marginTop: 5, marginBottom: 5}}>

						              		<TouchableOpacity onPress={this._editEvents.bind(this, item.info.id+'', item.info.is_active, item.info.is_dead)}>
								              	<View style={{flex:1, marginLeft: 5}}>
								              		<Text style={styles.locationTitle} >{item.info.name}</Text>
								              	</View>
							              	</TouchableOpacity>
							              	{(this.state.show_event_percentage)?

                      <View style={{width: '100%', marginTop:5, alignItems: 'center', borderColor:'#CFCFCF', borderWidth:0.5,justifyContent: 'center', flexDirection: 'row', backgroundColor: '#fff', height: 40,}}>
                        <View style= {styles.etvbrNumberHolder}>
                          <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                            <Text style={[styles.etvbrNumbers,{textDecorationLine:'none'}]}>{item.rel.enquiries}</Text>
                          </View>
                        </View>
                        <View style= {styles.etvbrNumberHolder}>
                          <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                            <Text style={[styles.etvbrNumbers,{textDecorationLine:'none'},{textDecorationLine:'none'}]}>{item.rel.tdrives}</Text>
                          </View>
                        </View>
                        <View style= {styles.etvbrNumberHolder}>
                          <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                            <Text style={[styles.etvbrNumbers,{textDecorationLine:'none'}]}>{item.rel.visits}</Text>
                          </View>
                        </View>
                        <View style= {styles.etvbrNumberHolder}>
                          <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                            <Text style={[styles.etvbrNumbers,{textDecorationLine:'none'}]}>{item.rel.bookings}</Text>
                          </View>
                        </View>
                        <View style= {styles.etvbrNumberHolder}>
                          <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                            <Text style={[styles.etvbrNumbers,{textDecorationLine:'none'}]}>{item.rel.retails}</Text>
                          </View>
                        </View>
                        <View style= {styles.etvbrNumberHolder}>
                          <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                            <Text style={[styles.etvbrNumbers,{textDecorationLine:'none'}]}>{item.rel.lost_drop}</Text>
                          </View>
                        </View>

                    </View>


											:


                      <View style={{width: '100%', marginTop:5, alignItems: 'center', borderColor:'#CFCFCF', borderWidth:0.5,justifyContent: 'center', flexDirection: 'row', backgroundColor:'#fff', height: 40,}}>

                           <View style= {styles.etvbrNumberHolder}>
                             <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                               <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center',}} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Enquiries', eventsSingleLocation.location_id, item.info.id, item.info.name)}>
                                 <Text style={styles.etvbrNumbers}>{item.abs.enquiries}</Text>
                               </TouchableOpacity>
                             </View>
                           </View>
                           <View style= {styles.etvbrNumberHolder}>
                             <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                                <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center',}} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Test Drives', eventsSingleLocation.location_id, item.info.id, item.info.name)}>
                                   <Text style={styles.etvbrNumbers}>{item.abs.tdrives}</Text>
                               </TouchableOpacity>
                             </View>
                           </View>
                           <View style= {styles.etvbrNumberHolder}>
                             <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                               <TouchableOpacity style={{flex:1, alignItems: 'center', justifyContent: 'center',}} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Visits', eventsSingleLocation.location_id, item.info.id, item.info.name)}>
                                 <Text style={styles.etvbrNumbers}>{item.abs.visits}</Text>
                               </TouchableOpacity>
                             </View>
                           </View>
                           <View style= {styles.etvbrNumberHolder}>
                             <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                             <TouchableOpacity style={{flex:1, alignItems: 'center', justifyContent: 'center',}} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Bookings', eventsSingleLocation.location_id, item.info.id, item.info.name)}>
                               <Text style={styles.etvbrNumbers}>{item.abs.bookings}</Text>
                             </TouchableOpacity>
                             </View>
                           </View>

                         <View style= {styles.etvbrNumberHolder}>
                           <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                             <TouchableOpacity style={{flex:1, alignItems: 'center', justifyContent: 'center'}} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Retails', eventsSingleLocation.location_id, item.info.id, item.info.name)}>
                               <Text style={styles.etvbrNumbers}>{item.abs.retails}</Text>
                             </TouchableOpacity>
                           </View>
                         </View>
                         <View style= {styles.etvbrNumberHolder}>
                           <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                             <TouchableOpacity style={{flex:1, alignItems: 'center', justifyContent: 'center'}} onLongPress={this.getDetails.bind(this, this.props.navigation, 'Lost Drop', eventsSingleLocation.location_id, item.info.id, item.info.name)}>
                               <Text style={styles.etvbrNumbers}>{item.abs.lost_drop}</Text>
                             </TouchableOpacity>
                           </View>
                         </View>
                     </View>

                    }


											{(eventsSingleLocation.event_categories[this.state.eventCategoryIndex].events.length-1 == index)
												?
													<View  style={{width: '100%', marginTop: 10, marginBottom: 5,}}>
												        <Text style={styles.etvbrTotalTitle}>Total</Text>
													        {(this.state.show_event_percentage)?
														        <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', flexDirection: 'row', height: 40}}>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_rel.enquiries}</Text>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_rel.tdrives}</Text>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_rel.visits}</Text>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_rel.bookings}</Text>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_rel.retails}</Text>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_rel.lost_drop}</Text>
																</View>:
																<View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', flexDirection: 'row', height: 40}}>

																<TouchableOpacity style={{flex:1, alignItems: 'center', justifyContent: 'center',marginTop: 7 }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Enquiries', "", "", 'All Enquiries')}*/>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_abs.enquiries}</Text>
																	</TouchableOpacity>
																<TouchableOpacity style={{flex:1, alignItems: 'center', justifyContent: 'center',marginTop: 7 }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Test Drive', "", "", 'All Test Drive')}*/>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_abs.tdrives}</Text>
																	</TouchableOpacity>
																<TouchableOpacity style={{flex:1, alignItems: 'center', justifyContent: 'center',marginTop: 7 }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Visits', "", "", 'All Visits')}*/>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_abs.visits}</Text>
																	</TouchableOpacity>
																<TouchableOpacity style={{flex:1, alignItems: 'center', justifyContent: 'center',marginTop: 7 }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Bookings', "", "", 'All Bookings')}*/>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_abs.bookings}</Text>
																	</TouchableOpacity>
																<TouchableOpacity style={{flex:1, alignItems: 'center', justifyContent: 'center' ,marginTop: 7,  }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Retails', "", "", 'All Retails')}*/>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_abs.retails}</Text>
																	</TouchableOpacity>
																<TouchableOpacity style={{flex:1, alignItems: 'center', justifyContent: 'center', marginTop: 7,  }} /*onLongPress={this.getDetails.bind(this, this.props.navigation, 'Lost Drop', "", "", 'All Lost Drop')}*/>
																	<Text style={styles.etvbrTotal}>{eventsSingleLocation.event_categories[this.state.eventCategoryIndex].event_category_abs.lost_drop}</Text>
																	</TouchableOpacity>
																</View>
															}
													</View> : null}
										</View>
									);
					              }
					          	}
					         	 keyExtractor={(item, index) => index+''}
					          />

					    </View>
              </View>

			</View>
			);
	}

}
