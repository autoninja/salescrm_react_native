package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by bannhi on 24/6/17.
 */

public class EvaluatorNamesListResponse {

    private String message;

    private String statusCode;

    private List<Result> result;

    private Error error;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public List<Result> getResult ()
    {
        return result;
    }

    public void setResult (List<Result> result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", statusCode = "+statusCode+", result = "+result+", error = "+error+"]";
    }

    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }

    public class Result
    {
        private String user_name;

        private String user_id;

        private String role_name;

        public String getUser_name ()
        {
            return user_name;
        }

        public void setUser_name (String user_name)
        {
            this.user_name = user_name;
        }

        public String getUser_id ()
        {
            return user_id;
        }

        public void setUser_id (String user_id)
        {
            this.user_id = user_id;
        }

        public String getRole_name ()
        {
            return role_name;
        }

        public void setRole_name (String role_name)
        {
            this.role_name = role_name;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [user_name = "+user_name+", user_id = "+user_id+", role_name = "+role_name+"]";
        }
    }
}
