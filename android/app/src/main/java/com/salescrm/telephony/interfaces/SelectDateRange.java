package com.salescrm.telephony.interfaces;

import com.squareup.otto.Produce;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by prateek on 13/6/17.
 */

public class SelectDateRange {

    ArrayList<Date> dateArrayList;

    public SelectDateRange(ArrayList<Date> dateArrayList){
        this.dateArrayList = dateArrayList;
    }

    /*@Produce
    public ArrayList<Date> passDates(ArrayList<Date> dateArrayList){
        return dateArrayList;
    }*/

    public ArrayList<Date> getDateArrayList() {
        return dateArrayList;
    }
}
