package com.salescrm.telephony.response;

/**
 * Created by akshata on 7/7/16.
 */
public class AddEmailAddressResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    private AddEmailAddressResponse addEmailAddressResponse;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public AddEmailAddressResponse(AddEmailAddressResponse addEmailAddressResponse) {
        this.addEmailAddressResponse = addEmailAddressResponse;
    }

    public AddEmailAddressResponse getAddEmailAddressResponse() {
        return addEmailAddressResponse;
    }

    public void setAddEmailAddressResponse(AddEmailAddressResponse addEmailAddressResponse) {
        this.addEmailAddressResponse = addEmailAddressResponse;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private String lead_last_updated;

        private String lead_id;

        private EmailData[] emailData;

        public String getLead_last_updated() {
            return lead_last_updated;
        }

        public void setLead_last_updated(String lead_last_updated) {
            this.lead_last_updated = lead_last_updated;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public EmailData[] getEmailData() {
            return emailData;
        }

        public void setEmailData(EmailData[] emailData) {
            this.emailData = emailData;
        }

        @Override
        public String toString() {
            return "ClassPojo [lead_last_updated = " + lead_last_updated + ", lead_id = " + lead_id + ", emailData = " + emailData + "]";
        }
    }

    public class EmailData {
        private String reliable;

        private String id;

        private String address;

        private String status;

        private String lead_email_mapping_id;

        private String email_address_id;

        private String lead_id;

        public String getReliable() {
            return reliable;
        }

        public void setReliable(String reliable) {
            this.reliable = reliable;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getLead_email_mapping_id() {
            return lead_email_mapping_id;
        }

        public void setLead_email_mapping_id(String lead_email_mapping_id) {
            this.lead_email_mapping_id = lead_email_mapping_id;
        }

        public String getEmail_address_id() {
            return email_address_id;
        }

        public void setEmail_address_id(String email_address_id) {
            this.email_address_id = email_address_id;
        }

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [reliable = " + reliable + ", id = " + id + ", address = " + address + ", status = " + status + ", lead_email_mapping_id = " + lead_email_mapping_id + ", email_address_id = " + email_address_id + ", lead_id = " + lead_id + "]";
        }
    }

    public class Error {
        private String details;

        private String type;

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "ClassPojo [details = " + details + ", type = " + type + "]";
        }
    }
}

