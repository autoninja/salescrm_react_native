import { realm } from './realm_schemas';
import { CommonModel } from '../models/common_model'

const EventCategoriesDBService = {
  findAll: function() {
    return realm.objects('EventCategoriesDB');
  },
  save: function(eventCategory) {
    realm.write(() => {
        realm.create('EventCategoriesDB', eventCategory, true);
    })
    let eventCategoriesDB = realm.objects('EventCategoriesDB');
    console.log('eventCategoriesDB.length', eventCategoriesDB.length);
  },
  getEventCategories: () => {
    let eventCategories = [];
    let eventCategoriesDB =  realm.objects('EventCategoriesDB');
    if(eventCategoriesDB) {
      eventCategoriesDB.map((eventCategoryDB)=> {
        eventCategories.push(new CommonModel(eventCategoryDB.id, eventCategoryDB.name));
      })
    }
    return eventCategories;
  },
  deleteAll: function() {
    realm.write(() => {
      realm.delete(realm.objects('EventCategoriesDB'));
    })
  },
}

export default EventCategoriesDBService;
