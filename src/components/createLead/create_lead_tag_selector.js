import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
export default class CreateLeadTagSelector extends Component{
  constructor(props) {
    super(props)
  }
  render() {
    var isSelected = (this.props.selected==this.props.item.id);
    let item = this.props.item;
    return(
      <TouchableOpacity style={[{flex:1}, this.props.style]} onPress = {() => this.props.onClick(item)}>
      <View style={[styles.shadowsView, {flex:1}]}>
        <View style={[styles.button, {borderColor:item.color, backgroundColor:isSelected?item.color:'#fff'}]}>
          <Text style={[styles.title, {color:isSelected?'#fff':item.color}]}>{item.text}</Text>
        </View>
      </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button : {
    margin:5,
    borderRadius:10,
    height:30,
    elevation:2,
    flex:1,
    borderWidth:1,
    justifyContent:'center',
    alignItems:'center'
  },
  title: {
    fontSize:14,
  },
  shadowsView: {
    width:'100%',
    shadowColor: "#808080",
    shadowOpacity: 0.4,
    shadowRadius: 2,
    shadowOffset: {
     height: 1,
     width: 0
   },
  }
})
