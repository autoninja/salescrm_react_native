import React from 'react';
import { createStackNavigator } from 'react-navigation';
import LostDrop from  './index';
import LostDetailsNewUsed from '../LostDetailsNewUsed';
import DropDetailsReasons from '../DropDetailsReasons';
import LostDetailsOEMLevel from '../LostDetailsOEMLevel';
import LostDetailsOEMDealerReasons from '../LostDetailsOEMDealerReasons';
import HamburgerIcon from '../../../../components/uielements/HamburgerIcon';
import BackIcon from "../../../../components/uielements/BackIcon";

const LostDropRouter = createStackNavigator(
    {
        LostDropMain : {
            screen:LostDrop,
            navigationOptions: ({ navigation }) => ({
                header: null
            })
        },
        LostDetailsNewUsed : {
            screen:LostDetailsNewUsed,
            navigationOptions: ({ navigation }) => ({
                title: 'Lost',
                headerBackTitle : null,
                headerLeft : <BackIcon navigationProps={ navigation }/>,
                headerStyle: {
                    backgroundColor: '#2e5e86'
                },
                headerTransperent: true,
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                    width: '100%',
                    textAlign: 'left',
                },
            })
        },
        DropDetailsReasons : {
            screen:DropDetailsReasons,
            navigationOptions: ({ navigation }) => ({
                title: 'Drop (Reason)',
                headerBackTitle : null,
                headerLeft : <BackIcon navigationProps={ navigation }/>,
                headerStyle: {
                    backgroundColor: '#2e5e86'
                },
                headerTransperent: true,
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                    width: '100%',
                    textAlign: 'left',
                },
            })
        },
        LostDetailsOEMLevel : {
            screen:LostDetailsOEMLevel,
            navigationOptions: ({ navigation }) => ({
                title: 'Lost',
                headerBackTitle : null,
                headerLeft : <BackIcon navigationProps={ navigation }/>,
                headerStyle: {
                    backgroundColor: '#2e5e86'
                },
                headerTransperent: true,
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                    width: '100%',
                    textAlign: 'left',
                },
            })
        },
        LostDetailsOEMDealerReasons : {
            screen:LostDetailsOEMDealerReasons,
            navigationOptions: ({ navigation }) => ({
                header: null
            })
        },
    },
    {
      cardStyle: {
            backgroundColor: '#303030BE',
            opacity: 1,
        },
        mode:'modal'
    },
    {
        initialRouteName: "LostDropMain",
        initialRouteKey: "LostDropMain"
    }
);

export default LostDropRouter;
