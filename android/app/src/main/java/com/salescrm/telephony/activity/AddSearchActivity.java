package com.salescrm.telephony.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.salescrm.telephony.activity.createEnquiry.RNCreateLeadActivity;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.model.SearchInputData;
import com.salescrm.telephony.R;
import com.salescrm.telephony.fragments.SearchResultsFragment;
import com.salescrm.telephony.fragments.SearchResultsNotFoundFragment;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by bharath on 27/5/16.
 */
public class AddSearchActivity extends AppCompatActivity implements SearchResultsFragment.OnEmptySearchResultListener,
        SearchResultsNotFoundFragment.OnStartAddLeadActivity, TextView.OnEditorActionListener {

    FrameLayout addSearchFrame;
    TextView btAddSearch;
    CardView cardViewSearchMain;
    Toolbar toolbar;
    FragmentManager fragmentManager;
    SearchResultsFragment searchResultsFragment;
    private CoordinatorLayout coordinatorLayout;
    private TextInputEditText etSearchMobileNumber, etSearchCustomerName, etSearchLeadId, etSearchEmailId;
    private Preferences pref = null;
    private Realm realm;
    private SearchInputData searchInputData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_search);
        pref = Preferences.getInstance();
        pref.load(getApplicationContext());
        realm = Realm.getDefaultInstance();

        RealmResults<SalesCRMRealmTable> deleteInvalidData = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", true).findAll();
        if (deleteInvalidData.size() > 0) {
            realm.beginTransaction();
            deleteInvalidData.deleteAllFromRealm();
            realm.commitTransaction();
        }
        // initialise ids
        addSearchFrame = (FrameLayout) findViewById(R.id.add_search_frame);
        btAddSearch =  findViewById(R.id.bt_add_search);
        cardViewSearchMain = (CardView) findViewById(R.id.search_main_card);
        toolbar = (Toolbar) findViewById(R.id.app_toolbar);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout_search);
        etSearchMobileNumber = (TextInputEditText) findViewById(R.id.et_search_mob_number);
        // etSearchEnquiryNumber = (TextInputEditText)
        // findViewById(R.id.et_search_enquiry_number);
        etSearchCustomerName = (TextInputEditText) findViewById(R.id.et_search_customer_name);
        etSearchLeadId = (TextInputEditText) findViewById(R.id.et_search_lead_id);
        etSearchEmailId = (TextInputEditText) findViewById(R.id.et_search_email_id);
        // cardViewSearchMain = (CardView) findViewById(R.id.search_main_card);
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.add_search_lead));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        // Click Listener keyboard search button
        etSearchMobileNumber.setOnEditorActionListener(this);
        // etSearchEnquiryNumber.setOnEditorActionListener(this);
        etSearchCustomerName.setOnEditorActionListener(this);
        etSearchLeadId.setOnEditorActionListener(this);
        etSearchEmailId.setOnEditorActionListener(this);

        btAddSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performSearch();

            }
        });

    }

    private void performSearch() {
        pref.setAddSearchNumber(etSearchMobileNumber.getText().toString());

        Util.HideKeyboard(this);
        if (isInputEmpty()) {
            showAlert("Invalid input");
        } else if (!isNumberValid()) {
            showAlert("Invalid mobile number");
        } else {
            if (fragmentManager == null) {
                fragmentManager = getSupportFragmentManager();
            }
            searchInputData = new SearchInputData(etSearchMobileNumber.getText().toString(), "",
                    etSearchCustomerName.getText().toString(), Util.getLong(etSearchLeadId.getText().toString()),
                    etSearchEmailId.getText().toString());
            searchResultsFragment = SearchResultsFragment.newInstance(searchInputData);
            fragmentManager.beginTransaction().add(R.id.coordinator_layout_search, searchResultsFragment)
                    .addToBackStack(null).commit();

            /* Lead Search CleverTap */
            StringBuilder stringBuilder = new StringBuilder();
            if (searchInputData.getMobile() != null && !searchInputData.getMobile().equalsIgnoreCase("-1")) {
                stringBuilder.append("Mobile,");
            }
            if (searchInputData.getCustomerName() != null
                    && !searchInputData.getCustomerName().equalsIgnoreCase("-1")) {
                stringBuilder.append("Name,");
            }
            if (searchInputData.getLeadID() != -1) {
                stringBuilder.append("Lead Id,");
                System.out.println("LeadId:" + searchInputData.getLeadID());
            }
            if (searchInputData.getEmailId() != null && !searchInputData.getEmailId().equalsIgnoreCase("-1")) {
                stringBuilder.append("Email Id,");
            }
            String searchedType = stringBuilder.substring(0, stringBuilder.length() - 1);

            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put(CleverTapConstants.EVENT_LEAD_SEARCH_KEY_SEARCH_TYPE, searchedType);
            CleverTapPush.pushEvent(CleverTapConstants.EVENT_LEAD_SEARCH, hashMap);
        }
    }

    private void showAlert(String alert) {

        Snackbar snackbar = Snackbar.make(coordinatorLayout, alert, Snackbar.LENGTH_SHORT).setAction("Close",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isInputEmpty() {
        return etSearchMobileNumber.getText().length() > 0 && etSearchMobileNumber.getText().length() < 10
                || (etSearchMobileNumber.getText().toString().equals("")
                        && etSearchCustomerName.getText().toString().equals("")
                        && etSearchLeadId.getText().toString().equals("")
                        && etSearchEmailId.getText().toString().equals(""));
    }

    public boolean isNumberValid() {
        boolean val = true;
        if (etSearchMobileNumber.getText().length() > 0) {
            val = etSearchMobileNumber.getText().length() == 10 && etSearchMobileNumber.getText().charAt(0) != '0';
        }
        return val;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        changeStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        if (fragmentManager != null && fragmentManager.getFragments() != null
                && fragmentManager.getFragments().size() != 0) {
            for (Fragment fragment : fragmentManager.getFragments()) {
                // fragmentManager.beginTransaction().remove(fragment).commit();
                fragmentManager.popBackStack();

            }
            fragmentManager = null;
        } else {
            AddSearchActivity.this.finish();
        }

    }

    public void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    @Override
    public void isEmptySearchResult(boolean val) {
        if (!val) {
            showAlert("Something went wrong");
        }
        showSearchResultNotFound();

    }

    private void showSearchResultNotFound() {
        changeStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        if (fragmentManager == null) {
            fragmentManager = getSupportFragmentManager();
        }
        fragmentManager.beginTransaction()
                .add(R.id.coordinator_layout_search, SearchResultsNotFoundFragment.newInstance(searchInputData))
                .addToBackStack(null).commitAllowingStateLoss();
    }

    @Override
    public void startAddLeadActivity(boolean val) {
        // startActivity(new Intent(AddSearchActivity.this, AddLeadActivity.class));
        // AddSearchActivity.this.finish();

        // Starting react native
        startActivity(new Intent(AddSearchActivity.this, RNCreateLeadActivity.class));
        AddSearchActivity.this.finish();

    }

    @Override
    public void startAddColdVisitActivity(boolean val) {
        Intent intent = new Intent(AddSearchActivity.this, RNColdVisitActivity.class);
        intent.putExtra("MOBILE_NUMBER", pref.getAddSearchNumber());
        startActivity(intent);
        AddSearchActivity.this.finish();

    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            performSearch();
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}
