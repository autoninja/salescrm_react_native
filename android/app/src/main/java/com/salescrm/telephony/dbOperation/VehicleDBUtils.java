package com.salescrm.telephony.dbOperation;

import com.salescrm.telephony.activity.createEnquiry.RNCreateLeadActivity;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandModelsDB;
import com.salescrm.telephony.db.car.CarBrandsDB;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class VehicleDBUtils {
    public static VehicleModel getInterestedVehicleModel(Realm realm) {
        UserDetails userDetails = realm.where(UserDetails.class).findFirst();
        if (userDetails != null && userDetails.getUserCarBrands().size() > 0) {
            CarBrandsDB brand = realm.where(CarBrandsDB.class)
                    .equalTo("brand_id", userDetails.getUserCarBrands().get(0).getBrand_id()).findFirst();
            if (brand != null) {
                List<VehicleModelsModel> models = new ArrayList<>();
                for (CarBrandModelsDB model : brand.getBrand_models()) {

                    if (model.getCategory().equalsIgnoreCase(WSConstants.CAR_CATEGORY_BOTH)) {

                        List<VehicleModelsVariantModel> variants = new ArrayList<>();
                        for (CarBrandModelVariantsDB variant : model.getCar_variants()) {
                            variants.add(new VehicleModelsVariantModel(Util.getInt(variant.getVariant_id()),
                                    variant.getVariant(), Util.getInt(variant.getFuel_type_id()),
                                    variant.getFuel_type(), Util.getInt(variant.getColor_id()), variant.getColor()));
                        }

                        models.add(new VehicleModelsModel(Util.getInt(model.getModel_id()), model.getModel_name(),
                                model.getCategory(), variants));
                    }
                }
                return new VehicleModel(Util.getInt(brand.getBrand_id()), brand.getBrand_name(), models);
            }

        }

        return null;
    }

    static class VehicleModel {
        int brand_id;
        String brand_name;
        List<VehicleModelsModel> brand_models;

        VehicleModel(int brand_id, String brand_name, List<VehicleModelsModel> brand_models) {
            this.brand_id = brand_id;
            this.brand_name = brand_name;
            this.brand_models = brand_models;
        }
    }

    static class VehicleModelsModel {
        int model_id;
        String model_name;
        String category;
        List<VehicleModelsVariantModel> car_variants;

        VehicleModelsModel(int model_id, String model_name, String category,
                List<VehicleModelsVariantModel> car_variants) {
            this.model_id = model_id;
            this.model_name = model_name;
            this.category = category;
            this.car_variants = car_variants;
        }
    }

    static class VehicleModelsVariantModel {
        int variant_id;
        String variant;
        int fuel_type_id;
        String fuel_type;
        int color_id;
        String color;

        VehicleModelsVariantModel(int variant_id, String variant, int fuel_type_id, String fuel_type, int color_id,
                String color) {
            this.variant_id = variant_id;
            this.variant = variant;
            this.fuel_type_id = fuel_type_id;
            this.fuel_type = fuel_type;
            this.color_id = color_id;
            this.color = color;
        }
    }
}
