package com.salescrm.telephony.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.salescrm.telephony.BuildConfig;
import com.salescrm.telephony.R;
import com.salescrm.telephony.db.AllMobileNumbersSearch;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.dbOperation.CallStatDbOperation;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;


/**
 * Created by Ravindra P on 27-11-2015.
 */
public class SplashActivity extends Activity {
    private static int SPLASH_TIME_OUT = 2000;
    private Realm realm;

    private int PERMISSION_ALL = 1;
    private String[] PERMISSIONS;
    private Preferences pref;
    private boolean permissionRequested = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //startService(new Intent(this, SyncData.class));
        realm = Realm.getDefaultInstance();
        setContentView(R.layout.splash_screen_layout);
        PERMISSIONS = new String[]{Manifest.permission.CAMERA,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.CALL_PHONE,
        };

        pref = Preferences.getInstance();
        pref.load(this);
        if (hasPermissions(PERMISSIONS)) {
            init();
        } else if(!permissionRequested) {
            permissionRequested = true;
            ActivityCompat.requestPermissions(SplashActivity.this, PERMISSIONS, PERMISSION_ALL);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void init() {

        RealmResults<SalesCRMRealmTable> deleteInvalidData = realm.where(SalesCRMRealmTable.class).equalTo("isCreatedTemporary", true).findAll();
        if(deleteInvalidData.size()>0){
            realm.beginTransaction();
            deleteInvalidData.deleteAllFromRealm();
            realm.commitTransaction();
        }

        if(realm.where(UserDetails.class).findFirst() == null
                || pref.getVersionCode() < BuildConfig.VERSION_CODE){
            System.out.println("Version changing");
            pref.setAppLastOpenedDate(0);
            if(pref.getVersionCode()<48) {
                pref.setIsLogedIn(false);
            }
        }
        boolean isOpenC360 = isOpenC360();
        if (pref.isLogedIn()) {
            Intent intent;
            if (pref.getAppLastOpenedDate()==0
                    || Util.getDifferenceBetweenDatesAll(Util.getTime(0),new Date(pref.getAppLastOpenedDate()))>0
                || realm.where(UserDetails.class).findFirst() == null
                || pref.getVersionCode() < BuildConfig.VERSION_CODE) {

                intent = new Intent(SplashActivity.this, OfflineSupportActivity.class);
                intent.putExtra("START_C360", isOpenC360);

            } else if(isOpenC360){
                intent = new Intent(SplashActivity.this, C360Activity.class);
            }
            else {
                intent = new Intent(SplashActivity.this, HomeActivity.class);
            }
            startActivity(intent);
            overridePendingTransition(0,0);
            finish();
        }
        else {
            setContentView(R.layout.splash_screen_layout);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    i.putExtra("START_C360", isOpenC360);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    public boolean hasPermissions(String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isSelectedNeverAskAgainPermissions(String... permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED) {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    return true;
                }

            }
        }
        return false;

    }
    private boolean isOpenC360() {
        boolean openC360 = false;
        if(getIntent()!=null && getIntent().getBooleanExtra("START_C360", false)) {
            pref.setLeadID(getDynamicLeadID());
            openC360 = true;
        }
        return openC360;
    }
    private String getDynamicLeadID() {
        String leadId = null;
        if(getIntent()!=null) {
            leadId = getIntent().getStringExtra("LEAD_ID");
        }
        return leadId;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        permissionRequested = false;
        if (requestCode == PERMISSION_ALL && hasPermissions(PERMISSIONS)) {
            init();

        } else {
            if (!isFinishing()) {
                new AutoDialog(SplashActivity.this, new AutoDialogClickListener() {
                    @Override
                    public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
                        if (isSelectedNeverAskAgainPermissions(PERMISSIONS)) {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);
                            SplashActivity.this.finish();
                        } else {
                            ActivityCompat.requestPermissions(SplashActivity.this, PERMISSIONS, PERMISSION_ALL);

                        }

                    }

                    @Override
                    public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {
                        SplashActivity.this.finish();
                    }
                }, new AutoDialogModel(isSelectedNeverAskAgainPermissions(PERMISSIONS) ? "Please enable permissions in the app settings" : "App needs permissions", "Ok", "Close")).show();
            }
        }
    }
}
