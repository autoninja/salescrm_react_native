package com.salescrm.telephony.db.create_lead;

import io.realm.RealmObject;

/**
 * Created by bharath on 8/12/16.
 */

public class Emails extends RealmObject{
    private String address;

    private boolean is_primary;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean getIs_primary() {
        return is_primary;
    }

    public void setIs_primary(boolean is_primary) {
        this.is_primary = is_primary;
    }

    @Override
    public String toString() {
        return "ClassPojo [address = " + address + ", is_primary = " + is_primary + "]";
    }
}
