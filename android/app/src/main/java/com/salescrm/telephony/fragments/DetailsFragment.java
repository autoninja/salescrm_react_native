package com.salescrm.telephony.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.Detail_Email_Adapter;
import com.salescrm.telephony.adapter.Details_Mobile_Adapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.dataitem.Detail_Email_model;
import com.salescrm.telephony.dataitem.Detail_Mobile_model;
import com.salescrm.telephony.db.BuyerTypeDB;
import com.salescrm.telephony.db.C360.DetailsPageDbHandler;
import com.salescrm.telephony.db.CustomerEmailId;
import com.salescrm.telephony.db.CustomerPhoneNumbers;
import com.salescrm.telephony.db.DseDetails;
import com.salescrm.telephony.db.EnqSourceDB;
import com.salescrm.telephony.db.LocationsDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.crm_user.TeamUserDB;
import com.salescrm.telephony.db.crm_user.UsersDB;
import com.salescrm.telephony.dbOperation.LeadMobileMappingDbOperations;
import com.salescrm.telephony.model.C360RestartModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AddEmailAddressResponse;
import com.salescrm.telephony.response.AddMobileNumberResponse;
import com.salescrm.telephony.response.ChangePrimaryEmailResponse;
import com.salescrm.telephony.response.CustomerEditResponse;
import com.salescrm.telephony.response.DeleteEmailResponse;
import com.salescrm.telephony.response.DeleteMobileResponse;
import com.salescrm.telephony.response.EditEmailResponse;
import com.salescrm.telephony.response.EditMobileNumberResponse;
import com.salescrm.telephony.response.LocationEditResponse;
import com.salescrm.telephony.response.LocationfromcrmsourceResponse;
import com.salescrm.telephony.response.PincodeResponse;
import com.salescrm.telephony.response.PrimaryMobileResponse;
import com.salescrm.telephony.response.SalesfromLocationResponse;
import com.salescrm.telephony.response.SaveEditLeadResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.services.service_handlers.DetailFragmentServiceHandler;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.salescrm.telephony.R.id.dialog1edit_email;


public class DetailsFragment extends Fragment implements
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener,
        com.wdullaer.materialdatetimepicker.time.TimePickerDialog.OnTimeSetListener,
        Details_Mobile_Adapter.AdapterInterface, Detail_Email_Adapter.EmailAdapterInterface {
    int mobileNumberLength = 0;
    String genderSelected = "";
    String selectedBuyerType = "";
    String selectedmodeOfPayment = "";
    String selectedTypeOfCustomerId = "";
    private static final int CAMERA_REQUEST = 1888;
    private Preferences pref;
    public static String selectedDseName, selectedCrmSrc, selectedCrmId, selectedLocName,
            selectedLocId, selectedBuyerTypeId, selectedDseId, selectedEnqSourceId;
    private LinearLayout add_mobile, add_email;
    private Details_Mobile_Adapter adapter;
    private Detail_Email_Adapter email_adapter;
    public static List<Detail_Mobile_model> mobile_items;
    private List<Detail_Email_model> email_items;
    EditMobileNumberResponse.MobileData edittedNumberObj;
    EditEmailResponse.EmailData edittedEmailObj;
    ProgressDialog progressDialog;
    DseDetails dseDetails;
    List<String> locationNamesList, locationIdsList, dseIdsList, dseNamesList, crmSrcList, crmIdsList;
    SalesfromLocationResponse dropdowndse;
    LocationfromcrmsourceResponse dropdownsrc;
    public Realm realm;
    Dialog dialog, dialogbuyer, dialog_residence, dialog_first_name, dialogcmpnyname,
            dialog_plusmobi, dialog1, dialog_email;
    public static String phone_mapping_id, email_mapping_id, closingDateTime;
    public static String phone_number, email, mob_id, email_id;

    private View parentLayout;
    private Snackbar snackbar;
    private View viewSnacbar;
    private TextView tvSnacbar;

    public static String emailID;
    private SalesCRMRealmTable salesCRMRealmTable;
    //CustomerPhoneNumbers customerPhoneNumbers;
    private LocationEditResponse locationedit;
    int index_mobile, index_email;
    static ListView list_add_mobile, list_add_email;
    ImageButton tv_plus_email;
    // String enquiryNumber = "";
    //LocationDropdownResponse locationDropdownResponse;
    //CustomerEmailId customerEmailId;
    Spinner dialog1location_edit1, dialog1sales_edit2, customerTypeSpinner, dialogcrm_sourceedit3, modeOfPaymentSpinner, genderDialogSpinner, buyerTypeSpinner;
    Spinner residenceLocality, officeLocality;
    ImageView visitingimg, tv_star, img_delete;
    private static int RESULT_LOAD_IMAGE = 1;
    ImageButton btn_dse_edit, btn_crm_source_edit, btn_mobile_edit, img_buyertype, img_customer_type,
            img_mode_payment, img_first_names, img_last_name, img_gender,
            img_age, img_email, img_residence_add, img_residence_pincode, img_cmpny_name,
            img_designation, img_officeadd, img_office_pincode, camera, img_visiting, tv_plus_mobi;
    Button save_add_email, cancel_add_email, save_add_mobi, cancel_add_mobi;
    EditText addEmailEditText, edit_dialog_age,
            edit_dialog_office_pincode, edit_dialog_office_address,
            edit_dialog_office_city, edit_dialog_office_state,
            edit_dialog_occupation,
            edit_dialog_company, addMobileNumberEditText, mobileNumberEditText, dialog1edit_residence,
            dialogedit_residence_pin,
            dialogedit_residence_city,
            dialogedit_residence_state,
            dialog1_edit_email,
            dialogedit2_buyer,
            dialogeditoem, edit_dialog_first_name, edit_dialog_last_name;
    public TextView tvCustomerId, tvOccupationText, tvAddressText, tvPhoneNumberText, tvEmailText,
            tv_crmsourcetype, txtmob, tvLeadIDText, tvSourceText, tvTypeText, tvFinanceText, tvEstimatedCloseText, tvLeadAgeText,
            tv_typeofcustomer, tvfname, tvresidencepincode,
            tvLeadcrmid, tvlocation_name, tv_company_name, tv_designation, tvgender,
            tv_sale_consultant_name, tvlastname, tvmobileno, tvofficeaddress, tv_residence_address,
            tvofficepincode, tvtbuyertype, tv_typeofPayment, tvclosingdate, tvage, tvmobilesss, dialogedit_closing_date, dialogedit_closing_time;

    int selectedLocPos = 0;
    int selectedDsePos = 0;
    int selectedCrmPos = 0;

    ConnectionDetectorService connectionDetectorService;
    private RelativeLayout relLoadingResidence, relLoadingComapany;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.details_new, container, false);
        parentLayout = view;
        pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(getActivity());
        tv_star = (ImageView) view.findViewById(R.id.tvstar);
        tvage = (TextView) view.findViewById(R.id.tvage);
        list_add_mobile = (ListView) view.findViewById(R.id.listone);
        list_add_email = (ListView) view.findViewById(R.id.list_email);
        tvLeadcrmid = (TextView) view.findViewById(R.id.tvLeadcrmid);
        tvCustomerId = (TextView) view.findViewById(R.id.tvLeadCarModel_one);
        //  tvenq = (TextView) view.findViewById(R.id.tvenq);
        tvresidencepincode = (TextView) view.findViewById(R.id.tvresidencepincode);
        tvlocation_name = (TextView) view.findViewById(R.id.locationName);
        tv_crmsourcetype = (TextView) view.findViewById(R.id.tv_crmsourcetype);
        //  tv_oemsource = (TextView) view.findViewById(R.id.tv_oemsource);
        tvofficepincode = (TextView) view.findViewById(R.id.tvofficepincode);
        tv_sale_consultant_name = (TextView) view.findViewById(R.id.tv_sale_consultant_name);
        tvtbuyertype = (TextView) view.findViewById(R.id.tvtbuyertype);
        tv_typeofcustomer = (TextView) view.findViewById(R.id.indidviual);
        tv_typeofPayment = (TextView) view.findViewById(R.id.tv_typeofbuyer);
        tvclosingdate = (TextView) view.findViewById(R.id.tvclosingdate);
        tv_designation = (TextView) view.findViewById(R.id.tv_designation);
        tv_company_name = (TextView) view.findViewById(R.id.cmpnyname);
        tv_residence_address = (TextView) view.findViewById(R.id.tv_residence_address);
        tvfname = (TextView) view.findViewById(R.id.tvfname);
        tvlastname = (TextView) view.findViewById(R.id.tvlastname);
        tvgender = (TextView) view.findViewById(R.id.tvmale_female);
        btn_dse_edit = (ImageButton) view.findViewById(R.id.btn_dse_edit);
        btn_crm_source_edit = (ImageButton) view.findViewById(R.id.btn_crm_source_edit);
        btn_crm_source_edit.setVisibility(View.GONE);
        tvofficeaddress = (TextView) view.findViewById(R.id.tvofficeaddress);
        tvmobilesss = (TextView) view.findViewById(R.id.tvmobilesss);
        tvmobileno = (TextView) view.findViewById(R.id.tvmobileno);
        img_buyertype = (ImageButton) view.findViewById(R.id.img_buyertype);
        img_customer_type = (ImageButton) view.findViewById(R.id.img_customer_type);
        img_mode_payment = (ImageButton) view.findViewById(R.id.img_mode_payment);
        //img_expected_closing_date = (ImageButton) view.findViewById(R.id.img_expected_closing_date);
        img_first_names = (ImageButton) view.findViewById(R.id.img_first_names);
        img_age = (ImageButton) view.findViewById(R.id.img_age);
        img_residence_add = (ImageButton) view.findViewById(R.id.img_residence_add);
        img_residence_pincode = (ImageButton) view.findViewById(R.id.img_residence_pincode);
        img_cmpny_name = (ImageButton) view.findViewById(R.id.img_cmpny_name);
        img_designation = (ImageButton) view.findViewById(R.id.img_designation);
        img_officeadd = (ImageButton) view.findViewById(R.id.img_officeadd);
        img_office_pincode = (ImageButton) view.findViewById(R.id.img_office_pincode);
        camera = (ImageButton) view.findViewById(R.id.camera);
        visitingimg = (ImageView) view.findViewById(R.id.visiting_img);
        tv_plus_email = (ImageButton) view.findViewById(R.id.tv_plus_email);
        mobile_items = new ArrayList<>();
        email_items = new ArrayList<>();
        locationNamesList = new ArrayList<>();
        dseNamesList = new ArrayList<>();
        crmIdsList = new ArrayList<>();
        crmSrcList = new ArrayList<>();
        dseIdsList = new ArrayList<>();
        locationIdsList = new ArrayList<>();

        connectionDetectorService = new ConnectionDetectorService(getContext());

        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("scheduledActivityId", Util.getInt(pref.getmSheduleActivityID()))
                .findFirst();

        System.out.print("DSE Details SheduleID -" + pref.getmSheduleActivityID());

        RealmResults<SalesCRMRealmTable> realmData = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Util.getInt(pref.getLeadID()))
                .findAll();
        for (int i = 0; i < realmData.size(); i++)
            System.out.print("DSE Details LeadID00 - " + realmData.get(i).getLeadLocationId() + " - " + realmData.get(i).getLeadLocationName() + " - " + realmData.get(i).getScheduledActivityId() + "\n");

        addNumbers();
        addEmails();
        // addEnquiryNumber();

        tvLeadcrmid.setText(pref.getLeadID());
        dseDetails = realm.where(DseDetails.class)
                .equalTo("customerID", Integer.parseInt(pref.getLeadID())).findFirst();
        if (dseDetails != null) {
            tv_sale_consultant_name.setText(dseDetails.getDseName());
            System.out.println("dsedetails" + dseDetails.getDseName());
        }

        if (salesCRMRealmTable != null) {
            System.out.print("DSE Details LeadID - " + Integer.parseInt(pref.getLeadID()));
            System.out.print("DSE Details Location - " + salesCRMRealmTable.getLeadLocationName() + " - " + salesCRMRealmTable.getLeadLocationId());
            //System.out.println("ENQUIRY NUMBER"+salesCRMRealmTable.getEnqnumber());
            tvlocation_name.setText(salesCRMRealmTable.getLeadLocationName());
            if (salesCRMRealmTable.getVinRegNo() != null && !salesCRMRealmTable.getVinRegNo().isEmpty()) {
                tv_crmsourcetype.setText(salesCRMRealmTable.getLeadSourceName() + "," + salesCRMRealmTable.getVinRegNo());
            } else {
                tv_crmsourcetype.setText(salesCRMRealmTable.getLeadSourceName());
            }


            //tv_oemsource.setText(salesCRMRealmTable.getOemSource());
            System.out.println("salesCRMRealmTable" + salesCRMRealmTable.getOemSource());
            tvmobilesss.setText(salesCRMRealmTable.getCustomerNumber());
            // tvenq.setText(salesCRMRealmTable.getEnqnumber());
            tvtbuyertype.setText(salesCRMRealmTable.getBuyerType());
            System.out.println("salesCRMRealmTable" + salesCRMRealmTable.getTypeOfCustomer());
            tv_typeofPayment.setText(salesCRMRealmTable.getModeOfPayment());
            tv_designation.setText(salesCRMRealmTable.getCustomerDesignation());
            tv_company_name.setText(salesCRMRealmTable.getCompanyName());
            //
            setHomeAddress();

            if (null != salesCRMRealmTable.getFirstName() && !salesCRMRealmTable.getFirstName().equalsIgnoreCase("")) {
                tvfname.setText(salesCRMRealmTable.getFirstName());
            } else {
                tvfname.setText("-");
            }


            if (Util.isNotNull(salesCRMRealmTable.getTitle())) {
                tvgender.setText(salesCRMRealmTable.getTitle());
            } else {
                tvgender.setText("-");
            }

            /*if (null!= salesCRMRealmTable.getGender() && !salesCRMRealmTable.getGender().equalsIgnoreCase("")) {
                tvgender.setText(salesCRMRealmTable.getGender());
            }else{
                tvgender.setText("-");
            }*/
            if (null != salesCRMRealmTable.getLastName() && !salesCRMRealmTable.getLastName().equalsIgnoreCase("")) {
                tvlastname.setText(salesCRMRealmTable.getLastName());
            } else {
                tvlastname.setText("-");
            }

            tvclosingdate.setText("" + salesCRMRealmTable.getCloseDate());

            if (salesCRMRealmTable.getCustomerAge() != null && !salesCRMRealmTable.getCustomerAge().equalsIgnoreCase("") && !salesCRMRealmTable.getCustomerAge().equalsIgnoreCase("-1")) {
                tvage.setText(salesCRMRealmTable.getCustomerAge());
            } else {
                tvage.setText("-");
            }



            setOfficeAddress();

            tvresidencepincode.setText(salesCRMRealmTable.getResidencePinCode());
            tvofficepincode.setText(salesCRMRealmTable.getOfficePinCode());

            if (salesCRMRealmTable.getModeOfPayment() == null) {
                tv_typeofPayment.setText("-");
            }
            if (salesCRMRealmTable.getBuyerTypeId() == null) {
                tvtbuyertype.setText("-");
            }
            if (salesCRMRealmTable.getTypeOfCustomer() == null) {
                tv_typeofcustomer.setText("-");
            } else {
                String typeofcustomer = salesCRMRealmTable.getTypeOfCustomer();
                typeofcustomer = Character.toUpperCase(typeofcustomer.charAt(0)) + typeofcustomer.substring(1);
                tv_typeofcustomer.setText(typeofcustomer);
            }
            if (salesCRMRealmTable.getCloseDate() == null) {
                tvclosingdate.setText("-");
            }

            if (salesCRMRealmTable.getCustomerAge() == null) {
                tvage.setText("-");
            }
            if (salesCRMRealmTable.getResidenceAddress() == null) {
                tvresidencepincode.setText("-");
            }
            if (salesCRMRealmTable.getOfficePinCode() == null) {
                tvofficepincode.setText("-");
            }

        }
        tv_plus_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isLeadActive()) {
                    return;
                }

                final Dialog dialog_addemail = new Dialog(getActivity());
                dialog_addemail.setContentView(R.layout.addemail_layout);
                Window window = dialog_addemail.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.TOP;
                wlp.y = 200;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
                save_add_email = (Button) dialog_addemail.findViewById(R.id.save_add_email);
                cancel_add_email = (Button) dialog_addemail.findViewById(R.id.cancel_add_email);
                addEmailEditText = (EditText) dialog_addemail.findViewById(R.id.editTextaddemail);
                // addEmailEditText.setText(salesCRMRealmTable.getCustomerEmail());

                cancel_add_email.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_addemail.dismiss();
                    }
                });
                save_add_email.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean close = true;
                        if (!Util.isValidEmail(addEmailEditText.getText().toString())) {
                            close = false;
                            Util.showToast(getContext(), "Invalid email address", Toast.LENGTH_SHORT);
                        } else if (connectionDetectorService.isConnectingToInternet() && !addEmailEditText.getText().toString().trim().isEmpty()) {
                            progressDialog = new ProgressDialog(getContext());
                            progressDialog.setMessage("Adding your email id..");
                            progressDialog.show();
                            DetailFragmentServiceHandler.getInstance(DetailsFragment.this.getContext()).AddEmailServiceHandler
                                    (salesCRMRealmTable.getLeadLastUpdated(), addEmailEditText.getText().toString().trim(), pref.getAccessToken(), pref.getLeadID());
                        } else {
                            close = false;
                            Util.showToast(getContext(), "No internet connections", Toast.LENGTH_LONG);

                        }
                        if (close) {
                            dialog_addemail.dismiss();
                        }
                    }
                });
                dialog_addemail.show();

            }
        });
        tv_plus_mobi = (ImageButton) view.findViewById(R.id.tv_plus_mobi);
        tv_plus_mobi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isLeadActive()) {
                    return;
                }
                dialog_plusmobi = new Dialog(getActivity());
                dialog_plusmobi.setContentView(R.layout.add_mobile_number);
                Window window = dialog_plusmobi.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.TOP;
                wlp.y = 200;
                wlp.y = 200;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
                save_add_mobi = (Button) dialog_plusmobi.findViewById(R.id.save_add_mob);
                cancel_add_mobi = (Button) dialog_plusmobi.findViewById(R.id.cancel_add_mob);
                addMobileNumberEditText = (EditText) dialog_plusmobi.findViewById(R.id.editTextaddmob);
                // addMobileNumberEditText.setText(salesCRMRealmTable.getCustomerNumber());
                cancel_add_mobi.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_plusmobi.dismiss();
                    }

                });
                save_add_mobi.setOnClickListener(new View.OnClickListener() {
                    @Override

                    public void onClick(View v) {

                        if (addMobileNumberEditText.getText().length() < 10) {
                            Toast.makeText(getActivity(), "Please enter a 10 digit mobile number", Toast.LENGTH_LONG).show();
                        } else {
                            if (connectionDetectorService.isConnectingToInternet()) {
                                progressDialog = new ProgressDialog(getContext());
                                progressDialog.setMessage("Adding mobile number..");
                                progressDialog.show();
                                DetailFragmentServiceHandler.getInstance(DetailsFragment.this.getContext()).addMobileNumberServiceHandler
                                        (salesCRMRealmTable.getLeadLastUpdated(), addMobileNumberEditText.getText().toString(), pref.getAccessToken(), pref.getLeadID());
                                dialog_plusmobi.dismiss();
                            } else {
                                Util.showToast(getContext(), "No internet connections", Toast.LENGTH_LONG);

                            }

                        }

                    }
                });
                dialog_plusmobi.show();

            }

        });
        img_buyertype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLeadActive()) {
                    handleBuyerTypeEdit();
                }
            }
        });
        img_customer_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        img_first_names.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isLeadActive()) {
                    return;
                }
                dialog_first_name = new Dialog(getActivity());
                dialog_first_name.setContentView(R.layout.custom_firstname);
                Window window = dialog_first_name.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.TOP;
                wlp.y = 200;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
                edit_dialog_first_name = (EditText) dialog_first_name.findViewById(R.id.dialog1edit_first_name);
                edit_dialog_first_name.setText(salesCRMRealmTable.getFirstName());
                edit_dialog_last_name = (EditText) dialog_first_name.findViewById(R.id.dialogedit2_last_name);
                edit_dialog_last_name.setText(salesCRMRealmTable.getLastName());
                genderDialogSpinner = (Spinner) dialog_first_name.findViewById(R.id.spinner_gender);
                ArrayAdapter<String> genderListAdpater = new ArrayAdapter<String>(getActivity(),
                        R.layout.spinner_item, WSConstants.genderList);
                genderListAdpater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                genderDialogSpinner.setAdapter(genderListAdpater);
                /* genderDialogSpinner.setSelection(WSConstants.genderList.indexOf(salesCRMRealmTable.getGender()));*/
                if (salesCRMRealmTable.getTitle() != null && !salesCRMRealmTable.getTitle().isEmpty()) {
                    genderDialogSpinner.setSelection(WSConstants.genderList.indexOf
                            (salesCRMRealmTable.getTitle()));
                } else {
                    genderDialogSpinner.setSelection(0);
                }
                genderDialogSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        genderSelected = WSConstants.genderList.get(position);
                        if (position == 0) {
                            genderSelected = "";
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                //genderDialogSpinner.setText(salesCRMRealmTable.getGender());
                edit_dialog_age = (EditText) dialog_first_name.findViewById(R.id.dialogedit4_age);
                if (salesCRMRealmTable.getCustomerAge() != null && !salesCRMRealmTable.getCustomerAge().equalsIgnoreCase("-1")
                        && !salesCRMRealmTable.getCustomerAge().equalsIgnoreCase("")) {
                    System.out.println("ohoh: " + salesCRMRealmTable.getCustomerAge());
                    edit_dialog_age.setText(salesCRMRealmTable.getCustomerAge());
                } else {
                    edit_dialog_age.setText("");
                }

                Button cancel = (Button) dialog_first_name.findViewById(R.id.cancel_first_name);
                Button save = (Button) dialog_first_name.findViewById(R.id.save_first_name);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_first_name.dismiss();
                    }
                });
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!edit_dialog_first_name.getText().toString().trim().equalsIgnoreCase("")) {
                            if (connectionDetectorService.isConnectingToInternet()) {
                                progressDialog = new ProgressDialog(getContext());
                                progressDialog.setMessage("Saving your changes..");
                                progressDialog.show();
                                DetailFragmentServiceHandler.getInstance(DetailsFragment.this.getContext())
                                        .saveCustomerDetailsServiceHandler
                                                (pref.getAccessToken(), pref.getLeadID(), salesCRMRealmTable.getLeadLastUpdated(),
                                                        edit_dialog_first_name.getText().toString().trim(),
                                                        edit_dialog_last_name.getText().toString().trim(),
                                                        salesCRMRealmTable.getResidenceAddress(),
                                                        salesCRMRealmTable.getResidencePinCode(),
                                                        salesCRMRealmTable.getResidenceLocality(),
                                                        salesCRMRealmTable.getResidenceCity(),
                                                        salesCRMRealmTable.getResidenceState(),
                                                        salesCRMRealmTable.getOfficeAddress(),
                                                        salesCRMRealmTable.getOfficePinCode(),
                                                        salesCRMRealmTable.getOfficeLocality(),
                                                        salesCRMRealmTable.getOfficeCity(),
                                                        salesCRMRealmTable.getOfficeState(),
                                                        salesCRMRealmTable.getCompanyName(), salesCRMRealmTable.getCustomerOccupation(),
                                                        salesCRMRealmTable.getGender(), genderSelected.trim(),
                                                        edit_dialog_age.getText().toString().trim(), WSConstants.PERSONAL_DETAILS, false);
                            } else {
                                Util.showToast(getContext(), "No internet connections", Toast.LENGTH_LONG);
                            }
                        } else {
                            Util.showToast(getContext(), "Name is mandatory", Toast.LENGTH_LONG);
                        }

                    }
                });

                dialog_first_name.show();
            }
        });
        img_residence_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isLeadActive()) {
                    return;
                }
                dialog_residence = new Dialog(getActivity());
                dialog_residence.setContentView(R.layout.custom_residence);

                dialogedit_residence_pin = (EditText) dialog_residence.findViewById(R.id.dialogedit_residence_pin);
                dialog1edit_residence = (EditText) dialog_residence.findViewById(R.id.dialog1edit_residence);
                residenceLocality =  dialog_residence.findViewById(R.id.dialog1_spinner_residence_locality);
                dialogedit_residence_city = (EditText) dialog_residence.findViewById(R.id.dialog1edit_residence_city);
                dialogedit_residence_state = (EditText) dialog_residence.findViewById(R.id.dialog1edit_residence_state);
                Button cancel = (Button) dialog_residence.findViewById(R.id.cancel_residence);
                Button save = (Button) dialog_residence.findViewById(R.id.save_residence);
                relLoadingResidence = (RelativeLayout) dialog_residence.findViewById(R.id.rel_loading_frame);

                dialog1edit_residence.setText(salesCRMRealmTable.getResidenceAddress());
                dialogedit_residence_pin.setText(salesCRMRealmTable.getResidencePinCode());
                if(getContext()!=null) {
                    residenceLocality.setAdapter(new ArrayAdapter<String>(getContext(),
                            android.R.layout.simple_list_item_1, new String[]{"Select Locality *"}));
                }
                dialogedit_residence_city.setText(salesCRMRealmTable.getResidenceCity());
                dialogedit_residence_state.setText(salesCRMRealmTable.getResidenceState());
                if(Util.isNotNull(salesCRMRealmTable.getResidencePinCode())) {
                    getPincodeDetails(salesCRMRealmTable.getResidencePinCode(), dialogedit_residence_pin,
                            residenceLocality, dialogedit_residence_city,
                            dialogedit_residence_state, relLoadingResidence, salesCRMRealmTable.getResidenceLocality());
                }

                dialogedit_residence_pin.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 6) {
                            getPincodeDetails(s.toString(), dialogedit_residence_pin,
                                    residenceLocality, dialogedit_residence_city,
                                    dialogedit_residence_state, relLoadingResidence, salesCRMRealmTable.getResidenceLocality());
                        }
                        else {
                            if(getContext()!=null) {
                                residenceLocality.setAdapter(new ArrayAdapter<String>(getContext(),
                                        android.R.layout.simple_list_item_1, new String[]{"Select Locality *"}));
                            }
                            dialogedit_residence_city.setText("");
                            dialogedit_residence_state.setText("");
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_residence.dismiss();
                    }
                });
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (connectionDetectorService.isConnectingToInternet()) {
                            if(residenceLocality.getSelectedItemPosition()>0) {
                                progressDialog = new ProgressDialog(getContext());
                                progressDialog.setMessage("Saving Residence Details..");
                                progressDialog.show();
                                DetailFragmentServiceHandler.getInstance(getContext())
                                        .saveCustomerDetailsServiceHandler(pref.getAccessToken(), pref.getLeadID(),
                                                salesCRMRealmTable.getLeadLastUpdated(),
                                                salesCRMRealmTable.getFirstName(), salesCRMRealmTable.getLastName(),
                                                dialog1edit_residence.getText().toString(),
                                                dialogedit_residence_pin.getText().toString(),
                                                residenceLocality.getSelectedItem().toString(),
                                                dialogedit_residence_city.getText().toString(),
                                                dialogedit_residence_state.getText().toString(),
                                                salesCRMRealmTable.getOfficeAddress(),
                                                salesCRMRealmTable.getOfficePinCode(),
                                                salesCRMRealmTable.getOfficeLocality(),
                                                salesCRMRealmTable.getOfficeCity(),
                                                salesCRMRealmTable.getOfficeState(),
                                                salesCRMRealmTable.getCompanyName(),
                                                salesCRMRealmTable.getCustomerOccupation(),
                                                salesCRMRealmTable.getGender(),
                                                salesCRMRealmTable.getTitle(),
                                                salesCRMRealmTable.getCustomerAge(),
                                                WSConstants.RESIDENCE_DETAILS, false);
                            }
                            else {
                                Util.showToast(getContext(), "Please enter valid details", Toast.LENGTH_LONG);
                            }

                        } else {
                            Util.showToast(getContext(), "No internet connections", Toast.LENGTH_LONG);
                        }
                    }
                });
                dialog_residence.show();
            }
        });


        img_cmpny_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isLeadActive()) {
                    return;
                }
                dialogcmpnyname = new Dialog(getActivity());
                dialogcmpnyname.setContentView(R.layout.custom_company_name);

                edit_dialog_company = (EditText) dialogcmpnyname.findViewById(R.id.dialog1edit__company_name);
                edit_dialog_occupation = (EditText) dialogcmpnyname.findViewById(R.id.dialogedit_designation);

                edit_dialog_office_pincode = (EditText) dialogcmpnyname.findViewById(R.id.dialogedit_pincode);
                edit_dialog_office_address = (EditText) dialogcmpnyname.findViewById(R.id.dialogedit_office_address);
                officeLocality =  dialogcmpnyname.findViewById(R.id.dialog1_spinner_office_locality);
                edit_dialog_office_city = (EditText) dialogcmpnyname.findViewById(R.id.dialog1edit_office_city);
                edit_dialog_office_state = (EditText) dialogcmpnyname.findViewById(R.id.dialog1edit_office_state);


                Button cancel = (Button) dialogcmpnyname.findViewById(R.id.cancel_cmpny_name);
                Button save = (Button) dialogcmpnyname.findViewById(R.id.save_cmpny_name);

                relLoadingComapany = (RelativeLayout) dialogcmpnyname.findViewById(R.id.rel_loading_frame);

                edit_dialog_company.setText(salesCRMRealmTable.getCompanyName());
                edit_dialog_occupation.setText(salesCRMRealmTable.getCustomerOccupation());
                edit_dialog_office_pincode.setText(salesCRMRealmTable.getOfficePinCode());
                edit_dialog_office_address.setText(salesCRMRealmTable.getOfficeAddress());
                if(getContext()!=null) {
                    officeLocality.setAdapter(new ArrayAdapter<String>(getContext(),
                            android.R.layout.simple_list_item_1, new String[]{"Select Locality *"}));
                }
                edit_dialog_office_city.setText(salesCRMRealmTable.getOfficeCity());
                edit_dialog_office_state.setText(salesCRMRealmTable.getOfficeCity());

                if(Util.isNotNull(salesCRMRealmTable.getOfficePinCode())) {
                    getPincodeDetails(salesCRMRealmTable.getOfficePinCode(), edit_dialog_office_pincode,
                            officeLocality, edit_dialog_office_city,
                            edit_dialog_office_state, relLoadingComapany, salesCRMRealmTable.getOfficeLocality());
                }




                edit_dialog_office_pincode.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() == 6) {
                            getPincodeDetails(s.toString(), edit_dialog_office_pincode,
                                    officeLocality, edit_dialog_office_city,
                                    edit_dialog_office_state, relLoadingComapany, salesCRMRealmTable.getOfficeLocality());
                        }
                        else {
                            if(getContext()!=null) {
                                officeLocality.setAdapter(new ArrayAdapter<String>(getContext(),
                                        android.R.layout.simple_list_item_1, new String[]{"Select Locality *"}));
                            }
                            edit_dialog_office_city.setText("");
                            edit_dialog_office_state.setText("");
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogcmpnyname.dismiss();

                    }
                });
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (connectionDetectorService.isConnectingToInternet()) {
                            if (officeLocality.getSelectedItemPosition() > 0) {
                                progressDialog = new ProgressDialog(getContext());
                                progressDialog.setMessage("Saving Company Details..");
                                progressDialog.show();
                                DetailFragmentServiceHandler.getInstance(getContext())
                                        .saveCustomerDetailsServiceHandler(pref.getAccessToken(), pref.getLeadID(),
                                                salesCRMRealmTable.getLeadLastUpdated(),
                                                salesCRMRealmTable.getFirstName(), salesCRMRealmTable.getLastName(),
                                                salesCRMRealmTable.getResidenceAddress(),
                                                salesCRMRealmTable.getResidencePinCode(),
                                                salesCRMRealmTable.getResidenceLocality(),
                                                salesCRMRealmTable.getResidenceCity(),
                                                salesCRMRealmTable.getResidenceState(),
                                                edit_dialog_office_address.getText().toString(),
                                                edit_dialog_office_pincode.getText().toString(),
                                                officeLocality.getSelectedItem().toString(),
                                                edit_dialog_office_city.getText().toString(),
                                                edit_dialog_office_state.getText().toString(),
                                                edit_dialog_company.getText().toString(), edit_dialog_occupation.getText().toString(),
                                                salesCRMRealmTable.getGender(), salesCRMRealmTable.getTitle(),
                                                salesCRMRealmTable.getCustomerAge(), WSConstants.COMPANY_DETAILS,
                                                true);
                            }
                            else {
                                Util.showToast(getContext(), "Please enter valid details", Toast.LENGTH_LONG);
                            }
                        }else {
                            Util.showToast(getContext(), "No internet connections", Toast.LENGTH_LONG);
                        }

                    }
                });
                dialogcmpnyname.show();

            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        btn_crm_source_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.customdialogone);
                Window window = dialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.TOP;
                wlp.y = 200;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);
                TextView txtdialog1 = (TextView) dialog.findViewById(R.id.txtdialog1);
                TextView txtdialog3 = (TextView) dialog.findViewById(R.id.txtdialog3);
                TextView txtdialog2 = (TextView) dialog.findViewById(R.id.txtdialog2);
                dialog1location_edit1 = (Spinner) dialog.findViewById(R.id.dialog1location_edit1);
                dialog1sales_edit2 = (Spinner) dialog.findViewById(R.id.dialog1sales_edit2);

                // txtdialog1.setVisibility(View.GONE);
                txtdialog1.setVisibility(View.GONE);
                txtdialog2.setVisibility(View.GONE);
                txtdialog3.setText("CRM SOURCE");
                dialog1location_edit1.setVisibility(View.GONE);
                dialog1sales_edit2.setVisibility(View.GONE);

                dialogcrm_sourceedit3 = (Spinner) dialog.findViewById(R.id.dialogcrm_sourceedit3);
                populateLocationEditDropDownList();
                Button cancel = (Button) dialog.findViewById(R.id.market_price_head);
                Button save = (Button) dialog.findViewById(R.id.save);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                save.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        /*selectedLocId = salesCRMRealmTable.getLeadLocationId();
                        selectedLocName = tvlocation_name.getText().toString().trim();*/
                        DseDetails dseDetails = realm.where(DseDetails.class)
                                .equalTo("dseName", salesCRMRealmTable.getLeadDseName())
                                .findFirst();
                        if (dseDetails != null) {
                            selectedDseId = dseDetails.getDseId();
                            selectedDseName = dseDetails.getDseName();
                        }
                        if (selectedLocId == null || selectedLocName == null) {
                            selectedLocId = "" + salesCRMRealmTable.getAllotDseAnswer().getLocationId();
                            selectedLocName = tvlocation_name.getText().toString().trim();
                        }
                        handleCrmSourceEdit();
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });

        return view;

    }

    private void getPincodeDetails(String pin, EditText etPincode, Spinner locality, EditText etCity, EditText etState, RelativeLayout relLoading, String localityOld) {
        etPincode.setEnabled(false);
        Util.HideKeyboard(getContext());
        relLoading.setVisibility(View.VISIBLE);
        ApiUtil.GetRestApiForCommon().getPincodeDetails(
                pin,
                new Callback<PincodeResponse>() {
                    @Override
                    public void success(PincodeResponse pincodeResponse, Response response) {
                        Util.updateHeaders(response.getHeaders());
                        relLoading.setVisibility(View.GONE);
                        etPincode.setEnabled(true);

                        if(pincodeResponse.getResult()!=null && pincodeResponse.getResult().getPindata()!=null &&pincodeResponse.getResult().getPindata().size()>0) {
                            List<PincodeResponse.Result.PinData> pincodes = pincodeResponse.getResult().getPindata();
                            etCity.setText(pincodes.get(0).getDistrictname());
                            etState.setText(pincodes.get(0).getStatename());
                            String[] localities = new String[pincodes.size()+1];
                            localities[0] = "Select Locality *";
                            int selectedPosition = 0;
                            for (int i=0;i<pincodes.size();i++) {
                                localities[i+1] = pincodes.get(i).getLocality();
                                if(Util.isNotNull(pincodes.get(i).getLocality())
                                        && pincodes.get(i).getLocality().equalsIgnoreCase(localityOld)) {
                                    selectedPosition = i+1;
                                }
                            }
                            if(getContext()!=null) {
                                locality.setAdapter(new ArrayAdapter<String>(getContext(),
                                        android.R.layout.simple_list_item_1, localities));
                                locality.setSelection(selectedPosition);
                            }


                        }
                        else {
                            Util.showToast(getContext(), "Please enter a valid pincode", Toast.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        relLoading.setVisibility(View.GONE);
                        etPincode.setEnabled(true);
                        Util.showToast(getContext(), "Please try again", Toast.LENGTH_LONG);

                    }
                });
    }


    private void assignValuesFromDB() {
        String assignedDSEName = salesCRMRealmTable.getLeadDseName();
        BuyerTypeDB buyerTypeDB = realm.where(BuyerTypeDB.class)
                .equalTo("type", selectedBuyerType)
                .findFirst();
        if (buyerTypeDB != null) {
            selectedBuyerTypeId = "" + buyerTypeDB.getId();
        }

        DseDetails dseDetails = realm.where(DseDetails.class)
                .equalTo("dseName", assignedDSEName)
                .findFirst();
        if (dseDetails != null) {
            selectedDseId = dseDetails.getDseId();
            selectedDseName = dseDetails.getDseName();
        }

        String leadSourceName = salesCRMRealmTable.getLeadSourceName();
        EnqSourceDB enqSourceDB = realm.where(EnqSourceDB.class)
                .equalTo("name", leadSourceName)
                .findFirst();
        if (enqSourceDB != null) {
            selectedEnqSourceId = "" + enqSourceDB.getId();
        }
        selectedLocId = salesCRMRealmTable.getLeadLocationId();
        selectedLocName = salesCRMRealmTable.getLeadLocationName();
    }

    public void populateLocationEditDropDownList() {
        ////////////////////////////CRM SOURCE TYPE///////////////////////////////
        RealmResults<EnqSourceDB> enqSrcDB = realm.where(EnqSourceDB.class)
                .findAll();
        if (enqSrcDB != null) {
            Iterator<EnqSourceDB> itr = enqSrcDB.iterator();
            crmIdsList.clear();
            crmSrcList.clear();
            if (tv_crmsourcetype.getText().toString().equalsIgnoreCase("")) {
                crmSrcList.add("<select>");
                crmIdsList.add("" + 0);

            }
            while (itr.hasNext()) {
                EnqSourceDB temp = itr.next();
                crmSrcList.add(temp.getName());
                crmIdsList.add("" + temp.getId());
            }
            selectedCrmPos = crmSrcList.indexOf(tv_crmsourcetype.getText().toString().trim());
            if (selectedCrmPos == -1) {
                selectedCrmPos = 0;
            }
        }
        ArrayAdapter<String> srcadapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, crmSrcList);
        srcadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        dialogcrm_sourceedit3.setAdapter(srcadapter);
        dialogcrm_sourceedit3.setSelection(selectedCrmPos);
        dialogcrm_sourceedit3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCrmSrc = crmSrcList.get(position);
                selectedCrmId = crmIdsList.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

///////////////////////////////////////LOCATION/////////////////////////////////////////////////
        RealmResults<LocationsDB> locDB = realm.where(LocationsDB.class)
                .findAll();

        if (locDB != null) {
            Iterator<LocationsDB> itr = locDB.iterator();
            locationNamesList.clear();
            locationIdsList.clear();
            if (tvlocation_name.getText().toString().equalsIgnoreCase("")) {
                locationNamesList.add("<select>");
                locationIdsList.add("" + 0);

            }
            while (itr.hasNext()) {
                LocationsDB temp = itr.next();
                locationNamesList.add(temp.getName());
                locationIdsList.add("" + temp.getId());

            }
            selectedLocPos = locationNamesList.indexOf(tvlocation_name.getText().toString().trim());
            if (selectedLocPos == -1) {
                selectedLocPos = 0;
            }
        }
        ArrayAdapter<String> locationadapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, locationNamesList);
        locationadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dialog1location_edit1.setAdapter(locationadapter);
        dialog1location_edit1.setSelection(selectedLocPos);
        dialog1location_edit1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedLocName = locationNamesList.get(position);
                selectedLocId = locationIdsList.get(position);
                RealmList<UsersDB> dseRealmList = new RealmList<UsersDB>();
                TeamUserDB teamUserDB;
                if (!selectedLocName.equalsIgnoreCase("<select>")) {
                    if (!selectedLocName.equalsIgnoreCase("ALL")) {
                        teamUserDB = realm.where(TeamUserDB.class)
                                .equalTo("locationId", Integer.parseInt(selectedLocId))
                                .findFirst();
                        if (teamUserDB != null && teamUserDB.getUserNewCarDBList().where().equalTo("leadNewCar", 0).findFirst() != null) {
                            dseRealmList = teamUserDB.getUserNewCarDBList().where().equalTo("leadNewCar", 0).findFirst().getUsers();

                        }
                        if (dseRealmList != null) {
                            dseNamesList.clear();
                            dseIdsList.clear();
                        }
                        setDSEAdapter(dseRealmList);
                    } else if (selectedLocName.equalsIgnoreCase("ALL")) {
                        RealmResults<TeamUserDB> teamUserDBRealmList = realm.where(TeamUserDB.class)
                                .findAll();
                        if (teamUserDBRealmList != null) {
                            Iterator<TeamUserDB> it = teamUserDBRealmList.iterator();
                            dseNamesList.clear();
                            dseIdsList.clear();
                            while (it.hasNext()) {
                                teamUserDB = it.next();
                                if (teamUserDB != null && teamUserDB.getUserNewCarDBList().where().equalTo("leadNewCar", 0).findFirst() != null) {
                                    dseRealmList = teamUserDB.getUserNewCarDBList().where().equalTo("leadNewCar", 0).findFirst().getUsers();
                                    setDSEAdapter(dseRealmList);
                                }

                            }
                        }

                    }

                } else {
                    dialog1sales_edit2.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
/////////////////////////////////////////DSE NAME////////////////////////////////////////////
        dialog1sales_edit2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDseName = dseNamesList.get(position);
                selectedDseId = dseIdsList.get(position);
                System.out.println("Saleside" + selectedDseId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void setDSEAdapter(RealmList<UsersDB> dseRealmList) {
        if (dseRealmList != null) {
            Iterator<UsersDB> itr = dseRealmList.iterator();
            if (tv_sale_consultant_name.getText().toString().equalsIgnoreCase("")) {
                dseNamesList.add("<select>");
                dseIdsList.add("" + 0);

            }
            while (itr.hasNext()) {
                UsersDB temp = itr.next();
                dseNamesList.add(temp.getName());
                dseIdsList.add(temp.getId());

            }
            selectedDsePos = dseNamesList.indexOf(tv_sale_consultant_name.getText().toString().trim());
            if (selectedDsePos == -1) {
                selectedDsePos = 0;
            }
        }
        ArrayAdapter<String> dseAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, dseNamesList);
        dseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dialog1sales_edit2.setAdapter(dseAdapter);
        dialog1sales_edit2.setSelection(selectedDsePos);

    }

    public void addEmails() {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findFirst();
        email_items.clear();
        if (salesCRMRealmTable != null && salesCRMRealmTable.customerEmailId != null) {
            if (!salesCRMRealmTable.customerEmailId.isEmpty()) {
                for (int i = 0; i < salesCRMRealmTable.customerEmailId.size(); i++) {
                    System.out.println("EMAIL" + salesCRMRealmTable.customerEmailId.get(i).getEmail() + " STATUS" + salesCRMRealmTable.customerEmailId.get(i).getEmailStatus());
                    if (salesCRMRealmTable.customerEmailId.get(i).getEmailStatus() != null && salesCRMRealmTable.customerEmailId.get(i).getEmailStatus().equalsIgnoreCase("PRIMARY")) {
                        email_items.add(i, new Detail_Email_model(salesCRMRealmTable.customerEmailId.get(i).getEmail(), 1));
                    } else {
                        email_items.add(i, new Detail_Email_model(salesCRMRealmTable.customerEmailId.get(i).getEmail(), 0));
                    }

                }
            }
            email_adapter = new Detail_Email_Adapter(getActivity(), email_items, this, isLeadActive());
            list_add_email.setAdapter(email_adapter);
            email_adapter.notifyDataSetChanged();
            changeEmailListViewHeight();
        }
    }

    /**
     * api integration done here for editing an email id
     *
     * @param position
     */
    public void CallEditEmailApi(final int position) {
        if (salesCRMRealmTable.customerEmailId != null && !salesCRMRealmTable.customerEmailId.isEmpty()) {
            // if (!salesCRMRealmTable.customerEmailId.isEmpty()) {
            final CustomerEmailId obj = salesCRMRealmTable.customerEmailId.get(position);
            System.out.println("EMAIL TO BE EDITTED" + obj.getEmail() + " id: " + obj.getLead_email_mapping_id());
            email_mapping_id = "" + obj.getLead_email_mapping_id();

            if (connectionDetectorService.isConnectingToInternet()) {
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Saving your changes..");
                progressDialog.show();
                DetailFragmentServiceHandler.getInstance(DetailsFragment.this.getContext()).editEmailServiceHandler
                        (salesCRMRealmTable.getLeadLastUpdated(), email_mapping_id, pref.getAccessToken(), pref.getLeadID(), dialog1_edit_email.getText().toString().trim(), position);

                // }
            } else {
                Util.showToast(getContext(), "No internet connections", Toast.LENGTH_LONG);


            }
        }


    }

    /**
     * handling deleting a photo
     *
     * @param position
     */
    @Override
    public void Calldelete_number_api(final int position) {
        if (salesCRMRealmTable.customerPhoneNumbers != null && connectionDetectorService.isConnectingToInternet()) {
            if (!salesCRMRealmTable.customerPhoneNumbers.isEmpty()) {
                CustomerPhoneNumbers obj = salesCRMRealmTable.customerPhoneNumbers.get(position);
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Deleting mobile number..");
                progressDialog.show();
                DetailFragmentServiceHandler.getInstance(DetailsFragment.this.getContext())
                        .deleteNumberServiceHandler(salesCRMRealmTable.getLeadLastUpdated(),
                                "" + obj.getLead_phone_mapping_id(), pref.getAccessToken(), Integer.parseInt(pref.getLeadID()), obj.getPhoneNumber() + "");

            }
        } else if (!connectionDetectorService.isConnectingToInternet()) {
            Util.showToast(getContext(), "No internet connections", Toast.LENGTH_LONG);
        }
    }

    /**
     * deleting an email which is secondaryr
     *
     * @param position
     */
    @Override
    public void Calldelete_email_api(final int position) {
        System.out.println("EMAIL POSITION DELETEd" + position);
        if (salesCRMRealmTable.customerEmailId != null && !salesCRMRealmTable.customerEmailId.isEmpty()) {
            // if (!salesCRMRealmTable.customerEmailId.isEmpty()) {
            final CustomerEmailId obj = salesCRMRealmTable.customerEmailId.get(position);
            email_mapping_id = "" + obj.getLead_email_mapping_id();
            if (connectionDetectorService.isConnectingToInternet()) {
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Deleting your mail id..");
                progressDialog.show();
                DetailFragmentServiceHandler.getInstance(DetailsFragment.this.getContext())
                        .deleteEmailServiceHandler(salesCRMRealmTable.getLeadLastUpdated(),
                                email_mapping_id, pref.getAccessToken(), Integer.parseInt(pref.getLeadID()));
            } else {
                Util.showToast(getContext(), "No internet connections", Toast.LENGTH_LONG);

            }

            // }
        }
    }

    /**
     * method to change the primary number
     *
     * @param position
     */

    public void changePrimaryNumber(final int position) {

        System.out.println("STARRED POSITION" + position);
        if (salesCRMRealmTable.customerPhoneNumbers != null && connectionDetectorService.isConnectingToInternet()) {
            if (!salesCRMRealmTable.customerPhoneNumbers.isEmpty()) {
                mob_id = "" + salesCRMRealmTable.customerPhoneNumbers.get(position).getPhoneNumberID();
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Changing your primary number..");
                progressDialog.show();
                DetailFragmentServiceHandler.getInstance(DetailsFragment.this.getContext()).changePrimaryNumberServiceHandler
                        (salesCRMRealmTable.getLeadLastUpdated(), pref.getAccessToken(), Integer.parseInt(pref.getLeadID()), mob_id, position);
            }
        } else if (!connectionDetectorService.isConnectingToInternet()) {
            Toast.makeText(getActivity(), "Please enable internet connection", Toast.LENGTH_LONG);
        }

    }

    /**
     * making an email primary
     *
     * @param position
     */
    @Override
    public void CallPrimaryEmail(final int position) {
        if (salesCRMRealmTable.customerEmailId != null && !salesCRMRealmTable.customerEmailId.isEmpty() && connectionDetectorService.isConnectingToInternet()) {
            //if (!salesCRMRealmTable.customerEmailId.isEmpty()) {
            emailID = salesCRMRealmTable.customerEmailId.get(position).getEmailID();
            email_id = salesCRMRealmTable.customerEmailId.get(position).getEmailID();
            System.out.println("POSITION : "+position);
            System.out.println("POSITION EMAIL: "+salesCRMRealmTable.customerEmailId.get(position).getEmail());
            System.out.println("POSITION ID: "+salesCRMRealmTable.customerEmailId.get(position).getEmailID());
            System.out.println("POSITION STATUS: "+salesCRMRealmTable.customerEmailId.get(position).getEmailStatus());
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Changing your primary mail id..");
            progressDialog.show();
            DetailFragmentServiceHandler.getInstance(DetailsFragment.this.getContext()).changePrimaryEmailIdServiceHandler(salesCRMRealmTable.getLeadLastUpdated(), pref.getAccessToken(), pref.getLeadID(), email_id, position);
        } else if (!connectionDetectorService.isConnectingToInternet()) {
            Toast.makeText(this.getActivity(), "Please enable internet connection", Toast.LENGTH_LONG);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        //DbUtils.updateCallingMobileNumbers(realm);
        addNumbers();
        addEmails();
        if (salesCRMRealmTable != null) {
            dseDetails = realm.where(DseDetails.class)
                    .equalTo("customerID", Integer.parseInt(pref.getLeadID())).findFirst();
            if (dseDetails != null && dseDetails.getDseName() != null) {
                tv_sale_consultant_name.setText(dseDetails.getDseName());
                if (salesCRMRealmTable.getLeadLocationName() != null) {
                    tvlocation_name.setText(salesCRMRealmTable.getLeadLocationName());
                    selectedLocId = salesCRMRealmTable.getLeadLocationId();
                    selectedLocName = salesCRMRealmTable.getLeadLocationName();
                }

                System.out.println("dsedetails" + dseDetails.getDseName());
            } else if (salesCRMRealmTable.getAllotDseAnswer() != null) {
                tv_sale_consultant_name.setText(realm.where(UsersDB.class)
                        .equalTo("id", salesCRMRealmTable.getAllotDseAnswer().getDseId() + "").findFirst().getName());
                tvlocation_name.setText(realm.where(LocationsDB.class)
                        .equalTo("id", salesCRMRealmTable.getAllotDseAnswer().getLocationId()).findFirst().getName());
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void createemail_edittext(final ViewGroup parent) {
        System.out.println("View Adding");
        LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textViewemail = new TextView(getContext());
        LinearLayout.LayoutParams lep = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        parent.addView(textViewemail, lep);
        textViewemail.setTextSize(16);
        textViewemail.setText(addEmailEditText.getText());

    }


    public void addNumbers() {
        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findFirst();
        mobile_items.clear();
        if (salesCRMRealmTable != null && salesCRMRealmTable.customerPhoneNumbers != null) {
            if (!salesCRMRealmTable.customerPhoneNumbers.isEmpty()) {
                mobileNumberLength = salesCRMRealmTable.customerPhoneNumbers.size();
                //System.out.println("MOBILE LENGTH"+mobileNumberLength);
                for (int i = 0; i < salesCRMRealmTable.customerPhoneNumbers.size(); i++) {
                    if (!(Long.toString(salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumber()).equals(""))) {
                        if (salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumberStatus() != null && salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumberStatus().equalsIgnoreCase("PRIMARY")) {
                            mobile_items.add(i, new Detail_Mobile_model(Long.toString(salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumber()), 1));
                        } else {
                            mobile_items.add(i, new Detail_Mobile_model(Long.toString(salesCRMRealmTable.customerPhoneNumbers.get(i).getPhoneNumber()), 0));
                        }

                    }
                }
            } else {
                mobile_items.add(0, new Detail_Mobile_model(salesCRMRealmTable.getCustomerNumber(), 1));
            }
            Iterator<Detail_Mobile_model> itr = mobile_items.iterator();
            while (itr.hasNext()) {
                Detail_Mobile_model obj = itr.next();
                System.out.println("MOBILE NUMBER" + obj.getNumber() + "STATUS" + obj.getValue());
            }
            adapter = new Details_Mobile_Adapter(getActivity(), mobile_items, this, isLeadActive());
            list_add_mobile.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            changeMobileListViewHeight();
        }
    }

    public void createmobile_edittext(final ViewGroup parent) {
        System.out.println("View Adding");
        LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = vi.inflate(R.layout.list_add_mobile, null);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        parent.addView(v, lp);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST) {
            visitingimg.setVisibility(View.VISIBLE);
            Bitmap mphoto = (Bitmap) data.getExtras().get("data");
            visitingimg.setImageBitmap(mphoto);
        }
        if (requestCode == RESULT_LOAD_IMAGE && null != data) {
            visitingimg.setVisibility(View.VISIBLE);
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            try {
                visitingimg.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        }
    }

    public void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo")) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                }
            }
        });
        builder.show();
    }

    public void handleCrmSourceEdit() {
        if (selectedCrmId != null && selectedDseId != null && selectedLocId != null &&
                !selectedCrmId.equalsIgnoreCase("0") && !selectedDseId.equalsIgnoreCase("0") && !selectedLocId.equalsIgnoreCase("0")) {
            if (connectionDetectorService.isConnectingToInternet()) {
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Saving your changes..");
                progressDialog.show();
                DetailFragmentServiceHandler.getInstance(getContext()).editCrmSourceServiceHandler
                        (salesCRMRealmTable.getLeadLastUpdated(), pref.getAccessToken(), pref.getLeadID(), selectedCrmId, selectedDseId, selectedLocId, true, true, true);
            } else {
                Util.showToast(getContext(), "No internet connections", Toast.LENGTH_LONG);

            }

        } else {
            Toast.makeText(getActivity(), "Please select fields properly", Toast.LENGTH_LONG);
        }


    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        dialogedit_closing_date.setText(String.format(Locale.getDefault(), "%d-%d-%d", (dayOfMonth), (monthOfYear + 1), year));
        // dialogedit_closing_date.setText(String.format(Locale.getDefault(), "%d-%d-%d", (monthOfYear), (dayOfMonth), year));
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        dialogedit_closing_time.setText(String.format(Locale.getDefault(), "%d-%d-%d", hourOfDay, minute, second));
    }

    public void handleMessage() {
        adapter.notifyDataSetChanged();
        changeMobileListViewHeight();
    }

    /**
     * changing the mobile listview view
     */
    private void changeMobileListViewHeight() {
        ViewGroup.LayoutParams mParam = list_add_mobile.getLayoutParams();
        mParam.height = (int) (adapter.getCount() * Util.convertDpToPixel(40, getContext()));
        list_add_mobile.setLayoutParams(mParam);
    }

    /**
     * changinf the email ids listview view
     */
    private void changeEmailListViewHeight() {
        ViewGroup.LayoutParams mParam = list_add_email.getLayoutParams();
        mParam.height = (int) (email_adapter.getCount() * Util.convertDpToPixel(40, getContext()));
        list_add_email.setLayoutParams(mParam);
    }

    public void email_handleMessage() {
        System.out.println("Deleet:delete" + email_items.size());
        email_items.remove(index_email);
        email_adapter.notifyDataSetChanged();
        changeEmailListViewHeight();

    }


    @Override
    public void Calleditmobile(final int position) {
        dialog1 = new Dialog(getActivity());
        dialog1.setContentView(R.layout.mobile_edit_dialog);
        Window window = dialog1.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.y = 200;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        TextView txt1dialog2 = (TextView) dialog1.findViewById(R.id.mobileEditTitle);
        mobileNumberEditText = (EditText) dialog1.findViewById(R.id.mobileEditText);
        mobileNumberEditText.setText(mobile_items.get(position).getNumber());
        System.out.println(mobile_items.get(position).getNumber());
        Button cancel1 = (Button) dialog1.findViewById(R.id.cancel1_editmobile);
        Button save1 = (Button) dialog1.findViewById(R.id.save1_editmobile);
        cancel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });
        save1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mobileNumberEditText.getText().length() < 10) {
                    Toast.makeText(getActivity(), "Please enter a 10 digit mobile number", Toast.LENGTH_LONG).show();
                } else {
                    if (salesCRMRealmTable.customerPhoneNumbers != null) {
                        if (!salesCRMRealmTable.customerPhoneNumbers.isEmpty()) {
                            final CustomerPhoneNumbers obj = salesCRMRealmTable.customerPhoneNumbers.get(position);
                            System.out.println("PHONE NUMBER TO BE EDITTED" + obj.getPhoneNumber() + " id: " + obj.getLead_phone_mapping_id());
                            phone_mapping_id = "" + obj.getLead_phone_mapping_id();
                            if (connectionDetectorService.isConnectingToInternet()) {
                                progressDialog = new ProgressDialog(getContext());
                                progressDialog.setMessage("Saving your changes..");
                                progressDialog.show();
                                DetailFragmentServiceHandler.getInstance(DetailsFragment.this.getContext()).editNumberServiceHandler(salesCRMRealmTable.getLeadLastUpdated(), phone_mapping_id,
                                        pref.getAccessToken(), pref.getLeadID(), mobileNumberEditText.getText().toString().trim(), position);
                            } else {
                                Util.showToast(getContext(), "No internet connections", Toast.LENGTH_LONG);
                            }

                        }
                    }
                    dialog1.dismiss();

                }

            }
        });
        dialog1.show();
    }


    @Override
    public void Calleditemail(final int position) {
        dialog_email = new Dialog(getActivity());
        dialog_email.setContentView(R.layout.custom_email);
        Window window = dialog_email.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.y = 200;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        TextView txtdialog1 = (TextView) dialog_email.findViewById(R.id.txtdialog1);
        TextView txtdialog3 = (TextView) dialog_email.findViewById(R.id.txtdialog3);
        dialog1_edit_email = (EditText) dialog_email.findViewById(dialog1edit_email);
        dialog1_edit_email.setText(email_items.get(position).getEmail());
        System.out.println(email_items.get(position).getEmail());
        Button cancel = (Button) dialog_email.findViewById(R.id.cancel_email);
        Button save = (Button) dialog_email.findViewById(R.id.save_email);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_email.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Util.isValidEmail(dialog1_edit_email.getText().toString())) {
                    CallEditEmailApi(position);
                    dialog_email.dismiss();
                } else {
                    Util.showToast(getContext(), "Invalid email address", Toast.LENGTH_SHORT);
                }

            }
        });
        dialog_email.show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mobile_items != null) {
            mobile_items.clear();
        }
    }


    private void handleBuyerTypeEdit() {
        int selectedPosition = 0;
        dialogbuyer = new Dialog(getActivity());
        // connectionDetectorService = new ConnectionDetectorService(getContext());
        dialogbuyer.setContentView(R.layout.custom_dialog_buyertype);
        Window window = dialogbuyer.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.y = 200;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        buyerTypeSpinner = (Spinner) dialogbuyer.findViewById(R.id.editBuyerTypeDialog);
        ArrayAdapter<String> typeOfBuyerAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, WSConstants.buyerTypeList);
        typeOfBuyerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        buyerTypeSpinner.setAdapter(typeOfBuyerAdapter);
        selectedPosition = WSConstants.buyerTypeList.indexOf(salesCRMRealmTable.getBuyerType());
        if (selectedPosition == -1) {
            selectedPosition = 0;
        }
        buyerTypeSpinner.setSelection(selectedPosition);
        buyerTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedBuyerType = WSConstants.buyerTypeList.get(position);
                selectedBuyerTypeId = "" + position;
                if (position == 0) {
                    selectedBuyerType = "";
                    selectedBuyerTypeId = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        modeOfPaymentSpinner = (Spinner) dialogbuyer.findViewById(R.id.editModeOfPaymentSpinner);
        customerTypeSpinner = (Spinner) dialogbuyer.findViewById(R.id.edtCustomerTypeSpinner);

        ArrayAdapter<String> customerTypeAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, WSConstants.customerType);

        customerTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        customerTypeSpinner.setAdapter(customerTypeAdapter);
        selectedPosition = WSConstants.customerType.indexOf(salesCRMRealmTable.getTypeOfCustomer());
        if (selectedPosition == -1) {
            selectedPosition = 0;
        }
        customerTypeSpinner.setSelection(selectedPosition);
        customerTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    selectedTypeOfCustomerId = "";
                } else {
                    selectedTypeOfCustomerId = "" + position;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<String> modeOfPaymentAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, WSConstants.paymentModes);
        modeOfPaymentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        modeOfPaymentSpinner.setAdapter(modeOfPaymentAdapter);
        selectedPosition = WSConstants.paymentModes.indexOf(salesCRMRealmTable.getModeOfPayment());
        if (selectedPosition == -1) {
            selectedPosition = 0;
        }
        modeOfPaymentSpinner.setSelection(selectedPosition);
        modeOfPaymentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedmodeOfPayment = WSConstants.paymentModes.get(position);
                if (position == 0) {
                    selectedmodeOfPayment = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        dialogedit_closing_date = (TextView) dialogbuyer.findViewById(R.id.dialogedit_closing_date);
        dialogedit_closing_time = (TextView) dialogbuyer.findViewById(R.id.dialogedit_closing_time);
        dialogedit_closing_time.setVisibility(View.INVISIBLE);

        dialogedit_closing_time.setText("<Select Time>");
        if (salesCRMRealmTable.getCloseDate() == null) {
            dialogedit_closing_date.setText("<Select Date>");
        } else {
            dialogedit_closing_date.setText(salesCRMRealmTable.getCloseDate().toString());
        }

        Button cancel = (Button) dialogbuyer.findViewById(R.id.cancel_buyer);
        Button save = (Button) dialogbuyer.findViewById(R.id.save_buyer);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogbuyer.dismiss();

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assignValuesFromDB();
                closingDateTime = dialogedit_closing_date.getText().toString();
                if (closingDateTime.equalsIgnoreCase("<Select Date>")) {
                    closingDateTime = "";
                }

                if (connectionDetectorService.isConnectingToInternet()) {
                    progressDialog = new ProgressDialog(getContext());
                    progressDialog.setMessage("Saving Buyer types..");
                    progressDialog.show();
                    DetailFragmentServiceHandler.getInstance(DetailsFragment.this.getContext())
                            .buyerTypeServicehandler(pref.getAccessToken(), pref.getLeadID(), salesCRMRealmTable.getLeadLastUpdated().trim(),
                                    salesCRMRealmTable.getLeadLocationId(), selectedDseId,
                                    selectedEnqSourceId, selectedBuyerTypeId, closingDateTime,
                                    selectedmodeOfPayment, selectedTypeOfCustomerId);
                } else {
                    Util.showToast(getContext(), "No internet connections", Toast.LENGTH_LONG);
                }
            }
        });
        dialogedit_closing_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.showDatePicker(DetailsFragment.this, getActivity(), R.id.dialogedit_closing_date, WSConstants.TYPE_DATE_PICKER.BUYER_CLOSING_DATE);

            }
        });
        dialogedit_closing_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.showTimePicker(DetailsFragment.this, getActivity(), R.id.dialogedit_closing_time);

            }
        });
        dialogbuyer.show();
    }

    private void callSnacbar(String message) {
        snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_SHORT);
        viewSnacbar = snackbar.getView();
        tvSnacbar = (TextView) viewSnacbar.findViewById(android.support.design.R.id.snackbar_text);
        tvSnacbar.setGravity(Gravity.CENTER_HORIZONTAL);
        tvSnacbar.setTextColor(Color.WHITE);
        viewSnacbar.setBackgroundColor(Color.BLACK);
        snackbar.show();
    }

  /*  private void addEnquiryNumber() {
        int count = 0;
        RealmResults<IntrestedCarDetails> intrestedCarsList = realm.where(IntrestedCarDetails.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                .findAll();

        if (intrestedCarsList != null) {
            Iterator<IntrestedCarDetails> itr = intrestedCarsList.iterator();
            enquiryNumber = "";
            while (itr.hasNext()) {
                IntrestedCarDetails temp = itr.next();
                RealmList<CarDmsDataDB> carDmsDataDBtemp = temp.getCarDmsDataList();
                Iterator<CarDmsDataDB> itrCarDMs = carDmsDataDBtemp.iterator();
                while (itrCarDMs.hasNext()) {
                    CarDmsDataDB obj = itrCarDMs.next();
                    enquiryNumber += obj.getEnquiry_number();
                    count++;
                    if (count < carDmsDataDBtemp.size()) {
                        enquiryNumber += ", ";
                    } else if (itr.hasNext()) {
                        enquiryNumber += ", ";
                    }

                }
            }
        }
        if(enquiryNumber != null)
        tvenq.setText(enquiryNumber.replace("null,", ""));
    }*/

    /**
     * Called when the Fragment is no longer started.  This is generally
     * Activity's lifecycle.
     */
    @Override
    public void onStop() {
        super.onStop();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        SalesCRMApplication.getBus().register(this);
    }

    ////////////////////////////////////////////////////// /*Mobile Number OTTO callbacks Section Starts*//////////////////////////////////////////////////////////////
    @Subscribe
    public void addMobileNumberOttoNotify(final AddMobileNumberResponse addMobileResponse) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        final AddMobileNumberResponse addMobileNumberResponse = addMobileResponse.getAddMobileNumberResponse();
        if (addMobileNumberResponse.getResult() != null && addMobileNumberResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK) &&
                addMobileNumberResponse.getResult().getMobileData() != null) {
            mobile_items.clear();
            for (int i = 0; i < addMobileNumberResponse.getResult().getMobileData().length; i++) {
                AddMobileNumberResponse.MobileData mobileData = addMobileNumberResponse.getResult().getMobileData()[i];
                phone_mapping_id = mobileData.getLead_phone_mapping_id();
                phone_number = mobileData.getNumber();
                mob_id = mobileData.getMobile_number_id();
                if (mobileData.getStatus() != null && mobileData.getStatus().equalsIgnoreCase("PRIMARY")) {
                    mobile_items.add(i, new Detail_Mobile_model(mobileData.getNumber(), 1));
                } else {
                    mobile_items.add(i, new Detail_Mobile_model(mobileData.getNumber(), 0));
                }
            }
            adapter.notifyDataSetChanged();
            changeMobileListViewHeight();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    DetailsPageDbHandler.getInstance().addMobToRealmOnline(Integer.parseInt(pref.getLeadID()), realm, addMobileNumberResponse);

                }
            });

        }

        LeadMobileMappingDbOperations.updateCallingMobileNumbers(realm, addMobileResponse);

    }

    @Subscribe
    public void deleteMobileNumberOttoNotify(final DeleteMobileResponse deleteMobileResponse) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        System.out.println("Called:onDELETEMOBILE");
        final DeleteMobileResponse deleteMobResp = deleteMobileResponse.getDeleteMobileResponse();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                CustomerPhoneNumbers result =
                        realm.where(CustomerPhoneNumbers.class).equalTo("lead_phone_mapping_id",
                                deleteMobResp.getLeadMappingId()).findFirst();
                if (result.isValid()) {
                    result.deleteFromRealm();
                }
                SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                        .findFirst();

                if (deleteMobResp.getResult() != null &&
                        deleteMobResp.getResult().getLead_last_updated() != null &&
                        !deleteMobResp.getResult().getLead_last_updated().equalsIgnoreCase("")) {
                    salesCRMRealmTable.setLeadLastUpdated(deleteMobResp.getResult().getLead_last_updated());
                    System.out.println("DB DATE LAST: " + deleteMobResp.getResult().getLead_last_updated());
                }

                addNumbers();
            }
        });
        LeadMobileMappingDbOperations.updateCallingMobileNumbers(realm, deleteMobileResponse);
    }

    @Subscribe
    public void editMobileNumberOttoNotify(final EditMobileNumberResponse editResponse) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        System.out.println("Called:onEDITMOBILE");
        final EditMobileNumberResponse editMobileNumberResponse = editResponse.getEditMobileNumberResponse();
        if (editMobileNumberResponse.getResult() != null && editMobileNumberResponse.getResult().getMobileData() != null &&
                editMobileNumberResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            for (int i = 0; i < editMobileNumberResponse.getResult().getMobileData().length; i++) {
                EditMobileNumberResponse.MobileData mobileData = editMobileNumberResponse.getResult().getMobileData()[i];
                if (mobileData.getStatus().equalsIgnoreCase("PRIMARY")) {
                    mobile_items.set(i, new Detail_Mobile_model(mobileData.getNumber(), 1));
                } else {
                    mobile_items.set(i, new Detail_Mobile_model(mobileData.getNumber(), 0));
                }
                if (mobileData.getLead_phone_mapping_id().equalsIgnoreCase(phone_mapping_id)) {
                    edittedNumberObj = mobileData;
                }

            }
            adapter.notifyDataSetChanged();
            changeMobileListViewHeight();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    DetailsPageDbHandler.getInstance().updateEdittedNumber(realm, edittedNumberObj,
                            Long.parseLong(phone_mapping_id), editMobileNumberResponse.getPosition(),
                            Integer.parseInt(pref.getLeadID()), editMobileNumberResponse.getResult().getLead_last_updated());

                }
            });
        }
        LeadMobileMappingDbOperations.updateCallingMobileNumbers(realm, editResponse);
    }

    @Subscribe
    public void primaryMobileNumberOttoNotify(final PrimaryMobileResponse primaryResponse) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        final PrimaryMobileResponse primaryMobileResponse = primaryResponse.getPrimaryMobileResponse();
        if (primaryMobileResponse != null && primaryMobileResponse.getStatusCode().equalsIgnoreCase("2002")) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    DetailsPageDbHandler.getInstance().updatePrimaryMobileNumber(realm,
                            mob_id, Integer.parseInt(pref.getLeadID()),
                            primaryMobileResponse.getPosition(), primaryMobileResponse.getResult().getLead_last_updated());
                    addNumbers();
                }
            });
        } else {
            if (primaryMobileResponse == null) {
                callSnacbar("This operation cannot be performed");
            } else {
                callSnacbar(primaryMobileResponse.getMessage());
            }

            Toast.makeText(this.getActivity(), "This operation cannot be performed", Toast.LENGTH_LONG);
        }
    }

    /////////////////////////////////////////////////////////////////*Mobile Number Section Ends*/////////////////////////////////////////////////////////////////////


    @Subscribe
    public void addEmailOttoNotify(AddEmailAddressResponse addEmailResponse) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        final AddEmailAddressResponse addEmailAddressResponse = addEmailResponse.getAddEmailAddressResponse();
        if (addEmailAddressResponse != null && addEmailAddressResponse.getResult() != null &&
                addEmailAddressResponse.getResult().getEmailData() != null &&
                addEmailAddressResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            email_items.clear();
            for (int i = 0; i < addEmailAddressResponse.getResult().getEmailData().length; i++) {
                AddEmailAddressResponse.EmailData emailData = addEmailAddressResponse.getResult().getEmailData()[i];
                email_mapping_id = emailData.getLead_email_mapping_id();
                email_id = emailData.getEmail_address_id();
                if (emailData.getStatus() != null && emailData.getStatus().equalsIgnoreCase("PRIMARY")) {
                    email_items.add(i, new Detail_Email_model(emailData.getAddress(), 1));
                } else {
                    email_items.add(i, new Detail_Email_model(emailData.getAddress(), 0));
                }

            }

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    DetailsPageDbHandler.getInstance().addEmailToRealmOnline(realm, addEmailAddressResponse, Integer.parseInt(pref.getLeadID()));
                    salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                            .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                            .findFirst();

                }
            });
            email_adapter.notifyDataSetChanged();
            changeEmailListViewHeight();
        }

    }

    @Subscribe
    public void deleteEmailIdOttoNotify(final DeleteEmailResponse deleteEmailResponse) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        System.out.println("Called:onDELeteEmail");
        final DeleteEmailResponse deleteEmailResp = deleteEmailResponse.getDeleteEmailResponse();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                CustomerEmailId result =
                        realm.where(CustomerEmailId.class).equalTo("lead_email_mapping_id", deleteEmailResp.getEmailMappingId()).findFirst();
                if (result.isValid()) {
                    result.deleteFromRealm();
                }
                SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", Integer.parseInt(pref.getLeadID()))
                        .findFirst();
                if (deleteEmailResp.getResult() != null &&
                        deleteEmailResp.getResult().getLead_last_updated() != null &&
                        !deleteEmailResp.getResult().getLead_last_updated().equalsIgnoreCase("")) {
                    salesCRMRealmTable.setLeadLastUpdated(deleteEmailResp.getResult().getLead_last_updated());
                    System.out.println("DB EMAIL DATE LAST: " + deleteEmailResp.getResult().getLead_last_updated());
                }
                addEmails();
            }
        });
    }

    @Subscribe
    public void editEmailOttoNotify(final EditEmailResponse editResponse) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        System.out.println("Called:onEDITEmail");
        final EditEmailResponse editEmailResponse = editResponse.getEditEmailResponse();
        if (editEmailResponse != null && editEmailResponse.getResult().getEmailData() != null
                && editEmailResponse.getResult().getEmailData() != null
                && editEmailResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            for (int i = 0; i < editEmailResponse.getResult().getEmailData().length; i++) {
                EditEmailResponse.EmailData emailData = editEmailResponse.getResult().getEmailData()[i];
                if (emailData.getStatus().equalsIgnoreCase("PRIMARY")) {
                    email_items.set(i, new Detail_Email_model(emailData.getAddress(), 1));
                } else {
                    email_items.set(i, new Detail_Email_model(emailData.getAddress(), 0));
                }
                if (emailData.getLead_email_mapping_id().equalsIgnoreCase(email_mapping_id)) {
                    edittedEmailObj = emailData;
                }

            }

            email_adapter.notifyDataSetChanged();
            changeEmailListViewHeight();
        }

        realm.executeTransaction(new Realm.Transaction() {
            @Override

            public void execute(Realm realm) {
                DetailsPageDbHandler.getInstance().updateEmailInRealm(realm, edittedEmailObj,
                        Long.parseLong(email_mapping_id), editEmailResponse.getPosition(),
                        Integer.parseInt(pref.getLeadID()), editEmailResponse.getResult().getLead_last_updated());
            }
        });


    }

    @Subscribe
    public void primaryEmailIdOttoNotify(final ChangePrimaryEmailResponse primaryResponse) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        System.out.println("Called:onStarMOBILE");
        System.out.println("MOB ID: " + mob_id);
        final ChangePrimaryEmailResponse changePrimaryEmailResponse = primaryResponse.getChangePrimaryEmailResponse();
        if (changePrimaryEmailResponse != null && changePrimaryEmailResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    DetailsPageDbHandler.getInstance().updatePrimaryEmailId(realm, emailID, Integer.parseInt(pref.getLeadID()),
                            changePrimaryEmailResponse.getPosition(), changePrimaryEmailResponse.getResult().getLead_last_updated());

                }
            });
            System.out.println("Called addEmail");
            addEmails();
        } else if (changePrimaryEmailResponse != null && !changePrimaryEmailResponse.getStatusCode().equalsIgnoreCase("2002")) {
            Toast.makeText(getContext(), changePrimaryEmailResponse.getMessage(), Toast.LENGTH_LONG);
        }
    }

    @Subscribe
    public void buyerTypeEditCallBack(final SaveEditLeadResponse saveEditLeadResponse) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        dialogbuyer.dismiss();

        final SaveEditLeadResponse saveeditresponse = (SaveEditLeadResponse) saveEditLeadResponse;
        if (saveeditresponse != null && saveEditLeadResponse.getStatusCode() != null && saveeditresponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    DetailsPageDbHandler.getInstance().updateBuyerType(realm, Integer.parseInt
                                    (pref.getLeadID()), selectedmodeOfPayment,
                            selectedBuyerType, selectedBuyerTypeId, closingDateTime, selectedTypeOfCustomerId,
                            selectedEnqSourceId, selectedDseId, selectedDseName, selectedLocId,
                            selectedLocName, false, true, saveEditLeadResponse.getResult().getLead_last_updated());

                    tvtbuyertype.setText(selectedBuyerType);
                    tvclosingdate.setText(closingDateTime);
                    tv_typeofPayment.setText(selectedmodeOfPayment);
                    if (!selectedTypeOfCustomerId.equalsIgnoreCase("") && !selectedTypeOfCustomerId.equalsIgnoreCase("0")) {
                        tv_typeofcustomer.setText(WSConstants.customerType.get(Integer.parseInt(selectedTypeOfCustomerId)));
                    }
                    C360RestartModel restartModel = new C360RestartModel();
                    restartModel.setTabPosition(2);
                    SalesCRMApplication.getBus().post(restartModel);

                    // salesCRMRealmTable.setLead_last_updated(saveeditresponse.getResult().getLead_last_updated());
                }
            });

        } else if (saveeditresponse != null && saveEditLeadResponse.getStatusCode() != null &&
                !saveeditresponse.getStatusCode().equalsIgnoreCase("2002")) {
            Toast.makeText(getContext(), saveeditresponse.getMessage(), Toast.LENGTH_LONG);
        }
    }

    @Subscribe
    public void customerDetailsOttoNotify(CustomerEditResponse customerEditResponse) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        final CustomerEditResponse custeditresponse = customerEditResponse.getCustomerEditResponse();
        if (custeditresponse != null && custeditresponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {

            switch (custeditresponse.getTypeOfEdit()) {
                ////////////////////////////////personal details edited//////////////////////////////////
                case WSConstants.PERSONAL_DETAILS:
                    if (custeditresponse != null && custeditresponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                DetailsPageDbHandler.getInstance().updateCustomerdetails(realm, Integer.parseInt
                                                (pref.getLeadID()), "", genderSelected.trim(),
                                        edit_dialog_first_name.getText().toString().trim(),
                                        edit_dialog_last_name.getText().toString().trim(),
                                        edit_dialog_age.getText().toString(), false, true, custeditresponse.getResult().getLead_last_updated());
                            }
                        });
                        dialog_first_name.dismiss();
                        if (!edit_dialog_first_name.getText().toString().equalsIgnoreCase("")) {
                            tvfname.setText(edit_dialog_first_name.getText().toString());
                        } else {
                            tvfname.setText("-");
                        }
                        if (!edit_dialog_last_name.getText().toString().equalsIgnoreCase("")) {
                            tvlastname.setText(edit_dialog_last_name.getText().toString());
                        } else {
                            tvlastname.setText("-");
                        }
                        if (!genderSelected.equalsIgnoreCase("")) {
                            tvgender.setText(genderSelected);
                        } else {
                            tvgender.setText("-");
                        }

                        tvage.setText(edit_dialog_age.getText().toString());
                    } /*else if (custeditresponse != null && !custeditresponse.getStatusCode().equalsIgnoreCase("2002")) {
                        Toast.makeText(getContext(), custeditresponse.getMessage(), Toast.LENGTH_LONG);
                    }*/
                    break;
                case WSConstants.COMPANY_DETAILS:
                    dialogcmpnyname.dismiss();
                    if (custeditresponse != null && custeditresponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                DetailsPageDbHandler.getInstance().updateOfficeDetails(realm, Integer.parseInt
                                                (pref.getLeadID()), edit_dialog_occupation.getText().toString().trim(),
                                        edit_dialog_office_address.getText().toString().trim(),
                                        edit_dialog_office_pincode.getText().toString().trim(),
                                        officeLocality.getSelectedItem().toString(),
                                        edit_dialog_office_city.getText().toString().trim(),
                                        edit_dialog_office_state.getText().toString().trim(),
                                        edit_dialog_company.getText().toString().trim(), false, true, custeditresponse.getResult().getLead_last_updated());

                                //tv_designation.setText(edit_dialog_occupation.getText().toString() + ", " + edit_dialog_company.getText().toString().trim());
                                tv_designation.setText(edit_dialog_occupation.getText().toString());
                                //tvofficeaddress.setText(edit_dialog_office_address.getText().toString());
                                setOfficeAddress();
                                tvofficepincode.setText(edit_dialog_office_pincode.getText().toString());
                                tv_company_name.setText(edit_dialog_company.getText().toString().trim());

                            }
                        });


                    }

                    break;
                ////////////////////////////////////residence details edited//////////////////////////////////
                case WSConstants.RESIDENCE_DETAILS:
                    dialog_residence.dismiss();
                    if (custeditresponse != null && custeditresponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                DetailsPageDbHandler.getInstance().updateResidenceType(realm, Integer.parseInt
                                                (pref.getLeadID()),
                                        dialog1edit_residence.getText().toString().trim(),
                                        dialogedit_residence_pin.getText().toString().trim(),
                                        residenceLocality.getSelectedItem().toString(),
                                        dialogedit_residence_city.getText().toString().trim(),
                                        dialogedit_residence_state.getText().toString().trim(),
                                        false, true,
                                        custeditresponse.getResult().getLead_last_updated());
                            }
                        });
//                        tv_residence_address.setText(dialog1edit_residence.getText().toString());
                        setHomeAddress();
                        tvresidencepincode.setText(dialogedit_residence_pin.getText().toString());


                    }
                    break;

            }
        } else if (custeditresponse != null && !custeditresponse.getStatusCode().equalsIgnoreCase("2002")) {
            Toast.makeText(getActivity(), custeditresponse.getMessage(), Toast.LENGTH_LONG);
        } else {
            Toast.makeText(getActivity(), "There is some problem, Please try after sometime", Toast.LENGTH_LONG);
        }


    }
    void setHomeAddress() {
       if(salesCRMRealmTable!=null && salesCRMRealmTable.isValid()  && tv_residence_address!=null && Util.isNotNull(salesCRMRealmTable.getFullResidenceAddress())) {
          tv_residence_address.setText(salesCRMRealmTable.getFullResidenceAddress());

       }
       else if(tv_residence_address!=null) {
           tv_residence_address.setText("-");
       }
    }
    void setOfficeAddress() {
       if(salesCRMRealmTable!=null && salesCRMRealmTable.isValid()  && tvofficeaddress!=null && Util.isNotNull(salesCRMRealmTable.getFullOfficeAddress())) {
           tvofficeaddress.setText(salesCRMRealmTable.getFullOfficeAddress());
       }
       else if(tvofficeaddress!=null) {
           tvofficeaddress.setText("-");
       }
    }

    @Subscribe
    public void crmSourceEditCallback(final LocationEditResponse locationEditResponse) {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        dialog.dismiss();
        if (locationEditResponse != null && locationEditResponse.getStatusCode() != null &&
                locationEditResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            // tvlocation_name.setText(selectedLocName);
            // tv_sale_consultant_name.setText(selectedDseName);
            tv_crmsourcetype.setText(selectedCrmSrc);
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    DetailsPageDbHandler.getInstance().updateLocationData(realm,
                            Integer.parseInt(pref.getLeadID()), selectedLocName, selectedLocId,
                            selectedDseName, selectedDseId, selectedCrmSrc, selectedCrmId, false, true, locationEditResponse.getResult().getLead_last_updated());

                }
            });
        } else if (locationEditResponse != null && locationEditResponse.getStatusCode() != null &&
                !locationEditResponse.getStatusCode().equalsIgnoreCase("2002")) {

            Toast.makeText(getActivity(), locationEditResponse.getMessage(), Toast.LENGTH_LONG);
        }

    }

    private boolean isLeadActive() {
        RealmResults<SalesCRMRealmTable> data = realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID())).findAll();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getIsLeadActive() == 0) {
                return false;
            }
        }
        return true;
    }

}




