import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, NativeModules, TouchableWithoutFeedback, KeyboardAvoidingView, ScrollView, TextInput, BackHandler, Modal
} from 'react-native'
import DateTimePicker from 'react-native-modal-datetime-picker';
//import Modal from "react-native-modal";
import SelectPopUpItem from "./popup_options_lead_detail"
import CustomDateTimePicker from '../../utils/DateTimePicker'
import Utils from '../../utils/Utils'
import RestClient from '../../network/rest_client'

const styles = StyleSheet.create({
    top_view: {
        flex: 1,
        alignItems: 'center',
    },
    lead_detail_view: {
        margin: 5,
        alignSelf: 'stretch',
    },
    customer_detail_view: {
        margin: 5,
        alignSelf: 'stretch',
    },
    lead_detail: {
        padding: 10,
        backgroundColor: '#fff'
    },
    heading: {
        fontSize: 16,
        color: '#000000',
        fontWeight: 'bold',
        marginTop: 5, marginBottom: 5,
        marginRight: 5
    },
    title: {
        fontSize: 16,
        color: '#000000',
        margin: 5
    },
    grey_line: {
        height: 1,
        backgroundColor: "#808080",
        marginBottom: 10,
        marginTop: 10
    },
    save: {
        backgroundColor: "#007fff"
    },
    cancel: {
        backgroundColor: "#808080"
    },
    listItem: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20,
        paddingBottom: 20,
        fontSize: 16,
        color: '#303030'
    },
    loading: {
        backgroundColor: "#fff",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: "center",
        alignItems: "center",
        opacity: 0.5,
        position: "absolute",
        alignSelf: "center",
        justifyContent: "center"
    },
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        padding: 20

    },
    textInputStyle: {
        borderColor: '#CFCFCF',
        borderBottomWidth: 1,
        marginTop: 8,
        color: '#303030',
        padding: 5
    },
})

const lead_mode_of_payment = ["FullCash", "Company Lease Plan", "In House Loan", "Out House Loan"];

export default class EditLeadDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isPickerVisible: false,
            type_of_picker: '',
            type_of_customer: this.props.leadDetails.type_of_customer,
            mode_of_payment: this.props.leadDetails.mode_of_payment,
            close_date: this.props.leadDetails.close_date,
            title: this.props.leadDetails.title,
            last_name: this.props.leadDetails.last_name,
            first_name: this.props.leadDetails.first_name,
            age: this.props.leadDetails.age + "",
            pin_code: this.props.leadDetails.pin_code,
            full_address: this.props.leadDetails.full_address,
            locality: this.props.leadDetails.locality,
            city: this.props.leadDetails.city,
            residence_state: this.props.leadDetails.state,
            office_pincode: this.props.leadDetails.office_pin_code,
            office_address: this.props.leadDetails.office_address,
            office_locality: this.props.leadDetails.office_locality,
            office_city: this.props.leadDetails.office_city,
            office_state: this.props.leadDetails.state,
            company_name: this.props.leadDetails.company_name,
            occupation: this.props.leadDetails.occupation,
            residence_locality_array: [],
            office_locality_array: [],
            dataItemPicker: {},
            dateTimePickerVisibility: false,
            dateTimePickerPayload: {},
            pincodeFetching: false,
            mobile_number: "",
            email: "",
            editContactInfo: this.props.editedContactInfo,
        }
    }



    _openPicker = (id) => {
        if (id == "close_date") {
            this.setState({
                dateTimePickerPayload: { from: 'evaluator_request_date' },
                dateTimePickerVisibility: true, datePickerMode: 'date',
                dateTimePickerMinimumDate: new Date(),
                dateTimePickerMaximumDate: undefined,
                type_of_picker: id
            }
            )
        }
        else {
            this.setState({ isPickerVisible: true, type_of_picker: id })
        }
    }

    _getLocality = (id) => {
        if (id == "office_locality") {
            //console.log("this.state.office_locality_array", JSON.stringify(this.state.office_locality_array))
            if (this.state.office_locality_array && this.state.office_locality_array.length == 0) {
                this.getPincodeDetails(this.state.office_pincode, "office_pincode");
            }
            this.setState({ isPickerVisible: true, type_of_picker: id })
        } if (id == "residence_locality") {
            if (this.state.residence_locality_array && this.state.residence_locality_array.length == 0) {
                this.getPincodeDetails(this.state.pin_code, "residence_pincode");
            }
            this.setState({ isPickerVisible: true, type_of_picker: id })
        }
    }

    onSave(editId) {
        if (editId == "add_number") {
            var { mobile_number } = this.state;
            this.props.onSave(editId, mobile_number);
            this.setState({ mobile_number: "" })
        }
        if (editId == "add_email") {
            var { email } = this.state;
            this.props.onSave(editId, email);
            this.setState({ email: "" })
        }
    }

    onCancel() {
        this.props.onCancel();
        this.setState({
            type_of_customer: this.props.leadDetails.type_of_customer,
            mode_of_payment: this.props.leadDetails.mode_of_payment,
            close_date: this.props.leadDetails.close_date,
            title: this.props.leadDetails.title,
            last_name: this.props.leadDetails.last_name,
            first_name: this.props.leadDetails.first_name,
            age: this.props.leadDetails.age,
            pin_code: this.props.leadDetails.pin_code,
            full_address: this.props.leadDetails.full_address,
            locality: this.props.leadDetails.locality,
            city: this.props.leadDetails.city,
            residence_state: this.props.leadDetails.state,
            office_pincode: this.props.leadDetails.office_pin_code,
            office_address: this.props.leadDetails.office_address,
            office_locality: this.props.leadDetails.office_locality,
            office_city: this.props.leadDetails.office_city,
            office_state: this.props.leadDetails.state,
            company_name: this.props.leadDetails.company_name,
            occupation: this.props.leadDetails.occupation,
            mobile_number: "",
            email: "",
        })
    }

    generateView(editId, details) {
        let data = {
            type_of_customer: this.state.type_of_customer,
            mode_of_payment: this.state.mode_of_payment,
            close_date: this.state.close_date,
            title: this.state.title,
            last_name: this.state.last_name,
            first_name: this.state.first_name,
            age: this.state.age,
            pin_code: this.state.pin_code,
            full_address: this.state.full_address,
            locality: this.state.locality,
            city: this.state.city,
            residence_state: this.state.residence_state,
            office_pincode: this.state.office_pincode,
            office_address: this.state.office_address,
            office_locality: this.state.office_locality,
            office_city: this.state.office_city,
            office_state: this.state.office_state,
            company_name: this.state.company_name,
            occupation: this.state.occupation,
        }
        switch (editId) {
            case 'edit_lead':
                return (<View style={{ backgroundColor: "#fff", padding: 20 }}>
                    <Text style={styles.heading}>Customer type</Text>
                    <TouchableOpacity onPress={() => this._openPicker("type_of_customer")}>
                        <Text style={styles.title}>{(this.state.type_of_customer) ? this.state.type_of_customer : "---"}</Text>
                    </TouchableOpacity>

                    <Text style={styles.heading}>Mode of Payment</Text>
                    <TouchableOpacity onPress={() => this._openPicker("mode_of_payment")}>
                        <Text style={styles.title}>{(this.state.mode_of_payment) ? this.state.mode_of_payment : "---"}</Text>
                    </TouchableOpacity>

                    <Text style={styles.heading}>Expected buying date</Text>
                    <TouchableOpacity onPress={() => this._openPicker("close_date")}>
                        <Text style={styles.title}>{(this.state.close_date) ? this.state.close_date : "---"}</Text>
                    </TouchableOpacity>

                    <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080' }} onPress={() => this.onCancel()}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: "#007fff" }} onPress={() => this.props.onSave(editId, data)}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Save</Text>
                        </TouchableOpacity>
                    </View>
                </View>);
            case 'edit_customer':
                return (<View style={{ backgroundColor: "#fff", padding: 20 }}>
                    <Text style={styles.heading}>Name</Text>
                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        placeholder='Enter'
                        value={this.state.first_name}
                        onChangeText={(text) => {
                            this.setState({ first_name: text });
                        }}
                    />
                    {/* <Text style={styles.title}>{(details.first_name)? details.first_name : "---"}</Text> */}

                    <Text style={styles.heading}>Last name</Text>
                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        placeholder='Enter'
                        value={this.state.last_name}
                        onChangeText={(text) => {
                            this.setState({ last_name: text });
                        }}
                    />
                    {/* <Text style={styles.title}>{(details.last_name)? details.last_name : "---"}</Text> */}

                    <Text style={styles.heading}>Title</Text>
                    <TouchableOpacity onPress={() => this._openPicker("title")}>
                        <Text style={styles.title}>{(this.state.title) ? this.state.title : "---"}</Text>
                    </TouchableOpacity>

                    <Text style={styles.heading}>Age</Text>
                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        placeholder='Enter age'
                        keyboardType='numeric'
                        value={this.state.age + ''}
                        onChangeText={(text) => {
                            this.setState({ age: text });
                        }}
                    />
                    {/* <Text style={styles.title}>{(details.age)? details.age : "---"}</Text> */}

                    <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080' }} onPress={() => this.onCancel()}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: "#007fff" }} onPress={() => this.props.onSave(editId, data)}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Save</Text>
                        </TouchableOpacity>
                    </View>
                </View>);

            case 'edit_residence_address':
                return (<View style={{ backgroundColor: "#fff", padding: 20 }}>
                    <Text style={styles.heading}>Residence pincode</Text>
                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        maxLength={6}
                        placeholder='Enter'
                        value={this.state.pin_code}
                        onChangeText={(text) => {
                            console.log("does it get called?")
                            this.setState({ pin_code: text });
                            if (text.length == 6) {
                                this.getPincodeDetails(text, "residence_pincode");
                            }

                        }}
                    />
                    {/* <Text style={styles.title}>{(details.first_name)? details.first_name : "---"}</Text> */}

                    <Text style={styles.heading}>Residence Address</Text>
                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        placeholder='Enter'
                        multiline={true}
                        value={this.state.full_address}
                        onChangeText={(text) => {
                            this.setState({ full_address: text });
                        }}
                    />
                    {/* <Text style={styles.title}>{(details.last_name)? details.last_name : "---"}</Text> */}

                    <Text style={styles.heading}>Locality</Text>
                    <TouchableOpacity onPress={() => (this.state.residence_locality_array && this.state.residence_locality_array.length > 0) ? this._openPicker("residence_locality") : this._getLocality("residence_locality")}>
                        <Text style={styles.title}>{(this.state.locality) ? this.state.locality : "Select Locality"}</Text>
                    </TouchableOpacity>

                    <Text style={styles.heading}>City</Text>
                    <Text style={styles.title}>{(this.state.city) ? this.state.city : "---"}</Text>


                    <Text style={styles.heading}>State</Text>
                    <Text style={styles.title}>{(this.state.residence_state) ? this.state.residence_state : "---"}</Text>


                    <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080' }} onPress={() => this.onCancel()}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: "#007fff" }} onPress={() => this.props.onSave(editId, data)}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Save</Text>
                        </TouchableOpacity>
                    </View>

                    {this.state.pincodeFetching && <View style={[styles.loading, { position: 'absolute' }]}>
                        <ActivityIndicator size="small" color="#007fff" />
                    </View>}
                </View>);
            case 'edit_office_address':
                return (<View style={{ backgroundColor: "#fff", padding: 20 }}>
                    <Text style={styles.heading}>Company name</Text>
                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        placeholder='Enter'
                        value={this.state.company_name}
                        onChangeText={(text) => {
                            this.setState({ company_name: text });
                        }}
                    />
                    <Text style={styles.heading}>Designation</Text>
                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        placeholder='Enter'
                        value={this.state.occupation}
                        onChangeText={(text) => {
                            this.setState({ occupation: text });
                        }}
                    />
                    <Text style={styles.heading}>office pincode</Text>
                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        maxLength={6}
                        placeholder='Enter'
                        value={this.state.office_pincode}
                        onChangeText={(text) => {
                            this.setState({ office_pincode: text });
                            if (text.length == 6) {
                                this.getPincodeDetails(text, "office_pincode");
                            }
                        }}
                    />
                    {/* <Text style={styles.title}>{(details.first_name)? details.first_name : "---"}</Text> */}

                    <Text style={styles.heading}>Office Address</Text>
                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        placeholder='Enter'
                        value={this.state.office_address}
                        onChangeText={(text) => {
                            this.setState({ office_address: text });
                        }}
                    />
                    {/* <Text style={styles.title}>{(details.last_name)? details.last_name : "---"}</Text> */}

                    <Text style={styles.heading}>Locality</Text>
                    <TouchableOpacity onPress={() => (this.state.office_locality_array && this.state.office_locality_array.length > 0) ? this._openPicker("office_locality") : this._getLocality("office_locality")}>
                        <Text style={styles.title}>{(this.state.office_locality) ? this.state.office_locality : "Select Locality"}</Text>
                    </TouchableOpacity>

                    <Text style={styles.heading}>City</Text>
                    <Text style={styles.title}>{(this.state.office_city) ? this.state.office_city : "---"}</Text>


                    <Text style={styles.heading}>State</Text>
                    <Text style={styles.title}>{(this.state.office_state) ? this.state.office_state : "---"}</Text>


                    <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080' }} onPress={() => this.onCancel()}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: "#007fff" }} onPress={() => this.props.onSave(editId, data)}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Save</Text>
                        </TouchableOpacity>
                    </View>

                    {this.state.pincodeFetching && <View style={[styles.loading, { position: 'absolute' }]}>
                        <ActivityIndicator size="small" color="#007fff" />
                    </View>}
                </View>);

            case "add_number":
                var { mobile_number } = this.state;
                return (<View style={{ backgroundColor: "#fff", padding: 20 }}>
                    <Text style={styles.heading}>Add mobile number</Text>

                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        placeholder='Enter number'
                        keyboardType='numeric'
                        maxLength={10}
                        value={mobile_number}
                        onChangeText={(text) => {
                            this.setState({ mobile_number: text });
                        }}
                    />

                    <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080' }} onPress={() => this.onCancel()}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: "#007fff" }} onPress={() => (this.state.mobile_number && this.state.mobile_number.length == 10) ? this.onSave(editId) : null}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Save</Text>
                        </TouchableOpacity>
                    </View>
                </View>);

            case "add_email":
                var { email } = this.state;
                return (<View style={{ backgroundColor: "#fff", padding: 20 }}>
                    <Text style={styles.heading}>Add email address</Text>

                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        placeholder='Enter email'
                        value={email}
                        onChangeText={(text) => {
                            this.setState({ email: text });

                        }}
                    />

                    <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080' }} onPress={() => this.onCancel()}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: "#007fff" }} onPress={() => this.onSave(editId)}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Save</Text>
                        </TouchableOpacity>
                    </View>
                </View>);

            case "edit_email":
                var { editContactInfo } = this.state;
                //console.log("editContactInfo", JSON.stringify(editContactInfo))
                return (<View style={{ backgroundColor: "#fff", padding: 20 }}>
                    <Text style={styles.heading}>Edit email address</Text>

                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        placeholder='Enter email'
                        value={editContactInfo.value}
                        onChangeText={(text) => {
                            editContactInfo.value = text
                            this.setState({ editContactInfo });

                        }}
                    />

                    <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080' }} onPress={() => this.onCancel()}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: "#007fff" }} onPress={() => this.props.onSave(editId, this.state.editContactInfo)}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Save</Text>
                        </TouchableOpacity>
                    </View>
                </View>);

            case "edit_number":
                var { editContactInfo } = this.state;
                //console.log("editContactInfo", JSON.stringify(editContactInfo))
                return (<View style={{ backgroundColor: "#fff", padding: 20 }}>
                    <Text style={styles.heading}>Edit mobile number</Text>

                    <TextInput
                        style={styles.textInputStyle}
                        //editable={vehicleModel ? true : false}
                        placeholder='Enter number'
                        keyboardType='numeric'
                        maxLength={10}
                        value={editContactInfo.value}
                        onChangeText={(text) => {
                            editContactInfo.value = text
                            this.setState({ editContactInfo });
                        }}
                    />

                    <View style={{ flexDirection: 'row', margin: 10, justifyContent: "center" }}>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: '#808080' }} onPress={() => this.onCancel()}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ margin: 10, alignSelf: 'center', backgroundColor: "#007fff" }} onPress={() => this.props.onSave(editId, this.state.editContactInfo)}>
                            <Text style={{ textAlign: 'center', color: "#fff", padding: 10 }}>Save</Text>
                        </TouchableOpacity>
                    </View>
                </View>);
        }
    }

    getPincodeDetails(pincode, pincode_for) {

        this.setState({ pincodeFetching: true });
        if (pincode && pincode != undefined) {
            new RestClient().getPincodeDetailsInternal(pincode).then((data) => {
                //console.log("data.resulty", JSON.stringify(data))
                if (data && data.result && data.result.pindata && data.result.pindata.length > 0) {
                    let pinData = data.result.pindata;
                    if (pincode_for == "residence_pincode") {
                        var { locality } = this.state;
                        var { city } = this.state;
                        var { residence_state } = this.state;
                        var { residence_locality_array } = this.state;

                        residence_state = pinData[0].statename;
                        //locality = pinData[0].locality;
                        city = pinData[0].districtname;
                        residence_locality_array = pinData;

                        this.setState({ pincodeFetching: false, residence_state, residence_locality_array, city })
                    } else {
                        var { office_locality } = this.state;
                        var { office_city } = this.state;
                        var { office_state } = this.state;
                        var { office_locality_array } = this.state;

                        office_state = pinData[0].statename;
                        //office_locality = pinData[0].locality;
                        office_city = pinData[0].districtname;
                        office_locality_array = pinData;

                        this.setState({ pincodeFetching: false, office_state, office_city, office_locality_array })
                    }
                }
                else {

                    this.setState({ pincodeFetching: false })
                }

            }).catch(error => {

                this.setState({ pincodeFetching: false });
            });

        }
    }

    selectedData = (value) => {
        var { type_of_picker } = this.state;
        console.log("hohu", type_of_picker, value)
        switch (type_of_picker) {
            case "type_of_customer":
                this.setState({ isPickerVisible: false, type_of_customer: value })
                return;
            case "mode_of_payment":
                this.setState({ isPickerVisible: false, mode_of_payment: value })
                return;
            case "title":
                this.setState({ isPickerVisible: false, title: value })
                return;
            case "close_date":
                this.setState({
                    close_date: Utils.getReadableDate(value),
                    dateTimePickerVisibility: false,
                    datePickerMode: 'date',
                    dateTimePickerMinimumDate: undefined,
                    dateTimePickerMaximumDate: undefined
                });
                return;
            case "residence_locality":
                this.setState({ isPickerVisible: false, residence_locality: value })
                return;

            case "office_locality":
                this.setState({ isPickerVisible: false, office_locality: value })
                return;

        }
    }

    onSelectPopUpItemCancel = () => {
        this.setState({ isPickerVisible: false })
    }

    render() {
        var details = this.props.leadDetails
        var editId = this.props.edit_id;

        return (
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" enabled={Platform.OS === 'ios'}>
                <View style={{ flex: 1, justifyContent: 'center' }}>

                    {/* <Modal
                    isVisible={this.props.visible}>
                    {this.generateView(editId, details)}
                </Modal>  */}

                    <Modal
                        visible={this.props.visible}
                        animationType={'slide'}
                        transparent={true}
                        onRequestClose={() => this.onCancel()}
                    >
                        <SelectPopUpItem
                            isPickerVisible={this.state.isPickerVisible}
                            pickerId={this.state.type_of_picker}
                            selectedData={this.selectedData}
                            residence_locality_array={this.state.residence_locality_array}
                            office_locality_array={this.state.office_locality_array}
                            onSelectPopUpItemCancel={this.onSelectPopUpItemCancel}
                        />

                        <CustomDateTimePicker
                            minimumDate={this.state.dateTimePickerMinimumDate}
                            maximumDate={this.state.dateTimePickerMaximumDate}
                            dateTimePickerVisibility={this.state.dateTimePickerVisibility}
                            datePickerMode={this.state.datePickerMode}
                            payload={this.state.dateTimePickerPayload}
                            handleDateTimePicked={(date, payload) => {

                                this.selectedData(date);
                            }}
                            hideDateTimePicked={() => {
                                this.setState({
                                    dateTimePickerVisibility: false,
                                    datePickerMode: 'date',
                                    dateTimePickerMinimumDate: undefined,
                                    dateTimePickerMaximumDate: undefined
                                })
                            }}
                        />

                        <View style={styles.overlay}>
                            {this.generateView(editId, details)}
                        </View>
                    </Modal>

                </View>
            </KeyboardAvoidingView>
        )
    }
}