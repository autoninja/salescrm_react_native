package com.salescrm.telephony.dbOperation;
import com.salescrm.telephony.db.CallStatDB;

import io.realm.Realm;
import io.realm.RealmResults;

public class CallStatDbOperation {
    public static void createOnStartCall(long createdAt,
                                         String mobileNumber,
                                         String leadId,
                                         String scheduledActivityId) {
        if(true) {
            return;
        }
        CallStatDB callStatDb = new CallStatDB();
        callStatDb.setCreatedAt(createdAt);
        callStatDb.setMobileNumber(mobileNumber);
        callStatDb.setLeadId(leadId);
        callStatDb.setScheduledActivityId(scheduledActivityId);
        callStatDb.setStartTime(createdAt);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(callStatDb);
        realm.commitTransaction();
    }
    public static void updateOnEndCall(long createdAt) {
        if(true) {
            return;
        }
        Realm realm = Realm.getDefaultInstance();
        CallStatDB callStatDB = realm.where(CallStatDB.class)
                    .equalTo("createdAt", createdAt)
                .findFirst();
        if(callStatDB!=null) {
            realm.beginTransaction();
            callStatDB.setEndTime(System.currentTimeMillis());
            callStatDB.setSuccess(true);
            realm.copyToRealmOrUpdate(callStatDB);
            realm.commitTransaction();
        }
    }
    public static CallStatDB get(long createdAt){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(CallStatDB.class)
                    .equalTo("createdAt", createdAt)
                .findFirst();
    }
    public static void updateOnApiCall() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<CallStatDB> callStats= realm.where(CallStatDB.class)
                    .findAll();
        for (CallStatDB callStatDB : callStats) {
            realm.beginTransaction();
            callStatDB.setSyncStatus(true);
            realm.copyToRealmOrUpdate(callStatDB);
            realm.commitTransaction();
        }
    }
}
