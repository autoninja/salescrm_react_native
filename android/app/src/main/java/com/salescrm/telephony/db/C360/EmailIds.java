package com.salescrm.telephony.db.C360;

import io.realm.RealmObject;

/**
 * Created by bannhi on 9/5/17.
 */

public class EmailIds extends RealmObject {

    String emailAddressId;
    String email;
    long leadId;
    boolean deletedOffline;
    boolean createdOffline;
    boolean editedOffline;
    String emailIdStatus;
    int customerId;
    String emailLeadMappingId;


    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailAddressId() {
        return emailAddressId;
    }

    public void setEmailAddressId(String emailAddressId) {
        this.emailAddressId = emailAddressId;
    }


    public boolean isDeletedOffline() {
        return deletedOffline;
    }

    public void setDeletedOffline(boolean deletedOffline) {
        this.deletedOffline = deletedOffline;
    }

    public boolean isCreatedOffline() {
        return createdOffline;
    }

    public void setCreatedOffline(boolean createdOffline) {
        this.createdOffline = createdOffline;
    }


    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public String getEmailIdStatus() {
        return emailIdStatus;
    }

    public void setEmailIdStatus(String emailIdStatus) {
        this.emailIdStatus = emailIdStatus;
    }

    public String getEmailLeadMappingId() {
        return emailLeadMappingId;
    }

    public void setEmailLeadMappingId(String emailLeadMappingId) {
        this.emailLeadMappingId = emailLeadMappingId;
    }

    public boolean isEditedOffline() {
        return editedOffline;
    }

    public void setEditedOffline(boolean editedOffline) {
        this.editedOffline = editedOffline;
    }
}

