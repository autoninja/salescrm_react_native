package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 14/12/16.
 */

public class ScoreboardDB extends RealmObject {

    @PrimaryKey
    private String user_id;

    private int leads_hot;

    private int leadsnumber;

    private int leads_cold;

    private int today_finance;

    private int booked;

    private int leads_warm;

    private int today_meeting;

    private int testdrivetotal;

    private int today_test_drive;

    private int today_callback;

    private int exchange;

    private int retail;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getLeads_hot ()
    {
        return leads_hot;
    }

    public void setLeads_hot (int leads_hot)
    {
        this.leads_hot = leads_hot;
    }

    public int getLeadsnumber ()
    {
        return leadsnumber;
    }

    public void setLeadsnumber (int leadsnumber)
    {
        this.leadsnumber = leadsnumber;
    }

    public int getLeads_cold ()
    {
        return leads_cold;
    }

    public void setLeads_cold (int leads_cold)
    {
        this.leads_cold = leads_cold;
    }

    public int getToday_finance ()
    {
        return today_finance;
    }

    public void setToday_finance (int today_finance)
    {
        this.today_finance = today_finance;
    }

    public int getBooked ()
    {
        return booked;
    }

    public void setBooked (int booked)
    {
        this.booked = booked;
    }

    public int getLeads_warm ()
    {
        return leads_warm;
    }

    public void setLeads_warm (int leads_warm)
    {
        this.leads_warm = leads_warm;
    }

    public int getToday_meeting ()
    {
        return today_meeting;
    }

    public void setToday_meeting (int today_meeting)
    {
        this.today_meeting = today_meeting;
    }

    public int getTestdrivetotal ()
    {
        return testdrivetotal;
    }

    public void setTestdrivetotal (int testdrivetotal)
    {
        this.testdrivetotal = testdrivetotal;
    }

    public int getToday_test_drive ()
    {
        return today_test_drive;
    }

    public void setToday_test_drive (int today_test_drive)
    {
        this.today_test_drive = today_test_drive;
    }

    public int getToday_callback ()
    {
        return today_callback;
    }

    public void setToday_callback (int today_callback)
    {
        this.today_callback = today_callback;
    }

    public int getExchange ()
    {
        return exchange;
    }

    public void setExchange (int exchange)
    {
        this.exchange = exchange;
    }

    public int getRetail ()
    {
        return retail;
    }

    public void setRetail (int retail)
    {
        this.retail = retail;
    }

}
