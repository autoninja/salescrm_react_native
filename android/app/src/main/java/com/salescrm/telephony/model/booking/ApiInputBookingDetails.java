package com.salescrm.telephony.model.booking;

/**
 * Created by bharath on 19/3/18.
 */

public class ApiInputBookingDetails {
    private ApiInputCarDetails car_details;
    private ApiInputCustomerDetails customer_details;
    private ApiInputPriceBreakup price_breakup;
    private ApiInputDiscount discounts;
    private ApiInputExchFinDetails payment_details;
    private String vin_allocation_status_id;
    private String vin_no;
    private String vin_allocation_status;
    private ApiInputVinAllocationDetails vin_allocation_details;

    public ApiInputExchFinDetails getPayment_details() {
        return payment_details;
    }

    public void setPayment_details(ApiInputExchFinDetails payment_details) {
        this.payment_details = payment_details;
    }

    public ApiInputCarDetails getCar_details() {
        return car_details;
    }

    public void setCar_details(ApiInputCarDetails car_details) {
        this.car_details = car_details;
    }

    public ApiInputCustomerDetails getCustomer_details() {
        return customer_details;
    }

    public void setCustomer_details(ApiInputCustomerDetails customer_details) {
        this.customer_details = customer_details;
    }

    public ApiInputPriceBreakup getPrice_breakup() {
        return price_breakup;
    }

    public void setPrice_breakup(ApiInputPriceBreakup price_breakup) {
        this.price_breakup = price_breakup;
    }

    public ApiInputDiscount getDiscounts() {
        return discounts;
    }

    public void setDiscounts(ApiInputDiscount discounts) {
        this.discounts = discounts;
    }

    public String getVin_allocation_status_id() {
        return vin_allocation_status_id;
    }

    public void setVin_allocation_status_id(String vin_allocation_status_id) {
        this.vin_allocation_status_id = vin_allocation_status_id;
    }

    public void setVin_no(String vin_no) {
        this.vin_no = vin_no;
    }

    public String getVin_no() {
        return vin_no;
    }

    public ApiInputVinAllocationDetails getVin_allocation_details() {
        return vin_allocation_details;
    }

    public void setVin_allocation_details(ApiInputVinAllocationDetails vin_allocation_details) {
        this.vin_allocation_details = vin_allocation_details;
    }

    public String getVin_allocation_status() {
        return vin_allocation_status;
    }

    public void setVin_allocation_status(String vin_allocation_status) {
        this.vin_allocation_status = vin_allocation_status;
    }
}

