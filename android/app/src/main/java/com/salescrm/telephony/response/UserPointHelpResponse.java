package com.salescrm.telephony.response;


import java.util.List;

public class UserPointHelpResponse {
    private String statusCode;

    private String message;

    private List<Result> result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public class Result {
        private Integer id;
        private String name;
        private Integer base_point;
        private Integer target;
        private Integer points_per_activity;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getBase_point() {
            return base_point;
        }

        public void setBase_point(Integer base_point) {
            this.base_point = base_point;
        }

        public Integer getTarget() {
            return target;
        }

        public void setTarget(Integer target) {
            this.target = target;
        }

        public Integer getPoints_per_activity() {
            return points_per_activity;
        }

        public void setPoints_per_activity(Integer points_per_activity) {
            this.points_per_activity = points_per_activity;
        }
    }
}
