package com.salescrm.telephony.interfaces;

/**
 * Created by bharath on 18/10/16.
 */

public interface FilterDataChangeListener {
    void onFilterDataChanged();
}
