package com.salescrm.telephony.db;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 25-08-2016.
 */
public class IntrestedCarActivityGroup extends RealmObject {

    public static final String INTRESTEDCARACTIVITYGROUP = "IntrestedCarActivityGroup";


    private long leadId;
    private String intrestedCarDetailActivityGroupName;

    public RealmList<IntrestedCarActivityGroupStages> IntrestedCarActivityGroupStages =  new RealmList<>();

    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public String getIntrestedCarDetailActivityGroupName() {
        return intrestedCarDetailActivityGroupName;
    }

    public void setIntrestedCarDetailActivityGroupName(String intrestedCarDetailActivityGroupName) {
        this.intrestedCarDetailActivityGroupName = intrestedCarDetailActivityGroupName;
    }

}
