import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native'
export default class RadioButton extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let isSelected = this.props.selected;
    let disabled = this.props.disabled;
    let lightMode = this.props.lightMode;
    let textColor;
    if (disabled === true) {
      if (lightMode) {
        textColor = "#cacccf"
      }
      else {
        textColor = "#808080"
      }
    }
    else {
      if (lightMode) {
        textColor = "#EEEEEE";
      }
      else {
        textColor = "#303030";
      }
    }
    return (
      <TouchableOpacity
        disabled={disabled}
        onPress={() => {
          this.props.onClick();
        }}
      >
        <View style={[{ flexDirection: 'row', alignItems: 'center' }, this.props.style]}>
          <View style={[{
            height: 16,
            width: 16,
            borderRadius: 12,
            borderWidth: 2,
            borderColor: isSelected ? lightMode ? '#ffffff' : '#007fff' : '#cfcfcf',
            alignItems: 'center',
            justifyContent: 'center',
          }]}>
            {
              isSelected ?
                <View style={{
                  height: 12,
                  width: 12,
                  borderRadius: 6,
                  borderColor: lightMode ? 'rgba(0,0,0,0)' : '#fff',
                  borderWidth: 2,
                  backgroundColor: lightMode ? '#ffffff' : '#007fff',
                }} />
                : null
            }
          </View>
          <Text style={{ color: textColor, fontSize: 16, marginLeft: 10, textAlignVertical: 'center' }}>{this.props.title}</Text>
        </View>
      </TouchableOpacity>

    );
  }
}
