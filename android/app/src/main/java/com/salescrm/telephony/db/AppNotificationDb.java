package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 21/6/17.
 */

public class AppNotificationDb extends RealmObject {
    @PrimaryKey
    int scheduledActivityId;
    boolean isNotificationShowed = false;
    long appNotificationTime;
    long scheduledActivityTime;
    SalesCRMRealmTable salesCRMRealmTable;

    public AppNotificationDb() {

    }
    public AppNotificationDb(int scheduledActivityId,
                             boolean isShowedNotification,
                             long appNotificationTime,
                             long scheduledActivityTime,
                             SalesCRMRealmTable salesCRMRealmTable) {
        this.scheduledActivityId = scheduledActivityId;
        this.isNotificationShowed = isShowedNotification;
        this.appNotificationTime = appNotificationTime;
        this.scheduledActivityTime = scheduledActivityTime;
        this.salesCRMRealmTable = salesCRMRealmTable;
    }

    public int getScheduledActivityId() {
        return scheduledActivityId;
    }

    public void setScheduledActivityId(int scheduledActivityId) {
        this.scheduledActivityId = scheduledActivityId;
    }

    public boolean isNotificationShowed() {
        return isNotificationShowed;
    }

    public void setNotificationShowed(boolean showedNotification) {
        isNotificationShowed = showedNotification;
    }

    public long getAppNotificationTime() {
        return appNotificationTime;
    }

    public void setAppNotificationTime(long appNotificationTime) {
        this.appNotificationTime = appNotificationTime;
    }

    public long getScheduledActivityTime() {
        return scheduledActivityTime;
    }

    public void setScheduledActivityTime(long scheduledActivityTime) {
        this.scheduledActivityTime = scheduledActivityTime;
    }

    public SalesCRMRealmTable getSalesCRMRealmTable() {
        return salesCRMRealmTable;
    }

    public void setSalesCRMRealmTable(SalesCRMRealmTable salesCRMRealmTable) {
        this.salesCRMRealmTable = salesCRMRealmTable;
    }
}
