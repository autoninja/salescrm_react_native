package com.salescrm.telephony.model.gamification;

import java.util.List;

/**
 * Created by prateek on 26/6/18.
 */

public class GameData {

    private List<BadgeDetails> badge_details;

    public List<BadgeDetails> getBadge_details() {
        return badge_details;
    }

    public void setBadge_details(List<BadgeDetails> badge_details) {
        this.badge_details = badge_details;
    }

    public class BadgeDetails {

        private String id;

        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
