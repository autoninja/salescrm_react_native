import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image } from 'react-native';
import RadioView from './radio_view'
import VoidView from './void_view'
import DropdownView from './dropdown_view'
import TextAreaView from './textarea_view'
import CheckboxView from './checkbox_view'
import DateTimeView from './date_time_view'
import DateView from './date_view'

export default class GenerateViews extends React.PureComponent {

	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			isFetching: false,
			responseArray: [],
		}
	}

	pushtoResponseArray = (item, update) => {

		this.props.formResponse(item, update)
		//console.log("Item Received After" ,JSON.stringify(item))
	}

	mandatoryFields = (item, update) => {
		this.props.mandatoryFields(item, update)
	}

	generateViews = (questionChildrenViews, viewType) => {
		//console.log("questionChildrenViews.formInputType", JSON.stringify(questionChildrenViews))
		if (questionChildrenViews.hidden == 1) {
			return;
		}
		if (questionChildrenViews.ifVisibleMandatory && viewType != "void") {
			this.mandatoryFields({ id: questionChildrenViews.fQId, isSelected: false })
		}
		switch (viewType) {
			case 'void':
				return (<VoidView key={questionChildrenViews.fQId} radioObject={questionChildrenViews} pushToarray={this.pushtoResponseArray} callMainGenerateViews={this.generateViews} />)
			case 'radio':
				return (<RadioView key={questionChildrenViews.fQId} radioObject={questionChildrenViews} pushToarray={this.pushtoResponseArray} callMainGenerateViews={this.generateViews} mandatoryField={this.mandatoryFields} />)
			case 'dropdown':
				return (<DropdownView fAnswerId={this.props.fAnswerId} key={questionChildrenViews.fQId} radioObject={questionChildrenViews} pushToarray={this.pushtoResponseArray} callMainGenerateViews={this.generateViews} mandatoryField={this.mandatoryFields} />)
			case 'textarea':
				return (<TextAreaView key={questionChildrenViews.fQId} radioObject={questionChildrenViews} pushToarray={this.pushtoResponseArray} callMainGenerateViews={this.generateViews} mandatoryField={this.mandatoryFields} />)
			case 'checkbox':
				return (<CheckboxView key={questionChildrenViews.fQId} radioObject={questionChildrenViews} pushToarray={this.pushtoResponseArray} callMainGenerateViews={this.generateViews} mandatoryField={this.mandatoryFields} />)
			case 'date_time':
				return (<DateTimeView key={questionChildrenViews.fQId} radioObject={questionChildrenViews} pushToarray={this.pushtoResponseArray} callMainGenerateViews={this.generateViews} mandatoryField={this.mandatoryFields} />)
			case 'date':
				return (<DateView key={questionChildrenViews.fQId} radioObject={questionChildrenViews} pushToarray={this.pushtoResponseArray} callMainGenerateViews={this.generateViews} mandatoryField={this.mandatoryFields} />)
			case 'default':
				return;

		}

	}
	render() {
		//console.log("innerObjects", "reached")
		//console.log("innerObjects", this.props.innerObjects.fQId)
		//console.log("pre_form_object", JSON.stringify(this.props.innerObjects))
		let innerObjects = this.props.innerObjects
		return (
			<View style={{
				flex: 1, backgroundColor: '#fff', paddingStart: 10,
				paddingEnd: 10, paddingTop: 4, paddingBottom: 4, borderRadius: 4
			}}>
				{this.generateViews(innerObjects, innerObjects.formInputType)}
			</View>
		)
	}
}