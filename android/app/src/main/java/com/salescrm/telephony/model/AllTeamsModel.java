package com.salescrm.telephony.model;

/**
 * Created by bharath on 22/8/17.
 */

public class AllTeamsModel {
    private String statusCode;

    private String message;

    private Result[] result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result[] getResult ()
    {
        return result;
    }

    public void setResult (Result[] result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private String id;

        private Users[] users;

        private String manager;

        private String team_leader;

        private String team_leader_id;

        private String location;

        private String manager_id;

        private String name;

        private String teamuserCount;

        private String location_id;

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public Users[] getUsers ()
        {
            return users;
        }

        public void setUsers (Users[] users)
        {
            this.users = users;
        }

        public String getManager ()
        {
            return manager;
        }

        public void setManager (String manager)
        {
            this.manager = manager;
        }

        public String getTeam_leader ()
        {
            return team_leader;
        }

        public void setTeam_leader (String team_leader)
        {
            this.team_leader = team_leader;
        }

        public String getTeam_leader_id ()
        {
            return team_leader_id;
        }

        public void setTeam_leader_id (String team_leader_id)
        {
            this.team_leader_id = team_leader_id;
        }

        public String getLocation ()
        {
            return location;
        }

        public void setLocation (String location)
        {
            this.location = location;
        }

        public String getManager_id ()
        {
            return manager_id;
        }

        public void setManager_id (String manager_id)
        {
            this.manager_id = manager_id;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getTeamuserCount ()
        {
            return teamuserCount;
        }

        public void setTeamuserCount (String teamuserCount)
        {
            this.teamuserCount = teamuserCount;
        }

        public String getLocation_id ()
        {
            return location_id;
        }

        public void setLocation_id (String location_id)
        {
            this.location_id = location_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [id = "+id+", users = "+users+", manager = "+manager+", team_leader = "+team_leader+", team_leader_id = "+team_leader_id+", location = "+location+", manager_id = "+manager_id+", name = "+name+", teamuserCount = "+teamuserCount+", location_id = "+location_id+"]";
        }

        public class Users
        {
            private String role_id;

            private String image_url;

            private String role;

            private String userName;

            private String user_id;

            public String getRole_id ()
            {
                return role_id;
            }

            public void setRole_id (String role_id)
            {
                this.role_id = role_id;
            }

            public String getImage_url ()
            {
                return image_url;
            }

            public void setImage_url (String image_url)
            {
                this.image_url = image_url;
            }

            public String getRole ()
            {
                return role;
            }

            public void setRole (String role)
            {
                this.role = role;
            }

            public String getUserName ()
            {
                return userName;
            }

            public void setUserName (String userName)
            {
                this.userName = userName;
            }

            public String getUser_id ()
            {
                return user_id;
            }

            public void setUser_id (String user_id)
            {
                this.user_id = user_id;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [role_id = "+role_id+", image_url = "+image_url+", role = "+role+", userName = "+userName+", user_id = "+user_id+"]";
            }
        }
    }
}
