class EvaluationManagerConfModel {
  constructor(locationId, boolEvaluationManager) {
    this.locationId = locationId;
    this.hasEvaluationManager = (String(boolEvaluationManager) == "true");
  }
}

module.exports = { EvaluationManagerConfModel };
