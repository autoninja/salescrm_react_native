package com.salescrm.telephony.services.service_handlers;

import android.app.Activity;

import com.salescrm.telephony.services.ConnectionDetectorService;

import io.realm.Realm;

/**
 * Created by bannhi on 21/8/17.
 */

public class ETVBRServiceHandler {

    ConnectionDetectorService connectionDetectorService;
    static Activity activity;
    Realm realm;
    static String errorMessage = "Please try again.";

    static ETVBRServiceHandler newInstance = new ETVBRServiceHandler();

    public static ETVBRServiceHandler getInstance(Activity ctx) {
        activity = ctx;
        // realm = rlm;
        if (newInstance == null)
            newInstance = new ETVBRServiceHandler();
        return newInstance;
    }

    private ETVBRServiceHandler() {

    }


}
