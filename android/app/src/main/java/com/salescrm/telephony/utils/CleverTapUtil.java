package com.salescrm.telephony.utils;

/**
 * Created by bannhi on 16/1/17.
 */

public class CleverTapUtil {

    String key;
    String value;

    public CleverTapUtil(String key, String value){
        super();
        this.key = key;
        this.value = value;
    }
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
