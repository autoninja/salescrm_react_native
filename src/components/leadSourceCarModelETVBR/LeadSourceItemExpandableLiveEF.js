import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Alert } from 'react-native';
import { FAB } from 'react-native-paper';
export default class LeadSourceItemExpandableLiveEF extends Component{
    constructor(props) {
        super(props);
        this.state = {expand:false, arrray:[1,2,3]};
    }
    expand() {
        this.setState({expand : !this.state.expand});
    }
    onLongPress() {
        // if(this.props.showPercentage) {
        //     Alert.alert('Long Disabled');
        // }
        // else {
        //     Alert.alert('Long Enabled');
        // }
    }
    render() {
        let data = this.props.data;
        let r,ex,f_i,f_o,pb,le;
        if(this.props.showPercentage) {
            r = data.rel.r;
            ex = data.rel.ex;
            f_i = data.rel.f_i;
            f_o = data.rel.f_o;
            pb = '-';
            le = '-';
        }
        else {
            r = data.abs.r;
            ex = data.abs.ex;
            f_i = data.abs.f_i;
            f_o = data.abs.f_o;
            pb = data.abs.pb;
            le = data.abs.le;
        }
         
        return(
            <View style={{paddingTop:6, margin:2}}>
                <Text style={styles.itemHeader}>{data.info.sourceName}</Text>
                <TouchableWithoutFeedback onPress={()=> {
                    this.expand();
                }}>
                <View style={styles.container}>
                    <View style={styles.itemEmpty}/>

                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{r}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{ex}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{f_i}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{f_o}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={[styles.itemText, {color:'#00bfa5'}]}>{pb}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onPress={()=> {
                                this.expand();
                            }}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={[styles.itemText, {color:'#00bfa5'}]}>{le}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                </View>        
                </TouchableWithoutFeedback>



                {this.state.expand && data.sources.map((item, index)=> {
                     let rInner,exInner,fIInner,fOInner,pBInner,lEInner;
                     if(this.props.showPercentage) {
                         rInner = item.rel.retails;
                         exInner = item.rel.exchanged;
                         fIInner = item.rel.in_house_financed;
                         fOInner = item.rel.out_house_financed;
                         pBInner = '-';
                         lEInner = '-';
                     }
                     else {
                         rInner = item.abs.retails;
                         exInner = item.abs.exchanged;
                         fIInner = item.abs.in_house_financed;
                         fOInner = item.abs.out_house_financed;
                         pBInner = item.abs.pending_bookings;
                         lEInner = item.abs.live_enquiries;
                     }
                    return(
                    <View style={styles.containerWhite} key={index+''}>
                    <View style={styles.itemEmpty}>
                        <Text 
                        style={styles.itemTextInner}
                        numberOfLines={2} 
                        ellipsizeMode='tail'
                        >{item.name}</Text>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{rInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{exInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{fIInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{fOInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={[styles.itemText, {color:'#00bfa5'}]}>{pBInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={[styles.itemText, {color:'#00bfa5'}]}>{lEInner}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>

                 
                </View>
                    );
                })}
                


            </View>
        )
    }
}

let styles = StyleSheet.create({
    container: {
        backgroundColor:'#eceff1',
        borderRadius:4,
        paddingTop:8,
        paddingBottom:8,
        paddingLeft:2,
        paddingRight:2,
        flexDirection:'row',
        elevation:2,
    },
    containerWhite: {
        backgroundColor:'#fff',
        height:48,
        paddingTop:8,
        paddingBottom:8,
        paddingLeft:2,
        paddingRight:2,
        flexDirection:'row',
        borderBottomColor:'#E7EAEF',
        borderBottomWidth:1,
    },
    itemEmpty: {
        flex:1.5,
        alignSelf:'center',
    },
    item: {
        flex:1,
        alignSelf:'center',
    },
    itemText: {
        fontSize:14,
        color:'#4B4B4B',
        alignSelf:'center',
        textAlign:'center'
    },
    itemTextInner: {
        fontSize:12,
        color:'#2D4F8B',
        alignSelf:'flex-start',
        textAlign:'left'
    },
    itemHeader: {
        fontSize:15,
        fontWeight:'bold',
        color:'#2D4F8B',
        alignSelf:'flex-start',
        paddingLeft:2,
       
    }
})