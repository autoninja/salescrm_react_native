import React from "react";
import { StyleSheet } from "react-native";
export default styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 4,
    paddingBottom: 4
  },
  main: {
    paddingLeft: 2,
    paddingRight: 2
  },
  header: {
    height: 48,
    flexDirection: "row",
    textAlign: "center"
  },
  headerText: {
    marginLeft: 20,
    color: "white",
    fontSize: 20,
    fontWeight: "bold"
  },
  content: {
    paddingStart: 20,
    margin: 6,
    backgroundColor: "#fff",
    borderRadius: 4,
    padding: 16
  },
  item: {
    backgroundColor: "#fff",
    padding: 10,
    marginTop: 10
  }
});
