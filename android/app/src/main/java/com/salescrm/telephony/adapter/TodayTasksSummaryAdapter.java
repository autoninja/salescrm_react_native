package com.salescrm.telephony.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.DetailsOfEtvbr;
import com.salescrm.telephony.activity.EtvbrImgDialog;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.model.DseModelTask;
import com.salescrm.telephony.model.TodaySummaryResponse;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Ravindra on 09-06-2017.
 */

public class TodayTasksSummaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final TodaySummaryResponse.Result summary;
    private Context context;
    private Preferences pref = null;
    private TodayTasksSummaryClickListener listener;

    private int VIEW_ITEM = 1;
    private int VIEW_TOTAL_FOOTER = 2;
    private boolean isUserTlOnly = false;
    private int tlColor, dseColor;
    private DseInflater dseInflater;
    private DataSetter dataSetter;
    private boolean isBike;

    public TodayTasksSummaryAdapter(Context context,
                                    TodaySummaryResponse.Result summary,
                                    boolean isUserTlOnly,
                                    TodayTasksSummaryClickListener listener
    ) {
        System.out.println("Is user tl onely:"+isUserTlOnly);
        this.context = context;
        this.pref = Preferences.getInstance();
        this.pref.load(context);
        this.listener = listener;
        this.summary = summary;
        this.isUserTlOnly = isUserTlOnly;
        tlColor = Color.parseColor("#E6E8F1");
        dseColor = Color.parseColor("#FFFFFF");
        dseInflater = new DseInflater();
        dataSetter = new DataSetter();
        isBike = DbUtils.isBike();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = null;
        if (viewType == VIEW_ITEM) {
            itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.todays_task_card_view, parent, false);
            return new TodayTasksSummaryAdapter.TodayTaskSummeryViewHolder(itemLayoutView);
        } else {
            itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.todays_task_card_view_bottom, parent, false);
            return new TotalViewHolder(itemLayoutView);
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        //Populate in view
        if (holder.getItemViewType() == VIEW_ITEM) {

            final TodaySummaryResponse.Result.Team_leaders teamLeader = summary.getTeam_leaders().get(position);
            final TodaySummaryResponse.Info teamLeadInfo = teamLeader.getInfo();
            if (teamLeadInfo == null) {
                return;
            }
            TodaySummaryResponse.Abs teamLeadAbs = teamLeader.getInfo().getAbs();

            final TodayTaskSummeryViewHolder todayHolder = (TodayTaskSummeryViewHolder) holder;
            // todayHolder.todayTask = teamLeader;

            if (isUserTlOnly) {

                todayHolder.cardViewMain.setVisibility(View.GONE);

                for (int i = 0; i < teamLeader.getSales_consultants().size(); i++) {
                    dseInflater.inflate(context, todayHolder.llSubCardsHolder, teamLeader.getSales_consultants().get(i), true);
                }

            } else {

                todayHolder.cardViewMain.setVisibility(View.VISIBLE);

                dataSetter.setData(todayHolder, teamLeadAbs, teamLeadInfo, WSConstants.ROLES.TEAM_LEAD);

                todayHolder.cardViewMain.setCardBackgroundColor(tlColor);

                todayHolder.cardViewMain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        teamLeader.setExpand(!teamLeader.isExpand());
                        if (teamLeader.isExpand()) {
                            todayHolder.llSubCardsHolder.removeAllViews();
                            for (int i = 0; i < teamLeader.getSales_consultants().size(); i++) {
                                dseInflater.inflate(context, todayHolder.llSubCardsHolder, teamLeader.getSales_consultants().get(i), false);
                            }
                        } else {
                            todayHolder.llSubCardsHolder.removeAllViews();
                        }

                    }
                });


            }

        } else {

            TodaySummaryResponse.Abs totalAbs = summary.getLocation_abs();
            TotalViewHolder totalViewHolder = (TotalViewHolder) holder;
            setTextViewData(totalViewHolder.txtCallNumber, totalAbs.getCalls());
            setTextViewData(totalViewHolder.txtTestDriveNumber, totalAbs.getTdrives());
            setTextViewData(totalViewHolder.txtVisitNumber, totalAbs.getVisits());
            setTextViewData(totalViewHolder.txtPostBookingNumber, totalAbs.getPost_bookings());
            setTextViewData(totalViewHolder.txtDeliveryNumber, totalAbs.getDeliveries());
            setTextViewData(totalViewHolder.txtPendingNumber, totalAbs.getPending_total_count());
            setTextViewData(totalViewHolder.txtUncalledNumber, totalAbs.getUncalled_tasks());
        }


    }


    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) {
            return VIEW_TOTAL_FOOTER;
        } else {
            return VIEW_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return summary.getTeam_leaders().size() + 1;
    }

    private void setTextViewData(TextView tv, Integer integer) {
        if (integer != null) {
            tv.setText(String.format(Locale.getDefault(), "%d", integer));
        } else {
            tv.setText("--");
        }
    }

    private void openUnCalledTasks(TodaySummaryResponse.Info info, String roleId, String uncalledTasksCount) {
       // Toast.makeText(context, "Opening uncalled task", Toast.LENGTH_SHORT).show();
        Intent uncalledTasks = new Intent(context, DetailsOfEtvbr.class);
        uncalledTasks.putExtra("clicked_position", "Uncalled Tasks");
        uncalledTasks.putExtra("title_name", info.getName()+"'s Enquiries ");
        uncalledTasks.putExtra("etvbrtotal", uncalledTasksCount);
        uncalledTasks.putExtra("user_id", info.getId()+"");
        uncalledTasks.putExtra("user_role_id", roleId);
        uncalledTasks.putExtra("start_date", getTodayDate());
        uncalledTasks.putExtra("user_location_id", summary.getLocation_id()+"");
        pref.setLocationId(summary.getLocation_id());
        context.startActivity(uncalledTasks);
    }

    private void openUserInfo(TodaySummaryResponse.Info info){
        if(info == null) {
            Util.showToast(context, "User info not available",Toast.LENGTH_SHORT);
            return;
        }
        Intent intent = new Intent(context, EtvbrImgDialog.class);
        Bundle bundle = new Bundle();
        bundle.putString("imgUrlEtvbr", info.getDp_url());
        bundle.putString("dseNameEtvbr", info.getName());
        intent.putExtras(bundle);
        context.startActivity(intent);
    }
    private String getTodayDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    private void openTasks(TodaySummaryResponse.Info info) {
        if (listener != null) {
            if (info.getId() != null) {
                DseModelTask dseModelTask = new DseModelTask(info.getId(), 2,
                        info.getName(), info.getDp_url());
                listener.onTodayTaskItemClick(dseModelTask);
            } else {
                Util.showToast(context, "User information not available", Toast.LENGTH_SHORT);
            }
        }
    }

    public interface TodayTasksSummaryClickListener {
        void onTodayTaskItemClick(DseModelTask dseModelTask);
    }

    private class TodayTaskSummeryViewHolder extends RecyclerView.ViewHolder {
        public View view, dividerView;
        ImageView imgUserProfile;
        private LinearLayout llParent;
        private TextView tvProfilePic, txtCallNumber, txtTestDriveNumber, txtVisitNumber, txtPostBookingNumber, txtDeliveryNumber, txtPendingNumber,
                txtUncalledNumber;
        private LinearLayout llSubCardsHolder;
        private CardView cardViewMain;

        TodayTaskSummeryViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            view = itemLayoutView;

            imgUserProfile = (ImageView) itemLayoutView.findViewById(R.id.img_user_profile);
            tvProfilePic = (TextView) itemLayoutView.findViewById(R.id.tv_user_profile);
            txtCallNumber = (TextView) itemLayoutView.findViewById(R.id.txt_call_number);
            txtTestDriveNumber = (TextView) itemLayoutView.findViewById(R.id.txt_test_drive_number);
            txtVisitNumber = (TextView) itemLayoutView.findViewById(R.id.txt_visit_number);
            txtPostBookingNumber = (TextView) itemLayoutView.findViewById(R.id.txt_post_booking_number);
            txtDeliveryNumber = (TextView) itemLayoutView.findViewById(R.id.txt_delivery_number);
            txtUncalledNumber = (TextView) itemLayoutView.findViewById(R.id.txt_task_no_call_number);
            txtPendingNumber = (TextView) itemLayoutView.findViewById(R.id.txt_pending_number);

            llParent = (LinearLayout) itemLayoutView.findViewById(R.id.ll_today_summary_main);
            llSubCardsHolder = (LinearLayout) itemLayoutView.findViewById(R.id.ll_today_summary_sub);
            cardViewMain = (CardView) itemLayoutView.findViewById(R.id.card_today_summary_main);
            dividerView = (View) itemLayoutView.findViewById(R.id.divider_view);

            itemLayoutView.findViewById(R.id.view_div_delivery_number);
            dividerView.setVisibility(View.GONE);
            if(isBike) {
                txtDeliveryNumber.setVisibility(View.GONE);
                txtVisitNumber.setVisibility(View.GONE);
                txtPostBookingNumber.setVisibility(View.GONE);

                itemLayoutView.findViewById(R.id.view_div_delivery_number).setVisibility(View.GONE);
                itemLayoutView.findViewById(R.id.view_div_visit_number).setVisibility(View.GONE);
                itemLayoutView.findViewById(R.id.view_div_post_booking_number).setVisibility(View.GONE);

                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        20
                );
                txtCallNumber.setLayoutParams(param);
                txtPendingNumber.setLayoutParams(param);
                txtUncalledNumber.setLayoutParams(param);
                txtTestDriveNumber.setLayoutParams(param);
            }

        }


    }

    private class TotalViewHolder extends RecyclerView.ViewHolder {
        public View view;
        private TextView txtCallNumber, txtTestDriveNumber, txtVisitNumber, txtPostBookingNumber, txtDeliveryNumber, txtPendingNumber,
               txtUncalledNumber;
        private TextView tvPendingTitle, tvTasksTitle;
        private Drawable drawableTasks, drawablePendings;

        TotalViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            view = itemLayoutView;
            txtCallNumber = (TextView) itemLayoutView.findViewById(R.id.txt_call_number);
            txtTestDriveNumber = (TextView) itemLayoutView.findViewById(R.id.txt_test_drive_number);
            txtVisitNumber = (TextView) itemLayoutView.findViewById(R.id.txt_visit_number);
            txtPostBookingNumber = (TextView) itemLayoutView.findViewById(R.id.txt_post_booking_number);
            txtDeliveryNumber = (TextView) itemLayoutView.findViewById(R.id.txt_delivery_number);
            txtUncalledNumber = (TextView) itemLayoutView.findViewById(R.id.txt_task_no_call_number);
            txtPendingNumber = (TextView) itemLayoutView.findViewById(R.id.txt_pending_number);
            tvPendingTitle = (TextView) itemLayoutView.findViewById(R.id.tvPendingTitle);
            tvTasksTitle = (TextView) itemLayoutView.findViewById(R.id.tvTasksTitle);

            if(isBike) {
                txtDeliveryNumber.setVisibility(View.GONE);
                txtVisitNumber.setVisibility(View.GONE);
                txtPostBookingNumber.setVisibility(View.GONE);

                itemLayoutView.findViewById(R.id.view_div_delivery_number).setVisibility(View.GONE);
                itemLayoutView.findViewById(R.id.view_div_visit_number).setVisibility(View.GONE);
                itemLayoutView.findViewById(R.id.view_div_post_booking_number).setVisibility(View.GONE);

                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        0,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        20
                );
                txtCallNumber.setLayoutParams(param);
                txtPendingNumber.setLayoutParams(param);
                txtUncalledNumber.setLayoutParams(param);
                txtTestDriveNumber.setLayoutParams(param);
            }

            drawableTasks = VectorDrawableCompat.create(context.getResources(), R.drawable.ic_call, null);
            drawablePendings = VectorDrawableCompat.create(context.getResources(), R.drawable.ic_pending, null);
            tvTasksTitle.setCompoundDrawablesWithIntrinsicBounds(drawableTasks, null, null, null);
            tvPendingTitle.setCompoundDrawablesWithIntrinsicBounds(drawablePendings, null, null, null);

        }
    }

    class DseInflater {

        private DataSetter dataSetter = new DataSetter();

        public void inflate(Context context, ViewGroup parent,
                            final TodaySummaryResponse.Result.Team_leaders.Sales_consultants sales_consultants, boolean b) {

            TodayTaskSummeryViewHolder todayTaskSummeryViewHolder = new TodayTaskSummeryViewHolder(
                    LayoutInflater.from(context).inflate(R.layout.todays_task_card_view,
                            parent,
                            false));
            todayTaskSummeryViewHolder.llParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(sales_consultants.getInfo()!=null) {
                        openTasks(sales_consultants.getInfo());
                    }
                }
            });
            if(sales_consultants.getInfo()!=null) {
                dataSetter.setData(todayTaskSummeryViewHolder,
                        sales_consultants.getInfo().getAbs(),
                        sales_consultants.getInfo(),
                        WSConstants.ROLES.DSE);
            }
            todayTaskSummeryViewHolder.llSubCardsHolder.setVisibility(View.GONE);
            if(b == true) {
                todayTaskSummeryViewHolder.dividerView.setVisibility(View.VISIBLE);
            }else {
                todayTaskSummeryViewHolder.dividerView.setVisibility(View.GONE);
            }
            parent.addView(todayTaskSummeryViewHolder.view);

        }

    }

    class DataSetter {

        public void setData(final TodayTaskSummeryViewHolder todayHolder,
                            TodaySummaryResponse.Abs abs,
                            final TodaySummaryResponse.Info info,
                            final String roleId) {

            setTextViewData(todayHolder.txtCallNumber, abs.getCalls());
            setTextViewData(todayHolder.txtTestDriveNumber, abs.getTdrives());
            setTextViewData(todayHolder.txtVisitNumber, abs.getVisits());
            setTextViewData(todayHolder.txtPostBookingNumber, abs.getPost_bookings());
            setTextViewData(todayHolder.txtDeliveryNumber, abs.getDeliveries());
            setTextViewData(todayHolder.txtPendingNumber, abs.getPending_total_count());
            setTextViewData(todayHolder.txtUncalledNumber, abs.getUncalled_tasks());
            if (info.getName() != null && info.getName().length() > 0) {
                todayHolder.tvProfilePic.setText(String.format(Locale.getDefault(),
                        "%s", info.getName().charAt(0)));
            }

            String dpUrl = null;
            if(Util.isNotNull(info.getDp_url())) {
                dpUrl = info.getDp_url();
            }

            Glide.with(context).
                    load(dpUrl)
                    .asBitmap()
                    .centerCrop()
                    .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                            todayHolder.imgUserProfile.setVisibility(View.GONE);
                            todayHolder.tvProfilePic.setVisibility(View.VISIBLE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            todayHolder.imgUserProfile.setVisibility(View.VISIBLE);
                            todayHolder.tvProfilePic.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(new BitmapImageViewTarget(todayHolder.imgUserProfile) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            if(resource!=null && context!=null && context.getResources()!=null) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                todayHolder.imgUserProfile.setImageDrawable(circularBitmapDrawable);
                            }
                        }
                    });


            todayHolder.txtUncalledNumber.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    openUnCalledTasks(info,roleId,todayHolder.txtUncalledNumber.getText().toString());
                    return false;
                }
            });

            todayHolder.imgUserProfile.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    openUserInfo(info);
                    return false;
                }
            });
            todayHolder.tvProfilePic.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    openUserInfo(info);
                    return false;
                }
            });
        }
    }

}
