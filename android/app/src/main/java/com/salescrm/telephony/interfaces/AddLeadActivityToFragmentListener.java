package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.model.AddLeadActivityInputData;
import com.salescrm.telephony.model.AddLeadCarInputData;
import com.salescrm.telephony.model.AddLeadCustomerInputData;
import com.salescrm.telephony.model.AddLeadDSEInputData;
import com.salescrm.telephony.model.AddLeadLeadInputData;
import com.salescrm.telephony.model.ViewPagerSwiped;

/**
 * Created by bharath on 31/8/16.
 */
public interface AddLeadActivityToFragmentListener {

    void onViewPagerMoved(ViewPagerSwiped viewPagerSwiped);
}
