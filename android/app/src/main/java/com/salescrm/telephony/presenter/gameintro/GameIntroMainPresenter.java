package com.salescrm.telephony.presenter.gameintro;


public class GameIntroMainPresenter {
    public static final int GAME_INTRO_WELCOME = 1;
    public static final int GAME_INTRO_TEAM_LEADER_BOARD = 2;
    public static final int GAME_INTRO_CONSULTANT_LEADER_BOARD = 3;
    public static final int GAME_INTRO_POINT_SYSTEM = 4;
    public static final int GAME_INTRO_GET_STARTED = 5;

    private GameIntroMainView gameIntroMainView;
    private int lastPosition = 0;

    public GameIntroMainPresenter(GameIntroMainView gameIntroMainView) {
        this.gameIntroMainView = gameIntroMainView;
    }

    public void init() {
        gameIntroMainView.init();
    }

    public void updateBottomOval(int position) {
        gameIntroMainView.updateOval(position,lastPosition);
        lastPosition = position;
    }

    public interface GameIntroMainView{
        void init();
        void updateOval(int currentPosition, int lastPosition);
    }
}
