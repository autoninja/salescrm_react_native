
class DealershipModel {
  constructor(dealer_id, dealer_name, dealer_db_name, user_id, user_name, user_dp_url, selected) {
    this.dealer_id = dealer_id;
    this.dealer_name = dealer_name;
    this.dealer_db_name = dealer_db_name;
    this.user_id = user_id;
    this.user_name = user_name;
    this.user_dp_url = user_dp_url;
    this.selected = selected || false;;
    this.updated_at = new Date();
  }
}

module.exports = DealershipModel;
