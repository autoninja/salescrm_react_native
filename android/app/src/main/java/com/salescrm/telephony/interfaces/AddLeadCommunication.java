package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.model.AddLeadActivityInputData;
import com.salescrm.telephony.model.AddLeadCarInputData;
import com.salescrm.telephony.model.AddLeadCustomerInputData;
import com.salescrm.telephony.model.AddLeadDSEInputData;
import com.salescrm.telephony.model.AddLeadLeadInputData;

/**
 * Created by bharath on 31/8/16.
 */
public interface AddLeadCommunication {
    void onFragmentCreated(AddLeadActivityToFragmentListener addLeadCommunication);
    void onCustomerDetailsEdited(AddLeadCustomerInputData addLeadCustomerInputData, int pos);

    void onLeadDetailsEdited(AddLeadLeadInputData addLeadCustomerInputData, int pos);

    void onCarDetailsEdited(AddLeadCarInputData addLeadCarInputData, int pos);

    void onAssignLeadDseEdited(AddLeadDSEInputData addLeadDSEInputData, int pos);

    void onSelectActivityEdited(AddLeadActivityInputData addLeadActivityInputData, AddLeadCarInputData carInputData, int pos);
}
