
class VehicleModel {
  constructor(brand_id, brand_name, brand_models) {
    this.brand_id = brand_id;
    this.brand_name = brand_name;
    this.brand_models = brand_models;
    this.updated_at = new Date();
  }
}

class VehicleModelsModel {
  constructor(model_id, model_name, category, car_variants) {
    this.model_id = model_id;
    this.model_name = model_name;
    this.category = category;
    this.car_variants = car_variants;
  }
}
class VehicleModelsVariantModel {
  constructor(variant_id, variant, fuel_type_id, fuel_type, color_id, color) {
    this.variant_id = variant_id;
    this.variant = variant;
    this.fuel_type_id = fuel_type_id;
    this.fuel_type = fuel_type;
    this.color_id = color_id;
    this.color = color;
  }
}

module.exports = {VehicleModel, VehicleModelsModel, VehicleModelsVariantModel};
