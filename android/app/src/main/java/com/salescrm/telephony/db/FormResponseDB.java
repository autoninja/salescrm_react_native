package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bharath on 31/10/16.
 */

public class FormResponseDB extends RealmObject {
    private String name;

    private FormAnswerValueDB value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FormAnswerValueDB getValue() {
        return value;
    }

    public void setValue(FormAnswerValueDB value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ClassPojo [name = " + name + ", value = " + value + "]";
    }




}