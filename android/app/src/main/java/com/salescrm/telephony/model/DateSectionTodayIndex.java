package com.salescrm.telephony.model;

import com.salescrm.telephony.dataitem.SectionModel;

import java.util.List;

/**
 * Created by bharath on 21/12/16.
 */
public class DateSectionTodayIndex  {
    private List<SectionModel> sections;
    private int todayIndex;

    public DateSectionTodayIndex(List<SectionModel> sections, int todayIndex) {
        this.sections = sections;
        this.todayIndex = todayIndex;
    }

    public List<SectionModel> getSections() {
        return sections;
    }

    public void setSections(List<SectionModel> sections) {
        this.sections = sections;
    }

    public int getTodayIndex() {
        return todayIndex;
    }

    public void setTodayIndex(int todayIndex) {
        this.todayIndex = todayIndex;
    }
}
