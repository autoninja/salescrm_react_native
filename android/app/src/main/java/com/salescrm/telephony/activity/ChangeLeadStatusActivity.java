package com.salescrm.telephony.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.db.car.ExchangeCarDetails;
import com.salescrm.telephony.db.IntrestedCarDetails;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.change_lead_stages.ChildReasons;
import com.salescrm.telephony.db.change_lead_stages.DroppedReasons;
import com.salescrm.telephony.db.change_lead_stages.LeadStagePipelineResult;
import com.salescrm.telephony.db.change_lead_stages.LostReasons;
import com.salescrm.telephony.db.change_lead_stages.NestedStage;
import com.salescrm.telephony.db.change_lead_stages.PipelineStages;
import com.salescrm.telephony.db.change_lead_stages.SavedChangedLeadStageDb;
import com.salescrm.telephony.db.change_lead_stages.Stages;
import com.salescrm.telephony.model.ForwardStageData;
import com.salescrm.telephony.preferences.Preferences;

import java.util.ArrayList;
import java.util.List;

import com.salescrm.telephony.response.GetPipelineResponse;
import com.salescrm.telephony.response.SavePipelineResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;


/**
 * Created by akshata on 31/5/16.
 */
public class ChangeLeadStatusActivity extends AppCompatActivity {
    private Realm realm;
    String pipelineName = "";
    private Preferences pref;
    private ProgressDialog progressDialog;
    Button savelead;
    List<String> listmodelid = new ArrayList<String>();
    List<String> listpipeline = new ArrayList<>();
    List<String> modelcarlists = new ArrayList<>();
    List<String> listpipestage = new ArrayList<>();
    List<String> lostreasons = new ArrayList<>();
    List<String> droppedreasons = new ArrayList<>();
    List<String> pipeLineID = new ArrayList<>();
    List<String> pipeLineStageId = new ArrayList<>();
    List<String> childReasonsList = new ArrayList<>();
    List<String> childReasonsId = new ArrayList<>();
    List<String> reasonsId = new ArrayList<>();
    TextView tv1_select_car, tv1lost, tv1dropped, tv1selectstage, tv1subreason, tv1selectclosingreason;
    Spinner spinner_tv1_select_pipeline, spinner_tv1_select_car, spinner_tv1_select_stage, spinner_lostreasons, spinner_droppedreasons;
    Spinner spinnerSubReasons, spinnerClosingreason;
    private ConnectionDetectorService connectionDetectorService;
    private SavedChangedLeadStageDb savedChangedLeadStageDb;
    private String selectedPipelineId = "", selectedStageId = "", leadlastUpdate = "", remarks = "", selectedReason = "", selectedSubReason = "";
    private String selectedClosingType = "", selectedReasonsId = "", selectedSubReasonsId = "", selectedCarModelId = "";
    private RealmResults<SavedChangedLeadStageDb> savedChangedLeadStageDbRealmResults;
    private int leadStatusSyncSize;
    EditText remarkChangeLeadStages;
    private View parentLayout;
    private Snackbar snackbar;
    private View viewSnacbar;
    private TextView tvSnacbar;
    private RealmResults<DroppedReasons> droppedReasonsRealmResults;
    private RealmResults<LostReasons> lostReasonsRealmResults;
    private SalesCRMRealmTable salesCRMRealmTable;
    private IntrestedCarDetails intrestedCarDetails;
    private UserDetails userDetails;
    private ExchangeCarDetails exchangeCarDetails;
    LeadStagePipelineResult leadStagePipelineResultFetch;

    ArrayAdapter<String> adaptercarpipelinestage;

    private ImageView ibBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_leadstatus);
        savelead = (Button) findViewById(R.id.save_lead_status);
        parentLayout = findViewById(R.id.card_view_change_lead);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(getApplicationContext());

        tv1lost = (TextView) findViewById(R.id.tv1lost);
        tv1_select_car = (TextView) findViewById(R.id.tv1_select_car);
        tv1dropped = (TextView) findViewById(R.id.tv1dropped);
        tv1selectstage = (TextView) findViewById(R.id.tv1selectstage);
        tv1subreason = (TextView) findViewById(R.id.tv1subreason);
        ibBack = (ImageView) findViewById(R.id.ib_form_back_activity);
        tv1selectclosingreason = (TextView) findViewById(R.id.tv1selectclosingreason);
        spinner_lostreasons = (Spinner) findViewById(R.id.spinner_lostreasons);
        spinner_droppedreasons = (Spinner) findViewById(R.id.spinner_droppedreasons);
        spinnerSubReasons = (Spinner) findViewById(R.id.spinner_subreasons);
        spinnerClosingreason = (Spinner) findViewById(R.id.spinner_closingreason);

        spinner_tv1_select_pipeline = (Spinner) findViewById(R.id.spinner_tv1_select_pipeline);
        spinner_tv1_select_car = (Spinner) findViewById(R.id.spinner_tv1_select_car);
        spinner_tv1_select_stage = (Spinner) findViewById(R.id.spinner_tv1_select_stage);
        remarkChangeLeadStages = (EditText) findViewById(R.id.remark_change_lead_stages);

        if(progressDialog == null) {
            progressDialog = new ProgressDialog(ChangeLeadStatusActivity.this);
            progressDialog.setMessage("Processing your data...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();
        intrestedCarDetails = realm.where(IntrestedCarDetails.class).equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();
        userDetails = realm.where(UserDetails.class).findFirst();
        exchangeCarDetails = realm.where(ExchangeCarDetails.class).equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();

        connectionDetectorService = new ConnectionDetectorService(ChangeLeadStatusActivity.this);
        if (connectionDetectorService.isConnectingToInternet()) {
            savedChangedLeadStageDbRealmResults = realm.where(SavedChangedLeadStageDb.class).equalTo("syncId", 1).findAll();
            if (savedChangedLeadStageDbRealmResults != null) {
                leadStatusSyncSize = savedChangedLeadStageDbRealmResults.size() - 1;
                sendPendingLeadStatus();
            } else {

            }
        }
        if (connectionDetectorService.isConnectingToInternet()) {
            callGetPiplineAPI();
        } else {
            setPipeline();
        }

        ibBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        spinner_tv1_select_pipeline.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    selectedPipelineId = pipeLineID.get(i - 1).toString();
                }
                String pipelineItem = spinner_tv1_select_pipeline.getSelectedItem().toString();
                if (listpipeline.get(i).equals("Lead Stages") || listpipeline.get(i).equals("Finance Stages")) {
                    tv1_select_car.setVisibility(View.GONE);
                    spinner_tv1_select_car.setVisibility(View.GONE);
                    tv1selectstage.setVisibility(View.VISIBLE);
                    spinner_tv1_select_stage.setVisibility(View.VISIBLE);
                    tv1_select_car.setVisibility(View.GONE);
                    spinner_tv1_select_car.setVisibility(View.GONE);
                    selectedCarModelId = "";
                    callPipelineStage(pipelineItem);
                } else if (listpipeline.get(i).equals("Test Drive Stages") || listpipeline.get(i).equals("Evaluation Stages") || listpipeline.get(i).equals("Proposal Stages")) {
                    tv1_select_car.setVisibility(View.VISIBLE);
                    spinner_tv1_select_car.setVisibility(View.VISIBLE);
                    tv1lost.setVisibility(View.GONE);
                    tv1dropped.setVisibility(View.GONE);
                    spinner_lostreasons.setVisibility(View.GONE);
                    spinner_droppedreasons.setVisibility(View.GONE);
                    tv1selectclosingreason.setVisibility(View.GONE);
                    spinnerClosingreason.setVisibility(View.GONE);
                    tv1subreason.setVisibility(View.GONE);
                    spinnerSubReasons.setVisibility(View.GONE);
                    tv1selectstage.setVisibility(View.GONE);
                    spinner_tv1_select_stage.setVisibility(View.GONE);
                    callPipelineStage(pipelineItem);
                } else if (pipelineItem.equalsIgnoreCase("<Select>")) {
                    tv1_select_car.setVisibility(View.GONE);
                    spinner_tv1_select_car.setVisibility(View.GONE);
                    tv1lost.setVisibility(View.GONE);
                    tv1dropped.setVisibility(View.GONE);
                    spinner_lostreasons.setVisibility(View.GONE);
                    spinner_droppedreasons.setVisibility(View.GONE);
                    spinner_tv1_select_stage.setVisibility(View.GONE);
                    tv1selectstage.setVisibility(View.GONE);
                    tv1selectclosingreason.setVisibility(View.GONE);
                    spinnerClosingreason.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_tv1_select_stage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // System.out.println("selected StageId :- " + pipeLineStageId.get(position).toString());
                if (position > 0) {
                    selectedStageId = pipeLineStageId.get(position - 1).toString();
                }
                //selectedStageId = pipeLineStageId.get(position).toString();
                if (listpipestage.get(position).equals("Closed")) {
                    tv1lost.setVisibility(View.GONE);
                    spinner_lostreasons.setVisibility(View.GONE);
                    tv1dropped.setVisibility(View.GONE);
                    spinner_droppedreasons.setVisibility(View.GONE);
                    tv1selectclosingreason.setVisibility(View.VISIBLE);
                    spinnerClosingreason.setVisibility(View.VISIBLE);
                    tv1subreason.setVisibility(View.GONE);
                    spinnerSubReasons.setVisibility(View.GONE);
                    ArrayAdapter<String> adapterClosingType = new ArrayAdapter<String>(getApplicationContext(),
                            R.layout.spinner_item, new String[]{"<Select>", "DROPPED", "LOST"});
                    adapterClosingType.setDropDownViewResource(R.layout.spinner_itemcolor);
                    spinnerClosingreason.setAdapter(adapterClosingType);
                    selectedClosingType = spinnerClosingreason.getSelectedItem().toString();
                } else if (listpipestage.get(position).equals("Invoiced") || listpipestage.get(position).equals("Booked") || listpipestage.get(position).equals("Assigned") || listpipestage.get(position).equals("Qualified")) {
                    tv1lost.setVisibility(View.GONE);
                    spinner_lostreasons.setVisibility(View.GONE);
                    tv1dropped.setVisibility(View.GONE);
                    spinner_droppedreasons.setVisibility(View.GONE);
                    tv1subreason.setVisibility(View.GONE);
                    spinnerSubReasons.setVisibility(View.GONE);
                    tv1selectclosingreason.setVisibility(View.GONE);
                    spinnerClosingreason.setVisibility(View.GONE);
                    selectedClosingType = "";
                }else if(listpipestage.get(position).equals("<Select>")){
                    tv1lost.setVisibility(View.GONE);
                    spinner_lostreasons.setVisibility(View.GONE);
                    tv1dropped.setVisibility(View.GONE);
                    spinner_droppedreasons.setVisibility(View.GONE);
                    tv1subreason.setVisibility(View.GONE);
                    spinnerSubReasons.setVisibility(View.GONE);
                    tv1selectclosingreason.setVisibility(View.GONE);
                    spinnerClosingreason.setVisibility(View.GONE);
                    selectedClosingType = "";
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner_tv1_select_car.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    selectedCarModelId = listmodelid.get(position-1).toString();
                    tv1selectstage.setVisibility(View.VISIBLE);
                    spinner_tv1_select_stage.setVisibility(View.VISIBLE);
                    arrangeCarModelStages(selectedCarModelId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerClosingreason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position >0) {
                    selectedClosingType = spinnerClosingreason.getSelectedItem().toString();
                    callReason(position);
                }else{
                    tv1subreason.setVisibility(View.GONE);
                    spinnerSubReasons.setVisibility(View.GONE);
                    tv1lost.setVisibility(View.GONE);
                    spinner_lostreasons.setVisibility(View.GONE);
                    tv1dropped.setVisibility(View.GONE);
                    spinner_droppedreasons.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        savelead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // progressDialog.dismiss();
                //Date date = new Date();
                //DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                //leadlastUpdate = dateFormat.format(date);
                leadlastUpdate = salesCRMRealmTable.getLeadLastUpdated();
                remarks = remarkChangeLeadStages.getText().toString();
                if (remarks.equalsIgnoreCase("")) {
                    callSnacbar("Remarks can't be empty");
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChangeLeadStatusActivity.this);
                    alertDialogBuilder.setMessage("Are you sure ?");
                    alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            remarks = remarkChangeLeadStages.getText().toString();
                            callSaveLeadStatus();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent in = new Intent(ChangeLeadStatusActivity.this, C360Activity.class);
                            startActivity(in);
                            finish();

                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }


            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void callReason(int position) {
        if (position == 1) {
            tv1lost.setVisibility(View.GONE);
            spinner_lostreasons.setVisibility(View.GONE);
            spinner_droppedreasons.setVisibility(View.VISIBLE);
            tv1dropped.setVisibility(View.VISIBLE);
            callDroppedReasons();
        } else if (position == 2) {
            tv1lost.setVisibility(View.VISIBLE);
            spinner_lostreasons.setVisibility(View.VISIBLE);
            spinner_droppedreasons.setVisibility(View.GONE);
            tv1dropped.setVisibility(View.GONE);
            callLostReasons();
        }
    }

    private void addPipelineToDb(Realm realm, List<GetPipelineResponse.Result> allpipelineresponse) {

        System.out.println("JSON Data:- "+new Gson().toJson(allpipelineresponse).toString());
        LeadStagePipelineResult leadStagePipelineResult = new LeadStagePipelineResult();
        for (int p = 0; p < allpipelineresponse.size(); p++) {
            leadStagePipelineResult.setLeadId(Integer.parseInt(allpipelineresponse.get(p).getLead_id()));
            leadStagePipelineResult.pipelineStages.clear();
            for (int i = 0; i < allpipelineresponse.get(p).getPipelineStages().size(); i++) {
                PipelineStages pipelineStagesDb = new PipelineStages();
                pipelineStagesDb.setPipeline_id(allpipelineresponse.get(p).getPipelineStages().get(i).getPipeline_id());
                pipelineStagesDb.setPipeline_name(allpipelineresponse.get(p).getPipelineStages().get(i).getPipeline_name());
                if (allpipelineresponse.get(p).getPipelineStages().get(i).getStages() != null) {
                    pipelineStagesDb.stages.clear();
                    for (int j = 0; j < allpipelineresponse.get(p).getPipelineStages().get(i).getStages().size(); j++) {
                        Stages stagesDb = new Stages();
                        if (allpipelineresponse.get(p).getPipelineStages().get(i).getStages().get(j).getCar_model() != null) {
                            stagesDb.setCategory_id(allpipelineresponse.get(p).getPipelineStages().get(i).getStages().get(j).getCategory_id());
                            stagesDb.setCar_model(allpipelineresponse.get(p).getPipelineStages().get(i).getStages().get(j).getCar_model());
                            stagesDb.nested_stages.clear();
                            for (int k = 0; k < allpipelineresponse.get(p).getPipelineStages().get(i).getStages().get(j).getNested_stages().size(); k++) {
                                NestedStage nestedStageDb = new NestedStage();
                                nestedStageDb.setStage_id(allpipelineresponse.get(p).getPipelineStages().get(i).getStages().get(j).getNested_stages().get(k).getStage_id());
                                nestedStageDb.setStage_name(allpipelineresponse.get(p).getPipelineStages().get(i).getStages().get(j).getNested_stages().get(k).getStage_name());
                                stagesDb.nested_stages.add(nestedStageDb);
                            }
                            pipelineStagesDb.stages.add(stagesDb);

                        } else {
                            stagesDb.setCategory_id(allpipelineresponse.get(p).getPipelineStages().get(i).getStages().get(j).getCategory_id());
                            stagesDb.setStage_id(allpipelineresponse.get(p).getPipelineStages().get(i).getStages().get(j).getStage_id());
                            stagesDb.setStage_name(allpipelineresponse.get(p).getPipelineStages().get(i).getStages().get(j).getStage_name());
                            pipelineStagesDb.stages.add(stagesDb);
                        }
                    }
                }
                //realm.copyToRealmOrUpdate(pipelineStagesDb);
                leadStagePipelineResult.pipelineStages.add(pipelineStagesDb);
            }

            leadStagePipelineResult.lostReasons.clear();
            //LostReasons lostReasonsDb = new LostReasons();
            for (int i = 0; i < allpipelineresponse.get(p).getLostReasons().size(); i++) {
                LostReasons lostReasonsDb = new LostReasons();
                lostReasonsDb.setId(allpipelineresponse.get(p).getLostReasons().get(i).getId());
                lostReasonsDb.setReason(allpipelineresponse.get(p).getLostReasons().get(i).getReason());
                lostReasonsDb.childReasons.clear();
                for (int j = 0; j < allpipelineresponse.get(p).getLostReasons().get(i).getChildReasons().size(); j++) {
                    ChildReasons childReasonsDb = new ChildReasons();
                    childReasonsDb.setId(allpipelineresponse.get(p).getLostReasons().get(i).getChildReasons().get(j).getId());
                    childReasonsDb.setReason(allpipelineresponse.get(p).getLostReasons().get(i).getChildReasons().get(j).getReason());
                    lostReasonsDb.childReasons.add(childReasonsDb);
                }
                //realm.copyToRealmOrUpdate(lostReasonsDb);
                leadStagePipelineResult.lostReasons.add(lostReasonsDb);
            }


            //DroppedReasons droppedReasonsDb = new DroppedReasons();
            leadStagePipelineResult.droppedReasons.clear();
            for (int i = 0; i < allpipelineresponse.get(p).getDroppedReasons().size(); i++) {
                DroppedReasons droppedReasonsDb = new DroppedReasons();
                droppedReasonsDb.setId(allpipelineresponse.get(p).getDroppedReasons().get(i).getId());
                droppedReasonsDb.setReason(allpipelineresponse.get(p).getDroppedReasons().get(i).getReason());
                droppedReasonsDb.childReasons.clear();
                for (int j = 0; j < allpipelineresponse.get(p).getDroppedReasons().get(i).getChildReasons().size(); j++) {
                    ChildReasons childReasonsDb = new ChildReasons();
                    childReasonsDb.setId(allpipelineresponse.get(p).getDroppedReasons().get(i).getChildReasons().get(j).getId());
                    childReasonsDb.setReason(allpipelineresponse.get(p).getDroppedReasons().get(i).getChildReasons().get(j).getReason());
                    droppedReasonsDb.childReasons.add(childReasonsDb);
                }
                //realm.copyToRealmOrUpdate(droppedReasonsDb);
                leadStagePipelineResult.droppedReasons.add(droppedReasonsDb);
            }

            realm.copyToRealmOrUpdate(leadStagePipelineResult);
        }
    }

    public void callDroppedReasons() {

        leadStagePipelineResultFetch = realm.where(LeadStagePipelineResult.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();
        //droppedReasonsRealmResults = realm.where(DroppedReasons.class).findAll();
        droppedreasons.clear();
        reasonsId.clear();
        droppedreasons.add("<Select>");
        for (int i = 0; i < leadStagePipelineResultFetch.getDroppedReasons().size(); i++) {
            droppedreasons.add(leadStagePipelineResultFetch.getDroppedReasons().get(i).getReason());
            reasonsId.add(leadStagePipelineResultFetch.getDroppedReasons().get(i).getId());
        }
        spinner_droppedreasons.setVisibility(View.VISIBLE);
        tv1dropped.setVisibility(View.VISIBLE);
        ArrayAdapter<String> adapterdroppedreasons = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item, droppedreasons);
        adapterdroppedreasons.setDropDownViewResource(R.layout.spinner_itemcolor);
        spinner_droppedreasons.setAdapter(adapterdroppedreasons);

        spinner_droppedreasons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //selectedReason = spinner_droppedreasons.getSelectedItem().toString();
                if(position > 0) {
                    selectedReasonsId = reasonsId.get(position-1).toString();
                    spinnerSubReasons.setVisibility(View.GONE);
                    tv1subreason.setVisibility(View.GONE);
                    position = position -1;
                    childReasonsList.clear();
                    childReasonsId.clear();
                    childReasonsList.add("<Select>");
                    if (leadStagePipelineResultFetch.getDroppedReasons().get(position).getChildReasons().size() > 0) {
                        for (int j = 0; j < leadStagePipelineResultFetch.getDroppedReasons().get(position).getChildReasons().size(); j++) {
                            childReasonsList.add(leadStagePipelineResultFetch.getDroppedReasons().get(position).getChildReasons().get(j).getReason());
                            childReasonsId.add(leadStagePipelineResultFetch.getDroppedReasons().get(position).getChildReasons().get(j).getId());
                        }
                        callSubReason(childReasonsList);
                    }
                }else {
                    spinnerSubReasons.setVisibility(View.GONE);
                    tv1subreason.setVisibility(View.GONE);
                }
                if(leadStagePipelineResultFetch.getDroppedReasons().size() < position){
                    if (leadStagePipelineResultFetch.getDroppedReasons().get(position).getChildReasons().size() > 0) {
                        for (int j = 0; j < leadStagePipelineResultFetch.getDroppedReasons().get(position).getChildReasons().size(); j++) {
                            childReasonsList.add(leadStagePipelineResultFetch.getDroppedReasons().get(position).getChildReasons().get(j).getReason());
                            childReasonsId.add(leadStagePipelineResultFetch.getDroppedReasons().get(position).getChildReasons().get(j).getId());
                        }
                        callSubReason(childReasonsList);
                    }
                    }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void callLostReasons() {

        leadStagePipelineResultFetch = realm.where(LeadStagePipelineResult.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();
        //lostReasonsRealmResults = realm.where(LostReasons.class).findAll();
        lostreasons.clear();
        reasonsId.clear();
        lostreasons.add("<Select>");
        for (int i = 0; i < leadStagePipelineResultFetch.getLostReasons().size(); i++) {
            lostreasons.add(leadStagePipelineResultFetch.getLostReasons().get(i).getReason());
            reasonsId.add(leadStagePipelineResultFetch.getLostReasons().get(i).getId());
        }
        ArrayAdapter<String> adapterlostreasons = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item, lostreasons);
        adapterlostreasons.setDropDownViewResource(R.layout.spinner_itemcolor);
        spinner_lostreasons.setAdapter(adapterlostreasons);
        tv1lost.setVisibility(View.VISIBLE);
        spinner_lostreasons.setVisibility(View.VISIBLE);

        spinner_lostreasons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedReason = spinner_lostreasons.getSelectedItem().toString();
                if(position > 0) {
                    selectedReasonsId = reasonsId.get(position-1).toString();
                    spinnerSubReasons.setVisibility(View.GONE);
                    tv1subreason.setVisibility(View.GONE);
                    position = position -1;
                    childReasonsList.clear();
                    childReasonsId.clear();
                    childReasonsList.add("<Select>");
                    if (leadStagePipelineResultFetch.getLostReasons().get(position).getChildReasons().size() > 0) {
                        for (int j = 0; j < leadStagePipelineResultFetch.getLostReasons().get(position).getChildReasons().size(); j++) {
                            childReasonsList.add(leadStagePipelineResultFetch.getLostReasons().get(position).getChildReasons().get(j).getReason());
                            childReasonsId.add(leadStagePipelineResultFetch.getLostReasons().get(position).getChildReasons().get(j).getId());
                        }
                        callSubReason(childReasonsList);
                    }
                }else {
                    spinnerSubReasons.setVisibility(View.GONE);
                    tv1subreason.setVisibility(View.GONE);
                }

                }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void callSubReason(List<String> childReasonList) {
        spinnerSubReasons.setVisibility(View.VISIBLE);
        tv1subreason.setVisibility(View.VISIBLE);
        ArrayAdapter<String> adapterSubReasons = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item, childReasonList);
        adapterSubReasons.setDropDownViewResource(R.layout.spinner_itemcolor);
        spinnerSubReasons.setAdapter(adapterSubReasons);

        spinnerSubReasons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //selectedSubReason = spinnerSubReasons.getSelectedItem().toString();
                if(position > 0 )
                selectedSubReasonsId = childReasonsId.get(position-1).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void callSaveLeadStatus() {
        if (connectionDetectorService.isConnectingToInternet()) {
            callLeadStatusAPI();
        } else {
            realm.beginTransaction();
            savedChangedLeadStageDb = realm.createObject(SavedChangedLeadStageDb.class);
            savedChangedLeadStageDb.setSyncId(1);
            savedChangedLeadStageDb.setLeadId(Integer.parseInt(pref.getLeadID()));
            savedChangedLeadStageDb.setSelcetedPipeline(selectedPipelineId);
            savedChangedLeadStageDb.setSelectedStage(selectedStageId);
            savedChangedLeadStageDb.setRemarks(remarks);
            savedChangedLeadStageDb.setLeadLastUpdate(leadlastUpdate);
            savedChangedLeadStageDb.setReason(selectedReasonsId);
            savedChangedLeadStageDb.setSubReason(selectedSubReasonsId);
            savedChangedLeadStageDb.setClosingType(selectedClosingType);
            savedChangedLeadStageDb.setModel(selectedCarModelId);
            realm.commitTransaction();
            callSnacbar("Done");
            Intent in = new Intent(ChangeLeadStatusActivity.this, C360Activity.class);
            startActivity(in);
            ChangeLeadStatusActivity.this.finish();
        }
        selectedReasonsId = "";
        selectedSubReasonsId = "";
    }

    private void callSnacbar(String message) {
        snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_SHORT);
        viewSnacbar = snackbar.getView();
        tvSnacbar = (TextView) viewSnacbar.findViewById(android.support.design.R.id.snackbar_text);
        tvSnacbar.setGravity(Gravity.CENTER_HORIZONTAL);
        tvSnacbar.setTextColor(Color.WHITE);
        viewSnacbar.setBackgroundColor(Color.BLACK);
        snackbar.show();
    }

    private ForwardStageData getLeadStatusToForward() {
        ForwardStageData forwardStageData = new ForwardStageData();
        forwardStageData.setLead_id(pref.getLeadID());
        forwardStageData.setLead_last_updated(leadlastUpdate);

        ForwardStageData.Response response = forwardStageData.new Response();
        response.setSelected_pipeline(selectedPipelineId);
        response.setRemarks(remarks);
        response.setSelected_stage(selectedStageId);
        //if Car model is needed
        if(!selectedCarModelId.equalsIgnoreCase("")) {
            response.setSelected_car(selectedCarModelId);
        }

        if (selectedPipelineId.equalsIgnoreCase("1") && selectedStageId.equalsIgnoreCase("9")) {
            ForwardStageData.Response.StageClosed stageClosed = response.new StageClosed();
            stageClosed.setSelected_closing_type(selectedClosingType);
            ForwardStageData.Response.StageClosed.ClosingType closingType = stageClosed.new ClosingType();
            closingType.setSelected_lead_closing_reason(selectedReasonsId);
            closingType.setSelected_lead_closing_reason_main(selectedSubReasonsId);
            if (selectedClosingType.equalsIgnoreCase("LOST")) {
                stageClosed.setLost(closingType);
            } else if (selectedClosingType.equalsIgnoreCase("DROPPED")) {
                stageClosed.setDropped(closingType);
            }
            response.setClosed(stageClosed);
        }
        forwardStageData.setResponse(response);

        return forwardStageData;
    }

    private void callLeadStatusAPI() {
        System.out.println("my lead status JSON:- "+new Gson().toJson(getLeadStatusToForward()).toString());
        progressDialog.show();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).SavePipeline(getLeadStatusToForward(), new Callback<SavePipelineResponse>() {
            @Override
            public void failure(RetrofitError error) {
                Log.e("Getsavecpipeline", "savepipeline - " + error.getMessage());
                progressDialog.dismiss();
            }

            @Override
            public void success(SavePipelineResponse savePipelineResponse, Response response) {
                Log.e("Getsavepipeline", "Success - ");
                System.out.println("GetsavepipelineMessage:- "+response.toString()+" --- "+savePipelineResponse.toString());
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }
                if(savePipelineResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(ChangeLeadStatusActivity.this,0);
                }

                SavePipelineResponse savepipeline = (SavePipelineResponse) savePipelineResponse;
                Toast.makeText(getApplicationContext(), "Forwarded", Toast.LENGTH_SHORT).show();
                Intent in = new Intent(ChangeLeadStatusActivity.this, C360Activity.class);
                startActivity(in);
                ChangeLeadStatusActivity.this.finish();
            }
        });
    }

    private void sendPendingLeadStatus() {
        if (leadStatusSyncSize > 0) {
            ForwardStageData forwardStageData = new ForwardStageData();
            forwardStageData.setLead_id(pref.getLeadID());
            forwardStageData.setLead_last_updated(savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getLeadLastUpdate());

            ForwardStageData.Response response = forwardStageData.new Response();
            response.setSelected_pipeline(savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getSelcetedPipeline());
            response.setRemarks(savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getRemarks());
            response.setSelected_stage(savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getSelectedStage());
            //if Car model is needed
            if(!savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getModel().equalsIgnoreCase("")){
                response.setSelected_car(savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getModel());
            }
            if (savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getSelcetedPipeline().equalsIgnoreCase("1")) {
                ForwardStageData.Response.StageClosed stageClosed = response.new StageClosed();
                stageClosed.setSelected_closing_type(savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getClosingType());
                ForwardStageData.Response.StageClosed.ClosingType closingType = stageClosed.new ClosingType();
                closingType.setSelected_lead_closing_reason(savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getReason());
                closingType.setSelected_lead_closing_reason_main(savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getSubReason());
                if (savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getClosingType().equalsIgnoreCase("Lost")) {
                    stageClosed.setLost(closingType);
                } else if (savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).getClosingType().equalsIgnoreCase("Dropped")) {
                    stageClosed.setDropped(closingType);
                }
                response.setClosed(stageClosed);
            }
            forwardStageData.setResponse(response);

            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).SavePipeline(forwardStageData,
                    new Callback<SavePipelineResponse>() {
                        @Override
                        public void success(SavePipelineResponse savePipelineResponse, Response response) {
                            realm.beginTransaction();
                            savedChangedLeadStageDbRealmResults.get(leadStatusSyncSize).setSyncId(0);
                            realm.commitTransaction();
                            leadStatusSyncSize--;
                            List<Header> headerList = response.getHeaders();
                            for (Header header : headerList) {
                                Log.e("sendsmsresponse", "sendsmsresponse -  " + header.getName() + " - " + header.getValue());
                                if (header.getName().equalsIgnoreCase("Authorization")) {
                                    ApiUtil.UpdateAccessToken(header.getValue());
                                }
                            }

                            if(savePipelineResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                                ApiUtil.InvalidUserLogout(ChangeLeadStatusActivity.this,0);
                            }

                            sendPendingLeadStatus();
                        }

                        @Override
                        public void failure(RetrofitError error) {

                        }
                    });
        } else {
            return;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent in = new Intent(ChangeLeadStatusActivity.this, C360Activity.class);
                startActivity(in);
                this.finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public void callGetPiplineAPI() {
        List<String> leadList = new ArrayList<>();
        leadList.clear();
        leadList.add(pref.getLeadID());

        progressDialog.show();
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetAllStagingPipeline(leadList, new Callback<GetPipelineResponse>() {

            @Override
            public void success(GetPipelineResponse getPipelineResponse, Response response) {
                Log.e("Getpipeline", "Success - ");
                //progressDialog.dismiss();

                if(getPipelineResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(ChangeLeadStatusActivity.this,0);
                }

                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }

                }
                final List<GetPipelineResponse.Result> allpipelineresponse = getPipelineResponse.getResult();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        addPipelineToDb(realm, allpipelineresponse);
                    }
                });

                setPipeline();
                progressDialog.dismiss();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Getpipeline", "getpipeline - " + error.getMessage());
                progressDialog.dismiss();
            }
        });
    }

    private void setPipeline() {

        //RealmResults<PipelineStages> pipelineStagesRealmResults = realm.where(PipelineStages.class).findAll();
        leadStagePipelineResultFetch = realm.where(LeadStagePipelineResult.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();
        listpipeline.clear();
        pipeLineID.clear();
        listpipeline.add("<Select>");
        System.out.println("Pipeline Size:- "+leadStagePipelineResultFetch.getPipelineStages().size());
        for (int i = 0; i < leadStagePipelineResultFetch.getPipelineStages().size(); i++) {
            if (leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_name().equalsIgnoreCase("Finance Stages")) {

            } else if (leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_name().equalsIgnoreCase("Test Drive Stages") ||
                    leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_name().equalsIgnoreCase("Proposal Stages")) {
                if (intrestedCarDetails != null) {
                    listpipeline.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_name());
                    pipeLineID.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_id());
                }
            } else if (leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_name().equalsIgnoreCase("Evaluation Stages")) {
                if (exchangeCarDetails != null) {
                    listpipeline.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_name());
                    pipeLineID.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_id());
                }
            } else {
                listpipeline.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_name());
                pipeLineID.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_id());
            }
            for (int j = 0; j < leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().size(); j++) {
                if (leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_name() != null) {
                    listpipestage.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_name());
                    pipeLineStageId.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_id());
                }
            }
        }

        ArrayAdapter<String> adaptercarpipeline = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item, listpipeline);
        adaptercarpipeline.setDropDownViewResource(R.layout.spinner_itemcolor);
        spinner_tv1_select_pipeline.setAdapter(adaptercarpipeline);
    }

    private void arrangeCarModelStages(String selectedCarModelId) {
        leadStagePipelineResultFetch = realm.where(LeadStagePipelineResult.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();
        listpipestage.clear();
        pipeLineStageId.clear();
        listpipestage.add("<Select>");
        for (int i = 0; i < leadStagePipelineResultFetch.getPipelineStages().size(); i++) {
            if (leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_name().equalsIgnoreCase(pipelineName)) {
                for (int j = 0; j < leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().size(); j++) {
                    if (leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getCategory_id().equalsIgnoreCase(selectedCarModelId)) {
                        listpipestage.clear();
                        pipeLineStageId.clear();
                        listpipestage.add("<Select>");
                        for (int k = 0; k < leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getNested_stages().size(); k++) {
                            listpipestage.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getNested_stages().get(k).getStage_name());
                            pipeLineStageId.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getNested_stages().get(k).getStage_id());
                        }
                        adaptercarpipelinestage.notifyDataSetChanged();
                    }
                }
            }
        }
    }
    public void callPipelineStage(String pipelineItem) {
        pipelineName = pipelineItem;
        //RealmResults<PipelineStages> pipelineStagesRealmResults = realm.where(PipelineStages.class).equalTo("pipeline_name", pipelineItem).findAll();
        leadStagePipelineResultFetch = realm.where(LeadStagePipelineResult.class)
                .equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();
        listpipestage.clear();
        pipeLineStageId.clear();
        listpipestage.add("<Select>");
        for (int i = 0; i < leadStagePipelineResultFetch.getPipelineStages().size(); i++) {
            if(leadStagePipelineResultFetch.getPipelineStages().get(i).getPipeline_name().equalsIgnoreCase(pipelineItem)) {
                modelcarlists.clear();
                listmodelid.clear();
                modelcarlists.add("<Select>");
                for (int j = 0; j < leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().size(); j++) {
                    if (leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getCar_model() != null) {
                        modelcarlists.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getCar_model());
                        listmodelid.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getCategory_id());
                        listpipestage.clear();
                        pipeLineStageId.clear();
                        listpipestage.add("<Select>");
                        for (int k = 0; k < leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getNested_stages().size(); k++) {
                            if (leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getNested_stages().get(k).getStage_name() != null) {
                                listpipestage.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getNested_stages().get(k).getStage_name());
                                pipeLineStageId.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getNested_stages().get(k).getStage_id());
                            }
                        }
                    } else if (leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_name() != null) {
                        if (leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_name().equalsIgnoreCase("Qualified")) {
                            if (salesCRMRealmTable.getCustomerName() != null && !salesCRMRealmTable.getCustomerName().equalsIgnoreCase("") && intrestedCarDetails != null) {
                                listpipestage.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_name());
                                pipeLineStageId.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_id());
                            } else {

                            }

                        } else if (leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_name().equalsIgnoreCase("Assigned")) {
                            if (userDetails != null) {
                            } else {
                                listpipestage.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_name());
                                pipeLineStageId.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_id());
                            }

                        } else if (leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_name().equalsIgnoreCase("Booked")
                                || leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_name().equalsIgnoreCase("Invoiced")) {

                        } else {
                            listpipestage.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_name());
                            pipeLineStageId.add(leadStagePipelineResultFetch.getPipelineStages().get(i).getStages().get(j).getStage_id());
                        }
                    }
                }
            }
        }
        adaptercarpipelinestage = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item, listpipestage);
        adaptercarpipelinestage.setDropDownViewResource(R.layout.spinner_itemcolor);
        spinner_tv1_select_stage.setAdapter(adaptercarpipelinestage);
        if(listpipestage.size() == 0) {
            spinner_tv1_select_stage.setVisibility(View.GONE);
            tv1selectstage.setVisibility(View.GONE);
        }

        ArrayAdapter<String> dataAdaptermodelcarlists = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item, modelcarlists);
        dataAdaptermodelcarlists.setDropDownViewResource(R.layout.spinner_itemcolor);
        spinner_tv1_select_car.setAdapter(dataAdaptermodelcarlists);
        if(modelcarlists.size() == 0) {
            spinner_tv1_select_car.setVisibility(View.GONE);
            tv1_select_car.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(ChangeLeadStatusActivity.this, C360Activity.class);
        startActivity(in);
        this.finish();
    }

}



