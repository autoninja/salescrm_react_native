import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, Alert } from 'react-native';
import { FAB } from 'react-native-paper';
export default class TotalLiveEF extends Component{
    constructor(props) {
        super(props);
    }
    onLongPress() {
        if(this.props.showPercentage) {
            Alert.alert('Long Disabled');
        }
        else {
            Alert.alert('Long Enabled');
        }
    }
    render() {
        let title = "Total";
        if(this.props.title) {
            title = this.props.title;
        }
        let data = this.props.data;
        let r,ex,f_i,f_o,pb,le;
        if(this.props.showPercentage) {
            r = data.rel.r;
            ex = data.rel.ex;
            f_i = data.rel.f_i;
            f_o = data.rel.f_o;
            pb = '-';
            le = '-';
        }
        else {
            r = data.abs.r;
            ex = data.abs.ex;
            f_i = data.abs.f_i;
            f_o = data.abs.f_o;
            pb = data.abs.pb;
            le = data.abs.le;
        }
       
        return(
            <View style={{margin:2}}>
                <View style={styles.containerWhite}>
                    <View style={styles.itemEmpty}>
                        <Text style={styles.itemHeader}>{title}</Text>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{r}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{ex}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{f_i}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={styles.itemText}>{f_o}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={[styles.itemText, {color:'#00bfa5'}]}>{pb}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    <View style= {styles.item}>
                        <View style= {{justifyContent: 'center',alignItems: 'center'}}>
                            <TouchableWithoutFeedback
                            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                            onLongPress={() => {
                                this.onLongPress();
                            }}>
                                <Text style={[styles.itemText, {color:'#00bfa5'}]}>{le}</Text>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>

                 
                </View>
            </View>
        )
    }
}

let styles = StyleSheet.create({
    containerWhite: {
        backgroundColor:'#fff',
        paddingTop:8,
        paddingBottom:8,
        paddingLeft:2,
        paddingRight:2,
        flexDirection:'row',
    },
    itemEmpty: {
        flex:1.5,
        alignSelf:'center',
    },
    item: {
        flex:1,
        alignSelf:'center',
    },
    itemText: {
        fontSize:14,
        color:'#1B075A',
        alignSelf:'center',
        textAlign:'center'
    },
    itemHeader: {
        fontSize:15,
        fontWeight:'bold',
        color:'#2D4F8B',
        alignSelf:'flex-start',
        paddingLeft:2,
       
    }
})