package com.salescrm.telephony.sync;

import android.content.Context;

import com.salescrm.telephony.offline.AutoFetchFormData;
import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.db.FormAnswerDB;
import com.salescrm.telephony.db.FormAnswerValueDB;
import com.salescrm.telephony.db.FormResponseDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.dbOperation.TasksDbOperation;
import com.salescrm.telephony.interfaces.AutoFetchFormListener;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.interfaces.SyncListener;
import com.salescrm.telephony.model.FormSubmissionActivityInputData;
import com.salescrm.telephony.model.FormSubmissionInputData;
import com.salescrm.telephony.model.FormSubmissionInputDataServer;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.FormSubmissionActivityResponse;
import com.salescrm.telephony.response.FormSubmissionResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bharath on 13/12/16.
 */

public class SyncFormData implements FetchC360OnCreateLeadListener, AutoFetchFormListener {

    public SyncListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private SalesCRMRealmTable salesCRMRealmTable;
    private String TAG = "SyncFormData";
    private RealmResults<FormAnswerDB> formAnswerDBList;
    private String generatedLeadId;
    private TasksDbOperation tasksDbOperation;

    public SyncFormData(SyncListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
        formAnswerDBList = realm.where(FormAnswerDB.class).equalTo("is_synced", false).findAll();
    }

    public void sync() {
        System.out.println("Form sync size:" + formAnswerDBList.size());
        if (formAnswerDBList.size() > 0) {
            FormSubmissionInputData formSubmissionInputData = getSubmissionInputData(formAnswerDBList.get(0));
            submitForm(formAnswerDBList.get(0), formSubmissionInputData);
        } else {
            listener.onFormSync();
        }


    }

    private FormSubmissionInputData getSubmissionInputData(FormAnswerDB formAnswerDB) {
        FormSubmissionInputData formSubmissionInputData = new FormSubmissionInputData();
        formSubmissionInputData.setLead_last_updated(formAnswerDB.getLead_last_updated());
        formSubmissionInputData.setLead_id(formAnswerDB.getLead_id());
        formSubmissionInputData.setAction_id(formAnswerDB.getAction_id());
        formSubmissionInputData.setScheduled_activity_id(formAnswerDB.getScheduled_activity_id());

        List<FormSubmissionInputData.Form_response> form_responseList = new ArrayList<>();

        RealmList<FormResponseDB> form_responseListDB = formAnswerDB.getForm_response();
        for (int i = 0; i < form_responseListDB.size(); i++) {
            FormSubmissionInputData.Form_response currentData = formSubmissionInputData.new Form_response();
            currentData.setName(form_responseListDB.get(i).getName());

            FormAnswerValueDB currentValueDB = form_responseListDB.get(i).getValue();

            FormSubmissionInputData.Form_response.Value value = currentData.new Value();
            value.setFAnsId(currentValueDB.getFAnsId());
            value.setAnswerValue(currentValueDB.getAnswerValue());
            value.setDisplayText(currentValueDB.getDisplayText());

            currentData.setValue(value);

            form_responseList.add(currentData);
        }

        formSubmissionInputData.setForm_response(form_responseList);
        return formSubmissionInputData;
    }


    private void submitForm(final FormAnswerDB data, FormSubmissionInputData formSubmissionInputData) {
        if (new ConnectionDetectorService(context).isConnectingToInternet()) {

            FormSubmissionActivityInputData formSubmissionActivityInputData = new FormSubmissionActivityInputData();
            formSubmissionActivityInputData.setLead_id(formSubmissionInputData.getLead_id());
            formSubmissionActivityInputData.setLead_last_updated(formSubmissionInputData.getLead_last_updated());
            formSubmissionActivityInputData.setSchedule_activity_id(formSubmissionInputData.getScheduled_activity_id());
            formSubmissionActivityInputData.setScheduled_type(data.getScheduled_activity_id() + "");
            formSubmissionActivityInputData.setForm_response(formSubmissionInputData.getForm_response());

            if (formSubmissionInputData.getAction_id().equalsIgnoreCase(WSConstants.FormAction.ADD_ACTIVITY + "")) {
                System.out.println("Creating activity..");
                System.out.println("String with quote:" + formSubmissionActivityInputData.toString());
                ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).submitNewActivity(formSubmissionActivityInputData, new Callback<FormSubmissionActivityResponse>() {
                    @Override
                    public void success(FormSubmissionActivityResponse o, Response response) {
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }
                        }
                        if (!o.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                            System.out.println("Success:0" + o.getMessage());
                            updateSyncStatus(data, false, o.getMessage());
                            //showAlert(validateOtpResponse.getMessage());
                        } else {
                            if (o.getResult() != null) {
                                TasksDbOperation.getInstance().updateLeadLastUpdated(realm, o.getResult().getLead_id(), o.getResult().getLead_last_updated());
                                updateSyncStatus(data, true, o.getMessage());
                            } else {
                                // relLoader.setVisibility(View.GONE);
                                updateSyncStatus(data, false, o.getMessage());
                                System.out.println("Success:2" + o.getMessage());
                                //         Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                                //showAlert(validateOtpResponse.getMessage());
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        updateSyncStatus(data, false, "Failed");
                        System.out.println("Something went wrong while submit form");
                    }
                });
            } else {

                System.out.println("Submitting form..");

                FormSubmissionInputDataServer doneForm = new FormSubmissionInputDataServer();
                doneForm.setLead_id(formSubmissionInputData.getLead_id());
                doneForm.setLead_last_updated(formSubmissionInputData.getLead_last_updated());
                doneForm.setAction_id(formSubmissionInputData.getAction_id());
                doneForm.setScheduled_activity_id(formSubmissionInputData.getScheduled_activity_id());

                List<FormSubmissionInputDataServer.Form_response> form_responses = new ArrayList<>();
                for (int i = 0; i < formSubmissionInputData.getForm_response().size(); i++) {
                    FormSubmissionInputDataServer.Form_response formResponseDone = new FormSubmissionInputDataServer().new Form_response();
                    formResponseDone.setName(formSubmissionInputData.getForm_response().get(i).getName());

                    FormSubmissionInputDataServer.Form_response.Value value =
                            new FormSubmissionInputDataServer().new Form_response().new Value();
                    value.setFAnsId(formSubmissionInputData.getForm_response().get(i).getValue().getFAnsId());
                    value.setDisplayText(formSubmissionInputData.getForm_response().get(i).getValue().getDisplayText());
                    value.setAnswerValue(formSubmissionInputData.getForm_response().get(i).getValue().getAnswerValue());

                    formResponseDone.setValue(value);
                    form_responses.add(formResponseDone);
                }
                doneForm.setForm_response(form_responses);

                ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).submitForm(doneForm, new Callback<FormSubmissionResponse>() {
                    @Override
                    public void success(FormSubmissionResponse o, Response response) {
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }
                        }
                        if (!o.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                            System.out.println("Success:0" + o.getMessage());
                            updateSyncStatus(data, false, o.getMessage());
                            //showAlert(validateOtpResponse.getMessage());
                        } else {
                            if (o.getResult().isSubmission_status()) {
                                TasksDbOperation.getInstance().updateLeadLastUpdated(realm, o.getResult().getLead_id(), o.getResult().getLead_last_updated());
                                updateSyncStatus(data, true, o.getMessage());
                            } else {
                                // relLoader.setVisibility(View.GONE);
                                updateSyncStatus(data, false, o.getMessage());
                                System.out.println("Success:2" + o.getMessage());
                                // Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
                                //showAlert(validateOtpResponse.getMessage());
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        updateSyncStatus(data, false, "Failed");
                        // Toast.makeText(context, error.getMessage() + "", Toast.LENGTH_SHORT).show();
                        System.out.println("Something went wrong while submit form");
                    }
                });
            }

        }

    }

    private void updateSyncStatus(final FormAnswerDB data, boolean b, String error) {
        if (data.isValid()) {
            realm.beginTransaction();
            data.setIs_synced(true);
            realm.commitTransaction();

            if (new ConnectionDetectorService(context).isConnectingToInternet()) {
                fetchC360(Util.getInt(data.getLead_id()));
            }
        }
    }
    private void fetchC360(int leadId) {
        this.generatedLeadId = leadId + "";
        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(generatedLeadId);
        new FetchC360InfoOnCreateLead(this, context, leadData, pref.getAppUserId()).call(false);

    }

    @Override
    public void onFetchC360OnCreateLeadSuccess() {
        new AutoFetchFormData(this, context).call(realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId", Util.getInt(generatedLeadId)).equalTo("isDone", false).findAll(), true);

    }

    @Override
    public void onFetchC360OnCreateLeadError() {
        System.out.println(TAG + "Failed");
        sync();
    }

    @Override
    public void onAutoFormsDataFetched(boolean b, RealmResults<SalesCRMRealmTable> data) {
        sync();
    }

    @Override
    public void onAutoFormsDataError(RetrofitError error, int from) {
        System.out.println(TAG + "Failed fetching forms");
        sync();

    }


}
