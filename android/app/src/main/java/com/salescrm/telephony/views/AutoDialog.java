package com.salescrm.telephony.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.OfflineSupportActivity;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.model.AutoDialogModel;

/**
 * Created by bharath on 28/10/16.
 */

public class AutoDialog {
    private Context context;
    private AutoDialogClickListener listener;
    private AutoDialogModel data;

    public AutoDialog( Context context,AutoDialogClickListener listener, AutoDialogModel data) {
        this.context =  context;
        this.listener = listener;
        this.data = data;
    }
    public AutoDialog( AutoDialogClickListener listener, AutoDialogModel data) {
        this.context = (Context) listener;
        this.listener = listener;
        this.data = data;
    }


    public void show() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if( dialog.getWindow()!=null){
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setContentView(R.layout.custom_dialog_yes_no_layout);
        TextView textView = (TextView) dialog.findViewById(R.id.tv_custom_dialog_title);
        textView.setText(data.getDialogTitle());

        TextView tvYes = (TextView) dialog.findViewById(R.id.bt_custom_dialog_yes);
        TextView tvNo = (TextView) dialog.findViewById(R.id.bt_custom_dialog_no);
        if(!TextUtils.isEmpty(data.getYesTitle())){
            tvYes.setText(data.getYesTitle());
        }
        if(!TextUtils.isEmpty(data.getNoTitle())){
            tvNo.setText(data.getNoTitle());
        }

        if(data.getNoTitle()!=null&&data.getNoTitle().equalsIgnoreCase("no_button")){
            tvNo.setVisibility(View.INVISIBLE);
            dialog.findViewById(R.id.frame_custom_dialog_no).setVisibility(View.INVISIBLE);
        }

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //your business logic
                dialog.dismiss();
                listener.onAutoDialogYesButtonClick(v,data);
            }
        });
        tvNo = (TextView)dialog.findViewById(R.id.bt_custom_dialog_no);

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                listener.onAutoDialogNoButtonClick(v,data);
            }
        });

        dialog.setCanceledOnTouchOutside(data.isCancellable());
        dialog.setCancelable(data.isCancellable());
        try {
            dialog.show();
        }
        catch (Exception ignored) {

        }

    }

}
