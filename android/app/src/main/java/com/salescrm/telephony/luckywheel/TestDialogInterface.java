package com.salescrm.telephony.luckywheel;

import android.app.Dialog;

/**
 * Created by prateek on 3/7/18.
 */

public interface TestDialogInterface {

    public void endTestDialog(Dialog dialog);
}
