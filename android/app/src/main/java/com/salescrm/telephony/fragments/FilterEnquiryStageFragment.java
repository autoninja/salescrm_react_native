package com.salescrm.telephony.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.FilterActivityTypeCustomAdapter;
import com.salescrm.telephony.dataitem.FilterActivityTypeModel;
import com.salescrm.telephony.db.FilterActivityType;
import com.salescrm.telephony.db.FilterEnquiryStage;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.FilterActivityAndStagesResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.CleverTapUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by Ravindra P on 03-06-2016.
 */
public class FilterEnquiryStageFragment extends Fragment {

    private static final Integer FILTER_ENQUIRY_STAGE_POS = 5;
    FilterSelectionHolder selectionHolder;
    private View rootView;
    private ListView listView;
    private int lastSelectedListItem = 0;
    private String[] items;
    private List<FilterActivityTypeModel> modelItems;
    private FilterActivityTypeCustomAdapter adapter;
    private Preferences pref;
    private FrameLayout frameProgress;
    private Realm realm;
    private String TAG = "FilterActivityTypeFragment";
    private int apiCallCount = 0;
    private SparseArray<String> selectedValues;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.filter_activity_type_fragment, container, false);
        listView = (ListView) rootView.findViewById(R.id.lvfilter_activity_type_item_list);
        frameProgress = (FrameLayout) rootView.findViewById(R.id.filter_progress_bar);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        selectionHolder = FilterSelectionHolder.getInstance();
        realm = Realm.getDefaultInstance();
        init();
        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FilterActivityTypeModel model = modelItems.get(position);
                if (model.getValue() == 1) {
                    model.setValue(0);
                    if (selectedValues.get(position) != null) {
                        selectedValues.remove(position);
                        selectionHolder.setMapData(FILTER_ENQUIRY_STAGE_POS, selectedValues);
                        if (selectedValues.size() == 0) {
                            selectionHolder.removeMapData(FILTER_ENQUIRY_STAGE_POS);
                        }
                    }
                } else {
                    model.setValue(1);
                    selectedValues.put(position, model.getName());
                    selectionHolder.setMapData(FILTER_ENQUIRY_STAGE_POS, selectedValues);
                }
                modelItems.set(position, model);
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
                lastSelectedListItem = position;
            }
        });

        return rootView;

    }

    private void init() {

        if (realm.where(FilterActivityType.class).findAll().isEmpty() && new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            makeAPICall();
        } else {
            showAdapter();
        }

        if (selectedValues == null) {
            selectedValues = new SparseArray<String>();
        }

    }

    private void showAdapter() {
        modelItems = new ArrayList<>();
        for (int i = 0; i < realm.where(FilterEnquiryStage.class).findAll().size(); i++) {
            FilterEnquiryStage filterEnquiryStage = realm.where(FilterEnquiryStage.class).findAll().get(i);
            modelItems.add(new FilterActivityTypeModel(filterEnquiryStage.getStage(), 0, filterEnquiryStage.getStage_id()));
        }

        selectedValues = selectionHolder.getMapData(FILTER_ENQUIRY_STAGE_POS);
        if (selectedValues != null) {
            for (int i = 0; i < selectedValues.size(); i++) {
                FilterActivityTypeModel model = modelItems.get(selectedValues.keyAt(i));
                if (model != null) {
                    model.setValue(1);
                    modelItems.set(selectedValues.keyAt(i), model);
                }
            }
        }

        adapter = new FilterActivityTypeCustomAdapter(getActivity(), modelItems);
        listView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void makeAPICall() {
        frameProgress.setVisibility(View.VISIBLE);
        ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetFilterActivityAndStages(new Callback<FilterActivityAndStagesResponse>() {
            @Override
            public void success(final FilterActivityAndStagesResponse filterActivityAndStagesResponse, Response response) {
                frameProgress.setVisibility(View.GONE);
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(filterActivityAndStagesResponse.getStatusCode().equalsIgnoreCase(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }

                if (!filterActivityAndStagesResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    System.out.println(TAG + "Success:0" + filterActivityAndStagesResponse.getMessage());
                    //showAlert(validateOtpResponse.getMessage());
                } else {
                    if (filterActivityAndStagesResponse.getResult() != null) {
                        System.out.println(TAG + "Success:1" + filterActivityAndStagesResponse.getMessage());
                        if (filterActivityAndStagesResponse.getResult() != null) {
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    insertData(filterActivityAndStagesResponse);
                                }
                            });
                            showAdapter();
                        }

                    } else {
                        System.out.println(TAG + "Success:2" + filterActivityAndStagesResponse.getMessage());
                        //showAlert(validateOtpResponse.getMessage());
                    }
                }


            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println("Failed");
                frameProgress.setVisibility(View.GONE);
                onErrorApiCallController(error);
            }
        });
    }

    private void onErrorApiCallController(RetrofitError error) {
        if (error.getKind() == RetrofitError.Kind.NETWORK) {
            apiCallCount++;
            if (apiCallCount <= WSConstants.API_MAX_CALL) {
                Toast.makeText(getContext(), "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
            } else {
                apiCallCount = 0;
            }
        } else {
            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            Util.systemPrint("Something happened in the server");
            apiCallCount = 0;
        }
    }


    private void insertData(FilterActivityAndStagesResponse data) {
        FilterActivityType filterActivityType = new FilterActivityType();
        FilterEnquiryStage filterEnquiryStage = new FilterEnquiryStage();
        for (int i = 0; i < data.getResult().getActivities().size(); i++) {
            filterActivityType.setId(data.getResult().getActivities().get(i).getId());
            filterActivityType.setName(data.getResult().getActivities().get(i).getName());
            realm.copyToRealm(filterActivityType);
        }

        for (int i = 0; i < data.getResult().getLeadStages().size(); i++) {
            filterEnquiryStage.setStage_id(data.getResult().getLeadStages().get(i).getStage_id());
            filterEnquiryStage.setStage(data.getResult().getLeadStages().get(i).getStage());
            realm.copyToRealm(filterEnquiryStage);
        }

    }

    public void callAdapter() {
        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                /*if (lastSelectedListItem == position) {
                    modelItems[position] = new FilterActivityTypeModel(0);
                } else if (lastSelectedListItem != position) {
                    modelItems[position] = new FilterActivityTypeModel(1);
                }*/

               /* for (int i = 0; i < parent.getCount(); i++) {
                    if (position == i ) {
                        modelItems[i] = new FilterActivityTypeModel(1);
                    } else {
                        modelItems[i] = new FilterActivityTypeModel(0);
                    }
                }*/
                adapter.notifyDataSetChanged();
                lastSelectedListItem = position;
            }
        });

    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }
}
