package com.salescrm.telephony.db.car;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 14/12/16.
 */
public class CarBrandModelsDB extends RealmObject{

    @PrimaryKey
    private String model_id;

    private RealmList<CarBrandModelVariantsDB> car_variants;

    @Index
    private String model_name;

    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public RealmList<CarBrandModelVariantsDB> getCar_variants() {
        return car_variants;
    }

    public void setCar_variants(RealmList<CarBrandModelVariantsDB> car_variants) {
        this.car_variants = car_variants;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }
}
