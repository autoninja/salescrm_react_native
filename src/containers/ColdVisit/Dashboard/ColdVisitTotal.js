import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
export default class ColdVisitTotal extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let { user_id, user_role_id, count, location_id } = this.props.info;

    return (
      <View
        style={{
          marginBottom: 10,
          marginTop: 10,
          flexDirection: "row",
          height: 40,
          backgroundColor: "#fff",
          alignItems: "center",
          borderRadius: 6,
          borderWidth: 1,
          borderColor: "#eceff1"
        }}
      >
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <View
            style={{ flex: 2, justifyContent: "center", alignItems: "center" }}
          >
            <Text
              style={{ color: "#2e5e86", fontSize: 16, fontWeight: "bold" }}
            >
              Total
            </Text>
          </View>
        </View>

        <View
          style={{
            flex: 3,
            height: "100%"
          }}
        />

        <View
          style={{
            flex: 1,
            height: "100%",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <TouchableOpacity
            hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
            onLongPress={() =>
              this.props.onLongPress("cold_visit_details", {
                info: {
                  user_id,
                  user_role_id,
                  title: "Team data",
                  location_id
                }
              })
            }
          >
            <Text
              style={{
                color: "#2e5e86",
                fontWeight: "bold",
                textDecorationLine: "underline"
              }}
            >
              {count}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
