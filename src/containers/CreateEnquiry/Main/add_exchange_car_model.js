let AddExchangeCarModel = {
    getInputDataServer: (exchange_car_fields, leadId) => {

        let exchangeVehicleDetailMain = exchange_car_fields[0].data;
        let exchangeEvaluatorMain = exchange_car_fields[1].data;
        let exchangeInspectionMain = exchange_car_fields[2].data;
        let exchangPriceMain = exchange_car_fields[3].data;

        let exVehicleDetail = new ExchangeVehicleDetail(exchangeVehicleDetailMain.vehicleBrand.text,
            exchangeVehicleDetailMain.vehicleModel.text,
            exchangeVehicleDetailMain.vehicleModel.id,
            exchangeVehicleDetailMain.vehicleVariant,
            exchangeVehicleDetailMain.vehicleColor,
            exchangeVehicleDetailMain.year.text,
            exchangeVehicleDetailMain.kmRun,
            exchangeVehicleDetailMain.ownerType.text,
            exchangeVehicleDetailMain.reg_no,
            exchangeVehicleDetailMain.vin_no,
            exchangeVehicleDetailMain.vehicleFuelType
        );

        let exEvaluatorReq = new ExchangeEvaluatorRequest(
            exchangeEvaluatorMain.evaluator.text,
            exchangeEvaluatorMain.evaluator.id,
            exchangeEvaluatorMain.evaluation_date,
            exchangeEvaluatorMain.activity_id,
            exchangeEvaluatorMain.remarks
        );

        let exInspection = new ExchangeInspection(
            exchangeInspectionMain.is_under_insurance,
            exchangeInspectionMain.is_doc_verified,
            exchangeInspectionMain.vehicle_condition,
            exchangeInspectionMain.car_images_array,

        );

        let exPrice = new ExchangePrice(
            exchangPriceMain.expected_price,
            exchangPriceMain.quoted_price
        );

        let exchangeCarInput = new ExchangeCarDetail(exVehicleDetail, exEvaluatorReq, exInspection, exPrice, leadId);

        return new CreateEnquiryInputServer(
            exchangeCarInput
        );
    }

};

class CreateEnquiryInputServer {
    constructor(exchange_car) {
        this.exchange_car = exchange_car;
    }
}

class ExchangeCarDetail {
    constructor(vehicle_details, evaluator_request, inspection, price, lead_id) {
        this.vehicle_details = vehicle_details;
        this.evaluator_request = evaluator_request;
        this.inspection = inspection;
        this.price = price;
        this.lead_id = lead_id;
    }
}

class ExchangeVehicleDetail {
    constructor(brand, model, model_id, variant, color, year, kms, owner_type, reg_no, vin_no, fuel_type) {
        this.brand = brand;
        this.model = model;
        this.model_id = model_id;
        this.variant = variant;
        this.color = color;
        this.year = year;
        this.kms = kms;
        this.owner_type = owner_type;
        this.reg_no = reg_no;
        this.vin_no = vin_no;
        this.fuel_type = fuel_type;
    }
}

class ExchangeEvaluatorRequest {
    constructor(name, user_id, date, activity_id, remarks) {
        this.name = name;
        this.user_id = user_id;
        this.date = date;
        this.activity_id = activity_id;
        this.remarks = remarks;
    }
}

class ExchangeInspection {
    constructor(under_insurance, doc_verified, vehicle_condition, vehicle_photos) {
        this.under_insurance = under_insurance;
        this.doc_verified = doc_verified;
        this.vehicle_condition = vehicle_condition;
        this.vehicle_photos = vehicle_photos;
    }
}

class ExchangePrice {
    constructor(expected_price, quoted_price) {
        this.expected_price = expected_price;
        this.quoted_price = quoted_price;
    }
}

module.exports = AddExchangeCarModel;