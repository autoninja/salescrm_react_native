package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 19-08-2016.
 */
public class DseDetails  extends RealmObject {

    public static final String DSEDETAILS = "DseDetails";

    @PrimaryKey
    private int dseDetailId;
    private int customerID;
    private String dseId="";
    private String dseName="";
    private String dseMobile="";


    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getDseDetailId() {
        return dseDetailId;
    }

    public void setDseDetailId(int dseDetailId) {
        this.dseDetailId = dseDetailId;
    }

    public String getDseId() {
        return dseId;
    }

    public void setDseId(String dseId) {
        this.dseId = dseId;
    }

    public String getDseName() {
        return dseName;
    }

    public void setDseName(String dseName) {
        this.dseName = dseName;
    }

    public String getDseMobile() {
        return dseMobile;
    }

    public void setDseMobile(String dseMobile) {
        this.dseMobile = dseMobile;
    }
}
