package com.salescrm.telephony.db.create_lead;

import io.realm.RealmObject;

/**
 * Created by bharath on 8/12/16.
 */

public class Car_details extends RealmObject {
    private int brand_id;
    private String fuel_type_id;

    private String color_id;

    private String model_id;

    private String variant_id;

    private boolean is_activity_target;

    private boolean is_primary;
    private String brand_name;
    private String model_name;
    private String variant_name;
    private String color_name;
    private String fuel_name;

//            private String no_of_car;

//            public String getNo_of_car() {
//                return no_of_car;
//            }

            /*public void setNo_of_car(int no_of_car) {
                this.no_of_car = String.valueOf(no_of_car);
            }*/

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public boolean is_activity_target() {
        return is_activity_target;
    }

    public boolean is_primary() {
        return is_primary;
    }

    public String getFuel_type_id() {
        return fuel_type_id;
    }

    public void setFuel_type_id(String fuel_type_id) {
        this.fuel_type_id = fuel_type_id;
    }

    public String getColor_id() {
        return color_id;
    }

    public void setColor_id(String color_id) {
        this.color_id = color_id;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public String getVariant_id() {
        return variant_id;
    }

    public void setVariant_id(String variant_id) {
        this.variant_id = variant_id;
    }

    public boolean getIs_activity_target() {
        return is_activity_target;
    }

    public void setIs_activity_target(boolean is_activity_target) {
        this.is_activity_target = is_activity_target;
    }

    public boolean getIs_primary() {
        return is_primary;
    }

    public void setIs_primary(boolean is_primary) {
        this.is_primary = is_primary;
    }

    @Override
    public String toString() {
        return "ClassPojo [fuel_type_id = " + fuel_type_id + ", color_id = " + color_id + ", model_id = " + model_id + ", variant_id = " + variant_id + ", is_activity_target = " + is_activity_target + ", is_primary = " + is_primary + "]";
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getVariant_name() {
        return variant_name;
    }

    public void setVariant_name(String variant_name) {
        this.variant_name = variant_name;
    }

    public String getColor_name() {
        return color_name;
    }

    public void setColor_name(String color_name) {
        this.color_name = color_name;
    }

    public String getFuel_name() {
        return fuel_name;
    }

    public void setFuel_name(String fuel_name) {
        this.fuel_name = fuel_name;
    }
}