package com.salescrm.telephony.fragments;


import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.TodayTasksSummaryMainPagerAdapter;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.TeamDashboardSwipedListener;
import com.salescrm.telephony.model.TeamDashboardSwiped;
import com.salescrm.telephony.model.TodaySummaryModel;
import com.salescrm.telephony.model.TodaySummaryResponse;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 14/11/17.
 */

public class TodayTasksSummaryMainFragment extends Fragment implements TeamDashboardSwipedListener {
    private RelativeLayout relLoader;
    private TextView tvTasksOrPending;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Preferences pref = null;
    private TodaySummaryModel todaySummaryModel;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view           = inflater.inflate(R.layout.fragment_today_tasks_summary_main,container, false);

        relLoader           = (RelativeLayout) view.findViewById(R.id.rel_loading_frame);
        tvTasksOrPending    = (TextView) view.findViewById(R.id.tv_pending_or_tasks_number);
        tabLayout           = (TabLayout) view.findViewById(R.id.tab_today_tasks_summary_main);
        viewPager           = (ViewPager) view.findViewById(R.id.view_pager_tasks_summary_main);

        swipeRefreshLayout  = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_today_summary);

        relLoader.setVisibility(View.GONE);
        pref = Preferences.getInstance();
        pref.load(getActivity());

        todaySummaryModel = TodaySummaryModel.getInstance();

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                changeTabStyle(tab.getPosition(), true);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                changeTabStyle(tab.getPosition(), false);

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                makeApiCall();

            }
        });

        initUi();
        makeApiCall();

        return view;
    }

    private void initUi() {
        if(DbUtils.isBike()) {
            view.findViewById(R.id.tv_visit_number).setVisibility(View.GONE);
            view.findViewById(R.id.tv_post_booking_number).setVisibility(View.GONE);
            view.findViewById(R.id.tv_delivery_number).setVisibility(View.GONE);

            TextView tvCall =  (TextView) view.findViewById(R.id.tv_call_number);
            TextView tvTD =  (TextView) view.findViewById(R.id.tv_test_drive_number);
            TextView tvTasks =  (TextView) view.findViewById(R.id.tv_pending_or_tasks_number);

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    20
            );
            if(DbUtils.isBike()){
                tvTD.setText("TR");
            }else{
                tvTD.setText("TD");
            }
            tvCall.setLayoutParams(param);
            tvTD.setLayoutParams(param);
            tvTasks.setLayoutParams(param);

        }
    }

    private void makeApiCall() {
        viewPager.setAdapter(null);
        relLoader.setVisibility(View.VISIBLE);
        //relAlert.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
        if(new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getTodaySummary(getTodayDate(), new Callback<TodaySummaryResponse>() {
                @Override
                public void success(TodaySummaryResponse todaySummaryResponse, Response response) {
                    relLoader.setVisibility(View.GONE);
                    if(todaySummaryResponse != null
                            && todaySummaryResponse.getStatusCode() != null
                            && todaySummaryResponse.getStatusCode() == WSConstants. RESPONSE_STATUS_OK
                            && todaySummaryResponse.getResult()!=null
                            && todaySummaryResponse.getResult().size()>0) {
                       // todaySummaryList.addAll(todaySummaryResponse.getResult());
                        todaySummaryModel.setResultList(todaySummaryResponse.getResult());
                        init();
                    }
                    else {
                        showErrorMessage("Error contacting server");
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showErrorMessage("Error contacting server");
                }
            });
        }

        else {
            showErrorMessage("No internet connections");
        }
    }

    private void showErrorMessage(String error) {

       relLoader.setVisibility(View.GONE);
       // tvAlert.setText(error);
        Util.showToast(getContext(),error +" while fetching Today's summary",Toast.LENGTH_SHORT);

    }

    private void init() {
        if(getActivity()==null || isRemoving() || isDetached()) {
            return;
        }

        tabLayout.removeAllTabs();
        boolean isUserTlOnly=false;

        List<String> userRoles = DbUtils.getRoles();
        if(userRoles.contains(WSConstants.ROLES.OPERATIONS)
                || userRoles.contains(WSConstants.ROLES.BRANCH_HEAD)
                || userRoles.contains(WSConstants.ROLES.MANAGER)) {
            System.out.println("User is branch head || user is sales manager");
            isUserTlOnly = false;
        }
        else if(userRoles.contains(WSConstants.ROLES.TEAM_LEAD) || userRoles.contains(WSConstants.ROLES.DSE)) {
            System.out.println("User is team lead");
            isUserTlOnly = true;
        }
        else {
            System.out.println("Doesn't belongs to any category");
        }


        if(todaySummaryModel.getResultList().size() <=1) {
            tabLayout.setVisibility(View.GONE);
        }
        else {
            for(int i=0; i<todaySummaryModel.getResultList().size(); i++) {
                TodaySummaryResponse.Result result = todaySummaryModel.getResultList().get(i);
                tabLayout.addTab(tabLayout.newTab().setText(result.getLocation_name()));

            }
            tabLayout.setVisibility(View.VISIBLE);
        }

        viewPager.setAdapter(new TodayTasksSummaryMainPagerAdapter(getChildFragmentManager(), isUserTlOnly));

        setColorForTabText(0);
        switchTabOnNotification();
    }

    private void switchTabOnNotification(){
        if(getActivity()!=null
                && getActivity().getIntent()!=null
                && getActivity().getIntent().getExtras()!=null
                && getActivity().getIntent().getExtras().getBoolean(WSConstants.OPEN_TASKS_FROM_NOTIFICATION_TRAY)
                && getActivity().getIntent().getExtras().getString(WSConstants.NOTIFICATION_CLICKED_NAME,"")
                .equalsIgnoreCase(WSConstants.FirebaseEvent.PENDING_ALERT)) {
            int position = 0;
            System.out.println("Location Id on Notifications"+getActivity()
                    .getIntent().getExtras().getInt(WSConstants.NOTIFICATION_LOCATION_ID,-1));
            for(int i=0; i<(todaySummaryModel==null?0:todaySummaryModel.getResultList().size());i++){
                if(todaySummaryModel.getResultList().get(i).getLocation_id()
                        == getActivity()
                        .getIntent().getExtras().getInt(WSConstants.NOTIFICATION_LOCATION_ID,-1)) {
                    position = i;
                    break;
                }

            }
            getActivity().getIntent().putExtra(WSConstants.NOTIFICATION_CLICKED_NAME,"");
            changeTabStyle(position, true);

        }
        else {
            changeTabStyle(0, true);
        }
    }

    private void setColorForTabText(int position) {
        if(position<0 || position>= tabLayout.getTabCount()) {
            return;
        }
        tabLayout.setTabTextColors(Color.GRAY, Color.GRAY);
    }

    private void changeTabStyle(int position, boolean select) {

        if(position<0 || position>= tabLayout.getTabCount()) {
            return;
        }

        AppCompatTextView tabText = ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0))
                .getChildAt(position)))).getChildAt(1)));
        if(select) {
            tabText.setTypeface(null, Typeface.BOLD);
            tabText.setTextColor(getResources().getColor(R.color.colorPrimary));
        } else {
            tabText.setTypeface(null, Typeface.NORMAL);
            tabText.setTextColor(Color.GRAY);
        }
    }

    private String getTodayDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    private TeamDashboardSwiped teamDashboardSwiped = TeamDashboardSwiped.getInstance(this,0);
    @Override
    public void onTeamDashboardSwiped(int position, int tabCount) {
        if(position == 0) {
            System.out.println("TeamDashboard Todays");
            /*if(Answers.getInstance()!=null) {
                Answers.getInstance().logCustom(new CustomEvent("Team Dashboard")
                        .putCustomAttribute("Dealer Name", pref.getDealerName())
                        .putCustomAttribute("IMEI", pref.getImeiNumber())
                        .putCustomAttribute("User Name", pref.getUserName())
                        .putCustomAttribute("Dashboard Type", "Today's Task")
                        .putCustomAttribute("Roles",   DbUtils.getRolesCombination()));

            }*/
            HashMap<String, Object> hashMap = new HashMap<String, Object>();
            hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_TODAY_TASKS);
            CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
