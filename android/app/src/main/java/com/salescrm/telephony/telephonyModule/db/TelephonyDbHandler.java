package com.salescrm.telephony.telephonyModule.db;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.LeadListDetails;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.GCMDialogResponse;
import com.salescrm.telephony.telephonyModule.interfaces.DbRecordSyncCountListener;
import com.salescrm.telephony.utils.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by bannhi on 15/3/17.
 */

public class TelephonyDbHandler {

    private static TelephonyDbHandler instance;
    private static TelephonyDB telephonyRecord;
    String dialog = "0";
    String numberToSearch = "";
    int count = 0;

    private TelephonyDbHandler() {

    }

    public static TelephonyDbHandler getInstance() {

        if (instance == null) {
            instance = new TelephonyDbHandler();
        }
        return instance;
    }

    /**
     * insert data of the recorded
     */
    public int insertIntoDB(Realm realm, int maxId, String uuId, String callId, String leadId, String typeOfCall, String calledNumber, String startDate, String endDate,
                            long duration, String callHangupCause, String filePath, String fileName, int fileSyncStatus,
                            int recordSyncStatus, int moduleType, String lastModifiedTime) {
        telephonyRecord = new TelephonyDB();
        int nextId = maxId + 1;

        System.out.println("NEW ID" + nextId);
        // insert new value
        telephonyRecord.setId(nextId);
        telephonyRecord.setUuId(uuId);
        telephonyRecord.setCallId(callId);
        telephonyRecord.setLeadId(leadId);
        telephonyRecord.setCall_type(typeOfCall);
        telephonyRecord.setCalledNumber(calledNumber);
        telephonyRecord.setStartTime(startDate);
        telephonyRecord.setEndTime(endDate);
        telephonyRecord.setDuration(duration);
        telephonyRecord.setCallHangUpCause(callHangupCause);
        telephonyRecord.setFilePath(filePath);
        telephonyRecord.setFileName(fileName);
        telephonyRecord.setFileCreatedDate(startDate);
        telephonyRecord.setFileSyncStatus(fileSyncStatus);
        telephonyRecord.setRecordSyncStatus(recordSyncStatus);
        telephonyRecord.setModuleType(moduleType);
        telephonyRecord.setLastModifiedTime(lastModifiedTime);
        realm.copyToRealmOrUpdate(telephonyRecord);
        return nextId;


    }

    /**
     * method to check the total number of unsynced records exist or not
     *
     * @return
     */
    public void dbRecordSyncCount(final DbRecordSyncCountListener listener) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(
                    new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    count = realm.where(TelephonyDB.class)
                            .equalTo("recordSyncStatus", 0)
                            .or()
                            .equalTo("fileSyncStatus", 0)
                            .findAll().size();
                    if(!realm.isClosed()) {
                        realm.close();
                    }
                    if (listener != null) {
                        listener.dbRecordSyncCountQueryResult(count);
                    }

                }
            });
        }
        catch (Exception ignored) {
                if(realm!=null && !realm.isClosed()) {
                    realm.close();
                }
        }
    }

    public void dbRecordCDRUnSyncedCount(final DbRecordSyncCountListener listener) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(
                    new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    count = realm.where(TelephonyDB.class)
                            .equalTo("recordSyncStatus", 0)
                            .findAll().size();
                    if(!realm.isClosed()) {
                        realm.close();
                    }
                    if (listener != null) {
                        listener.dbRecordSyncCountQueryResult(count);
                    }

                }
            });
        }
        catch (Exception ignored) {
                if(realm!=null && !realm.isClosed()) {
                    realm.close();
                }
        }
    }
    private int getTotal(Realm realm) {
        int total = 0;
        try{
            total = realm.where(TelephonyDB.class).findAll().size();

        }
        catch (Exception e) {

        }
        return total;
    }


    public int getFileSyncPercentage(Realm realm) {

        int total= getTotal(realm);
        int fileSynced = 0;
        try{
            fileSynced = realm.where(TelephonyDB.class)
                    .equalTo("fileSyncStatus", 1)
                    .findAll().size();
        }
        catch (Exception e) {

        }
        System.out.println("Total : "+total);
        System.out.println("File Synced : "+fileSynced);
        if(total == 0) {
            return 100;
        }
        return (100*fileSynced / total);
    }

    public int getCDRSyncPercentage(Realm realm) {

        int total= getTotal(realm);
        int cdrSynced = 0;
        try{
            cdrSynced = realm.where(TelephonyDB.class)
                    .equalTo("recordSyncStatus", 1)
                    .findAll().size();
        }
        catch (Exception e) {

        }
        System.out.println("Total : "+total);
        System.out.println("CDR Synced : "+cdrSynced);
        if(total == 0) {
            return 100;
        }
        return (100*cdrSynced / total);
    }


    /**
     * setting file sync status
     *
     * @param realm
     * @param uuId
     */
    public void updateFileSyncStatus(Realm realm, String uuId, int status) {
        telephonyRecord = realm.where(TelephonyDB.class)
                .equalTo("uuId", uuId)
                .findFirst();

        if (telephonyRecord != null) {
            telephonyRecord.setFileSyncStatus(status);
        }


    }

    /**
     * @param realm
     * @param uuId
     */
    public void deleteRecordFromDB(Realm realm, String uuId) {
        telephonyRecord = realm.where(TelephonyDB.class)
                .equalTo("uuId", uuId)
                .findFirst();

        if (telephonyRecord != null) {
            telephonyRecord.deleteFromRealm();
        }


    }

    /**
     * @param id
     * @param recordSyncStatus
     */
    public void updateAfterSync(Realm realm, int id, int recordSyncStatus) {

        telephonyRecord = realm.where(TelephonyDB.class)
                .equalTo("id", id)
                .findFirst();

        if (telephonyRecord != null) {
            telephonyRecord.setRecordSyncStatus(recordSyncStatus);
            System.out.println("UPDATED: " + id);
        }
    }

    /**
     * deleting 7 days old records
     */
    public void delete7DaysOldRecords() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Date compareDate = null;
                    String dateString = Util.getSevenDaysDateString();
                    try {
                        compareDate = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss").parse(dateString);
                    } catch (ParseException e) {
                        compareDate = new Date();
                        e.printStackTrace();
                    }
                    RealmResults<TelephonyDB> telephonyRecords = realm.where(TelephonyDB.class)
                            .equalTo("fileSyncStatus", 1)
                            .equalTo("recordSyncStatus", 1)
                            .findAll();

                    if (telephonyRecords != null) {
                        for (TelephonyDB temp : telephonyRecords) {
                            Date dbDate = null;
                            try {
                                dbDate = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss").parse(temp.getLastModifiedTime());
                            } catch (ParseException e) {
                                dbDate = new Date();
                                e.printStackTrace();
                            }
                            System.out.println("DB DATE" + dbDate + "CURRENT DATE" + compareDate);
                            if ((dbDate.before(compareDate) || dbDate.equals(compareDate)) && temp.isValid()) {
                                temp.deleteFromRealm();
                                System.out.println("RECORD DELETED");
                            }

                        }

                    }

                }
            });
            if(!realm.isClosed()){
                realm.close();
            }
        }
        catch (Exception ignored) {
            if(realm!=null && !realm.isClosed()){
                realm.close();
            }

        }

    }

    /**
     * delete records with null entries
     */
    public void deleteNullRecords() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<TelephonyDB> telephonyRecords = realm.where(TelephonyDB.class)
                            .equalTo("fileSyncStatus", 1)
                            .equalTo("recordSyncStatus", 1)
                            .equalTo("lastModifiedTime", "")
                            .findAll();
                    if (telephonyRecords != null) {
                        for (TelephonyDB temp : telephonyRecords) {
                            temp.deleteFromRealm();
                        }

                    }
                    if(!realm.isClosed()){
                        realm.close();
                    }


                }
            });
        }
        catch (Exception ignored) {
            if(realm!=null && !realm.isClosed()){
                realm.close();
            }
        }

    }

    /**
     * delete where records have empty file names
     */
    public void deleteFileEmptyRecord() {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<TelephonyDB> telephonyRecords = realm.where(TelephonyDB.class)
                            .equalTo("fileName", "")
                            .findAll();
                    if (telephonyRecords != null) {
                        for (TelephonyDB temp : telephonyRecords) {
                            temp.deleteFromRealm();
                        }

                    }
                    if(!realm.isClosed()){
                        realm.close();
                    }
                }
            });
        }
        catch (Exception e) {
            if(realm!=null && !realm.isClosed()){
                realm.close();
            }
        }
    }


    /**
     * return records in the recent order
     *
     * @param realm
     * @return
     */
    public RealmResults queryLastEvents(Realm realm) {
        RealmResults<TelephonyDB> telephonyRecords = realm.where(TelephonyDB.class)
                .findAllSorted("id", Sort.DESCENDING);

        return telephonyRecords;

    }

    /**
     * return record with the given mobile number
     *
     * @param
     * @return
     */
    public String getLeadFromMobileNumber(String mobileNumber, final Context context) {
        dialog = "0";
        if (mobileNumber == null) {
            return "";
        }
        if (mobileNumber.startsWith("0")) {
            numberToSearch = mobileNumber.substring(1);
        } else if (mobileNumber.contains("+91")) {
            numberToSearch = mobileNumber.substring(3);
        } else {
            numberToSearch = mobileNumber;
        }

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                dialog = "0";
                Preferences pref = Preferences.getInstance();
                pref.load(context);

                LeadListDetails leadListRecord = realm.where(LeadListDetails.class)
                        .contains("number", numberToSearch)
                        //.notEqualTo("stage_id","9")
                        .findFirst();

                if (leadListRecord != null) {
                    dialog = leadListRecord.getFirst_name() + leadListRecord.getLast_name() + "(Lead id: " + leadListRecord.getLead_id() + ")";

                    /**
                     * code that can be added if required
                     */
                   /* if (!leadListRecord.getLead_id().equalsIgnoreCase("0")) {
                        pref.setmTelephonyLeadID("" + leadListRecord.getLead_id());
                    }*/

                }else{
                    SalesCRMRealmTable record = realm.where(SalesCRMRealmTable.class)
                            .contains("customerNumber", numberToSearch)
                            .findFirst();
                    if(record!=null){
                        dialog = record.getFirstName() + record.getLastName() + "(Lead id: " + record.getLeadId() + ")";
                        /**
                         * code that can be added if required
                         */
                      /*  if (record.getLead_id() >= 0) {
                            pref.setmTelephonyLeadID("" + record.getLead_id());
                        }*/
                    }
                }
                System.out.println("TELEPHONY DB HANDLER PREF LEAD ID SET" + pref.getmTelephonyLeadID());
                GCMDialogResponse gcmDialogResponse = new GCMDialogResponse();
                gcmDialogResponse.setDialog(dialog);
                SalesCRMApplication.getBus().post(gcmDialogResponse);
               // realm.close();
            }
        });
        realm.close();
        return dialog;

    }

}
