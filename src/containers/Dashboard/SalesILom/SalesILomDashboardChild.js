import React, { Component } from 'react'
import { Platform, View, Text, Image, TouchableOpacity, YellowBox, Dimensions, FlatList, ActivityIndicator } from 'react-native'
import PercentageSwitch from '../../../components/uielements/PercentageSwitch'

import SalesILomItem from '../../../components/salesILom/sales_ilom_item'
import SalesILomItemChild from '../../../components/salesILom/sales_ilom_item_child'
import SalesILomItemExpandable from '../../../components/salesILom/sales_ilom_item_expandable'
import SalesILomResultItem from '../../../components/salesILom/sales_ilom_result_item'
import SalesILomHeader from '../../../components/salesILom/sales_ilom_header'
import Toast from 'react-native-easy-toast'

import { eventTeamDashboard } from '../../../clevertap/CleverTapConstants';
import CleverTapPush from '../../../clevertap/CleverTapPush'

export default class SalesILomChildDashboard extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title', 'Team Details'),
    };
  };
  constructor(props) {

    super(props);


    this.state = {
      show_percentage: false,
      result: {}, location_selected: this.props.navigation.getParam('location_selected', 0)
    }


    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);

  }

  componentDidMount() {
    CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrTeams });
  }

  leadSourceCarModelWiseETVBR(navigation, name, dp_url, location_id, user_id, role_id) {
    navigation.navigate('LeadSourceCarModelDistribution',
      {
        name,
        dp_url,
        start_date: global.global_filter_date.startDateStr,
        end_date: global.global_filter_date.endDateStr,
        location_id,
        user_id,
        role_id
      })
    console.log('leadSourceCarModelWiseETVBR');
  }
  _showDetailsOnSalesManager(navigation, title, sales_manager, from, location_id) {
    navigation.push('SalesILomDashboardChild', {
      title,
      sales_manager,
      from,
      location_id,
      location_selected: this.state.location_selected
    });
    console.log("_showDetailsOnSalesManager");
  }

  _etvbrLongPress(navigation, from, data) {
    if (from === 'user_info') {
      navigation.navigate('UserInfo', {
        name: data.info.name,
        dp_url: data.info.dp_url

      });
    }
    else if (from == 'etvbr_details') {
      navigation.navigate('ETVBRDetails', {
        data
      });
    }

    console.log("_etvbrLongPress" + from);
    console.log(data);
  }

  render() {

    const { navigation } = this.props;

    const from = navigation.getParam('from');

    const location = navigation.getParam('location', null);

    const sales_managers = location ? location.sales_managers : null;

    //For sales_manager click
    const salesManager = navigation.getParam('sales_manager');
    const teamLeaders = salesManager ? salesManager.team_leaders : null;

    const parent_role_id = navigation.getParam('parent_role_id');

    const location_id = navigation.getParam('location_id');



    let list;
    console.log('from:' + from + this.state.show_percentage);
    if (from == 'location') {

      list =
        <FlatList
          showsVerticalScrollIndicator={false}
          extraData={this.state}
          data={sales_managers}
          renderItem={({ item, index }) => {

            let mainView = <TouchableOpacity onPress={this._showDetailsOnSalesManager.bind(this, this.props.navigation, item.info.name + '\'s Team', item, 'sales_manager', location_id)}>
              <SalesILomItem
                leadSourceCarModelWiseETVBR=
                {this.leadSourceCarModelWiseETVBR.bind(this,
                  navigation,
                  item.info.name,
                  item.info.dp_url,
                  location_id,
                  item.info.id,
                  item.info.user_role_id)}
                onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                onPress={this._showDetailsOnSalesManager.bind(this, this.props.navigation, item.info.name + '\'s Team', item, 'sales_manager', location_id)}
                showPercentage={this.state.show_percentage} location={item}
                call_stats = {
                  {
                    call_duration: item.info.abs.call_duration,
                    calls_contacted: item.info.abs.calls_contacted,
                    calls_attempted: item.info.abs.calls_attempted
                  }
                }
                abs={
                  {
                    e: item.info.abs.enquiries,
                    t: item.info.abs.tdrives,
                    v: item.info.abs.visits,
                    b: item.info.abs.bookings,
                    r: item.info.abs.retails,
                    l: item.info.abs.lost_drop,
                    assigned: item.info.abs.enquiries,
                    callback: item.info.abs.tdrives,
                    prospect: item.info.abs.visits,
                    sale_confirmed: item.info.abs.bookings,
                    il_done: item.info.abs.retails,
                  }}

                rel={
                  {
                    e: item.info.rel.enquiries,
                    t: item.info.rel.tdrives,
                    v: item.info.rel.visits,
                    b: item.info.rel.bookings,
                    r: item.info.rel.retails,
                    l: item.info.rel.lost_drop,
                    assigned: item.info.rel.enquiries,
                    callback: item.info.rel.tdrives,
                    prospect: item.info.rel.visits,
                    sale_confirmed: item.info.rel.bookings,
                    il_done: item.info.rel.retails,

                  }}


                info={
                  {
                    dp_url: item.info.dp_url,
                    name: item.info.name,
                    user_id: item.info.id,
                    location_id: location_id,
                    user_role_id: item.info.user_role_id,
                  }
                }

                targets={
                  {
                    e: item.info.targets.enquiries,
                    t: item.info.targets.tdrives,
                    v: item.info.targets.visits,
                    b: item.info.targets.bookings,
                    r: item.info.targets.retails,
                    l: item.info.targets.lost_drop,
                  }
                }

              ></SalesILomItem>
            </TouchableOpacity>;

            if (index == sales_managers.length - 1) {
              return (<View>
                {mainView}

                <SalesILomResultItem
                  showPercentage={this.state.show_percentage}
                  onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                  call_stats = {
                    {
                      call_duration: location.location_abs.call_duration,
                      calls_contacted: location.location_abs.calls_contacted,
                      calls_attempted: location.location_abs.calls_attempted
                    }
                  }
                  abs={
                    {
                      e: location.location_abs.enquiries,
                      t: location.location_abs.tdrives,
                      v: location.location_abs.visits,
                      b: location.location_abs.bookings,
                      r: location.location_abs.retails,
                      l: location.location_abs.lost_drop,
                      assigned: location.location_abs.enquiries,
                      callback: location.location_abs.tdrives,
                      prospect: location.location_abs.visits,
                      sale_confirmed: location.location_abs.bookings,
                      il_done: location.location_abs.retails,
                    }
                  }

                  rel={
                    {
                      e: location.location_rel.enquiries,
                      t: location.location_rel.tdrives,
                      v: location.location_rel.visits,
                      b: location.location_rel.bookings,
                      r: location.location_rel.retails,
                      l: location.location_rel.lost_drop,
                      assigned: location.location_rel.enquiries,
                      callback: location.location_rel.tdrives,
                      prospect: location.location_rel.visits,
                      sale_confirmed: location.location_rel.bookings,
                      il_done: location.location_rel.retails,
                    }
                  }

                  info={
                    {
                      user_id: global.appUserId,
                      name: 'All SM',
                      location_id: location_id,
                      user_role_id: parent_role_id,
                    }
                  }
                  targets={
                    {
                      e: location.location_targets.enquiries,
                      t: location.location_targets.tdrives,
                      v: location.location_targets.visits,
                      b: location.location_targets.bookings,
                      r: location.location_targets.retails,
                      l: location.location_targets.lost_drop,
                    }
                  }

                ></SalesILomResultItem>

              </View>);
            }
            else {
              return (mainView);
            }


          }}
          keyExtractor={(item, index) => index + ''}
        />
    }
    else if (from == 'mutilple_location_tl') {
      //show scs for location tl
      console.log("MulipleLocationTL")
      let scData = [];
      if (sales_managers && sales_managers[0].team_leaders) {
        scData = sales_managers[0].team_leaders[0];
      }
      list =
        <FlatList
          showsVerticalScrollIndicator={false}
          data={scData.sales_consultants}
          renderItem={({ item, index }) => {

            let mainView = <TouchableOpacity >

              <SalesILomItemChild
                leadSourceCarModelWiseETVBR=
                {this.leadSourceCarModelWiseETVBR.bind(this,
                  navigation,
                  item.info.name,
                  item.info.dp_url,
                  location_id,
                  item.info.id,
                  item.info.user_role_id)}
                onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                showPercentage={this.state.show_percentage}
                call_stats = {
                  {
                    call_duration: item.info.abs.call_duration,
                    calls_contacted: item.info.abs.calls_contacted,
                    calls_attempted: item.info.abs.calls_attempted
                  }
                }
                abs={
                  {
                    e: item.info.abs.enquiries,
                    t: item.info.abs.tdrives,
                    v: item.info.abs.visits,
                    b: item.info.abs.bookings,
                    r: item.info.abs.retails,
                    l: item.info.abs.lost_drop,
                    assigned: item.info.abs.enquiries,
                    callback: item.info.abs.tdrives,
                    prospect: item.info.abs.visits,
                    sale_confirmed: item.info.abs.bookings,
                    il_done: item.info.abs.retails,
                  }
                }
                rel={
                  {
                    e: item.info.rel.enquiries,
                    t: item.info.rel.tdrives,
                    v: item.info.rel.visits,
                    b: item.info.rel.bookings,
                    r: item.info.rel.retails,
                    l: item.info.rel.lost_drop,
                    assigned: item.info.rel.enquiries,
                    callback: item.info.rel.tdrives,
                    prospect: item.info.rel.visits,
                    sale_confirmed: item.info.rel.bookings,
                    il_done: item.info.rel.retails,
                  }
                }
                info={
                  {
                    dp_url: item.info.dp_url,
                    name: item.info.name,
                    user_id: item.info.id,
                    location_id,
                    user_role_id: item.info.user_role_id,
                  }
                }
                targets={
                  {
                    e: item.info.targets.enquiries,
                    t: item.info.targets.tdrives,
                    v: item.info.targets.visits,
                    b: item.info.targets.bookings,
                    r: item.info.targets.retails,
                    l: item.info.targets.lost_drop,
                  }
                }></SalesILomItemChild>
            </TouchableOpacity>;

            if (index == scData.sales_consultants.length - 1) {
              return (<View>
                {mainView}
                <SalesILomResultItem
                  onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                  showPercentage={this.state.show_percentage}
                  call_stats = {
                    {
                      call_duration: scData.info.abs.call_duration,
                      calls_contacted: scData.info.abs.calls_contacted,
                      calls_attempted: scData.info.abs.calls_attempted
                    }
                  }
                  abs={
                    {
                      e: scData.info.abs.enquiries,
                      t: scData.info.abs.tdrives,
                      v: scData.info.abs.visits,
                      b: scData.info.abs.bookings,
                      r: scData.info.abs.retails,
                      l: scData.info.abs.lost_drop,
                      assigned: scData.info.abs.enquiries,
                      callback: scData.info.abs.tdrives,
                      prospect: scData.info.abs.visits,
                      sale_confirmed: scData.info.abs.bookings,
                      il_done: scData.info.abs.retails,
                    }
                  }
                  rel={
                    {
                      e: scData.info.rel.enquiries,
                      t: scData.info.rel.tdrives,
                      v: scData.info.rel.visits,
                      b: scData.info.rel.bookings,
                      r: scData.info.rel.retails,
                      l: scData.info.rel.lost_drop,
                      assigned: scData.info.rel.enquiries,
                      callback: scData.info.rel.tdrives,
                      prospect: scData.info.rel.visits,
                      sale_confirmed: scData.info.rel.bookings,
                      il_done: scData.info.rel.retails,
                    }
                  }
                  info={
                    {
                      user_id: scData.info.id,
                      name: 'Team\'s Data',
                      location_id: location_id,
                      user_role_id: scData.info.user_role_id,
                    }
                  }
                  targets={
                    {
                      e: scData.info.targets.enquiries,
                      t: scData.info.targets.tdrives,
                      v: scData.info.targets.visits,
                      b: scData.info.targets.bookings,
                      r: scData.info.targets.retails,
                      l: scData.info.targets.lost_drop,
                    }
                  }

                ></SalesILomResultItem>
              </View>);
            }
            else {
              return (mainView);
            }


          }}
          keyExtractor={(item, index) => index + ''}
        />
    }
    else if (from == 'sales_manager') {
      list = <FlatList
        showsVerticalScrollIndicator={false}
        extraData={this.state}
        data={teamLeaders}
        renderItem={({ item, index }) => {

          let mainViewExpandable = <SalesILomItemExpandable
            leadSourceCarModelWiseETVBRInner={
              this.leadSourceCarModelWiseETVBR.bind(this, navigation)
            }
            leadSourceCarModelWiseETVBR=
            {this.leadSourceCarModelWiseETVBR.bind(this,
              navigation,
              item.info.name,
              item.info.dp_url,
              location_id,
              item.info.id,
              item.info.user_role_id)}
            onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
            showPercentage={this.state.show_percentage}
            team_leader={item} 
            call_stats = {
              {
                call_duration: item.info.abs.call_duration,
                calls_contacted: item.info.abs.calls_contacted,
                calls_attempted: item.info.abs.calls_attempted
              }
            }
            abs={
              {
                e: item.info.abs.enquiries,
                t: item.info.abs.tdrives,
                v: item.info.abs.visits,
                b: item.info.abs.bookings,
                r: item.info.abs.retails,
                l: item.info.abs.lost_drop,
                assigned: item.info.abs.enquiries,
                callback: item.info.abs.tdrives,
                prospect: item.info.abs.visits,
                sale_confirmed: item.info.abs.bookings,
                il_done: item.info.abs.retails,

              }}

            rel={
              {
                e: item.info.rel.enquiries,
                t: item.info.rel.tdrives,
                v: item.info.rel.visits,
                b: item.info.rel.bookings,
                r: item.info.rel.retails,
                l: item.info.rel.lost_drop,
                assigned: item.info.rel.enquiries,
                callback: item.info.rel.tdrives,
                prospect: item.info.rel.visits,
                sale_confirmed: item.info.rel.bookings,
                il_done: item.info.rel.retails,

              }}

            info={
              {
                dp_url: item.info.dp_url,
                name: item.info.name,
                user_id: item.info.id,
                location_id: location_id,
                user_role_id: item.info.user_role_id,
              }
            }

            targets={
              {
                e: item.info.targets.enquiries,
                t: item.info.targets.tdrives,
                v: item.info.targets.visits,
                b: item.info.targets.bookings,
                r: item.info.targets.retails,
                l: item.info.targets.lost_drop,
              }
            }

          ></SalesILomItemExpandable>;

          if (index == teamLeaders.length - 1) {
            return (<View>
              {mainViewExpandable}

              <SalesILomResultItem
                onLongPress={this._etvbrLongPress.bind(this, this.props.navigation)}
                showPercentage={this.state.show_percentage}
                call_stats = {
                  {
                    call_duration: salesManager.info.abs.call_duration,
                    calls_contacted: salesManager.info.abs.calls_contacted,
                    calls_attempted: salesManager.info.abs.calls_attempted
                  }
                }
                abs={
                  {
                    e: salesManager.info.abs.enquiries,
                    t: salesManager.info.abs.tdrives,
                    v: salesManager.info.abs.visits,
                    b: salesManager.info.abs.bookings,
                    r: salesManager.info.abs.retails,
                    l: salesManager.info.abs.lost_drop,
                    assigned: salesManager.info.abs.enquiries,
                    callback: salesManager.info.abs.tdrives,
                    prospect: salesManager.info.abs.visits,
                    sale_confirmed: salesManager.info.abs.bookings,
                    il_done: salesManager.info.abs.retails,
                  }
                }
                rel={
                  {
                    e: salesManager.info.rel.enquiries,
                    t: salesManager.info.rel.tdrives,
                    v: salesManager.info.rel.visits,
                    b: salesManager.info.rel.bookings,
                    r: salesManager.info.rel.retails,
                    l: salesManager.info.rel.lost_drop,
                    assigned: salesManager.info.rel.enquiries,
                    callback: salesManager.info.rel.tdrives,
                    prospect: salesManager.info.rel.visits,
                    sale_confirmed: salesManager.info.rel.bookings,
                    il_done: salesManager.info.rel.retails,
                  }}
                info={
                  {
                    user_id: salesManager.info.id,
                    name: salesManager.info.name + '\'s Data',
                    location_id: location_id,
                    user_role_id: salesManager.info.user_role_id,
                  }
                }
                targets={
                  {
                    e: salesManager.info.targets.enquiries,
                    t: salesManager.info.targets.tdrives,
                    v: salesManager.info.targets.visits,
                    b: salesManager.info.targets.bookings,
                    r: salesManager.info.targets.retails,
                    l: salesManager.info.targets.lost_drop,
                  }
                }

              ></SalesILomResultItem>

            </View>);
          }
          else {
            return (mainViewExpandable);
          }


        }}
        keyExtractor={(item, index) => index + ''}
      />
    }

    return (
      <View style={{
        flex: 1,
        paddingTop: (Platform.OS) === 'ios' ? 20 : 10,
        backgroundColor: '#fff'
      }} >
        <Toast
          ref="toast"
          position='top'
          opacity={0.8}
        />

        <View style={{
          flex: 1,
          paddingBottom: 10, paddingLeft: 10, paddingRight: 10,
        }} >
          <View style={{
            flexDirection: 'row',
            height: 30,
          }}>
            <View style={{ flex: 1 / 2, height: 30, flexDirection: 'row', justifyContent: 'flex-start' }}>
              <View style={{ backgroundColor: '#F4F4F4', borderWidth: 1, borderColor: '#D0D8E4', borderRadius: 6, justifyContent: 'center' }}>
                <Text style={{ fontSize: 14, padding: 4, textAlign: 'center', color: '#1B143C' }}>{date}</Text>
              </View>
            </View>

            <View style={{ flexDirection: 'row', height: 30, flex: 1 / 2, justifyContent: 'flex-end', }} >
              <PercentageSwitch
                value={this.state.show_percentage}
                onValueChange={(value) => {
                  this.setState({ show_percentage: value });
                  if (value) {
                    CleverTapPush.pushEvent(eventTeamDashboard.event, { 'Dashboard Type': eventTeamDashboard.keyType.valuesType.etvbrPercentage });
                  }
                }}
              />
            </View>

          </View>
          <SalesILomHeader showPercentage={this.state.show_percentage} />
          {list}
        </View>
      </View>
    );
  }
}
