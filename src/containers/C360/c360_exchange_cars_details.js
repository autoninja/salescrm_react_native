import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback, ScrollView
} from 'react-native'
//import UserInfoService from '../../storage/user_info_service'


const styles = StyleSheet.create({
    top_view: {
        alignItems: 'center',
    },
    inner_main_block: {
        backgroundColor: "#ffffff",
        alignSelf: 'stretch',
        padding: 5,
        borderRadius: 2,
        marginRight: 10,
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 10,
    },
    view_straight_line: {
        width: '100%',
        height: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#a9a9a9',
        marginBottom: 10,
        marginTop: 10
    },
    car_name_title: {
        flex: 8,
        fontSize: 14,
        color: '#000000',
    },
    stage_name: {
        fontSize: 14,
        color: '#a9a9a9',
        marginLeft: 10
    },
    booking_detail: {
        width: 120,
        height: 30,
        padding: 5,
        borderRadius: 5,
        borderColor: '#2196f3',
        borderWidth: 1,
        margin: 5,
        alignItems: 'center',
        alignSelf: 'center'
    },
    test_drive: {
        backgroundColor: '#48b04b',
        padding: 5,
        borderRadius: 5,
        borderColor: '#48b04b',
        borderWidth: 1,
        marginRight: 10,
        marginBottom: 5,
        marginTop: 5,
    },
    button_next_stage: {
        backgroundColor: '#2196f3',
        padding: 5,
        alignSelf: 'center',
        borderColor: '#2196f3',
        marginRight: 10,
        marginBottom: 5,
        marginTop: 5,
    },
    button_invalid: {
        backgroundColor: '#808080',
        padding: 5,
        alignSelf: 'center',
        borderColor: '#2196f3',
        marginRight: 10,
        marginBottom: 5,
        marginTop: 5,
    },
    booking_id: {
        fontSize: 14,
        alignItems: 'flex-end',
        margin: 10
    },
    booking_tag: {
        flexDirection: 'row',
        alignContent: 'center',
    }
});

const USER_ROLE_EVALUATOR = 28;
const USER_ROLE_EVALUATOR_MANAGER = 31;
export default class ExchangeCarsDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //interestedCarsDetails : this.props.interestedCarsDetails,
            //roles: UserInfoService.getRoles(),
            roles: this.props.userRoles,
            exchangeCarsDetails: this.props.exchangeCarsDetails,
            reload: false
        }

    }

    isUserEvaluatorOrEvaluatorManager() {
        let { roles } = this.state;
        for (var i = 0; i < roles.length; i++) {
            if (roles[i].id == USER_ROLE_EVALUATOR || roles[i].id == USER_ROLE_EVALUATOR_MANAGER) {
                return true;
            }
        }
        return false;
    }
    getUserRole() {
        let { roles } = this.state;
        let role = 0;
        for (var i = 0; i < roles.length; i++) {
            if (roles[i].id == USER_ROLE_EVALUATOR || roles[i].id == USER_ROLE_EVALUATOR_MANAGER) {
                if (roles[i].id > role) {
                    role = roles[i].id
                }
            }
        }
        return role;
    }

    isEnable(item) {
        //console.error(this.props.exchangeCarsDetails)
        try {
            if (!this.isUserEvaluatorOrEvaluatorManager() || !item.activityDetails.activity
                || (this.getUserRole() == USER_ROLE_EVALUATOR && item.activityDetails.activity == 11)) {
                return false;
            } else {
                return true
            }
        } catch (e) {
            return false;
            //this.setState({ reload: true })
        }

    }

    render() {
        let { exchangeCarsDetails } = this.state;
        let { roles } = this.state;
        //console.error(this.isUserEvaluatorOrEvaluatorManager(), exchangeCarsDetails.activityDetails.activity, USER_ROLE_EVALUATOR)
        return (
            <View style={styles.top_view}>
                {(exchangeCarsDetails) ? exchangeCarsDetails.map((item, index) => {
                    return (
                        <View style={styles.inner_main_block} key={index}>
                            <View style={{ flexDirection: 'row', marginBottom: 5, marginLeft: 5, alignContent: 'center' }}>

                                <View style={{ flexDirection: 'column', flex: 1 }}>
                                    <Text style={styles.car_name_title}>{item.name}</Text>
                                    <Text style={{ fontSize: 12 }}>{(item.kms_run) ? item.kms_run + ", " : ""}Owner Type: {item.owner_type}</Text>
                                </View>
                                <TouchableOpacity style={styles.booking_detail} onPress={() => this._openExchangeDetail(item)}>
                                    <Text style={{ color: '#2196f3', fontSize: 12, alignSelf: 'center' }}>Exchange Details</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.view_straight_line} />


                            <TouchableOpacity disabled={!this.isEnable(item)} style={(this.isEnable(item)) ? styles.button_next_stage : styles.button_invalid} onPress={() => (this.isEnable(item)) ? this.markNextActivity(item) : null}>
                                <Text style={{ fontSize: 16, color: "#ffffff", alignSelf: 'center' }}>{
                                    (item.activityDetails && item.activityDetails.activity != null && item.activityDetails.activity == 10) ? "Old Car Evaluation" :
                                        (item && item.activityDetails && item.activityDetails.activity != null && item.activityDetails.activity == 11) ? "Manager's Evaluation"
                                            : "Done"}
                                </Text>
                            </TouchableOpacity>

                        </View>
                    )
                }) : null}
            </View>
        )
    }

    markNextActivity(item) {
        this.props.openMarkExchangeCarActivityOption(item)
    }

    _openExchangeDetail(item) {
        this.props.openUpdateExchangeDetail()
    }
}
