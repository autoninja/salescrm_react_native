package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bharath on 29/8/17.
 */

public class CustomerTypeDB extends RealmObject {
    private String id;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
