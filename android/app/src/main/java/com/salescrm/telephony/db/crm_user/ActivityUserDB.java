package com.salescrm.telephony.db.crm_user;

import io.realm.RealmObject;

/**
 * Created by bharath on 10/12/16.
 */

public class ActivityUserDB  extends RealmObject{
    private String id;

    private String activity_id;

    private String role_id;

    private String name;

    private String location_id;

    private String lead_source_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getLead_source_id() {
        return lead_source_id;
    }

    public void setLead_source_id(String lead_source_id) {
        this.lead_source_id = lead_source_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", activity_id = " + activity_id + ", role_id = " + role_id + ", name = " + name + ", location_id = " + location_id + ", lead_source_id = " + lead_source_id + "]";
    }
}
