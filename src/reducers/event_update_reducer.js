import { UPDATE_EVENT_DASHBOARD }  from '../actions/event_update_types'

const initialState = {
    update: false
  };
  
const eventUpdateReducer = (state = initialState, action) => {
    switch(action.type) {
      case UPDATE_EVENT_DASHBOARD:
        return {
          ...state,
          update: action.payload.update,
        };
      default:
        return state;
    }
  }
  
export default eventUpdateReducer;