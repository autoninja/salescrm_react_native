package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by Ravindra P on 14-10-2016.
 */

public class FormObjectAnswerChildren extends RealmObject {


    private String fAnsId;
    private String displayText;
    private String answerValue;


    public String getfAnsId() {
        return fAnsId;
    }

    public void setfAnsId(String fAnsId) {
        this.fAnsId = fAnsId;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public String getAnswerValue() {
        return answerValue;
    }

    public void setAnswerValue(String answerValue) {
        this.answerValue = answerValue;
    }
}
