package com.salescrm.telephony.response;

import java.util.List;

/**
 * Created by bannhi on 1/2/17.
 */

public class InterestedCarEditPrimaryResponse {
    private String statusCode;

    private String message;

    private List<Result> result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public List<Result> getResult ()
    {
        return result;
    }

    public void setResult (List<Result> result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private String fuel_type;

        private String model;

        private List<CarDmsData> carDmsData;

        private String model_id;

        private List <Car_activity_groups> car_activity_groups;

        private String lead_car_id;

        private String name;

        private String variant_id;

        private String brand;

        private String is_primary;

        private String variant_code;

        private String variant;

        private String color_id;

        private String color;

        public String getColor_id() {
            return color_id;
        }

        public void setColor_id(String color_id) {
            this.color_id = color_id;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getFuel_type ()
        {
            return fuel_type;
        }

        public void setFuel_type (String fuel_type)
        {
            this.fuel_type = fuel_type;
        }

        public String getModel ()
        {
            return model;
        }

        public void setModel (String model)
        {
            this.model = model;
        }

       /* public List<String> getCarDmsData ()
        {
            return carDmsData;
        }

        public void setCarDmsData (List<String> carDmsData)
        {
            this.carDmsData = carDmsData;
        }*/
        public List<CarDmsData> getCarDmsData() {
            return carDmsData;
        }

        public void setCarDmsData(List<CarDmsData> carDmsData) {
            this.carDmsData = carDmsData;
        }


        public String getModel_id ()
        {
            return model_id;
        }

        public void setModel_id (String model_id)
        {
            this.model_id = model_id;
        }

        public List<Car_activity_groups> getCar_activity_groups ()
        {
            return car_activity_groups;
        }

        public void setCar_activity_groups (List <Car_activity_groups> car_activity_groups)
        {
            this.car_activity_groups = car_activity_groups;
        }

        public String getLead_car_id ()
        {
            return lead_car_id;
        }

        public void setLead_car_id (String lead_car_id)
        {
            this.lead_car_id = lead_car_id;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getVariant_id ()
        {
            return variant_id;
        }

        public void setVariant_id (String variant_id)
        {
            this.variant_id = variant_id;
        }

        public String getBrand ()
        {
            return brand;
        }

        public void setBrand (String brand)
        {
            this.brand = brand;
        }

        public String getIs_primary ()
        {
            return is_primary;
        }

        public void setIs_primary (String is_primary)
        {
            this.is_primary = is_primary;
        }

        public String getVariant_code ()
        {
            return variant_code;
        }

        public void setVariant_code (String variant_code)
        {
            this.variant_code = variant_code;
        }

        public String getVariant ()
        {
            return variant;
        }

        public void setVariant (String variant)
        {
            this.variant = variant;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [fuel_type = "+fuel_type+", model = "+model+", carDmsData = "+carDmsData+", model_id = "+model_id+", car_activity_groups = "+car_activity_groups+", lead_car_id = "+lead_car_id+", name = "+name+", variant_id = "+variant_id+", brand = "+brand+", is_primary = "+is_primary+", variant_code = "+variant_code+", variant = "+variant+"]";
        }
    }
    public class Car_activity_groups
    {
        private String group_name;

        private List <Activity_stages> activity_stages;

        public String getGroup_name ()
        {
            return group_name;
        }

        public void setGroup_name (String group_name)
        {
            this.group_name = group_name;
        }

        public List<Activity_stages> getActivity_stages ()
        {
            return activity_stages;
        }

        public void setActivity_stages (List<Activity_stages> activity_stages)
        {
            this.activity_stages = activity_stages;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [group_name = "+group_name+", activity_stages = "+activity_stages+"]";
        }
    }
    public class Activity_stages
    {
        private String width;

        private String name;

        private String bar_class;

        private String stage_id;

        public String getWidth ()
        {
            return width;
        }

        public void setWidth (String width)
        {
            this.width = width;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getBar_class ()
        {
            return bar_class;
        }

        public void setBar_class (String bar_class)
        {
            this.bar_class = bar_class;
        }

        public String getStage_id ()
        {
            return stage_id;
        }

        public void setStage_id (String stage_id)
        {
            this.stage_id = stage_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [width = "+width+", name = "+name+", bar_class = "+bar_class+", stage_id = "+stage_id+"]";
        }
    }
    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }
}
