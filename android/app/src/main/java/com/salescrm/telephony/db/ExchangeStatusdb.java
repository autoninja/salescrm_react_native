package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by akshata on 14/10/16.
 */

public class ExchangeStatusdb extends RealmObject {

    public static final String ExchangeSTATUS = "exchangestatus";

    private long leadId;
    private  String width;
    private String exchangestatusname;
    private String bar_class;
    @PrimaryKey
    private String stage_id;

    public String getStage_id() {
        return stage_id;
    }

    public void setStage_id(String stage_id) {
        this.stage_id = stage_id;
    }

    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getExchangestatusname() {
        return exchangestatusname;
    }

    public void setExchangestatusname(String exchangestatusname) {
        this.exchangestatusname = exchangestatusname;
    }

    public String getBar_class() {
        return bar_class;
    }

    public void setBar_class(String bar_class) {
        this.bar_class = bar_class;
    }
}
