package com.salescrm.telephony.fragments;

import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.ActionPlanPagerAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.offline.FetchDseTasks;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.interfaces.ActionPlanHeaderUpdateListener;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.interfaces.FetchDseTasksListener;
import com.salescrm.telephony.model.ActionPlanApiListener;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.model.FilterDataChangedListener;
import com.salescrm.telephony.model.FilterSelectionHolder;
import com.salescrm.telephony.model.TabLayoutText;
import com.salescrm.telephony.model.ViewPagerSwiped;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.ActionPlanResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.FabricUtils;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.RetrofitError;

/**
 * Created by Ravindra P on 01-12-2015.A
 */
public class ActionPlanFragment extends Fragment implements AutoDialogClickListener, ActionPlanHeaderUpdateListener, FetchDseTasksListener {

    //private DbHelper dbHelper ;
    private Realm realm;
    private ViewPager viewPager;
    private ActionPlanPagerAdapter actionPlanPagerAdapter;
    private Preferences pref;
    private ProgressBar progress;
    private RelativeLayout progressLayout, relLoading;
    private TextView tvDseName;
    private ImageView imageDse;
    private TabLayout tabLayout;
    private ProgressBar actionPlanProgressBar;
    private int prevTab = -1;
    private SparseArray<TabLayoutText> progressMap;
    private ViewPagerSwiped viewPagerSwiped;
    private RelativeLayout relFooter;
    private FilterSelectionHolder filterSelectionHolder;

    private Integer dseId;
    private int dseRoleId;
    private String dseName;
    private String dsePhotoUrl;
    private LinearLayout msgStripLL;
    // Our handler for received Intents. This will be called whenever an Intent
// with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            Bundle data = intent.getExtras();
            if (data != null) {
                boolean isConnected = data.getBoolean("isConnected");
                if (filterSelectionHolder.isEmpty()) {
                    if (getActivity() != null && isAdded()) {
                        if (isConnected) {
                            updateFooter(View.GONE, getString(R.string.your_offline), false);
                        } else {
                            updateFooter(View.VISIBLE, getString(R.string.your_offline), true);
                        }
                    }
                }

            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_action_plan, container, false);
        System.out.println("OnCreate ActionPlan");
        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        filterSelectionHolder = FilterSelectionHolder.getInstance();
        viewPagerSwiped = new ViewPagerSwiped();
        progress = (ProgressBar) rootView.findViewById(R.id.pbProgress);
        progressLayout = (RelativeLayout) rootView.findViewById(R.id.progressLayout);
        actionPlanProgressBar = (ProgressBar) rootView.findViewById(R.id.action_plan_progress_bar);
        relLoading = (RelativeLayout) rootView.findViewById(R.id.rel_loading_frame);
        relFooter = (RelativeLayout) rootView.findViewById(R.id.rel_action_plan_footer);
        progressMap = new SparseArray<>();
        tabLayout = (TabLayout) rootView.findViewById(R.id.pager_header);
        viewPager = (ViewPager) rootView.findViewById(R.id.vpPager);
        tvDseName = (TextView) rootView.findViewById(R.id.tv_tasks_dse_name);
        imageDse = (ImageView) rootView.findViewById(R.id.iv_tasks_dse_image);
        msgStripLL = (LinearLayout) rootView.findViewById(R.id.msg_strip);
        //Check Redirect option
        init();
        setStrip();

        //Tabs for the main layout!
        tabLayout.addTab(tabLayout.newTab().setText("Today --/--"));
        tabLayout.addTab(tabLayout.newTab().setText("Future --"));
        tabLayout.addTab(tabLayout.newTab().setText("Done --"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);


        relFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filterSelectionHolder == null || !filterSelectionHolder.isEmpty()) {
                    clearFilter();
                }

            }
        });
        pushMyTaskToCleverTap(CleverTapConstants.EVENT_MY_TASK_TAB_VALUE_TODAY);
        FabricUtils.sendEvent(pref, "My Task", "Tab", "Today");

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0://SalesCRMApplication.sendEventToCleverTap("Mob Tab Today");
                        FabricUtils.sendEvent(pref, "My Task", "Tab", "Today");
                        pushMyTaskToCleverTap(CleverTapConstants.EVENT_MY_TASK_TAB_VALUE_TODAY);
                        break;
                    case 1: //SalesCRMApplication.sendEventToCleverTap("Mob Tab All");
                        FabricUtils.sendEvent(pref, "My Task", "Tab", "Future");
                        pushMyTaskToCleverTap(CleverTapConstants.EVENT_MY_TASK_TAB_VALUE_FUTURE);
                        break;
                    case 2: //SalesCRMApplication.sendEventToCleverTap("Mob Tab Done");
                        FabricUtils.sendEvent(pref, "My Task", "Tab", "Done");
                        pushMyTaskToCleverTap(CleverTapConstants.EVENT_MY_TASK_TAB_VALUE_VALUE_DONE);
                        break;
                }
                viewPagerSwiped.setNewPosition(tab.getPosition());
                viewPagerSwiped.setOldPosition(prevTab);
                SalesCRMApplication.getBus().post(viewPagerSwiped);
                viewPager.setCurrentItem(tab.getPosition());
                updateHeaderView();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter(WSConstants.NINJA_BROADCAST_MESSAGE));
        process();
        return rootView;
    }

    private void pushMyTaskToCleverTap(String value) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_MY_TASK_KEY_TAB, value);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_MY_TASK, hashMap);
    }

    @Override
    public void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }


    private void clearFilter() {

        //ClearFilter Dialog
        new AutoDialog(getContext(), this, new AutoDialogModel("Are you sure want to clear filter?", "Yes", "No")).show();

    }

    //Refresh progress bar || Update the text of the tabView
    private void updateHeaderView() {
        //Change bold and normal
        int pos = tabLayout.getSelectedTabPosition();
        if (prevTab == -1) {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            prevTab = 0;

        } else if (prevTab != pos) {
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setTypeface(null, Typeface.BOLD);
            ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(prevTab)))).getChildAt(1))).setTypeface(null, Typeface.NORMAL);
            prevTab = pos;
        }
        if (pos == 0) {
            //ProgressBar update
            int progress = getRealmResult(WSConstants.TODAY, true, false).size();
            int max = getRealmResult(WSConstants.TODAY, false, true).size();
            actionPlanProgressBar.setVisibility(View.VISIBLE);
            actionPlanProgressBar.setMax(max * 100);
            ObjectAnimator progressAnimator = ObjectAnimator.ofInt(actionPlanProgressBar, "progress", 0, progress * 100);
            progressAnimator.setDuration(500);
            progressAnimator.setInterpolator(new DecelerateInterpolator());
            progressAnimator.start();
        } else {
            actionPlanProgressBar.setVisibility(View.GONE);
        }

    }

    private RealmResults<SalesCRMRealmTable> getRealmResult(String when, boolean isDone, boolean isBothDoneAndActive) {
        boolean isCreatedTemp = false;
        //
        if (dseId.intValue() != pref.getAppUserId().intValue()) {
            isCreatedTemp = true;
        }
        RealmQuery<SalesCRMRealmTable> realmQuery = realm.where(SalesCRMRealmTable.class)
                .equalTo("isCreatedTemporary", isCreatedTemp)
                .equalTo("createdOffline", false)
                .equalTo("dseId", dseId);

        if (when != null) {
            if (when.equalsIgnoreCase(WSConstants.TODAY)) {
                realmQuery = realmQuery.lessThan("activityScheduleDate", Util.getTime(1));
            } else if (when.equalsIgnoreCase(WSConstants.FUTURE)) {
                realmQuery = realmQuery.greaterThanOrEqualTo("activityScheduleDate", Util.getTime(1));
            }
        }

        if (!isBothDoneAndActive) {
            realmQuery = realmQuery.equalTo("isDone", isDone);
        }
        return realmQuery.findAll();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // }
    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
        updateUi();

    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);
    }


    private void init() {
        if (getArguments() != null) {
            this.dseId = getArguments().getInt(WSConstants.USER_ID, 0);
            if (dseId == 0) {
                dseId = pref.getAppUserId();
            }
            this.dseRoleId = getArguments().getInt(WSConstants.USER_ROLE_ID, 0);
            this.dseName = getArguments().getString(WSConstants.USER_NAME, "DSE");
            this.dsePhotoUrl = getArguments().getString(WSConstants.USER_PHOTO_URL, "");
        } else {
            dseId = pref.getAppUserId();
            ;

        }
    }

    private void process() {

        if (dseId.intValue() == pref.getAppUserId().intValue()) {
            setAdapter();
        } else {
            //getData for selected dse
            tvDseName.setText("Getting " + dseName + "'s tasks");
            relLoading.setVisibility(View.VISIBLE);
            new FetchDseTasks(this, getContext(), dseId).call(getContext());

        }
    }

    private void setAdapter() {
        if (isAdded()) {
            relLoading.setVisibility(View.GONE);
            actionPlanPagerAdapter = new ActionPlanPagerAdapter(getChildFragmentManager(), ActionPlanFragment.this, dseId, pref.getAppUserId());
            viewPager.setAdapter(actionPlanPagerAdapter);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            viewPager.setCurrentItem(0);
            viewPager.setOffscreenPageLimit(3);
            if (!new ConnectionDetectorService(SalesCRMApplication.GetAppContext()).isConnectingToInternet()) {
                relLoading.setVisibility(View.GONE);
                updateFooter(View.VISIBLE, getString(R.string.your_offline), true);
            } else {
                updateFooter(View.GONE, getString(R.string.your_offline), false);
                if (pref.isFirstTime()) {
                    relLoading.setVisibility(View.VISIBLE);
                    System.out.println(":Empty Data:");
                    SalesCRMApplication.getBus().post(new ActionPlanApiListener(0));
                }

            }
        }
    }


    public void updateTabText() {
        //Change Text of tabView Today's task
        ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(0)))).getChildAt(1))).setText(
                String.format(Locale.getDefault(), "Today %d/%d", getRealmResult(WSConstants.TODAY, true, false).size(), getRealmResult(WSConstants.TODAY, false, true).size()));

        //Change Text of tabView All task
        ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(1)))).getChildAt(1))).setText(
                String.format(Locale.getDefault(), "Future %d", getRealmResult(WSConstants.FUTURE, false, false).size()));

        //Change Text of tabView Done task
        ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(2)))).getChildAt(1))).setText(
                String.format(Locale.getDefault(), "Done %d", getRealmResult(null, true, false).size()));
    }

    public void updateTabTextOnFilter(int pos, String text) {
        //Change Text of tabView Today's task
        ((AppCompatTextView) (((LinearLayout) ((((LinearLayout) tabLayout.getChildAt(0)).getChildAt(pos)))).getChildAt(1))).setText(
                text);

    }


    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

    public void updateFooter(int visibility, String text, boolean isOffline) {
        relFooter.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            TextView textView = (TextView) relFooter.findViewById(R.id.tv_action_plan_footer);
            textView.setText(text);
            ImageView imageView = (ImageView) relFooter.findViewById(R.id.iv_action_plan_footer);
            if (isOffline) {
                Drawable vector = VectorDrawableCompat.create(getResources(), R.drawable.ic_offline, getActivity().getTheme());
                imageView.setImageDrawable(vector);
            } else if (!filterSelectionHolder.isEmpty()) {
                Drawable vector = VectorDrawableCompat.create(getResources(), R.drawable.ic_clear, getActivity().getTheme());
                imageView.setImageDrawable(vector);
            }
        }


    }

    @Override
    public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
        filterSelectionHolder.clearAll();
        updateUi();
        SalesCRMApplication.getBus().post(new FilterDataChangedListener(true));
    }

    private void updateUi() {
        if (!filterSelectionHolder.isEmpty()) {
            updateFooter(View.VISIBLE, getString(R.string.clear_filter), false);
        } else if (!new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
            updateFooter(View.VISIBLE, getString(R.string.your_offline), true);
        } else {
            updateFooter(View.GONE, getString(R.string.clear_filter), false);
        }

        updateTabText();
        updateHeaderView();
    }

    @Override
    public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @Override
    public void onActionPlanHeaderUpdateRequired(int from) {
        updateHeaderView();
        updateTabText();
        System.out.println("Header update required:::" + from);
    }

    @Override
    public void onFilterAppliedUpdateText(int pos, String text) {
        updateTabTextOnFilter(pos, text);
        System.out.println("Header update required on Filter:::" + pos);

    }

    @Override
    public void onFetchDseTaskSuccess(List<ActionPlanResponse.Result.Data> allActionPlanData) {
        setAdapter();
        updateHeaderView();
        updateTabText();
    }

    @Override
    public void onFetchDseTaskError(RetrofitError error, int actionPlan) {
        Util.showToast(getContext(), "Error contacting server", Toast.LENGTH_SHORT);
        setAdapter();
        relLoading.setVisibility(View.VISIBLE);
    }

    private void setStrip() {
        if (pref.getmGameLocationId() == -1) {
            if (dseId.intValue() == pref.getAppUserId().intValue()) {
                OrderedRealmCollection<SalesCRMRealmTable> realmResult = realm.where(SalesCRMRealmTable.class)
                        .equalTo("isCreatedTemporary", false)
                        .lessThan("activityScheduleDate", Util.getTime(0))
                        .equalTo("isDone", false)
                        .equalTo("isLeadActive", 1)
                        .equalTo("createdOffline", false)
                        .equalTo("dseId", dseId)
                        .findAllSorted("activityScheduleDate", Sort.DESCENDING);
                if (realmResult != null && realmResult.size() < 10) {
                    msgStripLL.setVisibility(View.VISIBLE);
                } else {
                    msgStripLL.setVisibility(View.GONE);
                }
            } else {
                msgStripLL.setVisibility(View.GONE);
            }
        } else {
            msgStripLL.setVisibility(View.GONE);
        }
    }
}