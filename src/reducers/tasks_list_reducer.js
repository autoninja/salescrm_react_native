import { UPDATE_TAB_COUNT, UPDATE_TASKS_LIST_FILTER, UPDATE_TASKS_LIST_FILTER_UPDATE } from '../actions/tasks_list_types';

const initialState = {
  todaysTabTotal: 0,
  futureTabTotal: 0,
  doneTabTotal: 0,
  filters: [],
  filtersUpdate: false
};

const tasksListReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_TAB_COUNT:
      return {
        ...state,
        todaysTabTotal: action.payload.todaysTabTotal,
        futureTabTotal: action.payload.futureTabTotal,
        doneTabTotal: action.payload.doneTabTotal
      };
    case UPDATE_TASKS_LIST_FILTER:
      return {
        ...state,
        filters: action.payload.filters,
        filtersUpdate: action.payload.filtersUpdate
      }
    case UPDATE_TASKS_LIST_FILTER_UPDATE:
      return {
        ...state,
        filtersUpdate: action.payload.filtersUpdate
      }
    default:
      return state;
  }
}

export default tasksListReducer;
