package com.salescrm.telephony.db.team_dashboard_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;

/**
 * Created by prateek on 20/9/17.
 */

public class ResultEtvbrCars extends RealmObject {
    @SerializedName("abs")
    @Expose
    public RealmList<EtvbrCarsAbsolute> abs = new RealmList<>();
    @SerializedName("rel")
    @Expose
    public RealmList<EtvbrCarsRelational> rel = new RealmList<>();
    @SerializedName("abs_total")
    @Expose
    public EtvbrCarsAbsTotal absTotal;
    @SerializedName("rel_total")
    @Expose
    public EtvbrCarsRelTotal relTotal;

    public RealmList<EtvbrCarsAbsolute> getAbs() {
        return abs;
    }

    public void setAbs(RealmList<EtvbrCarsAbsolute> abs) {
        this.abs = abs;
    }

    public RealmList<EtvbrCarsRelational> getRel() {
        return rel;
    }

    public void setRel(RealmList<EtvbrCarsRelational> rel) {
        this.rel = rel;
    }

    public EtvbrCarsAbsTotal getAbsTotal() {
        return absTotal;
    }

    public void setAbsTotal(EtvbrCarsAbsTotal absTotal) {
        this.absTotal = absTotal;
    }

    public EtvbrCarsRelTotal getRelTotal() {
        return relTotal;
    }

    public void setRelTotal(EtvbrCarsRelTotal relTotal) {
        this.relTotal = relTotal;
    }
}
