import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
export default class CreateLeadButtonSelector extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    var title = this.props.title;
    var isSelected = (this.props.selected == this.props.id);
    let source = require('../../images/ic_back_white.png');
    if (this.props.icons) {
      if (isSelected) {
        source = this.props.icons.iconActive
      }
      else {
        source = this.props.icons.iconInActive;
      }
    }

    let isDisabled = this.props.disabled;


    return (
      <TouchableOpacity disabled={isDisabled} style={[{ flex: 1 }, this.props.style]} onPress={() => this.props.onClick(this.props.id)}>
        <View style={[styles.shadowsView, { flex: 1 }]}>
          <View style={isDisabled ? [styles.button, styles.buttonDisabled] : [styles.button, isSelected ? styles.buttonSelected : styles.buttonUnSelected]}>
            <Image
              source={source}
              resizeMode='contain'
              style={{ width: 30, height: 30 }}
            />
            <Text style={[styles.title, isSelected ? styles.titleSelected : styles.titleUnSelected, { marginTop: 6 }]}>{title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    margin: 5,
    borderRadius: 5,
    height: 80,
    elevation: 5,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonSelected: {
    backgroundColor: '#4470FF',
  },
  buttonUnSelected: {
    backgroundColor: '#FFFFFF',
  },
  buttonDisabled: {
    backgroundColor: '#808080',
  },
  title: {
    fontSize: 16,
  },
  titleSelected: {
    color: '#ffffff',
  },
  titleUnSelected: {
    color: '#2B3973',
  },
  shadowsView: {
    width: '100%',
    shadowColor: "#808080",
    shadowOpacity: 0.4,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0
    },
  }
})
