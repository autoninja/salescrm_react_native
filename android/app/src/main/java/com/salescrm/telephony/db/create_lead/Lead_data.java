package com.salescrm.telephony.db.create_lead;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by bharath on 8/12/16.
 */

public class Lead_data extends RealmObject {
    private String exp_closing_date;

    private RealmList<User_details> user_details;

    private RealmList<Emails> emails;

    private RealmList<Ex_car_details> ex_car_details;

    private RealmList<Ad_car_details> ad_car_details;

    private Integer location_id;

    private Customer_details customer_details;

    private RealmList<Car_details> car_details;

    private String mode_of_pay;

    private String lead_tag_id;

    private String lead_enq_date;

    private RealmList<Mobile_numbers> mobile_numbers;

    private String lead_source_id;
    private String lead_source_category_id;
    private String ref_vin_reg_no;


    public String getExp_closing_date() {
        return exp_closing_date;
    }

    public void setExp_closing_date(String exp_closing_date) {
        this.exp_closing_date = exp_closing_date;
    }

    public RealmList<User_details> getUser_details() {
        return user_details;
    }

    public void setUser_details(RealmList<User_details> user_details) {
        this.user_details = user_details;
    }

    public RealmList<Emails> getEmails() {
        return emails;
    }

    public void setEmails(RealmList<Emails> emails) {
        this.emails = emails;
    }

    public RealmList<Ex_car_details> getEx_car_details() {
        return ex_car_details;
    }

    public void setEx_car_details(RealmList<Ex_car_details> ex_car_details) {
        this.ex_car_details = ex_car_details;
    }

    public RealmList<Ad_car_details> getAd_car_details() {
        return ad_car_details;
    }

    public void setAd_car_details(RealmList<Ad_car_details> ad_car_details) {
        this.ad_car_details = ad_car_details;
    }

    public Integer getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Integer location_id) {
        this.location_id = location_id;
    }

    public Customer_details getCustomer_details() {
        return customer_details;
    }

    public void setCustomer_details(Customer_details customer_details) {
        this.customer_details = customer_details;
    }

    public RealmList<Car_details> getCar_details() {
        return car_details;
    }

    public void setCar_details(RealmList<Car_details> car_details) {
        this.car_details = car_details;
    }

    public String getMode_of_pay() {
        return mode_of_pay;
    }

    public void setMode_of_pay(String mode_of_pay) {
        this.mode_of_pay = mode_of_pay;
    }

    public String getLead_tag_id() {
        return lead_tag_id;
    }

    public void setLead_tag_id(String lead_tag_id) {
        this.lead_tag_id = lead_tag_id;
    }

    public String getLead_enq_date() {
        return lead_enq_date;
    }

    public void setLead_enq_date(String lead_enq_date) {
        this.lead_enq_date = lead_enq_date;
    }

    public RealmList<Mobile_numbers> getMobile_numbers() {
        return mobile_numbers;
    }

    public void setMobile_numbers(RealmList<Mobile_numbers> mobile_numbers) {
        this.mobile_numbers = mobile_numbers;
    }

    public String getLead_source_id() {
        return lead_source_id;
    }

    public void setLead_source_id(String lead_source_id) {
        this.lead_source_id = lead_source_id;
    }

    public String getLead_source_category_id() {
        return lead_source_category_id;
    }

    public void setLead_source_category_id(String lead_source_category_id) {
        this.lead_source_category_id = lead_source_category_id;
    }

    public String getRef_vin_reg_no() {
        return ref_vin_reg_no;
    }

    public void setRef_vin_reg_no(String ref_vin_reg_no) {
        this.ref_vin_reg_no = ref_vin_reg_no;
    }

    @Override
    public String toString() {
        return "ClassPojo [exp_closing_date = " + exp_closing_date + ", user_details = " + user_details + ", emails = " + emails + ", ex_car_details = " + ex_car_details + ", ad_car_details = " + ad_car_details + ", location_id = " + location_id + ", customer_details = " + customer_details + ", car_details = " + car_details + ", mode_of_pay = " + mode_of_pay + ", lead_tag_id = " + lead_tag_id + ", lead_enq_date = " + lead_enq_date + ", mobile_numbers = " + mobile_numbers + ", lead_source_id = " + lead_source_id + "]";
    }


}