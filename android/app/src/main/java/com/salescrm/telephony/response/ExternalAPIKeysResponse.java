package com.salescrm.telephony.response;

public class ExternalAPIKeysResponse {
    private Result result;

    private Error error;
    private String message;
    private String statusCode;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public class Result {
        String gov_data_api_keys;

        public String getGov_data_api_keys() {
            return gov_data_api_keys;
        }

        public void setGov_data_api_keys(String gov_data_api_keys) {
            this.gov_data_api_keys = gov_data_api_keys;
        }
    }
}
