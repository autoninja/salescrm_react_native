package com.salescrm.telephony.response.gamification;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bannhi on 27/12/17.
 */

public class TeamLeaderBoardResponse implements Serializable {

    private TeamLeaderBoardResponse teamLeaderBoardResponse;

        private String statusCode;

        private String message;

        private Result result;

        private Error error;

    public TeamLeaderBoardResponse(TeamLeaderBoardResponse teamLeaderBoardResponse) {
        this.teamLeaderBoardResponse = teamLeaderBoardResponse;
    }

    public TeamLeaderBoardResponse getTeamLeaderBoardResponse() {
        return teamLeaderBoardResponse;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result implements Serializable
    {


        private ArrayList<Team_leads> team_leads;

        public ArrayList<Team_leads> getTeam_leads ()
        {
            return team_leads;
        }

        public void setTeam_leads (ArrayList<Team_leads> team_leads)
        {
            this.team_leads = team_leads;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [team_leads = "+team_leads+"]";
        }
    }


    public class Team_leads implements Serializable
    {
        private String team_leader_id;

        private String team_average_points;

        private String team_name;

        private String user_id;

        private String best_team_ever;

        private String team_leader_name;

        private String team_leader_photo_url;

        private String team_id;

        private String spotlight;

        public String getTeam_leader_id ()
        {
            return team_leader_id;
        }

        public void setTeam_leader_id (String team_leader_id)
        {
            this.team_leader_id = team_leader_id;
        }

        public String getTeam_average_points ()
        {
            return team_average_points;
        }

        public void setTeam_average_points (String team_average_points)
        {
            this.team_average_points = team_average_points;
        }

        public String getTeam_name ()
        {
            return team_name;
        }

        public void setTeam_name (String team_name)
        {
            this.team_name = team_name;
        }

        public String getUser_id ()
        {
            return user_id;
        }

        public void setUser_id (String user_id)
        {
            this.user_id = user_id;
        }

        public String getBest_team_ever ()
        {
            return best_team_ever;
        }

        public void setBest_team_ever (String best_team_ever)
        {
            this.best_team_ever = best_team_ever;
        }

        public String getTeam_leader_name ()
        {
            return team_leader_name;
        }

        public void setTeam_leader_name (String team_leader_name)
        {
            this.team_leader_name = team_leader_name;
        }

        public String getTeam_leader_photo_url ()
        {
            return team_leader_photo_url;
        }

        public void setTeam_leader_photo_url (String team_leader_photo_url)
        {
            this.team_leader_photo_url = team_leader_photo_url;
        }

        public String getTeam_id ()
        {
            return team_id;
        }

        public void setTeam_id (String team_id)
        {
            this.team_id = team_id;
        }

        public String getSpotlight ()
        {
            return spotlight;
        }

        public void setSpotlight (String spotlight)
        {
            this.spotlight = spotlight;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [team_leader_id = "+team_leader_id+", team_average_points = "+team_average_points+", team_name = "+team_name+", user_id = "+user_id+", best_team_ever = "+best_team_ever+", team_leader_name = "+team_leader_name+", team_leader_photo_url = "+team_leader_photo_url+", team_id = "+team_id+", spotlight = "+spotlight+"]";
        }
    }




    public class Error implements Serializable
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }

}
