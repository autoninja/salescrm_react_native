package com.salescrm.telephony.db;

import com.salescrm.telephony.response.GeneralResponse;
import com.salescrm.telephony.utils.ApiUtil;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import retrofit.Callback;
import retrofit.http.Field;

/**
 * Created by bharath on 13/12/16.
 */

//Used for AllotDseForActivity

public class AllotDseDB extends RealmObject {

    private boolean is_synced=false;
    private Date dataCreatedDate;

    @PrimaryKey
    private int scheduledActivityId;

    private int leadId;
    private int locationId;
    private int dse_team_lead_id;
    private int dseId;
    private int manager_id;
    private String leadLastUpdated;

    public Date getDataCreatedDate() {
        return dataCreatedDate;
    }

    public void setDataCreatedDate(Date dataCreatedDate) {
        this.dataCreatedDate = dataCreatedDate;
    }

    public boolean is_synced() {
        return is_synced;
    }

    public void setIs_synced(boolean is_synced) {
        this.is_synced = is_synced;
    }

    public int getScheduledActivityId() {
        return scheduledActivityId;
    }

    public void setScheduledActivityId(int scheduledActivityId) {
        this.scheduledActivityId = scheduledActivityId;
    }

    public int getLeadId() {
        return leadId;
    }

    public void setLeadId(int leadId) {
        this.leadId = leadId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public int getDse_team_lead_id() {
        return dse_team_lead_id;
    }

    public void setDse_team_lead_id(int dse_team_lead_id) {
        this.dse_team_lead_id = dse_team_lead_id;
    }

    public int getDseId() {
        return dseId;
    }

    public void setDseId(int dseId) {
        this.dseId = dseId;
    }

    public int getManager_id() {
        return manager_id;
    }

    public void setManager_id(int manager_id) {
        this.manager_id = manager_id;
    }

    public String getLeadLastUpdated() {
        return leadLastUpdated;
    }

    public void setLeadLastUpdated(String leadLastUpdated) {
        this.leadLastUpdated = leadLastUpdated;
    }
}
