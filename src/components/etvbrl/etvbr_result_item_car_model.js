import React, { Component } from 'react'
import { View, Text, Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Alert} from 'react-native'


export default class ETVBRResultItemCarModel extends Component {
  constructor(props) {
    super(props);
  }
  _onLongPressButton() {
    console.log('Long press worked');
    Alert.alert('You long-pressed the button!')
  }
  render() {
    if(this.props.showPercentage) {
      return (
      <View style={{flexDirection:'column'}}>
      <View style={{backgroundColor:'#5E7692', height:1}}/>
      <View style={{flexDirection: 'row', height:60, backgroundColor:'#0000003c', alignItems:'center'}}>

      <View style={{flex:1.5, alignItems:'center'}} >

      <Text style={{ color:'#fff', fontWeight:'bold'}} >Total%</Text>


      </View>


      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.rel.e}</Text>
        </View>
      </View>



      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.rel.t}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.rel.v}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.rel.b}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.rel.r}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

          <Text style={{ color:'red', fontWeight:'bold'}} >{this.props.rel.l}</Text>
      </View>


      </View>
      <View style={{backgroundColor:'#5E7692', height:1}}/>
      </View>
    );
    }
    else {
    return(
      <View style={{flexDirection:'column'}}>
      <View style={{backgroundColor:'#5E7692', height:1}}/>
      <View style={{flexDirection: 'row', height:60, backgroundColor:'#0000003c', alignItems:'center'}}>

      <View style={{flex:1.5, alignItems:'center'}} >

        <Text style={{ color:'#fff', fontWeight:'bold'}} >Total</Text>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.abs.e}</Text>
        </View>

      </View>




      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.abs.t}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.abs.v}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.abs.b}</Text>
        </View>

      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
          <Text style={{ color:'#fff', fontWeight:'bold'}} >{this.props.abs.r}</Text>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#5E7692',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

          <Text style={{ color:'red', fontWeight:'bold'}} >{this.props.abs.l}</Text>
      </View>


      </View>
      <View style={{backgroundColor:'#5E7692', height:1}}/>
      </View>
    );
  }
  }
}
