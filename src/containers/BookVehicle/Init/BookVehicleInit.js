import React, { Component } from 'react';
import { View, Text, Platform, NativeModules } from 'react-native';
import { BOOKING_STAGES, BOOKING_ROUTES } from '../BookingUtils/BookingUtils'
import { getBookVehicleNavigator } from '../InternalNavigation/BookVehicle.navigation'

export default class BookVehicleInit extends Component {
    constructor(props) {
        super(props);
        let { info, interestedVehicles, userRoles, form_object } = this.getData();
        this.state = {
            info,
            interestedVehicles,
            userRoles,
            form_object
        }
    }
    getData() {
        let data = {
            userRoles: null,
            info: null,
            interestedVehicles: null,
            form_object: null
        }
        if (this.props.info) {
            data.info = this.props.info;
        }
        else if (this.props.navigation) {
            data.info = this.props.navigation.getParam('info');
        }
        if (this.props.interestedVehicles) {
            data.interestedVehicles = this.props.interestedVehicles;
        }
        else if (this.props.navigation) {
            data.interestedVehicles = this.props.navigation.getParam('interestedVehicles');
        }
        if (this.props.userRoles) {
            data.userRoles = this.props.userRoles;
        }
        else if (this.props.navigation) {
            data.userRoles = this.props.navigation.getParam('userRoles');
        }
        if (this.props.form_object) {
            data.form_object = this.props.form_object;
        }
        else if (this.props.navigation) {
            data.form_object = this.props.navigation.getParam('form_object');
        }
        return data;
    }

    showAlert(message) {
        try {
            this.refs.toast.show(message);
        }
        catch (e) {

        }
    }
    activeBookingCount(info) {
        let bookingCount = 0;
        if (info && info.c360Information && info.c360Information.interestedCarsDetails) {
            let interestedCarsDetails = info.c360Information.interestedCarsDetails;
            interestedCarsDetails.map((interestedVehicle) => {
                if (interestedVehicle.carDmsData && interestedVehicle.carDmsData.length > 0) {
                    if (interestedVehicle.carDmsData[0].car_stage_id != BOOKING_STAGES.STAGE_CANCELLED
                        && interestedVehicle.carDmsData[0].car_stage_id != BOOKING_STAGES.STAGE_DELIVERED) {
                        ++bookingCount;
                    }
                }
            })
        }
        return bookingCount;
    }
    goBack(navigation) {
        if (navigation) {
            navigation.goBack();
        }
        else if (this.props.navigation) {
            this.props.navigation.goBack();
        }
        else if (Platform.OS == "android") {
            NativeModules.ReactNativeToAndroid.exit("BOOK_VEHICLE");
        }
    }
    onApiSubmission() {
        //Redirect to respective page
        if (this.props.navigation) {
            this.props.navigation.state.params.requireRefreshC360();
        }
    }
    render() {
        let { info, interestedVehicles, userRoles, form_object } = this.state;
        let routing = {};
        if (info.action == "update_booking") {
            switch (parseInt(info.stageId)) {
                case BOOKING_STAGES.STAGE_INIT:
                    if (this.activeBookingCount(info) > 0) {
                        routing.initialRouteName = BOOKING_ROUTES.BookingPopups;
                        routing.popupData = {
                            title: "One vehicle is already in booking process. Do you still want to proceed?",
                            yesAction: 'total_selection',
                        }
                    }
                    else {
                        routing.initialRouteName = BOOKING_ROUTES.TotalBookingSelector
                    }
                    break;
                case BOOKING_STAGES.STAGE_BOOKED:
                    routing.initialRouteName = BOOKING_ROUTES.BookingPopups;
                    routing.popupData = {
                        title: "Full Payment Received",
                        yesAction: 'booking',
                        noAction: 'reschedule'
                    }
                    break;
                case BOOKING_STAGES.STAGE_FULL_PAYMENT_RECEIVED:
                    routing.initialRouteName = BOOKING_ROUTES.UpdateInvoice;
                    routing.popupData = {
                        title: "Have You Delivered The Car",
                        yesAction: 'booking',
                        noAction: 'reschedule'
                    }
                    break;
                case BOOKING_STAGES.STAGE_INVOICED:
                    routing.initialRouteName = BOOKING_ROUTES.BookingPopups;
                    routing.popupData = {
                        title: "Have You Delivered The Car",
                        yesAction: 'booking',
                        noAction: 'reschedule'
                    }
                    break;
            }
        }
        else if (info.action == "details_booking") {
            routing.initialRouteName = BOOKING_ROUTES.BookVehicleMain;
        }
        else if (info.action == "cancel_booking") {
            routing.initialRouteName = BOOKING_ROUTES.CancelBooking;

        }
        if (routing.initialRouteName) {
            let BOOK_VIEW = getBookVehicleNavigator(
                routing,
                {
                    goBack: this.goBack.bind(this),
                    onApiSubmission: this.onApiSubmission.bind(this)
                },
                info,
                userRoles,
                interestedVehicles,
                form_object

            )
            return (
                <BOOK_VIEW />
            )
        }
        return (
            <View style={{ flex: 1 }}>
                <Text>{info.stageId}</Text>
            </View>
        )
    }
}