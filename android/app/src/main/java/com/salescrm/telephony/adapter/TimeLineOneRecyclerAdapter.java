package com.salescrm.telephony.adapter;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.AllotDseActivity;
import com.salescrm.telephony.activity.BookBikeProcess;
import com.salescrm.telephony.activity.FormRenderingActivity;
import com.salescrm.telephony.activity.ILSendToVerificationProcess;
import com.salescrm.telephony.activity.ILomConfirmedProcess;
import com.salescrm.telephony.activity.PlanNextTaskPicker;
import com.salescrm.telephony.activity.ProformaPdfActivity;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.PlannedActivities;
import com.salescrm.telephony.interfaces.AudioCallbacks;
import com.salescrm.telephony.interfaces.MediaPlayerChecker;
import com.salescrm.telephony.interfaces.PlanNextTaskResultListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.LeadHistory;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.UtilsTime;
import com.salescrm.telephony.utils.WSConstants;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by prateek on 15/10/16.
 */
public class TimeLineOneRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements MediaPlayerChecker {

    //private RealmList<PlannedActivities> mFeedList = new RealmList<>();
    private List<LeadHistory.Result> mFeedListResult = new ArrayList<>();
    Context context;
    Activity activity;
    private static int currentPosition;
    private static String RECORDING_URL ;
    //Runnable mUpdateTimeTask;
    private boolean mActive = false;
    private MediaPlayer mMediaPlayer;
    private Handler mHandler;
    private int playingPosition = -1;
    private final static int HEADER_VIEW = 0;
    private final static int CONTENT_VIEW = 1;
    private final static int HISTORY_VIEW = 2;
    private Date dateFirst, firstDate;
    private Preferences pref = null;
    private boolean isInternetAvailable = true;
    //private boolean HISTORY_RECEIVED ;
    public static boolean FRAGMENT_DESTROYED = false;
    private ArrayList<Boolean> historyResponse;
    private List<PlannedActivities> mFeedList;
    private String[] formatStrings = {"dd MMM yyyy hh:mm aa", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss.SSS"};
    private Drawable drawableAttach;

    private callToParentListener callToParentListener;

    public TimeLineOneRecyclerAdapter(){    }

    public TimeLineOneRecyclerAdapter(Activity activity, List<PlannedActivities> mFeedList, List<LeadHistory.Result> mFeedListResult, Context context, boolean connectingToInternet, ArrayList<Boolean> historyResponse, callToParentListener listener) {
        this.mFeedList = mFeedList;
        this.mMediaPlayer = new MediaPlayer();
        FRAGMENT_DESTROYED = false;
        this.mHandler = new Handler();
        this.mFeedListResult = mFeedListResult;
        this.context = context;
        this.activity = activity;
        this.pref = Preferences.getInstance();
        this.pref.load(context);
        RECORDING_URL = "https://telephony.ninjacrm.com/api/cdr/fetchRecordingFile.php?dealer="+pref.getDealerName()+"&action=getFile&file=";
        this.isInternetAvailable = connectingToInternet;
        //this.HISTORY_RECEIVED = HISTORY_RECEIVED;
        this.historyResponse = historyResponse;
        this.callToParentListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        pref = Preferences.getInstance();
        pref.load(parent.getContext());
        switch (viewType) {
            case CONTENT_VIEW:
                view = View.inflate(parent.getContext(), R.layout.item_timeline, null);
                return new ItemHolder(view);
            case HISTORY_VIEW:
                view = View.inflate(parent.getContext(), R.layout.item_timeline, null);
                return new HistoryHolder(view);
            case HEADER_VIEW:
                view = View.inflate(parent.getContext(), R.layout.item_timeline_header, null);
                return new VHHeader(view);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final RecyclerView.ViewHolder iViewHolder = holder;
        drawableAttach = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_attachment, null);

        // Here i'm keeping a date to compare
        if (mFeedListResult.size() > 0) {
            String firstTime = mFeedListResult.get(mFeedListResult.size() - 1).getLogged_time();
            try {
                dateFirst = tryParse(firstTime);
                //dateFirst = new SimpleDateFormat("dd MMM yyyy hh:mm aa").parse(firstTime);
                String dateString = new SimpleDateFormat("MM-dd-yyyy").format(dateFirst);
                firstDate = new SimpleDateFormat("MM-dd-yyyy").parse(dateString);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (mFeedList.size() > 0) {
            String firstTime = mFeedList.get(mFeedList.size() - 1).getPlannedActivitiesDateTime();
            try {
                dateFirst = tryParse(firstTime);
                //dateFirst = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(firstTime);
                String dateString = new SimpleDateFormat("MM-dd-yyyy").format(dateFirst);
                firstDate = new SimpleDateFormat("MM-dd-yyyy").parse(dateString);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (holder instanceof ItemHolder) {

            ((ItemHolder) iViewHolder).llitem_timeline_expanded.setVisibility(View.GONE);
            ((ItemHolder) iViewHolder).lltimeline_view.setVisibility(View.VISIBLE);
            ((ItemHolder) iViewHolder).card_item_timeline.setVisibility(View.VISIBLE);
            ((ItemHolder) iViewHolder).llCardview.setVisibility(View.VISIBLE);

            final int pos = position - 1;
            if(mFeedList.get(pos) != null && mFeedList.get(pos).isValid()){
            ((ItemHolder) holder).lltimeline_view.setVisibility(View.VISIBLE);
            ((ItemHolder) holder).card_item_timeline.setVisibility(View.VISIBLE);
            //int count = ((ItemHolder) holder).llCardViewInner.getChildCount();
            // Clear the view to stop redundancy of data.
            ((ItemHolder) holder).llCardViewInner.removeAllViews();
            ((ItemHolder) holder).llExpandCardViewInner.removeAllViews();

            if (pos == mFeedList.size() - 1) {
                ((ItemHolder) holder).timeline_line_view2.setVisibility(View.INVISIBLE);
            } else {
                ((ItemHolder) holder).timeline_line_view2.setVisibility(View.VISIBLE);
            }

            if (mFeedList.get(pos).getPlannedActivitiesDateTime() != null) {
                String dateTime = mFeedList.get(pos).getPlannedActivitiesDateTime();

                try {
                    //Date initDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(dateTime);
                    Date initDate = tryParse(dateTime);
                    String dateString = new SimpleDateFormat("MM-dd-yyyy").format(initDate);
                    Date secondDate = new SimpleDateFormat("MM-dd-yyyy").parse(dateString);
                    SimpleDateFormat formatter = new SimpleDateFormat("EEE dd/MM");
                    SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm aa");
                    String parsedTime = timeFormatter.format(initDate);
                    String parsedDate = formatter.format(initDate);
                    String parsedDay = (new SimpleDateFormat("EEE")).format(initDate);
                    ((ItemHolder) holder).tvDate.setText(parsedDate);
                    ((ItemHolder) holder).tvTime.setText(parsedTime);
                    //((ItemHolder) holder).tvDays.setText(parsedDay);
                    ((ItemHolder) holder).tvDays.setText(getDateDiffString(firstDate, secondDate));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (mFeedList.get(pos).getPlannedActivitiesName() != null) {
                ((ItemHolder) holder).tvActionName.setText(mFeedList.get(pos).getPlannedActivitiesName());
            } else {
                ((ItemHolder) holder).tvActionName.setText("----");
            }
            if(mFeedList.get(pos).getPlannedActivitiesDetail().isValid()) {
                ((ItemHolder) holder).ll_cardview_inner_nested = new LinearLayout[mFeedList.get(pos).getPlannedActivitiesDetail().size()];
                for (int j = 0; j < mFeedList.get(pos).getPlannedActivitiesDetail().size(); j++) {
                    ((ItemHolder) holder).ll_cardview_inner_nested[j] = new LinearLayout(context);
                    ((ItemHolder) holder).ll_cardview_inner_nested[j].setOrientation(LinearLayout.HORIZONTAL);
                    TextView tv1 = new TextView(context);
                    tv1.setTextColor(Color.parseColor("#4a4a4a"));
                    TextView tv2 = new TextView(context);
                    tv2.setTextColor(Color.parseColor("#4a4a4a"));
                    if (mFeedList.get(pos).getPlannedActivitiesDetail().get(j).getPlannedActivitiesDetailValues() != null) {
                        String key = mFeedList.get(pos).getPlannedActivitiesDetail().get(j).getPlannedActivitiesDetailKeys().replace("_", " ");
                        tv1.setText(convertKeysIntoReadableForm(key) + ": ");
                        tv2.setText(mFeedList.get(pos).getPlannedActivitiesDetail().get(j).getPlannedActivitiesDetailValues());
                    } else {
                        tv1.setText("---");
                        tv2.setText("---");
                        tv1.setVisibility(View.GONE);
                        tv2.setVisibility(View.GONE);
                    }
                    String key = mFeedList.get(pos).getPlannedActivitiesDetail().get(j).getPlannedActivitiesDetailKeys();
                    String val = mFeedList.get(pos).getPlannedActivitiesDetail().get(j).getPlannedActivitiesDetailValues();
                    if(Util.isNotNull(key) && key.equalsIgnoreCase("Remarks")) {
                        ((ItemHolder) iViewHolder).ll_cardview_inner_nested[j].setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                Util.copy(context, val);
                                Util.showToast(context, "Copied to clipboard", Toast.LENGTH_SHORT);
                                return false;
                            }
                        });
                    }
                    ((ItemHolder) holder).ll_cardview_inner_nested[j].addView(tv1);
                    ((ItemHolder) holder).ll_cardview_inner_nested[j].addView(tv2);
                    ((ItemHolder) holder).llCardViewInner.addView(((ItemHolder) holder).ll_cardview_inner_nested[j]);

                }
            }
            if (mFeedList.get(pos).plannedActivitiesAction.size() == 0) {
                if ((mFeedList.get(pos).getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY_ID + "") ||
                        mFeedList.get(pos).getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.ALLOT_DSE_ID + "" )&& !mFeedList.get(pos).getActivityId().equalsIgnoreCase("10")
                                && !mFeedList.get(pos).getActivityId().equalsIgnoreCase("11"))) {
                    ((ItemHolder) holder).iDone.setVisibility(View.VISIBLE);
                    ((ItemHolder) holder).imageview_expand_timeline_card.setVisibility(View.VISIBLE);
                    ((ItemHolder) holder).card_item_timeline.setEnabled(true);
                } else {
                    ((ItemHolder) holder).iDone.setVisibility(View.GONE);
                    ((ItemHolder) holder).imageview_expand_timeline_card.setVisibility(View.GONE);
                    ((ItemHolder) holder).card_item_timeline.setEnabled(false);
                }
            } else {
                for (int a = 0; a < mFeedList.get(pos).plannedActivitiesAction.size(); a++) {
                    if (mFeedList.get(pos).plannedActivitiesAction.get(a).getPlannedActivitiesActionsId() == WSConstants.FormAction.FLAG || mFeedList.get(pos).getActivityId().equalsIgnoreCase("10")
                            || mFeedList.get(pos).getActivityId().equalsIgnoreCase("11")) {
                        ((ItemHolder) holder).imageview_expand_timeline_card.setVisibility(View.GONE);
                        ((ItemHolder) holder).card_item_timeline.setEnabled(false);
                    } else if (mFeedList.get(pos).plannedActivitiesAction.get(a).getPlannedActivitiesActionsId() == WSConstants.FormAction.RESCHEDULE ||
                            mFeedList.get(pos).plannedActivitiesAction.get(a).getPlannedActivitiesActionsId() == WSConstants.FormAction.PRE_DONE) {
                        ((ItemHolder) holder).imageview_expand_timeline_card.setVisibility(View.VISIBLE);
                        ((ItemHolder) holder).iDone.setVisibility(View.VISIBLE);
                        ((ItemHolder) holder).tv_action_plan_done.setVisibility(View.VISIBLE);
                        ((ItemHolder) holder).iReschedule.setVisibility(View.GONE);
                        ((ItemHolder) holder).tv_action_plan_reschedule.setVisibility(View.GONE);
                        ((ItemHolder) holder).card_item_timeline.setEnabled(true);
                    } else {
                        ((ItemHolder) holder).imageview_expand_timeline_card.setVisibility(View.GONE);
                        ((ItemHolder) holder).card_item_timeline.setEnabled(false);
                    }
                }
            }


            ((ItemHolder) holder).card_item_timeline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ItemHolder) iViewHolder).llitem_timeline_expanded.setVisibility(View.VISIBLE);
                    ((ItemHolder) iViewHolder).lltimeline_view.setVisibility(View.GONE);
                    ((ItemHolder) iViewHolder).card_item_timeline.setVisibility(View.GONE);
                    ((ItemHolder) iViewHolder).llCardview.setVisibility(View.GONE);
                    ((ItemHolder) iViewHolder).tvExpandAssignName.setVisibility(View.GONE);

                    ((ItemHolder) iViewHolder).tvDateExpanded.setText(((ItemHolder) iViewHolder).tvDate.getText().toString());
                    ((ItemHolder) iViewHolder).tvTimeExpanded.setText(((ItemHolder) iViewHolder).tvTime.getText().toString());
                    ((ItemHolder) iViewHolder).tvDaysExpanded.setText(((ItemHolder) iViewHolder).tvDays.getText().toString());

                    if (((ItemHolder) iViewHolder).tvActionName.getText() != null) {
                        ((ItemHolder) iViewHolder).tvExpandActionName.setText(((ItemHolder) iViewHolder).tvActionName.getText().toString());
                    }
                    if(mFeedList.get(pos).getPlannedActivitiesDetail().isValid()) {
                        ((ItemHolder) iViewHolder).llexapnd_cardview_inner_nested = new LinearLayout[mFeedList.get(pos).getPlannedActivitiesDetail().size()];
                        for (int j = 0; j < mFeedList.get(pos).getPlannedActivitiesDetail().size(); j++) {
                            ((ItemHolder) iViewHolder).llexapnd_cardview_inner_nested[j] = new LinearLayout(context);
                            ((ItemHolder) iViewHolder).llexapnd_cardview_inner_nested[j].setOrientation(LinearLayout.HORIZONTAL);
                            TextView tv1 = new TextView(context);
                            tv1.setTextColor(Color.parseColor("#4a4a4a"));
                            TextView tv2 = new TextView(context);
                            tv2.setTextColor(Color.parseColor("#4a4a4a"));
                            if (mFeedList.get(pos).getPlannedActivitiesDetail().get(j).getPlannedActivitiesDetailValues() != null) {
                                String key = mFeedList.get(pos).getPlannedActivitiesDetail().get(j).getPlannedActivitiesDetailKeys().replace("_", " ");
                                tv1.setText(convertKeysIntoReadableForm(key) + ": ");
                                tv2.setText(mFeedList.get(pos).getPlannedActivitiesDetail().get(j).getPlannedActivitiesDetailValues());
                            } else {
                                tv1.setText("---");
                                tv2.setText("---");
                            }
                            String key = mFeedList.get(pos).getPlannedActivitiesDetail().get(j).getPlannedActivitiesDetailKeys();
                            String val = mFeedList.get(pos).getPlannedActivitiesDetail().get(j).getPlannedActivitiesDetailValues();
                            if(Util.isNotNull(key) && key.equalsIgnoreCase("Remarks")) {
                                ((ItemHolder) iViewHolder).llexapnd_cardview_inner_nested[j].setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        Util.copy(context, val);
                                        Util.showToast(context, "Copied to clipboard", Toast.LENGTH_SHORT);
                                        return false;
                                    }
                                });
                            }
                            ((ItemHolder) iViewHolder).llexapnd_cardview_inner_nested[j].addView(tv1);
                            ((ItemHolder) iViewHolder).llexapnd_cardview_inner_nested[j].addView(tv2);
                            ((ItemHolder) iViewHolder).llExpandCardViewInner.addView(((ItemHolder) iViewHolder).llexapnd_cardview_inner_nested[j]);

                        }
                    }
                }
            });

            if (mFeedList.get(pos).getPlannedActivitiesName().equalsIgnoreCase(WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY) ||
                    mFeedList.get(pos).getPlannedActivitiesName().equalsIgnoreCase(WSConstants.TaskActivityName.ALLOT_DSE)) {

            }
            ((ItemHolder) holder).iReschedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Firebase Analytics
                    // SalesCRMApplication.sendEventToCleverTap("Mob Button Rechedule-timeline");
                    int scheduleId = mFeedList.get(pos).getPlannedActivityScheduleId();
                    pref.setScheduledActivityId(scheduleId);
                    Intent formRenderingActivityIntent = new Intent(context, FormRenderingActivity.class);
                    formRenderingActivityIntent.putExtra("form_title", "Reschedule");
                    formRenderingActivityIntent.putExtra("form_action", "Submit");
                    formRenderingActivityIntent.putExtra("action_id", WSConstants.FormAction.RESCHEDULE);
                    formRenderingActivityIntent.putExtra("from_c360", true);
                    formRenderingActivityIntent.putExtra("dseId", pref.getCurrentDseId());
                    formRenderingActivityIntent.putExtra("activity_id", Util.getInt(mFeedList.get(pos).getActivityId()));

                    context.startActivity(formRenderingActivityIntent);
                    if (context instanceof Activity) {
                        ((Activity) (context)).finish();
                    }

                }
            });

            ((ItemHolder) holder).iDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //SalesCRMApplication.sendEventToCleverTap("Mob Button Done-timeline");
                    int scheduleId = mFeedList.get(pos).getPlannedActivityScheduleId();
                    System.out.println("Folloup Scheduled Id:- " + mFeedList.get(pos).getPlannedActivityScheduleId());
                    pref.setScheduledActivityId(scheduleId);
                    if(Util.getInt(mFeedList.get(pos).getActivityId()) == WSConstants.TaskActivityName.NO_CAR_BOOKED_ID ||
                            Util.getInt(mFeedList.get(pos).getActivityId()) == WSConstants.TaskActivityName.RECEIVE_PAYMENT_ID ||
                            Util.getInt(mFeedList.get(pos).getActivityId()) == WSConstants.TaskActivityName.INVOICE_REQUEST_ID ||
                            Util.getInt(mFeedList.get(pos).getActivityId()) == WSConstants.TaskActivityName.DELIVERY_REQUEST_ID
                            ){

                        if (callToParentListener != null) {
                            ((ItemHolder) iViewHolder).llitem_timeline_expanded.setVisibility(View.GONE);
                            ((ItemHolder) iViewHolder).lltimeline_view.setVisibility(View.VISIBLE);
                            ((ItemHolder) iViewHolder).card_item_timeline.setVisibility(View.VISIBLE);
                            ((ItemHolder) iViewHolder).llCardview.setVisibility(View.VISIBLE);
                            ((ItemHolder) iViewHolder).llExpandCardViewInner.removeAllViews();
                            callToParentListener.onChangeTabPosition(1);
                        }

                    }
                    else if (mFeedList.get(pos).getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.SCHEDULE_NEXT_ACTIVITY_ID + "")) {

                        new PlanNextTaskPicker(context, new PlanNextTaskResultListener() {
                            @Override
                            public void onPlanNextTaskResult(int answerId) {
                                if (answerId == WSConstants.FormAnswerId.BOOK_CAR_ID) {
                                    if (callToParentListener != null) {
                                        ((ItemHolder) iViewHolder).llitem_timeline_expanded.setVisibility(View.GONE);
                                        ((ItemHolder) iViewHolder).lltimeline_view.setVisibility(View.VISIBLE);
                                        ((ItemHolder) iViewHolder).card_item_timeline.setVisibility(View.VISIBLE);
                                        ((ItemHolder) iViewHolder).llCardview.setVisibility(View.VISIBLE);
                                        ((ItemHolder) iViewHolder).llExpandCardViewInner.removeAllViews();
                                        callToParentListener.onChangeTabPosition(1);
                                    }
                                }
                                else if(answerId == WSConstants.FormAnswerId.BOOKED) {
                                    if (callToParentListener != null) {
                                        ((ItemHolder) iViewHolder).llitem_timeline_expanded.setVisibility(View.GONE);
                                        ((ItemHolder) iViewHolder).lltimeline_view.setVisibility(View.VISIBLE);
                                        ((ItemHolder) iViewHolder).card_item_timeline.setVisibility(View.VISIBLE);
                                        ((ItemHolder) iViewHolder).llCardview.setVisibility(View.VISIBLE);
                                        ((ItemHolder) iViewHolder).llExpandCardViewInner.removeAllViews();
                                        callToParentListener.onChangeTabPosition(1);
                                    }
                                }
                                else if(answerId == WSConstants.FormAnswerId.BOOK_RE) {

                                    new BookBikeProcess().show(activity, context,
                                            mFeedList.get(pos).getLeadID()+"",
                                            mFeedList.get(pos).getPlannedActivityScheduleId()+"", true);
                                }
                                else if(answerId == WSConstants.FormAnswerId.IL_RENEWED || answerId == WSConstants.FormAnswerId.SALES_CONFIRMED) {
                                    new ILomConfirmedProcess().show(null,
                                            context,
                                            mFeedList.get(pos).getLeadID()+"",
                                            mFeedList.get(pos).getPlannedActivityScheduleId()+"",
                                            answerId,
                                            null,
                                            ILomConfirmedProcess.FROM_C_360);
                                }
                                else if (answerId == WSConstants.FormAnswerId.VERIFICATION) {
                                    new ILSendToVerificationProcess().show(null,
                                            context,
                                            mFeedList.get(pos).getLeadID()+"",
                                            mFeedList.get(pos).getPlannedActivityScheduleId()+"",
                                            answerId,
                                            null,
                                            ILSendToVerificationProcess.FROM_C_360);
                                }
                                else {
                                    Intent formRenderingActivityIntent = new Intent(context, FormRenderingActivity.class);
                                    formRenderingActivityIntent.putExtra("form_title", " Plan Next Task");
                                    formRenderingActivityIntent.putExtra("form_action", "Submit");
                                    formRenderingActivityIntent.putExtra("action_id", WSConstants.FormAction.ADD_ACTIVITY);
                                    formRenderingActivityIntent.putExtra("scheduled_type", 2);
                                    formRenderingActivityIntent.putExtra("from_c360", true);
                                    formRenderingActivityIntent.putExtra("dseId", pref.getCurrentDseId());
                                    formRenderingActivityIntent.putExtra("answer_id", answerId);
                                    formRenderingActivityIntent.putExtra("activity_id", Util.getInt(mFeedList.get(pos).getActivityId()));
                                    context.startActivity(formRenderingActivityIntent);
                                    if (context instanceof Activity) {
                                        ((Activity) (context)).finish();
                                    }
                                }

                            }
                        }).show();

                    } else if (mFeedList.get(pos).getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.ALLOT_DSE_ID + "")) {
                        //Open Allot dse form
                        Intent intent = new Intent(context, AllotDseActivity.class);
                        intent.putExtra("lead_id", mFeedList.get(pos).getLeadID() + "");
                        //  intent.putExtra("lead_last_updated", actionPlanData.getLead_last_updated());
                        intent.putExtra("from_c360", true);
                        context.startActivity(intent);
                        if (context instanceof Activity) {
                            ((Activity) (context)).finish();
                        }
                    } else {
                        String pnt;
                        if (mFeedList.get(pos).getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.LOST_DROP_ID + "") ||
                                mFeedList.get(pos).getActivityId().equalsIgnoreCase(WSConstants.TaskActivityName.EVALUATION_REQUEST_ID + "")) {
                            pnt = "Submit";
                        } else {
                            pnt = "Plan Next Task";
                        }
                        Intent formRenderingActivityIntent = new Intent(context, FormRenderingActivity.class);
                        formRenderingActivityIntent.putExtra("form_title", "Update");
                        formRenderingActivityIntent.putExtra("form_action", pnt);
                        formRenderingActivityIntent.putExtra("action_id", WSConstants.FormAction.PRE_DONE);
                        formRenderingActivityIntent.putExtra("class_type", "ActionFragment");
                        formRenderingActivityIntent.putExtra("from_c360", true);
                        formRenderingActivityIntent.putExtra("dseId", pref.getCurrentDseId());
                        formRenderingActivityIntent.putExtra("activity_id", Util.getInt(mFeedList.get(pos).getActivityId()));
                        context.startActivity(formRenderingActivityIntent);
                        if (context instanceof Activity) {
                            ((Activity) (context)).finish();
                        }
                    }
                }
            });

            ((ItemHolder) holder).imageview_minimize_expand_timeline_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ItemHolder) iViewHolder).llitem_timeline_expanded.setVisibility(View.GONE);
                    ((ItemHolder) iViewHolder).lltimeline_view.setVisibility(View.VISIBLE);
                    ((ItemHolder) iViewHolder).card_item_timeline.setVisibility(View.VISIBLE);
                    ((ItemHolder) iViewHolder).llCardview.setVisibility(View.VISIBLE);
                    ((ItemHolder) iViewHolder).llExpandCardViewInner.removeAllViews();
                }
            });
        }

        } else if (holder instanceof HistoryHolder) {
            ((HistoryHolder) holder).lltimeline_view.setVisibility(View.VISIBLE);
            ((HistoryHolder) holder).card_item_timeline.setVisibility(View.VISIBLE);
            final int count = ((HistoryHolder) holder).llCardview.getChildCount();
            //((ItemHolder) holder).llCardview.removeViews(0, count);
            ((HistoryHolder) holder).llCardViewInner.removeAllViews();
            position = position - 2;
            if (mFeedList.size() >= position) {
                position = mFeedList.size() - position;
            } else {
                position = position - mFeedList.size();
            }

            if (position == mFeedListResult.size() - 1) {
                ((HistoryHolder) holder).timeline_line_view2.setVisibility(View.INVISIBLE);
            } else {
                ((HistoryHolder) holder).timeline_line_view2.setVisibility(View.VISIBLE);
            }

            if(mFeedListResult.get(position).getLog_type().equalsIgnoreCase("call")) {
                currentPosition = position;
                ((HistoryHolder) holder).btnPlay.setTag(position);
                ((HistoryHolder) holder).audioSeekBar.setTag(position);

                if (mFeedListResult.get(position).getLogged_time() != null) {
                    String dateTime = mFeedListResult.get(position).getLogged_time();
                    try {
                        Date initDate = tryParse(dateTime);
                        String dateString = new SimpleDateFormat("MM-dd-yyyy").format(initDate);
                        Date secondDate = new SimpleDateFormat("MM-dd-yyyy").parse(dateString);
                        SimpleDateFormat formatter = new SimpleDateFormat("EEE dd/MM");
                        SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm aa");
                        String parsedTime = timeFormatter.format(initDate);
                        String parsedDate = formatter.format(initDate);
                        String parsedDay = (new SimpleDateFormat("EEE")).format(initDate);
                        ((HistoryHolder) holder).tvDate.setText(parsedDate);
                        ((HistoryHolder) holder).tvTime.setText(parsedTime);
                        ((HistoryHolder) holder).tvDays.setText(getDateDiffString(firstDate, secondDate));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    ((HistoryHolder) holder).tvDate.setText("---");
                    ((HistoryHolder) holder).tvTime.setText("---");
                    ((HistoryHolder) holder).tvDays.setText("---");
                }


                if(mFeedListResult.get(position).getIcon_type().equalsIgnoreCase("crm_outgoing")){
                    ((HistoryHolder) holder).img_incoming_or_outgoing.setImageResource(R.drawable.ic_outgoing_call);
                }else{
                    ((HistoryHolder) holder).img_incoming_or_outgoing.setImageResource(R.drawable.ic_incoming_call);
                }

             //   int tagPositin = (int) ((HistoryHolder) holder).audioSeekBar.getTag();

                if (mMediaPlayer != null && mMediaPlayer.isPlaying() && playingPosition == position) {
                    ((HistoryHolder) holder).setActive();
                } else {
                    ((HistoryHolder) holder).setInactive();
                }

                final LeadHistory.Result feedResult = mFeedListResult.get(position);

                if (!feedResult.getAllHistoryObject().isEmpty()) {
                    for (int j = 0; j < feedResult.getAllHistoryObject().size(); j++) {
                        if(feedResult.getAllHistoryObject().get(j).getKey().equalsIgnoreCase("duration")){
                            long minutesOrSeconds = 0;
                            long seconds = Long.parseLong(feedResult.getAllHistoryObject().get(j).getValue());
                            System.out.println("duration - seconds -"+seconds);
                            long remainingSeconds = 0;
                            DecimalFormat df = new DecimalFormat("##");
                            DecimalFormat df1 = new DecimalFormat("##.");
                            if(seconds > 60) {
                                 minutesOrSeconds = (seconds % 3600) / 60;
                                 remainingSeconds  =  (seconds % 3600)  % 60;
                                 System.out.println("duration - minutesOrSeconds1 -"+minutesOrSeconds);
                                ((HistoryHolder) holder).songTotalDurationLabel.setText(""+df1.format(minutesOrSeconds)+df.format(remainingSeconds));
                            }else{
                                System.out.println("duration - minutesOrSeconds2 -"+minutesOrSeconds);
                                ((HistoryHolder) holder).songTotalDurationLabel.setText("00."+df.format(seconds));
                            }

                        }
                    }
                }

                ((HistoryHolder) holder).btnPlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!feedResult.getAllHistoryObject().isEmpty()) {
                            for (int j = 0; j < feedResult.getAllHistoryObject().size(); j++) {
                                if (feedResult.getAllHistoryObject().get(j).getKey().equalsIgnoreCase("recording_url")) {
                                    String url = "";
                                    try {
                                        url = RECORDING_URL + URLEncoder.encode(feedResult.getAllHistoryObject().get(j).getValue(), "UTF-8");
                                        System.out.println("audioSeekBar url - "+url);
                                    } catch (UnsupportedEncodingException e) {
                                        throw new RuntimeException(e);
                                    }

                                    int pos = (int) ((HistoryHolder) iViewHolder).btnPlay.getTag();
                                    System.out.println("audioSeekBar Position - "+pos + "- "+playingPosition);
                                    if(pos == playingPosition ){
                                         playingPosition = -1;
                                        ((HistoryHolder) iViewHolder).pausePlayingAudio();
                                    }else {
                                         playingPosition = pos;
                                        ((HistoryHolder) iViewHolder).playingAudio(url);
                                    }
                                    }
                            }
                        }

                    }
                });



                ((HistoryHolder) holder).rl_timeline_holder.setVisibility(View.GONE);
                ((HistoryHolder) holder).rl_audio_playback.setVisibility(View.VISIBLE);
                ((HistoryHolder) holder).relHistoryLocationView.setVisibility(View.GONE);

            }else{

                ((HistoryHolder) holder).rl_timeline_holder.setVisibility(View.VISIBLE);
                ((HistoryHolder) holder).rl_audio_playback.setVisibility(View.GONE);


                ((HistoryHolder) holder).tvActionName.setText(mFeedListResult.get(position).getTitle());
                LeadHistory.Result.LocationData locationData = mFeedListResult.get(position).getLocation_data();
                if (mFeedListResult.get(position).getRemarks() != null && !mFeedListResult.get(position).getRemarks().equalsIgnoreCase("")) {
                    ((HistoryHolder) holder).tvRemarks.setText("Remark: " + mFeedListResult.get(position).getRemarks());
                    ((HistoryHolder) holder).tvRemarks.setVisibility(View.VISIBLE);
                } else {
                    ((HistoryHolder) holder).tvRemarks.setVisibility(View.GONE);
                }

                if (locationData != null && Util.isNotNull(locationData.getLatitude()) && Util.isNotNull(locationData.getLongitude())) {
                    if(Util.isNotNull(locationData.getLocality())) {
                        ((HistoryHolder) holder).tvHistoryLocation.setText(locationData.getLocality());
                    }
                    else {
                        ((HistoryHolder) holder).tvHistoryLocation.setText("Location");
                    }
                    ((HistoryHolder) holder).relHistoryLocationView.setVisibility(View.VISIBLE);
                    ((HistoryHolder)holder).relHistoryLocationView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String uri = String.format(Locale.getDefault(), "geo:0,0?q=%s,%s(%s)", locationData.getLatitude(),
                                    locationData.getLongitude(), locationData.getLocality());
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                            context.startActivity(intent);
                        }
                    });
                } else {
                    ((HistoryHolder) holder).relHistoryLocationView.setVisibility(View.GONE);
                }


                ((HistoryHolder) holder).ll_cardview_inner_nested = new LinearLayout[mFeedListResult.get(position).getAllHistoryObject().size()];

                if (!mFeedListResult.get(position).getAllHistoryObject().isEmpty()) {
                    for (int j = 0; j < mFeedListResult.get(position).getAllHistoryObject().size(); j++) {
                        ((HistoryHolder) holder).ll_cardview_inner_nested[j] = new LinearLayout(context);
                        ((HistoryHolder) holder).ll_cardview_inner_nested[j].setOrientation(LinearLayout.HORIZONTAL);
                        TextView tv1 = new TextView(context);
                        tv1.setTextColor(Color.parseColor("#4a4a4a"));
                        TextView tv2 = new TextView(context);
                        tv2.setTextColor(Color.parseColor("#4a4a4a"));
                        if (mFeedListResult.get(position).getAllHistoryObject() != null && !mFeedListResult.get(position).getAllHistoryObject().get(j).getKey().equalsIgnoreCase("recording")) {
                            String key = mFeedListResult.get(position).getAllHistoryObject().get(j).getKey().replace("_", " ");
                            tv1.setText(convertKeysIntoReadableForm(key) + ": ");
                            Date ifDate = tryParse(mFeedListResult.get(position).getAllHistoryObject().get(j).getValue());
                            if (ifDate != null) {
                                String dateInner = new SimpleDateFormat("dd MMM yyyy hh:mm aa").format(ifDate);
                                tv2.setText(dateInner);
                            } else if(mFeedListResult.get(position).getAllHistoryObject().get(j).getValue().contains("http")){
                                final String pdfLink = mFeedListResult.get(position).getAllHistoryObject().get(j).getValue();
                                tv2.setText("Open Document");
                                tv2.setTextAppearance(context, R.style.ButtonSalesCRM);
                                tv2.setBackgroundColor(ContextCompat.getColor(context, R.color.search_text_color));
                                tv2.setPadding(10,10,10,10);
                                tv2.setTextSize(15);
                                //tv2.setCompoundDrawablePadding(5);
                                //tv2.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableAttach, null);
                                tv2.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(context, ProformaPdfActivity.class);
                                        pref.setProformaPdfLink(pdfLink);
                                        intent.putExtra("from_logs", true);
                                        context.startActivity(intent);
                                    }
                                });
                            } else {
                                tv2.setText(mFeedListResult.get(position).getAllHistoryObject().get(j).getValue());
                            }
                        } else {
                            tv1.setText("---");
                            tv2.setText("---");
                        }
                        ((HistoryHolder) holder).ll_cardview_inner_nested[j].addView(tv1);
                        ((HistoryHolder) holder).ll_cardview_inner_nested[j].addView(tv2);
                        ((HistoryHolder) holder).llCardViewInner.addView(((HistoryHolder) holder).ll_cardview_inner_nested[j]);

                    }
                } else {
                    //((HistoryHolder) holder).tvActionAssign.setText("----");
                    TextView tvEmpty = new TextView(context);
                    tvEmpty.setTextColor(Color.parseColor("#4a4a4a"));
                    tvEmpty.setText("----");
                    tvEmpty.setVisibility(View.GONE);
                    ((HistoryHolder) holder).llCardViewInner.addView(tvEmpty);
                    //((HistoryHolder) holder).tvActionAssign.setVisibility(View.VISIBLE);
                }

                if (mFeedListResult.get(position).getLogged_time() != null) {
                    String dateTime = mFeedListResult.get(position).getLogged_time();

                    try {
                        //Date initDate = new SimpleDateFormat("dd MMM yyyy hh:mm aa").parse(dateTime);
                        Date initDate = tryParse(dateTime);
                        String dateString = new SimpleDateFormat("MM-dd-yyyy").format(initDate);
                        Date secondDate = new SimpleDateFormat("MM-dd-yyyy").parse(dateString);
                        SimpleDateFormat formatter = new SimpleDateFormat("EEE dd/MM");
                        SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm aa");
                        String parsedTime = timeFormatter.format(initDate);
                        String parsedDate = formatter.format(initDate);
                        String parsedDay = (new SimpleDateFormat("EEE")).format(initDate);
                        ((HistoryHolder) holder).tvDate.setText(parsedDate);
                        ((HistoryHolder) holder).tvTime.setText(parsedTime);
                        //((HistoryHolder) holder).tvDays.setText(parsedDay);
                        ((HistoryHolder) holder).tvDays.setText(getDateDiffString(firstDate, secondDate));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    ((HistoryHolder) holder).tvDate.setText("---");
                    ((HistoryHolder) holder).tvTime.setText("---");
                    ((HistoryHolder) holder).tvDays.setText("---");
                }


            }


        } else if (holder instanceof VHHeader) {
            if (position == 0) {
                if (mFeedList.size() != 0) {
                    ((VHHeader) holder).tvheader.setText("Planned Tasks");
                    ((VHHeader) holder).tvheader.setVisibility(View.VISIBLE);
                } else {
                    ((VHHeader) holder).tvheader.setVisibility(View.GONE);
                }
            } else if (position == mFeedList.size() + 1) {
                //System.out.println("NO Draw:- "+mFeedListResult.size() + ", "+historyResponse.size());
                if (mFeedListResult.size() != 0) {
                    ((VHHeader) holder).tvheader.setText("Past Tasks");
                    // ((VHHeader) holder).tvheader.setPadding(0, 5, 0,0);
                    ((VHHeader) holder).tvheader.setVisibility(View.VISIBLE);
                    ((VHHeader) holder).progressBar.setVisibility(View.GONE);
                    ((VHHeader) holder).progressBar.clearAnimation();
                } else {
                    if (isInternetAvailable && historyResponse.size() == 0) {
                        ((VHHeader) holder).tvheader.setVisibility(View.GONE);
                        ((VHHeader) holder).progressBar.setVisibility(View.VISIBLE);
                        ObjectAnimator animation = ObjectAnimator.ofInt(((VHHeader) holder).progressBar, "progress", 0, 100); // see this max value coming back here, we animale towards that value
                        animation.setDuration(1000); //in milliseconds (time dosen't work for now)
                        animation.setInterpolator(new DecelerateInterpolator());
                        animation.start();
                    } else {
                        ((VHHeader) holder).tvheader.setVisibility(View.GONE);
                        ((VHHeader) holder).progressBar.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

   /* //Firebase Analytics
    private void callAnalytics(String key, String value, String event) {
        Bundle params = new Bundle();
        params.putString(key, value);
        SalesCRMApplication.getFirebaseAnalytics().logEvent(event, params);
    }*/

    @Override
    public int getItemCount() {
        return mFeedList.size() + mFeedListResult.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0 || position == mFeedList.size() + 1) {
            return HEADER_VIEW;
        } else if (position < mFeedList.size() + 1) {
            return CONTENT_VIEW;
            // } else if(mFeedListResult.size()>0){
        } else {
            return HISTORY_VIEW;
        }

    }

    private class ItemHolder extends RecyclerView.ViewHolder {

        //public   View parentLayout;
        View timeline_line_view2, timeline_circle_view;
        LinearLayout llitem_timeline_expanded, lltimeline_view;
        TextView tvActionName,tv_action_plan_reschedule,tv_action_plan_done;
        TextView tvActionAssigned[];
        TextView tvActionTag[];
        ImageView imageview_expand_timeline_card, imageview_minimize_expand_timeline_card;
        CardView card_item_timeline;
        TextView tvExpandActionName, tvExpandAssignName;
        LinearLayout llCardview, llCardViewInner, llexpand_cardview, llExpandCardViewInner;
        LinearLayout ll_cardview_inner_nested[], llexapnd_cardview_inner_nested[];
        TextView tvDays, tvDate, tvTime;
        TextView tvDaysExpanded, tvTimeExpanded, tvDateExpanded;
        ImageView iReschedule, iDone;
        LinearLayout llActionPlanReschedule;

        public ItemHolder(View itemView) {
            super(itemView);
            // parentLayout = itemView;
            timeline_line_view2 = (View) itemView.findViewById(R.id.timeline_line_view2);
            timeline_circle_view = (View) itemView.findViewById(R.id.timeline_circle_view);
            llitem_timeline_expanded = (LinearLayout) itemView.findViewById(R.id.llitem_timeline_expanded);
            lltimeline_view = (LinearLayout) itemView.findViewById(R.id.lltimeline_view);
            card_item_timeline = (CardView) itemView.findViewById(R.id.card_item_timeline);
            imageview_expand_timeline_card = (ImageView) itemView.findViewById(R.id.imageview_expand_timeline_card);
            imageview_minimize_expand_timeline_card = (ImageView) itemView.findViewById(R.id.imageview_minimize_expand_timeline_card);
            llCardview = (LinearLayout) itemView.findViewById(R.id.ll_cardview);
            llCardViewInner = (LinearLayout) llCardview.findViewById(R.id.ll_cardview_inner);
            llexpand_cardview = (LinearLayout) itemView.findViewById(R.id.llexpand_cardview);
            llExpandCardViewInner = (LinearLayout) llexpand_cardview.findViewById(R.id.llexpand_cardview_inner);

            tvActionName = (TextView) itemView.findViewById(R.id.tvaction_name);
            // tvActionAssigned = (TextView) itemView.findViewById(R.id.tvaction_assigned);

            tvExpandActionName = (TextView) llitem_timeline_expanded.findViewById(R.id.tvexpand_action_name);
            tvExpandAssignName = (TextView) llitem_timeline_expanded.findViewById(R.id.tvexpand_action_assigned);

            tvDays = (TextView) itemView.findViewById(R.id.tvdaytimeline);
            tvDate = (TextView) itemView.findViewById(R.id.tvdatetimeline);
            tvTime = (TextView) itemView.findViewById(R.id.tvtimetimeline);

            tvDaysExpanded = (TextView) llitem_timeline_expanded.findViewById(R.id.tvday_expanded);
            tvDateExpanded = (TextView) llitem_timeline_expanded.findViewById(R.id.tvdate_expanded);
            tvTimeExpanded = (TextView) llitem_timeline_expanded.findViewById(R.id.tvtime_expanded);
            tv_action_plan_reschedule = (TextView) itemView.findViewById(R.id.tv_action_plan_reschedule);
            iReschedule = (ImageView) itemView.findViewById(R.id.img_action_plan_reschedule);
            tv_action_plan_done  = (TextView) llitem_timeline_expanded.findViewById(R.id.tv_action_plan_done);

            iDone = (ImageView) itemView.findViewById(R.id.img_action_plan_done);
            llActionPlanReschedule = (LinearLayout) itemView.findViewById(R.id.ll_action_plan_reschedule);
            iReschedule.setVisibility(View.GONE);
            tv_action_plan_reschedule.setVisibility(View.GONE);


        }

    }


    private class HistoryHolder extends RecyclerView.ViewHolder implements SeekBar.OnSeekBarChangeListener{

        View timeline_line_view2, timeline_circle_view;
        LinearLayout llitem_timeline_expanded, lltimeline_view;
        TextView tvActionName, tvRemarks, tvHistoryLocation;
        RelativeLayout relHistoryLocationView;
        TextView tvActionAssigned[];
        TextView tvActionAssign;
        ImageView imageview_expand_timeline_card, imageview_minimize_expand_timeline_card,img_incoming_or_outgoing;
        CardView card_item_timeline;
        TextView tvExpandActionName, tvExpandAssignName;
        LinearLayout llCardview, llCardViewInner;
        TextView tvDate, tvTime, tvDays;
        LinearLayout ll_cardview_inner_nested[];
        LinearLayout ll_history_inner;
        RelativeLayout rl_timeline_holder;
        FrameLayout rl_audio_playback,frame_media_loader;
        ImageButton btnPlay;
        SeekBar audioSeekBar;
        TextView songTotalDurationLabel;
        AudioCallbacks mAudioCallbacks;


        public HistoryHolder(View itemView) {
            super(itemView);
            // parentLayout = itemView;
            timeline_line_view2 = (View) itemView.findViewById(R.id.timeline_line_view2);
            timeline_circle_view = (View) itemView.findViewById(R.id.timeline_circle_view);
            llitem_timeline_expanded = (LinearLayout) itemView.findViewById(R.id.llitem_timeline_expanded);
            lltimeline_view = (LinearLayout) itemView.findViewById(R.id.lltimeline_view);
            card_item_timeline = (CardView) itemView.findViewById(R.id.card_item_timeline);
            imageview_expand_timeline_card = (ImageView) itemView.findViewById(R.id.imageview_expand_timeline_card);
            imageview_minimize_expand_timeline_card = (ImageView) itemView.findViewById(R.id.imageview_minimize_expand_timeline_card);
            llCardview = (LinearLayout) itemView.findViewById(R.id.ll_cardview);
            card_item_timeline.setCardBackgroundColor(Color.parseColor("#B3B7D3"));
            llCardViewInner = (LinearLayout) llCardview.findViewById(R.id.ll_cardview_inner);
            rl_audio_playback = (FrameLayout) itemView.findViewById(R.id.rl_audio_playback);
            frame_media_loader = (FrameLayout) itemView.findViewById(R.id.frame_media_loader);
            rl_timeline_holder = (RelativeLayout) itemView.findViewById(R.id.rl_timeline_holder);
            btnPlay = (ImageButton) itemView.findViewById(R.id.btnPlay);
            audioSeekBar = (SeekBar) itemView.findViewById(R.id.songProgressBar);
            //songCurrentDurationLabel = (TextView) itemView.findViewById(R.id.songCurrentDurationLabel);
            songTotalDurationLabel = (TextView) itemView.findViewById(R.id.songTotalDurationLabel);
            img_incoming_or_outgoing = (ImageView) itemView.findViewById(R.id.img_incoming_or_outgoing);

            imageview_expand_timeline_card.setVisibility(View.INVISIBLE);
            imageview_minimize_expand_timeline_card.setVisibility(View.INVISIBLE);
            tvActionName = (TextView) itemView.findViewById(R.id.tvaction_name);
            tvActionAssign = (TextView) itemView.findViewById(R.id.tvaction_assigned);
            tvRemarks = (TextView) itemView.findViewById(R.id.tv_remark);
            tvHistoryLocation = (TextView) itemView.findViewById(R.id.tv_history_location);
            relHistoryLocationView = (RelativeLayout) itemView.findViewById(R.id.rel_history_location_view);

            tvExpandActionName = (TextView) llitem_timeline_expanded.findViewById(R.id.tvexpand_action_name);
            tvExpandAssignName = (TextView) llitem_timeline_expanded.findViewById(R.id.tvexpand_action_assigned);

            tvDate = (TextView) itemView.findViewById(R.id.tvdatetimeline);
            tvTime = (TextView) itemView.findViewById(R.id.tvtimetimeline);
            tvDays = (TextView) itemView.findViewById(R.id.tvdaytimeline);

            ll_history_inner = new LinearLayout(context);
            audioSeekBar.setOnSeekBarChangeListener(this);


            tvHistoryLocation.setPaintFlags(tvHistoryLocation.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

            mAudioCallbacks = new AudioCallbacks() {
                @Override
                public void onUpdate(int percentage) {
                    audioSeekBar.setProgress(percentage);
                    System.out.println("audioSeekBar 0 percentage -" + percentage);
                    if (percentage == 100) {
                        mAudioCallbacks.onStop();
                        btnPlay.setImageResource(R.drawable.btn_play);
                    }
                }

                @Override
                public void onPause() {
                    System.out.println("audioSeekBar 0 - paused" );
                    btnPlay.setImageResource(R.drawable.btn_play);
                    pausePlayingAudio();
                }

                @Override
                public void onStop() {
                    System.out.println("audioSeekBar 0 - stop" );
                    btnPlay.setImageResource(R.drawable.btn_play);
                    stopPlayingAudio();
                }

            };

        }


        void stopPlayingAudio() {
            if (mMediaPlayer != null) {
                if (mMediaPlayer.isPlaying()) {
                    System.out.println("audioSeekBar 0 - stopPlayingAudio()" );
                    updateAudioProgressBar();
                    mMediaPlayer.stop();
                    mMediaPlayer.reset();
                    audioSeekBar.setProgress(0);
                    btnPlay.setImageResource(R.drawable.btn_play);
                }
            }
        }

        void pausePlayingAudio() {
            if (mMediaPlayer != null) {
                if (mMediaPlayer.isPlaying()) {
                    System.out.println("audioSeekBar 0 - pausePlayingAudio()" );
                    mMediaPlayer.pause();
                     updateAudioProgressBar();
                     mAudioCallbacks.onPause();
                }
            }
        }

        void playingAudio(String url) {
            System.out.println("audioSeekBar 0 - playingAudio() - "+mMediaPlayer );
            if (mMediaPlayer != null) {
                try {
                    frame_media_loader.setVisibility(View.VISIBLE);
                    if(mMediaPlayer.isPlaying()) {
                        System.out.println("audioSeekBar 0 - pauseOldAudio");
                        mMediaPlayer.pause();
                        mMediaPlayer.stop();
                        audioSeekBar.setProgress(0);
                        btnPlay.setImageResource(R.drawable.btn_play);
                    }

                        System.out.println("audioSeekBar 0 - playingAudio 00");
                        mMediaPlayer.reset();
                        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        mMediaPlayer.setDataSource(url);
                        btnPlay.setImageResource(R.drawable.btn_pause);
                        mMediaPlayer.prepareAsync();
                        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                frame_media_loader.setVisibility(View.INVISIBLE);
                                // set Progress bar values
                                audioSeekBar.setProgress(0);
                                audioSeekBar.setMax(100);
                                updateAudioProgressBar();
                                mp.start();
                                System.out.println("audioSeekBar 0 - AudioStarted");
                                pushCleverTap();
                            }
                        });
                        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                btnPlay.setImageResource(R.drawable.btn_play);
                                //System.out.println("audioSeekBar 0 - OnCompletion()");
                                audioSeekBar.setProgress(0);
                                //mMediaPlayer.pause();
                                //mMediaPlayer.stop();
                            }
                        });

                } catch (IllegalArgumentException | IllegalStateException | IOException e) {
                    e.printStackTrace();
                }
            }
        }

        void updateAudioProgressBar() {
            System.out.println("audioSeekBar 0 - updateAudioProgressBar- ");
            mHandler.postDelayed(mUpdateTimeTask, 100);
        }

        public void setActive() {
            if (!mActive) {
                mActive = true;
                //removeCallbacks(mUpdateTimeProgressBar);
                mHandler.removeCallbacks(mUpdateTimeTask);
                audioSeekBar.setProgress(mMediaPlayer.getCurrentPosition());
                audioSeekBar.setVisibility(View.VISIBLE);
                mHandler.postDelayed(mUpdateTimeTask, 100);
            }
        }

        public void setInactive() {
            if (mActive) {
                mActive = false;
                mHandler.removeCallbacks(mUpdateTimeTask);
                audioSeekBar.setProgress(0);
            }
        }


        /**
         * Background Runnable thread
         */
        private Runnable mUpdateTimeTask = new Runnable() {
            public void run() {

                System.out.println("audioSeekBar 0 - mMediaPlayer- "+mMediaPlayer);

                if(mMediaPlayer != null) {

                    System.out.println("audioSeekBar 0 - FRAGMENT_DESTROYED- "+FRAGMENT_DESTROYED);

                    if(!FRAGMENT_DESTROYED){

                     System.out.println("audioSeekBar 0 - mUpdateTimeTask()");
                      try {
                        if (mMediaPlayer.isPlaying()) {
                            long totalDuration = mMediaPlayer.getDuration();
                            long currentDuration = mMediaPlayer.getCurrentPosition();
                            //audioCurrentDurationAudio.setText(UtilsTime.getFileTime(currentDuration));
                            songTotalDurationLabel.setText("" + Util.milliSecondsToTimer(currentDuration));
                            //System.out.println("audioSeekBar 0 - totalDuration-" + totalDuration);
                            int progress = UtilsTime.getProgressPercentage(currentDuration, totalDuration);
                            mAudioCallbacks.onUpdate(progress);
                            mHandler.postDelayed(this, 100);
                        }
                      } catch (Exception e) {
                            }

                  }else{
                        mHandler.removeCallbacks(mUpdateTimeTask);
                        stopAudio();
                    }
                }else{
                    mHandler.removeCallbacks(mUpdateTimeTask);
                    stopAudio();
                }
            }
        };

        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            System.out.println("audioSeekBar 0 - ProgressChanged- "+seekBar.getProgress());
            if(FRAGMENT_DESTROYED) {
                mHandler.removeCallbacks(mUpdateTimeTask);
                stopAudio();
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            System.out.println("audioSeekBar 0 - StartTrackingTouch- "+seekBar.getProgress());
            mHandler.removeCallbacks(mUpdateTimeTask);
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            if(mMediaPlayer != null) {
                int totalDuration = mMediaPlayer.getDuration();
                int currentPosition = UtilsTime.progressToTimer(seekBar.getProgress(), totalDuration);
                System.out.println("audioSeekBar 0 - StopTrackingTouch- " + currentPosition);
                mMediaPlayer.seekTo(currentPosition);
                updateAudioProgressBar();
            }
        }

    }

    class VHHeader extends RecyclerView.ViewHolder {

        TextView tvheader;
        public View layoutview;
        View timeline_line_view_header;
        ProgressBar progressBar;

        public VHHeader(View itemView) {
            super(itemView);
            layoutview = itemView;
            timeline_line_view_header = (View) layoutview.findViewById(R.id.timeline_line_view_header);
            tvheader = (TextView) layoutview.findViewById(R.id.tvheader);
            progressBar = (ProgressBar) layoutview.findViewById(R.id.progressBarTimeLine);
        }
    }

    public static String getDateDiffString(Date dateOne, Date dateTwo) {
        long timeOne = dateOne.getTime();
        long timeTwo = dateTwo.getTime();
        long oneDay = 1000 * 60 * 60 * 24;
        long day = (timeTwo - timeOne) / oneDay;

        return "Day " + (day + 1);
    }

    public StringBuilder convertKeysIntoReadableForm(String JsonKey) {
        String[] strArray = JsonKey.split(" ");
        StringBuilder stringKey = new StringBuilder();
        for (String s : strArray) {
            String caps = s.substring(0, 1).toUpperCase() + s.substring(1);
            stringKey.append(caps + " ");
        }
        return stringKey;
    }

    Date tryParse(String dateString) {
        for (String formatString : formatStrings) {
            try {
                return new SimpleDateFormat(formatString).parse(dateString);
            } catch (ParseException e) {
            }
        }

        return null;
    }
    public interface callToParentListener{
        void onChangeTabPosition(int i);
    }

    public void stopAudio() {
        System.out.println("audioSeekBar 0 - stopAudio()");
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()) {
                System.out.println("audioSeekBar 0 - stopAudio Playing");
                mMediaPlayer.stop();
                mMediaPlayer.reset();
                mMediaPlayer.release();
            }
            System.out.println("audioSeekBar 0 - stopAudio set null");
            mMediaPlayer = null;
        }
    }

    public void onPlayerClose(boolean status){
        System.out.println("audioSeekBar 0 - onPlayerClose - " +status);
        FRAGMENT_DESTROYED = status;
        if(status) {
            stopAudio();
        }
    }

    private void pushCleverTap() {
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_RECORDINGS);
    }

}
