

package com.salescrm.telephony.response;

import java.util.List;

public class GetPipelineResponse {

    private String statusCode;

    private String message;

    private List<Result> result;

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {

        private String lead_id;

        private List<PipelineStages> pipelineStages;

        private List<LostReasons> lostReasons;

        private List<DroppedReasons> droppedReasons;

        public String getLead_id() {
            return lead_id;
        }

        public void setLead_id(String lead_id) {
            this.lead_id = lead_id;
        }

        public List<PipelineStages> getPipelineStages() {
            return pipelineStages;
        }

        public void setPipelineStages(List<PipelineStages> pipelineStages) {
            this.pipelineStages = pipelineStages;
        }

        public List<LostReasons> getLostReasons() {
            return lostReasons;
        }

        public void setLostReasons(List<LostReasons> lostReasons) {
            this.lostReasons = lostReasons;
        }

        public List<DroppedReasons> getDroppedReasons() {
            return droppedReasons;
        }

        public void setDroppedReasons(List<DroppedReasons> droppedReasons) {
            this.droppedReasons = droppedReasons;
        }


        public class PipelineStages {
            private String pipeline_name;

            private String pipeline_id;

            private List<Stages> stages;

            public String getPipeline_name() {
                return pipeline_name;
            }

            public void setPipeline_name(String pipeline_name) {
                this.pipeline_name = pipeline_name;
            }

            public String getPipeline_id() {
                return pipeline_id;
            }

            public void setPipeline_id(String pipeline_id) {
                this.pipeline_id = pipeline_id;
            }

            public List<Stages> getStages() {
                return stages;
            }

            public void setStages(List<Stages> stages) {
                this.stages = stages;
            }
                }

        public class LostReasons {
            private String id;

            private List<ChildReasons> childReasons;

            private String reason;

            private String sequence;

            private String input_type;

            private String parent_closure_reason;

            private String processed;

            public String getProcessed() {
                return processed;
            }

            public void setProcessed(String processed) {
                this.processed = processed;
            }

            public String getSequence() {
                return sequence;
            }

            public void setSequence(String sequence) {
                this.sequence = sequence;
            }

            public String getInput_type() {
                return input_type;
            }

            public void setInput_type(String input_type) {
                this.input_type = input_type;
            }

            public String getParent_closure_reason() {
                return parent_closure_reason;
            }

            public void setParent_closure_reason(String parent_closure_reason) {
                this.parent_closure_reason = parent_closure_reason;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public List<ChildReasons> getChildReasons() {
                return childReasons;
            }

            public void setChildReasons(List<ChildReasons> childReasons) {
                this.childReasons = childReasons;
            }

            public String getReason() {
                return reason;
            }

            public void setReason(String reason) {
                this.reason = reason;
            }
        }

        public class DroppedReasons {
            private String id;

            private List<ChildReasons> childReasons;

            private String reason;

            private String sequence;

            private String input_type;

            private String parent_closure_reason;

            private String processed;

            public String getProcessed() {
                return processed;
            }

            public void setProcessed(String processed) {
                this.processed = processed;
            }

            public String getSequence() {
                return sequence;
            }

            public void setSequence(String sequence) {
                this.sequence = sequence;
            }

            public String getInput_type() {
                return input_type;
            }

            public void setInput_type(String input_type) {
                this.input_type = input_type;
            }

            public String getParent_closure_reason() {
                return parent_closure_reason;
            }

            public void setParent_closure_reason(String parent_closure_reason) {
                this.parent_closure_reason = parent_closure_reason;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public List<ChildReasons> getChildReasons() {
                return childReasons;
            }

            public void setChildReasonsg(List<ChildReasons> childReasons) {
                this.childReasons = childReasons;
            }

            public String getReason() {
                return reason;
            }

            public void setReason(String reason) {
                this.reason = reason;
            }
        }
        public class ChildReasons {
            private String id;

            private String processed;

            private List<ChildReasons> childReasons;

            private String reason;

            private String sequence;

            private String input_type;

            private String parent_closure_reason;

            public String getId ()
            {
                return id;
            }

            public void setId (String id)
            {
                this.id = id;
            }

            public String getProcessed ()
            {
                return processed;
            }

            public void setProcessed (String processed)
            {
                this.processed = processed;
            }

            public List<ChildReasons> getChildReasons ()
            {
                return childReasons;
            }

            public void setChildReasons (List<ChildReasons> childReasons)
            {
                this.childReasons = childReasons;
            }

            public String getReason ()
            {
                return reason;
            }

            public void setReason (String reason)
            {
                this.reason = reason;
            }

            public String getSequence ()
            {
                return sequence;
            }

            public void setSequence (String sequence)
            {
                this.sequence = sequence;
            }

            public String getInput_type ()
            {
                return input_type;
            }

            public void setInput_type (String input_type)
            {
                this.input_type = input_type;
            }

            public String getParent_closure_reason ()
            {
                return parent_closure_reason;
            }

            public void setParent_closure_reason (String parent_closure_reason)
            {
                this.parent_closure_reason = parent_closure_reason;
            }
        }

        public class Stages {
            private String stage_name;

            private String category_id;

            private String stage_id;

            private String car_model;

            private List<Nested_stages> nested_stages;

            public String getCar_model() {
                return car_model;
            }

            public void setCar_model(String car_model) {
                this.car_model = car_model;
            }

            public List<Nested_stages> getNested_stages() {
                return nested_stages;
            }

            public void setNested_stages(List<Nested_stages> nested_stages) {
                this.nested_stages = nested_stages;
            }

            public String getStage_name() {
                return stage_name;
            }

            public void setStage_name(String stage_name) {
                this.stage_name = stage_name;
            }

            public String getCategory_id() {
                return category_id;
            }

            public void setCategory_id(String category_id) {
                this.category_id = category_id;
            }

            public String getStage_id() {
                return stage_id;
            }

            public void setStage_id(String stage_id) {
                this.stage_id = stage_id;
            }
        }

        public class Nested_stages {
            private String stage_name;

            private String stage_id;

            public String getStage_name() {
                return stage_name;
            }

            public void setStage_name(String stage_name) {
                this.stage_name = stage_name;
            }

            public String getStage_id() {
                return stage_id;
            }

            public void setStage_id(String stage_id) {
                this.stage_id = stage_id;
            }
        }

    }

}







