import React, { Component } from 'react';
import { View, StyleSheet, Modal, Text, TouchableOpacity, Image, TextInput, FlatList, TouchableWithoutFeedback } from 'react-native';

export default class PickerList extends Component {
  constructor(props) {
    super(props)
    this.state = { filter: '' };
  }

  getFilteredData(listItems) {
    if (!this.state.filter) {
      return listItems;
    }
    return listItems.filter(item => item.text.toLowerCase().includes(this.state.filter.toLowerCase()));
  }

  render() {
    let data = this.props.data;
    let title = "Select";
    let fromId = null;
    let listItems = [];
    if (data) {
      listItems = data.listItems;
      fromId = data.fromId;
      if (data.title) {
        title = data.title;
      }

    }
    let dataListFiltered = this.getFilteredData(listItems);
    let visible = this.props.visible;
    let showFilter = this.props.showFilter;
    if (listItems) {
      showFilter = showFilter;
    }
    return (
      <Modal
        animationType="slide"
        visible={visible}
        transparent={true}
        onRequestClose={() => {
          this.setState({ filter: '' })
          this.props.onCancel()
        }}>

        <TouchableWithoutFeedback onPress={() => {
          this.setState({ filter: '' })
          this.props.onCancel()
        }}>
          <View style={styles.overlay}>
            <TouchableWithoutFeedback onPress={() => {

            }}>
              <View style={[styles.main, { flex: showFilter ? 1 : 0 }]}>
                <View style={{ flexDirection: 'row', borderRadius: 5, justifyContent: 'space-between', paddingTop: 20, paddingLeft: 20, paddingRight: 20, alignItems: 'center' }}>
                  <Text style={styles.title}>{title}</Text>
                  <TouchableOpacity
                    hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}
                    onPress={() => {
                      this.setState({ filter: '' })
                      this.props.onCancel()
                    }}>
                    <Image
                      source={require('../images/ic_close_black.png')}
                      style={{ width: 20, height: 20, alignSelf: 'flex-end' }}
                    />
                  </TouchableOpacity>
                </View>

                {showFilter &&
                  <TextInput
                    placeholder="Search"
                    placeholderTextColor="#e0e0e0"
                    style={{ fontSize: 16, marginTop: 10, marginLeft: 20, marginRight: 20, height: 40, borderColor: '#e0e0e0', borderWidth: 1, borderRadius: 5, padding: 10 }}
                    onChangeText={(filter) => this.setState({ filter })}
                    value={this.state.filter}
                  />}


                <View style={styles.lineMain} />
                <FlatList

                  data={dataListFiltered}
                  extraData={this.state}
                  renderItem={({ item, index }) => {
                    let isSelectedText = (data.selected && item.id == data.selected.id) ? true : false;
                    return (
                      <TouchableOpacity onPress={() => {
                        this.setState({ filter: '' })
                        this.props.onSelect(fromId, item, data.payload)
                      }}>
                        <View>
                          <Text style={[styles.listItem, { color: isSelectedText ? '#007fff' : '#303030' }]}>{item.text}</Text>
                          {index != dataListFiltered.length - 1 && <View style={styles.line} />}

                        </View>
                      </TouchableOpacity>
                    )
                  }}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.8)',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30

  },
  main: {
    backgroundColor: '#fff',
    borderRadius: 5,
    width: '100%',
    maxHeight: '90%',
  },
  title: {
    fontSize: 18,
    color: '#303030',
    fontWeight: 'bold'
  },
  listItem: {

    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
    fontSize: 16,
    color: '#303030'
  },
  line: {
    backgroundColor: '#e0e0e0',
    height: 1,
  },
  lineMain: {
    marginTop: 15,
    backgroundColor: '#e0e0e0',
    height: 1,
    borderRadius: 5,
  }
});
