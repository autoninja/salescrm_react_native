package com.salescrm.telephony.model.NewFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prateek on 4/8/18.
 */

public class NewFilterRequest {
    private List<Filters> filters = new ArrayList<>();

    public List<Filters> getFilters() {
        return filters;
    }

    public void setFilters(List<Filters> filters) {
        this.filters = filters;
    }
}
