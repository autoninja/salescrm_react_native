package com.salescrm.telephony.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ClientCertRequest;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.EmailSmsActivity;
import com.salescrm.telephony.db.CustomerEmailId;
import com.salescrm.telephony.db.DealerDataDb;
import com.salescrm.telephony.db.EmailResult;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

import static android.app.Activity.RESULT_OK;


public class EmailsFragment extends Fragment {
    private static RealmResults<EmailResult> smsEmailResponseone;
    Preferences pref;
    private Realm realm;
    EditText email_sub;
    WebView email_body;
    Spinner emailspinner1, emailspinner2;
    private EmailResult emailResult;
    private OnDataPassEmail onDataPassEmail;
    private UserDetails userDetails;
    private SalesCRMRealmTable salesCRMRealmTable;
    private Drawable drawableAttach;
    private EditText edtEmail;

    String emailAddress = "", emailType = "", emailContent = "", emailSubject = "", emailAttachment = "", emailCutomer = "", emailNotiFicationId;

    //Button btnAttachFile;
    TextView tvAttachment;
    ImageView imgSaveWebView;
    private DealerDataDb dealerDataDb;

    private static final int PICKED_FILE_RESULT_CODE = 1;

    public static EmailsFragment newInstance(RealmResults<EmailResult> SmsEmailResponse) {
        EmailsFragment emailss = new EmailsFragment();
        return emailss;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity a;
        if (context instanceof EmailSmsActivity) {
            a = (EmailSmsActivity) context;
            try {
                onDataPassEmail = (OnDataPassEmail) a;
            } catch (ClassCastException e) {
                throw new ClassCastException(a.toString() + " must implement OnDataPassMail");
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.email, container, false);
        final FragmentActivity c = getActivity();
        pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(getActivity());
        emailspinner1 = (Spinner) view.findViewById(R.id.emailspinner1);
        emailspinner2 = (Spinner) view.findViewById(R.id.emailspinner2);
        email_sub = (EditText) view.findViewById(R.id.email_sub);
        email_body = (WebView) view.findViewById(R.id.email_body);
        edtEmail = (EditText) view.findViewById(R.id.emailEdittext);
       // btnAttachFile = (Button) view.findViewById(R.id.attach_file);
        tvAttachment = (TextView) view.findViewById(R.id.attachment);
        imgSaveWebView = (ImageView) view.findViewById(R.id.save_webview);
        drawableAttach = VectorDrawableCompat
                .create(getActivity().getResources(), R.drawable.ic_attachment, null);
        final List<String> listemail = new ArrayList<String>();


        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();

        smsEmailResponseone = realm.where(EmailResult.class).equalTo("recipient_type_id", "1").findAll();
        final RealmResults<CustomerEmailId> emailid = realm.where(CustomerEmailId.class).equalTo("leadId", Integer.parseInt(pref.getLeadID())).findAll();

        for (int i = 0; i < emailid.size(); i++) {
            listemail.add(i, emailid.get(i).getEmail());
        }

        if(!pref.getProformaPdfLink().equalsIgnoreCase("")){
            edtEmail.setVisibility(View.VISIBLE);
            emailspinner1.setVisibility(View.GONE);
            edtEmail.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailAddress = edtEmail.getText().toString();
                sendMailData();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }else{
            emailspinner1.setVisibility(View.VISIBLE);
            edtEmail.setVisibility(View.GONE);
        }
        ArrayAdapter<String> dataAdapteremail = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, listemail);
        dataAdapteremail.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        emailspinner1.setAdapter(dataAdapteremail);
        emailspinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                emailAddress = emailspinner1.getItemAtPosition(emailspinner1.getSelectedItemPosition()).toString();
                sendMailData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final List<String> list1email1 = new ArrayList<String>();
        for (int j = 0; j < smsEmailResponseone.size(); j++) {
            list1email1.add(j, smsEmailResponseone.get(j).getName());
        }

        ArrayAdapter<String> dataAdapteremail1 = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, list1email1);
        dataAdapteremail1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        emailspinner2.setAdapter(dataAdapteremail1);
        emailspinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Spinner position" + position);
                String typeId = emailspinner2.getSelectedItem().toString();

                emailResult = realm.where(EmailResult.class).endsWith("name", typeId).findFirst();
                dealerDataDb = realm.where(DealerDataDb.class).findFirst();
                userDetails = realm.where(UserDetails.class).findFirst();

                if (emailResult.getSubject() != null){
                    String subString = emailResult.getSubject().replace("{{dealerName}}", dealerDataDb.getDname())
                            .replace("{{dealerCity}}", dealerDataDb.getDealerCity());
                    emailSubject = subString;
                }
                email_sub.setText(emailSubject);
                emailType = emailResult.getId();
                if(emailResult.getContent() != null){
                    String subString1 = emailResult.getContent().replace("{{dealerName}}", dealerDataDb.getDname())
                            .replace("{{custSupportEmailId}}", dealerDataDb.getCustSupportEmailId())
                            .replace("{{dealerAddress}}", dealerDataDb.getDealerAddress())
                            .replace("{{creName}}", userDetails.getName())
                            .replace("{{creMobNo}}", userDetails.getMobile())
                            .replace("{{custName}}", salesCRMRealmTable.getCustomerName())
                            .replace("{{ccmMobNo}}", dealerDataDb.getCcmMobNo())
                            .replace("{{ccmEmailId}}", dealerDataDb.getCcmEmailId())
                            .replace("{{hotlineNo}}", dealerDataDb.getHotlineNo())
                            .replace("{{dealerWebsite}}", dealerDataDb.getDealerWebsite())
                            .replace("{{dealerAddress}}", dealerDataDb.getDealerAddress());

                    emailContent = subString1;
                }
                //emailContent = emailResult.getContent();
                emailCutomer = salesCRMRealmTable.getCustomerName();
                emailNotiFicationId = emailResult.getNotification_type_id();

                final String mimeType = "text/html";
                final String encoding = "UTF-8";
                email_body.setBackgroundColor(Color.parseColor("#F9F9F9"));
                email_body.getSettings().setJavaScriptEnabled(true);
                email_body.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                email_body.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
                email_body.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);
                        //email_body.loadData("javascript:window.HTMLCONTENT.processHTML(this.document.locationNamesList.href = 'source://' + encodeURI(document.documentElement.outerHTML);",mimeType , encoding);
                        email_body.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");

                    }
                });
                email_body.loadData("<html contenteditable=\"true\">" + emailContent + "</hmtl>", mimeType, encoding);
                sendMailData();
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        email_sub.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                emailSubject = email_sub.getText().toString();
                sendMailData();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

      /*  btnAttachFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("file*//*");
                startActivityForResult(intent, PICKED_FILE_RESULT_CODE);
            }
        });*/

        /*tvAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvAttachment.setVisibility(View.GONE);
                emailAttachment = "";
                sendMailData();
            }
        });*/

        if(pref.getProformaPdfLink()!= null && !pref.getProformaPdfLink().equalsIgnoreCase("")){
            tvAttachment.setVisibility(View.VISIBLE);
            //tvAttachment.setText(""+ Util.getPdfFileNameFromLink(pref.getProformaPdfLink()));
            tvAttachment.setText("ProformaInvoice.pdf");
            tvAttachment.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableAttach, null);
            tvAttachment.setCompoundDrawablePadding(5);
        }else{
            tvAttachment.setVisibility(View.GONE);
        }

        imgSaveWebView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Your changes have been saved", Toast.LENGTH_SHORT).show();
                email_body.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
                email_body.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICKED_FILE_RESULT_CODE:
                if (resultCode == RESULT_OK) {
                    emailAttachment = data.getData().getPath();
                    tvAttachment.setText(emailAttachment);
                    tvAttachment.setVisibility(View.VISIBLE);
                    sendMailData();
                }
        }
    }

    private void sendMailData() {
        onDataPassEmail.onDataPassEmail(emailAddress,
                emailType, emailContent, emailSubject, emailAttachment, pref.getLeadID(), emailCutomer, emailNotiFicationId);
    }

    public interface OnDataPassEmail {
        void onDataPassEmail(String address, String mailType, String body, String subject, String attachment, String leadId, String customer, String typeID);
    }

    class MyJavaScriptInterface {
        @JavascriptInterface
        @SuppressWarnings("unused")
        public void processHTML(String html) {
            System.out.println("Content Read: - " + html);
            emailContent = html;
            sendMailData();
        }
    }
}
