import React, { Component } from 'react';
import { StyleSheet, ScrollView, Text, View, WebView } from 'react-native';
import { Table, TableWrapper, Row, Cell } from 'react-native-table-component';
import Switch from 'react-native-switch-pro';
import RestClient from '../../../network/rest_client';
import { Button, Dialog, Portal } from 'react-native-paper';
import styles from './communicationsettings.style';

class CommunicationSettings extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dialog : {
                activity_id: null,
                visible : false,
                commType : null,
                template : null,
                header : ''
            },
            loading: true,
            tableHead: [this.getHeaderText('Condition'), this.getHeaderText('SMS'),  this.getHeaderText('EMAIL')],
            tableData: [],
            error: false,
            errMsg: ''
        }
    }

    getHeaderText(text)
    {
        return <Text key={text} style={{fontWeight: 'bold', color: '#2e5e86'}}>{text}</Text>;
    }

    showDialog(commType, data)
    {
        const object = commType === 'sms' ? 'SMS' : 'Email';
        const commState = commType === 'sms' ? data.sms : data.email;
        const verb = commState.enabled ? 'Disable' : 'Enable';
        const subject = data.name;
        const title = verb + ' ' + subject + ' ' + object + ' ?' ;

        this.setState({
            dialog : {
                activity_id: data.activity_id,
                visible : true,
                switchState : commType === 'sms' ? data.sms.enabled : data.email.enabled,
                commType : commType,
                template : commType === 'sms' ? data.sms.template : data.email.template,
                header : title
            }
        });
    }

    handleCancel = () => {
        this.setState({
            dialog : {
                activity_id : null,
                switchState : null,
                visible : false,
                commType : "",
                template : "",
                header : ""
            }
        });
    };

    handleConfirm = () => {
        const newTableState = this.state.tableData.map((item) => {
            if(item.activity_id === this.state.dialog.activity_id) {
                if(this.state.dialog.commType === 'sms') {
                    return {...item, sms : {...item.sms, enabled: !item.sms.enabled}};
                }
                return {...item, email : {...item.email, enabled: !item.email.enabled}};
            }
            return item;
        });
        const restClient = new RestClient();
        restClient.setSmsEmailSettings({
            activity_id: this.state.dialog.activity_id,
            type: this.state.dialog.commType,
            enabled : !this.state.dialog.switchState
        }).then((data) => {
            if(data.result) {
                this.setState({
                    tableData: newTableState,
                    dialog : {
                        visible : false,
                        switchState : null,
                        commType : '',
                        template : '',
                        header : ''
                    }
                });
            } else {
                console.log('Aw Snap! Something went wrong :(');
            }
        }).catch((error) => {
            if(this._mounted) {
                this.setState({
                    isLoading : false,
                    error : true,
                    errMsg : 'Something went wrong :('
                });
            }
        });
    };

    componentDidMount()
    {
        const restClient = new RestClient();
        restClient.getSmsEmailSettings().then((data) => {
            this.setState({
                tableData : data.result.conditions,
                isLoading : false
            })
        }).catch((error) => {
            if(this._mounted) {
                this.setState({
                    isLoading : false,
                    error : true,
                    errMsg : 'Something went wrong :('
                });
            }
        });
    }

    getSubject()
    {
        switch (this.state.dialog.commType) {
            case 'sms' :
                return '';
            case 'email':
                return this.state.dialog.template.subject;
            default:
                return '';
        }
    }

    renderTemplate()
    {
        if(this.state.dialog.switchState) {
            return null;
        }
        let body = null;
        if(!this.state.dialog.visible) {
            body = <Text/>;
        } else if(this.state.dialog.commType === 'sms') {
            body = <Text>{this.state.dialog.template}</Text>;
        } else {
            const subject = '<p><b>Subject :</b>' +  this.getSubject() + '</p>';
            const email = subject + (this.state.dialog.template !== "" ? this.state.dialog.template.content : '');
            body = (
                <ScrollView>
                    <WebView source={{html:  email}} style={{minHeight: 200, maxHeight: 200}}/>
                </ScrollView>
            );
        }
        return <Dialog.ScrollArea style={{
            borderBottomWidth: 1,
            borderBottomColor: '#dcdcdc',
            borderTopWidth: 1,
            borderTopColor: '#dcdcdc',
        }}>{body}</Dialog.ScrollArea>;
    }

    render()
    {
        const state = this.state;
        const radioElement = (commType, condition) => {
            return (<Switch
                value={commType === 'sms' ? condition.sms.enabled : condition.email.enabled}
                height={21}
                width={35}
                backgroundActive={'#00E668'}
                backgroundInactive={'#8c8c8c'}
                circleStyle={{height: 16, width : 16}}
                onAsyncPress={(callback) => {
                    this.showDialog(commType, condition);
                    callback(false, value => this.setState({value}))
                }}
            />)
        };
        return (
            <View style={{backgroundColor: 'transparent', flex: 1}}>
                <ScrollView>
                    <View style={styles.container}>
                        <Table borderStyle={{borderColor: 'transparent'}}>
                            <Row data={state.tableHead} flexArr={[3, 1, 1]} style={styles.head} textStyle={styles.text}/>
                            {
                                state.tableData.map((rowData) => (
                                    <TableWrapper key={rowData.activity_id} style={styles.row} flexArr={[2, 1, 1]}>
                                        <Cell
                                            key={'condition_' + rowData.activity_id}
                                            data={rowData.name}
                                            textStyle={styles.text}
                                            style={{width: '60%'}}
                                        />
                                        <Cell
                                            key={'sms_' + rowData.activity_id}
                                            data={radioElement('sms', rowData)}
                                            textStyle={styles.text}
                                            style={{width: '20%'}}
                                        />
                                        <Cell
                                            key={'email_' + rowData.activity_id}
                                            data={radioElement('email', rowData)}
                                            textStyle={styles.text}
                                            style={{width: '20%'}}
                                        />
                                    </TableWrapper>
                                ))
                            }
                        </Table>
                    </View>
                    <Text style={{fontStyle: 'italic', textAlign: 'center', marginTop: 20}}>Outgoing SMS will be charged at 14 p / SMS</Text>
                </ScrollView>
                <Portal>
                    <Dialog
                        visible={this.state.dialog.visible}
                        dismissable={false}
                        onDismiss={this.handleCancel}
                    >
                        <Dialog.Title style={{color: '#2e5e86'}}>{this.state.dialog.header}</Dialog.Title>
                        {
                            this.renderTemplate()
                        }
                        <Dialog.Actions style={{padding:0}}>
                            <Button
                                onPress={this.handleCancel}
                                color='#7a7a7a'
                                style={{...styles.button, backgroundColor: '#ffffff'}}
                            >
                                Cancel
                            </Button>
                            <Button
                                onPress={this.handleConfirm}
                                color='#ffffff'
                                style={{...styles.button, backgroundColor: '#2196F3'}}
                            >
                                Confirm
                            </Button>
                        </Dialog.Actions>
                    </Dialog>
                </Portal>
            </View>

        )
    }
}

export default CommunicationSettings;
