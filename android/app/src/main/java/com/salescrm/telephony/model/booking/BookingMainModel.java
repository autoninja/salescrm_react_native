package com.salescrm.telephony.model.booking;

import android.support.graphics.drawable.VectorDrawableCompat;

import com.salescrm.telephony.utils.Util;

import java.util.Calendar;

import io.realm.Realm;

/**
 * Created by bharath on 22/2/18.
 */

public class BookingMainModel {
    private BookingCarModel bookingCarModel ;
    private BookingCustomerModel bookingCustomerModel;
    private BookingPriceBreakupModel bookingPriceBreakupModel;
    private BookingDiscountModel bookingDiscountModel;
    private BookingSectionModel bookingSectionModel;
    private int stageId;
    private Calendar date;
    private String remarks;
    private VectorDrawableCompat iconSuccess;
    private VectorDrawableCompat iconError;
    private BookingEditWrapperModel bookingEditWrapperModel;
    private Realm realm;
    private BookingExchFinModel bookingExchFinModel;
    private String vin_allocation_status_id;
    private String vin_no;
    private String vin_allocation_status;
    private BookingVinAllocationModel bookingVinAllocationModel;

    public BookingVinAllocationModel getBookingVinAllocationModel() {
        return bookingVinAllocationModel;
    }

    public void setBookingVinAllocationModel(BookingVinAllocationModel bookingVinAllocationModel) {
        this.bookingVinAllocationModel = bookingVinAllocationModel;
    }

    public BookingExchFinModel getBookingExchFinModel() {
        return bookingExchFinModel;
    }

    public void setBookingExchFinModel(BookingExchFinModel bookingExchFinModel) {
        this.bookingExchFinModel = bookingExchFinModel;
    }

    public BookingCarModel getBookingCarModel() {
        return bookingCarModel;
    }

    public void setBookingCarModel(BookingCarModel bookingCarModel) {
        this.bookingCarModel = bookingCarModel;
    }

    public BookingCustomerModel getBookingCustomerModel() {
        return bookingCustomerModel;
    }

    public void setBookingCustomerModel(BookingCustomerModel bookingCustomerModel) {
        this.bookingCustomerModel = bookingCustomerModel;
    }

    public BookingPriceBreakupModel getBookingPriceBreakupModel() {
        return bookingPriceBreakupModel;
    }

    public void setBookingPriceBreakupModel(BookingPriceBreakupModel bookingPriceBreakupModel) {
        this.bookingPriceBreakupModel = bookingPriceBreakupModel;
    }

    public BookingDiscountModel getBookingDiscountModel() {
        return bookingDiscountModel;
    }

    public BookingSectionModel getBookingSectionModel() {
        return bookingSectionModel;
    }

    public void setBookingSectionModel(BookingSectionModel bookingSectionModel) {
        this.bookingSectionModel = bookingSectionModel;
    }

    public void setBookingDiscountModel(BookingDiscountModel bookingDiscountModel) {
        this.bookingDiscountModel = bookingDiscountModel;
    }

    public void setStageId(int stageId) {
        this.stageId = stageId;
    }

    public int getStageId() {
        return stageId;
    }

    public Calendar getDate() {
        return date;
    }

    public String getDateAsSQLDate() {
        return date == null? null : Util.getSQLDateTime(date.getTime());
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getVin_allocation_status() {
        return vin_allocation_status;
    }

    public void setVin_allocation_status(String vin_allocation_status) {
        this.vin_allocation_status = vin_allocation_status;
    }

    public ApiInputBookingDetails getApiInputDetails() {
        ApiInputBookingDetails apiInputBookingDetails = new ApiInputBookingDetails();
        ApiInputCarDetails apiInputCarDetails =  new ApiInputCarDetails(bookingCarModel.getModelId(),
                bookingCarModel.getVariantId(),
                bookingCarModel.getFuelTypeId(),
                bookingCarModel.getColorTypeId(),
                bookingCarModel.getQuantity());


        ApiInputCustomerDetails apiInputCustomerDetails = new ApiInputCustomerDetails(bookingCustomerModel.getBookingName(),
                Util.getSQLDateTimeValidationCheck(bookingCustomerModel.getDateOfBirth()),bookingCustomerModel.getPanNumber(), bookingCustomerModel.getAddress(),
                bookingCustomerModel.getAadhaar(), bookingCustomerModel.getPinCode(),
                bookingCustomerModel.getCareOfType(), bookingCustomerModel.getCareOf(),
                bookingCustomerModel.getBuyerType(),bookingCustomerModel.getCustomerType()+"", bookingCustomerModel.getGst());

        ApiInputPriceBreakup apiInputPriceBreakup = new ApiInputPriceBreakup(bookingPriceBreakupModel.getExShowRoomPrice(),
                bookingPriceBreakupModel.getInsurance(),
                bookingPriceBreakupModel.getRegistration(),
                bookingPriceBreakupModel.getAccessories(),
                bookingPriceBreakupModel.getExtendedWarranty(),
                bookingPriceBreakupModel.getOthers(),
                bookingCarModel.getBookingAmount());

        ApiInputDiscount apiInputDiscount = new ApiInputDiscount(bookingDiscountModel.getCorporate(),
                bookingDiscountModel.getExchangeBonus(),
                bookingDiscountModel.getOemScheme(),
                bookingDiscountModel.getLoyaltyBonus(),
                bookingDiscountModel.getDealer(),
                bookingDiscountModel.getAccessories());

        ApiInputExchFinDetails apiInputExchFinDetails = new ApiInputExchFinDetails(bookingExchFinModel.getFinance(),
                bookingExchFinModel.getExchange());

        ApiInputVinAllocationDetails apiInputVinAllocationDetails = new ApiInputVinAllocationDetails(
                bookingVinAllocationModel.getVin_no(), bookingVinAllocationModel.getVin_allocation_status_id(),
                bookingVinAllocationModel.getVin_allocation_status());

        apiInputBookingDetails.setVin_allocation_details(apiInputVinAllocationDetails);


        apiInputBookingDetails.setCar_details(apiInputCarDetails);
        apiInputBookingDetails.setCustomer_details(apiInputCustomerDetails);
        apiInputBookingDetails.setPrice_breakup(apiInputPriceBreakup);
        apiInputBookingDetails.setDiscounts(apiInputDiscount);
        apiInputBookingDetails.setPayment_details(apiInputExchFinDetails);
        //apiInputBookingDetails.setVin_no(vin_allocation_status_id.equalsIgnoreCase("2")?null:vin_no);
        System.out.println("vin_no_vin_no "+vin_no);
        apiInputBookingDetails.setVin_no(vin_no);
        apiInputBookingDetails.setVin_allocation_status_id(vin_allocation_status_id);
        return apiInputBookingDetails;
    }

    public void setIconSuccess(VectorDrawableCompat iconSuccess) {
        this.iconSuccess = iconSuccess;
    }

    public void setIconError(VectorDrawableCompat iconError) {
        this.iconError = iconError;
    }

    public VectorDrawableCompat getIconSuccess() {
        return iconSuccess;
    }

    public VectorDrawableCompat getIconError() {
        return iconError;
    }

    public BookingEditWrapperModel getBookingEditWrapperModel() {
        return bookingEditWrapperModel;
    }

    public void setBookingEditWrapperModel(BookingEditWrapperModel bookingEditWrapperModel) {
        this.bookingEditWrapperModel = bookingEditWrapperModel;
    }

    public Realm getRealm() {
        return realm;
    }

    public void setRealm(Realm realm) {
        this.realm = realm;
    }

    public String getVin_allocation_status_id() {
        return vin_allocation_status_id;
    }

    public void setVin_allocation_status_id(String vin_allocation_status_id) {
        this.vin_allocation_status_id = vin_allocation_status_id;
    }

    public String getVin_no() {
        return vin_no;
    }

    public void setVin_no(String vin_no) {
        this.vin_no = vin_no;
    }
}
