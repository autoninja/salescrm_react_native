package com.salescrm.telephony.offline;

import android.content.Context;

import com.google.gson.Gson;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.ActivitiesDB;
import com.salescrm.telephony.db.EnqSourceDB;
import com.salescrm.telephony.db.LocationsDB;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.crm_user.ActivityUserDB;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.model.GetCreActivitiesInput;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.CreForActivityResponses;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 9/12/16.
 * Call this to get the user for assign activity
 */
public class FetchActivityUsersDetails implements Callback<CreForActivityResponses> {
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private int countCall = 0;
    private RealmList<UserLocationsDB> locationsList;

    public FetchActivityUsersDetails(OfflineSupportListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
        realm.beginTransaction();
        realm.where(ActivityUserDB.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void call() {
        UserDetails userDetails = realm.where(UserDetails.class).findFirst();
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            List<Integer> dataActivities = new ArrayList<>();


            GetCreActivitiesInput input = new GetCreActivitiesInput();

            RealmResults<ActivitiesDB> activitiesListResult = realm.where(ActivitiesDB.class).findAll();

            for (int i = 0; i < activitiesListResult.size(); i++) {
                dataActivities.add(activitiesListResult.get(i).getId());
            }
            input.setActivities(dataActivities);

            List<Integer> dataLeadSources = new ArrayList<>();
            RealmResults<EnqSourceDB> leadSourcesListResult = realm.where(EnqSourceDB.class).findAll();
            for (int i = 0; i < leadSourcesListResult.size(); i++) {
                dataLeadSources.add(leadSourcesListResult.get(i).getId());
            }
            input.setLeadSources(dataLeadSources);

            List<Integer> dataLocationsUser = new ArrayList<>();
            RealmResults<LocationsDB> locationUserListResult = realm.where(LocationsDB.class).findAll();
            dataLocationsUser.add(-1);
            for (int i = 0; i < locationUserListResult.size(); i++) {
                dataLocationsUser.add(locationUserListResult.get(i).getId());
            }
            input.setLocations(dataLocationsUser);

            input.setMatch("");
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).GetCreForActivities(input, this);
        } else {

            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ACTIVITY_USERS);
        }


    }


    @Override
    public void success(final CreForActivityResponses creForActivityResponses, Response response) {
        Util.updateHeaders(response.getHeaders());
        if (!creForActivityResponses.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + creForActivityResponses.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ACTIVITY_USERS);
        } else {
            if (creForActivityResponses.getResult() != null) {
                System.out.println("Success:1" + creForActivityResponses.getMessage());
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm1) {
                        insertData(realm1, creForActivityResponses.getResult());
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        listener.onActivityUsersFetched(creForActivityResponses);
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ACTIVITY_USERS);
                    }
                });


            } else {
                // relLoader.setVisibility(View.GONE);
                System.out.println("Success:2" + creForActivityResponses.getMessage());
                listener.onOfflineSupportApiError(null, WSConstants.OfflineAPIRequest.ACTIVITY_USERS);

                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }

    private void insertData(Realm realm, List<CreForActivityResponses.Result> data) {
        for (int i = 0; i < data.size(); i++) {
            realm.createObjectFromJson(ActivityUserDB.class, new Gson().toJson(data.get(i)));

        }
       System.out.println("real size: actvity:"+realm.where(ActivityUserDB.class).findAll().size());
    }

    @Override
    public void failure(RetrofitError error) {
        listener.onOfflineSupportApiError(error, WSConstants.OfflineAPIRequest.ACTIVITY_USERS);

    }

}
