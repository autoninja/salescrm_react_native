import React, { Component } from "react";
import { View, ScrollView } from 'react-native';
import styles from './tasksList.styles'


export default class TasksLoader extends Component {

    render() {
        return (
            <View>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>

                </View>
                <ScrollView
                    scrollEnabled={false}
                >
                    <View style={{
                        margin: 4,
                        marginTop: 20,
                        flex: 1,
                        height: 10,
                        borderRadius: 4,
                        backgroundColor: "#ebebeb"
                    }}
                    />

                    <View style={{
                        margin: 4,
                        marginTop: 10,
                        flex: 1,
                        height: 24,
                        borderRadius: 6,
                        backgroundColor: "#ebebeb"
                    }}
                    />

                    {DATA_LOADER.map((item) => {
                        return (

                            <View
                                style={[styles.cardWrapper, { flexDirection: 'column', marginTop: 8, padding: 10, height: 100, flex: 1, backgroundColor: "#fff" }]}
                            >
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                                    <View style={{
                                        flex: 4,
                                        height: 20,
                                        borderRadius: 6,
                                        backgroundColor: "#ebebeb"
                                    }}
                                    />
                                    <View style={{
                                        marginLeft: 10,
                                        flex: 6,
                                        height: 20,
                                        borderRadius: 6,
                                        backgroundColor: "#ebebeb"
                                    }}
                                    />
                                </View>

                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                                    <View style={{
                                        flex: 4,
                                        height: 10,
                                        borderRadius: 6,
                                        backgroundColor: "#ebebeb"
                                    }}
                                    />
                                    <View style={{
                                        marginLeft: 10,
                                        flex: 6,
                                        height: 10,
                                        borderRadius: 6,
                                        backgroundColor: "#ebebeb"
                                    }}
                                    />
                                </View>


                            </View>)
                    })}
                </ScrollView>
            </View>);
    }
}
const DATA_LOADER = [1, 2, 3, 4, 1, 2, 3, 4];