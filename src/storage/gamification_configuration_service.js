import React, { Component } from 'react';
import {realm} from './realm_schemas';

const GamificationConfigurationService = {

    getLocations: () => {
        const realmObj = realm.objects('GamificationLocations');
        let gameLocations = [];
        if(realmObj) {
            realmObj.map((role, index) => {
                gameLocations.push(role);
            });
            return gameLocations;
        }
        return null;
    },

    getSelectedGameLocationName: (id) => {
        console.log("gameLocationsId"+id);
        if(!id) {
            return "";
        }
        let realmObj = realm.objects('GamificationLocations').filtered('id == '+id);
        if(realmObj && realmObj.length>0) {
            return realmObj[0].name;
        }
        return "";
    },
    getSelectedGameLocation: (id) => {
        let realmObj = realm.objects('GamificationLocations').filtered('id == '+id);
        if(realmObj && realmObj.length>0) {
            return realmObj[0];
        }
        return null;
    },
    save: (game_config_data) => {
        realm.write(() =>
        {
            const user = realm.create('GamificationLocations', game_config_data, true);
        });
        const gameLocations = realm.objects('GamificationLocations');
        console.log('gameLocations.length', gameLocations.length);
    },
    deleteAll: () => {
        realm.write(() => {
            // realm.delete(realm.objects('GamificationConfig'));
            realm.delete(realm.objects('GamificationLocations'));
        })
    }

};

export default GamificationConfigurationService;
