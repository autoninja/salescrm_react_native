package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by bharath on 6/12/16.
 */

public class FormObjectsInputs {
    private List<Lead_data> lead_data;

    public List<Lead_data> getLead_data ()
    {
        return lead_data;
    }

    public void setLead_data (List<Lead_data> lead_data)
    {
        this.lead_data = lead_data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lead_data = "+lead_data+"]";
    }
    public class Lead_data
    {
        private String action_id;

        private String scheduled_activity_id;

        private String lead_id;

        private String schedule_type;

        public String getSchedule_type() {
            return schedule_type;
        }

        public void setSchedule_type(String schedule_type) {
            this.schedule_type = schedule_type;
        }

        public String getAction_id ()
        {
            return action_id;
        }

        public void setAction_id (String action_id)
        {
            this.action_id = action_id;
        }

        public String getScheduled_activity_id ()
        {
            return scheduled_activity_id;
        }

        public void setScheduled_activity_id (String scheduled_activity_id)
        {
            this.scheduled_activity_id = scheduled_activity_id;
        }

        public String getLead_id ()
        {
            return lead_id;
        }

        public void setLead_id (String lead_id)
        {
            this.lead_id = lead_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [action_id = "+action_id+", scheduled_activity_id = "+scheduled_activity_id+", lead_id = "+lead_id+"]";
        }
    }


}
