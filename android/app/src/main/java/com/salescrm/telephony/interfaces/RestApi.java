package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.luckywheel.ClaimBonusPojo;
import com.salescrm.telephony.adapter.SaveTargetAdapter;
import com.salescrm.telephony.model.AddExchangeCarModel;
import com.salescrm.telephony.model.AllLeadEnquiryListResponse;
import com.salescrm.telephony.model.AllTeamsModel;
import com.salescrm.telephony.model.ApiGenericModelResponse;
import com.salescrm.telephony.model.BookBikeModel;
import com.salescrm.telephony.model.BookCarModel;
import com.salescrm.telephony.model.SendToVerificationILModel;
import com.salescrm.telephony.model.SubmitILomModel;
import com.salescrm.telephony.model.CreateLeadInputDataServer;
import com.salescrm.telephony.model.CreateLeadcarIdData;
import com.salescrm.telephony.model.DistributeLeadInput;
import com.salescrm.telephony.model.EditCarModel;
import com.salescrm.telephony.model.EnquiryListStageTotal;
import com.salescrm.telephony.model.EtvbrCarModelLocationWise;
import com.salescrm.telephony.model.EtvbrGMPojo;
import com.salescrm.telephony.model.EtvbrLocationResponse;
import com.salescrm.telephony.model.EvaluationManagerModel;
import com.salescrm.telephony.model.EvaluationSubmitRequestModel;
import com.salescrm.telephony.model.ExchangeCarCancelModel;
import com.salescrm.telephony.model.ExchangeCarEditModel;
import com.salescrm.telephony.model.FilteredLeadIdsModel;
import com.salescrm.telephony.model.FormObjectsInputs;
import com.salescrm.telephony.model.FormSubmissionActivityInputData;
import com.salescrm.telephony.model.FormSubmissionInputDataServer;
import com.salescrm.telephony.model.ForwardStageData;
import com.salescrm.telephony.model.GetCreActivitiesInput;
import com.salescrm.telephony.model.GetOtpForPNT;
import com.salescrm.telephony.model.LeadSearchSuggestionModel;
import com.salescrm.telephony.model.LostDropAllowed;
import com.salescrm.telephony.model.ManagerHierarchyUserModel;
import com.salescrm.telephony.model.NewFilter.NewFilter;
import com.salescrm.telephony.model.OtpValidationPnt;
import com.salescrm.telephony.model.PendingStatsResponse;
import com.salescrm.telephony.model.ProfileImage;
import com.salescrm.telephony.model.ProformaInvoice;
import com.salescrm.telephony.model.ProformaInvoiceAnswer;
import com.salescrm.telephony.model.ProformaPdfResponse;
import com.salescrm.telephony.model.RescheduleData;
import com.salescrm.telephony.model.ShareWhatsappLog;
import com.salescrm.telephony.model.TodaySummaryResponse;
import com.salescrm.telephony.model.UpdateDeliveryModel;
import com.salescrm.telephony.model.UpdateInvoiceModel;
import com.salescrm.telephony.model.BookingAllocationModel;
import com.salescrm.telephony.model.booking.FullPaymentReceivedInput;
import com.salescrm.telephony.response.ActionPlanResponse;
import com.salescrm.telephony.response.ActiveEventsResponse;
import com.salescrm.telephony.response.AddCarsNewResponse;
import com.salescrm.telephony.response.AddEmailAddressResponse;
import com.salescrm.telephony.response.AddExchangeCarResponse;
import com.salescrm.telephony.response.AddLeadElementsResponse;
import com.salescrm.telephony.response.AddMobileNumberResponse;
import com.salescrm.telephony.response.AddNoteResponse;
import com.salescrm.telephony.response.AllLeadCustomerDetailsResponse;
import com.salescrm.telephony.response.AppConfResponse;
import com.salescrm.telephony.response.AppUserResponse;
import com.salescrm.telephony.response.AwsCredentialsResponse;
import com.salescrm.telephony.response.BookingConfirmResponse;
import com.salescrm.telephony.response.BookCarResponse;
import com.salescrm.telephony.response.CancelBookingResponse;
import com.salescrm.telephony.response.CancelInvoiceResponse;
import com.salescrm.telephony.response.CancelPaymentReponse;
import com.salescrm.telephony.response.CarBrandModelVariantsResponse;
import com.salescrm.telephony.response.CarModelDetailsResponse;
import com.salescrm.telephony.response.CarscolordetailsResponse;
import com.salescrm.telephony.response.ChangePrimaryEmailResponse;
import com.salescrm.telephony.response.CheckNumberIsPrimaryResponse;
import com.salescrm.telephony.response.DealershipsResponse;
import com.salescrm.telephony.response.EventCategoryResponse;
import com.salescrm.telephony.response.ExternalAPIKeysResponse;
import com.salescrm.telephony.response.HereReverseGeoResponse;
import com.salescrm.telephony.response.OtpRequestResponse;
import com.salescrm.telephony.response.OtpValidateResponse;
import com.salescrm.telephony.response.PincodeResponse;
import com.salescrm.telephony.response.PostBookingFormResponse;
import com.salescrm.telephony.response.SwitchDealershipResponse;
import com.salescrm.telephony.response.UserPointHelpResponse;
import com.salescrm.telephony.response.gamification.BadgesResponse;
import com.salescrm.telephony.response.gamification.ConsultantLeaderBoardResponse;
import com.salescrm.telephony.response.CreForActivityResponses;
import com.salescrm.telephony.response.CreateLeadResponse;
import com.salescrm.telephony.response.CustomerEditResponse;
import com.salescrm.telephony.response.DeleteEmailResponse;
import com.salescrm.telephony.response.DeleteMobileResponse;
import com.salescrm.telephony.response.EditEmailResponse;
import com.salescrm.telephony.response.EditMobileNumberResponse;
import com.salescrm.telephony.response.EtvbrDetailsResponse;
import com.salescrm.telephony.response.EvaluatorNamesListResponse;
import com.salescrm.telephony.response.FCMTokenRefreshResponse;
import com.salescrm.telephony.response.FilterActivityAndStagesResponse;
import com.salescrm.telephony.response.FormObjectRetrofit;
import com.salescrm.telephony.response.FormObjectsRetrofit;
import com.salescrm.telephony.response.FormSubmissionActivityResponse;
import com.salescrm.telephony.response.FormSubmissionResponse;
import com.salescrm.telephony.response.FullPaymentRecievedResponse;
import com.salescrm.telephony.response.gamification.GamificationSMProfileResponse;
import com.salescrm.telephony.response.gamification.GamificationTeamProfileResponse;
import com.salescrm.telephony.response.GeneralResponse;
import com.salescrm.telephony.response.GetAllSmsEmailResponse;
import com.salescrm.telephony.response.GetPipelineResponse;
import com.salescrm.telephony.response.ImageUploadResponse;
import com.salescrm.telephony.response.InterestedCarEditPrimaryResponse;
import com.salescrm.telephony.response.LeadHistory;
import com.salescrm.telephony.response.LeadMobileResponse;
import com.salescrm.telephony.response.LeadTeamUserResponse;
import com.salescrm.telephony.response.LocationEditResponse;
import com.salescrm.telephony.response.LoginOtpResponse;
import com.salescrm.telephony.response.MobileAppDataResponse;
import com.salescrm.telephony.response.PrimaryMobileResponse;
import com.salescrm.telephony.response.RescheduleTaskResponse;
import com.salescrm.telephony.response.SaveCarDetailsResponse;
import com.salescrm.telephony.response.SaveEditLeadResponse;
import com.salescrm.telephony.response.SaveExchangeCarEditResponse;
import com.salescrm.telephony.response.SavePipelineResponse;
import com.salescrm.telephony.response.SearchResponse;
import com.salescrm.telephony.response.SendEmailResponse;
import com.salescrm.telephony.response.SendsmsResponse;
import com.salescrm.telephony.response.SetTargetResponse;
import com.salescrm.telephony.response.SplitFormObjectsRetrofit;
import com.salescrm.telephony.response.gamification.DSEPointsResponseModel;
import com.salescrm.telephony.response.gamification.GameConfigResponse;
import com.salescrm.telephony.response.gamification.ManagerLeaderBoardResponse;
import com.salescrm.telephony.response.gamification.TeamLeaderBoardResponse;
import com.salescrm.telephony.response.UpdateDeliveryResponse;
import com.salescrm.telephony.response.UpdateInvoiceResponse;
import com.salescrm.telephony.response.ValidateOtpResponse;
import com.salescrm.telephony.response.gamification.WallOfFameResponse;
import com.salescrm.telephony.telephonyModule.SyncDbResponse;
import com.salescrm.telephony.telephonyModule.model.TelephonyModel;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.PATCH;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by Ravindra P on 27-11-2015.
 */
public interface RestApi {

   /* @FormUrlEncoded
    @POST("/jwt_auth/access_token")
    void GetAccessToken(@Field("username") String username, @Field("password") String password,
                        Callback<AccessTokenResponse> callback);*/

    @GET("/login/otp")
    void login(@Query("mobile_no") String mobile, @Query("device_id") String deviceId, Callback<OtpRequestResponse> callBack);

    @FormUrlEncoded
    @POST("/login/validate_otp")
    void validateOtp(@Field("mobile_no") String mobile, @Field("fcm_id") String fcm, @Field("otp") String otp, @Field("device_id") String deviceId, Callback<OtpValidateResponse> callBack);

    @GET("/jwt_auth/logout")
    void logout(Callback<Object> generalResponseCallBack);

    @FormUrlEncoded
    @POST("/loginAndGetOtp")
    void LoginAndGetOtp(@Field("client_code") String client_code, @Field("username") String username, @Field("password") String password,
                        @Field("imei") String imei,
                        Callback<LoginOtpResponse> callback);

    @FormUrlEncoded
    @POST("/validateotp")
    void ValidateOtp(@Field("imei") String imei, @Field("otp") String otp, @Field("gcm_id") String gcmId, @Field("sc") boolean sc, Callback<ValidateOtpResponse> callback);


    @GET("/tasks")
    void GetActionPlanData(@Query("page") int page, @Query("count") int count, @Query("is_mobile_request") boolean is_mobile_request,
                           @Query("ust") boolean ust, @Query("activity_type%5B%5D") int activityType,
                           @Query("basic_filter%5Bactivity%5D") String basicFilterActivity, @Query("basic_filter%5Bstatus%5D") String basicFilterStatus,
                           @Query("date_filter%5Btype%5D") String dateFilterType, @Query("date_filter%5BfilterDate%5D") String dateFilterVal1,
                           @Query("date_filter%5Bval2%5D") String dateFilterVal2,
                           @Query("order") String orderBy,
                           @Query("dseId") Integer dseId,
                           @Query("isLeadStatusDone") int isLeadStatusDone,
                           Callback<ActionPlanResponse> callback);

    @FormUrlEncoded
    @POST("/eventHistory/addNote")
    void AddNote(@Field("lead_id") String lead_id, @Field("lead_note") String lead_note, Callback<AddNoteResponse> callback);

    @FormUrlEncoded
    @POST("/EditCustomer360Controller/deleteMobileNumber")
    void DeleteNumber(@Field("lead_last_updated") String leadLastUpdated, @Field("lead_id") long lead_id, @Field("phone[lead_phone_mapping_id]") String phonelead_phone_mapping_id, Callback<DeleteMobileResponse> callback);


    @FormUrlEncoded
    @POST("/EditCustomer360Controller/deleteEmailAddr")
    void DeleteEmail(@Field("lead_last_updated") String leadLastUpdated, @Field("lead_id") long lead_id, @Field("email[lead_email_mapping_id]") String emaillead_email_mapping_id, Callback<DeleteEmailResponse> callback);

    @FormUrlEncoded
    @POST("/EditCustomer360Controller/saveNewLeadMobileNumber")
    void AddMobileNumber(@Field("lead_last_updated") String leadLastUpdated, @Field("newMob") String newMob, @Field("lead_id") long lead_id,
                         Callback<AddMobileNumberResponse> callback);

    @FormUrlEncoded
    @POST("/EditCustomer360Controller/changeSourceLocationDse")
    void ChangeLocationDse(@Field("lead_last_updated") String lead_last_updated, @Field("lead_id") String lead_id, @Field("lead_source_id") String lead_source_id, @Field("dse_id") String dse_id,
                           @Field("lead_source_edit_flag") boolean lead_source_edit_flag, @Field("location_edit_flag") boolean location_edit_flag,
                           @Field("dse_edit_flag") boolean dse_edit_flag, @Field("location_id") String location_id, Callback<LocationEditResponse> callback);


    @FormUrlEncoded
    @POST("/EditCustomer360Controller/saveEditedClientDetails")
    void CustomerEdit(@Field("lead_id") String lead_id, @Field("lead_last_updated") String lead_last_updated, @Field("edited_client_info[first_name]") String edited_client_infofirst_name,
                      @Field("edited_client_info[last_name]") String edited_client_infolast_name,
                      @Field("edited_client_info[address]") String edited_client_inforesidence_address,
                      @Field("edited_client_info[pin_code]") String edited_client_infopin_code,
                      @Field("edited_client_info[locality]") String residenceLocality,
                      @Field("edited_client_info[city]") String residenceCity,
                      @Field("edited_client_info[state]") String state,

                      @Field("edited_client_info[office_address]") String edited_client_infooffice_address,
                      @Field("edited_client_info[office_pin_code]") String edited_client_infooffice_pin_code,
                      @Field("edited_client_info[office_locality]") String officeLocality,
                      @Field("edited_client_info[office_city]") String officeCity,

                      @Field("edited_client_info[company_name]") String edited_client_infocompany_name,
                      @Field("edited_client_info[occupation]") String edited_client_infooccupation,
                      @Field("edited_client_info[gender]") String edited_client_infogender,
                      @Field("edited_client_info[title]") String edited_client_infotitle,
                      @Field("edited_client_info[customer_age]") String edited_client_infoage, Callback<CustomerEditResponse> callback);


    @FormUrlEncoded
    @POST("/EditCustomer360Controller/saveEditLeadMailId")
    void EditEmail(@Field("lead_last_updated") String leadLastUpdated, @Field("lead_id") String lead_id, @Field("object") String object, @Field("mail") String mail,
                   Callback<EditEmailResponse> callback);


    @FormUrlEncoded
    @POST("/EditCustomer360Controller/saveEditLeadPhone")
    void EditMobileNumber(@Field("lead_last_updated") String leadLastUpdated, @Field("lead_id") String lead_id, @Field("object") String object, @Field("phone") String phone,
                          Callback<EditMobileNumberResponse> callback);


    @FormUrlEncoded
    @POST("/EditCustomer360Controller/saveNewLeadMailId")
    void AddEmailAddress(@Field("lead_last_updated") String leadLastUpdated, @Field("lead_id") String lead_id, @Field("new_mail") String new_mail, Callback<AddEmailAddressResponse> callback);

    /*  @FormUrlEncoded
      @POST("/EditCustomer360Controller/saveNewLeadMailId")
      void AddEmailAddress(@Query("token") String token, @Field("is_mobile_request") boolean is_mobile_request,
                           @Field("lead_id") String lead_id, @Field("new_mail") String new_mail, Callback<AddEmailAddressResponse> callback);
  */
    @FormUrlEncoded
    @POST("/EditCustomer360Controller/saveLeadEdit")
    void SaveLeadEdit(@Field("lead_id") String lead_id, @Field("lead_last_updated") String lead_last_updated, @Field("data[location_id]") String datalocation_id,
                      @Field("data[dse_id]") String datadse_id, @Field("data[lead_source_id]") String datalead_source_id,
                      @Field("data[buyer_type_id]") String databuyer_type_id, @Field("data[expected_closing_date]") String dataexpected_closing_date,
                      @Field("data[mode_of_payment]") String datamode_of_payment, @Field("data[customer_type]") String dataoem_source_id,
                      Callback<SaveEditLeadResponse> callback);

    @FormUrlEncoded
    @POST("/add_task/fetch_search_result")
    void GetSearchLead(@Field("searchData[mobile]") String mobile,
                       @Field("searchData[enquiry]") String enqNumber,
                       @Field("searchData[customer]") String customer,
                       @Field("searchData[lead]") String lead,
                       @Field("searchData[email]") String email,
                       @Field("is_mobile_request") String isMobileRequest,
                       Callback<SearchResponse> callback);

    @POST("/add_task/fetch_lead_elements")
    void FetchLeadElements(@Body String body, Callback<AddLeadElementsResponse> callback);


    @FormUrlEncoded
    @POST("/EditCustomer360Controller/changePrimaryEmail")
    void ChangePrimaryAddress(@Field("lead_last_updated") String leadLastUpdated, @Field("lead_id") String lead_id, @Field("mail_id") String mail_id,
                              Callback<ChangePrimaryEmailResponse> callback);


    @GET("/api/get_all_templates")
    void GetAllTemplates(@Query("proforma_invoice") boolean proforma, Callback<GetAllSmsEmailResponse> callback);

    @FormUrlEncoded
    @POST("/api/send_sms")
    void Sendsms(@Field("lead_id") String leadId, @Field("number") String number, @Field("message") String messsage, Callback<SendsmsResponse> callback);

    @Multipart
    @POST("/api/email_user")
    void SendEmail(@Part("address") String address, @Part("mailType") String mailtype, @Part("body") String body,
                   @Part("subject") String subject, @Part("attachments") TypedFile attachment, @Part("lead_id") String leadId,
                   @Part("customer") String customer, @Part("proforma_pdf_link") String pdfLink,  Callback<SendEmailResponse> callback);


    @GET("/getCarModelDetails")
    void Getcarcolors(@Query("model_id") String model_id, Callback<CarscolordetailsResponse> callback);


    @FormUrlEncoded
    @POST("/EditCustomer360Controller/saveInterestedCarEdit")
    void saveEdittedCarDetails(@Field("lead_id") String lead_id,
                               @Field("presentLeadCarId") String presentLeadCarId,
                               @Field("editedCarObject[carModel]") String editedCarObjectcarModel,
                               @Field("editedCarObject[carVariant]") String editedCarObjectcarVariant,
                               @Field("editedCarObject[carColorId]") String editedCarObjectcarColorId,
                               @Field("editedCarObject[carFuelType]") String editedCarObjectcarFuelType,
                               @Field("editedCarObject[bookingId]") String editedBookingId,
                               @Field("editedCarObject[amtRecvd]") String editedAmtRecvd,
                               Callback<SaveCarDetailsResponse> callback);

    @FormUrlEncoded
    @POST("/changePrimaryCar")
    void makePrimaryInterestedCar(@Field("lead_id") String lead_id,
                                  @Field("leadCarId") String presentLeadCarId,
                                  @Field("lead_last_updated") String leadLastUpdated,
                                  Callback<InterestedCarEditPrimaryResponse> callback);

    @GET("/gamification/manager_leaderboard")
    void fetchManagerBoardDetails(@Query("month_year") String monthYear, Callback<ManagerLeaderBoardResponse> callback);

    @GET("/gamification/team_leaderboard/{locationId}")
    void fetchTeamLeaderBoardDetails(@Path("locationId") int locationId,@Query("month_year") String monthYear, Callback<TeamLeaderBoardResponse> callback);

    @GET("/gamification/dse_leaderboard/{locationId}")
    void fetchConsultantData(@Path("locationId") int locationId,@Query("month_year") String monthYear, Callback<ConsultantLeaderBoardResponse> callback);

    @GET("/gamification/wall_of_fame/{locationId}")
    void fetchWallOfFameData(@Path("locationId") int locationId, @Query("month_year") String monthYear, Callback<WallOfFameResponse> callback);

    @GET("/gamification/configuration")
    void fetchGamificationConfig(Callback<GameConfigResponse> callback);

    @GET("/gamification/dse_point_details/{userId}")
    void fetchDsePoints(@Path("userId") int userID, @Query("month_year") String monthYear, Callback<DSEPointsResponseModel> callback);

    @GET("/gamification/user_badge_details/{userId}")
    void fetchDseBadges(@Path("userId") int userID, @Query("month_year") String monthYear, Callback<BadgesResponse> callback);


    @POST("/editExchangeCar")
    void SaveEdittedExchangeCar(@Body ExchangeCarEditModel cardata, Callback<SaveExchangeCarEditResponse> carob);


    @FormUrlEncoded
    @POST("/EditCustomer360Controller/changePrimaryMobile")
    void ChangePrimaryMobile(@Field("lead_last_updated") String leadLastUpdated, @Field("lead_id") long lead_id, @Field("mob_id") String mob_id, Callback<PrimaryMobileResponse> callback);


    @GET("/add_task/checkPrimary")
    void CheckNumberPrimary(@Query("number") String number,
                            Callback<CheckNumberIsPrimaryResponse> callback);

    @GET("/getCarModelDetails")
    void GetCarModelDetails(@Query("model_id") String modelId,
                            Callback<CarModelDetailsResponse> callback);

    @GET("/get_team_user_details")
    void GetTeamUserDetails(@Query("leadSourceIds[]") ArrayList<Integer> leadSourceIds, @Query("leadNewCar") int leadNewCar, @Query("locationId") int locationId,
                            Callback<LeadTeamUserResponse> callback);

    @GET("/get_team_user_details")
    void GetTeamUserDetails(@Query("leadSourceId") int leadSourceIds, @Query("leadNewCar") int leadNewCar, @Query("locationId") int locationId,
                            Callback<LeadTeamUserResponse> callback);


    @FormUrlEncoded
    @POST("/c360/allotDSE")
    void allotDSE(@Field("lead_id") String leadId, @Field("location_id") String locationId, @Field("dse_team_lead_id") String dse_team_lead_id,
                  @Field("dse_id") String dseId, @Field("manager_id") String mangerId, @Field("lead_last_updated") String leadLastUpdated, Callback<GeneralResponse> callback);

    @Headers("Content-Type: application/json")
    @POST("/add_task/getCreForActivity")
    void GetCreForActivities(@Body GetCreActivitiesInput input, Callback<CreForActivityResponses> callback);


    //Create Lead Api
    //http://scrmstaging.ninjacrm.com:9000/modified_client/mobile/createLead
    @Headers("Content-Type: application/json")
    @POST("/createLead")
    void CreateLead(@Body CreateLeadInputDataServer data, Callback<CreateLeadResponse> o);

    @Headers("Content-Type: application/json")
    @POST("/EditCustomer360Controller/addNewInterestedCar")
    void Addnewcar(@Body CreateLeadcarIdData cardata, Callback<AddCarsNewResponse> carob);

    @Headers("Content-Type: application/json")
    @POST("/EditCustomer360Controller/addNewExchangeCar")
    void AddExchangeCar(@Body AddExchangeCarModel cardata, Callback<AddExchangeCarResponse> addExchangeCarResponseCallback);

   /* @POST("/addCars")
    void AddMultipleCar(@Body AddMultipleCarModel carData, Callback<AddMultipleCarResponse> addMultipleCarResponseCallback);
    // void SaveEdittedExchangeCar(@Body ExchangeCarEditModel cardata, Callback<SaveExchangeCarEditResponse> carob);
*/

    @FormUrlEncoded
    @POST("/stageForwarding/getStagesAndClosingReason")
    void GetAllStagingPipeline(@Field("lead_id[]") List<String> lead_id, Callback<GetPipelineResponse> callback);

    @Headers("Content-Type: application/json")
    @POST("/stageForwarding/forwardStage")
    void SavePipeline(@Body ForwardStageData forwardStageData, Callback<SavePipelineResponse> callback);

   /* @FormUrlEncoded
    @POST("/mobilescoreboard")
    void FetchScoreBoardData(@Field("user_id") String userId, @Field("roles") String roles, Callback<ScoreboardResponse> o);
*/
    @GET("/tasks/loadSearchElements")
    void GetFilterActivityAndStages(Callback<FilterActivityAndStagesResponse> callback);

    @GET("/formObject")
    void GetFormObject(@Query("action_id") int action_id, @Query("lead_id") String lead_id, @Query("scheduled_activity_id") String scheduled_activity_id, Callback<FormObjectRetrofit> callback);

    /*  @Headers("Content-Type: application/json")
      @POST("/formObjects")*/
    void GetFormObjects(@Body FormObjectsInputs data, Callback<FormObjectsRetrofit> callback);

    @Headers("Content-Type: application/json")
    @POST("/newFormObjs")
    void GetSplitFormObjects(@Body FormObjectsInputs data, Callback<SplitFormObjectsRetrofit> callback);


    @GET("/post_booking_activity_form")
    void getPostBookingForm(@Query("lead_id") String leadId, @Query("activity_id") String activityId, Callback<PostBookingFormResponse> callback);

    @GET("/c360/manualActivitiesToSchedule")
    void GetManualActivities(@Query("lead_id") String leadId, @Query("schedule_type") String scheduledType, Callback<FormObjectRetrofit> callback);

    @Headers("Content-Type: application/json")
    @POST("/c360/manualActivitiesToSchedule")
    void GetAllManualActivities(@Body FormObjectsInputs data, Callback<FormObjectsRetrofit> callback);

    @GET("/eventHistory/getLeadHistory")
    void getLeadHistory(@Query("lead_id") String lead_id, Callback<LeadHistory> callback);

    @Headers("Content-Type: application/json")
    @POST("/c360/mnewActivitySubmission")
    void submitNewActivity(@Body FormSubmissionActivityInputData data, Callback<FormSubmissionActivityResponse> o);

    @POST("/mformSubmission")
    void submitForm(@Body FormSubmissionInputDataServer data, Callback<FormSubmissionResponse> o);

    @FormUrlEncoded
    @POST("/updateFcmId")
    void refreshFcmToken(@Field("new_fcm") String newFcm, Callback<FCMTokenRefreshResponse> callback);

    @GET("/getModelsAndVariants")
    void getModelsAndVariants(@Query("brand_id[]") ArrayList<String> brand_id, Callback<CarBrandModelVariantsResponse> callback);

    @GET("/checkVersion")
    void getDealerCodes(@Query("api_version") String api_version, @Query("get_client_names") String get_client_names, Callback<MobileAppDataResponse> callback);


    @FormUrlEncoded
    @POST("/getAllLeadInformation")
    void GetAllLeadInformation(@Field("lead_id[]") ArrayList<String> lead_id, Callback<AllLeadCustomerDetailsResponse> callback);

    @POST("/syncCDR")
    void SyncCDR(@Body TelephonyModel data, Callback<SyncDbResponse> callback);

    @FormUrlEncoded
    @POST("/UserLeadsController/fetchLeads")
    void getAllLeadList(@Field("lead_sources[]") ArrayList<String> lead_sources,
                        @Field("lead_stage[]") ArrayList<String> lead_stage,
                        @Field("lead_tags[]") ArrayList<String> lead_tags,
                        @Field("location[]") ArrayList<String> loc,
                        @Field("dses[]") ArrayList<String> dses,
                        @Field("startLimit") String startLimit,
                        @Field("endLimit") String endLimit,
                        @Field("page") String page,
                        Callback<AllLeadEnquiryListResponse> callback);

    @FormUrlEncoded
    @POST("/UserLeadsController/fetchLeads")
    void getAllLeadListWithFilter(@Field("lead_sources[]") ArrayList<String> lead_sources,
                        @Field("lead_stage[]") ArrayList<String> lead_stage,
                        @Field("lead_tags[]") ArrayList<String> lead_tags,
                        @Field("location[]") ArrayList<String> loc,
                        @Field("dses[]") ArrayList<String> dses,
                        @Field("startLimit") String startLimit,
                        @Field("endLimit") String endLimit,
                        @Field("page") String page,
                        @Field("filters") String jsonObject,
                        Callback<AllLeadEnquiryListResponse> callback);

    @FormUrlEncoded
    @POST("/UserLeadsController/fetchLeads")
    void getAllLeadListWithFilter(@Field("lead_sources[]") ArrayList<String> lead_sources,
                                  @Field("lead_stage[]") ArrayList<String> lead_stage,
                                  @Field("lead_tags[]") ArrayList<String> lead_tags,
                                  @Field("location[]") ArrayList<String> loc,
                                  @Field("dses[]") ArrayList<String> dses,
                                  @Field("startLimit") String startLimit,
                                  @Field("endLimit") String endLimit,
                                  @Field("page") String page,
                                  @Field("filters") String jsonObject,
                                  @Field("count_only") boolean counts,
                                  Callback<EnquiryListStageTotal> callback);

    /*@Headers("Content-Type: application/json")
    @POST("/getEtvbr")
    void getEtvbr(@Body HashMap<String, String> hashMap, Callback<EtvbrModelPojo> callbackEtvbr);
*/

    @Multipart
    @POST("/upload_dp")
    void imageUpload(@Part("base64_image") String base64_image,
                     Callback<ImageUploadResponse> callback);


    @GET("/reports/tasks")
    void getTodaySummary(@Query("date") String date, Callback<TodaySummaryResponse> callback);


    @Headers("Content-Type: application/json")
    @POST("/updateInvoice")
    void updateInvoice(@Body UpdateInvoiceModel invoiceData, Callback<UpdateInvoiceResponse> updateInvoiceResponseCallback);

    @Headers("Content-Type: application/json")
    @POST("/updateDelivery")
    void updateDelivery(@Body UpdateDeliveryModel updateDeliveryModel, Callback<UpdateDeliveryResponse> updateDeliveryResponseCallback);

    @Headers("Content-Type: application/json")
    @POST("/bookCar")
    void bookCar(@Body BookCarModel cardata, Callback<BookCarResponse> bookResponse);

    @Headers("Content-Type: application/json")
    @POST("/editBookingDetails")
    void editBookingDetails(@Body EditCarModel editCarData, Callback<BookCarResponse> bookResponse);

    @Headers("Content-Type: application/json")
    @POST("/c360/mnewActivitySubmission")
    void submitEvaluationForm(@Body EvaluationSubmitRequestModel data, Callback<FormSubmissionActivityResponse> o);

    @FormUrlEncoded
    @POST("/cancelBooking")
    void cancelBooking(@Field("booking_id") int bookingId,
                       @Field("invoice_id") String invoiceId,
                       @Field("lead_car_id") int leadCarId,
                       @Field("location_id") int locationId,
                       @Field("enquiry_id") int enquiryId,
                       @Field("lead_id") long leadId,
                       @Field("lead_car_stage_id") int stageId,
                       @Field("cancel_reason") String reason,
                       @Field("lead_last_updated") String leadLastUpdated,
                       Callback<CancelBookingResponse> callback);

    @FormUrlEncoded
    @POST("/cancelInvoice")
    void cancelInvoice(@Field("booking_id") int bookingId,
                       @Field("lead_car_id") int leadCarId,
                       @Field("location_id") int locationId,
                       @Field("enquiry_id") int enquiryId,
                       @Field("lead_id") long leadId,
                       @Field("lead_car_stage_id") int stageId,
                       @Field("invoice_id") int invoiceId,
                       @Field("lead_last_updated") String leadLastUpdated,
                       Callback<CancelInvoiceResponse> callback);

    @Headers("Content-Type: application/json")
    @POST("/fullPaymentRecieved")
    void fullPaymentRecieved(@Body FullPaymentReceivedInput fullPaymentReceivedInput,
                             Callback<FullPaymentRecievedResponse> callback);


    @FormUrlEncoded
    @POST("/rescheduleTask")
    void rescheduleTask(@Field("reschedule_date") String date,
                        @Field("booking_id") int bookingId,
                        @Field("enquiry_id") int enquiryId,
                        @Field("lead_car_id") int leadCarID,
                        @Field("lead_id") long leadId,
                        @Field("lead_car_stage_id") int stageId,
                        @Field("invoice_id") int invoiceId,
                        @Field("remarks") String remarks,
                        Callback<RescheduleTaskResponse> callback);


    @FormUrlEncoded
    @POST("/cancelPayment")
    void cancelPayment(@Field("booking_id") int bookingId,
                       @Field("enquiry_id") int enquiryId,
                       @Field("lead_car_id") int leadCarID,
                       @Field("lead_id") long leadId,
                       @Field("lead_car_stage_id") int stageId,
                       @Field("lead_last_updated") String leadLastUpdated,
                       Callback<CancelPaymentReponse> callback);

    @GET("/getAllEvaluators")
    void getAllEvaluatorNames(@Query("lead_id") long leadID,
                              Callback<EvaluatorNamesListResponse> callback);

    @FormUrlEncoded
    @POST("/notify_upcoming_activity")
    void upcomingActivityReminder(@Field("scheduled_activity_id") Integer scheduledActivityId,
                                  Callback<Object> callback);

    @GET("/reports/targets")
    void getTargets(@Query("start_date") String start_date, @Query("end_date") String end_date, Callback<EtvbrGMPojo> callback);

    @GET("/reports/user_etvbr")
    void getEtvbrInfoWithLocation(@Query("start_date") String start_date, @Query("end_date") String end_date, @Query("filters") String filtersArrayList, Callback<EtvbrLocationResponse> callback);

    @Headers("Content-Type: application/json")
    @POST("/reports/setTargets")
    void setTargets(@Body SaveTargetAdapter.TargetArray targets,
                    Callback<SetTargetResponse> callback);

    @GET("/TeamsController/get_all_teams/{location_id}")
    void getAllTeams(@Path(value = "location_id") Integer locationId, Callback<AllTeamsModel> callback);

 /*   @FormUrlEncoded
    @POST("/getFilteredLeads")
    void getFilteredLeadIds(@Field("user_ids[]") ArrayList<String> user_id, Callback<FilteredLeadIdsModel> callback);
*/
    @FormUrlEncoded
    @POST("/getFilteredLeads")
    void getFilteredLeadIds(@Field("user_ids[]") ArrayList<String> user_id,
                            @Field("model_ids[]") ArrayList<String> models,
                            @Field("lead_source_ids[]") ArrayList<String> leadSources,
                            @Field("customer_type_ids[]") ArrayList<String> customerTypes,
                            @Field("stage_ids[]") ArrayList<String> stageIds,
                            Callback<FilteredLeadIdsModel> callback);

    @Headers("Content-Type: application/json")
    @POST("/transferLead")
    void distributeLeads(@Body DistributeLeadInput data, Callback<ApiGenericModelResponse> callback);

    @GET("/get_customer_suggestions/{data}")
    void getLeadSearchSuggestion(@Path(value = "data") String data, @Query("location_id") String location, Callback<LeadSearchSuggestionModel> callback);

    @GET("/teamsController/get_all_team_details/{location_id}")
    void getAllManagerHierarchyUsers(@Path(value = "location_id") Integer locationId, Callback<ManagerHierarchyUserModel> managerHierarchyUserCallback);

    @FormUrlEncoded
    @POST("/transferDSEToDifferentTeam")
    void moveUser(@Field("user_id") Integer userId, @Field("team_id") Integer teamId,
                  @Field("same_location") boolean isSameLocation, Callback<ApiGenericModelResponse> callback);

    @FormUrlEncoded
    @POST("/teamsController/change_manager_of_tl")
    void moveUser(@Field("team_leader_id") Integer userId, @Field("current_manager_id") Integer currentParentId,
                  @Field("new_manager_id") Integer newParentId, Callback<ApiGenericModelResponse> callback);


    @GET("/makeUserInactive/{id}")
    void deactivateUser(@Path(value = "id") Integer data, Callback<ApiGenericModelResponse> callback);

    @GET("/reports/model_etvbr")
    void getEtvbrCarsInfoLocationWise(@Query("start_date") String start_date, @Query("end_date") String end_date, @Query("filters") String filters, Callback<EtvbrCarModelLocationWise> callback);

    @GET("/appconf")
    void getAppConfiguration(Callback<AppConfResponse> callback);

    @GET("/users/info")
    void getUserDetails(Callback<AppUserResponse> callback);

    @GET("/users/dealerships")
    void getDealerships(Callback<DealershipsResponse> callback);

    @POST("/users/switch_dealership/{dealer_id}")
    void switchDealership(@Path(value = "dealer_id") Integer dealerId, @Body String body, Callback<SwitchDealershipResponse> callback);


    @GET("/pending/stats")
    void getPendingStats(Callback<PendingStatsResponse> callback);

    @GET("/image/{imei}")
    void getUserProfileImage(@Path(value = "imei") String imei, @Query("client_code") String clientCode, Callback<ProfileImage> callback);

    @GET("/reports/clickable_etvbr")
    void getEtvbrDetails(@Query("start_date") String start_date,@Query("end_date") String end_date,
                         @Query("clicked_val") String clicked_val,@Query("user_id") String user_id,
                         @Query("user_location_id") String user_location_id,@Query("user_role_id") String user_role_id,
                         @Query("offset") String offset,@Query("limit") String limit, @Query("filters") String filters,
                         Callback<EtvbrDetailsResponse> callback);

    @FormUrlEncoded
    @POST("/activity/{activity_id}/sendOTP")
    void getOtpForPNT(@Path(value = "activity_id") String activityId, @Field("lead_id") String leadId, Callback<GetOtpForPNT> callback);

    @GET("/activity/validateOTP")
    void validatePNTotp(@Query("lead_id") String leadId, @Query("otp") int otp, Callback<OtpValidationPnt> callback);

    @Headers("Content-Type: application/json")
    @POST("/bookBike")
    void bookBike(@Body BookBikeModel bookBikeModel, Callback<BookingConfirmResponse> bookResponse);

    @Headers("Content-Type: application/json")
    @POST("/il/{path}")
    void submitILom(@Path(value = "path") String path, @Body SubmitILomModel submitILomModel, Callback<BookingConfirmResponse> bookResponse);

    @Headers("Content-Type: application/json")
    @POST("/il/verify")
    void sendToVerificationIL( @Body SendToVerificationILModel submitILomModel, Callback<BookingConfirmResponse> bookResponse);

    @GET("/gamification/team_point_details/{user_id}")
    void GamificationTeamProfile(@Path(value = "user_id") String user_id, @Query("month_year") String monthYear, Callback<GamificationTeamProfileResponse> callback);

    @GET("/gamification/manager_point_details/{user_id}")
    void GamificationManagerProfileProfile(@Path(value = "user_id") String user_id, @Query("month_year") String monthYear, Callback<GamificationSMProfileResponse> callback);

    @GET("/lead_mobiles")
    void getLeadMobiles(Callback<LeadMobileResponse> callback);

    @GET("/proforma-invoice/details")
    void getProformaInvoiceDetails(@Query("lead_id") String leadId,  Callback<ProformaInvoice> callback);

    @POST("/proforma-invoice/generate-pdf")
    void getProformaInvoicePdfPreview(@Body ProformaInvoiceAnswer proformaInvoiceAnswer, Callback<ProformaPdfResponse> callback);

    @POST("/proforma-invoice/share")
    void shareLog(@Body ShareWhatsappLog shareWhatsappLog, Callback<Response> callback);

    @FormUrlEncoded
    @POST("/gamification/claim_bonus")
    void sendClaimedBonus(@Field("lead_id") String leadId,
                          @Field("scheduled_activity_id") String scheduleActivityId,
                          @Field("option_id") String optionId, @Field("spin_id") String spinId,
                          @Field("dse_id") String dseId,
                          @Field("loc_id") String locId,
                          Callback<ClaimBonusPojo> callback);


    @GET("/gamification/dse_point_help/{user_id}")
    void fetchUserHelpData(@Path("user_id") String userId, @Query("month_year") String monthYear, Callback<UserPointHelpResponse> callback);

    @GET("/etvbr_filters")
    void fetchFilters(Callback<NewFilter> callback);

    @GET("/vin_allocation_statuses")
    void fetchBookingAllocationData(Callback<BookingAllocationModel> callBack);

    @GET("/event/active_events")
    void getActiveEvents(Callback<ActiveEventsResponse> callBack);

    @GET("/event/categories")
    void getEventCategories(Callback<EventCategoryResponse> callBack);

    @GET("/external_api_keys")
    void getExternalAPIKeys(Callback<ExternalAPIKeysResponse> callBack);

    @GET("/resource/6176ee09-3d56-4a3b-8115-21841576b2f6?format=json&offset=0&limit=100")
    void getPincodeDetails(@Query("api-key") String apiKey, @Query("filters[pincode]") String pincode, @Query("format") Callback<PincodeResponse> callBack);

    @GET("/pincode/{pincode}")
    void getPincodeDetails(@Path("pincode") String pinCode, Callback<PincodeResponse> callBack);

    @GET("/reversegeocode.json")
    void getReverseGeoCode(@Query("app_id") String appId,
                           @Query("app_code") String appCode,
                           @Query("gen") int gen,
                           @Query("maxresults") int maxResult,
                           @Query("mode") String mode,
                           @Query("prox") String CSVLatLong,
                           Callback<HereReverseGeoResponse> callback


    );
    @GET("/hasEvaluationManagers")
    void getEvaluationManagerExist(Callback<EvaluationManagerModel> callBack);

    @PATCH("/rescheduleExchange")
    void rescheduleExchange(@Body RescheduleData rescheduleData, Callback<Response> callback);

    @PATCH("/cancelExchange")
    void cancelExchange(@Body ExchangeCarCancelModel exchangeCarCancelModel, Callback<Response> callback);

    @GET("/exchangeLostDropCheck")
    void getProceedFlag(@Query("lead_id") String leadId, Callback<LostDropAllowed> callback);

    @GET("/aws/creds")
    void getAwsTemporaryAccess(Callback<AwsCredentialsResponse> callback);

    @GET("/aws/creds")
    AwsCredentialsResponse getAwsTemporaryAccess();
}
