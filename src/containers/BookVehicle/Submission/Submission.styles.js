import React from "react";
import { StyleSheet } from "react-native";
export default styles = StyleSheet.create({
    linearGradient: {
        padding: 20,
        paddingBottom: 24,
        borderRadius: 10,
    },
    main: {
        backgroundColor: '#000000',
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 80,
        paddingBottom: 80,
        flex: 1,
        justifyContent: 'center'

    },
    header: {
        flexDirection: "row",
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    title: {
        color: "#eeeeee",
        fontSize: 18,
        textAlign: 'center',
    },
    date: {
        color: '#eee',
        fontSize: 16,
        textAlign: 'center',
        borderBottomWidth: 1,
        paddingBottom: 8,
        paddingTop: 10,
        marginStart: 10,
        marginEnd: 10,
        borderBottomColor: '#eee'
    },
    closeButton: {
        position: "absolute",
        right: 8,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
        height: 24,
        width: 24,
        borderRadius: 14
    },


});
