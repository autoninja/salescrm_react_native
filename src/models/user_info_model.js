
class UserInfoModel {
  constructor(id, name, user_name, mobile, email, dealer, dp_url, team_id, brand_id, brand_name, roles, locations) {
    this.id = id;
    this.name = name;
    this.user_name = user_name;
    this.mobile = mobile;
    this.email = email;
    this.dealer = dealer;
    this.dp_url = dp_url;
    this.team_id = team_id;
    this.brand_id = brand_id,
    this.brand_name = brand_name,
    this.roles = roles,
    this.locations = locations,
    this.updated_at = new Date();
  }
}

class UserRolesModel {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}
class UserLocationsModel {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}

module.exports = {UserInfoModel, UserRolesModel, UserLocationsModel};
