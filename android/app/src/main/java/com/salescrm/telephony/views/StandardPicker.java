package com.salescrm.telephony.views;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.StandardPickerGridAdapter;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.PlanNextTaskResultListener;
import com.salescrm.telephony.model.StandardPickerModel;
import com.salescrm.telephony.utils.WSConstants;

import java.util.List;

/**
 * Created by bharath on 14/3/18.
 */

public class StandardPicker {

    public void show(@NonNull Context context, @NonNull String title,
                     @NonNull final List<StandardPickerModel> standardPickerModels,
                     final StandardPickerListener standardPickerListener) {
        final Dialog dialog = new Dialog(context, R.style.AppTheme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
          //  dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }

        dialog.setContentView(R.layout.layout_standard_picker);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tv_picker_title);
        GridView gridView = (GridView) dialog.findViewById(R.id.grid_view_picker);

        tvTitle.setText(title);
        gridView.setAdapter(new StandardPickerGridAdapter(context, standardPickerModels));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();
                standardPickerListener.onSelectStandardPickerItem(standardPickerModels.get(position));
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


    }

    public interface StandardPickerListener{
        void onSelectStandardPickerItem(StandardPickerModel standardPickerModel);
    }


}
