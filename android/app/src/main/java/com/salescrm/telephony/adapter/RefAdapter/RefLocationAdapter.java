package com.salescrm.telephony.adapter.RefAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.DetailsOfEtvbr;
import com.salescrm.telephony.activity.EtvbrLocationActivity;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.etvbr_location.Location;
import com.salescrm.telephony.db.etvbr_location.ResultEtvbrLocation;
import com.salescrm.telephony.db.ref_location.RefLocation;
import com.salescrm.telephony.db.ref_location.RefResultLocation;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.fragments.EtvbrLocationFragment;
import com.salescrm.telephony.fragments.retExcFin.RefLocationFragment;
import com.salescrm.telephony.preferences.Preferences;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by prateek on 7/11/17.
 */

public class RefLocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private Realm realm;
    private RefResultLocation resultEtvbrLocation;
    private RealmResults<RefLocation> locationsResults;
    private Preferences pref;

    public RefLocationAdapter(FragmentActivity activity, RefResultLocation result, Realm realm) {
        this.context = activity;
        this.realm = realm;
        resultEtvbrLocation = result;
        pref = Preferences.getInstance();
        pref.load(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.ref_location_card_adapter, parent, false);
        return new RefLocationAdapter.RefLocationCardHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            if (RefLocationFragment.PERCENT) {
                viewCardsGMRel(holder, position);
            } else {
                viewCardsGMAbs(holder, position);
            }
    }

    private void viewCardsGMAbs(RecyclerView.ViewHolder holder, final int position) {
        final RefLocationCardHolder cardHolder = (RefLocationCardHolder) holder;
        //final int position = pos;
        cardHolder.cardView.setVisibility(View.VISIBLE);
        cardHolder.llinearLayout.setVisibility(View.GONE);
        //absolute = results.get(position).getAbs();
        cardHolder.tvTargetRetailCard.setVisibility(View.VISIBLE);
        cardHolder.llRetailsCard.setVisibility(View.VISIBLE);
        cardHolder.tvExchCard.setVisibility(View.VISIBLE);
        cardHolder.tvFinCard.setVisibility(View.VISIBLE);
        cardHolder.tvFinOutCard.setVisibility(View.VISIBLE);
        cardHolder.tvPBCard.setVisibility(View.VISIBLE);
        cardHolder.tvLECard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetExchCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetFinCard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetFinOutCard.setVisibility(View.GONE);
        cardHolder.tvTargetPBCard.setVisibility(View.GONE);
        cardHolder.tvTargetLECard.setVisibility(View.GONE);
        cardHolder.viewTargetCard.setVisibility(View.VISIBLE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 0);




        Drawable drawableImg = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_img_location, null);
        cardHolder.imgUserCard.setBackground(drawableImg);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        if (position < resultEtvbrLocation.getLocations().size()) {
            locationsResults = realm.where(RefLocation.class).findAll();

            if (locationsResults != null) {

                Paint paint = new Paint();
                paint.setColor(Color.BLUE);
                paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);

                cardHolder.tvRetailCard.setPaintFlags(paint.getFlags());
                cardHolder.tvRetailCard.setText("" + locationsResults.get(position).getLocationAbs().getRetails());

                cardHolder.tvLocationName.setPaintFlags(paint.getFlags());
                cardHolder.tvLocationName.setText("" + locationsResults.get(position).getLocationName());


                cardHolder.tvExchCard.setPaintFlags(paint.getFlags());
                cardHolder.tvExchCard.setText(""+locationsResults.get(position).getLocationAbs().getExchanged());

                cardHolder.tvFinCard.setPaintFlags(paint.getFlags());
                cardHolder.tvFinCard.setText(""+locationsResults.get(position).getLocationAbs().getIn_house_financed());

                cardHolder.tvFinOutCard.setPaintFlags(paint.getFlags());
                cardHolder.tvFinOutCard.setText(""+locationsResults.get(position).getLocationAbs().getOut_house_financed());

                cardHolder.tvPBCard.setPaintFlags(paint.getFlags());
                cardHolder.tvPBCard.setText(getReadableData(locationsResults.get(position).getLocationAbs().getPending_bookings()));

                cardHolder.tvLECard.setPaintFlags(paint.getFlags());
                cardHolder.tvLECard.setText(getReadableData(locationsResults.get(position).getLocationAbs().getLive_enquiries()));

                cardHolder.tvTargetRetailCard.setText("" + locationsResults.get(position).getLocationTargets().getRetails());
                cardHolder.tvTargetExchCard.setText("" + locationsResults.get(position).getLocationTargets().getExchanged());
                cardHolder.tvTargetFinCard.setText("" + locationsResults.get(position).getLocationTargets().getIn_house_financed());
                cardHolder.tvTargetFinOutCard.setText("" + locationsResults.get(position).getLocationTargets().getOut_house_financed());
                cardHolder.tvTargetPBCard.setText("" + locationsResults.get(position).getLocationTargets().getPending_bookings());
                cardHolder.tvTargetLECard.setText("" + locationsResults.get(position).getLocationTargets().getLive_enquiries());

                cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
                cardHolder.tvLocationName.setVisibility(View.VISIBLE);


                cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.refFooter.setVisibility(View.GONE);
                cardHolder.tvNameCard.setVisibility(View.GONE);
                //Picasso.with(context).load("https://s3-ap-southeast-1.amazonaws.com/salescrm-images/staging/uploads/profile_images//SATYANARYANA CH-48-1510145708.png").placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).into(cardHolder.imgUserCard);

                cardHolder.llInner.setBackgroundColor(Color.WHITE);
                cardHolder.tvRetailCard.setTextColor(Color.BLACK);
                cardHolder.tvUserTotalCard.setTextColor(Color.BLACK);
                cardHolder.tvExchCard.setTextColor(Color.BLACK);
                cardHolder.tvFinCard.setTextColor(Color.BLACK);
                cardHolder.tvFinOutCard.setTextColor(Color.BLACK);
                cardHolder.tvPBCard.setTextColor(Color.parseColor("#7ED321"));
                cardHolder.tvLECard.setTextColor(Color.parseColor("#7ED321"));


                cardHolder.tvRetailCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvUserTotalCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvExchCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvFinCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvFinOutCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvPBCard.setTypeface(null, Typeface.NORMAL);
                cardHolder.tvLECard.setTypeface(null, Typeface.NORMAL);

                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);
                cardHolder.viewFinCard.setVisibility(View.VISIBLE);
                cardHolder.viewFinOutCard.setVisibility(View.VISIBLE);
                cardHolder.viewPBCard.setVisibility(View.VISIBLE);
                cardHolder.viewLECard.setVisibility(View.VISIBLE);
                cardHolder.viewExchCard.setVisibility(View.VISIBLE);

                cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetFinCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetFinOutCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetPBCard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetLECard.setBackgroundColor(Color.parseColor("#C1C1C1"));
                cardHolder.tvTargetExchCard.setBackgroundColor(Color.parseColor("#C1C1C1"));

                cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "ref");
                        context.startActivity(intent);
                    }
                });

                cardHolder.tvRetailCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "ref");
                        context.startActivity(intent);
                    }
                });

                cardHolder.tvExchCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "ref");
                        context.startActivity(intent);
                    }
                });

                cardHolder.tvFinCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "ref");
                        context.startActivity(intent);
                    }
                });

                cardHolder.tvFinOutCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "ref");
                        context.startActivity(intent);
                    }
                });cardHolder.tvPBCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "ref");
                        context.startActivity(intent);
                    }
                });cardHolder.tvLECard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "ref");
                        context.startActivity(intent);
                    }
                });

                final String userId = ""+realm.where(UserDetails.class).findFirst().getUserId();


                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(4,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getRetails(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });

                cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(5,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getExchanged(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });

                cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(6,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getIn_house_financed(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });

                cardHolder.tvFinOutCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(7,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getOut_house_financed(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });
                cardHolder.tvPBCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(8,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getPending_bookings(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });
                cardHolder.tvLECard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(9,""+locationsResults.get(position).getLocationName(),
                                ""+userId,
                                ""+locationsResults.get(position).getLocationAbs().getLive_enquiries(),
                                ""+resultEtvbrLocation.getRoleId(),""+locationsResults.get(position).getLocationId());
                        return true;
                    }
                });
            }
        } else {
            if (resultEtvbrLocation != null) {
                cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));

                Paint paint = new Paint();
                paint.setColor(Color.BLUE);
                paint.setFlags(Paint.UNDERLINE_TEXT_FLAG);


                cardHolder.tvRetailCard.setPaintFlags(paint.getFlags());
                cardHolder.tvRetailCard.setText("" + resultEtvbrLocation.getTotalAbs().getRetails());


                cardHolder.tvExchCard.setPaintFlags(paint.getFlags());
                cardHolder.tvExchCard.setText("" + resultEtvbrLocation.getTotalAbs().getExchanged());

                cardHolder.tvFinCard.setPaintFlags(paint.getFlags());
                cardHolder.tvFinCard.setText("" + resultEtvbrLocation.getTotalAbs().getIn_house_financed());

                cardHolder.tvFinOutCard.setPaintFlags(paint.getFlags());
                cardHolder.tvFinOutCard.setText("" + resultEtvbrLocation.getTotalAbs().getOut_house_financed());

                cardHolder.tvPBCard.setPaintFlags(paint.getFlags());
                cardHolder.tvPBCard.setText(getReadableData(resultEtvbrLocation.getTotalAbs().getPending_bookings()));

                cardHolder.tvLECard.setPaintFlags(paint.getFlags());
                cardHolder.tvLECard.setText(getReadableData(resultEtvbrLocation.getTotalAbs().getLive_enquiries()));

                cardHolder.tvTargetRetailCard.setText("" + resultEtvbrLocation.getTotalTargets().getRetails());
                cardHolder.tvTargetExchCard.setText("" + resultEtvbrLocation.getTotalTargets().getExchanged());
                cardHolder.tvTargetFinCard.setText("" + resultEtvbrLocation.getTotalTargets().getIn_house_financed());
                cardHolder.tvTargetFinOutCard.setText("" + resultEtvbrLocation.getTotalTargets().getOut_house_financed());
                cardHolder.tvTargetPBCard.setText("" + resultEtvbrLocation.getTotalTargets().getPending_bookings());
                cardHolder.tvTargetLECard.setText("" + resultEtvbrLocation.getTotalTargets().getLive_enquiries());

                cardHolder.tvTargetTitleCard.setVisibility(View.VISIBLE);
                cardHolder.tvLocationName.setVisibility(View.GONE);

                cardHolder.tvNameCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
                cardHolder.refFooter.setVisibility(View.VISIBLE);
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setText("Achieved");

                cardHolder.tvTargetTitleCard.setText("Target");

                cardHolder.tvRetailCard.setTextColor(Color.WHITE);
                cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);
                //cardHolder.tvTargetTitleCard.setTextColor(Color.WHITE);
                cardHolder.tvExchCard.setTextColor(Color.WHITE);
                cardHolder.tvFinCard.setTextColor(Color.WHITE);
                cardHolder.tvFinOutCard.setTextColor(Color.WHITE);
                cardHolder.tvPBCard.setTextColor(Color.WHITE);
                cardHolder.tvLECard.setTextColor(Color.WHITE);


                cardHolder.tvRetailCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvUserTotalCard.setTypeface(null, Typeface.BOLD);
                //cardHolder.tvTargetTitleCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvExchCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvFinCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvFinOutCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvPBCard.setTypeface(null, Typeface.BOLD);
                cardHolder.tvLECard.setTypeface(null, Typeface.BOLD);

                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);

                cardHolder.tvTargetRetailCard.setBackgroundColor(Color.parseColor("#8C94AB"));
                cardHolder.viewTargetCard.setBackgroundColor(Color.parseColor("#8C94AB"));


                final String userId = "" + realm.where(UserDetails.class).findFirst().getUserId();



                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(4, "All Location's Data",
                                "" + userId,
                                "" + resultEtvbrLocation.getTotalAbs().getRetails(),
                                "" + resultEtvbrLocation.getRoleId(), "");
                        return true;
                    }
                });

                cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(5, "All Location's Data",
                                "" + userId,
                                "" + resultEtvbrLocation.getTotalAbs().getExchanged(),
                                "" + resultEtvbrLocation.getRoleId(), "");
                        return true;
                    }
                });

                cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(6, "All Location's Data",
                                "" + userId,
                                "" + resultEtvbrLocation.getTotalAbs().getIn_house_financed(),
                                "" + resultEtvbrLocation.getRoleId(), "");
                        return true;
                    }
                });

                cardHolder.tvFinOutCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(7, "All Location's Data",
                                "" + userId,
                                "" + resultEtvbrLocation.getTotalAbs().getOut_house_financed(),
                                "" + resultEtvbrLocation.getRoleId(), "");
                        return true;
                    }
                });

                cardHolder.tvPBCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(8, "All Location's Data",
                                "" + userId,
                                "" + resultEtvbrLocation.getTotalAbs().getPending_bookings(),
                                "" + resultEtvbrLocation.getRoleId(), "");
                        return true;
                    }
                });
                cardHolder.tvLECard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        goToDetailsOfEtvbr(9, "All Location's Data",
                                "" + userId,
                                "" + resultEtvbrLocation.getTotalAbs().getLive_enquiries(),
                                "" + resultEtvbrLocation.getRoleId(), "");
                        return true;
                    }
                });

            }
        }
    }

    private void viewCardsGMRel(RecyclerView.ViewHolder holder, final int position) {
        final RefLocationCardHolder cardHolder = (RefLocationCardHolder) holder;
        //final int position = pos;
        cardHolder.llRetailsCard.setVisibility(View.GONE);
        cardHolder.tvExchCard.setVisibility(View.VISIBLE);
        cardHolder.tvFinCard.setVisibility(View.VISIBLE);
        cardHolder.tvFinOutCard.setVisibility(View.VISIBLE);
        cardHolder.tvPBCard.setVisibility(View.VISIBLE);
        cardHolder.tvLECard.setVisibility(View.VISIBLE);
        cardHolder.tvTargetFinCard.setVisibility(View.GONE);
        cardHolder.tvTargetFinOutCard.setVisibility(View.GONE);
        cardHolder.tvTargetPBCard.setVisibility(View.GONE);
        cardHolder.tvTargetLECard.setVisibility(View.GONE);
        cardHolder.tvTargetExchCard.setVisibility(View.GONE);
        cardHolder.viewTargetCard.setVisibility(View.GONE);
        cardHolder.rlFrameLayoutCard.setPadding(0, 6, 0, 6);

        Drawable drawableImg = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_img_location, null);
        cardHolder.imgUserCard.setBackground(drawableImg);

        Drawable drawable = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_etvbr_target_blue, null);
        cardHolder.tvTargetTitleCard.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);

        cardHolder.llinearLayout.setVisibility(View.GONE);
        //rels = results.get(position).getRel();
        if (position < resultEtvbrLocation.getLocations().size()) {
            locationsResults = realm.where(RefLocation.class).findAll();

            if (locationsResults != null) {

                cardHolder.tvRetailCard.setPaintFlags(0);
                cardHolder.tvRetailCard.setText("" + locationsResults.get(position).getLocationRel().getRetails());

                cardHolder.tvLocationName.setPaintFlags(0);
                cardHolder.tvLocationName.setText("" + locationsResults.get(position).getLocationName());

                cardHolder.tvExchCard.setPaintFlags(0);
                cardHolder.tvExchCard.setText(""+locationsResults.get(position).getLocationRel().getExchanged());

                cardHolder.tvFinCard.setPaintFlags(0);
                cardHolder.tvFinCard.setText(""+locationsResults.get(position).getLocationRel().getIn_house_financed());

                cardHolder.tvFinOutCard.setPaintFlags(0);
                cardHolder.tvFinOutCard.setText(""+locationsResults.get(position).getLocationRel().getOut_house_financed());


                cardHolder.tvPBCard.setPaintFlags(0);
                cardHolder.tvPBCard.setText(getReadableData(locationsResults.get(position).getLocationRel().getPending_bookings()));


                cardHolder.tvLECard.setPaintFlags(0);
                cardHolder.tvLECard.setText(getReadableData(locationsResults.get(position).getLocationRel().getLive_enquiries()));


                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvFinOutCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvPBCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvLECard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
                cardHolder.tvLocationName.setVisibility(View.VISIBLE);


                cardHolder.imgUserCard.setVisibility(View.VISIBLE);
                cardHolder.tvUserTotalCard.setVisibility(View.GONE);
                cardHolder.refFooter.setVisibility(View.GONE);
                cardHolder.refFooter.setVisibility(View.GONE);
                cardHolder.tvNameCard.setVisibility(View.GONE);
                //Picasso.with(context).load("https://s3-ap-southeast-1.amazonaws.com/salescrm-images/staging/uploads/profile_images//SATYANARYANA CH-48-1510145708.png").placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).into(cardHolder.imgUserCard);


                cardHolder.llInner.setBackgroundColor(Color.WHITE);
                cardHolder.tvRetailCard.setTextColor(Color.BLACK);
                cardHolder.tvUserTotalCard.setTextColor(Color.BLACK);
                cardHolder.tvExchCard.setTextColor(Color.BLACK);
                cardHolder.tvFinCard.setTextColor(Color.BLACK);
                cardHolder.tvFinOutCard.setTextColor(Color.BLACK);
                cardHolder.tvPBCard.setTextColor(Color.parseColor("#7ED321"));
                cardHolder.tvLECard.setTextColor(Color.parseColor("#7ED321"));

                cardHolder.viewReatilCard.setVisibility(View.VISIBLE);

                cardHolder.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EtvbrLocationActivity.class);
                        intent.putExtra("location_id", locationsResults.get(position).getLocationId());
                        intent.putExtra("location_name", locationsResults.get(position).getLocationName());
                        intent.putExtra("adapter", "ref");
                        context.startActivity(intent);
                    }
                });
            }
        } else {
            if (resultEtvbrLocation != null) {
                cardHolder.llInner.setBackgroundColor(Color.parseColor("#243F6D"));


                cardHolder.tvRetailCard.setPaintFlags(0);
                cardHolder.tvRetailCard.setText("" + resultEtvbrLocation.getTotalRel().getRetails());


                cardHolder.tvExchCard.setPaintFlags(0);
                cardHolder.tvExchCard.setText(""+resultEtvbrLocation.getTotalRel().getExchanged());

                cardHolder.tvFinCard.setPaintFlags(0);
                cardHolder.tvFinCard.setText(""+resultEtvbrLocation.getTotalRel().getIn_house_financed());

                cardHolder.tvFinOutCard.setPaintFlags(0);
                cardHolder.tvFinOutCard.setText(""+resultEtvbrLocation.getTotalRel().getOut_house_financed());

                cardHolder.tvPBCard.setPaintFlags(0);
                cardHolder.tvPBCard.setText(getReadableData(resultEtvbrLocation.getTotalRel().getPending_bookings()));

                cardHolder.tvLECard.setPaintFlags(0);
                cardHolder.tvLECard.setText(getReadableData(resultEtvbrLocation.getTotalRel().getLive_enquiries()));


                cardHolder.tvRetailCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvExchCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvFinCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvFinOutCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvPBCard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvLECard.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        return true;
                    }
                });

                cardHolder.tvTargetTitleCard.setVisibility(View.GONE);
                cardHolder.tvLocationName.setVisibility(View.GONE);

                cardHolder.tvNameCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setVisibility(View.VISIBLE);
                cardHolder.refFooter.setVisibility(View.VISIBLE);
                cardHolder.refFooter.setVisibility(View.VISIBLE);
                cardHolder.imgUserCard.setVisibility(View.GONE);
                cardHolder.tvUserTotalCard.setText("Total%");

                cardHolder.tvRetailCard.setTextColor(Color.WHITE);
                cardHolder.tvUserTotalCard.setTextColor(Color.WHITE);
                cardHolder.tvExchCard.setTextColor(Color.WHITE);
                cardHolder.tvFinCard.setTextColor(Color.WHITE);
                cardHolder.tvFinOutCard.setTextColor(Color.WHITE);
                cardHolder.tvPBCard.setTextColor(Color.WHITE);
                cardHolder.tvLECard.setTextColor(Color.WHITE);

                cardHolder.viewReatilCard.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        //resultSMs = realm.where(ResultSM.class).findAll();
        if(resultEtvbrLocation.getLocations()!= null && resultEtvbrLocation.isValid() ) {
            return (resultEtvbrLocation.getLocations().size() > 0) ? resultEtvbrLocation.getLocations().size() + 1 : 0;
        }else{
            return 0;
        }
    }

    public class RefLocationCardHolder extends RecyclerView.ViewHolder {

        ImageView imgUser, imgUserCard;
        TextView tvNameCard, tvRetailCard, tvExchCard, tvFinCard, tvFinOutCard, tvPBCard, tvLECard;
        TextView tvName, tvRetail;
        View viewReatilCard, viewExchCard, viewFinCard, viewFinOutCard, viewPBCard, viewLECard;
        View viewEnquiry, viewTd, viewVisit, viewBooking, viewReatil;
        LinearLayout llinearLayout;
        LinearLayout llInner;
        TextView tvUserTotalCard;
        TextView tvUserTotal;
        CardView cardView;
        LinearLayout llRetailsCard, llExchCard, llFinCard, llFinOutCard;
        TextView tvTargetTitleCard;
        TextView tvTargetRetailCard, tvTargetExchCard, tvTargetFinCard, tvTargetFinOutCard, tvTargetPBCard, tvTargetLECard;
        View viewTargetCard;
        RelativeLayout rlFrameLayoutCard;
        TextView tvLocationName;
        private LinearLayout refFooter;


        public RefLocationCardHolder(View itemView) {
            super(itemView);
            imgUser = (ImageView) itemView.findViewById(R.id.img_user);
            tvName = (TextView) itemView.findViewById(R.id.text_user);
            tvRetail = (TextView) itemView.findViewById(R.id.txt_retail);
            //llaLinearLayout = (LinearLayout) itemView.findViewById(R.id.ll_today_summary_main);
            tvUserTotal = (TextView) itemView.findViewById(R.id.text_user_total);
            viewEnquiry = (View) itemView.findViewById(R.id.enquiry_view);
            viewTd = (View) itemView.findViewById(R.id.td_view);
            viewVisit = (View) itemView.findViewById(R.id.visit_view);
            viewBooking = (View) itemView.findViewById(R.id.booking_view);
            viewReatil = (View) itemView.findViewById(R.id.retail_view);
            llinearLayout = (LinearLayout) itemView.findViewById(R.id.llinearLayout);
            llInner = (LinearLayout) itemView.findViewById(R.id.card_ll);

            imgUserCard = (ImageView) itemView.findViewById(R.id.img_user_card);
            tvNameCard = (TextView) itemView.findViewById(R.id.text_user_card);
            tvRetailCard = (TextView) itemView.findViewById(R.id.txt_retail_card);
            tvUserTotalCard = (TextView) itemView.findViewById(R.id.text_user_total_card);
            viewReatilCard = (View) itemView.findViewById(R.id.retail_view_card);
            cardView = (CardView) itemView.findViewById(R.id.cardview_etvbr);
            tvTargetTitleCard = (TextView) itemView.findViewById(R.id.tv_target_title_card);
            tvTargetRetailCard = (TextView) itemView.findViewById(R.id.txt_target_retail_card);
            viewTargetCard = (View) itemView.findViewById(R.id.target_view_card);
            rlFrameLayoutCard = (RelativeLayout) itemView.findViewById(R.id.rl_frame_layout_card);
            tvLocationName = (TextView) itemView.findViewById(R.id.location_name);
            llRetailsCard = (LinearLayout) itemView.findViewById(R.id.retail_ll_card);
            viewExchCard = (View) itemView.findViewById(R.id.exch_view_card);
            viewFinCard = (View) itemView.findViewById(R.id.fin_view_card);
            viewFinOutCard = (View) itemView.findViewById(R.id.fin_out_view_card);
            viewPBCard = (View) itemView.findViewById(R.id.pending_bookings_view_card);
            viewLECard = (View) itemView.findViewById(R.id.live_enquiries_view_card);
            llExchCard = (LinearLayout) itemView.findViewById(R.id.exch_ll_card);
            llFinCard = (LinearLayout) itemView.findViewById(R.id.fin_ll_card);
            llFinOutCard = (LinearLayout) itemView.findViewById(R.id.fin_out_ll_card);
            tvTargetExchCard = (TextView) itemView.findViewById(R.id.txt_target_exch__card);
            tvTargetFinCard = (TextView) itemView.findViewById(R.id.txt_target_fin_card);
            tvTargetFinOutCard = (TextView) itemView.findViewById(R.id.txt_target_fin_out_card);
            tvTargetPBCard = (TextView) itemView.findViewById(R.id.txt_target_pending_bookings_card);
            tvTargetLECard = (TextView) itemView.findViewById(R.id.txt_target_live_enquiries_card);
            tvExchCard = (TextView) itemView.findViewById(R.id.txt_exch_card);
            tvFinCard = (TextView) itemView.findViewById(R.id.txt_fin_card);
            tvFinOutCard = (TextView) itemView.findViewById(R.id.txt_fin_out_card);
            tvPBCard = (TextView) itemView.findViewById(R.id.txt_pending_bookings_card);
            tvLECard = (TextView) itemView.findViewById(R.id.txt_live_enquiries_card);
            refFooter = itemView.findViewById(R.id.ref_footer);
        }
    }

    private void goToDetailsOfEtvbr(int position, String title_name, String user_id, String etvbrtotal, String user_role_id, String user_location_id) {

        Intent etvbrDetails =  null;

        switch (position){
            case 4:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Retails");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 5:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Exchanged");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 6:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "In House Financed");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 7:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Out House Financed");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 8:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Pending Bookings");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
            case 9:
                etvbrDetails = new Intent(context, DetailsOfEtvbr.class);
                etvbrDetails.putExtra("clicked_position", "Live Enquiries");
                etvbrDetails.putExtra("title_name", title_name);
                etvbrDetails.putExtra("etvbrtotal", etvbrtotal);
                etvbrDetails.putExtra("user_id", user_id);
                etvbrDetails.putExtra("user_role_id", user_role_id);
                etvbrDetails.putExtra("user_location_id", user_location_id);
                etvbrDetails.putExtra("start_date", "-1");
                break;
        }
        if(etvbrDetails != null) {
            if(user_location_id.equalsIgnoreCase("")){
                pref.setLocationId(-1);
            }else {
                pref.setLocationId(Integer.parseInt(user_location_id));
            }
            context.startActivity(etvbrDetails);
        }
    }

    private String getReadableData(Integer data) {
        if(data==null) {
            return "--";
        }
        return ""+data;
    }

}
