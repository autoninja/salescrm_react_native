package com.salescrm.telephony.utils

import com.scottyab.aescrypt.AESCrypt
import java.security.GeneralSecurityException

class NinjaEncryption {
    val password = "NINJA_ENCRYPTION"
    fun encrypt(str  : String): String {
        return  try {
            AESCrypt.encrypt(password, str)
        } catch (e: GeneralSecurityException) {
            str;
        }
        catch (e: Exception) {
            str;
        }
    }
    fun decrypt(encrypted: String): String {
        return  try {
            AESCrypt.decrypt(password, encrypted)
        } catch (e: GeneralSecurityException) {
            encrypted;
        }
        catch (e: Exception) {
            encrypted;
        }
    }
}