package com.salescrm.telephony.services.service_handlers;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.interfaces.CallBack;
import com.salescrm.telephony.response.UserPointHelpResponse;
import com.salescrm.telephony.response.gamification.BadgesResponse;
import com.salescrm.telephony.response.gamification.ConsultantLeaderBoardResponse;
import com.salescrm.telephony.response.gamification.DSEPointsResponseModel;
import com.salescrm.telephony.response.gamification.ManagerLeaderBoardResponse;
import com.salescrm.telephony.response.gamification.TeamLeaderBoardResponse;
import com.salescrm.telephony.response.gamification.WallOfFameResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.WSConstants;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bannhi on 27/12/17.
 */

public class GamificationServiceHandler {

    private static GamificationServiceHandler INSTANCE;
    private static Context context;
    private ConnectionDetectorService connectionDetectorService;

    private GamificationServiceHandler(Context ctx) {
        context = ctx;
    }

    public static synchronized GamificationServiceHandler getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new GamificationServiceHandler(context);
        }
        return INSTANCE;

    }

    public void fetchManagerBoardData(String accessToken, String monthYear, final CallBack callBack ) {
        connectionDetectorService = new ConnectionDetectorService(context);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).fetchManagerBoardDetails(monthYear, new Callback<ManagerLeaderBoardResponse>() {
                @Override
                public void success(ManagerLeaderBoardResponse managerLeaderBoardResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());

                        }
                    }

                    if (managerLeaderBoardResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        callBack.onCallBack(managerLeaderBoardResponse);
                    } else {
                        callBack.onError(managerLeaderBoardResponse.getError()!=null?managerLeaderBoardResponse.getError().getDetails():"No Data");
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    callBack.onError(error!=null ? error.toString() : "Failed");
                }
            });

        } else {
            callBack.onError("No Internet Connection");
        }
    }

    public void fetchTeamLeaderBoardData(String accessToken, int locationId, String monthYear, final CallBack callBack ) {
        connectionDetectorService = new ConnectionDetectorService(context);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).fetchTeamLeaderBoardDetails(locationId, monthYear, new Callback<TeamLeaderBoardResponse>() {
                @Override
                public void success(TeamLeaderBoardResponse teamLeaderBoardResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());

                        }
                    }

                    if (teamLeaderBoardResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        callBack.onCallBack(teamLeaderBoardResponse);
                    } else {
                        callBack.onError(teamLeaderBoardResponse.getStatusCode());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    callBack.onError(error!=null ? error.toString() : "Failed");
                }
            });

        } else {
            callBack.onError("No Internet Connection");
        }
    }

    public void fetchDsePoints(String accessToken, int userId, String monthYear, final CallBack callBack) {
        connectionDetectorService = new ConnectionDetectorService(context);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).fetchDsePoints(userId, monthYear, new Callback<DSEPointsResponseModel>() {
                @Override
                public void success(DSEPointsResponseModel dsePointsResponseModel, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());

                        }
                    }

                    if (dsePointsResponseModel.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        callBack.onCallBack(dsePointsResponseModel);
                    } else {
                        callBack.onError(dsePointsResponseModel.getStatusCode());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    callBack.onError(error!=null ? error.toString() : "Failed");
                }
            });

        } else {
            callBack.onError("No Internet Connection");

        }
    }

    public void fetchDSEBadges(String accessToken, int userId, String monthYear) {
        connectionDetectorService = new ConnectionDetectorService(context);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).fetchDseBadges(userId, monthYear, new Callback<BadgesResponse>() {
                @Override
                public void success(BadgesResponse badgesResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());

                        }
                    }

                    if (badgesResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        SalesCRMApplication.getBus().post(new BadgesResponse(badgesResponse));
                    } else {
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    SalesCRMApplication.getBus().post(true);
                }
            });

        } else {

        }
    }

    public void fetchConsultantData(String accessToken, int locationId, String monthYear, final CallBack callBack) {
        connectionDetectorService = new ConnectionDetectorService(context);
        if (connectionDetectorService.isConnectingToInternet()) {

            ApiUtil.GetRestApiWithHeader(accessToken).fetchConsultantData(locationId, monthYear, new Callback<ConsultantLeaderBoardResponse>() {
                @Override
                public void success(ConsultantLeaderBoardResponse consultantLeaderBoardResponse, Response response) {
                    System.out.println("RESPONSE AGAYA");
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());

                        }
                    }
                    if (consultantLeaderBoardResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {

                        callBack.onCallBack(consultantLeaderBoardResponse);
                    } else {
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("FAILED"+error.toString());
                }
            });

        } else {

        }
    }

    public void fetchWallOfFameData(String accessToken, int locationId, String monthYear) {
        connectionDetectorService = new ConnectionDetectorService(context);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).fetchWallOfFameData(locationId, monthYear, new Callback<WallOfFameResponse>() {
                @Override
                public void success(WallOfFameResponse wallOfFameResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());

                        }
                    }
                    if (wallOfFameResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        SalesCRMApplication.getBus().post(new WallOfFameResponse(wallOfFameResponse));
                    } else {
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("FAILED"+error.toString());
                }
            });

        } else {

        }
    }
  /*  public void fetchGamificationConfig(String accessToken) {
        connectionDetectorService = new ConnectionDetectorService(context);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accessToken).fetchGamificationConfig(new Callback<GamificationConfigResp>() {
                @Override
                public void success(GamificationConfigResp gamificationConfigResp, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());

                        }
                    }
                    if (gamificationConfigResp.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {

                    } else {
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("FAILED"+error.toString());
                }
            });

        } else {

        }
    }*/

    public void fetchUserHelpData(String accessToken, String userId, String monthYear, final CallBack callBack) {
        connectionDetectorService = new ConnectionDetectorService(context);
        if (connectionDetectorService.isConnectingToInternet()) {

            ApiUtil.GetRestApiWithHeader(accessToken).fetchUserHelpData(userId, monthYear, new Callback<UserPointHelpResponse>() {
                @Override
                public void success(UserPointHelpResponse userPointHelpResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());

                        }
                    }
                    if (userPointHelpResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        callBack.onCallBack(userPointHelpResponse);
                    } else {
                        callBack.onError("Error in the response");
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("FAILED"+error.toString());
                    callBack.onError("Error in the response");
                }
            });

        } else {
            callBack.onError("No internet connection");
        }
    }

}
