import React, { Component } from 'react'
import { View, Text, Image, TouchableWithoutFeedback, TouchableOpacity, ScrollView, Alert} from 'react-native'


export default class ETVBRFiltersItemMain extends Component {
  constructor(props) {
    super(props);
  }
  render() {

    return(
      <View>

      <View style={{flexDirection:'row',marginTop:10,padding:16, backgroundColor:'#fff', justifyContent:'space-between', borderRadius:6}}>

      <Text style={{color:(getETVBRFilterItem(this.props.key_name)>0?'#007fff':'#303030'),fontSize:18}}>{this.props.title}</Text>
      <Image source={ require( '../../images/ic_right_black.png')} style={{width:24, height:24}}/>
      </View>

      <View style={{backgroundColor:'#cccccc', height:1, width:'100%'}}/>

      </View>
    );
  }
}
