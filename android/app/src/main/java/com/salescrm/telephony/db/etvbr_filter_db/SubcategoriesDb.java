package com.salescrm.telephony.db.etvbr_filter_db;

import io.realm.RealmObject;

/**
 * Created by prateek on 8/8/18.
 */

public class SubcategoriesDb extends RealmObject {

    private String id;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
