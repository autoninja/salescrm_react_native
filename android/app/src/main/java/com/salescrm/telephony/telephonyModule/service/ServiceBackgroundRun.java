package com.salescrm.telephony.telephonyModule.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.salescrm.telephony.R;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.telephonyModule.PhonecallReceiver;
import com.salescrm.telephony.telephonyModule.db.TelephonyDbHandler;
import com.salescrm.telephony.telephonyModule.interfaces.DbRecordSyncCountListener;
import com.salescrm.telephony.utils.NotificationsUI;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by subaharan on 30/12/15.
 */
public class ServiceBackgroundRun extends Service implements Callback {
    final Handler handler = new Handler();
    PhonecallReceiver alarm = new PhonecallReceiver();
    String details;
    private Preferences pref = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startMyOwnForeground(){
        String channelName = "Background Service";
        NotificationChannel chan = new NotificationChannel(NotificationsUI.CHANNEL_ID_BACKGROUND, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NotificationsUI.CHANNEL_ID_BACKGROUND);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("NinjaCRM is Running")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(NotificationsUI.BACKGROUND_ID, notification);
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();


        try {
            if (Build.VERSION.SDK_INT >= 26) {
                startMyOwnForeground();
            }
        } catch (Exception e) {

        }
        pref = Preferences.getInstance();
        if (this != null) {
            pref.load(this);
            handler.postDelayed(new Runnable() {
                public void run() {
                    System.out.println("SERVICE IS CALLED");
                    pref.setmEnableMultithread(pref.getmEnableMultithread() + 1);
                    if (pref.getmEnableMultithread() == 30) {
                        pref.setmEnableMultithread(0);
                        pref.setLimitSyncStatus(false);
                    }
                    System.out.println("SERVICE IS CALLED AND STATUS IS" + pref.getmCallState());
                    if (pref.getmCallState() == 0) {
//                        System.out.println("SERVICE IS CALLED AND count IS"+
//                                TelephonyDbHandler.getInstance().dbRecordSyncCount(null));
                        TelephonyDbHandler.getInstance().dbRecordSyncCount(new DbRecordSyncCountListener() {
                            @Override
                            public void dbRecordSyncCountQueryResult(int count) {
                                System.out.println("TELEPHONY SERVICE IS CALLED AND count IS" + count);
                                pref.setLimitSyncStatus(false);
                                if (count > 0) {
                                    getApplicationContext().startService(new Intent(getApplicationContext(), SyncRecordInBackground.class));
                                } else {
                                    ServiceBackgroundRun.this.stopSelf();
                                    System.out.println("Service in killed");
                                }
                            }
                        });


                    }
                    handler.postDelayed(this, 1000 * 60 * 2); //now it is every 2 minutes
                }
            }, 1000 * 60 * 2);
        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }

    @Override
    public void onStart(Intent intent, int startId) {
    }

    @Override
    public void success(Object o, Response response) {
        //update db that records are synced for the records
    }

    @Override
    public void failure(RetrofitError error) {
        //handle error case
    }

}

