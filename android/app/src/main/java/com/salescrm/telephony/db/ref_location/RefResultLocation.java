package com.salescrm.telephony.db.ref_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 8/11/17.
 */

public class RefResultLocation extends RealmObject {

    @PrimaryKey
    @SerializedName("role_id")
    @Expose
    private Integer role_id;
    @SerializedName("total_abs")
    @Expose
    private RefTotalAbs total_abs;
    @SerializedName("total_rel")
    @Expose
    private RefTotalRel total_rel;
    @SerializedName("total_targets")
    @Expose
    private RefTotalTargets total_targets;
    @SerializedName("locations")
    @Expose
    private RealmList<RefLocation> locations = null;

    public Integer getRoleId() {
        return role_id;
    }

    public void setRoleId(Integer roleId) {
        this.role_id = roleId;
    }

    public RefTotalAbs getTotalAbs() {
        return total_abs;
    }

    public void setTotalAbs(RefTotalAbs totalAbs) {
        this.total_abs = totalAbs;
    }

    public RefTotalRel getTotalRel() {
        return total_rel;
    }

    public void setTotalRel(RefTotalRel totalRel) {
        this.total_rel = totalRel;
    }

    public RefTotalTargets getTotalTargets() {
        return total_targets;
    }

    public void setTotalTargets(RefTotalTargets totalTargets) {
        this.total_targets = totalTargets;
    }

    public RealmList<RefLocation> getLocations() {
        return locations;
    }

    public void setLocations(RealmList<RefLocation> locations) {
        this.locations = locations;
    }
}
