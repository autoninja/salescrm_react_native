import _ from 'lodash'
import { BOOKING_STAGES } from '../BookingUtils/BookingUtils'
import {
    CUSTOMER_TYPES,
    CUSTOMER_TYPE_ACTION_VALUES,
    CARE_OF_TYPES,
    FINANCE_TYPES,
    EXCHANGE_ITEMS,
    BUYER_TYPE_EXCHANGE_CAR,
    VIN_ALLOCATION_IDS,
    VIN_ALLOCATION_ACTION_VALUES
} from './BookingModelConstants'
import Utils from '../../../utils/Utils'

let getInterestedVehicleModelsItems = (interestedVehicle) => {
    let list = [];
    if (interestedVehicle) {
        interestedVehicle.brand_models.map((model) => {
            list.push({
                id: model.model_id,
                text: model.model_name,
                payload: { brand_id: interestedVehicle.brand_id, brand_name: interestedVehicle.brand_name }
            });
        })
    }
    return list;
}
let getInterestedVehicleVariantsItems = (interestedVehicle, modelItem) => {
    let list = [];
    if (interestedVehicle && modelItem) {
        interestedVehicle.brand_models.map((model) => {
            if (model.model_id == modelItem.id) {
                model.car_variants.map((variant) => {
                    if (list.map(item => item.id).indexOf(variant.variant_id) == -1) {
                        list.push({ id: variant.variant_id, text: variant.variant, payload: variant, model_id: modelItem.id });
                    }
                })
            }
        })
    }
    return list;
}

let getInterestedVehicleColorsItems = (interestedVehicle, variantItem) => {
    let list = [];
    if (interestedVehicle && variantItem) {
        interestedVehicle.brand_models.map((model) => {
            if (model.model_id == variantItem.model_id) {
                model.car_variants.map((variant) => {
                    if (variant.variant_id == variantItem.id) {
                        list.push({ id: variant.color_id, text: variant.color, payload: variant });
                    }
                })
            }
        })
    }
    return list;
}

let getBooleanValid = (val) => {
    if (val) {
        return true;
    }
    return false;
}
let getStringValue = (val) => {
    if (val || val == 0) {
        return val;
    }
    return "";
}
let getEditable = (val) => {
    if (val === false) {
        return false;
    }
    return true;
}
let isUserBillingExecutive = (userRoles) => {
    let billingExecutive = false;
    userRoles.map((userRole) => {
        if (userRole.id == 11) {
            billingExecutive = true;
        }
    })
    return billingExecutive;
}
updateEditable = (initModel) => {
    initModel.vehicleSection.vehicleModel.editable = false;
    initModel.vehicleSection.vehicleVariant.editable = false;
    initModel.vehicleSection.vehicleColor.editable = false;
    initModel.vehicleSection.bookingAmount.editable = false;
    initModel.vehicleSection.vinNumber.editable = false;
    initModel.vehicleSection.invoiceMobileNumber.editable = false;
    initModel.vehicleSection.invoiceDate.editable = false;

    initModel.customerDetailsSection.bookingName.editable = false;
    initModel.customerDetailsSection.dateOfBirth.editable = false;
    initModel.customerDetailsSection.customerType.editable = false;
    initModel.customerDetailsSection.customerPAN.editable = false;
    initModel.customerDetailsSection.customerAadhaar.editable = false;
    initModel.customerDetailsSection.companyGST.editable = false;
    initModel.customerDetailsSection.customerAddress.editable = false;
    initModel.customerDetailsSection.customerPincode.editable = false;
    initModel.customerDetailsSection.customerCareOfType.editable = false;
    initModel.customerDetailsSection.customerCareOfTypeValue.editable = false;

    initModel.priceBreakupSection.exShowRoomPrice.editable = false;
    initModel.priceBreakupSection.insurance.editable = false;
    initModel.priceBreakupSection.registration.editable = false;
    initModel.priceBreakupSection.accessories.editable = false;
    initModel.priceBreakupSection.extendedWarranty.editable = false;
    initModel.priceBreakupSection.others.editable = false;

    initModel.discountSection.corporate.editable = false;
    initModel.discountSection.exchangeLoyaltyBonus.editable = false;
    initModel.discountSection.referralBonus.editable = false;
    initModel.discountSection.oemScheme.editable = false;
    initModel.discountSection.dealer.editable = false;
    initModel.discountSection.freeAccessories.editable = false;

    initModel.exchangeFinanceSection.financeType.editable = false;
    initModel.exchangeFinanceSection.exchange.editable = false;

}
getInitModel = (info, userRoles) => {
    let actionVisibility = true;
    let initModel = {
        vehicleSection: {
            title: "Vehicle Details",
            vehicleModel: {
            },
            vehicleVariant: {
            },
            vehicleColor: {
            },
            bookingAmount: {
                editable: false,
                validation: '[1-9]\d*'
            },
            vinNumber: {
                visible: false
            },
            invoiceMobileNumber: {
                visible: false,
                validation: '^([1-9][0-9]{9})$'
            },
            invoiceDate: {
                visible: false
            },
            deliveryDate: {
                visible: false
            },
            deliveryChallan: {
                visible: false
            },
            allocation: {
                visible: false

            },
            allocationVinNumber: {
                visible: false
            }


        },
        customerDetailsSection: {
            bookingName: {},
            dateOfBirth: {},
            customerType: {
            },
            customerPAN: {
                validation: '^([A-Za-z0-9]{10})$'
            },
            customerAadhaar: {
                validation: '^([0-9]{12})$'
            },
            companyGST: {
                validation: '^([A-Za-z0-9]{15})$'
            },
            customerAddress: {
                mandatory: true
            },
            customerPincode: {
                mandatory: true,
                validation: '^([0-9]{6})$'
            },
            customerCareOfType: {
                value: CARE_OF_TYPES[0],
                valid: true,
                visible: true
            },
            customerCareOfTypeValue: {
                mandatory: true
            }
        },
        priceBreakupSection: {
            mandatory: true,
            exShowRoomPrice: {
                mandatory: true,
            },
            insurance: {},
            registration: {},
            accessories: {},
            extendedWarranty: {},
            others: {}
        },
        discountSection: {
            corporate: {},
            exchangeLoyaltyBonus: {},
            referralBonus: {},
            oemScheme: {},
            dealer: {},
            freeAccessories: {}
        },
        exchangeFinanceSection: {
            mandatory: true,
            financeType: {
                mandatory: true
            },
            exchange: {
                mandatory: true,
                visible: false
            }

        }
    }
    let { c360Information, stageId, leadCarId, action, booking_id } = info;
    if (action == "details_booking") {
        switch (parseInt(stageId)) {
            case BOOKING_STAGES.STAGE_BOOKED:
                stageId = BOOKING_STAGES.STAGE_INIT;
                break;
            case BOOKING_STAGES.STAGE_FULL_PAYMENT_RECEIVED:
                stageId = BOOKING_STAGES.STAGE_BOOKED;
                break;
            case BOOKING_STAGES.STAGE_INVOICED:
                stageId = BOOKING_STAGES.STAGE_FULL_PAYMENT_RECEIVED;
                actionVisibility = false;
                break;
            case BOOKING_STAGES.STAGE_DELIVERED:
                actionVisibility = false;
                break;
        }
    }
    switch (parseInt(stageId)) {
        case BOOKING_STAGES.STAGE_INIT:
            initModel.vehicleSection.bookingAmount.editable = true;

            initModel.vehicleSection.allocation.visible = true;
            initModel.vehicleSection.allocationVinNumber.visible = true;

            //Customer details
            initModel.customerDetailsSection.customerAddress.mandatory = false
            initModel.customerDetailsSection.customerAddress.valid = true
            initModel.customerDetailsSection.customerPincode.mandatory = false;
            initModel.customerDetailsSection.customerPincode.valid = true;
            initModel.customerDetailsSection.customerCareOfTypeValue.mandatory = false;
            initModel.customerDetailsSection.customerCareOfTypeValue.valid = true;


            //Price sections
            initModel.priceBreakupSection.mandatory = false;
            initModel.priceBreakupSection.exShowRoomPrice.mandatory = false;
            initModel.priceBreakupSection.exShowRoomPrice.valid = true;

            //Discount
            initModel.discountSection.mandatory = false;

            //Exchange/Finance
            initModel.exchangeFinanceSection.mandatory = false;
            initModel.exchangeFinanceSection.financeType.mandatory = false;
            initModel.exchangeFinanceSection.financeType.valid = true;
            initModel.exchangeFinanceSection.exchange.mandatory = false;
            initModel.exchangeFinanceSection.exchange.valid = true;
            break;
        case BOOKING_STAGES.STAGE_BOOKED:
            initModel.vehicleSection.allocation.visible = true;
            initModel.vehicleSection.allocationVinNumber.visible = true;
            //customer section
            break;
        case BOOKING_STAGES.STAGE_FULL_PAYMENT_RECEIVED:
            initModel.vehicleSection.vinNumber.visible = true;
            initModel.vehicleSection.invoiceMobileNumber.visible = true;
            initModel.vehicleSection.invoiceDate.visible = true;
            if (action == "details_booking") {
                updateEditable(initModel);
            }
            break;
        case BOOKING_STAGES.STAGE_INVOICED:
            initModel.vehicleSection.vinNumber.visible = true;
            initModel.vehicleSection.invoiceMobileNumber.visible = true;
            initModel.vehicleSection.invoiceDate.visible = true;
            initModel.vehicleSection.deliveryChallan.visible = true;
            initModel.vehicleSection.deliveryDate.visible = true;
            updateEditable(initModel);
            break;
        case BOOKING_STAGES.STAGE_DELIVERED:
            initModel.vehicleSection.vinNumber.visible = true;
            initModel.vehicleSection.invoiceMobileNumber.visible = true;
            initModel.vehicleSection.invoiceDate.visible = true;
            initModel.vehicleSection.deliveryChallan.visible = true;
            initModel.vehicleSection.deliveryDate.visible = true;
            updateEditable(initModel);
            initModel.vehicleSection.deliveryChallan.editable = false;
            initModel.vehicleSection.deliveryDate.editable = false;
            break;
    }
    if (c360Information) {
        if (c360Information.interestedCarsDetails) {
            c360Information.interestedCarsDetails.map((interestedCar) => {
                if (interestedCar.lead_car_id == leadCarId) {
                    if (getBooleanValid(interestedCar.model_id) && getBooleanValid(interestedCar.model)) {
                        initModel.vehicleSection.vehicleModel.value = {
                            id: interestedCar.model_id,
                            text: interestedCar.model
                        }
                        initModel.vehicleSection.vehicleModel.valid = true;
                    }
                    if (getBooleanValid(interestedCar.variant_id) && getBooleanValid(interestedCar.variant)) {
                        initModel.vehicleSection.vehicleVariant.value = {
                            id: interestedCar.variant_id,
                            text: interestedCar.variant,
                            model_id: interestedCar.model_id
                        }
                        initModel.vehicleSection.vehicleVariant.valid = true;
                    }
                    if (getBooleanValid(interestedCar.color_id) && getBooleanValid(interestedCar.color)) {
                        initModel.vehicleSection.vehicleColor.value = {
                            id: interestedCar.color_id,
                            text: interestedCar.color,
                            payload: {
                                fuel_type_id: interestedCar.fuel_type_id
                            }
                        }
                        initModel.vehicleSection.vehicleColor.valid = true;
                    }
                    let dmsData = interestedCar.carDmsData.filter((dms => dms.booking_id == booking_id))[0];
                    if (dmsData) {
                        initModel.vehicleSection.bookingAmount.value = dmsData.booking_amount;
                        initModel.vehicleSection.bookingAmount.valid = new RegExp(initModel.vehicleSection.bookingAmount.validation)
                            .test(dmsData.booking_amount);;

                        initModel.vehicleSection.vinNumber.value = dmsData.invoice_vin_no;
                        initModel.vehicleSection.vinNumber.valid = getBooleanValid(dmsData.invoice_vin_no);

                        initModel.vehicleSection.invoiceMobileNumber.value = dmsData.invoice_mob_no;
                        initModel.vehicleSection.invoiceMobileNumber.valid = new RegExp(initModel.vehicleSection.invoiceMobileNumber.validation)
                            .test(dmsData.invoice_mob_no);

                        if (getBooleanValid(dmsData.invoice_date)) {
                            let date = new Utils.getDate(dmsData.invoice_date);
                            initModel.vehicleSection.invoiceDate.value = date;
                            initModel.vehicleSection.invoiceDate.valid = true;
                        }


                        initModel.vehicleSection.deliveryChallan.value = dmsData.delivery_number;
                        initModel.vehicleSection.deliveryChallan.valid = getBooleanValid(dmsData.delivery_number);

                        if (getBooleanValid(dmsData.delivery_date)) {
                            let date = new Utils.getDate(dmsData.delivery_date);
                            initModel.vehicleSection.deliveryDate.value = date;
                            initModel.vehicleSection.deliveryDate.valid = true;
                        }


                        if (dmsData.booking_details) {
                            //Customer details sections:
                            let customerDetails = dmsData.booking_details.customer_details;
                            if (customerDetails) {

                                initModel.customerDetailsSection.bookingName.value = customerDetails.booking_name;
                                initModel.customerDetailsSection.bookingName.valid = getBooleanValid(customerDetails.booking_name);

                                if (getBooleanValid(customerDetails.dob)) {
                                    let date = new Utils.getDate(customerDetails.dob);
                                    initModel.customerDetailsSection.dateOfBirth.value = date;
                                    initModel.customerDetailsSection.dateOfBirth.valid = true;
                                }
                                initModel.customerDetailsSection.customerPAN.value = customerDetails.pan;
                                initModel.customerDetailsSection.customerPAN.valid = new RegExp(initModel.customerDetailsSection.customerPAN.validation)
                                    .test(customerDetails.pan);

                                initModel.customerDetailsSection.customerAadhaar.value = customerDetails.aadhaar;
                                initModel.customerDetailsSection.customerAadhaar.valid = new RegExp(initModel.customerDetailsSection.customerAadhaar.validation)
                                    .test(customerDetails.aadhaar);

                                initModel.customerDetailsSection.companyGST.value = customerDetails.gst;
                                initModel.customerDetailsSection.companyGST.valid = new RegExp(initModel.customerDetailsSection.companyGST.validation)
                                    .test(customerDetails.gst);


                                initModel.customerDetailsSection.customerAddress.value = customerDetails.address;
                                if (initModel.customerDetailsSection.customerAddress.mandatory) {
                                    initModel.customerDetailsSection.customerAddress.valid = getBooleanValid(customerDetails.address);
                                }

                                initModel.customerDetailsSection.customerPincode.value = customerDetails.pin_code;
                                if (initModel.customerDetailsSection.customerPincode.mandatory) {
                                    initModel.customerDetailsSection.customerPincode.valid = new RegExp(initModel.customerDetailsSection.customerPincode.validation)
                                        .test(customerDetails.pin_code);
                                }
                                if (getBooleanValid(customerDetails.care_of_type)) {
                                    CARE_OF_TYPES.map((careOfType) => {
                                        if (careOfType.text == customerDetails.care_of_type) {
                                            initModel.customerDetailsSection.customerCareOfType.value = careOfType;
                                        }
                                    })
                                }
                                initModel.customerDetailsSection.customerCareOfTypeValue.value = customerDetails.care_of;
                                if (initModel.customerDetailsSection.customerCareOfTypeValue.mandatory) {
                                    initModel.customerDetailsSection.customerCareOfTypeValue.valid = getBooleanValid(customerDetails.care_of);
                                }

                            }

                            //Price sections:
                            let priceBreakup = dmsData.booking_details.price_breakup;
                            if (priceBreakup) {
                                initModel.priceBreakupSection.exShowRoomPrice.value = priceBreakup.ex_showroom
                                if (initModel.priceBreakupSection.exShowRoomPrice.mandatory) {
                                    initModel.priceBreakupSection.exShowRoomPrice.valid = parseInt(priceBreakup.ex_showroom) > 0
                                }
                                initModel.priceBreakupSection.insurance.value = priceBreakup.insurance
                                initModel.priceBreakupSection.registration.value = priceBreakup.registration
                                initModel.priceBreakupSection.accessories.value = priceBreakup.accessories_sold
                                initModel.priceBreakupSection.extendedWarranty.value = priceBreakup.ew
                                initModel.priceBreakupSection.others.value = priceBreakup.others
                            }

                            //Discount sections:
                            let discounts = dmsData.booking_details.discounts;
                            if (discounts) {
                                initModel.discountSection.corporate.value = discounts.corporate
                                initModel.discountSection.exchangeLoyaltyBonus.value = discounts.exchange_bonus
                                initModel.discountSection.referralBonus.value = discounts.loyalty_bonus
                                initModel.discountSection.oemScheme.value = discounts.oem_scheme
                                initModel.discountSection.dealer.value = discounts.dealer
                                initModel.discountSection.freeAccessories.value = discounts.free_accessories
                            }
                            //Payment sections:
                            let paymentDetails = dmsData.booking_details.payment_details;
                            if (paymentDetails) {
                                if (getBooleanValid(paymentDetails.finance_type)) {
                                    FINANCE_TYPES.map((financeType) => {
                                        if (financeType.key == paymentDetails.finance_type) {
                                            initModel.exchangeFinanceSection.financeType.value = paymentDetails.finance_type;
                                            initModel.exchangeFinanceSection.financeType.valid = true;
                                        }
                                    })
                                }

                                if (getBooleanValid(paymentDetails.exchanged)) {
                                    EXCHANGE_ITEMS.map((exchangeItem) => {
                                        if (exchangeItem.key == paymentDetails.exchanged) {
                                            initModel.exchangeFinanceSection.exchange.value = paymentDetails.exchanged;
                                            initModel.exchangeFinanceSection.exchange.valid = true;
                                        }
                                    })
                                }
                            }

                            let vinAllocationDetails = dmsData.booking_details.vin_allocation_details;

                            //Allocation module:
                            if (vinAllocationDetails) {
                                if (getBooleanValid(vinAllocationDetails.vin_allocation_status)) {
                                    initModel.vehicleSection.title = initModel.vehicleSection.title +
                                        "(" + vinAllocationDetails.vin_allocation_status + ")"
                                    initModel.vehicleSection.allocationVinNumber.value = vinAllocationDetails.vin_no;
                                    let billingExecutive = isUserBillingExecutive(userRoles);

                                    if (vinAllocationDetails.vin_no) {
                                        initModel.vehicleSection.allocationVinNumber.valid = true;
                                        if (billingExecutive) {
                                            initModel.vehicleSection.allocationVinNumber.editable = true;
                                        }
                                        else {
                                            initModel.vehicleSection.allocationVinNumber.editable = false;
                                        }
                                    }
                                    if (initModel.vehicleSection.allocation.visible) {
                                        let vinAllocationItems = []
                                        let allottedValue = {
                                            id: vinAllocationDetails.vin_allocation_status_id,
                                            text: vinAllocationDetails.vin_allocation_status
                                        }
                                        vinAllocationItems.push(allottedValue);
                                        initModel.vehicleSection.allocation.value = allottedValue;
                                        info.vinAllocationStatuses.map((vinAllocationStatus) => {
                                            if (vinAllocationStatus.id != allottedValue.id
                                                && vinAllocationStatus.id != VIN_ALLOCATION_IDS.TO_BE_ALLOCATED) {
                                                vinAllocationStatus.role_ids.map((vinAllocationStatusRoleID) => {
                                                    for (let i = 0; i < userRoles.length; i++) {
                                                        if (vinAllocationStatusRoleID == userRoles[i].id) {
                                                            if (vinAllocationItems.map(vinItemCheck => vinItemCheck.id).indexOf(vinAllocationStatus.id) == -1) {
                                                                vinAllocationItems.push({
                                                                    id: vinAllocationStatus.id,
                                                                    text: vinAllocationStatus.name
                                                                })
                                                            }
                                                            break;
                                                        }
                                                    }

                                                })
                                            }
                                        })

                                        if (vinAllocationItems.length == 1) {
                                            initModel.vehicleSection.allocation.visible = false;
                                            initModel.vehicleSection.allocationVinNumber.visible = false;
                                        }
                                        else if (!billingExecutive) {
                                            switch (parseInt(vinAllocationDetails.vin_allocation_status_id)) {
                                                case VIN_ALLOCATION_IDS.TO_BE_ALLOCATED:
                                                case VIN_ALLOCATION_IDS.UN_ALLOCATED:
                                                case VIN_ALLOCATION_IDS.TO_ORDER:
                                                    initModel.vehicleSection.allocation.visible = false;
                                                    initModel.vehicleSection.allocationVinNumber.visible = false;
                                                    break;
                                            }
                                        }
                                        else {
                                            switch (parseInt(vinAllocationDetails.vin_allocation_status_id)) {
                                                case VIN_ALLOCATION_IDS.TO_BE_ALLOCATED:
                                                case VIN_ALLOCATION_IDS.UN_ALLOCATED:
                                                case VIN_ALLOCATION_IDS.TO_ORDER:
                                                    initModel.vehicleSection.allocationVinNumber.visible = false;
                                                    break;
                                            }
                                        }

                                        initModel.vehicleSection.allocation.listItems = vinAllocationItems;
                                        initModel.vehicleSection.allocation.actionValues = VIN_ALLOCATION_ACTION_VALUES
                                    }
                                }


                            }
                            else {
                                initModel.vehicleSection.allocation.visible = false;
                                initModel.vehicleSection.allocationVinNumber.visible = false;
                            }
                        }
                        else {
                            initModel.vehicleSection.allocation.visible = false;
                            initModel.vehicleSection.allocationVinNumber.visible = false;
                        }
                    }
                    else {
                        initModel.vehicleSection.allocation.visible = false;
                        initModel.vehicleSection.allocationVinNumber.visible = false;
                    }

                }
            })
        }
        //Filling customer details
        if (c360Information.customerDetails) {
            let customerDetails = c360Information.customerDetails;
            if (getBooleanValid(customerDetails.customer_type_id) && getBooleanValid(customerDetails.type_of_customer)) {
                initModel.customerDetailsSection.customerType.value = {
                    id: customerDetails.customer_type_id,
                    text: customerDetails.type_of_customer,
                }
                initModel.customerDetailsSection.customerType.valid = true;
                if (customerDetails.customer_type_id == CUSTOMER_TYPES[0].id
                    || customerDetails.customer_type_id == CUSTOMER_TYPES[1].id
                    || customerDetails.customer_type_id == CUSTOMER_TYPES[2].id
                    || customerDetails.customer_type_id == CUSTOMER_TYPES[4].id) {
                    initModel.customerDetailsSection.companyGST.visible = false;
                    initModel.customerDetailsSection.customerAadhaar.visible = true;
                    initModel.customerDetailsSection.customerCareOfType.visible = true;
                    initModel.customerDetailsSection.customerCareOfTypeValue.visible = true;
                }
                else {
                    initModel.customerDetailsSection.companyGST.visible = true;
                    initModel.customerDetailsSection.customerAadhaar.visible = false;
                    initModel.customerDetailsSection.customerCareOfType.visible = false;
                    initModel.customerDetailsSection.customerCareOfTypeValue.visible = false;
                }
            }
            if (getBooleanValid(customerDetails.buyer_type_id)
                && customerDetails.buyer_type_id == BUYER_TYPE_EXCHANGE_CAR) {
                initModel.exchangeFinanceSection.exchange.visible = true;
            }
        }

    }
    return { initModel, actionVisibility };
}
let getTotalValue = (items, isValueNegative) => {
    let value = 0;
    items.map((item) => {
        if (item.value && !isNaN(item.value)) {
            if (isValueNegative) {
                value -= parseInt(item.value);
            }
            else {
                value += parseInt(item.value);

            }
        }
    })
    return value;
}
let getSectionValidation = (section, allSections) => {
    let obj = {}
    if (section.id == "discount") {
        let totalDiscount = Math.abs(getTotalValue(section.items));
        let totalPriceBreakup = 0;
        allSections.map((item) => {
            if (item.id == "price_breakup") {
                totalPriceBreakup = Math.abs(getTotalValue(item.items));
            }
        })
        obj = {
            valid: totalDiscount <= totalPriceBreakup,
            dataChanged: totalDiscount > 0 || totalPriceBreakup > 0
        }
    }
    else if (section.id == "price_breakup") {
        let exShowRoomCheck = false;
        section.items.map((priceItem) => {
            if (priceItem.key == "ex_showroom_price") {
                exShowRoomCheck = parseInt(priceItem.value) > 0;
            }
        })
        obj = {
            valid: exShowRoomCheck,
            dataChanged: exShowRoomCheck
        }
    }
    else {
        let valid = true;
        let dataChanged = false;
        section.items.map((item) => {
            if (item.visible) {
                valid = valid && item.valid;
                if (item.value) {
                    dataChanged = true;
                }
            }
        })
        obj = {
            valid,
            dataChanged
        }
    }
    return obj;
}
let BookingModel = {
    shallowCopy: (formDataDest, formDataSource) => {
        formDataSource.map((sourceSection, i) => {
            sourceSection.items.map((sectionItem, j) => {
                formDataDest[i].items[j].value = sectionItem.value;
                formDataDest[i].items[j].valid = sectionItem.valid;
                formDataDest[i].items[j].listItems = sectionItem.listItems;
            });
        })
    },
    getBookingSections: (info, interestedVehicles, userRoles) => {
        let { initModel, actionVisibility } = getInitModel(info, userRoles);
        return {
            actionVisibility,
            sections:
                [
                    {
                        index: 0, id: 'vehicle', title: initModel.vehicleSection.title, expand: false,
                        mandatory: true,
                        getSectionValidation: function (section, allSections) {
                            return getSectionValidation(section, allSections);
                        },
                        items: [
                            vehicle_model = {
                                index: 0,
                                key: 'vehicle_model',
                                server_id: 'model_id',
                                view: 'picker',
                                headerText: 'Model',
                                showHeader: true,
                                pickerTitle: "Select Vehicle Model",
                                value: initModel.vehicleSection.vehicleModel.value,
                                mandatory: true,
                                valid: getBooleanValid(initModel.vehicleSection.vehicleModel.valid),
                                editable: getEditable(initModel.vehicleSection.vehicleModel.editable),
                                visible: true,
                                payload: null,
                                listItems: getInterestedVehicleModelsItems(interestedVehicles),
                                dependencies: [
                                    {
                                        key: 'vehicle_variant',
                                        trigger: 'update_list',
                                        getListItems: function (value) {
                                            return getInterestedVehicleVariantsItems(interestedVehicles, value);
                                        }
                                    },
                                    {
                                        key: 'vehicle_color',
                                        trigger: 'update_list',
                                        getListItems: function () {
                                            return [];
                                        }
                                    }
                                ]
                            },
                            {
                                index: 1,
                                key: 'vehicle_variant',
                                server_id: 'variant_id',
                                view: 'picker',
                                headerText: 'Variant',
                                showHeader: true,
                                pickerTitle: "Select Vehicle Variant",
                                value: initModel.vehicleSection.vehicleVariant.value,
                                mandatory: true,
                                valid: getBooleanValid(initModel.vehicleSection.vehicleVariant.valid),
                                editable: getEditable(initModel.vehicleSection.vehicleVariant.editable),
                                visible: true,
                                payload: null,
                                listItems: getInterestedVehicleVariantsItems(interestedVehicles, initModel.vehicleSection.vehicleModel.value),
                                dependencies: [
                                    {
                                        key: 'vehicle_color',
                                        trigger: 'update_list',
                                        getListItems: function (value) {
                                            return getInterestedVehicleColorsItems(interestedVehicles, value);
                                        }
                                    }
                                ]
                            },
                            {
                                index: 2,
                                key: 'vehicle_color',
                                server_id: 'color_id',
                                view: 'picker',
                                headerText: 'Color/Description',
                                showHeader: true,
                                pickerTitle: "Select",
                                value: initModel.vehicleSection.vehicleColor.value,
                                mandatory: true,
                                valid: getBooleanValid(initModel.vehicleSection.vehicleColor.valid),
                                editable: getEditable(initModel.vehicleSection.vehicleColor.editable),
                                visible: true,
                                payload: null,
                                listItems: getInterestedVehicleColorsItems(interestedVehicles, initModel.vehicleSection.vehicleVariant.value),
                            },
                            {
                                index: 3,
                                key: 'booking_amount',
                                view: 'edit_text',
                                headerText: 'Booking Amount',
                                showHeader: true,
                                hint: "Enter booking amount",
                                value: getStringValue(initModel.vehicleSection.bookingAmount.value),
                                mandatory: true,
                                valid: getBooleanValid(initModel.vehicleSection.bookingAmount.valid),
                                editable: getBooleanValid(initModel.vehicleSection.bookingAmount.editable),
                                visible: true,
                                payload: null,
                                keyboardType: 'numeric',
                                validation: initModel.vehicleSection.bookingAmount.validation
                            },
                            {
                                index: 4,
                                key: 'vin_no',
                                view: 'edit_text',
                                headerText: 'VIN Number',
                                showHeader: true,
                                hint: "Enter VIN Number",
                                value: getStringValue(initModel.vehicleSection.vinNumber.value),
                                mandatory: true,
                                valid: getBooleanValid(initModel.vehicleSection.vinNumber.valid),
                                editable: getEditable(initModel.vehicleSection.vinNumber.editable),
                                visible: getBooleanValid(initModel.vehicleSection.vinNumber.visible),
                                payload: null,
                            },
                            {
                                index: 5,
                                key: 'invoice_mob_no',
                                view: 'edit_text',
                                headerText: 'Invoice Mobile Number',
                                showHeader: true,
                                hint: "Enter Invoice Mobile Number",
                                value: getStringValue(initModel.vehicleSection.invoiceMobileNumber.value),
                                mandatory: true,
                                valid: getBooleanValid(initModel.vehicleSection.invoiceMobileNumber.valid),
                                editable: getEditable(initModel.vehicleSection.invoiceMobileNumber.editable),
                                visible: getBooleanValid(initModel.vehicleSection.invoiceMobileNumber.visible),
                                payload: null,
                                keyboardType: 'numeric',
                                validation: initModel.vehicleSection.invoiceMobileNumber.validation,
                                maxLength: 10,
                            },
                            {
                                index: 6,
                                key: 'invoice_date',
                                view: 'date_picker',
                                headerText: 'Invoice Date',
                                showHeader: true,
                                value: initModel.vehicleSection.invoiceDate.value,
                                mandatory: true,
                                valid: getBooleanValid(initModel.vehicleSection.invoiceDate.valid),
                                editable: getEditable(initModel.vehicleSection.invoiceDate.editable),
                                visible: getBooleanValid(initModel.vehicleSection.invoiceDate.visible),
                                payload: null,
                                maximumDate: new Date(),
                                minimumDate: Utils.getModDate(-2)
                            },
                            {
                                index: 7,
                                key: 'delivery_date',
                                view: 'date_picker',
                                headerText: 'Delivery Date',
                                showHeader: true,
                                value: initModel.vehicleSection.deliveryDate.value,
                                mandatory: true,
                                valid: getBooleanValid(initModel.vehicleSection.deliveryDate.valid),
                                editable: getEditable(initModel.vehicleSection.deliveryDate.editable),
                                visible: getBooleanValid(initModel.vehicleSection.deliveryDate.visible),
                                payload: null,
                                maximumDate: new Date(),
                                minimumDate: Utils.getModDate(-2)
                            },
                            {
                                index: 8,
                                key: 'delivery_number',
                                view: 'edit_text',
                                headerText: 'Delivery Challan',
                                showHeader: true,
                                hint: "Enter Delivery Challan",
                                value: getStringValue(initModel.vehicleSection.deliveryChallan.value),
                                mandatory: true,
                                valid: getBooleanValid(initModel.vehicleSection.deliveryChallan.valid),
                                editable: getEditable(initModel.vehicleSection.deliveryChallan.editable),
                                visible: getBooleanValid(initModel.vehicleSection.deliveryChallan.visible),
                                payload: null,
                            },
                            {
                                index: 9,
                                key: 'allocation',
                                view: 'picker',
                                pickerTitle: "Select",
                                showHeader: false,
                                value: initModel.vehicleSection.allocation.value,
                                mandatory: true,
                                valid: true,
                                editable: true,
                                visible: getBooleanValid(initModel.vehicleSection.allocation.visible),
                                payload: null,
                                listItems: initModel.vehicleSection.allocation.listItems,
                                actionValues: initModel.vehicleSection.allocation.actionValues
                            },
                            {
                                index: 10,
                                key: 'allocation_vin_number',
                                view: 'edit_text',
                                headerText: 'VIN Number',
                                showHeader: true,
                                hint: "Enter VIN Number",
                                value: getStringValue(initModel.vehicleSection.allocationVinNumber.value),
                                mandatory: true,
                                valid: getBooleanValid(initModel.vehicleSection.allocationVinNumber.valid),
                                editable: getEditable(initModel.vehicleSection.allocationVinNumber.editable),
                                visible: getBooleanValid(initModel.vehicleSection.allocationVinNumber.visible),
                                payload: null,
                            },

                        ],
                    },
                    {
                        index: 1, id: 'customer', title: 'Customer Details', expand: false,
                        getSectionValidation: function (section, allSections) {
                            return getSectionValidation(section, allSections);
                        },
                        mandatory: true,
                        items:
                            [
                                {
                                    index: 0,
                                    key: 'booking_name',
                                    server_id: 'booking_name',
                                    view: 'edit_text',
                                    headerText: 'Booking Name',
                                    showHeader: true,
                                    hint: "Enter booking name",
                                    value: getStringValue(initModel.customerDetailsSection.bookingName.value),
                                    mandatory: true,
                                    valid: getBooleanValid(initModel.customerDetailsSection.bookingName.valid),
                                    editable: getEditable(initModel.customerDetailsSection.bookingName.editable),
                                    visible: true,
                                    payload: null,
                                },
                                {
                                    index: 1,
                                    key: 'date_of_birth',
                                    server_id: 'dob',
                                    view: 'date_picker',
                                    headerText: 'Date of birth',
                                    showHeader: true,
                                    value: initModel.customerDetailsSection.dateOfBirth.value,
                                    mandatory: true,
                                    valid: getBooleanValid(initModel.customerDetailsSection.dateOfBirth.valid),
                                    editable: getEditable(initModel.customerDetailsSection.dateOfBirth.editable),
                                    visible: true,
                                    payload: null,
                                    maximumDate: new Date()
                                },
                                {
                                    index: 2,
                                    key: 'customer_type',
                                    server_id: 'customer_type_id',
                                    view: 'picker',
                                    headerText: 'Customer type',
                                    showHeader: true,
                                    pickerTitle: "Select",
                                    value: initModel.customerDetailsSection.customerType.value,
                                    mandatory: true,
                                    valid: getBooleanValid(initModel.customerDetailsSection.customerType.valid),
                                    editable: getEditable(initModel.customerDetailsSection.customerType.editable),
                                    visible: true,
                                    payload: null,
                                    listItems: CUSTOMER_TYPES,
                                    actionValues: CUSTOMER_TYPE_ACTION_VALUES
                                },
                                {
                                    index: 3,
                                    key: 'customer_pan',
                                    server_id: 'pan',
                                    view: 'edit_text',
                                    headerText: 'PAN',
                                    showHeader: true,
                                    hint: "Enter PAN number",
                                    value: getStringValue(initModel.customerDetailsSection.customerPAN.value),
                                    mandatory: true,
                                    valid: getBooleanValid(initModel.customerDetailsSection.customerPAN.valid),
                                    editable: getEditable(initModel.customerDetailsSection.customerPAN.editable),
                                    visible: true,
                                    payload: null,
                                    validation: initModel.customerDetailsSection.customerPAN.validation,
                                    autoCapitalize: 'characters',
                                    maxLength: 10
                                },
                                {
                                    index: 4,
                                    key: 'customer_aadhaar',
                                    server_id: 'aadhaar',
                                    view: 'edit_text',
                                    headerText: 'Aadhaar',
                                    showHeader: true,
                                    hint: "Enter Aadhaar Number",
                                    value: getStringValue(initModel.customerDetailsSection.customerAadhaar.value),
                                    mandatory: true,
                                    valid: getBooleanValid(initModel.customerDetailsSection.customerAadhaar.valid),
                                    editable: getEditable(initModel.customerDetailsSection.customerAadhaar.editable),
                                    visible: getBooleanValid(initModel.customerDetailsSection.customerAadhaar.visible),
                                    payload: null,
                                    validation: initModel.customerDetailsSection.customerAadhaar.validation,
                                    keyboardType: 'numeric',
                                    maxLength: 12
                                },
                                {
                                    index: 5,
                                    key: 'company_gst',
                                    server_id: 'gst',
                                    view: 'edit_text',
                                    headerText: 'GST',
                                    showHeader: true,
                                    hint: "Enter GST Number",
                                    value: getStringValue(initModel.customerDetailsSection.companyGST.value),
                                    mandatory: true,
                                    valid: getBooleanValid(initModel.customerDetailsSection.companyGST.valid),
                                    editable: getEditable(initModel.customerDetailsSection.companyGST.editable),
                                    visible: getBooleanValid(initModel.customerDetailsSection.companyGST.visible),
                                    payload: null,
                                    validation: initModel.customerDetailsSection.companyGST.validation,
                                    autoCapitalize: 'characters',
                                    maxLength: 15
                                },
                                {
                                    index: 6,
                                    key: 'customer_address',
                                    server_id: 'address',
                                    view: 'edit_text',
                                    headerText: 'Address',
                                    showHeader: true,
                                    hint: "Enter address",
                                    value: getStringValue(initModel.customerDetailsSection.customerAddress.value),
                                    mandatory: getBooleanValid(initModel.customerDetailsSection.customerAddress.mandatory),
                                    valid: getBooleanValid(initModel.customerDetailsSection.customerAddress.valid),
                                    editable: getEditable(initModel.customerDetailsSection.customerAddress.editable),
                                    visible: true,
                                    payload: null,
                                },
                                {
                                    index: 7,
                                    key: 'customer_pincode',
                                    server_id: 'pin_code',
                                    view: 'edit_text',
                                    headerText: 'Pincode',
                                    showHeader: true,
                                    hint: "Enter pincode",
                                    value: getStringValue(initModel.customerDetailsSection.customerPincode.value),
                                    mandatory: getBooleanValid(initModel.customerDetailsSection.customerPincode.mandatory),
                                    valid: getBooleanValid(initModel.customerDetailsSection.customerPincode.valid),
                                    editable: getEditable(initModel.customerDetailsSection.customerPincode.editable),
                                    visible: true,
                                    payload: null,
                                    keyboardType: 'numeric',
                                    maxLength: 6,
                                    validation: initModel.customerDetailsSection.customerPincode.validation,
                                },
                                {
                                    index: 8,
                                    key: 'customer_care_of',
                                    server_id: 'care_of_type',
                                    view: 'picker',
                                    pickerTitle: "Select",
                                    showHeader: false,
                                    value: initModel.customerDetailsSection.customerCareOfType.value,
                                    mandatory: getBooleanValid(initModel.customerDetailsSection.customerCareOfType.mandatory),
                                    valid: getBooleanValid(initModel.customerDetailsSection.customerCareOfType.valid),
                                    editable: getEditable(initModel.customerDetailsSection.customerCareOfType.editable),
                                    visible: getBooleanValid(initModel.customerDetailsSection.customerCareOfType.visible),
                                    payload: null,
                                    listItems: CARE_OF_TYPES
                                },
                                {
                                    index: 9,
                                    key: 'customer_care_of_value',
                                    server_id: 'care_of',
                                    view: 'edit_text',
                                    showHeader: false,
                                    hint: "Enter",
                                    value: getStringValue(initModel.customerDetailsSection.customerCareOfTypeValue.value),
                                    mandatory: getBooleanValid(initModel.customerDetailsSection.customerCareOfTypeValue.mandatory),
                                    valid: getBooleanValid(initModel.customerDetailsSection.customerCareOfTypeValue.valid),
                                    editable: getEditable(initModel.customerDetailsSection.customerCareOfTypeValue.editable),
                                    visible: initModel.customerDetailsSection.customerCareOfTypeValue.visible ? true : false,
                                    payload: null,
                                },
                            ]
                    },
                    {
                        index: 2, id: 'price_breakup', title: 'Price Breakup', expand: false,
                        mandatory: getBooleanValid(initModel.priceBreakupSection.mandatory),
                        getSectionValidation: function (section, allSections) {
                            return getSectionValidation(section, allSections);
                        },
                        getTotal: function () {
                            return getTotalValue(this.items);
                        },
                        items: [
                            {
                                index: 0,
                                key: 'ex_showroom_price',
                                server_id: 'ex_showroom',
                                view: 'edit_text',
                                headerText: 'Ex Showroom Price',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.priceBreakupSection.exShowRoomPrice.value),
                                keyboardType: 'numeric',
                                mandatory: getBooleanValid(initModel.priceBreakupSection.exShowRoomPrice.mandatory),
                                valid: getBooleanValid(initModel.priceBreakupSection.exShowRoomPrice.valid),
                                editable: getEditable(initModel.priceBreakupSection.exShowRoomPrice.editable),
                                visible: true,
                                payload: null,
                                validation: '[1-9]\d*'
                            },
                            {
                                index: 1,
                                key: 'insurance',
                                server_id: 'insurance',
                                view: 'edit_text',
                                headerText: 'Insurance',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.priceBreakupSection.insurance.value),
                                keyboardType: 'numeric',
                                mandatory: false,
                                valid: true,
                                editable: getEditable(initModel.priceBreakupSection.insurance.editable),
                                visible: true,
                                payload: null,
                                validation: '$'
                            },
                            {
                                index: 2,
                                key: 'registration',
                                server_id: 'registration',
                                view: 'edit_text',
                                headerText: 'Registration',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.priceBreakupSection.registration.value),
                                keyboardType: 'numeric',
                                mandatory: false,
                                valid: true,
                                editable: getEditable(initModel.priceBreakupSection.registration.editable),
                                visible: true,
                                payload: null,
                                validation: '$'
                            },
                            {
                                index: 3,
                                key: 'accessories',
                                server_id: 'accessories_sold',
                                view: 'edit_text',
                                headerText: 'Accessories',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.priceBreakupSection.accessories.value),
                                keyboardType: 'numeric',
                                mandatory: false,
                                valid: true,
                                editable: getEditable(initModel.priceBreakupSection.accessories.editable),
                                visible: true,
                                payload: null,
                                validation: '$'
                            },
                            {
                                index: 4,
                                key: 'extended_warranty',
                                server_id: 'ew',
                                view: 'edit_text',
                                headerText: 'Extended Warranty',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.priceBreakupSection.extendedWarranty.value),
                                keyboardType: 'numeric',
                                mandatory: false,
                                valid: true,
                                editable: getEditable(initModel.priceBreakupSection.extendedWarranty.editable),
                                visible: true,
                                payload: null,
                                validation: '$'
                            },
                            {
                                index: 5,
                                key: 'others',
                                server_id: 'others',
                                view: 'edit_text',
                                headerText: 'Others',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.priceBreakupSection.others.value),
                                keyboardType: 'numeric',
                                mandatory: false,
                                valid: true,
                                editable: getEditable(initModel.priceBreakupSection.others.editable),
                                visible: true,
                                payload: null,
                                validation: '$'
                            },
                        ]
                    },
                    {
                        index: 3, id: 'discount', title: 'Discount', expand: false,
                        mandatory: getBooleanValid(initModel.discountSection.mandatory),
                        getSectionValidation: function (section, allSections) {
                            return getSectionValidation(section, allSections);
                        },
                        getTotal: function () {
                            return getTotalValue(this.items, true);
                        },
                        items: [
                            {
                                index: 0,
                                key: 'corporate',
                                server_id: 'corporate',
                                view: 'edit_text',
                                headerText: 'Corporate',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.discountSection.corporate.value),
                                keyboardType: 'numeric',
                                mandatory: false,
                                valid: true,
                                editable: getEditable(initModel.discountSection.corporate.editable),
                                visible: true,
                                payload: null,
                                validation: '$'
                            },
                            {
                                index: 1,
                                key: 'exchange_loyalty_bonus',
                                server_id: 'exchange_bonus',
                                view: 'edit_text',
                                headerText: 'Exchange/Loyalty Bonus',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.discountSection.exchangeLoyaltyBonus.value),
                                keyboardType: 'numeric',
                                mandatory: false,
                                valid: true,
                                editable: getEditable(initModel.discountSection.exchangeLoyaltyBonus.editable),
                                visible: true,
                                payload: null,
                                validation: '$'
                            },
                            {
                                index: 2,
                                key: 'referral_bonus',
                                server_id: 'loyalty_bonus',
                                view: 'edit_text',
                                headerText: 'Referral Bonus',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.discountSection.referralBonus.value),
                                keyboardType: 'numeric',
                                mandatory: false,
                                valid: true,
                                editable: getEditable(initModel.discountSection.referralBonus.editable),
                                visible: true,
                                payload: null,
                                validation: '$'
                            },
                            {
                                index: 3,
                                key: 'oem_scheme',
                                server_id: 'oem_scheme',
                                view: 'edit_text',
                                headerText: 'OEM Scheme',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.discountSection.oemScheme.value),
                                keyboardType: 'numeric',
                                mandatory: false,
                                valid: true,
                                editable: getEditable(initModel.discountSection.oemScheme.editable),
                                visible: true,
                                payload: null,
                                validation: '$'
                            },
                            {
                                index: 4,
                                key: 'dealer',
                                server_id: 'dealer',
                                view: 'edit_text',
                                headerText: 'Dealer',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.discountSection.dealer.value),
                                keyboardType: 'numeric',
                                mandatory: false,
                                valid: true,
                                editable: getEditable(initModel.discountSection.dealer.editable),
                                visible: true,
                                payload: null,
                                validation: '$'
                            },
                            {
                                index: 5,
                                key: 'free_accessories',
                                server_id: 'free_accessories',
                                view: 'edit_text',
                                headerText: 'Free Accessories',
                                showHeader: true,
                                hint: '',
                                value: getStringValue(initModel.discountSection.freeAccessories.value),
                                keyboardType: 'numeric',
                                mandatory: false,
                                valid: true,
                                editable: getEditable(initModel.discountSection.freeAccessories.editable),
                                visible: true,
                                payload: null,
                                validation: '$'
                            },
                        ]
                    },
                    {
                        index: 4, id: 'exchange_finance', title: 'Exchange/Finance', expand: false,
                        mandatory: getBooleanValid(initModel.exchangeFinanceSection.mandatory),
                        getSectionValidation: function (section, allSections) {
                            return getSectionValidation(section, allSections);
                        },
                        value: getStringValue(initModel.exchangeFinanceSection.financeType.value),
                        items: [
                            {
                                index: 0,
                                key: 'finance_type',
                                server_id: 'finance_type',
                                view: 'radio',
                                headerText: 'Financing',
                                showHeader: true,
                                value: getStringValue(initModel.exchangeFinanceSection.financeType.value),
                                items: FINANCE_TYPES,
                                mandatory: getBooleanValid(initModel.exchangeFinanceSection.financeType.mandatory),
                                valid: getBooleanValid(initModel.exchangeFinanceSection.financeType.valid),
                                editable: getEditable(initModel.exchangeFinanceSection.financeType.editable),
                                visible: true,
                                payload: null,
                            },
                            {
                                index: 1,
                                key: 'exchange',
                                server_id: 'exchanged',
                                view: 'radio',
                                headerText: 'Exchange',
                                showHeader: true,
                                value: getStringValue(initModel.exchangeFinanceSection.exchange.value, true),
                                items: EXCHANGE_ITEMS,
                                mandatory: getBooleanValid(initModel.exchangeFinanceSection.exchange.mandatory),
                                valid: getBooleanValid(initModel.exchangeFinanceSection.exchange.valid),
                                editable: getEditable(initModel.exchangeFinanceSection.exchange.editable),
                                visible: getBooleanValid(initModel.exchangeFinanceSection.exchange.visible),
                                payload: null,
                            },
                        ]
                    }
                ]
        }
    },

}

module.exports = BookingModel;
