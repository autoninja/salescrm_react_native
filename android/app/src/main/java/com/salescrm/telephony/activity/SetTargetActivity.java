package com.salescrm.telephony.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.SetTargetAdapter;
import com.salescrm.telephony.db.GameLocationDB;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.team_dashboard_gm_db.Absolute;
import com.salescrm.telephony.db.team_dashboard_gm_db.Relational;
import com.salescrm.telephony.db.team_dashboard_gm_db.ResultEtvbr;
import com.salescrm.telephony.db.team_dashboard_gm_db.ResultSM;
import com.salescrm.telephony.db.team_dashboard_gm_db.TeamSM;
import com.salescrm.telephony.model.EtvbrGMPojo;
import com.salescrm.telephony.model.TargetSectionModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bannhi on 11/8/17.
 */

public class SetTargetActivity extends Fragment implements View.OnClickListener{

    RecyclerView recyclerView;
    private SetTargetAdapter targetsRecyclerAdapter;
    AlertDialog alertDialog;
    // private SwipeRefreshLayout swipeRefreshLayout;
    private int lastTLId = 0;
    ImageView setTargetButton;
    private String startDateString = "";
    private String endDateString = "";
    private int exchangeTotal, retailTotal, financeTotal = 0;
    private Preferences pref;
    TextView retail_total_tv, booking_total_tv, enquiry_total_tv;
    ArrayList<TextView> textViewList;
    private RelativeLayout rel_loading_frame;
    Realm realm;

    String selectedMonth = "current";
    private RealmList<ResultEtvbr> branchManagerDBRealmList = new RealmList<>();
    private RealmList<ResultSM> salesManagerDBRealmList = new RealmList<>();
    private RealmList<TeamSM> teamLeadsDBRealmList = new RealmList<>();
    private RealmList<Absolute> dseDBRealmList = new RealmList<>();
    private RealmList<Absolute> dseDBFullRealmList = new RealmList<>();
    private ArrayList<TargetSectionModel> targetSectionModelArrayList = new ArrayList<>();
    private RealmList<GameLocationDB> locationsList = new RealmList<>();
    ConnectionDetectorService connectionDetectorService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.set_target, container, false);
        recyclerView = (RecyclerView) rootview.findViewById(R.id.recyclerViewEtvbr);
        setTargetButton = (ImageView) rootview.findViewById(R.id.set_target_button);
        retail_total_tv = (TextView) rootview.findViewById(R.id.retail_no);
        booking_total_tv = (TextView) rootview.findViewById(R.id.booking_no);
        enquiry_total_tv = (TextView) rootview.findViewById(R.id.enquiries_no);
        rel_loading_frame = (RelativeLayout) rootview.findViewById(R.id.rel_loading_frame);
        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();
        branchManagerDBRealmList.clear();

        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TARGET);

        setTargetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPickMonthOption();
            }
        });
        return rootview;

    }


    @Override
    public void onResume() {
        super.onResume();
       // rel_loading_frame.setVisibility(View.VISIBLE);
        connectionDetectorService = new ConnectionDetectorService(getActivity());
        Calendar calendar = Calendar.getInstance();
        String date = new SimpleDateFormat("MMM yyyy").format(calendar.getTime());
        if (pref.getTargetMonth() == 0) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Targets - " + date);

        } else {
            calendar.add(Calendar.MONTH, 1);
            date = new SimpleDateFormat("MMM yyyy").format(calendar.getTime());
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Targets - " + date);
        }
        if (connectionDetectorService.isConnectingToInternet()) {
            getEtvbrGMInfoFromServer(pref.getTargetMonth(),false,null);
        } else {
            Toast.makeText(getContext(), "PLease enable internet", Toast.LENGTH_LONG);
            displayData();
        }

    }

    private void showPickMonthOption() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.set_month_target_dialog, null);
        dialogBuilder.setView(dialogView);
        TextView currentMonthTv = (TextView) dialogView.findViewById(R.id.current_month);
        TextView nextMonthTv = (TextView) dialogView.findViewById(R.id.next_month);
        Calendar calendar = Calendar.getInstance();
        currentMonthTv.setText("" + new SimpleDateFormat("MMM yyyy").format(calendar.getTime()));
        calendar.add(Calendar.MONTH, 1);
        nextMonthTv.setText("" + new SimpleDateFormat("MMM yyyy").format(calendar.getTime()));

        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
        nextMonthTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedMonth = "next";
                locationsList.clear();
                locationsList.addAll(realm.where(GameLocationDB.class).findAll());
                if(locationsList.size()>1){
                    alertDialog.dismiss();
                    showLocationDialog();
                }else if(locationsList!=null && locationsList.size()==1){

                    Intent i = new Intent(getActivity(), SaveTargetActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("month", selectedMonth);
                    bundle.putString("location",locationsList.get(0).getName());
                    bundle.putString("locationID",locationsList.get(0).getId()+"");
                    i.putExtras(bundle);
                    if (pref.getTargetMonth() == 0) {
                        //fetch next month data
                        alertDialog.dismiss();
                        getEtvbrGMInfoFromServer(1, true, i);
                    } else {
                        //not fetch anything
                        alertDialog.dismiss();
                        startActivity(i);
                    }
                }
                else {
                    Util.showToast(getContext(),"Target locked",Toast.LENGTH_LONG);
                }

            }
        });
        currentMonthTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedMonth = "current";
                locationsList.clear();
                locationsList.addAll(realm.where(GameLocationDB.class).findAll());
                if(locationsList!=null && locationsList.size()>1){
                    alertDialog.dismiss();
                    showLocationDialog();
                }else if(locationsList!=null && locationsList.size()==1) {
                    if (locationsList.get(0).isTargets_locked()) {
                        Util.showToast(getContext(), "Target locked for " + locationsList.get(0).getName(), Toast.LENGTH_LONG);
                    }
                    else {
                    Intent i = new Intent(getActivity(), SaveTargetActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("month", selectedMonth);
                    bundle.putString("location", locationsList.get(0).getName());
                    bundle.putString("locationID", locationsList.get(0).getId() + "");
                    i.putExtras(bundle);
                    if (pref.getTargetMonth() == 1) {
                        //fetch current month data
                        alertDialog.dismiss();
                        getEtvbrGMInfoFromServer(0, true, i);
                    } else {
                        //not fetch anything
                        alertDialog.dismiss();
                        startActivity(i);
                    }
                }
                }
                else {
                    Util.showToast(getContext(),"Target locked",Toast.LENGTH_LONG);
                }

            }
        });


    }
    private void showLocationDialog(){

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.target_sel_loc, null);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
        textViewList = new ArrayList<TextView>();
        LinearLayout mainLyt = (LinearLayout) dialogView.findViewById(R.id.main_ll);
        //using this locations for the time being
        for (int i = 0; i < locationsList.size(); i++) {

            TextView msg = new TextView(getActivity());
            msg.setBackgroundColor(Color.parseColor("#24FFFFFF"));
            msg.setText(locationsList.get(i).getName());
            msg.setTextSize(20);
            msg.setId(i);
            msg.setPadding(5,5,5,5);
            msg.setTextColor(ContextCompat.getColor(getActivity(), R.color.whitef));
            msg.setWidth((int) Util.convertDpToPixel(180, getContext()));
            msg.setHeight((int) Util.convertDpToPixel(49, getContext()));
            msg.setGravity(Gravity.CENTER);
            LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lparams.gravity = Gravity.CENTER_HORIZONTAL;
            msg.setLayoutParams(lparams);
            mainLyt.addView(msg);
            textViewList.add(msg);
            ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) msg.getLayoutParams();
            marginParams.topMargin = (int) Util.convertDpToPixel(18, getContext());
            msg.setOnClickListener(this);
        }


    }
    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
       int pos = textViewList.indexOf(v);
        if(pos>-1){
            Intent i = new Intent(getActivity(), SaveTargetActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("month", selectedMonth);
            bundle.putString("location",locationsList.get(pos).getName());
            bundle.putString("locationID",locationsList.get(pos).getId()+"");
            i.putExtras(bundle);
            if(selectedMonth.equalsIgnoreCase("next")){
                if(pref.getTargetMonth()==0){
                    //fetch next month data
                    alertDialog.dismiss();
                    getEtvbrGMInfoFromServer(1,true,i);
                }else{
                    //not fetch anything
                    alertDialog.dismiss();
                    startActivity(i);
                }
            }else{
                if(locationsList.get(pos).isTargets_locked()) {
                    Util.showToast(getContext(), "Target locked for " + locationsList.get(pos).getName(), Toast.LENGTH_LONG);

                }
                else {
                    if (pref.getTargetMonth() == 1) {
                        //fetch current month data
                        alertDialog.dismiss();
                        getEtvbrGMInfoFromServer(0, true, i);
                    } else {
                        //not fetch anything
                        alertDialog.dismiss();
                        startActivity(i);
                    }
                }
            }

        }

    }

    public void displayData() {
        rel_loading_frame.setVisibility(View.GONE);
        RealmResults<ResultEtvbr> branchManagerDBRealmResults = realm.where(ResultEtvbr.class)
                .findAllSorted("userId", Sort.ASCENDING);
        if (branchManagerDBRealmResults != null || branchManagerDBRealmResults.size() > 0) {
            for (int k = 0; k < branchManagerDBRealmResults.size(); k++) {
                salesManagerDBRealmList = branchManagerDBRealmResults.get(k).getSalesManager();
                for (int s = 0; s < salesManagerDBRealmList.size(); s++) {
                    teamLeadsDBRealmList = salesManagerDBRealmList.get(s).getTeams();
                    for (int t = 0; t < teamLeadsDBRealmList.size(); t++) {
                        // if(teamLeadsDBRealmList.get(t).getTeamId()!=null && !teamLeadsDBRealmList.get(t).getTeamName().equalsIgnoreCase("total") ){
                        TargetSectionModel temp = new TargetSectionModel();
                        temp.setSectionPostion(dseDBFullRealmList.size());
                        temp.setTeamLeadName(teamLeadsDBRealmList.get(t).getTeamName());
                        targetSectionModelArrayList.add(temp);
                        dseDBRealmList = teamLeadsDBRealmList.get(t).getAbs();
                        for (int d = 0; d < dseDBRealmList.size(); d++) {
                            dseDBFullRealmList.add(dseDBRealmList.get(d));
                            if (dseDBRealmList.get(d).getBookings_target() != null) {
                                exchangeTotal += dseDBRealmList.get(d).getExchange_target();
                            }
                            if (dseDBRealmList.get(d).getEnquiries_target() != null) {
                                financeTotal += dseDBRealmList.get(d).getFinance_target();
                            }
                            if (dseDBRealmList.get(d).getRetail_target() != null) {
                                retailTotal += dseDBRealmList.get(d).getRetail_target();
                            }
                            //   }

                        }


                    }
                }
            }

        }
        booking_total_tv.setText("" + exchangeTotal);
        enquiry_total_tv.setText("" + financeTotal);
        retail_total_tv.setText("" + retailTotal);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        targetsRecyclerAdapter = new SetTargetAdapter(getActivity(), dseDBFullRealmList, targetSectionModelArrayList);

        recyclerView.setAdapter(targetsRecyclerAdapter);
    }

    public void getEtvbrGMInfoFromServer(int targetMonth, final boolean dialog, final Intent i) {
        rel_loading_frame.setVisibility(View.VISIBLE);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        Date date = new Date();
        if (targetMonth == 0) {
            startDateString = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
            endDateString = new SimpleDateFormat("yyyy-MM-dd").format(date);
        } else {
            calendar.add(Calendar.MONTH, 1);
            startDateString = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
            calendar.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().getActualMaximum(calendar.DAY_OF_MONTH));
            endDateString = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        }
       ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getTargets(startDateString, endDateString, new Callback<EtvbrGMPojo>() {
            @Override
            public void success(final EtvbrGMPojo etvbrPojo, Response response) {
                //    setSwipeRefreshView(false);
                rel_loading_frame.setVisibility(View.GONE);
                etvbrPojo.setUserId(pref.getAppUserId());
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase(WSConstants.AUTHORIZATION)) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }

                if(etvbrPojo.getStatusCode().equals(WSConstants.TOKEN_EXPIRED)){
                    ApiUtil.InvalidUserLogout(getActivity(),0);
                }

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.delete(ResultEtvbr.class);
                        realm.delete(ResultSM.class);
                        realm.delete(Absolute.class);
                        realm.delete(Relational.class);
                        realm.delete(TeamSM.class);
                        dseDBFullRealmList.clear();
                        targetSectionModelArrayList.clear();
                        exchangeTotal = 0;
                        retailTotal = 0;
                        financeTotal = 0;
                        addEtvbrGmInfoToDb(realm, etvbrPojo.getResult());
                        if(dialog){
                            startActivity(i);
                        }else{
                            displayData();
                        }

                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                rel_loading_frame.setVisibility(View.GONE);
                //  setSwipeRefreshView(false);
                Util.showToast(getContext(), "Server not responding - " + error.getMessage(), Toast.LENGTH_SHORT);
            }
        });

     /*   Gson g = new Gson();
        final EtvbrGMPojo etvbrPojo = g.fromJson(WSConstants.jsonString, EtvbrGMPojo.class);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(ResultEtvbr.class);
                realm.delete(ResultSM.class);
                realm.delete(Absolute.class);
                realm.delete(Relational.class);
                realm.delete(TeamSM.class);
                dseDBFullRealmList.clear();
                targetSectionModelArrayList.clear();
                bookingTotal = 0;
                retailTotal = 0;
                enquiriesTotal = 0;
                addEtvbrGmInfoToDb(realm, etvbrPojo.getResult());
                if(dialog){
                    startActivity(i);
                }else{
                    displayData();
                }

            }
        });*/
    }

    private void addEtvbrGmInfoToDb(Realm realm, EtvbrGMPojo.Result etvbrGMPojo) {
        System.out.println("HelloAdele: " + new Gson().toJson(etvbrGMPojo).toString());
        ResultEtvbr resultEtvbr = new ResultEtvbr();
        resultEtvbr.setUserId(pref.getAppUserId());
        resultEtvbr.salesManager.clear();
        if (etvbrGMPojo.getSalesManager() != null) {
            for (int i = 0; i < etvbrGMPojo.getSalesManager().size(); i++) {
                ResultSM resultSM = new ResultSM();
                resultSM.setSmId(etvbrGMPojo.getSalesManager().get(i).getSmId());
                resultSM.setSmDpUrl(etvbrGMPojo.getSalesManager().get(i).getSmDpUrl());
                resultSM.setSmUsername(etvbrGMPojo.getSalesManager().get(i).getSmUsername());
                resultSM.teams.clear();
                for (int j = 0; j < etvbrGMPojo.getSalesManager().get(i).getTeams().size(); j++) {
                    TeamSM teamSM = new TeamSM();
                    teamSM.setTeamId(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getTeamId());
                    teamSM.setTlDpUrl(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getTlDpUrl());
                    teamSM.setTeamName(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getTeamName());
                    teamSM.abs.clear();
                    teamSM.rel.clear();
                    for (int k = 0; k < etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().size(); k++) {
                        Absolute absolute = new Absolute();
                        absolute.setUserId(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getUserId());
                        //absolute.setBookings(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getBookings());
                        absolute.setDpUrl(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getDpUrl());
                        //absolute.setEnquiries(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getEnquiries());
                        //absolute.setRetail(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getRetail());
                        //absolute.setTdrives(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getTdrives());
                        absolute.setUserName(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getUserName());
                        //absolute.setVisits(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getVisits());
                        absolute.setEnquiries_target(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getEnquiries_target());
                        absolute.setTdrives_target(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getTdrives_target());
                        absolute.setVisits_target(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getVisits_target());
                        absolute.setBookings_target(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getBookings_target());
                        absolute.setRetail_target(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getRetail_target());
                        absolute.setExchange_target(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getExchange_target());
                        absolute.setFinance_target(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getFinance_target());

                        //absolute.setSalesManager(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getSalesManager());
                        absolute.setLocation(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getLocation());
                        absolute.setLocationId(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getAbs().get(k).getLocationId());
                        teamSM.abs.add(absolute);
                    }
                    /*for (int l = 0; l < etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getRel().size(); l++) {
                        Relational relational = new Relational();
                        relational.setUserId(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getRel().get(l).getUserId());
                        relational.setBookings(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getRel().get(l).getBookings());
                        relational.setDpUrl(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getRel().get(l).getDpUrl());
                        relational.setEnquiries(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getRel().get(l).getEnquiries());
                        relational.setRetail(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getRel().get(l).getRetail());
                        relational.setTdrives(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getRel().get(l).getTdrives());
                        relational.setUserName(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getRel().get(l).getUserName());
                        relational.setVisits(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getRel().get(l).getVisits());
                        relational.setLocation(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getRel().get(l).getLocation());
                        //relational.setSalesManager(etvbrGMPojo.getSalesManager().get(i).getTeams().get(j).getRel().get(l).getSalesManager());
                        teamSM.rel.add(relational);
                    }*/
                    resultSM.teams.add(teamSM);
                }
                resultEtvbr.salesManager.add(resultSM);
            }
        }
        realm.copyToRealm(resultEtvbr);
    }


}
