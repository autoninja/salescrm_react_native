package com.salescrm.telephony.db.team_dashboard_gm_db;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by prateek on 13/7/17.
 */

public class Relational extends RealmObject{

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("dp_url")
    @Expose
    private String dpUrl;
    @SerializedName("enquiries")
    @Expose
    private Integer enquiries;
    @SerializedName("tdrives")
    @Expose
    private Integer tdrives;
    @SerializedName("visits")
    @Expose
    private Integer visits;
    @SerializedName("bookings")
    @Expose
    private Integer bookings;
    @SerializedName("retail")
    @Expose
    private Integer retail;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("Sales Manager")
    @Expose
    private String salesManager;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDpUrl() {
        return dpUrl;
    }

    public void setDpUrl(String dpUrl) {
        this.dpUrl = dpUrl;
    }

    public Integer getEnquiries() {
        return enquiries;
    }

    public void setEnquiries(Integer enquiries) {
        this.enquiries = enquiries;
    }

    public Integer getTdrives() {
        return tdrives;
    }

    public void setTdrives(Integer tdrives) {
        this.tdrives = tdrives;
    }

    public Integer getVisits() {
        return visits;
    }

    public void setVisits(Integer visits) {
        this.visits = visits;
    }

    public Integer getBookings() {
        return bookings;
    }

    public void setBookings(Integer bookings) {
        this.bookings = bookings;
    }

    public Integer getRetail() {
        return retail;
    }

    public void setRetail(Integer retail) {
        this.retail = retail;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSalesManager() {
        return salesManager;
    }

    public void setSalesManager(String salesManager) {
        this.salesManager = salesManager;
    }

}