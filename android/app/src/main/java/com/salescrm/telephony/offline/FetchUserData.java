package com.salescrm.telephony.offline;

import android.content.Context;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.UserCarBrand;
import com.salescrm.telephony.db.UserCarBrands;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.interfaces.OfflineSupportListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.AppUserResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 23/9/17.
 * Call this to get the user data
 */
public class FetchUserData implements Callback<AppUserResponse> {
    public OfflineSupportListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;

    public FetchUserData(OfflineSupportListener listener, Context context) {
        this.listener =listener;
        this.context = context;
      //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
    }

    public void call() {
        ConnectionDetectorService cd = new ConnectionDetectorService(SalesCRMApplication.GetAppContext());
        if (cd.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).getUserDetails(this);
        } else {
            System.out.println("No Internet Connection");
            listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.USER_DATA);

        }
    }


    @Override
    public void success(final AppUserResponse data, Response response) {
        Util.updateHeaders(response.getHeaders());

        if (!data.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
            System.out.println("Success:0" + data.getMessage());
            //showAlert(validateOtpResponse.getMessage());
            listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.USER_DATA);

        } else {
            if (data.getResult() != null) {
                System.out.println("Success:1" + data.getMessage());
                realm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        insertData(realm,data);
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        listener.onUserDataFetched(data);
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.USER_DATA);
                    }
                });


            } else {
                // relLoader.setVisibility(View.GONE);
                System.out.println("Success:2" + data.getMessage());
                listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.USER_DATA);
                //showAlert(validateOtpResponse.getMessage());
            }
        }
    }

    @Override
    public void failure(RetrofitError error) {
        System.out.println(error);
        listener.onOfflineSupportApiError(null,WSConstants.OfflineAPIRequest.USER_DATA);

    }

    private void insertData(Realm realm,AppUserResponse otpResponse) {

        realm.where(UserCarBrands.class).findAll().deleteAllFromRealm();
        realm.where(UserCarBrand.class).findAll().deleteAllFromRealm();
        realm.where(UserRolesDB.class).findAll().deleteAllFromRealm();
        realm.where(UserLocationsDB.class).findAll().deleteAllFromRealm();
        realm.where(UserDetails.class).findAll().deleteAllFromRealm();

        pref.setTeamId(otpResponse.getResult().getTeam_id());

        if(otpResponse.getResult().getRoles_user().size()>0) {
            pref.setRole(otpResponse.getResult().getRoles_user().get(0).getName());
            pref.setRoleId(otpResponse.getResult().getRoles_user().get(0).getId());
        }
        RealmList<UserCarBrands> userCarBrandsList = new RealmList<>();
        if (otpResponse.getResult().getBrands() != null ) {

            UserCarBrands userCarBrands = new UserCarBrands();
            userCarBrands.setBrand_id(otpResponse.getResult().getBrands().getBrand_id());

            RealmList<UserCarBrand> userCarBrandList = new RealmList<>();
            UserCarBrand userCarBrand = new UserCarBrand();
            userCarBrand.setId(otpResponse.getResult().getBrands().getBrand_id());
            userCarBrand.setName(otpResponse.getResult().getBrands().getBrand_name());

            userCarBrandList.add(userCarBrand);

            userCarBrands.setCarBrand(userCarBrandList);

            userCarBrandsList.add(userCarBrands);
        }

        RealmList<UserRolesDB> userRoleList = new RealmList<>();
        for (int i = 0; i < (otpResponse.getResult().getRoles_user() == null ? 0 : otpResponse.getResult().getRoles_user().size()); i++) {
            UserRolesDB userRolesDB = new UserRolesDB();
            userRolesDB.setName(otpResponse.getResult().getRoles_user().get(i).getName());
            userRolesDB.setId(otpResponse.getResult().getRoles_user().get(i).getId());
            userRoleList.add(userRolesDB);
        }
        RealmList<UserLocationsDB> userLocations = new RealmList<>();
        for (int i = 0; i < (otpResponse.getResult().getLocations() == null ? 0 : otpResponse.getResult().getLocations().size()); i++) {
            UserLocationsDB userLocationsDB = new UserLocationsDB();
            userLocationsDB.setLocation_id(otpResponse.getResult().getLocations().get(i).getLocation_id());
            userLocationsDB.setName(otpResponse.getResult().getLocations().get(i).getName());
            userLocations.add(userLocationsDB);
        }
        UserDetails userDetails = new UserDetails();
        userDetails.setUserId(Integer.parseInt(otpResponse.getResult().getData().getId()));
        userDetails.setName(otpResponse.getResult().getData().getName());
        userDetails.setUserName(otpResponse.getResult().getData().getUser_name());
        userDetails.setMobile(otpResponse.getResult().getData().getMobile());
        userDetails.setEmail(otpResponse.getResult().getData().getEmail());
        // userDetails.setModule(otpResponse.getResult().getData().get(i).getModule());
        userDetails.setDealer(otpResponse.getResult().getData().getDealer());
        pref.setDealerName(otpResponse.getResult().getData().getDealer());
        pref.setUserMobile(otpResponse.getResult().getData().getMobile());
        pref.setDseName(otpResponse.getResult().getData().getName());
        pref.setUserProfilePicUrl(otpResponse.getResult().getData().getDp_url());
        pref.setEmail(otpResponse.getResult().getData().getEmail());
        pref.setName(otpResponse.getResult().getData().getName());
        pref.setUserName(otpResponse.getResult().getData().getUser_name());


        // userDetails.setCrmType(Integer.parseInt(otpResponse.getResult().getData().get(i).getModule()));
        userDetails.setImgProfileUrl(otpResponse.getResult().getData().getDp_url());
        userDetails.setUserRoles(userRoleList);
        userDetails.setUserLocations(userLocations);
        userDetails.setUserCarBrands(userCarBrandsList);
        realm.copyToRealmOrUpdate(userDetails);


    }

}
