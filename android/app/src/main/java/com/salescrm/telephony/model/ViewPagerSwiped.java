package com.salescrm.telephony.model;

/**
 * Created by bharath on 23/8/16.
 */
public class ViewPagerSwiped {
    private int oldPosition;
    private int newPosition;
    private String enqSourceId;
    private String primaryCarId;
    private String locationId;

    public ViewPagerSwiped() {
    }

    public ViewPagerSwiped(int oldPosition, int newPosition) {
        this.oldPosition = oldPosition;
        this.newPosition = newPosition;
    }

    public ViewPagerSwiped(int oldPosition, int newPosition, String enqSourceId, String primaryCarId, String locationId) {
        this.oldPosition = oldPosition;
        this.newPosition = newPosition;
        this.enqSourceId = enqSourceId;
        this.primaryCarId = primaryCarId;
        this.locationId = locationId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getEnqSourceId() {
        return enqSourceId;
    }

    public void setEnqSourceId(String enqSourceId) {
        this.enqSourceId = enqSourceId;
    }

    public String getPrimaryCarId() {
        return primaryCarId;
    }

    public void setPrimaryCarId(String primaryCarId) {
        this.primaryCarId = primaryCarId;
    }

    public int getOldPosition() {
        return oldPosition;
    }

    public void setOldPosition(int oldPosition) {
        this.oldPosition = oldPosition;
    }

    public int getNewPosition() {
        return newPosition;
    }

    public void setNewPosition(int newPosition) {
        this.newPosition = newPosition;
    }
}
