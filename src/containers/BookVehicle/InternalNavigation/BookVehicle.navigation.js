import React from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import BookVehicleMain from '../Main'
import BookingPopups from '../BookingPopups/BookingPopups'
import UpdateInvoice from '../UpdateInvoice/UpdateInvoice'
import TotalBookingSelector from '../TotalBookingSelector/TotalBookingSelector'
import Submission from '../Submission/Submission'
import CancelBooking from '../CancelBooking/CancelBooking'
import AutoDialog from '../../../components/uielements/AutoDialog'
export const getBookVehicleNavigator = (routing, callbacks, info, userRoles, interestedVehicles, form_object) => {
    return (
        createAppContainer(
            createStackNavigator(
                {
                    BookVehicleMain: {
                        screen: props => (
                            <BookVehicleMain
                                {...props}
                                interestedVehicles={interestedVehicles}
                                userRoles={userRoles}
                                info={info}
                                callbacks={callbacks}
                                form_object={form_object}
                            />
                        ),
                        navigationOptions: ({ navigation }) => ({
                            header: null
                        })
                    },
                    TotalBookingSelector: {
                        screen: props => (
                            <TotalBookingSelector
                                {...props}
                                info={info}
                                callbacks={callbacks}
                            />
                        ),
                        navigationOptions: ({ navigation }) => ({
                            header: null
                        })
                    },
                    Submission: {
                        screen: props => (
                            <Submission
                                {...props}
                                info={info}
                                callbacks={callbacks}
                            />
                        ),
                        navigationOptions: ({ navigation }) => ({
                            header: null
                        })
                    },
                    BookingPopups: {
                        screen: props => (
                            <BookingPopups
                                popupData={routing.popupData}
                                {...props}
                                info={info}
                                callbacks={callbacks}
                            />
                        ),
                        navigationOptions: ({ navigation }) => ({
                            header: null
                        })
                    },
                    UpdateInvoice: {
                        screen: props => (
                            <UpdateInvoice
                                {...props}
                                info={info}
                                callbacks={callbacks}
                            />
                        ),
                        navigationOptions: ({ navigation }) => ({
                            header: null
                        })
                    },
                    CancelBooking: {
                        screen: props => (
                            <CancelBooking
                                {...props}
                                info={info}
                                callbacks={callbacks}
                            />
                        ),
                        navigationOptions: ({ navigation }) => ({
                            header: null
                        })
                    },
                    AutoDialog: {
                        screen: AutoDialog,
                        navigationOptions: ({ navigation }) => ({ header: null })
                    }
                },
                {
                    cardStyle: {
                        backgroundColor: "rgba(0,0,0,0.5)",
                        opacity: 1
                    },
                    mode: "modal",
                    initialRouteName: routing.initialRouteName
                },

            )
        )
    )
}