package com.salescrm.telephony.response.gamification;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by bannhi on 27/12/17.
 */

public class ManagerLeaderBoardResponse implements Serializable {

    private ManagerLeaderBoardResponse teamLeaderBoardResponse;

        private String statusCode;

        private String message;

        private Result result;

        private Error error;

    public ManagerLeaderBoardResponse(ManagerLeaderBoardResponse teamLeaderBoardResponse) {
        this.teamLeaderBoardResponse = teamLeaderBoardResponse;
    }

    public ManagerLeaderBoardResponse getTeamLeaderBoardResponse() {
        return teamLeaderBoardResponse;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result implements Serializable
    {
        private ArrayList<Managers> managers;

        public ArrayList<Managers> getManagers() {
            return managers;
        }

        public void setManagers(ArrayList<Managers> managers) {
            this.managers = managers;
        }
    }


    public class Managers implements Serializable
    {
        private String manager_name;

        private String user_id;

        private String manager_id;

        private String manager_average_points;

        private String manager_photo_url;

        private String team_id;

        private String spotlight;

        public String getManager_name() {
            return manager_name;
        }

        public void setManager_name(String manager_name) {
            this.manager_name = manager_name;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getManager_id() {
            return manager_id;
        }

        public void setManager_id(String manager_id) {
            this.manager_id = manager_id;
        }

        public String getManager_average_points() {
            return manager_average_points;
        }

        public void setManager_average_points(String manager_average_points) {
            this.manager_average_points = manager_average_points;
        }

        public String getManager_photo_url() {
            return manager_photo_url;
        }

        public void setManager_photo_url(String manager_photo_url) {
            this.manager_photo_url = manager_photo_url;
        }

        public String getTeam_id() {
            return team_id;
        }

        public void setTeam_id(String team_id) {
            this.team_id = team_id;
        }

        public String getSpotlight() {
            return spotlight;
        }

        public void setSpotlight(String spotlight) {
            this.spotlight = spotlight;
        }
    }




    public class Error implements Serializable
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }

}
