package com.salescrm.telephony.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.salescrm.telephony.fragments.gamification.LeaderBoardMainFragment;
import com.salescrm.telephony.fragments.ScoreboardDseFragment;

/**
 * Created by prateek on 24/8/17.
 */

public class ScoreboardDsePagerAdapter extends FragmentPagerAdapter{
    private static int NUM_ITEMS = 2;

    public ScoreboardDsePagerAdapter(FragmentManager fm, int i) {
        super(fm);
        NUM_ITEMS = i;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return ScoreboardDseFragment.newInstance(0, "Scoreboard");
            case 1:
                return LeaderBoardMainFragment.newInstance(1, "LeaderBoard");
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
