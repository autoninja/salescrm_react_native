package com.salescrm.telephony.model.booking;

/**
 * Created by bharath on 19/3/18.
 */

public class ApiInputPriceBreakup {
    private String ex_showroom;
    private String insurance;
    private String registration;
    private String accessories_sold;
    private String ew;
    private String others;
    private String booking_amount;

    public ApiInputPriceBreakup(String ex_showroom, String insurance, String registration, String accessories_sold, String ew, String others, String booking_amount) {
        this.ex_showroom = ex_showroom;
        this.insurance = insurance;
        this.registration = registration;
        this.accessories_sold = accessories_sold;
        this.ew = ew;
        this.others = others;
        this.booking_amount = booking_amount;
    }

    public String getEx_showroom() {
        return ex_showroom;
    }

    public void setEx_showroom(String ex_showroom) {
        this.ex_showroom = ex_showroom;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getAccessories_sold() {
        return accessories_sold;
    }

    public void setAccessories_sold(String accessories_sold) {
        this.accessories_sold = accessories_sold;
    }

    public String getEw() {
        return ew;
    }

    public void setEw(String ew) {
        this.ew = ew;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getBooking_amount() {
        return booking_amount;
    }

    public void setBooking_amount(String booking_amount) {
        this.booking_amount = booking_amount;
    }
}