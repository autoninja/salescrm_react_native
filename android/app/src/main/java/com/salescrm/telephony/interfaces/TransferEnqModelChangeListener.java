package com.salescrm.telephony.interfaces;

/**
 * Created by bharath on 21/8/17.
 */

public interface TransferEnqModelChangeListener {
    void onDataChanged();
    void onChangeSelectedItem();
}
