package com.salescrm.telephony.db.teams;

import io.realm.RealmObject;

/**
 * Created by bharath on 9/12/16.
 */

public class TeamUserDetailsDB extends RealmObject {
    private int id;
    private String name;
    private String mobile_number;
    private int role_id;

    public TeamUserDetailsDB() {
    }

    public TeamUserDetailsDB(int id, String name, String mobile_number) {
        this.id = id;
        this.name = name;
        this.mobile_number = mobile_number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }
}
