package com.salescrm.telephony.telephonyModule;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.os.Build;
import android.os.PowerManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.salescrm.telephony.db.CallStatDB;
import com.salescrm.telephony.dbOperation.CallStatDbOperation;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.telephonyModule.service.ServiceBackgroundRun;
import com.salescrm.telephony.utils.Util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/*
//Only outgoing call recording on android p and above for called from app
//On Android 5 and below :  Outgoing only supports for call made from app.
* */

public class PhonecallReceiver extends BroadcastReceiver {

    //The receiver will be recreated whenever android feels like it.  We need a static variable to remember data between instantiations

    private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    private static Date callStartTime;
    private static boolean isIncoming;
    private static String savedNumber;  //because the passed incoming is only valid in ringing
    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
    private Preferences pref = null;
    //public static boolean isCallGoingOn = false;
    private static boolean prevStateRinging;




    @Override
    public void onReceive(Context context, Intent intent) {

        pref = Preferences.getInstance();
        pref.load(context);
        if(pref.isTelephonyNeeded()) {
            pref.setIsCallOnGoing(true);
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
            wl.acquire();
            System.out.println("STATE:CHANGE:"+intent.getExtras().getString(TelephonyManager.EXTRA_STATE));
            System.out.println("STATE:CHANGE:Number:"+intent.getExtras().getString("android.intent.extra.PHONE_NUMBER"));
            System.out.println("STATE:CHANGE:Number:"+intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER));

            //Telephony Removed
//            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                Intent service = new Intent(context, ServiceBackgroundRun.class);
//                context.startForegroundService(service);
//            }
//            else {
//                Intent service = new Intent(context, ServiceBackgroundRun.class);
//                context.startService(service);
//            }

            //Recording part for below android 5
            if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
                outgoingCallTracker(intent, context);
                return;
            }
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
             /*   HashMap<String, Object> hashMap = new HashMap<String, Object>();
                hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_TYPE, CleverTapConstants.EVENT_TELEPHONY_TYPE_RECORDING_RESTRICTED);
                hashMap.put(CleverTapConstants.EVENT_TELEPHONY_KEY_FAILURE_REASON, CleverTapConstants.EVENT_TELEPHONY_FAILURE_REASON_ANDROID_PIE);
                CleverTapPush.pushEvent(CleverTapConstants.EVENT_TELEPHONY, hashMap);*/

             outgoingCallTracker(intent, context);
             return;
            }
            else if(Util.isNull(pref.getHotlineNumber())) {
                if(pref.isClientILom() || pref.isClientILBank()) {
                    outgoingCallTracker(intent, context);
                    return;
                }
            }

            //We listen to two intents.  The new outgoing call only tells us of an outgoing call.  We use it to get the number.
            if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
                Log.d("PhoneCallReceiver", "New Outgoing call");
                savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
            } else {
                try {
                    String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                    if(intent.getStringExtra(TelephonyManager.EXTRA_STATE) ==null) {
                        number = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
                    }

                    onCallStateChanged(context,
                            getStateInteger(intent.getExtras().getString(TelephonyManager.EXTRA_STATE)),
                            number);
                } catch (NullPointerException e) {

                }

            }

            wl.release();
        }else{
            //Toast.makeText(context,"Call triggered.",Toast.LENGTH_LONG).show();
        }
    }

    private void outgoingCallTracker(Intent intent, Context context) {
        if(intent!=null
                && intent.getExtras()!=null
                && intent.getExtras().getString(TelephonyManager.EXTRA_STATE)!=null) {
            String extraState = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            int state = getStateInteger(extraState);
            //Multiple triggering in lollipop and below
            if(pref.getmCallState() == state && Build.VERSION.SDK_INT<=Build.VERSION_CODES.LOLLIPOP_MR1) {
                return;
            }
            pref.setmCallState(state);
            if(extraState.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                CallStatDB callStatDB = CallStatDbOperation.get(pref.getCallStatCreatedAt());
                if(callStatDB!=null) {
                    callStartTime = new Date();
                    onOutgoingCallStarted(context,
                            callStatDB.getMobileNumber(),
                            formatter.format(callStartTime),
                            true
                            );
                }
            }
            else if (extraState.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_IDLE)){
                if(prevStateRinging) {
                    System.out.println("Call Check Incoming call has ended");
                    prevStateRinging = false;
                }
                else {
                    System.out.println("Call Check Outgoing call has ended");
                    CallStatDbOperation.updateOnEndCall(pref.getCallStatCreatedAt());
                    CallStatDB callStatDB = CallStatDbOperation.get(pref.getCallStatCreatedAt());
                    if(callStatDB!=null) {
                        callStartTime = new Date();
                        onOutgoingCallEnded(context,
                                callStatDB.getMobileNumber(),
                                formatter.format(callStartTime),
                                formatter.format(new Date()),
                                callStatDB.getScheduledActivityId());
                    }
                    pref.setCallStatCreatedAt(0);
                }
            }
            if(extraState.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_RINGING)) {
                prevStateRinging = true;
            }
        }
    }

    private int getStateInteger(String extraState) {
        int state = 0;
        if (extraState.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
            state = TelephonyManager.CALL_STATE_IDLE;
        } else if (extraState.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            state = TelephonyManager.CALL_STATE_OFFHOOK;
        } else if (extraState.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            state = TelephonyManager.CALL_STATE_RINGING;
        }
        return state;
    }

    //Derived classes should override these to respond to specific events of interest
    protected void onIncomingCallStarted(Context ctx, String number, String start) {
    }

    protected void onOutgoingCallStarted(Context ctx, String number, String start, boolean recordWhenHotlineNull) {
    }

    protected void onIncomingCallEnded(Context ctx, String number, String start, String end) {
    }

    protected void onOutgoingCallEnded(Context ctx, String number, String start, String end, String scheduledActivityId) {
    }

    protected void onMissedCall(Context ctx, String number, String start) {
    }

    protected void startRecordingIncomingCalls(Context ctx, String number, String start) {
    }


    //Incoming call-  goes from IDLE to RINGING when it rings, to OFFHOOK when it's answered, to IDLE when its hung up
    //Outgoing call-  goes from IDLE to OFFHOOK when it dials out, to IDLE when hung up
    public void onCallStateChanged(Context context, int state, String number) {
        if (context != null) {
            if (pref.getmCallState() == state) {
                //No change, debounce extras
                Log.d("PhoneCallReceiver", "no change");
                return;
            }
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    Log.d("PhoneCallReceiver", "Ringing");
                    isIncoming = true;
                    callStartTime = new Date();
                    savedNumber = number;
                    onIncomingCallStarted(context, number, formatter.format(callStartTime));
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.d("PhoneCallReceiver", "Offhook");
                    //Transition of ringing->offhook are pickups of incoming calls.  Nothing done on them
                    if (lastState == TelephonyManager.CALL_STATE_RINGING) {
                        Log.d("PhoneCallReceiver", "Ringing -> Offhook");
                        isIncoming = true;
                        callStartTime = new Date();
                        savedNumber = number;
                        startRecordingIncomingCalls(context, savedNumber, formatter.format(callStartTime));
                    } else {
                        Log.e("PhoneCallReceiver", "Idle -> Offhook");
                        isIncoming = false;
                        callStartTime = new Date();
                        savedNumber = number;
                        if (savedNumber != null && formatter != null && callStartTime != null) {
                            System.out.println("onOutgoingStarted");
                            onOutgoingCallStarted(context, savedNumber, formatter.format(callStartTime), false);
                        }

                    }
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    //Went to idle-  this is the end of a call.  What type depends on previous state(s)
                    if (lastState == TelephonyManager.CALL_STATE_RINGING) {
                        //Ring but no pickup-  a miss
                        Log.e("PhoneCallReceiver", "Miss");
                        onMissedCall(context, savedNumber, formatter.format(callStartTime));
                    } else if (isIncoming) {
                        Log.e("PhoneCallReceiver", "Incoming end");
                        if (savedNumber != null && formatter.format(new Date()) != null && callStartTime != null) {
                            onIncomingCallEnded(context, savedNumber, formatter.format(callStartTime), formatter.format(new Date()));
                        }
                    } else {
                        Log.e("PhoneCallReceiver", "Outgoing end");
                        if (savedNumber != null && formatter.format(new Date()) != null && callStartTime != null) {
                            String scheduledActivityId = null;
                            CallStatDbOperation.updateOnEndCall(pref.getCallStatCreatedAt());
                            CallStatDB callStatDB = CallStatDbOperation.get(pref.getCallStatCreatedAt());
                            if(callStatDB!=null) {
                                scheduledActivityId =callStatDB.getScheduledActivityId();
                            }
                            pref.setCallStatCreatedAt(0);
                            onOutgoingCallEnded(context,
                                    savedNumber,
                                    formatter.format(callStartTime),
                                    formatter.format(new Date()),
                                    scheduledActivityId);
                        }
                    }
                    break;
            }
            pref.setmCallState(state);
            lastState = state;
        }
    }

}