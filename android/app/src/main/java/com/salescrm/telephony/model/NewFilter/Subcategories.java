package com.salescrm.telephony.model.NewFilter;

/**
 * Created by prateek on 4/8/18.
 */

public class Subcategories {

    private String id;
    private boolean set_checked;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSet_checked() {
        return set_checked;
    }

    public void setSet_checked(boolean set_checked) {
        this.set_checked = set_checked;
    }
}
