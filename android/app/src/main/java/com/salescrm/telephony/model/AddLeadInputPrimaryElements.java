package com.salescrm.telephony.model;

/**
 * Created by bharath on 9/9/16.
 */
public class AddLeadInputPrimaryElements {

    private String enqSourceId,primaryCarId,locationId;
    private AddLeadCarInputData carInputData;

    public AddLeadInputPrimaryElements(String enqSourceId, String primaryCarId, String locationId, AddLeadCarInputData addLeadCarInputData) {
        this.enqSourceId = enqSourceId;
        this.primaryCarId = primaryCarId;
        this.locationId = locationId;
        this.carInputData = addLeadCarInputData;
    }

    public AddLeadCarInputData getCarInputData() {
        return carInputData;
    }

    public void setCarInputData(AddLeadCarInputData carInputData) {
        this.carInputData = carInputData;
    }

    public String getEnqSourceId() {
        return enqSourceId;
    }

    public void setEnqSourceId(String enqSourceId) {
        this.enqSourceId = enqSourceId;
    }

    public String getPrimaryCarId() {
        return primaryCarId;
    }

    public void setPrimaryCarId(String primaryCarId) {
        this.primaryCarId = primaryCarId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
}
