import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
export default class Header extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        if(this.props.isFromLiveEF) {
            if(this.props.showPercentage) {
                return(
                    <View style={styles.container}>
                        <View style={styles.itemEmpty}/>
                        <Text style={styles.item}>R%</Text>
                        <Text style={styles.item}>Ex%</Text>
                        <Text style={styles.item}>F/I%</Text>
                        <Text style={styles.item}>F/O%</Text>
                        <Text style={[styles.item, {color:'#00bfa5'}]}>PB%</Text>
                        <Text style={[styles.item, {color:'#00bfa5'}]}>LE%</Text>
                    </View>
                )
            }
            else {
                return(
                    <View style={styles.container}>
                        <View style={styles.itemEmpty}/>
                        <Text style={styles.item}>R</Text>
                        <Text style={styles.item}>Ex</Text>
                        <Text style={styles.item}>F/I</Text>
                        <Text style={styles.item}>F/O</Text>
                        <Text style={[styles.item, {color:'#00bfa5'}]}>PB</Text>
                        <Text style={[styles.item, {color:'#00bfa5'}]}>LE</Text>
                    </View>
                )
            }
        }
        else {
            if(this.props.showPercentage) {
                return(
                    <View style={styles.container}>
                        <View style={styles.itemEmpty}/>
                        <Text style={styles.item}>E%</Text>
                        <Text style={styles.item}>T%</Text>
                        <Text style={styles.item}>V%</Text>
                        <Text style={styles.item}>B%</Text>
                        <Text style={styles.item}>R%</Text>
                        <Text style={styles.item}>L%</Text>
                    </View>
                )
            }
            else {
                return(
                    <View style={styles.container}>
                        <View style={styles.itemEmpty}/>
                        <Text style={styles.item}>E</Text>
                        <Text style={styles.item}>T</Text>
                        <Text style={styles.item}>V</Text>
                        <Text style={styles.item}>B</Text>
                        <Text style={styles.item}>R</Text>
                        <Text style={styles.item}>L</Text>
                    </View>
                )
            }
        }
        
    }
}

let styles = StyleSheet.create({
    container: {
        margin:2,
        borderRadius:2, 
        borderColor:'#CFD9E6', 
        borderWidth:1, 
        padding:2,
        flexDirection:'row'
    },
    itemEmpty: {
        flex:1.5,
    },
    item: {
        flex:1,
        fontSize:13,
        color:'#2D4F8B',
        alignSelf:'center',
        textAlign:'center'
    }
})