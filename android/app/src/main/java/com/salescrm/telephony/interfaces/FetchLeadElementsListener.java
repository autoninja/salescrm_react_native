package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.response.AddLeadElementsResponse;

import retrofit.RetrofitError;

/**
 * Created by bharath on 14/10/16.
 */

public interface FetchLeadElementsListener {
    void onLeadElementsFetched(AddLeadElementsResponse addLeadElementsResponse);

    void onFetchLeadElementsError(RetrofitError error);
}
