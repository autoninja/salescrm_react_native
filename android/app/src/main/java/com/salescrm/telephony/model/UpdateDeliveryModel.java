package com.salescrm.telephony.model;

import com.salescrm.telephony.model.booking.ApiInputBookingDetails;

/**
 * Created by bannhi on 21/6/17.
 */

public class UpdateDeliveryModel {

    int booking_id;
    String booking_number;
    int lead_car_id;
    int enquiry_id;
    int location_id;
    long lead_id;
    int lead_car_stage_id;
    String lead_last_updated;
    UpdateDeliveryModel.DeliveryDetails delivery_details;
    private ApiInputBookingDetails booking_details;

    public String getLead_last_updated() {
        return lead_last_updated;
    }

    public void setLead_last_updated(String lead_last_updated) {
        this.lead_last_updated = lead_last_updated;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(int booking_id) {
        this.booking_id = booking_id;
    }

    public String getBooking_number() {
        return booking_number;
    }

    public void setBooking_number(String booking_number) {
        this.booking_number = booking_number;
    }

    public int getLead_car_id() {
        return lead_car_id;
    }

    public void setLead_car_id(int lead_car_id) {
        this.lead_car_id = lead_car_id;
    }

    public int getEnquiry_id() {
        return enquiry_id;
    }

    public void setEnquiry_id(int enquiry_id) {
        this.enquiry_id = enquiry_id;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public long getLead_id() {
        return lead_id;
    }

    public void setLead_id(long lead_id) {
        this.lead_id = lead_id;
    }

    public int getLead_car_stage_id() {
        return lead_car_stage_id;
    }

    public void setLead_car_stage_id(int lead_car_stage_id) {
        this.lead_car_stage_id = lead_car_stage_id;
    }

    public UpdateDeliveryModel.DeliveryDetails getDelivery_details() {
        return delivery_details;
    }

    public void setDelivery_details(UpdateDeliveryModel.DeliveryDetails delivery_details) {
        this.delivery_details = delivery_details;
    }

    public class DeliveryDetails{
        String delivery_date;
        String delivery_number;
        String invoice_number;
        String invoice_id;
       // String invoice_date;
       // String invoice_amount;
        String invoice_name;
      //  String invoice_address;
        String invoice_mob_no;
        String vin_no;

        public String getDelivery_number() {
            return delivery_number;
        }

        public void setDelivery_number(String delivery_number) {
            this.delivery_number = delivery_number;
        }

        public String getInvoice_number() {
            return invoice_number;
        }

        public void setInvoice_number(String invoice_number) {
            this.invoice_number = invoice_number;
        }

        public String getInvoice_id() {
            return invoice_id;
        }

        public void setInvoice_id(String invoice_id) {
            this.invoice_id = invoice_id;
        }

        public String getDelivery_date() {
            return delivery_date;
        }

        public void setDelivery_date(String delivery_date) {
            this.delivery_date = delivery_date;
        }
        /*  public String getInvoice_date() {
            return invoice_date;
        }

        public void setInvoice_date(String invoice_date) {
            this.invoice_date = invoice_date;
        }*/

       /* public String getInvoice_amount() {
            return invoice_amount;
        }

        public void setInvoice_amount(String invoice_amount) {
            this.invoice_amount = invoice_amount;
        }
*/
        public String getInvoice_name() {
            return invoice_name;
        }

        public void setInvoice_name(String invoice_name) {
            this.invoice_name = invoice_name;
        }

       /* public String getInvoice_address() {
            return invoice_address;
        }

        public void setInvoice_address(String invoice_address) {
            this.invoice_address = invoice_address;
        }*/

        public String getInvoice_mob_no() {
            return invoice_mob_no;
        }

        public void setInvoice_mob_no(String invoice_mob_no) {
            this.invoice_mob_no = invoice_mob_no;
        }

        public String getVin_no() {
            return vin_no;
        }

        public void setVin_no(String vin_no) {
            this.vin_no = vin_no;
        }
    }

    public ApiInputBookingDetails getBooking_details() {
        return booking_details;
    }

    public void setBooking_details(ApiInputBookingDetails booking_details) {
        this.booking_details = booking_details;
    }

}
