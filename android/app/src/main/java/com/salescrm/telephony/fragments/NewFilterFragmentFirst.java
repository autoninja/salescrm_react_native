package com.salescrm.telephony.fragments;

import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.NewFilterAdapter;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.interfaces.NewFilterCallFragmentListener;
import com.salescrm.telephony.interfaces.NewFilterNotifyDataSetChanged;
import com.salescrm.telephony.model.NewFilter.Filter;
import com.salescrm.telephony.utils.WSConstants;
import com.squareup.otto.Subscribe;

import java.util.List;

/**
 * Created by prateek on 31/7/18.
 */

public class NewFilterFragmentFirst extends Fragment {
    private RecyclerView recyclerView;
    private NewFilterAdapter newFilterAdapter;
    private Drawable drawable;
    private NewFilterCallFragmentListener newFilterCallFragmentListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rView = inflater.inflate(R.layout.new_filter_fragment_first, container, false);
        recyclerView = (RecyclerView) rView.findViewById(R.id.recycler_view_filter);
        newFilterCallFragmentListener = (NewFilterCallFragmentListener) getActivity();
        drawable = VectorDrawableCompat.create(getActivity().getResources(), R.drawable.ic_cancel_black, null);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        //System.out.println("HmaaraBjaaj: "+ new Gson().toJson(WSConstants.filterSelection).toString());
        callServer(WSConstants.filters);
        return rView;
    }
    private void callServer(List<Filter> newFilters) {
        newFilterAdapter = new NewFilterAdapter(getActivity(), newFilters);
        recyclerView.setAdapter(newFilterAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        newFilterCallFragmentListener.setOptions("Filter by", drawable);
        newFilterCallFragmentListener.setApply("Apply", 0);

    }

    @Subscribe
    public void newFilterDataSetChanged(NewFilterNotifyDataSetChanged newFilterNotifyDataSetChanged){
        if(newFilterNotifyDataSetChanged.isDatasetchanged()){
            newFilterAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        SalesCRMApplication.getBus().unregister(this);
    }
}
