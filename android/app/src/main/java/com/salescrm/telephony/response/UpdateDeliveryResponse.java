package com.salescrm.telephony.response;

/**
 * Created by bannhi on 19/6/17.
 */

public class UpdateDeliveryResponse {
    private String message;

    private String statusCode;

    private Result result;

    private Error error;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", statusCode = "+statusCode+", result = "+result+", error = "+error+"]";
    }

    public class Error
    {
        private String details;

        private String type;

        public String getDetails ()
        {
            return details;
        }

        public void setDetails (String details)
        {
            this.details = details;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [details = "+details+", type = "+type+"]";
        }
    }

    public class Result
    {
        private Integer scheduled_activity_id;
        private Integer activity_id;
        private Integer action_id;
        private String lead_last_updated;

        private String delivery_id;

        private String next_stage_id;

        public String getLead_last_updated() {
            return lead_last_updated;
        }

        public void setLead_last_updated(String lead_last_updated) {
            this.lead_last_updated = lead_last_updated;
        }

        public String getDelivery_id()
        {
            return delivery_id;
        }

        public void setDelivery_id(String delivery_id)
        {
            this.delivery_id = delivery_id;
        }

        public String getNext_stage_id ()
        {
            return next_stage_id;
        }

        public void setNext_stage_id (String next_stage_id)
        {
            this.next_stage_id = next_stage_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [delivery_id = "+ delivery_id +", next_stage_id = "+next_stage_id+"]";
        }

        public Integer getScheduled_activity_id() {
            return scheduled_activity_id;
        }

        public void setScheduled_activity_id(Integer scheduled_activity_id) {
            this.scheduled_activity_id = scheduled_activity_id;
        }

        public Integer getActivity_id() {
            return activity_id;
        }

        public void setActivity_id(Integer activity_id) {
            this.activity_id = activity_id;
        }

        public Integer getAction_id() {
            return action_id;
        }

        public void setAction_id(Integer action_id) {
            this.action_id = action_id;
        }
    }
}
