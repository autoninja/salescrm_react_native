
 class GameLocationsModel{
  constructor(id, name, gamification, targets_locked, start_date){
    this.id = id;
    this.name = name;
    this.gamification = gamification;
    this.targets_locked = targets_locked;
    this.start_date = start_date;
  }
}

module.exports = {GameLocationsModel};
