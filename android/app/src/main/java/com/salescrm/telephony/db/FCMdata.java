package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by prateek on 18/11/16.
 */

public class FCMdata extends RealmObject {

    private int fcmSync;
    private String fcmTokenOld;
    private String fcmTokenNew;

    public int getFcmSync() {
        return fcmSync;
    }

    public void setFcmSync(int fcmSync) {
        this.fcmSync = fcmSync;
    }

    public String getFcmTokenOld() {
        return fcmTokenOld;
    }

    public void setFcmTokenOld(String fcmTokenOld) {
        this.fcmTokenOld = fcmTokenOld;
    }

    public String getFcmTokenNew() {
        return fcmTokenNew;
    }

    public void setFcmTokenNew(String fcmTokenNew) {
        this.fcmTokenNew = fcmTokenNew;
    }
}
