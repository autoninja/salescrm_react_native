package com.salescrm.telephony.db.car;

import com.salescrm.telephony.db.ExchangeStatusdb;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by akshata on 14/10/16.
 */

public class ExchangeCarDetails extends RealmObject {

    public static final String EXCHANGECARDETAILS = "Exchangecardetails";

    private  long leadId;
    @PrimaryKey
    private String lead_exchange_car_id;
    private String exchangecarname;
    private String model_id;
    private String price_quoted;
    private String market_price;
    private String expected_price;
    private String purchase_date;
    private String reg_no;
    private String kms_run;
    private String owner_type;
    private String make_year;

    public RealmList<ExchangeStatusdb> exchangestatus = new RealmList<>();

    /**new column
     *
     */
    private String car_stage_id;
    private String evaluator_name;
    private String evaluation_date_time;
    private String evaluation_remarks;

    private String scheduled_activity_id;
    private String activity_id;
    private String remarks;
    private String user_name;
    private String scheduled_at;


    public String getEvaluation_date_time() {
        return evaluation_date_time;
    }

    public void setEvaluation_date_time(String evaluation_date_time) {
        this.evaluation_date_time = evaluation_date_time;
    }

    public String getEvaluation_remarks() {
        return evaluation_remarks;
    }

    public void setEvaluation_remarks(String evaluation_remarks) {
        this.evaluation_remarks = evaluation_remarks;
    }


    public String getEvaluator_name() {
        return evaluator_name;
    }

    public void setEvaluator_name(String evaluator_name) {
        this.evaluator_name = evaluator_name;
    }

    public String getCar_stage_id() {
        return car_stage_id;
    }

    public void setCar_stage_id(String car_stage_id) {
        this.car_stage_id = car_stage_id;
    }

    public RealmList<ExchangeStatusdb> getExchangestatus() {
        return exchangestatus;
    }

    public void setExchangestatus(RealmList<ExchangeStatusdb> exchangestatus) {
        this.exchangestatus = exchangestatus;
    }

    public String getReg_no() {
        return reg_no;
    }

    public void setReg_no(String reg_no) {
        this.reg_no = reg_no;
    }

    public String getKms_run() {
        return kms_run;
    }

    public void setKms_run(String kms_run) {
        this.kms_run = kms_run;
    }

    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public String getLead_exchange_car_id() {
        return lead_exchange_car_id;
    }

    public void setLead_exchange_car_id(String lead_exchange_car_id) {
        this.lead_exchange_car_id = lead_exchange_car_id;
    }

    public String getExchangecarname() {
        return exchangecarname;
    }

    public void setExchangecarname(String exchangecarname) {
        this.exchangecarname = exchangecarname;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public String getPrice_quoted() {
        return price_quoted;
    }

    public void setPrice_quoted(String price_quoted) {
        this.price_quoted = price_quoted;
    }

    public String getMarket_price() {
        return market_price;
    }

    public void setMarket_price(String market_price) {
        this.market_price = market_price;
    }

    public String getExpected_price() {
        return expected_price;
    }

    public void setExpected_price(String expected_price) {
        this.expected_price = expected_price;
    }

    public String getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(String purchase_date) {
        this.purchase_date = purchase_date;
    }

    public String getOwner_type() {
        return owner_type;
    }

    public void setOwner_type(String owner_type) {
        this.owner_type = owner_type;
    }

    public String getMake_year() {
        return make_year;
    }

    public void setMake_year(String make_year) {
        this.make_year = make_year;
    }

    public String getScheduled_activity_id() {
        return scheduled_activity_id;
    }

    public void setScheduled_activity_id(String scheduled_activity_id) {
        this.scheduled_activity_id = scheduled_activity_id;
    }

    public String getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(String activity_id) {
        this.activity_id = activity_id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getScheduled_at() {
        return scheduled_at;
    }

    public void setScheduled_at(String scheduled_at) {
        this.scheduled_at = scheduled_at;
    }
}
