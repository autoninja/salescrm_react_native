package com.salescrm.telephony.response.gamification;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bannhi on 22/2/18.
 */

public class DSEPointsResponseModel {
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    private DSEPointsResponseModel dsePointsResponseModel;

    public DSEPointsResponseModel(DSEPointsResponseModel dsePointsResponseModel){
        this.dsePointsResponseModel = dsePointsResponseModel;
    }
    public DSEPointsResponseModel getDsePointsResponseModel() {
        return dsePointsResponseModel;
    }

    public void setDsePointsResponseModel(DSEPointsResponseModel dsePointsResponseModel) {
        this.dsePointsResponseModel = dsePointsResponseModel;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private String total;

        private String views;

        private ArrayList<Category_points> category_points;

        private String bonus;

        public String getViews() {
            return views;
        }

        public void setViews(String views) {
            this.views = views;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public ArrayList<Category_points> getCategory_points() {
            return category_points;
        }

        public void setCategory_points(ArrayList<Category_points> category_points) {
            this.category_points = category_points;
        }

        public String getBonus() {
            return bonus;
        }

        public void setBonus(String bonus) {
            this.bonus = bonus;
        }

        @Override
        public String toString() {
            return "ClassPojo [total = " + total + ", category_points = " + category_points + ", bonus = " + bonus + "]";
        }
    }

    public class Category_points {

        private String id;

        private String name;

        private List<Points> points;

        private String sum;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Points> getPoints() {
            return points;
        }

        public void setPoints(List<Points> points) {
            this.points = points;
        }

        public String getSum() {
            return sum;
        }

        public void setSum(String sum) {
            this.sum = sum;
        }

        public class Points {
            private String total;
            private String count;
            private String value;

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getCount() {
                return count;
            }

            public void setCount(String count) {
                this.count = count;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }

    public class Error {
        private String details;

        private String type;

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "ClassPojo [details = " + details + ", type = " + type + "]";
        }
    }
}
