package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.db.UserLocationsDB;

/**
 * Created by bharath on 4/12/17.
 */

public interface UserLocationPickerCallBack {

    void onPickLocation(UserLocationsDB UserLocationsDB);
}
