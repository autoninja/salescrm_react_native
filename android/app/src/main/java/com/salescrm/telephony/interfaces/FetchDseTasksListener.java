package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.response.ActionPlanResponse;

import java.util.List;

import retrofit.RetrofitError;

/**
 * Created by bharath on 9/6/17.
 */

public interface FetchDseTasksListener {
    void onFetchDseTaskSuccess(List<ActionPlanResponse.Result.Data> allActionPlanData);

    void onFetchDseTaskError(RetrofitError error, int actionPlan);
}
