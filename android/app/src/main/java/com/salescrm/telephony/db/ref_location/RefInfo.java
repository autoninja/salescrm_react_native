package com.salescrm.telephony.db.ref_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by prateek on 8/11/17.
 */

public class RefInfo extends RealmObject {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("dp_url")
    @Expose
    private String dp_url;
    @SerializedName("abs")
    @Expose
    private RefAbsoluteValue abs;
    @SerializedName("rel")
    @Expose
    private RefRelationalValue rel;
    @SerializedName("targets")
    @Expose
    private RefTargetValue targets;
    @SerializedName("user_role_id")
    @Expose
    private String user_role_id;

    public String getUserRoleId() {
        return user_role_id;
    }

    public void setUserRoleId(String userRoleId) {
        this.user_role_id = userRoleId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDpUrl() {
        return dp_url;
    }

    public void setDpUrl(String dpUrl) {
        this.dp_url = dpUrl;
    }

    public RefAbsoluteValue getAbs() {
        return abs;
    }

    public void setAbs(RefAbsoluteValue abs) {
        this.abs = abs;
    }

    public RefRelationalValue getRel() {
        return rel;
    }

    public void setRel(RefRelationalValue rel) {
        this.rel = rel;
    }

    public RefTargetValue getTargets() {
        return targets;
    }

    public void setTargets(RefTargetValue targets) {
        this.targets = targets;
    }
}
