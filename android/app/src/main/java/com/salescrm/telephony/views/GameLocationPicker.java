package com.salescrm.telephony.views;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.GameLocationDB;
import com.salescrm.telephony.interfaces.GameLocationPickerCallBack;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

/**
 * Created by bharath on 02/07/18.
 */

public class GameLocationPicker {

    public void show(Context context, final GameLocationPickerCallBack callBack) {

        Realm realm = Realm.getDefaultInstance();

        RealmResults<GameLocationDB> realmResult = realm.where(GameLocationDB.class).equalTo("gamification", true).findAll();

        if (realmResult.size() == 1) {
            callBack.onPickGameLocation(null);
            return;
        }

        final Dialog dialog = new Dialog(context, R.style.AppTheme_Black);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
            //  dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }

        dialog.setContentView(R.layout.user_location_picker_layout);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.user_location_picker_title);
        RecyclerView recyclerView =
                (RecyclerView) dialog.findViewById(R.id.user_location_picker_recycler_view);


        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(new GameLocationPickerAdapter(context, realmResult, true, new GameLocationPickerViewCallBack() {
            @Override
            public void onClick(GameLocationDB gameLocationDB) {
                callBack.onPickGameLocation(gameLocationDB);
                dialog.dismiss();
            }
        }));

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                callBack.onPickGameLocation(null);
            }
        });
        recyclerView.setHasFixedSize(true);
        dialog.setCanceledOnTouchOutside(false);

        dialog.show();

    }

    private class GameLocationPickerAdapter extends RealmRecyclerViewAdapter<GameLocationDB, GameLocationPickerAdapter.GameLocationPickerViewHolder> {

        private GameLocationPickerViewCallBack callBack;

        GameLocationPickerAdapter(@NonNull Context context,
                                  @Nullable OrderedRealmCollection<GameLocationDB> data,
                                  boolean autoUpdate,
                                  GameLocationPickerViewCallBack callBack) {
            super(context, data, autoUpdate);
            this.callBack = callBack;
        }

        @Override
        public GameLocationPickerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemLayoutView = null;
            itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_location_picker_card, parent, false);
            return new GameLocationPickerViewHolder(itemLayoutView);
        }

        @Override
        public void onBindViewHolder(GameLocationPickerViewHolder holder, final int position) {
            if (getData() != null && getData().get(position) != null) {
                holder.title.setText(getData().get(position).getName());
                holder.frameLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callBack.onClick(getData().get(position));
                    }
                });
            }

        }

        class GameLocationPickerViewHolder extends RecyclerView.ViewHolder {
            private TextView title;
            private FrameLayout frameLayout;

            GameLocationPickerViewHolder(View itemView) {
                super(itemView);
                title = (TextView) itemView.findViewById(R.id.user_location_card_title);
                frameLayout = (FrameLayout) itemView.findViewById(R.id.user_location_card_frame);
            }
        }
    }

    private interface GameLocationPickerViewCallBack {
        void onClick(GameLocationDB gameLocationDB);
    }

}
