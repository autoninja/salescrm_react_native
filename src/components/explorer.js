
import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image} from 'react-native';
import { createStackNavigator } from 'react-navigation';
const img = "https://tineye.com/images/widgets/mona.jpg";


export default class desaExplorers extends React.Component {

	constructor(props)
		{
		    super(props);

		    this.state = {
		    	//isLoading: true,
		    	//isFetching:false,
		    	explorersArray: explorers
			}
		}

	/*componentDidMount(){
     	 this.makeRemoteRequest();
 	 }

 	makeRemoteRequest = () => {
    	this.setState({ isLoading: true, isFetching: true });
    	new RestClient().getConsultantData({})*/

	render () {
	    return (
	    	<View style= {{flex : 1, backgroundColor: '#2e5e86'}}>
	    	<FlatList
		    	ItemSeparatorComponent = {this.FlatListItemSeparator}
		    	data={this.state.explorersArray}
		    	renderItem= {({item, index}) =>(
		    		<TouchableOpacity onPress={ () => this.props.onClick('ConsultantDetails')}>
			    		<View style={{flex : 10, height: 50,flexDirection: 'row', alignItems : 'center', justifyContent: 'center', backgroundColor: '#09273b'}}>
				    		 <View style={{flex : 1, alignItems : 'center', justifyContent: 'center'}}>
				    		 	<Text style={{color: '#fff'}}> {item.rank}</Text>
				    		 </View>
				    		 <Image
		                          style={{width: 36, height: 36, borderRadius: 36/2, alignItems: 'center'}}
		                          source={{uri: img}}
		                      />
		                      <View style={{flex : 3, alignItems : 'flex-start', justifyContent: 'center', paddingLeft: 15}}>
				    		 	<Text style={{color: '#fff'}}> {item.name} </Text>
				    		 </View>

				    		 <View style={{flex : 4, alignItems : 'center', justifyContent: 'center'}}>

				    		 </View>

				    		 <Image
							        style={{ width: 24, height: 24, marginRight: 15}}
		                          	source={require('../images/blue_arrow.png')}
							        />

			    		</View>
		    		</TouchableOpacity>
		    		)}
		    	keyExtractor= {(item, index) => index.toString() }
		    	/>
		    	</View>
		    	);
	  }
	}

	const explorers = [
    { "rank":1, "name":"Abhi", "avatar": "", "trophy":"" },
    { "rank":2, "name":"Bobby", "avatar": "", "trophy":"" },
    { "rank":3, "name":"Chetan", "avatar": "", "trophy":"" },
    { "rank":4, "name":"David", "avatar": "", "trophy":"" },
    { "rank":5, "name":"Ehsan", "avatar": "", "trophy":"" }];
