package com.salescrm.telephony.response;

/**
 * Created by akshata on 9/12/16.
 */

public class AddCarsNewResponse {

    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ClassPojo [statusCode = " + statusCode + ", message = " + message + ", result = " + result + ", error = " + error + "]";
    }

    public class Result {
        private String color_id;

        private String model_id;

        private String lead_car_id;

        private String name;

        private String variant_id;

        private String is_primary;

        private String variant_code;

        public String getColor_id() {
            return color_id;
        }

        public void setColor_id(String color_id) {
            this.color_id = color_id;
        }

        public String getModel_id() {
            return model_id;
        }

        public void setModel_id(String model_id) {
            this.model_id = model_id;
        }

        public String getLead_car_id() {
            return lead_car_id;
        }

        public void setLead_car_id(String lead_car_id) {
            this.lead_car_id = lead_car_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getVariant_id() {
            return variant_id;
        }

        public void setVariant_id(String variant_id) {
            this.variant_id = variant_id;
        }

        public String getIs_primary() {
            return is_primary;
        }

        public void setIs_primary(String is_primary) {
            this.is_primary = is_primary;
        }

        public String getVariant_code() {
            return variant_code;
        }

        public void setVariant_code(String variant_code) {
            this.variant_code = variant_code;
        }

        @Override
        public String toString() {
            return "ClassPojo [color_id = " + color_id + ", model_id = " + model_id + ", lead_car_id = " + lead_car_id + ", name = " + name + ", variant_id = " + variant_id + ", is_primary = " + is_primary + ", variant_code = " + variant_code + "]";
        }
    }


    public class Error {
        private String details;

        private String type;

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "ClassPojo [details = " + details + ", type = " + type + "]";
        }
    }

}
