package com.salescrm.telephony;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.react.CleverTapPackage;
import com.facebook.react.ReactApplication;
//import com.reactnativecommunity.geolocation.GeolocationPackage;
import io.sentry.RNSentryPackage;
import com.smixx.fabric.FabricPackage;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.horcrux.svg.SvgPackage;
import com.microsoft.appcenter.reactnative.analytics.AppCenterReactNativeAnalyticsPackage;
import com.microsoft.appcenter.reactnative.appcenter.AppCenterReactNativePackage;
import com.microsoft.codepush.react.CodePush;
import com.microsoft.codepush.react.ReactInstanceHolder;
import com.oblador.vectoricons.VectorIconsPackage;
import com.salescrm.telephony.application.EventBus;
import com.salescrm.telephony.reactNative.ReactNativeToAndroidPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.zmxv.RNSound.RNSoundPackage;

import java.util.Arrays;
import java.util.List;

import io.realm.Realm;

// Application File does nothing || Created for react-native link

/**
 * Created by Bharath Kumar 25/07/2019
 */
public class MainApplication extends android.app.Application implements ReactApplication {
    protected final ReactNativeHost reactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        public List<ReactPackage> getPackages() {
            return Arrays.asList(
                    new ReactNativeToAndroidPackage(),
                    new SvgPackage(),
                    new RNSoundPackage(),
                    new VectorIconsPackage(),
                    new CleverTapPackage(),
                    new RNGestureHandlerPackage(),
                    new AppCenterReactNativeAnalyticsPackage(MainApplication.this, getResources().getString(R.string.appCenterCrashes_whenToSendCrashes)),
                    new AppCenterReactNativePackage(MainApplication.this),
                    new CodePush(BuildConfig.CODEPUSH_KEY, MainApplication.this, BuildConfig.DEBUG)

            );
        }
        @Override
        protected String getJSMainModuleName() {
            return "index.android";
        }

        @Override
        protected String getJSBundleFile() {
            return CodePush.getJSBundleFile();
        }

        @Override
        protected String getBundleAssetName() {
            return "index.android.bundle";
        }
    };
    private ReactInstanceHolder instanceHolder = new ReactInstanceHolder() {
        @Override
        public ReactInstanceManager getReactInstanceManager() {
            return reactNativeHost.getReactInstanceManager();
        }
    };

    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public ReactNativeHost getReactNativeHost() {
        return reactNativeHost;
    }


    @Override
    public void onTerminate() {
        Realm.getDefaultInstance().close();
        super.onTerminate();
    }
}