package com.salescrm.telephony.db.car;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bharath on 22/3/18.
 */

public class BookingPriceBreakupDB extends RealmObject {
    @PrimaryKey
    private String bookingId;
    private String exShowRoomPrice;
    private String insurance;
    private String registration;
    private String accessories;
    private String extendedWarranty;
    private String others;

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getExShowRoomPrice() {
        return exShowRoomPrice;
    }

    public void setExShowRoomPrice(String exShowRoomPrice) {
        this.exShowRoomPrice = exShowRoomPrice;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getAccessories() {
        return accessories;
    }

    public void setAccessories(String accessories) {
        this.accessories = accessories;
    }

    public String getExtendedWarranty() {
        return extendedWarranty;
    }

    public void setExtendedWarranty(String extendedWarranty) {
        this.extendedWarranty = extendedWarranty;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }
}
