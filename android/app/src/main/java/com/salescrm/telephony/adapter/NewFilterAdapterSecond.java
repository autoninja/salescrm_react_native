package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.fragments.NewFilterFragmentSecond;
import com.salescrm.telephony.model.NewFilter.Filter;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;

/**
 * Created by prateek on 31/7/18.
 */

public class NewFilterAdapterSecond  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private int parentPosition;
    private ArrayList<CheckBox> listCheckBoxes = new ArrayList<>();
    private CheckBoxSettings checkBoxSettings = new CheckBoxSettings();
    private ArrayList<TextView> tvOptionsList = new ArrayList<>();
    public NewFilterAdapterSecond(Activity context, int filterPosition) {
        this.context = context;
        this.parentPosition = filterPosition;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_filter_adapter_view_holder_second, parent, false);
        return new NewFilterAdapterSecond.NewFilterViewHolderSecond(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final NewFilterAdapterSecond.NewFilterViewHolderSecond newFilterViewHolderSecond
                = (NewFilterAdapterSecond.NewFilterViewHolderSecond) holder;
        if(WSConstants.filters.get(parentPosition).getValues().get(position).getHasChildren()){
            newFilterViewHolderSecond.llOption.setVisibility(View.VISIBLE);
        final Drawable drawableImgDown = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_arrowright, null);

        final Drawable drawableImgUp = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_arrowup, null);

        tvOptionsList.add(newFilterViewHolderSecond.tvOption);
        newFilterViewHolderSecond.tvOption.setText(WSConstants.filters.get(parentPosition).getValues().get(position).getName());
        newFilterViewHolderSecond.tvOption.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableImgDown, null);
            if(changeColorForSelectedOption(false, position)){
                newFilterViewHolderSecond.tvOption.setTextColor(Color.parseColor("#2196f3"));
                newFilterViewHolderSecond.tvOption.setTypeface(null, Typeface.BOLD);
            }else{
                newFilterViewHolderSecond.tvOption.setTextColor(Color.BLACK);
                newFilterViewHolderSecond.tvOption.setTypeface(null, Typeface.NORMAL);
            }
        newFilterViewHolderSecond.tvOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (newFilterViewHolderSecond.llInner.getVisibility() == View.VISIBLE) {
                        newFilterViewHolderSecond.llInner.setVisibility(View.GONE);
                        newFilterViewHolderSecond.llSelectAll.setVisibility(View.GONE);
                        newFilterViewHolderSecond.tvOption.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableImgDown, null);
                    } else {
                        newFilterViewHolderSecond.llInner.setVisibility(View.VISIBLE);
                        newFilterViewHolderSecond.llSelectAll.setVisibility(View.VISIBLE);
                        populateOptionsWithChild(newFilterViewHolderSecond, position);
                        newFilterViewHolderSecond.tvOption.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableImgUp, null);
                    }
                }
            });
        }else {
            populateOptionsWithNoChild(newFilterViewHolderSecond, position);
        }


    }

    private boolean changeColorForSelectedOption(boolean changeColor, int position) {
        for(int j = 0; j< WSConstants.filterSelection.getFilters().get(parentPosition).getValues().get(position).getSubcategories().size(); j++ ){
            if(WSConstants.filterSelection.getFilters().get(parentPosition).getValues().get(position).getSubcategories().get(j).isSet_checked()){
                changeColor = true;
            }
        }
        return changeColor;
    }

    private void populateOptionsWithChild(final NewFilterViewHolderSecond newFilterViewHolderSecond, final int position) {
        listCheckBoxes.clear();
        newFilterViewHolderSecond.llInner.removeAllViews();
        newFilterViewHolderSecond.llInner.setPadding(0, 0, 0, 0);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 5f);
        params.gravity = Gravity.CENTER;
        LinearLayout.LayoutParams paramsTv = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.WRAP_CONTENT, 4.5f);
        paramsTv.gravity = Gravity.LEFT|Gravity.CENTER;
        LinearLayout.LayoutParams paramsCheckBox = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
        paramsCheckBox.gravity = Gravity.CENTER;
        params.setMargins(0,0, 0,5);

        newFilterViewHolderSecond.checkboxSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View viewCheckBoxParentParent = (View) newFilterViewHolderSecond.checkboxSelectAll.getParent().getParent();
                LinearLayout viewCheckBoxParent = viewCheckBoxParentParent.findViewById(R.id.ll_inner);
                if(newFilterViewHolderSecond.checkboxSelectAll.isChecked()){
                    newFilterViewHolderSecond.checkboxSelectAll.setChecked(true);
                    selectAllCheckBoxes(true,viewCheckBoxParent);
                    //checkBoxSettings.setCheckOnAllBoxWithNoChild(true);
                }else{
                    newFilterViewHolderSecond.checkboxSelectAll.setChecked(false);
                    selectAllCheckBoxes(false,viewCheckBoxParent);
                   // checkBoxSettings.setCheckOnAllBoxWithNoChild(false);

                }
            }
        });
        for(int i = 0; i < WSConstants.filters.get(parentPosition).getValues().get(position).getSubcategories().size(); i++){
            final int innerPosition = i;
            LinearLayout llInnerChild = new LinearLayout(context);
            llInnerChild.setOrientation(LinearLayout.HORIZONTAL);
            llInnerChild.setLayoutParams(params);

            TextView tv = new TextView(context);
            tv.setText(""+WSConstants.filters.get(parentPosition).getValues().get(position).getSubcategories().get(i).getName());
            tv.setTextSize(15);
            tv.setTextColor(Color.parseColor("#8C000000"));
            tv.setLayoutParams(paramsTv);
            tv.setGravity(Gravity.LEFT|Gravity.CENTER);
            llInnerChild.addView(tv);

            CheckBox checkBox = new CheckBox(context);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    WSConstants.filterSelection.getFilters().get(parentPosition)
                            .getValues().get(position).getSubcategories()
                            .get(innerPosition).setSet_checked(b);
                    if(changeColorForSelectedOption(false, position)){
                        tvOptionsList.get(position).setTextColor(Color.parseColor("#2196f3"));
                        tvOptionsList.get(position).setTypeface(null, Typeface.BOLD);
                    }else{
                        tvOptionsList.get(position).setTextColor(Color.BLACK);
                        tvOptionsList.get(position).setTypeface(null, Typeface.NORMAL);
                    }
                }
            });

            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!checkBoxSettings.checkAllSelectedOrNot()){
                        newFilterViewHolderSecond.checkboxSelectAll.setChecked(false);
                    }
                }
            });

            if(WSConstants.filterSelection.getFilters().get(parentPosition)
                    .getValues().get(position).getSubcategories()
                    .get(innerPosition).isSet_checked()){
                checkBox.setChecked(true);
            }else {
                checkBox.setChecked(false);
            }
            checkBox.setId(i);
            checkBox.setLayoutParams(paramsCheckBox);
            checkBox.setText("");
            checkBox.setGravity(Gravity.RIGHT);
            checkBox.setTag("checkbox_inside");
            listCheckBoxes.add(checkBox);
            llInnerChild.addView(checkBox);
            newFilterViewHolderSecond.llInner.addView(llInnerChild);
        }
    }

    private void selectAllCheckBoxes(boolean checked, LinearLayout viewCheckBoxParent) {
        for(int i=0;i<viewCheckBoxParent.getChildCount();i++) {
            View view = viewCheckBoxParent.getChildAt(i).findViewWithTag("checkbox_inside");
            if(view!=null && view instanceof CheckBox) {
                ((CheckBox) view).setChecked(checked);
            }
        }
    }

    private void populateOptionsWithNoChild(final NewFilterViewHolderSecond newFilterViewHolderSecond, final int position) {
        listCheckBoxes.clear();
        newFilterViewHolderSecond.llOption.setVisibility(View.GONE);
        newFilterViewHolderSecond.llInner.setVisibility(View.VISIBLE);
        newFilterViewHolderSecond.llSelectAll.setVisibility(View.VISIBLE);
        newFilterViewHolderSecond.llInner.removeAllViews();
        newFilterViewHolderSecond.llInner.setPadding(0, 0, 0, 0);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 5f);
        params.gravity = Gravity.CENTER;
        LinearLayout.LayoutParams paramsTv = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.WRAP_CONTENT, 4.5f);
        paramsTv.gravity = Gravity.LEFT|Gravity.CENTER;
        LinearLayout.LayoutParams paramsCheckBox = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
        paramsCheckBox.gravity = Gravity.CENTER;
        params.setMargins(0,0, 0,5);

        newFilterViewHolderSecond.checkboxSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(newFilterViewHolderSecond.checkboxSelectAll.isChecked()){
                    newFilterViewHolderSecond.checkboxSelectAll.setChecked(true);
                    checkBoxSettings.setCheckOnAllBoxWithNoChild(true);
                }else{
                    newFilterViewHolderSecond.checkboxSelectAll.setChecked(false);
                    checkBoxSettings.setCheckOnAllBoxWithNoChild(false);
                }
            }
        });
        for(int i = 0; i < WSConstants.filters.get(parentPosition).getValues().size(); i++){
            final int innerPosition = i;
            LinearLayout llInnerChild = new LinearLayout(context);
            llInnerChild.setOrientation(LinearLayout.HORIZONTAL);
            llInnerChild.setLayoutParams(params);

            TextView tv = new TextView(context);
            tv.setText(""+WSConstants.filters.get(parentPosition).getValues().get(i).getName());
            tv.setTextSize(15);
            tv.setLayoutParams(paramsTv);
            tv.setGravity(Gravity.LEFT|Gravity.CENTER);
            llInnerChild.addView(tv);

            CheckBox checkBox = new CheckBox(context);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    WSConstants.filterSelection.getFilters().get(parentPosition)
                            .getValues().get(innerPosition).setSet_checked(b);

                }
            });

            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!checkBoxSettings.checkAllSelectedOrNot()){
                        newFilterViewHolderSecond.checkboxSelectAll.setChecked(false);
                    }
                }
            });

            if(WSConstants.filterSelection.getFilters().get(parentPosition)
                    .getValues().get(innerPosition).isSet_checked()){
                checkBox.setChecked(true);
            }else {
                checkBox.setChecked(false);
            }
            checkBox.setId(i);
            checkBox.setLayoutParams(paramsCheckBox);
            checkBox.setText("");
            checkBox.setGravity(Gravity.RIGHT);
            listCheckBoxes.add(checkBox);
            llInnerChild.addView(checkBox);
            newFilterViewHolderSecond.llInner.addView(llInnerChild);
        }
    }

    @Override
    public int getItemCount() {
        return WSConstants.filters.get(parentPosition).getValues().get(0).getHasChildren()
                ?(WSConstants.filters.get(parentPosition).getValues().size()>0
                ? WSConstants.filters.get(parentPosition).getValues().size():0):1;
    }

    public class NewFilterViewHolderSecond extends RecyclerView.ViewHolder {
        TextView tvOption;
        LinearLayout llInner;
        LinearLayout llOption;
        LinearLayout llSelectAll;
        CheckBox checkboxSelectAll;
        public NewFilterViewHolderSecond(View itemView) {
            super(itemView);
            tvOption = (TextView) itemView.findViewById(R.id.tvOption);
            llInner = (LinearLayout) itemView.findViewById(R.id.ll_inner);
            llOption = (LinearLayout) itemView.findViewById(R.id.ll_option);
            llSelectAll = (LinearLayout) itemView.findViewById(R.id.ll_selectall);
            checkboxSelectAll = (CheckBox) itemView.findViewById(R.id.checkBox_selectall);
        }
    }


    public class CheckBoxSettings {

        public void setCheckOnAllBoxWithNoChild(boolean selected) {
            for(int i=0; i<listCheckBoxes.size(); i++){
                listCheckBoxes.get(i).setChecked(selected);
            }
        }

        public boolean checkAllSelectedOrNot(){
            for(int i=0; i<listCheckBoxes.size(); i++){
                if(!listCheckBoxes.get(i).isChecked()){
                    return false;
                }
            }
            return true;
        }
    }
}
