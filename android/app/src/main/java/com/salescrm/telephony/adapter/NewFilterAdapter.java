package com.salescrm.telephony.adapter;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.NewFilterActivity;
import com.salescrm.telephony.fragments.NewFilterFragmentSecond;
import com.salescrm.telephony.interfaces.NewFilterCallFragmentListener;
import com.salescrm.telephony.model.NewFilter.Filter;
import com.salescrm.telephony.model.NewFilter.Values;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.List;

/**
 * Created by prateek on 31/7/18.
 */

public class NewFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private NewFilterCallFragmentListener newFilterCallFragmentListener;
    private List<Filter> filters;
    private Bundle bundle;
    private Fragment fragment;

    public NewFilterAdapter(Context context, List<Filter> newFilters) {
        this.context = context;
        this.newFilterCallFragmentListener = (NewFilterCallFragmentListener) context;
        this.filters = newFilters;
        this.bundle = new Bundle();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewLayout;
        viewLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_filter_adapter_view_holder, parent, false);
        return new NewFilterAdapter.NewFilterViewHolder(viewLayout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        Drawable drawableImg = VectorDrawableCompat
                .create(context.getResources(), R.drawable.ic_arrowright, null);


        NewFilterViewHolder newFilterViewHolder = (NewFilterViewHolder) holder;
        if(WSConstants.filters!= null && !WSConstants.filters.isEmpty()) {
            newFilterViewHolder.tvOption1.setText(WSConstants.filters.get(position).getName());
            newFilterViewHolder.tvOption1.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableImg, null);
            //loop to see the selected options to change the color
            if(changeColorForSelectedOption(false, WSConstants.filterSelection.getFilters().get(position).getValues())){
                newFilterViewHolder.tvOption1.setTextColor(Color.parseColor("#2196f3"));
                newFilterViewHolder.tvOption1.setTypeface(null, Typeface.BOLD);
            }else {
                newFilterViewHolder.tvOption1.setTextColor(Color.BLACK);
                newFilterViewHolder.tvOption1.setTypeface(null, Typeface.NORMAL);
            }
            newFilterViewHolder.tvOption1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //newFilterCallFragmentListener.replaceFragment(position, new NewFilterFragmentSecond());
                    bundle.putString("pos", "" + position);
                    fragment = new NewFilterFragmentSecond();
                    fragment.setArguments(bundle);
                    newFilterCallFragmentListener.replaceFragment(1, fragment);
                }
            });
        }

    }

    private boolean changeColorForSelectedOption(boolean changeColor, List<Values> values) {
        for(int j = 0; j< values.size(); j++ ){
                if(values.get(j).isHas_children()){
                    for(int k = 0; k<values.get(j).getSubcategories().size(); k++){
                        if(values.get(j).getSubcategories().get(k).isSet_checked()){
                            changeColor = true;
                        }
                    }
                }else {
                    if(values.get(j).isSet_checked()){
                        changeColor = true;
                    }
                }
        }
        return changeColor;
    }

    @Override
    public int getItemCount() {
        return filters!= null && filters.size()>0 ?filters.size():0;
    }

    public class NewFilterViewHolder extends RecyclerView.ViewHolder{

        private TextView tvOption1;
        public NewFilterViewHolder(View itemView) {
            super(itemView);
            tvOption1 = (TextView) itemView.findViewById(R.id.tvOption1);
        }
    }
}
