package com.salescrm.telephony.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Ravindra P on 29-08-2016.
 */
public class PlannedActivitiesAction  extends RealmObject {

    public static final String PLANNEDACTIVITYACTION = "PlannedActivitiesAction";

    private int plannedActivitiesActionsId;
    private String plannedActivitiesActionsIconType;
    private String plannedActivitiesAction;

    public int getPlannedActivitiesActionsId() {
        return plannedActivitiesActionsId;
    }

    public void setPlannedActivitiesActionsId(int plannedActivitiesActionsId) {
        this.plannedActivitiesActionsId = plannedActivitiesActionsId;
    }

    public String getPlannedActivitiesActionsIconType() {
        return plannedActivitiesActionsIconType;
    }

    public void setPlannedActivitiesActionsIconType(String plannedActivitiesActionsIconType) {
        this.plannedActivitiesActionsIconType = plannedActivitiesActionsIconType;
    }

    public String getPlannedActivitiesAction() {
        return plannedActivitiesAction;
    }

    public void setPlannedActivitiesAction(String plannedActivitiesAction) {
        this.plannedActivitiesAction = plannedActivitiesAction;
    }
}
