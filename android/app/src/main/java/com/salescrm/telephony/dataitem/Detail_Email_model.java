package com.salescrm.telephony.dataitem;

/**
 * Created by akshata on 5/8/16.
 */
public class Detail_Email_model {
    String email;
    int emailvalue;

    public Detail_Email_model(String email, int emailvalue) {
        this.email = email;
        this.emailvalue = emailvalue;
    }

    public Detail_Email_model(int emailvalue) {
        this.email= email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getEmailvalue() {
        return emailvalue;
    }

    public void setEmailvalue(int emailvalue) {
        this.emailvalue = emailvalue;
    }
}
