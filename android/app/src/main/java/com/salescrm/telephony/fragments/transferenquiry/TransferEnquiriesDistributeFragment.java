package com.salescrm.telephony.fragments.transferenquiry;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.salescrm.telephony.R;
import com.salescrm.telephony.interfaces.DistributeLeadsListener;
import com.salescrm.telephony.interfaces.TransferEnquiriesDistributeListener;
import com.salescrm.telephony.model.TransferEnquiriesModel;
import com.salescrm.telephony.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bharath on 19/8/17.
 */

public class TransferEnquiriesDistributeFragment extends Fragment implements TransferEnquiriesDistributeListener, DistributeLeadsListener {
    private View inflatedView;
    private RecyclerView recyclerView;
    private DistributeLeadsAdapter adapter;
    private List<TransferEnquiriesModel.Item> userItemList;
    private List<TransferEnquiriesModel.Item> leadsCount;
    private TransferEnquiriesModel transferEnquiriesModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        inflatedView = inflater.inflate(R.layout.fragment_transfer_enquiries_dist, container, false);
        recyclerView = (RecyclerView) inflatedView.findViewById(R.id.recycler_view_trans_enq_dist);
        userItemList = new ArrayList<>();
        leadsCount = new ArrayList<>();
        transferEnquiriesModel = TransferEnquiriesModel.getInstance(this);
        adapter = new DistributeLeadsAdapter(this, userItemList, getContext(), leadsCount);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        if(transferEnquiriesModel.getDistributeList()!=null &&
                transferEnquiriesModel.getDistributeList().size()>0){
            populateSavedUserLists();
        }
        return inflatedView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void populateUserLists() {
        userItemList.clear();
        leadsCount.clear();
        List<TransferEnquiriesModel.Item> data = transferEnquiriesModel.getUserListTo();
        for (int i = 0; i < (data == null ? 0 : data.size()); i++) {
            if (!data.get(i).isHeader() && data.get(i).isChecked()) {
                TransferEnquiriesModel.Item item = transferEnquiriesModel.new Item();
                item.setId(data.get(i).getId());
                item.setImgUrl(data.get(i).getImgUrl());
                item.setTitle(data.get(i).getTitle());
                userItemList.add(item);
            }
        }
        if (userItemList.size() == 0) {
            adapter.notifyDataSetChanged();
            return;
        }

        int lastItem = transferEnquiriesModel.getSelectedLeadList().size() % userItemList.size();
        int sum = 0;
        for (int i = 0; i < userItemList.size(); i++) {
            TransferEnquiriesModel.Item item = transferEnquiriesModel.new Item();
            item.setId(userItemList.get(i).getId());
            if (i == userItemList.size() - 1 && lastItem != 0) {
                item.setTitle(String.valueOf(transferEnquiriesModel.getSelectedLeadList().size() - sum > 0 ? transferEnquiriesModel.getSelectedLeadList().size() - sum : 0));
            } else {
                System.out.println("Total Leads :: "+transferEnquiriesModel.getSelectedLeadList().size());
                System.out.println("User Item Size :: "+(userItemList.size()));
                int addingValue = (int) Math.ceil(transferEnquiriesModel.getSelectedLeadList().size() / (userItemList.size()));
                System.out.println("Adding val : "+addingValue);
                item.setTitle(addingValue + "");
                sum += addingValue;

            }
            leadsCount.add(item);


        }
        transferEnquiriesModel.setDistributeList(leadsCount);
        adapter.notifyDataSetChanged();
    }

    private void populateSavedUserLists() {
        userItemList.clear();
        leadsCount.clear();
        List<TransferEnquiriesModel.Item> data = transferEnquiriesModel.getUserListTo();
        for (int i = 0; i < (data == null ? 0 : data.size()); i++) {
            if (!data.get(i).isHeader() && data.get(i).isChecked()) {
                TransferEnquiriesModel.Item item = transferEnquiriesModel.new Item();
                item.setId(data.get(i).getId());
                item.setImgUrl(data.get(i).getImgUrl());
                item.setTitle(data.get(i).getTitle());
                userItemList.add(item);
            }
        }
        if (userItemList.size() == 0) {
            adapter.notifyDataSetChanged();
            return;
        }
        leadsCount.addAll(transferEnquiriesModel.getDistributeList());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDataChanged() {
        populateUserLists();
    }

    @Override
    public void onChangeDistributeLeads(int position, TransferEnquiriesModel.Item item) {
        leadsCount.set(position, item);
        transferEnquiriesModel.setDistributeList(position, item);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private class DistributeLeadsAdapter extends RecyclerView.Adapter<DistributeLeadsAdapter.ViewHolderDistribute> {
        private List<TransferEnquiriesModel.Item> userItemLists;
        private Context context;
        private List<TransferEnquiriesModel.Item> leadsCount;
        private DistributeLeadsListener listener;

        DistributeLeadsAdapter(DistributeLeadsListener listener,
                               List<TransferEnquiriesModel.Item> userItemLists,
                               Context context,
                               List<TransferEnquiriesModel.Item> leadsCount) {
            this.userItemLists = userItemLists;
            this.context = context;
            this.leadsCount = leadsCount;
            this.listener = listener;
        }

        @Override
        public ViewHolderDistribute onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolderDistribute(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_transfer_enq_dist, parent, false),
                    new EditTextChangeListener());
        }

        @Override
        public void onBindViewHolder(final ViewHolderDistribute holder, final int pos) {
            final int position = holder.getAdapterPosition();
            holder.title.setText(userItemLists.get(position).getTitle());
            Glide.with(context).
                    load(userItemLists.get(position).getImgUrl())
                    .asBitmap()
                    .centerCrop()
                    .placeholder(R.drawable.ic_person_white_48dp)
                    .error(R.drawable.ic_person_white_48dp)
                    .into(new BitmapImageViewTarget(holder.imageView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            if(resource!=null && context!=null) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                holder.imageView.setImageDrawable(circularBitmapDrawable);
                            }
                        }
                    });
            holder.editTextChangeListener.updatePosition(position);
            holder.editText.setText(leadsCount.get(position).getTitle());

        }

        @Override
        public int getItemCount() {
            return userItemLists.size();
        }

        class ViewHolderDistribute extends RecyclerView.ViewHolder {
            private TextView title;
            private ImageView imageView;
            private EditText editText;
            private EditTextChangeListener editTextChangeListener;

            ViewHolderDistribute(View itemView, EditTextChangeListener etListener) {
                super(itemView);
                editText = (EditText) itemView.findViewById(R.id.et_transfer_enq_leads);
                title = (TextView) itemView.findViewById(R.id.tv_transfer_enq_profile);
                imageView = (ImageView) itemView.findViewById(R.id.img_transfer_enq_profile);
                this.editTextChangeListener = etListener;
                editText.addTextChangedListener(editTextChangeListener);
            }
        }

        private class EditTextChangeListener implements TextWatcher {

            private int position;

            void updatePosition(int position) {
                this.position = position;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (listener != null) {
                    TransferEnquiriesModel.Item item =  leadsCount.get(position);
                    item.setTitle(s.toString());
                    leadsCount.set(position, item);
                    listener.onChangeDistributeLeads(position, item);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        }

    }
}
