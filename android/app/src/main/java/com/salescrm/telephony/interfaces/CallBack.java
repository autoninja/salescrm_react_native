package com.salescrm.telephony.interfaces;

public interface CallBack<T> {
    void onCallBack();
    void onCallBack(T data);
    void onError(String error);
}
