package com.salescrm.telephony.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.preferences.Preferences;

public class RNLostDropAnalysisActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {

    private ReactInstanceManager mReactInstanceManager;
    private ReactRootView mReactRootView;
    private Preferences pref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pref = Preferences.getInstance();
        pref.load(this);

        String module = "LOST_DROP_ANALYSIS_NEW_USED";
        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().getString("fromId", 0 + "").equalsIgnoreCase("0")) {
                module = "LOST_DROP_ANALYSIS_NEW_USED";
            } else {
                module = "LOST_DROP_ANALYSIS_DROP_REASONS";
            }

        }

        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();
        Bundle initialProps = new Bundle();
        initialProps.putBoolean("fromAndroid", true);
        initialProps.putString("token", pref.getAccessToken());
        initialProps.putString("module", module);
        initialProps.putString("startDate", getIntent().getStringExtra("startDate"));
        initialProps.putString("endDate", getIntent().getStringExtra("endDate"));
        initialProps.putString("filters", getIntent().getStringExtra("filters"));
        initialProps.putString("modelData", getIntent().getStringExtra("modelData"));
        initialProps.putString("color", getIntent().getStringExtra("color"));

        mReactRootView = new ReactRootView(this);

        mReactRootView.startReactApplication(
                mReactInstanceManager,
                "NinjaCRMSales",
                initialProps
        );
        setContentView(mReactRootView);

    }

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }


    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }
        try {
            registerReceiver(broadcastReceiver, new IntentFilter("CLOSE_RNLostDropAnalysisActivity"));

        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals("CLOSE_RNLostDropAnalysisActivity")) {
                finish();
            }
        }
    };

}
