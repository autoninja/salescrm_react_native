import React, { Component } from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';

import LostDealershipHeader from '../../../../components/lostDrop/lost_dealership_header'
import LostDealershipItem from '../../../../components/lostDrop/lost_dealership_item'
import LostDropReasonHeader from '../../../../components/lostDrop/lost_drop_reason_header'
import LostDropReasonItem from '../../../../components/lostDrop/lost_drop_reason_item'
import LostDropRevenueLoss from '../../../../components/lostDrop/lost_drop_revenue_loss'

import styles from './LostDetailsOEMLevel.style.js'

class LostDetailsOEMLevel extends Component {

  static navigationOptions = ({ navigation }) => {
    let title = "Lost";
    let color = "#2e5e86";

    if(navigation.getParam('modelData')) {
      title +=" "+navigation.getParam('modelData').name+" To";
    }
    return {
      title,
      headerTintColor: '#ffffff',
      headerStyle: {
           backgroundColor: color,
           elevation:0
         },
    };
  };

  constructor(props) {
    super(props)
    this.state = {
      selectedTap : 0,
      dealershipsData : [],
      reasonsData : [],
    }

  }

  onTabPress(selectedTap) {
    this.setState({selectedTap})
  }
  showReasons(navigation, data) {
    navigation.navigate("LostDetailsOEMDealerReasons", {data});
  }

  displayTab(data) {
    if(this.state.selectedTap == 0) {
        let dealershipsData = [];
        let totalLoss = 0;
        if(data) {
          if(data.distributor) {
            dealershipsData = data.distributor;
          }
          totalLoss = data.total_loss;
        }
        return(
          <View style={{flex:1}}>
            {dealershipsData.length==0 && (
            <View style={{alignItems:'center',alignSelf:'center', flex:1, justifyContent: 'center',}}>
              <Text style={{alignSelf:'center', fontSize:20}}> No Data </Text>
            </View>
            )}
            { dealershipsData.length>0&& (
              <View>
              <LostDealershipHeader/>
              <FlatList
                    data = {dealershipsData}
                    renderItem = {({item, index}) => {
                      if(index == dealershipsData.length-1) {
                        return(
                          <View style={{paddingBottom:100}}>
                          <LostDealershipItem  data={item} showReasons={this.showReasons.bind(this, this.props.navigation)}/>
                          <LostDropRevenueLoss style={{marginTop:10, marginBottom:0}} loss={totalLoss}/>
                          </View>)
                      }
                      else {
                        return (
                          <LostDealershipItem  data={item} showReasons={this.showReasons.bind(this, this.props.navigation)}/>
                        );
                      }

                    }
                  }
                    keyExtractor={(item, index) => index+''}
                  />
                  </View>
            )}
            </View>)
    }
    else {
      let reasonsData = [];
      let totalLoss = 0;
      if(data) {
        if(data.reasons) {
          reasonsData = data.reasons;
        }
        totalLoss = data.total_loss;
      }
      return(
        <View style={{flex:1}}>
        {reasonsData.length==0 && (
          <View style={{alignItems:'center',alignSelf:'center', flex:1, justifyContent: 'center',}}>
            <Text style={{alignSelf:'center', fontSize:20}}> No Data </Text>
          </View>
        )}
        {reasonsData.length>0 && (
          <View>
          <LostDropReasonHeader/>
          <FlatList
                data = {reasonsData}
                renderItem = {({item, index}) => {
                  if(index == reasonsData.length-1) {
                    return (
                      <View style={{paddingBottom:100}}>
                        <LostDropReasonItem data={item}/>
                        <LostDropRevenueLoss style={{marginTop:100}} loss={totalLoss}/>
                      </View>
                    );
                  }
                  else {
                    return (
                      <LostDropReasonItem data={item}/>
                    );
                  }

                }
              }
                keyExtractor={(item, index) => index+''}
              />
            </View>
        )}

        </View>
      )
    }
  }

  render() {
    let navigation = this.props.navigation;
    let oem = "";
    let lostType = "("+navigation.getParam("carType")+")";
    let lostCount = 0;
    let data = navigation.getParam("data");
    let tabWiseData ;
    if(data) {
      oem = data.name;
      lostCount = data.abs;
      tabWiseData = data.toggle_data;
    }

    let color = navigation.getParam('color','#808080');



    return (
      <View style= {styles.container}>

        <View style= {[styles.header,{backgroundColor:color}]}>
          <View style= {styles.headerLeft}>

            <View style= {styles.titleContainer}>
              <Text style= {styles.title}> {oem} </Text>
              <Text style= {styles.subTitle}> {lostType} </Text>
            </View>

            <Text style= {[styles.title, {fontWeight:'500'}]}> {lostCount} </Text>
          </View>

          <View style= {styles.headerRight}>
            <View style= {styles.switchView}>
            <View style= {this.state.selectedTap == 1 ? styles.switchViewInActive :  styles.switchViewActive}>
                <Text onPress={() => this.onTabPress(0)} style= {this.state.selectedTap == 1 ? styles.switchTextInActive : styles.switchTextActive}>Dealership</Text>
            </View>

              <View style= {this.state.selectedTap == 0 ? styles.switchViewInActive :  styles.switchViewActive}>
                  <Text onPress={() => this.onTabPress(1)} style= {this.state.selectedTap == 0 ? styles.switchTextInActive : styles.switchTextActive}>Reason</Text>
              </View>
            </View>
          </View>
        </View>


        <View style= {styles.listContainer}>
          {this.displayTab(tabWiseData)}
        </View>

      </View>
    )
  }
}

export default LostDetailsOEMLevel;
