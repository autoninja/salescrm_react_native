package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.dataitem.Detail_Mobile_model;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserDetails;
import com.salescrm.telephony.db.UserRolesDB;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by akshata on 3/8/16.
 */
public class Details_Mobile_Adapter extends ArrayAdapter<Detail_Mobile_model>  {
    private final boolean isLeadActive;
    Context context;
    AdapterInterface buttonListener;
    List<Detail_Mobile_model> mobile_items = null;
    ImageButton tvstar,tvdelete,tvedit;
    SalesCRMRealmTable salesCRMRealmTable;
    Realm realm;
    private Preferences pref;
    static int position;
    static String pos;



    public Details_Mobile_Adapter(Context context, List<Detail_Mobile_model> resource, AdapterInterface buttonListener,boolean isLeadActive)
    {
        super(context, R.layout.c360_mobile_numbers_layout, resource);
        this.context = context;
        this.mobile_items = resource;
        this.buttonListener = buttonListener;
        this.isLeadActive = isLeadActive;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        convertView = inflater.inflate(R.layout.c360_mobile_numbers_layout, parent, false);
        TextView name = (TextView) convertView.findViewById(R.id.tvnumber);
        pref = Preferences.getInstance();
        realm=Realm.getDefaultInstance();
        pref.load(getContext());
        tvstar = (ImageButton) convertView.findViewById(R.id.tvstar);
        tvedit=(ImageButton) convertView.findViewById(R.id.tvedit);
        tvdelete=(ImageButton)convertView.findViewById(R.id.tvdelete);
        name.setText(mobile_items.get(position).getNumber());

        if (mobile_items.get(position).getValue() ==1) {
            System.out.println("helllo");
            tvstar.setImageResource(R.drawable.ic_filled_star_img_24);
            tvedit.setVisibility(View.INVISIBLE);
            tvdelete.setVisibility(View.INVISIBLE);
        } else {
            //name.setText(mobile_items.get(position).getNumber());
            tvstar.setImageResource(R.drawable.ic_star_border_black_24dp);
            tvedit.setVisibility(View.VISIBLE);
            tvstar.setVisibility(View.VISIBLE);
            tvdelete.setVisibility(View.VISIBLE);
        }

        tvedit.setOnClickListener(new View.OnClickListener(){
                                      @Override
                                      public void onClick(View v) {
                                          if(isLeadActive) {
                                              System.out.println("POSITION CLICKED: " + position);
                                              buttonListener.Calleditmobile(position);
                                          }
                                      }
                                  });


        salesCRMRealmTable = realm.where(SalesCRMRealmTable.class).equalTo("leadId", Integer.parseInt(pref.getLeadID())).findFirst();


        if(mobile_items.size()>1 && mobile_items.get(position).getValue() ==0){
            if(isUserOnlyTlOrSmOrBh()) {
                tvstar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mobile_items.get(position).getValue() == 0) {
                            if (isLeadActive) {
                                buttonListener.changePrimaryNumber(position);
                            }
                            // tvstar.setImageResource(R.drawable.ic_filled_star_img_24);
                        } else {
                            // tvstar.setImageResource(R.drawable.ic_star_border_black_24dp);
                        }


                    }
                });
            }else {
                tvstar.setVisibility(View.GONE);
            }
        }

        tvdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLeadActive) {
                    buttonListener.Calldelete_number_api(position);
                }
            }
        });
        return convertView;
    }


    public void inserData(Detail_Mobile_model itemclasses)
    {
       System.out.println(getCount());
        add(itemclasses);
    }
    public interface AdapterInterface{
       public void  handleMessage();
        public  void changePrimaryNumber(int position);
        public void Calldelete_number_api(int position);
        public  void  Calleditmobile(int position);

    }



    @Override
    public int getCount() {
      return mobile_items.size();

        }

        public boolean isUserOnlyTlOrSmOrBh(){
            UserDetails data = realm.where(UserDetails.class).findFirst();
            if (data != null) {
                RealmList<UserRolesDB> userRoles = data.getUserRoles();
                for (int i = 0; i < userRoles.size(); i++) {
                    if (userRoles.get(i).getId().equalsIgnoreCase(WSConstants.TEAM_LEAD_ROLE_ID)
                            ||userRoles.get(i).getId().equalsIgnoreCase(WSConstants.MANAGER_ROLE_ID)
                            ||userRoles.get(i).getId().equalsIgnoreCase(WSConstants.USER_ROLE_BRANCH_HEAD)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

