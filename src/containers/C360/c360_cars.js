import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback, ScrollView
} from 'react-native'
import InterestedCarsDetails from './interested_cars_details'
import ExchangeCarsDetails from './c360_exchange_cars_details'
import AdditionalCarsDetails from './c360_additional_cars_details'
import RestClient from '../../network/rest_client'
import EditCarData from './edit_car_data';
//import VehicleDBService from '../../storage/vehicle_db_service';
import NavigationServise from '../../navigation/NavigationService';
import C360MarkExchangeCarActivityOption from './c360_mark_exchange_car_activity_options';

const styles = StyleSheet.create({
    top_view: {
        flex: 1,
        alignItems: 'center',
        alignSelf: "stretch"
    },
    heading: {
        fontSize: 16,
        color: '#000000',
        fontWeight: 'bold',
        margin: 5
    },
    title: {
        fontSize: 16,
        color: '#000000',
        margin: 5
    },
    grey_line: {
        height: 0.5,
        backgroundColor: "#9a9a9a",
        marginBottom: 10,
        marginTop: 10
    },
    grey_line_inner: {
        height: 0.5,
        backgroundColor: "#9a9a9a",
        marginBottom: 5,
        marginTop: 5
    },
    save: {
        backgroundColor: "#007fff"
    },
    cancel: {
        backgroundColor: "#808080"
    },
    mobile_nos_view: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        padding: 5
    },
    email_view: {
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    block_heading_title: {
        fontSize: 16,
        textAlign: 'center',
        color: '#ffffff',
        marginBottom: 5,
        marginTop: 5,
        padding: 5,
        alignSelf: 'stretch',
        backgroundColor: "#53759D"
    }
})

export default class C360Cars extends Component {
    constructor(props) {
        super(props);
        this.state = {
            leadId: this.props.leadId,
            leadLastUpdated: this.props.leadLastUpdated,
            interestedCarsDetails: this.props.interestedCarsDetails,
            exchangeCarsDetails: this.props.exchangeCarsDetails,
            additionalCarsDetails: this.props.additionalCarsDetails,
            bookingDetails: this.props.bookingDetails,
            isViewAllowed: this.props.isViewAllowed,
            loading: false,
            openCarEdit: false,
            selectedItem: '',
            isMarkActivityOptionOpen: false,
            selectedExchangeCarItem: ''
        }
    }



    changePrimaryCar = (item) => {
        //console.log("interestedCarsDetails printed")
        let { interestedCarsDetails } = this.state;
        this.setState({ loading: true });
        let input_data = {
            "lead_id": this.state.leadId,
            "leadCarId": item.lead_car_id,
            "lead_last_updated": this.state.leadLastUpdated
        }

        new RestClient().changePrimaryCar(input_data)
            .then(data => {
                if (data) {
                    //console.log("saveLeadEditedData", JSON.stringify(data))
                    if (data.statusCode == "2002" && data.result) {
                        //console.log("saveLeadEditedData", JSON.stringify(data))
                        this.setState({ loading: false });
                        this.props.refresh360();
                        //this.setState({ loading: false });
                        //this.showAlert("Booking successfull");
                    } else if (data.message) {
                        //console.log("message", message)
                        this.setState({ loading: false });
                        //this.showAlert(data.message);
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                } else {
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                }
            })
            .catch(error => {
                console.error(error);
                this.setState({ loading: false });
                //this.showAlert("Failed to book bike! Try Again");
            });
    }

    closeEditCarDetail = () => {
        this.setState({ openCarEdit: false })
    }

    openCarDetail = (item) => {
        this.setState({ openCarEdit: true, selectedItem: item })
    }

    submitEditedCarData = (editedDetail, carDetail) => {
        //let { interestedCarsDetails } = this.state;
        //console.log("editedDetail", JSON.stringify(editedDetail))
        this.setState({ loading: true });
        let input_data = {
            "lead_id": this.state.leadId,
            "presentLeadCarId": carDetail.lead_car_id,
            editedCarObject: {
                "carModel": carDetail.model_id,
                "carVariant": editedDetail.variant_id,
                "carColorId": editedDetail.color_id,
                "carFuelType": 1,
                "bookingId": null,
                "amtRecvd": null
            }
        }

        new RestClient().changeEditedInterestedCar(input_data)
            .then(data => {
                if (data) {
                    //console.log("saveLeadEditedData", JSON.stringify(data))
                    if (data.statusCode == "2002" && data.result) {
                        //console.log("saveLeadEditedData", JSON.stringify(data))
                        this.setState({ loading: false });
                        this.props.refresh360();
                        //this.setState({ loading: false });
                        //this.showAlert("Booking successfull");
                    } else if (data.message) {
                        //console.log("message", message)
                        this.setState({ loading: false });
                        //this.showAlert(data.message);
                    } else {
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    }
                } else {
                    this.setState({ loading: false });
                    //this.showAlert("Failed to book bike! Try Again");
                }
            })
            .catch(error => {
                console.error(error);
                this.setState({ loading: false });
                //this.showAlert("Failed to book bike! Try Again");
            });
    }

    openUpdateExchangeDetail = () => {
        //console.error("UpdateExchangeCarDetails", this.props.navigation)
        NavigationServise.navigate("UpdateExchangeCarDetails",
            {
                fromAndroid: false,
                //userLocations : UserInfoService.getLocationsList(),
                // allVehicles: VehicleDBService.getAllVehicle(),
                allVehicles: this.props.allVehicles,
                evaluationManagers: null,
                leadId: this.state.leadId,
                updateWithoutChangingActivity: true,
                callRefresh360: this.callRefresh360,

            });
    }

    openMarkExchangeCarActivityOption = (item) => {
        this.setState({ isMarkActivityOptionOpen: true, selectedExchangeCarItem: item })
    }
    closeMarkExchangeCarActivityOption = () => {
        this.setState({ isMarkActivityOptionOpen: false, selectedExchangeCarItem: "" })
    }

    markExchangeCarActivity = () => {
        //console.error("UpdateExchangeCarDetails", this.props.navigation)
        this.setState({ isMarkActivityOptionOpen: false })
        NavigationServise.navigate("UpdateExchangeCarDetails",
            {
                fromAndroid: false,
                //userLocations : UserInfoService.getLocationsList(),
                //allVehicles: VehicleDBService.getAllVehicle(),
                allVehicles: this.props.allVehicles,
                evaluationManagers: null,
                leadId: this.state.leadId,
                updateWithoutChangingActivity: false,
                callRefresh360: this.callRefresh360,
            });

    }

    cancelOrRescheduleActivity = (data, viewId) => {
        //console.error(data, viewId)
        this.setState({ loading: true });
        setTimeout(() => {
            this.closeMarkExchangeCarActivityOption()
            if (viewId == 1) {
                this.setState({ loading: true });
                let input_data = {
                    "lead_id": this.state.leadId,
                    "date": data.date,
                    "remarks": data.remarks
                }

                new RestClient().rescheduleExchange(input_data)
                    .then(data => {
                        if (data) {
                            if (data.statusCode == "2002" && data.result) {
                                //console.log("saveLeadEditedData", JSON.stringify(data))
                                this.setState({ loading: false });
                                this.props.refresh360();
                                //this.setState({ loading: false });
                                //this.showAlert("Booking successfull");
                            } else if (data.message) {
                                //console.log("message", message)
                                this.setState({ loading: false });
                                //this.showAlert(data.message);
                            } else {
                                this.setState({ loading: false });
                                //this.showAlert("Failed to book bike! Try Again");
                            }
                        } else {
                            this.setState({ loading: false });
                            //this.showAlert("Failed to book bike! Try Again");
                        }
                    })
                    .catch(error => {
                        console.error(error);
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    });
            }

            if (viewId == 2) {
                this.setState({ loading: true });
                let input_data = {
                    "lead_id": this.state.leadId,
                    "remarks": data.remarks
                }

                new RestClient().cancelExchange(input_data)
                    .then(data => {
                        if (data) {
                            //console.log("saveLeadEditedData", JSON.stringify(data))
                            if (data.statusCode == "2002" && data.result) {
                                //console.log("saveLeadEditedData", JSON.stringify(data))
                                this.setState({ loading: false });
                                this.props.refresh360();
                                //this.setState({ loading: false });
                                //this.showAlert("Booking successfull");
                            } else if (data.message) {
                                //console.log("message", message)
                                this.setState({ loading: false });
                                //this.showAlert(data.message);
                            } else {
                                this.setState({ loading: false });
                                //this.showAlert("Failed to book bike! Try Again");
                            }
                        } else {
                            this.setState({ loading: false });
                            //this.showAlert("Failed to book bike! Try Again");
                        }
                    })
                    .catch(error => {
                        console.error(error);
                        this.setState({ loading: false });
                        //this.showAlert("Failed to book bike! Try Again");
                    });
            }
        }, 100);
        //this.setState({ isMarkActivityOptionOpen: false })
    }

    callRefresh360 = () => {
        this.props.refresh360()
    }

    goBack = (refresh) => {
        console.error(refresh)
    }


    render() {
        //console.log("interestedCarsDetails", JSON.stringify(this.props.interestedCarsDetails))
        //console.log("interestedCarsDetails", (this.state.interestedCarsDetails)?this.state.interestedCarsDetails:'')

        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (

            <View style={styles.top_view}>
                <ScrollView style={{ width: "100%" }}>
                    {(this.state.interestedCarsDetails.length > 0) ?
                        <Text style={styles.block_heading_title}>New</Text> : null}

                    {(this.state.interestedCarsDetails.length > 0) ?
                        <InterestedCarsDetails
                            {...this.props}
                            leadDetails={this.props.leadDetails}
                            c360Information={this.props.c360Information}
                            interestedCarsDetails={this.state.interestedCarsDetails}
                            changePrimaryCar={this.changePrimaryCar}
                            openCarDetail={this.openCarDetail}
                        /> : null}

                    {(this.state.exchangeCarsDetails.length > 0) ?
                        <Text style={styles.block_heading_title}>Exchange</Text> : null}

                    {(this.state.exchangeCarsDetails.length > 0) ?
                        <ExchangeCarsDetails
                            {...this.props}
                            exchangeCarsDetails={this.state.exchangeCarsDetails}
                            leadId={this.props.leadId}
                            openUpdateExchangeDetail={this.openUpdateExchangeDetail}
                            openMarkExchangeCarActivityOption={this.openMarkExchangeCarActivityOption}
                        /> : null}

                    {(this.state.additionalCarsDetails.length > 0) ?
                        <Text style={styles.block_heading_title}>Additional</Text> : null}

                    {(this.state.additionalCarsDetails.length > 0) ?
                        <AdditionalCarsDetails
                            additionalCarsDetails={this.state.additionalCarsDetails}
                        /> : null}
                </ScrollView>
                <EditCarData
                    interestedCar={this.state.selectedItem}
                    visible={this.state.openCarEdit}
                    closeEditCarDetail={this.closeEditCarDetail}
                    submitEditedCarData={this.submitEditedCarData}
                />
                <C360MarkExchangeCarActivityOption
                    exchangeCarDetail={this.state.selectedExchangeCarItem}
                    isMarkActivityOptionOpen={this.state.isMarkActivityOptionOpen}
                    closeMarkExchangeCarActivityOption={this.closeMarkExchangeCarActivityOption}
                    markExchangeCarActivity={this.markExchangeCarActivity}
                    cancelOrRescheduleActivity={this.cancelOrRescheduleActivity}
                />
            </View>
        );
    }
}