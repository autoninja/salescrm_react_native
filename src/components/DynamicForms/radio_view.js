import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image } from 'react-native';
import RadioButton from '../../components/uielements/RadioButton'

export default class RadioView extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			isFetching: false,
			activeRadio: '',
		}
	}

	componentWillUnmount() {
		if (this.props.radioObject && this.props.radioObject.fQId) {
			this.props.pushToarray({ name: this.props.radioObject.fQId }, -1)
		}
		if (this.props.radioObject.ifVisibleMandatory) {
			this.props.mandatoryField({ id: this.props.radioObject.fQId }, -1)
		}
	}

	generateView(innerObjectsValues) {
		//console.log("innerObjectRadio questionChildren", JSON.stringify(innerObjectsValues))
		return (
			<View>
				{(innerObjectsValues.title) ?
					<Text style={{ fontSize: 18, marginTop: 10, marginBottom: 5, fontWeight: 'bold' }}>{(innerObjectsValues.ifVisibleMandatory) ? innerObjectsValues.title + "*" : ''}</Text>
					: null}

				<View style={{ marginLeft: 10 }}>
					{(innerObjectsValues.answerChildren) ?
						innerObjectsValues.answerChildren.map((item, key) => {
							return (
								<View key={key}>
									<RadioButton key={key} selected={item == this.state.activeRadio} title={item.displayText.replace("\r\n", '')}
										onClick={() => {
											//console.log("item of radio button", JSON.stringify(item))
											this.props.pushToarray({ name: innerObjectsValues.fQId, value: item, questionId: innerObjectsValues.questionId })

											if (innerObjectsValues.ifVisibleMandatory) {
												this.props.mandatoryField({ id: innerObjectsValues.fQId, isSelected: true })
											}

											this.setState({ activeRadio: item })

										}}
									/>
									{(this.state.activeRadio && this.state.activeRadio == item) ?
										innerObjectsValues.questionChildren.map((item, key) => {
											//console.log("Mogembo", this.state.activeRadio.fAnsId, item.key)
											if (this.state.activeRadio.fAnsId == item.key) {
												//console.log("item.displayTextttt", item.key, item.values.length)
												return item.values.map(itemvalue => {
													return this.props.callMainGenerateViews(itemvalue, itemvalue.formInputType)
												})
											}
										}
										) : null}
								</View>
							)
						}) : null}
				</View>
			</View>
		)


	}

	render() {
		//console.log("innerObjects", "reached")
		//console.log("innerObjects", this.props.innerObjects.fQId)
		//console.error("innerObjectRadio")
		console.log("innerObjectRadio", JSON.stringify(this.props.innerObjects))
		return (
			<View style={{ flex: 1 }}>
				{this.generateView(this.props.radioObject)}
			</View>
		)
	}
}