package com.salescrm.telephony.model;


import java.util.List;

/**
 * Created by bharath on 15/9/16.
 */
public class CreateLeadInputData {
    private int scheduledActivityId;
    private Activity_data activity_data;

    private Lead_data lead_data;

    private String remarks;

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Activity_data getActivity_data() {
        return activity_data;
    }

    public void setActivity_data(Activity_data activity_data) {
        this.activity_data = activity_data;
    }

    public Lead_data getLead_data() {
        return lead_data;
    }

    public void setLead_data(Lead_data lead_data) {
        this.lead_data = lead_data;
    }

    @Override
    public String toString() {
        return "ClassPojo [activity_data = " + activity_data + ", lead_data = " + lead_data + "]";
    }

    public int getScheduledActivityId() {
        return scheduledActivityId;
    }

    public void setScheduledActivityId(int scheduledActivityId) {
        this.scheduledActivityId = scheduledActivityId;
    }

    public class Activity_data {

        private Activity_owner activity_owner;

        private String visit_type;

        private String scheduledDateTime;

        private String activity;

        private String address;

        private String visit_time;
        private String location_id;

        private String carId;
        private String carName;
        private String assignName;
        private String activityName;
        private String remarks;

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getCarId() {
            return carId;
        }

        public void setCarId(String carId) {
            this.carId = carId;
        }

        public String getCarName() {
            return carName;
        }

        public void setCarName(String carName) {
            this.carName = carName;
        }

        public String getAssignName() {
            return assignName;
        }

        public void setAssignName(String assignName) {
            this.assignName = assignName;
        }

        public String getActivityName() {
            return activityName;
        }

        public void setActivityName(String activityName) {
            this.activityName = activityName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getVisit_time() {
            return visit_time;
        }

        public void setVisit_time(String visit_time) {
            this.visit_time = visit_time;
        }

        public Activity_owner getActivity_owner() {
            return activity_owner;
        }

        public void setActivity_owner(Activity_owner activity_owner) {
            this.activity_owner = activity_owner;
        }

        public String getVisit_type() {
            return visit_type;
        }

        public void setVisit_type(String visit_type) {
            this.visit_type = visit_type;
        }

        public String getScheduledDateTime() {
            return scheduledDateTime;
        }

        public void setScheduledDateTime(String scheduledDateTime) {
            this.scheduledDateTime = scheduledDateTime;
        }

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        @Override
        public String toString() {
            return "ClassPojo [activity_owner = " + activity_owner + ", visit_type = " + visit_type + ", scheduledDateTime = " + scheduledDateTime + ", activity = " + activity + "]";
        }


        public class Activity_owner {
            private String role_id;

            private String user_id;

            public String getRole_id() {
                return role_id;
            }

            public void setRole_id(String role_id) {
                this.role_id = role_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            @Override
            public String toString() {
                return "ClassPojo [role_id = " + role_id + ", user_id = " + user_id + "]";
            }
        }
    }

    public class Lead_data {
        private String exp_closing_date;

        private List<User_details> user_details;

        private List<Emails> emails;

        private List<Ex_car_details> ex_car_details;

        private List<Ad_car_details> ad_car_details;

        private Integer location_id;

        private Customer_details customer_details;

        private List<Car_details> car_details;

        private String mode_of_pay;

        private String lead_tag_id;

        private String lead_enq_date;

        private List<Mobile_numbers> mobile_numbers;

        private String lead_source_id;
        private String lead_source_category_id;
        private String ref_vin_reg_no;


        public String getRef_vin_reg_no() {
            return ref_vin_reg_no;
        }

        public void setRef_vin_reg_no(String ref_vin_reg_no) {
            this.ref_vin_reg_no = ref_vin_reg_no;
        }

        public String getExp_closing_date() {
            return exp_closing_date;
        }

        public String getLead_source_category_id() {
            return lead_source_category_id;
        }

        public void setLead_source_category_id(String lead_source_category_id) {
            this.lead_source_category_id = lead_source_category_id;
        }

        public void setExp_closing_date(String exp_closing_date) {
            this.exp_closing_date = exp_closing_date;
        }

        public List<User_details> getUser_details() {
            return user_details;
        }

        public void setUser_details(List<User_details> user_details) {
            this.user_details = user_details;
        }

        public List<Emails> getEmails() {
            return emails;
        }

        public void setEmails(List<Emails> emails) {
            this.emails = emails;
        }

        public List<Ex_car_details> getEx_car_details() {
            return ex_car_details;
        }

        public void setEx_car_details(List<Ex_car_details> ex_car_details) {
            this.ex_car_details = ex_car_details;
        }

        public List<Ad_car_details> getAd_car_details() {
            return ad_car_details;
        }

        public void setAd_car_details(List<Ad_car_details> ad_car_details) {
            this.ad_car_details = ad_car_details;
        }

        public Integer getLocation_id() {
            return location_id;
        }

        public void setLocation_id(Integer location_id) {
            this.location_id = location_id;
        }

        public Customer_details getCustomer_details() {
            return customer_details;
        }

        public void setCustomer_details(Customer_details customer_details) {
            this.customer_details = customer_details;
        }

        public List<Car_details> getCar_details() {
            return car_details;
        }

        public void setCar_details(List<Car_details> car_details) {
            this.car_details = car_details;
        }

        public String getMode_of_pay() {
            return mode_of_pay;
        }

        public void setMode_of_pay(String mode_of_pay) {
            this.mode_of_pay = mode_of_pay;
        }

        public String getLead_tag_id() {
            return lead_tag_id;
        }

        public void setLead_tag_id(String lead_tag_id) {
            this.lead_tag_id = lead_tag_id;
        }

        public String getLead_enq_date() {
            return lead_enq_date;
        }

        public void setLead_enq_date(String lead_enq_date) {
            this.lead_enq_date = lead_enq_date;
        }

        public List<Mobile_numbers> getMobile_numbers() {
            return mobile_numbers;
        }

        public void setMobile_numbers(List<Mobile_numbers> mobile_numbers) {
            this.mobile_numbers = mobile_numbers;
        }

        public String getLead_source_id() {
            return lead_source_id;
        }

        public void setLead_source_id(String lead_source_id) {
            this.lead_source_id = lead_source_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [exp_closing_date = " + exp_closing_date + ", user_details = " + user_details + ", emails = " + emails + ", ex_car_details = " + ex_car_details + ", ad_car_details = " + ad_car_details + ", location_id = " + location_id + ", customer_details = " + customer_details + ", car_details = " + car_details + ", mode_of_pay = " + mode_of_pay + ", lead_tag_id = " + lead_tag_id + ", lead_enq_date = " + lead_enq_date + ", mobile_numbers = " + mobile_numbers + ", lead_source_id = " + lead_source_id + "]";
        }

        public class User_details {
            private String role_id;

            private String user_id;

            public String getRole_id() {
                return role_id;
            }

            public void setRole_id(String role_id) {
                this.role_id = role_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            @Override
            public String toString() {
                return "ClassPojo [role_id = " + role_id + ", user_id = " + user_id + "]";
            }
        }

        public class Ad_car_details {
            private String milage;

            private String kms_run;
            private int brand_id;
            private String model_id;

            private boolean is_primary;
            private boolean is_activity_target;

            private String purchase_date;
            private String brand_name;
            private String model_name;

            public int getBrand_id() {
                return brand_id;
            }

            public void setBrand_id(int brand_id) {
                this.brand_id = brand_id;
            }

            public boolean is_primary() {
                return is_primary;
            }

            public boolean is_activity_target() {
                return is_activity_target;
            }

            public void setIs_activity_target(boolean is_activity_target) {
                this.is_activity_target = is_activity_target;
            }

            public String getMilage() {
                return milage;
            }

            public void setMilage(String milage) {
                this.milage = milage;
            }

            public String getKms_run() {
                return kms_run;
            }

            public void setKms_run(String kms_run) {
                this.kms_run = kms_run;
            }

            public String getModel_id() {
                return model_id;
            }

            public void setModel_id(String model_id) {
                this.model_id = model_id;
            }

            public boolean getIs_primary() {
                return is_primary;
            }

            public void setIs_primary(boolean is_primary) {
                this.is_primary = is_primary;
            }

            public String getPurchase_date() {
                return purchase_date;
            }

            public void setPurchase_date(String purchase_date) {
                this.purchase_date = purchase_date;
            }

            @Override
            public String toString() {
                return "ClassPojo [milage = " + milage + ", kms_run = " + kms_run + ", model_id = " + model_id + ", is_primary = " + is_primary + ", purchase_date = " + purchase_date + "]";
            }

            public String getBrand_name() {
                return brand_name;
            }

            public void setBrand_name(String brand_name) {
                this.brand_name = brand_name;
            }

            public String getModel_name() {
                return model_name;
            }

            public void setModel_name(String model_name) {
                this.model_name = model_name;
            }
        }


        public class Ex_car_details {
            private String brand_name;
            private String model_name;
            private String milage;

            private String kms_run;
            private int brand_id;
            private String model_id;

            private boolean is_primary;

            private boolean is_activity_target;

            private String purchase_date;

            public boolean is_primary() {
                return is_primary;
            }

            public int getBrand_id() {
                return brand_id;
            }

            public void setBrand_id(int brand_id) {
                this.brand_id = brand_id;
            }

            public boolean is_activity_target() {
                return is_activity_target;
            }

            public void setIs_activity_target(boolean is_activity_target) {
                this.is_activity_target = is_activity_target;
            }

            public String getMilage() {
                return milage;
            }

            public void setMilage(String milage) {
                this.milage = milage;
            }

            public String getKms_run() {
                return kms_run;
            }

            public void setKms_run(String kms_run) {
                this.kms_run = kms_run;
            }

            public String getModel_id() {
                return model_id;
            }

            public void setModel_id(String model_id) {
                this.model_id = model_id;
            }

            public boolean getIs_primary() {
                return is_primary;
            }

            public void setIs_primary(boolean is_primary) {
                this.is_primary = is_primary;
            }

            public String getPurchase_date() {
                return purchase_date;
            }

            public void setPurchase_date(String purchase_date) {
                this.purchase_date = purchase_date;
            }

            public String getBrand_name() {
                return brand_name;
            }

            public void setBrand_name(String brand_name) {
                this.brand_name = brand_name;
            }

            public String getModel_name() {
                return model_name;
            }

            public void setModel_name(String model_name) {
                this.model_name = model_name;
            }

            @Override
            public String toString() {
                return "ClassPojo [milage = " + milage + ", kms_run = " + kms_run + ", model_id = " + model_id + ", is_primary = " + is_primary + ", purchase_date = " + purchase_date + "]";
            }
        }

        public class Car_details {
            private String brand_name;
            private String model_name;
            private String variant_name;
            private String color_name;
            private String fuel_name;
            private String fuel_type_id;

            private String color_id;
            private int brand_id;
            private String model_id;

            private String variant_id;

            private boolean is_activity_target;

            private boolean is_primary;

//            private String no_of_car;

//            public String getNo_of_car() {
//                return no_of_car;
//            }

            /*public void setNo_of_car(int no_of_car) {
                this.no_of_car = String.valueOf(no_of_car);
            }*/

            public String getBrand_name() {
                return brand_name;
            }

            public void setBrand_name(String brand_name) {
                this.brand_name = brand_name;
            }

            public String getModel_name() {
                return model_name;
            }

            public void setModel_name(String model_name) {
                this.model_name = model_name;
            }

            public String getVariant_name() {
                return variant_name;
            }

            public void setVariant_name(String variant_name) {
                this.variant_name = variant_name;
            }

            public String getColor_name() {
                return color_name;
            }

            public void setColor_name(String color_name) {
                this.color_name = color_name;
            }

            public String getFuel_name() {
                return fuel_name;
            }

            public void setFuel_name(String fuel_name) {
                this.fuel_name = fuel_name;
            }

            public int getBrand_id() {
                return brand_id;
            }

            public void setBrand_id(int brand_id) {
                this.brand_id = brand_id;
            }

            public boolean is_activity_target() {
                return is_activity_target;
            }

            public boolean is_primary() {
                return is_primary;
            }

            public String getFuel_type_id() {
                return fuel_type_id;
            }

            public void setFuel_type_id(String fuel_type_id) {
                this.fuel_type_id = fuel_type_id;
            }

            public String getColor_id() {
                return color_id;
            }

            public void setColor_id(String color_id) {
                this.color_id = color_id;
            }

            public String getModel_id() {
                return model_id;
            }

            public void setModel_id(String model_id) {
                this.model_id = model_id;
            }

            public String getVariant_id() {
                return variant_id;
            }

            public void setVariant_id(String variant_id) {
                this.variant_id = variant_id;
            }

            public boolean getIs_activity_target() {
                return is_activity_target;
            }

            public void setIs_activity_target(boolean is_activity_target) {
                this.is_activity_target = is_activity_target;
            }

            public boolean getIs_primary() {
                return is_primary;
            }

            public void setIs_primary(boolean is_primary) {
                this.is_primary = is_primary;
            }

            @Override
            public String toString() {
                return "ClassPojo [fuel_type_id = " + fuel_type_id + ", color_id = " + color_id + ", model_id = " + model_id + ", variant_id = " + variant_id + ", is_activity_target = " + is_activity_target + ", is_primary = " + is_primary + "]";
            }
        }

        public class Customer_details {
            private String office_pin_code;

            private String first_name;

            private String occupation;

            private String office_address;

            private String company_name;

            private String age;

            private String residence_address;

            private String last_name;

            private String pin_code;

            private String gender;

            private String title;

            private String office_city;

            private String city;

            public String getOffice_pin_code() {
                return office_pin_code;
            }

            public void setOffice_pin_code(String office_pin_code) {
                this.office_pin_code = office_pin_code;
            }

            public String getFirst_name() {
                return first_name;
            }

            public void setFirst_name(String first_name) {
                this.first_name = first_name;
            }

            public String getOccupation() {
                return occupation;
            }

            public void setOccupation(String occupation) {
                this.occupation = occupation;
            }

            public String getOffice_address() {
                return office_address;
            }

            public void setOffice_address(String office_address) {
                this.office_address = office_address;
            }

            public String getCompany_name() {
                return company_name;
            }

            public void setCompany_name(String company_name) {
                this.company_name = company_name;
            }

            public String getAge() {
                return age;
            }

            public void setAge(String age) {
                this.age = age;
            }

            public String getResidence_address() {
                return residence_address;
            }

            public void setResidence_address(String residence_address) {
                this.residence_address = residence_address;
            }

            public String getLast_name() {
                return last_name;
            }

            public void setLast_name(String last_name) {
                this.last_name = last_name;
            }

            public String getPin_code() {
                return pin_code;
            }

            public void setPin_code(String pin_code) {
                this.pin_code = pin_code;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getOffice_city() {
                return office_city;
            }

            public void setOffice_city(String office_city) {
                this.office_city = office_city;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            @Override
            public String toString() {
                return "ClassPojo [office_pin_code = " + office_pin_code + ", first_name = " + first_name + ", occupation = " + occupation + ", office_address = " + office_address + ", company_name = " + company_name + ", age = " + age + ", residence_address = " + residence_address + ", last_name = " + last_name + ", pin_code = " + pin_code + ", gender = " + gender + ", office_city = " + office_city + ", city = " + city + "]";
            }
        }


        public class Emails {
            private String address;

            private boolean is_primary;

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public boolean getIs_primary() {
                return is_primary;
            }

            public void setIs_primary(boolean is_primary) {
                this.is_primary = is_primary;
            }

            @Override
            public String toString() {
                return "ClassPojo [address = " + address + ", is_primary = " + is_primary + "]";
            }
        }

        public class Mobile_numbers {
            private String number;

            private boolean is_primary;

            public String getNumber() {
                return number;
            }

            public void setNumber(String number) {
                this.number = number;
            }

            public boolean getIs_primary() {
                return is_primary;
            }

            public void setIs_primary(boolean is_primary) {
                this.is_primary = is_primary;
            }

            @Override
            public String toString() {
                return "ClassPojo [number = " + number + ", is_primary = " + is_primary + "]";
            }
        }
    }

}
