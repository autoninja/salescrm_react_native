package com.salescrm.telephony.db;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by bharath on 29/6/17.
 */

public class EnqSubInnerSourceDB extends RealmObject {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
