import { realm } from './realm_schemas';
import { EvaluationManagerConfModel } from '../models/evaluation_manager_conf_model'

const EvaluationManagersConfDBService = {
  findAll: function () {
    return realm.objects('EvaluationManagersConfDB');
  },
  save: function (evaluationManagerConfList) {
    realm.write(() => {
      evaluationManagerConfList.map((evaluationManagerConf) => {
        realm.create('EvaluationManagersConfDB', evaluationManagerConf, true);
      })
    })
    let evaluationManagersConfDB = realm.objects('EvaluationManagersConfDB');
    console.log('EvaluationManagersConfDB.length', evaluationManagersConfDB.length);
  },
  getEvaluationManagerConf: () => {
    let evaluationManagersConfVal = [];
    let evaluationManagersConfDB = realm.objects('EvaluationManagersConfDB');
    if (evaluationManagersConfDB) {
      evaluationManagersConfDB.map((evaluationManagerConf) => {
        console.log('Hello:' + evaluationManagerConf.locationId, evaluationManagerConf.hasEvaluationManager)
        evaluationManagersConfVal.push(new EvaluationManagerConfModel(evaluationManagerConf.locationId, evaluationManagerConf.hasEvaluationManager));
      })
    }
    evaluationManagersConfVal.map((val) => {
      console.log('Hello:Again:' + val.locationId, val.hasEvaluationManager)
    })
    return evaluationManagersConfVal;
  },
  deleteAll: function () {
    realm.write(() => {
      realm.delete(realm.objects('EvaluationManagersConfDB'));
    })
  },
}

export default EvaluationManagersConfDBService;
