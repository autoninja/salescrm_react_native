package com.salescrm.telephony.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.salescrm.telephony.response.Error;

/**
 * Created by prateek on 28/11/17.
 */

public class GetOtpForPNT {

    @SerializedName("error")
    @Expose
    private Error error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public class Result {
        @SerializedName("primary_number")
        @Expose
        private String primaryNumber;

        public String getPrimaryNumber() {
            return primaryNumber;
        }

        public void setPrimaryNumber(String primaryNumber) {
            this.primaryNumber = primaryNumber;
        }
    }
}
