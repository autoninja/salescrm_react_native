package com.salescrm.telephony.interfaces;

import com.salescrm.telephony.model.FormSubmissionInputData;

import java.util.List;

/**
 * Created by bharath on 2/11/16.
 */

public interface FormUploadListener {
    void onClickFormUpload(FormSubmissionInputData formSubmissionInputData, int selectPntTaskAnswerValue);
    void onClickFormDataChangeListener(List<FormSubmissionInputData.Form_response> form_responses);
}
