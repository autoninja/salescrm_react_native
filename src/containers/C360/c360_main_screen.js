import React, { Component } from 'react';
import {
    Linking,
    Platform, View, Text, Image, Switch, TouchableOpacity, Button, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback
} from 'react-native'
import C360Tabs from './c360_tabs'
import RestClient from '../../network/rest_client'
import { connect } from 'react-redux';
import NavigationService from '../../navigation/NavigationService'


const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: "#2e5e86",
        paddingTop: (Platform.OS) === 'ios' ? 20 : 5,
    },
    top_view: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        paddingStart: 6,
        paddingEnd: 6,
        paddingTop: 16,
        paddingBottom: 16
    },
    bottom_view: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    top_view_name_heading: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
    },
    top_view_other_half: {
        alignSelf: 'stretch',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
        paddingStart: 24,
        paddingTop: 10
    },
    view_circle_light: {
        width: 8,
        height: 8,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        borderRadius: 4,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#00C853',
    },
    view_circle_dark: {
        width: 8,
        height: 8,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        borderRadius: 4,
        backgroundColor: '#00C853'
    },
})

class C360MainScreen extends Component {
    state = {
        closeRecodringPlayer: false,
    }

    constructor(props) {
        super(props);
        this.state = {
            isFetching: false,
            isLoading: false,
            results: '',
            leadId: this.props.navigation ? this.props.navigation.getParam('leadId') : this.props.leadId,
            open_tab: this.props.navigation ? this.props.navigation.getParam('OPEN_TAB', 'Tasks') : 'Tasks',
            formSubmissionInputData: this.getFormSubmissionInputData()
        };
    }
    getLeadId() {
        let leadId;
        if (this.props.navigation) {
            leadId = [this.props.navigation.getParam('leadId')];
        } else {
            leadId = [this.props.leadId]
        }
        return leadId;
    }
    componentDidMount() {
        this.getLeadInformation(this.getLeadId());
    }
    getFormSubmissionInputData() {
        if (this.props.navigation) {
            let formInputData = this.props.navigation.getParam('formSubmissionInputData', null);
            let data
            if (formInputData) {
                data = {
                    lead_id: formInputData.lead_id,
                    lead_last_updated: formInputData.lead_last_updated,
                    scheduled_activity_id: formInputData.scheduled_activity_id,
                    action_id: formInputData.action_id,
                    form_response: this.getFormsInput()
                }
            }
            return data;
        }
        return null;

    }

    getFormsInput() {
        let formInput = this.props.navigation.getParam('formSubmissionInputData', null);
        let inputArray = []
        formInput.form_response.map((item) => {
            inputArray.push({ name: item.name, value: { fAnsId: item.value.fAnsId, displayText: item.value.displayText, answerValue: item.value.answerValue } })
        })
        return inputArray;
    }

    getLeadInformation = (leadId) => {
        this.setState({ isLoading: true, isFetching: true });
        new RestClient().getC360LeadInformation(
            { lead_id: leadId }).then((responseJson) => {

                this.setState({
                    isLoading: false,
                    isFetching: false,
                    results: responseJson.result[0],

                }, function () {
                    // In this block you can do something with new state.
                    //console.log(responseJ
                    // const size  = responseJson.result.length;
                    // console.log(responseJson.result.length);
                });
            }).catch(error => {
                console.error(error);
                console.log(error)

            });
    }

    refresh360 = (leadId) => {
        this.setState({ open_tab: 'Vehicles' })
        this.getLeadInformation(this.getLeadId());
    }

    getStagePointer = () => {
        var pointer = 0;
        for (var i = 0; i < this.state.results.customer_details.customerDetails.lead_stage_progress.length; i++) {
            if (this.state.results.customer_details.customerDetails.lead_stage_progress[i].width == 100) {
                pointer = i;
            }
        }

        return pointer;
    }

    getStageName = () => {
        var pointer = 0;
        for (var i = 0; i < this.state.results.customer_details.customerDetails.lead_stage_progress.length; i++) {
            if (this.state.results.customer_details.customerDetails.lead_stage_progress[i].width == 100) {
                pointer = i;
            }
        }
        return this.state.results.customer_details.customerDetails.lead_stage_progress[pointer].name + '';
    }

    render() {
        //console.error("c360main", JSON.stringify(this.props.navigation.getParam('formSubmissionInputData', null)))
        let index = this.props.navigation ? this.props.navigation.dangerouslyGetParent().state.index : 0;
        let ilFlow = this.props.il_flow ? true : false;
        let ilBankFlow = this.props.il_bank_flow ? true : false;

        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator />
                </View>
            );
        }
        if (this.state.results != null && this.state.results.customer_details && this.state.results.customer_details.is_view_allowed) {
            let stageName = this.getStageName();
            let stagePointer = this.getStagePointer();
            return (
                <View style={styles.main}>
                    <View style={[styles.top_view, { borderLeftWidth: 2 }]}>
                        <View style={styles.top_view_name_heading}>
                            <TouchableOpacity onPress={() => {
                                this.props.closeRecodringPlayerFunc();
                                // get the index of the route

                                // handle the index we get
                                if (index > 0) {
                                    this.props.navigation.goBack()
                                }
                                else {
                                    NavigationService.navigate('Home');
                                }
                                global.OPEN_C360_WITH_DEEP_LINK = false;
                            }}>
                                <Image
                                    style={{ width: 24, height: 24, alignItems: 'center' }}
                                    source={require('../../images/ic_go_back.png')}
                                />

                            </TouchableOpacity>
                            <Text style={{ fontSize: 16, color: '#838383' }}>{this.state.results.customer_details.customerDetails.title} </Text>
                            <Text style={{ fontSize: 20 }}>{this.state.results.customer_details.customerDetails.first_name + this.state.results.customer_details.customerDetails.last_name}</Text>
                        </View>
                        <View style={styles.top_view_other_half}>
                            <View style={{ flexDirection: 'column' }}>
                                <View style={{ flexDirection: 'row' }}>
                                    {this.state.results.customer_details.customerDetails.lead_stage_progress.map((item, index) => {

                                        return (
                                            <View style={{ marginStart: 2, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }} key={index}>
                                                {(item.width == 100) ? <View style={styles.view_circle_dark} /> : <View style={styles.view_circle_light} />}
                                                {(stagePointer == index) ? <Image
                                                    style={{ width: 10, height: 10, alignItems: 'center', marginTop: 5, marginBottom: -5 }}
                                                    source={require('../../images/ic_up_arrow.png')}
                                                /> : <View style={{ width: 10, height: 10, marginTop: 5, marginBottom: -5 }}></View>}
                                            </View>
                                        )
                                    })
                                    }
                                </View>
                                <Text style={{
                                    fontSize: 13,
                                    marginStart: 2,
                                    borderRadius: 2, paddingStart: 20, paddingEnd: 20,
                                    backgroundColor: '#aada79', textAlign: 'center', color: '#406541'
                                }}>{stageName}</Text>

                            </View>
                            {(!ilFlow && !ilBankFlow) && (
                                <View style={{
                                    alignItems: 'flex-end',
                                    marginLeft: 20,
                                    paddingEnd: 20
                                }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 12 }}>{this.state.results.customer_details.customerDetails.lead_age}</Text>
                                        <Text style={{ fontSize: 12, backgroundColor: '#c6c6c6', marginLeft: 5, color: '#5e5e5e' }}> AGEING </Text>
                                    </View>
                                    <Text style={{ fontSize: 12, marginTop: 4 }}>{this.state.results.customer_details.interestedCarsDetails[0].variant}</Text>
                                </View>
                            )}



                            {(ilFlow || ilBankFlow) && (
                                <View style={{
                                    flex: 1,
                                    alignItems: 'flex-end',
                                    marginLeft: 20,
                                    paddingEnd: 24
                                }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 14 }}>{"Lead ID: "}</Text>
                                        <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{this.state.results.customer_details.customerDetails.lead_id}</Text>
                                    </View>
                                    {ilBankFlow && (
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={{ fontSize: 14 }}>{"OTB: "}</Text>
                                            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{this.state.results.customer_details.customerDetails.pin_code}</Text>
                                        </View>
                                    )}
                                </View>
                            )}


                        </View>
                        {(ilFlow) && (
                            <View style={[styles.top_view_other_half, {
                                paddingEnd: 24,
                                justifyContent: 'space-between'
                            }]}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: 14 }}>{"Policy end date: "}</Text>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold' }}>{this.state.results.customer_details.customerDetails.close_date}</Text>
                                </View>
                                <TouchableOpacity
                                    onPress={() => {
                                        Linking.openURL(this.state.results.customer_details.customerDetails.full_address);
                                    }}
                                    style={{ flexDirection: 'row', alignSelf: 'flex-end', }}>
                                    <Text style={{
                                        fontSize: 13, paddingTop: 2, paddingBottom: 2, paddingStart: 8, paddingEnd: 8,
                                        borderRadius: 2,
                                        backgroundColor: '#F44336', color: '#fff'
                                    }}>Edit Policy</Text>
                                </TouchableOpacity>
                            </View>
                        )}

                        {(ilBankFlow) && (
                            <View style={[styles.top_view_other_half, {
                                paddingEnd: 24,
                                justifyContent: 'space-between'
                            }]}>
                                <Text style={{ fontSize: 14 }}>{this.state.results.customer_details.customerDetails.full_address}</Text>

                            </View>
                        )}
                    </View>
                    <View style={styles.bottom_view}>
                        <C360Tabs leadId={this.state.leadId}
                            plannedActivities={this.state.results.customer_details.plannedActivities}
                            leadDetails={this.state.results.customer_details.customerDetails}
                            c360Information={this.state.results.customer_details}
                            interestedCarsDetails={this.state.results.customer_details.interestedCarsDetails}
                            exchangeCarsDetails={this.state.results.customer_details.exchangeCarsDetails}
                            additionalCarsDetails={this.state.results.customer_details.additionalCarsDetails}
                            bookingDetails={this.state.results.customer_details.bookingDetails}
                            isViewAllowed={this.state.results.customer_details.is_view_allowed}
                            refresh360={this.refresh360}
                            OPEN_TAB={this.state.open_tab}
                            formSubmissionInputData={this.state.formSubmissionInputData}
                            {...this.props}
                        />
                    </View>
                </View>
            );
        } else {
            return (
                <View style={styles.main}>
                    <Text style={{ justifyContent: 'center', alignItems: 'center', color: '#fff' }}>No Information available Or Unauthorised Access</Text>

                    <TouchableOpacity style={{ width: '100%', alignItems: "center", }}
                        onPress={() => {
                            this.props.closeRecodringPlayerFunc();
                            // get the index of the route

                            // handle the index we get
                            if (index > 0) {
                                this.props.navigation.goBack()
                            }
                            else {
                                NavigationService.navigate('Home');
                            }
                            global.OPEN_C360_WITH_DEEP_LINK = false;
                        }}
                    >
                        <View style={{ height: 40, width: 240, justifyContent: "center", marginTop: 40, backgroundColor: '#007fff', borderRadius: 6 }}>

                            <Text adjustsFontSizeToFit={true}
                                numberOfLines={1} style={{ textAlign: 'center', fontSize: 18, textAlignVertical: 'center', color: '#fff' }}>
                                Close
                        </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            );
        }
    }

}

const mapStateToProps = state => {
    return {

        closeRecodringPlayer: state.closeRecodringPlayer.closeRecodringPlayer
    }
}

const mapDispatchToProps = dispatch => {
    return {
        closeRecodringPlayerFunc: () => dispatch({ type: 'CLOSE_RECORD_PLAYING' })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(C360MainScreen)