import React, { Component } from 'react'
import { Keyboard, Platform, View, Text, Image, TouchableOpacity, YellowBox, ScrollView, Switch, FlatList, ActivityIndicator, TextInput, Button, KeyboardAvoidingView} from 'react-native'


import styles from '../styles/styles'

import DeviceInfo from 'react-native-device-info';
import RestClient from '../network/rest_client';
import Toast, {DURATION} from 'react-native-easy-toast';
import { StackActions, NavigationActions } from 'react-navigation';

import * as async_storage from '../storage/async_storage';


export default class Login extends Component {

  constructor(props) {

    super(props);
    this.state = {loading:false, mobile:''};



    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);

  }
  componentDidMount() {
    console.log("Datatat");
    console.log(JSON.stringify(this.props.navigation));
    async_storage.deleteAll();
  }

  login() {
    Keyboard.dismiss()
    if(this.state.mobile.length!=10) {
       this.refs.toast.show('Please enter 10 digit mobile number');
       return;
    }
    this.setState({loading:true});
    let { navigation } = this.props;
    new RestClient().login({
      mobile_no:this.state.mobile,
      device_id:DeviceInfo.getUniqueID()}).then( (data) => {
            this.setState({ loading: false})
            if(data) {
              if(data.result && data.result.success) {
                  navigation.navigate('ValidateOtp', {mobile:this.state.mobile, device_id:DeviceInfo.getUniqueID()});
              }
              else if(data.message) {
                  this.refs.toast.show(data.message);
              }
              else {
                  this.refs.toast.show('Error in login');
              }

            }
            else {
                this.refs.toast.show('Error in login');
            }

        }).catch(error => {
          this.setState({ loading: false});
          if (!error.status) {
              this.refs.toast.show('No Internet Connection');
          }
        });

  }

  render() {

    const { navigation } = this.props;

    let loaderStyle = this.state.loading?styles.overlay:{display:'none'};

    return <View style= {styles.MainContainerWhite}>
    <Toast
                    ref="toast"
                     style={{backgroundColor:'red'}}
                     fadeInDuration={750}
                     fadeOutDuration={750}
                     opacity={0.8}
                     textStyle={{color:'white'}}
                 />
    <ScrollView keyboardShouldPersistTaps="handled" contentContainerStyle={{flexGrow: 1}}>




           <View style={{flex:2,height:'100%', justifyContent:'center', alignItems:'center'}}>
           <Image source={require( '../images/ic_log_ninja_crm.png')} style={{marginTop:40,width:227, height:55.5,}}/>



           <View style={{flex:1,alignItems:'center', justifyContent:'center'}}>

           <TextInput
       style={{textAlign:'center',height: 50, marginLeft:20,marginRight:20,width:240, padding:2, borderColor: 'gray', borderBottomWidth: 1,}}
       onChangeText={(mobile) => this.setState({mobile})}
       maxLength = {10}
       placeholder="Enter Mobile Number"
       placeholderTextColor="#808080"
       value={this.state.mobile}
       keyboardType ='numeric'

     />





  <TouchableOpacity  style={{width:'100%', alignItems: "center",}} onPress ={()=> this.login()}>
   <View style={{height:40, width:240,justifyContent: "center", marginTop:40,backgroundColor:'#007fff', borderRadius:6}}>

   <Text adjustsFontSizeToFit={true}
        numberOfLines={1} style={{textAlign:'center', fontSize:18,textAlignVertical: 'center', color:'#fff'}}>
   Login
   </Text>



   </View>
   </TouchableOpacity>





           </View>

           </View>

           </ScrollView>




          <View style={{flexDirection:'row',justifyContent:'center'}}>
          <Text style= {{color:'#808080'}}>Powered by </Text>
          <Image source={require( '../images/ic_log_autoninja.png')} style={{width:67.5, height:15,}}/>
          </View>
          <View style={loaderStyle} >
          <ActivityIndicator
                animating={this.state.loading}
                style={{flex: 1,   alignSelf:'center',}}
                color="#fff"
                size="large"
                hidesWhenStopped={true}

          />
          </View>

            </View>




  }
}
