import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import MenuItem from './menu_item';
import styles from '../styles/styles';
import UserInfoService from '../storage/user_info_service';
import DealershipService from '../storage/dealership_service';
import AppConfDBService from '../storage/app_conf_db_service'
import StorageUtils from '../storage/storage_utils';
import RestClient from '../network/rest_client';
import { StackActions, NavigationActions } from 'react-navigation';
import Toast, { DURATION } from 'react-native-easy-toast';
import ImagePicker from 'react-native-image-picker';

const options = {
    title: 'Select Photo',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

export default class SlideMenu extends Component {

    constructor(props) {
        super(props);
        this.state = { user_info: {}, image_uploading: false, };
    }

    uploadImage(base64_image) {
        new RestClient().uploadImage({
            base64_image: base64_image
        }).then((data) => {
            this.setState({ image_uploading: false });
            if (data) {
                if (data.statusCode == '2002' && data.result) {
                    UserInfoService.update_dp_url(data.result.dp_url);
                    DealershipService.update_dp_url(data.result.dp_url);
                    let user_info = UserInfoService.findFirst();
                    this.setState({ user_info: user_info });
                }
                else if (data.message) {
                    this.refs.toast.show(data.message);
                }
                else {
                    this.refs.toast.show('Error uploading photo');
                }

            }
            else {
                this.refs.toast.show('Error uploading photo');
            }

        }).catch(error => {
            console.error(error);

            if (!error.status) {
                this.refs.toast.show('Network error');
            }

            else {
                this.refs.toast.show('Error uploading photo');
            }

            this.setState({ loading: false })
        });
    }


    showImagePicker() {
        this.setState({ image_uploading: true });
        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                this.setState({ image_uploading: false });
                console.log('User cancelled image picker');
            } else if (response.error) {
                this.setState({ image_uploading: false });
                console.log('ImagePicker Error: ', response.error);
            } else {
                const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                this.uploadImage(response.data);
            }
        });
    }

    componentDidMount() {
        let user_info = UserInfoService.findFirst();
        let roles = UserInfoService.getRoles();
        this.setState({ user_info: user_info, roles: roles, avatarSource: {} });
    }

    capitalizeFirstLetter(string) {
        if (!string) {
            return '';
        }
        return string.charAt(0).toUpperCase() + string.slice(1);

    }

    render() {
        let gameEnabled = StorageUtils.isGamificationEnabled();
        let settingsView = null;
        let teamShuffle = null;
        let isColdVisitEnabled = AppConfDBService.getVal('cold_visits_enabled');
        if (UserInfoService.isUserBranchHead() || UserInfoService.isUserOperations()) {
            settingsView = <View><View style={{ width: '100%', height: 0.3, backgroundColor: '#607D8B', marginTop: 2 }} /><MenuItem keyName='Settings' title='Settings' imageSource={require('../images/ic_settings.png')} navigationProps={this.props.navigation} /></View>
        }

        if (UserInfoService.isUserBranchHead()) {
            teamShuffle = <View><View style={{ width: '100%', height: 0.3, backgroundColor: '#607D8B', marginTop: 2 }} /><MenuItem keyName='TeamShuffle' title='Team Shuffle' imageSource={require('../images/ic_team_in_active.png')} navigationProps={this.props.navigation} /></View>
        }
        let app_user_name = 'User';
        let dp_url = '';
        let role = ''
        if (this.state.roles) {
            this.state.roles.map((user_role, index) => {
                role = role + user_role.name;
                if (index < this.state.roles.length - 1) {
                    role = role + ", ";
                }
            });
        }

        if (this.state.user_info) {
            role = role + ', ' + this.capitalizeFirstLetter(this.state.user_info.dealer);
            app_user_name = this.state.user_info.name;
            dp_url = this.state.user_info.dp_url;
            // console.log(this.state.roles);
        }

        return (
            <ScrollView contentContainerStyle={styles.contentContainer}>
                <View style={styles.sideMenuContainer}>
                    <ActivityIndicator
                        animating={this.state.image_uploading}
                        color="#fff"
                        size="large"
                        hidesWhenStopped={true}
                        style={{ flex: 1, alignSelf: 'center', display: (this.state.image_uploading ? 'flex' : 'none'), width: 96, height: 96, }}
                    />
                    <Toast
                        ref="toast"
                        style={{ backgroundColor: 'red' }}
                        fadeInDuration={750}
                        fadeOutDuration={750}
                        opacity={0.8}
                        textStyle={{ color: 'white' }}
                    />
                    <TouchableOpacity disabled={this.state.image_uploading} onPress={() => this.showImagePicker()}>
                        <Image
                            source={{ uri: dp_url }}
                            style={{ display: (this.state.image_uploading ? 'none' : 'flex'), width: 96, height: 96, borderRadius: 96 / 2, }}
                        />
                    </TouchableOpacity>

                    <Text style={styles.appTitle}> {app_user_name} </Text>
                    <TouchableOpacity onPress={() => {
                        this.props.navigation.dispatch(StackActions.push({
                            routeName: 'SwitchDealership'
                        }))
                    }}>
                        <View style={{ marginTop: 10, marginLeft: 5, marginRight: 5, padding: 6, borderWidth: 1, borderColor: '#eeeeee', borderRadius: 3, justifyContent: 'center' }}>
                            <Text style={{ alignSelf: 'center', color: '#eeeeee', fontSize: 16 }}> {role} </Text>
                        </View>
                    </TouchableOpacity>




                    <View style={{ width: '100%', marginTop: 10 }}>
                        {gameEnabled ? <MenuItem keyName='LeaderBoard' title='Leader Board' imageSource={require('../images/ic_leader_board.png')} navigationProps={this.props.navigation} /> : <View />}

                        <MenuItem keyName='TeamDashboard' title='Dashboard' imageSource={require('../images/ic_team_dashboard.png')} navigationProps={this.props.navigation} />

                        <MenuItem keyName='MyTasks' title='My Tasks' imageSource={require('../images/ic_my_tasks.png')} navigationProps={this.props.navigation} />

                        {teamShuffle}
                        
                        {settingsView}

                        {isColdVisitEnabled ? <MenuItem keyName='ColdVisitDashboard' title='Cold Visits' imageSource={require('../images/ic_cold_visit_menu.png')} navigationProps={this.props.navigation} /> : <View />}


                        <MenuItem keyName='Logout' title='Logout' imageSource={require('../images/ic_logout.png')} navigationProps={this.props.navigation} />
                    </View>





                </View>
            </ScrollView>
        );
    }
}
