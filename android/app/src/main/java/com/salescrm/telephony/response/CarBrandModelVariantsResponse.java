package com.salescrm.telephony.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bharath on 11/12/16.
 */

public class CarBrandModelVariantsResponse implements Serializable {

    private String statusCode;

    private String message;

    private List<Result> result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public List<Result> getResult ()
    {
        return result;
    }

    public void setResult (List<Result> result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }

    public class Result
    {
        private String brand_id;

        private List<Brand_models> brand_models;

        private String brand_name;

        public String getBrand_id ()
        {
            return brand_id;
        }

        public void setBrand_id (String brand_id)
        {
            this.brand_id = brand_id;
        }

        public List<Brand_models> getBrand_models ()
        {
            return brand_models;
        }

        public void setBrand_models (List<Brand_models> brand_models)
        {
            this.brand_models = brand_models;
        }

        public String getBrand_name ()
        {
            return brand_name;
        }

        public void setBrand_name (String brand_name)
        {
            this.brand_name = brand_name;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [brand_id = "+brand_id+", brand_models = "+brand_models+", brand_name = "+brand_name+"]";
        }

        public class Brand_models
        {
            private String model_id;

            private List<Car_variants> car_variants;

            private String model_name;

            private String category;

            public String getModel_id ()
            {
                return model_id;
            }

            public void setModel_id (String model_id)
            {
                this.model_id = model_id;
            }

            public List<Car_variants> getCar_variants ()
            {
                return car_variants;
            }

            public void setCar_variants (List<Car_variants> car_variants)
            {
                this.car_variants = car_variants;
            }

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public String getModel_name ()
            {
                return model_name;
            }

            public void setModel_name (String model_name)
            {
                this.model_name = model_name;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [model_id = "+model_id+", car_variants = "+car_variants+", model_name = "+model_name+"]";
            }
        }

        public class Car_variants
        {
            private String fuel_type_id;

            private String fuel_type;

            private String color;

            private String color_id;

            private String variant_id;

            private String variant;

            public String getFuel_type_id ()
            {
                return fuel_type_id;
            }

            public void setFuel_type_id (String fuel_type_id)
            {
                this.fuel_type_id = fuel_type_id;
            }

            public String getFuel_type ()
            {
                return fuel_type;
            }

            public void setFuel_type (String fuel_type)
            {
                this.fuel_type = fuel_type;
            }

            public String getColor ()
            {
                return color;
            }

            public void setColor (String color)
            {
                this.color = color;
            }

            public String getColor_id ()
            {
                return color_id;
            }

            public void setColor_id (String color_id)
            {
                this.color_id = color_id;
            }

            public String getVariant_id ()
            {
                return variant_id;
            }

            public void setVariant_id (String variant_id)
            {
                this.variant_id = variant_id;
            }

            public String getVariant ()
            {
                return variant;
            }

            public void setVariant (String variant)
            {
                this.variant = variant;
            }

            @Override
            public String toString()
            {
                return "ClassPojo [fuel_type_id = "+fuel_type_id+", fuel_type = "+fuel_type+", color = "+color+", color_id = "+color_id+", variant_id = "+variant_id+", variant = "+variant+"]";
            }
        }
    }
}

