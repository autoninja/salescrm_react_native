import React, { Component } from "react";
import { Platform, Linking, AppState } from "react-native";
const url = require("url");
import NavigationService from "./src/navigation/NavigationService";

import DeviceInfo from "react-native-device-info";
import RestClient from "./src/network/rest_client";
import UserInfoService from "./src/storage/user_info_service";
import * as async_storage from "./src/storage/async_storage";
import TopLevelNavigator from "./src/navigation/TopLevelNavigator";

import Utils from "./src/utils/Utils";

const CleverTap = require("clevertap-react-native");

import * as Sentry from "@sentry/react-native";

if (!__DEV__) {
  Sentry.init({
    dsn: "https://41c8cdb33fa746f6beaae1f6b3266e97@sentry.io/1761934"
  });
}

const endDate = new Date();
const startDate = new Date();
startDate.setDate(1);
const startDateStr =
  startDate.getFullYear() +
  "-" +
  ("0" + (startDate.getMonth() + 1)).slice(-2) +
  "-" +
  ("0" + startDate.getDate()).slice(-2);
const endDateStr =
  endDate.getFullYear() +
  "-" +
  ("0" + (endDate.getMonth() + 1)).slice(-2) +
  "-" +
  ("0" + endDate.getDate()).slice(-2);

global.global_filter_date = { startDateStr, endDateStr };
global.token = null;
global.appUserId = null;
global.selectedGameLocationId = null;
global.selectedGameDate = new Date();
global.selectedTeamdashboardTab = 0;
global.global_etvbr_filters_selected = [];
global.OPEN_C360_WITH_DEEP_LINK = false;
global.OPEN_C360_WITH_DEEP_LINK_LEAD_ID = 0;
global.DEALER_NAME = UserInfoService.findFirst()
  ? UserInfoService.findFirst().dealer
  : "";

window.getSelectedGameDate = function () {
  return (
    global.selectedGameDate.getMonth() +
    1 +
    "-" +
    global.selectedGameDate.getFullYear()
  );
};

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isUpdateAvailable: false,
      release_notes: [],
      isLoggedIn: false,
      loading: true,
      dataRefreshed: false,
      appState: AppState.currentState
    };
  }
  componentDidMount() {
    // optional: add listeners for CleverTap Events
    CleverTap.addListener(CleverTap.CleverTapProfileDidInitialize, event => {
      this._handleCleverTapEvent(
        CleverTap.CleverTapProfileDidInitialize,
        event
      );
    });
    CleverTap.addListener(CleverTap.CleverTapProfileSync, event => {
      this._handleCleverTapEvent(CleverTap.CleverTapProfileSync, event);
    });
    CleverTap.addListener(
      CleverTap.CleverTapInAppNotificationDismissed,
      event => {
        this._handleCleverTapEvent(
          CleverTap.CleverTapInAppNotificationDismissed,
          event
        );
      }
    );

    // for iOS only: register for push notifications
    CleverTap.registerForPush();

    // for iOS only; record a Screen View
    // CleverTap.recordScreenView('HomeView');

    // Listener to handle incoming deep links
    Linking.addEventListener("url", this._handleOpenUrl);

    // this handles the case where a deep link launches the application
    Linking.getInitialURL()
      .then(url => {
        if (url) {
          console.log("launch url", url);
          this._handleOpenUrl({ url }, true);
        }
      })
      .catch(err => console.log("launch url error", err));

    // check to see if CleverTap has a launch deep link
    // handles the case where the app is launched from a push notification containing a deep link
    CleverTap.getInitialUrl((err, url) => {
      if (url) {
        console.log("CleverTap launch url", url);
        this._handleOpenUrl({ url }, "CleverTap");
      } else if (err) {
        console.log("CleverTap launch url", err);
      }
    });

    AppState.addEventListener("change", this._handleAppStateChange);

    this.callUpdateCheck();
  }
  callUpdateCheck() {
    async_storage.getToken().then(token => {
      if (token) {
        global.token = token;
      }
      async_storage.isLoggedIn().then(loginStatus => {
        async_storage.getLastOpenedDate().then(date => {
          if (loginStatus === "1" && UserInfoService.findFirst()) {
            this.checkAppStatus();
            if (
              !date ||
              Utils.dateDiffInDays(new Date(parseInt(date)), new Date()) != 0
            ) {
              NavigationService.resetScreen("SplashScreen");
            }
          }
        });
      });
    });
  }
  componentWillUnmount() {
    // clean up listeners
    Linking.removeEventListener("url", this._handleOpenUrl);
    try {
      CleverTap.removeListeners();
    } catch (e) { }

    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      this.callUpdateCheck();
    }
    this.setState({ appState: nextAppState });
  };

  _handleOpenUrl(event, doNotOpenC360) {
    if (event != null && event.url != null) {
      var leadIdPattern = /lead_id=[0-9]+/i;
      let leadId = event.url.match(leadIdPattern);
      if (leadId && leadId.length > 0) {
        let redirectLeadId = leadId[0].split("=")[1];
        console.error(redirectLeadId);
        global.OPEN_C360_WITH_DEEP_LINK = true;
        global.OPEN_C360_WITH_DEEP_LINK_LEAD_ID = redirectLeadId;
        if (!doNotOpenC360) {
          NavigationService.navigate("SplashScreen");
        }
      }
    }
  }

  static _handleCleverTapEvent(eventName, event) {
    console.log("handleCleverTapEvent", eventName, event);
  }

  checkAppStatus() {
    new RestClient()
      .checkAppStatus({
        ios: Platform.OS === "ios",
        version: DeviceInfo.getVersion()
      })
      .then(data => {
        if (data.statusCode === 4001 || data.statusCode === 5001) {
          return;
        }
        if (data.result && data.result.here_maps_credentials) {
          async_storage.setHereMapAppId(data.result.here_maps_credentials.app_id);
          async_storage.setHereMapAppCode(data.result.here_maps_credentials.app_code);
          global.HERE_MAP_APP_ID = data.result.here_maps_credentials.app_id;
          global.HERE_MAP_APP_CODE = data.result.here_maps_credentials.app_code;
        }
        if (data.result && data.result.force_update === 1) {
          // NavigationService.resetScreen("ForceUpdate", {
          //   release_notes: data.result.release_notes
          // });
        }
      })
      .catch(error => { });
  }
  render() {
    return (
      <TopLevelNavigator
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
