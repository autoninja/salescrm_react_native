package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bharath on 10/4/17.
 */
public class ValidationObject extends RealmObject{

    private String message;

    private Integer id;

    private String dataType;

    private Integer max_val;

    private Integer min_val;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Integer getId ()
    {
        return id;
    }

    public void setId (Integer id)
    {
        this.id = id;
    }

    public String getDataType ()
    {
        return dataType;
    }

    public void setDataType (String dataType)
    {
        this.dataType = dataType;
    }

    public Integer getMax_val ()
    {
        return max_val;
    }

    public void setMax_val (Integer max_val)
    {
        this.max_val = max_val;
    }

    public Integer getMin_val ()
    {
        return min_val;
    }

    public void setMin_val (Integer min_val)
    {
        this.min_val = min_val;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", id = "+id+", dataType = "+dataType+", max_val = "+max_val+", min_val = "+min_val+"]";
    }
}