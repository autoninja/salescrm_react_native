package com.salescrm.telephony.db.car;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AdditionalCarsDetails extends RealmObject {

    @PrimaryKey
    private String lead_additional_car_id;

    private long leadId;
    private String name;
    private String model_id;
    private String price_quoted;
    private String market_price;
    private String expected_price;
    private String purchase_date;
    private String reg_no;
    private String kms_run;

    public String getLead_additional_car_id() {
        return lead_additional_car_id;
    }

    public void setLead_additional_car_id(String lead_additional_car_id) {
        this.lead_additional_car_id = lead_additional_car_id;
    }

    public long getLeadId() {
        return leadId;
    }

    public void setLeadId(long leadId) {
        this.leadId = leadId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public String getPrice_quoted() {
        return price_quoted;
    }

    public void setPrice_quoted(String price_quoted) {
        this.price_quoted = price_quoted;
    }

    public String getMarket_price() {
        return market_price;
    }

    public void setMarket_price(String market_price) {
        this.market_price = market_price;
    }

    public String getExpected_price() {
        return expected_price;
    }

    public void setExpected_price(String expected_price) {
        this.expected_price = expected_price;
    }

    public String getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(String purchase_date) {
        this.purchase_date = purchase_date;
    }

    public String getReg_no() {
        return reg_no;
    }

    public void setReg_no(String reg_no) {
        this.reg_no = reg_no;
    }

    public String getKms_run() {
        return kms_run;
    }

    public void setKms_run(String kms_run) {
        this.kms_run = kms_run;
    }
}
