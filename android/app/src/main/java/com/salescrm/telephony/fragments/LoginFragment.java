package com.salescrm.telephony.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.MobileAppDataDB;
import com.salescrm.telephony.interfaces.AutoDialogClickListener;
import com.salescrm.telephony.model.AutoDialogModel;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.MobileAppDataResponse;
import com.salescrm.telephony.response.OtpRequestResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;
import com.salescrm.telephony.views.AutoDialog;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import io.realm.Realm;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Ravindra P on 16-03-2016.
 */
public class LoginFragment extends Fragment implements Callback, AutoDialogClickListener {

    private View mrootView;

    private Preferences pref = null;
    private String imei;
    private Realm realm;
    public static String dummyIMEI;
    //public static String dummyOTPToken;
    static  MobileAppDataResponse    mMobileAppDataResponse;
    private FrameLayout frameLoader;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int GALLERY_IMAGE_REQUEST_CODE = 200;
    private final String IMAGE_NAME = "img_profile.jpg";
    private final String TXT_FILE_NAME = "clientCode.txt";

   // public static String dummyOtpResponse;


    private PackageInfo pInfo;
    private String version;
    private int verCode;
    private Uri uriFile = null;

    private AppCompatEditText etMobile;
    private Button btLogin;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        mrootView = rootView;
        pref = Preferences.getInstance();
        pref.load(getActivity());
        realm = Realm.getDefaultInstance();

        frameLoader = (FrameLayout) rootView.findViewById(R.id.login_frame_loader);
        btLogin = rootView.findViewById(R.id.bt_login);

        etMobile = rootView.findViewById(R.id.et_login_mobile_no);

       // progressBarImageLoading = (ProgressBar) rootView.findViewById(R.id.progressBarImageLoading);

        if(pref.getFcmId()== null || pref.getFcmId().equalsIgnoreCase("")) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            pref.setFcmId(refreshedToken);
            pref.setFcmSync(1);
        }

        //etMobile.setText(getMy10DigitPhoneNumber());

        /*if (!readFromTxtFile().equalsIgnoreCase("")) {
            String clientCode = readFromTxtFile().trim();
            etdealerid.setText(clientCode);
            callApiToGetImageUrl(pref.getImeiNumber(), clientCode);
        }*/

        pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        verCode = pInfo.versionCode;
        version = pInfo.versionName;


/*
        imageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                final CharSequence imageArray[] = { "Camera", "Gallery" };
                builder.setTitle("Upload From:");
                builder.setItems(imageArray, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        Intent iIntent;
                        if (imageArray[position].equals("Camera")) {
                            iIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            uriFile = getOutputMediaFileUri();
                            iIntent.putExtra(MediaStore.EXTRA_OUTPUT, getOutputMediaFileUri());
                            startActivityForResult(iIntent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                        }else if(imageArray[position].equals("Gallery")){
                            iIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            iIntent.setType("image/*");
                            startActivityForResult(iIntent, GALLERY_IMAGE_REQUEST_CODE);
                        }
                    }
                });
                builder.show();
            }
        });

        imageViewProfile.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(getActivity(), EtvbrImgDialog.class);
                Bundle bundle = new Bundle();
                bundle.putString("imgUrlEtvbr", pref.getDpUri());
                bundle.putString("dseNameEtvbr", pref.getDpUserName());
                intent.putExtras(bundle);
                getActivity().startActivity(intent);
                return true;
            }
        });*/

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.HideKeyboard(getContext());
                String mobileNo = etMobile.getText().toString();
                if(Util.isNotNull(mobileNo) && mobileNo.length()==10) {
                    if(new ConnectionDetectorService(getContext()).isConnectingToInternet()) {
                        login();
                    }
                    else {
                        showAlert("No Internet Connections");
                    }
                }
                else {
                    showAlert("Please enter a valid mobile number");
                }
            }
        });
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("Hello onActivityResult: "+ data);
        if(resultCode == RESULT_OK) {
            Bitmap bitmap;
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                pref.setDpUri(uriFile.toString());
               // setImageUrl(pref.getDpUri());
                try {
                    bitmap = BitmapFactory.decodeFile(Uri.parse(uriFile.toString()).getPath());
                    pref.setEncodedUri(encodeImage(bitmap));
                    pref.setDpTakenFromDevice(true);
                }catch (OutOfMemoryError e){
                    e.printStackTrace();
                }
            } else if (requestCode == GALLERY_IMAGE_REQUEST_CODE) {
                //uriFile = data.getData();
                pref.setDpUri(data.getData().toString());
                //setImageUrl(pref.getDpUri());
                InputStream imageStream = null;
                try {
                    imageStream = getActivity().getContentResolver().openInputStream(Uri.parse(pref.getDpUri()));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    bitmap = BitmapFactory.decodeStream(imageStream);
                    pref.setEncodedUri(encodeImage(bitmap));
                    pref.setDpTakenFromDevice(true);
                }catch (OutOfMemoryError e){
                    e.printStackTrace();
                }
            }
        }
    }

    private String encodeImage(Bitmap bm)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,30,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    private String readFromTxtFile() {
        StringBuilder text = new StringBuilder();
        try {
            File sdcard = new File(Environment.getExternalStorageDirectory(), WSConstants.MY_SALES_DIRECTORY);
            if(sdcard.exists()) {
                File file = new File(sdcard, TXT_FILE_NAME);

                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                br.close();
                return text.toString();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

/*    private void setImageUrl(String dpUri) {
                imageViewProfile.setVisibility(View.GONE);
                //progressBarImageLoading.setVisibility(View.VISIBLE);
                Picasso.with(getActivity()).load(Uri.parse(dpUri)).memoryPolicy(MemoryPolicy.NO_CACHE).
                        networkPolicy(NetworkPolicy.NO_CACHE).placeholder(R.drawable.ic_person_white_48dp).
                        error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).into(imageViewProfile, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        imageViewProfile.setVisibility(View.VISIBLE);
                        //progressBarImageLoading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        imageViewProfile.setVisibility(View.VISIBLE);
                        //progressBarImageLoading.setVisibility(View.GONE);
                    }
                });
    }*/

    /*private void callApiToGetImageUrl(String imeiNumber, String clientCode) {
        ApiUtil.GetRestApiForCommon().getUserProfileImage(imeiNumber, clientCode, new Callback<ProfileImage>() {
            @Override
            public void success(final ProfileImage profileImage, Response response) {
                if(profileImage.getResult()!= null){
                    if(profileImage.getResult().getImage() != null && !profileImage.getResult().getImage().equalsIgnoreCase("")) {
                        pref.setDpUri(profileImage.getResult().getImage());
                        pref.setDpUserName(profileImage.getResult().getUserName());
                        if(getActivity() != null && pref.getDpUri() != null && !pref.getDpUri().equalsIgnoreCase("")) {
                            setImageUrl(pref.getDpUri());
                            *//*etUserName.setText(pref.getDpUserName());
                            etUserName.setFocusable(false);
                            etUserName.setClickable(false);
                            btnSignin.setEnabled(true);*//*
                        }else {
                            //btnSignin.setEnabled(false);
                        }
                        pref.setDpTakenFromDevice(false);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println("Success Brother NO"+ error.getMessage());
            }
        });

    }*/

    public Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    private File getOutputMediaFile() {
        if(isExternalStorageAvailable()) {
            File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), WSConstants.MY_SALES_DIRECTORY);
            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            File mediaFile = new File(mediaStorageDir.getPath() + File.separator + IMAGE_NAME);
            return mediaFile;
        }else{
            return null;
        }
    }

    private boolean isExternalStorageAvailable () {
        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }

    private void storeClientCodeForever(String clientCode) {
        if(isExternalStorageAvailable()){
            File mainDir = new File(Environment.getExternalStorageDirectory(), WSConstants.MY_SALES_DIRECTORY);
            if(!mainDir.exists()){
                mainDir.mkdirs();
            }
            File filDir = new File(mainDir.getPath() + File.separator + TXT_FILE_NAME);
            try {
                FileOutputStream outStream = new FileOutputStream(filDir);
                outStream.write(clientCode.getBytes());
                outStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void init() {
        if(realm.where(MobileAppDataDB.class).findAll().size()==0){
            if(new ConnectionDetectorService(getContext()).isConnectingToInternet()){
                //getDealerDetails();
            }
            else {
                new AutoDialog(getContext(),this,new AutoDialogModel("Internet required to fetch dealer information","Retry","Close")).show();
            }

        }
        else {
            hideLoader();
        }
    }


    protected void login() {
        frameLoader.setVisibility(View.VISIBLE);
        Util.HideKeyboard(getActivity());
        String deviceID = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        String osVersion = android.os.Build.VERSION.RELEASE;
        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getApplicationContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        int verCode = pInfo.versionCode;
       // pref.setUserMobile(etPassword.getText().toString());
        ConnectionDetectorService cd = new ConnectionDetectorService(getActivity());
        if (cd.isConnectingToInternet()) {
            //System.out.println("LoginFragment:"Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID));
            //ApiUtil.GetRestApi().LoginAndGetOtp("sangeetha_crm", "demo", "123451234554322", this); //Ravindra
            // ApiUtil.GetRestApi().LoginAndGetOtp("test_telephony", "demo", "911436253703924", this);  // Bharath and akshata
            btLogin.setClickable(false);
            frameLoader.setVisibility(View.VISIBLE);

           /*
            MobileAppDataDB mobileAppDataDB = realm.where(MobileAppDataDB.class)
                    .equalTo("dealerCode", dealerCode)
                    .findFirst();
            if(mobileAppDataDB != null) {
                Log.e("mobileAppDataResponse","mobileAppDataResponse "+mobileAppDataDB.getDealerName());
                pref.setDealerName(mobileAppDataDB.getDealerName());
                pref.setDealerId(mobileAppDataDB.getDealerCode());
            }

            pref.setDealerId( etdealerid.getText().toString());
            storeClientCodeForever(pref.getDealerId().trim());*/
            pref.setDeviceId(deviceID);
            ApiUtil.GetRestApiForCommon().login(etMobile.getText().toString(), deviceID, this);

        } else {
            showAlert("No Internet Connection");
        }

    }


    @Override
    public void failure(RetrofitError arg0) {
        System.out.println("Error:" + arg0.toString());
        btLogin.setClickable(true);
        frameLoader.setVisibility(ProgressBar.GONE);
        //SalesCRMApplication.getCleverTapAPI().event.push("Mob Login failure");
        showAlert("Error reaching the server");
    }

    @Override
    public void success(Object arg0, Response arg1) {
        btLogin.setClickable(true);
        frameLoader.setVisibility(View.GONE);
        final OtpRequestResponse loginResponse = (OtpRequestResponse) arg0;
        if (loginResponse!=null
                && loginResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)
                && loginResponse.getResult()!=null
                && loginResponse.getResult().isSuccess()
        ) {
            pref.setUserMobile(etMobile.getText().toString());
            SalesCRMApplication.getBus().post(loginResponse);

            /*
            * pref.setUserId(loginResponse.getResult().getInfo().getMessage()[0].getId());
                System.out.println("MAP ID"+loginResponse.getResult().getInfo().getMessage()[0].getId());
                System.out.println("MAP ID FROM PREF"+pref.getUserId());
                pref.setDealerName(loginResponse.getResult().getInfo().getMessage()[0].getDealer());
                pref.setUserMobile(loginResponse.getResult().getInfo().getMessage()[0].getMobile());
                pref.setAccessToken(loginResponse.getResult().getToken());
                pref.setUserName(loginResponse.getResult().getInfo().getMessage()[0].getUser_name());
                pref.setPassword(etPassword.getText().toString());
                pref.setEmail(loginResponse.getResult().getInfo().getMessage()[0].getEmail());
                pref.setName(loginResponse.getResult().getInfo().getMessage()[0].getName());
                if(loginResponse.getResult().getInfo().getMessage().length > 0){
                    getUserProfile(loginResponse.getResult().getInfo().getMessage()[0].getName(),
                            loginResponse.getResult().getInfo().getMessage()[0].getId(),
                            loginResponse.getResult().getInfo().getMessage()[0].getMobile(),
                            loginResponse.getResult().getInfo().getMessage()[0].getDealer(),
                            loginResponse.getResult().getInfo().getMessage()[0].getEmail(),
                            loginResponse.getResult().getInfo().getMessage()[0].getUser_name());
                }*/

        } else if(loginResponse!=null && Util.isNotNull(loginResponse.getMessage())){
            showAlert(loginResponse.getMessage());
        }
        else {
            showAlert("Error in login");
        }
    }


    public void getUserProfile(String name, String id, String mobile, String dealer, String mail, String userName){
        HashMap<String, Object> profileUpdate = new HashMap<String, Object>();
        profileUpdate.put("Name", name);
        profileUpdate.put("Identity", dealer+"_"+userName);
        profileUpdate.put("Phone", mobile);
        profileUpdate.put("Email", mail);
        profileUpdate.put("Dealer", dealer);
        profileUpdate.put("UserId", id);
        profileUpdate.put("MSG-email", false);
        profileUpdate.put("MSG-push", true);
        profileUpdate.put("MSG-sms", true);
        //SalesCRMApplication.getCleverTapAPI().profile.push(profileUpdate);
    }
    @Override
    public void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Clear out all instances.
        //realm.close();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        realm.close();
    }

    public void showAlert(String message) {
        Snackbar snackbar = Snackbar.make(mrootView, message, Snackbar.LENGTH_LONG)
                .setAction("Close", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();

    }

    @Override
    public void onAutoDialogYesButtonClick(View view, AutoDialogModel data) {
      //  init();
    }

    @Override
    public void onAutoDialogNoButtonClick(View view, AutoDialogModel data) {
        getActivity().finish();
    }

    private abstract class DoubleClickListener implements View.OnClickListener {

        private static final long DOUBLE_CLICK_TIME_DELTA = 300;//milliseconds

        long lastClickTime = 0;

        @Override
        public void onClick(View v) {
            long clickTime = System.currentTimeMillis();
            if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                onDoubleClick(v);
            } else {
                onSingleClick(v);
            }
            lastClickTime = clickTime;
        }

        public abstract void onSingleClick(View v);

        public abstract void onDoubleClick(View v);
    }

  /*  void getDealerDetails(){

        pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        verCode = pInfo.versionCode;
        version = pInfo.versionName;

        pref.setVersionCode(verCode);
        pref.setVersionName(version);

        ApiUtil.GetCommonRestApi().getDealerCodes(version, "1", new Callback<MobileAppDataResponse>() {
            @Override
            public void success(MobileAppDataResponse mobileAppDataResponse, Response response) {

                if (mobileAppDataResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    mMobileAppDataResponse = mobileAppDataResponse;

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            addDataTODB(realm,mMobileAppDataResponse);

                        }
                    });
                    hideLoader();

                }
                else {
                    new AutoDialog(getContext(),LoginFragment.this,new AutoDialogModel("New Update of the app is available, Please update the app.","Retry","Close")).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                new AutoDialog(getContext(),LoginFragment.this,new AutoDialogModel("Failed to fetch dealer information","Retry","Close")).show();
            }
        });
    }*/

    private void hideLoader() {
        frameLoader.setVisibility(View.GONE);
    }


   /* private void addDataTODB(Realm realm,MobileAppDataResponse mobileAppDataResponse) {

        MobileAppDataDB mobileAppDataDB = new MobileAppDataDB();
        for(int i=0;i<mobileAppDataResponse.getResult().size();i++){
            Log.e("mobileAppDataResponse","mobileAppDataResponse "+mobileAppDataResponse.getResult().get(i).getName());
            mobileAppDataDB.setDealerCode(mobileAppDataResponse.getResult().get(i).getCode());
            mobileAppDataDB.setDealerName(mobileAppDataResponse.getResult().get(i).getName());
            realm.copyToRealmOrUpdate(mobileAppDataDB);
        }


    }
*/

    private String getMyPhoneNumber(){
        try{
            TelephonyManager mTelephonyMgr;
            mTelephonyMgr = (TelephonyManager)
                    getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            String number = mTelephonyMgr.getLine1Number();
            if(number!=null && number.length()==10) {
                return number;
            }

        }
        catch (Exception e) {

        }
        return "";

    }

    private String getMy10DigitPhoneNumber(){
        String s = getMyPhoneNumber();
        return s != null && s.length() > 2 ? s.substring(2) : null;
    }
}
