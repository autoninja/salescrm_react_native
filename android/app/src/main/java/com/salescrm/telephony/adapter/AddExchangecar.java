package com.salescrm.telephony.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.salescrm.telephony.db.UserCarBrand;
import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandsDB;
import com.salescrm.telephony.db.car.ExchangeCarBrandsDB;
import com.salescrm.telephony.fragments.AddExchangecarFragment;
import com.salescrm.telephony.fragments.AddcarFragment;
import com.salescrm.telephony.response.CarBrandResponse;
import com.salescrm.telephony.utils.Util;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

/**
 * Created by akshata on 27/5/16.
 */
public class AddExchangecar  extends FragmentPagerAdapter {
    int mNumOfTabs;
    private RealmResults<CarBrandsDB> userCarBrand;
    private RealmResults<ExchangeCarBrandsDB> exchangeCarBrandsDB;
    public AddExchangecar(FragmentManager fm, int mNumOfTabs, RealmResults<CarBrandsDB> userCarBrand, RealmResults<ExchangeCarBrandsDB> exchnCarBrandsDB) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
        this.userCarBrand = userCarBrand;
        this.exchangeCarBrandsDB = exchnCarBrandsDB;
    }


    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return AddcarFragment.newInstance(userCarBrand);

                case 1:
                    return AddExchangecarFragment.newInstance(exchangeCarBrandsDB);

                default:
                    return null;
            }
        }



}





