import React, { Component } from "react";
import {
  View,
  Text,
  ActivityIndicator,
  ScrollView,
  NativeModules,
  DeviceEventEmitter,
  Platform
} from "react-native";
import Geolocation from '@react-native-community/geolocation';
import CustomTextInput from "../../../components/createLead/custom_text_input";
import ItemPicker from "../../../components/createLead/item_picker";
import PickerList from "../../../utils/PickerList";
import Button from "../../../components/uielements/Button";
import MapView from "react-native-maps";
import { Marker } from "react-native-maps";

import RestClient from "../../../network/rest_client";

import styles from "./ColdVisit.styles";
import OTPInput from "../../../components/uielements/OTPInput";
import Loader from "../../../components/uielements/Loader";
import SuccessView from "../../../components/uielements/SuccessView";
import NavigationService from '../../../navigation/NavigationService'

export default class ColdVisitForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileNumber: Platform.OS == "android" ? this.props.mobileNumber : this.props.navigation.getParam('mobileNumber'),
      customerName: null,
      selectedLocation: this.autoSelectLocation(),
      locations: this.props.userLocations,
      remarks: null,
      mapRegion: null,
      lastLat: null,
      lastLong: null,
      currentLocationHolder: "Getting your location, please wait",
      addressFetchingFromLatLong: true,
      isReverseAddressValid: false,
      otp: "",
      loading: false,
      showItemPicker: false,
      dataItemPicker: {}
    };
  }
  autoSelectLocation() {
    if (this.props.userLocations && this.props.userLocations.length == 1) {
      return this.props.userLocations[0];
    }
    return null;
  }
  componentDidMount() {
    if (this.props.fromAndroid) {
      NativeModules.ReactNativeToAndroid.componentDidMount("COLD_VISIT");
    }
    if (Platform.OS == "ios") {
      Geolocation.getCurrentPosition(
        position => {
          let region = {
            latitude: parseFloat(position.coords.latitude),
            longitude: parseFloat(position.coords.longitude),
            latitudeDelta: 0.00922 * 1.5,
            longitudeDelta: 0.00421 * 1.5
          }
          this.onRegionChange(region, region.latitude, region.longitude);
          this.requestAddressFromLatLong(region.latitude + ',' + region.longitude)
        },
        error => Alert.alert('Error Getting the location, please enable the permission'),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
      );
    }
    else {
      this.subscription = DeviceEventEmitter.addListener(
        "onGettingLocationColdVisit",
        function (e: Event) {
          if (e) {
            let region = {
              latitude: parseFloat(e.LATITUDE),
              longitude: parseFloat(e.LONGITUDE),
              latitudeDelta: 0.00922 * 1.5,
              longitudeDelta: 0.00421 * 1.5
            };
            this.onRegionChange(region, region.latitude, region.longitude);
            this.requestAddressFromLatLong(
              region.latitude + "," + region.longitude
            );
          }
        }.bind(this)
      );
    }

  }
  componentWillUnmount() {
    //navigator.geolocation.clearWatch(this.watchID);
    if (this.props.fromAndroid) {
      NativeModules.ReactNativeToAndroid.componentWillUnmount("COLD_VISIT");
    }
  }
  onRegionChange(region, lastLat, lastLong) {
    this.setState({
      mapRegion: region,
      // If there are no new values set the current ones
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong
    });
  }

  requestAddressFromLatLong(latlong) {
    this.setState({
      currentLocationHolder: "Getting your location, please wait",
      addressFetchingFromLatLong: true,
      isReverseAddressValid: false
    });
    let params = {
      appId: this.props.HERE_MAP_APP_ID,
      appCode: this.props.HERE_MAP_APP_CODE,
      CSVLatLong: latlong
    };
    new RestClient()
      .getReverseGeoCode(params)
      .then(data => {
        if (
          data &&
          data.Response &&
          data.Response.View &&
          data.Response.View.length > 0 &&
          data.Response.View[0] &&
          data.Response.View[0].Result &&
          data.Response.View[0].Result.length > 0 &&
          data.Response.View[0].Result[0] &&
          data.Response.View[0].Result[0].Location &&
          data.Response.View[0].Result[0].Location.Address &&
          data.Response.View[0].Result[0].Location.Address.Label
        ) {
          let currentLocationHolder =
            data.Response.View[0].Result[0].Location.Address;
          this.setState({
            currentLocationHolder:
              currentLocationHolder.Label +
              ", " +
              currentLocationHolder.PostalCode,
            addressFetchingFromLatLong: false,
            isReverseAddressValid: true
          });
        } else {
          this.setState({
            currentLocationHolder: "Failed to get the address, Tap to retry",
            addressFetchingFromLatLong: false
          });
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({
          currentLocationHolder: "Failed to get the address, Tap to retry",
          addressFetchingFromLatLong: false
        });
      });
  }
  showAlert(alert) {
    if (this.props.fromAndroid) {
      NativeModules.ReactNativeToAndroid.showToast(alert);
    }
  }
  requestOTP() {
    this.setState({ loading: true });
    new RestClient()
      .getColdVisitGenerateOTP(this.state.mobileNumber)
      .then(data => {
        if (data) {
          if (data.statusCode == "2002" && data.result) {
            this.setState({ loading: false, otpRequested: true, otp: "" });
            this.showAlert("OTP has been sent to " + this.state.mobileNumber);
          } else if (data.message) {
            this.setState({ loading: false });
            this.showAlert(data.message);
          } else {
            this.setState({ loading: false });
            this.showAlert("Failed to send OTP! Try Again");
          }
        } else {
          this.setState({ loading: false });
          this.showAlert("Failed to send OTP! Try Again");
        }
      })
      .catch(error => {
        this.setState({ loading: false });
        this.showAlert("Failed to send OTP! Try Again");
      });
  }
  close() {
    if (Platform.OS == "android") {
      NativeModules.ReactNativeToAndroid.onColdVisitAdded();
    }
    else {
      NavigationService.resetScreen('Home');
    }
  }
  addColdVisit() {
    this.setState({ loading: true });
    let {
      mobileNumber,
      selectedLocation,
      customerName,
      remarks,
      lastLat,
      lastLong,
      otp,
      isReverseAddressValid,
      currentLocationHolder
    } = this.state;
    let input = {
      location_id: selectedLocation.id,
      customer_name: customerName,
      customer_address: isReverseAddressValid ? currentLocationHolder : "",
      latitude: lastLat,
      longitude: lastLong,
      remarks: remarks,
      mobile_number: mobileNumber,
      otp: otp
    };
    new RestClient()
      .postColdVisitSubmit(input)
      .then(data => {
        if (data) {
          if (data.statusCode == "2002" && data.result) {
            this.setState({ loading: false, isFormSubmitSuccess: true });
            setTimeout(
              function () {
                this.close();
              }.bind(this),
              1000
            );
            this.showAlert("Cold visit created!!");
          } else if (data.message) {
            this.setState({ loading: false });
            this.showAlert(data.message);
          } else {
            this.setState({ loading: false });
            this.showAlert("Failed to create cold visit! Try Again");
          }
        } else {
          this.setState({ loading: false });
          this.showAlert("Failed to create cold visit! Try Again");
        }
      })
      .catch(error => {
        console.error(error);
        this.setState({ loading: false });
        this.showAlert("Failed to create cold visit! Try Again");
      });
  }

  showPicker(data) {
    this.setState({ showItemPicker: true, dataItemPicker: data });
  }
  onSelect = (fromId, item, payload) => {
    this.setState({
      selectedLocation: item,
      showItemPicker: false
    });
  };
  onCancel = () => {
    this.setState({
      showItemPicker: false
    });
  };
  isFormValid() {
    let {
      selectedLocation,
      customerName,
      remarks,
      lastLat,
      lastLong
    } = this.state;

    let isFormValid = false;
    isFormValid = this.state.otp && this.state.otp.length == 4 ? true : false;
    if (
      customerName &&
      !customerName.isEmpty() &&
      remarks &&
      !remarks.isEmpty()
    ) {
      isFormValid = isFormValid && true;
    } else {
      isFormValid = false;
    }
    isFormValid =
      isFormValid && selectedLocation && !selectedLocation.text.isEmpty()
        ? true
        : false;

    isFormValid = isFormValid && lastLat && lastLong ? true : false;

    return isFormValid;
  }
  render() {
    let currentLocationHolder = this.state.currentLocationHolder;
    let locationSelected = "Select Location";
    let {
      mobileNumber,
      showItemPicker,
      dataItemPicker,
      locations,
      selectedLocation,
      customerName,
      remarks
    } = this.state;
    if (selectedLocation && selectedLocation.text) {
      locationSelected = selectedLocation.text;
    }
    return (
      <View style={[styles.main, { padding: 0, paddingBottom: 20 }]}>
        <PickerList
          showFilter={false}
          visible={showItemPicker}
          onSelect={this.onSelect}
          onCancel={this.onCancel}
          data={dataItemPicker}
        />
        <ScrollView>
          <View style={styles.main}>
            <CustomTextInput
              editable={false}
              selectTextOnFocus={false}
              showPlaceHolder={true}
              header="Customer Number"
              text={mobileNumber}
            />

            <CustomTextInput
              showPlaceHolder={true}
              header="Customer Name"
              hint="Enter customer name"
              text={customerName}
              onInputTextChanged={(id, text, payload) => {
                this.setState({ customerName: text });
              }}
            />
            <ItemPicker
              title={locationSelected}
              onClick={this.showPicker.bind(this, {
                title: "Location",
                fromId: "user_location",
                listItems: locations
              })}
            />

            <CustomTextInput
              showPlaceHolder={true}
              header="Remarks"
              hint="Add remarks"
              text={remarks}
              onInputTextChanged={(id, text, payload) => {
                this.setState({ remarks: text });
              }}
            />

            <View style={styles.mapViewHolder}>
              <View style={{ flexDirection: "row" }}>
                {this.state.addressFetchingFromLatLong && (
                  <ActivityIndicator
                    style={{ marginLeft: 10 }}
                    size="small"
                    color="#007fff"
                  />
                )}

                <Text style={styles.mapViewLocation}>
                  {currentLocationHolder}
                </Text>
              </View>
              <MapView
                liteMode={true}
                style={styles.mapView}
                region={this.state.mapRegion}
              >
                {this.state.lastLat && (
                  <Marker
                    coordinate={{
                      latitude: this.state.lastLat,
                      longitude: this.state.lastLong
                    }}
                  />
                )}
              </MapView>
            </View>
          </View>
        </ScrollView>
        {this.state.otpRequested && (
          <View>
            <OTPInput
              onOTPEntered={otp => {
                this.setState({ otp });
              }}
            />
            <Button
              disabled={!this.isFormValid()}
              onPress={() => this.addColdVisit()}
              title="Add Cold Visit"
            />
          </View>
        )}
        {!this.state.otpRequested && (
          <Button onPress={() => this.requestOTP()} title="Request OTP" />
        )}
        {this.state.loading && <Loader isLoading={this.state.loading} />}
        {this.state.isFormSubmitSuccess && (
          <SuccessView text="Cold Visit Added" />
        )}
      </View>
    );
  }
}

String.prototype.isEmpty = function () {
  return this.length === 0 || !this.trim();
};
