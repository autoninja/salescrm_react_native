package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by prateek on 29/8/17.
 */

public class ScoreboardDseDb extends RealmObject{

    private String retail_target;

    private String visits_target;

    private String bookings_target;

    private String location;

    private String sales_manager;

    private String dp_url;

    private String user_name;

    private String tdrives;

    private String bookings;

    private String enquiries_target;

    private String user_id;

    private String enquiries;

    private String tdrives_target;

    private String visits;

    private String retail;

    public String getRetail_target ()
    {
        return retail_target;
    }

    public void setRetail_target (String retail_target)
    {
        this.retail_target = retail_target;
    }

    public String getVisits_target ()
    {
        return visits_target;
    }

    public void setVisits_target (String visits_target)
    {
        this.visits_target = visits_target;
    }

    public String getBookings_target ()
    {
        return bookings_target;
    }

    public void setBookings_target (String bookings_target)
    {
        this.bookings_target = bookings_target;
    }

    public String getLocation ()
    {
        return location;
    }

    public void setLocation (String location)
    {
        this.location = location;
    }

    public String getSales_manager ()
    {
        return sales_manager;
    }

    public void setSales_manager (String sales_manager)
    {
        this.sales_manager = sales_manager;
    }

    public String getDp_url ()
    {
        return dp_url;
    }

    public void setDp_url (String dp_url)
    {
        this.dp_url = dp_url;
    }

    public String getUser_name ()
    {
        return user_name;
    }

    public void setUser_name (String user_name)
    {
        this.user_name = user_name;
    }

    public String getTdrives ()
    {
        return tdrives;
    }

    public void setTdrives (String tdrives)
    {
        this.tdrives = tdrives;
    }

    public String getBookings ()
    {
        return bookings;
    }

    public void setBookings (String bookings)
    {
        this.bookings = bookings;
    }

    public String getEnquiries_target ()
    {
        return enquiries_target;
    }

    public void setEnquiries_target (String enquiries_target)
    {
        this.enquiries_target = enquiries_target;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getEnquiries ()
    {
        return enquiries;
    }

    public void setEnquiries (String enquiries)
    {
        this.enquiries = enquiries;
    }

    public String getTdrives_target ()
    {
        return tdrives_target;
    }

    public void setTdrives_target (String tdrives_target)
    {
        this.tdrives_target = tdrives_target;
    }

    public String getVisits ()
    {
        return visits;
    }

    public void setVisits (String visits)
    {
        this.visits = visits;
    }

    public String getRetail ()
    {
        return retail;
    }

    public void setRetail (String retail)
    {
        this.retail = retail;
    }
}
