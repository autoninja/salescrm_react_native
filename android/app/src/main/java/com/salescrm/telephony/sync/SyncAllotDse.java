package com.salescrm.telephony.sync;

import android.content.Context;
import android.widget.Toast;

import com.salescrm.telephony.offline.AutoFetchFormData;
import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.db.AllotDseDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.interfaces.AutoFetchFormListener;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.interfaces.SyncListener;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.GeneralResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by bharath on 13/12/16.
 */

public class SyncAllotDse implements FetchC360OnCreateLeadListener ,AutoFetchFormListener{

    public SyncListener listener;
    private Context context;
    private Preferences pref = null;
    private Realm realm;
    private SalesCRMRealmTable salesCRMRealmTable;
    private String TAG = "SyncAllotDse";
    private RealmResults<AllotDseDB> allotDseDBList;
    private String generatedLeadId="";

    public SyncAllotDse(SyncListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        //  this.context=context;
        this.pref = Preferences.getInstance();
        realm = Realm.getDefaultInstance();
        pref.load(context);
        allotDseDBList = realm.where(AllotDseDB.class).equalTo("is_synced",false).findAll();
    }
    public void sync(){

        if(allotDseDBList.size()>0){
            submitAllotDse(allotDseDBList.get(0));
        }
        else {
            listener.onAllotDseSync();
        }


    }


    private void submitAllotDse(final AllotDseDB allotDseDB) {
        if (new ConnectionDetectorService(context).isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(pref.getAccessToken()).allotDSE(allotDseDB.getLeadId() + "", allotDseDB.getLocationId()+"", allotDseDB.getDse_team_lead_id()+"", allotDseDB.getDseId()+"",allotDseDB.getManager_id()+"", allotDseDB.getLeadLastUpdated(), new Callback<GeneralResponse>() {
                @Override
                public void success(GeneralResponse generalResponse, Response response) {
                    Util.updateHeaders(response.getHeaders());
                    if (!generalResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                        System.out.println("Success:0" + generalResponse.getMessage());
                        updateSyncStatus(allotDseDB,false,generalResponse.getMessage());
                        //showAlert(validateOtpResponse.getMessage());
                    } else {
                        if (generalResponse.getResult().equalsIgnoreCase("1")) {
                            System.out.println("Success:1" + generalResponse.getMessage());
                            updateSyncStatus(allotDseDB,true,generalResponse.getMessage());
                        } else {
                            // relLoader.setVisibility(View.GONE);
                            updateSyncStatus(allotDseDB,false,generalResponse.getMessage());
                            System.out.println("Success:2" + generalResponse.getMessage());
                            //showAlert(validateOtpResponse.getMessage());
                        }
                    }


                }

                @Override
                public void failure(RetrofitError error) {
                    updateSyncStatus(allotDseDB,false,"");
                }
            });
        }
    }

    private void updateSyncStatus(final AllotDseDB allotDseDB, boolean b,String error) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                allotDseDB.setIs_synced(true);

            }
        });
        fetchC360(allotDseDB.getLeadId());
        Toast.makeText(context, "DSE Assigned and Synced for Lead Id:"+allotDseDB.getLeadId(), Toast.LENGTH_SHORT).show();
    }
    private void fetchC360(int leadId) {
        this.generatedLeadId = leadId+"";
        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(generatedLeadId);
        new FetchC360InfoOnCreateLead(this,context,leadData,pref.getAppUserId()).call(true);

    }
    @Override
    public void onFetchC360OnCreateLeadSuccess() {
        new AutoFetchFormData(this,context).call(realm.where(SalesCRMRealmTable.class)
                .equalTo("leadId",Util.getInt(generatedLeadId)).equalTo("isDone",false).findAll(),true);

    }

    @Override
    public void onFetchC360OnCreateLeadError() {
        sync();
    }
    @Override
    public void onAutoFormsDataFetched(boolean b, RealmResults<SalesCRMRealmTable> data) {
        sync();
    }
    @Override
    public void onAutoFormsDataError(RetrofitError error, int from) {
       sync();
    }
}
