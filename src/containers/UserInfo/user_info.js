import React, { Component } from "react";
import {
  Platform,
  View,
  Text,
  Image,
  TouchableOpacity,
  YellowBox,
  ScrollView,
  Switch,
  FlatList,
  ActivityIndicator
} from "react-native";

import styles from "../../styles/styles";

export default class UserInfo extends Component {
  constructor(props) {
    super(props);
    this.state = { showImagePlaceHolder: true, imageLoadError: false };

    YellowBox.ignoreWarnings([
      "Warning: componentWillMount is deprecated",
      "Warning: componentWillReceiveProps is deprecated"
    ]);
  }

  render() {
    let { navigation } = this.props;
    let name = navigation.getParam("name", "User Name");
    let dp_url = navigation.getParam("dp_url", "dp_url");
    if (!dp_url) {
      dp_url = "empty";
    }
    return (
      <View style={styles.MainContainerBlack}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-end",
            padding: 20
          }}
        >
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Image
              source={require("../../images/ic_close_white.png")}
              style={{ width: 24, height: 24 }}
            />
          </TouchableOpacity>
        </View>

        <ScrollView
          minimumZoomScale={1}
          maximumZoomScale={5}
          showsVerticalScrollIndicator={false}
        >
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            {this.state.imageLoadError && (
              <Image
                source={require("../../images/ic_user_default.png")}
                style={{ width: 96, height: 96, position: "absolute" }}
              />
            )}
            {this.state.showImagePlaceHolder && (
              <ActivityIndicator
                animating={this.state.showImagePlaceHolder}
                style={{ flex: 1, alignSelf: "center", position: "absolute" }}
                color="#fff"
                size="large"
                hidesWhenStopped={true}
              />
            )}
            <Image
              onError={() => this.setState({ imageLoadError: true })}
              onLoadEnd={() => this.setState({ showImagePlaceHolder: false })}
              source={{ uri: dp_url }}
              style={{ width: 500, height: 500 }}
            />
          </View>
        </ScrollView>

        <View style={{ alignItems: "center", padding: 10 }}>
          <Text style={{ fontSize: 24, color: "#fff" }}> {name}</Text>
        </View>
      </View>
    );
  }
}
