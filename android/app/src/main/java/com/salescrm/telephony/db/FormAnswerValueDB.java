package com.salescrm.telephony.db;

import io.realm.RealmObject;

/**
 * Created by bharath on 31/10/16.
 */

public class FormAnswerValueDB extends RealmObject {
    private String fAnsId;

    private String displayText;

    private String answerValue;

    public String getFAnsId() {
        return fAnsId;
    }

    public void setFAnsId(String fAnsId) {
        this.fAnsId = fAnsId;
    }

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public String getAnswerValue() {
        return answerValue;
    }

    public void setAnswerValue(String answerValue) {
        this.answerValue = answerValue;
    }

    @Override
    public String toString() {
        return "ClassPojo [fAnsId = " + fAnsId + ", displayText = " + displayText + ", answerValue = " + answerValue + "]";
    }
}
