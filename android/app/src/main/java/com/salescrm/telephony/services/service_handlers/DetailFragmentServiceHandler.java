package com.salescrm.telephony.services.service_handlers;

import android.content.Context;
import android.util.Log;

import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.response.AddEmailAddressResponse;
import com.salescrm.telephony.response.AddMobileNumberResponse;
import com.salescrm.telephony.response.ChangePrimaryEmailResponse;
import com.salescrm.telephony.response.CustomerEditResponse;
import com.salescrm.telephony.response.DeleteEmailResponse;
import com.salescrm.telephony.response.DeleteMobileResponse;
import com.salescrm.telephony.response.EditEmailResponse;
import com.salescrm.telephony.response.EditMobileNumberResponse;
import com.salescrm.telephony.response.LocationEditResponse;
import com.salescrm.telephony.response.PrimaryMobileResponse;
import com.salescrm.telephony.response.SaveEditLeadResponse;
import com.salescrm.telephony.services.ConnectionDetectorService;
import com.salescrm.telephony.utils.ApiUtil;
import com.salescrm.telephony.utils.WSConstants;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by bannhi on 27/12/16.
 */

public class DetailFragmentServiceHandler {

    public static final DetailFragmentServiceHandler INSTANCE = new DetailFragmentServiceHandler();
    ConnectionDetectorService connectionDetectorService;
    static Context context;

    public static DetailFragmentServiceHandler getInstance(Context ctx) {
        context = ctx;
        return DetailFragmentServiceHandler.INSTANCE;
    }


    /**
     * adding a new mobile number
     *
     * @param mobileNumber
     * @param accesstoken
     * @param leadId
     */
    public void addMobileNumberServiceHandler(String leadLastUpdated, String mobileNumber, String accesstoken, String leadId) {
        connectionDetectorService = new ConnectionDetectorService(context);
        if (connectionDetectorService.isConnectingToInternet()) {
            ApiUtil.GetRestApiWithHeader(accesstoken).AddMobileNumber(leadLastUpdated.trim(), mobileNumber.trim(), Integer.parseInt(leadId), new Callback<AddMobileNumberResponse>() {
                @Override
                public void success(final AddMobileNumberResponse addMobileNumberResponse, Response response) {
                    List<Header> headerList = response.getHeaders();
                    for (Header header : headerList) {
                        Log.e("Addmobile", "Action -  " + header.getName() + " - " + header.getValue());
                        if (header.getName().equalsIgnoreCase("Authorization")) {
                            ApiUtil.UpdateAccessToken(header.getValue());
                        }
                    }
                    SalesCRMApplication.getBus().post(new AddMobileNumberResponse(addMobileNumberResponse));

                }

                @Override
                public void failure(RetrofitError error) {
                    error.printStackTrace();
                    Log.e("Getaddmbilenumber", "addmobilenumber - " + error.getMessage());
                }
            });
        } else {
            //add to DB
        }

    }

    /**
     * API call method to delete a phone number
     *
     * @param lead_phone_mapping_id
     * @param accessToken
     * @param leadId
     */
    public void deleteNumberServiceHandler(String leadLastUpdated, final String lead_phone_mapping_id, String accessToken, int leadId, final String mobile) {
        System.out.println("REQUEST DATE LAST: "+leadLastUpdated);
        ApiUtil.GetRestApiWithHeader(accessToken).DeleteNumber(leadLastUpdated, leadId, lead_phone_mapping_id.trim(), new Callback<DeleteMobileResponse>() {
            @Override
            public void failure(RetrofitError error) {
                Log.e("Deletenumber", "Delete - " + error.getMessage());
            }

            @Override
            public void success(final DeleteMobileResponse deleteMobileResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    Log.e("Deleteresponsel", "delete -  " + header.getName() + " - " + header.getValue());
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }
                if (deleteMobileResponse != null && deleteMobileResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    System.out.println("RESPONSE DATE LAST: "+deleteMobileResponse.getResult().getLead_last_updated());
                    deleteMobileResponse.setLeadMappingId(lead_phone_mapping_id);
                    deleteMobileResponse.setMobile(mobile);
                    SalesCRMApplication.getBus().post(new DeleteMobileResponse(deleteMobileResponse));
                }


            }
        });

    }

    /**
     * editting a number
     *
     * @param phone_mapping_id
     * @param accessToken
     * @param leadId
     * @param phonenumber
     * @param position
     */
    public void editNumberServiceHandler(String leadLastUpdated, String phone_mapping_id, String accessToken, String leadId, String phonenumber, final int position) {
        ApiUtil.GetRestApiWithHeader(accessToken).EditMobileNumber(leadLastUpdated, leadId, phone_mapping_id, phonenumber, new Callback<EditMobileNumberResponse>() {
            @Override
            public void success(EditMobileNumberResponse editMobileNumberResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }

                }
                editMobileNumberResponse.setPosition(position);
                SalesCRMApplication.getBus().post(new EditMobileNumberResponse(editMobileNumberResponse));

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Geteditmobile", "editmobilenumber - " + error.getMessage());

            }
        });
    }

    /**
     * changin the primary number
     *
     * @param accessToken
     * @param leadId
     * @param mobId
     * @param position
     */

    public void changePrimaryNumberServiceHandler(String leadLastUpdated, String accessToken, int leadId, String mobId, final int position) {
        System.out.println("STARRED POSITION:" + position);
        ApiUtil.GetRestApiWithHeader(accessToken).ChangePrimaryMobile(leadLastUpdated, leadId, mobId, new Callback<PrimaryMobileResponse>() {
            @Override
            public void failure(RetrofitError error) {
                Log.e("changeprimary", "changeprimary - " + error.getMessage());
                PrimaryMobileResponse primaryMobileResponse = null;
                SalesCRMApplication.getBus().post(new PrimaryMobileResponse(primaryMobileResponse));
            }

            @Override
            public void success(final PrimaryMobileResponse primaryMobileResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    Log.e("ChangePrimary", "changeprimary -  " + header.getName() + " - " + header.getValue());
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }
                primaryMobileResponse.setPosition(position);
                SalesCRMApplication.getBus().post(new PrimaryMobileResponse(primaryMobileResponse));

            }

        });
    }


    //////////////////////////////////////////////////////// EMail services start here //////////////////////////////////////////////////////////////////////////////////

    /**
     * adding a new email id
     *
     * @param accesstoken
     * @param leadId
     */
    public void AddEmailServiceHandler(String leadLastUpdated, String emailId, String accesstoken, String leadId) {

        ApiUtil.GetRestApiWithHeader(accesstoken).AddEmailAddress(leadLastUpdated, leadId, emailId, new Callback<AddEmailAddressResponse>() {
            @Override
            public void failure(RetrofitError error) {
                Log.e("Getaddemail", "addemail - " + error.getMessage());
            }

            @Override
            public void success(AddEmailAddressResponse addEmailAddressResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    Log.e("Addemail", "Action -  " + header.getName() + " - " + header.getValue());
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }

                }
                SalesCRMApplication.getBus().post(new AddEmailAddressResponse(addEmailAddressResponse));
            }


        });
    }

    /**
     * deleting an email id
     *
     * @param email_mapping_id
     * @param accessToken
     * @param leadId
     */
    public void deleteEmailServiceHandler(String leadLastUpdated, final String email_mapping_id, String accessToken, final int leadId) {
        ApiUtil.GetRestApiWithHeader(accessToken).DeleteEmail(leadLastUpdated, leadId, email_mapping_id, new Callback<DeleteEmailResponse>() {
            @Override
            public void failure(RetrofitError error) {
                Log.e("Deleteemail", "Delete - " + error.getMessage());

            }

            @Override
            public void success(final DeleteEmailResponse deleteEmailResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    Log.e("Deleteresponsel", "delete -  " + header.getName() + " - " + header.getValue());
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }

                }
                if (deleteEmailResponse != null && deleteEmailResponse.getStatusCode().equalsIgnoreCase(WSConstants.RESPONSE_OK)) {
                    deleteEmailResponse.setEmailMappingId(email_mapping_id);
                    SalesCRMApplication.getBus().post(new DeleteEmailResponse(deleteEmailResponse));
                }
            }
        });

    }

    /**
     * edit an email api
     *
     * @param email_mapping_id
     * @param accessToken
     * @param leadId
     * @param emailAddr
     * @param position
     */
    public void editEmailServiceHandler(String leadLastUpdated, final String email_mapping_id, String accessToken, String leadId, String emailAddr, final int position) {
        ApiUtil.GetRestApiWithHeader(accessToken).EditEmail(leadLastUpdated, leadId, email_mapping_id, emailAddr, new Callback<EditEmailResponse>() {
            @Override
            public void success(EditEmailResponse editEmailResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    Log.e("Editemail", "Editemail -  " + header.getName() + " - " + header.getValue());
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());

                    }

                }
                editEmailResponse.setPosition(position);
                SalesCRMApplication.getBus().post(new EditEmailResponse(editEmailResponse));
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Geteditemail", "editemail - " + error.getMessage());

            }
        });
    }

    /**
     * changin the primary number
     *
     * @param accessToken
     * @param leadId
     * @param position
     */

    public void changePrimaryEmailIdServiceHandler(String leadLastUpdated, String accessToken, String leadId, String emailId, final int position) {
        System.out.println("STARRED POSITION:" + position);
        ApiUtil.GetRestApiWithHeader(accessToken).ChangePrimaryAddress(leadLastUpdated, leadId, emailId, new Callback<ChangePrimaryEmailResponse>() {
            @Override
            public void success(final ChangePrimaryEmailResponse changePrimaryEmailResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    Log.e("ChangePrimary", "changeprimary -  " + header.getName() + " - " + header.getValue());
                    if (header.getName().equalsIgnoreCase("Authorization")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }
                changePrimaryEmailResponse.setPosition(position);
                SalesCRMApplication.getBus().post(new ChangePrimaryEmailResponse(changePrimaryEmailResponse));

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("changeprimary", "changeprimary - " + error.getMessage());
            }
        });
    }


    public void saveCustomerDetailsServiceHandler(String accessToken, String leadId, String leadLastUpdated,
                                                  String firstName, String lastName, String residence,
                                                  String pin,
                                                  String residenceLocality,
                                                  String residenceCity,
                                                  String residenceState,
                                                  String officeAddress, String officePin,
                                                  String officeLocality,
                                                  String officeCity,
                                                  String officeState,
                                                  String companyName, String occupation, String gender,
                                                  String title,
                                                  String customerAge, final int typeOfEdit, boolean isOfficeEdit) {


        ApiUtil.GetRestApiWithHeader(accessToken).CustomerEdit(leadId, leadLastUpdated, firstName, lastName,
                residence, pin, residenceLocality, residenceCity,
                isOfficeEdit?officeState:residenceState,
                officeAddress, officePin,
                officeLocality, officeCity,
                companyName, occupation,
                gender, title, customerAge, new Callback<CustomerEditResponse>() {
                    @Override
                    public void success(CustomerEditResponse customerEditResponse, Response response) {
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            Log.e("customeredit", "edit -  " + header.getName() + " - " + header.getValue());
                            if (header.getName().equalsIgnoreCase("Authorization")) {
                                ApiUtil.UpdateAccessToken(header.getValue());
                            }

                        }
                        customerEditResponse.setTypeOfEdit(typeOfEdit);
                        SalesCRMApplication.getBus().post(new CustomerEditResponse(customerEditResponse));
                    }


                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("Customedit", "customedit - " + error.getMessage());
                        SalesCRMApplication.getBus().post(new CustomerEditResponse());
                    }
                });

    }

    public void buyerTypeServicehandler(String accessToken, String leadId, String leadLastUpdated,
                                        String locationId,String dseId, String enqSourceId,
                                        String buyerTypeId, String closingDate, String modeOfPayment,
                                        String typeofCustomer) {
            ApiUtil.GetRestApiWithHeader(accessToken).SaveLeadEdit
                    (leadId, leadLastUpdated, locationId, dseId, enqSourceId, buyerTypeId, closingDate,
                            modeOfPayment, typeofCustomer, new Callback<SaveEditLeadResponse>() {
                                @Override
                                public void success(SaveEditLeadResponse saveEditLeadResponse, Response response) {
                                    List<Header> headerList = response.getHeaders();
                                    for (Header header : headerList) {
                                        Log.e("saveedit", "edit -  " + header.getName() + " - " + header.getValue());
                                        if (header.getName().equalsIgnoreCase("Authorization")) {
                                            ApiUtil.UpdateAccessToken(header.getValue());
                                        }
                                    }
                                    //SaveEditLeadResponse saveeditresponse = (SaveEditLeadResponse) saveEditLeadResponse;
                                    SalesCRMApplication.getBus().post(saveEditLeadResponse);
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Log.e("SaveEditLead", "saveeditlead - " + error.getMessage());
                                    SalesCRMApplication.getBus().post(new SaveEditLeadResponse());
                                }
                            });
       // }

    }

    public void editCrmSourceServiceHandler(String leadLastUpdated, String accessToken, String leadId, String selectedCrmId,
                                            String selectedDseId, String selectedLocId, boolean leadSourceEditFlag, boolean locationEditFlag, boolean dseEditFlag){
        ApiUtil.GetRestApiWithHeader(accessToken).ChangeLocationDse(leadLastUpdated, leadId, selectedCrmId,
                selectedDseId, leadSourceEditFlag, locationEditFlag, dseEditFlag, selectedLocId, new Callback<LocationEditResponse>() {

            @Override
            public void success(LocationEditResponse locationEditResponse, Response response) {
                List<Header> headerList = response.getHeaders();
                for (Header header : headerList) {
                    Log.e("locationedit", "locationedit-  " + header.getName() + " - " + header.getValue());
                    if (header.getName().equalsIgnoreCase("Authorizatiolocationn")) {
                        ApiUtil.UpdateAccessToken(header.getValue());
                    }
                }
                SalesCRMApplication.getBus().post(locationEditResponse);
            }


            @Override
            public void failure(RetrofitError error) {
                SalesCRMApplication.getBus().post(new LocationEditResponse());
            }

        });

    }

}
