package com.salescrm.telephony.model.booking;

import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.CarBrandModelsDB;

import io.realm.RealmResults;

/**
 * Created by bharath on 1/3/18.
 */

public class BookingRealmModel {
    private RealmResults<CarBrandModelsDB> carModelList;
    private RealmResults<CarBrandModelVariantsDB> carVariantDistinctList;
    private CarBrandModelsDB carRealmModelSelected;
    private RealmResults<CarBrandModelVariantsDB> colorList;

    public void setCarModelList(RealmResults<CarBrandModelsDB> carModelList) {
        this.carModelList = carModelList;
    }

    public RealmResults<CarBrandModelsDB> getCarModelList() {
        return carModelList;
    }

    public void setCarVariantDistinctList(RealmResults<CarBrandModelVariantsDB> carVariantDistinctList) {
        this.carVariantDistinctList = carVariantDistinctList;
    }

    public RealmResults<CarBrandModelVariantsDB> getCarVariantDistinctList() {
        return carVariantDistinctList;
    }

    public void setCarRealmModelSelected(CarBrandModelsDB carRealmModelSelected) {
        this.carRealmModelSelected = carRealmModelSelected;
    }

    public CarBrandModelsDB getCarRealmModelSelected() {
        return carRealmModelSelected;
    }

    public void setColorList(RealmResults<CarBrandModelVariantsDB> colorList) {
        this.colorList = colorList;
    }
}
