package com.salescrm.telephony.interfaces;

import android.app.Fragment;
import android.graphics.drawable.Drawable;

/**
 * Created by prateek on 31/7/18.
 */

public interface NewFilterCallFragmentListener {

     void replaceFragment(int i, Fragment fragment);

     void setOptions(String title, Drawable drawable);

     void setApply(String apply, int action_apply);
}
