package com.salescrm.telephony.response;

/**
 * Created by akshata on 10/8/16.
 */
public class GetEmailResponse {


    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public Error getError ()
    {
        return error;
    }

    public void setError (Error error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", message = "+message+", result = "+result+", error = "+error+"]";
    }


    public class Result {
        private Credetails.Activitydetails.LeadSourceMobNo.Template[] template;

        private String ccmEmailId;

        private String custSupportEmailId;

        private String ccmMobNo;

        private String dname;

        private String hotlineNo;

        private Credetails[] credetails;

        private String dealerAddress;

        private String pocName;

        private Credetails.Activitydetails.LeadSourceMobNo leadSourceMobNo;

        private String dealerCity;

        private String[] customerdetails;

        private String dbrand;

        private String crmMobNo;

        private String tdFeedbackLink;

        private String dealerWebsite;

        private Credetails.Activitydetails[] activitydetails;

        public Credetails.Activitydetails.LeadSourceMobNo.Template[] getTemplate() {
            return template;
        }

        public void setTemplate(Credetails.Activitydetails.LeadSourceMobNo.Template[] template) {
            this.template = template;
        }

        public String getCcmEmailId() {
            return ccmEmailId;
        }

        public void setCcmEmailId(String ccmEmailId) {
            this.ccmEmailId = ccmEmailId;
        }

        public String getCustSupportEmailId() {
            return custSupportEmailId;
        }

        public void setCustSupportEmailId(String custSupportEmailId) {
            this.custSupportEmailId = custSupportEmailId;
        }

        public String getCcmMobNo() {
            return ccmMobNo;
        }

        public void setCcmMobNo(String ccmMobNo) {
            this.ccmMobNo = ccmMobNo;
        }

        public String getDname() {
            return dname;
        }

        public void setDname(String dname) {
            this.dname = dname;
        }

        public String getHotlineNo() {
            return hotlineNo;
        }

        public void setHotlineNo(String hotlineNo) {
            this.hotlineNo = hotlineNo;
        }

        public Credetails[] getCredetails() {
            return credetails;
        }

        public void setCredetails(Credetails[] credetails) {
            this.credetails = credetails;
        }

        public String getDealerAddress() {
            return dealerAddress;
        }

        public void setDealerAddress(String dealerAddress) {
            this.dealerAddress = dealerAddress;
        }

        public String getPocName() {
            return pocName;
        }

        public void setPocName(String pocName) {
            this.pocName = pocName;
        }

        public Credetails.Activitydetails.LeadSourceMobNo getLeadSourceMobNo() {
            return leadSourceMobNo;
        }

        public void setLeadSourceMobNo(Credetails.Activitydetails.LeadSourceMobNo leadSourceMobNo) {
            this.leadSourceMobNo = leadSourceMobNo;
        }

        public String getDealerCity() {
            return dealerCity;
        }

        public void setDealerCity(String dealerCity) {
            this.dealerCity = dealerCity;
        }

        public String[] getCustomerdetails() {
            return customerdetails;
        }

        public void setCustomerdetails(String[] customerdetails) {
            this.customerdetails = customerdetails;
        }

        public String getDbrand() {
            return dbrand;
        }

        public void setDbrand(String dbrand) {
            this.dbrand = dbrand;
        }

        public String getCrmMobNo() {
            return crmMobNo;
        }

        public void setCrmMobNo(String crmMobNo) {
            this.crmMobNo = crmMobNo;
        }

        public String getTdFeedbackLink() {
            return tdFeedbackLink;
        }

        public void setTdFeedbackLink(String tdFeedbackLink) {
            this.tdFeedbackLink = tdFeedbackLink;
        }

        public String getDealerWebsite() {
            return dealerWebsite;
        }

        public void setDealerWebsite(String dealerWebsite) {
            this.dealerWebsite = dealerWebsite;
        }

        public Credetails.Activitydetails[] getActivitydetails() {
            return activitydetails;
        }

        public void setActivitydetails(Credetails.Activitydetails[] activitydetails) {
            this.activitydetails = activitydetails;
        }

        @Override
        public String toString() {
            return "ClassPojo [template = " + template + ", ccmEmailId = " + ccmEmailId + ", custSupportEmailId = " + custSupportEmailId + ", ccmMobNo = " + ccmMobNo + ", dname = " + dname + ", hotlineNo = " + hotlineNo + ", credetails = " + credetails + ", dealerAddress = " + dealerAddress + ", pocName = " + pocName + ", leadSourceMobNo = " + leadSourceMobNo + ", dealerCity = " + dealerCity + ", customerdetails = " + customerdetails + ", dbrand = " + dbrand + ", crmMobNo = " + crmMobNo + ", tdFeedbackLink = " + tdFeedbackLink + ", dealerWebsite = " + dealerWebsite + ", activitydetails = " + activitydetails + "]";
        }

        public class Credetails {
            private String cre_name;

            private String cre_mob_no;

            public String getCre_name() {
                return cre_name;
            }

            public void setCre_name(String cre_name) {
                this.cre_name = cre_name;
            }

            public String getCre_mob_no() {
                return cre_mob_no;
            }

            public void setCre_mob_no(String cre_mob_no) {
                this.cre_mob_no = cre_mob_no;
            }

            @Override
            public String toString() {
                return "ClassPojo [cre_name = " + cre_name + ", cre_mob_no = " + cre_mob_no + "]";
            }

            public class Activitydetails {
                private String model_name;

                private String td_time;

                public String getModel_name() {
                    return model_name;
                }

                public void setModel_name(String model_name) {
                    this.model_name = model_name;
                }

                public String getTd_time() {
                    return td_time;
                }

                public void setTd_time(String td_time) {
                    this.td_time = td_time;
                }

                @Override
                public String toString() {
                    return "ClassPojo [model_name = " + model_name + ", td_time = " + td_time + "]";
                }

                public class LeadSourceMobNo {
                    private String mobile_number;

                    public String getMobile_number() {
                        return mobile_number;
                    }

                    public void setMobile_number(String mobile_number) {
                        this.mobile_number = mobile_number;
                    }

                    @Override
                    public String toString() {
                        return "ClassPojo [mobile_number = " + mobile_number + "]";
                    }

                    public class Template {
                        private String content;

                        private String subject;

                        public String getContent() {
                            return content;
                        }

                        public void setContent(String content) {
                            this.content = content;
                        }

                        public String getSubject() {
                            return subject;
                        }

                        public void setSubject(String subject) {
                            this.subject = subject;
                        }

                        @Override
                        public String toString() {
                            return "ClassPojo [content = " + content + ", subject = " + subject + "]";
                        }

                        public class Error {
                            private String details;

                            private String type;

                            public String getDetails() {
                                return details;
                            }

                            public void setDetails(String details) {
                                this.details = details;
                            }

                            public String getType() {
                                return type;
                            }

                            public void setType(String type) {
                                this.type = type;
                            }

                            @Override
                            public String toString() {
                                return "ClassPojo [details = " + details + ", type = " + type + "]";
                            }
                        }
                    }
                }
            }
        }
    }

           }
