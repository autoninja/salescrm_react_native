package com.salescrm.telephony.db.events;

import io.realm.RealmList;
import io.realm.RealmObject;

public class EventsMainDB extends RealmObject {
    private int locationId;
    private String locationName;
    private RealmList<EventsLeadSourceCategoryDB> eventsLeadSourceCategoryDBS;

    public EventsMainDB() {
    }

    public EventsMainDB(int locationId, String locationName, RealmList<EventsLeadSourceCategoryDB> eventsLeadSourceCategoryDBS) {
        this.locationId = locationId;
        this.locationName = locationName;
        this.eventsLeadSourceCategoryDBS = eventsLeadSourceCategoryDBS;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public RealmList<EventsLeadSourceCategoryDB> getEventsLeadSourceCategoryDBS() {
        return eventsLeadSourceCategoryDBS;
    }

    public void setEventsLeadSourceCategoryDBS(RealmList<EventsLeadSourceCategoryDB> eventsLeadSourceCategoryDBS) {
        this.eventsLeadSourceCategoryDBS = eventsLeadSourceCategoryDBS;
    }
}
