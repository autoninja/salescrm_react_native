package com.salescrm.telephony.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.salescrm.telephony.R;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.DealershipsDB;
import com.salescrm.telephony.fragments.LoginFragment;
import com.salescrm.telephony.fragments.OtpCheckFragment;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.OtpRequestResponse;
import com.salescrm.telephony.response.OtpValidateResponse;
import com.salescrm.telephony.response.ValidateOtpResponse;
import com.squareup.otto.Subscribe;

import io.realm.Realm;

/**
 * Created by Ravindra P on 27-11-2015.
 */
public class LoginActivity extends AppCompatActivity {
    Preferences pref;
    private Realm realm;
    private ValidateOtpResponse otpResponse;
    private String imeiNumber;

    private void disableScreenCapture() {
        if(pref.isScreenshotEnabled()) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = Preferences.getInstance();
        pref.load(getApplicationContext());
        disableScreenCapture();
        realm = Realm.getDefaultInstance();
        setContentView(R.layout.activity_login);
        if(pref.getFcmId()== null || pref.getFcmId().equalsIgnoreCase("")) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            pref.setFcmId(refreshedToken);
            pref.setFcmSync(1);
        }
        getIMEINumber();
        showFragments();

    }


    @Subscribe
    public void onSuccessLoginOtp(OtpRequestResponse loginOtpResponse) {
        System.out.println("Called:onSuccessLoginOtp");
        Fragment fragment = new OtpCheckFragment();
        Bundle bundle = new Bundle();
        bundle.putString("otp", "");
        bundle.putString("mobile", pref.getUserMobile());
        fragment.setArguments(bundle);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.login_frame_container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Subscribe
    public void onSuccessValidateOtp(OtpValidateResponse validateOtpResponse) {
        System.out.println("Called:onSuccessValidateOtp");

        realm.beginTransaction();
        realm.where(DealershipsDB.class).findAll().deleteAllFromRealm();
        for(int i=0;i<validateOtpResponse.getResult().getUser_info().size();i++) {
            OtpValidateResponse.Result.UserInfo userInfo = validateOtpResponse.getResult().getUser_info().get(i);
            DealershipsDB dealershipsDB = new DealershipsDB();
            dealershipsDB.setDealer_id(userInfo.getDealer().getId());
            dealershipsDB.setDealer_name(userInfo.getDealer().getName());
            dealershipsDB.setDealer_db_name(userInfo.getDealer().getDb_name());

            dealershipsDB.setUser_id(userInfo.getUsers().getId());
            dealershipsDB.setUser_name(userInfo.getUsers().getName());
            dealershipsDB.setUser_dp_url(userInfo.getUsers().getImage_url());
            realm.copyToRealmOrUpdate(dealershipsDB);
        }
        realm.commitTransaction();


        Intent intent = new Intent(this, SwitchDealershipsActivity.class);
        intent.putExtra("START_C360", isOpenC360());
        startActivity(intent);
        LoginActivity.this.finish();

        pref.setIsLogedIn(true);

    }
    private boolean isOpenC360() {
        boolean openC360 = false;
        if(getIntent()!=null && getIntent().getBooleanExtra("START_C360", false)) {
            openC360 = true;
        }
        return openC360;
    }

    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        super.onBackPressed();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SalesCRMApplication.getBus().register(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        SalesCRMApplication.getBus().unregister(this);
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
        super.onSaveInstanceState(outState);
    }

    private void showFragments(){
        Fragment fragment = new LoginFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.login_frame_container, fragment)
                .commitAllowingStateLoss();
    }

    private void getIMEINumber() {
        try{
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                imeiNumber = telephonyManager.getDeviceId();
                pref.setImeiNumber(imeiNumber);
            }
        }
        catch (Exception e) {

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
