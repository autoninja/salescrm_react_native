package com.salescrm.telephony.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.salescrm.telephony.R;
import com.salescrm.telephony.adapter.TodayTasksSummaryAdapter;
import com.salescrm.telephony.interfaces.TeamSummaryToTaskListener;
import com.salescrm.telephony.model.DseModelTask;
import com.salescrm.telephony.model.TodaySummaryModel;

/**
 * Created by prateek on 5/6/17.
 */

public class TodayTasksSummaryChildFragment extends Fragment implements TodayTasksSummaryAdapter.TodayTasksSummaryClickListener {

    private RecyclerView mRecyclerView;
    private TeamSummaryToTaskListener homeActivityListener;
    private static boolean isUserTlOnly = false;
    private int position =0;


    public static TodayTasksSummaryChildFragment newInstance(boolean dataBoolean,
                                                             int position)  {
        isUserTlOnly = dataBoolean;
        TodayTasksSummaryChildFragment todayTasksSummaryChildFragment = new TodayTasksSummaryChildFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        todayTasksSummaryChildFragment.setArguments(bundle);
        return todayTasksSummaryChildFragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.task_summary_fragment, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.progressRecycler);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        if(getArguments()!=null) {
            position = getArguments().getInt("position",0);
        }
        setUpAdapter();

        return rootView;
    }

    private void setUpAdapter() {
        if(position < TodaySummaryModel.getInstance().getResultList().size()) {
            mRecyclerView.setAdapter(new TodayTasksSummaryAdapter(getContext(),
                    TodaySummaryModel.getInstance().getResultList().get(position),
                    isUserTlOnly,
                    this));
        }
    }

    @Override
    public void onAttach(Context context) {
        try {
            homeActivityListener = (TeamSummaryToTaskListener) context;
        }
        catch (ClassCastException e){
            System.out.println("ClassCastException TeamSummaryToTaskListener");
        }
        super.onAttach(context);
    }

    @Override
    public void onTodayTaskItemClick(DseModelTask dseModelTask) {
        System.out.println("Dse clicked");
        if(homeActivityListener!=null){
            homeActivityListener.onDseTaskSummaryClicked(dseModelTask,isUserTlOnly);
        }
    }

}
