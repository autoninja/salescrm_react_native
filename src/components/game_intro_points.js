import React, { Component } from 'react'
import { View, Text, Image, ScrollView, Switch,} from 'react-native'
import styles from '../styles/styles'


export default class GameIntroPoints extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount () {
}
  render() {
    return(
  <View style={styles.MainContainer}>
      <View style= {styles.MainContainerYellow}>
        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>

          <Image source={require('../images/point_system.png')}
          style={{flex:1,margin:30,}}
          resizeMode = 'contain'
           />

           <View style={{flex:1, alignItems:'center'}}>

           <View style={{flexDirection:'row', padding:10}}>

           <Text style={{color:'#fff',fontWeight:'bold',fontSize:19, fontStyle:'italic', paddingLeft:10}}>Point System</Text>
           </View>
           </View>

        </View>



        <View style={{flex:2,borderBottomLeftRadius:6,}}>
        <ScrollView>
        <View style={{flexDirection:'row',flex:1, padding:10}}>
        <Image
        source={require('../images/point_system_target.png')}
        resizeMode='contain'
        style={{height:24, width:24}}
        />
        <Text style={{flex:1,fontSize:18,color:'#fff', textAlign:'left', paddingLeft:10,flexWrap: 'wrap'}}>
          <Text>You get points for </Text>
          <Text style={{fontWeight: "bold"}}>Test Drive, Visits, Retails & No Pending Days </Text>
          <Text>based on your </Text>
          <Text style={{fontWeight: "bold"}}>TARGET</Text>
        </Text>
        </View>

        <View style={{flexDirection:'row',flex:1, padding:10}}>
        <Image
        source={require('../images/point_system_booster.png')}
        resizeMode='contain'
        style={{height:24, width:24}}
        />
        <Text style={{flex:1,fontSize:18,color:'#fff', textAlign:'left', paddingLeft:10,flexWrap: 'wrap'}}>
          <Text style={{fontWeight: "bold"}}>Booster </Text>
          <Text>period gives double points</Text>
        </Text>
        </View>

        <View style={{flexDirection:'row',flex:1, padding:10}}>
        <Image
        source={require('../images/point_system_badges.png')}
        resizeMode='contain'
        style={{height:24, width:24}}
        />
        <Text style={{flex:1,fontSize:18,color:'#fff', textAlign:'left', paddingLeft:10,flexWrap: 'wrap'}}>

          <Text>Collect </Text>
          <Text style={{fontWeight: "bold"}}>Badges </Text>
          <Text>for exceptional performance</Text>
        </Text>
        </View>
        </ScrollView>
        </View>



      </View>
      <View style={{alignItems: 'center', padding:10,flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
      {this.props.bottomViews}
      </View>
      </View>
    )
  }
}
