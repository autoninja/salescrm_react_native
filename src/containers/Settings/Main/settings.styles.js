import { StyleSheet } from 'react-native';
export default StyleSheet.create({
    mainContainer :{
        paddingTop : 10,
        paddingLeft : 15,
        paddingRight : 15,
        height: '100%'
    },
    touchableContainer: {
        flex: 1,
        flexDirection: 'row'
    },
    listItemContainer : {
        flex: 1,
        flexDirection: 'row',
        height: 60
    },
    listItemColumn: {
        paddingTop: 15,
        paddingLeft: 10,
        height: 60,
        backgroundColor: '#ffffff',
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 15
    },
    listItem: {
        backgroundColor: '#ffffff',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 10,
        elevation: 10
    },

    listItemTextContainer : {
        paddingBottom: 10,
        paddingLeft: 10
    },
    listItemText : {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#2e5e86'
    },
    listItemIconContainer: {
        width: 50,
        paddingLeft: 10,
        paddingTop: 10,
        borderRadius : 30,
        backgroundColor: '#F8F8F8'
    },
    listItemIcon : {
        height : 28,
        width : 28,
        paddingTop: 10
    }
});
