package com.salescrm.telephony.interfaces;

import retrofit.RetrofitError;

/**
 * Created by bharath on 14/10/16.
 */

public interface SyncListener {

    //Allot Dse
    void onAllotDseSync();

    void onOfflineSupportApiError(RetrofitError error, int fromId);


    void onCreateLeadSync();

    void onFormSync();

    void onCarSync();

}
