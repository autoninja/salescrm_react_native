package com.salescrm.telephony.model.booking;

/**
 * Created by bharath on 19/3/18.
 */

public class ApiInputDiscount {
    private String corporate, exchange_bonus,
            oem_scheme,
            loyalty_bonus,
            dealer,
            free_accessories;

    public ApiInputDiscount(String corporate, String exchange_bonus, String oem_scheme, String loyalty_bonus, String dealer, String free_accessories) {
        this.corporate = corporate;
        this.exchange_bonus = exchange_bonus;
        this.oem_scheme = oem_scheme;
        this.loyalty_bonus = loyalty_bonus;
        this.dealer = dealer;
        this.free_accessories = free_accessories;
    }

    public String getCorporate() {
        return corporate;
    }

    public void setCorporate(String corporate) {
        this.corporate = corporate;
    }

    public String getExchange_bonus() {
        return exchange_bonus;
    }

    public void setExchange_bonus(String exchange_bonus) {
        this.exchange_bonus = exchange_bonus;
    }

    public String getOem_scheme() {
        return oem_scheme;
    }

    public void setOem_scheme(String oem_scheme) {
        this.oem_scheme = oem_scheme;
    }

    public String getLoyalty_bonus() {
        return loyalty_bonus;
    }

    public void setLoyalty_bonus(String loyalty_bonus) {
        this.loyalty_bonus = loyalty_bonus;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public String getFree_accessories() {
        return free_accessories;
    }

    public void setFree_accessories(String free_accessories) {
        this.free_accessories = free_accessories;
    }
}