import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'


import LiveEFItemChild from './live_ef_item_child'

export default class LiveEFItemExpandable extends Component {
  constructor(props){
    super(props);
    this.state= {expand:false, showImagePlaceHolder:true};
  }
  _etvbrLongPress(from, info) {
  this.props.onLongPress(from, info)
  console.log(info)
  }
  render() {

    const {team_leader} = this.props;
    let imageSource;
    let placeHolder
    if(this.props.from_location) {
      imageSource = require( '../../images/ic_location.png');
      placeHolder = null;

    }
    else {
      imageSource = {uri : this.props.info.dp_url};
      placeHolder = <View style={{position:'absolute',display:(this.state.showImagePlaceHolder?'flex':'none'),width:36, height:36, borderRadius: 36/2,backgroundColor:'#FF8000', alignItems:'center', justifyContent:'center'}}><Text style={{color:'#fff'}}>{this.props.info.name.charAt(0).toUpperCase()}</Text></View>;;
      }


    let sc_views = []
    for(var i=0;i<team_leader.sales_consultants.length;i++) {
      let sc = team_leader.sales_consultants[i];
      sc_views.push(

        <LiveEFItemChild
          isUnderTl = {true}
          leadSourceCarModelWiseETVBR = 
          {()=> {this.props.leadSourceCarModelWiseETVBRInner(
          sc.info.name, 
          sc.info.dp_url, 
          this.props.info.location_id,
          sc.info.id,
          sc.info.user_role_id)}
          }
          onLongPress = {this._etvbrLongPress.bind(this)}
         showPercentage = {this.props.showPercentage} key = {sc.info.id}

                            abs = {
                            {
                              r: sc.info.abs.retails,
                              e: sc.info.abs.exchanged,
                              f_i: sc.info.abs.in_house_financed,
                              f_o: sc.info.abs.out_house_financed,
                              p_b: sc.info.abs.pending_bookings,
                              l_e: sc.info.abs.live_enquiries,

                              }}

                              rel = {
                              {
                                r: sc.info.rel.retails,
                                e: sc.info.rel.exchanged,
                                f_i: sc.info.rel.in_house_financed,
                                f_o: sc.info.rel.out_house_financed,
                                p_b: sc.info.rel.pending_bookings,
                                l_e: sc.info.rel.live_enquiries,

                                }}



                              info = {
                                {
                                  dp_url : sc.info.dp_url,
                                  name : sc.info.name,
                                  user_id:sc.info.id,
                                  location_id:this.props.info.location_id,
                                  user_role_id:sc.info.user_role_id,
                                }
                              }

                              targets = {
                                {
                                  r: sc.info.targets.retails,
                                  e: sc.info.targets.exchanged,
                                  f_i: sc.info.targets.in_house_financed,
                                  f_o: sc.info.targets.out_house_financed,
                                  p_b: sc.info.targets.pending_bookings,
                                  l_e: sc.info.targets.live_enquiries,
                                  }
                            }

                              ></LiveEFItemChild>)

                                }

    if(this.props.showPercentage) {


          return(

            <View>
            <TouchableOpacity onPress = {()=>this.setState({expand:!this.state.expand})}>
            <View style={{marginTop:10}}>
              <View style={{marginTop:4, marginBottom:4, flexDirection:'row', alignItems:'center'}}>
                <Text style={{flex:1, fontSize:15, color:'#2D4F8B',  }}>{this.props.info.name}</Text>
                <TouchableOpacity 
                 hitSlop={{top: 20, bottom: 20, left: 20, right: 20}} 
                onPress={()=>{this.props.leadSourceCarModelWiseETVBR()}}>
                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center',padding:3, }}>
                    <Text style={{fontSize:12, color:'#007FFF',}}>Source / Model</Text>
                    
                    </View>
                </TouchableOpacity>
            </View> 
            <View style={{flexDirection: 'row', height:60, backgroundColor:'#eceff1',  alignItems:'center', borderRadius:6}}>

            <View style={{flex:1.5, alignItems:'center', justifyContent:'center'}}>
              {placeHolder}
              <TouchableOpacity
                style={{position:'absolute',}}
                onPress = {()=>this.setState({expand:!this.state.expand})}
                onLongPress={() =>{
                this.props.onLongPress('user_info', 
                {info:{name:this.props.info.name, dp_url:this.props.info.dp_url}})
              }}>
                <Image
                  source= {imageSource}
                  style={{width:36, height:36, borderRadius: 36/2,}}
                />
            </TouchableOpacity>
            </View>






            <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#303030'}} >{this.props.rel.e}</Text>
              </View>


            </View>

            <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#303030'}} >{this.props.rel.f_i}</Text>
              </View>

            </View>

            <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#303030'}} >{this.props.rel.f_o}</Text>
              </View>

            </View>

            <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#00bfa5'}} >-</Text>
              </View>


            </View>

            <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
              borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>

                <Text style={{ color:'#00bfa5'}} >-</Text>
            </View>


            </View>
            </View>
            </TouchableOpacity>

            <View style = {{display:(this.state.expand?'flex':'none')}}>
            {sc_views}
            </View>
            </View>
          );

    }
    else {


    return(

      <View>
      <TouchableOpacity onPress = {()=>this.setState({expand:!this.state.expand})}>
      <View style={{marginTop:10}}>
              <View style={{marginTop:4, marginBottom:4, flexDirection:'row', alignItems:'center'}}>
                <Text style={{flex:1, fontSize:15, color:'#2D4F8B',  }}>{this.props.info.name}</Text>
                <TouchableOpacity
                 hitSlop={{top: 20, bottom: 20, left: 20, right: 20}} 
                onPress={()=>{this.props.leadSourceCarModelWiseETVBR()}}>
                <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center',padding:3, }}>
                    <Text style={{fontSize:12, color:'#007FFF',}}>Source / Model</Text>
                    
                    </View>
                </TouchableOpacity>
      </View> 
      <View style={{flexDirection: 'row', height:60, backgroundColor:'#eceff1', alignItems:'center', borderRadius:6}}>

      <View style={{flex:1.5, alignItems:'center', justifyContent:'center'}}>
              {placeHolder}
              <TouchableOpacity
                style={{position:'absolute',}}
                onPress = {()=>this.setState({expand:!this.state.expand})}
                onLongPress={() =>{
                this.props.onLongPress('user_info', 
                {info:{name:this.props.info.name, dp_url:this.props.info.dp_url}})
              }}>
                <Image
                  source= {imageSource}
                  style={{width:36, height:36, borderRadius: 36/2,}}
                />
            </TouchableOpacity>
            </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2 ,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onPress = {() => this.setState({expand:!this.state.expand})}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Retails',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#303030',textDecorationLine:'underline'}} >{this.props.abs.r}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030'}} >{this.props.targets.r}</Text>
        </View>
      </View>




      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onPress = {() => this.setState({expand:!this.state.expand})}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Exchanged',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#303030',textDecorationLine:'underline'}} >{this.props.abs.e}</Text>
        </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030'}} >{this.props.targets.e}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onPress = {() => this.setState({expand:!this.state.expand})}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'In House Financed',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#303030',textDecorationLine:'underline'}} >{this.props.abs.f_i}</Text>
          </TouchableOpacity>
        </View>

        <View style= {{flex:1, backgroundColor:'#cfd8dc',alignItems:'center'}}>
          <Text style={{ color:'#303030'}} >{this.props.targets.f_i}</Text>
        </View>
      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onPress = {() => this.setState({expand:!this.state.expand})}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Out House Financed',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#303030',textDecorationLine:'underline'}} >{this.props.abs.f_o}</Text>
        </TouchableOpacity>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3}}>
        <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onPress = {() => this.setState({expand:!this.state.expand})}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Pending Bookings',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#00bfa5',textDecorationLine:'underline'}} >{this.props.abs.p_b}</Text>
          </TouchableOpacity>
        </View>


      </View>

      <View style= {{flex:1,height:'100%',borderLeftColor: '#bdbdbd',
        borderLeftWidth: 0.3,justifyContent: "center",alignItems: "center"}}>
        <TouchableOpacity
         hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
        onPress = {() => this.setState({expand:!this.state.expand})}
        onLongPress={() => this.props.onLongPress('etvbr_details', {info:{clicked_val:'Live Enquiries',user_id:this.props.info.user_id,title:this.props.info.name, user_location_id:this.props.info.location_id,user_role_id:this.props.info.user_role_id}})}>

          <Text style={{ color:'#00bfa5',textDecorationLine:'underline'}} >{this.props.abs.l_e}</Text>
          </TouchableOpacity>
      </View>


      </View>
      </View>
      </TouchableOpacity>

      {this.state.expand && (
        <View>
           {sc_views}
        </View>
      )}
     
      </View>
    );
  }
  }
}
