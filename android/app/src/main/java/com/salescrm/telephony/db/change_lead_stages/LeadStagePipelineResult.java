package com.salescrm.telephony.db.change_lead_stages;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by prateek on 23/12/16.
 */
public class LeadStagePipelineResult extends RealmObject {

    @PrimaryKey
    private int leadId;

    public RealmList<PipelineStages> pipelineStages = new RealmList<>();
    public RealmList<LostReasons> lostReasons = new RealmList<>();
    public RealmList<DroppedReasons> droppedReasons = new RealmList<>();

    public RealmList<PipelineStages> getPipelineStages() {
        return pipelineStages;
    }

    public void setPipelineStages(RealmList<PipelineStages> pipelineStages) {
        this.pipelineStages = pipelineStages;
    }

    public RealmList<LostReasons> getLostReasons() {
        return lostReasons;
    }

    public void setLostReasons(RealmList<LostReasons> lostReasons) {
        this.lostReasons = lostReasons;
    }

    public RealmList<DroppedReasons> getDroppedReasons() {
        return droppedReasons;
    }

    public void setDroppedReasons(RealmList<DroppedReasons> droppedReasons) {
        this.droppedReasons = droppedReasons;
    }

    public int getLeadId() {
        return leadId;
    }

    public void setLeadId(int leadId) {
        this.leadId = leadId;
    }

}
