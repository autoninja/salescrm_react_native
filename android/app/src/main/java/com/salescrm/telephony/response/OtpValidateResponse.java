package com.salescrm.telephony.response;

import android.content.Intent;

import java.util.List;

public class OtpValidateResponse {
    private String statusCode;

    private String message;

    private Result result;

    private Error error;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public class Result {
        private List<UserInfo> user_info;
        private String token;

        public List<UserInfo> getUser_info() {
            return user_info;
        }

        public void setUser_info(List<UserInfo> user_info) {
            this.user_info = user_info;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public class UserInfo {
            private Dealer dealer;
            private Users users;

            public Dealer getDealer() {
                return dealer;
            }

            public void setDealer(Dealer dealer) {
                this.dealer = dealer;
            }

            public Users getUsers() {
                return users;
            }

            public void setUsers(Users users) {
                this.users = users;
            }

            public class Dealer {
                Integer id;
                String name;
                String db_name;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getDb_name() {
                    return db_name;
                }

                public void setDb_name(String db_name) {
                    this.db_name = db_name;
                }
            }
            public class Users {
                Integer id;
                String name;
                String dp_filename;
                String image_url;

                public String getImage_url() {
                    return image_url;
                }

                public void setImage_url(String image_url) {
                    this.image_url = image_url;
                }

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getDp_filename() {
                    return dp_filename;
                }

                public void setDp_filename(String dp_filename) {
                    this.dp_filename = dp_filename;
                }
            }

        }
    }
}
