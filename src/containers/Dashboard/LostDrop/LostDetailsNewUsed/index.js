import React, { Component } from "react";
import {
  View,
  FlatList,
  ActivityIndicator,
  Text,
  BackHandler
} from "react-native";
import {
  createMaterialTopTabNavigator,
  createAppContainer
} from "react-navigation";

import LostDetailsTab from "./lost_details_tab";
import RestClient from "../../../../network/rest_client";

export default class LostDetailsNewUsed extends Component {
  static navigationOptions = ({ navigation }) => {
    let title = "Lost";
    // if(global.fromAndroid && this.props && this.props.modelData && JSON.parse(this.props.modelData)) {
    //   title +=" "+ JSON.parse(this.props.modelData).name+" To";
    // }
    // else
    if (navigation.getParam("modelData")) {
      title += " " + navigation.getParam("modelData").name + " To";
    }
    return {
      title
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      tabs: [{ name: "New Car" }, { name: "Used Car" }],
      result: {},
      isLoading: true
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }
  componentDidMount() {
    this.getLostDropAnalysisBrandWise();
    this._mounted = true;
  }
  componentWillUnmount() {
    this._mounted = false;
    BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      "hardwareBackPress",
      this.handleBackButtonClick
    );
  }
  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    return true;
  }

  openPieDetails(carType, data, color) {
    let modelData = this.props.navigation.getParam("modelData");
    // if(global.fromAndroid && this.props.modelData) {
    //   modelData = JSON.parse(this.props.modelData);
    // }
    this.props.navigation.navigate("LostDetailsOEMLevel", {
      modelData: modelData,
      carType,
      data,
      color
    });
  }

  getLostDropAnalysisBrandWise() {
    var filters = [];
    for (var i = 0; i < global.global_lost_drop_filters_selected.length; i++) {
      filters.push(global.global_lost_drop_filters_selected[i]);
    }
    // if(global.fromAndroid && this.props.modelData) {
    //   filters.push({key:'car_model',values:[{id:JSON.parse(this.props.modelData).id}]})
    // }
    // else
    if (this.props.navigation.getParam("modelData")) {
      filters.push({
        key: "car_model",
        values: [{ id: this.props.navigation.getParam("modelData").id }]
      });
    }

    new RestClient()
      .getLostDropAnalysisBrandWise({
        start_date: global.global_filter_date.startDateStr,
        end_date: global.global_filter_date.endDateStr,
        filters: encodeURIComponent(JSON.stringify(filters))
      })
      .then(data => {
        if (!this._mounted) {
          return;
        }
        console.log(JSON.stringify(data));

        if (!this.state.isApiCalledAtleastOnce) {
          //this.pushCleverTapEvent({'Dashboard Type': eventTeamDashboard.keyType.valuesType.liveEFTeams});
        }

        this.setState({
          isApiCalledAtleastOnce: true,
          isLoading: false,
          result: data.result
        });
      })
      .catch(error => {
        //console.error(error);
        if (!this._mounted) {
          return;
        }
        this.setState({ isLoading: false });
        if (!error.status) {
          //  this.refs.toast.show('Connection Error');
        }
      });
  }

  _tabNavigator() {
    var tabs = {};
    var result = this.state.result;
    if (result && result.new_car) {
      tabs["New Car(" + result.new_car.total + ")"] = {
        screen: props => (
          <LostDetailsTab
            data={result.new_car}
            title={"New Car"}
            onClick={this.openPieDetails.bind(this)}
          />
        )
      };
    }
    if (result && result.old_car) {
      tabs["Used Car(" + result.old_car.total + ")"] = {
        screen: props => (
          <LostDetailsTab
            data={result.old_car}
            title={"Used Car"}
            onClick={this.openPieDetails.bind(this)}
          />
        )
      };
    }
    if (result && (result.new_car || result.old_car)) {
      return createAppContainer(
        createMaterialTopTabNavigator(tabs, {
          swipeEnabled: true,
          animationEnabled: true,
          backBehavior: "none",
          tabBarOptions: {
            style: {
              backgroundColor: "#fff",
              elevation: 0
            },
            labelStyle: {
              fontSize: 16
            },
            inactiveTintColor: "#808080",
            activeTintColor: "#2e5e86",
            upperCaseLabel: false,

            indicatorStyle: {
              backgroundColor: "#2e5e86",
              height: 1
            }
          }
        })
      );
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View
          style={{
            display: this.state.isLoading ? "flex" : "none",
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#fff"
          }}
        >
          <ActivityIndicator
            animating={this.state.isLoading}
            style={{ flex: 1, alignSelf: "center" }}
            color="#2e5e86"
            size="large"
            hidesWhenStopped={true}
          />
        </View>
      );
    }
    const Tabs = this._tabNavigator();
    if (
      this.state.result &&
      (this.state.result.new_car || this.state.result.old_car)
    ) {
      return <Tabs />;
    }
    return (
      <View
        style={{
          alignItems: "center",
          backgroundColor: "#fff",
          flex: 1,
          justifyContent: "center"
        }}
      >
        <Text style={{ alignSelf: "center", fontSize: 20 }}> No Data </Text>
      </View>
    );
  }
}
