import React, { Component } from 'react';

import { View, Text, ActivityIndicator} from 'react-native';


import { StackActions, NavigationActions } from 'react-navigation';

export default class LoginRedirector extends Component {

    constructor(props) {
        super(props);
        this.state = {loading:true};
    }

    componentDidMount() {
        const resetAction = StackActions.reset({
            index: 0, // <-- currect active route from actions array
            key:null,
            actions: [
                NavigationActions.navigate({ routeName: 'Login' }),
            ],
        });
        this.props.navigation.dispatch(resetAction);
    }
    render() {
        return(
            <View style = {{backgroundColor:'#fff',flex:1,justifyContent: 'center',
                alignItems: 'center',}}>

                <ActivityIndicator
                    animating={this.state.loading}
                    style={{ alignSelf:'center',}}
                    color="#007fff"
                    size="large"
                    hidesWhenStopped={true}

                />
                <Text style={{fontSize:18,color:'#303030', marginTop:10}}> Loading ..</Text>
            </View>)


    }
}
