import React, { Component } from 'react';
import {
    Platform, View, Text, Image, Switch, TouchableOpacity, Alert, ActivityIndicator, FlatList,
    StyleSheet, Modal, NativeModules, TouchableWithoutFeedback, ScrollView
} from 'react-native'
import Button from '../../components/uielements/Button'
import NavigationService from '../../navigation/NavigationService'
import { BOOKING_STAGES } from '../BookVehicle/BookingUtils/BookingUtils'
//import UserInfoService from '../../storage/user_info_service'
import { TASK_ACTIVITY_NAME } from '../BookVehicle/BookingUtils/BookingUtils'
const styles = StyleSheet.create({
    top_view: {
        alignItems: 'center',
        paddingTop: 10,
    },
    inner_main_block: {
        backgroundColor: "#E6ECFD",
        alignSelf: 'stretch',
        borderRadius: 4,
        marginStart: 8,
        marginEnd: 8,
        marginBottom: 16,
        paddingBottom: 16,
        paddingTop: 16,
    },
    view_straight_line: {
        height: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#cfcfcf',
        marginStart: 14,
        marginEnd: 14,
        marginTop: 16,
    },
    car_name_title: {
        fontSize: 14,
        color: '#3F3F3F',
        textAlignVertical: 'center',
        paddingStart: 4,
    },
    booking_detail: {
        padding: 4,
        borderRadius: 3,
        borderColor: '#2196f3',
        borderWidth: 1,
    },
    test_drive: {
        padding: 4,
        borderRadius: 3,
        backgroundColor: '#24a314',
    },
});


const USER_ROLE_SC = 4;
const USER_ROLE_TL = 6;
const USER_ROLE_SM = 8;
const USER_ROLE_BH = 21;
const USER_ROLE_OPS = 30;
const USER_ROLE_BILLING = 11;

export default class InterestedCarsDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //roles: UserInfoService.getRoles()
            roles: this.props.userRoles,
            leadDetails: this.props.leadDetails
        }
    }
    mainAction(action, info) {
        if (this.props.isClientTwoWheeler) {
            this.props.parentNavigation.navigate('BookBike', {
                interestedVehicles: this.props.interestedVehicles,
                formSubmissionInputData: this.props.formSubmissionInputData,
                leadId: this.state.leadDetails.lead_id,
                leadLastUpdated: this.state.leadDetails.lead_last_updated
            })
        }
        else {
            info.action = action;
            this.props.parentNavigation.navigate("BookCar", {
                info,
                interestedVehicles: this.props.interestedVehicles,
                userRoles: this.state.roles,
                form_object: this.props.formSubmissionInputData,
                requireRefreshC360: () => {
                    this.props.refresh360();
                }
            })
        }
    }

    render() {
        let { leadDetails } = this.state;
        let interestedCarsDetails = this.props.interestedCarsDetails;
        return (
            <View style={styles.top_view}>
                {interestedCarsDetails.map((interestedCar) => {
                    if (interestedCar.carDmsData.length > 0) {
                        return interestedCar.carDmsData.map(carDmsData => this.generateCarCardView(leadDetails, interestedCar, carDmsData));
                    }
                    else {
                        return this.generateCarCardView(leadDetails, interestedCar);
                    }

                })}
            </View>
        )
    }

    isCarEditAccessible(stageId) {
        let { roles, leadDetails } = this.state;
        if (leadDetails.lead_active != '0' && stageId == BOOKING_STAGES.STAGE_INIT) {
            for (var i = 0; i < roles.length; i++) {
                if (roles[i].id == USER_ROLE_SC
                    || roles[i].id == USER_ROLE_TL
                    || roles[i].id == USER_ROLE_SM || roles[i].id == USER_ROLE_BH || roles[i].id == USER_ROLE_OPS) {
                    return true;
                }
            }
        }
        return false;
    }
    isChangeStageEnabled() {
        if (this.state.leadDetails.lead_active == '0') {
            return false;
        }
        return this.props.isClientTwoWheeler ? true : false;
    }
    isActionAccessible(stageId) {
        let { leadDetails, roles } = this.state;
        if (leadDetails.lead_active != 1) {
            return false;
        }
        if (stageId == BOOKING_STAGES.STAGE_FULL_PAYMENT_RECEIVED) {
            for (let i = 0; i < roles.length; i++) {
                if (roles[i].id == USER_ROLE_BILLING) {
                    return true;
                }
            }
        }
        else {
            for (let i = 0; i < roles.length; i++) {
                if (roles[i].id == USER_ROLE_SC || roles[i].id == USER_ROLE_TL || roles[i].id == USER_ROLE_SM || roles[i].id == USER_ROLE_BH || roles[i].id == USER_ROLE_OPS) {
                    return true;
                }
            }
        }
        return false
    }
    isCancelAccessible() {
        let { leadDetails, roles } = this.state;
        if (leadDetails.lead_active != 1) {
            return false;
        }
        for (let i = 0; i < roles.length; i++) {
            if (roles[i].id == USER_ROLE_SC || roles[i].id == USER_ROLE_TL || roles[i].id == USER_ROLE_SM || roles[i].id == USER_ROLE_BH || roles[i].id == USER_ROLE_OPS) {
                return true;
            }
        }
        return false;
    }
    isPostBookingTDVisible() {
        let plannedActivities = this.props.plannedActivities
        for (var i = 0; i < plannedActivities.length; i++) {
            if (plannedActivities[i].activity_id == TASK_ACTIVITY_NAME.NO_CAR_BOOKED_ID || plannedActivities[i].activity_id == TASK_ACTIVITY_NAME.RECEIVE_PAYMENT_ID
                || plannedActivities[i].activity_id == TASK_ACTIVITY_NAME.INVOICE_REQUEST_ID || plannedActivities[i].activity_id == TASK_ACTIVITY_NAME.DELIVERY_REQUEST_ID) {
                return true
            }
        }
        return false
    }

    generateCarCardView = (leadDetails, interestedCar, carDmsData) => {
        let stageId;
        let bookingId;
        if (interestedCar) {
            if (carDmsData) {
                stageId = parseInt(carDmsData.car_stage_id);
                bookingId = carDmsData.booking_id
            } else {
                stageId = 0;
                bookingId = '';
            }
            let index = interestedCar.lead_car_id + bookingId
            let carEditVisibility = this.isCarEditAccessible(stageId);

            let imagePrimary = require('../../images/ic_star_outline.png');
            if (interestedCar.is_primary == 1) {
                imagePrimary = require('../../images/ic_star_filled.png');
            }

            let stage = {
                image: require('../../images/booked_tag.png'),
                visible: true,
                text: null
            };
            let bookingInfo = {
                text: carDmsData ? "Booking ID-" + carDmsData.booking_number : '',
                visible: true
            };
            let postBookingTDVisibility = this.isPostBookingTDVisible() && !this.props.isClientTwoWheeler;

            let tdInfo;
            let action = {
                visible: true,
                text: null,
            }
            let cancelBookingVisibility = true;

            switch (stageId) {
                case BOOKING_STAGES.STAGE_INIT:
                    if (interestedCar.test_drive_status == 1) {
                        tdInfo = "Test Drive: Conducted";
                    }
                    else {
                        tdInfo = "Test Drive: Not Conducted";
                    }
                    cancelBookingVisibility = false;

                    stage.visible = false;
                    bookingInfo.visible = false;
                    action.text = "Book Vehicle";
                    break;
                case BOOKING_STAGES.STAGE_BOOKED:
                    stage.text = "BOOKED";
                    action.text = "Full Payment Received"
                    break;
                case BOOKING_STAGES.STAGE_FULL_PAYMENT_RECEIVED:
                    stage.text = "BOOKED";
                    action.text = "Update Invoice"
                    break;
                case BOOKING_STAGES.STAGE_INVOICED:
                    stage.image = require('../../images/invoiced_tag.png')
                    stage.text = "INVOICED";
                    bookingInfo.text = bookingInfo.text + "\n" + "Invoice ID-" + carDmsData.invoice_number;
                    action.text = "Update Delivery"
                    break;
                case BOOKING_STAGES.STAGE_DELIVERED:
                    cancelBookingVisibility = false;
                    stage.image = require('../../images/delivered_tag.png')
                    stage.text = "DELIVERED";
                    action.visible = false;
                    break;
                case BOOKING_STAGES.STAGE_CANCELLED:
                    cancelBookingVisibility = false;
                    stage.image = require('../../images/cancelled_tag.png');
                    stage.text = "CANCELLED";
                    bookingInfo.visible = false;
                    action.visible = false;
                    break;
            }
            if (action.visible) {
                action.visible = this.isActionAccessible(stageId);
            }
            if (cancelBookingVisibility) {
                cancelBookingVisibility = this.isCancelAccessible();
            }

            let info = {
                leadCarId: interestedCar.lead_car_id,
                booking_id: carDmsData ? carDmsData.booking_id : null,
                leadId: leadDetails.lead_id,
                stageId,
                modelName: interestedCar.model,
                c360Information: this.props.c360Information
            }

            return (
                <View style={styles.inner_main_block} key={index}>
                    <View style={{
                        flexDirection: 'row',
                        marginStart: 12,
                        marginEnd: 12,
                        alignContent: 'center',
                    }}>
                        <TouchableOpacity
                            disabled={interestedCar.is_primary == 1}
                            onPress={() => this.changePrimaryCar(interestedCar)}>
                            <Image
                                style={{ width: 20, height: 20, }}
                                resizeMode='contain'
                                source={imagePrimary} />
                        </TouchableOpacity>
                        <View style={{ flexWrap: 'wrap', flex: 1, justifyContent: 'center' }}>
                            <Text style={styles.car_name_title}>{interestedCar.name}</Text>
                        </View>

                        {carEditVisibility && (<TouchableOpacity style={{ margin: 2 }} onPress={() => this.openCarDetail(interestedCar)}>
                            <Image
                                style={{ width: 20, height: 20 }}
                                resizeMode='contain'
                                source={require('../../images/ic_edit_blue_24dp.png')} />
                        </TouchableOpacity>)}
                    </View>
                    <View style={styles.view_straight_line} />

                    {(stage.visible || cancelBookingVisibility) && (
                        <View style={{ marginTop: 16, flexDirection: 'row', marginEnd: 14, alignContent: 'center', justifyContent: 'space-between' }}>
                            {stage.visible && (
                                <View>
                                    <Image
                                        style={{ alignSelf: 'flex-start' }}
                                        source={stage.image} />
                                    <Text style={{ paddingTop: 2, color: '#675816', textAlignVertical: 'center', fontSize: 12, position: 'absolute', alignSelf: 'center' }}>{stage.text}</Text>
                                </View>
                            )}

                            {cancelBookingVisibility && (
                                <TouchableOpacity
                                    style={{ alignSelf: 'flex-end', padding: 10 }}
                                    onPress={() => {
                                        this.mainAction("cancel_booking", info);
                                    }}
                                >
                                    <Text style={{ fontSize: 12, color: '#F44336', textAlignVertical: 'center' }}>Cancel Booking</Text>
                                </TouchableOpacity>
                            )}
                        </View>
                    )}
                    {(tdInfo ? true : false || bookingInfo.visible || postBookingTDVisibility) && (
                        <View style={{
                            marginStart: 32,
                            marginEnd: 12,
                            marginTop: 14
                        }}>
                            {bookingInfo.visible && <Text style={{ lineHeight: 24, fontSize: 14, alignItems: 'flex-end', color: '#7B7B7B' }}>{bookingInfo.text}</Text>}

                            {(tdInfo ? true : false || bookingInfo.visible || postBookingTDVisibility) && (<View style={{
                                flexDirection: 'row', marginTop: 10,
                                alignContent: 'center', justifyContent: 'space-between'
                            }}>
                                {tdInfo && <Text style={{ fontSize: 14, alignItems: 'flex-end', color: '#7B7B7B' }}>{tdInfo}</Text>}
                                {bookingInfo.visible && (
                                    <TouchableOpacity
                                        onPress={() => this.mainAction("details_booking", info)}
                                    >
                                        <View style={styles.booking_detail} >
                                            <Text style={{ color: '#2196f3', fontSize: 11 }}>Booking Details</Text>
                                        </View>
                                    </TouchableOpacity>

                                )}
                                {postBookingTDVisibility && (
                                    <TouchableOpacity
                                        onPress={() => NavigationService.navigate("FormRenderingRouter", {
                                            postBookingTDVisit: true,
                                            leadId: this.state.leadDetails.lead_id, scheduledActivityId: -1111, actionId: 5,
                                            form_title: "Update", lead_last_updated: null, activity_id: 23,
                                            form_action: "Send Otp"
                                        })}
                                    >
                                        <View style={styles.test_drive} >
                                            <Text style={{ color: '#ffffff', fontSize: 11 }}>Test Drive / Visit</Text>
                                        </View>
                                    </TouchableOpacity>
                                )}
                            </View>
                            )}

                        </View>)}
                    {action.visible && (
                        <Button fontSize={16} onPress={() => this.mainAction("update_booking", info)} title={action.text} />
                    )}
                </View>
            )
        }
    }


    changePrimaryCar(item) {
        this.props.changePrimaryCar(item)
    }

    openCarDetail(item) {
        this.props.openCarDetail(item)
    }
}
