package com.salescrm.telephony.model;

import java.util.List;

/**
 * Created by bharath on 1/9/16.
 */
public class AddLeadCarInputData {

    List<CreateLeadInputData.Lead_data.Car_details> newCarInputData;
    List<CreateLeadInputData.Lead_data.Ex_car_details> exCarInputData;
    List<CreateLeadInputData.Lead_data.Ad_car_details> addCarInputData;
    private String buyer_type_id;



    public AddLeadCarInputData() {
    }

    public AddLeadCarInputData(String buyerTypeId, List<CreateLeadInputData.Lead_data.Car_details> newCarInputData, List<CreateLeadInputData.Lead_data.Ex_car_details> exCarInputData, List<CreateLeadInputData.Lead_data.Ad_car_details> addCarInputData) {
        this.newCarInputData = newCarInputData;
        this.exCarInputData = exCarInputData;
        this.addCarInputData = addCarInputData;
        this.buyer_type_id = buyerTypeId;
    }

    public String getBuyer_type_id() {
        return buyer_type_id;
    }

    public void setBuyer_type_id(String buyer_type_id) {
        this.buyer_type_id = buyer_type_id;
    }

    public List<CreateLeadInputData.Lead_data.Car_details> getNewCarInputData() {
        return newCarInputData;
    }

    public void setNewCarInputData(List<CreateLeadInputData.Lead_data.Car_details> newCarInputData) {
        this.newCarInputData = newCarInputData;
    }

    public List<CreateLeadInputData.Lead_data.Ex_car_details> getExCarInputData() {
        return exCarInputData;
    }

    public void setExCarInputData(List<CreateLeadInputData.Lead_data.Ex_car_details> exCarInputData) {
        this.exCarInputData = exCarInputData;
    }

    public List<CreateLeadInputData.Lead_data.Ad_car_details> getAddCarInputData() {
        return addCarInputData;
    }

    public void setAddCarInputData(List<CreateLeadInputData.Lead_data.Ad_car_details> addCarInputData) {
        this.addCarInputData = addCarInputData;
    }

}
