package com.salescrm.telephony.db.etvbr_location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by prateek on 13/11/17.
 */

public class LocationCarModelResult extends RealmObject {
    /*@SerializedName("total_abs")
    @Expose
    private TotalAbs totalAbs;
    @SerializedName("total_rel")
    @Expose
    private TotalRel totalRel;*/
    @SerializedName("locations")
    @Expose
    private RealmList<LocationCarModelDb> locations = null;

    /*public TotalAbs getTotalAbs() {
        return totalAbs;
    }

    public void setTotalAbs(TotalAbs totalAbs) {
        this.totalAbs = totalAbs;
    }

    public TotalRel getTotalRel() {
        return totalRel;
    }

    public void setTotalRel(TotalRel totalRel) {
        this.totalRel = totalRel;
    }*/

    public RealmList<LocationCarModelDb> getLocations() {
        return locations;
    }

    public void setLocations(RealmList<LocationCarModelDb> locations) {
        this.locations = locations;
    }
}
