package com.salescrm.telephony.telephonyModule;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class PermissionDialog extends
        Activity {
    private Handler handler = new Handler(Looper.getMainLooper());
    static Context context = null;
    static AlertDialog alert;
    static AlertDialog.Builder dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.permission_dialog);
        context = this;
        dialog = new AlertDialog.Builder(context);
        dialog.setCancelable(false);
        //showMesage();
    }


    void showMesage() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.d("Dialog Started", "yes");

                dialog.setTitle("Turn off Permission Manager").setMessage("" +
                        "Recordings not getting created !" + "\n" + "Contact Autoninja if you need help.");

                dialog.setPositiveButton("ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int whichButton) {
                                alert.dismiss();
                                finish();
                            }
                        });
                alert = dialog.create();
                alert.setIcon(android.R.drawable.ic_dialog_alert);
                alert.show();

                new CountDownTimer(5000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                    }

                    @Override
                    public void onFinish() {
                    }
                }.start();
            }
        });
    }


}
