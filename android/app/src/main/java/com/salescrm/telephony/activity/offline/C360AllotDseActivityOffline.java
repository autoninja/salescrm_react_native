package com.salescrm.telephony.activity.offline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.LocationsDB;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.create_lead.Lead_data;
import com.salescrm.telephony.db.create_lead.User_details;
import com.salescrm.telephony.db.crm_user.TeamData;
import com.salescrm.telephony.db.crm_user.TeamUserDB;
import com.salescrm.telephony.db.crm_user.UserNewCarDB;
import com.salescrm.telephony.db.crm_user.UsersDB;
import com.salescrm.telephony.model.AddLeadDSEInputData;
import com.salescrm.telephony.model.CreateLeadInputData;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.response.LeadTeamUserResponse;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

import static android.text.TextUtils.isEmpty;

/**
 * Created by bharath on 8/11/16.
 */

public class C360AllotDseActivityOffline extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {
    private String TAG = "AddLeadDSEOfflineCreateLead";
    private AutoCompleteTextView spinnerDseBranch, autoTvSalesManager, autoTvTeamLead, autoTvDseName;
    private Realm realm;
    private Preferences pref = null;
    private int errorColor;
    private String leadColorTag;
    private LeadTeamUserResponse teamUserData;
    private Map<String, String> mangerMap, teamLeadMap, dseMap;
    private ArrayAdapter<Item> managerAdapter, teamLeadAdapter, dseAdapter;
    private Item managerSelectedItem, teamLeadSelectedItem, dseSelectedItem;
    private RealmList<TeamData> teamLeadData;
    private UserNewCarDB teamDateForLocation;
    private int leadNewCarId = 0;
    private int leadSource = 0;
    private Button btSubmit;
    private Lead_data leadData;
    private int scheduledActivityId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allot_dse);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        pref.load(getApplicationContext());

        mangerMap = new HashMap<String, String>();
        teamLeadMap = new HashMap<String, String>();
        dseMap = new HashMap<String, String>();
        spinnerDseBranch = (AutoCompleteTextView) findViewById(R.id.spinner_lead_dse_branch);
        autoTvSalesManager = (AutoCompleteTextView) findViewById(R.id.spinner_lead_sales_man);
        autoTvTeamLead = (AutoCompleteTextView) findViewById(R.id.spinner_lead_team_lead);
        autoTvDseName = (AutoCompleteTextView) findViewById(R.id.spinner_lead_dse_name);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            scheduledActivityId = bundle.getInt("scheduledActivityId", 0);
        }
        SalesCRMRealmTable realmData = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
        if (realmData != null) {
            leadData = realmData.getCreateLeadInputDataDB().getLead_data();
        }

        //setDseBranch
        // setSpinnerArrayAdapter(spinnerDseBranch,getDseLocationData());
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getDseLocationData());
        spinnerDseBranch.setAdapter(adapter);

        spinnerDseBranch.setThreshold(0);
        spinnerDseBranch.setOnFocusChangeListener(this);
        spinnerDseBranch.setOnClickListener(this);

        autoTvSalesManager.setThreshold(0);
        autoTvSalesManager.setOnFocusChangeListener(this);
        autoTvSalesManager.setOnClickListener(this);

        autoTvDseName.setThreshold(0);
        autoTvDseName.setOnFocusChangeListener(this);
        autoTvDseName.setOnClickListener(this);

        autoTvTeamLead.setThreshold(0);
        autoTvTeamLead.setOnFocusChangeListener(this);
        autoTvTeamLead.setOnClickListener(this);

        spinnerDseBranch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                managerSelectedItem = null;
                dseSelectedItem = null;
                teamLeadSelectedItem = null;
                autoTvSalesManager.setText("");
                autoTvTeamLead.setText("");
                autoTvDseName.setText("");
                leadSource = 0;
                leadNewCarId = 0;
                teamDateForLocation = getCrmUser();
                populateAdapters(teamDateForLocation);
                autoTvSalesManager.requestFocus();


            }

        });
        if(leadData!=null&&leadData.getLocation_id()!=null) {
            System.out.println("Position:"+getDseLocationDataSavedPosition());
            spinnerDseBranch.setText(getDseLocationName(leadData.getLocation_id()));
            managerSelectedItem = null;
            dseSelectedItem = null;
            teamLeadSelectedItem = null;
            autoTvSalesManager.setText("");
            autoTvTeamLead.setText("");
            autoTvDseName.setText("");
            leadSource = 0;
            leadNewCarId = 0;
            teamDateForLocation = getCrmUser();
            populateAdapters(teamDateForLocation);
          //  autoTvSalesManager.requestFocus();

        }
        autoTvSalesManager.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(C360AllotDseActivityOffline.this);
                // progressDialog.show();
                //getTeamLeadsAndDSEs();
                managerSelectedItem = managerAdapter.getItem(position);
                dseSelectedItem = null;
                teamLeadSelectedItem = null;
                autoTvTeamLead.setText("");
                autoTvDseName.setText("");
                populateAdaptersForTeam(teamDateForLocation);
                autoTvTeamLead.requestFocus();
            }
        });

        autoTvTeamLead.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(C360AllotDseActivityOffline.this);
                // progressDialog.show();
                autoTvDseName.setText("");
                dseSelectedItem = null;
                teamLeadSelectedItem = teamLeadAdapter.getItem(position);
         /*       System.out.println("Why not calling");*/
                populateAdaptersForDse(teamDateForLocation);
                autoTvDseName.requestFocus();
            }
        });

        autoTvDseName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Util.HideKeyboard(C360AllotDseActivityOffline.this);
                // progressDialog.show();
                //getTeamLeadsAndDSEs();
                // autoTvDseName.requestFocus();
                dseSelectedItem = dseAdapter.getItem(position);
                populateDataOnDseSelect();
            }
        });

        btSubmit = (Button) findViewById(R.id.bt_add_lead_action);


        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDataValid()) {
                    submitAllotDse();
                }

            }
        });

    }
    public void finishActivity(View view){
        onBackPressed();
    }
    private String getDseLocationName(int id) {
        if (realm.where(LocationsDB.class).equalTo("id", id).findAll().size() > 0) {
            return String.valueOf(realm.where(LocationsDB.class).equalTo("id", id).findFirst().getName());
        } else {
            return "";
        }
    }
    private void submitAllotDse() {
        realm.beginTransaction();
        leadData.setLocation_id(Util.getInt(getDseLocationId()));
        leadData.setUser_details(getUserDetails());
        realm.commitTransaction();
        Toast.makeText(this, "Dse Assigned", Toast.LENGTH_SHORT).show();
        C360AllotDseActivityOffline.this.finish();
    }


    private void populateDataOnDseSelect() {

        if (dseSelectedItem != null) {
            TeamData realmData = realm.where(TeamData.class).equalTo("user_id", dseSelectedItem.getId() + "").findFirst();
            if (realmData != null) {
                String managerId = realmData.getManager_id();
                String teamId = realmData.getTeam_leader_id();
                UsersDB realmManagerData = realm.where(UsersDB.class).equalTo("id", managerId).findFirst();
                UsersDB realmTeamData = realm.where(UsersDB.class).equalTo("id", teamId).findFirst();
                if (realmManagerData != null) {
                    managerSelectedItem = new Item(realmManagerData.getName(), realmManagerData.getId());
                    autoTvSalesManager.setText(managerSelectedItem.getText());
                }
                if (realmTeamData != null) {
                    teamLeadSelectedItem = new Item(realmTeamData.getName(), realmTeamData.getId());
                    autoTvTeamLead.setText(teamLeadSelectedItem.getText());
                    populateAdaptersForDse(teamDateForLocation);
                }
            }
        }
    }


    private String[] getDseLocationData() {
        String[] arr = new String[realm.where(LocationsDB.class).findAll().size()];
        for (int i = 0; i < realm.where(LocationsDB.class).findAll().size(); i++) {
            arr[i] = realm.where(LocationsDB.class).findAll().get(i).getName();
        }
        return arr;
    }
    private int getDseLocationDataSavedPosition() {
        for (int i = 0; i < realm.where(LocationsDB.class).findAll().size(); i++) {
            if(leadData.getLocation_id()==realm.where(LocationsDB.class).findAll().get(i).getId()){
                return i;
            }
        }
        return 0;
    }



    public void init() {


    }


    @Override
    public void onClick(View v) {
        if (v instanceof AutoCompleteTextView) {
            if (v.getId() == R.id.spinner_lead_dse_branch) {
                ((AutoCompleteTextView) v).showDropDown();
            } else if (!TextUtils.isEmpty(spinnerDseBranch.getText())) {
                ((AutoCompleteTextView) v).showDropDown();
            }

        }
    }


    private boolean isDataValid() {
         if(TextUtils.isEmpty(spinnerDseBranch.getText().toString())
                || TextUtils.isEmpty(autoTvSalesManager.getText().toString())
                || TextUtils.isEmpty(autoTvTeamLead.getText().toString())
                || TextUtils.isEmpty(autoTvDseName.getText().toString())){
             showAlert("Please fill the entry");
             return false;

         }
        if (!spinnerDseBranch.getText().toString().equalsIgnoreCase("")) {
            if (getDseLocationId().equalsIgnoreCase("")) {
                showAlert("Invalid Dse Branch");
                return false;
            }
        }
        if (!autoTvSalesManager.getText().toString().equalsIgnoreCase("")) {
            if (getSalesManagerId().equalsIgnoreCase("")) {
                showAlert("Invalid SalesManager");
                return false;
            }
        }
        if (!autoTvTeamLead.getText().toString().equalsIgnoreCase("")) {
            if (getTeamLeadId().equalsIgnoreCase("")) {
                showAlert("Invalid Team Lead");
                return false;
            }
        }
        if (!autoTvDseName.getText().toString().equalsIgnoreCase("")) {
            if (getDseId().equalsIgnoreCase("")) {
                showAlert("Invalid Dse Name");
                return false;
            }
        }
        if (!autoTvSalesManager.getText().toString().equalsIgnoreCase("")) {
            if (getDseLocationId().isEmpty() || getSalesManagerId().isEmpty() || getTeamLeadId().isEmpty() || getDseId().isEmpty()) {
                showAlert("Please up valid data");
                return false;
            }
        }
        if (!autoTvTeamLead.getText().toString().isEmpty()) {
            if (getDseLocationId().isEmpty() || getSalesManagerId().isEmpty() || getTeamLeadId().isEmpty() || getDseId().isEmpty()) {
                showAlert("Please up valid data");
                return false;
            }
        }
        if (!autoTvDseName.getText().toString().isEmpty()) {
            if (getDseLocationId().isEmpty() || getSalesManagerId().isEmpty() || getTeamLeadId().isEmpty() || getDseId().isEmpty()) {
                showAlert("Please up valid data");
                return false;
            }
        }
        return true;
    }

    private RealmList<User_details> getUserDetails() {
        RealmList<User_details> list = new RealmList<User_details>();
        User_details user_detailsManager = realm.createObject(User_details.class);
        User_details user_detailsRTeamLead = realm.createObject(User_details.class);
        User_details user_detailsDseLead = realm.createObject(User_details.class);
        if (!TextUtils.isEmpty(getSalesManagerId())) {
            user_detailsManager.setRole_id(WSConstants.MANAGER_ROLE_ID);
            user_detailsManager.setUser_id(getSalesManagerId());
            list.add(user_detailsManager);
        }
        if (!TextUtils.isEmpty(getTeamLeadId())) {
            user_detailsRTeamLead.setRole_id(WSConstants.TEAM_LEAD_ROLE_ID);
            user_detailsRTeamLead.setUser_id(getTeamLeadId());
            list.add(user_detailsRTeamLead);
        }
        if (!TextUtils.isEmpty(getDseId())) {
            user_detailsDseLead.setRole_id(WSConstants.DSE_ROLE_ID);
            user_detailsDseLead.setUser_id(getDseId());
            list.add(user_detailsDseLead);
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.println("List data at :" + i + ":::UserId:" + list.get(i).getUser_id() + ":::RoleId:" + list.get(i).getRole_id());
            //System.out.println(list.get(i).getRole_id());
        }
        return list;

    }


    private String getTeamLeadId() {
        if (teamLeadSelectedItem != null && autoTvTeamLead.getText().toString().equalsIgnoreCase(teamLeadSelectedItem.getText())) {
            return String.valueOf(teamLeadSelectedItem.getId());
        }
        return "";
    }

    private String getDseId() {
        if (dseSelectedItem != null && autoTvDseName.getText().toString().equalsIgnoreCase(dseSelectedItem.getText())) {
            return String.valueOf(dseSelectedItem.getId());
        }
        return "";
    }

    private String getSalesManagerId() {
        if (managerSelectedItem != null && autoTvSalesManager.getText().toString().equalsIgnoreCase(managerSelectedItem.getText())) {
            return String.valueOf(managerSelectedItem.getId());
        }
        return "";
    }


    private String getDseLocationId() {
        if (realm.where(LocationsDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, spinnerDseBranch.getText().toString()).findAll().size() > 0) {
            return String.valueOf(realm.where(LocationsDB.class).equalTo(WSConstants.SRCMRealDbFields.NAME, spinnerDseBranch.getText().toString()).findFirst().getId());
        } else {
            return "";
        }
    }


    private void populateAdapters(UserNewCarDB data) {

        mangerMap.clear();
        dseMap.clear();
        if (data != null) {

            for (int i = 0; i < data.getTeam_data().size(); i++) {
                String managerId = data.getTeam_data().get(i).getManager_id();
                String teamLeadId = data.getTeam_data().get(i).getTeam_leader_id();
                String dseId = data.getTeam_data().get(i).getUser_id();
                RealmResults<UsersDB> userData = data.getUsers().where().equalTo("id", managerId).findAll();
                if (userData.size() > 0) {
                    mangerMap.put(managerId, userData.get(0).getName());
                }
                RealmResults<UsersDB> dseDataData = data.getUsers().where().equalTo("id", dseId).findAll();
                if (userData.size() > 0) {
                    dseMap.put(dseId, dseDataData.get(0).getName());
                }

            }
            List<Item> dataForTheAdapter = new ArrayList<Item>();
            for (Object o : mangerMap.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                dataForTheAdapter.add(new Item(pair.getValue().toString(), pair.getKey().toString()));
            }
            managerAdapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, dataForTheAdapter);
            autoTvSalesManager.setAdapter(managerAdapter);

            List<Item> dataForTheAdapterDse = new ArrayList<Item>();
            for (Object o : dseMap.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                dataForTheAdapterDse.add(new Item(pair.getValue().toString(), pair.getKey().toString()));
            }
            dseAdapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, dataForTheAdapterDse);
            autoTvDseName.setAdapter(dseAdapter);
//            autoTvSalesManager.requestFocus();
        }
    }

    private void populateAdaptersForTeam(UserNewCarDB data) {
        teamLeadMap.clear();
        if (data != null) {
            RealmResults<TeamData> teamData = data.getTeam_data().where().equalTo("manager_id", getSalesManagerId()).findAll();
            for (int i = 0; i < teamData.size(); i++) {
                String teamLeadId = teamData.get(i).getTeam_leader_id();
                RealmResults<UsersDB> userData = data.getUsers().where().equalTo("id", teamLeadId).findAll();
                if (userData.size() > 0) {
                    teamLeadMap.put(teamLeadId, userData.get(0).getName());
                }

            }
            List<Item> dataForTheAdapter = new ArrayList<Item>();
            for (Object o : teamLeadMap.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                dataForTheAdapter.add(new Item(pair.getValue().toString(), pair.getKey().toString()));
            }
            teamLeadAdapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, dataForTheAdapter);
            autoTvTeamLead.setAdapter(teamLeadAdapter);
//            autoTvTeamLead.requestFocus();
        }

    }

    private void populateAdaptersForDse(UserNewCarDB data) {
        System.out.println("Called");
        dseMap.clear();
        if (data != null) {
            System.out.println("Called");

            RealmResults<TeamData> teamData = data.getTeam_data().where().equalTo("manager_id", getSalesManagerId()).equalTo("team_leader_id", getTeamLeadId()).findAll();
            for (int i = 0; i < teamData.size(); i++) {
                String dseId = teamData.get(i).getUser_id();
                RealmResults<UsersDB> userData = data.getUsers().where().equalTo("id", dseId).findAll();
                if (userData.size() > 0) {
                    dseMap.put(dseId, userData.get(0).getName());
                }

            }
            List<Item> dataForTheAdapter = new ArrayList<Item>();
            for (Object o : dseMap.entrySet()) {
                Map.Entry pair = (Map.Entry) o;
                dataForTheAdapter.add(new Item(pair.getValue().toString(), pair.getKey().toString()));
            }
            dseAdapter = new ArrayAdapter<Item>(this, android.R.layout.simple_list_item_1, dataForTheAdapter);
            autoTvDseName.setAdapter(dseAdapter);
            // autoTvDseName.requestFocus();
        }

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus && v instanceof AutoCompleteTextView && isEmpty(((AutoCompleteTextView) v).getText().toString())) {
            if (v.getId() == R.id.spinner_lead_dse_branch) {
                ((AutoCompleteTextView) v).showDropDown();
            } else if (!TextUtils.isEmpty(spinnerDseBranch.getText())) {
                ((AutoCompleteTextView) v).showDropDown();
            }
        }
    }


    private UserNewCarDB getCrmUser() {
        return realm.where(TeamUserDB.class).equalTo("locationId", Util.getInt(getDseLocationId())).findFirst().
                getUserNewCarDBList().where().equalTo("leadNewCar", leadNewCarId).equalTo("leadSource", leadSource).findFirst();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public class Item {

        private String mText;
        private int mId;

        public Item(String text, String id) {

            this.mText = text;
            this.mId = Integer.parseInt(id);
        }

        public int getId() {

            return mId;
        }

        public void setId(String id) {

            this.mId = Integer.parseInt(id);
        }

        public String getText() {

            return mText;
        }

        public void setText(String text) {

            this.mText = text;
        }

        @Override
        public String toString() {

            return this.mText;
        }
    }
    private void showAlert(String text){
        Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
    }
}