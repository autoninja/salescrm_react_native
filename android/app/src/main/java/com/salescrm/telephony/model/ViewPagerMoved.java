package com.salescrm.telephony.model;

/**
 * Created by bharath on 1/9/16.
 */
public class ViewPagerMoved {

    private int position;

    public ViewPagerMoved(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
