import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList, Alert, TouchableOpacity, Image } from 'react-native';
import TodayTasksUsedCarExchangeHeader from '../../../components/todaysTasks/todays_tasks_exchange_header'
import TodaysTaskItemExpandableExchange from '../../../components/todaysTasks/todays_task_item_expandable_exchange'
import TodaysTaskItemChildExchange from '../../../components/todaysTasks/todays_task_item_child_exchange'
import TodayTaskItemResultExchange from '../../../components/todaysTasks/todays_task_item_result_exchange'
//import TodaysTasksFooter from '../../../components/todaysTasks/todays_tasks_footer'

export default class TodaysTasksChild extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isRefreshing: false };
  }

  refresh() {
    this.setState({ isRefreshing: true });
    this.props.refresh();
  }
  render() {
    //console.error(JSON.stringify(this.props.data));
    let mainView = null;
    if (this.props.data) {
      let dataArray = this.props.data.evaluation_managers;
      let locationId = this.props.data.location_id;
      let locationData = this.props.data.location_abs;
      //if (this.props.expandable) {
      if (dataArray.length > 1) {
        mainView = <FlatList
          showsVerticalScrollIndicator={false}
          data={dataArray}
          renderItem={({ item, index }) => {

            let renderView = <TodaysTaskItemExpandableExchange
              onLongPress={this.props.onLongPress.bind(this)}
              evaluation_managers={item}
              info={
                {
                  id: item.info.id,
                  dp_url: item.info.dp_url,
                  name: item.info.name,
                  location_id: locationId,
                  user_role_id: 31
                }
              }
              abs={
                {
                  ed: item.info.abs.evaluations,
                  pq: item.info.abs.price_negotiations,
                  pending_total_count: item.info.abs.pending_total_count
                }
              }
            />
            if (index == dataArray.length - 1) {
              //Return with total & bottomView
              return (
                <View>
                  {renderView}
                  <TodayTaskItemResultExchange
                    abs={
                      {
                        ed: locationData.evaluations,
                        pq: locationData.price_negotiations,
                        pending_total_count: locationData.pending_total_count
                      }
                    }
                  />
                  {/* <TodaysTasksFooter /> */}
                </View>);
            }
            else {
              return (renderView);
            }

          }
          }
          keyExtractor={(item, index) => index.toString()}
          onRefresh={() => this.refresh()}
          refreshing={this.state.isRefreshing}
        />
      }
      else {
        mainView = <FlatList
          showsVerticalScrollIndicator={false}
          data={dataArray[0].evaluators}
          renderItem={({ item, index }) => {

            let renderView = <TodaysTaskItemChildExchange
              onLongPress={this.props.onLongPress.bind(this)}
              info={
                {
                  id: item.info.id,
                  dp_url: item.info.dp_url,
                  name: item.info.name,
                  location_id: locationId,
                  user_role_id: 4
                }
              }
              abs={
                {
                  ed: item.info.abs.evaluations,
                  pq: item.info.abs.price_negotiations,
                  pending_total_count: item.info.abs.pending_total_count
                }
              }
            />
            if (dataArray[0].evaluators.length == 1) {
              //Just salesConsultant
              //Return with bottom view
              return (<View>{renderView}</View>);
              //return (<View>{renderView}<TodaysTasksFooter /></View>);
            }
            else if (index == dataArray[0].evaluators.length - 1) {
              //Return with total & bottomView
              return (
                <View>
                  {renderView}
                  <TodayTaskItemResultExchange
                    abs={
                      {
                        ed: dataArray[0].info.abs.evaluations,
                        pq: dataArray[0].info.abs.price_negotiations,
                        pending_total_count: dataArray[0].info.abs.pending_total_count
                      }
                    }
                  />
                  {/* <TodaysTasksFooter /> */}
                </View>);
            }
            else {
              return (renderView);
            }

          }
          }
          keyExtractor={(item, index) => index.toString()}
          onRefresh={() => this.refresh()}
          refreshing={this.state.isRefreshing}
        />

      }
    }

    return (

      <View style={{ padding: 4, flex: 1 }}>
        <TodayTasksUsedCarExchangeHeader />
        <View style={{ flex: 1, marginTop: 6 }}>
          {mainView}
        </View>
      </View>
    );
  }
}
