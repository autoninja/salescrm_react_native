import React, { Component } from "react";
import { View, Image, Text, ActivityIndicator, TouchableOpacity, FlatList, Platform, NativeModules } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import RestClient from "../../network/rest_client";
import Button from "../../components/uielements/Button";
import styles from "./Search.styles";
import SearchResultItem from './SearchResultItem'
import NavigationService from "../../navigation/NavigationService";
export default class SearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true, result: [], apiError: false }
  }

  componentDidMount() {
    this.getSearchResult();
    this._mounted = true;
  }
  componentWillUnmount() {
    this._mounted = false;
  }
  getSearchResult() {
    this.setState({ loading: true })
    let { mobile_number, customer_name, lead_id, email_id } = this.props.navigation.getParam('info');
    new RestClient().postSearchResult({
      searchData: {
        mobile: mobile_number,
        enquiry: "",
        customer: customer_name,
        lead: lead_id,
        email: email_id
      },
      is_mobile_request: true
    })
      .then((data) => {
        if (!this._mounted) {
          return;
        }
        let loading = false;
        let result = [];
        let apiError = false;
        console.log("Data Search Result");
        console.log(JSON.stringify(data));
        if (data && data.statusCode == 2002 && data.result) {
          //Success
          result = data.result;
        }
        else {
          apiError = true;
        }
        this.setState({ loading, result, apiError })

      }).catch(error => {
        //console.error(error);
        if (!this._mounted) {
          return;
        }
        this.setState({ loading: false, apiError: true })
      });
  }
  openC360(leadId) {
    if (Platform.OS == "android") {
      NativeModules.ReactNativeToAndroid.navigateToC360FromDetailsDashboard(String(leadId),
        null
      );
    }
    else {
      NavigationService.navigate("C360MainScreen", { leadId });
    }
  }
  addLeadButtonVisibility() {
    let { uniqueAcrossDealerShip, addLeadEnabled } = this.props;
    let { mobile_number } = this.props.navigation.getParam('info');
    let { result } = this.state;
    console.log("addLeadEnabled:" + addLeadEnabled)
    if (!addLeadEnabled || !mobile_number) {
      return false;
    }
    else {

      let visibilty = false;
      for (i = 0; i < result.length; i++) {
        if (result[i].status == "PRIMARY") {
          if (result[i].active == 0) {
            visibilty = true;
          }
          //active == 1
          else if (!uniqueAcrossDealerShip && !result[i].my_location_lead) {
            visibilty = true;
          }
          else {
            visibilty = false;
            break;
          }
        }
        else {
          visibilty = true;
        }
      }
      return visibilty;
    }
  }
  addLead(mobile_number) {
    if (Platform.OS == "ios") {
      NavigationService.resetScreen("CreateEnquiryRouter", { primaryMobile: mobile_number });
    }
    else {
      NativeModules.ReactNativeToAndroid.startCreateLeadActivity(mobile_number + "");
      this.props.navigation.pop(2);
    }
  }
  addColdVisit(mobile_number) {
    if (Platform.OS == "ios") {
      NavigationService.resetScreen("ColdVisitRouter", { mobileNumber: mobile_number });
    }
    else {
      NativeModules.ReactNativeToAndroid.startColdVisitActivity(mobile_number + "");
      this.props.navigation.pop(2);
    }
  }
  render() {
    let { mobile_number } = this.props.navigation.getParam('info');
    let alertText = "";
    let { loading, result, apiError } = this.state;
    console.log(`is cold visit enabled ${this.props.coldVisitEnabled}`);
    if (apiError) {
      alertText = "Error getting the data";
    }
    else if (result.length == 0) {
      alertText = "No Lead found";
    }
    else {
      alertText = result.length + " result found";
    }
    return (
      <LinearGradient
        colors={["#2e5e86", "#2e3486"]}
        style={styles.linearGradient}
      >
        <View style={styles.main}>
          {loading && (
            <ActivityIndicator
              animating={loading}
              style={{ alignSelf: 'center', }}
              color="#fff"
              size="small"
              hidesWhenStopped={true}

            />
          )}
          {(!loading && (apiError || result.length == 0)) ?
            <View style={{ alignItems: 'center' }}>
              <Text style={{ fontSize: 16, color: "#eee", alignSelf: 'center' }}>{alertText}</Text>
              {(apiError) ?
                <Button
                  title={"Retry"}
                  onPress={() => {
                    this.getSearchResult();
                  }}
                />
                : null}
              {(!apiError && !mobile_number.isEmpty() && this.props.addLeadEnabled) ?
                <Button
                  title={"Add Lead"}
                  onPress={() => {
                    this.addLead(mobile_number);
                  }}
                />
                : null}
              {(!apiError && !mobile_number.isEmpty() && this.props.coldVisitEnabled) ?

                <Button
                  title="Add Cold Visit"
                  onPress={() => {
                    this.addColdVisit(mobile_number);
                  }}
                />
                : null}
            </View>
            :
            (!loading && !apiError && result.length > 0) ?
              <View>
                <Text style={{ fontSize: 14, color: "#eee", alignSelf: 'flex-start' }}>{alertText}</Text>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={result}
                  renderItem={({ item, index }) => {
                    if (this.props.showLeadCompetitor) {
                      return (
                        <SearchResultItem searchResult={item} openC360={this.openC360.bind(this, item.leadId)} />
                      )
                    }
                    else if (item.my_location_lead) {
                      return (
                        <SearchResultItem searchResult={item} openC360={this.openC360.bind(this, item.leadId)} />
                      )
                    }


                  }}
                  keyExtractor={(item, index) => index + ''}
                />
                {(this.addLeadButtonVisibility()) ?
                  <View>
                    <Button
                      title="Add Lead"
                      onPress={() => {
                        this.addLead(mobile_number);
                      }}
                    />
                    {(this.props.coldVisitEnabled) ?

                      <Button
                        title="Add Cold Visit"
                        onPress={() => {
                          this.addColdVisit(mobile_number);
                        }}
                      />
                      : null}
                  </View>
                  : null}


              </View>
              : null}

        </View>
      </LinearGradient>
    );
  }
}
String.prototype.isEmpty = function () {
  return (this.length === 0 || !this.trim());
};
