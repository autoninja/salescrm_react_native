package com.salescrm.telephony.activity.createEnquiry;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.google.gson.Gson;
import com.salescrm.telephony.R;
import com.salescrm.telephony.activity.C360Activity;
import com.salescrm.telephony.activity.HomeActivity;
import com.salescrm.telephony.application.SalesCRMApplication;
import com.salescrm.telephony.db.EvaluationManagerResultData;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.UserLocationsDB;
import com.salescrm.telephony.db.car.CarBrandModelVariantsDB;
import com.salescrm.telephony.db.car.ExchangeCarBrandModelsDB;
import com.salescrm.telephony.db.car.ExchangeCarBrandsDB;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.interfaces.FetchC360OnCreateLeadListener;
import com.salescrm.telephony.offline.FetchC360InfoOnCreateLead;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;
import com.salescrm.telephony.utils.WSConstants;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class RNUpdateExchangeCarDetailActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {

    private ReactInstanceManager mReactInstanceManager;
    private ReactRootView mReactRootView;
    private Realm realm;
    private Preferences pref;
    private ProgressDialog progressDialog;
    private boolean UPDATE_WITHOUT_CHANGING_ACTIVITY = false;
    private IntentFilter intentFilter = new IntentFilter();
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals("ON_UPDATE_RNUpdateExchangeCarDetailActivity")) {
                //progressDialog.show();
                //System.out.println(" Leeeeeee "+intent.getStringExtra("leadId"));
                //System.out.println("Calleddd...... RNUpdateExchangeCarDetailActivity");
                String leadId = intent.getStringExtra("leadId");
                String scheduledActivityId = intent.getStringExtra("scheduled_activity_id");
                //pref.setLeadID(leadId);
                //Intent intentHome = new Intent("Refresh_HomeActvity");
                //intent.putExtra("leadId", leadId);
                //sendBroadcast(intentHome);
                if (scheduledActivityId != null) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            SalesCRMRealmTable dbRow = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", Integer.parseInt(scheduledActivityId)).findFirst();
                            if (dbRow != null) {
                                /*if (actionId == 5) {

                                } else {
                                    dbRow.deleteFromRealm();
                                }*/
                                dbRow.setDone(true);
                            }
                        }
                    });

                }

                //Intent intentHome = new Intent(RNUpdateExchangeCarDetailActivity.this, HomeActivity.class);
                //intentHome.putExtra("leadId", 111);
                fetchC360(leadId);
                //startActivity(intentHome);
                //RNUpdateExchangeCarDetailActivity.this.finish();
            }else if(action != null && action.equals("CLOSE_RNUpdateExchangeCarDetailActivity")){
                RNUpdateExchangeCarDetailActivity.this.finish();
            }
        }
    };

    @Override
    public void invokeDefaultOnBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        pref = Preferences.getInstance();
        Preferences.load(this);
        setContentView(R.layout.activity_rn_update_exchange_car_detail);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Redirecting to Home !!!.. Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        intentFilter.addAction("ON_UPDATE_RNUpdateExchangeCarDetailActivity");
        intentFilter.addAction("CLOSE_RNUpdateExchangeCarDetailActivity");

        if(getIntent().getExtras() != null){
            UPDATE_WITHOUT_CHANGING_ACTIVITY = getIntent().getExtras().getBoolean("update_without_changing_activity");
        }

        mReactInstanceManager =
                ((SalesCRMApplication) getApplication()).getReactNativeHost().getReactInstanceManager();
        Bundle initialProps = new Bundle();
        initialProps.putBoolean("fromAndroid", true);
        initialProps.putString("token", Preferences.getAccessToken());
        initialProps.putString("module", "UPDATE_EXCHANGE_CAR_DETAIL");
        initialProps.putString("userLocations", new Gson().toJson(getLocationList()));
        initialProps.putString("allVehicles", new Gson().toJson(getAllVehicles()));
        initialProps.putString("evaluationManagers", new Gson().toJson(getEvaluationManager()));
        initialProps.putString("leadId", new Gson().toJson(pref.getLeadID()));
        initialProps.putBoolean("updateWithoutChangingActivity", UPDATE_WITHOUT_CHANGING_ACTIVITY);
        mReactRootView = findViewById(R.id.react_root_view_update_exchange_car);
        mReactRootView.startReactApplication(
                mReactInstanceManager,
                "NinjaCRMSales",
                initialProps
        );
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        mReactInstanceManager.onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this, this);
        }

        try {
            registerReceiver(broadcastReceiver, intentFilter);

        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }
        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }

    @Override
    public void onBackPressed() {
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause();
        }

        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // this.finish(); // close this activity and return to preview activity (if there is any)
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchC360(String leadId) {
        progressDialog.setMessage("Redirecting to Home !!!.. Please wait");
        progressDialog.show();
        //this.generatedLeadId = leadId;

        ArrayList<String> leadData = new ArrayList<>();
        leadData.add(leadId);
        new FetchC360InfoOnCreateLead(new FetchC360OnCreateLeadListener() {
            @Override
            public void onFetchC360OnCreateLeadSuccess() {
                progressDialog.dismiss();
                //Preferences.setLeadID("" + generatedLeadId);
                SalesCRMRealmTable salesCRMRealmTable = realm.where(SalesCRMRealmTable.class)
                        .equalTo("leadId", Util.getInt(leadId)).findFirst();
                if (salesCRMRealmTable != null) {
                    Preferences.setScheduledActivityId(salesCRMRealmTable.getScheduledActivityId());

                }
                //Intent intent = new Intent(RNUpdateExchangeCarDetailActivity.this, HomeActivity.class);
                //intent.putExtra("from_exchange_car", true);
                //startActivity(intent);
                pref.setReloadC360(true);
                RNUpdateExchangeCarDetailActivity.this.finish();
            }

            @Override
            public void onFetchC360OnCreateLeadError() {
                progressDialog.dismiss();
                Util.showToast(getApplicationContext(), "Failed to fetch Home information", Toast.LENGTH_SHORT);
                RNUpdateExchangeCarDetailActivity.this.finish();

            }
        }, getApplicationContext(), leadData, pref.getAppUserId()).call(true);

    }

    private Locations[] getLocationList() {
        RealmResults<UserLocationsDB> data =
                realm.where(UserLocationsDB.class).distinct("location_id");
        Locations[] userLocations = new Locations[data != null ? data.size() : 0];
        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i) != null) {
                    userLocations[i] = new Locations(Util.getInt(data.get(i).getLocation_id()), data.get(i).getName());
                }
            }
        }

        return userLocations;

    }
    private List<VehicleModel> getAllVehicles() {
        List<VehicleModel> brands = new ArrayList<>();
        RealmResults<ExchangeCarBrandsDB> brandsDB = realm.where(ExchangeCarBrandsDB.class).findAll();
        //System.out.println("BrandsDBLength:" + brandsDB.size());
        if (brandsDB != null) {
            for (ExchangeCarBrandsDB brand : brandsDB) {
                List<VehicleModelsModel> models = new ArrayList<>();
                for (ExchangeCarBrandModelsDB model : brand.getBrand_models()) {
                    if (model.getCategory().equalsIgnoreCase(WSConstants.CAR_CATEGORY_BOTH)
                            || model.getCategory().equalsIgnoreCase(WSConstants.CAR_CATEGORY_OLD_CAR)) {

                        List<VehicleModelsVariantModel> variants = new ArrayList<>();
                        for (CarBrandModelVariantsDB variant : model.getCar_variants()) {
                            variants.add(new VehicleModelsVariantModel(
                                    Util.getInt(variant.getVariant_id()),
                                    variant.getVariant(),
                                    Util.getInt(variant.getFuel_type_id()),
                                    variant.getFuel_type(),
                                    Util.getInt(variant.getColor_id()),
                                    variant.getColor()));
                        }

                        models.add(new VehicleModelsModel(Util.getInt(model.getModel_id()),
                                model.getModel_name(), model.getCategory(), variants));
                    }
                }
                brands.add(new VehicleModel(Util.getInt(brand.getBrand_id()), brand.getBrand_name(), models));
            }

        }
        return brands;
    }

    private List<EvaluationManager> getEvaluationManager(){
        List<EvaluationManager> evaluationManagers = new ArrayList<>();
        RealmResults<EvaluationManagerResultData> evaluationManagerResultData = realm.where(EvaluationManagerResultData.class).findAll();
        for(int i =0; i< evaluationManagerResultData.size(); i++){
            evaluationManagers.add(new EvaluationManager(evaluationManagerResultData.get(i).getLocationId(), evaluationManagerResultData.get(i).isHasEvaluationManager()));
        }
        return evaluationManagers;
    }

    class EvaluationManager{
        private String locationId;
        private boolean hasEvaluationManager;

        public EvaluationManager(String locationId, boolean hasEvaluationManager) {
            this.locationId = locationId;
            this.hasEvaluationManager = hasEvaluationManager;
        }
    }

    private class Locations {
        private int id;
        private String text;

        public Locations(int id, String text) {
            this.id = id;
            this.text = text;
        }
    }

    class VehicleModel {
        int brand_id;
        String brand_name;
        List<VehicleModelsModel> brand_models;

        VehicleModel(int brand_id, String brand_name, List<VehicleModelsModel> brand_models) {
            this.brand_id = brand_id;
            this.brand_name = brand_name;
            this.brand_models = brand_models;
        }
    }

    class VehicleModelsModel {
        int model_id;
        String model_name;
        String category;
        List<VehicleModelsVariantModel> car_variants;

        VehicleModelsModel(int model_id, String model_name, String category,
                           List<VehicleModelsVariantModel> car_variants) {
            this.model_id = model_id;
            this.model_name = model_name;
            this.category = category;
            this.car_variants = car_variants;
        }
    }

    class VehicleModelsVariantModel {
        int variant_id;
        String variant;
        int fuel_type_id;
        String fuel_type;
        int color_id;
        String color;

        VehicleModelsVariantModel(int variant_id, String variant,
                                  int fuel_type_id, String fuel_type, int color_id, String color) {
            this.variant_id = variant_id;
            this.variant = variant;
            this.fuel_type_id = fuel_type_id;
            this.fuel_type = fuel_type;
            this.color_id = color_id;
            this.color = color;
        }
    }
}
