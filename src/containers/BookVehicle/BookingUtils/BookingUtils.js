export const BOOKING_STAGES = {
    STAGE_INIT: 0,
    STAGE_BOOKED: 1,
    STAGE_CANCELLED: 2,
    STAGE_FULL_PAYMENT_RECEIVED: 3,
    STAGE_INVOICED: 4,
    STAGE_DELIVERED: 5
}
export const BOOKING_ROUTES = {
    BookVehicleMain: "BookVehicleMain",
    TotalBookingSelector: "TotalBookingSelector",
    BookingPopups: "BookingPopups",
    Submission: "Submission",
    UpdateInvoice: "UpdateInvoice",
    CancelBooking: "CancelBooking"
}

export const TASK_ACTIVITY_NAME = {
    NO_CAR_BOOKED_ID: 34,
    RECEIVE_PAYMENT_ID: 35,
    INVOICE_REQUEST_ID: 36,
    DELIVERY_REQUEST_ID: 37
}