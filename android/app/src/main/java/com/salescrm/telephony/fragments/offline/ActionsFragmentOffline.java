package com.salescrm.telephony.fragments.offline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.db.SalesCRMRealmTable;
import com.salescrm.telephony.db.create_lead.Activity_data;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.Util;

import java.text.SimpleDateFormat;
import java.util.Locale;

import io.realm.Realm;

/**
 * Created by bharath on 28/12/16.
 */
public class ActionsFragmentOffline extends Fragment {

    private Realm realm;
    private Preferences pref;
    private View view;
    private FrameLayout frameNoTask;
    private LinearLayout llTask, llDynamic;
    private TextView tvPlannedTask, tvDate, tvTaskName, tvDynamic;
    private Activity_data data;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.planed_action_fragment_offline, container, false);
        realm = Realm.getDefaultInstance();
        frameNoTask = (FrameLayout) view.findViewById(R.id.frame_c360_no_plan_actions);
        llTask = (LinearLayout) view.findViewById(R.id.ll_c360_plan_actions);
        tvPlannedTask = (TextView) view.findViewById(R.id.tv_plan_task_title);
        tvTaskName = (TextView) view.findViewById(R.id.tv_plan_task_name);
        tvDate = (TextView) view.findViewById(R.id.tv_plan_action_date);
        tvDynamic = (TextView) view.findViewById(R.id.tv_planed_task_dynamic);
        llDynamic = (LinearLayout) view.findViewById(R.id.ll_plan_task_dynamic);
        return view;
    }

    @Override
    public void onResume() {
        updateViews();
        super.onResume();
    }

    private void updateViews() {
        view.invalidate();

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            int scheduledActivityId = bundle.getInt("scheduledActivityId", 0);
            SalesCRMRealmTable realmData = realm.where(SalesCRMRealmTable.class).equalTo("scheduledActivityId", scheduledActivityId).findFirst();
            if (realmData != null) {
                data = realmData.getCreateLeadInputDataDB().getActivity_data();
            }
        }
        if (data != null) {
            if (data.getActivity() == null || data.getActivity().equalsIgnoreCase("")) {
                frameNoTask.setVisibility(View.VISIBLE);
                tvPlannedTask.setVisibility(View.GONE);
                llTask.setVisibility(View.GONE);
            } else {
                frameNoTask.setVisibility(View.GONE);
                tvPlannedTask.setVisibility(View.VISIBLE);
                llTask.setVisibility(View.VISIBLE);
                addDataToView();


            }
        }
    }

    private void addDataToView() {
        llDynamic.removeAllViews();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("E dd/MM hh:mm a", Locale.getDefault());
        SimpleDateFormat dateFormatterLeft = new SimpleDateFormat("E dd/MM\nhh:mm a", Locale.getDefault());
        if(data.getScheduledDateTime()!=null){
            String formattedDate = dateFormatterLeft.format(Util.getDate(data.getScheduledDateTime()));
           tvDate.setText(formattedDate);
        }


        tvTaskName.setText(data.getActivityName());
        if (Util.isNotNull(data.getAssignName())) {
            addView("Assignee: " + data.getAssignName());
        }
        if (Util.isNotNull(data.getCarName())) {
            addView("Car: " + data.getCarName());
        }

        if (Util.isNotNull(data.getVisit_type())) {
            if (data.getVisit_type().equalsIgnoreCase("1")) {
                addView("Visit Type: HOME VISIT");
            } else {
                addView("Visit Type: SHOWROOM VISIT");
            }
        }
        if (Util.isNotNull(data.getVisit_time())) {
            if(data.getVisit_time()!=null){
                String formattedDate = dateFormatter.format(Util.getDate(data.getVisit_time()));
                addView("Test Drive Time: " + formattedDate);
            }

        }
        addView("Remarks: " + data.getRemarks());


    }

    private void addView(String s) {
        TextView textView = new TextView(getContext());
        textView.setText(s);
        textView.setVisibility(View.VISIBLE);
        textView.setLayoutParams(tvDynamic.getLayoutParams());
        llDynamic.addView(textView);
    }
}
