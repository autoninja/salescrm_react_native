package com.salescrm.telephony.response;

import java.util.List;

public class ActiveEventsResponse {

    private Result result;

    private String error;

    private String message;

    private String statusCode;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "ClassPojo [result = " + result + ", error = " + error + ", message = " + message + ", statusCode = " + statusCode + "]";
    }

    public class Result
    {
        private List<Locations> locations;

        public List<Locations> getLocations() {
            return locations;
        }

        public void setLocations(List<Locations> locations) {
            this.locations = locations;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [locations = "+locations+"]";
        }
    }

    public class Locations
    {
        private String name;

        private List<Lead_source_categories> lead_source_categories;

        private int id;

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public List<Lead_source_categories> getLead_source_categories() {
            return lead_source_categories;
        }

        public void setLead_source_categories(List<Lead_source_categories> lead_source_categories) {
            this.lead_source_categories = lead_source_categories;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [name = "+name+", lead_source_categories = "+lead_source_categories+", id = "+id+"]";
        }
    }

    public class Lead_source_categories
    {
        private List<Lead_sources> lead_sources;

        private String name;

        private int id;

        public List<Lead_sources> getLead_sources() {
            return lead_sources;
        }

        public void setLead_sources(List<Lead_sources> lead_sources) {
            this.lead_sources = lead_sources;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [lead_sources = "+lead_sources+", name = "+name+", id = "+id+"]";
        }
    }

    public class Lead_sources
    {
        private String name;

        private int id;

        private List<Events> events;

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public List<Events> getEvents ()
        {
            return events;
        }

        public void setEvents (List<Events> events)
        {
            this.events = events;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [name = "+name+", id = "+id+", events = "+events+"]";
        }
    }

    public class Events
    {
        private String end_date;

        private String name;

        private int id;

        private String start_date;

        public String getEnd_date ()
        {
            return end_date;
        }

        public void setEnd_date (String end_date)
        {
            this.end_date = end_date;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getStart_date ()
        {
            return start_date;
        }

        public void setStart_date (String start_date)
        {
            this.start_date = start_date;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [end_date = "+end_date+", name = "+name+", id = "+id+", start_date = "+start_date+"]";
        }
    }



}