package com.salescrm.telephony.model;

/**
 * Created by bannhi on 25/9/17.
 */

public class Locations {

    private int id;
    private String locName;

    public Locations(){

    }
    public Locations(int id, String locName){
        this.id = id;
        this.locName = locName;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocName() {
        return locName;
    }

    public void setLocName(String locName) {
        this.locName = locName;
    }
}
