import React, { Component } from 'react';

import { Platform, StyleSheet, View, Text, Modal, Button, TouchableOpacity, Alert, FlatList} from 'react-native';
import GamificationConfigurationService from '../storage/gamification_configuration_service'
import styles from '../styles/styles'
import * as async_storage from '../storage/async_storage';

export default class GameLocationPickerDialog extends Component {

  constructor(props) {
  super(props);
  var locations = GamificationConfigurationService.getLocations();
  this.state = {locations}
  }


  _applyOrCloseFilter(navigation,item) {
      if(!item) {
          navigation.goBack();
          return;
      }
      async_storage.setSelectedGameLocationId(item.id.toString()).then(()=>{
        global.selectedGameLocationId = item.id;
        navigation.state.params.callback();
        navigation.goBack();
      });

  }


  render () {
    let navigation = this.props.navigation;


    return (

            <View style={styles.Alert_Main_View}>

            <View style = {{backgroundColor:'#2e3486',paddingTop:20, borderRadius:6,}}>
            <Text style = {{color:'#fff', marginBottom:20, fontSize:18, fontWeight:'bold', textAlign:'center'}}> Select Location </Text>
            <View style= {{margin:10}}>
            <FlatList
            extraData = {this.state.locations}
            data = {this.state.locations}
            renderItem={({item}) =>
            <TouchableOpacity onPress = { ()=> this._applyOrCloseFilter(this.props.navigation, item)}>
            <View style={{backgroundColor:'#2e5e86', borderRadius:4, padding:4, margin:10}}>
            <Text style = {{color:'#fff', padding:10}}> {item.name} </Text>
            </View>
            </TouchableOpacity>

          }
            keyExtractor={(item, index) => index+''}

            />
            </View>

           <View style= {{flexDirection:'row', alignItems:'center',height:60}}>
           <TouchableOpacity style = {{flex:1, alignItems:'center'}} onPress = { ()=> this._applyOrCloseFilter(this.props.navigation)}>
           <Text style={{color:'#fff', fontSize:16}}> Close </Text>
           </TouchableOpacity>

           </View>

            </View>

            </View>

    );
  }

}
