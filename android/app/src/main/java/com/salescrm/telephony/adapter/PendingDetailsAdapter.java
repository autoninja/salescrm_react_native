package com.salescrm.telephony.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.db.PendingStatsDB;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import io.realm.RealmList;

import static com.salescrm.telephony.R.id.call_designation;
import static com.salescrm.telephony.R.id.call_name;
import static com.salescrm.telephony.R.id.img_user;
import static com.salescrm.telephony.R.id.lower_most;
import static com.salescrm.telephony.R.id.middle_most;
import static com.salescrm.telephony.R.id.top_most;

/**
 * Created by bannhi on 12/9/17.
 */

public class PendingDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity activity;
    AlertDialog alertDialog;
    RealmList<PendingStatsDB> pendingStatsDBRealmList;
    private Preferences pref;
    private static final int TYPE_FOOTER = 2;
    private static final int TYPE_ITEM = 1;

    public PendingDetailsAdapter() {

    }

    public PendingDetailsAdapter(Activity activity, RealmList<PendingStatsDB> pendingStatsDBRealmList) {
        this.activity = activity;
        this.pendingStatsDBRealmList = pendingStatsDBRealmList;
        this.pref = Preferences.getInstance();
        pref.load(activity);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       /* View view = View.inflate(parent.getContext(), R.layout.pending_row, null);
        return new PendingDetailsAdapter.PendingItemHolder(view);*/

         if(viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from (parent.getContext ()).inflate (R.layout.footer_item, parent, false);
            return new PendingDetailsAdapter.FooterViewHolder (v);
        } else if(viewType == TYPE_ITEM) {
             View view = LayoutInflater.from (parent.getContext ()).inflate (R.layout.pending_row, parent, false);
                     //View.inflate(parent.getContext(), R.layout.pending_row, null);
             return new PendingDetailsAdapter.PendingItemHolder(view);
        }
        return null;
    }
    @Override
    public int getItemViewType (int position) {
        if(isPositionFooter (position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }
    private boolean isPositionFooter (int position) {
        if(pendingStatsDBRealmList!=null) {
            return position == pendingStatsDBRealmList.size();
        }
        return false;
    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
      /*  final RecyclerView.ViewHolder iViewHolder = holder;*/
        final int holderPosition = position ;
        List<String> userRoles = DbUtils.getRoles();
        if (holder instanceof PendingDetailsAdapter.FooterViewHolder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;

        }else if(holder instanceof PendingDetailsAdapter.PendingItemHolder){
            PendingItemHolder iViewHolder = (PendingItemHolder) holder;
            if (pendingStatsDBRealmList != null && holderPosition<pendingStatsDBRealmList.size() && pendingStatsDBRealmList.get(holderPosition).isValid()) {
                (iViewHolder).serial_number_tv.setText("" + (holderPosition + 1));
                (iViewHolder).dse_name_tv.setText(pendingStatsDBRealmList.get(holderPosition).getUserName());
                (iViewHolder).pending_total_tv.setText("" + pendingStatsDBRealmList.get(holderPosition).getPendingCount());
                (iViewHolder).avg_delay_tv.setText("" + pendingStatsDBRealmList.get(holderPosition).getMedianDelay() + " d");
                System.out.println("TEM MEMBER: "+pendingStatsDBRealmList.get(holderPosition).getTeam_member());
                if(pendingStatsDBRealmList.get(holderPosition).getTeam_member()==1){
                    (iViewHolder).background_ll.getBackground().setColorFilter(activity.getResources().getColor(R.color.black09), PorterDuff.Mode.SRC_ATOP);
                }else{
                    (iViewHolder).background_ll.getBackground().setColorFilter(activity.getResources().getColor(R.color.greyTint), PorterDuff.Mode.SRC_ATOP);
                }
               // (iViewHolder).background_ll.getBackground().setColorFilter(activity.getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
                if (pendingStatsDBRealmList.get(holderPosition).getVisibleTimes() > 1) {
                    (iViewHolder).count_tv.setText("" + pendingStatsDBRealmList.get(holderPosition).getVisibleTimes() + " times");
                } else {
                    (iViewHolder).count_tv.setText("" + pendingStatsDBRealmList.get(holderPosition).getVisibleTimes() + " time");
                }
                (iViewHolder).done_count_tv.setText("" + Math.abs(pendingStatsDBRealmList.get(holderPosition).getPendingCountDiff()));
                String url = pendingStatsDBRealmList.get(holderPosition).getUserImage();
                if (!url.isEmpty()) {
                    (iViewHolder).imgUser.setVisibility(View.VISIBLE);
                    (iViewHolder).text_user.setVisibility(View.GONE);
                    Picasso.with(activity).load(url).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(((PendingItemHolder) iViewHolder).imgUser);
                } else {
                    (iViewHolder).imgUser.setVisibility(View.GONE);
                    (iViewHolder).text_user.setVisibility(View.VISIBLE);
                    (iViewHolder).text_user.setText("" + pendingStatsDBRealmList.get(holderPosition).getUserName().toUpperCase().substring(0, 1));
                }
                System.out.println("NUMBER USER"+pendingStatsDBRealmList.get(position).getUserPhone()+"MANAGER"+pendingStatsDBRealmList.get(position).getManagerNum()+"TL"+pendingStatsDBRealmList.get(position).getTlNum());
                if((pendingStatsDBRealmList.get(position).getUserPhone()==null ||  pendingStatsDBRealmList.get(position).getUserPhone().isEmpty()) &&
                        (pendingStatsDBRealmList.get(position).getManagerNum()==null || pendingStatsDBRealmList.get(position).getManagerNum().isEmpty())&&
                        (pendingStatsDBRealmList.get(position).getTlNum()==null || pendingStatsDBRealmList.get(position).getTlNum().isEmpty())){
                    (iViewHolder).img_action_plan_call.setVisibility(View.INVISIBLE);
                }else{
                    (iViewHolder).img_action_plan_call.setVisibility(View.VISIBLE);
                }

            }

            (iViewHolder).img_action_plan_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showCallDialog(pendingStatsDBRealmList.get(holderPosition));
                }
            });
            (iViewHolder).serial_number_tv.setText("" + (holderPosition + 1));
        }

    }

    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        if(pendingStatsDBRealmList!=null) {
            return pendingStatsDBRealmList.size()+1;
        }else return 0;
    }


    private class PendingItemHolder extends RecyclerView.ViewHolder {

        TextView dse_name_tv, pending_total_tv, avg_delay_tv, text_user, count_tv, done_count_tv, serial_number_tv;
        ImageView imgUser;
        LinearLayout background_ll;
        ImageView img_action_plan_call;

        public PendingItemHolder(View itemView) {
            super(itemView);
            // parentLayout = itemView;
            serial_number_tv = (TextView) itemView.findViewById(R.id.serial_number);
            dse_name_tv = (TextView) itemView.findViewById(R.id.dse_name);
            pending_total_tv = (TextView) itemView.findViewById(R.id.pending_total);
            avg_delay_tv = (TextView) itemView.findViewById(R.id.avg_delay);
            imgUser = (ImageView) itemView.findViewById(img_user);
            text_user = (TextView) itemView.findViewById(R.id.text_user);
            count_tv = (TextView) itemView.findViewById(R.id.count);
            done_count_tv = (TextView) itemView.findViewById(R.id.done);
            img_action_plan_call = (ImageView) itemView.findViewById(R.id.img_action_plan_call);
            background_ll = (LinearLayout) itemView.findViewById(R.id.background_ll);

        }
    }
    private class FooterViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public FooterViewHolder(View itemView) {
            super(itemView);

        }
    }

    public void showCallDialog(final PendingStatsDB pendingStatsDBObj) {

        if(pendingStatsDBObj.isValid()){

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.call_pending_dialog, null);
        ImageView img_user, img_user1, img_user2;
        TextView text_user, text_user1, text_user2;
        TextView call_name_tv, call_name_tv1, call_name_tv2;
        TextView call_designation_tv, call_designation_tv1, call_designation_tv2;
        LinearLayout top_most_ll, middle_most_ll, lower_most_ll;

        img_user = (ImageView) dialogView.findViewById(R.id.img_user);
        img_user1 = (ImageView) dialogView.findViewById(R.id.img_user_1);
        img_user2 = (ImageView) dialogView.findViewById(R.id.img_user_2);

        text_user = (TextView) dialogView.findViewById(R.id.text_user);
        text_user1 = (TextView) dialogView.findViewById(R.id.text_user_1);
        text_user2 = (TextView) dialogView.findViewById(R.id.text_user_2);

        call_name_tv = (TextView) dialogView.findViewById(call_name);
        call_name_tv1 = (TextView) dialogView.findViewById(R.id.call_name_1);
        call_name_tv2 = (TextView) dialogView.findViewById(R.id.call_name_2);

        call_designation_tv = (TextView) dialogView.findViewById(call_designation);
        call_designation_tv1 = (TextView) dialogView.findViewById(R.id.call_designation_1);
        call_designation_tv2 = (TextView) dialogView.findViewById(R.id.call_designation_2);


        top_most_ll = (LinearLayout) dialogView.findViewById(top_most);
        middle_most_ll = (LinearLayout) dialogView.findViewById(middle_most);
        lower_most_ll = (LinearLayout) dialogView.findViewById(lower_most);

        if (pendingStatsDBObj.getManagerNum()!=null && !pendingStatsDBObj.getManagerNum().isEmpty() && pendingStatsDBObj.isValid()) {
            top_most_ll.setVisibility(View.VISIBLE);
            call_name_tv.setText(pendingStatsDBObj.getManagerName().toString().trim());
            call_designation_tv.setText("Sales Manager");
            if (!pendingStatsDBObj.getManagerImage().isEmpty()) {
                img_user.setVisibility(View.VISIBLE);
                text_user.setVisibility(View.GONE);
                Picasso.with(activity).load(pendingStatsDBObj.getManagerImage()).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(img_user);
            } else {
                img_user.setVisibility(View.GONE);
                text_user.setVisibility(View.VISIBLE);
                text_user.setText("" + pendingStatsDBObj.getManagerName().toUpperCase().substring(0, 1));
            }

        } else {
            top_most_ll.setVisibility(View.GONE);
        }

        if (pendingStatsDBObj.getTlNum()!=null && !pendingStatsDBObj.getTlNum().isEmpty() && pendingStatsDBObj.isValid()) {
            middle_most_ll.setVisibility(View.VISIBLE);
            call_name_tv1.setText(pendingStatsDBObj.getTlName().toString().trim());
            call_designation_tv1.setText("Team Lead");
            if (!pendingStatsDBObj.getTlImage().isEmpty()) {
                img_user1.setVisibility(View.VISIBLE);
                text_user1.setVisibility(View.GONE);
                Picasso.with(activity).load(pendingStatsDBObj.getTlImage()).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(img_user1);
            } else {
                img_user1.setVisibility(View.GONE);
                text_user1.setVisibility(View.VISIBLE);
                text_user1.setText("" + pendingStatsDBObj.getTlName().toUpperCase().substring(0, 1));
            }
        } else {
            middle_most_ll.setVisibility(View.GONE);
        }
        if (pendingStatsDBObj.getUserPhone()!=null && !pendingStatsDBObj.getUserPhone().isEmpty() && pendingStatsDBObj.isValid()) {
            lower_most_ll.setVisibility(View.VISIBLE);
            call_name_tv2.setText(pendingStatsDBObj.getUserName().toString().trim());
            call_designation_tv2.setText("DSE");
            if (!pendingStatsDBObj.getUserImage().isEmpty()) {
                img_user2.setVisibility(View.VISIBLE);
                text_user2.setVisibility(View.GONE);
                Picasso.with(activity).load(pendingStatsDBObj.getUserImage()).placeholder(R.drawable.ic_person_white_48dp).error(R.drawable.ic_person_white_48dp).transform(new CircleTransform()).fit().centerInside().into(img_user2);
            } else {
                img_user2.setVisibility(View.GONE);
                text_user2.setVisibility(View.VISIBLE);
                text_user2.setText("" + pendingStatsDBObj.getUserName().toUpperCase().substring(0, 1));
            }
        } else {
            lower_most_ll.setVisibility(View.GONE);
        }
        top_most_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    logEvent("Pending Calls");
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + pendingStatsDBObj.getManagerNum()));
                    activity.startActivity(callIntent);
                } catch (ActivityNotFoundException activityException) {
                    activityException.printStackTrace();
                }
            }
        });
        middle_most_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    logEvent("Pending Calls");
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + pendingStatsDBObj.getTlNum()));
                    activity.startActivity(callIntent);
                } catch (ActivityNotFoundException activityException) {
                    activityException.printStackTrace();
                }
            }
        });
        lower_most_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    logEvent("Pending Calls");
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + pendingStatsDBObj.getUserPhone()));
                    activity.startActivity(callIntent);
                } catch (ActivityNotFoundException activityException) {
                    activityException.printStackTrace();
                }
            }
        });


        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }   else{
            System.out.println("PendingDetailsAdapter pendingStatsDBObj is not valid.");
        }
}
    private void logEvent(String event){
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, event);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);

        /*if(Answers.getInstance()!=null) {
            Answers.getInstance().logCustom(new CustomEvent("Team Dashboard")
                    .putCustomAttribute("Dealer Name", pref.getDealerName())
                    .putCustomAttribute("IMEI", pref.getImeiNumber())
                    .putCustomAttribute("User Name", pref.getUserName())
                    .putCustomAttribute("Dashboard Type", event)
                    .putCustomAttribute("Roles",   DbUtils.getRolesCombination()));

        }*/
    }
}
