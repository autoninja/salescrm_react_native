package com.salescrm.telephony.model.NewFilter;

import com.salescrm.telephony.utils.WSConstants;

/**
 * Created by prateek on 6/8/18.
 */

public class EtvbrFilterApply {

    boolean applyFilter;

    public EtvbrFilterApply(boolean applyFilter) {
        this.applyFilter = applyFilter;
    }

    public boolean isApplyFilter() {
        return applyFilter;
    }
}

