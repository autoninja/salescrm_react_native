package com.salescrm.telephony.fragments.retExcFin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.salescrm.telephony.R;
import com.salescrm.telephony.clevertaputil.CleverTapConstants;
import com.salescrm.telephony.clevertaputil.CleverTapPush;
import com.salescrm.telephony.dbOperation.DbUtils;
import com.salescrm.telephony.preferences.Preferences;
import com.salescrm.telephony.utils.WSConstants;

import java.util.HashMap;

/**
 * Created by prateek on 22/9/17.
 */

public class RefContainerFragment extends Fragment {
    public LinearLayout footerOption;
    Fragment fragment;
    FragmentTransaction transaction;
    private TextView humanImage;
    private TextView carImage;
    private Drawable drawableHumanColor, drawableCarColor, drawableHumanTrans, drawableCarTrans;
    private Drawable drawableBikeColor, drawableBikeTrans;
    private Preferences pref;
    private int locationsSize = 0;
//    private ETVBRLDashboardSwiped etvbrlDashboardSwiped = ETVBRLDashboardSwiped.getInstance(new ETVBRLDashboardSwipedListener() {
//        @Override
//        public void onETVBRLDashboardSwiped(int position, int tabCount) {
//            if (position == 1 && fragment != null) {
//                System.out.println("SwipedRef:" + position);
//                logCleverTapEvent();
//            }
//        }
//    }, 1);

    public static RefContainerFragment newInstance(int i, String etvbr) {
        Bundle args = new Bundle();
        args.putInt("someInt", i);
        args.putString("someTitle", etvbr);
        RefContainerFragment fragment = new RefContainerFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        /*if(isVisibleToUser){
            WSConstants.ETVBR_TAB_OPENED = 2;
        }*/
        System.out.println("IS Fragment Visible to User:"+isVisibleToUser);
        if (isVisibleToUser) {
            logCleverTapEvent();
            WSConstants.ETVBR_TAB_OPENED = 2;
            /*fragment = new RefLocationFragment().newInstance(new EtvbrFooterChangeListener() {
                @Override
                public void setFooterVisible(boolean value) {
                    System.out.println("Container Called : Interface,  "+value);
                    if(value == false) {
                        footerOption.setVisibility(View.GONE);
                    }else {
                        footerOption.setVisibility(View.VISIBLE);
                    }
                    humanImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableHumanColor, null, null);
                    if(DbUtils.isBike()){
                        carImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableBikeTrans, null, null);
                        carImage.setText("Bike Model");
                    }else {
                        carImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableCarTrans, null, null);
                        carImage.setText("Car Model");
                    }
                    humanImage.setTextColor(Color.parseColor("#FF113059"));
                    carImage.setTextColor(Color.parseColor("#FF787878"));
                }
            });
            callFragment(fragment);*/
            Intent intentAddevent = new Intent("show_addEventsButtonForBroadcastReceiver");
            intentAddevent.putExtra("showAddEvent", false);
            getActivity().sendBroadcast(intentAddevent);
        }

        /*if(isVisibleToUser){
            fragment = new RefLocationFragment().newInstance(new EtvbrFooterChangeListener() {
                @Override
                public void setFooterVisible(boolean value) {
                    System.out.println("Container Called : Interface,  "+value);
                    if(value == false) {
                        footerOption.setVisibility(View.GONE);
                    }else {
                        footerOption.setVisibility(View.VISIBLE);
                    }
                }
            });
            callFragment(fragment);
        }*/
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ref_container_fragment, container, false);
        pref = Preferences.getInstance();
        System.out.println("Called RefCon onCreate");
        pref.load(getContext());
        locationsSize = DbUtils.getLocationsName().size();
        footerOption = (LinearLayout) rootView.findViewById(R.id.footer_options);
        humanImage = (TextView) rootView.findViewById(R.id.human_image);
        carImage = (TextView) rootView.findViewById(R.id.car_image);
        drawableHumanColor = VectorDrawableCompat
                .create(getActivity().getResources(), R.drawable.ic_human_color, null);
        drawableHumanTrans = VectorDrawableCompat
                .create(getActivity().getResources(), R.drawable.ic_human_transparent, null);
        drawableCarColor = VectorDrawableCompat
                .create(getActivity().getResources(), R.drawable.ic_car_color, null);
        drawableCarTrans = VectorDrawableCompat
                .create(getActivity().getResources(), R.drawable.ic_car_transparent, null);
        drawableBikeColor = VectorDrawableCompat
                .create(getActivity().getResources(), R.drawable.ic_bike_color, null);
        drawableBikeTrans = VectorDrawableCompat
                .create(getActivity().getResources(), R.drawable.ic_bike_transparent, null);

        humanImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableHumanColor, null, null);
        if (DbUtils.isBike()) {
            carImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableBikeTrans, null, null);
            carImage.setText("Bike Model");
        } else {
            carImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableCarTrans, null, null);
            carImage.setText("Car Model");
        }
        humanImage.setTextColor(Color.parseColor("#FF113059"));
        carImage.setTextColor(Color.parseColor("#FF787878"));
        //fragment = new EtvbrGMFragment();

       /* if(footerOption.getVisibility() == View.VISIBLE) {
            logEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_TEAMS);
        }
        else {
            logEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_ETVBR_LOCATIONS);
        }*/

        fragment = new RefLocationFragment().newInstance(new EtvbrFooterChangeListener() {
            @Override
            public void setFooterVisible(boolean value) {
                System.out.println("Container Called : Interface,  " + value);
                if (value == false) {
                    footerOption.setVisibility(View.GONE);
                } else {
                    footerOption.setVisibility(View.VISIBLE);
                }
            }
        });
        callFragment(fragment);

        carImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TEAM_DASHBOARD_FLAG = 1;

                logEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_CARS);

                fragment = new RefCarsFragment();
                callFragment(fragment);
                humanImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableHumanTrans, null, null);
                if (DbUtils.isBike()) {
                    carImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableBikeColor, null, null);
                    carImage.setText("Bike Model");
                } else {
                    carImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableCarColor, null, null);
                    carImage.setText("Car Model");
                }
                humanImage.setTextColor(Color.parseColor("#FF787878"));
                carImage.setTextColor(Color.parseColor("#FF113059"));
            }
        });

        humanImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TEAM_DASHBOARD_FLAG = 0;
                //fragment = new EtvbrGMFragment();
                logEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_TEAMS);

                fragment = new RefLocationFragment().newInstance(new EtvbrFooterChangeListener() {
                    @Override
                    public void setFooterVisible(boolean value) {
                        System.out.println("Container Called : Interface,  " + value);
                        if (value == false) {
                            footerOption.setVisibility(View.GONE);
                        } else {
                            footerOption.setVisibility(View.VISIBLE);
                        }
                    }
                });
                humanImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableHumanColor, null, null);
                if (DbUtils.isBike()) {
                    carImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableBikeTrans, null, null);
                    carImage.setText("Bike Model");
                } else {
                    carImage.setCompoundDrawablesWithIntrinsicBounds(null, drawableCarTrans, null, null);
                    carImage.setText("Car Model");
                }
                humanImage.setTextColor(Color.parseColor("#FF113059"));
                carImage.setTextColor(Color.parseColor("#FF787878"));
                callFragment(fragment);

            }
        });

        return rootView;
    }

    private void callFragment(Fragment fragment) {
        transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.ref_container, fragment); // give your fragment container id in first parameter
        transaction.addToBackStack(null);  // if written, this transaction will be added to backstack
        transaction.commitAllowingStateLoss();
    }

    private void logEvent(String event) {

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(CleverTapConstants.EVENT_TEAM_DASHBOARD_KEY_TYPE, event);
        CleverTapPush.pushEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD, hashMap);

    }

   /* public static void setFooterVisible(boolean visible){
        System.out.println("Container Called: "+visible);
        if(visible == false) {
            footerOption.setVisibility(View.GONE);
        }else {
            footerOption.setVisibility(View.VISIBLE);
        }
    }*/

    private void logCleverTapEvent() {
        if(fragment!=null) {
            if (footerOption.getVisibility() == View.VISIBLE || locationsSize <= 1) {
                if (fragment instanceof RefCarsFragment) {
                    logEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_CARS);
                } else {
                    logEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_TEAMS);

                }
            } else {
                logEvent(CleverTapConstants.EVENT_TEAM_DASHBOARD_TYPE_VALUE_EXCH_FIN_LOCATIONS);
            }
        }
    }

    interface EtvbrFooterChangeListener {
        void setFooterVisible(boolean value);
    }
    @Override
    public void onResume() {
        super.onResume();
        try {
            if(getActivity()!=null) {
                getActivity().registerReceiver(broadcastReceiver, new IntentFilter("LOG_LIVE_EF"));
            }

        } catch (Exception e) {

        }
    }
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals("LOG_LIVE_EF")) {
                logCleverTapEvent();
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        try {
            if(getActivity()!=null) {
                getActivity().unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {

        }
    }

}
