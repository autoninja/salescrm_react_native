import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native'

import styles from '../../styles/styles'

export default class TodayTasksHeader extends Component {
  constructor(props){
    super(props);
    this.state= {expand:false, showImagePlaceHolder:true};
  }

  render() {

          return(
            <View style={{flexDirection: 'row', height:30, backgroundColor:'#fff', borderColor:'#babdbe', borderWidth:0.8,alignItems:'center'}}>

            <View style={{flex:1.5, alignItems:'center'}} >

            </View>


            <View style= {{flex:1,height:'100%'}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#2e5e86', fontWeight:'700'}} >Call</Text>
              </View>

            </View>

            <View style= {{flex:1,height:'100%',}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
                <Text style={{ color:'#2e5e86', fontWeight:'800'}} >TD</Text>
              </View>
            </View>



            <View style= {{flex:1,height:'100%',}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
              <Text style={{ color:'#2e5e86', fontWeight:'800'}} >V</Text>
              </View>
            </View>

            <View style= {{flex:1,height:'100%',}}>
              <View style= {{flex:2,justifyContent: 'center',alignItems: 'center'}}>
              <Text style={{ color:'#2e5e86', fontWeight:'800'}} >PB</Text>
              </View>
            </View>


            <View style= {{flex:1,height:'100%',justifyContent: "center",alignItems: "center"}}>
              <Text style={{ color:'#2e5e86', fontWeight:'800'}} >D</Text>
            </View>

            <View style= {{flex:1,height:'100%',justifyContent: "center",alignItems: "center"}}>
            <Text style={{ color:'#F5A623', fontWeight:'800'}} >Pnd</Text>
            </View>
            {
              // <View style= {{flex:1,height:'100%',justifyContent: "center",alignItems: "center"}}>
              //  <Image resize='center' style={{height:13, width:13,}} source={require('../../images/ic_uncalled_tasks.png')}/>
              // </View>
            }






            </View>
          );
  }
}
